package com.nextimagemedical.adjportal;

import org.springframework.web.multipart.commons.CommonsMultipartFile;

public class FileData {
	private CommonsMultipartFile fileData;

	public FileData() {
	}

	public CommonsMultipartFile getFileData() {
		return fileData;
	}

	public void setFileData(CommonsMultipartFile fileData) {
		this.fileData = fileData;
	}
	
	
}
