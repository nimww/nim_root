package com.nextimagemedical.adjportal;

public class ContactUs {
	private String fName;
	private String lName;
	private String contactEmail;
	private String area;
	private String pre;
	private String exch;
	private String ta;
	private String email;
	private String phone;
	public ContactUs(){}
	public ContactUs(String fName, String lName, String contactEmail, String area, String pre, String exch, String ta, String email, String phone) {
		this.fName = fName;
		this.lName = lName;
		this.contactEmail = contactEmail;
		this.area = area;
		this.pre = pre;
		this.exch = exch;
		this.ta = ta;
		this.email = email;
		this.phone = phone;
	}
	public String getfName() {
		return fName;
	}
	public void setfName(String fName) {
		this.fName = fName;
	}
	public String getlName() {
		return lName;
	}
	public void setlName(String lName) {
		this.lName = lName;
	}
	public String getContactEmail() {
		return contactEmail;
	}
	public void setContactEmail(String contactEmail) {
		this.contactEmail = contactEmail;
	}
	public String getArea() {
		return area;
	}
	public void setArea(String area) {
		this.area = area;
	}
	public String getPre() {
		return pre;
	}
	public void setPre(String pre) {
		this.pre = pre;
	}
	public String getExch() {
		return exch;
	}
	public void setExch(String exch) {
		this.exch = exch;
	}
	public String getTa() {
		return ta;
	}
	public void setTa(String ta) {
		this.ta = ta;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	
	
}
