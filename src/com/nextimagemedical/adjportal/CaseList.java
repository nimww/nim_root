package com.nextimagemedical.adjportal;

import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;

import com.nextimagemedical.Pass.LinkEncryption;

public class CaseList {
	private String patientName;
	private String claim;
	private String recDate;
	private int encounterid;
	
	public CaseList(String patientName, String claim, String recDate, int encounterid) {
		this.patientName = patientName;
		this.claim = claim;
		this.recDate = recDate;
		this.encounterid = encounterid;
	}
	
	public String getPatientName() {
		return patientName;
	}
	public void setPatientName(String patientName) {
		this.patientName = patientName;
	}
	public String getClaim() {
		return claim;
	}
	public void setClaim(String claim) {
		this.claim = claim;
	}
	public String getRecDate() {
		return recDate;
	}
	public void setRecDate(String recDate) {
		this.recDate = recDate;
	}
	public int getEncounterid() {
		return encounterid;
	}
	public String getEncounteridEncrypt() throws UnsupportedEncodingException, GeneralSecurityException {
		return LinkEncryption.encrypt(Integer.toString(encounterid));
	}
	public void setEncounterid(int encounterid) {
		this.encounterid = encounterid;
	}
	
		
}
