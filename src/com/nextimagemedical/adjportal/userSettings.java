package com.nextimagemedical.adjportal;

public class userSettings{
	private String groupEmail;
	private String groupName;
	private String groupBranch;
	private String directPhone1;
	private String directPhone2;
	private String directPhone3;
	private String groupFax1;
	private String groupFax2;
	private String groupFax3;
	private String directMobile1;
	private String directMobile2;
	private String directMobile3;
	private int scheduled;
	private int reports;
	private int other;
	private String confirmPassword;
	private String newPassword;
	private String verifyNewPassword;
	public userSettings() {
	}
	public userSettings(String groupEmail, String groupName, String groupBranch, String directPhone1, String directPhone2, String directPhone3, String groupFax1, String groupFax2, String groupFax3, String directMobile1, String directMobile2, String directMobile3, int scheduled, int reports, int other, String confirmPassword, String newPassword, String verifyNewPassword) {
		this.groupEmail = groupEmail;
		this.groupName = groupName;
		this.groupBranch = groupBranch;
		this.directPhone1 = directPhone1;
		this.directPhone2 = directPhone2;
		this.directPhone3 = directPhone3;
		this.groupFax1 = groupFax1;
		this.groupFax2 = groupFax2;
		this.groupFax3 = groupFax3;
		this.directMobile1 = directMobile1;
		this.directMobile2 = directMobile2;
		this.directMobile3 = directMobile3;
		this.scheduled = scheduled;
		this.reports = reports;
		this.other = other;
		this.confirmPassword = confirmPassword;
		this.newPassword = newPassword;
		this.verifyNewPassword = verifyNewPassword;
	}
	public String getGroupEmail() {
		return groupEmail;
	}
	public void setGroupEmail(String groupEmail) {
		this.groupEmail = groupEmail;
	}
	public String getGroupName() {
		return groupName;
	}
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	public String getGroupBranch() {
		return groupBranch;
	}
	public void setGroupBranch(String groupBranch) {
		this.groupBranch = groupBranch;
	}
	public String getDirectPhone1() {
		return directPhone1;
	}
	public void setDirectPhone1(String directPhone1) {
		this.directPhone1 = directPhone1;
	}
	public String getDirectPhone2() {
		return directPhone2;
	}
	public void setDirectPhone2(String directPhone2) {
		this.directPhone2 = directPhone2;
	}
	public String getDirectPhone3() {
		return directPhone3;
	}
	public void setDirectPhone3(String directPhone3) {
		this.directPhone3 = directPhone3;
	}
	public String getGroupFax1() {
		return groupFax1;
	}
	public void setGroupFax1(String groupFax1) {
		this.groupFax1 = groupFax1;
	}
	public String getGroupFax2() {
		return groupFax2;
	}
	public void setGroupFax2(String groupFax2) {
		this.groupFax2 = groupFax2;
	}
	public String getGroupFax3() {
		return groupFax3;
	}
	public void setGroupFax3(String groupFax3) {
		this.groupFax3 = groupFax3;
	}
	public String getDirectMobile1() {
		return directMobile1;
	}
	public void setDirectMobile1(String directMobile1) {
		this.directMobile1 = directMobile1;
	}
	public String getDirectMobile2() {
		return directMobile2;
	}
	public void setDirectMobile2(String directMobile2) {
		this.directMobile2 = directMobile2;
	}
	public String getDirectMobile3() {
		return directMobile3;
	}
	public void setDirectMobile3(String directMobile3) {
		this.directMobile3 = directMobile3;
	}
	public int getScheduled() {
		return scheduled;
	}
	public void setScheduled(int scheduled) {
		this.scheduled = scheduled;
	}
	public int getReports() {
		return reports;
	}
	public void setReports(int reports) {
		this.reports = reports;
	}
	public int getOther() {
		return other;
	}
	public void setOther(int other) {
		this.other = other;
	}
	public String getConfirmPassword() {
		return confirmPassword;
	}
	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}
	public String getNewPassword() {
		return newPassword;
	}
	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}
	public String getVerifyNewPassword() {
		return verifyNewPassword;
	}
	public void setVerifyNewPassword(String verifyNewPassword) {
		this.verifyNewPassword = verifyNewPassword;
	}
}