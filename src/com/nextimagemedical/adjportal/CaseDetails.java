package com.nextimagemedical.adjportal;

import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;
import java.util.List;
import com.nextimagemedical.Pass.LinkEncryption;
/*
 * Do not auto generate, custom methods in place
 * 
 */
public class CaseDetails {
	private String patientName;
	private String claim;
	private String DOI;
	private String payerName;
	private String adjName;
	private String receieveDate;
	private int authDocId;
	private int rxDocId;
	private int reportDocid;
	private int isStat=0;
	private int encounterId;
	private String scanpass;
	private String mdName;
	private int caseStatus;
	private List<ServiceItem> serviceItem;
	private apptDetails apptdetails;
	public CaseDetails(String patientName, String claim, String dOI, String payerName, String adjName, String receieveDate, int authDocId, int rxDocId, int reportDocid, int isStat, int encounterId, String scanpass, String mdName, int caseStatus, List<ServiceItem> serviceItem, apptDetails apptdetails) {
		this.patientName = patientName;
		this.claim = claim;
		DOI = dOI;
		this.payerName = payerName;
		this.adjName = adjName;
		this.receieveDate = receieveDate;
		this.authDocId = authDocId;
		this.rxDocId = rxDocId;
		this.reportDocid = reportDocid;
		this.isStat = isStat;
		this.encounterId = encounterId;
		this.scanpass = scanpass;
		this.mdName = mdName;
		this.caseStatus = caseStatus;
		this.serviceItem = serviceItem;
		this.apptdetails = apptdetails;
	}
	public String getPatientName() {
		return patientName;
	}
	public void setPatientName(String patientName) {
		this.patientName = patientName;
	}
	public String getClaim() {
		return claim;
	}
	public void setClaim(String claim) {
		this.claim = claim;
	}
	public String getDOI() {
		return DOI;
	}
	public void setDOI(String dOI) {
		DOI = dOI;
	}
	public String getPayerName() {
		return payerName;
	}
	public void setPayerName(String payerName) {
		this.payerName = payerName;
	}
	public String getAdjName() {
		return adjName;
	}
	public void setAdjName(String adjName) {
		this.adjName = adjName;
	}
	public String getReceieveDate() {
		return receieveDate;
	}
	public void setReceieveDate(String receieveDate) {
		this.receieveDate = receieveDate;
	}
	public int getAuthDocId() {
		return authDocId;
	}
	public String getAuthDocIdEncrypt() throws UnsupportedEncodingException, GeneralSecurityException {
		return LinkEncryption.encrypt(Integer.toString(authDocId));
	}
	public void setAuthDocId(int authDocId) {
		this.authDocId = authDocId;
	}
	public int getRxDocId() {
		return rxDocId;
	}
	public String getRxDocIdEncrypt() throws UnsupportedEncodingException, GeneralSecurityException {
		return LinkEncryption.encrypt(Integer.toString(rxDocId));
	}
	public void setRxDocId(int rxDocId) {
		this.rxDocId = rxDocId;
	}
	public int getReportDocid() {
		return reportDocid;
	}
	public String getReportDocidEncrypt() throws UnsupportedEncodingException, GeneralSecurityException {
		return LinkEncryption.encrypt(Integer.toString(reportDocid));
	}
	public void setReportDocid(int reportDocid) {
		this.reportDocid = reportDocid;
	}
	public int getIsStat() {
		return isStat;
	}
	public void setIsStat(int isStat) {
		this.isStat = isStat;
	}
	public int getEncounterId() {
		return encounterId;
	}
	public String getEncounterIdEncrypt() throws UnsupportedEncodingException, GeneralSecurityException {
		return LinkEncryption.encrypt(Integer.toString(encounterId));
	}
	public void setEncounterId(int encounterId) {
		this.encounterId = encounterId;
	}
	public String getScanpass() {
		return scanpass;
	}
	public void setScanpass(String scanpass) {
		this.scanpass = scanpass;
	}
	public String getMdName() {
		return mdName;
	}
	public void setMdName(String mdName) {
		this.mdName = mdName;
	}
	public int getCaseStatus() {
		return caseStatus;
	}
	public void setCaseStatus(int caseStatus) {
		this.caseStatus = caseStatus;
	}
	public List<ServiceItem> getServiceItem() {
		return serviceItem;
	}
	public void setServiceItem(List<ServiceItem> serviceItem) {
		this.serviceItem = serviceItem;
	}
	public apptDetails getApptdetails() {
		return apptdetails;
	}
	public void setApptdetails(apptDetails apptdetails) {
		this.apptdetails = apptdetails;
	}
	
}
