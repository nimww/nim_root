package com.nextimagemedical.adjportal;

public class ServiceItem {
	private String cpt;
	private String bodyPart;
	private boolean isVoid;
	public ServiceItem(String cpt, String bodyPart, boolean isVoid) {
		this.cpt = cpt;
		this.bodyPart = bodyPart;
		this.isVoid = isVoid;
	}
	public String getCpt() {
		return cpt;
	}
	public void setCpt(String cpt) {
		this.cpt = cpt;
	}
	public String getBodyPart() {
		return bodyPart;
	}
	public void setBodyPart(String bodyPart) {
		this.bodyPart = bodyPart;
	}
	public boolean isVoid() {
		return isVoid;
	}
	public void setVoid(boolean isVoid) {
		this.isVoid = isVoid;
	}
}
