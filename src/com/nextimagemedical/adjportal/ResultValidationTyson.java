package com.nextimagemedical.adjportal;

public class ResultValidationTyson
{
  boolean goodReferral = false;

  boolean LinkType = false;

  boolean claimNumber = false;
  boolean adjusterFirstName = false;
  boolean adjusterLastName = false;
  boolean ncmFirstName = false;
  boolean ncmLastName = false;
  boolean teamMemberFirstName = false;
  boolean teamMemberLastName = false;
  boolean teamMemberAddress = false;
  boolean teamMemberCity = false;
  boolean teamMemberState = false;
  boolean teamMemberZip = false;

  boolean referringPhysicianFirstName = false;
  boolean referringPhysicianLastName = false;
  boolean physicianPhone1 = false;
  boolean physicianPhone2 = false;
  boolean physicianPhone3 = false;
  boolean physicianFax1 = false;
  boolean physicianFax2 = false;
  boolean physicianFax3 = false;
  boolean proc = false;
  boolean bp = false;
  boolean RuleOut = false;

  public boolean isGoodReferral() {
    return this.goodReferral;
  }
  public void setGoodReferral(boolean goodReferral) {
    this.goodReferral = goodReferral;
  }

  public boolean isLinkType()
  {
    return this.LinkType;
  }
  public void setLinkType(boolean LinkType) {
    this.LinkType = LinkType;
  }

  public boolean isClaimNumber()
  {
    return this.claimNumber;
  }
  public void setClaimNumber(boolean claimNumber) {
    this.claimNumber = claimNumber;
  }
  public boolean isAdjusterFirstName() {
    return this.adjusterFirstName;
  }
  public void setAdjusterFirstName(boolean adjusterFirstName) {
    this.adjusterFirstName = adjusterFirstName;
  }
  public boolean isAdjusterLastName() {
    return this.adjusterLastName;
  }
  public void setAdjusterLastName(boolean adjusterLastName) {
    this.adjusterLastName = adjusterLastName;
  }
  public boolean isNcmFirstName() {
    return this.ncmFirstName;
  }
  public void setNcmFirstName(boolean ncmFirstName) {
    this.ncmFirstName = ncmFirstName;
  }
  public boolean isNcmLastName() {
    return this.ncmLastName;
  }
  public void setNcmLastName(boolean ncmLastName) {
    this.ncmLastName = ncmLastName;
  }
  public boolean isTeamMemberFirstName() {
    return this.teamMemberFirstName;
  }
  public void setTeamMemberFirstName(boolean teamMemberFirstName) {
    this.teamMemberFirstName = teamMemberFirstName;
  }
  public boolean isTeamMemberLastName() {
    return this.teamMemberLastName;
  }
  public void setTeamMemberLastName(boolean teamMemberLastName) {
    this.teamMemberLastName = teamMemberLastName;
  }
  public boolean isTeamMemberAddress() {
    return this.teamMemberAddress;
  }
  public void setTeamMemberAddress(boolean teamMemberAddress) {
    this.teamMemberAddress = teamMemberAddress;
  }
  public boolean isTeamMemberCity() {
    return this.teamMemberCity;
  }
  public void setTeamMemberCity(boolean teamMemberCity) {
    this.teamMemberCity = teamMemberCity;
  }
  public boolean isTeamMemberState() {
    return this.teamMemberState;
  }
  public void setTeamMemberState(boolean teamMemberState) {
    this.teamMemberState = teamMemberState;
  }
  public boolean isTeamMemberZip() {
    return this.teamMemberZip;
  }
  public void setTeamMemberZip(boolean teamMemberZip) {
    this.teamMemberZip = teamMemberZip;
  }

  public boolean isReferringPhysicianFirstName()
  {
    return this.referringPhysicianFirstName;
  }
  public void setReferringPhysicianFirstName(boolean referringPhysicianFirstName) {
    this.referringPhysicianFirstName = referringPhysicianFirstName;
  }
  public boolean isReferringPhysicianLastName() {
    return this.referringPhysicianLastName;
  }
  public void setReferringPhysicianLastName(boolean referringPhysicianLastName) {
    this.referringPhysicianLastName = referringPhysicianLastName;
  }
  public boolean isPhysicianPhone1() {
    return this.physicianPhone1;
  }
  public void setPhysicianPhone1(boolean physicianPhone1) {
    this.physicianPhone1 = physicianPhone1;
  }
  public boolean isPhysicianPhone2() {
    return this.physicianPhone2;
  }
  public void setPhysicianPhone2(boolean physicianPhone2) {
    this.physicianPhone2 = physicianPhone2;
  }
  public boolean isPhysicianPhone3() {
    return this.physicianPhone3;
  }
  public void setPhysicianPhone3(boolean physicianPhone3) {
    this.physicianPhone3 = physicianPhone3;
  }

  public boolean isPhysicianFax1() {
    return this.physicianFax1;
  }
  public void setPhysicianFax1(boolean physicianFax1) {
    this.physicianFax1 = physicianFax1;
  }
  public boolean isPhysicianFax2() {
    return this.physicianFax2;
  }
  public void setPhysicianFax2(boolean physicianFax2) {
    this.physicianFax2 = physicianFax2;
  }
  public boolean isPhysicianFax3() {
    return this.physicianFax3;
  }
  public void setPhysicianFax3(boolean physicianFax3) {
    this.physicianFax3 = physicianFax3;
  }
  public boolean isProc() {
    return this.proc;
  }
  public void setProc(boolean proc) {
    this.proc = proc;
  }
  public boolean isBp() {
    return this.bp;
  }
  public void setBp(boolean bp) {
    this.bp = bp;
  }
  public boolean isRuleOut() {
    return this.RuleOut;
  }
  public void setRuleOut(boolean ruleOut) {
    this.RuleOut = ruleOut;
  }
}