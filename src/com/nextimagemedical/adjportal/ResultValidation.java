package com.nextimagemedical.adjportal;

public class ResultValidation
{
  boolean goodReferral = false;
  boolean fileRx = false;
  boolean fileOther = false;
  boolean CaseClaimNumber1 = false;
  boolean PatientFirstName = false;
  boolean PatientLastName = false;
  boolean refFirstName = false;
  boolean refLastName = false;
  boolean AdjusterPhone1 = false;
  boolean AdjusterPhone2 = false;
  boolean AdjusterPhone3 = false;
  boolean AdjusterFax1 = false;
  boolean AdjusterFax2 = false;
  boolean AdjusterFax3 = false;
  boolean PatientDOB = false;
  boolean PatientDOI = false;
  boolean PatientAddress1 = false;
  boolean PatientCity = false;
  boolean PatientStateID = false;
  boolean PatientZip = false;
  boolean PatientHomePhone1 = false;
  boolean PatientHomePhone2 = false;
  boolean PatientHomePhone3 = false;
  boolean MDName = false;
  boolean MDPhone1 = false;
  boolean MDPhone2 = false;
  boolean MDPhone3 = false;
  boolean MDFax1 = false;
  boolean MDFax2 = false;
  boolean MDFax3 = false;
  boolean proc = false;
  boolean bp = false;
  boolean RuleOut = false;

  public boolean isGoodReferral() {
    return this.goodReferral;
  }

  public void setGoodReferral(boolean goodReferral) {
    this.goodReferral = goodReferral;
  }

  public boolean isFileRx() {
    return this.fileRx;
  }

  public void setFileRx(boolean fileRx) {
    this.fileRx = fileRx;
  }

  public boolean isFileOther() {
    return this.fileOther;
  }

  public void setFileOther(boolean fileOther) {
    this.fileOther = fileOther;
  }

  public boolean isCaseClaimNumber1() {
    return this.CaseClaimNumber1;
  }

  public void setCaseClaimNumber1(boolean CaseClaimNumber1) {
    this.CaseClaimNumber1 = CaseClaimNumber1;
  }

  public boolean isPatientFirstName() {
    return this.PatientFirstName;
  }

  public void setPatientFirstName(boolean PatientFirstName) {
    this.PatientFirstName = PatientFirstName;
  }

  public boolean isPatientLastName() {
    return this.PatientLastName;
  }

  public void setPatientLastName(boolean PatientLastName) {
    this.PatientLastName = PatientLastName;
  }

  public boolean isRefFirstName()
  {
    return this.refFirstName;
  }

  public void setRefFirstName(boolean refFirstName) {
    this.refFirstName = refFirstName;
  }

  public boolean isRefLastName() {
    return this.refLastName;
  }

  public void setRefLastName(boolean refLastName) {
    this.refLastName = refLastName;
  }

  public boolean isAdjusterPhone1() {
    return this.AdjusterPhone1;
  }

  public void setAdjusterPhone1(boolean adjusterPhone1) {
    this.AdjusterPhone1 = adjusterPhone1;
  }

  public boolean isAdjusterPhone2() {
    return this.AdjusterPhone2;
  }

  public void setAdjusterPhone2(boolean adjusterPhone2) {
    this.AdjusterPhone2 = adjusterPhone2;
  }

  public boolean isAdjusterPhone3() {
    return this.AdjusterPhone3;
  }

  public void setAdjusterPhone3(boolean adjusterPhone3) {
    this.AdjusterPhone3 = adjusterPhone3;
  }

  public boolean isAdjusterFax1() {
    return this.AdjusterFax1;
  }

  public void setAdjusterFax1(boolean adjusterFax1) {
    this.AdjusterFax1 = adjusterFax1;
  }

  public boolean isAdjusterFax2() {
    return this.AdjusterFax2;
  }

  public void setAdjusterFax2(boolean adjusterFax2) {
    this.AdjusterFax2 = adjusterFax2;
  }

  public boolean isAdjusterFax3() {
    return this.AdjusterFax3;
  }

  public void setAdjusterFax3(boolean adjusterFax3) {
    this.AdjusterFax3 = adjusterFax3;
  }

  public boolean isPatientDOB() {
    return this.PatientDOB;
  }

  public void setPatientDOB(boolean patientDOB) {
    this.PatientDOB = patientDOB;
  }

  public boolean isPatientDOI() {
    return this.PatientDOI;
  }

  public void setPatientDOI(boolean patientDOI) {
    this.PatientDOI = patientDOI;
  }

  public boolean isPatientAddress1() {
    return this.PatientAddress1;
  }

  public void setPatientAddress1(boolean patientAddress1) {
    this.PatientAddress1 = patientAddress1;
  }

  public boolean isPatientCity() {
    return this.PatientCity;
  }

  public void setPatientCity(boolean patientCity) {
    this.PatientCity = patientCity;
  }

  public boolean isPatientStateID() {
    return this.PatientStateID;
  }

  public void setPatientStateID(boolean patientStateID) {
    this.PatientStateID = patientStateID;
  }

  public boolean isPatientZip() {
    return this.PatientZip;
  }

  public void setPatientZip(boolean patientZip) {
    this.PatientZip = patientZip;
  }

  public boolean isPatientHomePhone1() {
    return this.PatientHomePhone1;
  }

  public void setPatientHomePhone1(boolean patientHomePhone1) {
    this.PatientHomePhone1 = patientHomePhone1;
  }

  public boolean isPatientHomePhone2() {
    return this.PatientHomePhone2;
  }

  public void setPatientHomePhone2(boolean patientHomePhone2) {
    this.PatientHomePhone2 = patientHomePhone2;
  }

  public boolean isPatientHomePhone3() {
    return this.PatientHomePhone3;
  }

  public void setPatientHomePhone3(boolean patientHomePhone3) {
    this.PatientHomePhone3 = patientHomePhone3;
  }

  public boolean isMDName() {
    return this.MDName;
  }

  public void setMDName(boolean mDName) {
    this.MDName = mDName;
  }

  public boolean isMDPhone1() {
    return this.MDPhone1;
  }

  public void setMDPhone1(boolean mDPhone1) {
    this.MDPhone1 = mDPhone1;
  }

  public boolean isMDPhone2() {
    return this.MDPhone2;
  }

  public void setMDPhone2(boolean mDPhone2) {
    this.MDPhone2 = mDPhone2;
  }

  public boolean isMDPhone3() {
    return this.MDPhone3;
  }

  public void setMDPhone3(boolean mDPhone3) {
    this.MDPhone3 = mDPhone3;
  }

  public boolean isMDFax1() {
    return this.MDFax1;
  }

  public void setMDFax1(boolean mDFax1) {
    this.MDFax1 = mDFax1;
  }

  public boolean isMDFax2() {
    return this.MDFax2;
  }

  public void setMDFax2(boolean mDFax2) {
    this.MDFax2 = mDFax2;
  }

  public boolean isMDFax3() {
    return this.MDFax3;
  }

  public void setMDFax3(boolean mDFax3) {
    this.MDFax3 = mDFax3;
  }

  public boolean isProc() {
    return this.proc;
  }

  public void setProc(boolean proc) {
    this.proc = proc;
  }

  public boolean isBp() {
    return this.bp;
  }

  public void setBp(boolean bp) {
    this.bp = bp;
  }

  public boolean isRuleOut() {
    return this.RuleOut;
  }

  public void setRuleOut(boolean ruleOut) {
    this.RuleOut = ruleOut;
  }
}