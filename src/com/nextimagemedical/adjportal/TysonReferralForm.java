﻿package com.nextimagemedical.adjportal;

import java.util.List;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

public class TysonReferralForm
{
  CommonsMultipartFile upload1;
  CommonsMultipartFile upload2;
  String claimsOffice;
  String plantLocation;
  int linkType;
  String claimNumber;
  String precertNumber;
  int adjusterUID;
  String adjusterFirstName;
  String adjusterLastName;
  String adjusterEmail;
  String adjusterPhone1;
  String adjusterPhone2;
  String adjusterPhone3;
  String adjusterFax1;
  String adjusterFax2;
  String adjusterFax3;
  int ncmUID;
  String ncmFirstName;
  String ncmLastName;
  String ncmEmail;
  String ncmPhone1;
  String ncmPhone2;
  String ncmPhone3;
  String ncmFax1;
  String ncmFax2;
  String ncmFax3;
  int caseAdmin1UID;
  String caseAdmin1FirstName;
  String caseAdmin1LastName;
  String caseAdmin1Email;
  String caseAdmin1Phone1;
  String caseAdmin1Phone2;
  String caseAdmin1Phone3;
  String caseAdmin1Fax1;
  String caseAdmin1Fax2;
  String caseAdmin1Fax3;
  int caseAdmin2UID;
  String[] caseManagerFirstName;
  String[] caseManagerLastName;
  String[] caseManagerEmail;
  String[] cmPhoneAreaCode;
  String[] cmPhonePrefix;
  String[] cmPhoneExchange;
  String[] cmfaxAreaCode;
  String[] cmfaxPrefix;
  String[] cmfaxExchange;
  int caseAdmin3UID;
  int caseAdmin4UID;
  String teamMemberFirstName;
  String teamMemberLastName;
  String teamMemberAddress;
  String teamMemberCity;
  int teamMemberState;
  String teamMemberZip;
  String teamMemberPhone1;
  String teamMemberPhone2;
  String teamMemberPhone3;
  String teamMemberSecondPhone1;
  String teamMemberSecondPhone2;
  String teamMemberSecondPhone3;
  String height;
  int weight;
  int isClaust;
  int isStat;
  int hasMetal;
  String metalDetails;
  String scheduleNearZip;
  String followupAppointment;
  String teamMemberShift;
  String teamMemberPreferredAppointmentTime1;
  String teamMemberPreferredAppointmentTime2;
  String isScheduledAlready;
  String DOImonth;
  String DOIday;
  String DOIyear;
  String DOBmonth;
  String DOBday;
  String DOByear;
  String referringPhysicianFirstName;
  String referringPhysicianLastName;
  String physicianPhone1;
  String physicianPhone2;
  String physicianPhone3;
  String physicianFax1;
  String physicianFax2;
  String physicianFax3;
  int handCarryCD;
  int handCarryFilms;
  int ageReport;
  String specialInstructions;
  List<String> proc;
  List<String> RuleOut;
  List<String> bp;

  public CommonsMultipartFile getUpload1()
  {
    return this.upload1;
  }
  public void setUpload1(CommonsMultipartFile upload1) {
    this.upload1 = upload1;
  }
  public CommonsMultipartFile getUpload2() {
    return this.upload2;
  }
  public void setUpload2(CommonsMultipartFile upload2) {
    this.upload2 = upload2;
  }
  public String getClaimsOffice() {
    return this.claimsOffice;
  }
  public void setClaimsOffice(String claimsOffice) {
    this.claimsOffice = claimsOffice;
  }
  public int getLinkType() {
    return this.linkType;
  }
  public void setLinkType(int linkType) {
    this.linkType = linkType;
  }

  public String getPlantLocation()
  {
    return this.plantLocation;
  }
  public void setPlantLocation(String plantLocation) {
    this.plantLocation = plantLocation;
  }
  public String getClaimNumber() {
    return this.claimNumber;
  }
  public void setClaimNumber(String claimNumber) {
    this.claimNumber = claimNumber;
  }
  public String getPrecertNumber() {
    return this.precertNumber;
  }
  public void setPrecertNumber(String precertNumber) {
    this.precertNumber = precertNumber;
  }
  public int getAdjusterUID() {
    return this.adjusterUID;
  }
  public void setAdjusterUID(int adjusterUID) {
    this.adjusterUID = adjusterUID;
  }
  public String getAdjusterFirstName() {
    return this.adjusterFirstName;
  }
  public void setAdjusterFirstName(String adjusterFirstName) {
    this.adjusterFirstName = adjusterFirstName;
  }
  public String getAdjusterLastName() {
    return this.adjusterLastName;
  }
  public void setAdjusterLastName(String adjusterLastName) {
    this.adjusterLastName = adjusterLastName;
  }
  public String getAdjusterEmail() {
    return this.adjusterEmail;
  }
  public void setAdjusterEmail(String adjusterEmail) {
    this.adjusterEmail = adjusterEmail;
  }
  public String getAdjusterPhone1() {
    return this.adjusterPhone1;
  }
  public void setAdjusterPhone1(String adjusterPhone1) {
    this.adjusterPhone1 = adjusterPhone1;
  }
  public String getAdjusterPhone2() {
    return this.adjusterPhone2;
  }
  public void setAdjusterPhone2(String adjusterPhone2) {
    this.adjusterPhone2 = adjusterPhone2;
  }
  public String getAdjusterPhone3() {
    return this.adjusterPhone3;
  }
  public void setAdjusterPhone3(String adjusterPhone3) {
    this.adjusterPhone3 = adjusterPhone3;
  }
  public String getAdjusterFax1() {
    return this.adjusterFax1;
  }
  public void setAdjusterFax1(String adjusterFax1) {
    this.adjusterFax1 = adjusterFax1;
  }
  public String getAdjusterFax2() {
    return this.adjusterFax2;
  }
  public void setAdjusterFax2(String adjusterFax2) {
    this.adjusterFax2 = adjusterFax2;
  }
  public String getAdjusterFax3() {
    return this.adjusterFax3;
  }
  public void setAdjusterFax3(String adjusterFax3) {
    this.adjusterFax3 = adjusterFax3;
  }
  public int getNcmUID() {
    return this.ncmUID;
  }
  public void setNcmUID(int ncmUID) {
    this.ncmUID = ncmUID;
  }
  public String getNcmFirstName() {
    return this.ncmFirstName;
  }
  public void setNcmFirstName(String ncmFirstName) {
    this.ncmFirstName = ncmFirstName;
  }
  public String getNcmLastName() {
    return this.ncmLastName;
  }
  public void setNcmLastName(String ncmLastName) {
    this.ncmLastName = ncmLastName;
  }
  public String getNcmEmail() {
    return this.ncmEmail;
  }
  public void setNcmEmail(String ncmEmail) {
    this.ncmEmail = ncmEmail;
  }
  public String getNcmPhone1() {
    return this.ncmPhone1;
  }
  public void setNcmPhone1(String ncmPhone1) {
    this.ncmPhone1 = ncmPhone1;
  }
  public String getNcmPhone2() {
    return this.ncmPhone2;
  }
  public void setNcmPhone2(String ncmPhone2) {
    this.ncmPhone2 = ncmPhone2;
  }
  public String getNcmPhone3() {
    return this.ncmPhone3;
  }
  public void setNcmPhone3(String ncmPhone3) {
    this.ncmPhone3 = ncmPhone3;
  }
  public String getNcmFax1() {
    return this.ncmFax1;
  }
  public void setNcmFax1(String ncmFax1) {
    this.ncmFax1 = ncmFax1;
  }
  public String getNcmFax2() {
    return this.ncmFax2;
  }
  public void setNcmFax2(String ncmFax2) {
    this.ncmFax2 = ncmFax2;
  }
  public String getNcmFax3() {
    return this.ncmFax3;
  }
  public void setNcmFax3(String ncmFax3) {
    this.ncmFax3 = ncmFax3;
  }
  public int getCaseAdmin1UID() {
    return this.caseAdmin1UID;
  }
  public void setCaseAdmin1UID(int caseAdmin1UID) {
    this.caseAdmin1UID = caseAdmin1UID;
  }
  public String getCaseAdmin1FirstName() {
    return this.caseAdmin1FirstName;
  }
  public void setCaseAdmin1FirstName(String caseAdmin1FirstName) {
    this.caseAdmin1FirstName = caseAdmin1FirstName;
  }
  public String getCaseAdmin1LastName() {
    return this.caseAdmin1LastName;
  }
  public void setCaseAdmin1LastName(String caseAdmin1LastName) {
    this.caseAdmin1LastName = caseAdmin1LastName;
  }
  public String getCaseAdmin1Email() {
    return this.caseAdmin1Email;
  }
  public void setCaseAdmin1Email(String caseAdmin1Email) {
    this.caseAdmin1Email = caseAdmin1Email;
  }
  public String getCaseAdmin1Phone1() {
    return this.caseAdmin1Phone1;
  }
  public void setCaseAdmin1Phone1(String caseAdmin1Phone1) {
    this.caseAdmin1Phone1 = caseAdmin1Phone1;
  }
  public String getCaseAdmin1Phone2() {
    return this.caseAdmin1Phone2;
  }
  public void setCaseAdmin1Phone2(String caseAdmin1Phone2) {
    this.caseAdmin1Phone2 = caseAdmin1Phone2;
  }
  public String getCaseAdmin1Phone3() {
    return this.caseAdmin1Phone3;
  }
  public void setCaseAdmin1Phone3(String caseAdmin1Phone3) {
    this.caseAdmin1Phone3 = caseAdmin1Phone3;
  }
  public String getCaseAdmin1Fax1() {
    return this.caseAdmin1Fax1;
  }
  public void setCaseAdmin1Fax1(String caseAdmin1Fax1) {
    this.caseAdmin1Fax1 = caseAdmin1Fax1;
  }
  public String getCaseAdmin1Fax2() {
    return this.caseAdmin1Fax2;
  }
  public void setCaseAdmin1Fax2(String caseAdmin1Fax2) {
    this.caseAdmin1Fax2 = caseAdmin1Fax2;
  }
  public String getCaseAdmin1Fax3() {
    return this.caseAdmin1Fax3;
  }
  public void setCaseAdmin1Fax3(String caseAdmin1Fax3) {
    this.caseAdmin1Fax3 = caseAdmin1Fax3;
  }
  public int getCaseAdmin2UID() {
    return this.caseAdmin2UID;
  }
  public void setCaseAdmin2UID(int caseAdmin2UID) {
    this.caseAdmin2UID = caseAdmin2UID;
  }
  public String[] getCaseManagerFirstName() {
    return this.caseManagerFirstName;
  }
  public void setCaseManagerFirstName(String[] caseManagerFirstName) {
    this.caseManagerFirstName = caseManagerFirstName;
  }
  public String[] getCaseManagerLastName() {
    return this.caseManagerLastName;
  }
  public void setCaseManagerLastName(String[] caseManagerLastName) {
    this.caseManagerLastName = caseManagerLastName;
  }
  public String[] getCaseManagerEmail() {
    return this.caseManagerEmail;
  }
  public void setCaseManagerEmail(String[] caseManagerEmail) {
    this.caseManagerEmail = caseManagerEmail;
  }
  public String[] getCmPhoneAreaCode() {
    return this.cmPhoneAreaCode;
  }
  public void setCmPhoneAreaCode(String[] cmPhoneAreaCode) {
    this.cmPhoneAreaCode = cmPhoneAreaCode;
  }
  public String[] getCmPhonePrefix() {
    return this.cmPhonePrefix;
  }
  public void setCmPhonePrefix(String[] cmPhonePrefix) {
    this.cmPhonePrefix = cmPhonePrefix;
  }
  public String[] getCmPhoneExchange() {
    return this.cmPhoneExchange;
  }
  public void setCmPhoneExchange(String[] cmPhoneExchange) {
    this.cmPhoneExchange = cmPhoneExchange;
  }
  public String[] getCmfaxAreaCode() {
    return this.cmfaxAreaCode;
  }
  public void setCmfaxAreaCode(String[] cmfaxAreaCode) {
    this.cmfaxAreaCode = cmfaxAreaCode;
  }
  public String[] getCmfaxPrefix() {
    return this.cmfaxPrefix;
  }
  public void setCmfaxPrefix(String[] cmfaxPrefix) {
    this.cmfaxPrefix = cmfaxPrefix;
  }
  public String[] getCmfaxExchange() {
    return this.cmfaxExchange;
  }
  public void setCmfaxExchange(String[] cmfaxExchange) {
    this.cmfaxExchange = cmfaxExchange;
  }
  public int getCaseAdmin3UID() {
    return this.caseAdmin3UID;
  }
  public void setCaseAdmin3UID(int caseAdmin3UID) {
    this.caseAdmin3UID = caseAdmin3UID;
  }
  public int getCaseAdmin4UID() {
    return this.caseAdmin4UID;
  }
  public void setCaseAdmin4UID(int caseAdmin4UID) {
    this.caseAdmin4UID = caseAdmin4UID;
  }
  public String getTeamMemberFirstName() {
    return this.teamMemberFirstName;
  }
  public void setTeamMemberFirstName(String teamMemberFirstName) {
    this.teamMemberFirstName = teamMemberFirstName;
  }
  public String getTeamMemberLastName() {
    return this.teamMemberLastName;
  }
  public void setTeamMemberLastName(String teamMemberLastName) {
    this.teamMemberLastName = teamMemberLastName;
  }

  public String getTeamMemberAddress()
  {
    return this.teamMemberAddress;
  }
  public void setTeamMemberAddress(String teamMemberAddress) {
    this.teamMemberAddress = teamMemberAddress;
  }
  public String getTeamMemberCity() {
    return this.teamMemberCity;
  }
  public void setTeamMemberCity(String teamMemberCity) {
    this.teamMemberCity = teamMemberCity;
  }
  public int getTeamMemberState() {
    return this.teamMemberState;
  }
  public void setTeamMemberState(int teamMemberState) {
    this.teamMemberState = teamMemberState;
  }
  public String getTeamMemberZip() {
    return this.teamMemberZip;
  }
  public void setTeamMemberZip(String teamMemberZip) {
    this.teamMemberZip = teamMemberZip;
  }
  public String getTeamMemberPhone1() {
    return this.teamMemberPhone1;
  }
  public void setTeamMemberPhone1(String teamMemberPhone1) {
    this.teamMemberPhone1 = teamMemberPhone1;
  }
  public String getTeamMemberPhone2() {
    return this.teamMemberPhone2;
  }
  public void setTeamMemberPhone2(String teamMemberPhone2) {
    this.teamMemberPhone2 = teamMemberPhone2;
  }
  public String getTeamMemberPhone3() {
    return this.teamMemberPhone3;
  }
  public void setTeamMemberPhone3(String teamMemberPhone3) {
    this.teamMemberPhone3 = teamMemberPhone3;
  }
  public String getTeamMemberSecondPhone1() {
    return this.teamMemberSecondPhone1;
  }
  public void setTeamMemberSecondPhone1(String teamMemberSecondPhone1) {
    this.teamMemberSecondPhone1 = teamMemberSecondPhone1;
  }
  public String getTeamMemberSecondPhone2() {
    return this.teamMemberSecondPhone2;
  }
  public void setTeamMemberSecondPhone2(String teamMemberSecondPhone2) {
    this.teamMemberSecondPhone2 = teamMemberSecondPhone2;
  }
  public String getTeamMemberSecondPhone3() {
    return this.teamMemberSecondPhone3;
  }
  public void setTeamMemberSecondPhone3(String teamMemberSecondPhone3) {
    this.teamMemberSecondPhone3 = teamMemberSecondPhone3;
  }
  public String getHeight() {
    return this.height;
  }
  public void setHeight(String height) {
    this.height = height;
  }
  public int getWeight() {
    return this.weight;
  }
  public void setWeight(int weight) {
    this.weight = weight;
  }
  public int getIsClaust() {
    return this.isClaust;
  }
  public void setIsClaust(int isClaust) {
    this.isClaust = isClaust;
  }
  public int getIsStat() {
    return this.isStat;
  }
  public void setIsStat(int isStat) {
    this.isStat = isStat;
  }
  public int getHasMetal() {
    return this.hasMetal;
  }
  public void setHasMetal(int hasMetal) {
    this.hasMetal = hasMetal;
  }
  public String getMetalDetails() {
    return this.metalDetails;
  }
  public void setMetalDetails(String metalDetails) {
    this.metalDetails = metalDetails;
  }
  public String getScheduleNearZip() {
    return this.scheduleNearZip;
  }
  public void setScheduleNearZip(String scheduleNearZip) {
    this.scheduleNearZip = scheduleNearZip;
  }
  public String getFollowupAppointment() {
    return this.followupAppointment;
  }
  public void setFollowupAppointment(String followupAppointment) {
    this.followupAppointment = followupAppointment;
  }
  public String getTeamMemberShift() {
    return this.teamMemberShift;
  }
  public void setTeamMemberShift(String teamMemberShift) {
    this.teamMemberShift = teamMemberShift;
  }
  public String getTeamMemberPreferredAppointmentTime1() {
    return this.teamMemberPreferredAppointmentTime1;
  }

  public void setTeamMemberPreferredAppointmentTime1(String teamMemberPreferredAppointmentTime1) {
    this.teamMemberPreferredAppointmentTime1 = teamMemberPreferredAppointmentTime1;
  }
  public String getTeamMemberPreferredAppointmentTime2() {
    return this.teamMemberPreferredAppointmentTime2;
  }

  public void setTeamMemberPreferredAppointmentTime2(String teamMemberPreferredAppointmentTime2) {
    this.teamMemberPreferredAppointmentTime2 = teamMemberPreferredAppointmentTime2;
  }
  public String getIsScheduledAlready() {
    return this.isScheduledAlready;
  }
  public void setIsScheduledAlready(String isScheduledAlready) {
    this.isScheduledAlready = isScheduledAlready;
  }
  public String getDOImonth() {
    return this.DOImonth;
  }
  public void setDOImonth(String dOImonth) {
    this.DOImonth = dOImonth;
  }
  public String getDOIday() {
    return this.DOIday;
  }
  public void setDOIday(String dOIday) {
    this.DOIday = dOIday;
  }
  public String getDOIyear() {
    return this.DOIyear;
  }
  public void setDOIyear(String dOIyear) {
    this.DOIyear = dOIyear;
  }
  public String getDOBmonth() {
    return this.DOBmonth;
  }
  public void setDOBmonth(String dOBmonth) {
    this.DOBmonth = dOBmonth;
  }
  public String getDOBday() {
    return this.DOBday;
  }
  public void setDOBday(String dOBday) {
    this.DOBday = dOBday;
  }
  public String getDOByear() {
    return this.DOByear;
  }
  public void setDOByear(String dOByear) {
    this.DOByear = dOByear;
  }
  public String getReferringPhysicianFirstName() {
    return this.referringPhysicianFirstName;
  }
  public void setReferringPhysicianFirstName(String referringPhysicianFirstName) {
    this.referringPhysicianFirstName = referringPhysicianFirstName;
  }
  public String getReferringPhysicianLastName() {
    return this.referringPhysicianLastName;
  }
  public void setReferringPhysicianLastName(String referringPhysicianLastName) {
    this.referringPhysicianLastName = referringPhysicianLastName;
  }
  public String getPhysicianPhone1() {
    return this.physicianPhone1;
  }
  public void setPhysicianPhone1(String physicianPhone1) {
    this.physicianPhone1 = physicianPhone1;
  }
  public String getPhysicianPhone2() {
    return this.physicianPhone2;
  }
  public void setPhysicianPhone2(String physicianPhone2) {
    this.physicianPhone2 = physicianPhone2;
  }
  public String getPhysicianPhone3() {
    return this.physicianPhone3;
  }
  public void setPhysicianPhone3(String physicianPhone3) {
    this.physicianPhone3 = physicianPhone3;
  }
  public String getPhysicianFax1() {
    return this.physicianFax1;
  }
  public void setPhysicianFax1(String physicianFax1) {
    this.physicianFax1 = physicianFax1;
  }
  public String getPhysicianFax2() {
    return this.physicianFax2;
  }
  public void setPhysicianFax2(String physicianFax2) {
    this.physicianFax2 = physicianFax2;
  }
  public String getPhysicianFax3() {
    return this.physicianFax3;
  }
  public void setPhysicianFax3(String physicianFax3) {
    this.physicianFax3 = physicianFax3;
  }
  public int getHandCarryCD() {
    return this.handCarryCD;
  }
  public void setHandCarryCD(int handCarryCD) {
    this.handCarryCD = handCarryCD;
  }
  public int getHandCarryFilms() {
    return this.handCarryFilms;
  }
  public void setHandCarryFilms(int handCarryFilms) {
    this.handCarryFilms = handCarryFilms;
  }
  public int getAgeReport() {
    return this.ageReport;
  }
  public void setAgeReport(int ageReport) {
    this.ageReport = ageReport;
  }
  public String getSpecialInstructions() {
    return this.specialInstructions;
  }
  public void setSpecialInstructions(String specialInstructions) {
    this.specialInstructions = specialInstructions;
  }
  public List<String> getProc() {
    return this.proc;
  }
  public void setProc(List<String> proc) {
    this.proc = proc;
  }
  public List<String> getRuleOut() {
    return this.RuleOut;
  }
  public void setRuleOut(List<String> ruleOut) {
    this.RuleOut = ruleOut;
  }
  public List<String> getBp() {
    return this.bp;
  }
  public void setBp(List<String> bp) {
    this.bp = bp;
  }
}