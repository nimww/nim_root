package com.nextimagemedical.adjportal;

import com.winstaff.ConfigurationInformation;
import com.winstaff.ListElement;
import com.winstaff.NIM3_EncounterObject;
import com.winstaff.NIM3_EncounterObject2;
import com.winstaff.NIMUtils;
import com.winstaff.PLCUtils;
import com.winstaff.bltEmailTransaction;
import com.winstaff.bltNIM3_Appointment;
import com.winstaff.bltNIM3_CaseAccount;
import com.winstaff.bltNIM3_CommTrack;
import com.winstaff.bltNIM3_Document;
import com.winstaff.bltNIM3_Encounter;
import com.winstaff.bltNIM3_PayerMaster;
import com.winstaff.bltNIM3_Referral;
import com.winstaff.bltNIM3_Service;
import com.winstaff.bltNIM3_Service_List;
import com.winstaff.bltPracticeMaster;
import com.winstaff.bltStateLI;
import com.winstaff.bltUserAccount;
import com.winstaff.emailType_V3;
import com.winstaff.password.RandomString;
import com.winstaff.searchDB2;
import java.io.File;
import java.io.FileOutputStream;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Enumeration;
import java.util.LinkedHashMap;
import java.util.List;
import javax.servlet.http.HttpSession;
import org.apache.commons.fileupload.FileItem;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

public class AppUtils
{
  public static List<String> procedure = Arrays.asList(new String[] { "MRI", "CT", "US" });
  public static List<String> bodyPart = Arrays.asList(new String[] { "Cervical", "Thoracic", "Lumbar", "Upper Extremity", "Lower Extremity" });
  public static List<String> ruleOut = Arrays.asList(new String[] { "Pain", "CTS" });

  public static List<String> heightList = Arrays.asList(new String[] { "", "4 ft 0 in", "4 ft 1 in", "4 ft 2 in", "4 ft 3 in", "4 ft 4 in", "4 ft 5 in", "4 ft 6 in", "4 ft 7 in", "4 ft 8 in", "4 ft 9 in", "4 ft 10 in", "4 ft 11 in", "5 ft 0 in", "5 ft 1 in", "5 ft 2 in", "5 ft 3 in", "5 ft 4 in", "5 ft 5 in", "5 ft 6 in", "5 ft 7 in", "5 ft 8 in", "5 ft 9 in", "5 ft 10 in", "5 ft 11 in", "6 ft 0 in", "6 ft 1 in", 
    "6 ft 2 in", "6 ft 3 in", "6 ft 4 in", "6 ft 5 in", "6 ft 6 in", "6 ft 7 in", "6 ft\t8 in", "6 ft 9 in", "6 ft 10 in", "6 ft 11 in", "7 ft 0 in", "7 ft 1 in", "7 ft 2 in", "7 ft 3 in", "7 ft 4 in", "7 ft 5\tin", "7 ft 6 in", "7 ft 7 in", "7 ft 8 in", "7 ft 9 in", "7 ft 10 in", "7 ft 11 in" });

  public static List<String> month = Arrays.asList(new String[] { "", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12" });

  public static List<String> day = Arrays.asList(new String[] { "", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", 
    "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31" });

  public static List<String> getYears() {
    List list = new ArrayList();

    for (int x = new Integer(new SimpleDateFormat("yyyy").format(new Date())).intValue(); x > new Integer(new SimpleDateFormat("yyyy").format(new Date())).intValue() - 100; x--) {
      list.add(new Integer(x).toString());
    }

    return list;
  }

  public void getCurrentSession(HttpSession session, bltUserAccount CurrentUserAccount)
  {
  }

  public static LinkedHashMap<Integer, String> getBodyPartSearch()
  {
    LinkedHashMap lhm = new LinkedHashMap();

    searchDB2 search = new searchDB2();
    String query = "SELECT DISTINCT bp1, cptwizardid FROM tcptwizard ORDER BY bp1 ASC ";
    ResultSet rs = search.executeStatement(query);
    try
    {
      while (rs.next()) {
        lhm.put(Integer.valueOf(rs.getInt("cptwizardid")), rs.getString("bp1"));
      }
      search.closeAll();
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return lhm;
  }

  public static List<CaseList> getCaseList(bltUserAccount CurrentUserAccount) {
    searchDB2 mySS = new searchDB2();

    ResultSet myRS = null;
    List caselist = new ArrayList();

    String mySQL = "null";

    Integer iPayerID = CurrentUserAccount.getPayerID();

    if (CurrentUserAccount.getUserID()==25251){
        mySQL = " select tNIM3_Encounter.encounterid from tNIM3_CaseAccount INNER JOIN tNIM3_Referral on tNIM3_Referral.caseid = tNIM3_CaseAccount.caseid INNER JOIN tNIM3_Encounter on NIM3_Encounter.referralid = tNIM3_Referral.referralid  where (tNIM3_CaseAccount.PayerID in (839,1092,1093,1094,1232,1249,1265,1266,1284) OR tNIM3_CaseAccount.caseadministratorid in (839,1092,1093,1094,1232,1249,1265,1266,1284) or tNIM3_CaseAccount.caseadministrator2id in (839,1092,1093,1094,1232,1249,1265,1266,1284) or tNIM3_CaseAccount.caseadministrator3id in (839,1092,1093,1094,1232,1249,1265,1266,1284) or tNIM3_CaseAccount.caseadministrator4id in (839,1092,1093,1094,1232,1249,1265,1266,1284) or tNIM3_CaseAccount.nursecasemanagerid in (839,1092,1093,1094,1232,1249,1265,1266,1284) ) and tNIM3_Encounter.encounterstatusid in (" + NIMUtils.getSQL_Encounters_Active() + ")  order by ReceiveDate desc ";
        mySQL = "select tNIM3_Encounter.encounterid from tNIM3_CaseAccount INNER JOIN tNIM3_Referral on tNIM3_Referral.caseid = tNIM3_CaseAccount.caseid  INNER JOIN tNIM3_Encounter on tNIM3_Encounter.referralid = tNIM3_Referral.referralid where (tNIM3_CaseAccount.PayerID in ((select userid from tuseraccount where payerid in (839,1092,1093,1094,1232,1249,1265,1266,1284))) OR tNIM3_CaseAccount.caseadministratorid in ((select userid from tuseraccount where payerid in (839,1092,1093,1094,1232,1249,1265,1266,1284))) or  tNIM3_CaseAccount.caseadministrator2id in ((select userid from tuseraccount where payerid in (839,1092,1093,1094,1232,1249,1265,1266,1284))) or tNIM3_CaseAccount.caseadministrator3id in ((select userid from tuseraccount where payerid in (839,1092,1093,1094,1232,1249,1265,1266,1284))) or tNIM3_CaseAccount.caseadministrator4id in ((select userid from tuseraccount where payerid in (839,1092,1093,1094,1232,1249,1265,1266,1284))) or tNIM3_CaseAccount.nursecasemanagerid in (((select userid from tuseraccount where payerid in (839,1092,1093,1094,1232,1249,1265,1266,1284))))) and tNIM3_Encounter.encounterstatusid in (" + NIMUtils.getSQL_Encounters_Active() + ") order by tNIM3_Referral.ReceiveDate desc ";
    } else if (CurrentUserAccount.getAccountType().equalsIgnoreCase("AdjusterID3")){
      mySQL = "select tNIM3_Encounter.encounterid from tNIM3_CaseAccount INNER JOIN tNIM3_Referral on tNIM3_Referral.caseid = tNIM3_CaseAccount.caseid INNER JOIN tNIM3_Encounter on tNIM3_Encounter.referralid = tNIM3_Referral.referralid  where tNIM3_CaseAccount.PayerID =" + iPayerID + " OR tNIM3_CaseAccount.caseadministratorid =" + CurrentUserAccount.getUserID() + 
        " or tNIM3_CaseAccount.nursecasemanagerid = " + CurrentUserAccount.getUserID() + " and tNIM3_Encounter.encounterstatusid in (" + NIMUtils.getSQL_Encounters_Active() + ") order by ReceiveDate desc ";
    } else if (CurrentUserAccount.getAccountType().equalsIgnoreCase("AdjusterID2")) {
      mySQL = "select tNIM3_Encounter.encounterid from tNIM3_CaseAccount INNER JOIN tNIM3_Referral on tNIM3_Referral.caseid = tNIM3_CaseAccount.caseid INNER JOIN tNIM3_Encounter on tNIM3_Encounter.referralid = tNIM3_Referral.referralid  where ( (tNIM3_CaseAccount.adjusterid = " + CurrentUserAccount.getUserID() + 
        " OR tNIM3_CaseAccount.adjusterid in (select userid from tUserAccount where payerid=" + iPayerID + " AND managerid=" + CurrentUserAccount.getUserID() + " ) ) OR (tNIM3_CaseAccount.nursecasemanagerid = " + CurrentUserAccount.getUserID() + " OR tNIM3_CaseAccount.nursecasemanagerid in (select userid from tUserAccount where payerid=" + iPayerID + " AND managerid=" + 
        CurrentUserAccount.getUserID() + " ) )    OR (tNIM3_CaseAccount.caseadministratorid = " + CurrentUserAccount.getUserID() + " OR tNIM3_CaseAccount.caseadministratorid in (select userid from tUserAccount where payerid=" + iPayerID + " AND managerid=" + CurrentUserAccount.getUserID() + " ) ) OR (tNIM3_CaseAccount.caseadministrator2id = " + CurrentUserAccount.getUserID() + 
        " OR tNIM3_CaseAccount.caseadministrator2id in (select userid from tUserAccount where payerid=" + iPayerID + " AND managerid=" + CurrentUserAccount.getUserID() + " ) ) OR (tNIM3_CaseAccount.caseadministrator3id = " + CurrentUserAccount.getUserID() + " OR tNIM3_CaseAccount.caseadministrator3id in (select userid from tUserAccount where payerid=" + iPayerID + " AND managerid=" + 
        CurrentUserAccount.getUserID() + " ) )  OR (tNIM3_CaseAccount.caseadministrator4id = " + CurrentUserAccount.getUserID() + " OR tNIM3_CaseAccount.caseadministrator4id in (select userid from tUserAccount where payerid=" + iPayerID + " AND managerid=" + CurrentUserAccount.getUserID() + " ) )) and tNIM3_Encounter.encounterstatusid in (" + NIMUtils.getSQL_Encounters_Active() + 
        ") order by ReceiveDate desc ";
    }
    else if (CurrentUserAccount.getAccountType().equalsIgnoreCase("AdjusterID")) {
      mySQL = "select tNIM3_Encounter.encounterid from tNIM3_CaseAccount INNER JOIN tNIM3_Referral on tNIM3_Referral.caseid = tNIM3_CaseAccount.caseid INNER JOIN tNIM3_Encounter on tNIM3_Encounter.referralid = tNIM3_Referral.referralid  where (tNIM3_CaseAccount.adjusterid = " + CurrentUserAccount.getUserID() + " OR tNIM3_CaseAccount.nursecasemanagerid = " + CurrentUserAccount.getUserID() + 
        " OR tNIM3_CaseAccount.caseadministratorid = " + CurrentUserAccount.getUserID() + " OR tNIM3_CaseAccount.caseadministrator2id = " + CurrentUserAccount.getUserID() + " OR tNIM3_CaseAccount.caseadministrator3id = " + CurrentUserAccount.getUserID() + " OR tNIM3_CaseAccount.caseadministrator4id = " + CurrentUserAccount.getUserID() + ") and tNIM3_Encounter.encounterstatusid in (" + 
        NIMUtils.getSQL_Encounters_Active() + ") order by ReceiveDate desc";
    }

    myRS = mySS.executeStatement(mySQL);
    try
    {
      while ((myRS != null) && (myRS.next())) {
        NIM3_EncounterObject2 eo2 = new NIM3_EncounterObject2(Integer.valueOf(myRS.getInt("encounterid")), "loading");
        caselist.add(new CaseList(eo2.getNIM3_CaseAccount().getPatientLastName() + ", " + eo2.getNIM3_CaseAccount().getPatientFirstName(), eo2.getNIM3_CaseAccount().getCaseClaimNumber(), PLCUtils.getDisplayDate(eo2.getNIM3_Referral().getReceiveDate(), false), eo2.getNIM3_Encounter().getEncounterID().intValue()));
      }
      mySS.closeAll();
    }
    catch (Exception localException) {
    }
    return caselist;
  }

  public static CaseDetails getCaseDetails(int encounterid) {
    SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");
    NIM3_EncounterObject2 eo2 = new NIM3_EncounterObject2(Integer.valueOf(encounterid), "");

    return new CaseDetails(eo2.getNIM3_CaseAccount().getPatientLastName() + ", " + eo2.getNIM3_CaseAccount().getPatientFirstName(), eo2.getNIM3_CaseAccount().getCaseClaimNumber(), df.format(eo2.getNIM3_CaseAccount().getDateOfInjury()), new bltNIM3_PayerMaster(eo2.getNIM3_CaseAccount().getPayerID()).getPayerName(), eo2.getCase_Adjuster().getContactFirstName() + " " + 
      eo2.getCase_Adjuster().getContactLastName(), df.format(eo2.getNIM3_Referral().getReceiveDate()), eo2.getNIM3_Referral().getOrderFileID().intValue(), eo2.getNIM3_Referral().getRxFileID().intValue(), eo2.getNIM3_Encounter().getReportFileID().intValue(), eo2.getNIM3_Encounter().getisSTAT().intValue(), eo2.getNIM3_Encounter().getEncounterID().intValue(), eo2.getNIM3_Encounter().getScanPass(), eo2
      .getReferral_ReferringDoctor().getContactFirstName() + " " + eo2.getReferral_ReferringDoctor().getContactLastName(), NIMUtils.getCaseStatus(eo2.getNIM3_Encounter().getEncounterID().intValue()), getServiceItems(eo2.getNIM3_Service_List().elements()), getApptDetails(eo2.getNIM3_Encounter().getAppointmentID().intValue()));
  }

  public static List<NIM3_EncounterObject2> getRp()
  {
    List resultlist = new ArrayList();
    String query = "SELECT \"EncounterID\"FROM \"vMQ4_Encounter_NoVoid_BP\" WHERE \"Encounter_StatusID\" = '1' and \"Parent_PayerID\" = '307' and \"Practice_State\" = 'FL'";

    searchDB2 conn = new searchDB2();
    ResultSet rs = conn.executeStatement(query);
    try {
      do {
        resultlist.add(new NIM3_EncounterObject2(Integer.valueOf(rs.getInt("EncounterID")), ""));

        if (rs == null) break;  } while (rs.next());
    }
    catch (Exception localException)
    {
    }
    finally {
      conn.closeAll();
    }
    return resultlist;
  }

  public static ResultValidation validateReferalForm(ReferCase rc, bltUserAccount ua) {
    boolean goodReferral = true;
    ResultValidation rv = new ResultValidation();

    if ((!rc.getFileRx().isEmpty()) || (!rc.getFileOther().isEmpty())) {
      if (rc.getCaseClaimNumber1().equals("")) {
        goodReferral = false;
        rv.setCaseClaimNumber1(true);
      }
      if (rc.getPatientFirstName().equals("")) {
        goodReferral = false;
        rv.setPatientFirstName(true);
      }
      if (rc.getPatientLastName().equals("")) {
        goodReferral = false;
        rv.setPatientLastName(true);
      }
      if (goodReferral) {
        try {
          submitCase(rc, ua);
          rv.setGoodReferral(true);
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }

    }
    else if ((rc.getFileRx().isEmpty()) || (rc.getFileOther().isEmpty())) {
      if (rc.getCaseClaimNumber1().equals("")) {
        goodReferral = false;
        rv.setCaseClaimNumber1(true);
      }
      if (rc.getPatientFirstName().equals("")) {
        goodReferral = false;
        rv.setPatientFirstName(true);
      }
      if (rc.getPatientLastName().equals("")) {
        goodReferral = false;
        rv.setPatientLastName(true);
      }
      if (rc.getRefFirstName().equals("")) {
        goodReferral = false;
        rv.setRefFirstName(true);
      }
      if (rc.getRefLastName().equals("")) {
        goodReferral = false;
        rv.setRefLastName(true);
      }
      if (rc.getAdjusterPhone1().equals("")) {
        goodReferral = false;
        rv.setAdjusterPhone1(true);
      }
      if (rc.getAdjusterPhone2().equals("")) {
        goodReferral = false;
        rv.setAdjusterPhone2(true);
      }
      if (rc.getAdjusterPhone3().equals("")) {
        goodReferral = false;
        rv.setAdjusterPhone3(true);
      }
      if (rc.getAdjusterFax1().equals("")) {
        goodReferral = false;
        rv.setAdjusterFax1(true);
      }
      if (rc.getAdjusterFax2().equals("")) {
        goodReferral = false;
        rv.setAdjusterFax2(true);
      }
      if (rc.getAdjusterFax3().equals("")) {
        goodReferral = false;
        rv.setAdjusterFax3(true);
      }
      if (rc.getPatientDOB().equals("")) {
        goodReferral = false;
        rv.setPatientDOB(true);
      }
      if (rc.getPatientDOI().equals("")) {
        goodReferral = false;
        rv.setPatientDOI(true);
      }
      if (rc.getPatientAddress1().equals("")) {
        goodReferral = false;
        rv.setPatientAddress1(true);
      }
      if (rc.getPatientCity().equals("")) {
        goodReferral = false;
        rv.setPatientCity(true);
      }
      if (rc.getPatientStateID() == 0) {
        goodReferral = false;
        rv.setPatientStateID(true);
      }
      if (rc.getPatientZip().equals("")) {
        goodReferral = false;
        rv.setPatientZip(true);
      }
      if (rc.getPatientHomePhone1().equals("")) {
        goodReferral = false;
        rv.setPatientHomePhone1(true);
      }
      if (rc.getPatientHomePhone2().equals("")) {
        goodReferral = false;
        rv.setPatientHomePhone2(true);
      }
      if (rc.getPatientHomePhone3().equals("")) {
        goodReferral = false;
        rv.setPatientHomePhone3(true);
      }
      if (rc.getMDName().equals("")) {
        goodReferral = false;
        rv.setMDName(true);
      }
      if (rc.getMDPhone1().equals("")) {
        goodReferral = false;
        rv.setMDPhone1(true);
      }
      if (rc.getMDPhone2().equals("")) {
        goodReferral = false;
        rv.setMDPhone2(true);
      }
      if (rc.getMDPhone3().equals("")) {
        goodReferral = false;
        rv.setMDPhone3(true);
      }
      if (rc.getMDFax1().equals("")) {
        goodReferral = false;
        rv.setMDFax1(true);
      }
      if (rc.getMDFax2().equals("")) {
        goodReferral = false;
        rv.setMDFax2(true);
      }
      if (rc.getMDFax3().equals("")) {
        goodReferral = false;
        rv.setMDFax3(true);
      }
      if (rc.getProc().equals("")) {
        goodReferral = false;
        rv.setProc(true);
      }
      if (rc.getBp().equals("")) {
        goodReferral = false;
        rv.setBp(true);
      }
      if (rc.getRuleOut().equals("")) {
        goodReferral = false;
        rv.setRuleOut(true);
      }
      if (goodReferral) {
        try {
          submitCase(rc, ua);
          rv.setGoodReferral(true);
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }

    }

    return rv;
  }

  public static ReferCase submitCase(ReferCase refercase, bltUserAccount ua)
    throws SQLException
  {
    bltNIM3_CommTrack ct = new bltNIM3_CommTrack();
    bltNIM3_CaseAccount ca = new bltNIM3_CaseAccount();
    bltNIM3_Referral ref = new bltNIM3_Referral();
    bltNIM3_Encounter en = new bltNIM3_Encounter();
    bltNIM3_Document docRx = new bltNIM3_Document();
    bltNIM3_Document docOther = new bltNIM3_Document();

    String commtracks = "From Adjuster Web Portal\n\n";
    try
    {
      ca.setPatientFirstName(refercase.getPatientFirstName());
    }
    catch (Exception localException) {
    }
    try {
      ca.setPatientLastName(refercase.getPatientLastName());
    }
    catch (Exception localException1) {
    }
    try {
      ca.setPatientAddress1(refercase.getPatientAddress1());
    } catch (Exception localException2) {
    }
    try {
      ca.setPatientCity(refercase.getPatientCity());
    }
    catch (Exception localException3)
    {
    }
    try {
      ca.setPatientZIP(refercase.getPatientZip());
    }
    catch (Exception localException4)
    {
    }
    try {
      ca.setPayerID(ua.getPayerID());
    }
    catch (Exception localException5)
    {
    }
    try {
      ca.setPatientHomePhone(refercase.getPatientHomePhone1() + "-" + refercase.getPatientHomePhone2() + "-" + refercase.getPatientHomePhone3());
    }
    catch (Exception localException6)
    {
    }
    try {
      ca.setPatientCellPhone(refercase.getPatientMobilePhone1() + "-" + refercase.getPatientMobilePhone2() + "-" + refercase.getPatientMobilePhone3());
    }
    catch (Exception localException7)
    {
    }
    try {
      ca.setPatientHeight(refercase.getHeight());
    }
    catch (Exception localException8)
    {
    }
    try {
      ca.setPatientWeight(Integer.valueOf(refercase.getWeight()));
    }
    catch (Exception localException9)
    {
    }
    try {
      ca.setPatientDOB(new SimpleDateFormat("MM/dd/yyyy").parse(refercase.getPatientDOB()));
    }
    catch (Exception localException10) {
    }
    try {
      ca.setPatientStateID(Integer.valueOf(refercase.getPatientStateID()));
    }
    catch (Exception localException11)
    {
    }
    if (refercase.getLinkType() == 1)
      try {
        ca.setAdjusterID(ua.getUserID());
      } catch (Exception localException12) {
      }
    else if (refercase.getLinkType() == 2)
      try {
        ca.setNurseCaseManagerID(ua.getUserID());
      } catch (Exception localException13) {
      }
    else if (refercase.getLinkType() == 3)
      try {
        ca.setCaseAdministratorID(ua.getUserID());
      }
      catch (Exception localException14)
      {
      }
    try {
      ca.setEmployerName(refercase.getEmployerName() + " / " + refercase.getEmployerPhone1() + "-" + refercase.getEmployerPhone2() + "-" + refercase.getEmployerPhone3());
    }
    catch (Exception localException15)
    {
    }
    try {
      ca.setComments(
        "Ref: " + refercase.getRefFirstName() + " " + refercase.getRefLastName() + "\n" + 
        "Adj: " + ua.getContactFirstName() + ua.getContactLastName() + "\n" + 
        "Phone: " + refercase.getAdjusterPhone1() + "-" + refercase.getAdjusterPhone2() + "-" + refercase.getAdjusterPhone3() + "\n" + 
        "Fax: " + refercase.getAdjusterFax1() + "-" + refercase.getAdjusterFax2() + "-" + refercase.getAdjusterFax3() + "\n" + 
        "Email: " + refercase.getAdjusterEmail() + "\n" + 
        "Branch: " + refercase.getAdjusterBranch() + "\n" + 
        "Adj notes: " + refercase.getMessageText() + "\n\n" + 
        "MD: " + refercase.getMDName() + "\n" + 
        "Phone: " + refercase.getMDPhone1() + "-" + refercase.getMDPhone2() + "-" + refercase.getMDPhone3() + "\n" + 
        "Fax: " + refercase.getMDFax1() + "-" + refercase.getMDFax2() + "-" + refercase.getMDFax3() + "\n" + 
        "Email: " + refercase.getMDEmail() + "\n" + 
        "Follow up: " + refercase.getFollowUpDate() + "\n\n");

      commtracks = commtracks + "Ref: " + refercase.getRefFirstName() + " " + refercase.getRefLastName() + "\n" + 
        "Adj: " + ua.getContactFirstName() + ua.getContactLastName() + "\n" + 
        "Phone: " + refercase.getAdjusterPhone1() + "-" + refercase.getAdjusterPhone2() + "-" + refercase.getAdjusterPhone3() + "\n" + 
        "Fax: " + refercase.getAdjusterFax1() + "-" + refercase.getAdjusterFax2() + "-" + refercase.getAdjusterFax3() + "\n" + 
        "Email: " + refercase.getAdjusterEmail() + "\n" + 
        "Branch: " + refercase.getAdjusterBranch() + "\n" + 
        "Adj notes: " + refercase.getMessageText() + "\n\n" + 
        "MD: " + refercase.getMDName() + "\n" + 
        "Phone: " + refercase.getMDPhone1() + "-" + refercase.getMDPhone2() + "-" + refercase.getMDPhone3() + "\n" + 
        "Fax: " + refercase.getMDFax1() + "-" + refercase.getMDFax2() + "-" + refercase.getMDFax3() + "\n" + 
        "Email: " + refercase.getMDEmail() + "\n" + 
        "Follow up: " + refercase.getFollowUpDate() + "\n\n";
    }
    catch (Exception localException16)
    {
    }
    try {
      ca.setPatientIsClaus(Integer.valueOf(refercase.getClaust()));
    }
    catch (Exception localException17)
    {
    }
    try {
      ca.setPatientIsPregnant(Integer.valueOf(refercase.getPrego()));
    }
    catch (Exception localException18) {
    }
    try {
      ca.setPatientHasMetalInBody(Integer.valueOf(refercase.getMetal()));
    }
    catch (Exception localException19)
    {
    }
    try {
      ca.setPatientHasMetalInBodyDesc("Date of metal: " + refercase.getDOM());
    }
    catch (Exception localException20)
    {
    }

    try
    {
      ca.setPatientHasKidneyLiverHypertensionDiabeticConditionsDesc(refercase.getDiseases());
    }
    catch (Exception localException21)
    {
    }
    try {
      ca.setCaseStatusID(new Integer(1));
    }
    catch (Exception localException22)
    {
    }
    try {
      ca.setUniqueCreateDate(PLCUtils.getNowDate(false));
    }
    catch (Exception localException23)
    {
    }
    try {
      ca.setUniqueModifyDate(PLCUtils.getNowDate(false));
    }
    catch (Exception localException24)
    {
    }
    try {
      ca.setDateOfInjury(new SimpleDateFormat("MM/dd/yyyy").parse(refercase.getPatientDOI()));
    }
    catch (Exception localException25)
    {
    }
    try {
      ca.setUniqueModifyComments("Created by NID Online Form V2");
    }
    catch (Exception localException26)
    {
    }
    try {
      ca.setPatientSSN(refercase.getPatientSSN());
    }
    catch (Exception localException27)
    {
    }
    try {
      ca.setCaseClaimNumber(refercase.getCaseClaimNumber1());
    }
    catch (Exception localException28)
    {
    }
    ca.commitData();
    ca.setCaseCode("CP" + ca.getUniqueID() + RandomString.generateString(2).toUpperCase() + Math.round(Math.random() * 100.0D) + RandomString.generateString(2).toUpperCase());

    ca.commitData();
    try
    {
      ref.setCaseID(ca.getUniqueID());
    }
    catch (Exception localException29)
    {
    }
    try {
      ref.setUniqueCreateDate(PLCUtils.getNowDate(false));
    }
    catch (Exception localException30) {
    }
    try {
      ref.setUniqueModifyDate(PLCUtils.getNowDate(false));
    }
    catch (Exception localException31)
    {
    }
    try {
      ref.setUniqueModifyComments("Created by Online Adj Form");
    }
    catch (Exception localException32) {
    }
    try {
      ref.setReferralTypeID(Integer.valueOf(3));
    }
    catch (Exception localException33)
    {
    }
    try {
      ref.setReferralDate(PLCUtils.getNowDate(true));
    }
    catch (Exception localException34) {
    }
    try {
      ref.setReceiveDate(PLCUtils.getNowDate(true));
    }
    catch (Exception localException35)
    {
    }
    try {
      ref.setReferralStatusID(Integer.valueOf(1));
    }
    catch (Exception localException36) {
    }
    try {
      ref.setReferralMethod("Online (Sec)");
    }
    catch (Exception localException37) {
    }
    try {
      ref.setAttendingPhysicianID(Integer.valueOf(52));
    }
    catch (Exception localException38) {
    }
    try {
      String procList = "";

      for (int x = 0; x < refercase.getProc().size(); x++) {
        procList = procList + "Procedure " + (x + 1) + ": " + (String)refercase.getProc().get(x) + "\n" + 
          "Body Part " + (x + 1) + ": " + (String)refercase.getBp().get(x) + "\n" + 
          "Rule Out " + (x + 1) + ": " + (String)refercase.getRuleOut().get(x) + "\n";

        commtracks = commtracks + "Procedure " + (x + 1) + ": " + (String)refercase.getProc().get(x) + "\n" + 
          "Body Part " + (x + 1) + ": " + (String)refercase.getBp().get(x) + "\n" + 
          "Rule Out " + (x + 1) + ": " + (String)refercase.getRuleOut().get(x) + "\n";
      }
      ref.setComments(procList);
    }
    catch (Exception localException39)
    {
    }
    try {
      ref.setReferredByContactID(ua.getUserID());
    }
    catch (Exception localException40)
    {
    }
    try {
      ref.setComments(refercase.getMDName() + "\nFax: " + refercase.getMDFax1() + refercase.getMDFax2() + refercase.getMDFax3() + "\nPhone: " + refercase.getMDPhone1() + refercase.getMDPhone2() + refercase.getMDPhone3());

      commtracks = commtracks + "\nFax: " + refercase.getMDFax1() + refercase.getMDFax2() + refercase.getMDFax3() + "\nPhone: " + refercase.getMDPhone1() + refercase.getMDPhone2() + refercase.getMDPhone3();
    }
    catch (Exception localException41)
    {
    }
    ref.commitData();
    try
    {
      en.setReferralID(ref.getUniqueID());
    }
    catch (Exception localException42)
    {
    }
    try {
      en.setUniqueCreateDate(PLCUtils.getNowDate(false));
    }
    catch (Exception localException43)
    {
    }
    try {
      en.setUniqueModifyDate(PLCUtils.getNowDate(false));
    }
    catch (Exception localException44)
    {
    }
    try {
      en.setUniqueModifyComments("Created by Online Form");
    }
    catch (Exception localException45) {
    }
    try {
      en.setRequiresHandCarryCD(Integer.valueOf(refercase.getCd()));
    }
    catch (Exception localException46)
    {
    }
    try {
      en.setRequiresFilms(Integer.valueOf(refercase.getFilms()));
    }
    catch (Exception localException47)
    {
    }
    try {
      en.setRequiresAgeInjury(Integer.valueOf(refercase.getReport()));
    }
    catch (Exception localException48)
    {
    }
    try {
      en.setisSTAT(Integer.valueOf(refercase.getStat()));
    }
    catch (Exception localException49)
    {
    }
    try {
      en.setEncounterStatusID(Integer.valueOf(1));
    }
    catch (Exception localException50)
    {
    }

    try
    {
      en.setRequiresHandCarryCD(Integer.valueOf(1));
    }
    catch (Exception localException51)
    {
    }

    en.commitData();
    en.setScanPass("SP" + en.getUniqueID() + RandomString.generateString(2).toUpperCase() + Math.round(Math.random() * 100.0D) + RandomString.generateString(2).toUpperCase());
    en.commitData();

    commtracks = commtracks + "\n\nDate of Injury: " + refercase.getPatientDOI() + 
      "\nDate of Birth: " + refercase.getPatientDOB() + 
      "\nFollow up Appt: " + refercase.getFollowUpDate();
    try
    {
      ct.setCaseID(ca.getUniqueID());
      ct.setReferralID(ref.getUniqueID());
      ct.setEncounterID(en.getUniqueID());
      ct.setMessageText(commtracks);
      ct.setCommStart(new Date());
      ct.setCommEnd(new Date());
      ct.setMessageName(ua.getContactFirstName() + " " + ua.getContactLastName() + " " + ua.getUserID());
      ct.setMessageCompany("Adj Web Portal");

      ct.commitData();
    }
    catch (Exception localException52)
    {
    }
    try {
      if (refercase.getFileRx().getOriginalFilename().length() > 0) {
        String fileName = uploadFile(refercase.getFileRx());
        if (!fileName.isEmpty()) {
          docRx.setCaseID(ca.getUniqueID());
          docRx.setFileName(fileName);
          docRx.setReferralID(ref.getUniqueID());
          docRx.setUploadUserID(ua.getUserID());
          docRx.setComments("Rx");
          docRx.setDocName("Rx");
          docRx.setUniqueModifyComments(ua.getContactFirstName() + "_" + ua.getContactLastName() + "[" + ua.getUserID() + "]");
          docRx.commitData();
          ref.setRxFileID(docRx.getUniqueID());
          ref.commitData();
        }
      }
    } catch (Exception localException53) {
    }
    try {
      if (refercase.getFileOther().getOriginalFilename().length() > 0) {
        String fileName = uploadFile(refercase.getFileOther());
        if (!fileName.isEmpty()) {
          docOther.setCaseID(ca.getUniqueID());
          docOther.setFileName(fileName);
          docOther.setReferralID(ref.getUniqueID());
          docOther.setEncounterID(en.getUniqueID());
          docOther.setUploadUserID(ua.getUserID());
          docOther.setComments("Order");
          docOther.setDocName("Or");
          docOther.setUniqueModifyComments(ua.getContactFirstName() + "_" + ua.getContactLastName() + "[" + ua.getUserID() + "]");
          docOther.commitData();
          ref.setOrderFileID(docOther.getUniqueID());
          ref.commitData();
        }
      }
    } catch (Exception localException54) {
    }
    NIM3_EncounterObject eo = new NIM3_EncounterObject(en.getUniqueID());
    eo.ProcessEncounter(ua);

    return refercase;
  }

  public static boolean email(String eTo, String eFrom, String eSubject, String theBody) {
    try {
      bltEmailTransaction email = new bltEmailTransaction();
      email.setEmailTo(eTo);
      email.setEmailFrom(eFrom);
      email.setEmailSubject(eSubject);
      email.setEmailBody(theBody);
      email.setEmailBodyType(emailType_V3.PLAIN_TYPE);
      email.setTransactionDate(new Date());
      email.commitData(); } catch (Exception e) {
      return false;
    }
    return true;
  }
  public static boolean emailContactUs(ContactUs cu, bltUserAccount ua) {
    String theBody = 
      "FROM:\n" + 
      cu.getfName() + " " + cu.getlName() + "\n" + 
      "Email: " + cu.getContactEmail() + "\n" + 
      "Phone: " + cu.getArea() + "-" + cu.getPre() + "-" + cu.getExch() + "\n\n" + 
      "Contact by: " + (cu.getEmail() != null ? "Email " : "") + ((cu.getEmail() != null) && (cu.getPhone() != null) ? "& " : "") + (cu.getPhone() != null ? "Phone " : "") + "\n" + 
      "NOTES: \n" + 
      cu.getTa();

    return email("po.le@nextimagemedical.com", "support@nextimagemedical.com", "Contact Us", theBody);
  }
  public static ContactUs getContactUs(bltUserAccount ua) {
    return new ContactUs(ua.getContactFirstName(), ua.getContactLastName(), ua.getContactEmail().contains("@") ? ua.getContactEmail() : "", "", "", "", "", "email", "");
  }
  public static boolean emailCaseInquiry(String theBody, int eid, bltUserAccount ua) {
    NIM3_EncounterObject2 eo2 = new NIM3_EncounterObject2(Integer.valueOf(eid), "");
    String tempBody = "";
    tempBody = 
      "FROM:\n" + 
      ua.getContactFirstName() + " " + ua.getContactLastName() + " [" + ua.getUserID() + "]" + "\n" + 
      ua.getContactEmail() + "\n" + 
      ua.getContactPhone() + "\n\n" + 
      "Patient: " + eo2.getNIM3_CaseAccount().getPatientFirstName() + " " + eo2.getNIM3_CaseAccount().getPatientLastName() + "\n" + 
      "Scanpass: " + eo2.getNIM3_Encounter().getScanPass() + "\n\n" + 
      "NOTES: \n" + 
      theBody;
    return email("Scheduling@nextimagemedical.com", "support@nextimagemedical.com", "Client Portal Case Inquiry - " + eo2.getNIM3_Encounter().getScanPass(), tempBody);
  }

  public static int setUserAccount(bltUserAccount ua, userSettings us, String pw)
  {
    ua.setLogonUserPassword(pw);
    ua.setContactEmail(us.getGroupEmail());
    ua.setContactPhone(us.getDirectPhone1());
    ua.setContactFax(us.getGroupFax1());
    ua.setComm_Report_LevelUser(Integer.valueOf(us.getReports()));
    ua.setComm_Alerts_LevelUser(Integer.valueOf(us.getScheduled()));
    ua.setComm_Alerts2_LevelUser(Integer.valueOf(us.getOther()));
    try {
      ua.commitData();
      return 3;
    } catch (SQLException e) {
      e.printStackTrace();
    }return 9;
  }

  public static int setUserAccount(bltUserAccount ua, userSettings us)
  {
    ua.setContactEmail(us.getGroupEmail());
    ua.setContactPhone(us.getDirectPhone1());
    ua.setContactFax(us.getGroupFax1());
    ua.setComm_Report_LevelUser(Integer.valueOf(us.getReports()));
    ua.setComm_Alerts_LevelUser(Integer.valueOf(us.getScheduled()));
    ua.setComm_Alerts2_LevelUser(Integer.valueOf(us.getOther()));
    try {
      ua.commitData();
      return 3;
    } catch (SQLException e) {
      e.printStackTrace();
    }return 9;
  }

  public static boolean beta(String theBody, bltUserAccount ua)
  {
    try
    {
      bltEmailTransaction email = new bltEmailTransaction();
      email.setEmailTo("po.le@nextimagemedical.com");
      email.setEmailFrom("support@nextimagemedical.com");
      email.setEmailSubject("Beta Inquiry");
      email.setEmailBody(theBody);
      email.setEmailBodyType(emailType_V3.PLAIN_TYPE);
      email.setTransactionDate(new Date());
      email.commitData(); } catch (Exception e) {
      return false;
    }
    return true;
  }

  public static List<CaseList> getSearchResults(bltUserAccount CurrentUserAccount, String search)
  {
    searchDB2 mySS = new searchDB2();
    String mySQL = "null";
    String customWhere;
    if (search.contains(" ")) {
      String[] query = search.split(" ");
      customWhere = "and (ca.patientfirstname ~ '" + query[0] + "' or ca.patientlastname ~ '" + query[1] + "')";
    }
    else
    {
      if (search.contains(",")) {
        String[] query = search.split(",");
        customWhere = "and (ca.patientfirstname ~ '" + query[1] + "' or ca.patientlastname ~ '" + query[0] + "')";
      }
      else {
        customWhere = "and (ca.patientfirstname ~ '" + search + "' or ca.patientlastname ~ '" + search + "' or ca.caseclaimnumber ~ '" + search + "')"; } 
    }mySQL = "select en.encounterid from tnim3_caseaccount ca INNER JOIN tnim3_referral ref on ref.caseid = ca.caseid INNER JOIN tnim3_encounter en on en.referralid = ref.referralid where (ca.adjusterid = '" + CurrentUserAccount.getUserID() + "' or ca.nursecasemanagerid = '" + CurrentUserAccount.getUserID() + "' or ca.caseadministratorid = '" + CurrentUserAccount.getUserID() + "' or ca.caseadministrator2id = '" + CurrentUserAccount.getUserID() + "' or ca.caseadministrator3id = '" + CurrentUserAccount.getUserID() + "' or ca.caseadministrator4id = '" + CurrentUserAccount.getUserID() + "') " + customWhere;
    ResultSet myRS = mySS.executeStatement(mySQL);
    List caselist = new ArrayList();
    try {
      while ((myRS != null) && (myRS.next())) {
        NIM3_EncounterObject2 eo2 = new NIM3_EncounterObject2(Integer.valueOf(myRS.getInt("encounterid")), "loading");
        caselist.add(new CaseList(eo2.getNIM3_CaseAccount().getPatientFirstName() + " " + eo2.getNIM3_CaseAccount().getPatientLastName(), eo2.getNIM3_CaseAccount().getCaseClaimNumber(), PLCUtils.getDisplayDate(eo2.getNIM3_Referral().getReceiveDate(), false), eo2.getNIM3_Encounter().getEncounterID().intValue()));
      }
      mySS.closeAll();
    }
    catch (Exception localException) {
    }
    return caselist;
  }

  public static boolean commTrack(String theBody, int eid, bltUserAccount ua) {
    NIM3_EncounterObject2 eo2 = new NIM3_EncounterObject2(Integer.valueOf(eid), "");

    bltNIM3_CommTrack ct = new bltNIM3_CommTrack();

    ct.setCaseID(eo2.getNIM3_CaseAccount().getCaseID());
    ct.setReferralID(eo2.getNIM3_Referral().getReferralID());
    ct.setEncounterID(eo2.getNIM3_Encounter().getEncounterID());
    ct.setUniqueCreateDate(new Date());
    ct.setUniqueModifyDate(new Date());
    ct.setUniqueModifyComments(ua.getContactFirstName() + "_" + ua.getContactLastName() + "_[" + ua.getUserID() + "]");
    ct.setMessageName(ua.getContactFirstName() + " " + ua.getContactLastName());
    ct.setMessageText(theBody);
    ct.setMessageCompany(ua.getCompanyName());
    try {
      ct.commitData();
    } catch (SQLException e) {
      e.printStackTrace();
    }

    return false;
  }

  private static String uploadFile(CommonsMultipartFile fileData) {
    SimpleDateFormat fileDF = new SimpleDateFormat("yyyy-MM-dd");
    String sFileDF = "doc_" + RandomString.generateString(6).toLowerCase() + "_" + fileDF.format(PLCUtils.getNowDate(false)) + "_" + toFileSafe(fileData.getOriginalFilename());
    FileOutputStream outputStream = null;
    String filePath = ConfigurationInformation.sUploadFolderDirectory + sFileDF;
    try {
      outputStream = new FileOutputStream(new File(filePath));
      outputStream.write(fileData.getFileItem().get());
      outputStream.close();
    } catch (Exception e) {
      sFileDF = "";
    }

    return sFileDF;
  }

  public static String testuploadFile(TestForm tf)
  {
    SimpleDateFormat fileDF = new SimpleDateFormat("yyyy-MM-dd");
    String sFileDF = "doc_" + RandomString.generateString(6).toLowerCase() + "_" + fileDF.format(PLCUtils.getNowDate(false)) + "_" + toFileSafe(tf.getFileData().getOriginalFilename());
    FileOutputStream outputStream = null;
    String filePath = ConfigurationInformation.sUploadFolderDirectory + sFileDF;
    try {
      outputStream = new FileOutputStream(new File(filePath));
      outputStream.write(tf.getFileData().getFileItem().get());
      outputStream.close();
    } catch (Exception e) {
      sFileDF = "";
    }

    return sFileDF;
  }

  private static String toFileSafe(String param)
  {
    return param.replaceAll("\\W+", "_").toLowerCase().replaceAll("_pdf", ".pdf").replaceAll("_tif", ".tif");
  }

  public static String getDocumentName(int file_id)
  {
    return new bltNIM3_Document(Integer.valueOf(file_id)).getFileName();
  }

  private static List<ServiceItem> getServiceItems(Enumeration elements)
  {
    List serviceList = new ArrayList();
    Enumeration eList_Service = elements;
    while (eList_Service.hasMoreElements()) {
      ListElement leCurrentElement_Service = (ListElement)eList_Service.nextElement();
      bltNIM3_Service working_bltNIM3_Service = (bltNIM3_Service)leCurrentElement_Service.getObject();

      boolean isVoid = false;
      if (working_bltNIM3_Service.getServiceStatusID().intValue() == 5) {
        isVoid = true;
      }
      serviceList.add(new ServiceItem(working_bltNIM3_Service.getCPT(), NIMUtils.getCPTText(working_bltNIM3_Service.getCPT()), isVoid));
    }

    return serviceList;
  }

  private static apptDetails getApptDetails(int AppointmentID) {
    SimpleDateFormat df = new SimpleDateFormat("EEEE MMMM dd, yyyy hh:mma");

    bltNIM3_Appointment appt = new bltNIM3_Appointment(Integer.valueOf(AppointmentID));
    bltPracticeMaster pm = new bltPracticeMaster(appt.getProviderID());

    return new apptDetails(AppointmentID, df.format(appt.getAppointmentTime()), pm.getPracticeName(), pm.getOfficeAddress1() + " " + pm.getOfficeAddress2(), pm.getOfficeCity(), new bltStateLI(pm.getOfficeStateID()).getShortState(), pm.getOfficeZIP(), pm.getOfficePhone(), pm.getOfficeFaxNo(), pm.getPracticeID());
  }

  public static LinkedHashMap<Integer, String> typeList()
  {
    LinkedHashMap lhm = new LinkedHashMap();
    lhm.put(Integer.valueOf(0), "Select");
    lhm.put(Integer.valueOf(1), "Adjuster");
    lhm.put(Integer.valueOf(2), "Nurse Manager");
    lhm.put(Integer.valueOf(3), "Case Manager");

    return lhm;
  }

  public static LinkedHashMap<Integer, String> alertNotify() {
    LinkedHashMap lhm = new LinkedHashMap();
    lhm.put(Integer.valueOf(0), "N/A");
    lhm.put(Integer.valueOf(1), "No Alerts");
    lhm.put(Integer.valueOf(3), "Email Attachment");
    lhm.put(Integer.valueOf(4), "Fax");
    return lhm;
  }

  public static LinkedHashMap<Integer, String> getYesNo() {
    LinkedHashMap lhm = new LinkedHashMap();
    lhm.put(Integer.valueOf(1), "Yes");
    lhm.put(Integer.valueOf(2), "No");

    return lhm;
  }

  public static LinkedHashMap<Integer, String> getStates() throws SQLException {
    LinkedHashMap lhm = new LinkedHashMap();

    searchDB2 conn = new searchDB2();
    String query = "select stateid, longstate from tstateli order by longstate";
    ResultSet rs = conn.executeStatement(query);

    while (rs.next()) {
      lhm.put(Integer.valueOf(rs.getInt("stateid")), rs.getString("longstate"));
    }

    return lhm;
  }

  public static ResultValidationTyson tysonValidation(TysonReferralForm trf, bltUserAccount ua) {
    boolean goodReferral = true;
    ResultValidationTyson rvt = new ResultValidationTyson();

    if (trf.getLinkType() == 0) {
      goodReferral = false;
      rvt.setLinkType(true);
    }

    if (trf.getClaimNumber().equals("")) {
      goodReferral = false;
      rvt.setClaimNumber(true);
    }
    if (trf.getAdjusterFirstName().equals("")) {
      goodReferral = false;
      rvt.setAdjusterFirstName(true);
    }
    if (trf.getAdjusterLastName().equals("")) {
      goodReferral = false;
      rvt.setAdjusterLastName(true);
    }
    if (trf.getNcmFirstName().equals("")) {
      goodReferral = false;
      rvt.setNcmFirstName(true);
    }
    if (trf.getNcmLastName().equals("")) {
      goodReferral = false;
      rvt.setNcmLastName(true);
    }
    if (trf.getTeamMemberFirstName().equals("")) {
      goodReferral = false;
      rvt.setTeamMemberFirstName(true);
    }
    if (trf.getTeamMemberLastName().equals("")) {
      goodReferral = false;
      rvt.setTeamMemberLastName(true);
    }
    if (trf.getTeamMemberAddress().equals("")) {
      goodReferral = false;
      rvt.setTeamMemberAddress(true);
    }
    if (trf.getTeamMemberCity().equals("")) {
      goodReferral = false;
      rvt.setTeamMemberCity(true);
    }
    if (trf.getTeamMemberState() == 0) {
      goodReferral = false;
      rvt.setTeamMemberState(true);
    }
    if (trf.getTeamMemberZip().equals("")) {
      goodReferral = false;
      rvt.setTeamMemberZip(true);
    }

    if (trf.getReferringPhysicianFirstName().equals("")) {
      goodReferral = false;
      rvt.setReferringPhysicianFirstName(true);
    }
    if (trf.getReferringPhysicianLastName().equals("")) {
      goodReferral = false;
      rvt.setReferringPhysicianLastName(true);
    }
    if (trf.getPhysicianPhone1().equals("")) {
      goodReferral = false;
      rvt.setPhysicianPhone1(true);
    }
    if (trf.getPhysicianPhone2().equals("")) {
      goodReferral = false;
      rvt.setPhysicianPhone2(true);
    }
    if (trf.getPhysicianPhone3().equals("")) {
      goodReferral = false;
      rvt.setPhysicianPhone3(true);
    }
    if (trf.getPhysicianFax1().equals("")) {
      goodReferral = false;
      rvt.setPhysicianFax1(true);
    }
    if (trf.getPhysicianFax2().equals("")) {
      goodReferral = false;
      rvt.setPhysicianFax2(true);
    }
    if (trf.getPhysicianFax3().equals("")) {
      goodReferral = false;
      rvt.setPhysicianFax3(true);
    }
    if (trf.getProc().isEmpty()) {
      goodReferral = false;
      rvt.setProc(true);
    }
    if (trf.getBp().isEmpty()) {
      goodReferral = false;
      rvt.setBp(true);
    }
    if (trf.getRuleOut().isEmpty()) {
      goodReferral = false;
      rvt.setRuleOut(true);
    }
    if (goodReferral) {
      try {
        submitTysonCase(trf, ua);
        rvt.setGoodReferral(true);
      } catch (SQLException e) {
        e.printStackTrace();
      }
    }
    return rvt;
  }

  public static TysonReferralForm submitTysonCase(TysonReferralForm rc, bltUserAccount ua) throws SQLException {
    bltNIM3_CommTrack ct = new bltNIM3_CommTrack();
    bltNIM3_CaseAccount ca = new bltNIM3_CaseAccount();
    bltNIM3_Referral ref = new bltNIM3_Referral();
    bltNIM3_Encounter en = new bltNIM3_Encounter();
    bltNIM3_Document docRx = new bltNIM3_Document();
    bltNIM3_Document docOther = new bltNIM3_Document();
    String commtracks = "From Tyson Adjuster Web Portal\n\n";
    try
    {
      ca.setCaseStatusID(new Integer(1)); } catch (Exception localException) {
    }
    try {
      ca.setCaseClaimNumber(rc.getClaimNumber()); } catch (Exception localException1) {
    }
    try {
      ca.setPayerID(ua.getPayerID());
    } catch (Exception localException2) {
    }
    if (rc.getLinkType() == 1)
      try {
        ca.setAdjusterID(ua.getUserID());
      } catch (Exception localException3) {
      }
    else if (rc.getLinkType() == 2)
      try {
        ca.setNurseCaseManagerID(ua.getUserID());
      } catch (Exception localException4) {
      }
    else if (rc.getLinkType() == 3)
      try {
        ca.setCaseAdministratorID(ua.getUserID());
      }
      catch (Exception localException5)
      {
      }
    try {
      ca.setPatientFirstName(rc.getTeamMemberFirstName());
    }
    catch (Exception localException6) {
    }
    try {
      ca.setPatientLastName(rc.getTeamMemberLastName()); } catch (Exception localException7) {
    }
    try {
      ca.setPatientAddress1(rc.getTeamMemberAddress()); } catch (Exception localException8) {
    }
    try {
      ca.setPatientCity(rc.getTeamMemberCity()); } catch (Exception localException9) {
    }
    try {
      ca.setPatientStateID(Integer.valueOf(rc.getTeamMemberState())); } catch (Exception localException10) {
    }
    try {
      ca.setPatientZIP(rc.getTeamMemberZip()); } catch (Exception localException11) {
    }
    try {
      ca.setPatientHomePhone(rc.getTeamMemberPhone1() + "-" + rc.getTeamMemberPhone2() + "-" + rc.getTeamMemberPhone3()); } catch (Exception localException12) {
    }
    try {
      ca.setPatientCellPhone(rc.getTeamMemberSecondPhone1() + "-" + rc.getTeamMemberSecondPhone2() + "-" + rc.getTeamMemberSecondPhone3()); } catch (Exception localException13) {
    }
    try {
      ca.setDateOfInjury(new SimpleDateFormat("MM/dd/yyyy").parse(rc.getDOImonth() + "/" + rc.getDOIday() + "/" + rc.getDOIyear()));
    } catch (Exception localException14) {
    }
    try {
      ca.setPatientDOB(new SimpleDateFormat("MM/dd/yyyy").parse(rc.getDOBmonth() + "/" + rc.getDOBday() + "/" + rc.getDOByear()));
    } catch (Exception localException15) {
    }
    try {
      ca.setPatientHeight(rc.getHeight()); } catch (Exception localException16) {
    }
    try {
      ca.setPatientWeight(Integer.valueOf(rc.getWeight())); } catch (Exception localException17) {
    }
    try {
      ca.setPatientIsClaus(Integer.valueOf(rc.getIsClaust())); } catch (Exception localException18) {
    }
    try {
      ca.setPatientHasMetalInBody(Integer.valueOf(rc.getHasMetal())); } catch (Exception localException19) {
    }
    try {
      ca.setPatientHasMetalInBodyDesc(rc.getMetalDetails()); } catch (Exception localException20) {
    }
    try {
      en.setisSTAT(Integer.valueOf(rc.isStat));
    }
    catch (Exception localException21) {
    }
    commtracks = commtracks + 
      "Claims Office:" + rc.getClaimsOffice() + "\n" + 
      "Plant Location:" + rc.getPlantLocation() + "\n" + 
      "Claims Number: " + rc.getClaimNumber() + "\n" + 
      "Precert #: " + rc.getPrecertNumber() + "\n" + 
      "Adj: " + rc.getAdjusterFirstName() + " " + rc.getAdjusterLastName() + "\n" + 
      "Phone: " + rc.getAdjusterPhone1() + "-" + rc.getAdjusterPhone2() + "-" + rc.getAdjusterPhone3() + "\n" + 
      "Fax: " + rc.getAdjusterFax1() + "-" + rc.getAdjusterFax2() + "-" + rc.getAdjusterFax3() + "\n" + 
      "Email: " + rc.getAdjusterEmail() + "\n\n" + 
      "Nurse Case Manager Name: " + rc.getNcmFirstName() + " " + rc.getNcmLastName() + "\n" + 
      "Nurse Case Manager Email: " + rc.getNcmEmail() + "\n" + 
      "Nurse Case Manager Phone: " + rc.getNcmPhone1() + "-" + rc.getNcmPhone2() + "-" + rc.getNcmPhone3() + "\n" + 
      "Nurse Case Manager Fax: " + rc.getNcmFax1() + "-" + rc.getNcmFax2() + "-" + rc.getNcmFax3() + "\n\n" + 
      "Case Admin1 Name: " + rc.getCaseAdmin1FirstName() + " " + rc.getCaseAdmin1LastName() + "\n" + 
      "Case Admin1 Email: " + rc.getCaseAdmin1Email() + "\n" + 
      "Case Admin1 Phone: " + rc.getCaseAdmin1Phone1() + "-" + rc.getCaseAdmin1Phone2() + "-" + rc.getCaseAdmin1Phone3() + "\n" + 
      "Case Admin1 Fax: " + rc.getCaseAdmin1Fax1() + "-" + rc.getCaseAdmin1Fax2() + "-" + rc.getCaseAdmin1Fax3() + "\n\n";
    try
    {
      commtracks = commtracks + "Case Admin2 Name: " + rc.caseManagerFirstName[0] + " " + rc.caseManagerLastName[0] + "\n" + 
        "Case Admin2 Email" + rc.caseManagerEmail[0] + "\n" + 
        "Case Admin2 Phone " + rc.cmPhoneAreaCode[0] + "-" + rc.cmPhonePrefix[0] + "-" + rc.cmPhoneExchange[0] + "\n" + 
        "Case Admin2 Fax " + rc.cmfaxAreaCode[0] + "-" + rc.cmfaxPrefix[0] + "-" + rc.cmfaxExchange[0] + "\n\n";
    }
    catch (Exception localException22) {
    }
    try {
      commtracks = commtracks + "Case Admin3 Name: " + rc.caseManagerFirstName[1] + rc.caseManagerLastName[1] + "\n" + 
        "Case Admin3 Email" + rc.caseManagerEmail[1] + "\n" + 
        "Case Admin3 Phone " + rc.cmPhoneAreaCode[1] + "-" + rc.cmPhonePrefix[1] + "-" + rc.cmPhoneExchange[1] + "\n" + 
        "Case Admin3 Fax " + rc.cmfaxAreaCode[1] + "-" + rc.cmfaxPrefix[1] + "-" + rc.cmfaxExchange[1] + "\n\n";
    }
    catch (Exception localException23) {
    }
    try {
      commtracks = commtracks + "Case Admin4 Name: " + rc.caseManagerFirstName[2] + rc.caseManagerLastName[2] + "\n" + 
        "Case Admin4 Email" + rc.caseManagerEmail[2] + "\n" + 
        "Case Admin4 Phone " + rc.cmPhoneAreaCode[2] + "-" + rc.cmPhonePrefix[2] + "-" + rc.cmPhoneExchange[2] + "\n" + 
        "Case Admin4 Fax " + rc.cmfaxAreaCode[2] + "-" + rc.cmfaxPrefix[2] + "-" + rc.cmfaxExchange[2] + "\n\n";
    }
    catch (Exception localException24)
    {
    }
    commtracks = commtracks + "IW Name: " + rc.getTeamMemberFirstName() + " " + rc.getTeamMemberLastName() + "\n" + 
      "IW DOB: " + rc.getDOBmonth() + "/" + rc.getDOBday() + "/" + rc.getDOByear() + "\n" + 
      "IW Address: " + rc.getTeamMemberAddress() + " " + rc.getTeamMemberCity() + " " + new bltStateLI(Integer.valueOf(rc.getTeamMemberState())).getShortState() + " " + rc.getTeamMemberZip() + "\n" + 
      "IW Home Phone: " + rc.getTeamMemberPhone1() + "-" + rc.getTeamMemberPhone2() + "-" + rc.getTeamMemberPhone3() + "\n" + 
      "IW Alternate Phone: " + rc.getTeamMemberSecondPhone1() + "-" + rc.getTeamMemberSecondPhone2() + "-" + rc.getTeamMemberSecondPhone3() + "\n" + 
      "IW DOI: " + rc.getDOImonth() + "/" + rc.getDOIday() + "/" + rc.getDOIyear() + "\n\n" + 
      "Prefered Date and Time: " + rc.getTeamMemberPreferredAppointmentTime1() + "\n" + 
      "Alternate Date and Time: " + rc.getTeamMemberPreferredAppointmentTime2() + "\n" + 
      "Team Member Shift: " + rc.getTeamMemberShift() + "\n" + 
      "Prefered Sched Zip: " + rc.getScheduleNearZip() + "\n\n" + 
      "Physician name: " + rc.getReferringPhysicianFirstName() + " " + rc.getReferringPhysicianLastName() + "\n" + 
      "Physician Phone: " + rc.getPhysicianPhone1() + "-" + rc.getPhysicianPhone2() + "-" + rc.physicianPhone3 + "\n" + 
      "Physician Fax: " + rc.getPhysicianFax1() + "-" + rc.getPhysicianFax2() + "-" + rc.getPhysicianFax3() + "\n\n";
    try
    {
      ca.setComments(
        "Special requests: " + rc.getSpecialInstructions() + "\n" + 
        "MD Follow Up Appt: " + rc.getFollowupAppointment() + "\n" + 
        "Scheduled at: " + rc.getIsScheduledAlready() + "\n");
    } catch (Exception localException25) {
    }
    try {
      ct.setCaseID(ca.getUniqueID());
      ct.setReferralID(ref.getUniqueID());
      ct.setEncounterID(en.getUniqueID());
      ct.setMessageText(commtracks);
      ct.setAlertStatusCode(Integer.valueOf(1));
      ct.setCommStart(PLCUtils.getNowDate(false));
      ct.setCommEnd(PLCUtils.getNowDate(false));
      ct.setMessageName(ua.getContactFirstName() + " " + ua.getContactLastName() + " " + ua.getUserID());
      ct.setMessageCompany("Tyson Web Portal");

      ct.commitData(); } catch (Exception localException26) {
    }
    try {
      ca.setUniqueCreateDate(PLCUtils.getNowDate(false));
    }
    catch (Exception localException27) {
    }
    try {
      ca.setUniqueModifyDate(PLCUtils.getNowDate(false));
    } catch (Exception localException28) {
    }
    try {
      ca.setUniqueModifyComments("Created by NID Online Form V2");
    } catch (Exception localException29) {
    }
    ca.commitData();
    ca.setCaseCode("CP" + ca.getUniqueID() + RandomString.generateString(2).toUpperCase() + Math.round(Math.random() * 100.0D) + RandomString.generateString(2).toUpperCase());
    ca.commitData();
    try
    {
      String procList = "";

      for (int x = 0; x < rc.getProc().size(); x++) {
        procList = procList + "Procedure " + (x + 1) + ": " + (String)rc.getProc().get(x) + "\n" + 
          "Body Part " + (x + 1) + ": " + (String)rc.getBp().get(x) + "\n" + 
          "Rule Out " + (x + 1) + ": " + (String)rc.getRuleOut().get(x) + "\n";

        commtracks = commtracks + "Procedure " + (x + 1) + ": " + (String)rc.getProc().get(x) + "\n" + 
          "Body Part " + (x + 1) + ": " + (String)rc.getBp().get(x) + "\n" + 
          "Rule Out " + (x + 1) + ": " + (String)rc.getRuleOut().get(x) + "\n\n";
      }
      ref.setComments(procList);
    } catch (Exception localException30) {
    }
    try {
      ref.setCaseID(ca.getUniqueID()); } catch (Exception localException31) {
    }
    try {
      ref.setUniqueCreateDate(PLCUtils.getNowDate(false)); } catch (Exception localException32) {
    }
    try {
      ref.setUniqueModifyDate(PLCUtils.getNowDate(false)); } catch (Exception localException33) {
    }
    try {
      ref.setUniqueModifyComments("Created by NID Online Form V2"); } catch (Exception localException34) {
    }
    try {
      ref.setReferralTypeID(Integer.valueOf(3)); } catch (Exception localException35) {
    }
    try {
      ref.setReferralDate(PLCUtils.getNowDate(true)); } catch (Exception localException36) {
    }
    try {
      ref.setReceiveDate(PLCUtils.getNowDate(true)); } catch (Exception localException37) {
    }
    try {
      ref.setReferralStatusID(Integer.valueOf(1)); } catch (Exception localException38) {
    }
    try {
      ref.setReferralMethod("Online (Sec)"); } catch (Exception localException39) {
    }
    try {
      ref.setAttendingPhysicianID(Integer.valueOf(52)); } catch (Exception localException40) {
    }
    try {
      ref.setReferredByContactID(ua.getUserID()); } catch (Exception localException41) {
    }
    ref.setComments(commtracks);
    ref.commitData();
    try
    {
      en.setEncounterStatusID(Integer.valueOf(1)); } catch (Exception localException42) {
    }
    try {
      en.setReferralID(ref.getUniqueID());
    }
    catch (Exception localException43) {
    }
    try {
      en.setComments(
        "Special requests: " + rc.getSpecialInstructions() + "\n" + 
        "MD Follow Up Appt: " + rc.getFollowupAppointment() + "\n");
    } catch (Exception localException44) {
    }
    try {
      en.setPatientAvailability("Scheduled, at: " + rc.getIsScheduledAlready() + "<br />" + "Prefered appointment 1:" + rc.getTeamMemberPreferredAppointmentTime1() + "<br />" + "Prefered appointment 2: " + rc.getTeamMemberPreferredAppointmentTime2() + "<br />" + "Follow Up Appt:" + rc.getFollowupAppointment() + "Team Member Shift: " + rc.getTeamMemberShift());
    }
    catch (Exception localException45) {
    }
    try {
      en.setUniqueCreateDate(PLCUtils.getNowDate(false)); } catch (Exception localException46) {
    }
    try {
      en.setUniqueModifyDate(PLCUtils.getNowDate(false)); } catch (Exception localException47) {
    }
    try {
      en.setUniqueModifyComments("Created by Online Portal"); } catch (Exception localException48) {
    }
    try {
      en.setRequiresHandCarryCD(Integer.valueOf(rc.getHandCarryCD())); } catch (Exception localException49) {
    }
    try {
      en.setRequiresFilms(Integer.valueOf(rc.getHandCarryFilms())); } catch (Exception localException50) {
    }
    try {
      en.setRequiresAgeInjury(Integer.valueOf(rc.getAgeReport())); } catch (Exception localException51) {
    }
    try {
      en.setisSTAT(Integer.valueOf(rc.getIsStat())); } catch (Exception localException52) {
    }
    try {
      en.setEncounterStatusID(Integer.valueOf(1));
    }
    catch (Exception localException53) {
    }
    en.commitData();
    en.setScanPass("SP" + en.getUniqueID() + RandomString.generateString(2).toUpperCase() + Math.round(Math.random() * 100.0D) + RandomString.generateString(2).toUpperCase());
    en.commitData();
    try
    {
      if (rc.getUpload1().getOriginalFilename().length() > 0) {
        String fileName = uploadFile(rc.getUpload1());
        if (!fileName.isEmpty()) {
          docRx.setCaseID(ca.getUniqueID());
          docRx.setFileName(fileName);
          docRx.setReferralID(ref.getUniqueID());
          docRx.setUploadUserID(ua.getUserID());
          docRx.setComments("Rx");
          docRx.setDocName("Rx");
          docRx.setUniqueModifyComments(ua.getContactFirstName() + "_" + ua.getContactLastName() + "[" + ua.getUserID() + "]");
          docRx.commitData();
          ref.setRxFileID(docRx.getUniqueID());
          ref.commitData();
        }
      }
    } catch (Exception localException54) {
    }
    try { if (rc.getUpload2().getOriginalFilename().length() > 0) {
        String fileName = uploadFile(rc.getUpload2());
        if (!fileName.isEmpty()) {
          docOther.setCaseID(ca.getUniqueID());
          docOther.setFileName(fileName);
          docOther.setReferralID(ref.getUniqueID());
          docOther.setEncounterID(en.getUniqueID());
          docOther.setUploadUserID(ua.getUserID());
          docOther.setComments("Order");
          docOther.setDocName("Or");
          docOther.setUniqueModifyComments(ua.getContactFirstName() + "_" + ua.getContactLastName() + "[" + ua.getUserID() + "]");
          docOther.commitData();
          ref.setOrderFileID(docOther.getUniqueID());
          ref.commitData();
        }
      }
    } catch (Exception localException55) {
    }
    NIM3_EncounterObject eo = new NIM3_EncounterObject(en.getUniqueID());
    eo.ProcessEncounter(ua);
    commTrack(commtracks, en.getUniqueID().intValue(), ua);
    return rc;
  }
}