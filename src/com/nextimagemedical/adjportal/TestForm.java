package com.nextimagemedical.adjportal;

import org.springframework.web.multipart.commons.CommonsMultipartFile;

public class TestForm {
	private String name;
	private String data;
	private CommonsMultipartFile fileData;
	public TestForm() {
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
	public CommonsMultipartFile getFileData() {
		return fileData;
	}
	public void setFileData(CommonsMultipartFile fileData) {
		this.fileData = fileData;
	}
	
	
}
