package com.nextimagemedical.adjportal;

import com.nextimagemedical.Pass.LinkEncryption;
import com.winstaff.ConfigurationInformation;
import com.winstaff.bltNIM3_PayerMaster;
import com.winstaff.bltUserAccount;
import com.winstaff.password.Encrypt;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.io.IOUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
/**
 * this is a controller, it does stuff. 
 * @author hoppho
 *
 */
@Controller
public class ApplicationControllers
{
  @RequestMapping(value={"home"}, method={org.springframework.web.bind.annotation.RequestMethod.GET})
  public ModelAndView getHomePage(HttpSession session)
  {
    if (session.getAttribute("CurrentUserAccount") == null) {
      return new ModelAndView("customMessage", "cType", Integer.valueOf(1));
    }

    ModelAndView mav = new ModelAndView("home");
    mav.addObject("home", Boolean.valueOf(true));
    mav.addObject("caseList", AppUtils.getCaseList((bltUserAccount)session.getAttribute("CurrentUserAccount")));
    mav.addObject("ua", (bltUserAccount)session.getAttribute("CurrentUserAccount"));

    mav.addObject("pm", new bltNIM3_PayerMaster(((bltUserAccount)session.getAttribute("CurrentUserAccount")).getPayerID()));
    return mav;
  }

  @RequestMapping(value={"userSettings"}, method={org.springframework.web.bind.annotation.RequestMethod.GET})
  public ModelAndView getUserSettings(HttpSession session)
  {
    bltUserAccount ua = (bltUserAccount)session.getAttribute("CurrentUserAccount");
    userSettings us = new userSettings(ua.getContactEmail(), ua.getContactLastName(), ua.getContactFirstName(), 
      ua.getContactPhone(), "", "", 
      ua.getContactFax(), "", "", 
      ua.getContactMobile(), "", "", 
      ua.getComm_Alerts_LevelUser().intValue(), ua.getComm_Report_LevelUser().intValue(), ua.getComm_Alerts2_LevelUser().intValue(), 
      "", "", "");
    ModelAndView mav = new ModelAndView("userSettings");
    mav.addObject("userSettingsNav", Boolean.valueOf(true));
    mav.addObject("command", us);
    mav.addObject("alertNotify", AppUtils.alertNotify());
    return mav;
  }

  @RequestMapping(value={"userSettings"}, method={org.springframework.web.bind.annotation.RequestMethod.POST})
  public ModelAndView setUserSettings(HttpSession session, @ModelAttribute userSettings us) {
    Encrypt myEnc = new Encrypt();
    bltUserAccount ua = (bltUserAccount)session.getAttribute("CurrentUserAccount");
    if (us.getConfirmPassword().isEmpty())
      return new ModelAndView("customMessage", "cType", Integer.valueOf(AppUtils.setUserAccount(ua, us)));
    if ((!us.getConfirmPassword().isEmpty()) && (Encrypt.checkPasswords(ua.getLogonUserPassword(), Encrypt.getMD5Base64(us.getConfirmPassword()))) && (us.getNewPassword().length() > 7) && (us.getNewPassword().equals(us.getVerifyNewPassword()))) {
      return new ModelAndView("customMessage", "cType", Integer.valueOf(AppUtils.setUserAccount(ua, us, Encrypt.getMD5Base64(us.getNewPassword()))));
    }
    us.setConfirmPassword("");
    us.setVerifyNewPassword("");
    us.setNewPassword("");
    ModelAndView mav = new ModelAndView("userSettings");
    mav.addObject("userSettingsNav", Boolean.valueOf(true));
    mav.addObject("command", us);
    mav.addObject("alertNotify", AppUtils.alertNotify());
    mav.addObject("errorMsg", "There was an error changing your password, please try again.");
    mav.addObject("confirm", us.getConfirmPassword());
    mav.addObject("new", us.getNewPassword());
    mav.addObject("verify", us.getVerifyNewPassword());
    return mav;
  }

  @RequestMapping(value={"reports"}, method={org.springframework.web.bind.annotation.RequestMethod.GET})
  public ModelAndView getReports(HttpSession session) {
    ModelAndView mav = new ModelAndView("customMessage", "cType", Integer.valueOf(4));
    mav.addObject("reports", Boolean.valueOf(true));
    return mav;
  }

  @RequestMapping(value={"amerisysQuery"}, method={org.springframework.web.bind.annotation.RequestMethod.GET})
  public ModelAndView getamerisysQuery(HttpSession session) {
    ModelAndView mav = new ModelAndView("amerisysQuery");
    mav.addObject("command", AppUtils.getRp());

    return mav;
  }

  @RequestMapping(value={"contactUs"}, method={org.springframework.web.bind.annotation.RequestMethod.GET})
  public ModelAndView getContactUs(HttpSession session) {
    bltUserAccount ua = (bltUserAccount)session.getAttribute("CurrentUserAccount");
    ModelAndView mav = new ModelAndView("contactUs");
    mav.addObject("command", AppUtils.getContactUs(ua));
    return mav;
  }
  @RequestMapping(value={"contactUs"}, method={org.springframework.web.bind.annotation.RequestMethod.POST})
  public String getContactUs_sub(ContactUs cu, HttpSession session) {
    bltUserAccount ua = (bltUserAccount)session.getAttribute("CurrentUserAccount");
    AppUtils.emailContactUs(cu, ua);
    return "redirect:contactUs_sub";
  }
  @RequestMapping(value={"contactUs_sub"}, method={org.springframework.web.bind.annotation.RequestMethod.GET})
  public ModelAndView getContactUs_msg(HttpSession session) {
    return new ModelAndView("customMessage", "cType", Integer.valueOf(5));
  }
  @RequestMapping(value={"logout"}, method={org.springframework.web.bind.annotation.RequestMethod.GET})
  public String logout(HttpSession session) {
    session.invalidate();
    return "redirect:home";
  }
  @RequestMapping(value={"home"}, method={org.springframework.web.bind.annotation.RequestMethod.POST})
  public String getHomePageStart(@RequestParam("user_id") int user_id, HttpSession session) {
    session.setAttribute("CurrentUserAccount", new bltUserAccount(Integer.valueOf(user_id)));
    return "redirect:home";
  }
  @RequestMapping(value={"casePage"}, method={org.springframework.web.bind.annotation.RequestMethod.POST})
  public ModelAndView getCasePage(@RequestParam("eid") String eid, HttpSession session) throws NumberFormatException, GeneralSecurityException, IOException, ParseException {
    if (session.getAttribute("CurrentUserAccount") == null) {
      return new ModelAndView("customMessage", "cType", Integer.valueOf(1));
    }
    ModelAndView mav = new ModelAndView("casePage");
    mav.addObject("casePage", AppUtils.getCaseDetails(new Integer(LinkEncryption.decrypt(eid)).intValue()));
    return mav;
  }
  @RequestMapping(value={"upload"}, method={org.springframework.web.bind.annotation.RequestMethod.POST})
  public ModelAndView upload(@PathVariable("eid") int eid) throws ParseException, UnsupportedEncodingException, GeneralSecurityException {
    ModelAndView mav = new ModelAndView("casePage");
    mav.addObject("casePage", AppUtils.getCaseDetails(eid));
    return mav;
  }

  @RequestMapping(value={"caseInquiry"}, method={org.springframework.web.bind.annotation.RequestMethod.POST})
  @ResponseBody
  public String caseInquiry(@RequestParam("eid") String eid, @RequestParam("theBody") String theBody, HttpSession session) throws NumberFormatException, GeneralSecurityException, IOException {
    ModelAndView mav = new ModelAndView("caseInquiry");
    AppUtils.commTrack(theBody, new Integer(LinkEncryption.decrypt(eid)).intValue(), (bltUserAccount)session.getAttribute("CurrentUserAccount"));
    mav.addObject("pageMsg", Boolean.valueOf(AppUtils.emailCaseInquiry(theBody, new Integer(LinkEncryption.decrypt(eid)).intValue(), (bltUserAccount)session.getAttribute("CurrentUserAccount"))));
    return "Ok";
  }

  @RequestMapping(value={"betaInquiry"}, method={org.springframework.web.bind.annotation.RequestMethod.POST})
  @ResponseBody
  public String betaInquiry(@RequestParam("theBody") String theBody, HttpSession session) throws NumberFormatException, GeneralSecurityException, IOException {
    AppUtils.beta(theBody, (bltUserAccount)session.getAttribute("CurrentUserAccount"));
    return "Ok";
  }

  @RequestMapping(value={"referCase"}, method={org.springframework.web.bind.annotation.RequestMethod.GET})
  public String getRefer(HttpSession session) throws SQLException {
    List tysonID = Arrays.asList(new Integer[] { Integer.valueOf(138), Integer.valueOf(139), Integer.valueOf(143), Integer.valueOf(147), Integer.valueOf(148), Integer.valueOf(149) });
    if (tysonID.contains(((bltUserAccount)session.getAttribute("CurrentUserAccount")).getPayerID())) {
      return "redirect:Tyson";
    }
    return "redirect:generalRefer";
  }

  @RequestMapping(value={"generalRefer"}, method={org.springframework.web.bind.annotation.RequestMethod.GET})
  public ModelAndView getGen(HttpSession session) throws SQLException
  {
    if (session.getAttribute("CurrentUserAccount") == null) {
      return new ModelAndView("customMessage", "cType", Integer.valueOf(1));
    }

    ModelAndView mav = new ModelAndView("referCase");
    mav.addObject("referCaseNav", Boolean.valueOf(true));

    mav.addObject("yesNo", AppUtils.getYesNo());
    mav.addObject("stateList", AppUtils.getStates());

    mav.addObject("LinkType", AppUtils.typeList());
    mav.addObject("heightList", AppUtils.heightList);

    mav.addObject("procList", AppUtils.procedure);
    mav.addObject("bpList", AppUtils.bodyPart);
    mav.addObject("ruleList", AppUtils.ruleOut);
    mav.addObject("command", new ReferCase());
    return mav;
  }

  @RequestMapping(value={"referCase"}, method={org.springframework.web.bind.annotation.RequestMethod.POST})
  public String submitRefer(@ModelAttribute ReferCase referCase, HttpSession session)
    throws SQLException
  {
    AppUtils.submitCase(referCase, (bltUserAccount)session.getAttribute("CurrentUserAccount"));
    return "redirect:referCaseResponse";
  }

  @RequestMapping(value={"referCaseResponse"}, method={org.springframework.web.bind.annotation.RequestMethod.GET})
  public ModelAndView referCaseResponse()
  {
    return new ModelAndView("customMessage", "cType", Integer.valueOf(2));
  }

  @RequestMapping(value={"Tyson"}, method={org.springframework.web.bind.annotation.RequestMethod.GET})
  public ModelAndView sdfsfs(HttpSession session) throws SQLException {
    List claimsOfficeList = Arrays.asList(new String[] { "Please Select", "Dakota Dunes Claims Office", "Springdale AR Claims Office", "Oxford, AL", "Wilkesboro, NC","Olathe, KS" });
    List teamMemberplantLocationList = Arrays.asList(new String[] { "Please Select", "Albertville - AL", "Amarillo - TX", "Berryville - AR", "Berryville/Green Complex - AR", "Blountsville - AL", "Broken Bow - OK", "Buena Vista Plant - GA", "Buffalo Plant   - NY", "Carthage Complex - TX", "Carthage, MS - MS", "Center Complex - TX", "Cherokee - IA", "Chicago Bruss Plant - IL", "Chicago Culinary Plant - IL", "Clarksville Complex - AR", "Cobb Vantress - AR", "Columbia - NC", "Concordia - MO", "Corydon Complex - IN", "Council Bluffs  -  Case Ready - IA", "Council Bluffs  -  Cooked Meats - IA", "Cumming Complex - GA", "Dakota City Plant - IA", "Dakota Dunes - ND", "Dallas - TX", "Dardanelle Complex - AR", "Dawson - GA", "Denison Plant - IA", "Dexter Complex - MO", "Emporia - KS", "Finney County - KS", "Forest - MS", "Forest RVAF - MS", "Fort Smith Plant - AR", "Fort Worth Plant - TX", "Glen Allen Complex - VA", "Goodlettsville - TN", "Grannis - AR", "Green Bay Plant - WI", "Green Forest - AR", "Harmony - NC", "Harrisonburg Complex - VA", "Hope Complex - AR", "Houston Plant - TX", "Hutchinson, KS - KS", "Hutchinson, KS (Alternate Contact) - KS", "Jefferson Plant - WI", "Joslin - IL", "Lexington Plant - NE", "Logansport - IA", "Louisa County - IA", "Madison - WI", "Monett Complex - MO", "Monroe Complex - NC", "Montgomery City - MO", "MS Live Production - MS", "N. Richland Hills Plant - TX", "Nashville Complex - AR", "New Holland Complex - PA", "Noel Complex - MO", "North Little Rock - AR", "North Richland Hills - AR", "NWA Employment Center - AR", "Obion County Complex - TN", "Oglethorpe - GA", "Olathe - KS", "Olathe - KS", "Omaha Plant - NE", "Ottawa - IL", "Pasco Plant - WA", "Perry - IA", "Pine Bluff Complex - AR", "Portland, IN - IN", "Robards Complex - KY", "Rogers  -  Tyson Distribution Center - AR", "Rogers CNQ Plant - AR", "Russellville  -  TVDC - AR", "Russellville  -  Tyler Road - AR", "Russellville -  TVDC - AR", "RVAF Robards. - KY", "RVAF Texarkana - AR", "RVAFScranton - AR", "S Hutchinson Plant - KS", "Sanford - NC", "Santa Teresa - NM", "Sedalia Complex - MO", "Seguin, TX - TX", "Shelbyville Complex - TN", "Sherman - TX", "Springdale  -  Berry Street - AR", "Springdale  -  Corporate - AR", "Springdale - Randall Road - AR", "Springdale - Transportation - AR", "Springdale Complex - AR", "Storm Lake Plant - IA", "Temperanceville Complex - VA", "Tyson of Fayetteville Plants  - AR", "Tyson of Rogers - AR", "Van Buren - AR", "Vernon Plant - TX", "Vicksburg  -  Ceres - MS", "Vienna - GA", "Waldron Complex - AR", "Waterloo - IA", "Wilkesboro Complex - NC" });
    LinkedHashMap yesNo = new LinkedHashMap();
    yesNo.put(Integer.valueOf(1), "Yes");
    yesNo.put(Integer.valueOf(2), "No");
    if (session.getAttribute("CurrentUserAccount") == null) {
      return new ModelAndView("customMessage", "cType", Integer.valueOf(1));
    }
    ModelAndView mav = new ModelAndView("TysonReferralForm");
    mav.addObject("LinkType", AppUtils.typeList());
    mav.addObject("ua", (bltUserAccount)session.getAttribute("CurrentUserAccount"));
    mav.addObject("pm", new bltNIM3_PayerMaster(((bltUserAccount)session.getAttribute("CurrentUserAccount")).getPayerID()));
    mav.addObject("command", new TysonReferralForm());
    mav.addObject("claimsOfficeList", claimsOfficeList);
    mav.addObject("yesNo", yesNo);
    mav.addObject("teamMemberplantLocationList", teamMemberplantLocationList);
    mav.addObject("heightList", AppUtils.heightList);
    mav.addObject("stateList", AppUtils.getStates());
    mav.addObject("list", AppUtils.getYears());
    mav.addObject("month", AppUtils.month);
    mav.addObject("day", AppUtils.day);
    mav.addObject("procList", AppUtils.procedure);
    mav.addObject("bpList", AppUtils.bodyPart);
    mav.addObject("ruleList", AppUtils.ruleOut);
    return mav;
  }

  @RequestMapping(value={"TysonReferralForm"}, method={org.springframework.web.bind.annotation.RequestMethod.POST})
  public ModelAndView asdfa(@ModelAttribute TysonReferralForm tyson, HttpSession session) throws SQLException {
    ModelAndView mav = null;
    ResultValidationTyson rvt = AppUtils.tysonValidation(tyson, (bltUserAccount)session.getAttribute("CurrentUserAccount"));
    if (rvt.isGoodReferral()) {
      mav = new ModelAndView("customMessage", "cType", Integer.valueOf(10));
    } else {
      List claimsOfficeList = Arrays.asList(new String[] { "", "Dakota Dunes Claims Office", "Springdale AR Claims Office", "Oxford, AL", "Wilkesboro, NC", "Grandview, MO" });
      List teamMemberplantLocationList = Arrays.asList(new String[] { "", "Albertville - AL", "Amarillo - TX", "Berryville - AR", "Berryville/Green Complex - AR", "Blountsville - AL", "Broken Bow - OK", "Buena Vista Plant - GA", "Buffalo Plant   - NY", "Carthage Complex - TX", "Carthage, MS - MS", "Center Complex - TX", "Cherokee - IA", "Chicago Bruss Plant - IL", "Chicago Culinary Plant - IL", "Clarksville Complex - AR", "Cobb Vantress - AR", "Columbia - NC", "Concordia - MO", "Corydon Complex - IN", "Council Bluffs  -  Case Ready - IA", "Council Bluffs  -  Cooked Meats - IA", "Cumming Complex - GA", "Dakota City Plant - IA", "Dakota Dunes - ND", "Dallas - TX", "Dardanelle Complex - AR", "Dawson - GA", "Denison Plant - IA", "Dexter Complex - MO", "Emporia - KS", "Finney County - KS", "Forest - MS", "Forest RVAF - MS", "Fort Smith Plant - AR", "Fort Worth Plant - TX", "Glen Allen Complex - VA", "Goodlettsville - TN", "Grannis - AR", "Green Bay Plant - WI", "Green Forest - AR", "Harmony - NC", "Harrisonburg Complex - VA", "Hope Complex - AR", "Houston Plant - TX", "Hutchinson, KS - KS", "Hutchinson, KS (Alternate Contact) - KS", "Jefferson Plant - WI", "Joslin - IL", "Lexington Plant - NE", "Logansport - IA", "Louisa County - IA", "Madison - WI", "Monett Complex - MO", "Monroe Complex - NC", "Montgomery City - MO", "MS Live Production - MS", "N. Richland Hills Plant - TX", "Nashville Complex - AR", "New Holland Complex - PA", "Noel Complex - MO", "North Little Rock - AR", "North Richland Hills - AR", "NWA Employment Center - AR", "Obion County Complex - TN", "Oglethorpe - GA", "Olathe - KS", "Olathe - KS", "Omaha Plant - NE", "Ottawa - IL", "Pasco Plant - WA", "Perry - IA", "Pine Bluff Complex - AR", "Portland, IN - IN", "Robards Complex - KY", "Rogers  -  Tyson Distribution Center - AR", "Rogers CNQ Plant - AR", "Russellville  -  TVDC - AR", "Russellville  -  Tyler Road - AR", "Russellville -  TVDC - AR", "RVAF Robards. - KY", "RVAF Texarkana - AR", "RVAFScranton - AR", "S Hutchinson Plant - KS", "Sanford - NC", "Santa Teresa - NM", "Sedalia Complex - MO", "Seguin, TX - TX", "Shelbyville Complex - TN", "Sherman - TX", "Springdale  -  Berry Street - AR", "Springdale  -  Corporate - AR", "Springdale - Randall Road - AR", "Springdale - Transportation - AR", "Springdale Complex - AR", "Storm Lake Plant - IA", "Temperanceville Complex - VA", "Tyson of Fayetteville Plants  - AR", "Tyson of Rogers - AR", "Van Buren - AR", "Vernon Plant - TX", "Vicksburg  -  Ceres - MS", "Vienna - GA", "Waldron Complex - AR", "Waterloo - IA", "Wilkesboro Complex - NC" });
      LinkedHashMap yesNo = new LinkedHashMap();
      yesNo.put(Integer.valueOf(1), "Yes");
      yesNo.put(Integer.valueOf(2), "No");
      if (session.getAttribute("CurrentUserAccount") == null) {
        return new ModelAndView("customMessage", "cType", Integer.valueOf(1));
      }
      mav = new ModelAndView("TysonReferralForm");
      mav.addObject("tyson", Boolean.valueOf(true));
      mav.addObject("LinkType", AppUtils.typeList());
      mav.addObject("ua", (bltUserAccount)session.getAttribute("CurrentUserAccount"));
      mav.addObject("pm", new bltNIM3_PayerMaster(((bltUserAccount)session.getAttribute("CurrentUserAccount")).getPayerID()));
      mav.addObject("claimsOfficeList", claimsOfficeList);
      mav.addObject("yesNo", yesNo);
      mav.addObject("teamMemberplantLocationList", teamMemberplantLocationList);
      mav.addObject("heightList", AppUtils.heightList);
      mav.addObject("stateList", AppUtils.getStates());
      mav.addObject("list", AppUtils.getYears());
      mav.addObject("month", AppUtils.month);
      mav.addObject("day", AppUtils.day);
      mav.addObject("procList", AppUtils.procedure);
      mav.addObject("bpList", AppUtils.bodyPart);
      mav.addObject("ruleList", AppUtils.ruleOut);
      mav.addObject("rvt", rvt);
      mav.addObject("command", tyson);
      return mav;
    }
    return mav;
  }

  @RequestMapping(value={"test"}, method={org.springframework.web.bind.annotation.RequestMethod.GET})
  public ModelAndView test()
  {
    return new ModelAndView("test");
  }

  @RequestMapping(value={"viewDocument"}, method={org.springframework.web.bind.annotation.RequestMethod.POST})
  @ResponseBody
  public void getDocument(@RequestParam("file_id") String file_id, HttpServletResponse response)
    throws IOException, NumberFormatException, GeneralSecurityException
  {
    String documentName = AppUtils.getDocumentName(new Integer(LinkEncryption.decrypt(file_id)).intValue());
    InputStream is = new FileInputStream(new File(ConfigurationInformation.sUploadFolderDirectory + documentName));

    response.setContentType("application/pdf");

    response.setHeader("Content-Disposition", "inline;filename=\"" + documentName + "\"");
    IOUtils.copy(is, response.getOutputStream());
    response.flushBuffer();
  }
}