package com.nextimagemedical.adjportal;

import java.util.Date;
import java.util.List;

import org.springframework.web.multipart.commons.CommonsMultipartFile;

public class ReferCase {
	private String CaseClaimNumber1;
	private int LinkType;
	private String refFirstName;
	private String refLastName;
	private String AdjusterBranch;
	private String AdjusterPhone1;
	private String AdjusterPhone2;
	private String AdjusterPhone3;
	private String AdjusterPhone;
	private String AdjusterFax1;
	private String AdjusterFax2;
	private String AdjusterFax3;
	private String AdjusterFax;
	private String AdjusterEmail;
	private String PatientFirstName;
	private String PatientLastName;
	private String PatientDOB;
	private String PatientSSN;
	private String PatientDOI;
	private String PatientAddress1;
	private String PatientCity;
	private int PatientStateID;
	private String PatientZip;
	private String PatientHomePhone1;
	private String PatientHomePhone2;
	private String PatientHomePhone3;
	private String PatientHomePhone;
	private String PatientMobilePhone1;
	private String PatientMobilePhone2;
	private String PatientMobilePhone3;
	private String PatientMobilePhone;
	private String EmployerName;
	private String EmployerPhone1;
	private String EmployerPhone2;
	private String EmployerPhone3;
	private String EmployerPhone;
	private int claust;
	private String height;
	private int weight;
	private int prego;
	private String DOM;
	private String diseases;
	private String MDName;
	private String MDPhone1;
	private String MDPhone2;
	private String MDPhone3;
	private String MDPhone;
	private String MDFax1;
	private String MDFax2;
	private String MDFax3;
	private String MDFax;
	private String FollowUpDate;
	private String MDEmail;
	private List<String> proc;
	private List<String> bp;
	private List<String> RuleOut;
	private int cd;
	private int films;
	private int report;
	private String MessageText;
	private int stat;
	private int metal;
	private CommonsMultipartFile fileRx;
	private CommonsMultipartFile fileOther;
	
	public ReferCase() {
	}

	public String getCaseClaimNumber1() {
		return CaseClaimNumber1;
	}

	public void setCaseClaimNumber1(String caseClaimNumber1) {
		CaseClaimNumber1 = caseClaimNumber1;
	}

	public int getLinkType() {
		return LinkType;
	}

	public void setLinkType(int linkType) {
		LinkType = linkType;
	}

	public String getRefFirstName() {
		return refFirstName;
	}

	public void setRefFirstName(String refFirstName) {
		this.refFirstName = refFirstName;
	}

	public String getRefLastName() {
		return refLastName;
	}

	public void setRefLastName(String refLastName) {
		this.refLastName = refLastName;
	}

	public String getAdjusterBranch() {
		return AdjusterBranch;
	}

	public void setAdjusterBranch(String adjusterBranch) {
		AdjusterBranch = adjusterBranch;
	}

	public String getAdjusterPhone1() {
		return AdjusterPhone1;
	}

	public void setAdjusterPhone1(String adjusterPhone1) {
		AdjusterPhone1 = adjusterPhone1;
	}

	public String getAdjusterPhone2() {
		return AdjusterPhone2;
	}

	public void setAdjusterPhone2(String adjusterPhone2) {
		AdjusterPhone2 = adjusterPhone2;
	}

	public String getAdjusterPhone3() {
		return AdjusterPhone3;
	}

	public void setAdjusterPhone3(String adjusterPhone3) {
		AdjusterPhone3 = adjusterPhone3;
	}

	public String getAdjusterPhone() {
		return AdjusterPhone;
	}

	public void setAdjusterPhone(String adjusterPhone) {
		AdjusterPhone = adjusterPhone;
	}

	public String getAdjusterFax1() {
		return AdjusterFax1;
	}

	public void setAdjusterFax1(String adjusterFax1) {
		AdjusterFax1 = adjusterFax1;
	}

	public String getAdjusterFax2() {
		return AdjusterFax2;
	}

	public void setAdjusterFax2(String adjusterFax2) {
		AdjusterFax2 = adjusterFax2;
	}

	public String getAdjusterFax3() {
		return AdjusterFax3;
	}

	public void setAdjusterFax3(String adjusterFax3) {
		AdjusterFax3 = adjusterFax3;
	}

	public String getAdjusterFax() {
		return AdjusterFax;
	}

	public void setAdjusterFax(String adjusterFax) {
		AdjusterFax = adjusterFax;
	}

	public String getAdjusterEmail() {
		return AdjusterEmail;
	}

	public void setAdjusterEmail(String adjusterEmail) {
		AdjusterEmail = adjusterEmail;
	}

	public String getPatientFirstName() {
		return PatientFirstName;
	}

	public void setPatientFirstName(String patientFirstName) {
		PatientFirstName = patientFirstName;
	}

	public String getPatientLastName() {
		return PatientLastName;
	}

	public void setPatientLastName(String patientLastName) {
		PatientLastName = patientLastName;
	}

	public String getPatientDOB() {
		return PatientDOB;
	}

	public void setPatientDOB(String patientDOB) {
		PatientDOB = patientDOB;
	}

	public String getPatientSSN() {
		return PatientSSN;
	}

	public void setPatientSSN(String patientSSN) {
		PatientSSN = patientSSN;
	}

	public String getPatientDOI() {
		return PatientDOI;
	}

	public void setPatientDOI(String patientDOI) {
		PatientDOI = patientDOI;
	}

	public String getPatientAddress1() {
		return PatientAddress1;
	}

	public void setPatientAddress1(String patientAddress1) {
		PatientAddress1 = patientAddress1;
	}

	public String getPatientCity() {
		return PatientCity;
	}

	public void setPatientCity(String patientCity) {
		PatientCity = patientCity;
	}

	public int getPatientStateID() {
		return PatientStateID;
	}

	public void setPatientStateID(int patientStateID) {
		PatientStateID = patientStateID;
	}

	public String getPatientZip() {
		return PatientZip;
	}

	public void setPatientZip(String patientZip) {
		PatientZip = patientZip;
	}

	public String getPatientHomePhone1() {
		return PatientHomePhone1;
	}

	public void setPatientHomePhone1(String patientHomePhone1) {
		PatientHomePhone1 = patientHomePhone1;
	}

	public String getPatientHomePhone2() {
		return PatientHomePhone2;
	}

	public void setPatientHomePhone2(String patientHomePhone2) {
		PatientHomePhone2 = patientHomePhone2;
	}

	public String getPatientHomePhone3() {
		return PatientHomePhone3;
	}

	public void setPatientHomePhone3(String patientHomePhone3) {
		PatientHomePhone3 = patientHomePhone3;
	}

	public String getPatientHomePhone() {
		return PatientHomePhone;
	}

	public void setPatientHomePhone(String patientHomePhone) {
		PatientHomePhone = patientHomePhone;
	}

	public String getPatientMobilePhone1() {
		return PatientMobilePhone1;
	}

	public void setPatientMobilePhone1(String patientMobilePhone1) {
		PatientMobilePhone1 = patientMobilePhone1;
	}

	public String getPatientMobilePhone2() {
		return PatientMobilePhone2;
	}

	public void setPatientMobilePhone2(String patientMobilePhone2) {
		PatientMobilePhone2 = patientMobilePhone2;
	}

	public String getPatientMobilePhone3() {
		return PatientMobilePhone3;
	}

	public void setPatientMobilePhone3(String patientMobilePhone3) {
		PatientMobilePhone3 = patientMobilePhone3;
	}

	public String getPatientMobilePhone() {
		return PatientMobilePhone;
	}

	public void setPatientMobilePhone(String patientMobilePhone) {
		PatientMobilePhone = patientMobilePhone;
	}

	public String getEmployerName() {
		return EmployerName;
	}

	public void setEmployerName(String employerName) {
		EmployerName = employerName;
	}

	public String getEmployerPhone1() {
		return EmployerPhone1;
	}

	public void setEmployerPhone1(String employerPhone1) {
		EmployerPhone1 = employerPhone1;
	}

	public String getEmployerPhone2() {
		return EmployerPhone2;
	}

	public void setEmployerPhone2(String employerPhone2) {
		EmployerPhone2 = employerPhone2;
	}

	public String getEmployerPhone3() {
		return EmployerPhone3;
	}

	public void setEmployerPhone3(String employerPhone3) {
		EmployerPhone3 = employerPhone3;
	}

	public String getEmployerPhone() {
		return EmployerPhone;
	}

	public void setEmployerPhone(String employerPhone) {
		EmployerPhone = employerPhone;
	}

	public int getClaust() {
		return claust;
	}

	public void setClaust(int claust) {
		this.claust = claust;
	}

	public String getHeight() {
		return height;
	}

	public void setHeight(String height) {
		this.height = height;
	}

	public int getWeight() {
		return weight;
	}

	public void setWeight(int weight) {
		this.weight = weight;
	}

	public int getPrego() {
		return prego;
	}

	public void setPrego(int prego) {
		this.prego = prego;
	}

	public String getDOM() {
		return DOM;
	}

	public void setDOM(String dOM) {
		DOM = dOM;
	}

	public String getDiseases() {
		return diseases;
	}

	public void setDiseases(String diseases) {
		this.diseases = diseases;
	}

	public String getMDName() {
		return MDName;
	}

	public void setMDName(String mDName) {
		MDName = mDName;
	}

	public String getMDPhone1() {
		return MDPhone1;
	}

	public void setMDPhone1(String mDPhone1) {
		MDPhone1 = mDPhone1;
	}

	public String getMDPhone2() {
		return MDPhone2;
	}

	public void setMDPhone2(String mDPhone2) {
		MDPhone2 = mDPhone2;
	}

	public String getMDPhone3() {
		return MDPhone3;
	}

	public void setMDPhone3(String mDPhone3) {
		MDPhone3 = mDPhone3;
	}

	public String getMDPhone() {
		return MDPhone;
	}

	public void setMDPhone(String mDPhone) {
		MDPhone = mDPhone;
	}

	public String getMDFax1() {
		return MDFax1;
	}

	public void setMDFax1(String mDFax1) {
		MDFax1 = mDFax1;
	}

	public String getMDFax2() {
		return MDFax2;
	}

	public void setMDFax2(String mDFax2) {
		MDFax2 = mDFax2;
	}

	public String getMDFax3() {
		return MDFax3;
	}

	public void setMDFax3(String mDFax3) {
		MDFax3 = mDFax3;
	}

	public String getMDFax() {
		return MDFax;
	}

	public void setMDFax(String mDFax) {
		MDFax = mDFax;
	}

	public String getFollowUpDate() {
		return FollowUpDate;
	}

	public void setFollowUpDate(String followUpDate) {
		FollowUpDate = followUpDate;
	}

	public String getMDEmail() {
		return MDEmail;
	}

	public void setMDEmail(String mDEmail) {
		MDEmail = mDEmail;
	}

	public List<String> getProc() {
		return proc;
	}

	public void setProc(List<String> proc) {
		this.proc = proc;
	}

	public List<String> getBp() {
		return bp;
	}

	public void setBp(List<String> bp) {
		this.bp = bp;
	}

	public List<String> getRuleOut() {
		return RuleOut;
	}

	public void setRuleOut(List<String> ruleOut) {
		RuleOut = ruleOut;
	}

	public int getCd() {
		return cd;
	}

	public void setCd(int cd) {
		this.cd = cd;
	}

	public int getFilms() {
		return films;
	}

	public void setFilms(int films) {
		this.films = films;
	}

	public int getReport() {
		return report;
	}

	public void setReport(int report) {
		this.report = report;
	}

	public String getMessageText() {
		return MessageText;
	}

	public void setMessageText(String messageText) {
		MessageText = messageText;
	}

	public int getStat() {
		return stat;
	}

	public void setStat(int stat) {
		this.stat = stat;
	}

	public int getMetal() {
		return metal;
	}

	public void setMetal(int metal) {
		this.metal = metal;
	}

	public CommonsMultipartFile getFileRx() {
		return fileRx;
	}

	public void setFileRx(CommonsMultipartFile fileRx) {
		this.fileRx = fileRx;
	}

	public CommonsMultipartFile getFileOther() {
		return fileOther;
	}

	public void setFileOther(CommonsMultipartFile fileOther) {
		this.fileOther = fileOther;
	}
}
