package com.winstaff;
import java.awt.*;


public class ContactReferenceType extends java.lang.Object
{

	public ContactReferenceType()
	{
	}

	public void setName(String name)
	{
			this.name = name;
	}

	public String getName()
	{
		return this.name;
	}

	public void setCompanyName(String Companyname)
	{
			this.companyName = Companyname;
	}

	public String getCompanyName()
	{
		return this.companyName;
	}


	public void setAddress1(String address1)
	{
			this.address1 = address1;
	}

	public String getAddress1()
	{
		return this.address1;
	}

	public void setAddress2(String address2)
	{
			this.address2 = address2;
	}

	public String getAddress2()
	{
		return this.address2;
	}

	public void setCity(String city)
	{
			this.city = city;
	}

	public String getCity()
	{
		return this.city;
	}

	public void setStateID(Integer stateID)
	{
		this.stateID = stateID;
		this.stateString = new bltStateLI(stateID).getShortState();
	}

	public Integer getStateID()
	{
		return this.stateID;
	}

	public void setZIP(String ZIP)
	{
			this.ZIP = ZIP;
	}

	public String getZIP()
	{
		return this.ZIP;
	}

	public void setProvince(String province)
	{
			this.province = province;
	}

	public String getProvince()
	{
		return this.province;
	}

	public void setCountryID(Integer countryID)
	{
		this.countryID = countryID;
		this.countryString = new bltCountryLI(stateID).getCountryLong();
	}

	public Integer getCountryID()
	{
		return this.countryID;
	}

	public void setEmail(String email)
	{
			this.email = email;
	}

	public String getEmail()
	{
		return this.email;
	}

	public void setPhone(String phone)
	{
			this.phone = phone;
	}

	public String getPhone()
	{
		return this.phone;
	}

	public void setFax(String fax)
	{
			this.fax = fax;
	}

	public String getFax()
	{
		return this.fax;
	}

	public String getMailerAddressFullString()
	{
		String myVal = "";
		if (this.getName()!=null&&!this.getName().equalsIgnoreCase(""))
		{
		    myVal +=this.getName() + "\n";
		}
		if (this.getAddress1()!=null&&!this.getAddress2().equalsIgnoreCase(""))
		{
		    myVal +=this.getAddress1() + "\n";
		}
		if (this.getAddress2()!=null&&!this.getAddress2().equalsIgnoreCase(""))
		{
		    myVal +=this.getAddress2() + "\n";
		}
        myVal +=this.getCity() + ", ";
		if (this.getProvince()!=null&&!this.getProvince().equalsIgnoreCase(""))
		{
		    myVal +=this.getProvince() + " ";
		}
		else if (this.getStateString()!=null&&!this.getStateString().equalsIgnoreCase(""))
		{
		    myVal +=this.getStateString() + " ";
		}
	    myVal +=this.getZIP() + " ";
		if (this.getCountryString()!=null&&!this.getCountryString().equalsIgnoreCase(""))
		{
		    myVal +=this.getCountryString() + "\n";
		}
		else if (this.getCountryString()!=null&&!this.getCountryString().equalsIgnoreCase(""))
		{
		    myVal +="\n";
		}
		return myVal;
	}


	public String getStateString()
	{
		return this.stateString;
	}

	public String getCountryString()
	{
		return this.countryString;
	}


	protected String stateString = "";
	protected String countryString = "";
	protected String phone = "";
	protected Integer countryID;
	protected String province = "";
	protected String city = "";
	protected String name = "";
	protected String companyName = "";
	protected String email = "";
	protected String ZIP = "";
	protected Integer stateID;
	protected String address2 = "";
	protected String address1 = "";
	protected String fax = "";

}
