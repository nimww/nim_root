

package com.winstaff;

import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public class dbtProfessionalLiability extends Object implements InttProfessionalLiability
{

        db_NewBase    dbnbDB;

    public dbtProfessionalLiability()
    {
        dbnbDB = new db_NewBase( "tProfessionalLiability", "ProfessionalLiabilityID" );

    }    // End of default constructor

    public dbtProfessionalLiability( Integer iNewID )
    {
        dbnbDB = new db_NewBase( "tProfessionalLiability", "ProfessionalLiabilityID" );
        setUniqueID( iNewID );
    }    // End of Constructor knowing an ID

    public void commitData() throws SQLException
    {
            //no longer trapping/ignoring errors 2011-03-17
            dbnbDB.commitData();
    }    // End of commitData()

    public void commitDataForced() throws SQLException
    {
        dbnbDB.commitDataForced();
    }    // End of commitData()

    public Integer getUniqueID()
    {
        return dbnbDB.getUniqueID();
    }    // End of getUniqueID()

    public void setUniqueID( Integer iNewID )
    {
        dbnbDB.setUniqueID( iNewID );
    }    // End of setUniqueID()

    public void setModifyComments( String sNew )
    {
        dbnbDB.setModifyComments( sNew );
    }    // End of setUniqueID()


    public void setProfessionalLiabilityID(Integer newValue)
    {
                dbnbDB.setFieldData( "ProfessionalLiabilityID", newValue.toString() );
    }

    public Integer getProfessionalLiabilityID()
    {
        String           sValue = dbnbDB.getFieldData( "ProfessionalLiabilityID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setField(String fieldN, Object fieldV)
    {DebugLogger.println("setField Method not implemented yet in classProfessionalLiability!");}

    public String getFieldType(String fieldN)
    {return null;}


    public void setUniqueCreateDate(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "UniqueCreateDate", formatter.format( newValue ) );
    }

    public Date getUniqueCreateDate()
    {
        String           sValue = dbnbDB.getFieldData( "UniqueCreateDate" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setUniqueModifyDate(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "UniqueModifyDate", formatter.format( newValue ) );
    }

    public Date getUniqueModifyDate()
    {
        String           sValue = dbnbDB.getFieldData( "UniqueModifyDate" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setUniqueModifyComments(String newValue)
    {
                dbnbDB.setFieldData( "UniqueModifyComments", newValue.toString() );
    }

    public String getUniqueModifyComments()
    {
        String           sValue = dbnbDB.getFieldData( "UniqueModifyComments" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setPhysicianID(Integer newValue)
    {
                dbnbDB.setFieldData( "PhysicianID", newValue.toString() );
    }

    public Integer getPhysicianID()
    {
        String           sValue = dbnbDB.getFieldData( "PhysicianID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setCoverageType(Integer newValue)
    {
                dbnbDB.setFieldData( "CoverageType", newValue.toString() );
    }

    public Integer getCoverageType()
    {
        String           sValue = dbnbDB.getFieldData( "CoverageType" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setInsuranceCarrier(String newValue)
    {
                dbnbDB.setFieldData( "InsuranceCarrier", newValue.toString() );
    }

    public String getInsuranceCarrier()
    {
        String           sValue = dbnbDB.getFieldData( "InsuranceCarrier" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setPolicyHolder(String newValue)
    {
                dbnbDB.setFieldData( "PolicyHolder", newValue.toString() );
    }

    public String getPolicyHolder()
    {
        String           sValue = dbnbDB.getFieldData( "PolicyHolder" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setAgentName(String newValue)
    {
                dbnbDB.setFieldData( "AgentName", newValue.toString() );
    }

    public String getAgentName()
    {
        String           sValue = dbnbDB.getFieldData( "AgentName" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setAddress1(String newValue)
    {
                dbnbDB.setFieldData( "Address1", newValue.toString() );
    }

    public String getAddress1()
    {
        String           sValue = dbnbDB.getFieldData( "Address1" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setAddress2(String newValue)
    {
                dbnbDB.setFieldData( "Address2", newValue.toString() );
    }

    public String getAddress2()
    {
        String           sValue = dbnbDB.getFieldData( "Address2" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setCity(String newValue)
    {
                dbnbDB.setFieldData( "City", newValue.toString() );
    }

    public String getCity()
    {
        String           sValue = dbnbDB.getFieldData( "City" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setStateID(Integer newValue)
    {
                dbnbDB.setFieldData( "StateID", newValue.toString() );
    }

    public Integer getStateID()
    {
        String           sValue = dbnbDB.getFieldData( "StateID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setProvince(String newValue)
    {
                dbnbDB.setFieldData( "Province", newValue.toString() );
    }

    public String getProvince()
    {
        String           sValue = dbnbDB.getFieldData( "Province" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setZIP(String newValue)
    {
                dbnbDB.setFieldData( "ZIP", newValue.toString() );
    }

    public String getZIP()
    {
        String           sValue = dbnbDB.getFieldData( "ZIP" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setCountryID(Integer newValue)
    {
                dbnbDB.setFieldData( "CountryID", newValue.toString() );
    }

    public Integer getCountryID()
    {
        String           sValue = dbnbDB.getFieldData( "CountryID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setPhone(String newValue)
    {
                dbnbDB.setFieldData( "Phone", newValue.toString() );
    }

    public String getPhone()
    {
        String           sValue = dbnbDB.getFieldData( "Phone" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setFax(String newValue)
    {
                dbnbDB.setFieldData( "Fax", newValue.toString() );
    }

    public String getFax()
    {
        String           sValue = dbnbDB.getFieldData( "Fax" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setContactName(String newValue)
    {
                dbnbDB.setFieldData( "ContactName", newValue.toString() );
    }

    public String getContactName()
    {
        String           sValue = dbnbDB.getFieldData( "ContactName" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setContactEmail(String newValue)
    {
                dbnbDB.setFieldData( "ContactEmail", newValue.toString() );
    }

    public String getContactEmail()
    {
        String           sValue = dbnbDB.getFieldData( "ContactEmail" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setPolicyNumber(String newValue)
    {
                dbnbDB.setFieldData( "PolicyNumber", newValue.toString() );
    }

    public String getPolicyNumber()
    {
        String           sValue = dbnbDB.getFieldData( "PolicyNumber" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setInsuredFromDate(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "InsuredFromDate", formatter.format( newValue ) );
    }

    public Date getInsuredFromDate()
    {
        String           sValue = dbnbDB.getFieldData( "InsuredFromDate" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setInsuredToDate(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "InsuredToDate", formatter.format( newValue ) );
    }

    public Date getInsuredToDate()
    {
        String           sValue = dbnbDB.getFieldData( "InsuredToDate" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setOriginalEffectiveDate(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "OriginalEffectiveDate", formatter.format( newValue ) );
    }

    public Date getOriginalEffectiveDate()
    {
        String           sValue = dbnbDB.getFieldData( "OriginalEffectiveDate" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setTerminationDate(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "TerminationDate", formatter.format( newValue ) );
    }

    public Date getTerminationDate()
    {
        String           sValue = dbnbDB.getFieldData( "TerminationDate" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setPerClaimAmount(Double newValue)
    {
                java.text.DecimalFormat defDecFormat2 = new java.text.DecimalFormat(PLCUtils.String_defDecFormat2);
                dbnbDB.setFieldData( "PerClaimAmount", defDecFormat2.format(newValue) );
    }

    public Double getPerClaimAmount()
    {
        String           sValue = dbnbDB.getFieldData( "PerClaimAmount" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDoubleValue : sValue;
        return new Double( sValue  ); 
    }

    public void setAggregateAmount(Double newValue)
    {
                java.text.DecimalFormat defDecFormat2 = new java.text.DecimalFormat(PLCUtils.String_defDecFormat2);
                dbnbDB.setFieldData( "AggregateAmount", defDecFormat2.format(newValue) );
    }

    public Double getAggregateAmount()
    {
        String           sValue = dbnbDB.getFieldData( "AggregateAmount" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDoubleValue : sValue;
        return new Double( sValue  ); 
    }

    public void setDescOfSurcharge(String newValue)
    {
                dbnbDB.setFieldData( "DescOfSurcharge", newValue.toString() );
    }

    public String getDescOfSurcharge()
    {
        String           sValue = dbnbDB.getFieldData( "DescOfSurcharge" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setDocuLinkID(Integer newValue)
    {
                dbnbDB.setFieldData( "DocuLinkID", newValue.toString() );
    }

    public Integer getDocuLinkID()
    {
        String           sValue = dbnbDB.getFieldData( "DocuLinkID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setComments(String newValue)
    {
                dbnbDB.setFieldData( "Comments", newValue.toString() );
    }

    public String getComments()
    {
        String           sValue = dbnbDB.getFieldData( "Comments" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

}    // End of bltProfessionalLiability class definition
