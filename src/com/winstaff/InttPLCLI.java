package com.winstaff;


import com.winstaff.dbtPLCLI;
import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public interface InttPLCLI extends dbTableInterface
{

    public void setPLCID(Integer newValue);
    public Integer getPLCID();
    public void setPLCShort(String newValue);
    public String getPLCShort();
    public void setPLCLong(String newValue);
    public String getPLCLong();
    public void setCompanyID(Integer newValue);
    public Integer getCompanyID();
    public void setAdminID(Integer newValue);
    public Integer getAdminID();
    public void setHCOID(Integer newValue);
    public Integer getHCOID();
}    // End of bltPLCLI class definition
