

package com.winstaff;

import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public class dbtWorkHistory extends Object implements InttWorkHistory
{

        db_NewBase    dbnbDB;

    public dbtWorkHistory()
    {
        dbnbDB = new db_NewBase( "tWorkHistory", "WorkHistoryID" );

    }    // End of default constructor

    public dbtWorkHistory( Integer iNewID )
    {
        dbnbDB = new db_NewBase( "tWorkHistory", "WorkHistoryID" );
        setUniqueID( iNewID );
    }    // End of Constructor knowing an ID

    public void commitData() throws SQLException
    {
            //no longer trapping/ignoring errors 2011-03-17
            dbnbDB.commitData();
    }    // End of commitData()

    public void commitDataForced() throws SQLException
    {
        dbnbDB.commitDataForced();
    }    // End of commitData()

    public Integer getUniqueID()
    {
        return dbnbDB.getUniqueID();
    }    // End of getUniqueID()

    public void setUniqueID( Integer iNewID )
    {
        dbnbDB.setUniqueID( iNewID );
    }    // End of setUniqueID()

    public void setModifyComments( String sNew )
    {
        dbnbDB.setModifyComments( sNew );
    }    // End of setUniqueID()


    public void setWorkHistoryID(Integer newValue)
    {
                dbnbDB.setFieldData( "WorkHistoryID", newValue.toString() );
    }

    public Integer getWorkHistoryID()
    {
        String           sValue = dbnbDB.getFieldData( "WorkHistoryID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setField(String fieldN, Object fieldV)
    {DebugLogger.println("setField Method not implemented yet in classWorkHistory!");}

    public String getFieldType(String fieldN)
    {return null;}


    public void setUniqueCreateDate(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "UniqueCreateDate", formatter.format( newValue ) );
    }

    public Date getUniqueCreateDate()
    {
        String           sValue = dbnbDB.getFieldData( "UniqueCreateDate" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setUniqueModifyDate(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "UniqueModifyDate", formatter.format( newValue ) );
    }

    public Date getUniqueModifyDate()
    {
        String           sValue = dbnbDB.getFieldData( "UniqueModifyDate" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setUniqueModifyComments(String newValue)
    {
                dbnbDB.setFieldData( "UniqueModifyComments", newValue.toString() );
    }

    public String getUniqueModifyComments()
    {
        String           sValue = dbnbDB.getFieldData( "UniqueModifyComments" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setPhysicianID(Integer newValue)
    {
                dbnbDB.setFieldData( "PhysicianID", newValue.toString() );
    }

    public Integer getPhysicianID()
    {
        String           sValue = dbnbDB.getFieldData( "PhysicianID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setWorkHistoryTypeID(Integer newValue)
    {
                dbnbDB.setFieldData( "WorkHistoryTypeID", newValue.toString() );
    }

    public Integer getWorkHistoryTypeID()
    {
        String           sValue = dbnbDB.getFieldData( "WorkHistoryTypeID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setName(String newValue)
    {
                dbnbDB.setFieldData( "Name", newValue.toString() );
    }

    public String getName()
    {
        String           sValue = dbnbDB.getFieldData( "Name" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setWorkDescription(String newValue)
    {
                dbnbDB.setFieldData( "WorkDescription", newValue.toString() );
    }

    public String getWorkDescription()
    {
        String           sValue = dbnbDB.getFieldData( "WorkDescription" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setFromDate(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "FromDate", formatter.format( newValue ) );
    }

    public Date getFromDate()
    {
        String           sValue = dbnbDB.getFieldData( "FromDate" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setToDate(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "ToDate", formatter.format( newValue ) );
    }

    public Date getToDate()
    {
        String           sValue = dbnbDB.getFieldData( "ToDate" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setReasonForLeaving(String newValue)
    {
                dbnbDB.setFieldData( "ReasonForLeaving", newValue.toString() );
    }

    public String getReasonForLeaving()
    {
        String           sValue = dbnbDB.getFieldData( "ReasonForLeaving" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setAddress1(String newValue)
    {
                dbnbDB.setFieldData( "Address1", newValue.toString() );
    }

    public String getAddress1()
    {
        String           sValue = dbnbDB.getFieldData( "Address1" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setAddress2(String newValue)
    {
                dbnbDB.setFieldData( "Address2", newValue.toString() );
    }

    public String getAddress2()
    {
        String           sValue = dbnbDB.getFieldData( "Address2" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setCity(String newValue)
    {
                dbnbDB.setFieldData( "City", newValue.toString() );
    }

    public String getCity()
    {
        String           sValue = dbnbDB.getFieldData( "City" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setStateID(Integer newValue)
    {
                dbnbDB.setFieldData( "StateID", newValue.toString() );
    }

    public Integer getStateID()
    {
        String           sValue = dbnbDB.getFieldData( "StateID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setProvince(String newValue)
    {
                dbnbDB.setFieldData( "Province", newValue.toString() );
    }

    public String getProvince()
    {
        String           sValue = dbnbDB.getFieldData( "Province" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setZIP(String newValue)
    {
                dbnbDB.setFieldData( "ZIP", newValue.toString() );
    }

    public String getZIP()
    {
        String           sValue = dbnbDB.getFieldData( "ZIP" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setCountryID(Integer newValue)
    {
                dbnbDB.setFieldData( "CountryID", newValue.toString() );
    }

    public Integer getCountryID()
    {
        String           sValue = dbnbDB.getFieldData( "CountryID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setPhone(String newValue)
    {
                dbnbDB.setFieldData( "Phone", newValue.toString() );
    }

    public String getPhone()
    {
        String           sValue = dbnbDB.getFieldData( "Phone" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setFax(String newValue)
    {
                dbnbDB.setFieldData( "Fax", newValue.toString() );
    }

    public String getFax()
    {
        String           sValue = dbnbDB.getFieldData( "Fax" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setContactName(String newValue)
    {
                dbnbDB.setFieldData( "ContactName", newValue.toString() );
    }

    public String getContactName()
    {
        String           sValue = dbnbDB.getFieldData( "ContactName" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setContactEmail(String newValue)
    {
                dbnbDB.setFieldData( "ContactEmail", newValue.toString() );
    }

    public String getContactEmail()
    {
        String           sValue = dbnbDB.getFieldData( "ContactEmail" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setComments(String newValue)
    {
                dbnbDB.setFieldData( "Comments", newValue.toString() );
    }

    public String getComments()
    {
        String           sValue = dbnbDB.getFieldData( "Comments" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

}    // End of bltWorkHistory class definition
