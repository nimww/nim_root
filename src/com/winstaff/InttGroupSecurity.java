package com.winstaff;


import com.winstaff.dbtGroupSecurity;
import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public interface InttGroupSecurity extends dbTableInterface
{

    public void setGroupSecurityID(Integer newValue);
    public Integer getGroupSecurityID();
    public void setUniqueCreateDate(Date newValue);
    public Date getUniqueCreateDate();
    public void setUniqueModifyDate(Date newValue);
    public Date getUniqueModifyDate();
    public void setUniqueModifyComments(String newValue);
    public String getUniqueModifyComments();
    public void setSecurityGroupID(Integer newValue);
    public Integer getSecurityGroupID();
    public void setItemName(String newValue);
    public String getItemName();
    public void setAccessLevel(Integer newValue);
    public Integer getAccessLevel();
}    // End of bltGroupSecurity class definition
