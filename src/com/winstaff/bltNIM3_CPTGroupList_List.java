
package com.winstaff;
/*
 * bltNIM3_CPTGroupList_List.java
 *
 * Created: Tue Jun 09 19:13:50 PDT 2009
 */



import java.sql.*;




/**
 *
 * @author  Scott Ellis via listClassGenCode
 * @version 2.5
 */
public class bltNIM3_CPTGroupList_List extends db_BaseRelationTable
{
    /** Default constructor
    */    
    public bltNIM3_CPTGroupList_List ( Integer iCPTGroupID )
    {
        super( "tNIM3_CPTGroupList", "LookupID", "LookupID", "CPTGroupID", iCPTGroupID );
    }   // End of bltNIM3_CPTGroupList_List()
    

    public void setField(String fieldN, Object fieldV)
    {DebugLogger.println("setField Method not implemented yet in classbltNIM3_CPTGroupList_List!");}

    public String getFieldType(String fieldN)
    {return null;}

    public bltNIM3_CPTGroupList_List ( Integer iCPTGroupID, String extraWhere, String OrderBy )
    {
        super( "tNIM3_CPTGroupList", "LookupID", "LookupID", "CPTGroupID", iCPTGroupID, extraWhere,OrderBy );
    }   // End of bltNIM3_CPTGroupList_List()
     /** Method pull information from the result set and load into class attributes.
    * @param sFieldList Comma delimited list of fields for which data has been retrieved.
    * @param rs Result set from which to pull the data from.
    * @throws SQLException Passes on SQLException
    */      
    protected void transferData( ResultSet rs ) throws SQLException
    {
        while ( rs.next()  )
        {
            Integer         iValue              = new Integer( rs.getString( sLinkFieldName ) );
            Integer         iLinkRecordID       = new Integer( rs.getString( sUniqueIDFieldName ) );
            Object          oReferenceObject;
            ListElement     leNewElement;
            
            oReferenceObject    = new bltNIM3_CPTGroupList( iValue );
            
            leNewElement        = new ListElement( iLinkRecordID, oReferenceObject );
            
            vList.add( leNewElement );

        }
    }   // End of transferData()
}   // End of bltNIM3_CPTGroupList_List class

