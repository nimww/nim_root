
package com.winstaff;
/*
 * bltNIM3_Encounter_List.java
 *
 * Created: Tue May 21 14:49:07 PDT 2013
 */



import java.sql.*;




/**
 *
 * @author  Scott Ellis via listClassGenCode
 * @version 2.5
 */
public class bltNIM3_Encounter_List extends db_BaseRelationTable
{
    /** Default constructor
    */    
    public bltNIM3_Encounter_List ( Integer iReferralID )
    {
        super( "tNIM3_Encounter", "EncounterID", "EncounterID", "ReferralID", iReferralID );
    }   // End of bltNIM3_Encounter_List()
    

    public void setField(String fieldN, Object fieldV)
    {DebugLogger.println("setField Method not implemented yet in classbltNIM3_Encounter_List!");}

    public String getFieldType(String fieldN)
    {return null;}

    public bltNIM3_Encounter_List ( Integer iReferralID, String extraWhere, String OrderBy )
    {
        super( "tNIM3_Encounter", "EncounterID", "EncounterID", "ReferralID", iReferralID, extraWhere,OrderBy );
    }   // End of bltNIM3_Encounter_List()
     /** Method pull information from the result set and load into class attributes.
    * @param sFieldList Comma delimited list of fields for which data has been retrieved.
    * @param rs Result set from which to pull the data from.
    * @throws SQLException Passes on SQLException
    */      
    protected void transferData( ResultSet rs ) throws SQLException
    {
        while ( rs.next()  )
        {
            Integer         iValue              = new Integer( rs.getString( sLinkFieldName ) );
            Integer         iLinkRecordID       = new Integer( rs.getString( sUniqueIDFieldName ) );
            Object          oReferenceObject;
            ListElement     leNewElement;
            
            oReferenceObject    = new bltNIM3_Encounter( iValue );
            
            leNewElement        = new ListElement( iLinkRecordID, oReferenceObject );
            
            vList.add( leNewElement );

        }
    }   // End of transferData()
}   // End of bltNIM3_Encounter_List class

