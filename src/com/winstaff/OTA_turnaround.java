package com.winstaff;

import java.io.DataOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.text.SimpleDateFormat;

import org.apache.commons.lang3.StringEscapeUtils;

/**
 * Generates and calculates OTA status and turn over times
 * 
 * @author Po Le
 * @since 25-March-2013
 */
public class OTA_turnaround {

	/**
	 * Main methods runs all the logic
	 * 
	 * @param args
	 * @throws SQLException
	 * @throws IOException
	 */
	public static void main(String[] args) throws SQLException, IOException {
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
		searchDB2 conn = new searchDB2();
		String query = "select e.scanpass, e.iscourtesy, e.seenetdev_waiting, c.uniquecreatedate openDate, c.alertstatuscode, c.uniquemodifydate closeDate, c.comments, EXTRACT(day FROM c.uniquemodifydate-c.uniquecreatedate) days, EXTRACT(hour FROM c.uniquemodifydate-c.uniquecreatedate) hours, EXTRACT(min FROM c.uniquemodifydate-c.uniquecreatedate) mins from tnim3_encounter e left join tnim3_commtrack c on c.encounterid = e.encounterid where e.seenetdev_flagged = 1 and c.commtypeid = 4 and c.uniquecreatedate > '2013-03-01' order by e.scanpass";
		query = "select e.scanpass, e.iscourtesy, e.seenetdev_waiting, to_char(c.uniquecreatedate,'yyyy-MM-dd') openDate, c.alertstatuscode, to_char(c.uniquemodifydate,'yyyy-MM-dd') closeDate, c.comments, EXTRACT(day FROM c.uniquemodifydate-c.uniquecreatedate) days, EXTRACT(hour FROM c.uniquemodifydate-c.uniquecreatedate) hours, EXTRACT(min FROM c.uniquemodifydate-c.uniquecreatedate) mins from tnim3_encounter e left join tnim3_commtrack c on c.encounterid = e.encounterid where e.seenetdev_flagged = 1 and c.commtypeid = 4 and c.uniquecreatedate > '2013-03-01' order by e.scanpass";
		java.sql.ResultSet result = conn.executeStatement(query);

		String header = "Scanpass," + "Open Date," + "Status," + "Close Date,"
				+ " Type," + "Turn Over," + "Case Comments\n";
		String fileName = "export/OTA_turnaround.csv";
		DataOutputStream out = new DataOutputStream(new FileOutputStream(
				fileName));
		out.writeBytes(header);
		String body = "";

		while (result.next()) {

			// Scanpass, Open Date
			body = result.getString("scanpass") + ","
					+ result.getString("openDate") + ",";
			// Status
			if (result.getInt("seenetdev_waiting") == 1) {
				body += "Open,";
			} else {
				body += "Closed,";
			}
			// Close Date
			if (result.getInt("seenetdev_waiting") == 1) {
				body += "--,";
			} else {
				body += result.getString("closeDate") + ",";
			}
			// Type
			if (result.getInt("seenetdev_waiting") == 1) {
				body += "Open,";
			}
			else if (result.getInt("iscourtesy") == 1) {
				body += "Courtesy,";
			} else if (result.getInt("iscourtesy") == 3) {
				body += "Declined,";
			} else if (result.getInt("alertstatuscode") == 3) {
				body += "OTA,";
			} else if (result.getInt("alertstatuscode") == 2) {
				body += "In Network,";
			} else {body += "Open,";}

			// Turn Over
			if (result.getInt("seenetdev_waiting") == 1) {
				body += "--,";
			}
			else if (result.getInt("alertstatuscode") == 0) {
				body += "--,";
			} else if (workingDays.getWorkingHours(result.getDate("openDate"),
					result.getDate("closeDate")) > 0) {
				body += workingDays.getWorkingHours(result.getDate("openDate"),
						result.getDate("closeDate")) + " hrs,";
			} else {
				body += result.getInt("mins") + " mins,";
			}
			// Comments
			body += StringEscapeUtils.escapeCsv(result.getString("comments"));
			body += "\n";
			out.writeBytes(body);
		}
		conn.closeAll();
		out.flush();
	}
}
