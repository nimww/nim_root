

package com.winstaff;

import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public class dbtNIM3_RetroWorklist extends Object implements InttNIM3_RetroWorklist
{

        db_NewBase    dbnbDB;

    public dbtNIM3_RetroWorklist()
    {
        dbnbDB = new db_NewBase( "tNIM3_RetroWorklist", "RetroWorklistID" );

    }    // End of default constructor

    public dbtNIM3_RetroWorklist( Integer iNewID )
    {
        dbnbDB = new db_NewBase( "tNIM3_RetroWorklist", "RetroWorklistID" );
        setUniqueID( iNewID );
    }    // End of Constructor knowing an ID

    public void commitData() throws SQLException
    {
            //no longer trapping/ignoring errors 2011-03-17
            dbnbDB.commitData();
    }    // End of commitData()

    public void commitDataForced() throws SQLException
    {
        dbnbDB.commitDataForced();
    }    // End of commitData()

    public Integer getUniqueID()
    {
        return dbnbDB.getUniqueID();
    }    // End of getUniqueID()

    public void setUniqueID( Integer iNewID )
    {
        dbnbDB.setUniqueID( iNewID );
    }    // End of setUniqueID()

    public void setModifyComments( String sNew )
    {
        dbnbDB.setModifyComments( sNew );
    }    // End of setUniqueID()


    public void setRetroWorklistID(Integer newValue)
    {
                dbnbDB.setFieldData( "RetroWorklistID", newValue.toString() );
    }

    public Integer getRetroWorklistID()
    {
        String           sValue = dbnbDB.getFieldData( "RetroWorklistID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setField(String fieldN, Object fieldV)
    {DebugLogger.println("setField Method not implemented yet in classNIM3_RetroWorklist!");}

    public String getFieldType(String fieldN)
    {return null;}


    public void setUniqueCreateDate(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "UniqueCreateDate", formatter.format( newValue ) );
    }

    public Date getUniqueCreateDate()
    {
        String           sValue = dbnbDB.getFieldData( "UniqueCreateDate" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setUniqueModifyDate(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "UniqueModifyDate", formatter.format( newValue ) );
    }

    public Date getUniqueModifyDate()
    {
        String           sValue = dbnbDB.getFieldData( "UniqueModifyDate" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setUniqueModifyComments(String newValue)
    {
                dbnbDB.setFieldData( "UniqueModifyComments", newValue.toString() );
    }

    public String getUniqueModifyComments()
    {
        String           sValue = dbnbDB.getFieldData( "UniqueModifyComments" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setPayerID(Integer newValue)
    {
                dbnbDB.setFieldData( "PayerID", newValue.toString() );
    }

    public Integer getPayerID()
    {
        String           sValue = dbnbDB.getFieldData( "PayerID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setCaseClaimNumber(String newValue)
    {
                dbnbDB.setFieldData( "CaseClaimNumber", newValue.toString() );
    }

    public String getCaseClaimNumber()
    {
        String           sValue = dbnbDB.getFieldData( "CaseClaimNumber" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setEmployerName(String newValue)
    {
                dbnbDB.setFieldData( "EmployerName", newValue.toString() );
    }

    public String getEmployerName()
    {
        String           sValue = dbnbDB.getFieldData( "EmployerName" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setEmployerPhone(String newValue)
    {
                dbnbDB.setFieldData( "EmployerPhone", newValue.toString() );
    }

    public String getEmployerPhone()
    {
        String           sValue = dbnbDB.getFieldData( "EmployerPhone" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setEmployerAddress1(String newValue)
    {
                dbnbDB.setFieldData( "EmployerAddress1", newValue.toString() );
    }

    public String getEmployerAddress1()
    {
        String           sValue = dbnbDB.getFieldData( "EmployerAddress1" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setEmployerAddress2(String newValue)
    {
                dbnbDB.setFieldData( "EmployerAddress2", newValue.toString() );
    }

    public String getEmployerAddress2()
    {
        String           sValue = dbnbDB.getFieldData( "EmployerAddress2" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setEmployerCity(String newValue)
    {
                dbnbDB.setFieldData( "EmployerCity", newValue.toString() );
    }

    public String getEmployerCity()
    {
        String           sValue = dbnbDB.getFieldData( "EmployerCity" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setEmployerCounty(String newValue)
    {
                dbnbDB.setFieldData( "EmployerCounty", newValue.toString() );
    }

    public String getEmployerCounty()
    {
        String           sValue = dbnbDB.getFieldData( "EmployerCounty" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setEmployerState(String newValue)
    {
                dbnbDB.setFieldData( "EmployerState", newValue.toString() );
    }

    public String getEmployerState()
    {
        String           sValue = dbnbDB.getFieldData( "EmployerState" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setEmployerZip(String newValue)
    {
                dbnbDB.setFieldData( "EmployerZip", newValue.toString() );
    }

    public String getEmployerZip()
    {
        String           sValue = dbnbDB.getFieldData( "EmployerZip" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setDateCaseOpened(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "DateCaseOpened", formatter.format( newValue ) );
    }

    public Date getDateCaseOpened()
    {
        String           sValue = dbnbDB.getFieldData( "DateCaseOpened" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setDateOfInjury(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "DateOfInjury", formatter.format( newValue ) );
    }

    public Date getDateOfInjury()
    {
        String           sValue = dbnbDB.getFieldData( "DateOfInjury" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setAttorneyFirstName(String newValue)
    {
                dbnbDB.setFieldData( "AttorneyFirstName", newValue.toString() );
    }

    public String getAttorneyFirstName()
    {
        String           sValue = dbnbDB.getFieldData( "AttorneyFirstName" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setAttorneyLastName(String newValue)
    {
                dbnbDB.setFieldData( "AttorneyLastName", newValue.toString() );
    }

    public String getAttorneyLastName()
    {
        String           sValue = dbnbDB.getFieldData( "AttorneyLastName" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setAttorneyWorkPhone(String newValue)
    {
                dbnbDB.setFieldData( "AttorneyWorkPhone", newValue.toString() );
    }

    public String getAttorneyWorkPhone()
    {
        String           sValue = dbnbDB.getFieldData( "AttorneyWorkPhone" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setAttorneyFax(String newValue)
    {
                dbnbDB.setFieldData( "AttorneyFax", newValue.toString() );
    }

    public String getAttorneyFax()
    {
        String           sValue = dbnbDB.getFieldData( "AttorneyFax" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setPatientAccountNumber(String newValue)
    {
                dbnbDB.setFieldData( "PatientAccountNumber", newValue.toString() );
    }

    public String getPatientAccountNumber()
    {
        String           sValue = dbnbDB.getFieldData( "PatientAccountNumber" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setPatientFirstName(String newValue)
    {
                dbnbDB.setFieldData( "PatientFirstName", newValue.toString() );
    }

    public String getPatientFirstName()
    {
        String           sValue = dbnbDB.getFieldData( "PatientFirstName" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setPatientLastName(String newValue)
    {
                dbnbDB.setFieldData( "PatientLastName", newValue.toString() );
    }

    public String getPatientLastName()
    {
        String           sValue = dbnbDB.getFieldData( "PatientLastName" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setPatientDOB(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "PatientDOB", formatter.format( newValue ) );
    }

    public Date getPatientDOB()
    {
        String           sValue = dbnbDB.getFieldData( "PatientDOB" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setPatientSSN(String newValue)
    {
                dbnbDB.setFieldData( "PatientSSN", newValue.toString() );
    }

    public String getPatientSSN()
    {
        String           sValue = dbnbDB.getFieldData( "PatientSSN" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setPatientGender(String newValue)
    {
                dbnbDB.setFieldData( "PatientGender", newValue.toString() );
    }

    public String getPatientGender()
    {
        String           sValue = dbnbDB.getFieldData( "PatientGender" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setPatientAddress1(String newValue)
    {
                dbnbDB.setFieldData( "PatientAddress1", newValue.toString() );
    }

    public String getPatientAddress1()
    {
        String           sValue = dbnbDB.getFieldData( "PatientAddress1" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setPatientAddress2(String newValue)
    {
                dbnbDB.setFieldData( "PatientAddress2", newValue.toString() );
    }

    public String getPatientAddress2()
    {
        String           sValue = dbnbDB.getFieldData( "PatientAddress2" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setPatientCity(String newValue)
    {
                dbnbDB.setFieldData( "PatientCity", newValue.toString() );
    }

    public String getPatientCity()
    {
        String           sValue = dbnbDB.getFieldData( "PatientCity" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setPatientState(String newValue)
    {
                dbnbDB.setFieldData( "PatientState", newValue.toString() );
    }

    public String getPatientState()
    {
        String           sValue = dbnbDB.getFieldData( "PatientState" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setPatientZip(String newValue)
    {
                dbnbDB.setFieldData( "PatientZip", newValue.toString() );
    }

    public String getPatientZip()
    {
        String           sValue = dbnbDB.getFieldData( "PatientZip" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setPatientHomePhone(String newValue)
    {
                dbnbDB.setFieldData( "PatientHomePhone", newValue.toString() );
    }

    public String getPatientHomePhone()
    {
        String           sValue = dbnbDB.getFieldData( "PatientHomePhone" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setPatientWorkPhone(String newValue)
    {
                dbnbDB.setFieldData( "PatientWorkPhone", newValue.toString() );
    }

    public String getPatientWorkPhone()
    {
        String           sValue = dbnbDB.getFieldData( "PatientWorkPhone" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setPatientCellPhone(String newValue)
    {
                dbnbDB.setFieldData( "PatientCellPhone", newValue.toString() );
    }

    public String getPatientCellPhone()
    {
        String           sValue = dbnbDB.getFieldData( "PatientCellPhone" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setPatientEmail(String newValue)
    {
                dbnbDB.setFieldData( "PatientEmail", newValue.toString() );
    }

    public String getPatientEmail()
    {
        String           sValue = dbnbDB.getFieldData( "PatientEmail" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setCorrusClaimId(String newValue)
    {
                dbnbDB.setFieldData( "CorrusClaimId", newValue.toString() );
    }

    public String getCorrusClaimId()
    {
        String           sValue = dbnbDB.getFieldData( "CorrusClaimId" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setDateofService(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "DateofService", formatter.format( newValue ) );
    }

    public Date getDateofService()
    {
        String           sValue = dbnbDB.getFieldData( "DateofService" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setClaimdate(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "Claimdate", formatter.format( newValue ) );
    }

    public Date getClaimdate()
    {
        String           sValue = dbnbDB.getFieldData( "Claimdate" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setClientClaimNum(String newValue)
    {
                dbnbDB.setFieldData( "ClientClaimNum", newValue.toString() );
    }

    public String getClientClaimNum()
    {
        String           sValue = dbnbDB.getFieldData( "ClientClaimNum" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setClientName(String newValue)
    {
                dbnbDB.setFieldData( "ClientName", newValue.toString() );
    }

    public String getClientName()
    {
        String           sValue = dbnbDB.getFieldData( "ClientName" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setCPTCode(String newValue)
    {
                dbnbDB.setFieldData( "CPTCode", newValue.toString() );
    }

    public String getCPTCode()
    {
        String           sValue = dbnbDB.getFieldData( "CPTCode" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setModifier1(String newValue)
    {
                dbnbDB.setFieldData( "Modifier1", newValue.toString() );
    }

    public String getModifier1()
    {
        String           sValue = dbnbDB.getFieldData( "Modifier1" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setModifier2(String newValue)
    {
                dbnbDB.setFieldData( "Modifier2", newValue.toString() );
    }

    public String getModifier2()
    {
        String           sValue = dbnbDB.getFieldData( "Modifier2" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setUnits(Double newValue)
    {
                java.text.DecimalFormat defDecFormat2 = new java.text.DecimalFormat(PLCUtils.String_defDecFormat2);
                dbnbDB.setFieldData( "Units", defDecFormat2.format(newValue) );
    }

    public Double getUnits()
    {
        String           sValue = dbnbDB.getFieldData( "Units" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDoubleValue : sValue;
        return new Double( sValue  ); 
    }

    public void setProviderCharge(Double newValue)
    {
                java.text.DecimalFormat defDecFormat2 = new java.text.DecimalFormat(PLCUtils.String_defDecFormat2);
                dbnbDB.setFieldData( "ProviderCharge", defDecFormat2.format(newValue) );
    }

    public Double getProviderCharge()
    {
        String           sValue = dbnbDB.getFieldData( "ProviderCharge" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDoubleValue : sValue;
        return new Double( sValue  ); 
    }

    public void setStateAllowance(Double newValue)
    {
                java.text.DecimalFormat defDecFormat2 = new java.text.DecimalFormat(PLCUtils.String_defDecFormat2);
                dbnbDB.setFieldData( "StateAllowance", defDecFormat2.format(newValue) );
    }

    public Double getStateAllowance()
    {
        String           sValue = dbnbDB.getFieldData( "StateAllowance" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDoubleValue : sValue;
        return new Double( sValue  ); 
    }

    public void setPayerAllowance(Double newValue)
    {
                java.text.DecimalFormat defDecFormat2 = new java.text.DecimalFormat(PLCUtils.String_defDecFormat2);
                dbnbDB.setFieldData( "PayerAllowance", defDecFormat2.format(newValue) );
    }

    public Double getPayerAllowance()
    {
        String           sValue = dbnbDB.getFieldData( "PayerAllowance" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDoubleValue : sValue;
        return new Double( sValue  ); 
    }

    public void setNetworkAllowance(Double newValue)
    {
                java.text.DecimalFormat defDecFormat2 = new java.text.DecimalFormat(PLCUtils.String_defDecFormat2);
                dbnbDB.setFieldData( "NetworkAllowance", defDecFormat2.format(newValue) );
    }

    public Double getNetworkAllowance()
    {
        String           sValue = dbnbDB.getFieldData( "NetworkAllowance" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDoubleValue : sValue;
        return new Double( sValue  ); 
    }

    public void setNetworkSavings(Double newValue)
    {
                java.text.DecimalFormat defDecFormat2 = new java.text.DecimalFormat(PLCUtils.String_defDecFormat2);
                dbnbDB.setFieldData( "NetworkSavings", defDecFormat2.format(newValue) );
    }

    public Double getNetworkSavings()
    {
        String           sValue = dbnbDB.getFieldData( "NetworkSavings" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDoubleValue : sValue;
        return new Double( sValue  ); 
    }

    public void setRejectDescription(String newValue)
    {
                dbnbDB.setFieldData( "RejectDescription", newValue.toString() );
    }

    public String getRejectDescription()
    {
        String           sValue = dbnbDB.getFieldData( "RejectDescription" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setDiagCode1(String newValue)
    {
                dbnbDB.setFieldData( "DiagCode1", newValue.toString() );
    }

    public String getDiagCode1()
    {
        String           sValue = dbnbDB.getFieldData( "DiagCode1" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setDiagCode2(String newValue)
    {
                dbnbDB.setFieldData( "DiagCode2", newValue.toString() );
    }

    public String getDiagCode2()
    {
        String           sValue = dbnbDB.getFieldData( "DiagCode2" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setDiagCode3(String newValue)
    {
                dbnbDB.setFieldData( "DiagCode3", newValue.toString() );
    }

    public String getDiagCode3()
    {
        String           sValue = dbnbDB.getFieldData( "DiagCode3" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setDiagCode4(String newValue)
    {
                dbnbDB.setFieldData( "DiagCode4", newValue.toString() );
    }

    public String getDiagCode4()
    {
        String           sValue = dbnbDB.getFieldData( "DiagCode4" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setClaimstatus(String newValue)
    {
                dbnbDB.setFieldData( "Claimstatus", newValue.toString() );
    }

    public String getClaimstatus()
    {
        String           sValue = dbnbDB.getFieldData( "Claimstatus" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setCaseManagerFirstName(String newValue)
    {
                dbnbDB.setFieldData( "CaseManagerFirstName", newValue.toString() );
    }

    public String getCaseManagerFirstName()
    {
        String           sValue = dbnbDB.getFieldData( "CaseManagerFirstName" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setCaseManagerLastName(String newValue)
    {
                dbnbDB.setFieldData( "CaseManagerLastName", newValue.toString() );
    }

    public String getCaseManagerLastName()
    {
        String           sValue = dbnbDB.getFieldData( "CaseManagerLastName" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setAccountReference(String newValue)
    {
                dbnbDB.setFieldData( "AccountReference", newValue.toString() );
    }

    public String getAccountReference()
    {
        String           sValue = dbnbDB.getFieldData( "AccountReference" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setControverted(String newValue)
    {
                dbnbDB.setFieldData( "Controverted", newValue.toString() );
    }

    public String getControverted()
    {
        String           sValue = dbnbDB.getFieldData( "Controverted" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setMMI(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "MMI", formatter.format( newValue ) );
    }

    public Date getMMI()
    {
        String           sValue = dbnbDB.getFieldData( "MMI" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setDatabaseID(String newValue)
    {
                dbnbDB.setFieldData( "DatabaseID", newValue.toString() );
    }

    public String getDatabaseID()
    {
        String           sValue = dbnbDB.getFieldData( "DatabaseID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setJurisdicationState(String newValue)
    {
                dbnbDB.setFieldData( "JurisdicationState", newValue.toString() );
    }

    public String getJurisdicationState()
    {
        String           sValue = dbnbDB.getFieldData( "JurisdicationState" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setJurisdicationZip(String newValue)
    {
                dbnbDB.setFieldData( "JurisdicationZip", newValue.toString() );
    }

    public String getJurisdicationZip()
    {
        String           sValue = dbnbDB.getFieldData( "JurisdicationZip" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setAdjusterReferenceID(String newValue)
    {
                dbnbDB.setFieldData( "AdjusterReferenceID", newValue.toString() );
    }

    public String getAdjusterReferenceID()
    {
        String           sValue = dbnbDB.getFieldData( "AdjusterReferenceID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setAdjusteFirstName(String newValue)
    {
                dbnbDB.setFieldData( "AdjusteFirstName", newValue.toString() );
    }

    public String getAdjusteFirstName()
    {
        String           sValue = dbnbDB.getFieldData( "AdjusteFirstName" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setAdjusterLastName(String newValue)
    {
                dbnbDB.setFieldData( "AdjusterLastName", newValue.toString() );
    }

    public String getAdjusterLastName()
    {
        String           sValue = dbnbDB.getFieldData( "AdjusterLastName" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setAdjusterPhoneNumber(String newValue)
    {
                dbnbDB.setFieldData( "AdjusterPhoneNumber", newValue.toString() );
    }

    public String getAdjusterPhoneNumber()
    {
        String           sValue = dbnbDB.getFieldData( "AdjusterPhoneNumber" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setAdjusterFaxNumber(String newValue)
    {
                dbnbDB.setFieldData( "AdjusterFaxNumber", newValue.toString() );
    }

    public String getAdjusterFaxNumber()
    {
        String           sValue = dbnbDB.getFieldData( "AdjusterFaxNumber" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setAdjusterEmail(String newValue)
    {
                dbnbDB.setFieldData( "AdjusterEmail", newValue.toString() );
    }

    public String getAdjusterEmail()
    {
        String           sValue = dbnbDB.getFieldData( "AdjusterEmail" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setAdjusterCompany(String newValue)
    {
                dbnbDB.setFieldData( "AdjusterCompany", newValue.toString() );
    }

    public String getAdjusterCompany()
    {
        String           sValue = dbnbDB.getFieldData( "AdjusterCompany" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setAdjusterAddress1(String newValue)
    {
                dbnbDB.setFieldData( "AdjusterAddress1", newValue.toString() );
    }

    public String getAdjusterAddress1()
    {
        String           sValue = dbnbDB.getFieldData( "AdjusterAddress1" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setAdjusterAddress2(String newValue)
    {
                dbnbDB.setFieldData( "AdjusterAddress2", newValue.toString() );
    }

    public String getAdjusterAddress2()
    {
        String           sValue = dbnbDB.getFieldData( "AdjusterAddress2" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setAdjusterCity(String newValue)
    {
                dbnbDB.setFieldData( "AdjusterCity", newValue.toString() );
    }

    public String getAdjusterCity()
    {
        String           sValue = dbnbDB.getFieldData( "AdjusterCity" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setAdjusterState(String newValue)
    {
                dbnbDB.setFieldData( "AdjusterState", newValue.toString() );
    }

    public String getAdjusterState()
    {
        String           sValue = dbnbDB.getFieldData( "AdjusterState" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setAdjusterZip(String newValue)
    {
                dbnbDB.setFieldData( "AdjusterZip", newValue.toString() );
    }

    public String getAdjusterZip()
    {
        String           sValue = dbnbDB.getFieldData( "AdjusterZip" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setReferringDoctorReferenceID(String newValue)
    {
                dbnbDB.setFieldData( "ReferringDoctorReferenceID", newValue.toString() );
    }

    public String getReferringDoctorReferenceID()
    {
        String           sValue = dbnbDB.getFieldData( "ReferringDoctorReferenceID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setReferringDoctorNPI(String newValue)
    {
                dbnbDB.setFieldData( "ReferringDoctorNPI", newValue.toString() );
    }

    public String getReferringDoctorNPI()
    {
        String           sValue = dbnbDB.getFieldData( "ReferringDoctorNPI" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setReferringDoctorMedicalLicense(String newValue)
    {
                dbnbDB.setFieldData( "ReferringDoctorMedicalLicense", newValue.toString() );
    }

    public String getReferringDoctorMedicalLicense()
    {
        String           sValue = dbnbDB.getFieldData( "ReferringDoctorMedicalLicense" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setReferringDoctorFirstName(String newValue)
    {
                dbnbDB.setFieldData( "ReferringDoctorFirstName", newValue.toString() );
    }

    public String getReferringDoctorFirstName()
    {
        String           sValue = dbnbDB.getFieldData( "ReferringDoctorFirstName" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setReferringDoctorLastName(String newValue)
    {
                dbnbDB.setFieldData( "ReferringDoctorLastName", newValue.toString() );
    }

    public String getReferringDoctorLastName()
    {
        String           sValue = dbnbDB.getFieldData( "ReferringDoctorLastName" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setReferringDoctorPhoneNumber(String newValue)
    {
                dbnbDB.setFieldData( "ReferringDoctorPhoneNumber", newValue.toString() );
    }

    public String getReferringDoctorPhoneNumber()
    {
        String           sValue = dbnbDB.getFieldData( "ReferringDoctorPhoneNumber" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setReferringDoctorFaxNumber(String newValue)
    {
                dbnbDB.setFieldData( "ReferringDoctorFaxNumber", newValue.toString() );
    }

    public String getReferringDoctorFaxNumber()
    {
        String           sValue = dbnbDB.getFieldData( "ReferringDoctorFaxNumber" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setReferringDoctorEmail(String newValue)
    {
                dbnbDB.setFieldData( "ReferringDoctorEmail", newValue.toString() );
    }

    public String getReferringDoctorEmail()
    {
        String           sValue = dbnbDB.getFieldData( "ReferringDoctorEmail" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setReferringDoctorCompany(String newValue)
    {
                dbnbDB.setFieldData( "ReferringDoctorCompany", newValue.toString() );
    }

    public String getReferringDoctorCompany()
    {
        String           sValue = dbnbDB.getFieldData( "ReferringDoctorCompany" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setReferringDoctorAddress1(String newValue)
    {
                dbnbDB.setFieldData( "ReferringDoctorAddress1", newValue.toString() );
    }

    public String getReferringDoctorAddress1()
    {
        String           sValue = dbnbDB.getFieldData( "ReferringDoctorAddress1" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setReferringDoctorAddress2(String newValue)
    {
                dbnbDB.setFieldData( "ReferringDoctorAddress2", newValue.toString() );
    }

    public String getReferringDoctorAddress2()
    {
        String           sValue = dbnbDB.getFieldData( "ReferringDoctorAddress2" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setReferringDoctorCity(String newValue)
    {
                dbnbDB.setFieldData( "ReferringDoctorCity", newValue.toString() );
    }

    public String getReferringDoctorCity()
    {
        String           sValue = dbnbDB.getFieldData( "ReferringDoctorCity" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setReferringDoctorState(String newValue)
    {
                dbnbDB.setFieldData( "ReferringDoctorState", newValue.toString() );
    }

    public String getReferringDoctorState()
    {
        String           sValue = dbnbDB.getFieldData( "ReferringDoctorState" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setReferringDoctorZip(String newValue)
    {
                dbnbDB.setFieldData( "ReferringDoctorZip", newValue.toString() );
    }

    public String getReferringDoctorZip()
    {
        String           sValue = dbnbDB.getFieldData( "ReferringDoctorZip" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setNotes(String newValue)
    {
                dbnbDB.setFieldData( "Notes", newValue.toString() );
    }

    public String getNotes()
    {
        String           sValue = dbnbDB.getFieldData( "Notes" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setLineID(Integer newValue)
    {
                dbnbDB.setFieldData( "LineID", newValue.toString() );
    }

    public Integer getLineID()
    {
        String           sValue = dbnbDB.getFieldData( "LineID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setRenderingProviderReferenceID(String newValue)
    {
                dbnbDB.setFieldData( "RenderingProviderReferenceID", newValue.toString() );
    }

    public String getRenderingProviderReferenceID()
    {
        String           sValue = dbnbDB.getFieldData( "RenderingProviderReferenceID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setRenderingProviderNPI(String newValue)
    {
                dbnbDB.setFieldData( "RenderingProviderNPI", newValue.toString() );
    }

    public String getRenderingProviderNPI()
    {
        String           sValue = dbnbDB.getFieldData( "RenderingProviderNPI" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setRenderingProviderTaxID(String newValue)
    {
                dbnbDB.setFieldData( "RenderingProviderTaxID", newValue.toString() );
    }

    public String getRenderingProviderTaxID()
    {
        String           sValue = dbnbDB.getFieldData( "RenderingProviderTaxID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setRenderingProviderMedicalLicense(String newValue)
    {
                dbnbDB.setFieldData( "RenderingProviderMedicalLicense", newValue.toString() );
    }

    public String getRenderingProviderMedicalLicense()
    {
        String           sValue = dbnbDB.getFieldData( "RenderingProviderMedicalLicense" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setRenderingProviderFirstName(String newValue)
    {
                dbnbDB.setFieldData( "RenderingProviderFirstName", newValue.toString() );
    }

    public String getRenderingProviderFirstName()
    {
        String           sValue = dbnbDB.getFieldData( "RenderingProviderFirstName" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setRenderingProviderLastName(String newValue)
    {
                dbnbDB.setFieldData( "RenderingProviderLastName", newValue.toString() );
    }

    public String getRenderingProviderLastName()
    {
        String           sValue = dbnbDB.getFieldData( "RenderingProviderLastName" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setRenderingProviderPhoneNumber(String newValue)
    {
                dbnbDB.setFieldData( "RenderingProviderPhoneNumber", newValue.toString() );
    }

    public String getRenderingProviderPhoneNumber()
    {
        String           sValue = dbnbDB.getFieldData( "RenderingProviderPhoneNumber" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setRenderingProviderFaxNumber(String newValue)
    {
                dbnbDB.setFieldData( "RenderingProviderFaxNumber", newValue.toString() );
    }

    public String getRenderingProviderFaxNumber()
    {
        String           sValue = dbnbDB.getFieldData( "RenderingProviderFaxNumber" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setRenderingProviderEmail(String newValue)
    {
                dbnbDB.setFieldData( "RenderingProviderEmail", newValue.toString() );
    }

    public String getRenderingProviderEmail()
    {
        String           sValue = dbnbDB.getFieldData( "RenderingProviderEmail" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setRenderingProviderCompany(String newValue)
    {
                dbnbDB.setFieldData( "RenderingProviderCompany", newValue.toString() );
    }

    public String getRenderingProviderCompany()
    {
        String           sValue = dbnbDB.getFieldData( "RenderingProviderCompany" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setRenderingProviderAddress1(String newValue)
    {
                dbnbDB.setFieldData( "RenderingProviderAddress1", newValue.toString() );
    }

    public String getRenderingProviderAddress1()
    {
        String           sValue = dbnbDB.getFieldData( "RenderingProviderAddress1" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setRenderingProviderAddress2(String newValue)
    {
                dbnbDB.setFieldData( "RenderingProviderAddress2", newValue.toString() );
    }

    public String getRenderingProviderAddress2()
    {
        String           sValue = dbnbDB.getFieldData( "RenderingProviderAddress2" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setRenderingProviderCity(String newValue)
    {
                dbnbDB.setFieldData( "RenderingProviderCity", newValue.toString() );
    }

    public String getRenderingProviderCity()
    {
        String           sValue = dbnbDB.getFieldData( "RenderingProviderCity" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setRenderingProviderState(String newValue)
    {
                dbnbDB.setFieldData( "RenderingProviderState", newValue.toString() );
    }

    public String getRenderingProviderState()
    {
        String           sValue = dbnbDB.getFieldData( "RenderingProviderState" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setRenderingProviderZip(String newValue)
    {
                dbnbDB.setFieldData( "RenderingProviderZip", newValue.toString() );
    }

    public String getRenderingProviderZip()
    {
        String           sValue = dbnbDB.getFieldData( "RenderingProviderZip" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setBillingProviderReferenceID(String newValue)
    {
                dbnbDB.setFieldData( "BillingProviderReferenceID", newValue.toString() );
    }

    public String getBillingProviderReferenceID()
    {
        String           sValue = dbnbDB.getFieldData( "BillingProviderReferenceID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setBillingProviderNPI(String newValue)
    {
                dbnbDB.setFieldData( "BillingProviderNPI", newValue.toString() );
    }

    public String getBillingProviderNPI()
    {
        String           sValue = dbnbDB.getFieldData( "BillingProviderNPI" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setBillingProviderTaxID(String newValue)
    {
                dbnbDB.setFieldData( "BillingProviderTaxID", newValue.toString() );
    }

    public String getBillingProviderTaxID()
    {
        String           sValue = dbnbDB.getFieldData( "BillingProviderTaxID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setBillingProviderMedicalLicense(String newValue)
    {
                dbnbDB.setFieldData( "BillingProviderMedicalLicense", newValue.toString() );
    }

    public String getBillingProviderMedicalLicense()
    {
        String           sValue = dbnbDB.getFieldData( "BillingProviderMedicalLicense" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setBillingProviderFirstName(String newValue)
    {
                dbnbDB.setFieldData( "BillingProviderFirstName", newValue.toString() );
    }

    public String getBillingProviderFirstName()
    {
        String           sValue = dbnbDB.getFieldData( "BillingProviderFirstName" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setBillingProviderLastName(String newValue)
    {
                dbnbDB.setFieldData( "BillingProviderLastName", newValue.toString() );
    }

    public String getBillingProviderLastName()
    {
        String           sValue = dbnbDB.getFieldData( "BillingProviderLastName" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setBillingProviderPhoneNumber(String newValue)
    {
                dbnbDB.setFieldData( "BillingProviderPhoneNumber", newValue.toString() );
    }

    public String getBillingProviderPhoneNumber()
    {
        String           sValue = dbnbDB.getFieldData( "BillingProviderPhoneNumber" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setBillingProviderFaxNumber(String newValue)
    {
                dbnbDB.setFieldData( "BillingProviderFaxNumber", newValue.toString() );
    }

    public String getBillingProviderFaxNumber()
    {
        String           sValue = dbnbDB.getFieldData( "BillingProviderFaxNumber" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setBillingProviderEmail(String newValue)
    {
                dbnbDB.setFieldData( "BillingProviderEmail", newValue.toString() );
    }

    public String getBillingProviderEmail()
    {
        String           sValue = dbnbDB.getFieldData( "BillingProviderEmail" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setBillingProviderCompany(String newValue)
    {
                dbnbDB.setFieldData( "BillingProviderCompany", newValue.toString() );
    }

    public String getBillingProviderCompany()
    {
        String           sValue = dbnbDB.getFieldData( "BillingProviderCompany" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setBillingProviderAddress1(String newValue)
    {
                dbnbDB.setFieldData( "BillingProviderAddress1", newValue.toString() );
    }

    public String getBillingProviderAddress1()
    {
        String           sValue = dbnbDB.getFieldData( "BillingProviderAddress1" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setBillingProviderAddress2(String newValue)
    {
                dbnbDB.setFieldData( "BillingProviderAddress2", newValue.toString() );
    }

    public String getBillingProviderAddress2()
    {
        String           sValue = dbnbDB.getFieldData( "BillingProviderAddress2" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setBillingProviderCity(String newValue)
    {
                dbnbDB.setFieldData( "BillingProviderCity", newValue.toString() );
    }

    public String getBillingProviderCity()
    {
        String           sValue = dbnbDB.getFieldData( "BillingProviderCity" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setBillingProviderState(String newValue)
    {
                dbnbDB.setFieldData( "BillingProviderState", newValue.toString() );
    }

    public String getBillingProviderState()
    {
        String           sValue = dbnbDB.getFieldData( "BillingProviderState" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setBillingProviderZip(String newValue)
    {
                dbnbDB.setFieldData( "BillingProviderZip", newValue.toString() );
    }

    public String getBillingProviderZip()
    {
        String           sValue = dbnbDB.getFieldData( "BillingProviderZip" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setNetwork_UserField1(String newValue)
    {
                dbnbDB.setFieldData( "Network_UserField1", newValue.toString() );
    }

    public String getNetwork_UserField1()
    {
        String           sValue = dbnbDB.getFieldData( "Network_UserField1" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setNetwork_UserField2(String newValue)
    {
                dbnbDB.setFieldData( "Network_UserField2", newValue.toString() );
    }

    public String getNetwork_UserField2()
    {
        String           sValue = dbnbDB.getFieldData( "Network_UserField2" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setNetwork_UserField3(String newValue)
    {
                dbnbDB.setFieldData( "Network_UserField3", newValue.toString() );
    }

    public String getNetwork_UserField3()
    {
        String           sValue = dbnbDB.getFieldData( "Network_UserField3" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setNetwork_UserField4(String newValue)
    {
                dbnbDB.setFieldData( "Network_UserField4", newValue.toString() );
    }

    public String getNetwork_UserField4()
    {
        String           sValue = dbnbDB.getFieldData( "Network_UserField4" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setNetwork_UserField5(String newValue)
    {
                dbnbDB.setFieldData( "Network_UserField5", newValue.toString() );
    }

    public String getNetwork_UserField5()
    {
        String           sValue = dbnbDB.getFieldData( "Network_UserField5" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setNIM_ProviderReferenceID(String newValue)
    {
                dbnbDB.setFieldData( "NIM_ProviderReferenceID", newValue.toString() );
    }

    public String getNIM_ProviderReferenceID()
    {
        String           sValue = dbnbDB.getFieldData( "NIM_ProviderReferenceID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setNIM_ProviderNPI(String newValue)
    {
                dbnbDB.setFieldData( "NIM_ProviderNPI", newValue.toString() );
    }

    public String getNIM_ProviderNPI()
    {
        String           sValue = dbnbDB.getFieldData( "NIM_ProviderNPI" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setNIM_ProviderTaxID(String newValue)
    {
                dbnbDB.setFieldData( "NIM_ProviderTaxID", newValue.toString() );
    }

    public String getNIM_ProviderTaxID()
    {
        String           sValue = dbnbDB.getFieldData( "NIM_ProviderTaxID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setNIM_ProviderMedicalLicense(String newValue)
    {
                dbnbDB.setFieldData( "NIM_ProviderMedicalLicense", newValue.toString() );
    }

    public String getNIM_ProviderMedicalLicense()
    {
        String           sValue = dbnbDB.getFieldData( "NIM_ProviderMedicalLicense" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setNIM_ProviderFirstName(String newValue)
    {
                dbnbDB.setFieldData( "NIM_ProviderFirstName", newValue.toString() );
    }

    public String getNIM_ProviderFirstName()
    {
        String           sValue = dbnbDB.getFieldData( "NIM_ProviderFirstName" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setNIM_ProviderLastName(String newValue)
    {
                dbnbDB.setFieldData( "NIM_ProviderLastName", newValue.toString() );
    }

    public String getNIM_ProviderLastName()
    {
        String           sValue = dbnbDB.getFieldData( "NIM_ProviderLastName" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setNIM_ProviderPhoneNumber(String newValue)
    {
                dbnbDB.setFieldData( "NIM_ProviderPhoneNumber", newValue.toString() );
    }

    public String getNIM_ProviderPhoneNumber()
    {
        String           sValue = dbnbDB.getFieldData( "NIM_ProviderPhoneNumber" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setNIM_ProviderFaxNumber(String newValue)
    {
                dbnbDB.setFieldData( "NIM_ProviderFaxNumber", newValue.toString() );
    }

    public String getNIM_ProviderFaxNumber()
    {
        String           sValue = dbnbDB.getFieldData( "NIM_ProviderFaxNumber" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setNIM_ProviderEmail(String newValue)
    {
                dbnbDB.setFieldData( "NIM_ProviderEmail", newValue.toString() );
    }

    public String getNIM_ProviderEmail()
    {
        String           sValue = dbnbDB.getFieldData( "NIM_ProviderEmail" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setNIM_ProviderCompany(String newValue)
    {
                dbnbDB.setFieldData( "NIM_ProviderCompany", newValue.toString() );
    }

    public String getNIM_ProviderCompany()
    {
        String           sValue = dbnbDB.getFieldData( "NIM_ProviderCompany" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setNIM_ProviderAddress1(String newValue)
    {
                dbnbDB.setFieldData( "NIM_ProviderAddress1", newValue.toString() );
    }

    public String getNIM_ProviderAddress1()
    {
        String           sValue = dbnbDB.getFieldData( "NIM_ProviderAddress1" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setNIM_ProviderAddress2(String newValue)
    {
                dbnbDB.setFieldData( "NIM_ProviderAddress2", newValue.toString() );
    }

    public String getNIM_ProviderAddress2()
    {
        String           sValue = dbnbDB.getFieldData( "NIM_ProviderAddress2" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setNIM_ProviderCity(String newValue)
    {
                dbnbDB.setFieldData( "NIM_ProviderCity", newValue.toString() );
    }

    public String getNIM_ProviderCity()
    {
        String           sValue = dbnbDB.getFieldData( "NIM_ProviderCity" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setNIM_ProviderState(String newValue)
    {
                dbnbDB.setFieldData( "NIM_ProviderState", newValue.toString() );
    }

    public String getNIM_ProviderState()
    {
        String           sValue = dbnbDB.getFieldData( "NIM_ProviderState" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setNIM_ProviderZip(String newValue)
    {
                dbnbDB.setFieldData( "NIM_ProviderZip", newValue.toString() );
    }

    public String getNIM_ProviderZip()
    {
        String           sValue = dbnbDB.getFieldData( "NIM_ProviderZip" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setNIM_ProviderMatchFound(Integer newValue)
    {
                dbnbDB.setFieldData( "NIM_ProviderMatchFound", newValue.toString() );
    }

    public Integer getNIM_ProviderMatchFound()
    {
        String           sValue = dbnbDB.getFieldData( "NIM_ProviderMatchFound" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setNIM_ProviderMatchAcceptsRetro(Integer newValue)
    {
                dbnbDB.setFieldData( "NIM_ProviderMatchAcceptsRetro", newValue.toString() );
    }

    public Integer getNIM_ProviderMatchAcceptsRetro()
    {
        String           sValue = dbnbDB.getFieldData( "NIM_ProviderMatchAcceptsRetro" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setNIM_ProviderMatchPositiveSavings(Integer newValue)
    {
                dbnbDB.setFieldData( "NIM_ProviderMatchPositiveSavings", newValue.toString() );
    }

    public Integer getNIM_ProviderMatchPositiveSavings()
    {
        String           sValue = dbnbDB.getFieldData( "NIM_ProviderMatchPositiveSavings" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setNIM_AcceptClaim(Integer newValue)
    {
                dbnbDB.setFieldData( "NIM_AcceptClaim", newValue.toString() );
    }

    public Integer getNIM_AcceptClaim()
    {
        String           sValue = dbnbDB.getFieldData( "NIM_AcceptClaim" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setNIM_DateAccepted(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "NIM_DateAccepted", formatter.format( newValue ) );
    }

    public Date getNIM_DateAccepted()
    {
        String           sValue = dbnbDB.getFieldData( "NIM_DateAccepted" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setNIM_UserAcceptedID(Integer newValue)
    {
                dbnbDB.setFieldData( "NIM_UserAcceptedID", newValue.toString() );
    }

    public Integer getNIM_UserAcceptedID()
    {
        String           sValue = dbnbDB.getFieldData( "NIM_UserAcceptedID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setNIM_DateExported(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "NIM_DateExported", formatter.format( newValue ) );
    }

    public Date getNIM_DateExported()
    {
        String           sValue = dbnbDB.getFieldData( "NIM_DateExported" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setNIM_EncounterID(Integer newValue)
    {
                dbnbDB.setFieldData( "NIM_EncounterID", newValue.toString() );
    }

    public Integer getNIM_EncounterID()
    {
        String           sValue = dbnbDB.getFieldData( "NIM_EncounterID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setNIM_ContractProviderCost(Double newValue)
    {
                java.text.DecimalFormat defDecFormat2 = new java.text.DecimalFormat(PLCUtils.String_defDecFormat2);
                dbnbDB.setFieldData( "NIM_ContractProviderCost", defDecFormat2.format(newValue) );
    }

    public Double getNIM_ContractProviderCost()
    {
        String           sValue = dbnbDB.getFieldData( "NIM_ContractProviderCost" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDoubleValue : sValue;
        return new Double( sValue  ); 
    }

    public void setNIM_FinalProviderCost(Double newValue)
    {
                java.text.DecimalFormat defDecFormat2 = new java.text.DecimalFormat(PLCUtils.String_defDecFormat2);
                dbnbDB.setFieldData( "NIM_FinalProviderCost", defDecFormat2.format(newValue) );
    }

    public Double getNIM_FinalProviderCost()
    {
        String           sValue = dbnbDB.getFieldData( "NIM_FinalProviderCost" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDoubleValue : sValue;
        return new Double( sValue  ); 
    }

    public void setNIM_PayerAllow(Double newValue)
    {
                java.text.DecimalFormat defDecFormat2 = new java.text.DecimalFormat(PLCUtils.String_defDecFormat2);
                dbnbDB.setFieldData( "NIM_PayerAllow", defDecFormat2.format(newValue) );
    }

    public Double getNIM_PayerAllow()
    {
        String           sValue = dbnbDB.getFieldData( "NIM_PayerAllow" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDoubleValue : sValue;
        return new Double( sValue  ); 
    }

}    // End of bltNIM3_RetroWorklist class definition
