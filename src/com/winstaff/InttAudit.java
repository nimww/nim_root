package com.winstaff;


import com.winstaff.dbtAudit;
import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public interface InttAudit extends dbTableInterface
{

    public void setAuditID(Integer newValue);
    public Integer getAuditID();
    public void setUniqueCreateDate(Date newValue);
    public Date getUniqueCreateDate();
    public void setUniqueModifyDate(Date newValue);
    public Date getUniqueModifyDate();
    public void setUniqueModifyComments(String newValue);
    public String getUniqueModifyComments();
    public void setAuditDate(Date newValue);
    public Date getAuditDate();
    public void setTableName(String newValue);
    public String getTableName();
    public void setFieldName(String newValue);
    public String getFieldName();
    public void setrefID(String newValue);
    public String getrefID();
    public void setValueChange(String newValue);
    public String getValueChange();
    public void setModifier(String newValue);
    public String getModifier();
    public void setComments(String newValue);
    public String getComments();
}    // End of bltAudit class definition
