

package com.winstaff;

import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;
import java.util.Enumeration;

public class bltNIM3_RetroWorklist extends Object implements InttNIM3_RetroWorklist
{

    dbtNIM3_RetroWorklist    dbDB;

    public bltNIM3_RetroWorklist()
    {
        dbDB = new dbtNIM3_RetroWorklist();
        dbDB.setUniqueCreateDate(new java.util.Date());
        makeRequiredHash();
        makeExpiredHash();
        makeEnglishHash();

    }    // End of default constructor

    public bltNIM3_RetroWorklist( Integer iNewID )
    {        dbDB = new dbtNIM3_RetroWorklist( iNewID );
        makeRequiredHash();
        makeExpiredHash();
        makeEnglishHash();
        iSecurityCheck = new Integer(2);
    }    // End of Constructor knowing an ID


    public bltNIM3_RetroWorklist( Integer iNewID, Integer iGroupSecurityID )
    {
        this.GroupSecurityInit(iGroupSecurityID);
        dbDB = new dbtNIM3_RetroWorklist( iNewID );
        makeExpiredHash();
        makeEnglishHash();
        makeRequiredHash(iGroupSecurityID);
    }    // End of Constructor knowing an ID

    public bltNIM3_RetroWorklist( Integer iGroupSecurityID, boolean isTrue )
    {
        this.GroupSecurityInit(iGroupSecurityID);
        dbDB = new dbtNIM3_RetroWorklist(  );
        makeExpiredHash();
        makeEnglishHash();
        makeRequiredHash(iGroupSecurityID);
    }    // End of Security unknown ID

    public void GroupSecurityInit( Integer iGroupSecurityID )
    {
        bltSecurityGroupMaster mySGM = new bltSecurityGroupMaster(iGroupSecurityID);
        this.UserSecurityID = iGroupSecurityID;
        if (mySGM.getRunFieldSecurityID().intValue()==1)
        {
            this.runFieldSecurity = true;
            makeRequiredHash(iGroupSecurityID);
        }
        iSecurityCheck = SecurityCheck.CheckItem("nim1", iGroupSecurityID);
    }  

    public EventReferenceType getEventReference()
    {
        return EventUtils.getEventReference("tNIM3_RetroWorklist", this.getUniqueID());
    }  

    public String convertString2LogString(String normalString) 
    {
        String myVal = "";
        for (int i=0;i<normalString.length();i++)
        {
            if ( (normalString.charAt(i)!='\r')&&(normalString.charAt(i)!='\n')&&(normalString.charAt(i)!='\t') )
            {
                myVal+=normalString.charAt(i);
            }
            else
            {
                myVal+="<NLT>";
            }
        }
        return myVal;
    }
    public void runAudit()
    {
        String AuditString = new java.util.Date()+" \t " ;  
        AuditString += " tNIM3_RetroWorklist "; 
        AuditString += " RetroWorklistID ="+this.getUniqueID(); 
        AuditString += " PayerID ="+this.getPayerID(); 
        for (int i=0;i<this.AuditVector.size();i++)
        {
            AuditString+="\t"+convertString2LogString((String)this.AuditVector.elementAt(i));
        }
        AuditLogger.printLine(AuditString);
    }
    public void commitData() throws SQLException
    {
        runAudit();
        dbDB.setModifyComments(this.getUniqueModifyComments());
        dbDB.setUniqueModifyDate(new java.util.Date());
        dbDB.commitData();
        DataControlUtils.dataChanged("tNIM3_RetroWorklist",this.getPayerID() ,this.AuditVector.size(), "PayerID");
        this.AuditVector.removeAllElements();
    }    // End of commitData()

    public void commitDataForced() throws SQLException
    {
        dbDB.commitDataForced();
    }    // End of commitData()

    public Integer getUniqueID()
    {
        return dbDB.getUniqueID();
    }    // End of getUniqueID()

    public void setUniqueID( Integer iNewID )
    {
        dbDB.setUniqueID( iNewID );
    }    // End of setUniqueID()


    public void setRetroWorklistID(Integer newValue)
    {
        dbDB.setRetroWorklistID(newValue);
    }

    public Integer getRetroWorklistID()
    {
        return dbDB.getRetroWorklistID();
    }

    java.util.Vector AuditVector = new java.util.Vector();

    public void setUniqueCreateDate(Date newValue)
    {
        this.setUniqueCreateDate(newValue,this.UserSecurityID);


    }
    public Date getUniqueCreateDate()
    {
        return this.getUniqueCreateDate(this.UserSecurityID);
    }

    public void setUniqueCreateDate(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='UniqueCreateDate' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[UniqueCreateDate]=["+newValue+"]");
                   dbDB.setUniqueCreateDate(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[UniqueCreateDate]=["+newValue+"]");
           dbDB.setUniqueCreateDate(newValue);
         }
    }
    public Date getUniqueCreateDate(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='UniqueCreateDate' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getUniqueCreateDate();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getUniqueCreateDate();
         }
        return myVal;
    }

    public void setUniqueModifyDate(Date newValue)
    {
        this.setUniqueModifyDate(newValue,this.UserSecurityID);


    }
    public Date getUniqueModifyDate()
    {
        return this.getUniqueModifyDate(this.UserSecurityID);
    }

    public void setUniqueModifyDate(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='UniqueModifyDate' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[UniqueModifyDate]=["+newValue+"]");
                   dbDB.setUniqueModifyDate(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[UniqueModifyDate]=["+newValue+"]");
           dbDB.setUniqueModifyDate(newValue);
         }
    }
    public Date getUniqueModifyDate(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='UniqueModifyDate' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getUniqueModifyDate();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getUniqueModifyDate();
         }
        return myVal;
    }

    public void setUniqueModifyComments(String newValue)
    {
        this.setUniqueModifyComments(newValue,this.UserSecurityID);


    }
    public String getUniqueModifyComments()
    {
        return this.getUniqueModifyComments(this.UserSecurityID);
    }

    public void setUniqueModifyComments(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='UniqueModifyComments' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[UniqueModifyComments]=["+newValue+"]");
                   dbDB.setUniqueModifyComments(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[UniqueModifyComments]=["+newValue+"]");
           dbDB.setUniqueModifyComments(newValue);
         }
    }
    public String getUniqueModifyComments(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='UniqueModifyComments' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getUniqueModifyComments();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getUniqueModifyComments();
         }
        return myVal;
    }

    public void setPayerID(Integer newValue)
    {
        this.setPayerID(newValue,this.UserSecurityID);


    }
    public Integer getPayerID()
    {
        return this.getPayerID(this.UserSecurityID);
    }

    public void setPayerID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PayerID' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[PayerID]=["+newValue+"]");
                   dbDB.setPayerID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[PayerID]=["+newValue+"]");
           dbDB.setPayerID(newValue);
         }
    }
    public Integer getPayerID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PayerID' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPayerID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPayerID();
         }
        return myVal;
    }

    public void setCaseClaimNumber(String newValue)
    {
        this.setCaseClaimNumber(newValue,this.UserSecurityID);


    }
    public String getCaseClaimNumber()
    {
        return this.getCaseClaimNumber(this.UserSecurityID);
    }

    public void setCaseClaimNumber(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CaseClaimNumber' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[CaseClaimNumber]=["+newValue+"]");
                   dbDB.setCaseClaimNumber(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[CaseClaimNumber]=["+newValue+"]");
           dbDB.setCaseClaimNumber(newValue);
         }
    }
    public String getCaseClaimNumber(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CaseClaimNumber' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getCaseClaimNumber();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getCaseClaimNumber();
         }
        return myVal;
    }

    public void setEmployerName(String newValue)
    {
        this.setEmployerName(newValue,this.UserSecurityID);


    }
    public String getEmployerName()
    {
        return this.getEmployerName(this.UserSecurityID);
    }

    public void setEmployerName(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='EmployerName' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[EmployerName]=["+newValue+"]");
                   dbDB.setEmployerName(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[EmployerName]=["+newValue+"]");
           dbDB.setEmployerName(newValue);
         }
    }
    public String getEmployerName(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='EmployerName' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getEmployerName();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getEmployerName();
         }
        return myVal;
    }

    public void setEmployerPhone(String newValue)
    {
        this.setEmployerPhone(newValue,this.UserSecurityID);


    }
    public String getEmployerPhone()
    {
        return this.getEmployerPhone(this.UserSecurityID);
    }

    public void setEmployerPhone(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='EmployerPhone' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[EmployerPhone]=["+newValue+"]");
                   dbDB.setEmployerPhone(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[EmployerPhone]=["+newValue+"]");
           dbDB.setEmployerPhone(newValue);
         }
    }
    public String getEmployerPhone(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='EmployerPhone' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getEmployerPhone();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getEmployerPhone();
         }
        return myVal;
    }

    public void setEmployerAddress1(String newValue)
    {
        this.setEmployerAddress1(newValue,this.UserSecurityID);


    }
    public String getEmployerAddress1()
    {
        return this.getEmployerAddress1(this.UserSecurityID);
    }

    public void setEmployerAddress1(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='EmployerAddress1' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[EmployerAddress1]=["+newValue+"]");
                   dbDB.setEmployerAddress1(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[EmployerAddress1]=["+newValue+"]");
           dbDB.setEmployerAddress1(newValue);
         }
    }
    public String getEmployerAddress1(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='EmployerAddress1' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getEmployerAddress1();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getEmployerAddress1();
         }
        return myVal;
    }

    public void setEmployerAddress2(String newValue)
    {
        this.setEmployerAddress2(newValue,this.UserSecurityID);


    }
    public String getEmployerAddress2()
    {
        return this.getEmployerAddress2(this.UserSecurityID);
    }

    public void setEmployerAddress2(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='EmployerAddress2' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[EmployerAddress2]=["+newValue+"]");
                   dbDB.setEmployerAddress2(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[EmployerAddress2]=["+newValue+"]");
           dbDB.setEmployerAddress2(newValue);
         }
    }
    public String getEmployerAddress2(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='EmployerAddress2' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getEmployerAddress2();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getEmployerAddress2();
         }
        return myVal;
    }

    public void setEmployerCity(String newValue)
    {
        this.setEmployerCity(newValue,this.UserSecurityID);


    }
    public String getEmployerCity()
    {
        return this.getEmployerCity(this.UserSecurityID);
    }

    public void setEmployerCity(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='EmployerCity' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[EmployerCity]=["+newValue+"]");
                   dbDB.setEmployerCity(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[EmployerCity]=["+newValue+"]");
           dbDB.setEmployerCity(newValue);
         }
    }
    public String getEmployerCity(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='EmployerCity' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getEmployerCity();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getEmployerCity();
         }
        return myVal;
    }

    public void setEmployerCounty(String newValue)
    {
        this.setEmployerCounty(newValue,this.UserSecurityID);


    }
    public String getEmployerCounty()
    {
        return this.getEmployerCounty(this.UserSecurityID);
    }

    public void setEmployerCounty(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='EmployerCounty' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[EmployerCounty]=["+newValue+"]");
                   dbDB.setEmployerCounty(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[EmployerCounty]=["+newValue+"]");
           dbDB.setEmployerCounty(newValue);
         }
    }
    public String getEmployerCounty(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='EmployerCounty' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getEmployerCounty();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getEmployerCounty();
         }
        return myVal;
    }

    public void setEmployerState(String newValue)
    {
        this.setEmployerState(newValue,this.UserSecurityID);


    }
    public String getEmployerState()
    {
        return this.getEmployerState(this.UserSecurityID);
    }

    public void setEmployerState(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='EmployerState' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[EmployerState]=["+newValue+"]");
                   dbDB.setEmployerState(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[EmployerState]=["+newValue+"]");
           dbDB.setEmployerState(newValue);
         }
    }
    public String getEmployerState(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='EmployerState' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getEmployerState();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getEmployerState();
         }
        return myVal;
    }

    public void setEmployerZip(String newValue)
    {
        this.setEmployerZip(newValue,this.UserSecurityID);


    }
    public String getEmployerZip()
    {
        return this.getEmployerZip(this.UserSecurityID);
    }

    public void setEmployerZip(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='EmployerZip' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[EmployerZip]=["+newValue+"]");
                   dbDB.setEmployerZip(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[EmployerZip]=["+newValue+"]");
           dbDB.setEmployerZip(newValue);
         }
    }
    public String getEmployerZip(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='EmployerZip' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getEmployerZip();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getEmployerZip();
         }
        return myVal;
    }

    public void setDateCaseOpened(Date newValue)
    {
        this.setDateCaseOpened(newValue,this.UserSecurityID);


    }
    public Date getDateCaseOpened()
    {
        return this.getDateCaseOpened(this.UserSecurityID);
    }

    public void setDateCaseOpened(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='DateCaseOpened' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[DateCaseOpened]=["+newValue+"]");
                   dbDB.setDateCaseOpened(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[DateCaseOpened]=["+newValue+"]");
           dbDB.setDateCaseOpened(newValue);
         }
    }
    public Date getDateCaseOpened(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='DateCaseOpened' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getDateCaseOpened();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getDateCaseOpened();
         }
        return myVal;
    }

    public void setDateOfInjury(Date newValue)
    {
        this.setDateOfInjury(newValue,this.UserSecurityID);


    }
    public Date getDateOfInjury()
    {
        return this.getDateOfInjury(this.UserSecurityID);
    }

    public void setDateOfInjury(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='DateOfInjury' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[DateOfInjury]=["+newValue+"]");
                   dbDB.setDateOfInjury(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[DateOfInjury]=["+newValue+"]");
           dbDB.setDateOfInjury(newValue);
         }
    }
    public Date getDateOfInjury(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='DateOfInjury' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getDateOfInjury();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getDateOfInjury();
         }
        return myVal;
    }

    public void setAttorneyFirstName(String newValue)
    {
        this.setAttorneyFirstName(newValue,this.UserSecurityID);


    }
    public String getAttorneyFirstName()
    {
        return this.getAttorneyFirstName(this.UserSecurityID);
    }

    public void setAttorneyFirstName(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AttorneyFirstName' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[AttorneyFirstName]=["+newValue+"]");
                   dbDB.setAttorneyFirstName(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[AttorneyFirstName]=["+newValue+"]");
           dbDB.setAttorneyFirstName(newValue);
         }
    }
    public String getAttorneyFirstName(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AttorneyFirstName' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getAttorneyFirstName();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getAttorneyFirstName();
         }
        return myVal;
    }

    public void setAttorneyLastName(String newValue)
    {
        this.setAttorneyLastName(newValue,this.UserSecurityID);


    }
    public String getAttorneyLastName()
    {
        return this.getAttorneyLastName(this.UserSecurityID);
    }

    public void setAttorneyLastName(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AttorneyLastName' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[AttorneyLastName]=["+newValue+"]");
                   dbDB.setAttorneyLastName(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[AttorneyLastName]=["+newValue+"]");
           dbDB.setAttorneyLastName(newValue);
         }
    }
    public String getAttorneyLastName(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AttorneyLastName' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getAttorneyLastName();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getAttorneyLastName();
         }
        return myVal;
    }

    public void setAttorneyWorkPhone(String newValue)
    {
        this.setAttorneyWorkPhone(newValue,this.UserSecurityID);


    }
    public String getAttorneyWorkPhone()
    {
        return this.getAttorneyWorkPhone(this.UserSecurityID);
    }

    public void setAttorneyWorkPhone(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AttorneyWorkPhone' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[AttorneyWorkPhone]=["+newValue+"]");
                   dbDB.setAttorneyWorkPhone(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[AttorneyWorkPhone]=["+newValue+"]");
           dbDB.setAttorneyWorkPhone(newValue);
         }
    }
    public String getAttorneyWorkPhone(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AttorneyWorkPhone' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getAttorneyWorkPhone();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getAttorneyWorkPhone();
         }
        return myVal;
    }

    public void setAttorneyFax(String newValue)
    {
        this.setAttorneyFax(newValue,this.UserSecurityID);


    }
    public String getAttorneyFax()
    {
        return this.getAttorneyFax(this.UserSecurityID);
    }

    public void setAttorneyFax(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AttorneyFax' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[AttorneyFax]=["+newValue+"]");
                   dbDB.setAttorneyFax(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[AttorneyFax]=["+newValue+"]");
           dbDB.setAttorneyFax(newValue);
         }
    }
    public String getAttorneyFax(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AttorneyFax' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getAttorneyFax();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getAttorneyFax();
         }
        return myVal;
    }

    public void setPatientAccountNumber(String newValue)
    {
        this.setPatientAccountNumber(newValue,this.UserSecurityID);


    }
    public String getPatientAccountNumber()
    {
        return this.getPatientAccountNumber(this.UserSecurityID);
    }

    public void setPatientAccountNumber(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientAccountNumber' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[PatientAccountNumber]=["+newValue+"]");
                   dbDB.setPatientAccountNumber(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[PatientAccountNumber]=["+newValue+"]");
           dbDB.setPatientAccountNumber(newValue);
         }
    }
    public String getPatientAccountNumber(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientAccountNumber' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPatientAccountNumber();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPatientAccountNumber();
         }
        return myVal;
    }

    public void setPatientFirstName(String newValue)
    {
        this.setPatientFirstName(newValue,this.UserSecurityID);


    }
    public String getPatientFirstName()
    {
        return this.getPatientFirstName(this.UserSecurityID);
    }

    public void setPatientFirstName(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientFirstName' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[PatientFirstName]=["+newValue+"]");
                   dbDB.setPatientFirstName(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[PatientFirstName]=["+newValue+"]");
           dbDB.setPatientFirstName(newValue);
         }
    }
    public String getPatientFirstName(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientFirstName' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPatientFirstName();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPatientFirstName();
         }
        return myVal;
    }

    public void setPatientLastName(String newValue)
    {
        this.setPatientLastName(newValue,this.UserSecurityID);


    }
    public String getPatientLastName()
    {
        return this.getPatientLastName(this.UserSecurityID);
    }

    public void setPatientLastName(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientLastName' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[PatientLastName]=["+newValue+"]");
                   dbDB.setPatientLastName(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[PatientLastName]=["+newValue+"]");
           dbDB.setPatientLastName(newValue);
         }
    }
    public String getPatientLastName(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientLastName' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPatientLastName();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPatientLastName();
         }
        return myVal;
    }

    public void setPatientDOB(Date newValue)
    {
        this.setPatientDOB(newValue,this.UserSecurityID);


    }
    public Date getPatientDOB()
    {
        return this.getPatientDOB(this.UserSecurityID);
    }

    public void setPatientDOB(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientDOB' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[PatientDOB]=["+newValue+"]");
                   dbDB.setPatientDOB(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[PatientDOB]=["+newValue+"]");
           dbDB.setPatientDOB(newValue);
         }
    }
    public Date getPatientDOB(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientDOB' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPatientDOB();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPatientDOB();
         }
        return myVal;
    }

    public void setPatientSSN(String newValue)
    {
        this.setPatientSSN(newValue,this.UserSecurityID);


    }
    public String getPatientSSN()
    {
        return this.getPatientSSN(this.UserSecurityID);
    }

    public void setPatientSSN(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientSSN' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[PatientSSN]=["+newValue+"]");
                   dbDB.setPatientSSN(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[PatientSSN]=["+newValue+"]");
           dbDB.setPatientSSN(newValue);
         }
    }
    public String getPatientSSN(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientSSN' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPatientSSN();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPatientSSN();
         }
        return myVal;
    }

    public void setPatientGender(String newValue)
    {
        this.setPatientGender(newValue,this.UserSecurityID);


    }
    public String getPatientGender()
    {
        return this.getPatientGender(this.UserSecurityID);
    }

    public void setPatientGender(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientGender' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[PatientGender]=["+newValue+"]");
                   dbDB.setPatientGender(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[PatientGender]=["+newValue+"]");
           dbDB.setPatientGender(newValue);
         }
    }
    public String getPatientGender(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientGender' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPatientGender();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPatientGender();
         }
        return myVal;
    }

    public void setPatientAddress1(String newValue)
    {
        this.setPatientAddress1(newValue,this.UserSecurityID);


    }
    public String getPatientAddress1()
    {
        return this.getPatientAddress1(this.UserSecurityID);
    }

    public void setPatientAddress1(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientAddress1' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[PatientAddress1]=["+newValue+"]");
                   dbDB.setPatientAddress1(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[PatientAddress1]=["+newValue+"]");
           dbDB.setPatientAddress1(newValue);
         }
    }
    public String getPatientAddress1(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientAddress1' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPatientAddress1();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPatientAddress1();
         }
        return myVal;
    }

    public void setPatientAddress2(String newValue)
    {
        this.setPatientAddress2(newValue,this.UserSecurityID);


    }
    public String getPatientAddress2()
    {
        return this.getPatientAddress2(this.UserSecurityID);
    }

    public void setPatientAddress2(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientAddress2' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[PatientAddress2]=["+newValue+"]");
                   dbDB.setPatientAddress2(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[PatientAddress2]=["+newValue+"]");
           dbDB.setPatientAddress2(newValue);
         }
    }
    public String getPatientAddress2(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientAddress2' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPatientAddress2();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPatientAddress2();
         }
        return myVal;
    }

    public void setPatientCity(String newValue)
    {
        this.setPatientCity(newValue,this.UserSecurityID);


    }
    public String getPatientCity()
    {
        return this.getPatientCity(this.UserSecurityID);
    }

    public void setPatientCity(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientCity' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[PatientCity]=["+newValue+"]");
                   dbDB.setPatientCity(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[PatientCity]=["+newValue+"]");
           dbDB.setPatientCity(newValue);
         }
    }
    public String getPatientCity(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientCity' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPatientCity();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPatientCity();
         }
        return myVal;
    }

    public void setPatientState(String newValue)
    {
        this.setPatientState(newValue,this.UserSecurityID);


    }
    public String getPatientState()
    {
        return this.getPatientState(this.UserSecurityID);
    }

    public void setPatientState(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientState' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[PatientState]=["+newValue+"]");
                   dbDB.setPatientState(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[PatientState]=["+newValue+"]");
           dbDB.setPatientState(newValue);
         }
    }
    public String getPatientState(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientState' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPatientState();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPatientState();
         }
        return myVal;
    }

    public void setPatientZip(String newValue)
    {
        this.setPatientZip(newValue,this.UserSecurityID);


    }
    public String getPatientZip()
    {
        return this.getPatientZip(this.UserSecurityID);
    }

    public void setPatientZip(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientZip' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[PatientZip]=["+newValue+"]");
                   dbDB.setPatientZip(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[PatientZip]=["+newValue+"]");
           dbDB.setPatientZip(newValue);
         }
    }
    public String getPatientZip(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientZip' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPatientZip();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPatientZip();
         }
        return myVal;
    }

    public void setPatientHomePhone(String newValue)
    {
        this.setPatientHomePhone(newValue,this.UserSecurityID);


    }
    public String getPatientHomePhone()
    {
        return this.getPatientHomePhone(this.UserSecurityID);
    }

    public void setPatientHomePhone(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientHomePhone' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[PatientHomePhone]=["+newValue+"]");
                   dbDB.setPatientHomePhone(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[PatientHomePhone]=["+newValue+"]");
           dbDB.setPatientHomePhone(newValue);
         }
    }
    public String getPatientHomePhone(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientHomePhone' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPatientHomePhone();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPatientHomePhone();
         }
        return myVal;
    }

    public void setPatientWorkPhone(String newValue)
    {
        this.setPatientWorkPhone(newValue,this.UserSecurityID);


    }
    public String getPatientWorkPhone()
    {
        return this.getPatientWorkPhone(this.UserSecurityID);
    }

    public void setPatientWorkPhone(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientWorkPhone' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[PatientWorkPhone]=["+newValue+"]");
                   dbDB.setPatientWorkPhone(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[PatientWorkPhone]=["+newValue+"]");
           dbDB.setPatientWorkPhone(newValue);
         }
    }
    public String getPatientWorkPhone(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientWorkPhone' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPatientWorkPhone();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPatientWorkPhone();
         }
        return myVal;
    }

    public void setPatientCellPhone(String newValue)
    {
        this.setPatientCellPhone(newValue,this.UserSecurityID);


    }
    public String getPatientCellPhone()
    {
        return this.getPatientCellPhone(this.UserSecurityID);
    }

    public void setPatientCellPhone(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientCellPhone' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[PatientCellPhone]=["+newValue+"]");
                   dbDB.setPatientCellPhone(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[PatientCellPhone]=["+newValue+"]");
           dbDB.setPatientCellPhone(newValue);
         }
    }
    public String getPatientCellPhone(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientCellPhone' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPatientCellPhone();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPatientCellPhone();
         }
        return myVal;
    }

    public void setPatientEmail(String newValue)
    {
        this.setPatientEmail(newValue,this.UserSecurityID);


    }
    public String getPatientEmail()
    {
        return this.getPatientEmail(this.UserSecurityID);
    }

    public void setPatientEmail(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientEmail' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[PatientEmail]=["+newValue+"]");
                   dbDB.setPatientEmail(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[PatientEmail]=["+newValue+"]");
           dbDB.setPatientEmail(newValue);
         }
    }
    public String getPatientEmail(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientEmail' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPatientEmail();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPatientEmail();
         }
        return myVal;
    }

    public void setCorrusClaimId(String newValue)
    {
        this.setCorrusClaimId(newValue,this.UserSecurityID);


    }
    public String getCorrusClaimId()
    {
        return this.getCorrusClaimId(this.UserSecurityID);
    }

    public void setCorrusClaimId(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CorrusClaimId' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[CorrusClaimId]=["+newValue+"]");
                   dbDB.setCorrusClaimId(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[CorrusClaimId]=["+newValue+"]");
           dbDB.setCorrusClaimId(newValue);
         }
    }
    public String getCorrusClaimId(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CorrusClaimId' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getCorrusClaimId();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getCorrusClaimId();
         }
        return myVal;
    }

    public void setDateofService(Date newValue)
    {
        this.setDateofService(newValue,this.UserSecurityID);


    }
    public Date getDateofService()
    {
        return this.getDateofService(this.UserSecurityID);
    }

    public void setDateofService(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='DateofService' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[DateofService]=["+newValue+"]");
                   dbDB.setDateofService(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[DateofService]=["+newValue+"]");
           dbDB.setDateofService(newValue);
         }
    }
    public Date getDateofService(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='DateofService' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getDateofService();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getDateofService();
         }
        return myVal;
    }

    public void setClaimdate(Date newValue)
    {
        this.setClaimdate(newValue,this.UserSecurityID);


    }
    public Date getClaimdate()
    {
        return this.getClaimdate(this.UserSecurityID);
    }

    public void setClaimdate(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Claimdate' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[Claimdate]=["+newValue+"]");
                   dbDB.setClaimdate(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[Claimdate]=["+newValue+"]");
           dbDB.setClaimdate(newValue);
         }
    }
    public Date getClaimdate(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Claimdate' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getClaimdate();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getClaimdate();
         }
        return myVal;
    }

    public void setClientClaimNum(String newValue)
    {
        this.setClientClaimNum(newValue,this.UserSecurityID);


    }
    public String getClientClaimNum()
    {
        return this.getClientClaimNum(this.UserSecurityID);
    }

    public void setClientClaimNum(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ClientClaimNum' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[ClientClaimNum]=["+newValue+"]");
                   dbDB.setClientClaimNum(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[ClientClaimNum]=["+newValue+"]");
           dbDB.setClientClaimNum(newValue);
         }
    }
    public String getClientClaimNum(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ClientClaimNum' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getClientClaimNum();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getClientClaimNum();
         }
        return myVal;
    }

    public void setClientName(String newValue)
    {
        this.setClientName(newValue,this.UserSecurityID);


    }
    public String getClientName()
    {
        return this.getClientName(this.UserSecurityID);
    }

    public void setClientName(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ClientName' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[ClientName]=["+newValue+"]");
                   dbDB.setClientName(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[ClientName]=["+newValue+"]");
           dbDB.setClientName(newValue);
         }
    }
    public String getClientName(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ClientName' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getClientName();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getClientName();
         }
        return myVal;
    }

    public void setCPTCode(String newValue)
    {
        this.setCPTCode(newValue,this.UserSecurityID);


    }
    public String getCPTCode()
    {
        return this.getCPTCode(this.UserSecurityID);
    }

    public void setCPTCode(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CPTCode' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[CPTCode]=["+newValue+"]");
                   dbDB.setCPTCode(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[CPTCode]=["+newValue+"]");
           dbDB.setCPTCode(newValue);
         }
    }
    public String getCPTCode(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CPTCode' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getCPTCode();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getCPTCode();
         }
        return myVal;
    }

    public void setModifier1(String newValue)
    {
        this.setModifier1(newValue,this.UserSecurityID);


    }
    public String getModifier1()
    {
        return this.getModifier1(this.UserSecurityID);
    }

    public void setModifier1(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Modifier1' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[Modifier1]=["+newValue+"]");
                   dbDB.setModifier1(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[Modifier1]=["+newValue+"]");
           dbDB.setModifier1(newValue);
         }
    }
    public String getModifier1(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Modifier1' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getModifier1();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getModifier1();
         }
        return myVal;
    }

    public void setModifier2(String newValue)
    {
        this.setModifier2(newValue,this.UserSecurityID);


    }
    public String getModifier2()
    {
        return this.getModifier2(this.UserSecurityID);
    }

    public void setModifier2(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Modifier2' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[Modifier2]=["+newValue+"]");
                   dbDB.setModifier2(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[Modifier2]=["+newValue+"]");
           dbDB.setModifier2(newValue);
         }
    }
    public String getModifier2(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Modifier2' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getModifier2();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getModifier2();
         }
        return myVal;
    }

    public void setUnits(Double newValue)
    {
        this.setUnits(newValue,this.UserSecurityID);


    }
    public Double getUnits()
    {
        return this.getUnits(this.UserSecurityID);
    }

    public void setUnits(Double newValue, Integer GroupRefID)
    {
        Double myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Units' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[Units]=["+newValue+"]");
                   dbDB.setUnits(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[Units]=["+newValue+"]");
           dbDB.setUnits(newValue);
         }
    }
    public Double getUnits(Integer GroupRefID)
    {
        Double myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Units' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getUnits();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getUnits();
         }
        return myVal;
    }

    public void setProviderCharge(Double newValue)
    {
        this.setProviderCharge(newValue,this.UserSecurityID);


    }
    public Double getProviderCharge()
    {
        return this.getProviderCharge(this.UserSecurityID);
    }

    public void setProviderCharge(Double newValue, Integer GroupRefID)
    {
        Double myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ProviderCharge' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[ProviderCharge]=["+newValue+"]");
                   dbDB.setProviderCharge(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[ProviderCharge]=["+newValue+"]");
           dbDB.setProviderCharge(newValue);
         }
    }
    public Double getProviderCharge(Integer GroupRefID)
    {
        Double myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ProviderCharge' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getProviderCharge();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getProviderCharge();
         }
        return myVal;
    }

    public void setStateAllowance(Double newValue)
    {
        this.setStateAllowance(newValue,this.UserSecurityID);


    }
    public Double getStateAllowance()
    {
        return this.getStateAllowance(this.UserSecurityID);
    }

    public void setStateAllowance(Double newValue, Integer GroupRefID)
    {
        Double myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='StateAllowance' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[StateAllowance]=["+newValue+"]");
                   dbDB.setStateAllowance(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[StateAllowance]=["+newValue+"]");
           dbDB.setStateAllowance(newValue);
         }
    }
    public Double getStateAllowance(Integer GroupRefID)
    {
        Double myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='StateAllowance' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getStateAllowance();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getStateAllowance();
         }
        return myVal;
    }

    public void setPayerAllowance(Double newValue)
    {
        this.setPayerAllowance(newValue,this.UserSecurityID);


    }
    public Double getPayerAllowance()
    {
        return this.getPayerAllowance(this.UserSecurityID);
    }

    public void setPayerAllowance(Double newValue, Integer GroupRefID)
    {
        Double myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PayerAllowance' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[PayerAllowance]=["+newValue+"]");
                   dbDB.setPayerAllowance(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[PayerAllowance]=["+newValue+"]");
           dbDB.setPayerAllowance(newValue);
         }
    }
    public Double getPayerAllowance(Integer GroupRefID)
    {
        Double myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PayerAllowance' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPayerAllowance();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPayerAllowance();
         }
        return myVal;
    }

    public void setNetworkAllowance(Double newValue)
    {
        this.setNetworkAllowance(newValue,this.UserSecurityID);


    }
    public Double getNetworkAllowance()
    {
        return this.getNetworkAllowance(this.UserSecurityID);
    }

    public void setNetworkAllowance(Double newValue, Integer GroupRefID)
    {
        Double myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='NetworkAllowance' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[NetworkAllowance]=["+newValue+"]");
                   dbDB.setNetworkAllowance(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[NetworkAllowance]=["+newValue+"]");
           dbDB.setNetworkAllowance(newValue);
         }
    }
    public Double getNetworkAllowance(Integer GroupRefID)
    {
        Double myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='NetworkAllowance' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getNetworkAllowance();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getNetworkAllowance();
         }
        return myVal;
    }

    public void setNetworkSavings(Double newValue)
    {
        this.setNetworkSavings(newValue,this.UserSecurityID);


    }
    public Double getNetworkSavings()
    {
        return this.getNetworkSavings(this.UserSecurityID);
    }

    public void setNetworkSavings(Double newValue, Integer GroupRefID)
    {
        Double myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='NetworkSavings' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[NetworkSavings]=["+newValue+"]");
                   dbDB.setNetworkSavings(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[NetworkSavings]=["+newValue+"]");
           dbDB.setNetworkSavings(newValue);
         }
    }
    public Double getNetworkSavings(Integer GroupRefID)
    {
        Double myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='NetworkSavings' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getNetworkSavings();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getNetworkSavings();
         }
        return myVal;
    }

    public void setRejectDescription(String newValue)
    {
        this.setRejectDescription(newValue,this.UserSecurityID);


    }
    public String getRejectDescription()
    {
        return this.getRejectDescription(this.UserSecurityID);
    }

    public void setRejectDescription(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='RejectDescription' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[RejectDescription]=["+newValue+"]");
                   dbDB.setRejectDescription(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[RejectDescription]=["+newValue+"]");
           dbDB.setRejectDescription(newValue);
         }
    }
    public String getRejectDescription(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='RejectDescription' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getRejectDescription();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getRejectDescription();
         }
        return myVal;
    }

    public void setDiagCode1(String newValue)
    {
        this.setDiagCode1(newValue,this.UserSecurityID);


    }
    public String getDiagCode1()
    {
        return this.getDiagCode1(this.UserSecurityID);
    }

    public void setDiagCode1(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='DiagCode1' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[DiagCode1]=["+newValue+"]");
                   dbDB.setDiagCode1(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[DiagCode1]=["+newValue+"]");
           dbDB.setDiagCode1(newValue);
         }
    }
    public String getDiagCode1(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='DiagCode1' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getDiagCode1();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getDiagCode1();
         }
        return myVal;
    }

    public void setDiagCode2(String newValue)
    {
        this.setDiagCode2(newValue,this.UserSecurityID);


    }
    public String getDiagCode2()
    {
        return this.getDiagCode2(this.UserSecurityID);
    }

    public void setDiagCode2(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='DiagCode2' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[DiagCode2]=["+newValue+"]");
                   dbDB.setDiagCode2(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[DiagCode2]=["+newValue+"]");
           dbDB.setDiagCode2(newValue);
         }
    }
    public String getDiagCode2(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='DiagCode2' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getDiagCode2();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getDiagCode2();
         }
        return myVal;
    }

    public void setDiagCode3(String newValue)
    {
        this.setDiagCode3(newValue,this.UserSecurityID);


    }
    public String getDiagCode3()
    {
        return this.getDiagCode3(this.UserSecurityID);
    }

    public void setDiagCode3(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='DiagCode3' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[DiagCode3]=["+newValue+"]");
                   dbDB.setDiagCode3(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[DiagCode3]=["+newValue+"]");
           dbDB.setDiagCode3(newValue);
         }
    }
    public String getDiagCode3(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='DiagCode3' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getDiagCode3();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getDiagCode3();
         }
        return myVal;
    }

    public void setDiagCode4(String newValue)
    {
        this.setDiagCode4(newValue,this.UserSecurityID);


    }
    public String getDiagCode4()
    {
        return this.getDiagCode4(this.UserSecurityID);
    }

    public void setDiagCode4(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='DiagCode4' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[DiagCode4]=["+newValue+"]");
                   dbDB.setDiagCode4(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[DiagCode4]=["+newValue+"]");
           dbDB.setDiagCode4(newValue);
         }
    }
    public String getDiagCode4(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='DiagCode4' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getDiagCode4();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getDiagCode4();
         }
        return myVal;
    }

    public void setClaimstatus(String newValue)
    {
        this.setClaimstatus(newValue,this.UserSecurityID);


    }
    public String getClaimstatus()
    {
        return this.getClaimstatus(this.UserSecurityID);
    }

    public void setClaimstatus(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Claimstatus' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[Claimstatus]=["+newValue+"]");
                   dbDB.setClaimstatus(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[Claimstatus]=["+newValue+"]");
           dbDB.setClaimstatus(newValue);
         }
    }
    public String getClaimstatus(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Claimstatus' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getClaimstatus();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getClaimstatus();
         }
        return myVal;
    }

    public void setCaseManagerFirstName(String newValue)
    {
        this.setCaseManagerFirstName(newValue,this.UserSecurityID);


    }
    public String getCaseManagerFirstName()
    {
        return this.getCaseManagerFirstName(this.UserSecurityID);
    }

    public void setCaseManagerFirstName(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CaseManagerFirstName' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[CaseManagerFirstName]=["+newValue+"]");
                   dbDB.setCaseManagerFirstName(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[CaseManagerFirstName]=["+newValue+"]");
           dbDB.setCaseManagerFirstName(newValue);
         }
    }
    public String getCaseManagerFirstName(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CaseManagerFirstName' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getCaseManagerFirstName();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getCaseManagerFirstName();
         }
        return myVal;
    }

    public void setCaseManagerLastName(String newValue)
    {
        this.setCaseManagerLastName(newValue,this.UserSecurityID);


    }
    public String getCaseManagerLastName()
    {
        return this.getCaseManagerLastName(this.UserSecurityID);
    }

    public void setCaseManagerLastName(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CaseManagerLastName' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[CaseManagerLastName]=["+newValue+"]");
                   dbDB.setCaseManagerLastName(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[CaseManagerLastName]=["+newValue+"]");
           dbDB.setCaseManagerLastName(newValue);
         }
    }
    public String getCaseManagerLastName(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CaseManagerLastName' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getCaseManagerLastName();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getCaseManagerLastName();
         }
        return myVal;
    }

    public void setAccountReference(String newValue)
    {
        this.setAccountReference(newValue,this.UserSecurityID);


    }
    public String getAccountReference()
    {
        return this.getAccountReference(this.UserSecurityID);
    }

    public void setAccountReference(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AccountReference' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[AccountReference]=["+newValue+"]");
                   dbDB.setAccountReference(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[AccountReference]=["+newValue+"]");
           dbDB.setAccountReference(newValue);
         }
    }
    public String getAccountReference(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AccountReference' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getAccountReference();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getAccountReference();
         }
        return myVal;
    }

    public void setControverted(String newValue)
    {
        this.setControverted(newValue,this.UserSecurityID);


    }
    public String getControverted()
    {
        return this.getControverted(this.UserSecurityID);
    }

    public void setControverted(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Controverted' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[Controverted]=["+newValue+"]");
                   dbDB.setControverted(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[Controverted]=["+newValue+"]");
           dbDB.setControverted(newValue);
         }
    }
    public String getControverted(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Controverted' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getControverted();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getControverted();
         }
        return myVal;
    }

    public void setMMI(Date newValue)
    {
        this.setMMI(newValue,this.UserSecurityID);


    }
    public Date getMMI()
    {
        return this.getMMI(this.UserSecurityID);
    }

    public void setMMI(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='MMI' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[MMI]=["+newValue+"]");
                   dbDB.setMMI(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[MMI]=["+newValue+"]");
           dbDB.setMMI(newValue);
         }
    }
    public Date getMMI(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='MMI' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getMMI();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getMMI();
         }
        return myVal;
    }

    public void setDatabaseID(String newValue)
    {
        this.setDatabaseID(newValue,this.UserSecurityID);


    }
    public String getDatabaseID()
    {
        return this.getDatabaseID(this.UserSecurityID);
    }

    public void setDatabaseID(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='DatabaseID' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[DatabaseID]=["+newValue+"]");
                   dbDB.setDatabaseID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[DatabaseID]=["+newValue+"]");
           dbDB.setDatabaseID(newValue);
         }
    }
    public String getDatabaseID(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='DatabaseID' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getDatabaseID();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getDatabaseID();
         }
        return myVal;
    }

    public void setJurisdicationState(String newValue)
    {
        this.setJurisdicationState(newValue,this.UserSecurityID);


    }
    public String getJurisdicationState()
    {
        return this.getJurisdicationState(this.UserSecurityID);
    }

    public void setJurisdicationState(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='JurisdicationState' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[JurisdicationState]=["+newValue+"]");
                   dbDB.setJurisdicationState(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[JurisdicationState]=["+newValue+"]");
           dbDB.setJurisdicationState(newValue);
         }
    }
    public String getJurisdicationState(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='JurisdicationState' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getJurisdicationState();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getJurisdicationState();
         }
        return myVal;
    }

    public void setJurisdicationZip(String newValue)
    {
        this.setJurisdicationZip(newValue,this.UserSecurityID);


    }
    public String getJurisdicationZip()
    {
        return this.getJurisdicationZip(this.UserSecurityID);
    }

    public void setJurisdicationZip(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='JurisdicationZip' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[JurisdicationZip]=["+newValue+"]");
                   dbDB.setJurisdicationZip(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[JurisdicationZip]=["+newValue+"]");
           dbDB.setJurisdicationZip(newValue);
         }
    }
    public String getJurisdicationZip(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='JurisdicationZip' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getJurisdicationZip();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getJurisdicationZip();
         }
        return myVal;
    }

    public void setAdjusterReferenceID(String newValue)
    {
        this.setAdjusterReferenceID(newValue,this.UserSecurityID);


    }
    public String getAdjusterReferenceID()
    {
        return this.getAdjusterReferenceID(this.UserSecurityID);
    }

    public void setAdjusterReferenceID(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AdjusterReferenceID' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[AdjusterReferenceID]=["+newValue+"]");
                   dbDB.setAdjusterReferenceID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[AdjusterReferenceID]=["+newValue+"]");
           dbDB.setAdjusterReferenceID(newValue);
         }
    }
    public String getAdjusterReferenceID(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AdjusterReferenceID' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getAdjusterReferenceID();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getAdjusterReferenceID();
         }
        return myVal;
    }

    public void setAdjusteFirstName(String newValue)
    {
        this.setAdjusteFirstName(newValue,this.UserSecurityID);


    }
    public String getAdjusteFirstName()
    {
        return this.getAdjusteFirstName(this.UserSecurityID);
    }

    public void setAdjusteFirstName(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AdjusteFirstName' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[AdjusteFirstName]=["+newValue+"]");
                   dbDB.setAdjusteFirstName(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[AdjusteFirstName]=["+newValue+"]");
           dbDB.setAdjusteFirstName(newValue);
         }
    }
    public String getAdjusteFirstName(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AdjusteFirstName' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getAdjusteFirstName();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getAdjusteFirstName();
         }
        return myVal;
    }

    public void setAdjusterLastName(String newValue)
    {
        this.setAdjusterLastName(newValue,this.UserSecurityID);


    }
    public String getAdjusterLastName()
    {
        return this.getAdjusterLastName(this.UserSecurityID);
    }

    public void setAdjusterLastName(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AdjusterLastName' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[AdjusterLastName]=["+newValue+"]");
                   dbDB.setAdjusterLastName(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[AdjusterLastName]=["+newValue+"]");
           dbDB.setAdjusterLastName(newValue);
         }
    }
    public String getAdjusterLastName(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AdjusterLastName' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getAdjusterLastName();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getAdjusterLastName();
         }
        return myVal;
    }

    public void setAdjusterPhoneNumber(String newValue)
    {
        this.setAdjusterPhoneNumber(newValue,this.UserSecurityID);


    }
    public String getAdjusterPhoneNumber()
    {
        return this.getAdjusterPhoneNumber(this.UserSecurityID);
    }

    public void setAdjusterPhoneNumber(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AdjusterPhoneNumber' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[AdjusterPhoneNumber]=["+newValue+"]");
                   dbDB.setAdjusterPhoneNumber(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[AdjusterPhoneNumber]=["+newValue+"]");
           dbDB.setAdjusterPhoneNumber(newValue);
         }
    }
    public String getAdjusterPhoneNumber(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AdjusterPhoneNumber' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getAdjusterPhoneNumber();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getAdjusterPhoneNumber();
         }
        return myVal;
    }

    public void setAdjusterFaxNumber(String newValue)
    {
        this.setAdjusterFaxNumber(newValue,this.UserSecurityID);


    }
    public String getAdjusterFaxNumber()
    {
        return this.getAdjusterFaxNumber(this.UserSecurityID);
    }

    public void setAdjusterFaxNumber(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AdjusterFaxNumber' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[AdjusterFaxNumber]=["+newValue+"]");
                   dbDB.setAdjusterFaxNumber(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[AdjusterFaxNumber]=["+newValue+"]");
           dbDB.setAdjusterFaxNumber(newValue);
         }
    }
    public String getAdjusterFaxNumber(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AdjusterFaxNumber' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getAdjusterFaxNumber();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getAdjusterFaxNumber();
         }
        return myVal;
    }

    public void setAdjusterEmail(String newValue)
    {
        this.setAdjusterEmail(newValue,this.UserSecurityID);


    }
    public String getAdjusterEmail()
    {
        return this.getAdjusterEmail(this.UserSecurityID);
    }

    public void setAdjusterEmail(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AdjusterEmail' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[AdjusterEmail]=["+newValue+"]");
                   dbDB.setAdjusterEmail(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[AdjusterEmail]=["+newValue+"]");
           dbDB.setAdjusterEmail(newValue);
         }
    }
    public String getAdjusterEmail(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AdjusterEmail' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getAdjusterEmail();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getAdjusterEmail();
         }
        return myVal;
    }

    public void setAdjusterCompany(String newValue)
    {
        this.setAdjusterCompany(newValue,this.UserSecurityID);


    }
    public String getAdjusterCompany()
    {
        return this.getAdjusterCompany(this.UserSecurityID);
    }

    public void setAdjusterCompany(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AdjusterCompany' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[AdjusterCompany]=["+newValue+"]");
                   dbDB.setAdjusterCompany(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[AdjusterCompany]=["+newValue+"]");
           dbDB.setAdjusterCompany(newValue);
         }
    }
    public String getAdjusterCompany(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AdjusterCompany' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getAdjusterCompany();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getAdjusterCompany();
         }
        return myVal;
    }

    public void setAdjusterAddress1(String newValue)
    {
        this.setAdjusterAddress1(newValue,this.UserSecurityID);


    }
    public String getAdjusterAddress1()
    {
        return this.getAdjusterAddress1(this.UserSecurityID);
    }

    public void setAdjusterAddress1(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AdjusterAddress1' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[AdjusterAddress1]=["+newValue+"]");
                   dbDB.setAdjusterAddress1(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[AdjusterAddress1]=["+newValue+"]");
           dbDB.setAdjusterAddress1(newValue);
         }
    }
    public String getAdjusterAddress1(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AdjusterAddress1' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getAdjusterAddress1();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getAdjusterAddress1();
         }
        return myVal;
    }

    public void setAdjusterAddress2(String newValue)
    {
        this.setAdjusterAddress2(newValue,this.UserSecurityID);


    }
    public String getAdjusterAddress2()
    {
        return this.getAdjusterAddress2(this.UserSecurityID);
    }

    public void setAdjusterAddress2(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AdjusterAddress2' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[AdjusterAddress2]=["+newValue+"]");
                   dbDB.setAdjusterAddress2(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[AdjusterAddress2]=["+newValue+"]");
           dbDB.setAdjusterAddress2(newValue);
         }
    }
    public String getAdjusterAddress2(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AdjusterAddress2' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getAdjusterAddress2();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getAdjusterAddress2();
         }
        return myVal;
    }

    public void setAdjusterCity(String newValue)
    {
        this.setAdjusterCity(newValue,this.UserSecurityID);


    }
    public String getAdjusterCity()
    {
        return this.getAdjusterCity(this.UserSecurityID);
    }

    public void setAdjusterCity(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AdjusterCity' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[AdjusterCity]=["+newValue+"]");
                   dbDB.setAdjusterCity(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[AdjusterCity]=["+newValue+"]");
           dbDB.setAdjusterCity(newValue);
         }
    }
    public String getAdjusterCity(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AdjusterCity' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getAdjusterCity();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getAdjusterCity();
         }
        return myVal;
    }

    public void setAdjusterState(String newValue)
    {
        this.setAdjusterState(newValue,this.UserSecurityID);


    }
    public String getAdjusterState()
    {
        return this.getAdjusterState(this.UserSecurityID);
    }

    public void setAdjusterState(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AdjusterState' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[AdjusterState]=["+newValue+"]");
                   dbDB.setAdjusterState(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[AdjusterState]=["+newValue+"]");
           dbDB.setAdjusterState(newValue);
         }
    }
    public String getAdjusterState(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AdjusterState' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getAdjusterState();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getAdjusterState();
         }
        return myVal;
    }

    public void setAdjusterZip(String newValue)
    {
        this.setAdjusterZip(newValue,this.UserSecurityID);


    }
    public String getAdjusterZip()
    {
        return this.getAdjusterZip(this.UserSecurityID);
    }

    public void setAdjusterZip(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AdjusterZip' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[AdjusterZip]=["+newValue+"]");
                   dbDB.setAdjusterZip(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[AdjusterZip]=["+newValue+"]");
           dbDB.setAdjusterZip(newValue);
         }
    }
    public String getAdjusterZip(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AdjusterZip' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getAdjusterZip();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getAdjusterZip();
         }
        return myVal;
    }

    public void setReferringDoctorReferenceID(String newValue)
    {
        this.setReferringDoctorReferenceID(newValue,this.UserSecurityID);


    }
    public String getReferringDoctorReferenceID()
    {
        return this.getReferringDoctorReferenceID(this.UserSecurityID);
    }

    public void setReferringDoctorReferenceID(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ReferringDoctorReferenceID' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[ReferringDoctorReferenceID]=["+newValue+"]");
                   dbDB.setReferringDoctorReferenceID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[ReferringDoctorReferenceID]=["+newValue+"]");
           dbDB.setReferringDoctorReferenceID(newValue);
         }
    }
    public String getReferringDoctorReferenceID(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ReferringDoctorReferenceID' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getReferringDoctorReferenceID();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getReferringDoctorReferenceID();
         }
        return myVal;
    }

    public void setReferringDoctorNPI(String newValue)
    {
        this.setReferringDoctorNPI(newValue,this.UserSecurityID);


    }
    public String getReferringDoctorNPI()
    {
        return this.getReferringDoctorNPI(this.UserSecurityID);
    }

    public void setReferringDoctorNPI(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ReferringDoctorNPI' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[ReferringDoctorNPI]=["+newValue+"]");
                   dbDB.setReferringDoctorNPI(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[ReferringDoctorNPI]=["+newValue+"]");
           dbDB.setReferringDoctorNPI(newValue);
         }
    }
    public String getReferringDoctorNPI(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ReferringDoctorNPI' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getReferringDoctorNPI();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getReferringDoctorNPI();
         }
        return myVal;
    }

    public void setReferringDoctorMedicalLicense(String newValue)
    {
        this.setReferringDoctorMedicalLicense(newValue,this.UserSecurityID);


    }
    public String getReferringDoctorMedicalLicense()
    {
        return this.getReferringDoctorMedicalLicense(this.UserSecurityID);
    }

    public void setReferringDoctorMedicalLicense(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ReferringDoctorMedicalLicense' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[ReferringDoctorMedicalLicense]=["+newValue+"]");
                   dbDB.setReferringDoctorMedicalLicense(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[ReferringDoctorMedicalLicense]=["+newValue+"]");
           dbDB.setReferringDoctorMedicalLicense(newValue);
         }
    }
    public String getReferringDoctorMedicalLicense(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ReferringDoctorMedicalLicense' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getReferringDoctorMedicalLicense();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getReferringDoctorMedicalLicense();
         }
        return myVal;
    }

    public void setReferringDoctorFirstName(String newValue)
    {
        this.setReferringDoctorFirstName(newValue,this.UserSecurityID);


    }
    public String getReferringDoctorFirstName()
    {
        return this.getReferringDoctorFirstName(this.UserSecurityID);
    }

    public void setReferringDoctorFirstName(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ReferringDoctorFirstName' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[ReferringDoctorFirstName]=["+newValue+"]");
                   dbDB.setReferringDoctorFirstName(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[ReferringDoctorFirstName]=["+newValue+"]");
           dbDB.setReferringDoctorFirstName(newValue);
         }
    }
    public String getReferringDoctorFirstName(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ReferringDoctorFirstName' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getReferringDoctorFirstName();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getReferringDoctorFirstName();
         }
        return myVal;
    }

    public void setReferringDoctorLastName(String newValue)
    {
        this.setReferringDoctorLastName(newValue,this.UserSecurityID);


    }
    public String getReferringDoctorLastName()
    {
        return this.getReferringDoctorLastName(this.UserSecurityID);
    }

    public void setReferringDoctorLastName(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ReferringDoctorLastName' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[ReferringDoctorLastName]=["+newValue+"]");
                   dbDB.setReferringDoctorLastName(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[ReferringDoctorLastName]=["+newValue+"]");
           dbDB.setReferringDoctorLastName(newValue);
         }
    }
    public String getReferringDoctorLastName(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ReferringDoctorLastName' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getReferringDoctorLastName();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getReferringDoctorLastName();
         }
        return myVal;
    }

    public void setReferringDoctorPhoneNumber(String newValue)
    {
        this.setReferringDoctorPhoneNumber(newValue,this.UserSecurityID);


    }
    public String getReferringDoctorPhoneNumber()
    {
        return this.getReferringDoctorPhoneNumber(this.UserSecurityID);
    }

    public void setReferringDoctorPhoneNumber(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ReferringDoctorPhoneNumber' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[ReferringDoctorPhoneNumber]=["+newValue+"]");
                   dbDB.setReferringDoctorPhoneNumber(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[ReferringDoctorPhoneNumber]=["+newValue+"]");
           dbDB.setReferringDoctorPhoneNumber(newValue);
         }
    }
    public String getReferringDoctorPhoneNumber(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ReferringDoctorPhoneNumber' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getReferringDoctorPhoneNumber();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getReferringDoctorPhoneNumber();
         }
        return myVal;
    }

    public void setReferringDoctorFaxNumber(String newValue)
    {
        this.setReferringDoctorFaxNumber(newValue,this.UserSecurityID);


    }
    public String getReferringDoctorFaxNumber()
    {
        return this.getReferringDoctorFaxNumber(this.UserSecurityID);
    }

    public void setReferringDoctorFaxNumber(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ReferringDoctorFaxNumber' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[ReferringDoctorFaxNumber]=["+newValue+"]");
                   dbDB.setReferringDoctorFaxNumber(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[ReferringDoctorFaxNumber]=["+newValue+"]");
           dbDB.setReferringDoctorFaxNumber(newValue);
         }
    }
    public String getReferringDoctorFaxNumber(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ReferringDoctorFaxNumber' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getReferringDoctorFaxNumber();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getReferringDoctorFaxNumber();
         }
        return myVal;
    }

    public void setReferringDoctorEmail(String newValue)
    {
        this.setReferringDoctorEmail(newValue,this.UserSecurityID);


    }
    public String getReferringDoctorEmail()
    {
        return this.getReferringDoctorEmail(this.UserSecurityID);
    }

    public void setReferringDoctorEmail(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ReferringDoctorEmail' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[ReferringDoctorEmail]=["+newValue+"]");
                   dbDB.setReferringDoctorEmail(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[ReferringDoctorEmail]=["+newValue+"]");
           dbDB.setReferringDoctorEmail(newValue);
         }
    }
    public String getReferringDoctorEmail(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ReferringDoctorEmail' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getReferringDoctorEmail();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getReferringDoctorEmail();
         }
        return myVal;
    }

    public void setReferringDoctorCompany(String newValue)
    {
        this.setReferringDoctorCompany(newValue,this.UserSecurityID);


    }
    public String getReferringDoctorCompany()
    {
        return this.getReferringDoctorCompany(this.UserSecurityID);
    }

    public void setReferringDoctorCompany(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ReferringDoctorCompany' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[ReferringDoctorCompany]=["+newValue+"]");
                   dbDB.setReferringDoctorCompany(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[ReferringDoctorCompany]=["+newValue+"]");
           dbDB.setReferringDoctorCompany(newValue);
         }
    }
    public String getReferringDoctorCompany(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ReferringDoctorCompany' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getReferringDoctorCompany();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getReferringDoctorCompany();
         }
        return myVal;
    }

    public void setReferringDoctorAddress1(String newValue)
    {
        this.setReferringDoctorAddress1(newValue,this.UserSecurityID);


    }
    public String getReferringDoctorAddress1()
    {
        return this.getReferringDoctorAddress1(this.UserSecurityID);
    }

    public void setReferringDoctorAddress1(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ReferringDoctorAddress1' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[ReferringDoctorAddress1]=["+newValue+"]");
                   dbDB.setReferringDoctorAddress1(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[ReferringDoctorAddress1]=["+newValue+"]");
           dbDB.setReferringDoctorAddress1(newValue);
         }
    }
    public String getReferringDoctorAddress1(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ReferringDoctorAddress1' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getReferringDoctorAddress1();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getReferringDoctorAddress1();
         }
        return myVal;
    }

    public void setReferringDoctorAddress2(String newValue)
    {
        this.setReferringDoctorAddress2(newValue,this.UserSecurityID);


    }
    public String getReferringDoctorAddress2()
    {
        return this.getReferringDoctorAddress2(this.UserSecurityID);
    }

    public void setReferringDoctorAddress2(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ReferringDoctorAddress2' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[ReferringDoctorAddress2]=["+newValue+"]");
                   dbDB.setReferringDoctorAddress2(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[ReferringDoctorAddress2]=["+newValue+"]");
           dbDB.setReferringDoctorAddress2(newValue);
         }
    }
    public String getReferringDoctorAddress2(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ReferringDoctorAddress2' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getReferringDoctorAddress2();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getReferringDoctorAddress2();
         }
        return myVal;
    }

    public void setReferringDoctorCity(String newValue)
    {
        this.setReferringDoctorCity(newValue,this.UserSecurityID);


    }
    public String getReferringDoctorCity()
    {
        return this.getReferringDoctorCity(this.UserSecurityID);
    }

    public void setReferringDoctorCity(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ReferringDoctorCity' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[ReferringDoctorCity]=["+newValue+"]");
                   dbDB.setReferringDoctorCity(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[ReferringDoctorCity]=["+newValue+"]");
           dbDB.setReferringDoctorCity(newValue);
         }
    }
    public String getReferringDoctorCity(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ReferringDoctorCity' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getReferringDoctorCity();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getReferringDoctorCity();
         }
        return myVal;
    }

    public void setReferringDoctorState(String newValue)
    {
        this.setReferringDoctorState(newValue,this.UserSecurityID);


    }
    public String getReferringDoctorState()
    {
        return this.getReferringDoctorState(this.UserSecurityID);
    }

    public void setReferringDoctorState(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ReferringDoctorState' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[ReferringDoctorState]=["+newValue+"]");
                   dbDB.setReferringDoctorState(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[ReferringDoctorState]=["+newValue+"]");
           dbDB.setReferringDoctorState(newValue);
         }
    }
    public String getReferringDoctorState(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ReferringDoctorState' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getReferringDoctorState();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getReferringDoctorState();
         }
        return myVal;
    }

    public void setReferringDoctorZip(String newValue)
    {
        this.setReferringDoctorZip(newValue,this.UserSecurityID);


    }
    public String getReferringDoctorZip()
    {
        return this.getReferringDoctorZip(this.UserSecurityID);
    }

    public void setReferringDoctorZip(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ReferringDoctorZip' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[ReferringDoctorZip]=["+newValue+"]");
                   dbDB.setReferringDoctorZip(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[ReferringDoctorZip]=["+newValue+"]");
           dbDB.setReferringDoctorZip(newValue);
         }
    }
    public String getReferringDoctorZip(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ReferringDoctorZip' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getReferringDoctorZip();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getReferringDoctorZip();
         }
        return myVal;
    }

    public void setNotes(String newValue)
    {
        this.setNotes(newValue,this.UserSecurityID);


    }
    public String getNotes()
    {
        return this.getNotes(this.UserSecurityID);
    }

    public void setNotes(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Notes' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[Notes]=["+newValue+"]");
                   dbDB.setNotes(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[Notes]=["+newValue+"]");
           dbDB.setNotes(newValue);
         }
    }
    public String getNotes(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Notes' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getNotes();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getNotes();
         }
        return myVal;
    }

    public void setLineID(Integer newValue)
    {
        this.setLineID(newValue,this.UserSecurityID);


    }
    public Integer getLineID()
    {
        return this.getLineID(this.UserSecurityID);
    }

    public void setLineID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='LineID' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[LineID]=["+newValue+"]");
                   dbDB.setLineID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[LineID]=["+newValue+"]");
           dbDB.setLineID(newValue);
         }
    }
    public Integer getLineID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='LineID' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getLineID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getLineID();
         }
        return myVal;
    }

    public void setRenderingProviderReferenceID(String newValue)
    {
        this.setRenderingProviderReferenceID(newValue,this.UserSecurityID);


    }
    public String getRenderingProviderReferenceID()
    {
        return this.getRenderingProviderReferenceID(this.UserSecurityID);
    }

    public void setRenderingProviderReferenceID(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='RenderingProviderReferenceID' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[RenderingProviderReferenceID]=["+newValue+"]");
                   dbDB.setRenderingProviderReferenceID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[RenderingProviderReferenceID]=["+newValue+"]");
           dbDB.setRenderingProviderReferenceID(newValue);
         }
    }
    public String getRenderingProviderReferenceID(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='RenderingProviderReferenceID' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getRenderingProviderReferenceID();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getRenderingProviderReferenceID();
         }
        return myVal;
    }

    public void setRenderingProviderNPI(String newValue)
    {
        this.setRenderingProviderNPI(newValue,this.UserSecurityID);


    }
    public String getRenderingProviderNPI()
    {
        return this.getRenderingProviderNPI(this.UserSecurityID);
    }

    public void setRenderingProviderNPI(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='RenderingProviderNPI' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[RenderingProviderNPI]=["+newValue+"]");
                   dbDB.setRenderingProviderNPI(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[RenderingProviderNPI]=["+newValue+"]");
           dbDB.setRenderingProviderNPI(newValue);
         }
    }
    public String getRenderingProviderNPI(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='RenderingProviderNPI' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getRenderingProviderNPI();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getRenderingProviderNPI();
         }
        return myVal;
    }

    public void setRenderingProviderTaxID(String newValue)
    {
        this.setRenderingProviderTaxID(newValue,this.UserSecurityID);


    }
    public String getRenderingProviderTaxID()
    {
        return this.getRenderingProviderTaxID(this.UserSecurityID);
    }

    public void setRenderingProviderTaxID(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='RenderingProviderTaxID' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[RenderingProviderTaxID]=["+newValue+"]");
                   dbDB.setRenderingProviderTaxID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[RenderingProviderTaxID]=["+newValue+"]");
           dbDB.setRenderingProviderTaxID(newValue);
         }
    }
    public String getRenderingProviderTaxID(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='RenderingProviderTaxID' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getRenderingProviderTaxID();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getRenderingProviderTaxID();
         }
        return myVal;
    }

    public void setRenderingProviderMedicalLicense(String newValue)
    {
        this.setRenderingProviderMedicalLicense(newValue,this.UserSecurityID);


    }
    public String getRenderingProviderMedicalLicense()
    {
        return this.getRenderingProviderMedicalLicense(this.UserSecurityID);
    }

    public void setRenderingProviderMedicalLicense(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='RenderingProviderMedicalLicense' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[RenderingProviderMedicalLicense]=["+newValue+"]");
                   dbDB.setRenderingProviderMedicalLicense(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[RenderingProviderMedicalLicense]=["+newValue+"]");
           dbDB.setRenderingProviderMedicalLicense(newValue);
         }
    }
    public String getRenderingProviderMedicalLicense(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='RenderingProviderMedicalLicense' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getRenderingProviderMedicalLicense();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getRenderingProviderMedicalLicense();
         }
        return myVal;
    }

    public void setRenderingProviderFirstName(String newValue)
    {
        this.setRenderingProviderFirstName(newValue,this.UserSecurityID);


    }
    public String getRenderingProviderFirstName()
    {
        return this.getRenderingProviderFirstName(this.UserSecurityID);
    }

    public void setRenderingProviderFirstName(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='RenderingProviderFirstName' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[RenderingProviderFirstName]=["+newValue+"]");
                   dbDB.setRenderingProviderFirstName(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[RenderingProviderFirstName]=["+newValue+"]");
           dbDB.setRenderingProviderFirstName(newValue);
         }
    }
    public String getRenderingProviderFirstName(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='RenderingProviderFirstName' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getRenderingProviderFirstName();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getRenderingProviderFirstName();
         }
        return myVal;
    }

    public void setRenderingProviderLastName(String newValue)
    {
        this.setRenderingProviderLastName(newValue,this.UserSecurityID);


    }
    public String getRenderingProviderLastName()
    {
        return this.getRenderingProviderLastName(this.UserSecurityID);
    }

    public void setRenderingProviderLastName(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='RenderingProviderLastName' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[RenderingProviderLastName]=["+newValue+"]");
                   dbDB.setRenderingProviderLastName(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[RenderingProviderLastName]=["+newValue+"]");
           dbDB.setRenderingProviderLastName(newValue);
         }
    }
    public String getRenderingProviderLastName(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='RenderingProviderLastName' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getRenderingProviderLastName();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getRenderingProviderLastName();
         }
        return myVal;
    }

    public void setRenderingProviderPhoneNumber(String newValue)
    {
        this.setRenderingProviderPhoneNumber(newValue,this.UserSecurityID);


    }
    public String getRenderingProviderPhoneNumber()
    {
        return this.getRenderingProviderPhoneNumber(this.UserSecurityID);
    }

    public void setRenderingProviderPhoneNumber(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='RenderingProviderPhoneNumber' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[RenderingProviderPhoneNumber]=["+newValue+"]");
                   dbDB.setRenderingProviderPhoneNumber(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[RenderingProviderPhoneNumber]=["+newValue+"]");
           dbDB.setRenderingProviderPhoneNumber(newValue);
         }
    }
    public String getRenderingProviderPhoneNumber(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='RenderingProviderPhoneNumber' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getRenderingProviderPhoneNumber();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getRenderingProviderPhoneNumber();
         }
        return myVal;
    }

    public void setRenderingProviderFaxNumber(String newValue)
    {
        this.setRenderingProviderFaxNumber(newValue,this.UserSecurityID);


    }
    public String getRenderingProviderFaxNumber()
    {
        return this.getRenderingProviderFaxNumber(this.UserSecurityID);
    }

    public void setRenderingProviderFaxNumber(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='RenderingProviderFaxNumber' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[RenderingProviderFaxNumber]=["+newValue+"]");
                   dbDB.setRenderingProviderFaxNumber(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[RenderingProviderFaxNumber]=["+newValue+"]");
           dbDB.setRenderingProviderFaxNumber(newValue);
         }
    }
    public String getRenderingProviderFaxNumber(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='RenderingProviderFaxNumber' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getRenderingProviderFaxNumber();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getRenderingProviderFaxNumber();
         }
        return myVal;
    }

    public void setRenderingProviderEmail(String newValue)
    {
        this.setRenderingProviderEmail(newValue,this.UserSecurityID);


    }
    public String getRenderingProviderEmail()
    {
        return this.getRenderingProviderEmail(this.UserSecurityID);
    }

    public void setRenderingProviderEmail(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='RenderingProviderEmail' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[RenderingProviderEmail]=["+newValue+"]");
                   dbDB.setRenderingProviderEmail(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[RenderingProviderEmail]=["+newValue+"]");
           dbDB.setRenderingProviderEmail(newValue);
         }
    }
    public String getRenderingProviderEmail(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='RenderingProviderEmail' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getRenderingProviderEmail();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getRenderingProviderEmail();
         }
        return myVal;
    }

    public void setRenderingProviderCompany(String newValue)
    {
        this.setRenderingProviderCompany(newValue,this.UserSecurityID);


    }
    public String getRenderingProviderCompany()
    {
        return this.getRenderingProviderCompany(this.UserSecurityID);
    }

    public void setRenderingProviderCompany(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='RenderingProviderCompany' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[RenderingProviderCompany]=["+newValue+"]");
                   dbDB.setRenderingProviderCompany(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[RenderingProviderCompany]=["+newValue+"]");
           dbDB.setRenderingProviderCompany(newValue);
         }
    }
    public String getRenderingProviderCompany(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='RenderingProviderCompany' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getRenderingProviderCompany();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getRenderingProviderCompany();
         }
        return myVal;
    }

    public void setRenderingProviderAddress1(String newValue)
    {
        this.setRenderingProviderAddress1(newValue,this.UserSecurityID);


    }
    public String getRenderingProviderAddress1()
    {
        return this.getRenderingProviderAddress1(this.UserSecurityID);
    }

    public void setRenderingProviderAddress1(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='RenderingProviderAddress1' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[RenderingProviderAddress1]=["+newValue+"]");
                   dbDB.setRenderingProviderAddress1(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[RenderingProviderAddress1]=["+newValue+"]");
           dbDB.setRenderingProviderAddress1(newValue);
         }
    }
    public String getRenderingProviderAddress1(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='RenderingProviderAddress1' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getRenderingProviderAddress1();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getRenderingProviderAddress1();
         }
        return myVal;
    }

    public void setRenderingProviderAddress2(String newValue)
    {
        this.setRenderingProviderAddress2(newValue,this.UserSecurityID);


    }
    public String getRenderingProviderAddress2()
    {
        return this.getRenderingProviderAddress2(this.UserSecurityID);
    }

    public void setRenderingProviderAddress2(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='RenderingProviderAddress2' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[RenderingProviderAddress2]=["+newValue+"]");
                   dbDB.setRenderingProviderAddress2(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[RenderingProviderAddress2]=["+newValue+"]");
           dbDB.setRenderingProviderAddress2(newValue);
         }
    }
    public String getRenderingProviderAddress2(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='RenderingProviderAddress2' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getRenderingProviderAddress2();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getRenderingProviderAddress2();
         }
        return myVal;
    }

    public void setRenderingProviderCity(String newValue)
    {
        this.setRenderingProviderCity(newValue,this.UserSecurityID);


    }
    public String getRenderingProviderCity()
    {
        return this.getRenderingProviderCity(this.UserSecurityID);
    }

    public void setRenderingProviderCity(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='RenderingProviderCity' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[RenderingProviderCity]=["+newValue+"]");
                   dbDB.setRenderingProviderCity(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[RenderingProviderCity]=["+newValue+"]");
           dbDB.setRenderingProviderCity(newValue);
         }
    }
    public String getRenderingProviderCity(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='RenderingProviderCity' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getRenderingProviderCity();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getRenderingProviderCity();
         }
        return myVal;
    }

    public void setRenderingProviderState(String newValue)
    {
        this.setRenderingProviderState(newValue,this.UserSecurityID);


    }
    public String getRenderingProviderState()
    {
        return this.getRenderingProviderState(this.UserSecurityID);
    }

    public void setRenderingProviderState(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='RenderingProviderState' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[RenderingProviderState]=["+newValue+"]");
                   dbDB.setRenderingProviderState(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[RenderingProviderState]=["+newValue+"]");
           dbDB.setRenderingProviderState(newValue);
         }
    }
    public String getRenderingProviderState(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='RenderingProviderState' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getRenderingProviderState();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getRenderingProviderState();
         }
        return myVal;
    }

    public void setRenderingProviderZip(String newValue)
    {
        this.setRenderingProviderZip(newValue,this.UserSecurityID);


    }
    public String getRenderingProviderZip()
    {
        return this.getRenderingProviderZip(this.UserSecurityID);
    }

    public void setRenderingProviderZip(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='RenderingProviderZip' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[RenderingProviderZip]=["+newValue+"]");
                   dbDB.setRenderingProviderZip(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[RenderingProviderZip]=["+newValue+"]");
           dbDB.setRenderingProviderZip(newValue);
         }
    }
    public String getRenderingProviderZip(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='RenderingProviderZip' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getRenderingProviderZip();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getRenderingProviderZip();
         }
        return myVal;
    }

    public void setBillingProviderReferenceID(String newValue)
    {
        this.setBillingProviderReferenceID(newValue,this.UserSecurityID);


    }
    public String getBillingProviderReferenceID()
    {
        return this.getBillingProviderReferenceID(this.UserSecurityID);
    }

    public void setBillingProviderReferenceID(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='BillingProviderReferenceID' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[BillingProviderReferenceID]=["+newValue+"]");
                   dbDB.setBillingProviderReferenceID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[BillingProviderReferenceID]=["+newValue+"]");
           dbDB.setBillingProviderReferenceID(newValue);
         }
    }
    public String getBillingProviderReferenceID(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='BillingProviderReferenceID' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getBillingProviderReferenceID();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getBillingProviderReferenceID();
         }
        return myVal;
    }

    public void setBillingProviderNPI(String newValue)
    {
        this.setBillingProviderNPI(newValue,this.UserSecurityID);


    }
    public String getBillingProviderNPI()
    {
        return this.getBillingProviderNPI(this.UserSecurityID);
    }

    public void setBillingProviderNPI(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='BillingProviderNPI' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[BillingProviderNPI]=["+newValue+"]");
                   dbDB.setBillingProviderNPI(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[BillingProviderNPI]=["+newValue+"]");
           dbDB.setBillingProviderNPI(newValue);
         }
    }
    public String getBillingProviderNPI(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='BillingProviderNPI' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getBillingProviderNPI();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getBillingProviderNPI();
         }
        return myVal;
    }

    public void setBillingProviderTaxID(String newValue)
    {
        this.setBillingProviderTaxID(newValue,this.UserSecurityID);


    }
    public String getBillingProviderTaxID()
    {
        return this.getBillingProviderTaxID(this.UserSecurityID);
    }

    public void setBillingProviderTaxID(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='BillingProviderTaxID' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[BillingProviderTaxID]=["+newValue+"]");
                   dbDB.setBillingProviderTaxID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[BillingProviderTaxID]=["+newValue+"]");
           dbDB.setBillingProviderTaxID(newValue);
         }
    }
    public String getBillingProviderTaxID(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='BillingProviderTaxID' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getBillingProviderTaxID();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getBillingProviderTaxID();
         }
        return myVal;
    }

    public void setBillingProviderMedicalLicense(String newValue)
    {
        this.setBillingProviderMedicalLicense(newValue,this.UserSecurityID);


    }
    public String getBillingProviderMedicalLicense()
    {
        return this.getBillingProviderMedicalLicense(this.UserSecurityID);
    }

    public void setBillingProviderMedicalLicense(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='BillingProviderMedicalLicense' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[BillingProviderMedicalLicense]=["+newValue+"]");
                   dbDB.setBillingProviderMedicalLicense(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[BillingProviderMedicalLicense]=["+newValue+"]");
           dbDB.setBillingProviderMedicalLicense(newValue);
         }
    }
    public String getBillingProviderMedicalLicense(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='BillingProviderMedicalLicense' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getBillingProviderMedicalLicense();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getBillingProviderMedicalLicense();
         }
        return myVal;
    }

    public void setBillingProviderFirstName(String newValue)
    {
        this.setBillingProviderFirstName(newValue,this.UserSecurityID);


    }
    public String getBillingProviderFirstName()
    {
        return this.getBillingProviderFirstName(this.UserSecurityID);
    }

    public void setBillingProviderFirstName(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='BillingProviderFirstName' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[BillingProviderFirstName]=["+newValue+"]");
                   dbDB.setBillingProviderFirstName(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[BillingProviderFirstName]=["+newValue+"]");
           dbDB.setBillingProviderFirstName(newValue);
         }
    }
    public String getBillingProviderFirstName(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='BillingProviderFirstName' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getBillingProviderFirstName();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getBillingProviderFirstName();
         }
        return myVal;
    }

    public void setBillingProviderLastName(String newValue)
    {
        this.setBillingProviderLastName(newValue,this.UserSecurityID);


    }
    public String getBillingProviderLastName()
    {
        return this.getBillingProviderLastName(this.UserSecurityID);
    }

    public void setBillingProviderLastName(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='BillingProviderLastName' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[BillingProviderLastName]=["+newValue+"]");
                   dbDB.setBillingProviderLastName(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[BillingProviderLastName]=["+newValue+"]");
           dbDB.setBillingProviderLastName(newValue);
         }
    }
    public String getBillingProviderLastName(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='BillingProviderLastName' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getBillingProviderLastName();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getBillingProviderLastName();
         }
        return myVal;
    }

    public void setBillingProviderPhoneNumber(String newValue)
    {
        this.setBillingProviderPhoneNumber(newValue,this.UserSecurityID);


    }
    public String getBillingProviderPhoneNumber()
    {
        return this.getBillingProviderPhoneNumber(this.UserSecurityID);
    }

    public void setBillingProviderPhoneNumber(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='BillingProviderPhoneNumber' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[BillingProviderPhoneNumber]=["+newValue+"]");
                   dbDB.setBillingProviderPhoneNumber(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[BillingProviderPhoneNumber]=["+newValue+"]");
           dbDB.setBillingProviderPhoneNumber(newValue);
         }
    }
    public String getBillingProviderPhoneNumber(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='BillingProviderPhoneNumber' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getBillingProviderPhoneNumber();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getBillingProviderPhoneNumber();
         }
        return myVal;
    }

    public void setBillingProviderFaxNumber(String newValue)
    {
        this.setBillingProviderFaxNumber(newValue,this.UserSecurityID);


    }
    public String getBillingProviderFaxNumber()
    {
        return this.getBillingProviderFaxNumber(this.UserSecurityID);
    }

    public void setBillingProviderFaxNumber(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='BillingProviderFaxNumber' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[BillingProviderFaxNumber]=["+newValue+"]");
                   dbDB.setBillingProviderFaxNumber(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[BillingProviderFaxNumber]=["+newValue+"]");
           dbDB.setBillingProviderFaxNumber(newValue);
         }
    }
    public String getBillingProviderFaxNumber(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='BillingProviderFaxNumber' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getBillingProviderFaxNumber();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getBillingProviderFaxNumber();
         }
        return myVal;
    }

    public void setBillingProviderEmail(String newValue)
    {
        this.setBillingProviderEmail(newValue,this.UserSecurityID);


    }
    public String getBillingProviderEmail()
    {
        return this.getBillingProviderEmail(this.UserSecurityID);
    }

    public void setBillingProviderEmail(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='BillingProviderEmail' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[BillingProviderEmail]=["+newValue+"]");
                   dbDB.setBillingProviderEmail(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[BillingProviderEmail]=["+newValue+"]");
           dbDB.setBillingProviderEmail(newValue);
         }
    }
    public String getBillingProviderEmail(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='BillingProviderEmail' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getBillingProviderEmail();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getBillingProviderEmail();
         }
        return myVal;
    }

    public void setBillingProviderCompany(String newValue)
    {
        this.setBillingProviderCompany(newValue,this.UserSecurityID);


    }
    public String getBillingProviderCompany()
    {
        return this.getBillingProviderCompany(this.UserSecurityID);
    }

    public void setBillingProviderCompany(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='BillingProviderCompany' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[BillingProviderCompany]=["+newValue+"]");
                   dbDB.setBillingProviderCompany(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[BillingProviderCompany]=["+newValue+"]");
           dbDB.setBillingProviderCompany(newValue);
         }
    }
    public String getBillingProviderCompany(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='BillingProviderCompany' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getBillingProviderCompany();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getBillingProviderCompany();
         }
        return myVal;
    }

    public void setBillingProviderAddress1(String newValue)
    {
        this.setBillingProviderAddress1(newValue,this.UserSecurityID);


    }
    public String getBillingProviderAddress1()
    {
        return this.getBillingProviderAddress1(this.UserSecurityID);
    }

    public void setBillingProviderAddress1(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='BillingProviderAddress1' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[BillingProviderAddress1]=["+newValue+"]");
                   dbDB.setBillingProviderAddress1(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[BillingProviderAddress1]=["+newValue+"]");
           dbDB.setBillingProviderAddress1(newValue);
         }
    }
    public String getBillingProviderAddress1(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='BillingProviderAddress1' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getBillingProviderAddress1();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getBillingProviderAddress1();
         }
        return myVal;
    }

    public void setBillingProviderAddress2(String newValue)
    {
        this.setBillingProviderAddress2(newValue,this.UserSecurityID);


    }
    public String getBillingProviderAddress2()
    {
        return this.getBillingProviderAddress2(this.UserSecurityID);
    }

    public void setBillingProviderAddress2(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='BillingProviderAddress2' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[BillingProviderAddress2]=["+newValue+"]");
                   dbDB.setBillingProviderAddress2(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[BillingProviderAddress2]=["+newValue+"]");
           dbDB.setBillingProviderAddress2(newValue);
         }
    }
    public String getBillingProviderAddress2(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='BillingProviderAddress2' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getBillingProviderAddress2();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getBillingProviderAddress2();
         }
        return myVal;
    }

    public void setBillingProviderCity(String newValue)
    {
        this.setBillingProviderCity(newValue,this.UserSecurityID);


    }
    public String getBillingProviderCity()
    {
        return this.getBillingProviderCity(this.UserSecurityID);
    }

    public void setBillingProviderCity(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='BillingProviderCity' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[BillingProviderCity]=["+newValue+"]");
                   dbDB.setBillingProviderCity(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[BillingProviderCity]=["+newValue+"]");
           dbDB.setBillingProviderCity(newValue);
         }
    }
    public String getBillingProviderCity(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='BillingProviderCity' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getBillingProviderCity();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getBillingProviderCity();
         }
        return myVal;
    }

    public void setBillingProviderState(String newValue)
    {
        this.setBillingProviderState(newValue,this.UserSecurityID);


    }
    public String getBillingProviderState()
    {
        return this.getBillingProviderState(this.UserSecurityID);
    }

    public void setBillingProviderState(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='BillingProviderState' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[BillingProviderState]=["+newValue+"]");
                   dbDB.setBillingProviderState(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[BillingProviderState]=["+newValue+"]");
           dbDB.setBillingProviderState(newValue);
         }
    }
    public String getBillingProviderState(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='BillingProviderState' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getBillingProviderState();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getBillingProviderState();
         }
        return myVal;
    }

    public void setBillingProviderZip(String newValue)
    {
        this.setBillingProviderZip(newValue,this.UserSecurityID);


    }
    public String getBillingProviderZip()
    {
        return this.getBillingProviderZip(this.UserSecurityID);
    }

    public void setBillingProviderZip(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='BillingProviderZip' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[BillingProviderZip]=["+newValue+"]");
                   dbDB.setBillingProviderZip(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[BillingProviderZip]=["+newValue+"]");
           dbDB.setBillingProviderZip(newValue);
         }
    }
    public String getBillingProviderZip(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='BillingProviderZip' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getBillingProviderZip();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getBillingProviderZip();
         }
        return myVal;
    }

    public void setNetwork_UserField1(String newValue)
    {
        this.setNetwork_UserField1(newValue,this.UserSecurityID);


    }
    public String getNetwork_UserField1()
    {
        return this.getNetwork_UserField1(this.UserSecurityID);
    }

    public void setNetwork_UserField1(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Network_UserField1' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[Network_UserField1]=["+newValue+"]");
                   dbDB.setNetwork_UserField1(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[Network_UserField1]=["+newValue+"]");
           dbDB.setNetwork_UserField1(newValue);
         }
    }
    public String getNetwork_UserField1(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Network_UserField1' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getNetwork_UserField1();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getNetwork_UserField1();
         }
        return myVal;
    }

    public void setNetwork_UserField2(String newValue)
    {
        this.setNetwork_UserField2(newValue,this.UserSecurityID);


    }
    public String getNetwork_UserField2()
    {
        return this.getNetwork_UserField2(this.UserSecurityID);
    }

    public void setNetwork_UserField2(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Network_UserField2' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[Network_UserField2]=["+newValue+"]");
                   dbDB.setNetwork_UserField2(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[Network_UserField2]=["+newValue+"]");
           dbDB.setNetwork_UserField2(newValue);
         }
    }
    public String getNetwork_UserField2(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Network_UserField2' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getNetwork_UserField2();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getNetwork_UserField2();
         }
        return myVal;
    }

    public void setNetwork_UserField3(String newValue)
    {
        this.setNetwork_UserField3(newValue,this.UserSecurityID);


    }
    public String getNetwork_UserField3()
    {
        return this.getNetwork_UserField3(this.UserSecurityID);
    }

    public void setNetwork_UserField3(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Network_UserField3' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[Network_UserField3]=["+newValue+"]");
                   dbDB.setNetwork_UserField3(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[Network_UserField3]=["+newValue+"]");
           dbDB.setNetwork_UserField3(newValue);
         }
    }
    public String getNetwork_UserField3(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Network_UserField3' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getNetwork_UserField3();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getNetwork_UserField3();
         }
        return myVal;
    }

    public void setNetwork_UserField4(String newValue)
    {
        this.setNetwork_UserField4(newValue,this.UserSecurityID);


    }
    public String getNetwork_UserField4()
    {
        return this.getNetwork_UserField4(this.UserSecurityID);
    }

    public void setNetwork_UserField4(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Network_UserField4' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[Network_UserField4]=["+newValue+"]");
                   dbDB.setNetwork_UserField4(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[Network_UserField4]=["+newValue+"]");
           dbDB.setNetwork_UserField4(newValue);
         }
    }
    public String getNetwork_UserField4(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Network_UserField4' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getNetwork_UserField4();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getNetwork_UserField4();
         }
        return myVal;
    }

    public void setNetwork_UserField5(String newValue)
    {
        this.setNetwork_UserField5(newValue,this.UserSecurityID);


    }
    public String getNetwork_UserField5()
    {
        return this.getNetwork_UserField5(this.UserSecurityID);
    }

    public void setNetwork_UserField5(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Network_UserField5' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[Network_UserField5]=["+newValue+"]");
                   dbDB.setNetwork_UserField5(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[Network_UserField5]=["+newValue+"]");
           dbDB.setNetwork_UserField5(newValue);
         }
    }
    public String getNetwork_UserField5(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Network_UserField5' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getNetwork_UserField5();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getNetwork_UserField5();
         }
        return myVal;
    }

    public void setNIM_ProviderReferenceID(String newValue)
    {
        this.setNIM_ProviderReferenceID(newValue,this.UserSecurityID);


    }
    public String getNIM_ProviderReferenceID()
    {
        return this.getNIM_ProviderReferenceID(this.UserSecurityID);
    }

    public void setNIM_ProviderReferenceID(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='NIM_ProviderReferenceID' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[NIM_ProviderReferenceID]=["+newValue+"]");
                   dbDB.setNIM_ProviderReferenceID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[NIM_ProviderReferenceID]=["+newValue+"]");
           dbDB.setNIM_ProviderReferenceID(newValue);
         }
    }
    public String getNIM_ProviderReferenceID(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='NIM_ProviderReferenceID' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getNIM_ProviderReferenceID();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getNIM_ProviderReferenceID();
         }
        return myVal;
    }

    public void setNIM_ProviderNPI(String newValue)
    {
        this.setNIM_ProviderNPI(newValue,this.UserSecurityID);


    }
    public String getNIM_ProviderNPI()
    {
        return this.getNIM_ProviderNPI(this.UserSecurityID);
    }

    public void setNIM_ProviderNPI(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='NIM_ProviderNPI' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[NIM_ProviderNPI]=["+newValue+"]");
                   dbDB.setNIM_ProviderNPI(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[NIM_ProviderNPI]=["+newValue+"]");
           dbDB.setNIM_ProviderNPI(newValue);
         }
    }
    public String getNIM_ProviderNPI(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='NIM_ProviderNPI' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getNIM_ProviderNPI();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getNIM_ProviderNPI();
         }
        return myVal;
    }

    public void setNIM_ProviderTaxID(String newValue)
    {
        this.setNIM_ProviderTaxID(newValue,this.UserSecurityID);


    }
    public String getNIM_ProviderTaxID()
    {
        return this.getNIM_ProviderTaxID(this.UserSecurityID);
    }

    public void setNIM_ProviderTaxID(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='NIM_ProviderTaxID' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[NIM_ProviderTaxID]=["+newValue+"]");
                   dbDB.setNIM_ProviderTaxID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[NIM_ProviderTaxID]=["+newValue+"]");
           dbDB.setNIM_ProviderTaxID(newValue);
         }
    }
    public String getNIM_ProviderTaxID(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='NIM_ProviderTaxID' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getNIM_ProviderTaxID();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getNIM_ProviderTaxID();
         }
        return myVal;
    }

    public void setNIM_ProviderMedicalLicense(String newValue)
    {
        this.setNIM_ProviderMedicalLicense(newValue,this.UserSecurityID);


    }
    public String getNIM_ProviderMedicalLicense()
    {
        return this.getNIM_ProviderMedicalLicense(this.UserSecurityID);
    }

    public void setNIM_ProviderMedicalLicense(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='NIM_ProviderMedicalLicense' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[NIM_ProviderMedicalLicense]=["+newValue+"]");
                   dbDB.setNIM_ProviderMedicalLicense(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[NIM_ProviderMedicalLicense]=["+newValue+"]");
           dbDB.setNIM_ProviderMedicalLicense(newValue);
         }
    }
    public String getNIM_ProviderMedicalLicense(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='NIM_ProviderMedicalLicense' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getNIM_ProviderMedicalLicense();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getNIM_ProviderMedicalLicense();
         }
        return myVal;
    }

    public void setNIM_ProviderFirstName(String newValue)
    {
        this.setNIM_ProviderFirstName(newValue,this.UserSecurityID);


    }
    public String getNIM_ProviderFirstName()
    {
        return this.getNIM_ProviderFirstName(this.UserSecurityID);
    }

    public void setNIM_ProviderFirstName(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='NIM_ProviderFirstName' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[NIM_ProviderFirstName]=["+newValue+"]");
                   dbDB.setNIM_ProviderFirstName(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[NIM_ProviderFirstName]=["+newValue+"]");
           dbDB.setNIM_ProviderFirstName(newValue);
         }
    }
    public String getNIM_ProviderFirstName(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='NIM_ProviderFirstName' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getNIM_ProviderFirstName();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getNIM_ProviderFirstName();
         }
        return myVal;
    }

    public void setNIM_ProviderLastName(String newValue)
    {
        this.setNIM_ProviderLastName(newValue,this.UserSecurityID);


    }
    public String getNIM_ProviderLastName()
    {
        return this.getNIM_ProviderLastName(this.UserSecurityID);
    }

    public void setNIM_ProviderLastName(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='NIM_ProviderLastName' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[NIM_ProviderLastName]=["+newValue+"]");
                   dbDB.setNIM_ProviderLastName(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[NIM_ProviderLastName]=["+newValue+"]");
           dbDB.setNIM_ProviderLastName(newValue);
         }
    }
    public String getNIM_ProviderLastName(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='NIM_ProviderLastName' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getNIM_ProviderLastName();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getNIM_ProviderLastName();
         }
        return myVal;
    }

    public void setNIM_ProviderPhoneNumber(String newValue)
    {
        this.setNIM_ProviderPhoneNumber(newValue,this.UserSecurityID);


    }
    public String getNIM_ProviderPhoneNumber()
    {
        return this.getNIM_ProviderPhoneNumber(this.UserSecurityID);
    }

    public void setNIM_ProviderPhoneNumber(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='NIM_ProviderPhoneNumber' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[NIM_ProviderPhoneNumber]=["+newValue+"]");
                   dbDB.setNIM_ProviderPhoneNumber(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[NIM_ProviderPhoneNumber]=["+newValue+"]");
           dbDB.setNIM_ProviderPhoneNumber(newValue);
         }
    }
    public String getNIM_ProviderPhoneNumber(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='NIM_ProviderPhoneNumber' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getNIM_ProviderPhoneNumber();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getNIM_ProviderPhoneNumber();
         }
        return myVal;
    }

    public void setNIM_ProviderFaxNumber(String newValue)
    {
        this.setNIM_ProviderFaxNumber(newValue,this.UserSecurityID);


    }
    public String getNIM_ProviderFaxNumber()
    {
        return this.getNIM_ProviderFaxNumber(this.UserSecurityID);
    }

    public void setNIM_ProviderFaxNumber(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='NIM_ProviderFaxNumber' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[NIM_ProviderFaxNumber]=["+newValue+"]");
                   dbDB.setNIM_ProviderFaxNumber(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[NIM_ProviderFaxNumber]=["+newValue+"]");
           dbDB.setNIM_ProviderFaxNumber(newValue);
         }
    }
    public String getNIM_ProviderFaxNumber(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='NIM_ProviderFaxNumber' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getNIM_ProviderFaxNumber();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getNIM_ProviderFaxNumber();
         }
        return myVal;
    }

    public void setNIM_ProviderEmail(String newValue)
    {
        this.setNIM_ProviderEmail(newValue,this.UserSecurityID);


    }
    public String getNIM_ProviderEmail()
    {
        return this.getNIM_ProviderEmail(this.UserSecurityID);
    }

    public void setNIM_ProviderEmail(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='NIM_ProviderEmail' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[NIM_ProviderEmail]=["+newValue+"]");
                   dbDB.setNIM_ProviderEmail(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[NIM_ProviderEmail]=["+newValue+"]");
           dbDB.setNIM_ProviderEmail(newValue);
         }
    }
    public String getNIM_ProviderEmail(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='NIM_ProviderEmail' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getNIM_ProviderEmail();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getNIM_ProviderEmail();
         }
        return myVal;
    }

    public void setNIM_ProviderCompany(String newValue)
    {
        this.setNIM_ProviderCompany(newValue,this.UserSecurityID);


    }
    public String getNIM_ProviderCompany()
    {
        return this.getNIM_ProviderCompany(this.UserSecurityID);
    }

    public void setNIM_ProviderCompany(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='NIM_ProviderCompany' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[NIM_ProviderCompany]=["+newValue+"]");
                   dbDB.setNIM_ProviderCompany(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[NIM_ProviderCompany]=["+newValue+"]");
           dbDB.setNIM_ProviderCompany(newValue);
         }
    }
    public String getNIM_ProviderCompany(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='NIM_ProviderCompany' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getNIM_ProviderCompany();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getNIM_ProviderCompany();
         }
        return myVal;
    }

    public void setNIM_ProviderAddress1(String newValue)
    {
        this.setNIM_ProviderAddress1(newValue,this.UserSecurityID);


    }
    public String getNIM_ProviderAddress1()
    {
        return this.getNIM_ProviderAddress1(this.UserSecurityID);
    }

    public void setNIM_ProviderAddress1(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='NIM_ProviderAddress1' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[NIM_ProviderAddress1]=["+newValue+"]");
                   dbDB.setNIM_ProviderAddress1(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[NIM_ProviderAddress1]=["+newValue+"]");
           dbDB.setNIM_ProviderAddress1(newValue);
         }
    }
    public String getNIM_ProviderAddress1(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='NIM_ProviderAddress1' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getNIM_ProviderAddress1();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getNIM_ProviderAddress1();
         }
        return myVal;
    }

    public void setNIM_ProviderAddress2(String newValue)
    {
        this.setNIM_ProviderAddress2(newValue,this.UserSecurityID);


    }
    public String getNIM_ProviderAddress2()
    {
        return this.getNIM_ProviderAddress2(this.UserSecurityID);
    }

    public void setNIM_ProviderAddress2(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='NIM_ProviderAddress2' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[NIM_ProviderAddress2]=["+newValue+"]");
                   dbDB.setNIM_ProviderAddress2(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[NIM_ProviderAddress2]=["+newValue+"]");
           dbDB.setNIM_ProviderAddress2(newValue);
         }
    }
    public String getNIM_ProviderAddress2(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='NIM_ProviderAddress2' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getNIM_ProviderAddress2();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getNIM_ProviderAddress2();
         }
        return myVal;
    }

    public void setNIM_ProviderCity(String newValue)
    {
        this.setNIM_ProviderCity(newValue,this.UserSecurityID);


    }
    public String getNIM_ProviderCity()
    {
        return this.getNIM_ProviderCity(this.UserSecurityID);
    }

    public void setNIM_ProviderCity(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='NIM_ProviderCity' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[NIM_ProviderCity]=["+newValue+"]");
                   dbDB.setNIM_ProviderCity(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[NIM_ProviderCity]=["+newValue+"]");
           dbDB.setNIM_ProviderCity(newValue);
         }
    }
    public String getNIM_ProviderCity(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='NIM_ProviderCity' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getNIM_ProviderCity();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getNIM_ProviderCity();
         }
        return myVal;
    }

    public void setNIM_ProviderState(String newValue)
    {
        this.setNIM_ProviderState(newValue,this.UserSecurityID);


    }
    public String getNIM_ProviderState()
    {
        return this.getNIM_ProviderState(this.UserSecurityID);
    }

    public void setNIM_ProviderState(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='NIM_ProviderState' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[NIM_ProviderState]=["+newValue+"]");
                   dbDB.setNIM_ProviderState(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[NIM_ProviderState]=["+newValue+"]");
           dbDB.setNIM_ProviderState(newValue);
         }
    }
    public String getNIM_ProviderState(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='NIM_ProviderState' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getNIM_ProviderState();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getNIM_ProviderState();
         }
        return myVal;
    }

    public void setNIM_ProviderZip(String newValue)
    {
        this.setNIM_ProviderZip(newValue,this.UserSecurityID);


    }
    public String getNIM_ProviderZip()
    {
        return this.getNIM_ProviderZip(this.UserSecurityID);
    }

    public void setNIM_ProviderZip(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='NIM_ProviderZip' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[NIM_ProviderZip]=["+newValue+"]");
                   dbDB.setNIM_ProviderZip(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[NIM_ProviderZip]=["+newValue+"]");
           dbDB.setNIM_ProviderZip(newValue);
         }
    }
    public String getNIM_ProviderZip(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='NIM_ProviderZip' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getNIM_ProviderZip();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getNIM_ProviderZip();
         }
        return myVal;
    }

    public void setNIM_ProviderMatchFound(Integer newValue)
    {
        this.setNIM_ProviderMatchFound(newValue,this.UserSecurityID);


    }
    public Integer getNIM_ProviderMatchFound()
    {
        return this.getNIM_ProviderMatchFound(this.UserSecurityID);
    }

    public void setNIM_ProviderMatchFound(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='NIM_ProviderMatchFound' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[NIM_ProviderMatchFound]=["+newValue+"]");
                   dbDB.setNIM_ProviderMatchFound(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[NIM_ProviderMatchFound]=["+newValue+"]");
           dbDB.setNIM_ProviderMatchFound(newValue);
         }
    }
    public Integer getNIM_ProviderMatchFound(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='NIM_ProviderMatchFound' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getNIM_ProviderMatchFound();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getNIM_ProviderMatchFound();
         }
        return myVal;
    }

    public void setNIM_ProviderMatchAcceptsRetro(Integer newValue)
    {
        this.setNIM_ProviderMatchAcceptsRetro(newValue,this.UserSecurityID);


    }
    public Integer getNIM_ProviderMatchAcceptsRetro()
    {
        return this.getNIM_ProviderMatchAcceptsRetro(this.UserSecurityID);
    }

    public void setNIM_ProviderMatchAcceptsRetro(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='NIM_ProviderMatchAcceptsRetro' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[NIM_ProviderMatchAcceptsRetro]=["+newValue+"]");
                   dbDB.setNIM_ProviderMatchAcceptsRetro(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[NIM_ProviderMatchAcceptsRetro]=["+newValue+"]");
           dbDB.setNIM_ProviderMatchAcceptsRetro(newValue);
         }
    }
    public Integer getNIM_ProviderMatchAcceptsRetro(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='NIM_ProviderMatchAcceptsRetro' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getNIM_ProviderMatchAcceptsRetro();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getNIM_ProviderMatchAcceptsRetro();
         }
        return myVal;
    }

    public void setNIM_ProviderMatchPositiveSavings(Integer newValue)
    {
        this.setNIM_ProviderMatchPositiveSavings(newValue,this.UserSecurityID);


    }
    public Integer getNIM_ProviderMatchPositiveSavings()
    {
        return this.getNIM_ProviderMatchPositiveSavings(this.UserSecurityID);
    }

    public void setNIM_ProviderMatchPositiveSavings(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='NIM_ProviderMatchPositiveSavings' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[NIM_ProviderMatchPositiveSavings]=["+newValue+"]");
                   dbDB.setNIM_ProviderMatchPositiveSavings(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[NIM_ProviderMatchPositiveSavings]=["+newValue+"]");
           dbDB.setNIM_ProviderMatchPositiveSavings(newValue);
         }
    }
    public Integer getNIM_ProviderMatchPositiveSavings(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='NIM_ProviderMatchPositiveSavings' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getNIM_ProviderMatchPositiveSavings();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getNIM_ProviderMatchPositiveSavings();
         }
        return myVal;
    }

    public void setNIM_AcceptClaim(Integer newValue)
    {
        this.setNIM_AcceptClaim(newValue,this.UserSecurityID);


    }
    public Integer getNIM_AcceptClaim()
    {
        return this.getNIM_AcceptClaim(this.UserSecurityID);
    }

    public void setNIM_AcceptClaim(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='NIM_AcceptClaim' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[NIM_AcceptClaim]=["+newValue+"]");
                   dbDB.setNIM_AcceptClaim(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[NIM_AcceptClaim]=["+newValue+"]");
           dbDB.setNIM_AcceptClaim(newValue);
         }
    }
    public Integer getNIM_AcceptClaim(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='NIM_AcceptClaim' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getNIM_AcceptClaim();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getNIM_AcceptClaim();
         }
        return myVal;
    }

    public void setNIM_DateAccepted(Date newValue)
    {
        this.setNIM_DateAccepted(newValue,this.UserSecurityID);


    }
    public Date getNIM_DateAccepted()
    {
        return this.getNIM_DateAccepted(this.UserSecurityID);
    }

    public void setNIM_DateAccepted(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='NIM_DateAccepted' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[NIM_DateAccepted]=["+newValue+"]");
                   dbDB.setNIM_DateAccepted(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[NIM_DateAccepted]=["+newValue+"]");
           dbDB.setNIM_DateAccepted(newValue);
         }
    }
    public Date getNIM_DateAccepted(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='NIM_DateAccepted' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getNIM_DateAccepted();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getNIM_DateAccepted();
         }
        return myVal;
    }

    public void setNIM_UserAcceptedID(Integer newValue)
    {
        this.setNIM_UserAcceptedID(newValue,this.UserSecurityID);


    }
    public Integer getNIM_UserAcceptedID()
    {
        return this.getNIM_UserAcceptedID(this.UserSecurityID);
    }

    public void setNIM_UserAcceptedID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='NIM_UserAcceptedID' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[NIM_UserAcceptedID]=["+newValue+"]");
                   dbDB.setNIM_UserAcceptedID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[NIM_UserAcceptedID]=["+newValue+"]");
           dbDB.setNIM_UserAcceptedID(newValue);
         }
    }
    public Integer getNIM_UserAcceptedID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='NIM_UserAcceptedID' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getNIM_UserAcceptedID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getNIM_UserAcceptedID();
         }
        return myVal;
    }

    public void setNIM_DateExported(Date newValue)
    {
        this.setNIM_DateExported(newValue,this.UserSecurityID);


    }
    public Date getNIM_DateExported()
    {
        return this.getNIM_DateExported(this.UserSecurityID);
    }

    public void setNIM_DateExported(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='NIM_DateExported' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[NIM_DateExported]=["+newValue+"]");
                   dbDB.setNIM_DateExported(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[NIM_DateExported]=["+newValue+"]");
           dbDB.setNIM_DateExported(newValue);
         }
    }
    public Date getNIM_DateExported(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='NIM_DateExported' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getNIM_DateExported();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getNIM_DateExported();
         }
        return myVal;
    }

    public void setNIM_EncounterID(Integer newValue)
    {
        this.setNIM_EncounterID(newValue,this.UserSecurityID);


    }
    public Integer getNIM_EncounterID()
    {
        return this.getNIM_EncounterID(this.UserSecurityID);
    }

    public void setNIM_EncounterID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='NIM_EncounterID' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[NIM_EncounterID]=["+newValue+"]");
                   dbDB.setNIM_EncounterID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[NIM_EncounterID]=["+newValue+"]");
           dbDB.setNIM_EncounterID(newValue);
         }
    }
    public Integer getNIM_EncounterID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='NIM_EncounterID' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getNIM_EncounterID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getNIM_EncounterID();
         }
        return myVal;
    }

    public void setNIM_ContractProviderCost(Double newValue)
    {
        this.setNIM_ContractProviderCost(newValue,this.UserSecurityID);


    }
    public Double getNIM_ContractProviderCost()
    {
        return this.getNIM_ContractProviderCost(this.UserSecurityID);
    }

    public void setNIM_ContractProviderCost(Double newValue, Integer GroupRefID)
    {
        Double myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='NIM_ContractProviderCost' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[NIM_ContractProviderCost]=["+newValue+"]");
                   dbDB.setNIM_ContractProviderCost(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[NIM_ContractProviderCost]=["+newValue+"]");
           dbDB.setNIM_ContractProviderCost(newValue);
         }
    }
    public Double getNIM_ContractProviderCost(Integer GroupRefID)
    {
        Double myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='NIM_ContractProviderCost' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getNIM_ContractProviderCost();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getNIM_ContractProviderCost();
         }
        return myVal;
    }

    public void setNIM_FinalProviderCost(Double newValue)
    {
        this.setNIM_FinalProviderCost(newValue,this.UserSecurityID);


    }
    public Double getNIM_FinalProviderCost()
    {
        return this.getNIM_FinalProviderCost(this.UserSecurityID);
    }

    public void setNIM_FinalProviderCost(Double newValue, Integer GroupRefID)
    {
        Double myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='NIM_FinalProviderCost' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[NIM_FinalProviderCost]=["+newValue+"]");
                   dbDB.setNIM_FinalProviderCost(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[NIM_FinalProviderCost]=["+newValue+"]");
           dbDB.setNIM_FinalProviderCost(newValue);
         }
    }
    public Double getNIM_FinalProviderCost(Integer GroupRefID)
    {
        Double myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='NIM_FinalProviderCost' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getNIM_FinalProviderCost();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getNIM_FinalProviderCost();
         }
        return myVal;
    }

    public void setNIM_PayerAllow(Double newValue)
    {
        this.setNIM_PayerAllow(newValue,this.UserSecurityID);


    }
    public Double getNIM_PayerAllow()
    {
        return this.getNIM_PayerAllow(this.UserSecurityID);
    }

    public void setNIM_PayerAllow(Double newValue, Integer GroupRefID)
    {
        Double myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='NIM_PayerAllow' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[NIM_PayerAllow]=["+newValue+"]");
                   dbDB.setNIM_PayerAllow(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[NIM_PayerAllow]=["+newValue+"]");
           dbDB.setNIM_PayerAllow(newValue);
         }
    }
    public Double getNIM_PayerAllow(Integer GroupRefID)
    {
        Double myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='NIM_PayerAllow' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getNIM_PayerAllow();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getNIM_PayerAllow();
         }
        return myVal;
    }

    public void setRequiredFieldList(java.util.Hashtable requiredFieldList)
	 {

        this.requiredFieldList = requiredFieldList;
    }

    public java.util.Hashtable getRequiredFieldList()
    {
        return this.requiredFieldList;
    }
    public void setExpiredFieldList(java.util.Hashtable expiredFieldList)
    {
        this.expiredFieldList = expiredFieldList;
    }

    public void setEnglishFieldList(java.util.Hashtable englishFieldList)
    {
        this.englishFieldList = englishFieldList;
    }

    public java.util.Hashtable getExpiredFieldList()
    {
        return this.expiredFieldList;
    }
    public boolean isExpired(java.util.Date myDate, int daysVector)
    {
       return isExpired(myDate,new java.util.Date(),daysVector);
    }


    public boolean isRead(String fieldN, Integer GroupRefID)
    {
        boolean myVal = false;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='"+fieldN+"' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==1)
               {
                   myVal = true;
               }
            }
        }
        else
        {

           if (iSecurityCheck.intValue()>=1)
           {
                  myVal = true;
           }
        }
        return myVal;
    }


    public boolean isWrite(String fieldN, Integer GroupRefID)
    {
        boolean myVal = false;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='"+fieldN+"' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {
                   myVal = true;
               }
            }
        }
        else
        {

           if (iSecurityCheck.intValue()>=2)
           {
                  myVal = true;
           }
        }
        return myVal;
    }


    public boolean isExpired(java.util.Date myDate, java.util.Date refDate,int daysVector)
    {
        java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat("yyyy-MM-dd");
        boolean myVal = true;
        try
        {
            if (myDate.after(dbdf.parse("1800-01-10")))
            {
                if (myDate.after(dateChange(refDate,daysVector)))
                {
                    myVal=false;
                }
            }
            else
            {
                myVal = false;
            }
        }
        catch (Exception Datee)
        {
        }
        return myVal;
    }

    public java.util.Date dateChange(java.util.Date myD, int dayChange)
    {
        java.util.Calendar calendar = java.util.Calendar.getInstance();
        calendar.setTime(myD);
        calendar.add(java.util.Calendar.DATE, dayChange);
        java.util.Date resultAsDate = new java.util.Date(calendar.getTime().getTime());
        return resultAsDate;
    }

    public boolean isRequired(String fieldN)
    {
        return isRequired(fieldN, new Integer("1"));
    }

    public boolean isRequired(String fieldN, Integer GroupRefID)
    {
        boolean myVal = false;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='"+fieldN+"' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getRequiredID().intValue()==1)
               {
                   myVal = true;
               }
            }
        }
        else
        {
            if (this.getRequiredFieldList().containsKey(fieldN))
            {
                myVal = true;
            }
        }
        return myVal;
    }
    public boolean isExpiredCheck(String fieldN)
    {
        return isExpiredCheck(fieldN, new Integer ("1"));
    }

    public boolean isExpiredCheck(String fieldN, Integer GroupRefID)
    {
        boolean myVal = false;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='"+fieldN+"' AND TableName='tNIM3_RetroWorklist'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getTrackedID().intValue()==1)
               {
                   myVal = true;
               }
            }
        }
        else
        {
            if (this.getExpiredFieldList().containsKey(fieldN))
           {
                myVal = true;
            }
        }
        return myVal;
    }

    public void makeRequiredHash()
    {
       makeRequiredHash(new Integer(1));
    }

    public void makeRequiredHash(Integer GroupRefID)
    {

        java.util.Hashtable newHash = new java.util.Hashtable();
        if (runFieldSecurity)
        {
            java.util.Enumeration myKeys = englishFieldList.keys(); 
            while (myKeys.hasMoreElements()) 
            {
                 String myElement = (String) myKeys.nextElement();
                 bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='"+myElement+"' AND TableName='tNIM3_RetroWorklist'" ,"");
                 bltFieldSecurity        working_bltFieldSecurity;
                 ListElement         leCurrentElement;
                 Enumeration eList = myFSL.elements();
                 if (eList.hasMoreElements())
                 {
                     leCurrentElement    = (ListElement) eList.nextElement();
                     working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
                     if (working_bltFieldSecurity.getRequiredID().intValue()==1)
                     {
                         newHash.put(myElement,new Boolean(true));
                     }
                 }
            }
        }
        else
        {
            newHash.put("PayerID",new Boolean(true));
            newHash.put("CaseClaimNumber",new Boolean(true));
            newHash.put("EmployerName",new Boolean(true));
            newHash.put("DateCaseOpened",new Boolean(true));
            newHash.put("DateOfInjury",new Boolean(true));
            newHash.put("AttorneyFirstName",new Boolean(true));
            newHash.put("AttorneyLastName",new Boolean(true));
            newHash.put("AttorneyWorkPhone",new Boolean(true));
            newHash.put("AttorneyFax",new Boolean(true));
            newHash.put("PatientAccountNumber",new Boolean(true));
            newHash.put("PatientGender",new Boolean(true));
            newHash.put("DateofService",new Boolean(true));
            newHash.put("Claimdate",new Boolean(true));
            newHash.put("MMI",new Boolean(true));
        }
        this.setRequiredFieldList(newHash);
    }

    public void makeEnglishHash()
    {

        java.util.Hashtable newHash = new java.util.Hashtable();        newHash.put("UniqueCreateDate","Item Create Date");
        newHash.put("UniqueModifyDate","Item Modify Date");
        newHash.put("UniqueModifyComments","Item Modification Comments");
        newHash.put("PayerID","PayerID");
        newHash.put("CaseClaimNumber","CaseClaimNumber");
        newHash.put("EmployerName","EmployerName");
        newHash.put("EmployerPhone","EmployerPhone");
        newHash.put("EmployerAddress1","EmployerAddress1");
        newHash.put("EmployerAddress2","EmployerAddress2");
        newHash.put("EmployerCity","EmployerCity");
        newHash.put("EmployerCounty","EmployerCounty");
        newHash.put("EmployerState","EmployerState");
        newHash.put("EmployerZip","EmployerZip");
        newHash.put("DateCaseOpened","DateCaseOpened");
        newHash.put("DateOfInjury","DateOfInjury");
        newHash.put("AttorneyFirstName","AttorneyFirstName");
        newHash.put("AttorneyLastName","AttorneyLastName");
        newHash.put("AttorneyWorkPhone","AttorneyWorkPhone");
        newHash.put("AttorneyFax","AttorneyFax");
        newHash.put("PatientAccountNumber","PatientAccountNumber");
        newHash.put("PatientFirstName","PatientFirstName");
        newHash.put("PatientLastName","PatientLastName");
        newHash.put("PatientDOB","PatientDOB");
        newHash.put("PatientSSN","PatientSSN");
        newHash.put("PatientGender","PatientGender");
        newHash.put("PatientAddress1","PatientAddress1");
        newHash.put("PatientAddress2","PatientAddress2");
        newHash.put("PatientCity","PatientCity");
        newHash.put("PatientState","PatientState");
        newHash.put("PatientZip","PatientZip");
        newHash.put("PatientHomePhone","PatientHomePhone");
        newHash.put("PatientWorkPhone","PatientWorkPhone");
        newHash.put("PatientCellPhone","PatientCellPhone");
        newHash.put("PatientEmail","PatientEmail");
        newHash.put("CorrusClaimId","CorrusClaimId");
        newHash.put("DateofService","DateofService");
        newHash.put("Claimdate","Claimdate");
        newHash.put("ClientClaimNum","ClientClaimNum");
        newHash.put("ClientName","ClientName");
        newHash.put("CPTCode","CPTCode");
        newHash.put("Modifier1","Modifier1");
        newHash.put("Modifier2","Modifier2");
        newHash.put("Units","Units");
        newHash.put("ProviderCharge","ProviderCharge");
        newHash.put("StateAllowance","StateAllowance");
        newHash.put("PayerAllowance","PayerAllowance");
        newHash.put("NetworkAllowance","NetworkAllowance");
        newHash.put("NetworkSavings","NetworkSavings");
        newHash.put("RejectDescription","RejectDescription");
        newHash.put("DiagCode1","DiagCode1");
        newHash.put("DiagCode2","DiagCode2");
        newHash.put("DiagCode3","DiagCode3");
        newHash.put("DiagCode4","DiagCode4");
        newHash.put("Claimstatus","Claimstatus");
        newHash.put("CaseManagerFirstName","CaseManagerFirstName");
        newHash.put("CaseManagerLastName","CaseManagerLastName");
        newHash.put("AccountReference","AccountReference");
        newHash.put("Controverted","Controverted");
        newHash.put("MMI","MMI");
        newHash.put("DatabaseID","DatabaseID");
        newHash.put("JurisdicationState","JurisdicationState");
        newHash.put("JurisdicationZip","JurisdicationZip");
        newHash.put("AdjusterReferenceID","AdjusterReferenceID");
        newHash.put("AdjusteFirstName","AdjusteFirstName");
        newHash.put("AdjusterLastName","AdjusterLastName");
        newHash.put("AdjusterPhoneNumber","AdjusterPhoneNumber");
        newHash.put("AdjusterFaxNumber","AdjusterFaxNumber");
        newHash.put("AdjusterEmail","AdjusterEmail");
        newHash.put("AdjusterCompany","AdjusterCompany");
        newHash.put("AdjusterAddress1","AdjusterAddress1");
        newHash.put("AdjusterAddress2","AdjusterAddress2");
        newHash.put("AdjusterCity","AdjusterCity");
        newHash.put("AdjusterState","AdjusterState");
        newHash.put("AdjusterZip","AdjusterZip");
        newHash.put("ReferringDoctorReferenceID","ReferringDoctorReferenceID");
        newHash.put("ReferringDoctorNPI","ReferringDoctorNPI");
        newHash.put("ReferringDoctorMedicalLicense","ReferringDoctorMedicalLicense");
        newHash.put("ReferringDoctorFirstName","ReferringDoctorFirstName");
        newHash.put("ReferringDoctorLastName","ReferringDoctorLastName");
        newHash.put("ReferringDoctorPhoneNumber","ReferringDoctorPhoneNumber");
        newHash.put("ReferringDoctorFaxNumber","ReferringDoctorFaxNumber");
        newHash.put("ReferringDoctorEmail","ReferringDoctorEmail");
        newHash.put("ReferringDoctorCompany","ReferringDoctorCompany");
        newHash.put("ReferringDoctorAddress1","ReferringDoctorAddress1");
        newHash.put("ReferringDoctorAddress2","ReferringDoctorAddress2");
        newHash.put("ReferringDoctorCity","ReferringDoctorCity");
        newHash.put("ReferringDoctorState","ReferringDoctorState");
        newHash.put("ReferringDoctorZip","ReferringDoctorZip");
        newHash.put("Notes","Notes");
        newHash.put("LineID","LineID");
        newHash.put("RenderingProviderReferenceID","RenderingProviderReferenceID");
        newHash.put("RenderingProviderNPI","RenderingProviderNPI");
        newHash.put("RenderingProviderTaxID","RenderingProviderTaxID");
        newHash.put("RenderingProviderMedicalLicense","RenderingProviderMedicalLicense");
        newHash.put("RenderingProviderFirstName","RenderingProviderFirstName");
        newHash.put("RenderingProviderLastName","RenderingProviderLastName");
        newHash.put("RenderingProviderPhoneNumber","RenderingProviderPhoneNumber");
        newHash.put("RenderingProviderFaxNumber","RenderingProviderFaxNumber");
        newHash.put("RenderingProviderEmail","RenderingProviderEmail");
        newHash.put("RenderingProviderCompany","RenderingProviderCompany");
        newHash.put("RenderingProviderAddress1","RenderingProviderAddress1");
        newHash.put("RenderingProviderAddress2","RenderingProviderAddress2");
        newHash.put("RenderingProviderCity","RenderingProviderCity");
        newHash.put("RenderingProviderState","RenderingProviderState");
        newHash.put("RenderingProviderZip","RenderingProviderZip");
        newHash.put("BillingProviderReferenceID","BillingProviderReferenceID");
        newHash.put("BillingProviderNPI","BillingProviderNPI");
        newHash.put("BillingProviderTaxID","BillingProviderTaxID");
        newHash.put("BillingProviderMedicalLicense","BillingProviderMedicalLicense");
        newHash.put("BillingProviderFirstName","BillingProviderFirstName");
        newHash.put("BillingProviderLastName","BillingProviderLastName");
        newHash.put("BillingProviderPhoneNumber","BillingProviderPhoneNumber");
        newHash.put("BillingProviderFaxNumber","BillingProviderFaxNumber");
        newHash.put("BillingProviderEmail","BillingProviderEmail");
        newHash.put("BillingProviderCompany","BillingProviderCompany");
        newHash.put("BillingProviderAddress1","BillingProviderAddress1");
        newHash.put("BillingProviderAddress2","BillingProviderAddress2");
        newHash.put("BillingProviderCity","BillingProviderCity");
        newHash.put("BillingProviderState","BillingProviderState");
        newHash.put("BillingProviderZip","BillingProviderZip");
        newHash.put("Network_UserField1","Network_UserField1");
        newHash.put("Network_UserField2","Network_UserField2");
        newHash.put("Network_UserField3","Network_UserField3");
        newHash.put("Network_UserField4","Network_UserField4");
        newHash.put("Network_UserField5","Network_UserField5");
        newHash.put("NIM_ProviderReferenceID","NIM_ProviderReferenceID");
        newHash.put("NIM_ProviderNPI","NIM_ProviderNPI");
        newHash.put("NIM_ProviderTaxID","NIM_ProviderTaxID");
        newHash.put("NIM_ProviderMedicalLicense","NIM_ProviderMedicalLicense");
        newHash.put("NIM_ProviderFirstName","NIM_ProviderFirstName");
        newHash.put("NIM_ProviderLastName","NIM_ProviderLastName");
        newHash.put("NIM_ProviderPhoneNumber","NIM_ProviderPhoneNumber");
        newHash.put("NIM_ProviderFaxNumber","NIM_ProviderFaxNumber");
        newHash.put("NIM_ProviderEmail","NIM_ProviderEmail");
        newHash.put("NIM_ProviderCompany","NIM_ProviderCompany");
        newHash.put("NIM_ProviderAddress1","NIM_ProviderAddress1");
        newHash.put("NIM_ProviderAddress2","NIM_ProviderAddress2");
        newHash.put("NIM_ProviderCity","NIM_ProviderCity");
        newHash.put("NIM_ProviderState","NIM_ProviderState");
        newHash.put("NIM_ProviderZip","NIM_ProviderZip");
        newHash.put("NIM_ProviderMatchFound","NIM_ProviderMatchFound");
        newHash.put("NIM_ProviderMatchAcceptsRetro","NIM_ProviderMatchAcceptsRetro");
        newHash.put("NIM_ProviderMatchPositiveSavings","NIM_ProviderMatchPositiveSavings");
        newHash.put("NIM_AcceptClaim","NIM_AcceptClaim");
        newHash.put("NIM_DateAccepted","NIM_DateAccepted");
        newHash.put("NIM_UserAcceptedID","NIM_UserAcceptedID");
        newHash.put("NIM_DateExported","NIM_DateExported");
        newHash.put("NIM_EncounterID","NIM_EncounterID");
        newHash.put("NIM_ContractProviderCost","NIM_ContractProviderCost");
        newHash.put("NIM_FinalProviderCost","NIM_FinalProviderCost");
        newHash.put("NIM_PayerAllow","NIM_PayerAllow");

        this.setEnglishFieldList(newHash);
    }
    public void makeExpiredHash()
    {

        java.util.Hashtable newHash = new java.util.Hashtable();
        this.setExpiredFieldList(newHash);
    }

    public java.util.Hashtable englishFieldList = new java.util.Hashtable();
    public java.util.Hashtable requiredFieldList = new java.util.Hashtable();
    public java.util.Hashtable expiredFieldList = new java.util.Hashtable();

    public boolean isComplete()
    {
	     boolean myVal = true;
	     java.util.Enumeration myKeys = this.getRequiredFieldList().keys(); 
	     while (myKeys.hasMoreElements()) 
	     {
	         String myElement = (String) myKeys.nextElement();
	         if (!isComplete(myElement))
	         {
	             myVal = false;
	         }
	     }
	     return myVal;
    }

    public String getEnglish(String fieldN)
    {
        String myVal = null;
        if (this.englishFieldList.containsKey(fieldN))
        {
            myVal = (String)this.englishFieldList.get(fieldN);
        }
        return myVal;
    }
    public java.util.Vector isComplete(int daysVector)
    {
	     boolean myVal = false;
	     java.util.Enumeration myKeys = this.getExpiredFieldList().keys(); 
	     java.util.Vector myExpiredList = new java.util.Vector(); 
	     while (myKeys.hasMoreElements()) 
	     {
	         String myElement = (String) myKeys.nextElement();
	         if (isExpired(myElement,daysVector))
	         {
	             myVal = true;
	             myExpiredList.addElement(myElement);
	         }
	     }
	     return myExpiredList;
    }

    public void setField(String fieldN, Object fieldV)
    {
     boolean myVal = false;
     try
     {
        java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat("yyyy-MM-dd");

        if (fieldN.equals("RetroWorklistID"))
        {
             this.setRetroWorklistID((Integer)fieldV);
        }

        else if (fieldN.equals("UniqueCreateDate"))
        {
            this.setUniqueCreateDate((java.util.Date)fieldV);
        }

        else if (fieldN.equals("UniqueModifyDate"))
        {
            this.setUniqueModifyDate((java.util.Date)fieldV);
        }

        else if (fieldN.equals("UniqueModifyComments"))
        {
            this.setUniqueModifyComments((String)fieldV);
        }

        else if (fieldN.equals("PayerID"))
        {
             this.setPayerID((Integer)fieldV);
        }

        else if (fieldN.equals("CaseClaimNumber"))
        {
            this.setCaseClaimNumber((String)fieldV);
        }

        else if (fieldN.equals("EmployerName"))
        {
            this.setEmployerName((String)fieldV);
        }

        else if (fieldN.equals("EmployerPhone"))
        {
            this.setEmployerPhone((String)fieldV);
        }

        else if (fieldN.equals("EmployerAddress1"))
        {
            this.setEmployerAddress1((String)fieldV);
        }

        else if (fieldN.equals("EmployerAddress2"))
        {
            this.setEmployerAddress2((String)fieldV);
        }

        else if (fieldN.equals("EmployerCity"))
        {
            this.setEmployerCity((String)fieldV);
        }

        else if (fieldN.equals("EmployerCounty"))
        {
            this.setEmployerCounty((String)fieldV);
        }

        else if (fieldN.equals("EmployerState"))
        {
            this.setEmployerState((String)fieldV);
        }

        else if (fieldN.equals("EmployerZip"))
        {
            this.setEmployerZip((String)fieldV);
        }

        else if (fieldN.equals("DateCaseOpened"))
        {
            this.setDateCaseOpened((java.util.Date)fieldV);
        }

        else if (fieldN.equals("DateOfInjury"))
        {
            this.setDateOfInjury((java.util.Date)fieldV);
        }

        else if (fieldN.equals("AttorneyFirstName"))
        {
            this.setAttorneyFirstName((String)fieldV);
        }

        else if (fieldN.equals("AttorneyLastName"))
        {
            this.setAttorneyLastName((String)fieldV);
        }

        else if (fieldN.equals("AttorneyWorkPhone"))
        {
            this.setAttorneyWorkPhone((String)fieldV);
        }

        else if (fieldN.equals("AttorneyFax"))
        {
            this.setAttorneyFax((String)fieldV);
        }

        else if (fieldN.equals("PatientAccountNumber"))
        {
            this.setPatientAccountNumber((String)fieldV);
        }

        else if (fieldN.equals("PatientFirstName"))
        {
            this.setPatientFirstName((String)fieldV);
        }

        else if (fieldN.equals("PatientLastName"))
        {
            this.setPatientLastName((String)fieldV);
        }

        else if (fieldN.equals("PatientDOB"))
        {
            this.setPatientDOB((java.util.Date)fieldV);
        }

        else if (fieldN.equals("PatientSSN"))
        {
            this.setPatientSSN((String)fieldV);
        }

        else if (fieldN.equals("PatientGender"))
        {
            this.setPatientGender((String)fieldV);
        }

        else if (fieldN.equals("PatientAddress1"))
        {
            this.setPatientAddress1((String)fieldV);
        }

        else if (fieldN.equals("PatientAddress2"))
        {
            this.setPatientAddress2((String)fieldV);
        }

        else if (fieldN.equals("PatientCity"))
        {
            this.setPatientCity((String)fieldV);
        }

        else if (fieldN.equals("PatientState"))
        {
            this.setPatientState((String)fieldV);
        }

        else if (fieldN.equals("PatientZip"))
        {
            this.setPatientZip((String)fieldV);
        }

        else if (fieldN.equals("PatientHomePhone"))
        {
            this.setPatientHomePhone((String)fieldV);
        }

        else if (fieldN.equals("PatientWorkPhone"))
        {
            this.setPatientWorkPhone((String)fieldV);
        }

        else if (fieldN.equals("PatientCellPhone"))
        {
            this.setPatientCellPhone((String)fieldV);
        }

        else if (fieldN.equals("PatientEmail"))
        {
            this.setPatientEmail((String)fieldV);
        }

        else if (fieldN.equals("CorrusClaimId"))
        {
            this.setCorrusClaimId((String)fieldV);
        }

        else if (fieldN.equals("DateofService"))
        {
            this.setDateofService((java.util.Date)fieldV);
        }

        else if (fieldN.equals("Claimdate"))
        {
            this.setClaimdate((java.util.Date)fieldV);
        }

        else if (fieldN.equals("ClientClaimNum"))
        {
            this.setClientClaimNum((String)fieldV);
        }

        else if (fieldN.equals("ClientName"))
        {
            this.setClientName((String)fieldV);
        }

        else if (fieldN.equals("CPTCode"))
        {
            this.setCPTCode((String)fieldV);
        }

        else if (fieldN.equals("Modifier1"))
        {
            this.setModifier1((String)fieldV);
        }

        else if (fieldN.equals("Modifier2"))
        {
            this.setModifier2((String)fieldV);
        }

        else if (fieldN.equals("Units"))
        {
            this.setUnits((Double)fieldV);
        }

        else if (fieldN.equals("ProviderCharge"))
        {
            this.setProviderCharge((Double)fieldV);
        }

        else if (fieldN.equals("StateAllowance"))
        {
            this.setStateAllowance((Double)fieldV);
        }

        else if (fieldN.equals("PayerAllowance"))
        {
            this.setPayerAllowance((Double)fieldV);
        }

        else if (fieldN.equals("NetworkAllowance"))
        {
            this.setNetworkAllowance((Double)fieldV);
        }

        else if (fieldN.equals("NetworkSavings"))
        {
            this.setNetworkSavings((Double)fieldV);
        }

        else if (fieldN.equals("RejectDescription"))
        {
            this.setRejectDescription((String)fieldV);
        }

        else if (fieldN.equals("DiagCode1"))
        {
            this.setDiagCode1((String)fieldV);
        }

        else if (fieldN.equals("DiagCode2"))
        {
            this.setDiagCode2((String)fieldV);
        }

        else if (fieldN.equals("DiagCode3"))
        {
            this.setDiagCode3((String)fieldV);
        }

        else if (fieldN.equals("DiagCode4"))
        {
            this.setDiagCode4((String)fieldV);
        }

        else if (fieldN.equals("Claimstatus"))
        {
            this.setClaimstatus((String)fieldV);
        }

        else if (fieldN.equals("CaseManagerFirstName"))
        {
            this.setCaseManagerFirstName((String)fieldV);
        }

        else if (fieldN.equals("CaseManagerLastName"))
        {
            this.setCaseManagerLastName((String)fieldV);
        }

        else if (fieldN.equals("AccountReference"))
        {
            this.setAccountReference((String)fieldV);
        }

        else if (fieldN.equals("Controverted"))
        {
            this.setControverted((String)fieldV);
        }

        else if (fieldN.equals("MMI"))
        {
            this.setMMI((java.util.Date)fieldV);
        }

        else if (fieldN.equals("DatabaseID"))
        {
            this.setDatabaseID((String)fieldV);
        }

        else if (fieldN.equals("JurisdicationState"))
        {
            this.setJurisdicationState((String)fieldV);
        }

        else if (fieldN.equals("JurisdicationZip"))
        {
            this.setJurisdicationZip((String)fieldV);
        }

        else if (fieldN.equals("AdjusterReferenceID"))
        {
            this.setAdjusterReferenceID((String)fieldV);
        }

        else if (fieldN.equals("AdjusteFirstName"))
        {
            this.setAdjusteFirstName((String)fieldV);
        }

        else if (fieldN.equals("AdjusterLastName"))
        {
            this.setAdjusterLastName((String)fieldV);
        }

        else if (fieldN.equals("AdjusterPhoneNumber"))
        {
            this.setAdjusterPhoneNumber((String)fieldV);
        }

        else if (fieldN.equals("AdjusterFaxNumber"))
        {
            this.setAdjusterFaxNumber((String)fieldV);
        }

        else if (fieldN.equals("AdjusterEmail"))
        {
            this.setAdjusterEmail((String)fieldV);
        }

        else if (fieldN.equals("AdjusterCompany"))
        {
            this.setAdjusterCompany((String)fieldV);
        }

        else if (fieldN.equals("AdjusterAddress1"))
        {
            this.setAdjusterAddress1((String)fieldV);
        }

        else if (fieldN.equals("AdjusterAddress2"))
        {
            this.setAdjusterAddress2((String)fieldV);
        }

        else if (fieldN.equals("AdjusterCity"))
        {
            this.setAdjusterCity((String)fieldV);
        }

        else if (fieldN.equals("AdjusterState"))
        {
            this.setAdjusterState((String)fieldV);
        }

        else if (fieldN.equals("AdjusterZip"))
        {
            this.setAdjusterZip((String)fieldV);
        }

        else if (fieldN.equals("ReferringDoctorReferenceID"))
        {
            this.setReferringDoctorReferenceID((String)fieldV);
        }

        else if (fieldN.equals("ReferringDoctorNPI"))
        {
            this.setReferringDoctorNPI((String)fieldV);
        }

        else if (fieldN.equals("ReferringDoctorMedicalLicense"))
        {
            this.setReferringDoctorMedicalLicense((String)fieldV);
        }

        else if (fieldN.equals("ReferringDoctorFirstName"))
        {
            this.setReferringDoctorFirstName((String)fieldV);
        }

        else if (fieldN.equals("ReferringDoctorLastName"))
        {
            this.setReferringDoctorLastName((String)fieldV);
        }

        else if (fieldN.equals("ReferringDoctorPhoneNumber"))
        {
            this.setReferringDoctorPhoneNumber((String)fieldV);
        }

        else if (fieldN.equals("ReferringDoctorFaxNumber"))
        {
            this.setReferringDoctorFaxNumber((String)fieldV);
        }

        else if (fieldN.equals("ReferringDoctorEmail"))
        {
            this.setReferringDoctorEmail((String)fieldV);
        }

        else if (fieldN.equals("ReferringDoctorCompany"))
        {
            this.setReferringDoctorCompany((String)fieldV);
        }

        else if (fieldN.equals("ReferringDoctorAddress1"))
        {
            this.setReferringDoctorAddress1((String)fieldV);
        }

        else if (fieldN.equals("ReferringDoctorAddress2"))
        {
            this.setReferringDoctorAddress2((String)fieldV);
        }

        else if (fieldN.equals("ReferringDoctorCity"))
        {
            this.setReferringDoctorCity((String)fieldV);
        }

        else if (fieldN.equals("ReferringDoctorState"))
        {
            this.setReferringDoctorState((String)fieldV);
        }

        else if (fieldN.equals("ReferringDoctorZip"))
        {
            this.setReferringDoctorZip((String)fieldV);
        }

        else if (fieldN.equals("Notes"))
        {
            this.setNotes((String)fieldV);
        }

        else if (fieldN.equals("LineID"))
        {
             this.setLineID((Integer)fieldV);
        }

        else if (fieldN.equals("RenderingProviderReferenceID"))
        {
            this.setRenderingProviderReferenceID((String)fieldV);
        }

        else if (fieldN.equals("RenderingProviderNPI"))
        {
            this.setRenderingProviderNPI((String)fieldV);
        }

        else if (fieldN.equals("RenderingProviderTaxID"))
        {
            this.setRenderingProviderTaxID((String)fieldV);
        }

        else if (fieldN.equals("RenderingProviderMedicalLicense"))
        {
            this.setRenderingProviderMedicalLicense((String)fieldV);
        }

        else if (fieldN.equals("RenderingProviderFirstName"))
        {
            this.setRenderingProviderFirstName((String)fieldV);
        }

        else if (fieldN.equals("RenderingProviderLastName"))
        {
            this.setRenderingProviderLastName((String)fieldV);
        }

        else if (fieldN.equals("RenderingProviderPhoneNumber"))
        {
            this.setRenderingProviderPhoneNumber((String)fieldV);
        }

        else if (fieldN.equals("RenderingProviderFaxNumber"))
        {
            this.setRenderingProviderFaxNumber((String)fieldV);
        }

        else if (fieldN.equals("RenderingProviderEmail"))
        {
            this.setRenderingProviderEmail((String)fieldV);
        }

        else if (fieldN.equals("RenderingProviderCompany"))
        {
            this.setRenderingProviderCompany((String)fieldV);
        }

        else if (fieldN.equals("RenderingProviderAddress1"))
        {
            this.setRenderingProviderAddress1((String)fieldV);
        }

        else if (fieldN.equals("RenderingProviderAddress2"))
        {
            this.setRenderingProviderAddress2((String)fieldV);
        }

        else if (fieldN.equals("RenderingProviderCity"))
        {
            this.setRenderingProviderCity((String)fieldV);
        }

        else if (fieldN.equals("RenderingProviderState"))
        {
            this.setRenderingProviderState((String)fieldV);
        }

        else if (fieldN.equals("RenderingProviderZip"))
        {
            this.setRenderingProviderZip((String)fieldV);
        }

        else if (fieldN.equals("BillingProviderReferenceID"))
        {
            this.setBillingProviderReferenceID((String)fieldV);
        }

        else if (fieldN.equals("BillingProviderNPI"))
        {
            this.setBillingProviderNPI((String)fieldV);
        }

        else if (fieldN.equals("BillingProviderTaxID"))
        {
            this.setBillingProviderTaxID((String)fieldV);
        }

        else if (fieldN.equals("BillingProviderMedicalLicense"))
        {
            this.setBillingProviderMedicalLicense((String)fieldV);
        }

        else if (fieldN.equals("BillingProviderFirstName"))
        {
            this.setBillingProviderFirstName((String)fieldV);
        }

        else if (fieldN.equals("BillingProviderLastName"))
        {
            this.setBillingProviderLastName((String)fieldV);
        }

        else if (fieldN.equals("BillingProviderPhoneNumber"))
        {
            this.setBillingProviderPhoneNumber((String)fieldV);
        }

        else if (fieldN.equals("BillingProviderFaxNumber"))
        {
            this.setBillingProviderFaxNumber((String)fieldV);
        }

        else if (fieldN.equals("BillingProviderEmail"))
        {
            this.setBillingProviderEmail((String)fieldV);
        }

        else if (fieldN.equals("BillingProviderCompany"))
        {
            this.setBillingProviderCompany((String)fieldV);
        }

        else if (fieldN.equals("BillingProviderAddress1"))
        {
            this.setBillingProviderAddress1((String)fieldV);
        }

        else if (fieldN.equals("BillingProviderAddress2"))
        {
            this.setBillingProviderAddress2((String)fieldV);
        }

        else if (fieldN.equals("BillingProviderCity"))
        {
            this.setBillingProviderCity((String)fieldV);
        }

        else if (fieldN.equals("BillingProviderState"))
        {
            this.setBillingProviderState((String)fieldV);
        }

        else if (fieldN.equals("BillingProviderZip"))
        {
            this.setBillingProviderZip((String)fieldV);
        }

        else if (fieldN.equals("Network_UserField1"))
        {
            this.setNetwork_UserField1((String)fieldV);
        }

        else if (fieldN.equals("Network_UserField2"))
        {
            this.setNetwork_UserField2((String)fieldV);
        }

        else if (fieldN.equals("Network_UserField3"))
        {
            this.setNetwork_UserField3((String)fieldV);
        }

        else if (fieldN.equals("Network_UserField4"))
        {
            this.setNetwork_UserField4((String)fieldV);
        }

        else if (fieldN.equals("Network_UserField5"))
        {
            this.setNetwork_UserField5((String)fieldV);
        }

        else if (fieldN.equals("NIM_ProviderReferenceID"))
        {
            this.setNIM_ProviderReferenceID((String)fieldV);
        }

        else if (fieldN.equals("NIM_ProviderNPI"))
        {
            this.setNIM_ProviderNPI((String)fieldV);
        }

        else if (fieldN.equals("NIM_ProviderTaxID"))
        {
            this.setNIM_ProviderTaxID((String)fieldV);
        }

        else if (fieldN.equals("NIM_ProviderMedicalLicense"))
        {
            this.setNIM_ProviderMedicalLicense((String)fieldV);
        }

        else if (fieldN.equals("NIM_ProviderFirstName"))
        {
            this.setNIM_ProviderFirstName((String)fieldV);
        }

        else if (fieldN.equals("NIM_ProviderLastName"))
        {
            this.setNIM_ProviderLastName((String)fieldV);
        }

        else if (fieldN.equals("NIM_ProviderPhoneNumber"))
        {
            this.setNIM_ProviderPhoneNumber((String)fieldV);
        }

        else if (fieldN.equals("NIM_ProviderFaxNumber"))
        {
            this.setNIM_ProviderFaxNumber((String)fieldV);
        }

        else if (fieldN.equals("NIM_ProviderEmail"))
        {
            this.setNIM_ProviderEmail((String)fieldV);
        }

        else if (fieldN.equals("NIM_ProviderCompany"))
        {
            this.setNIM_ProviderCompany((String)fieldV);
        }

        else if (fieldN.equals("NIM_ProviderAddress1"))
        {
            this.setNIM_ProviderAddress1((String)fieldV);
        }

        else if (fieldN.equals("NIM_ProviderAddress2"))
        {
            this.setNIM_ProviderAddress2((String)fieldV);
        }

        else if (fieldN.equals("NIM_ProviderCity"))
        {
            this.setNIM_ProviderCity((String)fieldV);
        }

        else if (fieldN.equals("NIM_ProviderState"))
        {
            this.setNIM_ProviderState((String)fieldV);
        }

        else if (fieldN.equals("NIM_ProviderZip"))
        {
            this.setNIM_ProviderZip((String)fieldV);
        }

        else if (fieldN.equals("NIM_ProviderMatchFound"))
        {
             this.setNIM_ProviderMatchFound((Integer)fieldV);
        }

        else if (fieldN.equals("NIM_ProviderMatchAcceptsRetro"))
        {
             this.setNIM_ProviderMatchAcceptsRetro((Integer)fieldV);
        }

        else if (fieldN.equals("NIM_ProviderMatchPositiveSavings"))
        {
             this.setNIM_ProviderMatchPositiveSavings((Integer)fieldV);
        }

        else if (fieldN.equals("NIM_AcceptClaim"))
        {
             this.setNIM_AcceptClaim((Integer)fieldV);
        }

        else if (fieldN.equals("NIM_DateAccepted"))
        {
            this.setNIM_DateAccepted((java.util.Date)fieldV);
        }

        else if (fieldN.equals("NIM_UserAcceptedID"))
        {
             this.setNIM_UserAcceptedID((Integer)fieldV);
        }

        else if (fieldN.equals("NIM_DateExported"))
        {
            this.setNIM_DateExported((java.util.Date)fieldV);
        }

        else if (fieldN.equals("NIM_EncounterID"))
        {
             this.setNIM_EncounterID((Integer)fieldV);
        }

        else if (fieldN.equals("NIM_ContractProviderCost"))
        {
            this.setNIM_ContractProviderCost((Double)fieldV);
        }

        else if (fieldN.equals("NIM_FinalProviderCost"))
        {
            this.setNIM_FinalProviderCost((Double)fieldV);
        }

        else if (fieldN.equals("NIM_PayerAllow"))
        {
            this.setNIM_PayerAllow((Double)fieldV);
        }

	    }
	    catch(Exception e2)
	    {}
	    //return myVal;
	}

    public String getFieldType(String fieldN)
    {
     String  myVal = null;
     try
     {
        java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat("yyyy-MM-dd");

        if (fieldN.equals("RetroWorklistID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("UniqueCreateDate"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("UniqueModifyDate"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("UniqueModifyComments"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("PayerID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("CaseClaimNumber"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("EmployerName"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("EmployerPhone"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("EmployerAddress1"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("EmployerAddress2"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("EmployerCity"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("EmployerCounty"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("EmployerState"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("EmployerZip"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("DateCaseOpened"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("DateOfInjury"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("AttorneyFirstName"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("AttorneyLastName"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("AttorneyWorkPhone"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("AttorneyFax"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("PatientAccountNumber"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("PatientFirstName"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("PatientLastName"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("PatientDOB"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("PatientSSN"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("PatientGender"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("PatientAddress1"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("PatientAddress2"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("PatientCity"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("PatientState"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("PatientZip"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("PatientHomePhone"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("PatientWorkPhone"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("PatientCellPhone"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("PatientEmail"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("CorrusClaimId"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("DateofService"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("Claimdate"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("ClientClaimNum"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("ClientName"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("CPTCode"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("Modifier1"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("Modifier2"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("Units"))
        {
            myVal = "Double";
        }

        else if (fieldN.equals("ProviderCharge"))
        {
            myVal = "Double";
        }

        else if (fieldN.equals("StateAllowance"))
        {
            myVal = "Double";
        }

        else if (fieldN.equals("PayerAllowance"))
        {
            myVal = "Double";
        }

        else if (fieldN.equals("NetworkAllowance"))
        {
            myVal = "Double";
        }

        else if (fieldN.equals("NetworkSavings"))
        {
            myVal = "Double";
        }

        else if (fieldN.equals("RejectDescription"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("DiagCode1"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("DiagCode2"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("DiagCode3"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("DiagCode4"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("Claimstatus"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("CaseManagerFirstName"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("CaseManagerLastName"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("AccountReference"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("Controverted"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("MMI"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("DatabaseID"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("JurisdicationState"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("JurisdicationZip"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("AdjusterReferenceID"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("AdjusteFirstName"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("AdjusterLastName"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("AdjusterPhoneNumber"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("AdjusterFaxNumber"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("AdjusterEmail"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("AdjusterCompany"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("AdjusterAddress1"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("AdjusterAddress2"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("AdjusterCity"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("AdjusterState"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("AdjusterZip"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("ReferringDoctorReferenceID"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("ReferringDoctorNPI"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("ReferringDoctorMedicalLicense"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("ReferringDoctorFirstName"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("ReferringDoctorLastName"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("ReferringDoctorPhoneNumber"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("ReferringDoctorFaxNumber"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("ReferringDoctorEmail"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("ReferringDoctorCompany"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("ReferringDoctorAddress1"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("ReferringDoctorAddress2"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("ReferringDoctorCity"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("ReferringDoctorState"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("ReferringDoctorZip"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("Notes"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("LineID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("RenderingProviderReferenceID"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("RenderingProviderNPI"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("RenderingProviderTaxID"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("RenderingProviderMedicalLicense"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("RenderingProviderFirstName"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("RenderingProviderLastName"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("RenderingProviderPhoneNumber"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("RenderingProviderFaxNumber"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("RenderingProviderEmail"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("RenderingProviderCompany"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("RenderingProviderAddress1"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("RenderingProviderAddress2"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("RenderingProviderCity"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("RenderingProviderState"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("RenderingProviderZip"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("BillingProviderReferenceID"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("BillingProviderNPI"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("BillingProviderTaxID"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("BillingProviderMedicalLicense"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("BillingProviderFirstName"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("BillingProviderLastName"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("BillingProviderPhoneNumber"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("BillingProviderFaxNumber"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("BillingProviderEmail"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("BillingProviderCompany"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("BillingProviderAddress1"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("BillingProviderAddress2"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("BillingProviderCity"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("BillingProviderState"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("BillingProviderZip"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("Network_UserField1"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("Network_UserField2"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("Network_UserField3"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("Network_UserField4"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("Network_UserField5"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("NIM_ProviderReferenceID"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("NIM_ProviderNPI"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("NIM_ProviderTaxID"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("NIM_ProviderMedicalLicense"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("NIM_ProviderFirstName"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("NIM_ProviderLastName"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("NIM_ProviderPhoneNumber"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("NIM_ProviderFaxNumber"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("NIM_ProviderEmail"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("NIM_ProviderCompany"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("NIM_ProviderAddress1"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("NIM_ProviderAddress2"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("NIM_ProviderCity"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("NIM_ProviderState"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("NIM_ProviderZip"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("NIM_ProviderMatchFound"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("NIM_ProviderMatchAcceptsRetro"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("NIM_ProviderMatchPositiveSavings"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("NIM_AcceptClaim"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("NIM_DateAccepted"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("NIM_UserAcceptedID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("NIM_DateExported"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("NIM_EncounterID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("NIM_ContractProviderCost"))
        {
            myVal = "Double";
        }

        else if (fieldN.equals("NIM_FinalProviderCost"))
        {
            myVal = "Double";
        }

        else if (fieldN.equals("NIM_PayerAllow"))
        {
            myVal = "Double";
        }

	    }
	    catch(Exception e2)
	    {}
	    return myVal;
	}

    public boolean isComplete(String fieldN)
    {
	     boolean myVal = false;
	     try
	     {
        java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat("yyyy-MM-dd");

	     if (fieldN.equals("RetroWorklistID"))

	     {
                    if (this.getRetroWorklistID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("UniqueCreateDate"))

	     {
                    if (this.getUniqueCreateDate().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("UniqueModifyDate"))

	     {
                    if (this.getUniqueModifyDate().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("UniqueModifyComments"))

	     {
                    if (this.getUniqueModifyComments().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("PayerID"))

	     {
                    if (this.getPayerID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("CaseClaimNumber"))

	     {
                    if (this.getCaseClaimNumber().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("EmployerName"))

	     {
                    if (this.getEmployerName().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("EmployerPhone"))

	     {
                    if (this.getEmployerPhone().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("EmployerAddress1"))

	     {
                    if (this.getEmployerAddress1().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("EmployerAddress2"))

	     {
                    if (this.getEmployerAddress2().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("EmployerCity"))

	     {
                    if (this.getEmployerCity().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("EmployerCounty"))

	     {
                    if (this.getEmployerCounty().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("EmployerState"))

	     {
                    if (this.getEmployerState().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("EmployerZip"))

	     {
                    if (this.getEmployerZip().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("DateCaseOpened"))

	     {
                    if (this.getDateCaseOpened().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("DateOfInjury"))

	     {
                    if (this.getDateOfInjury().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("AttorneyFirstName"))

	     {
                    if (this.getAttorneyFirstName().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("AttorneyLastName"))

	     {
                    if (this.getAttorneyLastName().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("AttorneyWorkPhone"))

	     {
                    if (this.getAttorneyWorkPhone().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("AttorneyFax"))

	     {
                    if (this.getAttorneyFax().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("PatientAccountNumber"))

	     {
                    if (this.getPatientAccountNumber().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("PatientFirstName"))

	     {
                    if (this.getPatientFirstName().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("PatientLastName"))

	     {
                    if (this.getPatientLastName().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("PatientDOB"))

	     {
                    if (this.getPatientDOB().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("PatientSSN"))

	     {
                    if (this.getPatientSSN().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("PatientGender"))

	     {
                    if (this.getPatientGender().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("PatientAddress1"))

	     {
                    if (this.getPatientAddress1().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("PatientAddress2"))

	     {
                    if (this.getPatientAddress2().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("PatientCity"))

	     {
                    if (this.getPatientCity().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("PatientState"))

	     {
                    if (this.getPatientState().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("PatientZip"))

	     {
                    if (this.getPatientZip().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("PatientHomePhone"))

	     {
                    if (this.getPatientHomePhone().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("PatientWorkPhone"))

	     {
                    if (this.getPatientWorkPhone().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("PatientCellPhone"))

	     {
                    if (this.getPatientCellPhone().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("PatientEmail"))

	     {
                    if (this.getPatientEmail().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("CorrusClaimId"))

	     {
                    if (this.getCorrusClaimId().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("DateofService"))

	     {
                    if (this.getDateofService().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("Claimdate"))

	     {
                    if (this.getClaimdate().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("ClientClaimNum"))

	     {
                    if (this.getClientClaimNum().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("ClientName"))

	     {
                    if (this.getClientName().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("CPTCode"))

	     {
                    if (this.getCPTCode().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("Modifier1"))

	     {
                    if (this.getModifier1().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("Modifier2"))

	     {
                    if (this.getModifier2().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("Units"))

	     {
                    if (this.getUnits().equals(new Double("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("ProviderCharge"))

	     {
                    if (this.getProviderCharge().equals(new Double("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("StateAllowance"))

	     {
                    if (this.getStateAllowance().equals(new Double("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("PayerAllowance"))

	     {
                    if (this.getPayerAllowance().equals(new Double("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("NetworkAllowance"))

	     {
                    if (this.getNetworkAllowance().equals(new Double("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("NetworkSavings"))

	     {
                    if (this.getNetworkSavings().equals(new Double("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("RejectDescription"))

	     {
                    if (this.getRejectDescription().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("DiagCode1"))

	     {
                    if (this.getDiagCode1().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("DiagCode2"))

	     {
                    if (this.getDiagCode2().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("DiagCode3"))

	     {
                    if (this.getDiagCode3().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("DiagCode4"))

	     {
                    if (this.getDiagCode4().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("Claimstatus"))

	     {
                    if (this.getClaimstatus().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("CaseManagerFirstName"))

	     {
                    if (this.getCaseManagerFirstName().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("CaseManagerLastName"))

	     {
                    if (this.getCaseManagerLastName().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("AccountReference"))

	     {
                    if (this.getAccountReference().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("Controverted"))

	     {
                    if (this.getControverted().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("MMI"))

	     {
                    if (this.getMMI().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("DatabaseID"))

	     {
                    if (this.getDatabaseID().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("JurisdicationState"))

	     {
                    if (this.getJurisdicationState().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("JurisdicationZip"))

	     {
                    if (this.getJurisdicationZip().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("AdjusterReferenceID"))

	     {
                    if (this.getAdjusterReferenceID().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("AdjusteFirstName"))

	     {
                    if (this.getAdjusteFirstName().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("AdjusterLastName"))

	     {
                    if (this.getAdjusterLastName().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("AdjusterPhoneNumber"))

	     {
                    if (this.getAdjusterPhoneNumber().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("AdjusterFaxNumber"))

	     {
                    if (this.getAdjusterFaxNumber().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("AdjusterEmail"))

	     {
                    if (this.getAdjusterEmail().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("AdjusterCompany"))

	     {
                    if (this.getAdjusterCompany().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("AdjusterAddress1"))

	     {
                    if (this.getAdjusterAddress1().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("AdjusterAddress2"))

	     {
                    if (this.getAdjusterAddress2().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("AdjusterCity"))

	     {
                    if (this.getAdjusterCity().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("AdjusterState"))

	     {
                    if (this.getAdjusterState().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("AdjusterZip"))

	     {
                    if (this.getAdjusterZip().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("ReferringDoctorReferenceID"))

	     {
                    if (this.getReferringDoctorReferenceID().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("ReferringDoctorNPI"))

	     {
                    if (this.getReferringDoctorNPI().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("ReferringDoctorMedicalLicense"))

	     {
                    if (this.getReferringDoctorMedicalLicense().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("ReferringDoctorFirstName"))

	     {
                    if (this.getReferringDoctorFirstName().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("ReferringDoctorLastName"))

	     {
                    if (this.getReferringDoctorLastName().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("ReferringDoctorPhoneNumber"))

	     {
                    if (this.getReferringDoctorPhoneNumber().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("ReferringDoctorFaxNumber"))

	     {
                    if (this.getReferringDoctorFaxNumber().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("ReferringDoctorEmail"))

	     {
                    if (this.getReferringDoctorEmail().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("ReferringDoctorCompany"))

	     {
                    if (this.getReferringDoctorCompany().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("ReferringDoctorAddress1"))

	     {
                    if (this.getReferringDoctorAddress1().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("ReferringDoctorAddress2"))

	     {
                    if (this.getReferringDoctorAddress2().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("ReferringDoctorCity"))

	     {
                    if (this.getReferringDoctorCity().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("ReferringDoctorState"))

	     {
                    if (this.getReferringDoctorState().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("ReferringDoctorZip"))

	     {
                    if (this.getReferringDoctorZip().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("Notes"))

	     {
                    if (this.getNotes().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("LineID"))

	     {
                    if (this.getLineID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("RenderingProviderReferenceID"))

	     {
                    if (this.getRenderingProviderReferenceID().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("RenderingProviderNPI"))

	     {
                    if (this.getRenderingProviderNPI().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("RenderingProviderTaxID"))

	     {
                    if (this.getRenderingProviderTaxID().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("RenderingProviderMedicalLicense"))

	     {
                    if (this.getRenderingProviderMedicalLicense().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("RenderingProviderFirstName"))

	     {
                    if (this.getRenderingProviderFirstName().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("RenderingProviderLastName"))

	     {
                    if (this.getRenderingProviderLastName().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("RenderingProviderPhoneNumber"))

	     {
                    if (this.getRenderingProviderPhoneNumber().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("RenderingProviderFaxNumber"))

	     {
                    if (this.getRenderingProviderFaxNumber().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("RenderingProviderEmail"))

	     {
                    if (this.getRenderingProviderEmail().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("RenderingProviderCompany"))

	     {
                    if (this.getRenderingProviderCompany().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("RenderingProviderAddress1"))

	     {
                    if (this.getRenderingProviderAddress1().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("RenderingProviderAddress2"))

	     {
                    if (this.getRenderingProviderAddress2().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("RenderingProviderCity"))

	     {
                    if (this.getRenderingProviderCity().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("RenderingProviderState"))

	     {
                    if (this.getRenderingProviderState().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("RenderingProviderZip"))

	     {
                    if (this.getRenderingProviderZip().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("BillingProviderReferenceID"))

	     {
                    if (this.getBillingProviderReferenceID().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("BillingProviderNPI"))

	     {
                    if (this.getBillingProviderNPI().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("BillingProviderTaxID"))

	     {
                    if (this.getBillingProviderTaxID().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("BillingProviderMedicalLicense"))

	     {
                    if (this.getBillingProviderMedicalLicense().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("BillingProviderFirstName"))

	     {
                    if (this.getBillingProviderFirstName().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("BillingProviderLastName"))

	     {
                    if (this.getBillingProviderLastName().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("BillingProviderPhoneNumber"))

	     {
                    if (this.getBillingProviderPhoneNumber().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("BillingProviderFaxNumber"))

	     {
                    if (this.getBillingProviderFaxNumber().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("BillingProviderEmail"))

	     {
                    if (this.getBillingProviderEmail().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("BillingProviderCompany"))

	     {
                    if (this.getBillingProviderCompany().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("BillingProviderAddress1"))

	     {
                    if (this.getBillingProviderAddress1().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("BillingProviderAddress2"))

	     {
                    if (this.getBillingProviderAddress2().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("BillingProviderCity"))

	     {
                    if (this.getBillingProviderCity().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("BillingProviderState"))

	     {
                    if (this.getBillingProviderState().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("BillingProviderZip"))

	     {
                    if (this.getBillingProviderZip().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("Network_UserField1"))

	     {
                    if (this.getNetwork_UserField1().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("Network_UserField2"))

	     {
                    if (this.getNetwork_UserField2().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("Network_UserField3"))

	     {
                    if (this.getNetwork_UserField3().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("Network_UserField4"))

	     {
                    if (this.getNetwork_UserField4().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("Network_UserField5"))

	     {
                    if (this.getNetwork_UserField5().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("NIM_ProviderReferenceID"))

	     {
                    if (this.getNIM_ProviderReferenceID().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("NIM_ProviderNPI"))

	     {
                    if (this.getNIM_ProviderNPI().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("NIM_ProviderTaxID"))

	     {
                    if (this.getNIM_ProviderTaxID().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("NIM_ProviderMedicalLicense"))

	     {
                    if (this.getNIM_ProviderMedicalLicense().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("NIM_ProviderFirstName"))

	     {
                    if (this.getNIM_ProviderFirstName().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("NIM_ProviderLastName"))

	     {
                    if (this.getNIM_ProviderLastName().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("NIM_ProviderPhoneNumber"))

	     {
                    if (this.getNIM_ProviderPhoneNumber().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("NIM_ProviderFaxNumber"))

	     {
                    if (this.getNIM_ProviderFaxNumber().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("NIM_ProviderEmail"))

	     {
                    if (this.getNIM_ProviderEmail().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("NIM_ProviderCompany"))

	     {
                    if (this.getNIM_ProviderCompany().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("NIM_ProviderAddress1"))

	     {
                    if (this.getNIM_ProviderAddress1().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("NIM_ProviderAddress2"))

	     {
                    if (this.getNIM_ProviderAddress2().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("NIM_ProviderCity"))

	     {
                    if (this.getNIM_ProviderCity().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("NIM_ProviderState"))

	     {
                    if (this.getNIM_ProviderState().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("NIM_ProviderZip"))

	     {
                    if (this.getNIM_ProviderZip().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("NIM_ProviderMatchFound"))

	     {
                    if (this.getNIM_ProviderMatchFound().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("NIM_ProviderMatchAcceptsRetro"))

	     {
                    if (this.getNIM_ProviderMatchAcceptsRetro().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("NIM_ProviderMatchPositiveSavings"))

	     {
                    if (this.getNIM_ProviderMatchPositiveSavings().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("NIM_AcceptClaim"))

	     {
                    if (this.getNIM_AcceptClaim().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("NIM_DateAccepted"))

	     {
                    if (this.getNIM_DateAccepted().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("NIM_UserAcceptedID"))

	     {
                    if (this.getNIM_UserAcceptedID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("NIM_DateExported"))

	     {
                    if (this.getNIM_DateExported().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("NIM_EncounterID"))

	     {
                    if (this.getNIM_EncounterID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("NIM_ContractProviderCost"))

	     {
                    if (this.getNIM_ContractProviderCost().equals(new Double("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("NIM_FinalProviderCost"))

	     {
                    if (this.getNIM_FinalProviderCost().equals(new Double("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("NIM_PayerAllow"))

	     {
                    if (this.getNIM_PayerAllow().equals(new Double("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	    }
	    catch(Exception e2)
	    {}
	    return myVal;
	}

    public boolean isExpired(String fieldN,int daysVector)
    {
        return isExpired(fieldN,new java.util.Date(),daysVector);
    }

    public boolean isExpired(String fieldN,java.util.Date refDate,int daysVector)
    {
	     boolean myVal = false;
	     try
	     {
        java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat("yyyy-MM-dd");

        if (fieldN.equals("UniqueCreateDate"))

	     {
                    if (this.isExpired(this.getUniqueCreateDate(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

        if (fieldN.equals("UniqueModifyDate"))

	     {
                    if (this.isExpired(this.getUniqueModifyDate(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

        if (fieldN.equals("DateCaseOpened"))

	     {
                    if (this.isExpired(this.getDateCaseOpened(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

        if (fieldN.equals("DateOfInjury"))

	     {
                    if (this.isExpired(this.getDateOfInjury(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

        if (fieldN.equals("PatientDOB"))

	     {
                    if (this.isExpired(this.getPatientDOB(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

        if (fieldN.equals("DateofService"))

	     {
                    if (this.isExpired(this.getDateofService(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

        if (fieldN.equals("Claimdate"))

	     {
                    if (this.isExpired(this.getClaimdate(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

        if (fieldN.equals("MMI"))

	     {
                    if (this.isExpired(this.getMMI(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

        if (fieldN.equals("NIM_DateAccepted"))

	     {
                    if (this.isExpired(this.getNIM_DateAccepted(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

        if (fieldN.equals("NIM_DateExported"))

	     {
                    if (this.isExpired(this.getNIM_DateExported(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

	    }
	    catch(Exception e2)
	    {}
	    return myVal;
	}

    public java.util.Date getDateField(String fieldN)
    {
	     java.util.Date myVal = null;
	     try
	     {
        java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat("yyyy-MM-dd");

        if (fieldN.equals("UniqueCreateDate"))

	     {
             myVal = this.getUniqueCreateDate();
        }

        if (fieldN.equals("UniqueModifyDate"))

	     {
             myVal = this.getUniqueModifyDate();
        }

        if (fieldN.equals("DateCaseOpened"))

	     {
             myVal = this.getDateCaseOpened();
        }

        if (fieldN.equals("DateOfInjury"))

	     {
             myVal = this.getDateOfInjury();
        }

        if (fieldN.equals("PatientDOB"))

	     {
             myVal = this.getPatientDOB();
        }

        if (fieldN.equals("DateofService"))

	     {
             myVal = this.getDateofService();
        }

        if (fieldN.equals("Claimdate"))

	     {
             myVal = this.getClaimdate();
        }

        if (fieldN.equals("MMI"))

	     {
             myVal = this.getMMI();
        }

        if (fieldN.equals("NIM_DateAccepted"))

	     {
             myVal = this.getNIM_DateAccepted();
        }

        if (fieldN.equals("NIM_DateExported"))

	     {
             myVal = this.getNIM_DateExported();
        }

	    }
	    catch(Exception e2)
	    {}
	    return myVal;
	}
	public boolean runFieldSecurity = false;
	public Integer UserSecurityID = null;
   public Integer iSecurityCheck = null;
}    // End of bltNIM3_RetroWorklist class definition
