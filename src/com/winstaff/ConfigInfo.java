package com.winstaff;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Properties;

public class ConfigInfo {

	public String propertyFile = "/var/lib/tomcat6/webapps/ConfigInfo.properties";
	public static String sDBType;
	public static String sJDBCDriverName;

	public String sURL;
	public String sMaster;
	public String sStandby;

	public String getsURL() {
		return sURL;
	}

	public void initURL() {
		
		System.out.println(propertyFile);
		
		try {
			Properties prop = new Properties();
			prop.load(new FileInputStream(propertyFile));
			sURL = prop.getProperty("sURL");
			sMaster = prop.getProperty("sMaster");
			sStandby = prop.getProperty("sStandby");
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		
	}

	/**
	 * Read properties file in the constructor.
	 */
	 public ConfigInfo() {
		 super();
		 initURL();
	 }

	// Normal case
	public static boolean test_syncMasterToStandby = true;
	
	// Recovery case
	public static boolean test_syncStandbyToMaster = false;
	
	// Bring up standby as 'new' master
	public static boolean test_bringUpStandbyAsMaster = false;
	
	/**
	 * Main tester method.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
	
		ConfigInfo cf = new ConfigInfo();
		
		// bring up the standby as a writable temporary master.  you will need to sync-back from the slave to the master
		// before booting both databases in the 'normal' master/slave configuration
		
		// run this test pretending the master is not available.
		if (test_bringUpStandbyAsMaster) {
			cf.bringUpStandbyAsMaster();
		}
		
		if (test_syncStandbyToMaster) {
			cf.syncStandbyToMaster();
		}
		
		// normal procedure for copying master files to standby and bring up the standby in a slave mode to the master.
		if (test_syncMasterToStandby) {
			cf.syncMasterToStandby();
		}
		
	}
	
	/**
	 * Restart the standby as a read/writable database. If this is successful
	 * the recovery.conf file is renamed to recovery.done
	 * Shell into the standby like you were doing this at a prompt.
	 * 
	 * You will have to rename this done file back to its original name.
	 * 
	 */
	public void bringUpStandbyAsMaster() {

		String ssh = "ssh root@"+sStandby+" ";
		String bringUpStandbyAsMaster = " echo \"standby_mode = 'off'\" >> /var/lib/postgresql/9.1/main/recovery.conf ";
		String restart = " service postgresql restart ";

		bashCommand(ssh + bringUpStandbyAsMaster);
		bashCommand(ssh + restart);
	}
	
	/**
	 * This is the normal procedure for copying master files to standby
	 * and bring up the standby in a slave mode to the master.
	 */
	public void syncMasterToStandby () {
		stopMaster();
		stopStandby();
		rsyncMasterToStandby();
		startStandby();
		startMaster();		
	}
	
	/**
	 * This rsync from master to standby (slave) is done with keys on both the master and slave.
	 * Here is how this is done: https://blogs.oracle.com/jkini/entry/how_to_scp_scp_and
	 *
	 */
	public void rsyncMasterToStandby() {	
		String ssh = "ssh root@"+sMaster+" ";			
		String cmd = "rsync --rsh='ssh -p22'  -av --exclude pg_xlog --exclude postgresql.conf /var/lib/postgresql/9.1/main/* "+sStandby+":/var/lib/postgresql/9.1/main/";
		bashCommand(ssh + cmd);				
	}
	
	/**
	 * 
	 * This method is performed after you are done using the standby as a master and want to resume using the original master
	 * with the standby in read-only mode.  Since we were using the standby as a master the original master was shutdown.
	 * Now the standby actually has more data (and up-to-date data) then the master has, so we need to copy from the standby to the master,
	 * and once again enter the original master-slave configuration.
	 * 
	 * 1. Turn off both master and standby (if not already off)
	 * 2. Copy from standby to Master.
	 * 3. Bring up standby in standby mode.
	 * 4. Bring up master.
	 * 
	 */
	public void syncStandbyToMaster() {
		stopMaster();
		stopStandby();	
		rsyncStandbyToMaster();
		recoverConfigurationFile();
		startStandby();
		startMaster();	
	}
	
	/**
	 * Copy files from standby back to the master.
	 * You would do this if you had been using the standby as a temporary master and wanted to bring back the master once it was updated.
	 */
	public void rsyncStandbyToMaster() {	
		String ssh = "ssh root@"+sStandby+" ";			
		String cmd = "rsync --rsh='ssh -p22'  -av --exclude pg_xlog --exclude postgresql.conf /var/lib/postgresql/9.1/main/* "+sMaster+":/var/lib/postgresql/9.1/main/";
		bashCommand(ssh + cmd);				
	}

	public void recoverConfigurationFile () {
		
		// rename recovery.done file back to recovery.conf
		String ssh = "ssh root@"+sStandby+" ";
		String move = " mv /var/lib/postgresql/9.1/main/recovery.done /var/lib/postgresql/9.1/main/recovery.conf ";
		bashCommand(ssh + move);
		
		// turn standby mode back on
		String standbyMode = " echo \"standby_mode = 'on'\" >> /var/lib/postgresql/9.1/main/recovery.conf ";		
		bashCommand(ssh + standbyMode);
	}
	
	public void startMaster () {
		String ssh = "ssh root@"+sMaster+" ";
		String start = " service postgresql start ";
		bashCommand(ssh + start);		
	}

	public void stopMaster () {		
		String ssh = "ssh root@"+sMaster+" ";
		String stop = " service postgresql stop ";
		bashCommand(ssh + stop);		
	}

	public void startStandby () {
		String ssh = "ssh root@"+sStandby+" ";
		String start = " service postgresql start ";
		bashCommand(ssh + start);			
	}

	public void stopStandby () {
		String ssh = "ssh root@"+sStandby+" ";
		String stop = " service postgresql stop ";
		bashCommand(ssh + stop);		
	}
	
	public void bashCommand(String cmd) {
		try {
			System.out.println("Executing " + cmd + " on remote host.");
			Process proc = Runtime.getRuntime().exec(cmd);
			BufferedReader read = new BufferedReader(new InputStreamReader(
					proc.getInputStream()));
			try {
				proc.waitFor();
			} catch (InterruptedException e) {
				System.out.println(e.getMessage());
			}
			while (read.ready()) {
				System.out.println(read.readLine());
			}
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
	}

	public void determineMaster() {
		// try {
		// java.sql.Connection cCon = java.sql.DriverManager.getConnection(
		// cf.getsURL(),
		// // ConfigurationInformation.sURL,
		// ConfigurationInformation.sUser,
		// ConfigurationInformation.sPassword);
		//
		// // Replace any single qoute with 2 single qoutes
		// if (source.contains("'")) {
		// source = source.replaceAll("'", "''");
		// }
		//
		// String stmt =
		// "SELECT document FROM tnim3_document WHERE filename = '"
		// + source + "'";
		// System.out.println("stmt= " + stmt);
		// PreparedStatement ps = cCon.prepareStatement(stmt);
		// ResultSet rs = ps.executeQuery();
		// while (rs.next()) {
		// byte[] imgBytes = rs.getBytes(1);
		// FileOutputStream out = new FileOutputStream(target);
		// try {
		// out.write(imgBytes);
		// } finally {
		// out.close();
		// }
		// }
		// rs.close();
		// ps.close();
		// cCon.close();
		// } catch (Exception eeee) {
		// System.out.println("File INSERT Error: " + eeee.toString());
		// }
	}

	public static String sUser;
	public static String sPassword;
	public static String sTomcat;
	public static String serverName;
	public static int NIM_DB_NID_PARENT_PAYER;

	public static boolean bRequiredByGroup;
	public static boolean bExpiredByGroup;
	public static boolean bSecurityByGroup;

	public static boolean bRunAudit;
	public static boolean bInDebugMode;
	public static boolean bInDebugModeShowFldSets;
	public static boolean bCreateLogBasic;
	public static boolean bCreateLogFull;

	public static String sDisplayHidden;
	public static String sDateTimeFormat;
	public static String sDefaultDateValue;
	public static String sDefaultIntegerValue;
	public static String sDefaultBooleanValue;
	public static String sDefaultStringValue;
	public static String sDefaultDoubleValue;

	public static String sLogFolder = "/var/lib/" + sTomcat + "/webapps/logs/";
	public static String sUploadFolderDirectory = "/var/lib/" + sTomcat
			+ "/webapps/nimdox/";
	public static String sDICOMFolderDirectory = "/var/lib/" + sTomcat
			+ "/webapps/DICOM/";

	public static String sLinkedPDFDirectory;
	public static String sUnLinkedPDFDirectory;
	public static String serverIP;
	public static String applicationFolder;
	public static String applicationVersion;
	public static long sessionTimeOutInMillis;

	public static String SMTPServer;
	public static String ErrorFromAddress;
	public static String ErrorToAddress;
	public static String FaxFromAddress;
	public static String SMTPServerHost;
	public static String SMTPServerHostPort;
	public static String SMTPServerUser;
	public static String SMTPServerPass;
	public static String ReturnEmail;

	public void test_variableLoading(String[] args) {

		Properties prop = new Properties();

		try {

			prop.load(new FileInputStream(propertyFile));

			sDBType = prop.getProperty("sDBType");
			sJDBCDriverName = prop.getProperty("sJDBCDriverName");
			// sURL = prop.getProperty("sURL");
			sUser = prop.getProperty("sUser");
			sPassword = prop.getProperty("sPassword");
			sTomcat = prop.getProperty("sTomcat");
			serverName = prop.getProperty("serverName");
			NIM_DB_NID_PARENT_PAYER = new Integer(
					prop.getProperty("NIM_DB_NID_PARENT_PAYER")).intValue();

			bRequiredByGroup = Boolean.parseBoolean(prop
					.getProperty("bRequiredByGroup"));
			bExpiredByGroup = Boolean.parseBoolean(prop
					.getProperty("bExpiredByGroup"));
			bSecurityByGroup = Boolean.parseBoolean(prop
					.getProperty("bSecurityByGroup"));

			bRunAudit = Boolean.parseBoolean(prop.getProperty("bRunAudit"));
			bInDebugMode = Boolean.parseBoolean(prop
					.getProperty("bInDebugMode"));
			bInDebugModeShowFldSets = Boolean.parseBoolean(prop
					.getProperty("bInDebugModeShowFldSets"));
			bCreateLogBasic = Boolean.parseBoolean(prop
					.getProperty("bCreateLogBasic"));
			bCreateLogFull = Boolean.parseBoolean(prop
					.getProperty("bCreateLogFull"));

			sDisplayHidden = prop.getProperty("sDisplayHidden");
			sDateTimeFormat = prop.getProperty("sDateTimeFormat");
			sDefaultDateValue = prop.getProperty("sDefaultDateValue");
			sDefaultIntegerValue = prop.getProperty("sDefaultIntegerValue");
			sDefaultBooleanValue = prop.getProperty("sDefaultBooleanValue");
			sDefaultStringValue = prop.getProperty("sDefaultStringValue");
			sDefaultDoubleValue = prop.getProperty("sDefaultDoubleValue");

			sLinkedPDFDirectory = prop.getProperty("sLinkedPDFDirectory");
			sUnLinkedPDFDirectory = prop.getProperty("sUnLinkedPDFDirectory");
			serverIP = prop.getProperty("serverIP");
			applicationFolder = prop.getProperty("applicationFolder");
			applicationVersion = prop.getProperty("applicationVersion");
			sessionTimeOutInMillis = new Long(
					prop.getProperty("sessionTimeOutInMillis")).longValue();

			SMTPServer = prop.getProperty("SMTPServer");
			ErrorFromAddress = prop.getProperty("ErrorFromAddress");
			ErrorToAddress = prop.getProperty("ErrorToAddress");
			FaxFromAddress = prop.getProperty("FaxFromAddress");
			SMTPServerHost = prop.getProperty("SMTPServerHost");
			SMTPServerHostPort = prop.getProperty("SMTPServerHostPort");
			SMTPServerUser = prop.getProperty("SMTPServerUser");
			SMTPServerPass = prop.getProperty("SMTPServerPass");
			ReturnEmail = prop.getProperty("ReturnEmail");

		} catch (IOException ex) {
			ex.printStackTrace();
		}
		printOut();
	}

	public static void printOut() {

		System.out.println("sDBType = " + sDBType);
		System.out.println("sJDBCDriverName = " + sJDBCDriverName);
		// System.out.println("sURL = "+sURL);
		System.out.println("sUser = " + sUser);
		System.out.println("sPassword = " + sPassword);
		System.out.println("sTomcat = " + sTomcat);
		System.out.println("serverName = " + serverName);
		System.out.println("NIM_DB_NID_PARENT_PAYER = "
				+ NIM_DB_NID_PARENT_PAYER);
		System.out.println("bRequiredByGroup = " + bRequiredByGroup);
		System.out.println("bExpiredByGroup = " + bExpiredByGroup);
		System.out.println("bSecurityByGroup = " + bSecurityByGroup);
		System.out.println("bRunAudit = " + bRunAudit);
		System.out.println("bInDebugMode = " + bInDebugMode);
		System.out.println("bInDebugModeShowFldSets = "
				+ bInDebugModeShowFldSets);
		System.out.println("bCreateLogBasic = " + bCreateLogBasic);
		System.out.println("bCreateLogFull = " + bCreateLogFull);
		System.out.println("sDisplayHidden = " + sDisplayHidden);
		System.out.println("sDateTimeFormat = " + sDateTimeFormat);
		System.out.println("sDefaultDateValue = " + sDefaultDateValue);
		System.out.println("sDefaultIntegerValue = " + sDefaultIntegerValue);
		System.out.println("sDefaultBooleanValue = " + sDefaultBooleanValue);
		System.out.println("sDefaultStringValue = " + sDefaultStringValue);
		System.out.println("sLinkedPDFDirectory = " + sLinkedPDFDirectory);
		System.out.println("sUnLinkedPDFDirectory = " + sUnLinkedPDFDirectory);
		System.out.println("serverIP = " + serverIP);
		System.out.println("applicationFolder = " + applicationFolder);
		System.out.println("applicationVersion = " + applicationVersion);
		System.out
				.println("sessionTimeOutInMillis = " + sessionTimeOutInMillis);
		System.out.println("SMTPServer = " + SMTPServer);
		System.out.println("ErrorFromAddress = " + ErrorFromAddress);
		System.out.println("ErrorToAddress = " + ErrorToAddress);
		System.out.println("FaxFromAddress = " + FaxFromAddress);
		System.out.println("SMTPServerHost = " + SMTPServerHost);
		System.out.println("SMTPServerHostPort = " + SMTPServerHostPort);
		System.out.println("SMTPServerUser = " + SMTPServerUser);
		System.out.println("SMTPServerPass = " + SMTPServerPass);
		System.out.println("ReturnEmail = " + ReturnEmail);

	}

}
