

package com.winstaff;

import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public class dbtServiceStatusLI extends Object implements InttServiceStatusLI
{

        db_NewBase    dbnbDB;

    public dbtServiceStatusLI()
    {
        dbnbDB = new db_NewBase( "tServiceStatusLI", "ServiceStatusID" );

    }    // End of default constructor

    public dbtServiceStatusLI( Integer iNewID )
    {
        dbnbDB = new db_NewBase( "tServiceStatusLI", "ServiceStatusID" );
        setUniqueID( iNewID );
    }    // End of Constructor knowing an ID

    public void commitData() throws SQLException
    {
            //no longer trapping/ignoring errors 2011-03-17
            dbnbDB.commitData();
    }    // End of commitData()

    public void commitDataForced() throws SQLException
    {
        dbnbDB.commitDataForced();
    }    // End of commitData()

    public Integer getUniqueID()
    {
        return dbnbDB.getUniqueID();
    }    // End of getUniqueID()

    public void setUniqueID( Integer iNewID )
    {
        dbnbDB.setUniqueID( iNewID );
    }    // End of setUniqueID()

    public void setModifyComments( String sNew )
    {
        dbnbDB.setModifyComments( sNew );
    }    // End of setUniqueID()


    public void setServiceStatusID(Integer newValue)
    {
                dbnbDB.setFieldData( "ServiceStatusID", newValue.toString() );
    }

    public Integer getServiceStatusID()
    {
        String           sValue = dbnbDB.getFieldData( "ServiceStatusID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setField(String fieldN, Object fieldV)
    {DebugLogger.println("setField Method not implemented yet in class!");}

    public String getFieldType(String fieldN)
    {return null;}


    public void setServiceStatusShort(String newValue)
    {
                dbnbDB.setFieldData( "ServiceStatusShort", newValue.toString() );
    }

    public String getServiceStatusShort()
    {
        String           sValue = dbnbDB.getFieldData( "ServiceStatusShort" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setServiceStatusLong(String newValue)
    {
                dbnbDB.setFieldData( "ServiceStatusLong", newValue.toString() );
    }

    public String getServiceStatusLong()
    {
        String           sValue = dbnbDB.getFieldData( "ServiceStatusLong" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

}    // End of bltServiceStatusLI class definition
