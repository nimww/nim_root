

package com.winstaff;

import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public class dbtVoidLeakReasonLI extends Object implements InttVoidLeakReasonLI
{

        db_NewBase    dbnbDB;

    public dbtVoidLeakReasonLI()
    {
        dbnbDB = new db_NewBase( "tVoidLeakReasonLI", "VoidLeakReasonID" );

    }    // End of default constructor

    public dbtVoidLeakReasonLI( Integer iNewID )
    {
        dbnbDB = new db_NewBase( "tVoidLeakReasonLI", "VoidLeakReasonID" );
        setUniqueID( iNewID );
    }    // End of Constructor knowing an ID

    public void commitData() throws SQLException
    {
            //no longer trapping/ignoring errors 2011-03-17
            dbnbDB.commitData();
    }    // End of commitData()

    public void commitDataForced() throws SQLException
    {
        dbnbDB.commitDataForced();
    }    // End of commitData()

    public Integer getUniqueID()
    {
        return dbnbDB.getUniqueID();
    }    // End of getUniqueID()

    public void setUniqueID( Integer iNewID )
    {
        dbnbDB.setUniqueID( iNewID );
    }    // End of setUniqueID()

    public void setModifyComments( String sNew )
    {
        dbnbDB.setModifyComments( sNew );
    }    // End of setUniqueID()


    public void setVoidLeakReasonID(Integer newValue)
    {
                dbnbDB.setFieldData( "VoidLeakReasonID", newValue.toString() );
    }

    public Integer getVoidLeakReasonID()
    {
        String           sValue = dbnbDB.getFieldData( "VoidLeakReasonID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setField(String fieldN, Object fieldV)
    {DebugLogger.println("setField Method not implemented yet in class!");}

    public String getFieldType(String fieldN)
    {return null;}


    public void setVoidLeakReasonShort(String newValue)
    {
                dbnbDB.setFieldData( "VoidLeakReasonShort", newValue.toString() );
    }

    public String getVoidLeakReasonShort()
    {
        String           sValue = dbnbDB.getFieldData( "VoidLeakReasonShort" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setVoidLeakReasonLong(String newValue)
    {
                dbnbDB.setFieldData( "VoidLeakReasonLong", newValue.toString() );
    }

    public String getVoidLeakReasonLong()
    {
        String           sValue = dbnbDB.getFieldData( "VoidLeakReasonLong" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

}    // End of bltVoidLeakReasonLI class definition
