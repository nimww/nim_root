
package com.winstaff;
/*
 * bltNIM3_Modality_List.java
 *
 * Created: Wed Nov 04 14:37:41 PST 2009
 */



import java.sql.*;




/**
 *
 * @author  Scott Ellis via listClassGenCode
 * @version 2.5
 */
public class bltNIM3_Modality_List extends db_BaseRelationTable
{
    /** Default constructor
    */    
    public bltNIM3_Modality_List ( Integer iPracticeID )
    {
        super( "tNIM3_Modality", "ModalityID", "ModalityID", "PracticeID", iPracticeID );
    }   // End of bltNIM3_Modality_List()
    

    public void setField(String fieldN, Object fieldV)
    {DebugLogger.println("setField Method not implemented yet in classbltNIM3_Modality_List!");}

    public String getFieldType(String fieldN)
    {return null;}

    public bltNIM3_Modality_List ( Integer iPracticeID, String extraWhere, String OrderBy )
    {
        super( "tNIM3_Modality", "ModalityID", "ModalityID", "PracticeID", iPracticeID, extraWhere,OrderBy );
    }   // End of bltNIM3_Modality_List()
     /** Method pull information from the result set and load into class attributes.
    * @param sFieldList Comma delimited list of fields for which data has been retrieved.
    * @param rs Result set from which to pull the data from.
    * @throws SQLException Passes on SQLException
    */      
    protected void transferData( ResultSet rs ) throws SQLException
    {
        while ( rs.next()  )
        {
            Integer         iValue              = new Integer( rs.getString( sLinkFieldName ) );
            Integer         iLinkRecordID       = new Integer( rs.getString( sUniqueIDFieldName ) );
            Object          oReferenceObject;
            ListElement     leNewElement;
            
            oReferenceObject    = new bltNIM3_Modality( iValue );
            
            leNewElement        = new ListElement( iLinkRecordID, oReferenceObject );
            
            vList.add( leNewElement );

        }
    }   // End of transferData()
}   // End of bltNIM3_Modality_List class

