

package com.winstaff;

import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public class dbtNIM3_Appointment extends Object implements InttNIM3_Appointment
{

        db_NewBase    dbnbDB;

    public dbtNIM3_Appointment()
    {
        dbnbDB = new db_NewBase( "tNIM3_Appointment", "AppointmentID" );

    }    // End of default constructor

    public dbtNIM3_Appointment( Integer iNewID )
    {
        dbnbDB = new db_NewBase( "tNIM3_Appointment", "AppointmentID" );
        setUniqueID( iNewID );
    }    // End of Constructor knowing an ID

    public void commitData() throws SQLException
    {
            //no longer trapping/ignoring errors 2011-03-17
            dbnbDB.commitData();
    }    // End of commitData()

    public void commitDataForced() throws SQLException
    {
        dbnbDB.commitDataForced();
    }    // End of commitData()

    public Integer getUniqueID()
    {
        return dbnbDB.getUniqueID();
    }    // End of getUniqueID()

    public void setUniqueID( Integer iNewID )
    {
        dbnbDB.setUniqueID( iNewID );
    }    // End of setUniqueID()

    public void setModifyComments( String sNew )
    {
        dbnbDB.setModifyComments( sNew );
    }    // End of setUniqueID()


    public void setAppointmentID(Integer newValue)
    {
                dbnbDB.setFieldData( "AppointmentID", newValue.toString() );
    }

    public Integer getAppointmentID()
    {
        String           sValue = dbnbDB.getFieldData( "AppointmentID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setField(String fieldN, Object fieldV)
    {DebugLogger.println("setField Method not implemented yet in classNIM3_Appointment!");}

    public String getFieldType(String fieldN)
    {return null;}


    public void setUniqueCreateDate(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "UniqueCreateDate", formatter.format( newValue ) );
    }

    public Date getUniqueCreateDate()
    {
        String           sValue = dbnbDB.getFieldData( "UniqueCreateDate" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setUniqueModifyDate(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "UniqueModifyDate", formatter.format( newValue ) );
    }

    public Date getUniqueModifyDate()
    {
        String           sValue = dbnbDB.getFieldData( "UniqueModifyDate" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setUniqueModifyComments(String newValue)
    {
                dbnbDB.setFieldData( "UniqueModifyComments", newValue.toString() );
    }

    public String getUniqueModifyComments()
    {
        String           sValue = dbnbDB.getFieldData( "UniqueModifyComments" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setProviderID(Integer newValue)
    {
                dbnbDB.setFieldData( "ProviderID", newValue.toString() );
    }

    public Integer getProviderID()
    {
        String           sValue = dbnbDB.getFieldData( "ProviderID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setICID(Integer newValue)
    {
                dbnbDB.setFieldData( "ICID", newValue.toString() );
    }

    public Integer getICID()
    {
        String           sValue = dbnbDB.getFieldData( "ICID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setiStatus(Integer newValue)
    {
                dbnbDB.setFieldData( "iStatus", newValue.toString() );
    }

    public Integer getiStatus()
    {
        String           sValue = dbnbDB.getFieldData( "iStatus" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setAppointmentTime(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "AppointmentTime", formatter.format( newValue ) );
    }

    public Date getAppointmentTime()
    {
        String           sValue = dbnbDB.getFieldData( "AppointmentTime" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setActualTime(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "ActualTime", formatter.format( newValue ) );
    }

    public Date getActualTime()
    {
        String           sValue = dbnbDB.getFieldData( "ActualTime" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setAppointmentConfirmation(String newValue)
    {
                dbnbDB.setFieldData( "AppointmentConfirmation", newValue.toString() );
    }

    public String getAppointmentConfirmation()
    {
        String           sValue = dbnbDB.getFieldData( "AppointmentConfirmation" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setScheduler(String newValue)
    {
                dbnbDB.setFieldData( "Scheduler", newValue.toString() );
    }

    public String getScheduler()
    {
        String           sValue = dbnbDB.getFieldData( "Scheduler" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setInitialEncounterID(Integer newValue)
    {
                dbnbDB.setFieldData( "InitialEncounterID", newValue.toString() );
    }

    public Integer getInitialEncounterID()
    {
        String           sValue = dbnbDB.getFieldData( "InitialEncounterID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setScheduler_UserID(Integer newValue)
    {
                dbnbDB.setFieldData( "Scheduler_UserID", newValue.toString() );
    }

    public Integer getScheduler_UserID()
    {
        String           sValue = dbnbDB.getFieldData( "Scheduler_UserID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setProviderConfirmSet(Integer newValue)
    {
                dbnbDB.setFieldData( "ProviderConfirmSet", newValue.toString() );
    }

    public Integer getProviderConfirmSet()
    {
        String           sValue = dbnbDB.getFieldData( "ProviderConfirmSet" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setProviderConfirmSet_UserID(Integer newValue)
    {
                dbnbDB.setFieldData( "ProviderConfirmSet_UserID", newValue.toString() );
    }

    public Integer getProviderConfirmSet_UserID()
    {
        String           sValue = dbnbDB.getFieldData( "ProviderConfirmSet_UserID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setProviderConfirmDate(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "ProviderConfirmDate", formatter.format( newValue ) );
    }

    public Date getProviderConfirmDate()
    {
        String           sValue = dbnbDB.getFieldData( "ProviderConfirmDate" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setProviderAppointmentConfirmation(String newValue)
    {
                dbnbDB.setFieldData( "ProviderAppointmentConfirmation", newValue.toString() );
    }

    public String getProviderAppointmentConfirmation()
    {
        String           sValue = dbnbDB.getFieldData( "ProviderAppointmentConfirmation" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setProviderConfirmShow(Integer newValue)
    {
                dbnbDB.setFieldData( "ProviderConfirmShow", newValue.toString() );
    }

    public Integer getProviderConfirmShow()
    {
        String           sValue = dbnbDB.getFieldData( "ProviderConfirmShow" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setProviderConfirmShow_UserID(Integer newValue)
    {
                dbnbDB.setFieldData( "ProviderConfirmShow_UserID", newValue.toString() );
    }

    public Integer getProviderConfirmShow_UserID()
    {
        String           sValue = dbnbDB.getFieldData( "ProviderConfirmShow_UserID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setProviderConfirmShowDate(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "ProviderConfirmShowDate", formatter.format( newValue ) );
    }

    public Date getProviderConfirmShowDate()
    {
        String           sValue = dbnbDB.getFieldData( "ProviderConfirmShowDate" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setProviderNotes(String newValue)
    {
                dbnbDB.setFieldData( "ProviderNotes", newValue.toString() );
    }

    public String getProviderNotes()
    {
        String           sValue = dbnbDB.getFieldData( "ProviderNotes" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setVoidNotes(String newValue)
    {
                dbnbDB.setFieldData( "VoidNotes", newValue.toString() );
    }

    public String getVoidNotes()
    {
        String           sValue = dbnbDB.getFieldData( "VoidNotes" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setPatientConfirmSet(Integer newValue)
    {
                dbnbDB.setFieldData( "PatientConfirmSet", newValue.toString() );
    }

    public Integer getPatientConfirmSet()
    {
        String           sValue = dbnbDB.getFieldData( "PatientConfirmSet" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setPatientConfirmSet_UserID(Integer newValue)
    {
                dbnbDB.setFieldData( "PatientConfirmSet_UserID", newValue.toString() );
    }

    public Integer getPatientConfirmSet_UserID()
    {
        String           sValue = dbnbDB.getFieldData( "PatientConfirmSet_UserID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setPatientConfirmDate(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "PatientConfirmDate", formatter.format( newValue ) );
    }

    public Date getPatientConfirmDate()
    {
        String           sValue = dbnbDB.getFieldData( "PatientConfirmDate" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setComments(String newValue)
    {
                dbnbDB.setFieldData( "Comments", newValue.toString() );
    }

    public String getComments()
    {
        String           sValue = dbnbDB.getFieldData( "Comments" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

}    // End of bltNIM3_Appointment class definition
