package com.winstaff;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.Calendar;
import java.util.Random;

/**
 * 
 * This class performs a dummy update to a temporary table. This is currently
 * being done to allow the postgresql binary replication to "stay alive". This
 * class is called from a cron entry with an execution frequency set in the cron entry.
 * 
 */
public class StandbyPinger {

	ConfigurationInformation config = new ConfigurationInformation();

	private String sClassIdentifier;

	static public void main(String args[]) {
		System.out.println("\nStarting StandbyPinger");
		StandbyPinger pinger = new StandbyPinger();
		pinger.ping();
		System.out.println("\nFinished.");
	}

	private void ping() {
		
        Calendar cal = Calendar.getInstance();
        int day = cal.get(Calendar.DATE);
        int month = cal.get(Calendar.MONTH) + 1;
        int year = cal.get(Calendar.YEAR);
        int hour = cal.get(Calendar.HOUR);
        int minute = cal.get(Calendar.MINUTE);
        int seconds = cal.get(Calendar.SECOND);
   
        String time = month+"-"+day+"-"+year+":"+hour+":"+minute+":"+seconds;
        
		try {
			Random generator = new Random();
			int r = generator.nextInt();
			update(r+" Updated on: "+time);
			verify(r+" Updated on: "+time);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void update(String in) throws Exception {
		try {
			java.sql.Connection cCon = java.sql.DriverManager.getConnection(
					ConfigurationInformation.sURL,
					ConfigurationInformation.sUser,
					ConfigurationInformation.sPassword);
			java.sql.PreparedStatement ps = cCon
					.prepareStatement("UPDATE \"public\".\"ttemp\" SET text = '"+in+"'");
			this.sClassIdentifier = this.getClass().getName();
			DebugLogger
					.println(sClassIdentifier
							+ ": Query: "
							+ "Updating tText with dummy inserted data for binary replication keep-alive.");
			ps.executeUpdate();
			ps.close();
			cCon.close();
		} catch (Exception eeee) {
			System.out.println("tText keep alive - Error: " + eeee.toString());
		}
	}
	
	
	private void verify(String in) throws Exception {
		
		// Wait for 2 seconds to allow a slow replication.
		Thread.sleep(2000);
		
		try {
			java.sql.Connection cCon = java.sql.DriverManager.getConnection(
					ConfigurationInformation.sStandby_URL,
					ConfigurationInformation.sUser,
					ConfigurationInformation.sPassword);
			java.sql.PreparedStatement ps = cCon
					.prepareStatement("select * from public.ttemp where text = '"+in+"'");
			this.sClassIdentifier = this.getClass().getName();
			DebugLogger
					.println(sClassIdentifier
							+ ": Query: "
							+ "Checking if Standby has received data update just performed on Master.");
			ps.executeQuery();
			ResultSet rs = ps.getResultSet();
			String result = null;
			try {
			  while (rs.next()) {
			    result = rs.getString(2);
			  }
			} finally {
				  rs.close();
			}
				
			if (in.compareTo(result)!=0) {
				System.out.println("** FATAL ERROR ***");
				System.out.println("in = "+in);
				System.out.println("result = "+result);
				
			} else {
				System.out.println("-- Standby OK --");		
				System.out.println("in = "+in);
				System.out.println("result = "+result);				
			}
			ps.close();
			cCon.close();
		} catch (Exception eeee) {
			System.out.println("tText stay alive - Error: " + eeee.toString());
		}
	}	
	

}