package com.winstaff;

import java.io.DataOutputStream;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Vector;

/**
 * Created with IntelliJ IDEA.
 * User: Scott
 * Date: 9/29/13
 * Time: 5:07 PM
 * To change this template use File | Settings | File Templates.
 */
public class NID_Export_Partners {


    static public void main(String args[])
    {
        NID_Export_Partners testMe = new NID_Export_Partners();
        int currentMonth = Calendar.getInstance().get(Calendar.MONTH);
        int currentYear = Calendar.getInstance().get(Calendar.YEAR);
        try
        {
            if (currentMonth==3||currentMonth==6||currentMonth==9)
            {
                //this means it's the beginning of a new quarter and we now care about last quarter
                testMe.runMe(currentYear, currentMonth-3, 3, "QTR-" + currentYear + "-" + Math.round(currentMonth/3));
            }
            if (currentMonth==1)
            {
                //this means it's the beginning of a new quarter and we now care about last quarter whihc is last year 9 to 11
                testMe.runMe(currentYear-1, 9, 3, "QTR-" + (currentYear-1) + "-4");
            }

            //now we run the month program
            if (currentMonth==0)
            {
                //this means it's the beginning of a new quarter and we now care about last quarter whihc is last year 9 to 11
                testMe.runMe(currentYear-1, 11, 1, "MONTH-"+ (currentYear-1) + "-12" );
            } else {
                testMe.runMe(currentYear, currentMonth-1,1, "MONTH-"+ currentYear + "-" + currentMonth );
            }
        } catch (Exception e) {
            System.out.println("Error in Thread: "+ e );
        }
    }



    public static void runMe(int startYear, int startMonth, int goFor, String fSuffix)
    {
        String delim = ",";
        String CS_Version = "NID Partner Payouts 1.0";
        searchDB2 mySDBA = new searchDB2();
        try
        {
            System.out.println("==============================================================================");
            System.out.println("Running Cost Savings Export Version " + CS_Version);
            java.text.SimpleDateFormat displayDateSDF1Full = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1Full);
            System.out.println("Start: " + displayDateSDF1Full.format(PLCUtils.getNowDate(true)));
            //SQL Prepared Statements Update 2011

            String mySQL = "SELECT\n" +
                    "\t\"public\".\"vMQ4_NID_Parter_Payouts\".\"PayerID\",\n" +
                    "\t\"public\".\"vMQ4_NID_Parter_Payouts\".\"PayerName\",\n" +
                    "\tCOUNT(\n" +
                    "\t\t\"public\".\"vMQ4_NID_Parter_Payouts\".isnid\n" +
                    "\t)AS \"iCount\",\n" +
                    "\tSUM(\n" +
                    "\t\t\"public\".\"vMQ4_NID_Parter_Payouts\".\"Service_sum_AllowAmount\"\n" +
                    "\t)AS \"Allow_Total\",\n" +
                    "\tSUM(\n" +
                    "\t\t\"public\".\"vMQ4_NID_Parter_Payouts\".\"Service_sum_PaidOutAmount\"\n" +
                    "\t)AS \"Cost_Total\",\n" +
                    "\tSUM(\n" +
                    "\t\t\"public\".\"vMQ4_NID_Parter_Payouts\".\"Service_sum_AllowAmount\" - \"public\".\"vMQ4_NID_Parter_Payouts\".\"Service_sum_PaidOutAmount\"\n" +
                    "\t)AS \"Gain_Total\",\n" +
                    "\tAVG(\n" +
                    "\t\t\"public\".\"vMQ4_NID_Parter_Payouts\".commissionhighpercentageself\n" +
                    "\t)as \"Partner_Commission_Percentage\",\n" +
                    "\tSUM(\n" +
                    "\t\t\"public\".\"vMQ4_NID_Parter_Payouts\".\"Commission_Self_Amount\"\n" +
                    "\t)as \"Partner_Commission_Total\",\n" +
                    "\t\"public\".\"vMQ4_NID_Parter_Payouts\".\"Parent_PayerID\",\n" +
                    "\t\"public\".\"vMQ4_NID_Parter_Payouts\".\"Parent_PayerName\",\n" +
                    "\tAVG(\n" +
                    "\t\t\"public\".\"vMQ4_NID_Parter_Payouts\".commissionhighpercentageparent \n" +
                    "\t) as \"PartnerParent_Commission_Percentage\",\n" +
                    "\tSUM(\n" +
                    "\t\t\"public\".\"vMQ4_NID_Parter_Payouts\".\"Commission_Parent_Amount\"\n" +
                    "\t) as \"PartnerParent_Commission_Total\"\n" +
                    "FROM\n" +
                    "\t\"public\".\"vMQ4_NID_Parter_Payouts\"\n" +
                    "WHERE\n" +
                    "\t\"public\".\"vMQ4_NID_Parter_Payouts\".\"Encounter_TimeTrack_ReqPaidOut_iMonth\" >= ?\n" +
                    "AND \"public\".\"vMQ4_NID_Parter_Payouts\".\"Encounter_TimeTrack_ReqPaidOut_iMonth\" <(? + ?)\n" +
                    "AND \"public\".\"vMQ4_NID_Parter_Payouts\".\"Encounter_TimeTrack_ReqPaidOut_iYear\" = ?\n" +
                    "GROUP BY\n" +
                    "\t\"public\".\"vMQ4_NID_Parter_Payouts\".\"PayerID\",\n" +
                    "\t\"public\".\"vMQ4_NID_Parter_Payouts\".\"PayerName\",\n" +
                    "\t\"public\".\"vMQ4_NID_Parter_Payouts\".\"Parent_PayerID\",\n" +
                    "\t\"public\".\"vMQ4_NID_Parter_Payouts\".\"Parent_PayerName\"";

            db_PreparedStatementObject myDPSO = new db_PreparedStatementObject(mySQL);
            myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.INT_TYPE,startMonth+""));
            myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.INT_TYPE,startMonth+""));
            myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.INT_TYPE,goFor+""));
            myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.INT_TYPE,startYear+""));
            searchDB2 mySS = new searchDB2();
            java.sql.ResultSet myRS = mySS.executePreparedStatement(myDPSO);

            int totalCnt=0;
            int successCnt=0;
            java.util.Vector<String> myOut = new Vector<String>();
            java.sql.ResultSetMetaData rsmd = myRS.getMetaData();
            String myHeader="PayerID,PayerName,CountAsSelf,TotalAsSelf,CountAsParent,TotalAsParent,NetCount,NetTotal";
//            for (int i=1;i<=rsmd.getColumnCount();i++){
//                if (i>1){
//                    myHeader+= delim;
//                }
//                myHeader+= rsmd.getColumnName(i);
//            }
            System.out.println("");
            System.out.println("Writing File");

            SimpleDateFormat formatter1              = new SimpleDateFormat( "MM_dd_yyyy_hh_mm" );
            SimpleDateFormat formatter2              = new SimpleDateFormat( "yyyy-MM-dd_HH:mm" );
            SimpleDateFormat formatterFN             = new SimpleDateFormat( "yyyy-MM-dd" );
            String              newDateFileNameString   = formatter1.format( PLCUtils.getNowDate(true) );
//            String sFileName = "export/Export2_CSR" + newDateFileNameString + ".txt";
            String sFileName = "export/Export_NID_Partners_" + fSuffix + "_"+ formatterFN.format(new java.util.Date())+".csv";
            DataOutputStream out;
            out = new DataOutputStream( new FileOutputStream( sFileName ) );
            out.writeBytes( fSuffix );
            out.writeBytes( "\n" );
            out.writeBytes( myHeader );
            out.writeBytes( "\n" );

            java.util.Hashtable<Integer,NID_PartnerPayoutObject> finalPayout = new java.util.Hashtable<Integer,NID_PartnerPayoutObject>();
//            while (myRS.next())
//            {
//                for (int i=1;i<=rsmd.getColumnCount();i++){
//                    if (i>1){
//                        out.writeBytes( delim);
//                    }
//                    out.writeBytes( myRS.getString(i) );
//                }
//                out.writeBytes( "\n" );
//            }
            while (myRS.next())
            {
                totalCnt++;
                Integer temp_payerID = myRS.getInt("Payerid");
                String temp_payerName = myRS.getString("PayerName");
                NID_PartnerPayoutObject tempPPO = new NID_PartnerPayoutObject(temp_payerID, temp_payerName);
                if (finalPayout.containsKey(temp_payerID)){
                    tempPPO = finalPayout.get(temp_payerID);
                }
                tempPPO.addToPayerCount_AsSelf(myRS.getInt("iCount"));
                tempPPO.addToPayerTotal_AsSelf( myRS.getDouble("Partner_Commission_Total"));
                finalPayout.put(temp_payerID, tempPPO);

                //repeat for parent
                Integer temp_parentPayerID = myRS.getInt("Parent_PayerID");
                String temp_payerPayerName = myRS.getString("Parent_PayerName");
                NID_PartnerPayoutObject tempPPO_parent = new NID_PartnerPayoutObject(temp_parentPayerID, temp_payerPayerName);
                if (finalPayout.containsKey(temp_parentPayerID)){
                    tempPPO_parent = finalPayout.get(temp_parentPayerID);
                }
                tempPPO_parent.addToPayerCount_AsParent(myRS.getInt("iCount"));
                tempPPO_parent.addToPayerTotal_AsParent( myRS.getDouble("PartnerParent_Commission_Total"));
                finalPayout.put(temp_parentPayerID, tempPPO_parent);
                System.out.println(".." +totalCnt);


            }
            totalCnt = 0;
            for (NID_PartnerPayoutObject myPPO: finalPayout.values()){
                totalCnt++;
                System.out.println("***" +totalCnt);
                out.writeBytes( myPPO.getPayerID().toString());
                out.writeBytes( ",");
                out.writeBytes( myPPO.getPayerName());
                out.writeBytes( ",");
                out.writeBytes( myPPO.getPayerCount_AsSelf().toString());
                out.writeBytes( ",");
                out.writeBytes( myPPO.getPayerTotal_AsSelf().toString());
                out.writeBytes( ",");
                out.writeBytes( myPPO.getPayerCount_AsParent().toString());
                out.writeBytes( ",");
                out.writeBytes( myPPO.getPayerTotal_AsParent().toString());
                out.writeBytes( ",");
                out.writeBytes( myPPO.getPayerCount().toString());
                out.writeBytes( ",");
                out.writeBytes( myPPO.getPayerTotal().toString());
                out.writeBytes( "\n" );
            }
            mySDBA.closeAll();
            out.flush();
            System.out.println("");
            System.out.println("Finish: " + displayDateSDF1Full.format(PLCUtils.getNowDate(true)));
            System.out.println("==============================================================================");
        }
        catch(Exception e)
        {
            System.out.println(e);
        } finally {
            mySDBA.closeAll();
        }

    }






}
