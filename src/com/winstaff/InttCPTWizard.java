

package com.winstaff;

import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public interface InttCPTWizard extends dbTableInterface
{

    public void setCPTWizardID(Integer newValue);
    public Integer getCPTWizardID();
    public void setUniqueCreateDate(Date newValue);
    public Date getUniqueCreateDate();
    public void setUniqueModifyDate(Date newValue);
    public Date getUniqueModifyDate();
    public void setUniqueModifyComments(String newValue);
    public String getUniqueModifyComments();
    public void setModality(String newValue);
    public String getModality();
    public void setSpecial(String newValue);
    public String getSpecial();
    public void setContrast(String newValue);
    public String getContrast();
    public void setBodyPart(String newValue);
    public String getBodyPart();
    public void setOrientation(String newValue);
    public String getOrientation();
    public void setCPT1(String newValue);
    public String getCPT1();
    public void setBP1(String newValue);
    public String getBP1();
    public void setCPT2(String newValue);
    public String getCPT2();
    public void setBP2(String newValue);
    public String getBP2();
    public void setCPT3(String newValue);
    public String getCPT3();
    public void setBP3(String newValue);
    public String getBP3();
    public void setCPT4(String newValue);
    public String getCPT4();
    public void setBP4(String newValue);
    public String getBP4();
    public void setCPT5(String newValue);
    public String getCPT5();
    public void setBP5(String newValue);
    public String getBP5();
    public void setAllowNIM(Integer newValue);
    public Integer getAllowNIM();
    public void setAllowNID(Integer newValue);
    public Integer getAllowNID();
}    // End of bltCPTWizard class definition
