
package com.winstaff;
/*
 * bltPracticeActivity_List_AdminID.java
 *
 * Created: Wed Nov 04 10:57:02 PST 2009
 */



import java.sql.*;




/**
 *
 * @author  Scott Ellis via listClassGenCode
 * @version 2.5
 */
public class bltPracticeActivity_List_AdminID extends db_BaseRelationTable
{
    /** Default constructor
    */    
    public bltPracticeActivity_List_AdminID ( Integer iAdminID )
    {
        super( "tPracticeActivity", "PracticeActivityID", "PracticeActivityID", "AdminID", iAdminID );
    }   // End of bltPracticeActivity_List_AdminID()
    

    public void setField(String fieldN, Object fieldV)
    {DebugLogger.println("setField Method not implemented yet in classbltPracticeActivity_List_AdminID!");}

    public String getFieldType(String fieldN)
    {return null;}

    public bltPracticeActivity_List_AdminID ( Integer iAdminID, String extraWhere, String OrderBy )
    {
        super( "tPracticeActivity", "PracticeActivityID", "PracticeActivityID", "AdminID", iAdminID, extraWhere,OrderBy );
    }   // End of bltPracticeActivity_List_AdminID()
     /** Method pull information from the result set and load into class attributes.
    * @param sFieldList Comma delimited list of fields for which data has been retrieved.
    * @param rs Result set from which to pull the data from.
    * @throws SQLException Passes on SQLException
    */      
    protected void transferData( ResultSet rs ) throws SQLException
    {
        while ( rs.next()  )
        {
            Integer         iValue              = new Integer( rs.getString( sLinkFieldName ) );
            Integer         iLinkRecordID       = new Integer( rs.getString( sUniqueIDFieldName ) );
            Object          oReferenceObject;
            ListElement     leNewElement;
            
            oReferenceObject    = new bltPracticeActivity( iValue );
            
            leNewElement        = new ListElement( iLinkRecordID, oReferenceObject );
            
            vList.add( leNewElement );

        }
    }   // End of transferData()
}   // End of bltPracticeActivity_List_AdminID class

