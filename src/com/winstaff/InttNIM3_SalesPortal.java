

package com.winstaff;

import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public interface InttNIM3_SalesPortal extends dbTableInterface
{

    public void setSalesPortalID(Integer newValue);
    public Integer getSalesPortalID();
    public void setUniqueCreateDate(Date newValue);
    public Date getUniqueCreateDate();
    public void setUniqueModifyDate(Date newValue);
    public Date getUniqueModifyDate();
    public void setUniqueModifyComments(String newValue);
    public String getUniqueModifyComments();
    public void setStatusID(Integer newValue);
    public Integer getStatusID();
    public void setUserID(Integer newValue);
    public Integer getUserID();
    public void setTrackingID(Integer newValue);
    public Integer getTrackingID();
}    // End of bltNIM3_SalesPortal class definition
