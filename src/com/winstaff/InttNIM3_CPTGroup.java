

package com.winstaff;

import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public interface InttNIM3_CPTGroup extends dbTableInterface
{

    public void setCPTGroupID(Integer newValue);
    public Integer getCPTGroupID();
    public void setUniqueCreateDate(Date newValue);
    public Date getUniqueCreateDate();
    public void setUniqueModifyDate(Date newValue);
    public Date getUniqueModifyDate();
    public void setUniqueModifyComments(String newValue);
    public String getUniqueModifyComments();
    public void setGroupName(String newValue);
    public String getGroupName();
}    // End of bltNIM3_CPTGroup class definition
