package com.winstaff.login;

import com.winstaff.DebugLogger;
import com.winstaff.SQLParameterType;
import com.winstaff.db_PreparedStatementObject;
import com.winstaff.password.Encrypt;
import com.winstaff.searchDB2;

/**
 * Created with IntelliJ IDEA.
 * User: Scott
 * Date: 7/1/13
 * Time: 3:53 PM
 * To change this template use File | Settings | File Templates.
 */
public class LoginUtils {


    public static Integer checkLogin(String username, String password, String accountType){
        searchDB2 mySS = new searchDB2();
        try{
            db_PreparedStatementObject myDPSO = new db_PreparedStatementObject("select * from tUserAccount where UPPER(LogonUserName)= UPPER(?) and UPPER(AccountType)=UPPER(?)");
            myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,username));
            myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,accountType));
            java.sql.ResultSet myRS = mySS.executePreparedStatement(myDPSO);
            com.winstaff.password.Encrypt myEnc = new com.winstaff.password.Encrypt();
            if (myRS!=null&&myRS.next())
            {
                String thePassword = myRS.getString("LogonUserPassword");
                Integer UserStatus = new Integer(myRS.getString("Status"));
                if ( (UserStatus.intValue()>0)&&(Encrypt.checkPasswords(thePassword, myEnc.getMD5Base64(password))) )
                {
                    return myRS.getInt("UserID");
                }
            }
            mySS.closeAll();
        }
        catch(Exception ee)   {
                DebugLogger.e("LoginUtils:checkLogin","Error in login check ["+ee+"]");
        } finally {
            mySS.closeAll();
        }
        return null;
    }

}
