package com.winstaff.login;

import com.winstaff.*;
import com.winstaff.password.Encrypt;

import java.util.Vector;

/**
 * Created with IntelliJ IDEA.
 * User: Scott
 * Date: 7/1/13
 * Time: 4:28 PM
 * To change this template use File | Settings | File Templates.
 */
public class LoginObject_Partner {

    Integer authUserID;
    bltUserAccount authUser;
    bltNIM3_PayerMaster payerMaster;

    public LoginObject_Partner(Integer authUserID) {
        this.authUserID = authUserID;
    }

    public Integer getAuthUserID() {
        return authUserID;
    }

    public void setAuthUserID(Integer authUserID) {
        this.authUserID = authUserID;
    }

    public bltUserAccount getAuthUser() {
        if (authUser==null){
            authUser = new bltUserAccount(this.getAuthUserID());
        }
        return authUser;
    }

    public void setAuthUser(bltUserAccount authUser) {
        this.authUser = authUser;
    }

    public bltNIM3_PayerMaster getPayerMaster() {
        if (payerMaster==null){
            payerMaster = new bltNIM3_PayerMaster(this.getAuthUser().getPayerID());
        }
        return payerMaster;
    }

    public void setPayerMaster(bltNIM3_PayerMaster payerMaster) {
        this.payerMaster = payerMaster;
    }

}
