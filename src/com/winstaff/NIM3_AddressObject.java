package com.winstaff;

/**
 * Created by IntelliJ IDEA.
 * User: ste
 * Date: Oct 14, 2010
 * Time: 10:42:04 PM
 * To change this template use File | Settings | File Templates.
 */
public class NIM3_AddressObject
{
    private String AddressName = "";
    private String Address1 = "";
    private String Address2 = "";
    private String City = "";


    private Integer StateID = 0;
    private String State = "";
    private String ZIP = "";
    private String Phone = "";
    private String Fax ="";

    public String getAddress1() {
        return Address1;
    }

    public void setAddress1(String address1) {
        Address1 = address1;
    }

    public String getAddress2() {
        return Address2;
    }

    public void setAddress2(String address2) {
        Address2 = address2;
    }

    public String getAddressName() {
        return AddressName;
    }

    public void setAddressName(String addressName) {
        AddressName = addressName;
    }

    public String getCity() {
        return City;
    }

    public void setCity(String city) {
        City = city;
    }

    public String getFax() {
        return Fax;
    }

    public void setFax(String fax) {
        Fax = fax;
    }

    public String getPhone() {
        return Phone;
    }

    public void setPhone(String phone) {
        Phone = phone;
    }

    public String getState() {
        return State;
    }

    public String getZIP() {
        return ZIP;
    }

    public void setZIP(String ZIP) {
        this.ZIP = ZIP;
    }
    public Integer getStateID() {
        return StateID;
    }

    public void setStateID(Integer stateID) {
        StateID = stateID;
        State = new bltStateLI(stateID).getShortState();
    }

    public String getAddressFull(String sDel)
    {
        return this.getAddress1() + sDel + this.getAddress2() + sDel + this.getCity() + ", " + this.getState() + " " + this.getZIP();

    }
}
