package com.winstaff;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import javax.mail.Address;
import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.Flags;
import javax.mail.Flags.Flag;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.search.FlagTerm;
import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class EDI_BunchCare
{
  private static final String host = "outlook.office365.com";
  private static final String user = "bunchcare-edi@nextimagemedical.com";
  private static final String password = "NextBunch3390";
  private static final String fileoutput = "/bunchcare_temp_files/";
  private List<String> fileHandled = new ArrayList();
  private static final Authenticator auth = new EDI_BunchCare();

  private static final Properties props = new Properties();

  static {
    props.put("mail.store.protocol", "imaps");
  }

  private void logProcess(Date sentDate, Address[] from, String subject, String contentType)
  {
    System.out.println(sentDate + "\t" + from[0] + "\t" + subject + "\t" + contentType);
  }

  private void writeOut(InputStream is, String fileName)
  {
    try
    {
      OutputStream os = new FileOutputStream("/bunchcare_temp_files/" + fileName);
      byte[] buffer = new byte[1024];
      int bytesRead;
      while ((bytesRead = is.read(buffer)) != -1)
      {
        int bytesRead;
        os.write(buffer, 0, bytesRead);
      }
      os.flush();
      os.close();
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  private void runEmailFetch()
  {
    System.out.println("run job");

    Session session = Session.getInstance(props, auth);
    try
    {
      Store store = session.getStore("imaps");
      store.connect("outlook.office365.com", "bunchcare-edi@nextimagemedical.com", "NextBunch3390");
      System.out.println("javax connected: " + store.isConnected());

      Folder folder = store.getFolder("INBOX");
      folder.open(2);

      FlagTerm unAnswered = new FlagTerm(new Flags(Flags.Flag.ANSWERED), false);
      Message[] message = folder.search(unAnswered);
      void tmp111_108 = new EDI_BunchCare(); tmp111_108.getClass(); EDI_BunchCare.sFTPHandler sftp = new EDI_BunchCare.sFTPHandler(tmp111_108);
      void tmp132_129 = new EDI_BunchCare(); tmp132_129.getClass(); EDI_BunchCare.sshFileHandler ssh = new EDI_BunchCare.sshFileHandler(tmp132_129);
      for (int i = 0; i < message.length; i++) {
        System.out.println("found message");
        Message m = message[i];

        logProcess(m.getSentDate(), m.getFrom(), m.getSubject(), m.getContentType());

        if ((m.getContent() instanceof Multipart)) {
          Multipart multipart = (Multipart)m.getContent();
          for (int f = 0; f < multipart.getCount(); f++) {
            BodyPart bodypart = multipart.getBodyPart(f);

            if ((bodypart.getContent() instanceof InputStream)) {
              InputStream is = (InputStream)bodypart.getContent();
              String fileName = bodypart.getFileName().replace(" ", "_");
              writeOut(is, fileName);
              sftp.upload(fileName);
              runParseToDatabase(fileName);
              this.fileHandled.add(fileName);
              ssh.moveFile(fileName);

              is.close();
            }
          }
        }
      }
      sftp.close();
      ssh.close();
      System.out.println("closing javax");
      folder.close(true);
      store.close();
    }
    catch (MessagingException mex) {
      mex.printStackTrace();
    } catch (IOException ioex) {
      ioex.printStackTrace(); }  } 
  private void runParseToDatabase(String fileName) { List badTrigger = Arrays.asList(new String[] { "Claim Eligibility Feed - ORA", "CASEID", "Confidential  @2011 All Rights Reserved" });
    List datePos = Arrays.asList(new Integer[] { Integer.valueOf(5), Integer.valueOf(16), Integer.valueOf(17), Integer.valueOf(23), Integer.valueOf(24), Integer.valueOf(26), Integer.valueOf(28), Integer.valueOf(45) });
    List rfList = new ArrayList();
    HSSFDateUtil hssfdateutil = new HSSFDateUtil();
    EDI_BunchCare.logDB ldb = new EDI_BunchCare.logDB(this, null);
    XSSFSheet ws;
    try { XSSFWorkbook wb = new XSSFWorkbook(new FileInputStream(new File("/bunchcare_temp_files/" + fileName)));
      ws = wb.getSheetAt(0);

      Iterator ri = ws.iterator();

      ri.next();
      ri.next();

      while (ri.hasNext()) {
        Row row = (Row)ri.next();
        try {
          Iterator ci = row.cellIterator();
          List referralFormList = new ArrayList();
          while (ci.hasNext()) {
            Cell cell = (Cell)ci.next();
            try
            {
              referralFormList.add(cell.getStringCellValue());
            } catch (Exception e) {
              try {
                referralFormList.add(Double.valueOf(cell.getNumericCellValue()));
              }
              catch (Exception localException1) {
              }
            }
          }
          int c = 0;

          String caseid = new Long(new Double(((Double)referralFormList.get(c++)).doubleValue()).longValue()).toString();
          String carriercaseid = (String)referralFormList.get(c++);
          String patientfirstname = (String)referralFormList.get(c++);
          String patientlastname = (String)referralFormList.get(c++);
          String patientmiddleinitial = (String)referralFormList.get(c++);
          Date patientdob = HSSFDateUtil.getJavaDate(((Double)referralFormList.get(c++)).doubleValue());
          String patientssn = (String)referralFormList.get(c++);
          String patientgender = (String)referralFormList.get(c++);
          String patientaddress1 = (String)referralFormList.get(c++);
          String patientaddress2 = (String)referralFormList.get(c++);
          String patientcity = (String)referralFormList.get(c++);
          String patientstate = (String)referralFormList.get(c++);
          String patientzip = (String)referralFormList.get(c++);
          String patientphone = (String)referralFormList.get(c++);
          String jobtitle = (String)referralFormList.get(c++);
          String compensability = (String)referralFormList.get(c++);
          Date compensability_effective_date = HSSFDateUtil.getJavaDate(((Double)referralFormList.get(c++)).doubleValue());
          Date injurydate = HSSFDateUtil.getJavaDate(((Double)referralFormList.get(c++)).doubleValue());
          String employer = (String)referralFormList.get(c++);
          String bodypartcode = (String)referralFormList.get(c++);
          String naturecode = (String)referralFormList.get(c++);
          String causecode = (String)referralFormList.get(c++);
          String carriercasestatus = (String)referralFormList.get(c++);
          Date carrierstatus_effective_date = null;
          try {
            carrierstatus_effective_date = HSSFDateUtil.getJavaDate(((Double)referralFormList.get(c++)).doubleValue());
          } catch (Exception e) {
            try {
              c--;
              carrierstatus_effective_date = dateParse((String)referralFormList.get(c++));
            } catch (Exception localException2) {
            }
          }
          Date carrierstatus_closure_date = null;
          try {
            carrierstatus_closure_date = HSSFDateUtil.getJavaDate(((Double)referralFormList.get(c++)).doubleValue());
          } catch (Exception e) {
            try {
              c--;
              carrierstatus_closure_date = dateParse((String)referralFormList.get(c++));
            } catch (Exception localException3) {
            }
          }
          String bunch_casestatus = (String)referralFormList.get(c++);
          Date bunch_casestatus_effective_date = HSSFDateUtil.getJavaDate(((Double)referralFormList.get(c++)).doubleValue());
          String bunch_casephase = (String)referralFormList.get(c++);
          Date bunch_casephase_effective_date = HSSFDateUtil.getJavaDate(((Double)referralFormList.get(c++)).doubleValue());
          String juris_state = (String)referralFormList.get(c++);
          String adjusterfirstname = (String)referralFormList.get(c++);
          String adjusterlastname = (String)referralFormList.get(c++);
          String adjusteremail = (String)referralFormList.get(c++);
          String nursefirstname = (String)referralFormList.get(c++);
          String nurselastname = (String)referralFormList.get(c++);
          String nurseemail = (String)referralFormList.get(c++);
          String facilityid = new Long(new Double(((Double)referralFormList.get(c++)).doubleValue()).longValue()).toString();
          String facilityname = (String)referralFormList.get(c++);
          String facilityaddress1 = (String)referralFormList.get(c++);
          String facilityaddress2 = (String)referralFormList.get(c++);
          String facilitycity = (String)referralFormList.get(c++);
          String facilitystate = (String)referralFormList.get(c++);
          String facilityzip = (String)referralFormList.get(c++);
          String deathind = (String)referralFormList.get(c++);
          Date deathdate = dateParse((String)referralFormList.get(c++));

          rfList.add(new EDI_BunchCare.ReferralForm(this, caseid, carriercaseid, patientfirstname, patientlastname, patientmiddleinitial, patientdob, patientssn, patientgender, patientaddress1, patientaddress2, patientcity, patientstate, patientzip, patientphone, jobtitle, compensability, compensability_effective_date, injurydate, employer, bodypartcode, naturecode, causecode, carriercasestatus, carrierstatus_effective_date, carrierstatus_closure_date, bunch_casestatus, bunch_casestatus_effective_date, 
            bunch_casephase, bunch_casephase_effective_date, juris_state, adjusterfirstname, adjusterlastname, adjusteremail, nursefirstname, nurselastname, nurseemail, facilityid, facilityname, facilityaddress1, facilityaddress2, facilitycity, facilitystate, facilityzip, deathind, deathdate));
        }
        catch (Exception e) {
          e.printStackTrace();
        }
      }
    } catch (Exception e)
    {
      e.printStackTrace();
    }
    for (EDI_BunchCare.ReferralForm rf : rfList)
    {
      ldb.update(rf);
    }
    ldb.close(); }

  private Date dateParse(String s)
  {
    SimpleDateFormat sdf = new SimpleDateFormat("M/dd/yy");
    Date date = new Date();
    String a = "1/01/1800";
    try {
      date = sdf.parse(s);
    } catch (ParseException e) {
      try {
        date = new SimpleDateFormat("M/dd/yyyy").parse(a);
      } catch (ParseException localParseException1) {
      }
    }
    return date;
  }

  public static void main(String[] args) throws Exception {
    EDI_BunchCare edi_bunchcare = new EDI_BunchCare();
    edi_bunchcare.runEmailFetch();
  }
}