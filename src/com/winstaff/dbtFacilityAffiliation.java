

package com.winstaff;

import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public class dbtFacilityAffiliation extends Object implements InttFacilityAffiliation
{

        db_NewBase    dbnbDB;

    public dbtFacilityAffiliation()
    {
        dbnbDB = new db_NewBase( "tFacilityAffiliation", "AffiliationID" );

    }    // End of default constructor

    public dbtFacilityAffiliation( Integer iNewID )
    {
        dbnbDB = new db_NewBase( "tFacilityAffiliation", "AffiliationID" );
        setUniqueID( iNewID );
    }    // End of Constructor knowing an ID

    public void commitData() throws SQLException
    {
            //no longer trapping/ignoring errors 2011-03-17
            dbnbDB.commitData();
    }    // End of commitData()

    public void commitDataForced() throws SQLException
    {
        dbnbDB.commitDataForced();
    }    // End of commitData()

    public Integer getUniqueID()
    {
        return dbnbDB.getUniqueID();
    }    // End of getUniqueID()

    public void setUniqueID( Integer iNewID )
    {
        dbnbDB.setUniqueID( iNewID );
    }    // End of setUniqueID()

    public void setModifyComments( String sNew )
    {
        dbnbDB.setModifyComments( sNew );
    }    // End of setUniqueID()


    public void setAffiliationID(Integer newValue)
    {
                dbnbDB.setFieldData( "AffiliationID", newValue.toString() );
    }

    public Integer getAffiliationID()
    {
        String           sValue = dbnbDB.getFieldData( "AffiliationID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setField(String fieldN, Object fieldV)
    {DebugLogger.println("setField Method not implemented yet in classFacilityAffiliation!");}

    public String getFieldType(String fieldN)
    {return null;}


    public void setUniqueCreateDate(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "UniqueCreateDate", formatter.format( newValue ) );
    }

    public Date getUniqueCreateDate()
    {
        String           sValue = dbnbDB.getFieldData( "UniqueCreateDate" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setUniqueModifyDate(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "UniqueModifyDate", formatter.format( newValue ) );
    }

    public Date getUniqueModifyDate()
    {
        String           sValue = dbnbDB.getFieldData( "UniqueModifyDate" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setUniqueModifyComments(String newValue)
    {
                dbnbDB.setFieldData( "UniqueModifyComments", newValue.toString() );
    }

    public String getUniqueModifyComments()
    {
        String           sValue = dbnbDB.getFieldData( "UniqueModifyComments" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setPhysicianID(Integer newValue)
    {
                dbnbDB.setFieldData( "PhysicianID", newValue.toString() );
    }

    public Integer getPhysicianID()
    {
        String           sValue = dbnbDB.getFieldData( "PhysicianID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setIsPrimary(Integer newValue)
    {
                dbnbDB.setFieldData( "IsPrimary", newValue.toString() );
    }

    public Integer getIsPrimary()
    {
        String           sValue = dbnbDB.getFieldData( "IsPrimary" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setAppointmentLevel(String newValue)
    {
                dbnbDB.setFieldData( "AppointmentLevel", newValue.toString() );
    }

    public String getAppointmentLevel()
    {
        String           sValue = dbnbDB.getFieldData( "AppointmentLevel" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setStartDate(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "StartDate", formatter.format( newValue ) );
    }

    public Date getStartDate()
    {
        String           sValue = dbnbDB.getFieldData( "StartDate" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setEndDate(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "EndDate", formatter.format( newValue ) );
    }

    public Date getEndDate()
    {
        String           sValue = dbnbDB.getFieldData( "EndDate" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setPendingDate(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "PendingDate", formatter.format( newValue ) );
    }

    public Date getPendingDate()
    {
        String           sValue = dbnbDB.getFieldData( "PendingDate" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setFacilityName(String newValue)
    {
                dbnbDB.setFieldData( "FacilityName", newValue.toString() );
    }

    public String getFacilityName()
    {
        String           sValue = dbnbDB.getFieldData( "FacilityName" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setFacilityDepartment(String newValue)
    {
                dbnbDB.setFieldData( "FacilityDepartment", newValue.toString() );
    }

    public String getFacilityDepartment()
    {
        String           sValue = dbnbDB.getFieldData( "FacilityDepartment" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setFacilityAddress1(String newValue)
    {
                dbnbDB.setFieldData( "FacilityAddress1", newValue.toString() );
    }

    public String getFacilityAddress1()
    {
        String           sValue = dbnbDB.getFieldData( "FacilityAddress1" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setFacilityAddress2(String newValue)
    {
                dbnbDB.setFieldData( "FacilityAddress2", newValue.toString() );
    }

    public String getFacilityAddress2()
    {
        String           sValue = dbnbDB.getFieldData( "FacilityAddress2" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setFacilityCity(String newValue)
    {
                dbnbDB.setFieldData( "FacilityCity", newValue.toString() );
    }

    public String getFacilityCity()
    {
        String           sValue = dbnbDB.getFieldData( "FacilityCity" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setFacilityStateID(Integer newValue)
    {
                dbnbDB.setFieldData( "FacilityStateID", newValue.toString() );
    }

    public Integer getFacilityStateID()
    {
        String           sValue = dbnbDB.getFieldData( "FacilityStateID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setFacilityProvince(String newValue)
    {
                dbnbDB.setFieldData( "FacilityProvince", newValue.toString() );
    }

    public String getFacilityProvince()
    {
        String           sValue = dbnbDB.getFieldData( "FacilityProvince" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setFacilityZIP(String newValue)
    {
                dbnbDB.setFieldData( "FacilityZIP", newValue.toString() );
    }

    public String getFacilityZIP()
    {
        String           sValue = dbnbDB.getFieldData( "FacilityZIP" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setFacilityCountryID(Integer newValue)
    {
                dbnbDB.setFieldData( "FacilityCountryID", newValue.toString() );
    }

    public Integer getFacilityCountryID()
    {
        String           sValue = dbnbDB.getFieldData( "FacilityCountryID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setFacilityPhone(String newValue)
    {
                dbnbDB.setFieldData( "FacilityPhone", newValue.toString() );
    }

    public String getFacilityPhone()
    {
        String           sValue = dbnbDB.getFieldData( "FacilityPhone" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setFacilityFax(String newValue)
    {
                dbnbDB.setFieldData( "FacilityFax", newValue.toString() );
    }

    public String getFacilityFax()
    {
        String           sValue = dbnbDB.getFieldData( "FacilityFax" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setContactName(String newValue)
    {
                dbnbDB.setFieldData( "ContactName", newValue.toString() );
    }

    public String getContactName()
    {
        String           sValue = dbnbDB.getFieldData( "ContactName" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setContactEmail(String newValue)
    {
                dbnbDB.setFieldData( "ContactEmail", newValue.toString() );
    }

    public String getContactEmail()
    {
        String           sValue = dbnbDB.getFieldData( "ContactEmail" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setReasonForLeaving(String newValue)
    {
                dbnbDB.setFieldData( "ReasonForLeaving", newValue.toString() );
    }

    public String getReasonForLeaving()
    {
        String           sValue = dbnbDB.getFieldData( "ReasonForLeaving" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setAdmissionPriviledges(Integer newValue)
    {
                dbnbDB.setFieldData( "AdmissionPriviledges", newValue.toString() );
    }

    public Integer getAdmissionPriviledges()
    {
        String           sValue = dbnbDB.getFieldData( "AdmissionPriviledges" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setAdmissionArrangements(String newValue)
    {
                dbnbDB.setFieldData( "AdmissionArrangements", newValue.toString() );
    }

    public String getAdmissionArrangements()
    {
        String           sValue = dbnbDB.getFieldData( "AdmissionArrangements" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setUnrestrictedAdmission(Integer newValue)
    {
                dbnbDB.setFieldData( "UnrestrictedAdmission", newValue.toString() );
    }

    public Integer getUnrestrictedAdmission()
    {
        String           sValue = dbnbDB.getFieldData( "UnrestrictedAdmission" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setTempPriviledges(Integer newValue)
    {
                dbnbDB.setFieldData( "TempPriviledges", newValue.toString() );
    }

    public Integer getTempPriviledges()
    {
        String           sValue = dbnbDB.getFieldData( "TempPriviledges" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setInpatientCare(Integer newValue)
    {
                dbnbDB.setFieldData( "InpatientCare", newValue.toString() );
    }

    public Integer getInpatientCare()
    {
        String           sValue = dbnbDB.getFieldData( "InpatientCare" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setPercentAdmissions(String newValue)
    {
                dbnbDB.setFieldData( "PercentAdmissions", newValue.toString() );
    }

    public String getPercentAdmissions()
    {
        String           sValue = dbnbDB.getFieldData( "PercentAdmissions" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setComments(String newValue)
    {
                dbnbDB.setFieldData( "Comments", newValue.toString() );
    }

    public String getComments()
    {
        String           sValue = dbnbDB.getFieldData( "Comments" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

}    // End of bltFacilityAffiliation class definition
