package com.winstaff;
/*
 * bltSupportBase_List.java
 *
 * Created: Mon Sep 22 16:53:51 PDT 2003
 */



import java.sql.*;
import com.winstaff.dbtSupportBase;
import com.winstaff.ListElement;


/**
 *
 * @author  Scott Ellis via listClassGenCode
 * @version 2.5
 */
public class bltSupportBase_List extends db_BaseRelationTable
{
    /** Default constructor
    */    
    public bltSupportBase_List ( Integer iCompanyID )
    {
        super( "tSupportBase", "SupportBaseID", "SupportBaseID", "CompanyID", iCompanyID );
    }   // End of bltSupportBase_List()
    

    public void setField(String fieldN, Object fieldV)
    {DebugLogger.println("setField Method not implemented yet in classbltSupportBase_List!");}

    public String getFieldType(String fieldN)
    {return null;}

    public bltSupportBase_List ( Integer iCompanyID, String extraWhere, String OrderBy )
    {
        super( "tSupportBase", "SupportBaseID", "SupportBaseID", "CompanyID", iCompanyID, extraWhere,OrderBy );
    }   // End of bltSupportBase_List()
     /** Method pull information from the result set and load into class attributes.
    * @param sFieldList Comma delimited list of fields for which data has been retrieved.
    * @param rs Result set from which to pull the data from.
    * @throws SQLException Passes on SQLException
    */      
    protected void transferData( ResultSet rs ) throws SQLException
    {
        while ( rs.next()  )
        {
            Integer         iValue              = new Integer( rs.getString( sLinkFieldName ) );
            Integer         iLinkRecordID       = new Integer( rs.getString( sUniqueIDFieldName ) );
            Object          oReferenceObject;
            ListElement     leNewElement;
            
            oReferenceObject    = new bltSupportBase( iValue );
            
            leNewElement        = new ListElement( iLinkRecordID, oReferenceObject );
            
            vList.add( leNewElement );

        }
    }   // End of transferData()
}   // End of bltSupportBase_List class

