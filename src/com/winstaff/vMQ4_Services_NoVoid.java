package com.winstaff;

import java.io.DataOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.SQLException;

public class vMQ4_Services_NoVoid {

	/**
	 * @param args
	 * @throws SQLException 
	 * @throws IOException 
	 */
	public static void main(String[] args) throws SQLException, IOException {
		java.text.SimpleDateFormat displayDateSDF1Full = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1Full);
		System.out.println("Start: " + displayDateSDF1Full.format(PLCUtils.getNowDate(true)));
		System.out.println("Writing headers");
		String header = "CaseID\t" +
				"CaseCode\t" +
				"CaseClaimNumber\t" +
				"PayerID\t" +
				"PayerName\t" +
				"Payer_TypeID\t" +
				"Payer_SalesDivision\t" +
				"Payer_AcquisitionDivision\t" +
				"PayerCity\t" +
				"PayerState\t" +
				"Parent_PayerID\t" +
				"Parent_PayerName\t" +
				"ParentPayer_TypeID\t" +
				"ParentPayer_SalesDivision\t" +
				"ParentPayer_AcquisitionDivsion\t" +
				"UA_AssignedTo_UserID\t" +
				"UA_AssignedTo_UserName\t" +
				"UA_AssignedTo_FirstName\t" +
				"UA_AssignedTo_LastName\t" +
				"UA_AssignedTo_Email\t" +
				"UA_Adjuster_UserID\t" +
				"UA_Adjuster_FirstName\t" +
				"UA_Adjuster_LastName\t" +
				"UA_Adjuster_Email\t" +
				"UA_NCM_UserID\t" +
				"UA_NCM_FirstName\t" +
				"UA_NCM_LastName\t" +
				"UA_NCM_Email\t" +
				"UA_ReferralSource_UserID\t" +
				"UA_ReferralSource_FirstName\t" +
				"UA_ReferralSource_LastName\t" +
				"UA_ReferralSource_Email\t" +
				"EmployerName\t" +
				"PatientFirstName\t" +
				"PatientLastName\t" +
				"DateOfInjury\t" +
				"DateOfInjury_display\t" +
				"DateOfInjury_iMonth\t" +
				"DateOfInjury_iDay\t" +
				"DateOfInjury_iDOW\t" +
				"DateOfInjury_iYear\t" +
				"PatientDOB\t" +
				"PatientDOB_display\t" +
				"PatientDOB_iMonth\t" +
				"PatientDOB_iDay\t" +
				"PatientDOB_iDOW\t" +
				"PatientDOB_iYear\t" +
				"PatientAddress1\t" +
				"PatientAddress2\t" +
				"PatientCity\t" +
				"PatientState\t" +
				"PatientHomePhone\t" +
				"PatientCellPhone\t" +
				"PreScreen_IsClaus\t" +
				"PreScreen_ReqOpenModality\t" +
				"PreScreen_HasImplants\t" +
				"PreScreen_HasMetal\t" +
				"PreScreen_Allergies\t" +
				"PreScreen_HasRecentSurgery\t" +
				"PreScreen_PreviousMRIs\t" +
				"PreScreen_OtherCondition\t" +
				"PreScreen_IsPregnant\t" +
				"PreScreen_Height\t" +
				"PreScreen_Weight\t" +
				"ReferralID\t" +
				"ReferralStatusID\t" +
				"Referral_ReceiveDate\t" +
				"Referral_ReceiveDate_display\t" +
				"Referral_ReceiveDate_iMonth\t" +
				"Referral_ReceiveDate_iDay\t" +
				"Referral_ReceiveDate_iDOW\t" +
				"Referral_ReceiveDate_iYear\t" +
				"Referral_OrderFileID\t" +
				"Referral_RxFileID\t" +
				"UA_ReferringDr_UserID\t" +
				"UA_ReferringDr_FirstName\t" +
				"UA_ReferringDr_LastName\t" +
				"UA_ReferringDr_Email\t" +
				"EncounterID\t" +
				"Encounter_StatusID\t" +
				"Encounter_Status\t" +
				"Encounter_Type\t" +
				"Encounter_ScanPass\t" +
				"Encounter_DateOfService\t" +
				"Encounter_DateOfService_display\t" +
				"Encounter_DateOfService_iMonth\t" +
				"Encounter_DateOfService_iDay\t" +
				"Encounter_DateOfService_iDOW\t" +
				"Encounter_DateOfService_iYear\t" +
				"Encounter_ReportFileID\t" +
				"Encounter_VoidLeakReason\t" +
				"Encounter_ExportPaymentToQB\t" +
				"Encounter_IsSTAT\t" +
				"Encounter_IsRetro\t" +
				"Encounter_ReqFilms\t" +
				"Encounter_HasBeenRescheduled\t" +
				"Encounter_ReqAging\t" +
				"Encounter_IsCourtesy\t" +
				"Encounter_HCFA_ToPayerFileID\t" +
				"Encounter_HCFA_FromProvider_FileID\t" +
				"Encounter_SentTo_Bill_Pay\t" +
				"Encounter_SentTo_Bill_Pay_display\t" +
				"Encounter_SentTo_Bill_Pay_iMonth\t" +
				"Encounter_SentTo_Bill_Pay_iDay\t" +
				"Encounter_SentTo_Bill_Pay_iDOW\t" +
				"Encounter_SentTo_Bill_Pay_iYear\t" +
				"Encounter_Rec_Bill_Pro\t" +
				"Encounter_Rec_Bill_Pro_display\t" +
				"Encounter_Rec_Bill_Pro_iMonth\t" +
				"Encounter_Rec_Bill_Pro_iDay\t" +
				"Encounter_Rec_Bill_Pro_iDOW\t" +
				"Encounter_Rec_Bill_Pro_iYear\t" +
				"Encounter_TimeTrack_ReqRec\t" +
				"Encounter_TimeTrack_ReqRec_display\t" +
				"Encounter_TimeTrack_ReqRec_iMonth\t" +
				"Encounter_TimeTrack_ReqRec_iDay\t" +
				"Encounter_TimeTrack_ReqRec_iDOW\t" +
				"Encounter_TimeTrack_ReqRec_iYear\t" +
				"Encounter_TimeTrack_ReqCreated\t" +
				"Encounter_TimeTrack_ReqCreated_display\t" +
				"Encounter_TimeTrack_ReqCreated_iMonth\t" +
				"Encounter_TimeTrack_ReqCreated_iDay\t" +
				"Encounter_TimeTrack_ReqCreated_iDOW\t" +
				"Encounter_TimeTrack_ReqCreated_iYear\t" +
				"Encounter_TimeTrack_ReqProc\t" +
				"Encounter_TimeTrack_ReqProc_display\t" +
				"Encounter_TimeTrack_ReqProc_iMonth\t" +
				"Encounter_TimeTrack_ReqProc_iDay\t" +
				"Encounter_TimeTrack_ReqProc_iDOW\t" +
				"Encounter_TimeTrack_ReqProc_iYear\t" +
				"Encounter_TimeTrack_ReqSched\t" +
				"Encounter_TimeTrack_ReqSched_display\t" +
				"Encounter_TimeTrack_ReqSched_iMonth\t" +
				"Encounter_TimeTrack_ReqSched_iDay\t" +
				"Encounter_TimeTrack_ReqSched_iDOW\t" +
				"Encounter_TimeTrack_ReqSched_iYear\t" +
				"Encounter_TimeTrack_ReqDelivered\t" +
				"Encounter_TimeTrack_ReqDelivered_display\t" +
				"Encounter_TimeTrack_ReqDelivered_iMonth\t" +
				"Encounter_TimeTrack_ReqDelivered_iDay\t" +
				"Encounter_TimeTrack_ReqDelivered_iDOW\t" +
				"Encounter_TimeTrack_ReqDelivered_iYear\t" +
				"Encounter_TimeTrack_ReqPaidIN\t" +
				"Encounter_TimeTrack_ReqPaidIN_display\t" +
				"Encounter_TimeTrack_ReqPaidIN_iMonth\t" +
				"Encounter_TimeTrack_ReqPaidIN_iDay\t" +
				"Encounter_TimeTrack_ReqPaidIN_iDOW\t" +
				"Encounter_TimeTrack_ReqPaidIN_iYear\t" +
				"Encounter_TimeTrack_ReqPaidOut\t" +
				"Encounter_TimeTrack_ReqPaidOut_display\t" +
				"Encounter_TimeTrack_ReqPaidOut_iMonth\t" +
				"Encounter_TimeTrack_ReqPaidOut_iDay\t" +
				"Encounter_TimeTrack_ReqPaidOut_iDOW\t" +
				"Encounter_TimeTrack_ReqPaidOut_iYear\t" +
				"Encounter_TimeTrack_ReqApproved\t" +
				"Encounter_TimeTrack_ReqApproved_display\t" +
				"Encounter_TimeTrack_ReqApproved_iMonth\t" +
				"Encounter_TimeTrack_ReqApproved_iDay\t" +
				"Encounter_TimeTrack_ReqApproved_iDOW\t" +
				"Encounter_TimeTrack_ReqApproved_iYear\t" +
				"Encounter_TimeTrack_ReqInitialAppointment\t" +
				"Encounter_TimeTrack_ReqInitialAppointment_display\t" +
				"Encounter_TimeTrack_ReqInitialAppointment_iMonth\t" +
				"Encounter_TimeTrack_ReqInitialAppointment_iDay\t" +
				"Encounter_TimeTrack_ReqInitialAppointment_iDOW\t" +
				"Encounter_TimeTrack_ReqInitialAppointment_iYear\t" +
				"Encounter_TimeTrack_ReqRxReview\t" +
				"Encounter_TimeTrack_ReqRxReview_display\t" +
				"Encounter_TimeTrack_ReqRxReview_iMonth\t" +
				"Encounter_TimeTrack_ReqRxReview_iDay\t" +
				"Encounter_TimeTrack_ReqRxReview_iDOW\t" +
				"Encounter_TimeTrack_ReqRxReview_iYear\t" +
				"Encounter_TimeTrack_ReqRpReview\t" +
				"Encounter_TimeTrack_ReqRpReview_display\t" +
				"Encounter_TimeTrack_ReqRpReview_iMonth\t" +
				"Encounter_TimeTrack_ReqRpReview_iDay\t" +
				"Encounter_TimeTrack_ReqRpReview_iDOW\t" +
				"Encounter_TimeTrack_ReqRpReview_iYear\t" +
				"AppointmentID\t" +
				"Encounter_PaidToProvider_CheckCalc\t" +
				"Appointment_ProviderID\t" +
				"Appointment_AppointmentTime\t" +
				"Appointment_AppointmentTime_display\t" +
				"Appointment_AppointmentTime_iMonth\t" +
				"Appointment_AppointmentTime_iDay\t" +
				"Appointment_AppointmentTime_iDOW\t" +
				"Appointment_AppointmentTime_iYear\t" +
				"Appointment_StatusID\t" +
				"PracticeID\t" +
				"Practice_Name\t" +
				"Practice_City\t" +
				"Practice_State\t" +
				"Practice_IsBlueStar\t" +
				"Practice_PetLinQID\t" +
				"Practice_SelectMRI_ID\t" +
				"ServiceID\t" +
				"Service_StatusID\t" +
				"Service_Status\t" +
				"Service_TypeID\t" +
				"Service_CPT\t" +
				"Service_CPT_Qty\t" +
				"Service_CPTModifier\t" +
				"Service_CPTBodyPart\t" +
				"Service_ICD_1\t" +
				"Service_ICD_2\t" +
				"Service_ICD_3\t" +
				"Service_ICD_4\t" +
				"Service_CPTText\t" +
				"Service_BillAmount\t" +
				"Service_AllowAmount\t" +
				"Service_AllowAmountAdjustment\t" +
				"Service_ReceivedAmount\t" +
				"Service_PaidOutAmount\t" +
				"Service_ReceivedAmount_CheckCalc\t" +
				"Service_modality\t" +
				"State_Fee+Schedule\t" +
				"Medicare_Fee_Schedule\t\n";
		String fileName = "export/vMQ4_Services_NoVoid2.txt";
		DataOutputStream out = new DataOutputStream( new FileOutputStream( fileName ) );
		out.writeBytes( header );
		String body = "";
		
		searchDB2 conn = new searchDB2();
		
		String query = "SELECT \"vMQ4_Services\".\"Practice_Zip\",\"vMQ4_Services\".\"CaseID\", \"vMQ4_Services\".\"CaseCode\", \"vMQ4_Services\".\"CaseClaimNumber\", \"vMQ4_Services\".\"PayerID\", \"vMQ4_Services\".\"PayerName\", \"vMQ4_Services\".\"Payer_TypeID\", \"vMQ4_Services\".\"Payer_SalesDivision\", \"vMQ4_Services\".\"Payer_AcquisitionDivision\", \"vMQ4_Services\".\"PayerCity\", \"vMQ4_Services\".\"PayerState\", \"vMQ4_Services\".\"Parent_PayerID\", \"vMQ4_Services\".\"Parent_PayerName\", \"vMQ4_Services\".\"ParentPayer_TypeID\", \"vMQ4_Services\".\"ParentPayer_SalesDivision\", \"vMQ4_Services\".\"ParentPayer_AcquisitionDivsion\", \"vMQ4_Services\".\"UA_AssignedTo_UserID\", \"vMQ4_Services\".\"UA_AssignedTo_UserName\", \"vMQ4_Services\".\"UA_AssignedTo_FirstName\", \"vMQ4_Services\".\"UA_AssignedTo_LastName\", \"vMQ4_Services\".\"UA_AssignedTo_Email\", \"vMQ4_Services\".\"UA_Adjuster_UserID\", \"vMQ4_Services\".\"UA_Adjuster_FirstName\", \"vMQ4_Services\".\"UA_Adjuster_LastName\", \"vMQ4_Services\".\"UA_Adjuster_Email\", \"vMQ4_Services\".\"UA_NCM_UserID\", \"vMQ4_Services\".\"UA_NCM_FirstName\", \"vMQ4_Services\".\"UA_NCM_LastName\", \"vMQ4_Services\".\"UA_NCM_Email\", \"vMQ4_Services\".\"UA_ReferralSource_UserID\", \"vMQ4_Services\".\"UA_ReferralSource_FirstName\", \"vMQ4_Services\".\"UA_ReferralSource_LastName\", \"vMQ4_Services\".\"UA_ReferralSource_Email\", \"vMQ4_Services\".\"EmployerName\", \"vMQ4_Services\".\"PatientFirstName\", \"vMQ4_Services\".\"PatientLastName\", \"vMQ4_Services\".\"DateOfInjury\", \"vMQ4_Services\".\"DateOfInjury_display\", \"vMQ4_Services\".\"DateOfInjury_iMonth\", \"vMQ4_Services\".\"DateOfInjury_iDay\", \"vMQ4_Services\".\"DateOfInjury_iDOW\", \"vMQ4_Services\".\"DateOfInjury_iYear\", \"vMQ4_Services\".\"PatientDOB\", \"vMQ4_Services\".\"PatientDOB_display\", \"vMQ4_Services\".\"PatientDOB_iMonth\", \"vMQ4_Services\".\"PatientDOB_iDay\", \"vMQ4_Services\".\"PatientDOB_iDOW\", \"vMQ4_Services\".\"PatientDOB_iYear\", \"vMQ4_Services\".\"PatientAddress1\", \"vMQ4_Services\".\"PatientAddress2\", \"vMQ4_Services\".\"PatientCity\", \"vMQ4_Services\".\"PatientState\", \"vMQ4_Services\".\"PatientHomePhone\", \"vMQ4_Services\".\"PatientCellPhone\", \"vMQ4_Services\".\"PreScreen_IsClaus\", \"vMQ4_Services\".\"PreScreen_ReqOpenModality\", \"vMQ4_Services\".\"PreScreen_HasImplants\", \"vMQ4_Services\".\"PreScreen_HasMetal\", \"vMQ4_Services\".\"PreScreen_Allergies\", \"vMQ4_Services\".\"PreScreen_HasRecentSurgery\", \"vMQ4_Services\".\"PreScreen_PreviousMRIs\", \"vMQ4_Services\".\"PreScreen_OtherCondition\", \"vMQ4_Services\".\"PreScreen_IsPregnant\", \"vMQ4_Services\".\"PreScreen_Height\", \"vMQ4_Services\".\"PreScreen_Weight\", \"vMQ4_Services\".\"ReferralID\", \"vMQ4_Services\".\"ReferralStatusID\", \"vMQ4_Services\".\"Referral_ReceiveDate\", \"vMQ4_Services\".\"Referral_ReceiveDate_display\", \"vMQ4_Services\".\"Referral_ReceiveDate_iMonth\", \"vMQ4_Services\".\"Referral_ReceiveDate_iDay\", \"vMQ4_Services\".\"Referral_ReceiveDate_iDOW\", \"vMQ4_Services\".\"Referral_ReceiveDate_iYear\", \"vMQ4_Services\".\"Referral_OrderFileID\", \"vMQ4_Services\".\"Referral_RxFileID\", \"vMQ4_Services\".\"UA_ReferringDr_UserID\", \"vMQ4_Services\".\"UA_ReferringDr_FirstName\", \"vMQ4_Services\".\"UA_ReferringDr_LastName\", \"vMQ4_Services\".\"UA_ReferringDr_Email\", \"vMQ4_Services\".\"EncounterID\", \"vMQ4_Services\".\"Encounter_StatusID\", \"vMQ4_Services\".\"Encounter_Status\", \"vMQ4_Services\".\"Encounter_Type\", \"vMQ4_Services\".\"Encounter_ScanPass\", \"vMQ4_Services\".\"Encounter_DateOfService\", \"vMQ4_Services\".\"Encounter_DateOfService_display\", \"vMQ4_Services\".\"Encounter_DateOfService_iMonth\", \"vMQ4_Services\".\"Encounter_DateOfService_iDay\", \"vMQ4_Services\".\"Encounter_DateOfService_iDOW\", \"vMQ4_Services\".\"Encounter_DateOfService_iYear\", \"vMQ4_Services\".\"Encounter_ReportFileID\", \"vMQ4_Services\".\"Encounter_VoidLeakReason\", \"vMQ4_Services\".\"Encounter_ExportPaymentToQB\", \"vMQ4_Services\".\"Encounter_IsSTAT\", \"vMQ4_Services\".\"Encounter_IsRetro\", \"vMQ4_Services\".\"Encounter_ReqFilms\", \"vMQ4_Services\".\"Encounter_HasBeenRescheduled\", \"vMQ4_Services\".\"Encounter_ReqAging\", \"vMQ4_Services\".\"Encounter_IsCourtesy\", \"vMQ4_Services\".\"Encounter_HCFA_ToPayerFileID\", \"vMQ4_Services\".\"Encounter_HCFA_FromProvider_FileID\", \"vMQ4_Services\".\"Encounter_SentTo_Bill_Pay\", \"vMQ4_Services\".\"Encounter_SentTo_Bill_Pay_display\", \"vMQ4_Services\".\"Encounter_SentTo_Bill_Pay_iMonth\", \"vMQ4_Services\".\"Encounter_SentTo_Bill_Pay_iDay\", \"vMQ4_Services\".\"Encounter_SentTo_Bill_Pay_iDOW\", \"vMQ4_Services\".\"Encounter_SentTo_Bill_Pay_iYear\", \"vMQ4_Services\".\"Encounter_Rec_Bill_Pro\", \"vMQ4_Services\".\"Encounter_Rec_Bill_Pro_display\", \"vMQ4_Services\".\"Encounter_Rec_Bill_Pro_iMonth\", \"vMQ4_Services\".\"Encounter_Rec_Bill_Pro_iDay\", \"vMQ4_Services\".\"Encounter_Rec_Bill_Pro_iDOW\", \"vMQ4_Services\".\"Encounter_Rec_Bill_Pro_iYear\", \"vMQ4_Services\".\"Encounter_TimeTrack_ReqRec\", \"vMQ4_Services\".\"Encounter_TimeTrack_ReqRec_display\", \"vMQ4_Services\".\"Encounter_TimeTrack_ReqRec_iMonth\", \"vMQ4_Services\".\"Encounter_TimeTrack_ReqRec_iDay\", \"vMQ4_Services\".\"Encounter_TimeTrack_ReqRec_iDOW\", \"vMQ4_Services\".\"Encounter_TimeTrack_ReqRec_iYear\", \"vMQ4_Services\".\"Encounter_TimeTrack_ReqCreated\", \"vMQ4_Services\".\"Encounter_TimeTrack_ReqCreated_display\", \"vMQ4_Services\".\"Encounter_TimeTrack_ReqCreated_iMonth\", \"vMQ4_Services\".\"Encounter_TimeTrack_ReqCreated_iDay\", \"vMQ4_Services\".\"Encounter_TimeTrack_ReqCreated_iDOW\", \"vMQ4_Services\".\"Encounter_TimeTrack_ReqCreated_iYear\", \"vMQ4_Services\".\"Encounter_TimeTrack_ReqProc\", \"vMQ4_Services\".\"Encounter_TimeTrack_ReqProc_display\", \"vMQ4_Services\".\"Encounter_TimeTrack_ReqProc_iMonth\", \"vMQ4_Services\".\"Encounter_TimeTrack_ReqProc_iDay\", \"vMQ4_Services\".\"Encounter_TimeTrack_ReqProc_iDOW\", \"vMQ4_Services\".\"Encounter_TimeTrack_ReqProc_iYear\", \"vMQ4_Services\".\"Encounter_TimeTrack_ReqSched\", \"vMQ4_Services\".\"Encounter_TimeTrack_ReqSched_display\", \"vMQ4_Services\".\"Encounter_TimeTrack_ReqSched_iMonth\", \"vMQ4_Services\".\"Encounter_TimeTrack_ReqSched_iDay\", \"vMQ4_Services\".\"Encounter_TimeTrack_ReqSched_iDOW\", \"vMQ4_Services\".\"Encounter_TimeTrack_ReqSched_iYear\", \"vMQ4_Services\".\"Encounter_TimeTrack_ReqDelivered\", \"vMQ4_Services\".\"Encounter_TimeTrack_ReqDelivered_display\", \"vMQ4_Services\".\"Encounter_TimeTrack_ReqDelivered_iMonth\", \"vMQ4_Services\".\"Encounter_TimeTrack_ReqDelivered_iDay\", \"vMQ4_Services\".\"Encounter_TimeTrack_ReqDelivered_iDOW\", \"vMQ4_Services\".\"Encounter_TimeTrack_ReqDelivered_iYear\", \"vMQ4_Services\".\"Encounter_TimeTrack_ReqPaidIN\", \"vMQ4_Services\".\"Encounter_TimeTrack_ReqPaidIN_display\", \"vMQ4_Services\".\"Encounter_TimeTrack_ReqPaidIN_iMonth\", \"vMQ4_Services\".\"Encounter_TimeTrack_ReqPaidIN_iDay\", \"vMQ4_Services\".\"Encounter_TimeTrack_ReqPaidIN_iDOW\", \"vMQ4_Services\".\"Encounter_TimeTrack_ReqPaidIN_iYear\", \"vMQ4_Services\".\"Encounter_TimeTrack_ReqPaidOut\", \"vMQ4_Services\".\"Encounter_TimeTrack_ReqPaidOut_display\", \"vMQ4_Services\".\"Encounter_TimeTrack_ReqPaidOut_iMonth\", \"vMQ4_Services\".\"Encounter_TimeTrack_ReqPaidOut_iDay\", \"vMQ4_Services\".\"Encounter_TimeTrack_ReqPaidOut_iDOW\", \"vMQ4_Services\".\"Encounter_TimeTrack_ReqPaidOut_iYear\", \"vMQ4_Services\".\"Encounter_TimeTrack_ReqApproved\", \"vMQ4_Services\".\"Encounter_TimeTrack_ReqApproved_display\", \"vMQ4_Services\".\"Encounter_TimeTrack_ReqApproved_iMonth\", \"vMQ4_Services\".\"Encounter_TimeTrack_ReqApproved_iDay\", \"vMQ4_Services\".\"Encounter_TimeTrack_ReqApproved_iDOW\", \"vMQ4_Services\".\"Encounter_TimeTrack_ReqApproved_iYear\", \"vMQ4_Services\".\"Encounter_TimeTrack_ReqInitialAppointment\", \"vMQ4_Services\".\"Encounter_TimeTrack_ReqInitialAppointment_display\", \"vMQ4_Services\".\"Encounter_TimeTrack_ReqInitialAppointment_iMonth\", \"vMQ4_Services\".\"Encounter_TimeTrack_ReqInitialAppointment_iDay\", \"vMQ4_Services\".\"Encounter_TimeTrack_ReqInitialAppointment_iDOW\", \"vMQ4_Services\".\"Encounter_TimeTrack_ReqInitialAppointment_iYear\", \"vMQ4_Services\".\"Encounter_TimeTrack_ReqRxReview\", \"vMQ4_Services\".\"Encounter_TimeTrack_ReqRxReview_display\", \"vMQ4_Services\".\"Encounter_TimeTrack_ReqRxReview_iMonth\", \"vMQ4_Services\".\"Encounter_TimeTrack_ReqRxReview_iDay\", \"vMQ4_Services\".\"Encounter_TimeTrack_ReqRxReview_iDOW\", \"vMQ4_Services\".\"Encounter_TimeTrack_ReqRxReview_iYear\", \"vMQ4_Services\".\"Encounter_TimeTrack_ReqRpReview\", \"vMQ4_Services\".\"Encounter_TimeTrack_ReqRpReview_display\", \"vMQ4_Services\".\"Encounter_TimeTrack_ReqRpReview_iMonth\", \"vMQ4_Services\".\"Encounter_TimeTrack_ReqRpReview_iDay\", \"vMQ4_Services\".\"Encounter_TimeTrack_ReqRpReview_iDOW\", \"vMQ4_Services\".\"Encounter_TimeTrack_ReqRpReview_iYear\", \"vMQ4_Services\".\"AppointmentID\", \"vMQ4_Services\".\"Encounter_PaidToProvider_CheckCalc\", \"vMQ4_Services\".\"Appointment_ProviderID\", \"vMQ4_Services\".\"Appointment_AppointmentTime\", \"vMQ4_Services\".\"Appointment_AppointmentTime_display\", \"vMQ4_Services\".\"Appointment_AppointmentTime_iMonth\", \"vMQ4_Services\".\"Appointment_AppointmentTime_iDay\", \"vMQ4_Services\".\"Appointment_AppointmentTime_iDOW\", \"vMQ4_Services\".\"Appointment_AppointmentTime_iYear\", \"vMQ4_Services\".\"Appointment_StatusID\", \"vMQ4_Services\".\"PracticeID\", \"vMQ4_Services\".\"Practice_Name\", \"vMQ4_Services\".\"Practice_City\", \"vMQ4_Services\".\"Practice_State\", \"vMQ4_Services\".\"Practice_IsBlueStar\", \"vMQ4_Services\".\"Practice_PetLinQID\", \"vMQ4_Services\".\"Practice_SelectMRI_ID\", \"vMQ4_Services\".\"ServiceID\", \"vMQ4_Services\".\"Service_StatusID\", \"vMQ4_Services\".\"Service_Status\", \"vMQ4_Services\".\"Service_TypeID\", \"vMQ4_Services\".\"Service_CPT\", \"vMQ4_Services\".\"Service_CPT_Qty\", \"vMQ4_Services\".\"Service_CPTModifier\", \"vMQ4_Services\".\"Service_CPTBodyPart\", \"vMQ4_Services\".\"Service_ICD_1\", \"vMQ4_Services\".\"Service_ICD_2\", \"vMQ4_Services\".\"Service_ICD_3\", \"vMQ4_Services\".\"Service_ICD_4\", \"vMQ4_Services\".\"Service_CPTText\", \"vMQ4_Services\".\"Service_BillAmount\", \"vMQ4_Services\".\"Service_AllowAmount\", \"vMQ4_Services\".\"Service_AllowAmountAdjustment\", \"vMQ4_Services\".\"Service_ReceivedAmount\", \"vMQ4_Services\".\"Service_PaidOutAmount\", \"vMQ4_Services\".\"Service_ReceivedAmount_CheckCalc\", \"vMQ4_Services\".\"Service_modality\" FROM (\"vMQ4_Services\" JOIN \"vMQ4_Filter_Service_BP\" ON ((\"vMQ4_Services\".\"ServiceID\" = \"vMQ4_Filter_Service_BP\".serviceid))) WHERE (\"vMQ4_Services\".\"Encounter_StatusID\" = ANY (ARRAY[0, 1, 4, 6, 7, 8, 9, 11, 12])) " +
					//"limit 100" +
					"";
		 
		java.sql.ResultSet myRS = conn.executeStatement(query);
		System.out.println("Writing body");
		while (myRS.next()){
			try{body = (
					myRS.getString("CaseID")+ "\t" +
					myRS.getString("CaseCode")+ "\t" +
					myRS.getString("CaseClaimNumber")+ "\t" +
					myRS.getString("PayerID")+ "\t" +
					myRS.getString("PayerName")+ "\t" +
					myRS.getString("Payer_TypeID")+ "\t" +
					myRS.getString("Payer_SalesDivision")+ "\t" +
					myRS.getString("Payer_AcquisitionDivision")+ "\t" +
					myRS.getString("PayerCity")+ "\t" +
					myRS.getString("PayerState")+ "\t" +
					myRS.getString("Parent_PayerID")+ "\t" +
					myRS.getString("Parent_PayerName")+ "\t" +
					myRS.getString("ParentPayer_TypeID")+ "\t" +
					myRS.getString("ParentPayer_SalesDivision")+ "\t" +
					myRS.getString("ParentPayer_AcquisitionDivsion")+ "\t" +
					myRS.getString("UA_AssignedTo_UserID")+ "\t" +
					myRS.getString("UA_AssignedTo_UserName")+ "\t" +
					myRS.getString("UA_AssignedTo_FirstName")+ "\t" +
					myRS.getString("UA_AssignedTo_LastName")+ "\t" +
					myRS.getString("UA_AssignedTo_Email")+ "\t" +
					myRS.getString("UA_Adjuster_UserID")+ "\t" +
					myRS.getString("UA_Adjuster_FirstName")+ "\t" +
					myRS.getString("UA_Adjuster_LastName")+ "\t" +
					myRS.getString("UA_Adjuster_Email")+ "\t" +
					myRS.getString("UA_NCM_UserID")+ "\t" +
					myRS.getString("UA_NCM_FirstName")+ "\t" +
					myRS.getString("UA_NCM_LastName")+ "\t" +
					myRS.getString("UA_NCM_Email")+ "\t" +
					myRS.getString("UA_ReferralSource_UserID")+ "\t" +
					myRS.getString("UA_ReferralSource_FirstName")+ "\t" +
					myRS.getString("UA_ReferralSource_LastName")+ "\t" +
					myRS.getString("UA_ReferralSource_Email")+ "\t" +
					myRS.getString("EmployerName")+ "\t" +
					myRS.getString("PatientFirstName")+ "\t" +
					myRS.getString("PatientLastName")+ "\t" +
					myRS.getString("DateOfInjury")+ "\t" +
					myRS.getString("DateOfInjury_display")+ "\t" +
					myRS.getString("DateOfInjury_iMonth")+ "\t" +
					myRS.getString("DateOfInjury_iDay")+ "\t" +
					myRS.getString("DateOfInjury_iDOW")+ "\t" +
					myRS.getString("DateOfInjury_iYear")+ "\t" +
					myRS.getString("PatientDOB")+ "\t" +
					myRS.getString("PatientDOB_display")+ "\t" +
					myRS.getString("PatientDOB_iMonth")+ "\t" +
					myRS.getString("PatientDOB_iDay")+ "\t" +
					myRS.getString("PatientDOB_iDOW")+ "\t" +
					myRS.getString("PatientDOB_iYear")+ "\t" +
					myRS.getString("PatientAddress1")+ "\t" +
					myRS.getString("PatientAddress2")+ "\t" +
					myRS.getString("PatientCity")+ "\t" +
					myRS.getString("PatientState")+ "\t" +
					myRS.getString("PatientHomePhone")+ "\t" +
					myRS.getString("PatientCellPhone")+ "\t" +
					myRS.getString("PreScreen_IsClaus")+ "\t" +
					myRS.getString("PreScreen_ReqOpenModality")+ "\t" +
					myRS.getString("PreScreen_HasImplants")+ "\t" +
					myRS.getString("PreScreen_HasMetal")+ "\t" +
					myRS.getString("PreScreen_Allergies")+ "\t" +
					myRS.getString("PreScreen_HasRecentSurgery")+ "\t" +
					myRS.getString("PreScreen_PreviousMRIs")+ "\t" +
					myRS.getString("PreScreen_OtherCondition")+ "\t" +
					myRS.getString("PreScreen_IsPregnant")+ "\t" +
					myRS.getString("PreScreen_Height")+ "\t" +
					myRS.getString("PreScreen_Weight")+ "\t" +
					myRS.getString("ReferralID")+ "\t" +
					myRS.getString("ReferralStatusID")+ "\t" +
					myRS.getString("Referral_ReceiveDate")+ "\t" +
					myRS.getString("Referral_ReceiveDate_display")+ "\t" +
					myRS.getString("Referral_ReceiveDate_iMonth")+ "\t" +
					myRS.getString("Referral_ReceiveDate_iDay")+ "\t" +
					myRS.getString("Referral_ReceiveDate_iDOW")+ "\t" +
					myRS.getString("Referral_ReceiveDate_iYear")+ "\t" +
					myRS.getString("Referral_OrderFileID")+ "\t" +
					myRS.getString("Referral_RxFileID")+ "\t" +
					myRS.getString("UA_ReferringDr_UserID")+ "\t" +
					myRS.getString("UA_ReferringDr_FirstName")+ "\t" +
					myRS.getString("UA_ReferringDr_LastName")+ "\t" +
					myRS.getString("UA_ReferringDr_Email")+ "\t" +
					myRS.getString("EncounterID")+ "\t" +
					myRS.getString("Encounter_StatusID")+ "\t" +
					myRS.getString("Encounter_Status")+ "\t" +
					myRS.getString("Encounter_Type")+ "\t" +
					myRS.getString("Encounter_ScanPass")+ "\t" +
					myRS.getString("Encounter_DateOfService")+ "\t" +
					myRS.getString("Encounter_DateOfService_display")+ "\t" +
					myRS.getString("Encounter_DateOfService_iMonth")+ "\t" +
					myRS.getString("Encounter_DateOfService_iDay")+ "\t" +
					myRS.getString("Encounter_DateOfService_iDOW")+ "\t" +
					myRS.getString("Encounter_DateOfService_iYear")+ "\t" +
					myRS.getString("Encounter_ReportFileID")+ "\t" +
					myRS.getString("Encounter_VoidLeakReason")+ "\t" +
					myRS.getString("Encounter_ExportPaymentToQB")+ "\t" +
					myRS.getString("Encounter_IsSTAT")+ "\t" +
					myRS.getString("Encounter_IsRetro")+ "\t" +
					myRS.getString("Encounter_ReqFilms")+ "\t" +
					myRS.getString("Encounter_HasBeenRescheduled")+ "\t" +
					myRS.getString("Encounter_ReqAging")+ "\t" +
					myRS.getString("Encounter_IsCourtesy")+ "\t" +
					myRS.getString("Encounter_HCFA_ToPayerFileID")+ "\t" +
					myRS.getString("Encounter_HCFA_FromProvider_FileID")+ "\t" +
					myRS.getString("Encounter_SentTo_Bill_Pay")+ "\t" +
					myRS.getString("Encounter_SentTo_Bill_Pay_display")+ "\t" +
					myRS.getString("Encounter_SentTo_Bill_Pay_iMonth")+ "\t" +
					myRS.getString("Encounter_SentTo_Bill_Pay_iDay")+ "\t" +
					myRS.getString("Encounter_SentTo_Bill_Pay_iDOW")+ "\t" +
					myRS.getString("Encounter_SentTo_Bill_Pay_iYear")+ "\t" +
					myRS.getString("Encounter_Rec_Bill_Pro")+ "\t" +
					myRS.getString("Encounter_Rec_Bill_Pro_display")+ "\t" +
					myRS.getString("Encounter_Rec_Bill_Pro_iMonth")+ "\t" +
					myRS.getString("Encounter_Rec_Bill_Pro_iDay")+ "\t" +
					myRS.getString("Encounter_Rec_Bill_Pro_iDOW")+ "\t" +
					myRS.getString("Encounter_Rec_Bill_Pro_iYear")+ "\t" +
					myRS.getString("Encounter_TimeTrack_ReqRec")+ "\t" +
					myRS.getString("Encounter_TimeTrack_ReqRec_display")+ "\t" +
					myRS.getString("Encounter_TimeTrack_ReqRec_iMonth")+ "\t" +
					myRS.getString("Encounter_TimeTrack_ReqRec_iDay")+ "\t" +
					myRS.getString("Encounter_TimeTrack_ReqRec_iDOW")+ "\t" +
					myRS.getString("Encounter_TimeTrack_ReqRec_iYear")+ "\t" +
					myRS.getString("Encounter_TimeTrack_ReqCreated")+ "\t" +
					myRS.getString("Encounter_TimeTrack_ReqCreated_display")+ "\t" +
					myRS.getString("Encounter_TimeTrack_ReqCreated_iMonth")+ "\t" +
					myRS.getString("Encounter_TimeTrack_ReqCreated_iDay")+ "\t" +
					myRS.getString("Encounter_TimeTrack_ReqCreated_iDOW")+ "\t" +
					myRS.getString("Encounter_TimeTrack_ReqCreated_iYear")+ "\t" +
					myRS.getString("Encounter_TimeTrack_ReqProc")+ "\t" +
					myRS.getString("Encounter_TimeTrack_ReqProc_display")+ "\t" +
					myRS.getString("Encounter_TimeTrack_ReqProc_iMonth")+ "\t" +
					myRS.getString("Encounter_TimeTrack_ReqProc_iDay")+ "\t" +
					myRS.getString("Encounter_TimeTrack_ReqProc_iDOW")+ "\t" +
					myRS.getString("Encounter_TimeTrack_ReqProc_iYear")+ "\t" +
					myRS.getString("Encounter_TimeTrack_ReqSched")+ "\t" +
					myRS.getString("Encounter_TimeTrack_ReqSched_display")+ "\t" +
					myRS.getString("Encounter_TimeTrack_ReqSched_iMonth")+ "\t" +
					myRS.getString("Encounter_TimeTrack_ReqSched_iDay")+ "\t" +
					myRS.getString("Encounter_TimeTrack_ReqSched_iDOW")+ "\t" +
					myRS.getString("Encounter_TimeTrack_ReqSched_iYear")+ "\t" +
					myRS.getString("Encounter_TimeTrack_ReqDelivered")+ "\t" +
					myRS.getString("Encounter_TimeTrack_ReqDelivered_display")+ "\t" +
					myRS.getString("Encounter_TimeTrack_ReqDelivered_iMonth")+ "\t" +
					myRS.getString("Encounter_TimeTrack_ReqDelivered_iDay")+ "\t" +
					myRS.getString("Encounter_TimeTrack_ReqDelivered_iDOW")+ "\t" +
					myRS.getString("Encounter_TimeTrack_ReqDelivered_iYear")+ "\t" +
					myRS.getString("Encounter_TimeTrack_ReqPaidIN")+ "\t" +
					myRS.getString("Encounter_TimeTrack_ReqPaidIN_display")+ "\t" +
					myRS.getString("Encounter_TimeTrack_ReqPaidIN_iMonth")+ "\t" +
					myRS.getString("Encounter_TimeTrack_ReqPaidIN_iDay")+ "\t" +
					myRS.getString("Encounter_TimeTrack_ReqPaidIN_iDOW")+ "\t" +
					myRS.getString("Encounter_TimeTrack_ReqPaidIN_iYear")+ "\t" +
					myRS.getString("Encounter_TimeTrack_ReqPaidOut")+ "\t" +
					myRS.getString("Encounter_TimeTrack_ReqPaidOut_display")+ "\t" +
					myRS.getString("Encounter_TimeTrack_ReqPaidOut_iMonth")+ "\t" +
					myRS.getString("Encounter_TimeTrack_ReqPaidOut_iDay")+ "\t" +
					myRS.getString("Encounter_TimeTrack_ReqPaidOut_iDOW")+ "\t" +
					myRS.getString("Encounter_TimeTrack_ReqPaidOut_iYear")+ "\t" +
					myRS.getString("Encounter_TimeTrack_ReqApproved")+ "\t" +
					myRS.getString("Encounter_TimeTrack_ReqApproved_display")+ "\t" +
					myRS.getString("Encounter_TimeTrack_ReqApproved_iMonth")+ "\t" +
					myRS.getString("Encounter_TimeTrack_ReqApproved_iDay")+ "\t" +
					myRS.getString("Encounter_TimeTrack_ReqApproved_iDOW")+ "\t" +
					myRS.getString("Encounter_TimeTrack_ReqApproved_iYear")+ "\t" +
					myRS.getString("Encounter_TimeTrack_ReqInitialAppointment")+ "\t" +
					myRS.getString("Encounter_TimeTrack_ReqInitialAppointment_display")+ "\t" +
					myRS.getString("Encounter_TimeTrack_ReqInitialAppointment_iMonth")+ "\t" +
					myRS.getString("Encounter_TimeTrack_ReqInitialAppointment_iDay")+ "\t" +
					myRS.getString("Encounter_TimeTrack_ReqInitialAppointment_iDOW")+ "\t" +
					myRS.getString("Encounter_TimeTrack_ReqInitialAppointment_iYear")+ "\t" +
					myRS.getString("Encounter_TimeTrack_ReqRxReview")+ "\t" +
					myRS.getString("Encounter_TimeTrack_ReqRxReview_display")+ "\t" +
					myRS.getString("Encounter_TimeTrack_ReqRxReview_iMonth")+ "\t" +
					myRS.getString("Encounter_TimeTrack_ReqRxReview_iDay")+ "\t" +
					myRS.getString("Encounter_TimeTrack_ReqRxReview_iDOW")+ "\t" +
					myRS.getString("Encounter_TimeTrack_ReqRxReview_iYear")+ "\t" +
					myRS.getString("Encounter_TimeTrack_ReqRpReview")+ "\t" +
					myRS.getString("Encounter_TimeTrack_ReqRpReview_display")+ "\t" +
					myRS.getString("Encounter_TimeTrack_ReqRpReview_iMonth")+ "\t" +
					myRS.getString("Encounter_TimeTrack_ReqRpReview_iDay")+ "\t" +
					myRS.getString("Encounter_TimeTrack_ReqRpReview_iDOW")+ "\t" +
					myRS.getString("Encounter_TimeTrack_ReqRpReview_iYear")+ "\t" +
					myRS.getString("AppointmentID")+ "\t" +
					myRS.getString("Encounter_PaidToProvider_CheckCalc")+ "\t" +
					myRS.getString("Appointment_ProviderID")+ "\t" +
					myRS.getString("Appointment_AppointmentTime")+ "\t" +
					myRS.getString("Appointment_AppointmentTime_display")+ "\t" +
					myRS.getString("Appointment_AppointmentTime_iMonth")+ "\t" +
					myRS.getString("Appointment_AppointmentTime_iDay")+ "\t" +
					myRS.getString("Appointment_AppointmentTime_iDOW")+ "\t" +
					myRS.getString("Appointment_AppointmentTime_iYear")+ "\t" +
					myRS.getString("Appointment_StatusID")+ "\t" +
					myRS.getString("PracticeID")+ "\t" +
					myRS.getString("Practice_Name")+ "\t" +
					myRS.getString("Practice_City")+ "\t" +
					myRS.getString("Practice_State")+ "\t" +
					myRS.getString("Practice_IsBlueStar")+ "\t" +
					myRS.getString("Practice_PetLinQID")+ "\t" +
					myRS.getString("Practice_SelectMRI_ID")+ "\t" +
					myRS.getString("ServiceID")+ "\t" +
					myRS.getString("Service_StatusID")+ "\t" +
					myRS.getString("Service_Status")+ "\t" +
					myRS.getString("Service_TypeID")+ "\t" +
					myRS.getString("Service_CPT")+ "\t" +
					myRS.getString("Service_CPT_Qty")+ "\t" +
					myRS.getString("Service_CPTModifier")+ "\t" +
					myRS.getString("Service_CPTBodyPart")+ "\t" +
					myRS.getString("Service_ICD_1")+ "\t" +
					myRS.getString("Service_ICD_2")+ "\t" +
					myRS.getString("Service_ICD_3")+ "\t" +
					myRS.getString("Service_ICD_4")+ "\t" +
					myRS.getString("Service_CPTText")+ "\t" +
					myRS.getString("Service_BillAmount")+ "\t" +
					myRS.getString("Service_AllowAmount")+ "\t" +
					myRS.getString("Service_AllowAmountAdjustment")+ "\t" +
					myRS.getString("Service_ReceivedAmount")+ "\t" +
					myRS.getString("Service_PaidOutAmount")+ "\t" +
					myRS.getString("Service_ReceivedAmount_CheckCalc")+ "\t" +
					myRS.getString("Service_modality")+ "\t" +
					NIMUtils.getFeeSchedulePrice(1, 1.00, myRS.getString("Practice_Zip"), myRS.getString("Service_CPT"), myRS.getString("Service_CPTModifier"), myRS.getDate("Appointment_AppointmentTime"))+"\t" +
					NIMUtils.getFeeSchedulePrice(2, 1.00, myRS.getString("Practice_Zip"), myRS.getString("Service_CPT"), myRS.getString("Service_CPTModifier"), myRS.getDate("Appointment_AppointmentTime"))+"\t"
				);
			}
			catch (Exception E){}
			body += "\n";
			out.writeBytes( body );
		}
		conn.closeAll();
		
		System.out.println("Writing to system " + displayDateSDF1Full.format(PLCUtils.getNowDate(true)));
		
        out.flush();
        System.out.println("Fin " + displayDateSDF1Full.format(PLCUtils.getNowDate(true)));
	}

}
