

package com.winstaff;

import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public class dbtNIM3_CommTrack extends Object implements InttNIM3_CommTrack
{

        db_NewBase    dbnbDB;

    public dbtNIM3_CommTrack()
    {
        dbnbDB = new db_NewBase( "tNIM3_CommTrack", "CommTrackID" );

    }    // End of default constructor

    public dbtNIM3_CommTrack( Integer iNewID )
    {
        dbnbDB = new db_NewBase( "tNIM3_CommTrack", "CommTrackID" );
        setUniqueID( iNewID );
    }    // End of Constructor knowing an ID

    public void commitData() throws SQLException
    {
            //no longer trapping/ignoring errors 2011-03-17
            dbnbDB.commitData();
    }    // End of commitData()

    public void commitDataForced() throws SQLException
    {
        dbnbDB.commitDataForced();
    }    // End of commitData()

    public Integer getUniqueID()
    {
        return dbnbDB.getUniqueID();
    }    // End of getUniqueID()

    public void setUniqueID( Integer iNewID )
    {
        dbnbDB.setUniqueID( iNewID );
    }    // End of setUniqueID()

    public void setModifyComments( String sNew )
    {
        dbnbDB.setModifyComments( sNew );
    }    // End of setUniqueID()


    public void setCommTrackID(Integer newValue)
    {
                dbnbDB.setFieldData( "CommTrackID", newValue.toString() );
    }

    public Integer getCommTrackID()
    {
        String           sValue = dbnbDB.getFieldData( "CommTrackID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setField(String fieldN, Object fieldV)
    {DebugLogger.println("setField Method not implemented yet in classNIM3_CommTrack!");}

    public String getFieldType(String fieldN)
    {return null;}


    public void setUniqueCreateDate(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "UniqueCreateDate", formatter.format( newValue ) );
    }

    public Date getUniqueCreateDate()
    {
        String           sValue = dbnbDB.getFieldData( "UniqueCreateDate" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setUniqueModifyDate(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "UniqueModifyDate", formatter.format( newValue ) );
    }

    public Date getUniqueModifyDate()
    {
        String           sValue = dbnbDB.getFieldData( "UniqueModifyDate" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setUniqueModifyComments(String newValue)
    {
                dbnbDB.setFieldData( "UniqueModifyComments", newValue.toString() );
    }

    public String getUniqueModifyComments()
    {
        String           sValue = dbnbDB.getFieldData( "UniqueModifyComments" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setCaseID(Integer newValue)
    {
                dbnbDB.setFieldData( "CaseID", newValue.toString() );
    }

    public Integer getCaseID()
    {
        String           sValue = dbnbDB.getFieldData( "CaseID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setEncounterID(Integer newValue)
    {
                dbnbDB.setFieldData( "EncounterID", newValue.toString() );
    }

    public Integer getEncounterID()
    {
        String           sValue = dbnbDB.getFieldData( "EncounterID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setReferralID(Integer newValue)
    {
                dbnbDB.setFieldData( "ReferralID", newValue.toString() );
    }

    public Integer getReferralID()
    {
        String           sValue = dbnbDB.getFieldData( "ReferralID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setServiceID(Integer newValue)
    {
                dbnbDB.setFieldData( "ServiceID", newValue.toString() );
    }

    public Integer getServiceID()
    {
        String           sValue = dbnbDB.getFieldData( "ServiceID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setIntUserID(Integer newValue)
    {
                dbnbDB.setFieldData( "IntUserID", newValue.toString() );
    }

    public Integer getIntUserID()
    {
        String           sValue = dbnbDB.getFieldData( "IntUserID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setExtUserID(Integer newValue)
    {
                dbnbDB.setFieldData( "ExtUserID", newValue.toString() );
    }

    public Integer getExtUserID()
    {
        String           sValue = dbnbDB.getFieldData( "ExtUserID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setCommTypeID(Integer newValue)
    {
                dbnbDB.setFieldData( "CommTypeID", newValue.toString() );
    }

    public Integer getCommTypeID()
    {
        String           sValue = dbnbDB.getFieldData( "CommTypeID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setCommStart(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "CommStart", formatter.format( newValue ) );
    }

    public Date getCommStart()
    {
        String           sValue = dbnbDB.getFieldData( "CommStart" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setCommEnd(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "CommEnd", formatter.format( newValue ) );
    }

    public Date getCommEnd()
    {
        String           sValue = dbnbDB.getFieldData( "CommEnd" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setMessageText(String newValue)
    {
                dbnbDB.setFieldData( "MessageText", newValue.toString() );
    }

    public String getMessageText()
    {
        String           sValue = dbnbDB.getFieldData( "MessageText" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setMessageName(String newValue)
    {
                dbnbDB.setFieldData( "MessageName", newValue.toString() );
    }

    public String getMessageName()
    {
        String           sValue = dbnbDB.getFieldData( "MessageName" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setMessageCompany(String newValue)
    {
                dbnbDB.setFieldData( "MessageCompany", newValue.toString() );
    }

    public String getMessageCompany()
    {
        String           sValue = dbnbDB.getFieldData( "MessageCompany" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setMessageEmail(String newValue)
    {
                dbnbDB.setFieldData( "MessageEmail", newValue.toString() );
    }

    public String getMessageEmail()
    {
        String           sValue = dbnbDB.getFieldData( "MessageEmail" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setMessagePhone(String newValue)
    {
                dbnbDB.setFieldData( "MessagePhone", newValue.toString() );
    }

    public String getMessagePhone()
    {
        String           sValue = dbnbDB.getFieldData( "MessagePhone" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setMessageFax(String newValue)
    {
                dbnbDB.setFieldData( "MessageFax", newValue.toString() );
    }

    public String getMessageFax()
    {
        String           sValue = dbnbDB.getFieldData( "MessageFax" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setAlertStatusCode(Integer newValue)
    {
                dbnbDB.setFieldData( "AlertStatusCode", newValue.toString() );
    }

    public Integer getAlertStatusCode()
    {
        String           sValue = dbnbDB.getFieldData( "AlertStatusCode" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setComments(String newValue)
    {
                dbnbDB.setFieldData( "Comments", newValue.toString() );
    }

    public String getComments()
    {
        String           sValue = dbnbDB.getFieldData( "Comments" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setCommReferenceID(Integer newValue)
    {
                dbnbDB.setFieldData( "CommReferenceID", newValue.toString() );
    }

    public Integer getCommReferenceID()
    {
        String           sValue = dbnbDB.getFieldData( "CommReferenceID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setLink_UserID(Integer newValue)
    {
                dbnbDB.setFieldData( "Link_UserID", newValue.toString() );
    }

    public Integer getLink_UserID()
    {
        String           sValue = dbnbDB.getFieldData( "Link_UserID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setMessageSubject(String newValue)
    {
                dbnbDB.setFieldData( "MessageSubject", newValue.toString() );
    }

    public String getMessageSubject()
    {
        String           sValue = dbnbDB.getFieldData( "MessageSubject" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }
    
    public static void main (String [] args) {
    	dbtNIM3_CommTrack obj = new dbtNIM3_CommTrack();
    	int id = obj.getIntUserID().intValue();
    	System.out.println("Default id = "+id);
    }

}    // End of getIntUserID class definition
