package com.winstaff;


import com.winstaff.dbtPracticeTypeLI;
import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public interface InttPracticeTypeLI extends dbTableInterface
{

    public void setPracticeTypeID(Integer newValue);
    public Integer getPracticeTypeID();
    public void setPracticeTypeLong(String newValue);
    public String getPracticeTypeLong();
}    // End of bltPracticeTypeLI class definition
