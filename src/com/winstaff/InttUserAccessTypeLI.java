package com.winstaff;


import com.winstaff.dbtUserAccessTypeLI;
import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public interface InttUserAccessTypeLI extends dbTableInterface
{

    public void setUserAccessTypeID(Integer newValue);
    public Integer getUserAccessTypeID();
    public void setUserAccessTypeLong(String newValue);
    public String getUserAccessTypeLong();
}    // End of bltUserAccessTypeLI class definition
