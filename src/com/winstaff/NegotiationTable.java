package com.winstaff;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * Provides NetDev Negotiation data
 * @author Po Le
 *
 */
public class NegotiationTable {
	/**
	 * Stores ZipCode
	 */
	private static String ssZIP;
	
	/**
	 * Stores Distance (miles)
	 */
	private static int iRange;
	
	/**
	 * Constructor, needs params to start data collection.
	 * @param ssZIP ZipCode
	 * @param iRange Distance
	 */
	public NegotiationTable(String ssZIP,int iRange){
		this.ssZIP = ssZIP;
		this.iRange = iRange;
	}
	
	/**
	 * LinkedHashMap that stores FSID data. Could of been a class instead, but oh well.
	 * @param fsid FSID to calculate.
	 * @return LinkedHashMap of 6 modals of inputed FSID
	 */ 
	public static LinkedHashMap<String, String> getFSID(int fsid){
		LinkedHashMap<String, String> FSID = new LinkedHashMap<String, String>();
		// getPayerAllowAmount(Integer iPayerID,String sZIP, String sCPT, String sMod, java.util.Date EffDate)
		FSID.put("CTw", NIMUtils.getPayerAllowAmount(fsid,ssZIP.trim().substring(0,5),"ct_w","",new Date()).toString());
		FSID.put("CTwo", NIMUtils.getPayerAllowAmount(fsid,ssZIP.trim().substring(0,5),"ct_wo","",new Date()).toString());
		FSID.put("CTwwo", NIMUtils.getPayerAllowAmount(fsid,ssZIP.trim().substring(0,5),"ct_wwo","",new Date()).toString());
		FSID.put("MRw", NIMUtils.getPayerAllowAmount(fsid,ssZIP.trim().substring(0,5),"mr_w","",new Date()).toString());
		FSID.put("MRwo", NIMUtils.getPayerAllowAmount(fsid,ssZIP.trim().substring(0,5),"mr_wo","",new Date()).toString());
		FSID.put("MRwwo", NIMUtils.getPayerAllowAmount(fsid,ssZIP.trim().substring(0,5),"mr_wwo","",new Date()).toString());
		FSID.put("query", fsid+" | "+ssZIP+" | "+"mr_wwo"+" | "+""+" | "+new Date());
		
		//NIMUtils.getPayerAllowAmount(1500,myPractM_AO.getZIP().trim().substring(0,5),Service_modality,"",new java.util.Date() )
		return FSID;
	}
	/**
	 * Gets a list of Payers from inputed zip and distance
	 * @return List of Payers
	 */
	public static List<Payerlist> getPayerist(){
		List<Payerlist> pl = new ArrayList<Payerlist>();
		String query ="select payerid, parentpayerid, payername, price_mod_ct_w,price_mod_ct_wo,price_mod_ct_wwo, price_mod_mri_w,price_mod_mri_wo,price_mod_mri_wwo,(select count(*) from \"vMQ4_Encounter_NoVoid_BP\" mq where pm.payerid = mq.\"PayerID\" and \"Referral_ReceiveDate_iYear\" = 2011) volume2011,(select count(*) from \"vMQ4_Encounter_NoVoid_BP\" mq where pm.payerid = mq.\"PayerID\" and \"Referral_ReceiveDate_iYear\" = 2012) volume2012,(select count(*) from \"vMQ4_Encounter_NoVoid_BP\" mq where pm.payerid = mq.\"PayerID\" and \"Referral_ReceiveDate_iYear\" = 2013) volume2013 from tnim3_payermaster pm " +
		"where officestateid = "+getStateFromZip() +
		" and price_mod_ct_w+price_mod_ct_wo+price_mod_ct_wwo+price_mod_mri_w+price_mod_mri_wo+price_mod_mri_wwo!=0 order by payerid";
		searchDB2 conn = new searchDB2();
		java.sql.ResultSet rs = conn.executeStatement(query);
		try {
			while (rs.next()){
				pl.add(new Payerlist(rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getString(7), rs.getString(8), rs.getString(9),rs.getString(10),rs.getString(11),rs.getString(12)));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			conn.closeAll();
		}
		return pl;
	}
	/**
	 * Gets a list of IC from inputed zip and distance
	 * @return List of ICs
	 */
	public static List<IClist> getIClist(){
		ZIP_CODE_object myZCo = NIMUtils.get_zips_in_range(ssZIP,iRange);
        java.util.Vector myV = myZCo.getZipsInRange(iRange);
		
        String zipSQL = "";
        boolean isFirst = true;
        for (int i10=0;i10<myV.size();i10++)
        {
        	if (isFirst){
        		zipSQL +=(String) myV.elementAt(i10);
        		isFirst = false;
        	}
        	zipSQL +="|"+(String) myV.elementAt(i10);
           
        }
		List<IClist> icList= new ArrayList<IClist>();
		
		String query = "select practiceid, practicename, price_mod_ct_w,price_mod_ct_wo,price_mod_ct_wwo,price_mod_mri_w,price_mod_mri_wo,price_mod_mri_wwo from tpracticemaster where " + 
				"officezip SIMILAR TO '%("+zipSQL+")%' "+
				"and price_mod_ct_w+price_mod_ct_wo+price_mod_ct_wwo+price_mod_mri_w+price_mod_mri_wo+price_mod_mri_wwo>0";
		
		searchDB2 conn = new searchDB2();
		
		java.sql.ResultSet rs = conn.executeStatement(query);
		
		try {
			while(rs.next()){
				icList.add(new IClist (rs.getString(2),rs.getString(3),rs.getString(4),rs.getString(5),rs.getString(6),rs.getString(7),rs.getString(8)));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			conn.closeAll();
		}
		
		return icList;
		
	}
	/**
	 * Translate ssZIP into state string
	 * @return Short state
	 */
 	private static String getStateFromZip(){
		String query  = "select stateid from zip_code zip join tstateli st on st.shortstate = zip.state_prefix where zip.zip_code = '"+ssZIP+"'";
		searchDB2 conn = new searchDB2();
		
		java.sql.ResultSet rs = conn.executeStatement(query);
		
		try {
			while(rs.next()){
				return rs.getString("stateid");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			conn.closeAll();
		}
		
		return null;
	}
 	/**
 	 * Model Class that Stores Payer Info to be parsed in view
 	 * @author Po Le
 	 *
 	 */
	public static class Payerlist{
		private String payerName;
		private String priceCTw;
		private String priceCTwo;
		private String priceCTwwo;
		private String priceMRw;
		private String priceMRwo;
		private String priceMRwwo;
		private String volume2011;
		private String volume2012;
		private String volume2013;
		public Payerlist(String payerName, String priceCTw, String priceCTwo, String priceCTwwo, String priceMRw, String priceMRwo, String priceMRwwo, String volume2011, String volume2012, String volume2013) {
			this.payerName = payerName;
			this.priceCTw = priceCTw;
			this.priceCTwo = priceCTwo;
			this.priceCTwwo = priceCTwwo;
			this.priceMRw = priceMRw;
			this.priceMRwo = priceMRwo;
			this.priceMRwwo = priceMRwwo;
			this.volume2011 = volume2011;
			this.volume2012 = volume2012;
			this.volume2013 = volume2013;
		}
		public String getPayerName() {
			return payerName;
		}
		public void setPayerName(String payerName) {
			this.payerName = payerName;
		}
		public String getPriceCTw() {
			return priceCTw;
		}
		public void setPriceCTw(String priceCTw) {
			this.priceCTw = priceCTw;
		}
		public String getPriceCTwo() {
			return priceCTwo;
		}
		public void setPriceCTwo(String priceCTwo) {
			this.priceCTwo = priceCTwo;
		}
		public String getPriceCTwwo() {
			return priceCTwwo;
		}
		public void setPriceCTwwo(String priceCTwwo) {
			this.priceCTwwo = priceCTwwo;
		}
		public String getPriceMRw() {
			return priceMRw;
		}
		public void setPriceMRw(String priceMRw) {
			this.priceMRw = priceMRw;
		}
		public String getPriceMRwo() {
			return priceMRwo;
		}
		public void setPriceMRwo(String priceMRwo) {
			this.priceMRwo = priceMRwo;
		}
		public String getPriceMRwwo() {
			return priceMRwwo;
		}
		public void setPriceMRwwo(String priceMRwwo) {
			this.priceMRwwo = priceMRwwo;
		}
		public String getVolume2011() {
			return volume2011;
		}
		public void setVolume2011(String volume2011) {
			this.volume2011 = volume2011;
		}
		public String getVolume2012() {
			return volume2012;
		}
		public void setVolume2012(String volume2012) {
			this.volume2012 = volume2012;
		}
		public String getVolume2013() {
			return volume2013;
		}
		public void setVolume2013(String volume2013) {
			this.volume2013 = volume2013;
		}
		
		
	}
	
	/**
 	 * Model Class that Stores IC Info to be parsed in view
 	 * @author Po Le
 	 *
 	 */
	public static class  IClist{
		private String practiceName;
		private String priceCTw;
		private String priceCTwo;
		private String priceCTwwo;
		private String priceMRw;
		private String priceMRwo;
		private String priceMRwwo;
		public IClist(String practiceName, String priceCTw, String priceCTwo, String priceCTwwo, String priceMRw, String priceMRwo, String priceMRwwo) {
			this.practiceName = practiceName;
			this.priceCTw = priceCTw;
			this.priceCTwo = priceCTwo;
			this.priceCTwwo = priceCTwwo;
			this.priceMRw = priceMRw;
			this.priceMRwo = priceMRwo;
			this.priceMRwwo = priceMRwwo;
		}
		public String getPracticeName() {
			return practiceName;
		}
		public void setPracticeName(String practiceName) {
			this.practiceName = practiceName;
		}
		public String getPriceCTw() {
			return priceCTw;
		}
		public void setPriceCTw(String priceCTw) {
			this.priceCTw = priceCTw;
		}
		public String getPriceCTwo() {
			return priceCTwo;
		}
		public void setPriceCTwo(String priceCTwo) {
			this.priceCTwo = priceCTwo;
		}
		public String getPriceCTwwo() {
			return priceCTwwo;
		}
		public void setPriceCTwwo(String priceCTwwo) {
			this.priceCTwwo = priceCTwwo;
		}
		public String getPriceMRw() {
			return priceMRw;
		}
		public void setPriceMRw(String priceMRw) {
			this.priceMRw = priceMRw;
		}
		public String getPriceMRwo() {
			return priceMRwo;
		}
		public void setPriceMRwo(String priceMRwo) {
			this.priceMRwo = priceMRwo;
		}
		public String getPriceMRwwo() {
			return priceMRwwo;
		}
		public void setPriceMRwwo(String priceMRwwo) {
			this.priceMRwwo = priceMRwwo;
		}
		
	}
	
	
	
	
}

