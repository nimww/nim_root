

package com.winstaff;

import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public interface InttCCardTransaction extends dbTableInterface
{

    public void setCCardTransactionID(Integer newValue);
    public Integer getCCardTransactionID();
    public void setUniqueCreateDate(Date newValue);
    public Date getUniqueCreateDate();
    public void setUniqueModifyDate(Date newValue);
    public Date getUniqueModifyDate();
    public void setUniqueModifyComments(String newValue);
    public String getUniqueModifyComments();
    public void setTransactionDate(Date newValue);
    public Date getTransactionDate();
    public void setActionID(Integer newValue);
    public Integer getActionID();
    public void setCName(String newValue);
    public String getCName();
    public void setCAddress(String newValue);
    public String getCAddress();
    public void setCCity(String newValue);
    public String getCCity();
    public void setCState(String newValue);
    public String getCState();
    public void setCZip(String newValue);
    public String getCZip();
    public void setCCNumber(Integer newValue);
    public Integer getCCNumber();
    public void setCEXP(String newValue);
    public String getCEXP();
    public void setOrderNumber(String newValue);
    public String getOrderNumber();
    public void setAuthNumber(String newValue);
    public String getAuthNumber();
    public void setAmountCharged(String newValue);
    public String getAmountCharged();
    public void setEncounterID(Integer newValue);
    public Integer getEncounterID();
}    // End of bltCCardTransaction class definition
