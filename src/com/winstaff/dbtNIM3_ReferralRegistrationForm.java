

package com.winstaff;

import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public class dbtNIM3_ReferralRegistrationForm extends Object implements InttNIM3_ReferralRegistrationForm
{

        db_NewBase    dbnbDB;

    public dbtNIM3_ReferralRegistrationForm()
    {
        dbnbDB = new db_NewBase( "tNIM3_ReferralRegistrationForm", "PortalRegisterID" );

    }    // End of default constructor

    public dbtNIM3_ReferralRegistrationForm( Integer iNewID )
    {
        dbnbDB = new db_NewBase( "tNIM3_ReferralRegistrationForm", "PortalRegisterID" );
        setUniqueID( iNewID );
    }    // End of Constructor knowing an ID

    public void commitData() throws SQLException
    {
            //no longer trapping/ignoring errors 2011-03-17
            dbnbDB.commitData();
    }    // End of commitData()

    public void commitDataForced() throws SQLException
    {
        dbnbDB.commitDataForced();
    }    // End of commitData()

    public Integer getUniqueID()
    {
        return dbnbDB.getUniqueID();
    }    // End of getUniqueID()

    public void setUniqueID( Integer iNewID )
    {
        dbnbDB.setUniqueID( iNewID );
    }    // End of setUniqueID()

    public void setModifyComments( String sNew )
    {
        dbnbDB.setModifyComments( sNew );
    }    // End of setUniqueID()


    public void setPortalRegisterID(Integer newValue)
    {
                dbnbDB.setFieldData( "PortalRegisterID", newValue.toString() );
    }

    public Integer getPortalRegisterID()
    {
        String           sValue = dbnbDB.getFieldData( "PortalRegisterID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setField(String fieldN, Object fieldV)
    {DebugLogger.println("setField Method not implemented yet in classReferralRegistrationForm!");}

    public String getFieldType(String fieldN)
    {return null;}


    public void setUniqueCreateDate(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "UniqueCreateDate", formatter.format( newValue ) );
    }

    public Date getUniqueCreateDate()
    {
        String           sValue = dbnbDB.getFieldData( "UniqueCreateDate" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setUniqueModifyDate(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "UniqueModifyDate", formatter.format( newValue ) );
    }

    public Date getUniqueModifyDate()
    {
        String           sValue = dbnbDB.getFieldData( "UniqueModifyDate" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setUniqueModifyComments(String newValue)
    {
                dbnbDB.setFieldData( "UniqueModifyComments", newValue.toString() );
    }

    public String getUniqueModifyComments()
    {
        String           sValue = dbnbDB.getFieldData( "UniqueModifyComments" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setStatusID(Integer newValue)
    {
                dbnbDB.setFieldData( "StatusID", newValue.toString() );
    }

    public Integer getStatusID()
    {
        String           sValue = dbnbDB.getFieldData( "StatusID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setFirstName(String newValue)
    {
                dbnbDB.setFieldData( "FirstName", newValue.toString() );
    }

    public String getFirstName()
    {
        String           sValue = dbnbDB.getFieldData( "FirstName" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setLastName(String newValue)
    {
                dbnbDB.setFieldData( "LastName", newValue.toString() );
    }

    public String getLastName()
    {
        String           sValue = dbnbDB.getFieldData( "LastName" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setPhone(String newValue)
    {
                dbnbDB.setFieldData( "Phone", newValue.toString() );
    }

    public String getPhone()
    {
        String           sValue = dbnbDB.getFieldData( "Phone" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setFax(String newValue)
    {
                dbnbDB.setFieldData( "Fax", newValue.toString() );
    }

    public String getFax()
    {
        String           sValue = dbnbDB.getFieldData( "Fax" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setEmail(String newValue)
    {
                dbnbDB.setFieldData( "Email", newValue.toString() );
    }

    public String getEmail()
    {
        String           sValue = dbnbDB.getFieldData( "Email" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setMessageText(String newValue)
    {
                dbnbDB.setFieldData( "MessageText", newValue.toString() );
    }

    public String getMessageText()
    {
        String           sValue = dbnbDB.getFieldData( "MessageText" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setEncounterID(Integer newValue)
    {
                dbnbDB.setFieldData( "EncounterID", newValue.toString() );
    }

    public Integer getEncounterID()
    {
        String           sValue = dbnbDB.getFieldData( "EncounterID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

}    // End of bltNIM3_ReferralRegistrationForm class definition
