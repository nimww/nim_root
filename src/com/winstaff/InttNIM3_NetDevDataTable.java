

package com.winstaff;

import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public interface InttNIM3_NetDevDataTable extends dbTableInterface
{

    public void setNetDevDataTableID(Integer newValue);
    public Integer getNetDevDataTableID();
    public void setUniqueCreateDate(Date newValue);
    public Date getUniqueCreateDate();
    public void setUniqueModifyDate(Date newValue);
    public Date getUniqueModifyDate();
    public void setUniqueModifyComments(String newValue);
    public String getUniqueModifyComments();
    public void setEncounterID(Integer newValue);
    public Integer getEncounterID();
    public void setStatusID(Integer newValue);
    public Integer getStatusID();
    public void setTimeStarted(Date newValue);
    public Date getTimeStarted();
}    // End of bltNIM3_NetDevDataTable class definition
