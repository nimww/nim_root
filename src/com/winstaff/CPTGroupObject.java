package com.winstaff;

import java.util.Vector;

/**
 * Created by IntelliJ IDEA.
 * User: ste
 * Date: Jun 4, 2010
 * Time: 6:28:03 PM
 * To change this template use File | Settings | File Templates.
 */
public class CPTGroupObject
{
    public CPTGroupObject(Vector CPTList)
    {
        this.CPTList = CPTList;
    }

    public CPTGroupObject()
    {
        CPTList = new Vector();
    }


    public CPTGroupObject(String sCPT1, String sCPT2, String sCPT3, String sCPT4, String sCPT5)
        {
            CPTList = new Vector();
            CPTList.add(sCPT1);
            CPTList.add(sCPT2);
            CPTList.add(sCPT3);
            CPTList.add(sCPT4);
            CPTList.add(sCPT5);
        }

    public CPTGroupObject(String sCPT1, String sCPT2, String sCPT3, String sCPT4)
        {
            CPTList = new Vector();
            CPTList.add(sCPT1);
            CPTList.add(sCPT2);
            CPTList.add(sCPT3);
            CPTList.add(sCPT4);
        }

    public CPTGroupObject(String sCPT1, String sCPT2, String sCPT3)
        {
            CPTList = new Vector();
            CPTList.add(sCPT1);
            CPTList.add(sCPT2);
            CPTList.add(sCPT3);
        }

    public CPTGroupObject(String sCPT1, String sCPT2)
        {
            CPTList = new Vector();
            CPTList.add(sCPT1);
            CPTList.add(sCPT2);
        }

    public CPTGroupObject(String sCPT1)
        {
            CPTList = new Vector();
            CPTList.add(sCPT1);
        }

    public Vector getCPTList() {
        return CPTList;
    }

    public void setCPTList(Vector CPTList) {
        this.CPTList = CPTList;
    }

    public void addToCPTList(String sCPT)
    {
        this.getCPTList().add(sCPT);
    }

    public boolean isEmpty()
    {
        return (this.getCPTList().isEmpty());
    }

    public String getSQLDB_ArrayList()
    {
        String myVal = "";
        try
        {
            for (int i=0;i<this.getCPTList().size();i++)
            {
                if (i!=0)
                {
                    myVal += ", ";
                }
                myVal += "'" + (String) this.getCPTList().elementAt(i) + "'";
            }
        }
        catch (Exception e)
        {
            myVal = null;
        }
        return myVal;
    }

    public String getFirstCPT()
    {
        String myVal = null;
        try
        {
            myVal = (String) this.getCPTList().elementAt(0);
        }
        catch (Exception e)
        {
            myVal = null;
        }
        return myVal;
    }


    private boolean hasMRI = false;
    private boolean hasCT = false;
    private boolean hasEMG = false;
    private boolean hasFL = false;
    private boolean hasUS = false;
    private boolean hasNM = false;
    private boolean hasPET = false;
    private boolean hasXR = false;


    public boolean isHasMRI()
    {
        if (!DiagnosticAnalysisHasRun)
        {
            this.RunDiagnosticAnalysis();
        }
        return hasMRI;
    }

    public void setHasMRI(boolean hasMRI)
    {
        this.hasMRI = hasMRI;
    }

    public boolean isHasCT()
    {
        if (!DiagnosticAnalysisHasRun)
        {
            this.RunDiagnosticAnalysis();
        }
        return hasCT;
    }

    public void setHasCT(boolean hasCT)
    {
        this.hasCT = hasCT;
    }

    public boolean isHasEMG()
    {
        if (!DiagnosticAnalysisHasRun)
        {
            this.RunDiagnosticAnalysis();
        }
        return hasEMG;
    }

    public void setHasEMG(boolean hasEMG)
    {
        this.hasEMG = hasEMG;
    }

    public boolean isHasFL()
    {
        if (!DiagnosticAnalysisHasRun)
        {
            this.RunDiagnosticAnalysis();
        }
        return hasFL;
    }

    public void setHasFL(boolean hasFL)
    {
        this.hasFL = hasFL;
    }

    public boolean isHasUS()
    {
        if (!DiagnosticAnalysisHasRun)
        {
            this.RunDiagnosticAnalysis();
        }
        return hasUS;
    }

    public void setHasUS(boolean hasUS)
    {
        this.hasUS = hasUS;
    }

    public boolean isHasNM()
    {
        if (!DiagnosticAnalysisHasRun)
        {
            this.RunDiagnosticAnalysis();
        }
        return hasNM;
    }

    public void setHasNM(boolean hasNM)
    {
        this.hasNM = hasNM;
    }

    public boolean isHasPET()
    {
        if (!DiagnosticAnalysisHasRun)
        {
            this.RunDiagnosticAnalysis();
        }
        return hasPET;
    }

    public void setHasPET(boolean hasPET)
    {
        this.hasPET = hasPET;
    }

    public boolean isHasXR()
    {
        if (!DiagnosticAnalysisHasRun)
        {
            this.RunDiagnosticAnalysis();
        }
        return hasXR;
    }

    private void RunDiagnosticAnalysis()
    {
        this.DiagnosticAnalysisHasRun = true;
    }

    public void setHasXR(boolean hasXR)
    {
        this.hasXR = hasXR;
    }


    private boolean DiagnosticAnalysisHasRun = false;
    private Vector CPTList = null;
}
