

package com.winstaff;

import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public class dbtNIM3_EligibilityReference extends Object implements InttNIM3_EligibilityReference
{

        db_NewBase    dbnbDB;

    public dbtNIM3_EligibilityReference()
    {
        dbnbDB = new db_NewBase( "tNIM3_EligibilityReference", "EligibilityReferenceID" );

    }    // End of default constructor

    public dbtNIM3_EligibilityReference( Integer iNewID )
    {
        dbnbDB = new db_NewBase( "tNIM3_EligibilityReference", "EligibilityReferenceID" );
        setUniqueID( iNewID );
    }    // End of Constructor knowing an ID

    public void commitData() throws SQLException
    {
        //no longer trapping/ignoring errors 2011-03-17
        dbnbDB.commitData();
    }    // End of commitData()

    public void commitDataDisableAudit() throws SQLException
    {
        //no longer trapping/ignoring errors 2011-03-17
        dbnbDB.commitDataDisableAudit();
    }    // End of commitData()

    public void commitDataForced() throws SQLException
    {
        dbnbDB.commitDataForced();
    }    // End of commitData()

    public Integer getUniqueID()
    {
        return dbnbDB.getUniqueID();
    }    // End of getUniqueID()

    public void setUniqueID( Integer iNewID )
    {
        dbnbDB.setUniqueID( iNewID );
    }    // End of setUniqueID()

    public void setModifyComments( String sNew )
    {
        dbnbDB.setModifyComments( sNew );
    }    // End of setUniqueID()


    public void setEligibilityReferenceID(Integer newValue)
    {
                dbnbDB.setFieldData( "EligibilityReferenceID", newValue.toString() );
    }

    public Integer getEligibilityReferenceID()
    {
        String           sValue = dbnbDB.getFieldData( "EligibilityReferenceID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setField(String fieldN, Object fieldV)
    {DebugLogger.println("setField Method not implemented yet in classNIM3_EligibilityReference!");}

    public String getFieldType(String fieldN)
    {return null;}


    public void setUniqueCreateDate(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "UniqueCreateDate", formatter.format( newValue ) );
    }

    public Date getUniqueCreateDate()
    {
        String           sValue = dbnbDB.getFieldData( "UniqueCreateDate" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setUniqueModifyDate(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "UniqueModifyDate", formatter.format( newValue ) );
    }

    public Date getUniqueModifyDate()
    {
        String           sValue = dbnbDB.getFieldData( "UniqueModifyDate" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setUniqueModifyComments(String newValue)
    {
                dbnbDB.setFieldData( "UniqueModifyComments", newValue.toString() );
    }

    public String getUniqueModifyComments()
    {
        String           sValue = dbnbDB.getFieldData( "UniqueModifyComments" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setPayerID(Integer newValue)
    {
                dbnbDB.setFieldData( "PayerID", newValue.toString() );
    }

    public Integer getPayerID()
    {
        String           sValue = dbnbDB.getFieldData( "PayerID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setCaseClaimNumber(String newValue)
    {
                dbnbDB.setFieldData( "CaseClaimNumber", newValue.toString() );
    }

    public String getCaseClaimNumber()
    {
        String           sValue = dbnbDB.getFieldData( "CaseClaimNumber" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setClientPayerID(String newValue)
    {
                dbnbDB.setFieldData( "ClientPayerID", newValue.toString() );
    }

    public String getClientPayerID()
    {
        String           sValue = dbnbDB.getFieldData( "ClientPayerID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setPayerIndicator(String newValue)
    {
                dbnbDB.setFieldData( "PayerIndicator", newValue.toString() );
    }

    public String getPayerIndicator()
    {
        String           sValue = dbnbDB.getFieldData( "PayerIndicator" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setEmployerName(String newValue)
    {
                dbnbDB.setFieldData( "EmployerName", newValue.toString() );
    }

    public String getEmployerName()
    {
        String           sValue = dbnbDB.getFieldData( "EmployerName" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setEmployerPhone(String newValue)
    {
                dbnbDB.setFieldData( "EmployerPhone", newValue.toString() );
    }

    public String getEmployerPhone()
    {
        String           sValue = dbnbDB.getFieldData( "EmployerPhone" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setEmployerAddress1(String newValue)
    {
                dbnbDB.setFieldData( "EmployerAddress1", newValue.toString() );
    }

    public String getEmployerAddress1()
    {
        String           sValue = dbnbDB.getFieldData( "EmployerAddress1" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setEmployerAddress2(String newValue)
    {
                dbnbDB.setFieldData( "EmployerAddress2", newValue.toString() );
    }

    public String getEmployerAddress2()
    {
        String           sValue = dbnbDB.getFieldData( "EmployerAddress2" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setEmployerCity(String newValue)
    {
                dbnbDB.setFieldData( "EmployerCity", newValue.toString() );
    }

    public String getEmployerCity()
    {
        String           sValue = dbnbDB.getFieldData( "EmployerCity" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setEmployerCounty(String newValue)
    {
                dbnbDB.setFieldData( "EmployerCounty", newValue.toString() );
    }

    public String getEmployerCounty()
    {
        String           sValue = dbnbDB.getFieldData( "EmployerCounty" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setEmployerState(String newValue)
    {
                dbnbDB.setFieldData( "EmployerState", newValue.toString() );
    }

    public String getEmployerState()
    {
        String           sValue = dbnbDB.getFieldData( "EmployerState" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setEmployerZip(String newValue)
    {
                dbnbDB.setFieldData( "EmployerZip", newValue.toString() );
    }

    public String getEmployerZip()
    {
        String           sValue = dbnbDB.getFieldData( "EmployerZip" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setDateCaseOpened(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "DateCaseOpened", formatter.format( newValue ) );
    }

    public Date getDateCaseOpened()
    {
        String           sValue = dbnbDB.getFieldData( "DateCaseOpened" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setDateOfInjury(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "DateOfInjury", formatter.format( newValue ) );
    }

    public Date getDateOfInjury()
    {
        String           sValue = dbnbDB.getFieldData( "DateOfInjury" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setAttorneyFirstName(String newValue)
    {
                dbnbDB.setFieldData( "AttorneyFirstName", newValue.toString() );
    }

    public String getAttorneyFirstName()
    {
        String           sValue = dbnbDB.getFieldData( "AttorneyFirstName" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setAttorneyLastName(String newValue)
    {
                dbnbDB.setFieldData( "AttorneyLastName", newValue.toString() );
    }

    public String getAttorneyLastName()
    {
        String           sValue = dbnbDB.getFieldData( "AttorneyLastName" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setAttorneyWorkPhone(String newValue)
    {
                dbnbDB.setFieldData( "AttorneyWorkPhone", newValue.toString() );
    }

    public String getAttorneyWorkPhone()
    {
        String           sValue = dbnbDB.getFieldData( "AttorneyWorkPhone" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setAttorneyFax(String newValue)
    {
                dbnbDB.setFieldData( "AttorneyFax", newValue.toString() );
    }

    public String getAttorneyFax()
    {
        String           sValue = dbnbDB.getFieldData( "AttorneyFax" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setPatientAccountNumber(String newValue)
    {
                dbnbDB.setFieldData( "PatientAccountNumber", newValue.toString() );
    }

    public String getPatientAccountNumber()
    {
        String           sValue = dbnbDB.getFieldData( "PatientAccountNumber" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setPatientFirstName(String newValue)
    {
                dbnbDB.setFieldData( "PatientFirstName", newValue.toString() );
    }

    public String getPatientFirstName()
    {
        String           sValue = dbnbDB.getFieldData( "PatientFirstName" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setPatientLastName(String newValue)
    {
                dbnbDB.setFieldData( "PatientLastName", newValue.toString() );
    }

    public String getPatientLastName()
    {
        String           sValue = dbnbDB.getFieldData( "PatientLastName" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setPatientDOB(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "PatientDOB", formatter.format( newValue ) );
    }

    public Date getPatientDOB()
    {
        String           sValue = dbnbDB.getFieldData( "PatientDOB" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setPatientSSN(String newValue)
    {
                dbnbDB.setFieldData( "PatientSSN", newValue.toString() );
    }

    public String getPatientSSN()
    {
        String           sValue = dbnbDB.getFieldData( "PatientSSN" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setPatientGender(String newValue)
    {
                dbnbDB.setFieldData( "PatientGender", newValue.toString() );
    }

    public String getPatientGender()
    {
        String           sValue = dbnbDB.getFieldData( "PatientGender" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setPatientAddress1(String newValue)
    {
                dbnbDB.setFieldData( "PatientAddress1", newValue.toString() );
    }

    public String getPatientAddress1()
    {
        String           sValue = dbnbDB.getFieldData( "PatientAddress1" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setPatientAddress2(String newValue)
    {
                dbnbDB.setFieldData( "PatientAddress2", newValue.toString() );
    }

    public String getPatientAddress2()
    {
        String           sValue = dbnbDB.getFieldData( "PatientAddress2" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setPatientCity(String newValue)
    {
                dbnbDB.setFieldData( "PatientCity", newValue.toString() );
    }

    public String getPatientCity()
    {
        String           sValue = dbnbDB.getFieldData( "PatientCity" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setPatientState(String newValue)
    {
                dbnbDB.setFieldData( "PatientState", newValue.toString() );
    }

    public String getPatientState()
    {
        String           sValue = dbnbDB.getFieldData( "PatientState" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setPatientZip(String newValue)
    {
                dbnbDB.setFieldData( "PatientZip", newValue.toString() );
    }

    public String getPatientZip()
    {
        String           sValue = dbnbDB.getFieldData( "PatientZip" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setPatientHomePhone(String newValue)
    {
                dbnbDB.setFieldData( "PatientHomePhone", newValue.toString() );
    }

    public String getPatientHomePhone()
    {
        String           sValue = dbnbDB.getFieldData( "PatientHomePhone" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setPatientWorkPhone(String newValue)
    {
                dbnbDB.setFieldData( "PatientWorkPhone", newValue.toString() );
    }

    public String getPatientWorkPhone()
    {
        String           sValue = dbnbDB.getFieldData( "PatientWorkPhone" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setPatientCellPhone(String newValue)
    {
                dbnbDB.setFieldData( "PatientCellPhone", newValue.toString() );
    }

    public String getPatientCellPhone()
    {
        String           sValue = dbnbDB.getFieldData( "PatientCellPhone" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setPatientEmail(String newValue)
    {
                dbnbDB.setFieldData( "PatientEmail", newValue.toString() );
    }

    public String getPatientEmail()
    {
        String           sValue = dbnbDB.getFieldData( "PatientEmail" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setCorrusClaimId(String newValue)
    {
                dbnbDB.setFieldData( "CorrusClaimId", newValue.toString() );
    }

    public String getCorrusClaimId()
    {
        String           sValue = dbnbDB.getFieldData( "CorrusClaimId" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setClaimdate(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "Claimdate", formatter.format( newValue ) );
    }

    public Date getClaimdate()
    {
        String           sValue = dbnbDB.getFieldData( "Claimdate" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setClientClaimNum(String newValue)
    {
                dbnbDB.setFieldData( "ClientClaimNum", newValue.toString() );
    }

    public String getClientClaimNum()
    {
        String           sValue = dbnbDB.getFieldData( "ClientClaimNum" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setPrimaryDiagCode(String newValue)
    {
                dbnbDB.setFieldData( "PrimaryDiagCode", newValue.toString() );
    }

    public String getPrimaryDiagCode()
    {
        String           sValue = dbnbDB.getFieldData( "PrimaryDiagCode" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setDiagCode2(String newValue)
    {
                dbnbDB.setFieldData( "DiagCode2", newValue.toString() );
    }

    public String getDiagCode2()
    {
        String           sValue = dbnbDB.getFieldData( "DiagCode2" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setDiagCode3(String newValue)
    {
                dbnbDB.setFieldData( "DiagCode3", newValue.toString() );
    }

    public String getDiagCode3()
    {
        String           sValue = dbnbDB.getFieldData( "DiagCode3" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setClaimstatus(String newValue)
    {
                dbnbDB.setFieldData( "Claimstatus", newValue.toString() );
    }

    public String getClaimstatus()
    {
        String           sValue = dbnbDB.getFieldData( "Claimstatus" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setCaseManagerFirstName(String newValue)
    {
                dbnbDB.setFieldData( "CaseManagerFirstName", newValue.toString() );
    }

    public String getCaseManagerFirstName()
    {
        String           sValue = dbnbDB.getFieldData( "CaseManagerFirstName" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setCaseManagerLastName(String newValue)
    {
                dbnbDB.setFieldData( "CaseManagerLastName", newValue.toString() );
    }

    public String getCaseManagerLastName()
    {
        String           sValue = dbnbDB.getFieldData( "CaseManagerLastName" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setAccountReference(String newValue)
    {
                dbnbDB.setFieldData( "AccountReference", newValue.toString() );
    }

    public String getAccountReference()
    {
        String           sValue = dbnbDB.getFieldData( "AccountReference" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setControverted(String newValue)
    {
                dbnbDB.setFieldData( "Controverted", newValue.toString() );
    }

    public String getControverted()
    {
        String           sValue = dbnbDB.getFieldData( "Controverted" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setMMI(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "MMI", formatter.format( newValue ) );
    }

    public Date getMMI()
    {
        String           sValue = dbnbDB.getFieldData( "MMI" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setDatabaseID(String newValue)
    {
                dbnbDB.setFieldData( "DatabaseID", newValue.toString() );
    }

    public String getDatabaseID()
    {
        String           sValue = dbnbDB.getFieldData( "DatabaseID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setJurisdicationState(String newValue)
    {
                dbnbDB.setFieldData( "JurisdicationState", newValue.toString() );
    }

    public String getJurisdicationState()
    {
        String           sValue = dbnbDB.getFieldData( "JurisdicationState" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setAdjusterReferenceID(String newValue)
    {
                dbnbDB.setFieldData( "AdjusterReferenceID", newValue.toString() );
    }

    public String getAdjusterReferenceID()
    {
        String           sValue = dbnbDB.getFieldData( "AdjusterReferenceID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setAdjusterFirstName(String newValue)
    {
                dbnbDB.setFieldData( "AdjusterFirstName", newValue.toString() );
    }

    public String getAdjusterFirstName()
    {
        String           sValue = dbnbDB.getFieldData( "AdjusterFirstName" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setAdjusterLastName(String newValue)
    {
                dbnbDB.setFieldData( "AdjusterLastName", newValue.toString() );
    }

    public String getAdjusterLastName()
    {
        String           sValue = dbnbDB.getFieldData( "AdjusterLastName" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setAdjusterPhoneNumber(String newValue)
    {
                dbnbDB.setFieldData( "AdjusterPhoneNumber", newValue.toString() );
    }

    public String getAdjusterPhoneNumber()
    {
        String           sValue = dbnbDB.getFieldData( "AdjusterPhoneNumber" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setAdjusterFaxNumber(String newValue)
    {
                dbnbDB.setFieldData( "AdjusterFaxNumber", newValue.toString() );
    }

    public String getAdjusterFaxNumber()
    {
        String           sValue = dbnbDB.getFieldData( "AdjusterFaxNumber" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setAdjusterEmail(String newValue)
    {
                dbnbDB.setFieldData( "AdjusterEmail", newValue.toString() );
    }

    public String getAdjusterEmail()
    {
        String           sValue = dbnbDB.getFieldData( "AdjusterEmail" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setAdjusterCompany(String newValue)
    {
                dbnbDB.setFieldData( "AdjusterCompany", newValue.toString() );
    }

    public String getAdjusterCompany()
    {
        String           sValue = dbnbDB.getFieldData( "AdjusterCompany" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setAdjusterAddress1(String newValue)
    {
                dbnbDB.setFieldData( "AdjusterAddress1", newValue.toString() );
    }

    public String getAdjusterAddress1()
    {
        String           sValue = dbnbDB.getFieldData( "AdjusterAddress1" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setAdjusterAddress2(String newValue)
    {
                dbnbDB.setFieldData( "AdjusterAddress2", newValue.toString() );
    }

    public String getAdjusterAddress2()
    {
        String           sValue = dbnbDB.getFieldData( "AdjusterAddress2" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setAdjusterCity(String newValue)
    {
                dbnbDB.setFieldData( "AdjusterCity", newValue.toString() );
    }

    public String getAdjusterCity()
    {
        String           sValue = dbnbDB.getFieldData( "AdjusterCity" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setAdjusterState(String newValue)
    {
                dbnbDB.setFieldData( "AdjusterState", newValue.toString() );
    }

    public String getAdjusterState()
    {
        String           sValue = dbnbDB.getFieldData( "AdjusterState" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setAdjusterZip(String newValue)
    {
                dbnbDB.setFieldData( "AdjusterZip", newValue.toString() );
    }

    public String getAdjusterZip()
    {
        String           sValue = dbnbDB.getFieldData( "AdjusterZip" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setReferringDoctorReferenceID(String newValue)
    {
                dbnbDB.setFieldData( "ReferringDoctorReferenceID", newValue.toString() );
    }

    public String getReferringDoctorReferenceID()
    {
        String           sValue = dbnbDB.getFieldData( "ReferringDoctorReferenceID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setReferringDoctorNPI(String newValue)
    {
                dbnbDB.setFieldData( "ReferringDoctorNPI", newValue.toString() );
    }

    public String getReferringDoctorNPI()
    {
        String           sValue = dbnbDB.getFieldData( "ReferringDoctorNPI" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setReferringDoctorMedicalLicense(String newValue)
    {
                dbnbDB.setFieldData( "ReferringDoctorMedicalLicense", newValue.toString() );
    }

    public String getReferringDoctorMedicalLicense()
    {
        String           sValue = dbnbDB.getFieldData( "ReferringDoctorMedicalLicense" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setReferringDoctorFirstName(String newValue)
    {
                dbnbDB.setFieldData( "ReferringDoctorFirstName", newValue.toString() );
    }

    public String getReferringDoctorFirstName()
    {
        String           sValue = dbnbDB.getFieldData( "ReferringDoctorFirstName" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setReferringDoctorLastName(String newValue)
    {
                dbnbDB.setFieldData( "ReferringDoctorLastName", newValue.toString() );
    }

    public String getReferringDoctorLastName()
    {
        String           sValue = dbnbDB.getFieldData( "ReferringDoctorLastName" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setReferringDoctorPhoneNumber(String newValue)
    {
                dbnbDB.setFieldData( "ReferringDoctorPhoneNumber", newValue.toString() );
    }

    public String getReferringDoctorPhoneNumber()
    {
        String           sValue = dbnbDB.getFieldData( "ReferringDoctorPhoneNumber" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setReferringDoctorFaxNumber(String newValue)
    {
                dbnbDB.setFieldData( "ReferringDoctorFaxNumber", newValue.toString() );
    }

    public String getReferringDoctorFaxNumber()
    {
        String           sValue = dbnbDB.getFieldData( "ReferringDoctorFaxNumber" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setReferringDoctorEmail(String newValue)
    {
                dbnbDB.setFieldData( "ReferringDoctorEmail", newValue.toString() );
    }

    public String getReferringDoctorEmail()
    {
        String           sValue = dbnbDB.getFieldData( "ReferringDoctorEmail" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setReferringDoctorCompany(String newValue)
    {
                dbnbDB.setFieldData( "ReferringDoctorCompany", newValue.toString() );
    }

    public String getReferringDoctorCompany()
    {
        String           sValue = dbnbDB.getFieldData( "ReferringDoctorCompany" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setReferringDoctorAddress1(String newValue)
    {
                dbnbDB.setFieldData( "ReferringDoctorAddress1", newValue.toString() );
    }

    public String getReferringDoctorAddress1()
    {
        String           sValue = dbnbDB.getFieldData( "ReferringDoctorAddress1" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setReferringDoctorAddress2(String newValue)
    {
                dbnbDB.setFieldData( "ReferringDoctorAddress2", newValue.toString() );
    }

    public String getReferringDoctorAddress2()
    {
        String           sValue = dbnbDB.getFieldData( "ReferringDoctorAddress2" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setReferringDoctorCity(String newValue)
    {
                dbnbDB.setFieldData( "ReferringDoctorCity", newValue.toString() );
    }

    public String getReferringDoctorCity()
    {
        String           sValue = dbnbDB.getFieldData( "ReferringDoctorCity" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setReferringDoctorState(String newValue)
    {
                dbnbDB.setFieldData( "ReferringDoctorState", newValue.toString() );
    }

    public String getReferringDoctorState()
    {
        String           sValue = dbnbDB.getFieldData( "ReferringDoctorState" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setReferringDoctorZip(String newValue)
    {
                dbnbDB.setFieldData( "ReferringDoctorZip", newValue.toString() );
    }

    public String getReferringDoctorZip()
    {
        String           sValue = dbnbDB.getFieldData( "ReferringDoctorZip" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setNotes(String newValue)
    {
                dbnbDB.setFieldData( "Notes", newValue.toString() );
    }

    public String getNotes()
    {
        String           sValue = dbnbDB.getFieldData( "Notes" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

}    // End of bltNIM3_EligibilityReference class definition
