package com.winstaff;

import java.io.*;
import java.sql.*;
import java.text.ParseException;
import java.util.*;

public class dashboard_summary {
	
	public static String writeWeightedAvg(String modality, String desc) throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException{
		Class.forName(ConfigurationInformation.sJDBCDriverName).newInstance();
		Connection con = DriverManager.getConnection(
			ConfigurationInformation.sURL,
			ConfigurationInformation.sUser,
			ConfigurationInformation.sPassword
		);
	
		String query = "select price_mod_"+modality+", count_mod_"+modality+" from \"nim_network\" where price_mod_"+modality+" !=0 and count_mod_"+modality+"!=0";
		Statement sql = con.createStatement();
		ResultSet results = sql.executeQuery(query);
		int totalPrice=0; int totalCount=0;
		while (results!=null&&results.next())
		{
			totalPrice += results.getInt("price_mod_"+modality+"") * results.getInt("count_mod_"+modality+"");
			totalCount += results.getInt("count_mod_"+modality+"");
		}
		results.close();
		
		return "Weighted Avg Rate ("+desc+"): "+totalPrice/totalCount;
		
	}
	public static String techOnly(String modality,String tech, String state) throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException{
		Class.forName(ConfigurationInformation.sJDBCDriverName).newInstance();
		Connection con = DriverManager.getConnection(
			ConfigurationInformation.sURL,
			ConfigurationInformation.sUser,
			ConfigurationInformation.sPassword
		);
	
		String query = "select sum(price_mod_"+modality+"_"+tech+")/count(price_mod_"+modality+"_"+tech+") as count from nim_network where price_mod_"+modality+"_"+tech+" !=0 and \"sOfficeState\"='"+state+"' ORDER BY 1";
		Statement sql = con.createStatement();
		ResultSet results = sql.executeQuery(query);
		int totalPrice=0;
		while (results!=null&&results.next())
		{
			totalPrice = results.getInt("count");
		}
		results.close();
		
		return Integer.toString(totalPrice);
		
	}
	
	public static void main(String[] args) throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException, ParseException, IOException {
		
		String outputLocation = args[0];
		
		Class.forName(ConfigurationInformation.sJDBCDriverName).newInstance();
		Connection con = DriverManager.getConnection(
			ConfigurationInformation.sURL,
			ConfigurationInformation.sUser,
			ConfigurationInformation.sPassword
		);
		Statement sql = con.createStatement();
		int totalContractedCenters = 0;
		int totalOTA = 0;
		
		BufferedWriter out = new BufferedWriter(new FileWriter(outputLocation+"outfile_loading.html"));
		out.write("<span class=\"title\">Dashboard Summary</span><br>");
		out.write("\n");
		
		//Get total contracted centers
		String query = "select count(DISTINCT practiceid) from nim_network where contractingstatusid in (2,8,9)";
		ResultSet results = sql.executeQuery(query);
		while (results!=null&&results.next())
		{
			totalContractedCenters = results.getInt("count");
		}
		results.close();
		
		out.write("Total Contracted Centers: <a href=\"AdminPracticeAll_query.jsp?orderBy=PracticeName,%20ownername2,%20tPracticeMaster.OfficeStateID&maxResults=20000&startID=0&SpecialServicesID=0&ActivityOnly=0&dueBy=&RelationshipTypeID=2&ownername2=&PracticeID=&ContractingStatusID=-40&PracticeName=&OfficeAddress1=&OfficeCity=&OfficeStateID=0&OfficeZip=&ZCCounty=&ModalityTypeID=-1\">"+totalContractedCenters+"</a><br>");
		out.write("\n");
		
		//Get total OTA centers
		query = "select count(DISTINCT practiceid) from nim_network where contractingstatusid in (1)";
		results = sql.executeQuery(query);
		while (results!=null&&results.next())
		{
			totalOTA = results.getInt("count");
		}
		results.close();
		
		out.write("Total OTA Centers: <a href=\"AdminPracticeAll_query.jsp?orderBy=PracticeName,%20ownername2,%20tPracticeMaster.OfficeStateID&maxResults=20000&startID=0&SpecialServicesID=0&ActivityOnly=0&dueBy=&RelationshipTypeID=2&ownername2=&PracticeID=&ContractingStatusID=1&PracticeName=&OfficeAddress1=&OfficeCity=&OfficeStateID=0&OfficeZip=&ZCCounty=&ModalityTypeID=-1\">"+totalOTA+"</a><br>");
		out.write("\n");
		
		out.write(writeWeightedAvg("mri_wo", "MRI WO"));
		out.write("<br>\n");
		out.write(writeWeightedAvg("mri_w", "MRI W"));
		out.write("<br>\n");
		out.write(writeWeightedAvg("mri_wwo", "MRI WWO"));
		out.write("<br>\n");
		out.write(writeWeightedAvg("ct_wo", "CT WO"));
		out.write("<br>\n");
		out.write(writeWeightedAvg("ct_w", "CT W"));
		out.write("<br>\n");
		out.write(writeWeightedAvg("ct_wwo", "CT WWO"));
		out.write("<br>\n");
		
		String[] state = {"CA","FL","GA"};
		out.write("<br><span class=\"title\">Total Tech-only:</span><br>");
		for (int x = 0;x<3;x++){
			out.write("<table border=1 style=\"padding:2px\"><tr>" +
					"<td><strong>"+state[x]+"</strong></td><td>MRI wo</td><td>MRI w</td><td>MRI wwo</td><td>CT wo</td><td>CT w</td><td>CT wwo</td>" +
					"<tr><td>TC: </td><td>"+techOnly("mri_wo","tc",state[x])+"</td><td>"+techOnly("mri_w","tc",state[x])+"</td><td>"+techOnly("mri_wwo","tc",state[x])+"</td><td>"+techOnly("ct_wo","tc",state[x])+"</td><td>"+techOnly("ct_w","tc",state[x])+"</td><td>"+techOnly("ct_wwo","tc",state[x])+"</td></tr>" +
					"<tR><td>26: </td><td>"+techOnly("mri_wo","26",state[x])+"</td><td>"+techOnly("mri_w","26",state[x])+"</td><td>"+techOnly("mri_wwo","26",state[x])+"</td><td>"+techOnly("ct_wo","26",state[x])+"</td><td>"+techOnly("ct_w","26",state[x])+"</td><td>"+techOnly("ct_wwo","26",state[x])+"</td></tr>" +
					"</tr></table><br>");
		}
		
		out.write("<br>Updated: "+new java.util.Date());
		
		
		
		
		
		
		out.close();
		
	}
}
