package com.winstaff;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.Vector;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;

public class Unpaid_Underpaid_By_Adjuster{
		private static String TOMCAT = "tomcat7";
		private static String user = "root";
		private static String password = "vc8trBfYdJ";
		private static String host = "173.204.89.28";
		private static String localDir = "/var/lib/" + TOMCAT + "/webapps/ROOT/WEB-INF/classes/UNPAID_UNDERPAID_BY_ADJ/";
	public static void main(String[] args) throws IOException, SQLException{
		String query = "SELECT * FROM \"unpaid_or_underpaid_cliams_by_adjuster\"";
		searchDB2 connection = new searchDB2();
		java.sql.ResultSet rs = connection.executeStatement(query);
		getunpaid_Underpaid_List(rs);
		pullfile();
		connection.closeAll();
	}
	public static void getunpaid_Underpaid_List(ResultSet rs) throws SQLException{
		try {
				PrintWriter writer = new PrintWriter(new FileWriter("UNPAID_UNDERPAID_BY_ADJ/paid_unpaid.csv"));
				ResultSetMetaData meta = rs.getMetaData();
				int numOfColumns = meta.getColumnCount();
				//String myHeaders = "\"" + meta.getColumnName(1) + "\"";
				String myHeaders = meta.getColumnName(1);
				for(int i = 2; i < numOfColumns + 1; i++){
					myHeaders += "," +  meta.getColumnName(i);
				}
				writer.println(myHeaders);
				while(rs.next()){
					String row = rs.getString(1);
					for(int i = 2; i < numOfColumns + 1; i++){
						row += "," + rs.getString(i) + "";
					}
					writer.println(row);
				}
				writer.close();
			}catch (IOException e) {
				e.printStackTrace();
			}
	}
	
	public static void pullfile() throws SQLException {
		java.text.SimpleDateFormat displayDateSDF1Full = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1Full);
		System.out.println("Running csv file check " + displayDateSDF1Full.format(PLCUtils.getNowDate(true)));

		int listSize = 0;
		JSch jsch = new JSch();
		Session session = null;
		try {
			session = jsch.getSession(user, host, 22);
			session.setConfig("StrictHostKeyChecking", "no");
			session.setPassword(password);
			session.connect();

			Channel channel = session.openChannel("sftp");
			channel.connect();
			ChannelSftp sftpChannel = (ChannelSftp) channel;
			sftpChannel.cd(localDir);
			Vector<ChannelSftp.LsEntry> list = sftpChannel.ls("*.csv");
			listSize = list.size();
			String emailBody = "CSV file has been updated";

			if (list.size() > 0) {
				for (ChannelSftp.LsEntry entry : list) {
					System.out.println("Getting: " + entry.getFilename());
					emailBody += entry.getFilename() + "\n";
					sftpChannel.get("*.csv", localDir);
					System.out.println("Deleting: " + entry.getFilename());
					sftpChannel.rm(entry.getFilename());
				}

				System.out.println("Sending Email Alerts");

				String emailFrom = "support@nextimagemedical.com";
				String emailSubject = "csv server file dump";

				email("giovanni.hernandez@nextimagemedical.com", emailFrom, emailSubject, emailBody);

			}

			sftpChannel.exit();
			session.disconnect();

		} catch (JSchException e) {
			e.printStackTrace(); // To change body of catch statement use File |
									// Settings | File Templates.
		} catch (SftpException e) {
			e.printStackTrace();
		}
		System.out.println("Done pulling csv from NIM server");
		// System.exit(listSize);
	}
	private static void email(String emailTo, String emailFrom, String theSubject, String theBody) throws SQLException {
		bltEmailTransaction email = new bltEmailTransaction();
		email.setEmailTo(emailTo);
		email.setEmailFrom(emailFrom);
		email.setEmailSubject(theSubject);
		email.setEmailBody(theBody);
		email.setEmailBodyType(emailType_V3.PLAIN_TYPE);
		email.setTransactionDate(new java.util.Date());
		email.commitData();
	}
}