
package com.winstaff;
/*
 * bltBillingTransaction_List.java
 *
 * Created: Wed Apr 27 19:02:30 PDT 2011
 */



import java.sql.*;




/**
 *
 * @author  Scott Ellis via listClassGenCode
 * @version 2.5
 */
public class bltBillingTransaction_List extends db_BaseRelationTable
{
    /** Default constructor
    */    
    public bltBillingTransaction_List ( Integer iBillingID )
    {
        super( "tBillingTransaction", "BillingTransactionID", "BillingTransactionID", "BillingID", iBillingID );
    }   // End of bltBillingTransaction_List()
    

    public void setField(String fieldN, Object fieldV)
    {DebugLogger.println("setField Method not implemented yet in classbltBillingTransaction_List!");}

    public String getFieldType(String fieldN)
    {return null;}

    public bltBillingTransaction_List ( Integer iBillingID, String extraWhere, String OrderBy )
    {
        super( "tBillingTransaction", "BillingTransactionID", "BillingTransactionID", "BillingID", iBillingID, extraWhere,OrderBy );
    }   // End of bltBillingTransaction_List()
     /** Method pull information from the result set and load into class attributes.
    * @param sFieldList Comma delimited list of fields for which data has been retrieved.
    * @param rs Result set from which to pull the data from.
    * @throws SQLException Passes on SQLException
    */      
    protected void transferData( ResultSet rs ) throws SQLException
    {
        while ( rs.next()  )
        {
            Integer         iValue              = new Integer( rs.getString( sLinkFieldName ) );
            Integer         iLinkRecordID       = new Integer( rs.getString( sUniqueIDFieldName ) );
            Object          oReferenceObject;
            ListElement     leNewElement;
            
            oReferenceObject    = new bltBillingTransaction( iValue );
            
            leNewElement        = new ListElement( iLinkRecordID, oReferenceObject );
            
            vList.add( leNewElement );

        }
    }   // End of transferData()
}   // End of bltBillingTransaction_List class

