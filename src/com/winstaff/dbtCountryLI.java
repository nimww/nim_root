

package com.winstaff;

import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public class dbtCountryLI extends Object implements InttCountryLI
{

        db_NewBase    dbnbDB;

    public dbtCountryLI()
    {
        dbnbDB = new db_NewBase( "tCountryLI", "CountryID" );

    }    // End of default constructor

    public dbtCountryLI( Integer iNewID )
    {
        dbnbDB = new db_NewBase( "tCountryLI", "CountryID" );
        setUniqueID( iNewID );
    }    // End of Constructor knowing an ID

    public void commitData() throws SQLException
    {
            //no longer trapping/ignoring errors 2011-03-17
            dbnbDB.commitData();
    }    // End of commitData()

    public void commitDataForced() throws SQLException
    {
        dbnbDB.commitDataForced();
    }    // End of commitData()

    public Integer getUniqueID()
    {
        return dbnbDB.getUniqueID();
    }    // End of getUniqueID()

    public void setUniqueID( Integer iNewID )
    {
        dbnbDB.setUniqueID( iNewID );
    }    // End of setUniqueID()

    public void setModifyComments( String sNew )
    {
        dbnbDB.setModifyComments( sNew );
    }    // End of setUniqueID()


    public void setCountryID(Integer newValue)
    {
                dbnbDB.setFieldData( "CountryID", newValue.toString() );
    }

    public Integer getCountryID()
    {
        String           sValue = dbnbDB.getFieldData( "CountryID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setField(String fieldN, Object fieldV)
    {DebugLogger.println("setField Method not implemented yet in class!");}

    public String getFieldType(String fieldN)
    {return null;}


    public void setCountryCode(String newValue)
    {
                dbnbDB.setFieldData( "CountryCode", newValue.toString() );
    }

    public String getCountryCode()
    {
        String           sValue = dbnbDB.getFieldData( "CountryCode" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setCountryLong(String newValue)
    {
                dbnbDB.setFieldData( "CountryLong", newValue.toString() );
    }

    public String getCountryLong()
    {
        String           sValue = dbnbDB.getFieldData( "CountryLong" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

}    // End of bltCountryLI class definition
