

package com.winstaff;

import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public interface InttNIM3_ServiceBillingTransaction extends dbTableInterface
{

    public void setServiceBillingTransactionID(Integer newValue);
    public Integer getServiceBillingTransactionID();
    public void setUniqueCreateDate(Date newValue);
    public Date getUniqueCreateDate();
    public void setUniqueModifyDate(Date newValue);
    public Date getUniqueModifyDate();
    public void setUniqueModifyComments(String newValue);
    public String getUniqueModifyComments();
    public void setServiceID(Integer newValue);
    public Integer getServiceID();
    public void setServiceBillingTransactionTypeID(Integer newValue);
    public Integer getServiceBillingTransactionTypeID();
    public void setExportedDate(Date newValue);
    public Date getExportedDate();
    public void setExportComments(String newValue);
    public String getExportComments();
    public void setBilledDate(Date newValue);
    public Date getBilledDate();
    public void setBillComments(String newValue);
    public String getBillComments();
    public void setTransactionTitle(String newValue);
    public String getTransactionTitle();
    public void setTransactionNote(String newValue);
    public String getTransactionNote();
    public void setTransactionAmount(Double newValue);
    public Double getTransactionAmount();
    public void setReferenceNumber(String newValue);
    public String getReferenceNumber();
    public void setReferenceDate(Date newValue);
    public Date getReferenceDate();
    public void setReferenceNotes(String newValue);
    public String getReferenceNotes();
    public void setDetail_Item_Qty(Integer newValue);
    public Integer getDetail_Item_Qty();
    public void setGenerated_UserID(Integer newValue);
    public Integer getGenerated_UserID();
    public void setComments(String newValue);
    public String getComments();
}    // End of bltNIM3_ServiceBillingTransaction class definition
