package com.winstaff;
/*
 * bltGroupSecurity_List.java
 *
 * Created: Wed Apr 02 15:09:17 PST 2003
 */



import java.sql.*;
import com.winstaff.dbtGroupSecurity;
import com.winstaff.ListElement;


/**
 *
 * @author  Scott Ellis via listClassGenCode
 * @version 2.5
 */
public class bltGroupSecurity_List extends db_BaseRelationTable
{
    /** Default constructor
    */    
    public bltGroupSecurity_List ( Integer iSecurityGroupID )
    {
        super( "tGroupSecurity", "GroupSecurityID", "GroupSecurityID", "SecurityGroupID", iSecurityGroupID );
    }   // End of bltGroupSecurity_List()
    

    public void setField(String fieldN, Object fieldV)
    {DebugLogger.println("setField Method not implemented yet in classbltGroupSecurity_List!");}

    public String getFieldType(String fieldN)
    {return null;}

    public bltGroupSecurity_List ( Integer iSecurityGroupID, String extraWhere, String OrderBy )
    {
        super( "tGroupSecurity", "GroupSecurityID", "GroupSecurityID", "SecurityGroupID", iSecurityGroupID, extraWhere,OrderBy );
    }   // End of bltGroupSecurity_List()
     /** Method pull information from the result set and load into class attributes.
    * @param sFieldList Comma delimited list of fields for which data has been retrieved.
    * @param rs Result set from which to pull the data from.
    * @throws SQLException Passes on SQLException
    */      
    protected void transferData( ResultSet rs ) throws SQLException
    {
        while ( rs.next()  )
        {
            Integer         iValue              = new Integer( rs.getString( sLinkFieldName ) );
            Integer         iLinkRecordID       = new Integer( rs.getString( sUniqueIDFieldName ) );
            Object          oReferenceObject;
            ListElement     leNewElement;
            
            oReferenceObject    = new bltGroupSecurity( iValue );
            
            leNewElement        = new ListElement( iLinkRecordID, oReferenceObject );
            
            vList.add( leNewElement );

        }
    }   // End of transferData()
}   // End of bltGroupSecurity_List class

