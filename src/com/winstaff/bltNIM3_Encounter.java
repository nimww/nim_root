

package com.winstaff;

import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;
import java.util.Enumeration;

public class bltNIM3_Encounter extends Object implements InttNIM3_Encounter
{

    dbtNIM3_Encounter    dbDB;

    public bltNIM3_Encounter()
    {
        dbDB = new dbtNIM3_Encounter();
        dbDB.setUniqueCreateDate(new java.util.Date());
        makeRequiredHash();
        makeExpiredHash();
        makeEnglishHash();

    }    // End of default constructor

    public bltNIM3_Encounter( Integer iNewID )
    {        dbDB = new dbtNIM3_Encounter( iNewID );
        makeRequiredHash();
        makeExpiredHash();
        makeEnglishHash();
        iSecurityCheck = new Integer(2);
    }    // End of Constructor knowing an ID


    public bltNIM3_Encounter( Integer iNewID, Integer iGroupSecurityID )
    {
        this.GroupSecurityInit(iGroupSecurityID);
        dbDB = new dbtNIM3_Encounter( iNewID );
        makeExpiredHash();
        makeEnglishHash();
        makeRequiredHash(iGroupSecurityID);
    }    // End of Constructor knowing an ID

    public bltNIM3_Encounter( Integer iGroupSecurityID, boolean isTrue )
    {
        this.GroupSecurityInit(iGroupSecurityID);
        dbDB = new dbtNIM3_Encounter(  );
        makeExpiredHash();
        makeEnglishHash();
        makeRequiredHash(iGroupSecurityID);
    }    // End of Security unknown ID

    public void GroupSecurityInit( Integer iGroupSecurityID )
    {
        bltSecurityGroupMaster mySGM = new bltSecurityGroupMaster(iGroupSecurityID);
        this.UserSecurityID = iGroupSecurityID;
        if (mySGM.getRunFieldSecurityID().intValue()==1)
        {
            this.runFieldSecurity = true;
            makeRequiredHash(iGroupSecurityID);
        }
        iSecurityCheck = SecurityCheck.CheckItem("nim1", iGroupSecurityID);
    }  

    public EventReferenceType getEventReference()
    {
        return EventUtils.getEventReference("tNIM3_Encounter", this.getUniqueID());
    }  

    public String convertString2LogString(String normalString) 
    {
        String myVal = "";
        for (int i=0;i<normalString.length();i++)
        {
            if ( (normalString.charAt(i)!='\r')&&(normalString.charAt(i)!='\n')&&(normalString.charAt(i)!='\t') )
            {
                myVal+=normalString.charAt(i);
            }
            else
            {
                myVal+="<NLT>";
            }
        }
        return myVal;
    }
    public void runAudit()
    {
        String AuditString = new java.util.Date()+" \t " ;  
        AuditString += " tNIM3_Encounter "; 
        AuditString += " EncounterID ="+this.getUniqueID(); 
        AuditString += " ReferralID ="+this.getReferralID(); 
        for (int i=0;i<this.AuditVector.size();i++)
        {
            AuditString+="\t"+convertString2LogString((String)this.AuditVector.elementAt(i));
        }
        AuditLogger.printLine(AuditString);
    }
    public void commitData() throws SQLException
    {
        runAudit();
        dbDB.setModifyComments(this.getUniqueModifyComments());
        dbDB.setUniqueModifyDate(new java.util.Date());
        dbDB.commitData();
        DataControlUtils.dataChanged("tNIM3_Encounter",this.getReferralID() ,this.AuditVector.size(), "ReferralID");
        this.AuditVector.removeAllElements();
    }    // End of commitData()

    public void commitDataForced() throws SQLException
    {
        dbDB.commitDataForced();
    }    // End of commitData()

    public Integer getUniqueID()
    {
        return dbDB.getUniqueID();
    }    // End of getUniqueID()

    public void setUniqueID( Integer iNewID )
    {
        dbDB.setUniqueID( iNewID );
    }    // End of setUniqueID()


    public void setEncounterID(Integer newValue)
    {
        dbDB.setEncounterID(newValue);
    }

    public Integer getEncounterID()
    {
        return dbDB.getEncounterID();
    }

    java.util.Vector AuditVector = new java.util.Vector();

    public void setUniqueCreateDate(Date newValue)
    {
        this.setUniqueCreateDate(newValue,this.UserSecurityID);


    }
    public Date getUniqueCreateDate()
    {
        return this.getUniqueCreateDate(this.UserSecurityID);
    }

    public void setUniqueCreateDate(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='UniqueCreateDate' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[UniqueCreateDate]=["+newValue+"]");
                   dbDB.setUniqueCreateDate(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[UniqueCreateDate]=["+newValue+"]");
           dbDB.setUniqueCreateDate(newValue);
         }
    }
    public Date getUniqueCreateDate(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='UniqueCreateDate' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getUniqueCreateDate();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getUniqueCreateDate();
         }
        return myVal;
    }

    public void setUniqueModifyDate(Date newValue)
    {
        this.setUniqueModifyDate(newValue,this.UserSecurityID);


    }
    public Date getUniqueModifyDate()
    {
        return this.getUniqueModifyDate(this.UserSecurityID);
    }

    public void setUniqueModifyDate(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='UniqueModifyDate' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[UniqueModifyDate]=["+newValue+"]");
                   dbDB.setUniqueModifyDate(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[UniqueModifyDate]=["+newValue+"]");
           dbDB.setUniqueModifyDate(newValue);
         }
    }
    public Date getUniqueModifyDate(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='UniqueModifyDate' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getUniqueModifyDate();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getUniqueModifyDate();
         }
        return myVal;
    }

    public void setUniqueModifyComments(String newValue)
    {
        this.setUniqueModifyComments(newValue,this.UserSecurityID);


    }
    public String getUniqueModifyComments()
    {
        return this.getUniqueModifyComments(this.UserSecurityID);
    }

    public void setUniqueModifyComments(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='UniqueModifyComments' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[UniqueModifyComments]=["+newValue+"]");
                   dbDB.setUniqueModifyComments(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[UniqueModifyComments]=["+newValue+"]");
           dbDB.setUniqueModifyComments(newValue);
         }
    }
    public String getUniqueModifyComments(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='UniqueModifyComments' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getUniqueModifyComments();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getUniqueModifyComments();
         }
        return myVal;
    }

    public void setReferralID(Integer newValue)
    {
        this.setReferralID(newValue,this.UserSecurityID);


    }
    public Integer getReferralID()
    {
        return this.getReferralID(this.UserSecurityID);
    }

    public void setReferralID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ReferralID' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[ReferralID]=["+newValue+"]");
                   dbDB.setReferralID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[ReferralID]=["+newValue+"]");
           dbDB.setReferralID(newValue);
         }
    }
    public Integer getReferralID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ReferralID' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getReferralID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getReferralID();
         }
        return myVal;
    }

    public void setEncounterTypeID(Integer newValue)
    {
        this.setEncounterTypeID(newValue,this.UserSecurityID);


    }
    public Integer getEncounterTypeID()
    {
        return this.getEncounterTypeID(this.UserSecurityID);
    }

    public void setEncounterTypeID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='EncounterTypeID' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[EncounterTypeID]=["+newValue+"]");
                   dbDB.setEncounterTypeID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[EncounterTypeID]=["+newValue+"]");
           dbDB.setEncounterTypeID(newValue);
         }
    }
    public Integer getEncounterTypeID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='EncounterTypeID' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getEncounterTypeID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getEncounterTypeID();
         }
        return myVal;
    }

    public void setScanPass(String newValue)
    {
        this.setScanPass(newValue,this.UserSecurityID);


    }
    public String getScanPass()
    {
        return this.getScanPass(this.UserSecurityID);
    }

    public void setScanPass(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ScanPass' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[ScanPass]=["+newValue+"]");
                   dbDB.setScanPass(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[ScanPass]=["+newValue+"]");
           dbDB.setScanPass(newValue);
         }
    }
    public String getScanPass(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ScanPass' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getScanPass();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getScanPass();
         }
        return myVal;
    }

    public void setAppointmentID(Integer newValue)
    {
        this.setAppointmentID(newValue,this.UserSecurityID);


    }
    public Integer getAppointmentID()
    {
        return this.getAppointmentID(this.UserSecurityID);
    }

    public void setAppointmentID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AppointmentID' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[AppointmentID]=["+newValue+"]");
                   dbDB.setAppointmentID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[AppointmentID]=["+newValue+"]");
           dbDB.setAppointmentID(newValue);
         }
    }
    public Integer getAppointmentID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AppointmentID' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getAppointmentID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getAppointmentID();
         }
        return myVal;
    }

    public void setAttendingPhysicianID(Integer newValue)
    {
        this.setAttendingPhysicianID(newValue,this.UserSecurityID);


    }
    public Integer getAttendingPhysicianID()
    {
        return this.getAttendingPhysicianID(this.UserSecurityID);
    }

    public void setAttendingPhysicianID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AttendingPhysicianID' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[AttendingPhysicianID]=["+newValue+"]");
                   dbDB.setAttendingPhysicianID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[AttendingPhysicianID]=["+newValue+"]");
           dbDB.setAttendingPhysicianID(newValue);
         }
    }
    public Integer getAttendingPhysicianID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AttendingPhysicianID' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getAttendingPhysicianID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getAttendingPhysicianID();
         }
        return myVal;
    }

    public void setReferringPhysicianID(Integer newValue)
    {
        this.setReferringPhysicianID(newValue,this.UserSecurityID);


    }
    public Integer getReferringPhysicianID()
    {
        return this.getReferringPhysicianID(this.UserSecurityID);
    }

    public void setReferringPhysicianID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ReferringPhysicianID' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[ReferringPhysicianID]=["+newValue+"]");
                   dbDB.setReferringPhysicianID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[ReferringPhysicianID]=["+newValue+"]");
           dbDB.setReferringPhysicianID(newValue);
         }
    }
    public Integer getReferringPhysicianID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ReferringPhysicianID' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getReferringPhysicianID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getReferringPhysicianID();
         }
        return myVal;
    }

    public void setDateOfService(Date newValue)
    {
        this.setDateOfService(newValue,this.UserSecurityID);


    }
    public Date getDateOfService()
    {
        return this.getDateOfService(this.UserSecurityID);
    }

    public void setDateOfService(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='DateOfService' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[DateOfService]=["+newValue+"]");
                   dbDB.setDateOfService(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[DateOfService]=["+newValue+"]");
           dbDB.setDateOfService(newValue);
         }
    }
    public Date getDateOfService(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='DateOfService' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getDateOfService();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getDateOfService();
         }
        return myVal;
    }

    public void setNextActionDate(Date newValue)
    {
        this.setNextActionDate(newValue,this.UserSecurityID);


    }
    public Date getNextActionDate()
    {
        return this.getNextActionDate(this.UserSecurityID);
    }

    public void setNextActionDate(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='NextActionDate' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[NextActionDate]=["+newValue+"]");
                   dbDB.setNextActionDate(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[NextActionDate]=["+newValue+"]");
           dbDB.setNextActionDate(newValue);
         }
    }
    public Date getNextActionDate(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='NextActionDate' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getNextActionDate();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getNextActionDate();
         }
        return myVal;
    }

    public void setNextActionTaskID(Integer newValue)
    {
        this.setNextActionTaskID(newValue,this.UserSecurityID);


    }
    public Integer getNextActionTaskID()
    {
        return this.getNextActionTaskID(this.UserSecurityID);
    }

    public void setNextActionTaskID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='NextActionTaskID' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[NextActionTaskID]=["+newValue+"]");
                   dbDB.setNextActionTaskID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[NextActionTaskID]=["+newValue+"]");
           dbDB.setNextActionTaskID(newValue);
         }
    }
    public Integer getNextActionTaskID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='NextActionTaskID' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getNextActionTaskID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getNextActionTaskID();
         }
        return myVal;
    }

    public void setSentToRefDr(Date newValue)
    {
        this.setSentToRefDr(newValue,this.UserSecurityID);


    }
    public Date getSentToRefDr()
    {
        return this.getSentToRefDr(this.UserSecurityID);
    }

    public void setSentToRefDr(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SentToRefDr' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[SentToRefDr]=["+newValue+"]");
                   dbDB.setSentToRefDr(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[SentToRefDr]=["+newValue+"]");
           dbDB.setSentToRefDr(newValue);
         }
    }
    public Date getSentToRefDr(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SentToRefDr' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getSentToRefDr();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getSentToRefDr();
         }
        return myVal;
    }

    public void setSentToRefDr_CRID(Integer newValue)
    {
        this.setSentToRefDr_CRID(newValue,this.UserSecurityID);


    }
    public Integer getSentToRefDr_CRID()
    {
        return this.getSentToRefDr_CRID(this.UserSecurityID);
    }

    public void setSentToRefDr_CRID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SentToRefDr_CRID' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[SentToRefDr_CRID]=["+newValue+"]");
                   dbDB.setSentToRefDr_CRID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[SentToRefDr_CRID]=["+newValue+"]");
           dbDB.setSentToRefDr_CRID(newValue);
         }
    }
    public Integer getSentToRefDr_CRID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SentToRefDr_CRID' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getSentToRefDr_CRID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getSentToRefDr_CRID();
         }
        return myVal;
    }

    public void setSentToAdj(Date newValue)
    {
        this.setSentToAdj(newValue,this.UserSecurityID);


    }
    public Date getSentToAdj()
    {
        return this.getSentToAdj(this.UserSecurityID);
    }

    public void setSentToAdj(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SentToAdj' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[SentToAdj]=["+newValue+"]");
                   dbDB.setSentToAdj(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[SentToAdj]=["+newValue+"]");
           dbDB.setSentToAdj(newValue);
         }
    }
    public Date getSentToAdj(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SentToAdj' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getSentToAdj();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getSentToAdj();
         }
        return myVal;
    }

    public void setSentToAdj_CRID(Integer newValue)
    {
        this.setSentToAdj_CRID(newValue,this.UserSecurityID);


    }
    public Integer getSentToAdj_CRID()
    {
        return this.getSentToAdj_CRID(this.UserSecurityID);
    }

    public void setSentToAdj_CRID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SentToAdj_CRID' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[SentToAdj_CRID]=["+newValue+"]");
                   dbDB.setSentToAdj_CRID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[SentToAdj_CRID]=["+newValue+"]");
           dbDB.setSentToAdj_CRID(newValue);
         }
    }
    public Integer getSentToAdj_CRID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SentToAdj_CRID' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getSentToAdj_CRID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getSentToAdj_CRID();
         }
        return myVal;
    }

    public void setSentToIC(Date newValue)
    {
        this.setSentToIC(newValue,this.UserSecurityID);


    }
    public Date getSentToIC()
    {
        return this.getSentToIC(this.UserSecurityID);
    }

    public void setSentToIC(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SentToIC' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[SentToIC]=["+newValue+"]");
                   dbDB.setSentToIC(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[SentToIC]=["+newValue+"]");
           dbDB.setSentToIC(newValue);
         }
    }
    public Date getSentToIC(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SentToIC' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getSentToIC();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getSentToIC();
         }
        return myVal;
    }

    public void setSentToIC_CRID(Integer newValue)
    {
        this.setSentToIC_CRID(newValue,this.UserSecurityID);


    }
    public Integer getSentToIC_CRID()
    {
        return this.getSentToIC_CRID(this.UserSecurityID);
    }

    public void setSentToIC_CRID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SentToIC_CRID' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[SentToIC_CRID]=["+newValue+"]");
                   dbDB.setSentToIC_CRID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[SentToIC_CRID]=["+newValue+"]");
           dbDB.setSentToIC_CRID(newValue);
         }
    }
    public Integer getSentToIC_CRID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SentToIC_CRID' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getSentToIC_CRID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getSentToIC_CRID();
         }
        return myVal;
    }

    public void setSentTo_Appt_Conf_Pat(Date newValue)
    {
        this.setSentTo_Appt_Conf_Pat(newValue,this.UserSecurityID);


    }
    public Date getSentTo_Appt_Conf_Pat()
    {
        return this.getSentTo_Appt_Conf_Pat(this.UserSecurityID);
    }

    public void setSentTo_Appt_Conf_Pat(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SentTo_Appt_Conf_Pat' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[SentTo_Appt_Conf_Pat]=["+newValue+"]");
                   dbDB.setSentTo_Appt_Conf_Pat(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[SentTo_Appt_Conf_Pat]=["+newValue+"]");
           dbDB.setSentTo_Appt_Conf_Pat(newValue);
         }
    }
    public Date getSentTo_Appt_Conf_Pat(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SentTo_Appt_Conf_Pat' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getSentTo_Appt_Conf_Pat();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getSentTo_Appt_Conf_Pat();
         }
        return myVal;
    }

    public void setSentTo_Appt_Conf_Pat_CRID(Integer newValue)
    {
        this.setSentTo_Appt_Conf_Pat_CRID(newValue,this.UserSecurityID);


    }
    public Integer getSentTo_Appt_Conf_Pat_CRID()
    {
        return this.getSentTo_Appt_Conf_Pat_CRID(this.UserSecurityID);
    }

    public void setSentTo_Appt_Conf_Pat_CRID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SentTo_Appt_Conf_Pat_CRID' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[SentTo_Appt_Conf_Pat_CRID]=["+newValue+"]");
                   dbDB.setSentTo_Appt_Conf_Pat_CRID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[SentTo_Appt_Conf_Pat_CRID]=["+newValue+"]");
           dbDB.setSentTo_Appt_Conf_Pat_CRID(newValue);
         }
    }
    public Integer getSentTo_Appt_Conf_Pat_CRID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SentTo_Appt_Conf_Pat_CRID' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getSentTo_Appt_Conf_Pat_CRID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getSentTo_Appt_Conf_Pat_CRID();
         }
        return myVal;
    }

    public void setSentTo_SP_Pat(Date newValue)
    {
        this.setSentTo_SP_Pat(newValue,this.UserSecurityID);


    }
    public Date getSentTo_SP_Pat()
    {
        return this.getSentTo_SP_Pat(this.UserSecurityID);
    }

    public void setSentTo_SP_Pat(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SentTo_SP_Pat' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[SentTo_SP_Pat]=["+newValue+"]");
                   dbDB.setSentTo_SP_Pat(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[SentTo_SP_Pat]=["+newValue+"]");
           dbDB.setSentTo_SP_Pat(newValue);
         }
    }
    public Date getSentTo_SP_Pat(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SentTo_SP_Pat' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getSentTo_SP_Pat();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getSentTo_SP_Pat();
         }
        return myVal;
    }

    public void setSentTo_SP_Pat_CRID(Integer newValue)
    {
        this.setSentTo_SP_Pat_CRID(newValue,this.UserSecurityID);


    }
    public Integer getSentTo_SP_Pat_CRID()
    {
        return this.getSentTo_SP_Pat_CRID(this.UserSecurityID);
    }

    public void setSentTo_SP_Pat_CRID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SentTo_SP_Pat_CRID' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[SentTo_SP_Pat_CRID]=["+newValue+"]");
                   dbDB.setSentTo_SP_Pat_CRID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[SentTo_SP_Pat_CRID]=["+newValue+"]");
           dbDB.setSentTo_SP_Pat_CRID(newValue);
         }
    }
    public Integer getSentTo_SP_Pat_CRID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SentTo_SP_Pat_CRID' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getSentTo_SP_Pat_CRID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getSentTo_SP_Pat_CRID();
         }
        return myVal;
    }

    public void setSentTo_SP_RefDr(Date newValue)
    {
        this.setSentTo_SP_RefDr(newValue,this.UserSecurityID);


    }
    public Date getSentTo_SP_RefDr()
    {
        return this.getSentTo_SP_RefDr(this.UserSecurityID);
    }

    public void setSentTo_SP_RefDr(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SentTo_SP_RefDr' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[SentTo_SP_RefDr]=["+newValue+"]");
                   dbDB.setSentTo_SP_RefDr(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[SentTo_SP_RefDr]=["+newValue+"]");
           dbDB.setSentTo_SP_RefDr(newValue);
         }
    }
    public Date getSentTo_SP_RefDr(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SentTo_SP_RefDr' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getSentTo_SP_RefDr();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getSentTo_SP_RefDr();
         }
        return myVal;
    }

    public void setSentTo_SP_RefDr_CRID(Integer newValue)
    {
        this.setSentTo_SP_RefDr_CRID(newValue,this.UserSecurityID);


    }
    public Integer getSentTo_SP_RefDr_CRID()
    {
        return this.getSentTo_SP_RefDr_CRID(this.UserSecurityID);
    }

    public void setSentTo_SP_RefDr_CRID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SentTo_SP_RefDr_CRID' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[SentTo_SP_RefDr_CRID]=["+newValue+"]");
                   dbDB.setSentTo_SP_RefDr_CRID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[SentTo_SP_RefDr_CRID]=["+newValue+"]");
           dbDB.setSentTo_SP_RefDr_CRID(newValue);
         }
    }
    public Integer getSentTo_SP_RefDr_CRID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SentTo_SP_RefDr_CRID' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getSentTo_SP_RefDr_CRID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getSentTo_SP_RefDr_CRID();
         }
        return myVal;
    }

    public void setSentTo_SP_Adj(Date newValue)
    {
        this.setSentTo_SP_Adj(newValue,this.UserSecurityID);


    }
    public Date getSentTo_SP_Adj()
    {
        return this.getSentTo_SP_Adj(this.UserSecurityID);
    }

    public void setSentTo_SP_Adj(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SentTo_SP_Adj' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[SentTo_SP_Adj]=["+newValue+"]");
                   dbDB.setSentTo_SP_Adj(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[SentTo_SP_Adj]=["+newValue+"]");
           dbDB.setSentTo_SP_Adj(newValue);
         }
    }
    public Date getSentTo_SP_Adj(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SentTo_SP_Adj' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getSentTo_SP_Adj();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getSentTo_SP_Adj();
         }
        return myVal;
    }

    public void setSentTo_SP_Adj_CRID(Integer newValue)
    {
        this.setSentTo_SP_Adj_CRID(newValue,this.UserSecurityID);


    }
    public Integer getSentTo_SP_Adj_CRID()
    {
        return this.getSentTo_SP_Adj_CRID(this.UserSecurityID);
    }

    public void setSentTo_SP_Adj_CRID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SentTo_SP_Adj_CRID' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[SentTo_SP_Adj_CRID]=["+newValue+"]");
                   dbDB.setSentTo_SP_Adj_CRID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[SentTo_SP_Adj_CRID]=["+newValue+"]");
           dbDB.setSentTo_SP_Adj_CRID(newValue);
         }
    }
    public Integer getSentTo_SP_Adj_CRID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SentTo_SP_Adj_CRID' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getSentTo_SP_Adj_CRID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getSentTo_SP_Adj_CRID();
         }
        return myVal;
    }

    public void setSentTo_SP_IC(Date newValue)
    {
        this.setSentTo_SP_IC(newValue,this.UserSecurityID);


    }
    public Date getSentTo_SP_IC()
    {
        return this.getSentTo_SP_IC(this.UserSecurityID);
    }

    public void setSentTo_SP_IC(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SentTo_SP_IC' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[SentTo_SP_IC]=["+newValue+"]");
                   dbDB.setSentTo_SP_IC(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[SentTo_SP_IC]=["+newValue+"]");
           dbDB.setSentTo_SP_IC(newValue);
         }
    }
    public Date getSentTo_SP_IC(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SentTo_SP_IC' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getSentTo_SP_IC();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getSentTo_SP_IC();
         }
        return myVal;
    }

    public void setSentTo_SP_IC_CRID(Integer newValue)
    {
        this.setSentTo_SP_IC_CRID(newValue,this.UserSecurityID);


    }
    public Integer getSentTo_SP_IC_CRID()
    {
        return this.getSentTo_SP_IC_CRID(this.UserSecurityID);
    }

    public void setSentTo_SP_IC_CRID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SentTo_SP_IC_CRID' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[SentTo_SP_IC_CRID]=["+newValue+"]");
                   dbDB.setSentTo_SP_IC_CRID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[SentTo_SP_IC_CRID]=["+newValue+"]");
           dbDB.setSentTo_SP_IC_CRID(newValue);
         }
    }
    public Integer getSentTo_SP_IC_CRID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SentTo_SP_IC_CRID' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getSentTo_SP_IC_CRID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getSentTo_SP_IC_CRID();
         }
        return myVal;
    }

    public void setSentTo_DataProc_RefDr(Date newValue)
    {
        this.setSentTo_DataProc_RefDr(newValue,this.UserSecurityID);


    }
    public Date getSentTo_DataProc_RefDr()
    {
        return this.getSentTo_DataProc_RefDr(this.UserSecurityID);
    }

    public void setSentTo_DataProc_RefDr(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SentTo_DataProc_RefDr' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[SentTo_DataProc_RefDr]=["+newValue+"]");
                   dbDB.setSentTo_DataProc_RefDr(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[SentTo_DataProc_RefDr]=["+newValue+"]");
           dbDB.setSentTo_DataProc_RefDr(newValue);
         }
    }
    public Date getSentTo_DataProc_RefDr(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SentTo_DataProc_RefDr' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getSentTo_DataProc_RefDr();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getSentTo_DataProc_RefDr();
         }
        return myVal;
    }

    public void setSentTo_DataProc_RefDr_CRID(Integer newValue)
    {
        this.setSentTo_DataProc_RefDr_CRID(newValue,this.UserSecurityID);


    }
    public Integer getSentTo_DataProc_RefDr_CRID()
    {
        return this.getSentTo_DataProc_RefDr_CRID(this.UserSecurityID);
    }

    public void setSentTo_DataProc_RefDr_CRID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SentTo_DataProc_RefDr_CRID' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[SentTo_DataProc_RefDr_CRID]=["+newValue+"]");
                   dbDB.setSentTo_DataProc_RefDr_CRID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[SentTo_DataProc_RefDr_CRID]=["+newValue+"]");
           dbDB.setSentTo_DataProc_RefDr_CRID(newValue);
         }
    }
    public Integer getSentTo_DataProc_RefDr_CRID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SentTo_DataProc_RefDr_CRID' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getSentTo_DataProc_RefDr_CRID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getSentTo_DataProc_RefDr_CRID();
         }
        return myVal;
    }

    public void setSentTo_DataProc_Adj(Date newValue)
    {
        this.setSentTo_DataProc_Adj(newValue,this.UserSecurityID);


    }
    public Date getSentTo_DataProc_Adj()
    {
        return this.getSentTo_DataProc_Adj(this.UserSecurityID);
    }

    public void setSentTo_DataProc_Adj(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SentTo_DataProc_Adj' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[SentTo_DataProc_Adj]=["+newValue+"]");
                   dbDB.setSentTo_DataProc_Adj(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[SentTo_DataProc_Adj]=["+newValue+"]");
           dbDB.setSentTo_DataProc_Adj(newValue);
         }
    }
    public Date getSentTo_DataProc_Adj(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SentTo_DataProc_Adj' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getSentTo_DataProc_Adj();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getSentTo_DataProc_Adj();
         }
        return myVal;
    }

    public void setSentTo_DataProc_Adj_CRID(Integer newValue)
    {
        this.setSentTo_DataProc_Adj_CRID(newValue,this.UserSecurityID);


    }
    public Integer getSentTo_DataProc_Adj_CRID()
    {
        return this.getSentTo_DataProc_Adj_CRID(this.UserSecurityID);
    }

    public void setSentTo_DataProc_Adj_CRID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SentTo_DataProc_Adj_CRID' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[SentTo_DataProc_Adj_CRID]=["+newValue+"]");
                   dbDB.setSentTo_DataProc_Adj_CRID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[SentTo_DataProc_Adj_CRID]=["+newValue+"]");
           dbDB.setSentTo_DataProc_Adj_CRID(newValue);
         }
    }
    public Integer getSentTo_DataProc_Adj_CRID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SentTo_DataProc_Adj_CRID' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getSentTo_DataProc_Adj_CRID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getSentTo_DataProc_Adj_CRID();
         }
        return myVal;
    }

    public void setSentTo_Bill_Pay(Date newValue)
    {
        this.setSentTo_Bill_Pay(newValue,this.UserSecurityID);


    }
    public Date getSentTo_Bill_Pay()
    {
        return this.getSentTo_Bill_Pay(this.UserSecurityID);
    }

    public void setSentTo_Bill_Pay(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SentTo_Bill_Pay' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[SentTo_Bill_Pay]=["+newValue+"]");
                   dbDB.setSentTo_Bill_Pay(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[SentTo_Bill_Pay]=["+newValue+"]");
           dbDB.setSentTo_Bill_Pay(newValue);
         }
    }
    public Date getSentTo_Bill_Pay(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SentTo_Bill_Pay' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getSentTo_Bill_Pay();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getSentTo_Bill_Pay();
         }
        return myVal;
    }

    public void setSentTo_Bill_Pay_CRID(Integer newValue)
    {
        this.setSentTo_Bill_Pay_CRID(newValue,this.UserSecurityID);


    }
    public Integer getSentTo_Bill_Pay_CRID()
    {
        return this.getSentTo_Bill_Pay_CRID(this.UserSecurityID);
    }

    public void setSentTo_Bill_Pay_CRID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SentTo_Bill_Pay_CRID' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[SentTo_Bill_Pay_CRID]=["+newValue+"]");
                   dbDB.setSentTo_Bill_Pay_CRID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[SentTo_Bill_Pay_CRID]=["+newValue+"]");
           dbDB.setSentTo_Bill_Pay_CRID(newValue);
         }
    }
    public Integer getSentTo_Bill_Pay_CRID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SentTo_Bill_Pay_CRID' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getSentTo_Bill_Pay_CRID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getSentTo_Bill_Pay_CRID();
         }
        return myVal;
    }

    public void setRec_Bill_Pay(Date newValue)
    {
        this.setRec_Bill_Pay(newValue,this.UserSecurityID);


    }
    public Date getRec_Bill_Pay()
    {
        return this.getRec_Bill_Pay(this.UserSecurityID);
    }

    public void setRec_Bill_Pay(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Rec_Bill_Pay' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[Rec_Bill_Pay]=["+newValue+"]");
                   dbDB.setRec_Bill_Pay(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[Rec_Bill_Pay]=["+newValue+"]");
           dbDB.setRec_Bill_Pay(newValue);
         }
    }
    public Date getRec_Bill_Pay(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Rec_Bill_Pay' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getRec_Bill_Pay();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getRec_Bill_Pay();
         }
        return myVal;
    }

    public void setRec_Bill_Pay_CRID(Integer newValue)
    {
        this.setRec_Bill_Pay_CRID(newValue,this.UserSecurityID);


    }
    public Integer getRec_Bill_Pay_CRID()
    {
        return this.getRec_Bill_Pay_CRID(this.UserSecurityID);
    }

    public void setRec_Bill_Pay_CRID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Rec_Bill_Pay_CRID' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[Rec_Bill_Pay_CRID]=["+newValue+"]");
                   dbDB.setRec_Bill_Pay_CRID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[Rec_Bill_Pay_CRID]=["+newValue+"]");
           dbDB.setRec_Bill_Pay_CRID(newValue);
         }
    }
    public Integer getRec_Bill_Pay_CRID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Rec_Bill_Pay_CRID' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getRec_Bill_Pay_CRID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getRec_Bill_Pay_CRID();
         }
        return myVal;
    }

    public void setSentTo_Bill_Pro(Date newValue)
    {
        this.setSentTo_Bill_Pro(newValue,this.UserSecurityID);


    }
    public Date getSentTo_Bill_Pro()
    {
        return this.getSentTo_Bill_Pro(this.UserSecurityID);
    }

    public void setSentTo_Bill_Pro(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SentTo_Bill_Pro' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[SentTo_Bill_Pro]=["+newValue+"]");
                   dbDB.setSentTo_Bill_Pro(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[SentTo_Bill_Pro]=["+newValue+"]");
           dbDB.setSentTo_Bill_Pro(newValue);
         }
    }
    public Date getSentTo_Bill_Pro(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SentTo_Bill_Pro' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getSentTo_Bill_Pro();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getSentTo_Bill_Pro();
         }
        return myVal;
    }

    public void setSentTo_Bill_Pro_CRID(Integer newValue)
    {
        this.setSentTo_Bill_Pro_CRID(newValue,this.UserSecurityID);


    }
    public Integer getSentTo_Bill_Pro_CRID()
    {
        return this.getSentTo_Bill_Pro_CRID(this.UserSecurityID);
    }

    public void setSentTo_Bill_Pro_CRID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SentTo_Bill_Pro_CRID' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[SentTo_Bill_Pro_CRID]=["+newValue+"]");
                   dbDB.setSentTo_Bill_Pro_CRID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[SentTo_Bill_Pro_CRID]=["+newValue+"]");
           dbDB.setSentTo_Bill_Pro_CRID(newValue);
         }
    }
    public Integer getSentTo_Bill_Pro_CRID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SentTo_Bill_Pro_CRID' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getSentTo_Bill_Pro_CRID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getSentTo_Bill_Pro_CRID();
         }
        return myVal;
    }

    public void setRec_Bill_Pro(Date newValue)
    {
        this.setRec_Bill_Pro(newValue,this.UserSecurityID);


    }
    public Date getRec_Bill_Pro()
    {
        return this.getRec_Bill_Pro(this.UserSecurityID);
    }

    public void setRec_Bill_Pro(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Rec_Bill_Pro' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[Rec_Bill_Pro]=["+newValue+"]");
                   dbDB.setRec_Bill_Pro(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[Rec_Bill_Pro]=["+newValue+"]");
           dbDB.setRec_Bill_Pro(newValue);
         }
    }
    public Date getRec_Bill_Pro(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Rec_Bill_Pro' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getRec_Bill_Pro();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getRec_Bill_Pro();
         }
        return myVal;
    }

    public void setRec_Bill_Pro_CRID(Integer newValue)
    {
        this.setRec_Bill_Pro_CRID(newValue,this.UserSecurityID);


    }
    public Integer getRec_Bill_Pro_CRID()
    {
        return this.getRec_Bill_Pro_CRID(this.UserSecurityID);
    }

    public void setRec_Bill_Pro_CRID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Rec_Bill_Pro_CRID' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[Rec_Bill_Pro_CRID]=["+newValue+"]");
                   dbDB.setRec_Bill_Pro_CRID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[Rec_Bill_Pro_CRID]=["+newValue+"]");
           dbDB.setRec_Bill_Pro_CRID(newValue);
         }
    }
    public Integer getRec_Bill_Pro_CRID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Rec_Bill_Pro_CRID' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getRec_Bill_Pro_CRID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getRec_Bill_Pro_CRID();
         }
        return myVal;
    }

    public void setReportFileID(Integer newValue)
    {
        this.setReportFileID(newValue,this.UserSecurityID);


    }
    public Integer getReportFileID()
    {
        return this.getReportFileID(this.UserSecurityID);
    }

    public void setReportFileID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ReportFileID' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[ReportFileID]=["+newValue+"]");
                   dbDB.setReportFileID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[ReportFileID]=["+newValue+"]");
           dbDB.setReportFileID(newValue);
         }
    }
    public Integer getReportFileID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ReportFileID' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getReportFileID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getReportFileID();
         }
        return myVal;
    }

    public void setCapabilityReportFileID(Integer newValue)
    {
        this.setCapabilityReportFileID(newValue,this.UserSecurityID);


    }
    public Integer getCapabilityReportFileID()
    {
        return this.getCapabilityReportFileID(this.UserSecurityID);
    }

    public void setCapabilityReportFileID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CapabilityReportFileID' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[CapabilityReportFileID]=["+newValue+"]");
                   dbDB.setCapabilityReportFileID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[CapabilityReportFileID]=["+newValue+"]");
           dbDB.setCapabilityReportFileID(newValue);
         }
    }
    public Integer getCapabilityReportFileID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CapabilityReportFileID' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getCapabilityReportFileID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getCapabilityReportFileID();
         }
        return myVal;
    }

    public void setDICOMFileID(Integer newValue)
    {
        this.setDICOMFileID(newValue,this.UserSecurityID);


    }
    public Integer getDICOMFileID()
    {
        return this.getDICOMFileID(this.UserSecurityID);
    }

    public void setDICOMFileID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='DICOMFileID' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[DICOMFileID]=["+newValue+"]");
                   dbDB.setDICOMFileID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[DICOMFileID]=["+newValue+"]");
           dbDB.setDICOMFileID(newValue);
         }
    }
    public Integer getDICOMFileID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='DICOMFileID' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getDICOMFileID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getDICOMFileID();
         }
        return myVal;
    }

    public void setLODID(Integer newValue)
    {
        this.setLODID(newValue,this.UserSecurityID);


    }
    public Integer getLODID()
    {
        return this.getLODID(this.UserSecurityID);
    }

    public void setLODID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='LODID' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[LODID]=["+newValue+"]");
                   dbDB.setLODID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[LODID]=["+newValue+"]");
           dbDB.setLODID(newValue);
         }
    }
    public Integer getLODID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='LODID' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getLODID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getLODID();
         }
        return myVal;
    }

    public void setComments(String newValue)
    {
        this.setComments(newValue,this.UserSecurityID);


    }
    public String getComments()
    {
        return this.getComments(this.UserSecurityID);
    }

    public void setComments(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Comments' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[Comments]=["+newValue+"]");
                   dbDB.setComments(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[Comments]=["+newValue+"]");
           dbDB.setComments(newValue);
         }
    }
    public String getComments(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Comments' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getComments();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getComments();
         }
        return myVal;
    }

    public void setAuditNotes(String newValue)
    {
        this.setAuditNotes(newValue,this.UserSecurityID);


    }
    public String getAuditNotes()
    {
        return this.getAuditNotes(this.UserSecurityID);
    }

    public void setAuditNotes(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AuditNotes' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[AuditNotes]=["+newValue+"]");
                   dbDB.setAuditNotes(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[AuditNotes]=["+newValue+"]");
           dbDB.setAuditNotes(newValue);
         }
    }
    public String getAuditNotes(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AuditNotes' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getAuditNotes();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getAuditNotes();
         }
        return myVal;
    }

    public void setEncounterStatusID(Integer newValue)
    {
        this.setEncounterStatusID(newValue,this.UserSecurityID);


    }
    public Integer getEncounterStatusID()
    {
        return this.getEncounterStatusID(this.UserSecurityID);
    }

    public void setEncounterStatusID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='EncounterStatusID' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[EncounterStatusID]=["+newValue+"]");
                   dbDB.setEncounterStatusID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[EncounterStatusID]=["+newValue+"]");
           dbDB.setEncounterStatusID(newValue);
         }
    }
    public Integer getEncounterStatusID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='EncounterStatusID' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getEncounterStatusID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getEncounterStatusID();
         }
        return myVal;
    }

    public void setPaidToProviderCheck1Number(String newValue)
    {
        this.setPaidToProviderCheck1Number(newValue,this.UserSecurityID);


    }
    public String getPaidToProviderCheck1Number()
    {
        return this.getPaidToProviderCheck1Number(this.UserSecurityID);
    }

    public void setPaidToProviderCheck1Number(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PaidToProviderCheck1Number' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[PaidToProviderCheck1Number]=["+newValue+"]");
                   dbDB.setPaidToProviderCheck1Number(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[PaidToProviderCheck1Number]=["+newValue+"]");
           dbDB.setPaidToProviderCheck1Number(newValue);
         }
    }
    public String getPaidToProviderCheck1Number(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PaidToProviderCheck1Number' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPaidToProviderCheck1Number();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPaidToProviderCheck1Number();
         }
        return myVal;
    }

    public void setPaidToProviderCheck1Amount(Double newValue)
    {
        this.setPaidToProviderCheck1Amount(newValue,this.UserSecurityID);


    }
    public Double getPaidToProviderCheck1Amount()
    {
        return this.getPaidToProviderCheck1Amount(this.UserSecurityID);
    }

    public void setPaidToProviderCheck1Amount(Double newValue, Integer GroupRefID)
    {
        Double myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PaidToProviderCheck1Amount' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[PaidToProviderCheck1Amount]=["+newValue+"]");
                   dbDB.setPaidToProviderCheck1Amount(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[PaidToProviderCheck1Amount]=["+newValue+"]");
           dbDB.setPaidToProviderCheck1Amount(newValue);
         }
    }
    public Double getPaidToProviderCheck1Amount(Integer GroupRefID)
    {
        Double myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PaidToProviderCheck1Amount' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPaidToProviderCheck1Amount();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPaidToProviderCheck1Amount();
         }
        return myVal;
    }

    public void setPaidToProviderCheck1Date(Date newValue)
    {
        this.setPaidToProviderCheck1Date(newValue,this.UserSecurityID);


    }
    public Date getPaidToProviderCheck1Date()
    {
        return this.getPaidToProviderCheck1Date(this.UserSecurityID);
    }

    public void setPaidToProviderCheck1Date(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PaidToProviderCheck1Date' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[PaidToProviderCheck1Date]=["+newValue+"]");
                   dbDB.setPaidToProviderCheck1Date(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[PaidToProviderCheck1Date]=["+newValue+"]");
           dbDB.setPaidToProviderCheck1Date(newValue);
         }
    }
    public Date getPaidToProviderCheck1Date(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PaidToProviderCheck1Date' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPaidToProviderCheck1Date();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPaidToProviderCheck1Date();
         }
        return myVal;
    }

    public void setPaidToProviderCheck2Number(String newValue)
    {
        this.setPaidToProviderCheck2Number(newValue,this.UserSecurityID);


    }
    public String getPaidToProviderCheck2Number()
    {
        return this.getPaidToProviderCheck2Number(this.UserSecurityID);
    }

    public void setPaidToProviderCheck2Number(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PaidToProviderCheck2Number' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[PaidToProviderCheck2Number]=["+newValue+"]");
                   dbDB.setPaidToProviderCheck2Number(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[PaidToProviderCheck2Number]=["+newValue+"]");
           dbDB.setPaidToProviderCheck2Number(newValue);
         }
    }
    public String getPaidToProviderCheck2Number(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PaidToProviderCheck2Number' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPaidToProviderCheck2Number();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPaidToProviderCheck2Number();
         }
        return myVal;
    }

    public void setPaidToProviderCheck2Amount(Double newValue)
    {
        this.setPaidToProviderCheck2Amount(newValue,this.UserSecurityID);


    }
    public Double getPaidToProviderCheck2Amount()
    {
        return this.getPaidToProviderCheck2Amount(this.UserSecurityID);
    }

    public void setPaidToProviderCheck2Amount(Double newValue, Integer GroupRefID)
    {
        Double myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PaidToProviderCheck2Amount' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[PaidToProviderCheck2Amount]=["+newValue+"]");
                   dbDB.setPaidToProviderCheck2Amount(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[PaidToProviderCheck2Amount]=["+newValue+"]");
           dbDB.setPaidToProviderCheck2Amount(newValue);
         }
    }
    public Double getPaidToProviderCheck2Amount(Integer GroupRefID)
    {
        Double myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PaidToProviderCheck2Amount' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPaidToProviderCheck2Amount();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPaidToProviderCheck2Amount();
         }
        return myVal;
    }

    public void setPaidToProviderCheck2Date(Date newValue)
    {
        this.setPaidToProviderCheck2Date(newValue,this.UserSecurityID);


    }
    public Date getPaidToProviderCheck2Date()
    {
        return this.getPaidToProviderCheck2Date(this.UserSecurityID);
    }

    public void setPaidToProviderCheck2Date(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PaidToProviderCheck2Date' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[PaidToProviderCheck2Date]=["+newValue+"]");
                   dbDB.setPaidToProviderCheck2Date(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[PaidToProviderCheck2Date]=["+newValue+"]");
           dbDB.setPaidToProviderCheck2Date(newValue);
         }
    }
    public Date getPaidToProviderCheck2Date(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PaidToProviderCheck2Date' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPaidToProviderCheck2Date();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPaidToProviderCheck2Date();
         }
        return myVal;
    }

    public void setPaidToProviderCheck3Number(String newValue)
    {
        this.setPaidToProviderCheck3Number(newValue,this.UserSecurityID);


    }
    public String getPaidToProviderCheck3Number()
    {
        return this.getPaidToProviderCheck3Number(this.UserSecurityID);
    }

    public void setPaidToProviderCheck3Number(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PaidToProviderCheck3Number' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[PaidToProviderCheck3Number]=["+newValue+"]");
                   dbDB.setPaidToProviderCheck3Number(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[PaidToProviderCheck3Number]=["+newValue+"]");
           dbDB.setPaidToProviderCheck3Number(newValue);
         }
    }
    public String getPaidToProviderCheck3Number(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PaidToProviderCheck3Number' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPaidToProviderCheck3Number();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPaidToProviderCheck3Number();
         }
        return myVal;
    }

    public void setPaidToProviderCheck3Amount(Double newValue)
    {
        this.setPaidToProviderCheck3Amount(newValue,this.UserSecurityID);


    }
    public Double getPaidToProviderCheck3Amount()
    {
        return this.getPaidToProviderCheck3Amount(this.UserSecurityID);
    }

    public void setPaidToProviderCheck3Amount(Double newValue, Integer GroupRefID)
    {
        Double myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PaidToProviderCheck3Amount' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[PaidToProviderCheck3Amount]=["+newValue+"]");
                   dbDB.setPaidToProviderCheck3Amount(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[PaidToProviderCheck3Amount]=["+newValue+"]");
           dbDB.setPaidToProviderCheck3Amount(newValue);
         }
    }
    public Double getPaidToProviderCheck3Amount(Integer GroupRefID)
    {
        Double myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PaidToProviderCheck3Amount' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPaidToProviderCheck3Amount();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPaidToProviderCheck3Amount();
         }
        return myVal;
    }

    public void setPaidToProviderCheck3Date(Date newValue)
    {
        this.setPaidToProviderCheck3Date(newValue,this.UserSecurityID);


    }
    public Date getPaidToProviderCheck3Date()
    {
        return this.getPaidToProviderCheck3Date(this.UserSecurityID);
    }

    public void setPaidToProviderCheck3Date(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PaidToProviderCheck3Date' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[PaidToProviderCheck3Date]=["+newValue+"]");
                   dbDB.setPaidToProviderCheck3Date(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[PaidToProviderCheck3Date]=["+newValue+"]");
           dbDB.setPaidToProviderCheck3Date(newValue);
         }
    }
    public Date getPaidToProviderCheck3Date(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PaidToProviderCheck3Date' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPaidToProviderCheck3Date();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPaidToProviderCheck3Date();
         }
        return myVal;
    }

    public void setSentTo_ReqRec_RefDr(Date newValue)
    {
        this.setSentTo_ReqRec_RefDr(newValue,this.UserSecurityID);


    }
    public Date getSentTo_ReqRec_RefDr()
    {
        return this.getSentTo_ReqRec_RefDr(this.UserSecurityID);
    }

    public void setSentTo_ReqRec_RefDr(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SentTo_ReqRec_RefDr' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[SentTo_ReqRec_RefDr]=["+newValue+"]");
                   dbDB.setSentTo_ReqRec_RefDr(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[SentTo_ReqRec_RefDr]=["+newValue+"]");
           dbDB.setSentTo_ReqRec_RefDr(newValue);
         }
    }
    public Date getSentTo_ReqRec_RefDr(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SentTo_ReqRec_RefDr' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getSentTo_ReqRec_RefDr();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getSentTo_ReqRec_RefDr();
         }
        return myVal;
    }

    public void setSentTo_ReqRec_RefDr_CRID(Integer newValue)
    {
        this.setSentTo_ReqRec_RefDr_CRID(newValue,this.UserSecurityID);


    }
    public Integer getSentTo_ReqRec_RefDr_CRID()
    {
        return this.getSentTo_ReqRec_RefDr_CRID(this.UserSecurityID);
    }

    public void setSentTo_ReqRec_RefDr_CRID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SentTo_ReqRec_RefDr_CRID' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[SentTo_ReqRec_RefDr_CRID]=["+newValue+"]");
                   dbDB.setSentTo_ReqRec_RefDr_CRID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[SentTo_ReqRec_RefDr_CRID]=["+newValue+"]");
           dbDB.setSentTo_ReqRec_RefDr_CRID(newValue);
         }
    }
    public Integer getSentTo_ReqRec_RefDr_CRID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SentTo_ReqRec_RefDr_CRID' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getSentTo_ReqRec_RefDr_CRID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getSentTo_ReqRec_RefDr_CRID();
         }
        return myVal;
    }

    public void setSentTo_ReqRec_Adj(Date newValue)
    {
        this.setSentTo_ReqRec_Adj(newValue,this.UserSecurityID);


    }
    public Date getSentTo_ReqRec_Adj()
    {
        return this.getSentTo_ReqRec_Adj(this.UserSecurityID);
    }

    public void setSentTo_ReqRec_Adj(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SentTo_ReqRec_Adj' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[SentTo_ReqRec_Adj]=["+newValue+"]");
                   dbDB.setSentTo_ReqRec_Adj(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[SentTo_ReqRec_Adj]=["+newValue+"]");
           dbDB.setSentTo_ReqRec_Adj(newValue);
         }
    }
    public Date getSentTo_ReqRec_Adj(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SentTo_ReqRec_Adj' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getSentTo_ReqRec_Adj();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getSentTo_ReqRec_Adj();
         }
        return myVal;
    }

    public void setSentTo_ReqRec_Adj_CRID(Integer newValue)
    {
        this.setSentTo_ReqRec_Adj_CRID(newValue,this.UserSecurityID);


    }
    public Integer getSentTo_ReqRec_Adj_CRID()
    {
        return this.getSentTo_ReqRec_Adj_CRID(this.UserSecurityID);
    }

    public void setSentTo_ReqRec_Adj_CRID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SentTo_ReqRec_Adj_CRID' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[SentTo_ReqRec_Adj_CRID]=["+newValue+"]");
                   dbDB.setSentTo_ReqRec_Adj_CRID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[SentTo_ReqRec_Adj_CRID]=["+newValue+"]");
           dbDB.setSentTo_ReqRec_Adj_CRID(newValue);
         }
    }
    public Integer getSentTo_ReqRec_Adj_CRID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SentTo_ReqRec_Adj_CRID' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getSentTo_ReqRec_Adj_CRID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getSentTo_ReqRec_Adj_CRID();
         }
        return myVal;
    }

    public void setTimeTrack_ReqRec(Date newValue)
    {
        this.setTimeTrack_ReqRec(newValue,this.UserSecurityID);


    }
    public Date getTimeTrack_ReqRec()
    {
        return this.getTimeTrack_ReqRec(this.UserSecurityID);
    }

    public void setTimeTrack_ReqRec(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='TimeTrack_ReqRec' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[TimeTrack_ReqRec]=["+newValue+"]");
                   dbDB.setTimeTrack_ReqRec(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[TimeTrack_ReqRec]=["+newValue+"]");
           dbDB.setTimeTrack_ReqRec(newValue);
         }
    }
    public Date getTimeTrack_ReqRec(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='TimeTrack_ReqRec' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getTimeTrack_ReqRec();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getTimeTrack_ReqRec();
         }
        return myVal;
    }

    public void setTimeTrack_ReqRec_UserID(Integer newValue)
    {
        this.setTimeTrack_ReqRec_UserID(newValue,this.UserSecurityID);


    }
    public Integer getTimeTrack_ReqRec_UserID()
    {
        return this.getTimeTrack_ReqRec_UserID(this.UserSecurityID);
    }

    public void setTimeTrack_ReqRec_UserID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='TimeTrack_ReqRec_UserID' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[TimeTrack_ReqRec_UserID]=["+newValue+"]");
                   dbDB.setTimeTrack_ReqRec_UserID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[TimeTrack_ReqRec_UserID]=["+newValue+"]");
           dbDB.setTimeTrack_ReqRec_UserID(newValue);
         }
    }
    public Integer getTimeTrack_ReqRec_UserID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='TimeTrack_ReqRec_UserID' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getTimeTrack_ReqRec_UserID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getTimeTrack_ReqRec_UserID();
         }
        return myVal;
    }

    public void setTimeTrack_ReqCreated(Date newValue)
    {
        this.setTimeTrack_ReqCreated(newValue,this.UserSecurityID);


    }
    public Date getTimeTrack_ReqCreated()
    {
        return this.getTimeTrack_ReqCreated(this.UserSecurityID);
    }

    public void setTimeTrack_ReqCreated(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='TimeTrack_ReqCreated' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[TimeTrack_ReqCreated]=["+newValue+"]");
                   dbDB.setTimeTrack_ReqCreated(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[TimeTrack_ReqCreated]=["+newValue+"]");
           dbDB.setTimeTrack_ReqCreated(newValue);
         }
    }
    public Date getTimeTrack_ReqCreated(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='TimeTrack_ReqCreated' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getTimeTrack_ReqCreated();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getTimeTrack_ReqCreated();
         }
        return myVal;
    }

    public void setTimeTrack_ReqCreated_UserID(Integer newValue)
    {
        this.setTimeTrack_ReqCreated_UserID(newValue,this.UserSecurityID);


    }
    public Integer getTimeTrack_ReqCreated_UserID()
    {
        return this.getTimeTrack_ReqCreated_UserID(this.UserSecurityID);
    }

    public void setTimeTrack_ReqCreated_UserID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='TimeTrack_ReqCreated_UserID' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[TimeTrack_ReqCreated_UserID]=["+newValue+"]");
                   dbDB.setTimeTrack_ReqCreated_UserID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[TimeTrack_ReqCreated_UserID]=["+newValue+"]");
           dbDB.setTimeTrack_ReqCreated_UserID(newValue);
         }
    }
    public Integer getTimeTrack_ReqCreated_UserID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='TimeTrack_ReqCreated_UserID' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getTimeTrack_ReqCreated_UserID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getTimeTrack_ReqCreated_UserID();
         }
        return myVal;
    }

    public void setTimeTrack_ReqProc(Date newValue)
    {
        this.setTimeTrack_ReqProc(newValue,this.UserSecurityID);


    }
    public Date getTimeTrack_ReqProc()
    {
        return this.getTimeTrack_ReqProc(this.UserSecurityID);
    }

    public void setTimeTrack_ReqProc(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='TimeTrack_ReqProc' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[TimeTrack_ReqProc]=["+newValue+"]");
                   dbDB.setTimeTrack_ReqProc(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[TimeTrack_ReqProc]=["+newValue+"]");
           dbDB.setTimeTrack_ReqProc(newValue);
         }
    }
    public Date getTimeTrack_ReqProc(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='TimeTrack_ReqProc' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getTimeTrack_ReqProc();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getTimeTrack_ReqProc();
         }
        return myVal;
    }

    public void setTimeTrack_ReqProc_UserID(Integer newValue)
    {
        this.setTimeTrack_ReqProc_UserID(newValue,this.UserSecurityID);


    }
    public Integer getTimeTrack_ReqProc_UserID()
    {
        return this.getTimeTrack_ReqProc_UserID(this.UserSecurityID);
    }

    public void setTimeTrack_ReqProc_UserID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='TimeTrack_ReqProc_UserID' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[TimeTrack_ReqProc_UserID]=["+newValue+"]");
                   dbDB.setTimeTrack_ReqProc_UserID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[TimeTrack_ReqProc_UserID]=["+newValue+"]");
           dbDB.setTimeTrack_ReqProc_UserID(newValue);
         }
    }
    public Integer getTimeTrack_ReqProc_UserID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='TimeTrack_ReqProc_UserID' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getTimeTrack_ReqProc_UserID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getTimeTrack_ReqProc_UserID();
         }
        return myVal;
    }

    public void setTimeTrack_ReqSched(Date newValue)
    {
        this.setTimeTrack_ReqSched(newValue,this.UserSecurityID);


    }
    public Date getTimeTrack_ReqSched()
    {
        return this.getTimeTrack_ReqSched(this.UserSecurityID);
    }

    public void setTimeTrack_ReqSched(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='TimeTrack_ReqSched' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[TimeTrack_ReqSched]=["+newValue+"]");
                   dbDB.setTimeTrack_ReqSched(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[TimeTrack_ReqSched]=["+newValue+"]");
           dbDB.setTimeTrack_ReqSched(newValue);
         }
    }
    public Date getTimeTrack_ReqSched(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='TimeTrack_ReqSched' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getTimeTrack_ReqSched();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getTimeTrack_ReqSched();
         }
        return myVal;
    }

    public void setTimeTrack_ReqSched_UserID(Integer newValue)
    {
        this.setTimeTrack_ReqSched_UserID(newValue,this.UserSecurityID);


    }
    public Integer getTimeTrack_ReqSched_UserID()
    {
        return this.getTimeTrack_ReqSched_UserID(this.UserSecurityID);
    }

    public void setTimeTrack_ReqSched_UserID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='TimeTrack_ReqSched_UserID' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[TimeTrack_ReqSched_UserID]=["+newValue+"]");
                   dbDB.setTimeTrack_ReqSched_UserID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[TimeTrack_ReqSched_UserID]=["+newValue+"]");
           dbDB.setTimeTrack_ReqSched_UserID(newValue);
         }
    }
    public Integer getTimeTrack_ReqSched_UserID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='TimeTrack_ReqSched_UserID' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getTimeTrack_ReqSched_UserID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getTimeTrack_ReqSched_UserID();
         }
        return myVal;
    }

    public void setTimeTrack_ReqDelivered(Date newValue)
    {
        this.setTimeTrack_ReqDelivered(newValue,this.UserSecurityID);


    }
    public Date getTimeTrack_ReqDelivered()
    {
        return this.getTimeTrack_ReqDelivered(this.UserSecurityID);
    }

    public void setTimeTrack_ReqDelivered(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='TimeTrack_ReqDelivered' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[TimeTrack_ReqDelivered]=["+newValue+"]");
                   dbDB.setTimeTrack_ReqDelivered(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[TimeTrack_ReqDelivered]=["+newValue+"]");
           dbDB.setTimeTrack_ReqDelivered(newValue);
         }
    }
    public Date getTimeTrack_ReqDelivered(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='TimeTrack_ReqDelivered' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getTimeTrack_ReqDelivered();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getTimeTrack_ReqDelivered();
         }
        return myVal;
    }

    public void setTimeTrack_ReqDelivered_UserID(Integer newValue)
    {
        this.setTimeTrack_ReqDelivered_UserID(newValue,this.UserSecurityID);


    }
    public Integer getTimeTrack_ReqDelivered_UserID()
    {
        return this.getTimeTrack_ReqDelivered_UserID(this.UserSecurityID);
    }

    public void setTimeTrack_ReqDelivered_UserID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='TimeTrack_ReqDelivered_UserID' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[TimeTrack_ReqDelivered_UserID]=["+newValue+"]");
                   dbDB.setTimeTrack_ReqDelivered_UserID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[TimeTrack_ReqDelivered_UserID]=["+newValue+"]");
           dbDB.setTimeTrack_ReqDelivered_UserID(newValue);
         }
    }
    public Integer getTimeTrack_ReqDelivered_UserID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='TimeTrack_ReqDelivered_UserID' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getTimeTrack_ReqDelivered_UserID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getTimeTrack_ReqDelivered_UserID();
         }
        return myVal;
    }

    public void setTimeTrack_ReqPaidIn(Date newValue)
    {
        this.setTimeTrack_ReqPaidIn(newValue,this.UserSecurityID);


    }
    public Date getTimeTrack_ReqPaidIn()
    {
        return this.getTimeTrack_ReqPaidIn(this.UserSecurityID);
    }

    public void setTimeTrack_ReqPaidIn(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='TimeTrack_ReqPaidIn' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[TimeTrack_ReqPaidIn]=["+newValue+"]");
                   dbDB.setTimeTrack_ReqPaidIn(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[TimeTrack_ReqPaidIn]=["+newValue+"]");
           dbDB.setTimeTrack_ReqPaidIn(newValue);
         }
    }
    public Date getTimeTrack_ReqPaidIn(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='TimeTrack_ReqPaidIn' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getTimeTrack_ReqPaidIn();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getTimeTrack_ReqPaidIn();
         }
        return myVal;
    }

    public void setTimeTrack_ReqPaidIn_UserID(Integer newValue)
    {
        this.setTimeTrack_ReqPaidIn_UserID(newValue,this.UserSecurityID);


    }
    public Integer getTimeTrack_ReqPaidIn_UserID()
    {
        return this.getTimeTrack_ReqPaidIn_UserID(this.UserSecurityID);
    }

    public void setTimeTrack_ReqPaidIn_UserID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='TimeTrack_ReqPaidIn_UserID' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[TimeTrack_ReqPaidIn_UserID]=["+newValue+"]");
                   dbDB.setTimeTrack_ReqPaidIn_UserID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[TimeTrack_ReqPaidIn_UserID]=["+newValue+"]");
           dbDB.setTimeTrack_ReqPaidIn_UserID(newValue);
         }
    }
    public Integer getTimeTrack_ReqPaidIn_UserID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='TimeTrack_ReqPaidIn_UserID' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getTimeTrack_ReqPaidIn_UserID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getTimeTrack_ReqPaidIn_UserID();
         }
        return myVal;
    }

    public void setTimeTrack_ReqPaidOut(Date newValue)
    {
        this.setTimeTrack_ReqPaidOut(newValue,this.UserSecurityID);


    }
    public Date getTimeTrack_ReqPaidOut()
    {
        return this.getTimeTrack_ReqPaidOut(this.UserSecurityID);
    }

    public void setTimeTrack_ReqPaidOut(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='TimeTrack_ReqPaidOut' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[TimeTrack_ReqPaidOut]=["+newValue+"]");
                   dbDB.setTimeTrack_ReqPaidOut(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[TimeTrack_ReqPaidOut]=["+newValue+"]");
           dbDB.setTimeTrack_ReqPaidOut(newValue);
         }
    }
    public Date getTimeTrack_ReqPaidOut(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='TimeTrack_ReqPaidOut' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getTimeTrack_ReqPaidOut();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getTimeTrack_ReqPaidOut();
         }
        return myVal;
    }

    public void setTimeTrack_ReqPaidOut_UserID(Integer newValue)
    {
        this.setTimeTrack_ReqPaidOut_UserID(newValue,this.UserSecurityID);


    }
    public Integer getTimeTrack_ReqPaidOut_UserID()
    {
        return this.getTimeTrack_ReqPaidOut_UserID(this.UserSecurityID);
    }

    public void setTimeTrack_ReqPaidOut_UserID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='TimeTrack_ReqPaidOut_UserID' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[TimeTrack_ReqPaidOut_UserID]=["+newValue+"]");
                   dbDB.setTimeTrack_ReqPaidOut_UserID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[TimeTrack_ReqPaidOut_UserID]=["+newValue+"]");
           dbDB.setTimeTrack_ReqPaidOut_UserID(newValue);
         }
    }
    public Integer getTimeTrack_ReqPaidOut_UserID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='TimeTrack_ReqPaidOut_UserID' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getTimeTrack_ReqPaidOut_UserID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getTimeTrack_ReqPaidOut_UserID();
         }
        return myVal;
    }

    public void setProviderInvoiceID(Integer newValue)
    {
        this.setProviderInvoiceID(newValue,this.UserSecurityID);


    }
    public Integer getProviderInvoiceID()
    {
        return this.getProviderInvoiceID(this.UserSecurityID);
    }

    public void setProviderInvoiceID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ProviderInvoiceID' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[ProviderInvoiceID]=["+newValue+"]");
                   dbDB.setProviderInvoiceID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[ProviderInvoiceID]=["+newValue+"]");
           dbDB.setProviderInvoiceID(newValue);
         }
    }
    public Integer getProviderInvoiceID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ProviderInvoiceID' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getProviderInvoiceID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getProviderInvoiceID();
         }
        return myVal;
    }

    public void setTimeTrack_ReqRec_RCodeID(Integer newValue)
    {
        this.setTimeTrack_ReqRec_RCodeID(newValue,this.UserSecurityID);


    }
    public Integer getTimeTrack_ReqRec_RCodeID()
    {
        return this.getTimeTrack_ReqRec_RCodeID(this.UserSecurityID);
    }

    public void setTimeTrack_ReqRec_RCodeID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='TimeTrack_ReqRec_RCodeID' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[TimeTrack_ReqRec_RCodeID]=["+newValue+"]");
                   dbDB.setTimeTrack_ReqRec_RCodeID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[TimeTrack_ReqRec_RCodeID]=["+newValue+"]");
           dbDB.setTimeTrack_ReqRec_RCodeID(newValue);
         }
    }
    public Integer getTimeTrack_ReqRec_RCodeID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='TimeTrack_ReqRec_RCodeID' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getTimeTrack_ReqRec_RCodeID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getTimeTrack_ReqRec_RCodeID();
         }
        return myVal;
    }

    public void setTimeTrack_ReqCreated_RCodeID(Integer newValue)
    {
        this.setTimeTrack_ReqCreated_RCodeID(newValue,this.UserSecurityID);


    }
    public Integer getTimeTrack_ReqCreated_RCodeID()
    {
        return this.getTimeTrack_ReqCreated_RCodeID(this.UserSecurityID);
    }

    public void setTimeTrack_ReqCreated_RCodeID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='TimeTrack_ReqCreated_RCodeID' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[TimeTrack_ReqCreated_RCodeID]=["+newValue+"]");
                   dbDB.setTimeTrack_ReqCreated_RCodeID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[TimeTrack_ReqCreated_RCodeID]=["+newValue+"]");
           dbDB.setTimeTrack_ReqCreated_RCodeID(newValue);
         }
    }
    public Integer getTimeTrack_ReqCreated_RCodeID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='TimeTrack_ReqCreated_RCodeID' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getTimeTrack_ReqCreated_RCodeID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getTimeTrack_ReqCreated_RCodeID();
         }
        return myVal;
    }

    public void setTimeTrack_ReqProc_RCodeID(Integer newValue)
    {
        this.setTimeTrack_ReqProc_RCodeID(newValue,this.UserSecurityID);


    }
    public Integer getTimeTrack_ReqProc_RCodeID()
    {
        return this.getTimeTrack_ReqProc_RCodeID(this.UserSecurityID);
    }

    public void setTimeTrack_ReqProc_RCodeID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='TimeTrack_ReqProc_RCodeID' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[TimeTrack_ReqProc_RCodeID]=["+newValue+"]");
                   dbDB.setTimeTrack_ReqProc_RCodeID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[TimeTrack_ReqProc_RCodeID]=["+newValue+"]");
           dbDB.setTimeTrack_ReqProc_RCodeID(newValue);
         }
    }
    public Integer getTimeTrack_ReqProc_RCodeID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='TimeTrack_ReqProc_RCodeID' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getTimeTrack_ReqProc_RCodeID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getTimeTrack_ReqProc_RCodeID();
         }
        return myVal;
    }

    public void setTimeTrack_ReqSched_RCodeID(Integer newValue)
    {
        this.setTimeTrack_ReqSched_RCodeID(newValue,this.UserSecurityID);


    }
    public Integer getTimeTrack_ReqSched_RCodeID()
    {
        return this.getTimeTrack_ReqSched_RCodeID(this.UserSecurityID);
    }

    public void setTimeTrack_ReqSched_RCodeID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='TimeTrack_ReqSched_RCodeID' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[TimeTrack_ReqSched_RCodeID]=["+newValue+"]");
                   dbDB.setTimeTrack_ReqSched_RCodeID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[TimeTrack_ReqSched_RCodeID]=["+newValue+"]");
           dbDB.setTimeTrack_ReqSched_RCodeID(newValue);
         }
    }
    public Integer getTimeTrack_ReqSched_RCodeID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='TimeTrack_ReqSched_RCodeID' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getTimeTrack_ReqSched_RCodeID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getTimeTrack_ReqSched_RCodeID();
         }
        return myVal;
    }

    public void setTimeTrack_ReqDelivered_RCodeID(Integer newValue)
    {
        this.setTimeTrack_ReqDelivered_RCodeID(newValue,this.UserSecurityID);


    }
    public Integer getTimeTrack_ReqDelivered_RCodeID()
    {
        return this.getTimeTrack_ReqDelivered_RCodeID(this.UserSecurityID);
    }

    public void setTimeTrack_ReqDelivered_RCodeID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='TimeTrack_ReqDelivered_RCodeID' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[TimeTrack_ReqDelivered_RCodeID]=["+newValue+"]");
                   dbDB.setTimeTrack_ReqDelivered_RCodeID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[TimeTrack_ReqDelivered_RCodeID]=["+newValue+"]");
           dbDB.setTimeTrack_ReqDelivered_RCodeID(newValue);
         }
    }
    public Integer getTimeTrack_ReqDelivered_RCodeID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='TimeTrack_ReqDelivered_RCodeID' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getTimeTrack_ReqDelivered_RCodeID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getTimeTrack_ReqDelivered_RCodeID();
         }
        return myVal;
    }

    public void setTimeTrack_ReqPaidIn_RCodeID(Integer newValue)
    {
        this.setTimeTrack_ReqPaidIn_RCodeID(newValue,this.UserSecurityID);


    }
    public Integer getTimeTrack_ReqPaidIn_RCodeID()
    {
        return this.getTimeTrack_ReqPaidIn_RCodeID(this.UserSecurityID);
    }

    public void setTimeTrack_ReqPaidIn_RCodeID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='TimeTrack_ReqPaidIn_RCodeID' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[TimeTrack_ReqPaidIn_RCodeID]=["+newValue+"]");
                   dbDB.setTimeTrack_ReqPaidIn_RCodeID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[TimeTrack_ReqPaidIn_RCodeID]=["+newValue+"]");
           dbDB.setTimeTrack_ReqPaidIn_RCodeID(newValue);
         }
    }
    public Integer getTimeTrack_ReqPaidIn_RCodeID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='TimeTrack_ReqPaidIn_RCodeID' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getTimeTrack_ReqPaidIn_RCodeID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getTimeTrack_ReqPaidIn_RCodeID();
         }
        return myVal;
    }

    public void setTimeTrack_ReqPaidOut_RCodeID(Integer newValue)
    {
        this.setTimeTrack_ReqPaidOut_RCodeID(newValue,this.UserSecurityID);


    }
    public Integer getTimeTrack_ReqPaidOut_RCodeID()
    {
        return this.getTimeTrack_ReqPaidOut_RCodeID(this.UserSecurityID);
    }

    public void setTimeTrack_ReqPaidOut_RCodeID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='TimeTrack_ReqPaidOut_RCodeID' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[TimeTrack_ReqPaidOut_RCodeID]=["+newValue+"]");
                   dbDB.setTimeTrack_ReqPaidOut_RCodeID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[TimeTrack_ReqPaidOut_RCodeID]=["+newValue+"]");
           dbDB.setTimeTrack_ReqPaidOut_RCodeID(newValue);
         }
    }
    public Integer getTimeTrack_ReqPaidOut_RCodeID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='TimeTrack_ReqPaidOut_RCodeID' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getTimeTrack_ReqPaidOut_RCodeID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getTimeTrack_ReqPaidOut_RCodeID();
         }
        return myVal;
    }

    public void setTimeTrack_ReqApproved_RCodeID(Integer newValue)
    {
        this.setTimeTrack_ReqApproved_RCodeID(newValue,this.UserSecurityID);


    }
    public Integer getTimeTrack_ReqApproved_RCodeID()
    {
        return this.getTimeTrack_ReqApproved_RCodeID(this.UserSecurityID);
    }

    public void setTimeTrack_ReqApproved_RCodeID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='TimeTrack_ReqApproved_RCodeID' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[TimeTrack_ReqApproved_RCodeID]=["+newValue+"]");
                   dbDB.setTimeTrack_ReqApproved_RCodeID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[TimeTrack_ReqApproved_RCodeID]=["+newValue+"]");
           dbDB.setTimeTrack_ReqApproved_RCodeID(newValue);
         }
    }
    public Integer getTimeTrack_ReqApproved_RCodeID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='TimeTrack_ReqApproved_RCodeID' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getTimeTrack_ReqApproved_RCodeID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getTimeTrack_ReqApproved_RCodeID();
         }
        return myVal;
    }

    public void setTimeTrack_ReqApproved(Date newValue)
    {
        this.setTimeTrack_ReqApproved(newValue,this.UserSecurityID);


    }
    public Date getTimeTrack_ReqApproved()
    {
        return this.getTimeTrack_ReqApproved(this.UserSecurityID);
    }

    public void setTimeTrack_ReqApproved(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='TimeTrack_ReqApproved' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[TimeTrack_ReqApproved]=["+newValue+"]");
                   dbDB.setTimeTrack_ReqApproved(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[TimeTrack_ReqApproved]=["+newValue+"]");
           dbDB.setTimeTrack_ReqApproved(newValue);
         }
    }
    public Date getTimeTrack_ReqApproved(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='TimeTrack_ReqApproved' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getTimeTrack_ReqApproved();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getTimeTrack_ReqApproved();
         }
        return myVal;
    }

    public void setTimeTrack_ReqApproved_UserID(Integer newValue)
    {
        this.setTimeTrack_ReqApproved_UserID(newValue,this.UserSecurityID);


    }
    public Integer getTimeTrack_ReqApproved_UserID()
    {
        return this.getTimeTrack_ReqApproved_UserID(this.UserSecurityID);
    }

    public void setTimeTrack_ReqApproved_UserID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='TimeTrack_ReqApproved_UserID' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[TimeTrack_ReqApproved_UserID]=["+newValue+"]");
                   dbDB.setTimeTrack_ReqApproved_UserID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[TimeTrack_ReqApproved_UserID]=["+newValue+"]");
           dbDB.setTimeTrack_ReqApproved_UserID(newValue);
         }
    }
    public Integer getTimeTrack_ReqApproved_UserID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='TimeTrack_ReqApproved_UserID' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getTimeTrack_ReqApproved_UserID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getTimeTrack_ReqApproved_UserID();
         }
        return myVal;
    }

    public void setSentTo_ReqRec_Adm(Date newValue)
    {
        this.setSentTo_ReqRec_Adm(newValue,this.UserSecurityID);


    }
    public Date getSentTo_ReqRec_Adm()
    {
        return this.getSentTo_ReqRec_Adm(this.UserSecurityID);
    }

    public void setSentTo_ReqRec_Adm(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SentTo_ReqRec_Adm' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[SentTo_ReqRec_Adm]=["+newValue+"]");
                   dbDB.setSentTo_ReqRec_Adm(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[SentTo_ReqRec_Adm]=["+newValue+"]");
           dbDB.setSentTo_ReqRec_Adm(newValue);
         }
    }
    public Date getSentTo_ReqRec_Adm(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SentTo_ReqRec_Adm' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getSentTo_ReqRec_Adm();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getSentTo_ReqRec_Adm();
         }
        return myVal;
    }

    public void setSentTo_ReqRec_Adm_CRID(Integer newValue)
    {
        this.setSentTo_ReqRec_Adm_CRID(newValue,this.UserSecurityID);


    }
    public Integer getSentTo_ReqRec_Adm_CRID()
    {
        return this.getSentTo_ReqRec_Adm_CRID(this.UserSecurityID);
    }

    public void setSentTo_ReqRec_Adm_CRID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SentTo_ReqRec_Adm_CRID' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[SentTo_ReqRec_Adm_CRID]=["+newValue+"]");
                   dbDB.setSentTo_ReqRec_Adm_CRID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[SentTo_ReqRec_Adm_CRID]=["+newValue+"]");
           dbDB.setSentTo_ReqRec_Adm_CRID(newValue);
         }
    }
    public Integer getSentTo_ReqRec_Adm_CRID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SentTo_ReqRec_Adm_CRID' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getSentTo_ReqRec_Adm_CRID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getSentTo_ReqRec_Adm_CRID();
         }
        return myVal;
    }

    public void setSentTo_SP_Adm(Date newValue)
    {
        this.setSentTo_SP_Adm(newValue,this.UserSecurityID);


    }
    public Date getSentTo_SP_Adm()
    {
        return this.getSentTo_SP_Adm(this.UserSecurityID);
    }

    public void setSentTo_SP_Adm(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SentTo_SP_Adm' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[SentTo_SP_Adm]=["+newValue+"]");
                   dbDB.setSentTo_SP_Adm(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[SentTo_SP_Adm]=["+newValue+"]");
           dbDB.setSentTo_SP_Adm(newValue);
         }
    }
    public Date getSentTo_SP_Adm(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SentTo_SP_Adm' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getSentTo_SP_Adm();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getSentTo_SP_Adm();
         }
        return myVal;
    }

    public void setSentTo_SP_Adm_CRID(Integer newValue)
    {
        this.setSentTo_SP_Adm_CRID(newValue,this.UserSecurityID);


    }
    public Integer getSentTo_SP_Adm_CRID()
    {
        return this.getSentTo_SP_Adm_CRID(this.UserSecurityID);
    }

    public void setSentTo_SP_Adm_CRID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SentTo_SP_Adm_CRID' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[SentTo_SP_Adm_CRID]=["+newValue+"]");
                   dbDB.setSentTo_SP_Adm_CRID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[SentTo_SP_Adm_CRID]=["+newValue+"]");
           dbDB.setSentTo_SP_Adm_CRID(newValue);
         }
    }
    public Integer getSentTo_SP_Adm_CRID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SentTo_SP_Adm_CRID' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getSentTo_SP_Adm_CRID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getSentTo_SP_Adm_CRID();
         }
        return myVal;
    }

    public void setSentToAdm(Date newValue)
    {
        this.setSentToAdm(newValue,this.UserSecurityID);


    }
    public Date getSentToAdm()
    {
        return this.getSentToAdm(this.UserSecurityID);
    }

    public void setSentToAdm(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SentToAdm' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[SentToAdm]=["+newValue+"]");
                   dbDB.setSentToAdm(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[SentToAdm]=["+newValue+"]");
           dbDB.setSentToAdm(newValue);
         }
    }
    public Date getSentToAdm(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SentToAdm' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getSentToAdm();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getSentToAdm();
         }
        return myVal;
    }

    public void setSentToAdm_CRID(Integer newValue)
    {
        this.setSentToAdm_CRID(newValue,this.UserSecurityID);


    }
    public Integer getSentToAdm_CRID()
    {
        return this.getSentToAdm_CRID(this.UserSecurityID);
    }

    public void setSentToAdm_CRID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SentToAdm_CRID' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[SentToAdm_CRID]=["+newValue+"]");
                   dbDB.setSentToAdm_CRID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[SentToAdm_CRID]=["+newValue+"]");
           dbDB.setSentToAdm_CRID(newValue);
         }
    }
    public Integer getSentToAdm_CRID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SentToAdm_CRID' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getSentToAdm_CRID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getSentToAdm_CRID();
         }
        return myVal;
    }

    public void setTimeTrack_ReqInitialAppointment_RCodeID(Integer newValue)
    {
        this.setTimeTrack_ReqInitialAppointment_RCodeID(newValue,this.UserSecurityID);


    }
    public Integer getTimeTrack_ReqInitialAppointment_RCodeID()
    {
        return this.getTimeTrack_ReqInitialAppointment_RCodeID(this.UserSecurityID);
    }

    public void setTimeTrack_ReqInitialAppointment_RCodeID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='TimeTrack_ReqInitialAppointment_RCodeID' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[TimeTrack_ReqInitialAppointment_RCodeID]=["+newValue+"]");
                   dbDB.setTimeTrack_ReqInitialAppointment_RCodeID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[TimeTrack_ReqInitialAppointment_RCodeID]=["+newValue+"]");
           dbDB.setTimeTrack_ReqInitialAppointment_RCodeID(newValue);
         }
    }
    public Integer getTimeTrack_ReqInitialAppointment_RCodeID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='TimeTrack_ReqInitialAppointment_RCodeID' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getTimeTrack_ReqInitialAppointment_RCodeID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getTimeTrack_ReqInitialAppointment_RCodeID();
         }
        return myVal;
    }

    public void setTimeTrack_ReqInitialAppointment(Date newValue)
    {
        this.setTimeTrack_ReqInitialAppointment(newValue,this.UserSecurityID);


    }
    public Date getTimeTrack_ReqInitialAppointment()
    {
        return this.getTimeTrack_ReqInitialAppointment(this.UserSecurityID);
    }

    public void setTimeTrack_ReqInitialAppointment(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='TimeTrack_ReqInitialAppointment' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[TimeTrack_ReqInitialAppointment]=["+newValue+"]");
                   dbDB.setTimeTrack_ReqInitialAppointment(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[TimeTrack_ReqInitialAppointment]=["+newValue+"]");
           dbDB.setTimeTrack_ReqInitialAppointment(newValue);
         }
    }
    public Date getTimeTrack_ReqInitialAppointment(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='TimeTrack_ReqInitialAppointment' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getTimeTrack_ReqInitialAppointment();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getTimeTrack_ReqInitialAppointment();
         }
        return myVal;
    }

    public void setTimeTrack_ReqInitialAppointment_UserID(Integer newValue)
    {
        this.setTimeTrack_ReqInitialAppointment_UserID(newValue,this.UserSecurityID);


    }
    public Integer getTimeTrack_ReqInitialAppointment_UserID()
    {
        return this.getTimeTrack_ReqInitialAppointment_UserID(this.UserSecurityID);
    }

    public void setTimeTrack_ReqInitialAppointment_UserID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='TimeTrack_ReqInitialAppointment_UserID' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[TimeTrack_ReqInitialAppointment_UserID]=["+newValue+"]");
                   dbDB.setTimeTrack_ReqInitialAppointment_UserID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[TimeTrack_ReqInitialAppointment_UserID]=["+newValue+"]");
           dbDB.setTimeTrack_ReqInitialAppointment_UserID(newValue);
         }
    }
    public Integer getTimeTrack_ReqInitialAppointment_UserID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='TimeTrack_ReqInitialAppointment_UserID' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getTimeTrack_ReqInitialAppointment_UserID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getTimeTrack_ReqInitialAppointment_UserID();
         }
        return myVal;
    }

    public void setTimeTrack_ReqRxReview_RCodeID(Integer newValue)
    {
        this.setTimeTrack_ReqRxReview_RCodeID(newValue,this.UserSecurityID);


    }
    public Integer getTimeTrack_ReqRxReview_RCodeID()
    {
        return this.getTimeTrack_ReqRxReview_RCodeID(this.UserSecurityID);
    }

    public void setTimeTrack_ReqRxReview_RCodeID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='TimeTrack_ReqRxReview_RCodeID' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[TimeTrack_ReqRxReview_RCodeID]=["+newValue+"]");
                   dbDB.setTimeTrack_ReqRxReview_RCodeID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[TimeTrack_ReqRxReview_RCodeID]=["+newValue+"]");
           dbDB.setTimeTrack_ReqRxReview_RCodeID(newValue);
         }
    }
    public Integer getTimeTrack_ReqRxReview_RCodeID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='TimeTrack_ReqRxReview_RCodeID' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getTimeTrack_ReqRxReview_RCodeID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getTimeTrack_ReqRxReview_RCodeID();
         }
        return myVal;
    }

    public void setTimeTrack_ReqRxReview(Date newValue)
    {
        this.setTimeTrack_ReqRxReview(newValue,this.UserSecurityID);


    }
    public Date getTimeTrack_ReqRxReview()
    {
        return this.getTimeTrack_ReqRxReview(this.UserSecurityID);
    }

    public void setTimeTrack_ReqRxReview(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='TimeTrack_ReqRxReview' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[TimeTrack_ReqRxReview]=["+newValue+"]");
                   dbDB.setTimeTrack_ReqRxReview(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[TimeTrack_ReqRxReview]=["+newValue+"]");
           dbDB.setTimeTrack_ReqRxReview(newValue);
         }
    }
    public Date getTimeTrack_ReqRxReview(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='TimeTrack_ReqRxReview' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getTimeTrack_ReqRxReview();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getTimeTrack_ReqRxReview();
         }
        return myVal;
    }

    public void setTimeTrack_ReqRxReview_UserID(Integer newValue)
    {
        this.setTimeTrack_ReqRxReview_UserID(newValue,this.UserSecurityID);


    }
    public Integer getTimeTrack_ReqRxReview_UserID()
    {
        return this.getTimeTrack_ReqRxReview_UserID(this.UserSecurityID);
    }

    public void setTimeTrack_ReqRxReview_UserID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='TimeTrack_ReqRxReview_UserID' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[TimeTrack_ReqRxReview_UserID]=["+newValue+"]");
                   dbDB.setTimeTrack_ReqRxReview_UserID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[TimeTrack_ReqRxReview_UserID]=["+newValue+"]");
           dbDB.setTimeTrack_ReqRxReview_UserID(newValue);
         }
    }
    public Integer getTimeTrack_ReqRxReview_UserID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='TimeTrack_ReqRxReview_UserID' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getTimeTrack_ReqRxReview_UserID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getTimeTrack_ReqRxReview_UserID();
         }
        return myVal;
    }

    public void setTimeTrack_ReqRpReview_RCodeID(Integer newValue)
    {
        this.setTimeTrack_ReqRpReview_RCodeID(newValue,this.UserSecurityID);


    }
    public Integer getTimeTrack_ReqRpReview_RCodeID()
    {
        return this.getTimeTrack_ReqRpReview_RCodeID(this.UserSecurityID);
    }

    public void setTimeTrack_ReqRpReview_RCodeID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='TimeTrack_ReqRpReview_RCodeID' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[TimeTrack_ReqRpReview_RCodeID]=["+newValue+"]");
                   dbDB.setTimeTrack_ReqRpReview_RCodeID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[TimeTrack_ReqRpReview_RCodeID]=["+newValue+"]");
           dbDB.setTimeTrack_ReqRpReview_RCodeID(newValue);
         }
    }
    public Integer getTimeTrack_ReqRpReview_RCodeID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='TimeTrack_ReqRpReview_RCodeID' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getTimeTrack_ReqRpReview_RCodeID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getTimeTrack_ReqRpReview_RCodeID();
         }
        return myVal;
    }

    public void setTimeTrack_ReqRpReview(Date newValue)
    {
        this.setTimeTrack_ReqRpReview(newValue,this.UserSecurityID);


    }
    public Date getTimeTrack_ReqRpReview()
    {
        return this.getTimeTrack_ReqRpReview(this.UserSecurityID);
    }

    public void setTimeTrack_ReqRpReview(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='TimeTrack_ReqRpReview' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[TimeTrack_ReqRpReview]=["+newValue+"]");
                   dbDB.setTimeTrack_ReqRpReview(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[TimeTrack_ReqRpReview]=["+newValue+"]");
           dbDB.setTimeTrack_ReqRpReview(newValue);
         }
    }
    public Date getTimeTrack_ReqRpReview(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='TimeTrack_ReqRpReview' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getTimeTrack_ReqRpReview();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getTimeTrack_ReqRpReview();
         }
        return myVal;
    }

    public void setTimeTrack_ReqRpReview_UserID(Integer newValue)
    {
        this.setTimeTrack_ReqRpReview_UserID(newValue,this.UserSecurityID);


    }
    public Integer getTimeTrack_ReqRpReview_UserID()
    {
        return this.getTimeTrack_ReqRpReview_UserID(this.UserSecurityID);
    }

    public void setTimeTrack_ReqRpReview_UserID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='TimeTrack_ReqRpReview_UserID' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[TimeTrack_ReqRpReview_UserID]=["+newValue+"]");
                   dbDB.setTimeTrack_ReqRpReview_UserID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[TimeTrack_ReqRpReview_UserID]=["+newValue+"]");
           dbDB.setTimeTrack_ReqRpReview_UserID(newValue);
         }
    }
    public Integer getTimeTrack_ReqRpReview_UserID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='TimeTrack_ReqRpReview_UserID' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getTimeTrack_ReqRpReview_UserID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getTimeTrack_ReqRpReview_UserID();
         }
        return myVal;
    }

    public void setNextActionNotes(String newValue)
    {
        this.setNextActionNotes(newValue,this.UserSecurityID);


    }
    public String getNextActionNotes()
    {
        return this.getNextActionNotes(this.UserSecurityID);
    }

    public void setNextActionNotes(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='NextActionNotes' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[NextActionNotes]=["+newValue+"]");
                   dbDB.setNextActionNotes(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[NextActionNotes]=["+newValue+"]");
           dbDB.setNextActionNotes(newValue);
         }
    }
    public String getNextActionNotes(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='NextActionNotes' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getNextActionNotes();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getNextActionNotes();
         }
        return myVal;
    }

    public void setVoidLeakReasonID(Integer newValue)
    {
        this.setVoidLeakReasonID(newValue,this.UserSecurityID);


    }
    public Integer getVoidLeakReasonID()
    {
        return this.getVoidLeakReasonID(this.UserSecurityID);
    }

    public void setVoidLeakReasonID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='VoidLeakReasonID' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[VoidLeakReasonID]=["+newValue+"]");
                   dbDB.setVoidLeakReasonID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[VoidLeakReasonID]=["+newValue+"]");
           dbDB.setVoidLeakReasonID(newValue);
         }
    }
    public Integer getVoidLeakReasonID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='VoidLeakReasonID' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getVoidLeakReasonID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getVoidLeakReasonID();
         }
        return myVal;
    }

    public void setExportPaymentToQB(Date newValue)
    {
        this.setExportPaymentToQB(newValue,this.UserSecurityID);


    }
    public Date getExportPaymentToQB()
    {
        return this.getExportPaymentToQB(this.UserSecurityID);
    }

    public void setExportPaymentToQB(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ExportPaymentToQB' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[ExportPaymentToQB]=["+newValue+"]");
                   dbDB.setExportPaymentToQB(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[ExportPaymentToQB]=["+newValue+"]");
           dbDB.setExportPaymentToQB(newValue);
         }
    }
    public Date getExportPaymentToQB(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ExportPaymentToQB' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getExportPaymentToQB();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getExportPaymentToQB();
         }
        return myVal;
    }

    public void setSentTo_SP_NCM(Date newValue)
    {
        this.setSentTo_SP_NCM(newValue,this.UserSecurityID);


    }
    public Date getSentTo_SP_NCM()
    {
        return this.getSentTo_SP_NCM(this.UserSecurityID);
    }

    public void setSentTo_SP_NCM(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SentTo_SP_NCM' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[SentTo_SP_NCM]=["+newValue+"]");
                   dbDB.setSentTo_SP_NCM(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[SentTo_SP_NCM]=["+newValue+"]");
           dbDB.setSentTo_SP_NCM(newValue);
         }
    }
    public Date getSentTo_SP_NCM(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SentTo_SP_NCM' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getSentTo_SP_NCM();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getSentTo_SP_NCM();
         }
        return myVal;
    }

    public void setSentTo_SP_NCM_CRID(Integer newValue)
    {
        this.setSentTo_SP_NCM_CRID(newValue,this.UserSecurityID);


    }
    public Integer getSentTo_SP_NCM_CRID()
    {
        return this.getSentTo_SP_NCM_CRID(this.UserSecurityID);
    }

    public void setSentTo_SP_NCM_CRID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SentTo_SP_NCM_CRID' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[SentTo_SP_NCM_CRID]=["+newValue+"]");
                   dbDB.setSentTo_SP_NCM_CRID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[SentTo_SP_NCM_CRID]=["+newValue+"]");
           dbDB.setSentTo_SP_NCM_CRID(newValue);
         }
    }
    public Integer getSentTo_SP_NCM_CRID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SentTo_SP_NCM_CRID' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getSentTo_SP_NCM_CRID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getSentTo_SP_NCM_CRID();
         }
        return myVal;
    }

    public void setSentTo_RP_NCM(Date newValue)
    {
        this.setSentTo_RP_NCM(newValue,this.UserSecurityID);


    }
    public Date getSentTo_RP_NCM()
    {
        return this.getSentTo_RP_NCM(this.UserSecurityID);
    }

    public void setSentTo_RP_NCM(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SentTo_RP_NCM' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[SentTo_RP_NCM]=["+newValue+"]");
                   dbDB.setSentTo_RP_NCM(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[SentTo_RP_NCM]=["+newValue+"]");
           dbDB.setSentTo_RP_NCM(newValue);
         }
    }
    public Date getSentTo_RP_NCM(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SentTo_RP_NCM' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getSentTo_RP_NCM();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getSentTo_RP_NCM();
         }
        return myVal;
    }

    public void setSentTo_RP_NCM_CRID(Integer newValue)
    {
        this.setSentTo_RP_NCM_CRID(newValue,this.UserSecurityID);


    }
    public Integer getSentTo_RP_NCM_CRID()
    {
        return this.getSentTo_RP_NCM_CRID(this.UserSecurityID);
    }

    public void setSentTo_RP_NCM_CRID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SentTo_RP_NCM_CRID' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[SentTo_RP_NCM_CRID]=["+newValue+"]");
                   dbDB.setSentTo_RP_NCM_CRID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[SentTo_RP_NCM_CRID]=["+newValue+"]");
           dbDB.setSentTo_RP_NCM_CRID(newValue);
         }
    }
    public Integer getSentTo_RP_NCM_CRID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SentTo_RP_NCM_CRID' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getSentTo_RP_NCM_CRID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getSentTo_RP_NCM_CRID();
         }
        return myVal;
    }

    public void setSentTo_ReqRec_NCM(Date newValue)
    {
        this.setSentTo_ReqRec_NCM(newValue,this.UserSecurityID);


    }
    public Date getSentTo_ReqRec_NCM()
    {
        return this.getSentTo_ReqRec_NCM(this.UserSecurityID);
    }

    public void setSentTo_ReqRec_NCM(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SentTo_ReqRec_NCM' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[SentTo_ReqRec_NCM]=["+newValue+"]");
                   dbDB.setSentTo_ReqRec_NCM(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[SentTo_ReqRec_NCM]=["+newValue+"]");
           dbDB.setSentTo_ReqRec_NCM(newValue);
         }
    }
    public Date getSentTo_ReqRec_NCM(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SentTo_ReqRec_NCM' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getSentTo_ReqRec_NCM();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getSentTo_ReqRec_NCM();
         }
        return myVal;
    }

    public void setSentTo_ReqRec_NCM_CRID(Integer newValue)
    {
        this.setSentTo_ReqRec_NCM_CRID(newValue,this.UserSecurityID);


    }
    public Integer getSentTo_ReqRec_NCM_CRID()
    {
        return this.getSentTo_ReqRec_NCM_CRID(this.UserSecurityID);
    }

    public void setSentTo_ReqRec_NCM_CRID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SentTo_ReqRec_NCM_CRID' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[SentTo_ReqRec_NCM_CRID]=["+newValue+"]");
                   dbDB.setSentTo_ReqRec_NCM_CRID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[SentTo_ReqRec_NCM_CRID]=["+newValue+"]");
           dbDB.setSentTo_ReqRec_NCM_CRID(newValue);
         }
    }
    public Integer getSentTo_ReqRec_NCM_CRID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SentTo_ReqRec_NCM_CRID' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getSentTo_ReqRec_NCM_CRID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getSentTo_ReqRec_NCM_CRID();
         }
        return myVal;
    }

    public void setisSTAT(Integer newValue)
    {
        this.setisSTAT(newValue,this.UserSecurityID);


    }
    public Integer getisSTAT()
    {
        return this.getisSTAT(this.UserSecurityID);
    }

    public void setisSTAT(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='isSTAT' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[isSTAT]=["+newValue+"]");
                   dbDB.setisSTAT(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[isSTAT]=["+newValue+"]");
           dbDB.setisSTAT(newValue);
         }
    }
    public Integer getisSTAT(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='isSTAT' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getisSTAT();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getisSTAT();
         }
        return myVal;
    }

    public void setRequiresFilms(Integer newValue)
    {
        this.setRequiresFilms(newValue,this.UserSecurityID);


    }
    public Integer getRequiresFilms()
    {
        return this.getRequiresFilms(this.UserSecurityID);
    }

    public void setRequiresFilms(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='RequiresFilms' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[RequiresFilms]=["+newValue+"]");
                   dbDB.setRequiresFilms(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[RequiresFilms]=["+newValue+"]");
           dbDB.setRequiresFilms(newValue);
         }
    }
    public Integer getRequiresFilms(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='RequiresFilms' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getRequiresFilms();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getRequiresFilms();
         }
        return myVal;
    }

    public void setHasBeenRescheduled(Integer newValue)
    {
        this.setHasBeenRescheduled(newValue,this.UserSecurityID);


    }
    public Integer getHasBeenRescheduled()
    {
        return this.getHasBeenRescheduled(this.UserSecurityID);
    }

    public void setHasBeenRescheduled(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='HasBeenRescheduled' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[HasBeenRescheduled]=["+newValue+"]");
                   dbDB.setHasBeenRescheduled(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[HasBeenRescheduled]=["+newValue+"]");
           dbDB.setHasBeenRescheduled(newValue);
         }
    }
    public Integer getHasBeenRescheduled(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='HasBeenRescheduled' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getHasBeenRescheduled();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getHasBeenRescheduled();
         }
        return myVal;
    }

    public void setSentTo_SP_Adm2(Date newValue)
    {
        this.setSentTo_SP_Adm2(newValue,this.UserSecurityID);


    }
    public Date getSentTo_SP_Adm2()
    {
        return this.getSentTo_SP_Adm2(this.UserSecurityID);
    }

    public void setSentTo_SP_Adm2(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SentTo_SP_Adm2' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[SentTo_SP_Adm2]=["+newValue+"]");
                   dbDB.setSentTo_SP_Adm2(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[SentTo_SP_Adm2]=["+newValue+"]");
           dbDB.setSentTo_SP_Adm2(newValue);
         }
    }
    public Date getSentTo_SP_Adm2(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SentTo_SP_Adm2' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getSentTo_SP_Adm2();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getSentTo_SP_Adm2();
         }
        return myVal;
    }

    public void setSentTo_SP_Adm2_CRID(Integer newValue)
    {
        this.setSentTo_SP_Adm2_CRID(newValue,this.UserSecurityID);


    }
    public Integer getSentTo_SP_Adm2_CRID()
    {
        return this.getSentTo_SP_Adm2_CRID(this.UserSecurityID);
    }

    public void setSentTo_SP_Adm2_CRID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SentTo_SP_Adm2_CRID' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[SentTo_SP_Adm2_CRID]=["+newValue+"]");
                   dbDB.setSentTo_SP_Adm2_CRID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[SentTo_SP_Adm2_CRID]=["+newValue+"]");
           dbDB.setSentTo_SP_Adm2_CRID(newValue);
         }
    }
    public Integer getSentTo_SP_Adm2_CRID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SentTo_SP_Adm2_CRID' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getSentTo_SP_Adm2_CRID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getSentTo_SP_Adm2_CRID();
         }
        return myVal;
    }

    public void setSentTo_RP_Adm2(Date newValue)
    {
        this.setSentTo_RP_Adm2(newValue,this.UserSecurityID);


    }
    public Date getSentTo_RP_Adm2()
    {
        return this.getSentTo_RP_Adm2(this.UserSecurityID);
    }

    public void setSentTo_RP_Adm2(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SentTo_RP_Adm2' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[SentTo_RP_Adm2]=["+newValue+"]");
                   dbDB.setSentTo_RP_Adm2(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[SentTo_RP_Adm2]=["+newValue+"]");
           dbDB.setSentTo_RP_Adm2(newValue);
         }
    }
    public Date getSentTo_RP_Adm2(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SentTo_RP_Adm2' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getSentTo_RP_Adm2();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getSentTo_RP_Adm2();
         }
        return myVal;
    }

    public void setSentTo_RP_Adm2_CRID(Integer newValue)
    {
        this.setSentTo_RP_Adm2_CRID(newValue,this.UserSecurityID);


    }
    public Integer getSentTo_RP_Adm2_CRID()
    {
        return this.getSentTo_RP_Adm2_CRID(this.UserSecurityID);
    }

    public void setSentTo_RP_Adm2_CRID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SentTo_RP_Adm2_CRID' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[SentTo_RP_Adm2_CRID]=["+newValue+"]");
                   dbDB.setSentTo_RP_Adm2_CRID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[SentTo_RP_Adm2_CRID]=["+newValue+"]");
           dbDB.setSentTo_RP_Adm2_CRID(newValue);
         }
    }
    public Integer getSentTo_RP_Adm2_CRID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SentTo_RP_Adm2_CRID' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getSentTo_RP_Adm2_CRID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getSentTo_RP_Adm2_CRID();
         }
        return myVal;
    }

    public void setSentTo_ReqRec_Adm2(Date newValue)
    {
        this.setSentTo_ReqRec_Adm2(newValue,this.UserSecurityID);


    }
    public Date getSentTo_ReqRec_Adm2()
    {
        return this.getSentTo_ReqRec_Adm2(this.UserSecurityID);
    }

    public void setSentTo_ReqRec_Adm2(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SentTo_ReqRec_Adm2' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[SentTo_ReqRec_Adm2]=["+newValue+"]");
                   dbDB.setSentTo_ReqRec_Adm2(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[SentTo_ReqRec_Adm2]=["+newValue+"]");
           dbDB.setSentTo_ReqRec_Adm2(newValue);
         }
    }
    public Date getSentTo_ReqRec_Adm2(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SentTo_ReqRec_Adm2' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getSentTo_ReqRec_Adm2();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getSentTo_ReqRec_Adm2();
         }
        return myVal;
    }

    public void setSentTo_ReqRec_Adm2_CRID(Integer newValue)
    {
        this.setSentTo_ReqRec_Adm2_CRID(newValue,this.UserSecurityID);


    }
    public Integer getSentTo_ReqRec_Adm2_CRID()
    {
        return this.getSentTo_ReqRec_Adm2_CRID(this.UserSecurityID);
    }

    public void setSentTo_ReqRec_Adm2_CRID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SentTo_ReqRec_Adm2_CRID' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[SentTo_ReqRec_Adm2_CRID]=["+newValue+"]");
                   dbDB.setSentTo_ReqRec_Adm2_CRID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[SentTo_ReqRec_Adm2_CRID]=["+newValue+"]");
           dbDB.setSentTo_ReqRec_Adm2_CRID(newValue);
         }
    }
    public Integer getSentTo_ReqRec_Adm2_CRID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SentTo_ReqRec_Adm2_CRID' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getSentTo_ReqRec_Adm2_CRID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getSentTo_ReqRec_Adm2_CRID();
         }
        return myVal;
    }

    public void setSentTo_SP_Adm3(Date newValue)
    {
        this.setSentTo_SP_Adm3(newValue,this.UserSecurityID);


    }
    public Date getSentTo_SP_Adm3()
    {
        return this.getSentTo_SP_Adm3(this.UserSecurityID);
    }

    public void setSentTo_SP_Adm3(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SentTo_SP_Adm3' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[SentTo_SP_Adm3]=["+newValue+"]");
                   dbDB.setSentTo_SP_Adm3(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[SentTo_SP_Adm3]=["+newValue+"]");
           dbDB.setSentTo_SP_Adm3(newValue);
         }
    }
    public Date getSentTo_SP_Adm3(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SentTo_SP_Adm3' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getSentTo_SP_Adm3();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getSentTo_SP_Adm3();
         }
        return myVal;
    }

    public void setSentTo_SP_Adm3_CRID(Integer newValue)
    {
        this.setSentTo_SP_Adm3_CRID(newValue,this.UserSecurityID);


    }
    public Integer getSentTo_SP_Adm3_CRID()
    {
        return this.getSentTo_SP_Adm3_CRID(this.UserSecurityID);
    }

    public void setSentTo_SP_Adm3_CRID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SentTo_SP_Adm3_CRID' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[SentTo_SP_Adm3_CRID]=["+newValue+"]");
                   dbDB.setSentTo_SP_Adm3_CRID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[SentTo_SP_Adm3_CRID]=["+newValue+"]");
           dbDB.setSentTo_SP_Adm3_CRID(newValue);
         }
    }
    public Integer getSentTo_SP_Adm3_CRID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SentTo_SP_Adm3_CRID' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getSentTo_SP_Adm3_CRID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getSentTo_SP_Adm3_CRID();
         }
        return myVal;
    }

    public void setSentTo_RP_Adm3(Date newValue)
    {
        this.setSentTo_RP_Adm3(newValue,this.UserSecurityID);


    }
    public Date getSentTo_RP_Adm3()
    {
        return this.getSentTo_RP_Adm3(this.UserSecurityID);
    }

    public void setSentTo_RP_Adm3(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SentTo_RP_Adm3' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[SentTo_RP_Adm3]=["+newValue+"]");
                   dbDB.setSentTo_RP_Adm3(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[SentTo_RP_Adm3]=["+newValue+"]");
           dbDB.setSentTo_RP_Adm3(newValue);
         }
    }
    public Date getSentTo_RP_Adm3(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SentTo_RP_Adm3' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getSentTo_RP_Adm3();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getSentTo_RP_Adm3();
         }
        return myVal;
    }

    public void setSentTo_RP_Adm3_CRID(Integer newValue)
    {
        this.setSentTo_RP_Adm3_CRID(newValue,this.UserSecurityID);


    }
    public Integer getSentTo_RP_Adm3_CRID()
    {
        return this.getSentTo_RP_Adm3_CRID(this.UserSecurityID);
    }

    public void setSentTo_RP_Adm3_CRID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SentTo_RP_Adm3_CRID' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[SentTo_RP_Adm3_CRID]=["+newValue+"]");
                   dbDB.setSentTo_RP_Adm3_CRID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[SentTo_RP_Adm3_CRID]=["+newValue+"]");
           dbDB.setSentTo_RP_Adm3_CRID(newValue);
         }
    }
    public Integer getSentTo_RP_Adm3_CRID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SentTo_RP_Adm3_CRID' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getSentTo_RP_Adm3_CRID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getSentTo_RP_Adm3_CRID();
         }
        return myVal;
    }

    public void setSentTo_ReqRec_Adm3(Date newValue)
    {
        this.setSentTo_ReqRec_Adm3(newValue,this.UserSecurityID);


    }
    public Date getSentTo_ReqRec_Adm3()
    {
        return this.getSentTo_ReqRec_Adm3(this.UserSecurityID);
    }

    public void setSentTo_ReqRec_Adm3(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SentTo_ReqRec_Adm3' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[SentTo_ReqRec_Adm3]=["+newValue+"]");
                   dbDB.setSentTo_ReqRec_Adm3(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[SentTo_ReqRec_Adm3]=["+newValue+"]");
           dbDB.setSentTo_ReqRec_Adm3(newValue);
         }
    }
    public Date getSentTo_ReqRec_Adm3(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SentTo_ReqRec_Adm3' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getSentTo_ReqRec_Adm3();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getSentTo_ReqRec_Adm3();
         }
        return myVal;
    }

    public void setSentTo_ReqRec_Adm3_CRID(Integer newValue)
    {
        this.setSentTo_ReqRec_Adm3_CRID(newValue,this.UserSecurityID);


    }
    public Integer getSentTo_ReqRec_Adm3_CRID()
    {
        return this.getSentTo_ReqRec_Adm3_CRID(this.UserSecurityID);
    }

    public void setSentTo_ReqRec_Adm3_CRID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SentTo_ReqRec_Adm3_CRID' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[SentTo_ReqRec_Adm3_CRID]=["+newValue+"]");
                   dbDB.setSentTo_ReqRec_Adm3_CRID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[SentTo_ReqRec_Adm3_CRID]=["+newValue+"]");
           dbDB.setSentTo_ReqRec_Adm3_CRID(newValue);
         }
    }
    public Integer getSentTo_ReqRec_Adm3_CRID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SentTo_ReqRec_Adm3_CRID' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getSentTo_ReqRec_Adm3_CRID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getSentTo_ReqRec_Adm3_CRID();
         }
        return myVal;
    }

    public void setSentTo_SP_Adm4(Date newValue)
    {
        this.setSentTo_SP_Adm4(newValue,this.UserSecurityID);


    }
    public Date getSentTo_SP_Adm4()
    {
        return this.getSentTo_SP_Adm4(this.UserSecurityID);
    }

    public void setSentTo_SP_Adm4(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SentTo_SP_Adm4' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[SentTo_SP_Adm4]=["+newValue+"]");
                   dbDB.setSentTo_SP_Adm4(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[SentTo_SP_Adm4]=["+newValue+"]");
           dbDB.setSentTo_SP_Adm4(newValue);
         }
    }
    public Date getSentTo_SP_Adm4(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SentTo_SP_Adm4' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getSentTo_SP_Adm4();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getSentTo_SP_Adm4();
         }
        return myVal;
    }

    public void setSentTo_SP_Adm4_CRID(Integer newValue)
    {
        this.setSentTo_SP_Adm4_CRID(newValue,this.UserSecurityID);


    }
    public Integer getSentTo_SP_Adm4_CRID()
    {
        return this.getSentTo_SP_Adm4_CRID(this.UserSecurityID);
    }

    public void setSentTo_SP_Adm4_CRID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SentTo_SP_Adm4_CRID' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[SentTo_SP_Adm4_CRID]=["+newValue+"]");
                   dbDB.setSentTo_SP_Adm4_CRID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[SentTo_SP_Adm4_CRID]=["+newValue+"]");
           dbDB.setSentTo_SP_Adm4_CRID(newValue);
         }
    }
    public Integer getSentTo_SP_Adm4_CRID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SentTo_SP_Adm4_CRID' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getSentTo_SP_Adm4_CRID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getSentTo_SP_Adm4_CRID();
         }
        return myVal;
    }

    public void setSentTo_RP_Adm4(Date newValue)
    {
        this.setSentTo_RP_Adm4(newValue,this.UserSecurityID);


    }
    public Date getSentTo_RP_Adm4()
    {
        return this.getSentTo_RP_Adm4(this.UserSecurityID);
    }

    public void setSentTo_RP_Adm4(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SentTo_RP_Adm4' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[SentTo_RP_Adm4]=["+newValue+"]");
                   dbDB.setSentTo_RP_Adm4(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[SentTo_RP_Adm4]=["+newValue+"]");
           dbDB.setSentTo_RP_Adm4(newValue);
         }
    }
    public Date getSentTo_RP_Adm4(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SentTo_RP_Adm4' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getSentTo_RP_Adm4();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getSentTo_RP_Adm4();
         }
        return myVal;
    }

    public void setSentTo_RP_Adm4_CRID(Integer newValue)
    {
        this.setSentTo_RP_Adm4_CRID(newValue,this.UserSecurityID);


    }
    public Integer getSentTo_RP_Adm4_CRID()
    {
        return this.getSentTo_RP_Adm4_CRID(this.UserSecurityID);
    }

    public void setSentTo_RP_Adm4_CRID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SentTo_RP_Adm4_CRID' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[SentTo_RP_Adm4_CRID]=["+newValue+"]");
                   dbDB.setSentTo_RP_Adm4_CRID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[SentTo_RP_Adm4_CRID]=["+newValue+"]");
           dbDB.setSentTo_RP_Adm4_CRID(newValue);
         }
    }
    public Integer getSentTo_RP_Adm4_CRID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SentTo_RP_Adm4_CRID' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getSentTo_RP_Adm4_CRID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getSentTo_RP_Adm4_CRID();
         }
        return myVal;
    }

    public void setSentTo_ReqRec_Adm4(Date newValue)
    {
        this.setSentTo_ReqRec_Adm4(newValue,this.UserSecurityID);


    }
    public Date getSentTo_ReqRec_Adm4()
    {
        return this.getSentTo_ReqRec_Adm4(this.UserSecurityID);
    }

    public void setSentTo_ReqRec_Adm4(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SentTo_ReqRec_Adm4' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[SentTo_ReqRec_Adm4]=["+newValue+"]");
                   dbDB.setSentTo_ReqRec_Adm4(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[SentTo_ReqRec_Adm4]=["+newValue+"]");
           dbDB.setSentTo_ReqRec_Adm4(newValue);
         }
    }
    public Date getSentTo_ReqRec_Adm4(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SentTo_ReqRec_Adm4' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getSentTo_ReqRec_Adm4();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getSentTo_ReqRec_Adm4();
         }
        return myVal;
    }

    public void setSentTo_ReqRec_Adm4_CRID(Integer newValue)
    {
        this.setSentTo_ReqRec_Adm4_CRID(newValue,this.UserSecurityID);


    }
    public Integer getSentTo_ReqRec_Adm4_CRID()
    {
        return this.getSentTo_ReqRec_Adm4_CRID(this.UserSecurityID);
    }

    public void setSentTo_ReqRec_Adm4_CRID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SentTo_ReqRec_Adm4_CRID' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[SentTo_ReqRec_Adm4_CRID]=["+newValue+"]");
                   dbDB.setSentTo_ReqRec_Adm4_CRID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[SentTo_ReqRec_Adm4_CRID]=["+newValue+"]");
           dbDB.setSentTo_ReqRec_Adm4_CRID(newValue);
         }
    }
    public Integer getSentTo_ReqRec_Adm4_CRID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SentTo_ReqRec_Adm4_CRID' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getSentTo_ReqRec_Adm4_CRID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getSentTo_ReqRec_Adm4_CRID();
         }
        return myVal;
    }

    public void setRequiresAgeInjury(Integer newValue)
    {
        this.setRequiresAgeInjury(newValue,this.UserSecurityID);


    }
    public Integer getRequiresAgeInjury()
    {
        return this.getRequiresAgeInjury(this.UserSecurityID);
    }

    public void setRequiresAgeInjury(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='RequiresAgeInjury' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[RequiresAgeInjury]=["+newValue+"]");
                   dbDB.setRequiresAgeInjury(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[RequiresAgeInjury]=["+newValue+"]");
           dbDB.setRequiresAgeInjury(newValue);
         }
    }
    public Integer getRequiresAgeInjury(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='RequiresAgeInjury' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getRequiresAgeInjury();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getRequiresAgeInjury();
         }
        return myVal;
    }

    public void setisCourtesy(Integer newValue)
    {
        this.setisCourtesy(newValue,this.UserSecurityID);


    }
    public Integer getisCourtesy()
    {
        return this.getisCourtesy(this.UserSecurityID);
    }

    public void setisCourtesy(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='isCourtesy' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[isCourtesy]=["+newValue+"]");
                   dbDB.setisCourtesy(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[isCourtesy]=["+newValue+"]");
           dbDB.setisCourtesy(newValue);
         }
    }
    public Integer getisCourtesy(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='isCourtesy' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getisCourtesy();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getisCourtesy();
         }
        return myVal;
    }

    public void setBillingHCFA_ToPayer_FileID(Integer newValue)
    {
        this.setBillingHCFA_ToPayer_FileID(newValue,this.UserSecurityID);


    }
    public Integer getBillingHCFA_ToPayer_FileID()
    {
        return this.getBillingHCFA_ToPayer_FileID(this.UserSecurityID);
    }

    public void setBillingHCFA_ToPayer_FileID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='BillingHCFA_ToPayer_FileID' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[BillingHCFA_ToPayer_FileID]=["+newValue+"]");
                   dbDB.setBillingHCFA_ToPayer_FileID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[BillingHCFA_ToPayer_FileID]=["+newValue+"]");
           dbDB.setBillingHCFA_ToPayer_FileID(newValue);
         }
    }
    public Integer getBillingHCFA_ToPayer_FileID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='BillingHCFA_ToPayer_FileID' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getBillingHCFA_ToPayer_FileID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getBillingHCFA_ToPayer_FileID();
         }
        return myVal;
    }

    public void setBillingHCFA_FromProvider_FileID(Integer newValue)
    {
        this.setBillingHCFA_FromProvider_FileID(newValue,this.UserSecurityID);


    }
    public Integer getBillingHCFA_FromProvider_FileID()
    {
        return this.getBillingHCFA_FromProvider_FileID(this.UserSecurityID);
    }

    public void setBillingHCFA_FromProvider_FileID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='BillingHCFA_FromProvider_FileID' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[BillingHCFA_FromProvider_FileID]=["+newValue+"]");
                   dbDB.setBillingHCFA_FromProvider_FileID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[BillingHCFA_FromProvider_FileID]=["+newValue+"]");
           dbDB.setBillingHCFA_FromProvider_FileID(newValue);
         }
    }
    public Integer getBillingHCFA_FromProvider_FileID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='BillingHCFA_FromProvider_FileID' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getBillingHCFA_FromProvider_FileID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getBillingHCFA_FromProvider_FileID();
         }
        return myVal;
    }

    public void setisRetro(Integer newValue)
    {
        this.setisRetro(newValue,this.UserSecurityID);


    }
    public Integer getisRetro()
    {
        return this.getisRetro(this.UserSecurityID);
    }

    public void setisRetro(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='isRetro' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[isRetro]=["+newValue+"]");
                   dbDB.setisRetro(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[isRetro]=["+newValue+"]");
           dbDB.setisRetro(newValue);
         }
    }
    public Integer getisRetro(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='isRetro' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getisRetro();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getisRetro();
         }
        return myVal;
    }

    public void setExportAPToQB(Date newValue)
    {
        this.setExportAPToQB(newValue,this.UserSecurityID);


    }
    public Date getExportAPToQB()
    {
        return this.getExportAPToQB(this.UserSecurityID);
    }

    public void setExportAPToQB(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ExportAPToQB' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[ExportAPToQB]=["+newValue+"]");
                   dbDB.setExportAPToQB(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[ExportAPToQB]=["+newValue+"]");
           dbDB.setExportAPToQB(newValue);
         }
    }
    public Date getExportAPToQB(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ExportAPToQB' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getExportAPToQB();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getExportAPToQB();
         }
        return myVal;
    }

    public void setExportARToQB(Date newValue)
    {
        this.setExportARToQB(newValue,this.UserSecurityID);


    }
    public Date getExportARToQB()
    {
        return this.getExportARToQB(this.UserSecurityID);
    }

    public void setExportARToQB(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ExportARToQB' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[ExportARToQB]=["+newValue+"]");
                   dbDB.setExportARToQB(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[ExportARToQB]=["+newValue+"]");
           dbDB.setExportARToQB(newValue);
         }
    }
    public Date getExportARToQB(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ExportARToQB' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getExportARToQB();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getExportARToQB();
         }
        return myVal;
    }

    public void setAmountBilledByProvider(Double newValue)
    {
        this.setAmountBilledByProvider(newValue,this.UserSecurityID);


    }
    public Double getAmountBilledByProvider()
    {
        return this.getAmountBilledByProvider(this.UserSecurityID);
    }

    public void setAmountBilledByProvider(Double newValue, Integer GroupRefID)
    {
        Double myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AmountBilledByProvider' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[AmountBilledByProvider]=["+newValue+"]");
                   dbDB.setAmountBilledByProvider(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[AmountBilledByProvider]=["+newValue+"]");
           dbDB.setAmountBilledByProvider(newValue);
         }
    }
    public Double getAmountBilledByProvider(Integer GroupRefID)
    {
        Double myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AmountBilledByProvider' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getAmountBilledByProvider();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getAmountBilledByProvider();
         }
        return myVal;
    }

    public void setSeeNetDev_Flagged(Integer newValue)
    {
        this.setSeeNetDev_Flagged(newValue,this.UserSecurityID);


    }
    public Integer getSeeNetDev_Flagged()
    {
        return this.getSeeNetDev_Flagged(this.UserSecurityID);
    }

    public void setSeeNetDev_Flagged(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SeeNetDev_Flagged' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[SeeNetDev_Flagged]=["+newValue+"]");
                   dbDB.setSeeNetDev_Flagged(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[SeeNetDev_Flagged]=["+newValue+"]");
           dbDB.setSeeNetDev_Flagged(newValue);
         }
    }
    public Integer getSeeNetDev_Flagged(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SeeNetDev_Flagged' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getSeeNetDev_Flagged();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getSeeNetDev_Flagged();
         }
        return myVal;
    }

    public void setSeeNetDev_ReqSentToNetDev(Date newValue)
    {
        this.setSeeNetDev_ReqSentToNetDev(newValue,this.UserSecurityID);


    }
    public Date getSeeNetDev_ReqSentToNetDev()
    {
        return this.getSeeNetDev_ReqSentToNetDev(this.UserSecurityID);
    }

    public void setSeeNetDev_ReqSentToNetDev(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SeeNetDev_ReqSentToNetDev' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[SeeNetDev_ReqSentToNetDev]=["+newValue+"]");
                   dbDB.setSeeNetDev_ReqSentToNetDev(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[SeeNetDev_ReqSentToNetDev]=["+newValue+"]");
           dbDB.setSeeNetDev_ReqSentToNetDev(newValue);
         }
    }
    public Date getSeeNetDev_ReqSentToNetDev(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SeeNetDev_ReqSentToNetDev' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getSeeNetDev_ReqSentToNetDev();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getSeeNetDev_ReqSentToNetDev();
         }
        return myVal;
    }

    public void setSeeNetDev_ReqSentToNetDev_UserID(Integer newValue)
    {
        this.setSeeNetDev_ReqSentToNetDev_UserID(newValue,this.UserSecurityID);


    }
    public Integer getSeeNetDev_ReqSentToNetDev_UserID()
    {
        return this.getSeeNetDev_ReqSentToNetDev_UserID(this.UserSecurityID);
    }

    public void setSeeNetDev_ReqSentToNetDev_UserID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SeeNetDev_ReqSentToNetDev_UserID' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[SeeNetDev_ReqSentToNetDev_UserID]=["+newValue+"]");
                   dbDB.setSeeNetDev_ReqSentToNetDev_UserID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[SeeNetDev_ReqSentToNetDev_UserID]=["+newValue+"]");
           dbDB.setSeeNetDev_ReqSentToNetDev_UserID(newValue);
         }
    }
    public Integer getSeeNetDev_ReqSentToNetDev_UserID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SeeNetDev_ReqSentToNetDev_UserID' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getSeeNetDev_ReqSentToNetDev_UserID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getSeeNetDev_ReqSentToNetDev_UserID();
         }
        return myVal;
    }

    public void setSeeNetDev_Waiting(Integer newValue)
    {
        this.setSeeNetDev_Waiting(newValue,this.UserSecurityID);


    }
    public Integer getSeeNetDev_Waiting()
    {
        return this.getSeeNetDev_Waiting(this.UserSecurityID);
    }

    public void setSeeNetDev_Waiting(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SeeNetDev_Waiting' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[SeeNetDev_Waiting]=["+newValue+"]");
                   dbDB.setSeeNetDev_Waiting(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[SeeNetDev_Waiting]=["+newValue+"]");
           dbDB.setSeeNetDev_Waiting(newValue);
         }
    }
    public Integer getSeeNetDev_Waiting(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SeeNetDev_Waiting' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getSeeNetDev_Waiting();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getSeeNetDev_Waiting();
         }
        return myVal;
    }

    public void setPatientAvailability(String newValue)
    {
        this.setPatientAvailability(newValue,this.UserSecurityID);


    }
    public String getPatientAvailability()
    {
        return this.getPatientAvailability(this.UserSecurityID);
    }

    public void setPatientAvailability(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientAvailability' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[PatientAvailability]=["+newValue+"]");
                   dbDB.setPatientAvailability(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[PatientAvailability]=["+newValue+"]");
           dbDB.setPatientAvailability(newValue);
         }
    }
    public String getPatientAvailability(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientAvailability' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPatientAvailability();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPatientAvailability();
         }
        return myVal;
    }

    public void setRequiresOnlineImage(Integer newValue)
    {
        this.setRequiresOnlineImage(newValue,this.UserSecurityID);


    }
    public Integer getRequiresOnlineImage()
    {
        return this.getRequiresOnlineImage(this.UserSecurityID);
    }

    public void setRequiresOnlineImage(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='RequiresOnlineImage' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[RequiresOnlineImage]=["+newValue+"]");
                   dbDB.setRequiresOnlineImage(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[RequiresOnlineImage]=["+newValue+"]");
           dbDB.setRequiresOnlineImage(newValue);
         }
    }
    public Integer getRequiresOnlineImage(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='RequiresOnlineImage' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getRequiresOnlineImage();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getRequiresOnlineImage();
         }
        return myVal;
    }

    public void setPricing_Structure(Integer newValue)
    {
        this.setPricing_Structure(newValue,this.UserSecurityID);


    }
    public Integer getPricing_Structure()
    {
        return this.getPricing_Structure(this.UserSecurityID);
    }

    public void setPricing_Structure(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Pricing_Structure' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[Pricing_Structure]=["+newValue+"]");
                   dbDB.setPricing_Structure(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[Pricing_Structure]=["+newValue+"]");
           dbDB.setPricing_Structure(newValue);
         }
    }
    public Integer getPricing_Structure(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Pricing_Structure' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPricing_Structure();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPricing_Structure();
         }
        return myVal;
    }

    public void setSeeNetDev_SelectedPracticeID(Integer newValue)
    {
        this.setSeeNetDev_SelectedPracticeID(newValue,this.UserSecurityID);


    }
    public Integer getSeeNetDev_SelectedPracticeID()
    {
        return this.getSeeNetDev_SelectedPracticeID(this.UserSecurityID);
    }

    public void setSeeNetDev_SelectedPracticeID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SeeNetDev_SelectedPracticeID' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[SeeNetDev_SelectedPracticeID]=["+newValue+"]");
                   dbDB.setSeeNetDev_SelectedPracticeID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[SeeNetDev_SelectedPracticeID]=["+newValue+"]");
           dbDB.setSeeNetDev_SelectedPracticeID(newValue);
         }
    }
    public Integer getSeeNetDev_SelectedPracticeID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SeeNetDev_SelectedPracticeID' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getSeeNetDev_SelectedPracticeID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getSeeNetDev_SelectedPracticeID();
         }
        return myVal;
    }

    public void setActionDate_OnlineImageRequest(Date newValue)
    {
        this.setActionDate_OnlineImageRequest(newValue,this.UserSecurityID);


    }
    public Date getActionDate_OnlineImageRequest()
    {
        return this.getActionDate_OnlineImageRequest(this.UserSecurityID);
    }

    public void setActionDate_OnlineImageRequest(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ActionDate_OnlineImageRequest' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[ActionDate_OnlineImageRequest]=["+newValue+"]");
                   dbDB.setActionDate_OnlineImageRequest(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[ActionDate_OnlineImageRequest]=["+newValue+"]");
           dbDB.setActionDate_OnlineImageRequest(newValue);
         }
    }
    public Date getActionDate_OnlineImageRequest(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ActionDate_OnlineImageRequest' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getActionDate_OnlineImageRequest();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getActionDate_OnlineImageRequest();
         }
        return myVal;
    }

    public void setActionDate_ReportRequest(Date newValue)
    {
        this.setActionDate_ReportRequest(newValue,this.UserSecurityID);


    }
    public Date getActionDate_ReportRequest()
    {
        return this.getActionDate_ReportRequest(this.UserSecurityID);
    }

    public void setActionDate_ReportRequest(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ActionDate_ReportRequest' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[ActionDate_ReportRequest]=["+newValue+"]");
                   dbDB.setActionDate_ReportRequest(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[ActionDate_ReportRequest]=["+newValue+"]");
           dbDB.setActionDate_ReportRequest(newValue);
         }
    }
    public Date getActionDate_ReportRequest(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ActionDate_ReportRequest' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getActionDate_ReportRequest();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getActionDate_ReportRequest();
         }
        return myVal;
    }

    public void setVoid_UserID(Integer newValue)
    {
        this.setVoid_UserID(newValue,this.UserSecurityID);


    }
    public Integer getVoid_UserID()
    {
        return this.getVoid_UserID(this.UserSecurityID);
    }

    public void setVoid_UserID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Void_UserID' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[Void_UserID]=["+newValue+"]");
                   dbDB.setVoid_UserID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[Void_UserID]=["+newValue+"]");
           dbDB.setVoid_UserID(newValue);
         }
    }
    public Integer getVoid_UserID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Void_UserID' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getVoid_UserID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getVoid_UserID();
         }
        return myVal;
    }

    public void setRequiresHandCarryCD(Integer newValue)
    {
        this.setRequiresHandCarryCD(newValue,this.UserSecurityID);


    }
    public Integer getRequiresHandCarryCD()
    {
        return this.getRequiresHandCarryCD(this.UserSecurityID);
    }

    public void setRequiresHandCarryCD(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='RequiresHandCarryCD' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[RequiresHandCarryCD]=["+newValue+"]");
                   dbDB.setRequiresHandCarryCD(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[RequiresHandCarryCD]=["+newValue+"]");
           dbDB.setRequiresHandCarryCD(newValue);
         }
    }
    public Integer getRequiresHandCarryCD(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='RequiresHandCarryCD' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getRequiresHandCarryCD();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getRequiresHandCarryCD();
         }
        return myVal;
    }

    public void setReportFollowUpCounter(Integer newValue)
    {
        this.setReportFollowUpCounter(newValue,this.UserSecurityID);


    }
    public Integer getReportFollowUpCounter()
    {
        return this.getReportFollowUpCounter(this.UserSecurityID);
    }

    public void setReportFollowUpCounter(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ReportFollowUpCounter' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[ReportFollowUpCounter]=["+newValue+"]");
                   dbDB.setReportFollowUpCounter(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[ReportFollowUpCounter]=["+newValue+"]");
           dbDB.setReportFollowUpCounter(newValue);
         }
    }
    public Integer getReportFollowUpCounter(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ReportFollowUpCounter' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getReportFollowUpCounter();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getReportFollowUpCounter();
         }
        return myVal;
    }

    public void setProblemCategory(String newValue)
    {
        this.setProblemCategory(newValue,this.UserSecurityID);


    }
    public String getProblemCategory()
    {
        return this.getProblemCategory(this.UserSecurityID);
    }

    public void setProblemCategory(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ProblemCategory' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[ProblemCategory]=["+newValue+"]");
                   dbDB.setProblemCategory(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[ProblemCategory]=["+newValue+"]");
           dbDB.setProblemCategory(newValue);
         }
    }
    public String getProblemCategory(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ProblemCategory' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getProblemCategory();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getProblemCategory();
         }
        return myVal;
    }

    public void setProblemDescription(String newValue)
    {
        this.setProblemDescription(newValue,this.UserSecurityID);


    }
    public String getProblemDescription()
    {
        return this.getProblemDescription(this.UserSecurityID);
    }

    public void setProblemDescription(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ProblemDescription' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[ProblemDescription]=["+newValue+"]");
                   dbDB.setProblemDescription(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[ProblemDescription]=["+newValue+"]");
           dbDB.setProblemDescription(newValue);
         }
    }
    public String getProblemDescription(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ProblemDescription' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getProblemDescription();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getProblemDescription();
         }
        return myVal;
    }

    public void setAgeCommentLock(Integer newValue)
    {
        this.setAgeCommentLock(newValue,this.UserSecurityID);


    }
    public Integer getAgeCommentLock()
    {
        return this.getAgeCommentLock(this.UserSecurityID);
    }

    public void setAgeCommentLock(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AgeCommentLock' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[AgeCommentLock]=["+newValue+"]");
                   dbDB.setAgeCommentLock(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[AgeCommentLock]=["+newValue+"]");
           dbDB.setAgeCommentLock(newValue);
         }
    }
    public Integer getAgeCommentLock(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AgeCommentLock' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getAgeCommentLock();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getAgeCommentLock();
         }
        return myVal;
    }

    public void setSentTo_ReqRec_Pt(Date newValue)
    {
        this.setSentTo_ReqRec_Pt(newValue,this.UserSecurityID);


    }
    public Date getSentTo_ReqRec_Pt()
    {
        return this.getSentTo_ReqRec_Pt(this.UserSecurityID);
    }

    public void setSentTo_ReqRec_Pt(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SentTo_ReqRec_Pt' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[SentTo_ReqRec_Pt]=["+newValue+"]");
                   dbDB.setSentTo_ReqRec_Pt(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[SentTo_ReqRec_Pt]=["+newValue+"]");
           dbDB.setSentTo_ReqRec_Pt(newValue);
         }
    }
    public Date getSentTo_ReqRec_Pt(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SentTo_ReqRec_Pt' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getSentTo_ReqRec_Pt();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getSentTo_ReqRec_Pt();
         }
        return myVal;
    }

    public void setSentToPt(Date newValue)
    {
        this.setSentToPt(newValue,this.UserSecurityID);


    }
    public Date getSentToPt()
    {
        return this.getSentToPt(this.UserSecurityID);
    }

    public void setSentToPt(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SentToPt' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[SentToPt]=["+newValue+"]");
                   dbDB.setSentToPt(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[SentToPt]=["+newValue+"]");
           dbDB.setSentToPt(newValue);
         }
    }
    public Date getSentToPt(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SentToPt' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getSentToPt();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getSentToPt();
         }
        return myVal;
    }

    public void setSentTo_SP_Pt(Date newValue)
    {
        this.setSentTo_SP_Pt(newValue,this.UserSecurityID);


    }
    public Date getSentTo_SP_Pt()
    {
        return this.getSentTo_SP_Pt(this.UserSecurityID);
    }

    public void setSentTo_SP_Pt(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SentTo_SP_Pt' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[SentTo_SP_Pt]=["+newValue+"]");
                   dbDB.setSentTo_SP_Pt(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[SentTo_SP_Pt]=["+newValue+"]");
           dbDB.setSentTo_SP_Pt(newValue);
         }
    }
    public Date getSentTo_SP_Pt(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SentTo_SP_Pt' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getSentTo_SP_Pt();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getSentTo_SP_Pt();
         }
        return myVal;
    }

    public void setNIDReferralSource(String newValue)
    {
        this.setNIDReferralSource(newValue,this.UserSecurityID);


    }
    public String getNIDReferralSource()
    {
        return this.getNIDReferralSource(this.UserSecurityID);
    }

    public void setNIDReferralSource(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='NIDReferralSource' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[NIDReferralSource]=["+newValue+"]");
                   dbDB.setNIDReferralSource(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[NIDReferralSource]=["+newValue+"]");
           dbDB.setNIDReferralSource(newValue);
         }
    }
    public String getNIDReferralSource(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='NIDReferralSource' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getNIDReferralSource();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getNIDReferralSource();
         }
        return myVal;
    }

    public void setNIDReferralCode(String newValue)
    {
        this.setNIDReferralCode(newValue,this.UserSecurityID);


    }
    public String getNIDReferralCode()
    {
        return this.getNIDReferralCode(this.UserSecurityID);
    }

    public void setNIDReferralCode(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='NIDReferralCode' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[NIDReferralCode]=["+newValue+"]");
                   dbDB.setNIDReferralCode(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[NIDReferralCode]=["+newValue+"]");
           dbDB.setNIDReferralCode(newValue);
         }
    }
    public String getNIDReferralCode(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='NIDReferralCode' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getNIDReferralCode();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getNIDReferralCode();
         }
        return myVal;
    }

    public void setCCTransactionID(Integer newValue)
    {
        this.setCCTransactionID(newValue,this.UserSecurityID);


    }
    public Integer getCCTransactionID()
    {
        return this.getCCTransactionID(this.UserSecurityID);
    }

    public void setCCTransactionID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CCTransactionID' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[CCTransactionID]=["+newValue+"]");
                   dbDB.setCCTransactionID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[CCTransactionID]=["+newValue+"]");
           dbDB.setCCTransactionID(newValue);
         }
    }
    public Integer getCCTransactionID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CCTransactionID' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getCCTransactionID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getCCTransactionID();
         }
        return myVal;
    }

    public void setisVIP(Integer newValue)
    {
        this.setisVIP(newValue,this.UserSecurityID);


    }
    public Integer getisVIP()
    {
        return this.getisVIP(this.UserSecurityID);
    }

    public void setisVIP(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='isVIP' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[isVIP]=["+newValue+"]");
                   dbDB.setisVIP(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[isVIP]=["+newValue+"]");
           dbDB.setisVIP(newValue);
         }
    }
    public Integer getisVIP(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='isVIP' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getisVIP();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getisVIP();
         }
        return myVal;
    }

    public void setHCFAinvoice(String newValue)
    {
        this.setHCFAinvoice(newValue,this.UserSecurityID);


    }
    public String getHCFAinvoice()
    {
        return this.getHCFAinvoice(this.UserSecurityID);
    }

    public void setHCFAinvoice(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='HCFAinvoice' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[HCFAinvoice]=["+newValue+"]");
                   dbDB.setHCFAinvoice(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[HCFAinvoice]=["+newValue+"]");
           dbDB.setHCFAinvoice(newValue);
         }
    }
    public String getHCFAinvoice(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='HCFAinvoice' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getHCFAinvoice();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getHCFAinvoice();
         }
        return myVal;
    }

    public void setRequiredFieldList(java.util.Hashtable requiredFieldList)
	 {

        this.requiredFieldList = requiredFieldList;
    }

    public java.util.Hashtable getRequiredFieldList()
    {
        return this.requiredFieldList;
    }
    public void setExpiredFieldList(java.util.Hashtable expiredFieldList)
    {
        this.expiredFieldList = expiredFieldList;
    }

    public void setEnglishFieldList(java.util.Hashtable englishFieldList)
    {
        this.englishFieldList = englishFieldList;
    }

    public java.util.Hashtable getExpiredFieldList()
    {
        return this.expiredFieldList;
    }
    public boolean isExpired(java.util.Date myDate, int daysVector)
    {
       return isExpired(myDate,new java.util.Date(),daysVector);
    }


    public boolean isRead(String fieldN, Integer GroupRefID)
    {
        boolean myVal = false;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='"+fieldN+"' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==1)
               {
                   myVal = true;
               }
            }
        }
        else
        {

           if (iSecurityCheck.intValue()>=1)
           {
                  myVal = true;
           }
        }
        return myVal;
    }


    public boolean isWrite(String fieldN, Integer GroupRefID)
    {
        boolean myVal = false;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='"+fieldN+"' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {
                   myVal = true;
               }
            }
        }
        else
        {

           if (iSecurityCheck.intValue()>=2)
           {
                  myVal = true;
           }
        }
        return myVal;
    }


    public boolean isExpired(java.util.Date myDate, java.util.Date refDate,int daysVector)
    {
        java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat("yyyy-MM-dd");
        boolean myVal = true;
        try
        {
            if (myDate.after(dbdf.parse("1800-01-10")))
            {
                if (myDate.after(dateChange(refDate,daysVector)))
                {
                    myVal=false;
                }
            }
            else
            {
                myVal = false;
            }
        }
        catch (Exception Datee)
        {
        }
        return myVal;
    }

    public java.util.Date dateChange(java.util.Date myD, int dayChange)
    {
        java.util.Calendar calendar = java.util.Calendar.getInstance();
        calendar.setTime(myD);
        calendar.add(java.util.Calendar.DATE, dayChange);
        java.util.Date resultAsDate = new java.util.Date(calendar.getTime().getTime());
        return resultAsDate;
    }

    public boolean isRequired(String fieldN)
    {
        return isRequired(fieldN, new Integer("1"));
    }

    public boolean isRequired(String fieldN, Integer GroupRefID)
    {
        boolean myVal = false;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='"+fieldN+"' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getRequiredID().intValue()==1)
               {
                   myVal = true;
               }
            }
        }
        else
        {
            if (this.getRequiredFieldList().containsKey(fieldN))
            {
                myVal = true;
            }
        }
        return myVal;
    }
    public boolean isExpiredCheck(String fieldN)
    {
        return isExpiredCheck(fieldN, new Integer ("1"));
    }

    public boolean isExpiredCheck(String fieldN, Integer GroupRefID)
    {
        boolean myVal = false;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='"+fieldN+"' AND TableName='tNIM3_Encounter'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getTrackedID().intValue()==1)
               {
                   myVal = true;
               }
            }
        }
        else
        {
            if (this.getExpiredFieldList().containsKey(fieldN))
           {
                myVal = true;
            }
        }
        return myVal;
    }

    public void makeRequiredHash()
    {
       makeRequiredHash(new Integer(1));
    }

    public void makeRequiredHash(Integer GroupRefID)
    {

        java.util.Hashtable newHash = new java.util.Hashtable();
        if (runFieldSecurity)
        {
            java.util.Enumeration myKeys = englishFieldList.keys(); 
            while (myKeys.hasMoreElements()) 
            {
                 String myElement = (String) myKeys.nextElement();
                 bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='"+myElement+"' AND TableName='tNIM3_Encounter'" ,"");
                 bltFieldSecurity        working_bltFieldSecurity;
                 ListElement         leCurrentElement;
                 Enumeration eList = myFSL.elements();
                 if (eList.hasMoreElements())
                 {
                     leCurrentElement    = (ListElement) eList.nextElement();
                     working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
                     if (working_bltFieldSecurity.getRequiredID().intValue()==1)
                     {
                         newHash.put(myElement,new Boolean(true));
                     }
                 }
            }
        }
        else
        {
            newHash.put("ReferralID",new Boolean(true));
            newHash.put("ScanPass",new Boolean(true));
        }
        this.setRequiredFieldList(newHash);
    }

    public void makeEnglishHash()
    {

        java.util.Hashtable newHash = new java.util.Hashtable();        newHash.put("UniqueCreateDate","UniqueCreateDate");
        newHash.put("UniqueModifyDate","UniqueModifyDate");
        newHash.put("UniqueModifyComments","UniqueModifyComments");
        newHash.put("ReferralID","ReferralID");
        newHash.put("EncounterTypeID","Type");
        newHash.put("ScanPass","ScanPass");
        newHash.put("AppointmentID","AppointmentID");
        newHash.put("AttendingPhysicianID","Attending Physician");
        newHash.put("ReferringPhysicianID","Referring Physician");
        newHash.put("DateOfService","Date Of Service");
        newHash.put("NextActionDate","Next Action Date");
        newHash.put("NextActionTaskID","Next Action Task");
        newHash.put("SentToRefDr","Sent To Ref Dr");
        newHash.put("SentToRefDr_CRID","SentToRefDr_CRID");
        newHash.put("SentToAdj","Sent To Adjuster");
        newHash.put("SentToAdj_CRID","SentToAdj_CRID");
        newHash.put("SentToIC","Sent To Provider");
        newHash.put("SentToIC_CRID","SentToIC_CRID");
        newHash.put("SentTo_Appt_Conf_Pat","Appt Conf Pat");
        newHash.put("SentTo_Appt_Conf_Pat_CRID","SentTo_Appt_Conf_Pat_CRID");
        newHash.put("SentTo_SP_Pat","SP Sent To Pat");
        newHash.put("SentTo_SP_Pat_CRID","SentTo_SP_Pat_CRID");
        newHash.put("SentTo_SP_RefDr","SP Sent To Ref Dr");
        newHash.put("SentTo_SP_RefDr_CRID","SentTo_SP_RefDr_CRID");
        newHash.put("SentTo_SP_Adj","SP Sent To Adjuster");
        newHash.put("SentTo_SP_Adj_CRID","SentTo_SP_Adj_CRID");
        newHash.put("SentTo_SP_IC","SP Sent To Provider");
        newHash.put("SentTo_SP_IC_CRID","SentTo_SP_IC_CRID");
        newHash.put("SentTo_DataProc_RefDr","Data Proc Sent To Ref Dr");
        newHash.put("SentTo_DataProc_RefDr_CRID","SentTo_DataProc_RefDr_CRID");
        newHash.put("SentTo_DataProc_Adj","Data Proc Sent To Adjuster");
        newHash.put("SentTo_DataProc_Adj_CRID","SentTo_DataProc_Adj_CRID");
        newHash.put("SentTo_Bill_Pay","Bill Sent To Payer");
        newHash.put("SentTo_Bill_Pay_CRID","SentTo_Bill_Pay_CRID");
        newHash.put("Rec_Bill_Pay","Payment Rec By Payer");
        newHash.put("Rec_Bill_Pay_CRID","Rec_Bill_Pay_CRID");
        newHash.put("SentTo_Bill_Pro","Payment Sent To Provider");
        newHash.put("SentTo_Bill_Pro_CRID","SentTo_Bill_Pro_CRID");
        newHash.put("Rec_Bill_Pro","Bill Rec By Provider");
        newHash.put("Rec_Bill_Pro_CRID","Rec_Bill_Pro_CRID");
        newHash.put("ReportFileID","ReportFileID");
        newHash.put("CapabilityReportFileID","CapabilityReportFileID");
        newHash.put("DICOMFileID","DICOMFileID");
        newHash.put("LODID","Level of Disability");
        newHash.put("Comments","Comments");
        newHash.put("AuditNotes","Audit Notes");
        newHash.put("EncounterStatusID","Encounter Status");
        newHash.put("PaidToProviderCheck1Number","PaidToProviderCheck1Number");
        newHash.put("PaidToProviderCheck1Amount","PaidToProviderCheck1Amount");
        newHash.put("PaidToProviderCheck1Date","PaidToProviderCheck1Date");
        newHash.put("PaidToProviderCheck2Number","PaidToProviderCheck2Number");
        newHash.put("PaidToProviderCheck2Amount","PaidToProviderCheck2Amount");
        newHash.put("PaidToProviderCheck2Date","PaidToProviderCheck2Date");
        newHash.put("PaidToProviderCheck3Number","PaidToProviderCheck3Number");
        newHash.put("PaidToProviderCheck3Amount","PaidToProviderCheck3Amount");
        newHash.put("PaidToProviderCheck3Date","PaidToProviderCheck3Date");
        newHash.put("SentTo_ReqRec_RefDr","Sent To Request Received Ref Dr");
        newHash.put("SentTo_ReqRec_RefDr_CRID","SentTo_ReqRec_RefDr_CRID");
        newHash.put("SentTo_ReqRec_Adj","Sent To Request Received Adj");
        newHash.put("SentTo_ReqRec_Adj_CRID","SentTo_ReqRec_Adj_CRID");
        newHash.put("TimeTrack_ReqRec","TimeTrack_ReqRec");
        newHash.put("TimeTrack_ReqRec_UserID","TimeTrack_ReqRec_UserID");
        newHash.put("TimeTrack_ReqCreated","TimeTrack_ReqCreated");
        newHash.put("TimeTrack_ReqCreated_UserID","TimeTrack_ReqCreated_UserID");
        newHash.put("TimeTrack_ReqProc","TimeTrack_ReqProc");
        newHash.put("TimeTrack_ReqProc_UserID","TimeTrack_ReqProc_UserID");
        newHash.put("TimeTrack_ReqSched","TimeTrack_ReqSched");
        newHash.put("TimeTrack_ReqSched_UserID","TimeTrack_ReqSched_UserID");
        newHash.put("TimeTrack_ReqDelivered","TimeTrack_ReqDelivered");
        newHash.put("TimeTrack_ReqDelivered_UserID","TimeTrack_ReqDelivered_UserID");
        newHash.put("TimeTrack_ReqPaidIn","TimeTrack_ReqPaidIn");
        newHash.put("TimeTrack_ReqPaidIn_UserID","TimeTrack_ReqPaidIn_UserID");
        newHash.put("TimeTrack_ReqPaidOut","TimeTrack_ReqPaidOut");
        newHash.put("TimeTrack_ReqPaidOut_UserID","TimeTrack_ReqPaidOut_UserID");
        newHash.put("ProviderInvoiceID","ProviderInvoiceID");
        newHash.put("TimeTrack_ReqRec_RCodeID","TimeTrack_ReqRec_RCodeID");
        newHash.put("TimeTrack_ReqCreated_RCodeID","TimeTrack_ReqCreated_RCodeID");
        newHash.put("TimeTrack_ReqProc_RCodeID","TimeTrack_ReqProc_RCodeID");
        newHash.put("TimeTrack_ReqSched_RCodeID","TimeTrack_ReqSched_RCodeID");
        newHash.put("TimeTrack_ReqDelivered_RCodeID","TimeTrack_ReqDelivered_RCodeID");
        newHash.put("TimeTrack_ReqPaidIn_RCodeID","TimeTrack_ReqPaidIn_RCodeID");
        newHash.put("TimeTrack_ReqPaidOut_RCodeID","TimeTrack_ReqPaidOut_RCodeID");
        newHash.put("TimeTrack_ReqApproved_RCodeID","TimeTrack_ReqApproved_RCodeID");
        newHash.put("TimeTrack_ReqApproved","TimeTrack_ReqApproved");
        newHash.put("TimeTrack_ReqApproved_UserID","TimeTrack_ReqApproved_UserID");
        newHash.put("SentTo_ReqRec_Adm","Sent To Request Received Adm");
        newHash.put("SentTo_ReqRec_Adm_CRID","SentTo_ReqRec_Adm_CRID");
        newHash.put("SentTo_SP_Adm","SP Sent To Admin");
        newHash.put("SentTo_SP_Adm_CRID","SentTo_SP_Adm_CRID");
        newHash.put("SentToAdm","Sent To Admin");
        newHash.put("SentToAdm_CRID","SentToAdm_CRID");
        newHash.put("TimeTrack_ReqInitialAppointment_RCodeID","TimeTrack_ReqInitialAppointment_RCodeID");
        newHash.put("TimeTrack_ReqInitialAppointment","TimeTrack_ReqInitialAppointment");
        newHash.put("TimeTrack_ReqInitialAppointment_UserID","TimeTrack_ReqInitialAppointment_UserID");
        newHash.put("TimeTrack_ReqRxReview_RCodeID","TimeTrack_ReqRxReview_RCodeID");
        newHash.put("TimeTrack_ReqRxReview","TimeTrack_ReqRxReview");
        newHash.put("TimeTrack_ReqRxReview_UserID","TimeTrack_ReqRxReview_UserID");
        newHash.put("TimeTrack_ReqRpReview_RCodeID","TimeTrack_ReqRpReview_RCodeID");
        newHash.put("TimeTrack_ReqRpReview","TimeTrack_ReqRpReview");
        newHash.put("TimeTrack_ReqRpReview_UserID","TimeTrack_ReqRpReview_UserID");
        newHash.put("NextActionNotes","Next Action Notes");
        newHash.put("VoidLeakReasonID","VoidLeakReasonID");
        newHash.put("ExportPaymentToQB","ExportPaymentToQB");
        newHash.put("SentTo_SP_NCM","SentTo_SP_NCM");
        newHash.put("SentTo_SP_NCM_CRID","SentTo_SP_NCM_CRID");
        newHash.put("SentTo_RP_NCM","SentTo_RP_NCM");
        newHash.put("SentTo_RP_NCM_CRID","SentTo_RP_NCM_CRID");
        newHash.put("SentTo_ReqRec_NCM","SentTo_ReqRec_NCM");
        newHash.put("SentTo_ReqRec_NCM_CRID","SentTo_ReqRec_NCM_CRID");
        newHash.put("isSTAT","isSTAT");
        newHash.put("RequiresFilms","RequiresFilms");
        newHash.put("HasBeenRescheduled","HasBeenRescheduled");
        newHash.put("SentTo_SP_Adm2","SentTo_SP_Adm2");
        newHash.put("SentTo_SP_Adm2_CRID","SentTo_SP_Adm2_CRID");
        newHash.put("SentTo_RP_Adm2","SentTo_RP_Adm2");
        newHash.put("SentTo_RP_Adm2_CRID","SentTo_RP_Adm2_CRID");
        newHash.put("SentTo_ReqRec_Adm2","SentTo_ReqRec_Adm2");
        newHash.put("SentTo_ReqRec_Adm2_CRID","SentTo_ReqRec_Adm2_CRID");
        newHash.put("SentTo_SP_Adm3","SentTo_SP_Adm3");
        newHash.put("SentTo_SP_Adm3_CRID","SentTo_SP_Adm3_CRID");
        newHash.put("SentTo_RP_Adm3","SentTo_RP_Adm3");
        newHash.put("SentTo_RP_Adm3_CRID","SentTo_RP_Adm3_CRID");
        newHash.put("SentTo_ReqRec_Adm3","SentTo_ReqRec_Adm3");
        newHash.put("SentTo_ReqRec_Adm3_CRID","SentTo_ReqRec_Adm3_CRID");
        newHash.put("SentTo_SP_Adm4","SentTo_SP_Adm4");
        newHash.put("SentTo_SP_Adm4_CRID","SentTo_SP_Adm4_CRID");
        newHash.put("SentTo_RP_Adm4","SentTo_RP_Adm4");
        newHash.put("SentTo_RP_Adm4_CRID","SentTo_RP_Adm4_CRID");
        newHash.put("SentTo_ReqRec_Adm4","SentTo_ReqRec_Adm4");
        newHash.put("SentTo_ReqRec_Adm4_CRID","SentTo_ReqRec_Adm4_CRID");
        newHash.put("RequiresAgeInjury","RequiresAgeInjury");
        newHash.put("isCourtesy","isCourtesy");
        newHash.put("BillingHCFA_ToPayer_FileID","BillingHCFA_ToPayer_FileID");
        newHash.put("BillingHCFA_FromProvider_FileID","BillingHCFA_FromProvider_FileID");
        newHash.put("isRetro","isRetro");
        newHash.put("ExportAPToQB","ExportAPToQB");
        newHash.put("ExportARToQB","ExportARToQB");
        newHash.put("AmountBilledByProvider","AmountBilledByProvider");
        newHash.put("SeeNetDev_Flagged","SeeNetDev_Flagged");
        newHash.put("SeeNetDev_ReqSentToNetDev","SeeNetDev_ReqSentToNetDev");
        newHash.put("SeeNetDev_ReqSentToNetDev_UserID","SeeNetDev_ReqSentToNetDev_UserID");
        newHash.put("SeeNetDev_Waiting","SeeNetDev_Waiting");
        newHash.put("PatientAvailability","Patient Availability");
        newHash.put("RequiresOnlineImage","RequiresOnlineImage");
        newHash.put("Pricing_Structure","Pricing_Structure");
        newHash.put("SeeNetDev_SelectedPracticeID","SeeNetDev_SelectedPracticeID");
        newHash.put("ActionDate_OnlineImageRequest","ActionDate_OnlineImageRequest");
        newHash.put("ActionDate_ReportRequest","ActionDate_ReportRequest");
        newHash.put("Void_UserID","Void_UserID");
        newHash.put("RequiresHandCarryCD","RequiresHandCarryCD");
        newHash.put("ReportFollowUpCounter","ReportFollowUpCounter");
        newHash.put("ProblemCategory","ProblemCategory");
        newHash.put("ProblemDescription","ProblemDescription");
        newHash.put("AgeCommentLock","AgeCommentLock");
        newHash.put("SentTo_ReqRec_Pt","SentTo_ReqRec_Pt");
        newHash.put("SentToPt","SentToPt");
        newHash.put("SentTo_SP_Pt","SentTo_SP_Pt");
        newHash.put("NIDReferralSource","NIDReferralSource");
        newHash.put("NIDReferralCode","NIDReferralCode");
        newHash.put("CCTransactionID","CCTransactionID");
        newHash.put("isVIP","isVIP");
        newHash.put("HCFAinvoice","HCFAinvoice");

        this.setEnglishFieldList(newHash);
    }
    public void makeExpiredHash()
    {

        java.util.Hashtable newHash = new java.util.Hashtable();
        this.setExpiredFieldList(newHash);
    }

    public java.util.Hashtable englishFieldList = new java.util.Hashtable();
    public java.util.Hashtable requiredFieldList = new java.util.Hashtable();
    public java.util.Hashtable expiredFieldList = new java.util.Hashtable();

    public boolean isComplete()
    {
	     boolean myVal = true;
	     java.util.Enumeration myKeys = this.getRequiredFieldList().keys(); 
	     while (myKeys.hasMoreElements()) 
	     {
	         String myElement = (String) myKeys.nextElement();
	         if (!isComplete(myElement))
	         {
	             myVal = false;
	         }
	     }
	     return myVal;
    }

    public String getEnglish(String fieldN)
    {
        String myVal = null;
        if (this.englishFieldList.containsKey(fieldN))
        {
            myVal = (String)this.englishFieldList.get(fieldN);
        }
        return myVal;
    }
    public java.util.Vector isComplete(int daysVector)
    {
	     boolean myVal = false;
	     java.util.Enumeration myKeys = this.getExpiredFieldList().keys(); 
	     java.util.Vector myExpiredList = new java.util.Vector(); 
	     while (myKeys.hasMoreElements()) 
	     {
	         String myElement = (String) myKeys.nextElement();
	         if (isExpired(myElement,daysVector))
	         {
	             myVal = true;
	             myExpiredList.addElement(myElement);
	         }
	     }
	     return myExpiredList;
    }

    public void setField(String fieldN, Object fieldV)
    {
     boolean myVal = false;
     try
     {
        java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat("yyyy-MM-dd");

        if (fieldN.equals("EncounterID"))
        {
             this.setEncounterID((Integer)fieldV);
        }

        else if (fieldN.equals("UniqueCreateDate"))
        {
            this.setUniqueCreateDate((java.util.Date)fieldV);
        }

        else if (fieldN.equals("UniqueModifyDate"))
        {
            this.setUniqueModifyDate((java.util.Date)fieldV);
        }

        else if (fieldN.equals("UniqueModifyComments"))
        {
            this.setUniqueModifyComments((String)fieldV);
        }

        else if (fieldN.equals("ReferralID"))
        {
             this.setReferralID((Integer)fieldV);
        }

        else if (fieldN.equals("EncounterTypeID"))
        {
             this.setEncounterTypeID((Integer)fieldV);
        }

        else if (fieldN.equals("ScanPass"))
        {
            this.setScanPass((String)fieldV);
        }

        else if (fieldN.equals("AppointmentID"))
        {
             this.setAppointmentID((Integer)fieldV);
        }

        else if (fieldN.equals("AttendingPhysicianID"))
        {
             this.setAttendingPhysicianID((Integer)fieldV);
        }

        else if (fieldN.equals("ReferringPhysicianID"))
        {
             this.setReferringPhysicianID((Integer)fieldV);
        }

        else if (fieldN.equals("DateOfService"))
        {
            this.setDateOfService((java.util.Date)fieldV);
        }

        else if (fieldN.equals("NextActionDate"))
        {
            this.setNextActionDate((java.util.Date)fieldV);
        }

        else if (fieldN.equals("NextActionTaskID"))
        {
             this.setNextActionTaskID((Integer)fieldV);
        }

        else if (fieldN.equals("SentToRefDr"))
        {
            this.setSentToRefDr((java.util.Date)fieldV);
        }

        else if (fieldN.equals("SentToRefDr_CRID"))
        {
             this.setSentToRefDr_CRID((Integer)fieldV);
        }

        else if (fieldN.equals("SentToAdj"))
        {
            this.setSentToAdj((java.util.Date)fieldV);
        }

        else if (fieldN.equals("SentToAdj_CRID"))
        {
             this.setSentToAdj_CRID((Integer)fieldV);
        }

        else if (fieldN.equals("SentToIC"))
        {
            this.setSentToIC((java.util.Date)fieldV);
        }

        else if (fieldN.equals("SentToIC_CRID"))
        {
             this.setSentToIC_CRID((Integer)fieldV);
        }

        else if (fieldN.equals("SentTo_Appt_Conf_Pat"))
        {
            this.setSentTo_Appt_Conf_Pat((java.util.Date)fieldV);
        }

        else if (fieldN.equals("SentTo_Appt_Conf_Pat_CRID"))
        {
             this.setSentTo_Appt_Conf_Pat_CRID((Integer)fieldV);
        }

        else if (fieldN.equals("SentTo_SP_Pat"))
        {
            this.setSentTo_SP_Pat((java.util.Date)fieldV);
        }

        else if (fieldN.equals("SentTo_SP_Pat_CRID"))
        {
             this.setSentTo_SP_Pat_CRID((Integer)fieldV);
        }

        else if (fieldN.equals("SentTo_SP_RefDr"))
        {
            this.setSentTo_SP_RefDr((java.util.Date)fieldV);
        }

        else if (fieldN.equals("SentTo_SP_RefDr_CRID"))
        {
             this.setSentTo_SP_RefDr_CRID((Integer)fieldV);
        }

        else if (fieldN.equals("SentTo_SP_Adj"))
        {
            this.setSentTo_SP_Adj((java.util.Date)fieldV);
        }

        else if (fieldN.equals("SentTo_SP_Adj_CRID"))
        {
             this.setSentTo_SP_Adj_CRID((Integer)fieldV);
        }

        else if (fieldN.equals("SentTo_SP_IC"))
        {
            this.setSentTo_SP_IC((java.util.Date)fieldV);
        }

        else if (fieldN.equals("SentTo_SP_IC_CRID"))
        {
             this.setSentTo_SP_IC_CRID((Integer)fieldV);
        }

        else if (fieldN.equals("SentTo_DataProc_RefDr"))
        {
            this.setSentTo_DataProc_RefDr((java.util.Date)fieldV);
        }

        else if (fieldN.equals("SentTo_DataProc_RefDr_CRID"))
        {
             this.setSentTo_DataProc_RefDr_CRID((Integer)fieldV);
        }

        else if (fieldN.equals("SentTo_DataProc_Adj"))
        {
            this.setSentTo_DataProc_Adj((java.util.Date)fieldV);
        }

        else if (fieldN.equals("SentTo_DataProc_Adj_CRID"))
        {
             this.setSentTo_DataProc_Adj_CRID((Integer)fieldV);
        }

        else if (fieldN.equals("SentTo_Bill_Pay"))
        {
            this.setSentTo_Bill_Pay((java.util.Date)fieldV);
        }

        else if (fieldN.equals("SentTo_Bill_Pay_CRID"))
        {
             this.setSentTo_Bill_Pay_CRID((Integer)fieldV);
        }

        else if (fieldN.equals("Rec_Bill_Pay"))
        {
            this.setRec_Bill_Pay((java.util.Date)fieldV);
        }

        else if (fieldN.equals("Rec_Bill_Pay_CRID"))
        {
             this.setRec_Bill_Pay_CRID((Integer)fieldV);
        }

        else if (fieldN.equals("SentTo_Bill_Pro"))
        {
            this.setSentTo_Bill_Pro((java.util.Date)fieldV);
        }

        else if (fieldN.equals("SentTo_Bill_Pro_CRID"))
        {
             this.setSentTo_Bill_Pro_CRID((Integer)fieldV);
        }

        else if (fieldN.equals("Rec_Bill_Pro"))
        {
            this.setRec_Bill_Pro((java.util.Date)fieldV);
        }

        else if (fieldN.equals("Rec_Bill_Pro_CRID"))
        {
             this.setRec_Bill_Pro_CRID((Integer)fieldV);
        }

        else if (fieldN.equals("ReportFileID"))
        {
             this.setReportFileID((Integer)fieldV);
        }

        else if (fieldN.equals("CapabilityReportFileID"))
        {
             this.setCapabilityReportFileID((Integer)fieldV);
        }

        else if (fieldN.equals("DICOMFileID"))
        {
             this.setDICOMFileID((Integer)fieldV);
        }

        else if (fieldN.equals("LODID"))
        {
             this.setLODID((Integer)fieldV);
        }

        else if (fieldN.equals("Comments"))
        {
            this.setComments((String)fieldV);
        }

        else if (fieldN.equals("AuditNotes"))
        {
            this.setAuditNotes((String)fieldV);
        }

        else if (fieldN.equals("EncounterStatusID"))
        {
             this.setEncounterStatusID((Integer)fieldV);
        }

        else if (fieldN.equals("PaidToProviderCheck1Number"))
        {
            this.setPaidToProviderCheck1Number((String)fieldV);
        }

        else if (fieldN.equals("PaidToProviderCheck1Amount"))
        {
            this.setPaidToProviderCheck1Amount((Double)fieldV);
        }

        else if (fieldN.equals("PaidToProviderCheck1Date"))
        {
            this.setPaidToProviderCheck1Date((java.util.Date)fieldV);
        }

        else if (fieldN.equals("PaidToProviderCheck2Number"))
        {
            this.setPaidToProviderCheck2Number((String)fieldV);
        }

        else if (fieldN.equals("PaidToProviderCheck2Amount"))
        {
            this.setPaidToProviderCheck2Amount((Double)fieldV);
        }

        else if (fieldN.equals("PaidToProviderCheck2Date"))
        {
            this.setPaidToProviderCheck2Date((java.util.Date)fieldV);
        }

        else if (fieldN.equals("PaidToProviderCheck3Number"))
        {
            this.setPaidToProviderCheck3Number((String)fieldV);
        }

        else if (fieldN.equals("PaidToProviderCheck3Amount"))
        {
            this.setPaidToProviderCheck3Amount((Double)fieldV);
        }

        else if (fieldN.equals("PaidToProviderCheck3Date"))
        {
            this.setPaidToProviderCheck3Date((java.util.Date)fieldV);
        }

        else if (fieldN.equals("SentTo_ReqRec_RefDr"))
        {
            this.setSentTo_ReqRec_RefDr((java.util.Date)fieldV);
        }

        else if (fieldN.equals("SentTo_ReqRec_RefDr_CRID"))
        {
             this.setSentTo_ReqRec_RefDr_CRID((Integer)fieldV);
        }

        else if (fieldN.equals("SentTo_ReqRec_Adj"))
        {
            this.setSentTo_ReqRec_Adj((java.util.Date)fieldV);
        }

        else if (fieldN.equals("SentTo_ReqRec_Adj_CRID"))
        {
             this.setSentTo_ReqRec_Adj_CRID((Integer)fieldV);
        }

        else if (fieldN.equals("TimeTrack_ReqRec"))
        {
            this.setTimeTrack_ReqRec((java.util.Date)fieldV);
        }

        else if (fieldN.equals("TimeTrack_ReqRec_UserID"))
        {
             this.setTimeTrack_ReqRec_UserID((Integer)fieldV);
        }

        else if (fieldN.equals("TimeTrack_ReqCreated"))
        {
            this.setTimeTrack_ReqCreated((java.util.Date)fieldV);
        }

        else if (fieldN.equals("TimeTrack_ReqCreated_UserID"))
        {
             this.setTimeTrack_ReqCreated_UserID((Integer)fieldV);
        }

        else if (fieldN.equals("TimeTrack_ReqProc"))
        {
            this.setTimeTrack_ReqProc((java.util.Date)fieldV);
        }

        else if (fieldN.equals("TimeTrack_ReqProc_UserID"))
        {
             this.setTimeTrack_ReqProc_UserID((Integer)fieldV);
        }

        else if (fieldN.equals("TimeTrack_ReqSched"))
        {
            this.setTimeTrack_ReqSched((java.util.Date)fieldV);
        }

        else if (fieldN.equals("TimeTrack_ReqSched_UserID"))
        {
             this.setTimeTrack_ReqSched_UserID((Integer)fieldV);
        }

        else if (fieldN.equals("TimeTrack_ReqDelivered"))
        {
            this.setTimeTrack_ReqDelivered((java.util.Date)fieldV);
        }

        else if (fieldN.equals("TimeTrack_ReqDelivered_UserID"))
        {
             this.setTimeTrack_ReqDelivered_UserID((Integer)fieldV);
        }

        else if (fieldN.equals("TimeTrack_ReqPaidIn"))
        {
            this.setTimeTrack_ReqPaidIn((java.util.Date)fieldV);
        }

        else if (fieldN.equals("TimeTrack_ReqPaidIn_UserID"))
        {
             this.setTimeTrack_ReqPaidIn_UserID((Integer)fieldV);
        }

        else if (fieldN.equals("TimeTrack_ReqPaidOut"))
        {
            this.setTimeTrack_ReqPaidOut((java.util.Date)fieldV);
        }

        else if (fieldN.equals("TimeTrack_ReqPaidOut_UserID"))
        {
             this.setTimeTrack_ReqPaidOut_UserID((Integer)fieldV);
        }

        else if (fieldN.equals("ProviderInvoiceID"))
        {
             this.setProviderInvoiceID((Integer)fieldV);
        }

        else if (fieldN.equals("TimeTrack_ReqRec_RCodeID"))
        {
             this.setTimeTrack_ReqRec_RCodeID((Integer)fieldV);
        }

        else if (fieldN.equals("TimeTrack_ReqCreated_RCodeID"))
        {
             this.setTimeTrack_ReqCreated_RCodeID((Integer)fieldV);
        }

        else if (fieldN.equals("TimeTrack_ReqProc_RCodeID"))
        {
             this.setTimeTrack_ReqProc_RCodeID((Integer)fieldV);
        }

        else if (fieldN.equals("TimeTrack_ReqSched_RCodeID"))
        {
             this.setTimeTrack_ReqSched_RCodeID((Integer)fieldV);
        }

        else if (fieldN.equals("TimeTrack_ReqDelivered_RCodeID"))
        {
             this.setTimeTrack_ReqDelivered_RCodeID((Integer)fieldV);
        }

        else if (fieldN.equals("TimeTrack_ReqPaidIn_RCodeID"))
        {
             this.setTimeTrack_ReqPaidIn_RCodeID((Integer)fieldV);
        }

        else if (fieldN.equals("TimeTrack_ReqPaidOut_RCodeID"))
        {
             this.setTimeTrack_ReqPaidOut_RCodeID((Integer)fieldV);
        }

        else if (fieldN.equals("TimeTrack_ReqApproved_RCodeID"))
        {
             this.setTimeTrack_ReqApproved_RCodeID((Integer)fieldV);
        }

        else if (fieldN.equals("TimeTrack_ReqApproved"))
        {
            this.setTimeTrack_ReqApproved((java.util.Date)fieldV);
        }

        else if (fieldN.equals("TimeTrack_ReqApproved_UserID"))
        {
             this.setTimeTrack_ReqApproved_UserID((Integer)fieldV);
        }

        else if (fieldN.equals("SentTo_ReqRec_Adm"))
        {
            this.setSentTo_ReqRec_Adm((java.util.Date)fieldV);
        }

        else if (fieldN.equals("SentTo_ReqRec_Adm_CRID"))
        {
             this.setSentTo_ReqRec_Adm_CRID((Integer)fieldV);
        }

        else if (fieldN.equals("SentTo_SP_Adm"))
        {
            this.setSentTo_SP_Adm((java.util.Date)fieldV);
        }

        else if (fieldN.equals("SentTo_SP_Adm_CRID"))
        {
             this.setSentTo_SP_Adm_CRID((Integer)fieldV);
        }

        else if (fieldN.equals("SentToAdm"))
        {
            this.setSentToAdm((java.util.Date)fieldV);
        }

        else if (fieldN.equals("SentToAdm_CRID"))
        {
             this.setSentToAdm_CRID((Integer)fieldV);
        }

        else if (fieldN.equals("TimeTrack_ReqInitialAppointment_RCodeID"))
        {
             this.setTimeTrack_ReqInitialAppointment_RCodeID((Integer)fieldV);
        }

        else if (fieldN.equals("TimeTrack_ReqInitialAppointment"))
        {
            this.setTimeTrack_ReqInitialAppointment((java.util.Date)fieldV);
        }

        else if (fieldN.equals("TimeTrack_ReqInitialAppointment_UserID"))
        {
             this.setTimeTrack_ReqInitialAppointment_UserID((Integer)fieldV);
        }

        else if (fieldN.equals("TimeTrack_ReqRxReview_RCodeID"))
        {
             this.setTimeTrack_ReqRxReview_RCodeID((Integer)fieldV);
        }

        else if (fieldN.equals("TimeTrack_ReqRxReview"))
        {
            this.setTimeTrack_ReqRxReview((java.util.Date)fieldV);
        }

        else if (fieldN.equals("TimeTrack_ReqRxReview_UserID"))
        {
             this.setTimeTrack_ReqRxReview_UserID((Integer)fieldV);
        }

        else if (fieldN.equals("TimeTrack_ReqRpReview_RCodeID"))
        {
             this.setTimeTrack_ReqRpReview_RCodeID((Integer)fieldV);
        }

        else if (fieldN.equals("TimeTrack_ReqRpReview"))
        {
            this.setTimeTrack_ReqRpReview((java.util.Date)fieldV);
        }

        else if (fieldN.equals("TimeTrack_ReqRpReview_UserID"))
        {
             this.setTimeTrack_ReqRpReview_UserID((Integer)fieldV);
        }

        else if (fieldN.equals("NextActionNotes"))
        {
            this.setNextActionNotes((String)fieldV);
        }

        else if (fieldN.equals("VoidLeakReasonID"))
        {
             this.setVoidLeakReasonID((Integer)fieldV);
        }

        else if (fieldN.equals("ExportPaymentToQB"))
        {
            this.setExportPaymentToQB((java.util.Date)fieldV);
        }

        else if (fieldN.equals("SentTo_SP_NCM"))
        {
            this.setSentTo_SP_NCM((java.util.Date)fieldV);
        }

        else if (fieldN.equals("SentTo_SP_NCM_CRID"))
        {
             this.setSentTo_SP_NCM_CRID((Integer)fieldV);
        }

        else if (fieldN.equals("SentTo_RP_NCM"))
        {
            this.setSentTo_RP_NCM((java.util.Date)fieldV);
        }

        else if (fieldN.equals("SentTo_RP_NCM_CRID"))
        {
             this.setSentTo_RP_NCM_CRID((Integer)fieldV);
        }

        else if (fieldN.equals("SentTo_ReqRec_NCM"))
        {
            this.setSentTo_ReqRec_NCM((java.util.Date)fieldV);
        }

        else if (fieldN.equals("SentTo_ReqRec_NCM_CRID"))
        {
             this.setSentTo_ReqRec_NCM_CRID((Integer)fieldV);
        }

        else if (fieldN.equals("isSTAT"))
        {
             this.setisSTAT((Integer)fieldV);
        }

        else if (fieldN.equals("RequiresFilms"))
        {
             this.setRequiresFilms((Integer)fieldV);
        }

        else if (fieldN.equals("HasBeenRescheduled"))
        {
             this.setHasBeenRescheduled((Integer)fieldV);
        }

        else if (fieldN.equals("SentTo_SP_Adm2"))
        {
            this.setSentTo_SP_Adm2((java.util.Date)fieldV);
        }

        else if (fieldN.equals("SentTo_SP_Adm2_CRID"))
        {
             this.setSentTo_SP_Adm2_CRID((Integer)fieldV);
        }

        else if (fieldN.equals("SentTo_RP_Adm2"))
        {
            this.setSentTo_RP_Adm2((java.util.Date)fieldV);
        }

        else if (fieldN.equals("SentTo_RP_Adm2_CRID"))
        {
             this.setSentTo_RP_Adm2_CRID((Integer)fieldV);
        }

        else if (fieldN.equals("SentTo_ReqRec_Adm2"))
        {
            this.setSentTo_ReqRec_Adm2((java.util.Date)fieldV);
        }

        else if (fieldN.equals("SentTo_ReqRec_Adm2_CRID"))
        {
             this.setSentTo_ReqRec_Adm2_CRID((Integer)fieldV);
        }

        else if (fieldN.equals("SentTo_SP_Adm3"))
        {
            this.setSentTo_SP_Adm3((java.util.Date)fieldV);
        }

        else if (fieldN.equals("SentTo_SP_Adm3_CRID"))
        {
             this.setSentTo_SP_Adm3_CRID((Integer)fieldV);
        }

        else if (fieldN.equals("SentTo_RP_Adm3"))
        {
            this.setSentTo_RP_Adm3((java.util.Date)fieldV);
        }

        else if (fieldN.equals("SentTo_RP_Adm3_CRID"))
        {
             this.setSentTo_RP_Adm3_CRID((Integer)fieldV);
        }

        else if (fieldN.equals("SentTo_ReqRec_Adm3"))
        {
            this.setSentTo_ReqRec_Adm3((java.util.Date)fieldV);
        }

        else if (fieldN.equals("SentTo_ReqRec_Adm3_CRID"))
        {
             this.setSentTo_ReqRec_Adm3_CRID((Integer)fieldV);
        }

        else if (fieldN.equals("SentTo_SP_Adm4"))
        {
            this.setSentTo_SP_Adm4((java.util.Date)fieldV);
        }

        else if (fieldN.equals("SentTo_SP_Adm4_CRID"))
        {
             this.setSentTo_SP_Adm4_CRID((Integer)fieldV);
        }

        else if (fieldN.equals("SentTo_RP_Adm4"))
        {
            this.setSentTo_RP_Adm4((java.util.Date)fieldV);
        }

        else if (fieldN.equals("SentTo_RP_Adm4_CRID"))
        {
             this.setSentTo_RP_Adm4_CRID((Integer)fieldV);
        }

        else if (fieldN.equals("SentTo_ReqRec_Adm4"))
        {
            this.setSentTo_ReqRec_Adm4((java.util.Date)fieldV);
        }

        else if (fieldN.equals("SentTo_ReqRec_Adm4_CRID"))
        {
             this.setSentTo_ReqRec_Adm4_CRID((Integer)fieldV);
        }

        else if (fieldN.equals("RequiresAgeInjury"))
        {
             this.setRequiresAgeInjury((Integer)fieldV);
        }

        else if (fieldN.equals("isCourtesy"))
        {
             this.setisCourtesy((Integer)fieldV);
        }

        else if (fieldN.equals("BillingHCFA_ToPayer_FileID"))
        {
             this.setBillingHCFA_ToPayer_FileID((Integer)fieldV);
        }

        else if (fieldN.equals("BillingHCFA_FromProvider_FileID"))
        {
             this.setBillingHCFA_FromProvider_FileID((Integer)fieldV);
        }

        else if (fieldN.equals("isRetro"))
        {
             this.setisRetro((Integer)fieldV);
        }

        else if (fieldN.equals("ExportAPToQB"))
        {
            this.setExportAPToQB((java.util.Date)fieldV);
        }

        else if (fieldN.equals("ExportARToQB"))
        {
            this.setExportARToQB((java.util.Date)fieldV);
        }

        else if (fieldN.equals("AmountBilledByProvider"))
        {
            this.setAmountBilledByProvider((Double)fieldV);
        }

        else if (fieldN.equals("SeeNetDev_Flagged"))
        {
             this.setSeeNetDev_Flagged((Integer)fieldV);
        }

        else if (fieldN.equals("SeeNetDev_ReqSentToNetDev"))
        {
            this.setSeeNetDev_ReqSentToNetDev((java.util.Date)fieldV);
        }

        else if (fieldN.equals("SeeNetDev_ReqSentToNetDev_UserID"))
        {
             this.setSeeNetDev_ReqSentToNetDev_UserID((Integer)fieldV);
        }

        else if (fieldN.equals("SeeNetDev_Waiting"))
        {
             this.setSeeNetDev_Waiting((Integer)fieldV);
        }

        else if (fieldN.equals("PatientAvailability"))
        {
            this.setPatientAvailability((String)fieldV);
        }

        else if (fieldN.equals("RequiresOnlineImage"))
        {
             this.setRequiresOnlineImage((Integer)fieldV);
        }

        else if (fieldN.equals("Pricing_Structure"))
        {
             this.setPricing_Structure((Integer)fieldV);
        }

        else if (fieldN.equals("SeeNetDev_SelectedPracticeID"))
        {
             this.setSeeNetDev_SelectedPracticeID((Integer)fieldV);
        }

        else if (fieldN.equals("ActionDate_OnlineImageRequest"))
        {
            this.setActionDate_OnlineImageRequest((java.util.Date)fieldV);
        }

        else if (fieldN.equals("ActionDate_ReportRequest"))
        {
            this.setActionDate_ReportRequest((java.util.Date)fieldV);
        }

        else if (fieldN.equals("Void_UserID"))
        {
             this.setVoid_UserID((Integer)fieldV);
        }

        else if (fieldN.equals("RequiresHandCarryCD"))
        {
             this.setRequiresHandCarryCD((Integer)fieldV);
        }

        else if (fieldN.equals("ReportFollowUpCounter"))
        {
             this.setReportFollowUpCounter((Integer)fieldV);
        }

        else if (fieldN.equals("ProblemCategory"))
        {
            this.setProblemCategory((String)fieldV);
        }

        else if (fieldN.equals("ProblemDescription"))
        {
            this.setProblemDescription((String)fieldV);
        }

        else if (fieldN.equals("AgeCommentLock"))
        {
             this.setAgeCommentLock((Integer)fieldV);
        }

        else if (fieldN.equals("SentTo_ReqRec_Pt"))
        {
            this.setSentTo_ReqRec_Pt((java.util.Date)fieldV);
        }

        else if (fieldN.equals("SentToPt"))
        {
            this.setSentToPt((java.util.Date)fieldV);
        }

        else if (fieldN.equals("SentTo_SP_Pt"))
        {
            this.setSentTo_SP_Pt((java.util.Date)fieldV);
        }

        else if (fieldN.equals("NIDReferralSource"))
        {
            this.setNIDReferralSource((String)fieldV);
        }

        else if (fieldN.equals("NIDReferralCode"))
        {
            this.setNIDReferralCode((String)fieldV);
        }

        else if (fieldN.equals("CCTransactionID"))
        {
             this.setCCTransactionID((Integer)fieldV);
        }

        else if (fieldN.equals("isVIP"))
        {
             this.setisVIP((Integer)fieldV);
        }

        else if (fieldN.equals("HCFAinvoice"))
        {
            this.setHCFAinvoice((String)fieldV);
        }

	    }
	    catch(Exception e2)
	    {}
	    //return myVal;
	}

    public String getFieldType(String fieldN)
    {
     String  myVal = null;
     try
     {
        java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat("yyyy-MM-dd");

        if (fieldN.equals("EncounterID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("UniqueCreateDate"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("UniqueModifyDate"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("UniqueModifyComments"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("ReferralID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("EncounterTypeID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("ScanPass"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("AppointmentID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("AttendingPhysicianID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("ReferringPhysicianID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("DateOfService"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("NextActionDate"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("NextActionTaskID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("SentToRefDr"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("SentToRefDr_CRID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("SentToAdj"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("SentToAdj_CRID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("SentToIC"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("SentToIC_CRID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("SentTo_Appt_Conf_Pat"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("SentTo_Appt_Conf_Pat_CRID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("SentTo_SP_Pat"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("SentTo_SP_Pat_CRID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("SentTo_SP_RefDr"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("SentTo_SP_RefDr_CRID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("SentTo_SP_Adj"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("SentTo_SP_Adj_CRID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("SentTo_SP_IC"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("SentTo_SP_IC_CRID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("SentTo_DataProc_RefDr"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("SentTo_DataProc_RefDr_CRID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("SentTo_DataProc_Adj"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("SentTo_DataProc_Adj_CRID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("SentTo_Bill_Pay"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("SentTo_Bill_Pay_CRID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("Rec_Bill_Pay"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("Rec_Bill_Pay_CRID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("SentTo_Bill_Pro"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("SentTo_Bill_Pro_CRID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("Rec_Bill_Pro"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("Rec_Bill_Pro_CRID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("ReportFileID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("CapabilityReportFileID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("DICOMFileID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("LODID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("Comments"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("AuditNotes"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("EncounterStatusID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("PaidToProviderCheck1Number"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("PaidToProviderCheck1Amount"))
        {
            myVal = "Double";
        }

        else if (fieldN.equals("PaidToProviderCheck1Date"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("PaidToProviderCheck2Number"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("PaidToProviderCheck2Amount"))
        {
            myVal = "Double";
        }

        else if (fieldN.equals("PaidToProviderCheck2Date"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("PaidToProviderCheck3Number"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("PaidToProviderCheck3Amount"))
        {
            myVal = "Double";
        }

        else if (fieldN.equals("PaidToProviderCheck3Date"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("SentTo_ReqRec_RefDr"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("SentTo_ReqRec_RefDr_CRID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("SentTo_ReqRec_Adj"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("SentTo_ReqRec_Adj_CRID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("TimeTrack_ReqRec"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("TimeTrack_ReqRec_UserID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("TimeTrack_ReqCreated"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("TimeTrack_ReqCreated_UserID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("TimeTrack_ReqProc"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("TimeTrack_ReqProc_UserID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("TimeTrack_ReqSched"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("TimeTrack_ReqSched_UserID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("TimeTrack_ReqDelivered"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("TimeTrack_ReqDelivered_UserID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("TimeTrack_ReqPaidIn"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("TimeTrack_ReqPaidIn_UserID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("TimeTrack_ReqPaidOut"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("TimeTrack_ReqPaidOut_UserID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("ProviderInvoiceID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("TimeTrack_ReqRec_RCodeID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("TimeTrack_ReqCreated_RCodeID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("TimeTrack_ReqProc_RCodeID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("TimeTrack_ReqSched_RCodeID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("TimeTrack_ReqDelivered_RCodeID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("TimeTrack_ReqPaidIn_RCodeID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("TimeTrack_ReqPaidOut_RCodeID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("TimeTrack_ReqApproved_RCodeID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("TimeTrack_ReqApproved"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("TimeTrack_ReqApproved_UserID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("SentTo_ReqRec_Adm"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("SentTo_ReqRec_Adm_CRID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("SentTo_SP_Adm"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("SentTo_SP_Adm_CRID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("SentToAdm"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("SentToAdm_CRID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("TimeTrack_ReqInitialAppointment_RCodeID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("TimeTrack_ReqInitialAppointment"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("TimeTrack_ReqInitialAppointment_UserID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("TimeTrack_ReqRxReview_RCodeID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("TimeTrack_ReqRxReview"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("TimeTrack_ReqRxReview_UserID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("TimeTrack_ReqRpReview_RCodeID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("TimeTrack_ReqRpReview"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("TimeTrack_ReqRpReview_UserID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("NextActionNotes"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("VoidLeakReasonID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("ExportPaymentToQB"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("SentTo_SP_NCM"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("SentTo_SP_NCM_CRID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("SentTo_RP_NCM"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("SentTo_RP_NCM_CRID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("SentTo_ReqRec_NCM"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("SentTo_ReqRec_NCM_CRID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("isSTAT"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("RequiresFilms"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("HasBeenRescheduled"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("SentTo_SP_Adm2"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("SentTo_SP_Adm2_CRID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("SentTo_RP_Adm2"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("SentTo_RP_Adm2_CRID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("SentTo_ReqRec_Adm2"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("SentTo_ReqRec_Adm2_CRID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("SentTo_SP_Adm3"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("SentTo_SP_Adm3_CRID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("SentTo_RP_Adm3"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("SentTo_RP_Adm3_CRID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("SentTo_ReqRec_Adm3"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("SentTo_ReqRec_Adm3_CRID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("SentTo_SP_Adm4"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("SentTo_SP_Adm4_CRID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("SentTo_RP_Adm4"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("SentTo_RP_Adm4_CRID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("SentTo_ReqRec_Adm4"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("SentTo_ReqRec_Adm4_CRID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("RequiresAgeInjury"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("isCourtesy"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("BillingHCFA_ToPayer_FileID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("BillingHCFA_FromProvider_FileID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("isRetro"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("ExportAPToQB"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("ExportARToQB"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("AmountBilledByProvider"))
        {
            myVal = "Double";
        }

        else if (fieldN.equals("SeeNetDev_Flagged"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("SeeNetDev_ReqSentToNetDev"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("SeeNetDev_ReqSentToNetDev_UserID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("SeeNetDev_Waiting"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("PatientAvailability"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("RequiresOnlineImage"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("Pricing_Structure"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("SeeNetDev_SelectedPracticeID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("ActionDate_OnlineImageRequest"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("ActionDate_ReportRequest"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("Void_UserID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("RequiresHandCarryCD"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("ReportFollowUpCounter"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("ProblemCategory"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("ProblemDescription"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("AgeCommentLock"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("SentTo_ReqRec_Pt"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("SentToPt"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("SentTo_SP_Pt"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("NIDReferralSource"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("NIDReferralCode"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("CCTransactionID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("isVIP"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("HCFAinvoice"))
        {
            myVal = "String";
        }

	    }
	    catch(Exception e2)
	    {}
	    return myVal;
	}

    public boolean isComplete(String fieldN)
    {
	     boolean myVal = false;
	     try
	     {
        java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat("yyyy-MM-dd");

	     if (fieldN.equals("EncounterID"))

	     {
                    if (this.getEncounterID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("UniqueCreateDate"))

	     {
                    if (this.getUniqueCreateDate().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("UniqueModifyDate"))

	     {
                    if (this.getUniqueModifyDate().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("UniqueModifyComments"))

	     {
                    if (this.getUniqueModifyComments().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("ReferralID"))

	     {
                    if (this.getReferralID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("EncounterTypeID"))

	     {
                    if (this.getEncounterTypeID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("ScanPass"))

	     {
                    if (this.getScanPass().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("AppointmentID"))

	     {
                    if (this.getAppointmentID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("AttendingPhysicianID"))

	     {
                    if (this.getAttendingPhysicianID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("ReferringPhysicianID"))

	     {
                    if (this.getReferringPhysicianID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("DateOfService"))

	     {
                    if (this.getDateOfService().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("NextActionDate"))

	     {
                    if (this.getNextActionDate().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("NextActionTaskID"))

	     {
                    if (this.getNextActionTaskID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("SentToRefDr"))

	     {
                    if (this.getSentToRefDr().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("SentToRefDr_CRID"))

	     {
                    if (this.getSentToRefDr_CRID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("SentToAdj"))

	     {
                    if (this.getSentToAdj().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("SentToAdj_CRID"))

	     {
                    if (this.getSentToAdj_CRID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("SentToIC"))

	     {
                    if (this.getSentToIC().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("SentToIC_CRID"))

	     {
                    if (this.getSentToIC_CRID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("SentTo_Appt_Conf_Pat"))

	     {
                    if (this.getSentTo_Appt_Conf_Pat().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("SentTo_Appt_Conf_Pat_CRID"))

	     {
                    if (this.getSentTo_Appt_Conf_Pat_CRID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("SentTo_SP_Pat"))

	     {
                    if (this.getSentTo_SP_Pat().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("SentTo_SP_Pat_CRID"))

	     {
                    if (this.getSentTo_SP_Pat_CRID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("SentTo_SP_RefDr"))

	     {
                    if (this.getSentTo_SP_RefDr().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("SentTo_SP_RefDr_CRID"))

	     {
                    if (this.getSentTo_SP_RefDr_CRID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("SentTo_SP_Adj"))

	     {
                    if (this.getSentTo_SP_Adj().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("SentTo_SP_Adj_CRID"))

	     {
                    if (this.getSentTo_SP_Adj_CRID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("SentTo_SP_IC"))

	     {
                    if (this.getSentTo_SP_IC().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("SentTo_SP_IC_CRID"))

	     {
                    if (this.getSentTo_SP_IC_CRID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("SentTo_DataProc_RefDr"))

	     {
                    if (this.getSentTo_DataProc_RefDr().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("SentTo_DataProc_RefDr_CRID"))

	     {
                    if (this.getSentTo_DataProc_RefDr_CRID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("SentTo_DataProc_Adj"))

	     {
                    if (this.getSentTo_DataProc_Adj().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("SentTo_DataProc_Adj_CRID"))

	     {
                    if (this.getSentTo_DataProc_Adj_CRID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("SentTo_Bill_Pay"))

	     {
                    if (this.getSentTo_Bill_Pay().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("SentTo_Bill_Pay_CRID"))

	     {
                    if (this.getSentTo_Bill_Pay_CRID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("Rec_Bill_Pay"))

	     {
                    if (this.getRec_Bill_Pay().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("Rec_Bill_Pay_CRID"))

	     {
                    if (this.getRec_Bill_Pay_CRID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("SentTo_Bill_Pro"))

	     {
                    if (this.getSentTo_Bill_Pro().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("SentTo_Bill_Pro_CRID"))

	     {
                    if (this.getSentTo_Bill_Pro_CRID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("Rec_Bill_Pro"))

	     {
                    if (this.getRec_Bill_Pro().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("Rec_Bill_Pro_CRID"))

	     {
                    if (this.getRec_Bill_Pro_CRID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("ReportFileID"))

	     {
                    if (this.getReportFileID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("CapabilityReportFileID"))

	     {
                    if (this.getCapabilityReportFileID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("DICOMFileID"))

	     {
                    if (this.getDICOMFileID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("LODID"))

	     {
                    if (this.getLODID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("Comments"))

	     {
                    if (this.getComments().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("AuditNotes"))

	     {
                    if (this.getAuditNotes().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("EncounterStatusID"))

	     {
                    if (this.getEncounterStatusID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("PaidToProviderCheck1Number"))

	     {
                    if (this.getPaidToProviderCheck1Number().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("PaidToProviderCheck1Amount"))

	     {
                    if (this.getPaidToProviderCheck1Amount().equals(new Double("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("PaidToProviderCheck1Date"))

	     {
                    if (this.getPaidToProviderCheck1Date().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("PaidToProviderCheck2Number"))

	     {
                    if (this.getPaidToProviderCheck2Number().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("PaidToProviderCheck2Amount"))

	     {
                    if (this.getPaidToProviderCheck2Amount().equals(new Double("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("PaidToProviderCheck2Date"))

	     {
                    if (this.getPaidToProviderCheck2Date().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("PaidToProviderCheck3Number"))

	     {
                    if (this.getPaidToProviderCheck3Number().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("PaidToProviderCheck3Amount"))

	     {
                    if (this.getPaidToProviderCheck3Amount().equals(new Double("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("PaidToProviderCheck3Date"))

	     {
                    if (this.getPaidToProviderCheck3Date().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("SentTo_ReqRec_RefDr"))

	     {
                    if (this.getSentTo_ReqRec_RefDr().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("SentTo_ReqRec_RefDr_CRID"))

	     {
                    if (this.getSentTo_ReqRec_RefDr_CRID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("SentTo_ReqRec_Adj"))

	     {
                    if (this.getSentTo_ReqRec_Adj().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("SentTo_ReqRec_Adj_CRID"))

	     {
                    if (this.getSentTo_ReqRec_Adj_CRID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("TimeTrack_ReqRec"))

	     {
                    if (this.getTimeTrack_ReqRec().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("TimeTrack_ReqRec_UserID"))

	     {
                    if (this.getTimeTrack_ReqRec_UserID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("TimeTrack_ReqCreated"))

	     {
                    if (this.getTimeTrack_ReqCreated().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("TimeTrack_ReqCreated_UserID"))

	     {
                    if (this.getTimeTrack_ReqCreated_UserID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("TimeTrack_ReqProc"))

	     {
                    if (this.getTimeTrack_ReqProc().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("TimeTrack_ReqProc_UserID"))

	     {
                    if (this.getTimeTrack_ReqProc_UserID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("TimeTrack_ReqSched"))

	     {
                    if (this.getTimeTrack_ReqSched().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("TimeTrack_ReqSched_UserID"))

	     {
                    if (this.getTimeTrack_ReqSched_UserID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("TimeTrack_ReqDelivered"))

	     {
                    if (this.getTimeTrack_ReqDelivered().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("TimeTrack_ReqDelivered_UserID"))

	     {
                    if (this.getTimeTrack_ReqDelivered_UserID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("TimeTrack_ReqPaidIn"))

	     {
                    if (this.getTimeTrack_ReqPaidIn().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("TimeTrack_ReqPaidIn_UserID"))

	     {
                    if (this.getTimeTrack_ReqPaidIn_UserID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("TimeTrack_ReqPaidOut"))

	     {
                    if (this.getTimeTrack_ReqPaidOut().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("TimeTrack_ReqPaidOut_UserID"))

	     {
                    if (this.getTimeTrack_ReqPaidOut_UserID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("ProviderInvoiceID"))

	     {
                    if (this.getProviderInvoiceID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("TimeTrack_ReqRec_RCodeID"))

	     {
                    if (this.getTimeTrack_ReqRec_RCodeID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("TimeTrack_ReqCreated_RCodeID"))

	     {
                    if (this.getTimeTrack_ReqCreated_RCodeID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("TimeTrack_ReqProc_RCodeID"))

	     {
                    if (this.getTimeTrack_ReqProc_RCodeID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("TimeTrack_ReqSched_RCodeID"))

	     {
                    if (this.getTimeTrack_ReqSched_RCodeID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("TimeTrack_ReqDelivered_RCodeID"))

	     {
                    if (this.getTimeTrack_ReqDelivered_RCodeID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("TimeTrack_ReqPaidIn_RCodeID"))

	     {
                    if (this.getTimeTrack_ReqPaidIn_RCodeID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("TimeTrack_ReqPaidOut_RCodeID"))

	     {
                    if (this.getTimeTrack_ReqPaidOut_RCodeID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("TimeTrack_ReqApproved_RCodeID"))

	     {
                    if (this.getTimeTrack_ReqApproved_RCodeID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("TimeTrack_ReqApproved"))

	     {
                    if (this.getTimeTrack_ReqApproved().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("TimeTrack_ReqApproved_UserID"))

	     {
                    if (this.getTimeTrack_ReqApproved_UserID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("SentTo_ReqRec_Adm"))

	     {
                    if (this.getSentTo_ReqRec_Adm().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("SentTo_ReqRec_Adm_CRID"))

	     {
                    if (this.getSentTo_ReqRec_Adm_CRID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("SentTo_SP_Adm"))

	     {
                    if (this.getSentTo_SP_Adm().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("SentTo_SP_Adm_CRID"))

	     {
                    if (this.getSentTo_SP_Adm_CRID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("SentToAdm"))

	     {
                    if (this.getSentToAdm().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("SentToAdm_CRID"))

	     {
                    if (this.getSentToAdm_CRID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("TimeTrack_ReqInitialAppointment_RCodeID"))

	     {
                    if (this.getTimeTrack_ReqInitialAppointment_RCodeID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("TimeTrack_ReqInitialAppointment"))

	     {
                    if (this.getTimeTrack_ReqInitialAppointment().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("TimeTrack_ReqInitialAppointment_UserID"))

	     {
                    if (this.getTimeTrack_ReqInitialAppointment_UserID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("TimeTrack_ReqRxReview_RCodeID"))

	     {
                    if (this.getTimeTrack_ReqRxReview_RCodeID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("TimeTrack_ReqRxReview"))

	     {
                    if (this.getTimeTrack_ReqRxReview().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("TimeTrack_ReqRxReview_UserID"))

	     {
                    if (this.getTimeTrack_ReqRxReview_UserID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("TimeTrack_ReqRpReview_RCodeID"))

	     {
                    if (this.getTimeTrack_ReqRpReview_RCodeID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("TimeTrack_ReqRpReview"))

	     {
                    if (this.getTimeTrack_ReqRpReview().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("TimeTrack_ReqRpReview_UserID"))

	     {
                    if (this.getTimeTrack_ReqRpReview_UserID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("NextActionNotes"))

	     {
                    if (this.getNextActionNotes().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("VoidLeakReasonID"))

	     {
                    if (this.getVoidLeakReasonID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("ExportPaymentToQB"))

	     {
                    if (this.getExportPaymentToQB().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("SentTo_SP_NCM"))

	     {
                    if (this.getSentTo_SP_NCM().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("SentTo_SP_NCM_CRID"))

	     {
                    if (this.getSentTo_SP_NCM_CRID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("SentTo_RP_NCM"))

	     {
                    if (this.getSentTo_RP_NCM().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("SentTo_RP_NCM_CRID"))

	     {
                    if (this.getSentTo_RP_NCM_CRID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("SentTo_ReqRec_NCM"))

	     {
                    if (this.getSentTo_ReqRec_NCM().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("SentTo_ReqRec_NCM_CRID"))

	     {
                    if (this.getSentTo_ReqRec_NCM_CRID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("isSTAT"))

	     {
                    if (this.getisSTAT().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("RequiresFilms"))

	     {
                    if (this.getRequiresFilms().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("HasBeenRescheduled"))

	     {
                    if (this.getHasBeenRescheduled().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("SentTo_SP_Adm2"))

	     {
                    if (this.getSentTo_SP_Adm2().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("SentTo_SP_Adm2_CRID"))

	     {
                    if (this.getSentTo_SP_Adm2_CRID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("SentTo_RP_Adm2"))

	     {
                    if (this.getSentTo_RP_Adm2().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("SentTo_RP_Adm2_CRID"))

	     {
                    if (this.getSentTo_RP_Adm2_CRID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("SentTo_ReqRec_Adm2"))

	     {
                    if (this.getSentTo_ReqRec_Adm2().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("SentTo_ReqRec_Adm2_CRID"))

	     {
                    if (this.getSentTo_ReqRec_Adm2_CRID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("SentTo_SP_Adm3"))

	     {
                    if (this.getSentTo_SP_Adm3().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("SentTo_SP_Adm3_CRID"))

	     {
                    if (this.getSentTo_SP_Adm3_CRID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("SentTo_RP_Adm3"))

	     {
                    if (this.getSentTo_RP_Adm3().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("SentTo_RP_Adm3_CRID"))

	     {
                    if (this.getSentTo_RP_Adm3_CRID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("SentTo_ReqRec_Adm3"))

	     {
                    if (this.getSentTo_ReqRec_Adm3().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("SentTo_ReqRec_Adm3_CRID"))

	     {
                    if (this.getSentTo_ReqRec_Adm3_CRID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("SentTo_SP_Adm4"))

	     {
                    if (this.getSentTo_SP_Adm4().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("SentTo_SP_Adm4_CRID"))

	     {
                    if (this.getSentTo_SP_Adm4_CRID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("SentTo_RP_Adm4"))

	     {
                    if (this.getSentTo_RP_Adm4().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("SentTo_RP_Adm4_CRID"))

	     {
                    if (this.getSentTo_RP_Adm4_CRID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("SentTo_ReqRec_Adm4"))

	     {
                    if (this.getSentTo_ReqRec_Adm4().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("SentTo_ReqRec_Adm4_CRID"))

	     {
                    if (this.getSentTo_ReqRec_Adm4_CRID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("RequiresAgeInjury"))

	     {
                    if (this.getRequiresAgeInjury().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("isCourtesy"))

	     {
                    if (this.getisCourtesy().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("BillingHCFA_ToPayer_FileID"))

	     {
                    if (this.getBillingHCFA_ToPayer_FileID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("BillingHCFA_FromProvider_FileID"))

	     {
                    if (this.getBillingHCFA_FromProvider_FileID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("isRetro"))

	     {
                    if (this.getisRetro().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("ExportAPToQB"))

	     {
                    if (this.getExportAPToQB().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("ExportARToQB"))

	     {
                    if (this.getExportARToQB().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("AmountBilledByProvider"))

	     {
                    if (this.getAmountBilledByProvider().equals(new Double("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("SeeNetDev_Flagged"))

	     {
                    if (this.getSeeNetDev_Flagged().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("SeeNetDev_ReqSentToNetDev"))

	     {
                    if (this.getSeeNetDev_ReqSentToNetDev().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("SeeNetDev_ReqSentToNetDev_UserID"))

	     {
                    if (this.getSeeNetDev_ReqSentToNetDev_UserID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("SeeNetDev_Waiting"))

	     {
                    if (this.getSeeNetDev_Waiting().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("PatientAvailability"))

	     {
                    if (this.getPatientAvailability().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("RequiresOnlineImage"))

	     {
                    if (this.getRequiresOnlineImage().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("Pricing_Structure"))

	     {
                    if (this.getPricing_Structure().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("SeeNetDev_SelectedPracticeID"))

	     {
                    if (this.getSeeNetDev_SelectedPracticeID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("ActionDate_OnlineImageRequest"))

	     {
                    if (this.getActionDate_OnlineImageRequest().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("ActionDate_ReportRequest"))

	     {
                    if (this.getActionDate_ReportRequest().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("Void_UserID"))

	     {
                    if (this.getVoid_UserID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("RequiresHandCarryCD"))

	     {
                    if (this.getRequiresHandCarryCD().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("ReportFollowUpCounter"))

	     {
                    if (this.getReportFollowUpCounter().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("ProblemCategory"))

	     {
                    if (this.getProblemCategory().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("ProblemDescription"))

	     {
                    if (this.getProblemDescription().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("AgeCommentLock"))

	     {
                    if (this.getAgeCommentLock().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("SentTo_ReqRec_Pt"))

	     {
                    if (this.getSentTo_ReqRec_Pt().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("SentToPt"))

	     {
                    if (this.getSentToPt().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("SentTo_SP_Pt"))

	     {
                    if (this.getSentTo_SP_Pt().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("NIDReferralSource"))

	     {
                    if (this.getNIDReferralSource().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("NIDReferralCode"))

	     {
                    if (this.getNIDReferralCode().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("CCTransactionID"))

	     {
                    if (this.getCCTransactionID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("isVIP"))

	     {
                    if (this.getisVIP().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("HCFAinvoice"))

	     {
                    if (this.getHCFAinvoice().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	    }
	    catch(Exception e2)
	    {}
	    return myVal;
	}

    public boolean isExpired(String fieldN,int daysVector)
    {
        return isExpired(fieldN,new java.util.Date(),daysVector);
    }

    public boolean isExpired(String fieldN,java.util.Date refDate,int daysVector)
    {
	     boolean myVal = false;
	     try
	     {
        java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat("yyyy-MM-dd");

        if (fieldN.equals("UniqueCreateDate"))

	     {
                    if (this.isExpired(this.getUniqueCreateDate(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

        if (fieldN.equals("UniqueModifyDate"))

	     {
                    if (this.isExpired(this.getUniqueModifyDate(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

        if (fieldN.equals("DateOfService"))

	     {
                    if (this.isExpired(this.getDateOfService(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

        if (fieldN.equals("NextActionDate"))

	     {
                    if (this.isExpired(this.getNextActionDate(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

        if (fieldN.equals("SentToRefDr"))

	     {
                    if (this.isExpired(this.getSentToRefDr(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

        if (fieldN.equals("SentToAdj"))

	     {
                    if (this.isExpired(this.getSentToAdj(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

        if (fieldN.equals("SentToIC"))

	     {
                    if (this.isExpired(this.getSentToIC(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

        if (fieldN.equals("SentTo_Appt_Conf_Pat"))

	     {
                    if (this.isExpired(this.getSentTo_Appt_Conf_Pat(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

        if (fieldN.equals("SentTo_SP_Pat"))

	     {
                    if (this.isExpired(this.getSentTo_SP_Pat(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

        if (fieldN.equals("SentTo_SP_RefDr"))

	     {
                    if (this.isExpired(this.getSentTo_SP_RefDr(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

        if (fieldN.equals("SentTo_SP_Adj"))

	     {
                    if (this.isExpired(this.getSentTo_SP_Adj(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

        if (fieldN.equals("SentTo_SP_IC"))

	     {
                    if (this.isExpired(this.getSentTo_SP_IC(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

        if (fieldN.equals("SentTo_DataProc_RefDr"))

	     {
                    if (this.isExpired(this.getSentTo_DataProc_RefDr(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

        if (fieldN.equals("SentTo_DataProc_Adj"))

	     {
                    if (this.isExpired(this.getSentTo_DataProc_Adj(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

        if (fieldN.equals("SentTo_Bill_Pay"))

	     {
                    if (this.isExpired(this.getSentTo_Bill_Pay(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

        if (fieldN.equals("Rec_Bill_Pay"))

	     {
                    if (this.isExpired(this.getRec_Bill_Pay(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

        if (fieldN.equals("SentTo_Bill_Pro"))

	     {
                    if (this.isExpired(this.getSentTo_Bill_Pro(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

        if (fieldN.equals("Rec_Bill_Pro"))

	     {
                    if (this.isExpired(this.getRec_Bill_Pro(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

        if (fieldN.equals("PaidToProviderCheck1Date"))

	     {
                    if (this.isExpired(this.getPaidToProviderCheck1Date(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

        if (fieldN.equals("PaidToProviderCheck2Date"))

	     {
                    if (this.isExpired(this.getPaidToProviderCheck2Date(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

        if (fieldN.equals("PaidToProviderCheck3Date"))

	     {
                    if (this.isExpired(this.getPaidToProviderCheck3Date(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

        if (fieldN.equals("SentTo_ReqRec_RefDr"))

	     {
                    if (this.isExpired(this.getSentTo_ReqRec_RefDr(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

        if (fieldN.equals("SentTo_ReqRec_Adj"))

	     {
                    if (this.isExpired(this.getSentTo_ReqRec_Adj(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

        if (fieldN.equals("TimeTrack_ReqRec"))

	     {
                    if (this.isExpired(this.getTimeTrack_ReqRec(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

        if (fieldN.equals("TimeTrack_ReqCreated"))

	     {
                    if (this.isExpired(this.getTimeTrack_ReqCreated(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

        if (fieldN.equals("TimeTrack_ReqProc"))

	     {
                    if (this.isExpired(this.getTimeTrack_ReqProc(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

        if (fieldN.equals("TimeTrack_ReqSched"))

	     {
                    if (this.isExpired(this.getTimeTrack_ReqSched(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

        if (fieldN.equals("TimeTrack_ReqDelivered"))

	     {
                    if (this.isExpired(this.getTimeTrack_ReqDelivered(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

        if (fieldN.equals("TimeTrack_ReqPaidIn"))

	     {
                    if (this.isExpired(this.getTimeTrack_ReqPaidIn(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

        if (fieldN.equals("TimeTrack_ReqPaidOut"))

	     {
                    if (this.isExpired(this.getTimeTrack_ReqPaidOut(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

        if (fieldN.equals("TimeTrack_ReqApproved"))

	     {
                    if (this.isExpired(this.getTimeTrack_ReqApproved(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

        if (fieldN.equals("SentTo_ReqRec_Adm"))

	     {
                    if (this.isExpired(this.getSentTo_ReqRec_Adm(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

        if (fieldN.equals("SentTo_SP_Adm"))

	     {
                    if (this.isExpired(this.getSentTo_SP_Adm(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

        if (fieldN.equals("SentToAdm"))

	     {
                    if (this.isExpired(this.getSentToAdm(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

        if (fieldN.equals("TimeTrack_ReqInitialAppointment"))

	     {
                    if (this.isExpired(this.getTimeTrack_ReqInitialAppointment(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

        if (fieldN.equals("TimeTrack_ReqRxReview"))

	     {
                    if (this.isExpired(this.getTimeTrack_ReqRxReview(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

        if (fieldN.equals("TimeTrack_ReqRpReview"))

	     {
                    if (this.isExpired(this.getTimeTrack_ReqRpReview(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

        if (fieldN.equals("ExportPaymentToQB"))

	     {
                    if (this.isExpired(this.getExportPaymentToQB(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

        if (fieldN.equals("SentTo_SP_NCM"))

	     {
                    if (this.isExpired(this.getSentTo_SP_NCM(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

        if (fieldN.equals("SentTo_RP_NCM"))

	     {
                    if (this.isExpired(this.getSentTo_RP_NCM(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

        if (fieldN.equals("SentTo_ReqRec_NCM"))

	     {
                    if (this.isExpired(this.getSentTo_ReqRec_NCM(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

        if (fieldN.equals("SentTo_SP_Adm2"))

	     {
                    if (this.isExpired(this.getSentTo_SP_Adm2(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

        if (fieldN.equals("SentTo_RP_Adm2"))

	     {
                    if (this.isExpired(this.getSentTo_RP_Adm2(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

        if (fieldN.equals("SentTo_ReqRec_Adm2"))

	     {
                    if (this.isExpired(this.getSentTo_ReqRec_Adm2(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

        if (fieldN.equals("SentTo_SP_Adm3"))

	     {
                    if (this.isExpired(this.getSentTo_SP_Adm3(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

        if (fieldN.equals("SentTo_RP_Adm3"))

	     {
                    if (this.isExpired(this.getSentTo_RP_Adm3(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

        if (fieldN.equals("SentTo_ReqRec_Adm3"))

	     {
                    if (this.isExpired(this.getSentTo_ReqRec_Adm3(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

        if (fieldN.equals("SentTo_SP_Adm4"))

	     {
                    if (this.isExpired(this.getSentTo_SP_Adm4(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

        if (fieldN.equals("SentTo_RP_Adm4"))

	     {
                    if (this.isExpired(this.getSentTo_RP_Adm4(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

        if (fieldN.equals("SentTo_ReqRec_Adm4"))

	     {
                    if (this.isExpired(this.getSentTo_ReqRec_Adm4(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

        if (fieldN.equals("ExportAPToQB"))

	     {
                    if (this.isExpired(this.getExportAPToQB(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

        if (fieldN.equals("ExportARToQB"))

	     {
                    if (this.isExpired(this.getExportARToQB(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

        if (fieldN.equals("SeeNetDev_ReqSentToNetDev"))

	     {
                    if (this.isExpired(this.getSeeNetDev_ReqSentToNetDev(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

        if (fieldN.equals("ActionDate_OnlineImageRequest"))

	     {
                    if (this.isExpired(this.getActionDate_OnlineImageRequest(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

        if (fieldN.equals("ActionDate_ReportRequest"))

	     {
                    if (this.isExpired(this.getActionDate_ReportRequest(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

        if (fieldN.equals("SentTo_ReqRec_Pt"))

	     {
                    if (this.isExpired(this.getSentTo_ReqRec_Pt(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

        if (fieldN.equals("SentToPt"))

	     {
                    if (this.isExpired(this.getSentToPt(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

        if (fieldN.equals("SentTo_SP_Pt"))

	     {
                    if (this.isExpired(this.getSentTo_SP_Pt(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

	    }
	    catch(Exception e2)
	    {}
	    return myVal;
	}

    public java.util.Date getDateField(String fieldN)
    {
	     java.util.Date myVal = null;
	     try
	     {
        java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat("yyyy-MM-dd");

        if (fieldN.equals("UniqueCreateDate"))

	     {
             myVal = this.getUniqueCreateDate();
        }

        if (fieldN.equals("UniqueModifyDate"))

	     {
             myVal = this.getUniqueModifyDate();
        }

        if (fieldN.equals("DateOfService"))

	     {
             myVal = this.getDateOfService();
        }

        if (fieldN.equals("NextActionDate"))

	     {
             myVal = this.getNextActionDate();
        }

        if (fieldN.equals("SentToRefDr"))

	     {
             myVal = this.getSentToRefDr();
        }

        if (fieldN.equals("SentToAdj"))

	     {
             myVal = this.getSentToAdj();
        }

        if (fieldN.equals("SentToIC"))

	     {
             myVal = this.getSentToIC();
        }

        if (fieldN.equals("SentTo_Appt_Conf_Pat"))

	     {
             myVal = this.getSentTo_Appt_Conf_Pat();
        }

        if (fieldN.equals("SentTo_SP_Pat"))

	     {
             myVal = this.getSentTo_SP_Pat();
        }

        if (fieldN.equals("SentTo_SP_RefDr"))

	     {
             myVal = this.getSentTo_SP_RefDr();
        }

        if (fieldN.equals("SentTo_SP_Adj"))

	     {
             myVal = this.getSentTo_SP_Adj();
        }

        if (fieldN.equals("SentTo_SP_IC"))

	     {
             myVal = this.getSentTo_SP_IC();
        }

        if (fieldN.equals("SentTo_DataProc_RefDr"))

	     {
             myVal = this.getSentTo_DataProc_RefDr();
        }

        if (fieldN.equals("SentTo_DataProc_Adj"))

	     {
             myVal = this.getSentTo_DataProc_Adj();
        }

        if (fieldN.equals("SentTo_Bill_Pay"))

	     {
             myVal = this.getSentTo_Bill_Pay();
        }

        if (fieldN.equals("Rec_Bill_Pay"))

	     {
             myVal = this.getRec_Bill_Pay();
        }

        if (fieldN.equals("SentTo_Bill_Pro"))

	     {
             myVal = this.getSentTo_Bill_Pro();
        }

        if (fieldN.equals("Rec_Bill_Pro"))

	     {
             myVal = this.getRec_Bill_Pro();
        }

        if (fieldN.equals("PaidToProviderCheck1Date"))

	     {
             myVal = this.getPaidToProviderCheck1Date();
        }

        if (fieldN.equals("PaidToProviderCheck2Date"))

	     {
             myVal = this.getPaidToProviderCheck2Date();
        }

        if (fieldN.equals("PaidToProviderCheck3Date"))

	     {
             myVal = this.getPaidToProviderCheck3Date();
        }

        if (fieldN.equals("SentTo_ReqRec_RefDr"))

	     {
             myVal = this.getSentTo_ReqRec_RefDr();
        }

        if (fieldN.equals("SentTo_ReqRec_Adj"))

	     {
             myVal = this.getSentTo_ReqRec_Adj();
        }

        if (fieldN.equals("TimeTrack_ReqRec"))

	     {
             myVal = this.getTimeTrack_ReqRec();
        }

        if (fieldN.equals("TimeTrack_ReqCreated"))

	     {
             myVal = this.getTimeTrack_ReqCreated();
        }

        if (fieldN.equals("TimeTrack_ReqProc"))

	     {
             myVal = this.getTimeTrack_ReqProc();
        }

        if (fieldN.equals("TimeTrack_ReqSched"))

	     {
             myVal = this.getTimeTrack_ReqSched();
        }

        if (fieldN.equals("TimeTrack_ReqDelivered"))

	     {
             myVal = this.getTimeTrack_ReqDelivered();
        }

        if (fieldN.equals("TimeTrack_ReqPaidIn"))

	     {
             myVal = this.getTimeTrack_ReqPaidIn();
        }

        if (fieldN.equals("TimeTrack_ReqPaidOut"))

	     {
             myVal = this.getTimeTrack_ReqPaidOut();
        }

        if (fieldN.equals("TimeTrack_ReqApproved"))

	     {
             myVal = this.getTimeTrack_ReqApproved();
        }

        if (fieldN.equals("SentTo_ReqRec_Adm"))

	     {
             myVal = this.getSentTo_ReqRec_Adm();
        }

        if (fieldN.equals("SentTo_SP_Adm"))

	     {
             myVal = this.getSentTo_SP_Adm();
        }

        if (fieldN.equals("SentToAdm"))

	     {
             myVal = this.getSentToAdm();
        }

        if (fieldN.equals("TimeTrack_ReqInitialAppointment"))

	     {
             myVal = this.getTimeTrack_ReqInitialAppointment();
        }

        if (fieldN.equals("TimeTrack_ReqRxReview"))

	     {
             myVal = this.getTimeTrack_ReqRxReview();
        }

        if (fieldN.equals("TimeTrack_ReqRpReview"))

	     {
             myVal = this.getTimeTrack_ReqRpReview();
        }

        if (fieldN.equals("ExportPaymentToQB"))

	     {
             myVal = this.getExportPaymentToQB();
        }

        if (fieldN.equals("SentTo_SP_NCM"))

	     {
             myVal = this.getSentTo_SP_NCM();
        }

        if (fieldN.equals("SentTo_RP_NCM"))

	     {
             myVal = this.getSentTo_RP_NCM();
        }

        if (fieldN.equals("SentTo_ReqRec_NCM"))

	     {
             myVal = this.getSentTo_ReqRec_NCM();
        }

        if (fieldN.equals("SentTo_SP_Adm2"))

	     {
             myVal = this.getSentTo_SP_Adm2();
        }

        if (fieldN.equals("SentTo_RP_Adm2"))

	     {
             myVal = this.getSentTo_RP_Adm2();
        }

        if (fieldN.equals("SentTo_ReqRec_Adm2"))

	     {
             myVal = this.getSentTo_ReqRec_Adm2();
        }

        if (fieldN.equals("SentTo_SP_Adm3"))

	     {
             myVal = this.getSentTo_SP_Adm3();
        }

        if (fieldN.equals("SentTo_RP_Adm3"))

	     {
             myVal = this.getSentTo_RP_Adm3();
        }

        if (fieldN.equals("SentTo_ReqRec_Adm3"))

	     {
             myVal = this.getSentTo_ReqRec_Adm3();
        }

        if (fieldN.equals("SentTo_SP_Adm4"))

	     {
             myVal = this.getSentTo_SP_Adm4();
        }

        if (fieldN.equals("SentTo_RP_Adm4"))

	     {
             myVal = this.getSentTo_RP_Adm4();
        }

        if (fieldN.equals("SentTo_ReqRec_Adm4"))

	     {
             myVal = this.getSentTo_ReqRec_Adm4();
        }

        if (fieldN.equals("ExportAPToQB"))

	     {
             myVal = this.getExportAPToQB();
        }

        if (fieldN.equals("ExportARToQB"))

	     {
             myVal = this.getExportARToQB();
        }

        if (fieldN.equals("SeeNetDev_ReqSentToNetDev"))

	     {
             myVal = this.getSeeNetDev_ReqSentToNetDev();
        }

        if (fieldN.equals("ActionDate_OnlineImageRequest"))

	     {
             myVal = this.getActionDate_OnlineImageRequest();
        }

        if (fieldN.equals("ActionDate_ReportRequest"))

	     {
             myVal = this.getActionDate_ReportRequest();
        }

        if (fieldN.equals("SentTo_ReqRec_Pt"))

	     {
             myVal = this.getSentTo_ReqRec_Pt();
        }

        if (fieldN.equals("SentToPt"))

	     {
             myVal = this.getSentToPt();
        }

        if (fieldN.equals("SentTo_SP_Pt"))

	     {
             myVal = this.getSentTo_SP_Pt();
        }

	    }
	    catch(Exception e2)
	    {}
	    return myVal;
	}
	public boolean runFieldSecurity = false;
	public Integer UserSecurityID = null;
   public Integer iSecurityCheck = null;
}    // End of bltNIM3_Encounter class definition
