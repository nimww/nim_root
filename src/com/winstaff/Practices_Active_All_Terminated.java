package com.winstaff;
import java.io.*;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

public class Practices_Active_All_Terminated{
	static HSSFWorkbook hwb = new HSSFWorkbook();
	public static void main(String[]args) throws IOException{
		String theBody = "Beef Up Reports! ";
		String theSubject = "Provider List Sent out";
		try {
			getNetDevContractPerformance();
			email("giovanni.hernandez@nextimagemedical.com","support@nextimagemedical.com",theSubject,theBody);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	public static void getNetDevContractPerformance() throws IOException{
		String new_query = "select distinct on (prac.practiceid) prac.practiceid, prac.officeaddress1,prac.officeaddress2, prac.officecity, tsta.shortstate as officestate, prac.officezip, prac.officefederaltaxid, prac.npinumber,to_date(prac.initialcontractdate::text, 'YYYY-MM-DD') as effective_date\n" +
				"from tpracticemaster as prac\n" +
				"inner join tstateli tsta\n" +
				"on tsta.stateid = prac.officestateid\n" +
				"where prac.terminated != 1\n" +
				"and prac.contractingstatusid in (2,8,10,11,12)\n" +
				"and prac.initialcontractdate Between (now() - '1 month' :: INTERVAL):: TIMESTAMP AND now()\n" +
				"order by prac.practiceid desc";
		String terminated_query = "select distinct on (prac.practiceid) prac.practiceid, prac.initialcontractdate, prac.officeaddress1,prac.officeaddress2, prac.officecity, tsta.shortstate as officestate, prac.officezip, prac.officefederaltaxid, prac.npinumber,to_date(prac.terminateddate::text, 'YYYY-MM-DD') as terminated_date\n" +
				"from tpracticemaster as prac\n" +
				"inner join tstateli tsta\n" +
				"on tsta.stateid = prac.officestateid\n" +
				"WHERE prac.terminateddate Between (now() - '1 month' :: INTERVAL):: TIMESTAMP AND now()\n" +
				"and prac.terminated = 1\n" +
				"order by prac.practiceid desc";
		String allActive_query = "select distinct on (prac.practiceid) prac.practiceid, prac.officeaddress1, prac.officeaddress2, prac.officecity, tsta.shortstate as officestate, prac.officezip, prac.officefederaltaxid, prac.npinumber,to_date(prac.initialcontractdate::text, 'YYYY-MM-DD') as effective_date\n" +
				"from tpracticemaster as prac\n" +
				"inner join tstateli tsta\n" +
				"on tsta.stateid = prac.officestateid\n" +
				"where prac.terminated != 1\n" +
				"and prac.contractingstatusid in (1,2,8,10,11,12)\n" +
				"order by prac.practiceid desc";
		searchDB2 ss = new searchDB2();
		ResultSet rs1 = ss.executeStatement(new_query);
		ResultSet rs2 = ss.executeStatement(terminated_query);
		ResultSet rs3 = ss.executeStatement(allActive_query);
		String npl = "export/net_dev_contract_performance.xls";
		HSSFWorkbook hwb = new HSSFWorkbook();
		HSSFSheet sheet1 = hwb.createSheet("New Provider List");
		HSSFSheet sheet2 = hwb.createSheet("Terminated Provider List");
		HSSFSheet sheet3 = hwb.createSheet("Active Provider List");
		HSSFRow rh1 = sheet1.createRow((int) 0);
		HSSFRow rh2 = sheet2.createRow((int) 0);
		HSSFRow rh3 = sheet3.createRow((int) 0);
		
		rh1.createCell((int) 0).setCellValue("practiceid");
		rh1.createCell((int) 1).setCellValue("officeaddress1");
		rh1.createCell((int) 2).setCellValue("officeaddress2");
		rh1.createCell((int) 3).setCellValue("officecity");
		rh1.createCell((int) 4).setCellValue("officestate");
		rh1.createCell((int) 4).setCellValue("officezip");
		rh1.createCell((int) 5).setCellValue("officefederaltaxid");
		rh1.createCell((int) 6).setCellValue("npinumber");
		rh1.createCell((int) 7).setCellValue("effective_date");
		int i = 1;
		try {
			while(rs1.next()){
				HSSFRow row1 = sheet2.createRow((int) i);
				row1.createCell((int) 0).setCellValue(rs1.getString("practiceid"));
				row1.createCell((int) 1).setCellValue(rs1.getString("officeaddress1"));
				row1.createCell((int) 2).setCellValue(rs1.getString("officeaddress2"));
				row1.createCell((int) 3).setCellValue(rs1.getString("officecity"));
				row1.createCell((int) 4).setCellValue(rs1.getString("officestate"));
				row1.createCell((int) 5).setCellValue(rs1.getString("officezip"));
				row1.createCell((int) 6).setCellValue(rs1.getString("officefederaltaxid"));
				row1.createCell((int) 7).setCellValue(rs1.getString("npinumber"));
				row1.createCell((int) 8).setCellValue(rs1.getString("effective_date"));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		rh2.createCell((int) 0).setCellValue("practiceid");
		rh2.createCell((int) 1).setCellValue("initialcontractdate");
		rh2.createCell((int) 2).setCellValue("officeaddress1");
		rh2.createCell((int) 3).setCellValue("officeaddress2");
		rh2.createCell((int) 4).setCellValue("officecity");
		rh2.createCell((int) 5).setCellValue("officestate");
		rh2.createCell((int) 6).setCellValue("officezip");
		rh2.createCell((int) 7).setCellValue("officefederaltaxid");
		rh2.createCell((int) 8).setCellValue("npinumber");
		rh2.createCell((int) 9).setCellValue("terminated_date");
		int j = 1;
		try {
			while(rs2.next()){
				HSSFRow row2 = sheet2.createRow((int) j);
				row2.createCell((int) 0).setCellValue(rs2.getString("practiceid"));
				row2.createCell((int) 1).setCellValue(rs2.getString("initialcontractdate"));
				row2.createCell((int) 2).setCellValue(rs2.getString("officeaddress1"));
				row2.createCell((int) 3).setCellValue(rs2.getString("officeaddress2"));
				row2.createCell((int) 4).setCellValue(rs2.getString("officecity"));
				row2.createCell((int) 5).setCellValue(rs2.getString("officestate"));
				row2.createCell((int) 6).setCellValue(rs2.getString("officezip"));
				row2.createCell((int) 7).setCellValue(rs2.getString("officefederaltaxid"));
				row2.createCell((int) 8).setCellValue(rs2.getString("npinumber"));
				row2.createCell((int) 9).setCellValue(rs2.getString("terminated_date"));
				j++;
			}
			}catch(Exception e){
			}
		rh3.createCell((int) 0).setCellValue("practiceid");
		rh3.createCell((int) 1).setCellValue("officeaddress1");
		rh3.createCell((int) 2).setCellValue("officeaddress2");
		rh3.createCell((int) 3).setCellValue("officecity");
		rh3.createCell((int) 4).setCellValue("officestate");
		rh3.createCell((int) 5).setCellValue("officezip");
		rh3.createCell((int) 6).setCellValue("officefederaltaxid");
		rh3.createCell((int) 7).setCellValue("npinumber");
		rh3.createCell((int) 8).setCellValue("effective_date");
		int z = 1;
		try {
			while(rs3.next()){
				HSSFRow row3 = sheet3.createRow((int) z);
				row3.createCell((int) 0).setCellValue(rs3.getString("practiceid"));
				row3.createCell((int) 1).setCellValue(rs3.getString("officeaddress1"));
				row3.createCell((int) 2).setCellValue(rs3.getString("officeaddress2"));
				row3.createCell((int) 3).setCellValue(rs3.getString("officecity"));
				row3.createCell((int) 4).setCellValue(rs3.getString("officestate"));
				row3.createCell((int) 5).setCellValue(rs3.getString("officezip"));
				row3.createCell((int) 6).setCellValue(rs3.getString("officefederaltaxid"));
				row3.createCell((int) 7).setCellValue(rs3.getString("npinumber"));
				row3.createCell((int) 8).setCellValue(rs3.getString("effective_date"));
				z++;
			}
			
			}catch(Exception e){
			}
		FileOutputStream fileout = null;
		try {
			fileout = new FileOutputStream(npl);
			hwb.write(fileout);
			fileout.close();
			System.out.println("Net-dev Performance List!!..");	
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}finally{
			ss.closeAll();
		}
	}
	
	
	public static void email(String emailTo, String emailFrom, String theSubject, String theBody) throws SQLException{
		bltEmailTransaction et = new bltEmailTransaction();
		et.setEmailTo(emailTo);
		et.setEmailFrom(emailFrom);
		et.setEmailSubject(theSubject);
		et.setEmailBody(theBody);
		et.setEmailBodyType(emailType_V3.HTML_TYPE);
		et.setTransactionDate(new java.util.Date());
		et.commitData();
	}
	
	
	
	
	
	
	
	
	
	
}
