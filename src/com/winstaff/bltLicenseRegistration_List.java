package com.winstaff;
/*
 * bltLicenseRegistration_List.java
 *
 * Created: Tue Aug 26 12:34:31 PDT 2003
 */



import java.sql.*;
import com.winstaff.ListElement;


/**
 *
 * @author  Scott Ellis via listClassGenCode
 * @version 2.5
 */
public class bltLicenseRegistration_List extends db_BaseRelationTable
{
    /** Default constructor
    */    
    public bltLicenseRegistration_List ( Integer iPhysicianID )
    {
        super( "tLicenseRegistration", "LicenseRegistrationID", "LicenseRegistrationID", "PhysicianID", iPhysicianID );
    }   // End of bltLicenseRegistration_List()
    

    public void setField(String fieldN, Object fieldV)
    {DebugLogger.println("setField Method not implemented yet in classbltLicenseRegistration_List!");}

    public String getFieldType(String fieldN)
    {return null;}

    public bltLicenseRegistration_List ( Integer iPhysicianID, String extraWhere, String OrderBy )
    {
        super( "tLicenseRegistration", "LicenseRegistrationID", "LicenseRegistrationID", "PhysicianID", iPhysicianID, extraWhere,OrderBy );
    }   // End of bltLicenseRegistration_List()
     /** Method pull information from the result set and load into class attributes.
    * @param sFieldList Comma delimited list of fields for which data has been retrieved.
    * @param rs Result set from which to pull the data from.
    * @throws SQLException Passes on SQLException
    */      
    protected void transferData( ResultSet rs ) throws SQLException
    {
        while ( rs.next()  )
        {
            Integer         iValue              = new Integer( rs.getString( sLinkFieldName ) );
            Integer         iLinkRecordID       = new Integer( rs.getString( sUniqueIDFieldName ) );
            Object          oReferenceObject;
            ListElement     leNewElement;
            
            oReferenceObject    = new bltLicenseRegistration( iValue );
            
            leNewElement        = new ListElement( iLinkRecordID, oReferenceObject );
            
            vList.add( leNewElement );

        }
    }   // End of transferData()
}   // End of bltLicenseRegistration_List class

