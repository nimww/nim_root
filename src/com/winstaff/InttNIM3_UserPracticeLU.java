

package com.winstaff;

import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public interface InttNIM3_UserPracticeLU extends dbTableInterface
{

    public void setLookupID(Integer newValue);
    public Integer getLookupID();
    public void setUniqueCreateDate(Date newValue);
    public Date getUniqueCreateDate();
    public void setUniqueModifyDate(Date newValue);
    public Date getUniqueModifyDate();
    public void setUniqueModifyComments(String newValue);
    public String getUniqueModifyComments();
    public void setUserID(Integer newValue);
    public Integer getUserID();
    public void setPracticeID(Integer newValue);
    public Integer getPracticeID();
    public void setIsPreferred(Integer newValue);
    public Integer getIsPreferred();
    public void setIsBlocked(Integer newValue);
    public Integer getIsBlocked();
    public void setComments(String newValue);
    public String getComments();
}    // End of bltNIM3_UserPracticeLU class definition
