

package com.winstaff;

import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public class dbtUserAccount extends Object implements InttUserAccount
{

        db_NewBase    dbnbDB;

    public dbtUserAccount()
    {
        dbnbDB = new db_NewBase( "tUserAccount", "UserID" );

    }    // End of default constructor

    public dbtUserAccount( Integer iNewID )
    {
        dbnbDB = new db_NewBase( "tUserAccount", "UserID" );
        setUniqueID( iNewID );
    }    // End of Constructor knowing an ID

    public void commitData() throws SQLException
    {
            //no longer trapping/ignoring errors 2011-03-17
            dbnbDB.commitData();
    }    // End of commitData()

    public void commitDataForced() throws SQLException
    {
        dbnbDB.commitDataForced();
    }    // End of commitData()

    public Integer getUniqueID()
    {
        return dbnbDB.getUniqueID();
    }    // End of getUniqueID()

    public void setUniqueID( Integer iNewID )
    {
        dbnbDB.setUniqueID( iNewID );
    }    // End of setUniqueID()

    public void setModifyComments( String sNew )
    {
        dbnbDB.setModifyComments( sNew );
    }    // End of setUniqueID()


    public void setUserID(Integer newValue)
    {
                dbnbDB.setFieldData( "UserID", newValue.toString() );
    }

    public Integer getUserID()
    {
        String           sValue = dbnbDB.getFieldData( "UserID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setField(String fieldN, Object fieldV)
    {DebugLogger.println("setField Method not implemented yet in classUserAccount!");}

    public String getFieldType(String fieldN)
    {return null;}


    public void setUniqueCreateDate(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "UniqueCreateDate", formatter.format( newValue ) );
    }

    public Date getUniqueCreateDate()
    {
        String           sValue = dbnbDB.getFieldData( "UniqueCreateDate" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setUniqueModifyDate(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "UniqueModifyDate", formatter.format( newValue ) );
    }

    public Date getUniqueModifyDate()
    {
        String           sValue = dbnbDB.getFieldData( "UniqueModifyDate" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setUniqueModifyComments(String newValue)
    {
                dbnbDB.setFieldData( "UniqueModifyComments", newValue.toString() );
    }

    public String getUniqueModifyComments()
    {
        String           sValue = dbnbDB.getFieldData( "UniqueModifyComments" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setPLCID(Integer newValue)
    {
                dbnbDB.setFieldData( "PLCID", newValue.toString() );
    }

    public Integer getPLCID()
    {
        String           sValue = dbnbDB.getFieldData( "PLCID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setStartPage(String newValue)
    {
                dbnbDB.setFieldData( "StartPage", newValue.toString() );
    }

    public String getStartPage()
    {
        String           sValue = dbnbDB.getFieldData( "StartPage" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setAccountType(String newValue)
    {
                dbnbDB.setFieldData( "AccountType", newValue.toString() );
    }

    public String getAccountType()
    {
        String           sValue = dbnbDB.getFieldData( "AccountType" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setSecurityGroupID(Integer newValue)
    {
                dbnbDB.setFieldData( "SecurityGroupID", newValue.toString() );
    }

    public Integer getSecurityGroupID()
    {
        String           sValue = dbnbDB.getFieldData( "SecurityGroupID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setGenericSecurityGroupID(Integer newValue)
    {
                dbnbDB.setFieldData( "GenericSecurityGroupID", newValue.toString() );
    }

    public Integer getGenericSecurityGroupID()
    {
        String           sValue = dbnbDB.getFieldData( "GenericSecurityGroupID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setReferenceID(Integer newValue)
    {
                dbnbDB.setFieldData( "ReferenceID", newValue.toString() );
    }

    public Integer getReferenceID()
    {
        String           sValue = dbnbDB.getFieldData( "ReferenceID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setAccessType(Integer newValue)
    {
                dbnbDB.setFieldData( "AccessType", newValue.toString() );
    }

    public Integer getAccessType()
    {
        String           sValue = dbnbDB.getFieldData( "AccessType" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setStatus(Integer newValue)
    {
                dbnbDB.setFieldData( "Status", newValue.toString() );
    }

    public Integer getStatus()
    {
        String           sValue = dbnbDB.getFieldData( "Status" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setLogonUserName(String newValue)
    {
                dbnbDB.setFieldData( "LogonUserName", newValue.toString() );
    }

    public String getLogonUserName()
    {
        String           sValue = dbnbDB.getFieldData( "LogonUserName" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setLogonUserPassword(String newValue)
    {
                dbnbDB.setFieldData( "LogonUserPassword", newValue.toString() );
    }

    public String getLogonUserPassword()
    {
        String           sValue = dbnbDB.getFieldData( "LogonUserPassword" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setAttestKeyword1(String newValue)
    {
                dbnbDB.setFieldData( "AttestKeyword1", newValue.toString() );
    }

    public String getAttestKeyword1()
    {
        String           sValue = dbnbDB.getFieldData( "AttestKeyword1" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setAttestKeyword2(String newValue)
    {
                dbnbDB.setFieldData( "AttestKeyword2", newValue.toString() );
    }

    public String getAttestKeyword2()
    {
        String           sValue = dbnbDB.getFieldData( "AttestKeyword2" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setAttestKeywordTemp1(String newValue)
    {
                dbnbDB.setFieldData( "AttestKeywordTemp1", newValue.toString() );
    }

    public String getAttestKeywordTemp1()
    {
        String           sValue = dbnbDB.getFieldData( "AttestKeywordTemp1" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setAttestKeywordTemp2(String newValue)
    {
                dbnbDB.setFieldData( "AttestKeywordTemp2", newValue.toString() );
    }

    public String getAttestKeywordTemp2()
    {
        String           sValue = dbnbDB.getFieldData( "AttestKeywordTemp2" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setContactFirstName(String newValue)
    {
                dbnbDB.setFieldData( "ContactFirstName", newValue.toString() );
    }

    public String getContactFirstName()
    {
        String           sValue = dbnbDB.getFieldData( "ContactFirstName" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setContactLastName(String newValue)
    {
                dbnbDB.setFieldData( "ContactLastName", newValue.toString() );
    }

    public String getContactLastName()
    {
        String           sValue = dbnbDB.getFieldData( "ContactLastName" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setContactEmail(String newValue)
    {
                dbnbDB.setFieldData( "ContactEmail", newValue.toString() );
    }

    public String getContactEmail()
    {
        String           sValue = dbnbDB.getFieldData( "ContactEmail" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setContactEmail2(String newValue)
    {
                dbnbDB.setFieldData( "ContactEmail2", newValue.toString() );
    }

    public String getContactEmail2()
    {
        String           sValue = dbnbDB.getFieldData( "ContactEmail2" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setContactAddress1(String newValue)
    {
                dbnbDB.setFieldData( "ContactAddress1", newValue.toString() );
    }

    public String getContactAddress1()
    {
        String           sValue = dbnbDB.getFieldData( "ContactAddress1" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setContactAddress2(String newValue)
    {
                dbnbDB.setFieldData( "ContactAddress2", newValue.toString() );
    }

    public String getContactAddress2()
    {
        String           sValue = dbnbDB.getFieldData( "ContactAddress2" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setContactCity(String newValue)
    {
                dbnbDB.setFieldData( "ContactCity", newValue.toString() );
    }

    public String getContactCity()
    {
        String           sValue = dbnbDB.getFieldData( "ContactCity" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setContactStateID(Integer newValue)
    {
                dbnbDB.setFieldData( "ContactStateID", newValue.toString() );
    }

    public Integer getContactStateID()
    {
        String           sValue = dbnbDB.getFieldData( "ContactStateID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setContactProvince(String newValue)
    {
                dbnbDB.setFieldData( "ContactProvince", newValue.toString() );
    }

    public String getContactProvince()
    {
        String           sValue = dbnbDB.getFieldData( "ContactProvince" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setContactZIP(String newValue)
    {
                dbnbDB.setFieldData( "ContactZIP", newValue.toString() );
    }

    public String getContactZIP()
    {
        String           sValue = dbnbDB.getFieldData( "ContactZIP" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setContactCountryID(Integer newValue)
    {
                dbnbDB.setFieldData( "ContactCountryID", newValue.toString() );
    }

    public Integer getContactCountryID()
    {
        String           sValue = dbnbDB.getFieldData( "ContactCountryID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setContactPhone(String newValue)
    {
                dbnbDB.setFieldData( "ContactPhone", newValue.toString() );
    }

    public String getContactPhone()
    {
        String           sValue = dbnbDB.getFieldData( "ContactPhone" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setContactFax(String newValue)
    {
                dbnbDB.setFieldData( "ContactFax", newValue.toString() );
    }

    public String getContactFax()
    {
        String           sValue = dbnbDB.getFieldData( "ContactFax" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setContactMobile(String newValue)
    {
                dbnbDB.setFieldData( "ContactMobile", newValue.toString() );
    }

    public String getContactMobile()
    {
        String           sValue = dbnbDB.getFieldData( "ContactMobile" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setSecretQuestion(String newValue)
    {
                dbnbDB.setFieldData( "SecretQuestion", newValue.toString() );
    }

    public String getSecretQuestion()
    {
        String           sValue = dbnbDB.getFieldData( "SecretQuestion" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setSecretAnswer(String newValue)
    {
                dbnbDB.setFieldData( "SecretAnswer", newValue.toString() );
    }

    public String getSecretAnswer()
    {
        String           sValue = dbnbDB.getFieldData( "SecretAnswer" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setCreditCardFullName(String newValue)
    {
                dbnbDB.setFieldData( "CreditCardFullName", newValue.toString() );
    }

    public String getCreditCardFullName()
    {
        String           sValue = dbnbDB.getFieldData( "CreditCardFullName" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setCreditCardNumber(String newValue)
    {
                dbnbDB.setFieldData( "CreditCardNumber", newValue.toString() );
    }

    public String getCreditCardNumber()
    {
        String           sValue = dbnbDB.getFieldData( "CreditCardNumber" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setCreditCardType(String newValue)
    {
                dbnbDB.setFieldData( "CreditCardType", newValue.toString() );
    }

    public String getCreditCardType()
    {
        String           sValue = dbnbDB.getFieldData( "CreditCardType" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setCreditCardExpMonth(String newValue)
    {
                dbnbDB.setFieldData( "CreditCardExpMonth", newValue.toString() );
    }

    public String getCreditCardExpMonth()
    {
        String           sValue = dbnbDB.getFieldData( "CreditCardExpMonth" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setCreditCardExpYear(String newValue)
    {
                dbnbDB.setFieldData( "CreditCardExpYear", newValue.toString() );
    }

    public String getCreditCardExpYear()
    {
        String           sValue = dbnbDB.getFieldData( "CreditCardExpYear" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setCreditCardChargeDate(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "CreditCardChargeDate", formatter.format( newValue ) );
    }

    public Date getCreditCardChargeDate()
    {
        String           sValue = dbnbDB.getFieldData( "CreditCardChargeDate" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setCreditCardPostDate(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "CreditCardPostDate", formatter.format( newValue ) );
    }

    public Date getCreditCardPostDate()
    {
        String           sValue = dbnbDB.getFieldData( "CreditCardPostDate" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setAlertEmail(String newValue)
    {
                dbnbDB.setFieldData( "AlertEmail", newValue.toString() );
    }

    public String getAlertEmail()
    {
        String           sValue = dbnbDB.getFieldData( "AlertEmail" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setAlertDays(Integer newValue)
    {
                dbnbDB.setFieldData( "AlertDays", newValue.toString() );
    }

    public Integer getAlertDays()
    {
        String           sValue = dbnbDB.getFieldData( "AlertDays" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setBillingComments(String newValue)
    {
                dbnbDB.setFieldData( "BillingComments", newValue.toString() );
    }

    public String getBillingComments()
    {
        String           sValue = dbnbDB.getFieldData( "BillingComments" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setPromoCode(String newValue)
    {
                dbnbDB.setFieldData( "PromoCode", newValue.toString() );
    }

    public String getPromoCode()
    {
        String           sValue = dbnbDB.getFieldData( "PromoCode" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setCompanyType(Integer newValue)
    {
                dbnbDB.setFieldData( "CompanyType", newValue.toString() );
    }

    public Integer getCompanyType()
    {
        String           sValue = dbnbDB.getFieldData( "CompanyType" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setCompanyName(String newValue)
    {
                dbnbDB.setFieldData( "CompanyName", newValue.toString() );
    }

    public String getCompanyName()
    {
        String           sValue = dbnbDB.getFieldData( "CompanyName" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setCompanyAddress1(String newValue)
    {
                dbnbDB.setFieldData( "CompanyAddress1", newValue.toString() );
    }

    public String getCompanyAddress1()
    {
        String           sValue = dbnbDB.getFieldData( "CompanyAddress1" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setCompanyAddress2(String newValue)
    {
                dbnbDB.setFieldData( "CompanyAddress2", newValue.toString() );
    }

    public String getCompanyAddress2()
    {
        String           sValue = dbnbDB.getFieldData( "CompanyAddress2" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setCompanyCity(String newValue)
    {
                dbnbDB.setFieldData( "CompanyCity", newValue.toString() );
    }

    public String getCompanyCity()
    {
        String           sValue = dbnbDB.getFieldData( "CompanyCity" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setCompanyStateID(Integer newValue)
    {
                dbnbDB.setFieldData( "CompanyStateID", newValue.toString() );
    }

    public Integer getCompanyStateID()
    {
        String           sValue = dbnbDB.getFieldData( "CompanyStateID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setCompanyZIP(String newValue)
    {
                dbnbDB.setFieldData( "CompanyZIP", newValue.toString() );
    }

    public String getCompanyZIP()
    {
        String           sValue = dbnbDB.getFieldData( "CompanyZIP" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setCompanyPhone(String newValue)
    {
                dbnbDB.setFieldData( "CompanyPhone", newValue.toString() );
    }

    public String getCompanyPhone()
    {
        String           sValue = dbnbDB.getFieldData( "CompanyPhone" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setCompanyFax(String newValue)
    {
                dbnbDB.setFieldData( "CompanyFax", newValue.toString() );
    }

    public String getCompanyFax()
    {
        String           sValue = dbnbDB.getFieldData( "CompanyFax" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setComments(String newValue)
    {
                dbnbDB.setFieldData( "Comments", newValue.toString() );
    }

    public String getComments()
    {
        String           sValue = dbnbDB.getFieldData( "Comments" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setBillingID(Integer newValue)
    {
                dbnbDB.setFieldData( "BillingID", newValue.toString() );
    }

    public Integer getBillingID()
    {
        String           sValue = dbnbDB.getFieldData( "BillingID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setPHDBAcknowledgementStatus(Integer newValue)
    {
                dbnbDB.setFieldData( "PHDBAcknowledgementStatus", newValue.toString() );
    }

    public Integer getPHDBAcknowledgementStatus()
    {
        String           sValue = dbnbDB.getFieldData( "PHDBAcknowledgementStatus" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setPHDBLetterHeadStatus(Integer newValue)
    {
                dbnbDB.setFieldData( "PHDBLetterHeadStatus", newValue.toString() );
    }

    public Integer getPHDBLetterHeadStatus()
    {
        String           sValue = dbnbDB.getFieldData( "PHDBLetterHeadStatus" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setTaxID(String newValue)
    {
                dbnbDB.setFieldData( "TaxID", newValue.toString() );
    }

    public String getTaxID()
    {
        String           sValue = dbnbDB.getFieldData( "TaxID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setNotUsingPHDBID(Integer newValue)
    {
                dbnbDB.setFieldData( "NotUsingPHDBID", newValue.toString() );
    }

    public Integer getNotUsingPHDBID()
    {
        String           sValue = dbnbDB.getFieldData( "NotUsingPHDBID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setNotUsingPHDBComments(String newValue)
    {
                dbnbDB.setFieldData( "NotUsingPHDBComments", newValue.toString() );
    }

    public String getNotUsingPHDBComments()
    {
        String           sValue = dbnbDB.getFieldData( "NotUsingPHDBComments" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setPayerID(Integer newValue)
    {
                dbnbDB.setFieldData( "PayerID", newValue.toString() );
    }

    public Integer getPayerID()
    {
        String           sValue = dbnbDB.getFieldData( "PayerID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setManagerID(Integer newValue)
    {
                dbnbDB.setFieldData( "ManagerID", newValue.toString() );
    }

    public Integer getManagerID()
    {
        String           sValue = dbnbDB.getFieldData( "ManagerID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setComm_Email_RequiresAttachement(Integer newValue)
    {
                dbnbDB.setFieldData( "Comm_Email_RequiresAttachement", newValue.toString() );
    }

    public Integer getComm_Email_RequiresAttachement()
    {
        String           sValue = dbnbDB.getFieldData( "Comm_Email_RequiresAttachement" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setComm_Alerts_LevelUser(Integer newValue)
    {
                dbnbDB.setFieldData( "Comm_Alerts_LevelUser", newValue.toString() );
    }

    public Integer getComm_Alerts_LevelUser()
    {
        String           sValue = dbnbDB.getFieldData( "Comm_Alerts_LevelUser" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setComm_Alerts_LevelManager(Integer newValue)
    {
                dbnbDB.setFieldData( "Comm_Alerts_LevelManager", newValue.toString() );
    }

    public Integer getComm_Alerts_LevelManager()
    {
        String           sValue = dbnbDB.getFieldData( "Comm_Alerts_LevelManager" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setComm_Alerts_LevelBranch(Integer newValue)
    {
                dbnbDB.setFieldData( "Comm_Alerts_LevelBranch", newValue.toString() );
    }

    public Integer getComm_Alerts_LevelBranch()
    {
        String           sValue = dbnbDB.getFieldData( "Comm_Alerts_LevelBranch" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setComm_Alerts2_LevelUser(Integer newValue)
    {
                dbnbDB.setFieldData( "Comm_Alerts2_LevelUser", newValue.toString() );
    }

    public Integer getComm_Alerts2_LevelUser()
    {
        String           sValue = dbnbDB.getFieldData( "Comm_Alerts2_LevelUser" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setComm_Alerts2_LevelManager(Integer newValue)
    {
                dbnbDB.setFieldData( "Comm_Alerts2_LevelManager", newValue.toString() );
    }

    public Integer getComm_Alerts2_LevelManager()
    {
        String           sValue = dbnbDB.getFieldData( "Comm_Alerts2_LevelManager" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setComm_Alerts2_LevelBranch(Integer newValue)
    {
                dbnbDB.setFieldData( "Comm_Alerts2_LevelBranch", newValue.toString() );
    }

    public Integer getComm_Alerts2_LevelBranch()
    {
        String           sValue = dbnbDB.getFieldData( "Comm_Alerts2_LevelBranch" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setComm_Report_LevelUser(Integer newValue)
    {
                dbnbDB.setFieldData( "Comm_Report_LevelUser", newValue.toString() );
    }

    public Integer getComm_Report_LevelUser()
    {
        String           sValue = dbnbDB.getFieldData( "Comm_Report_LevelUser" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setComm_Report_LevelManager(Integer newValue)
    {
                dbnbDB.setFieldData( "Comm_Report_LevelManager", newValue.toString() );
    }

    public Integer getComm_Report_LevelManager()
    {
        String           sValue = dbnbDB.getFieldData( "Comm_Report_LevelManager" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setComm_Report_LevelBranch(Integer newValue)
    {
                dbnbDB.setFieldData( "Comm_Report_LevelBranch", newValue.toString() );
    }

    public Integer getComm_Report_LevelBranch()
    {
        String           sValue = dbnbDB.getFieldData( "Comm_Report_LevelBranch" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setUserNPI(String newValue)
    {
                dbnbDB.setFieldData( "UserNPI", newValue.toString() );
    }

    public String getUserNPI()
    {
        String           sValue = dbnbDB.getFieldData( "UserNPI" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setUserStateLicense(String newValue)
    {
                dbnbDB.setFieldData( "UserStateLicense", newValue.toString() );
    }

    public String getUserStateLicense()
    {
        String           sValue = dbnbDB.getFieldData( "UserStateLicense" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setUserStateLicenseDesc(String newValue)
    {
                dbnbDB.setFieldData( "UserStateLicenseDesc", newValue.toString() );
    }

    public String getUserStateLicenseDesc()
    {
        String           sValue = dbnbDB.getFieldData( "UserStateLicenseDesc" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setSelectMRI_ID(Integer newValue)
    {
                dbnbDB.setFieldData( "SelectMRI_ID", newValue.toString() );
    }

    public Integer getSelectMRI_ID()
    {
        String           sValue = dbnbDB.getFieldData( "SelectMRI_ID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setSelectMRI_Notes(String newValue)
    {
                dbnbDB.setFieldData( "SelectMRI_Notes", newValue.toString() );
    }

    public String getSelectMRI_Notes()
    {
        String           sValue = dbnbDB.getFieldData( "SelectMRI_Notes" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setMELicense(String newValue)
    {
                dbnbDB.setFieldData( "MELicense", newValue.toString() );
    }

    public String getMELicense()
    {
        String           sValue = dbnbDB.getFieldData( "MELicense" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setSelectMRI_UserType(String newValue)
    {
                dbnbDB.setFieldData( "SelectMRI_UserType", newValue.toString() );
    }

    public String getSelectMRI_UserType()
    {
        String           sValue = dbnbDB.getFieldData( "SelectMRI_UserType" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setImportantNotes_Alert(String newValue)
    {
                dbnbDB.setFieldData( "ImportantNotes_Alert", newValue.toString() );
    }

    public String getImportantNotes_Alert()
    {
        String           sValue = dbnbDB.getFieldData( "ImportantNotes_Alert" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setEmailAlertNotes_Alert(String newValue)
    {
                dbnbDB.setFieldData( "EmailAlertNotes_Alert", newValue.toString() );
    }

    public String getEmailAlertNotes_Alert()
    {
        String           sValue = dbnbDB.getFieldData( "EmailAlertNotes_Alert" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setImportantNotes(String newValue)
    {
                dbnbDB.setFieldData( "ImportantNotes", newValue.toString() );
    }

    public String getImportantNotes()
    {
        String           sValue = dbnbDB.getFieldData( "ImportantNotes" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setNIM_UserType(String newValue)
    {
                dbnbDB.setFieldData( "NIM_UserType", newValue.toString() );
    }

    public String getNIM_UserType()
    {
        String           sValue = dbnbDB.getFieldData( "NIM_UserType" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setTotalMonthlyMRIs(Integer newValue)
    {
                dbnbDB.setFieldData( "TotalMonthlyMRIs", newValue.toString() );
    }

    public Integer getTotalMonthlyMRIs()
    {
        String           sValue = dbnbDB.getFieldData( "TotalMonthlyMRIs" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setExpectedMonthlyMRIs(Integer newValue)
    {
                dbnbDB.setFieldData( "ExpectedMonthlyMRIs", newValue.toString() );
    }

    public Integer getExpectedMonthlyMRIs()
    {
        String           sValue = dbnbDB.getFieldData( "ExpectedMonthlyMRIs" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setData_Rank(Integer newValue)
    {
                dbnbDB.setFieldData( "Data_Rank", newValue.toString() );
    }

    public Integer getData_Rank()
    {
        String           sValue = dbnbDB.getFieldData( "Data_Rank" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setData_3MonthAverage(Integer newValue)
    {
                dbnbDB.setFieldData( "Data_3MonthAverage", newValue.toString() );
    }

    public Integer getData_3MonthAverage()
    {
        String           sValue = dbnbDB.getFieldData( "Data_3MonthAverage" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setData_Referrals_AllTime(Integer newValue)
    {
                dbnbDB.setFieldData( "Data_Referrals_AllTime", newValue.toString() );
    }

    public Integer getData_Referrals_AllTime()
    {
        String           sValue = dbnbDB.getFieldData( "Data_Referrals_AllTime" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setData_Referrals_AnalysisTotal(Integer newValue)
    {
                dbnbDB.setFieldData( "Data_Referrals_AnalysisTotal", newValue.toString() );
    }

    public Integer getData_Referrals_AnalysisTotal()
    {
        String           sValue = dbnbDB.getFieldData( "Data_Referrals_AnalysisTotal" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setData_Referrals_AnalysisTurnAroundAverage_ToSched(Double newValue)
    {
                java.text.DecimalFormat defDecFormat2 = new java.text.DecimalFormat(PLCUtils.String_defDecFormat2);
                dbnbDB.setFieldData( "Data_Referrals_AnalysisTurnAroundAverage_ToSched", defDecFormat2.format(newValue) );
    }

    public Double getData_Referrals_AnalysisTurnAroundAverage_ToSched()
    {
        String           sValue = dbnbDB.getFieldData( "Data_Referrals_AnalysisTurnAroundAverage_ToSched" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDoubleValue : sValue;
        return new Double( sValue  ); 
    }

    public void setData_Referrals_AnalysisCostSavings(Double newValue)
    {
                java.text.DecimalFormat defDecFormat2 = new java.text.DecimalFormat(PLCUtils.String_defDecFormat2);
                dbnbDB.setFieldData( "Data_Referrals_AnalysisCostSavings", defDecFormat2.format(newValue) );
    }

    public Double getData_Referrals_AnalysisCostSavings()
    {
        String           sValue = dbnbDB.getFieldData( "Data_Referrals_AnalysisCostSavings" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDoubleValue : sValue;
        return new Double( sValue  ); 
    }

    public void setInternal_PrimaryContact(Integer newValue)
    {
                dbnbDB.setFieldData( "Internal_PrimaryContact", newValue.toString() );
    }

    public Integer getInternal_PrimaryContact()
    {
        String           sValue = dbnbDB.getFieldData( "Internal_PrimaryContact" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setLastContacted(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "LastContacted", formatter.format( newValue ) );
    }

    public Date getLastContacted()
    {
        String           sValue = dbnbDB.getFieldData( "LastContacted" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setDateOfBirth(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "DateOfBirth", formatter.format( newValue ) );
    }

    public Date getDateOfBirth()
    {
        String           sValue = dbnbDB.getFieldData( "DateOfBirth" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setPersonalInsights(String newValue)
    {
                dbnbDB.setFieldData( "PersonalInsights", newValue.toString() );
    }

    public String getPersonalInsights()
    {
        String           sValue = dbnbDB.getFieldData( "PersonalInsights" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setAllowAsPrimaryContact(Integer newValue)
    {
                dbnbDB.setFieldData( "AllowAsPrimaryContact", newValue.toString() );
    }

    public Integer getAllowAsPrimaryContact()
    {
        String           sValue = dbnbDB.getFieldData( "AllowAsPrimaryContact" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setIsAdmin(Integer newValue)
    {
                dbnbDB.setFieldData( "IsAdmin", newValue.toString() );
    }

    public Integer getIsAdmin()
    {
        String           sValue = dbnbDB.getFieldData( "IsAdmin" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

}    // End of bltUserAccount class definition
