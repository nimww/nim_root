package com.winstaff;
import java.awt.*;
import java.sql.*;
import java.util.Vector;
import com.winstaff.replaceSubstring;

public class runSQL extends java.lang.Object
{

	public runSQL()
	{
	}

	public void setDbClassForName(String dbClassForName)
	{
			this.dbClassForName = dbClassForName;
	}

	public String getDbClassForName()
	{
		return this.dbClassForName;
	}

	public void setDbConnectionURL(String dbConnectionURL)
	{
			this.dbConnectionURL = dbConnectionURL;
	}

	public String getDbConnectionURL()
	{
		return this.dbConnectionURL;
	}

	public void setDbUser(String dbUser)
	{
			this.dbUser = dbUser;
	}

	public String getDbUser()
	{
		return this.dbUser;
	}

	public void setDbPassword(String dbPassword)
	{
			this.dbPassword = dbPassword;
	}
	

	public String getDbPassword()
	{
		return this.dbPassword;
	}

	public void setSqlCmdList(Vector sqlCmdList)
	{
        this.sqlCmdList = sqlCmdList;
	}


	public Vector getSqlCmdList()
	{
		return this.sqlCmdList;
	}

	public String changeStatement(String stmt, String cIn,String cOut)
	{
	    String myValue = stmt;
	    if (!cIn.equalsIgnoreCase("")||!cOut.equalsIgnoreCase(""))
	    {
            replaceSubstring rs1 = new replaceSubstring();
            myValue = rs1.getReplacedSubstring(stmt, cIn, cOut);
        }
	    return myValue;
	}


	public boolean isRunSQL()
	{
	    try
	    {
	        Class.forName( this.getDbClassForName() );
	        Connection  cCon = DriverManager.getConnection( this.getDbConnectionURL(),this.dbUser,this.dbPassword );        
	        Statement   stmt = cCon.createStatement();
	        for (int i=0;i<=this.getSqlCmdList().size();i++)
	        {
	            String myStatement = changeStatement((String)this.getSqlCmdList().elementAt(i),this.changeIn,this.changeOut);
		    
	            if (this.isHaltOnAll())
	            {
	                System.out.println("["+ (String)this.getSqlCmdList().elementAt(i)+"]" );
        			stmt.executeUpdate( myStatement );
	            }
	            else
	            {
	                try
	                {
	                    System.out.println("["+ (String)this.getSqlCmdList().elementAt(i)+"]" );
	                    stmt.executeUpdate( myStatement );
	                }
	                catch (Exception e2003)
	                {
	                    System.out.println("Error (no hault):" + e2003);
	                }
	            }
	        }
	        stmt.close();
	        cCon.close();
	    }
	    catch(Exception e)
	    {
	        System.out.println("isRunSQL ERROR: " + e);
	    }
	        return true;
	}

	public void setHaltOnAll(boolean haltOnAll)
	{
			this.haltOnAll = haltOnAll;
	}

	public boolean isHaltOnAll()
	{
		return this.haltOnAll;
	}


	protected boolean haltOnAll = true;

	protected String dbConnectionURL = "";
	protected String dbClassForName = "";
	protected String dbPassword = "";
	protected Vector sqlCmdList;
	protected String dbUser = "";
	public String changeIn = ",'Init',1,";
	public String changeOut = ",'Init',2,";

}
