

package com.winstaff;

import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public class dbtNIM3_ServiceBillingTransaction extends Object implements InttNIM3_ServiceBillingTransaction
{

        db_NewBase    dbnbDB;

    public dbtNIM3_ServiceBillingTransaction()
    {
        dbnbDB = new db_NewBase( "tNIM3_ServiceBillingTransaction", "ServiceBillingTransactionID" );

    }    // End of default constructor

    public dbtNIM3_ServiceBillingTransaction( Integer iNewID )
    {
        dbnbDB = new db_NewBase( "tNIM3_ServiceBillingTransaction", "ServiceBillingTransactionID" );
        setUniqueID( iNewID );
    }    // End of Constructor knowing an ID

    public void commitData() throws SQLException
    {
            //no longer trapping/ignoring errors 2011-03-17
            dbnbDB.commitData();
    }    // End of commitData()

    public void commitDataForced() throws SQLException
    {
        dbnbDB.commitDataForced();
    }    // End of commitData()

    public Integer getUniqueID()
    {
        return dbnbDB.getUniqueID();
    }    // End of getUniqueID()

    public void setUniqueID( Integer iNewID )
    {
        dbnbDB.setUniqueID( iNewID );
    }    // End of setUniqueID()

    public void setModifyComments( String sNew )
    {
        dbnbDB.setModifyComments( sNew );
    }    // End of setUniqueID()


    public void setServiceBillingTransactionID(Integer newValue)
    {
                dbnbDB.setFieldData( "ServiceBillingTransactionID", newValue.toString() );
    }

    public Integer getServiceBillingTransactionID()
    {
        String           sValue = dbnbDB.getFieldData( "ServiceBillingTransactionID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setField(String fieldN, Object fieldV)
    {DebugLogger.println("setField Method not implemented yet in classNIM3_ServiceBillingTransaction!");}

    public String getFieldType(String fieldN)
    {return null;}


    public void setUniqueCreateDate(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "UniqueCreateDate", formatter.format( newValue ) );
    }

    public Date getUniqueCreateDate()
    {
        String           sValue = dbnbDB.getFieldData( "UniqueCreateDate" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setUniqueModifyDate(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "UniqueModifyDate", formatter.format( newValue ) );
    }

    public Date getUniqueModifyDate()
    {
        String           sValue = dbnbDB.getFieldData( "UniqueModifyDate" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setUniqueModifyComments(String newValue)
    {
                dbnbDB.setFieldData( "UniqueModifyComments", newValue.toString() );
    }

    public String getUniqueModifyComments()
    {
        String           sValue = dbnbDB.getFieldData( "UniqueModifyComments" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setServiceID(Integer newValue)
    {
                dbnbDB.setFieldData( "ServiceID", newValue.toString() );
    }

    public Integer getServiceID()
    {
        String           sValue = dbnbDB.getFieldData( "ServiceID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setServiceBillingTransactionTypeID(Integer newValue)
    {
                dbnbDB.setFieldData( "ServiceBillingTransactionTypeID", newValue.toString() );
    }

    public Integer getServiceBillingTransactionTypeID()
    {
        String           sValue = dbnbDB.getFieldData( "ServiceBillingTransactionTypeID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setExportedDate(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "ExportedDate", formatter.format( newValue ) );
    }

    public Date getExportedDate()
    {
        String           sValue = dbnbDB.getFieldData( "ExportedDate" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setExportComments(String newValue)
    {
                dbnbDB.setFieldData( "ExportComments", newValue.toString() );
    }

    public String getExportComments()
    {
        String           sValue = dbnbDB.getFieldData( "ExportComments" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setBilledDate(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "BilledDate", formatter.format( newValue ) );
    }

    public Date getBilledDate()
    {
        String           sValue = dbnbDB.getFieldData( "BilledDate" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setBillComments(String newValue)
    {
                dbnbDB.setFieldData( "BillComments", newValue.toString() );
    }

    public String getBillComments()
    {
        String           sValue = dbnbDB.getFieldData( "BillComments" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setTransactionTitle(String newValue)
    {
                dbnbDB.setFieldData( "TransactionTitle", newValue.toString() );
    }

    public String getTransactionTitle()
    {
        String           sValue = dbnbDB.getFieldData( "TransactionTitle" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setTransactionNote(String newValue)
    {
                dbnbDB.setFieldData( "TransactionNote", newValue.toString() );
    }

    public String getTransactionNote()
    {
        String           sValue = dbnbDB.getFieldData( "TransactionNote" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setTransactionAmount(Double newValue)
    {
                java.text.DecimalFormat defDecFormat2 = new java.text.DecimalFormat(PLCUtils.String_defDecFormat2);
                dbnbDB.setFieldData( "TransactionAmount", defDecFormat2.format(newValue) );
    }

    public Double getTransactionAmount()
    {
        String           sValue = dbnbDB.getFieldData( "TransactionAmount" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDoubleValue : sValue;
        return new Double( sValue  ); 
    }

    public void setReferenceNumber(String newValue)
    {
                dbnbDB.setFieldData( "ReferenceNumber", newValue.toString() );
    }

    public String getReferenceNumber()
    {
        String           sValue = dbnbDB.getFieldData( "ReferenceNumber" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setReferenceDate(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "ReferenceDate", formatter.format( newValue ) );
    }

    public Date getReferenceDate()
    {
        String           sValue = dbnbDB.getFieldData( "ReferenceDate" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setReferenceNotes(String newValue)
    {
                dbnbDB.setFieldData( "ReferenceNotes", newValue.toString() );
    }

    public String getReferenceNotes()
    {
        String           sValue = dbnbDB.getFieldData( "ReferenceNotes" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setDetail_Item_Qty(Integer newValue)
    {
                dbnbDB.setFieldData( "Detail_Item_Qty", newValue.toString() );
    }

    public Integer getDetail_Item_Qty()
    {
        String           sValue = dbnbDB.getFieldData( "Detail_Item_Qty" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setGenerated_UserID(Integer newValue)
    {
                dbnbDB.setFieldData( "Generated_UserID", newValue.toString() );
    }

    public Integer getGenerated_UserID()
    {
        String           sValue = dbnbDB.getFieldData( "Generated_UserID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setComments(String newValue)
    {
                dbnbDB.setFieldData( "Comments", newValue.toString() );
    }

    public String getComments()
    {
        String           sValue = dbnbDB.getFieldData( "Comments" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

}    // End of bltNIM3_ServiceBillingTransaction class definition
