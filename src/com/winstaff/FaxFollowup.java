package com.winstaff;

import java.util.Calendar;
import java.util.Date;

import org.joda.time.DateTime;

/**
*
*	eFax Interface for Medical Report(MR) follow-up faxes:
*
*	1. Query is run twice a day at 5am and 12pm PST that scans entire database. Skip weekends.
*
*	2. Query will look for the following parameters to generate an eFax reminder to imaging center:
*
*		a. Encounters’ appointment date/time that has passed current time.
*		b. Report Received Date is blank.
*		c. Ignore Void, Closed cases.
*
*	3. MR Fax Status(new field) value is increased by 1 so we know how many times faxed. This field is in the Encounter table.
*
*	4. If MR Fax Status value is =4, show warning on Home Page and  should call immediately. If case is STAT, it appears immediately after appointment. 
*	   All others, do not show on Home page until MR Fax Status value is = 4.
*
*	5. Need to add new field called Require Patient Carry CD? Y/N in the Encounter table. If answer is Y,  insert a check mark next to Patient to Hand Carry CD on the fax form.
*
*	6. If Requires On-Line Image is Yes, and NetDev Practice record DICOM Forward is N, then check the box next to Mail CD to NextImage.
*
*	7. If Requires On-Line Image is Yes, and NetDev Practice record DICOM Forward is Y, then check the box next to Electronic Transfer Image to NextImage.
*
*   Changes required on approximately 4/23/12
*   
*   Based on feedback from Schedulers, we need to change the logic on when the auto fax is sent out.
*   1. Reduce the max number of faxes to 3 instead of 4.
*   2. Delay the 1st fax to 12 hours after the appointment, then send at the next 5am
*   3. 2nd fax at the next 2pm 
*   4. 3rd fax next day 2pm *   
*
*
*
**/

public class FaxFollowup {

	// Maximum number of times we attempt to Fax the followup for a specific case.
	final static int MAXFAX = 3;
	
	int reportFollowupCounter = 0;  // Fax report sent counter
	int requiresHandCarry = 0; 		// Require Patient Carry CD?
	int requiresonlineimage = 0;	// Requires On-Line Image
	Integer dicomForward;			// DICOM Forward flag.
	
	/**
	 * 
	 */
	public void process( boolean runMode) {	
		
		searchDB2 mySDBA = new searchDB2();
		String emailBody = null;
		int i = 0;
        int counter = 0;       
        if (runMode){
            System.out.println("RunMode: True");
        } else{
            System.out.println("RunMode: False");
        }
		try {
			//String mySQLA = "SELECT EncounterID, requireshandcarrycd, reportfollowupcounter, requiresonlineimage FROM tnim3_encounter WHERE dateofservice > '3/8/2012' AND  reportfileid = 0 ORDER BY EncounterID";
			//String mySQLA = "SELECT EncounterID, requireshandcarrycd, reportfollowupcounter, requiresonlineimage FROM tnim3_encounter WHERE dateofservice > '3/8/2012' AND reportfileid = 0 and iscourtesy<>1 ORDER BY EncounterID";
			
			//Modified query excludes NID case 6-10-13 - Po
			String mySQLA = "SELECT EncounterID, requireshandcarrycd, reportfollowupcounter, tnim3_encounter.requiresonlineimage FROM tnim3_encounter join tnim3_referral on tnim3_referral.referralid = tnim3_encounter.referralid join tnim3_caseaccount on tnim3_caseaccount.caseid = tnim3_referral.caseid join tnim3_payermaster on tnim3_payermaster.payerid = tnim3_caseaccount.payerid WHERE dateofservice > '3/8/2012' AND reportfileid = 0 and iscourtesy<>1 and tnim3_payermaster.payername !~ 'NID' ORDER BY EncounterID";
			
			java.sql.ResultSet myRSA = mySDBA.executeStatement(mySQLA);
			while (myRSA.next()) {
				
				String tmp = myRSA.getString("encounterid");			
				reportFollowupCounter = myRSA.getInt("reportfollowupcounter");
				requiresHandCarry = myRSA.getInt("requireshandcarrycd");
				requiresonlineimage = myRSA.getInt("requiresonlineimage");			
				
				NIM3_EncounterObject2 myEO2 = new NIM3_EncounterObject2(
						new Integer(tmp), "Load for eFax check.");
				
				if (myEO2.getNIM3_Appointment() != null && !myEO2.isEncounterVoid() && !myEO2.isNextImageDirectCase() ) {
					
                    Date dbAppointment = myEO2.getNIM3_Appointment().getAppointmentTime();
                    DateTime appt = new DateTime(dbAppointment);
                    
                    // We are using DateTime to get at hours data.
                    DateTime now = new DateTime();
                    DateTime staggered12Hours = now.minusHours(12);
                    
                    boolean fiveAM = false;
                    boolean twoPM = false;
                    
                    // Determine if right now 5 am or 2 pm.
                    // We are going to (grossly) approximate this because this script will NOT fire
                    // off not EXACTLY at 5am and 2 pm.
                    
                    if ((4 < now.getHourOfDay()) && (now.getHourOfDay() < 6)) {
                    	fiveAM = true;
                    } else {
                        if ((13 < now.getHourOfDay()) && (now.getHourOfDay() < 15)) {
                        	twoPM = true;
                        }                    	
                    }
                    
					// If the appointment date is in the past (even if you subtract 12 hours from 'now')
                    // and we have not sent out any notifications then send one out at 5 am only.
                    // This will not be strictly enforced because they could miss their first appt. at 5:05 pm.
                    
					if ((appt.compareTo(staggered12Hours)<0 && reportFollowupCounter==0 && fiveAM) || 
						(appt.compareTo(staggered12Hours)<0 && reportFollowupCounter==1 && twoPM) ||
						(appt.compareTo(staggered12Hours)<0 && reportFollowupCounter==2 && twoPM)) {
		
						if (reportFollowupCounter == MAXFAX) {
							DebugLogger.println("\n\n Max fax limit exceeded");
							continue;
						}
                        counter++;

                        String toFax = "[not-set]";
                        //Check to see if Override is filled out.
                        if ( myEO2.getAppointment_PracticeMaster().getFaxReportReminderOverride().length()>9 ) {
                            toFax = NIMUtils.fax2EmailFax(myEO2.getAppointment_PracticeMaster().getFaxReportReminderOverride());
                        } else {
                            toFax = NIMUtils.fax2EmailFax(myEO2.getAppointment_PracticeMaster().getOfficeFaxNo());
                        }
                        //Print out every 10th one.  For checking fax numbers.
                        if (counter%10==0){
                            System.out.println("--> " + toFax);
                        }

						if (runMode){
                            // Get the DICOM Forward flag.
                            dicomForward = myEO2.getAppointment_PracticeMaster().getDICOMForward();
                            
                            // Construct email body.
                            emailBody = makeEmailBody(myEO2, dbAppointment);	

                            // Get ready to put email to database.
                            // STE: I switched this over from the Email Object to the EmailType_V3 object which is a helper class for sending the emails
                            emailType_V3 myEmail = new emailType_V3();
                            myEmail.setTo(toFax);
                            myEmail.setFrom(myEO2.getCase_AssignedTo().getContactEmail());
                            myEmail.setBody(emailBody);
                            // Add Scan Pass # in Subject line.
                            myEmail.setSubject("Appointment Reminder Notice (SP# "+myEO2.getScanPass()+")");
                            Integer myIsSendTrackID = myEmail.isSendMail_Track();
                            

                            // Increment the fax report sent counter					
                            Integer id = new Integer(tmp);
                            bltNIM3_Encounter NIM3_Encounter = new bltNIM3_Encounter(id);
                            NIM3_Encounter.setReportFollowUpCounter(new Integer(reportFollowupCounter+1));
                            NIM3_Encounter.commitData();

                            //create Comm Track
                            bltNIM3_CommTrack myNCT = new bltNIM3_CommTrack();
                            myNCT.setUniqueModifyComments("NIMUtils:sendAlertEmail:System Generated");
                            myNCT.setCaseID(myEO2.getNIM3_CaseAccount().getCaseID());
                            myNCT.setReferralID(myEO2.getNIM3_Referral().getReferralID());
                            myNCT.setEncounterID(myEO2.getNIM3_Encounter().getEncounterID());
                            myNCT.setCommStart(PLCUtils.getNowDate(false));
                            myNCT.setCommEnd(PLCUtils.getNowDate(false));
                            myNCT.setCommTypeID(new Integer(3));
                            myNCT.setAlertStatusCode(new Integer(1));
                            //myNCT.setMessageName("System on behalf of " + CurrentUserAccount.getContactFirstName()+" "+CurrentUserAccount.getContactLastName());
                            myNCT.setMessageText("Fax [Report Reminder (" + (myEO2.getNIM3_Encounter().getReportFollowUpCounter()+1) + ")] Generated to ICID: " +  myEO2.getAppointment_PracticeMaster().getPracticeID() + " at " + toFax );
                            bltNIM3_CommReference myNCR = new bltNIM3_CommReference();
                            myNCR.setUniqueModifyComments("NIMUtils:sendAlertEmail:System Generated");
                            myNCR.setCRTypeID(new Integer(1));  //1 = email   2 = fax email  3 = fax manual
                            myNCR.setRefID(myIsSendTrackID);
                            myNCR.commitData();
                            myNCT.setCommReferenceID(myNCR.getUniqueID());
                            myNCT.commitData();

                        }
					}
				}								
			}
            System.out.println("Total Count: " + counter);

		} catch (Exception e) {
			System.out.println(e);
		}	
	}
	
	
	/**
	 * Construct the email body out of code taken from NIMUtils.java
	 * 
	 * @param myEO2
	 * @param dbAppointment
	 * @return
	 */
	public String makeEmailBody(NIM3_EncounterObject2 myEO2, Date dbAppointment){
	
        java.text.SimpleDateFormat displayDateSDF = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);
        java.text.SimpleDateFormat displayDateTimeSDF = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF2Full);
        
        java.text.SimpleDateFormat displayDateDay = new java.text.SimpleDateFormat(PLCUtils.String_displayDateDay);
        java.text.SimpleDateFormat displayDateDayWeek = new java.text.SimpleDateFormat(PLCUtils.String_displayDateDayWeek);
        java.text.SimpleDateFormat displayDateMonth = new java.text.SimpleDateFormat(PLCUtils.String_displayDateMonth);
        java.text.SimpleDateFormat displayDateYear = new java.text.SimpleDateFormat(PLCUtils.String_displayDateYear);
        java.text.SimpleDateFormat displayDateHour = new java.text.SimpleDateFormat(PLCUtils.String_displayDateHour);
        
        String sAppointmentTime = displayDateTimeSDF.format((dbAppointment));
        String sAppointmentTimeDay = displayDateDay.format((dbAppointment));
        String sAppointmentTimeDayWeek = displayDateDayWeek.format((dbAppointment));
        String sAppointmentTimeMonth = displayDateMonth.format((dbAppointment));
        String sAppointmentTimeYear = displayDateYear.format((dbAppointment));
        String sAppointmentHour = displayDateHour.format((dbAppointment));
       
		String numberOfNotices;
		
		// We fax this reminder from 1 to 3 times.
		
		switch (reportFollowupCounter) {
            case 0:  numberOfNotices = "Do Not Ignore";
                     break;
            case 1:  numberOfNotices = "Second Notice";
                     break;
            case 2:  numberOfNotices = "Final Notice";
                     break;
            //case 3:  numberOfNotices = "Final Notice";
                     //break;
            default: numberOfNotices = "Do Not Ignore";
                     break;
        }        
        		 
        String theBody = "\tImportant Reminder\n";
        theBody += "\n\t" + numberOfNotices + "!\n";  
        theBody += "\nOne of our patients was recently scheduled for a diagnostic test at your facility";    
        if (reportFollowupCounter > 0) {
            theBody += " and we have still not received the medical report:\n";       	
        } else {
            theBody += ":\n";          	
        }
        
        // Make the DOB an easier to read format
        java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);
		Date dob = myEO2.getNIM3_CaseAccount().getPatientDOB();
        String dobStr =  displayDateSDF1.format(dob);
		
        theBody += "\nPatient Name:\t" + myEO2.getNIM3_CaseAccount().getPatientLastName() + ", "  + myEO2.getNIM3_CaseAccount().getPatientFirstName() +  "\tGender: " + myEO2.getNIM3_CaseAccount().getPatientGender() + "\tDOB: " + dobStr;        
        theBody += "\nAppointment Date and Time:\t" + sAppointmentTimeDayWeek + " - " + sAppointmentTime;
        theBody += "\nDiagnostic Test:";
        
        bltNIM3_Service        working_bltNIM3_Service;
        ListElement         leCurrentElement_Service;
        java.util.Enumeration eList_Service = myEO2.getNIM3_Service_List().elements();
        int cnt55=0;
        while (eList_Service.hasMoreElements())
        {
            cnt55++;
            leCurrentElement_Service    = (ListElement) eList_Service.nextElement();
            working_bltNIM3_Service  = (bltNIM3_Service) leCurrentElement_Service.getObject();
            if ( working_bltNIM3_Service.getServiceStatusID()==1)
            {
                theBody += "\n";
                if ( !working_bltNIM3_Service.getCPT().equalsIgnoreCase(""))
                {
                    theBody += "\t" + NIMUtils.getCPTText(working_bltNIM3_Service.getCPT()) + "";
                }
                if ( !working_bltNIM3_Service.getCPTBodyPart().equalsIgnoreCase(""))
                {
                    theBody += "\t" + working_bltNIM3_Service.getCPTBodyPart();
                }
                if ( !working_bltNIM3_Service.getCPTText().equalsIgnoreCase(""))
                {
                    theBody += "\t|Diag/Rule-Out:\t" + working_bltNIM3_Service.getCPTText();
                }
            }

        }
        
        theBody += "\nInstructions: " + myEO2.getNIM3_Encounter().getComments();
        if (myEO2.getNIM3_Encounter().getRequiresAgeInjury()==1)
        {
            theBody += "\nAGE INJURY:\t\tYES";
        }
        
        theBody += "\n\nThis is ";
        if (reportFollowupCounter == 0) {
            theBody += "a courtesy reminder";    	
        } else {
            theBody += "the "+numberOfNotices;          	
        }
        
        theBody += " to fax us the medical diagnostic report within 24-48 hours of the appointment time. ";
        theBody += "\nIf for any reason the patient did not make their appointment please check one of the boxes below and fax back to us immediately:\n";    
        theBody += "\n[ ] Patient missed their appointment and it has not been rescheduled.";           
        theBody += "\n[ ] Patient missed their appointment but rescheduled the test on the following Date ___________________ ";                
        theBody += "and Time: ___________________ ";                
        
        theBody += "\n\n*** Instructions for Medical Records Department ***\n";
        
        if (requiresHandCarry != 0) {
            theBody += "\n[*]";       	
        }
        else {
            theBody += "\n[ ]";       	      	
        }
        theBody += " Patient to hand carry CD";

        // If Requires On-Line Image is Yes, and NetDev Practice record DICOM Forward is N,
        // then check the box next to Mail CD to NextImage.
        if (requiresonlineimage != 0 && dicomForward.intValue() == 0 ) {
            theBody += "\n[*]";       	
        }
        else {
            theBody += "\n[ ]";       	      	
        }
        theBody += " Mail CD to NextImage";
        
        // If Requires On-Line Image is Yes, and NetDev Practice record DICOM Forward is Y, 
        // then check the box next to Electronic Transfer Image to NextImage.    
        // then check the box next to Electronic Transfer Image to NextImage.
        if (requiresonlineimage != 0 && dicomForward.intValue() != 0 ) {
            theBody += "\n[*]";       	
        }
        else {
            theBody += "\n[ ]";       	      	
        }        
        theBody += " Electronic transfer image to NextImage";

        theBody += "\n\n(800) 637-5164 - Fax\n(888) 318-5111 - Voice\nNextImage Medical, Inc.\n3390 Carmel Mountain Rd. Suite 150\nSan Diego, CA 92121";
        theBody += "\n";
        
        return theBody;
        
	}
	
	public static void main(String[] args) {	
		System.out.println("Starting Fax Followup.\n");
        FaxFollowup fax = new FaxFollowup();
        if (args[0].equalsIgnoreCase("--debug")){
            fax.process(false);
        } else {
            fax.process(true);
        }
		System.out.println("\nFINISHED.");
	}
	
}
