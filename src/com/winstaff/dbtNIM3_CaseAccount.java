

package com.winstaff;

import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public class dbtNIM3_CaseAccount extends Object implements InttNIM3_CaseAccount
{

        db_NewBase    dbnbDB;

    public dbtNIM3_CaseAccount()
    {
        dbnbDB = new db_NewBase( "tNIM3_CaseAccount", "CaseID" );

    }    // End of default constructor

    public dbtNIM3_CaseAccount( Integer iNewID )
    {
        dbnbDB = new db_NewBase( "tNIM3_CaseAccount", "CaseID" );
        setUniqueID( iNewID );
    }    // End of Constructor knowing an ID

    public void commitData() throws SQLException
    {
            //no longer trapping/ignoring errors 2011-03-17
            dbnbDB.commitData();
    }    // End of commitData()

    public void commitDataForced() throws SQLException
    {
        dbnbDB.commitDataForced();
    }    // End of commitData()

    public Integer getUniqueID()
    {
        return dbnbDB.getUniqueID();
    }    // End of getUniqueID()

    public void setUniqueID( Integer iNewID )
    {
        dbnbDB.setUniqueID( iNewID );
    }    // End of setUniqueID()

    public void setModifyComments( String sNew )
    {
        dbnbDB.setModifyComments( sNew );
    }    // End of setUniqueID()


    public void setCaseID(Integer newValue)
    {
                dbnbDB.setFieldData( "CaseID", newValue.toString() );
    }

    public Integer getCaseID()
    {
        String           sValue = dbnbDB.getFieldData( "CaseID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setField(String fieldN, Object fieldV)
    {DebugLogger.println("setField Method not implemented yet in classNIM3_CaseAccount!");}

    public String getFieldType(String fieldN)
    {return null;}


    public void setUniqueCreateDate(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "UniqueCreateDate", formatter.format( newValue ) );
    }

    public Date getUniqueCreateDate()
    {
        String           sValue = dbnbDB.getFieldData( "UniqueCreateDate" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setUniqueModifyDate(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "UniqueModifyDate", formatter.format( newValue ) );
    }

    public Date getUniqueModifyDate()
    {
        String           sValue = dbnbDB.getFieldData( "UniqueModifyDate" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setUniqueModifyComments(String newValue)
    {
                dbnbDB.setFieldData( "UniqueModifyComments", newValue.toString() );
    }

    public String getUniqueModifyComments()
    {
        String           sValue = dbnbDB.getFieldData( "UniqueModifyComments" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setPayerID(Integer newValue)
    {
                dbnbDB.setFieldData( "PayerID", newValue.toString() );
    }

    public Integer getPayerID()
    {
        String           sValue = dbnbDB.getFieldData( "PayerID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setCaseClaimNumber(String newValue)
    {
                dbnbDB.setFieldData( "CaseClaimNumber", newValue.toString() );
    }

    public String getCaseClaimNumber()
    {
        String           sValue = dbnbDB.getFieldData( "CaseClaimNumber" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setCaseCode(String newValue)
    {
                dbnbDB.setFieldData( "CaseCode", newValue.toString() );
    }

    public String getCaseCode()
    {
        String           sValue = dbnbDB.getFieldData( "CaseCode" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setWCBNumber(String newValue)
    {
                dbnbDB.setFieldData( "WCBNumber", newValue.toString() );
    }

    public String getWCBNumber()
    {
        String           sValue = dbnbDB.getFieldData( "WCBNumber" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setCaseStatusID(Integer newValue)
    {
                dbnbDB.setFieldData( "CaseStatusID", newValue.toString() );
    }

    public Integer getCaseStatusID()
    {
        String           sValue = dbnbDB.getFieldData( "CaseStatusID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setAdjusterID(Integer newValue)
    {
                dbnbDB.setFieldData( "AdjusterID", newValue.toString() );
    }

    public Integer getAdjusterID()
    {
        String           sValue = dbnbDB.getFieldData( "AdjusterID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setOccMedPatientID(String newValue)
    {
                dbnbDB.setFieldData( "OccMedPatientID", newValue.toString() );
    }

    public String getOccMedPatientID()
    {
        String           sValue = dbnbDB.getFieldData( "OccMedPatientID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setEmployerID(Integer newValue)
    {
                dbnbDB.setFieldData( "EmployerID", newValue.toString() );
    }

    public Integer getEmployerID()
    {
        String           sValue = dbnbDB.getFieldData( "EmployerID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setEmployerName(String newValue)
    {
                dbnbDB.setFieldData( "EmployerName", newValue.toString() );
    }

    public String getEmployerName()
    {
        String           sValue = dbnbDB.getFieldData( "EmployerName" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setEmployerPhone(String newValue)
    {
                dbnbDB.setFieldData( "EmployerPhone", newValue.toString() );
    }

    public String getEmployerPhone()
    {
        String           sValue = dbnbDB.getFieldData( "EmployerPhone" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setEmployerFax(String newValue)
    {
                dbnbDB.setFieldData( "EmployerFax", newValue.toString() );
    }

    public String getEmployerFax()
    {
        String           sValue = dbnbDB.getFieldData( "EmployerFax" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setJobID(Integer newValue)
    {
                dbnbDB.setFieldData( "JobID", newValue.toString() );
    }

    public Integer getJobID()
    {
        String           sValue = dbnbDB.getFieldData( "JobID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setJobNotes(String newValue)
    {
                dbnbDB.setFieldData( "JobNotes", newValue.toString() );
    }

    public String getJobNotes()
    {
        String           sValue = dbnbDB.getFieldData( "JobNotes" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setPayerAccountIDNumber(String newValue)
    {
                dbnbDB.setFieldData( "PayerAccountIDNumber", newValue.toString() );
    }

    public String getPayerAccountIDNumber()
    {
        String           sValue = dbnbDB.getFieldData( "PayerAccountIDNumber" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setPatientFirstName(String newValue)
    {
                dbnbDB.setFieldData( "PatientFirstName", newValue.toString() );
    }

    public String getPatientFirstName()
    {
        String           sValue = dbnbDB.getFieldData( "PatientFirstName" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setPatientLastName(String newValue)
    {
                dbnbDB.setFieldData( "PatientLastName", newValue.toString() );
    }

    public String getPatientLastName()
    {
        String           sValue = dbnbDB.getFieldData( "PatientLastName" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setPatientAccountNumber(String newValue)
    {
                dbnbDB.setFieldData( "PatientAccountNumber", newValue.toString() );
    }

    public String getPatientAccountNumber()
    {
        String           sValue = dbnbDB.getFieldData( "PatientAccountNumber" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setPatientExpirationDate(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "PatientExpirationDate", formatter.format( newValue ) );
    }

    public Date getPatientExpirationDate()
    {
        String           sValue = dbnbDB.getFieldData( "PatientExpirationDate" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setPatientIsActive(Integer newValue)
    {
                dbnbDB.setFieldData( "PatientIsActive", newValue.toString() );
    }

    public Integer getPatientIsActive()
    {
        String           sValue = dbnbDB.getFieldData( "PatientIsActive" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setPatientDOB(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "PatientDOB", formatter.format( newValue ) );
    }

    public Date getPatientDOB()
    {
        String           sValue = dbnbDB.getFieldData( "PatientDOB" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setPatientSSN(String newValue)
    {
                dbnbDB.setFieldData( "PatientSSN", newValue.toString() );
    }

    public String getPatientSSN()
    {
        String           sValue = dbnbDB.getFieldData( "PatientSSN" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setPatientGender(String newValue)
    {
                dbnbDB.setFieldData( "PatientGender", newValue.toString() );
    }

    public String getPatientGender()
    {
        String           sValue = dbnbDB.getFieldData( "PatientGender" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setPatientAddress1(String newValue)
    {
                dbnbDB.setFieldData( "PatientAddress1", newValue.toString() );
    }

    public String getPatientAddress1()
    {
        String           sValue = dbnbDB.getFieldData( "PatientAddress1" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setPatientAddress2(String newValue)
    {
                dbnbDB.setFieldData( "PatientAddress2", newValue.toString() );
    }

    public String getPatientAddress2()
    {
        String           sValue = dbnbDB.getFieldData( "PatientAddress2" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setPatientCity(String newValue)
    {
                dbnbDB.setFieldData( "PatientCity", newValue.toString() );
    }

    public String getPatientCity()
    {
        String           sValue = dbnbDB.getFieldData( "PatientCity" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setPatientStateID(Integer newValue)
    {
                dbnbDB.setFieldData( "PatientStateID", newValue.toString() );
    }

    public Integer getPatientStateID()
    {
        String           sValue = dbnbDB.getFieldData( "PatientStateID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setPatientZIP(String newValue)
    {
                dbnbDB.setFieldData( "PatientZIP", newValue.toString() );
    }

    public String getPatientZIP()
    {
        String           sValue = dbnbDB.getFieldData( "PatientZIP" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setPatientHomePhone(String newValue)
    {
                dbnbDB.setFieldData( "PatientHomePhone", newValue.toString() );
    }

    public String getPatientHomePhone()
    {
        String           sValue = dbnbDB.getFieldData( "PatientHomePhone" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setPatientWorkPhone(String newValue)
    {
                dbnbDB.setFieldData( "PatientWorkPhone", newValue.toString() );
    }

    public String getPatientWorkPhone()
    {
        String           sValue = dbnbDB.getFieldData( "PatientWorkPhone" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setPatientCellPhone(String newValue)
    {
                dbnbDB.setFieldData( "PatientCellPhone", newValue.toString() );
    }

    public String getPatientCellPhone()
    {
        String           sValue = dbnbDB.getFieldData( "PatientCellPhone" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setPatientCCAccountFullName(String newValue)
    {
                dbnbDB.setFieldData( "PatientCCAccountFullName", newValue.toString() );
    }

    public String getPatientCCAccountFullName()
    {
        String           sValue = dbnbDB.getFieldData( "PatientCCAccountFullName" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setPatientCCAccountNumber1(String newValue)
    {
                dbnbDB.setFieldData( "PatientCCAccountNumber1", newValue.toString() );
    }

    public String getPatientCCAccountNumber1()
    {
        String           sValue = dbnbDB.getFieldData( "PatientCCAccountNumber1" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setPatientCCAccountNumber2(String newValue)
    {
                dbnbDB.setFieldData( "PatientCCAccountNumber2", newValue.toString() );
    }

    public String getPatientCCAccountNumber2()
    {
        String           sValue = dbnbDB.getFieldData( "PatientCCAccountNumber2" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setPatientCCAccountType(String newValue)
    {
                dbnbDB.setFieldData( "PatientCCAccountType", newValue.toString() );
    }

    public String getPatientCCAccountType()
    {
        String           sValue = dbnbDB.getFieldData( "PatientCCAccountType" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setPatientCCAccountExpMonth(String newValue)
    {
                dbnbDB.setFieldData( "PatientCCAccountExpMonth", newValue.toString() );
    }

    public String getPatientCCAccountExpMonth()
    {
        String           sValue = dbnbDB.getFieldData( "PatientCCAccountExpMonth" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setPatientCCAccountExpYear(String newValue)
    {
                dbnbDB.setFieldData( "PatientCCAccountExpYear", newValue.toString() );
    }

    public String getPatientCCAccountExpYear()
    {
        String           sValue = dbnbDB.getFieldData( "PatientCCAccountExpYear" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setSupervisorName(String newValue)
    {
                dbnbDB.setFieldData( "SupervisorName", newValue.toString() );
    }

    public String getSupervisorName()
    {
        String           sValue = dbnbDB.getFieldData( "SupervisorName" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setSupervisorPhone(String newValue)
    {
                dbnbDB.setFieldData( "SupervisorPhone", newValue.toString() );
    }

    public String getSupervisorPhone()
    {
        String           sValue = dbnbDB.getFieldData( "SupervisorPhone" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setSupervisorFax(String newValue)
    {
                dbnbDB.setFieldData( "SupervisorFax", newValue.toString() );
    }

    public String getSupervisorFax()
    {
        String           sValue = dbnbDB.getFieldData( "SupervisorFax" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setDateOfInjury(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "DateOfInjury", formatter.format( newValue ) );
    }

    public Date getDateOfInjury()
    {
        String           sValue = dbnbDB.getFieldData( "DateOfInjury" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setInjuryDescription(String newValue)
    {
                dbnbDB.setFieldData( "InjuryDescription", newValue.toString() );
    }

    public String getInjuryDescription()
    {
        String           sValue = dbnbDB.getFieldData( "InjuryDescription" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setLODID(Integer newValue)
    {
                dbnbDB.setFieldData( "LODID", newValue.toString() );
    }

    public Integer getLODID()
    {
        String           sValue = dbnbDB.getFieldData( "LODID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setNurseCaseManagerID(Integer newValue)
    {
                dbnbDB.setFieldData( "NurseCaseManagerID", newValue.toString() );
    }

    public Integer getNurseCaseManagerID()
    {
        String           sValue = dbnbDB.getFieldData( "NurseCaseManagerID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setPatientAccountID(Integer newValue)
    {
                dbnbDB.setFieldData( "PatientAccountID", newValue.toString() );
    }

    public Integer getPatientAccountID()
    {
        String           sValue = dbnbDB.getFieldData( "PatientAccountID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setAttorneyFirstName(String newValue)
    {
                dbnbDB.setFieldData( "AttorneyFirstName", newValue.toString() );
    }

    public String getAttorneyFirstName()
    {
        String           sValue = dbnbDB.getFieldData( "AttorneyFirstName" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setAttorneyLastName(String newValue)
    {
                dbnbDB.setFieldData( "AttorneyLastName", newValue.toString() );
    }

    public String getAttorneyLastName()
    {
        String           sValue = dbnbDB.getFieldData( "AttorneyLastName" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setAttorneyAddress1(String newValue)
    {
                dbnbDB.setFieldData( "AttorneyAddress1", newValue.toString() );
    }

    public String getAttorneyAddress1()
    {
        String           sValue = dbnbDB.getFieldData( "AttorneyAddress1" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setAttorneyAddress2(String newValue)
    {
                dbnbDB.setFieldData( "AttorneyAddress2", newValue.toString() );
    }

    public String getAttorneyAddress2()
    {
        String           sValue = dbnbDB.getFieldData( "AttorneyAddress2" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setAttorneyCity(String newValue)
    {
                dbnbDB.setFieldData( "AttorneyCity", newValue.toString() );
    }

    public String getAttorneyCity()
    {
        String           sValue = dbnbDB.getFieldData( "AttorneyCity" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setAttorneyStateID(Integer newValue)
    {
                dbnbDB.setFieldData( "AttorneyStateID", newValue.toString() );
    }

    public Integer getAttorneyStateID()
    {
        String           sValue = dbnbDB.getFieldData( "AttorneyStateID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setAttorneyZIP(String newValue)
    {
                dbnbDB.setFieldData( "AttorneyZIP", newValue.toString() );
    }

    public String getAttorneyZIP()
    {
        String           sValue = dbnbDB.getFieldData( "AttorneyZIP" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setAttorneyFax(String newValue)
    {
                dbnbDB.setFieldData( "AttorneyFax", newValue.toString() );
    }

    public String getAttorneyFax()
    {
        String           sValue = dbnbDB.getFieldData( "AttorneyFax" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setAttorneyWorkPhone(String newValue)
    {
                dbnbDB.setFieldData( "AttorneyWorkPhone", newValue.toString() );
    }

    public String getAttorneyWorkPhone()
    {
        String           sValue = dbnbDB.getFieldData( "AttorneyWorkPhone" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setAttorneyCellPhone(String newValue)
    {
                dbnbDB.setFieldData( "AttorneyCellPhone", newValue.toString() );
    }

    public String getAttorneyCellPhone()
    {
        String           sValue = dbnbDB.getFieldData( "AttorneyCellPhone" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setComments(String newValue)
    {
                dbnbDB.setFieldData( "Comments", newValue.toString() );
    }

    public String getComments()
    {
        String           sValue = dbnbDB.getFieldData( "Comments" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setAuditNotes(String newValue)
    {
                dbnbDB.setFieldData( "AuditNotes", newValue.toString() );
    }

    public String getAuditNotes()
    {
        String           sValue = dbnbDB.getFieldData( "AuditNotes" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setPatientIsClaus(Integer newValue)
    {
                dbnbDB.setFieldData( "PatientIsClaus", newValue.toString() );
    }

    public Integer getPatientIsClaus()
    {
        String           sValue = dbnbDB.getFieldData( "PatientIsClaus" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setPatientIsHWRatio(Integer newValue)
    {
                dbnbDB.setFieldData( "PatientIsHWRatio", newValue.toString() );
    }

    public Integer getPatientIsHWRatio()
    {
        String           sValue = dbnbDB.getFieldData( "PatientIsHWRatio" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setAssignedToID(Integer newValue)
    {
                dbnbDB.setFieldData( "AssignedToID", newValue.toString() );
    }

    public Integer getAssignedToID()
    {
        String           sValue = dbnbDB.getFieldData( "AssignedToID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setAssignedToRoleID(Integer newValue)
    {
                dbnbDB.setFieldData( "AssignedToRoleID", newValue.toString() );
    }

    public Integer getAssignedToRoleID()
    {
        String           sValue = dbnbDB.getFieldData( "AssignedToRoleID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setCaseAdministratorID(Integer newValue)
    {
                dbnbDB.setFieldData( "CaseAdministratorID", newValue.toString() );
    }

    public Integer getCaseAdministratorID()
    {
        String           sValue = dbnbDB.getFieldData( "CaseAdministratorID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setBillToContactID(Integer newValue)
    {
                dbnbDB.setFieldData( "BillToContactID", newValue.toString() );
    }

    public Integer getBillToContactID()
    {
        String           sValue = dbnbDB.getFieldData( "BillToContactID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setURContactID(Integer newValue)
    {
                dbnbDB.setFieldData( "URContactID", newValue.toString() );
    }

    public Integer getURContactID()
    {
        String           sValue = dbnbDB.getFieldData( "URContactID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setReferredByContactID(Integer newValue)
    {
                dbnbDB.setFieldData( "ReferredByContactID", newValue.toString() );
    }

    public Integer getReferredByContactID()
    {
        String           sValue = dbnbDB.getFieldData( "ReferredByContactID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setPatientHasImplants(Integer newValue)
    {
                dbnbDB.setFieldData( "PatientHasImplants", newValue.toString() );
    }

    public Integer getPatientHasImplants()
    {
        String           sValue = dbnbDB.getFieldData( "PatientHasImplants" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setPatientHasMetalInBody(Integer newValue)
    {
                dbnbDB.setFieldData( "PatientHasMetalInBody", newValue.toString() );
    }

    public Integer getPatientHasMetalInBody()
    {
        String           sValue = dbnbDB.getFieldData( "PatientHasMetalInBody" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setPatientHasAllergies(Integer newValue)
    {
                dbnbDB.setFieldData( "PatientHasAllergies", newValue.toString() );
    }

    public Integer getPatientHasAllergies()
    {
        String           sValue = dbnbDB.getFieldData( "PatientHasAllergies" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setPatientHasRecentSurgery(Integer newValue)
    {
                dbnbDB.setFieldData( "PatientHasRecentSurgery", newValue.toString() );
    }

    public Integer getPatientHasRecentSurgery()
    {
        String           sValue = dbnbDB.getFieldData( "PatientHasRecentSurgery" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setPatientHasPreviousMRIs(Integer newValue)
    {
                dbnbDB.setFieldData( "PatientHasPreviousMRIs", newValue.toString() );
    }

    public Integer getPatientHasPreviousMRIs()
    {
        String           sValue = dbnbDB.getFieldData( "PatientHasPreviousMRIs" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setPatientHasKidneyLiverHypertensionDiabeticConditions(Integer newValue)
    {
                dbnbDB.setFieldData( "PatientHasKidneyLiverHypertensionDiabeticConditions", newValue.toString() );
    }

    public Integer getPatientHasKidneyLiverHypertensionDiabeticConditions()
    {
        String           sValue = dbnbDB.getFieldData( "PatientHasKidneyLiverHypertensionDiabeticConditions" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setPatientIsPregnant(Integer newValue)
    {
                dbnbDB.setFieldData( "PatientIsPregnant", newValue.toString() );
    }

    public Integer getPatientIsPregnant()
    {
        String           sValue = dbnbDB.getFieldData( "PatientIsPregnant" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setPatientHeight(String newValue)
    {
                dbnbDB.setFieldData( "PatientHeight", newValue.toString() );
    }

    public String getPatientHeight()
    {
        String           sValue = dbnbDB.getFieldData( "PatientHeight" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setPatientWeight(Integer newValue)
    {
                dbnbDB.setFieldData( "PatientWeight", newValue.toString() );
    }

    public Integer getPatientWeight()
    {
        String           sValue = dbnbDB.getFieldData( "PatientWeight" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setPatientHasImplantsDesc(String newValue)
    {
                dbnbDB.setFieldData( "PatientHasImplantsDesc", newValue.toString() );
    }

    public String getPatientHasImplantsDesc()
    {
        String           sValue = dbnbDB.getFieldData( "PatientHasImplantsDesc" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setPatientHasMetalInBodyDesc(String newValue)
    {
                dbnbDB.setFieldData( "PatientHasMetalInBodyDesc", newValue.toString() );
    }

    public String getPatientHasMetalInBodyDesc()
    {
        String           sValue = dbnbDB.getFieldData( "PatientHasMetalInBodyDesc" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setPatientHasAllergiesDesc(String newValue)
    {
                dbnbDB.setFieldData( "PatientHasAllergiesDesc", newValue.toString() );
    }

    public String getPatientHasAllergiesDesc()
    {
        String           sValue = dbnbDB.getFieldData( "PatientHasAllergiesDesc" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setPatientHasRecentSurgeryDesc(String newValue)
    {
                dbnbDB.setFieldData( "PatientHasRecentSurgeryDesc", newValue.toString() );
    }

    public String getPatientHasRecentSurgeryDesc()
    {
        String           sValue = dbnbDB.getFieldData( "PatientHasRecentSurgeryDesc" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setPatientHasPreviousMRIsDesc(String newValue)
    {
                dbnbDB.setFieldData( "PatientHasPreviousMRIsDesc", newValue.toString() );
    }

    public String getPatientHasPreviousMRIsDesc()
    {
        String           sValue = dbnbDB.getFieldData( "PatientHasPreviousMRIsDesc" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setPatientHasKidneyLiverHypertensionDiabeticConditionsDesc(String newValue)
    {
                dbnbDB.setFieldData( "PatientHasKidneyLiverHypertensionDiabeticConditionsDesc", newValue.toString() );
    }

    public String getPatientHasKidneyLiverHypertensionDiabeticConditionsDesc()
    {
        String           sValue = dbnbDB.getFieldData( "PatientHasKidneyLiverHypertensionDiabeticConditionsDesc" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setPatientIsPregnantDesc(String newValue)
    {
                dbnbDB.setFieldData( "PatientIsPregnantDesc", newValue.toString() );
    }

    public String getPatientIsPregnantDesc()
    {
        String           sValue = dbnbDB.getFieldData( "PatientIsPregnantDesc" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setCaseAdministrator2ID(Integer newValue)
    {
                dbnbDB.setFieldData( "CaseAdministrator2ID", newValue.toString() );
    }

    public Integer getCaseAdministrator2ID()
    {
        String           sValue = dbnbDB.getFieldData( "CaseAdministrator2ID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setCaseAdministrator3ID(Integer newValue)
    {
                dbnbDB.setFieldData( "CaseAdministrator3ID", newValue.toString() );
    }

    public Integer getCaseAdministrator3ID()
    {
        String           sValue = dbnbDB.getFieldData( "CaseAdministrator3ID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setCaseAdministrator4ID(Integer newValue)
    {
                dbnbDB.setFieldData( "CaseAdministrator4ID", newValue.toString() );
    }

    public Integer getCaseAdministrator4ID()
    {
        String           sValue = dbnbDB.getFieldData( "CaseAdministrator4ID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setEmployerLocationID(Integer newValue)
    {
                dbnbDB.setFieldData( "EmployerLocationID", newValue.toString() );
    }

    public Integer getEmployerLocationID()
    {
        String           sValue = dbnbDB.getFieldData( "EmployerLocationID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setPatientEmail(String newValue)
    {
                dbnbDB.setFieldData( "PatientEmail", newValue.toString() );
    }

    public String getPatientEmail()
    {
        String           sValue = dbnbDB.getFieldData( "PatientEmail" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setMobilePhoneCarrier(String newValue)
    {
                dbnbDB.setFieldData( "MobilePhoneCarrier", newValue.toString() );
    }

    public String getMobilePhoneCarrier()
    {
        String           sValue = dbnbDB.getFieldData( "MobilePhoneCarrier" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setIsCurrentlyOnMeds(Integer newValue)
    {
                dbnbDB.setFieldData( "IsCurrentlyOnMeds", newValue.toString() );
    }

    public Integer getIsCurrentlyOnMeds()
    {
        String           sValue = dbnbDB.getFieldData( "IsCurrentlyOnMeds" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setIsCurrentlyOnMedsDesc(String newValue)
    {
                dbnbDB.setFieldData( "IsCurrentlyOnMedsDesc", newValue.toString() );
    }

    public String getIsCurrentlyOnMedsDesc()
    {
        String           sValue = dbnbDB.getFieldData( "IsCurrentlyOnMedsDesc" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

}    // End of bltNIM3_CaseAccount class definition
