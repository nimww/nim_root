

package com.winstaff;

import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public interface InttUpdateItemStatusLI extends dbTableInterface
{

    public void setUpdateItemStatusID(Integer newValue);
    public Integer getUpdateItemStatusID();
    public void setUpdateItemStatusText(String newValue);
    public String getUpdateItemStatusText();
}    // End of bltUpdateItemStatusLI class definition
