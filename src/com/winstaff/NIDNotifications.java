package com.winstaff;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.SQLException;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.AcroFields;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;
import com.winstaff.NIM3_EncounterObject2;
import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

/**
 * This pulls all the referrals from the W3 region and groups by Payer/Adjuster and type of modality
 */

public class NIDNotifications{
	/**
	 * Integer to id scanpass for call to schedule
	 */
		public final static int SP_NOTICE_CALL_TO_SCHEDULE_IW = 1;
		public final static int SP_SEND_RX_REF_MD = 2;
		public final static int SP_NOTICE_APPOINTMENT_DETAILS_REF_MD = 3;
		public final static int SP_FINAL_READY_TO_VIEW_REF_MD = 4;	
		
		/**
		 * dthedhgd
		 * @param iEncounterID ID of encounter to pull info from
		 * @param spNotice Defines which SP to use 
		 * @param currentuseraccount user id of current user logged into system
		 * 1 = sp_ic_note, 
		 * 2 = need rx, 
		 * 3 = appt details refmd, 
		 * 4 = final md
		 * @throws IOException
		 * @throws DocumentException
		 */
		public static final void SP_Notifications(int iEncounterID, int spNotice, int currentuseraccount) throws IOException, DocumentException{				
			
			
			PdfReader reader = null;
			AcroFields form = null;
			PdfStamper stamper = null;			
			
			NIM3_EncounterObject2 myEO2 = new NIM3_EncounterObject2(iEncounterID,"");
			
			String outputName = random(iEncounterID,spNotice);
			
			java.text.SimpleDateFormat df = new java.text.SimpleDateFormat("MM-dd-yyyy");
			
			java.util.Enumeration eList_Service = myEO2.getNIM3_Service_List().elements(); 
			bltNIM3_Service        working_bltNIM3_Service;
			ListElement         leCurrentElement_Service; 
			
			String cpt = "";
			while (eList_Service.hasMoreElements()) {
                leCurrentElement_Service    = (ListElement) eList_Service.nextElement();
                working_bltNIM3_Service  = (bltNIM3_Service) leCurrentElement_Service.getObject();
                if ( working_bltNIM3_Service.getServiceStatusID()==1) {
                    cpt += working_bltNIM3_Service.getCPTBodyPart();
                }                
            }
			
			if(spNotice == SP_NOTICE_CALL_TO_SCHEDULE_IW ){
				String icReferralTemplate = "/home/g/Downloads/ICReferralNotification.pdf";
				reader = new PdfReader( icReferralTemplate );
				stamper = new PdfStamper(reader,new FileOutputStream(outputName),'\0', true);
				form = stamper.getAcroFields(); 
				
				form.setField("ScanPass Authorization#_aUsErGbplz-Ew5L5MeKLcQ",  myEO2.getNIM3_Encounter().getScanPass());
				form.setField("First name_Bn4u0bTA5tyTE*lTW6*Fjg", myEO2.getNIM3_CaseAccount().getPatientFirstName());
				form.setField("Last name_uSH6LRDDFZTKTrSpswR4mw", myEO2.getNIM3_CaseAccount().getPatientLastName());
				form.setField("Gender_siTg2HzcD23l3bcBIr4PIg", myEO2.getNIM3_CaseAccount().getPatientGender());
				form.setField("Address_liERv6dEi*RGeIwGfYQkaw", myEO2.getNIM3_CaseAccount().getPatientAddress1() + " " + myEO2.getNIM3_CaseAccount().getPatientAddress2() + myEO2.getNIM3_CaseAccount().getPatientCity() +  new bltStateLI(myEO2.getNIM3_CaseAccount().getPatientStateID()).getShortState() + myEO2.getNIM3_CaseAccount().getPatientZIP());
				form.setField("Phone_R7upbV1r5pYW-rJlhVwTMA", myEO2.getNIM3_CaseAccount().getPatientHomePhone());
				form.setField("DOB_albURqR7t5G7kclJ*u4Lhg", df.format(myEO2.getNIM3_CaseAccount().getPatientDOB()));
				form.setField("Height_j-nAy-MezmA6jRHR4ICggA", myEO2.getNIM3_CaseAccount().getPatientHeight());
				form.setField("Weight_L*HAGsgRQOEofAfP3Q8IVw", myEO2.getNIM3_CaseAccount().getPatientWeight().toString());
				form.setField("Referring Physician_RH7Rp-JqUqjqVsr0RVNStg", myEO2.getReferral_ReferringDoctor().getContactFirstName() + " " + myEO2.getReferral_ReferringDoctor().getContactLastName());
				form.setField("Physician Phone_WlTNMeAKxIbuKLemmpSl5w", myEO2.getReferral_ReferringDoctor().getContactPhone());
				form.setField("Physician Fax_ansbmEAWr4MceWTAqHS99A", myEO2.getReferral_ReferringDoctor().getContactFax());
				form.setField("Authorized Services_rZbi0Rg8pC18TtqjBO4ubw", cpt);
				form.setField("Please fax all medical reports_4bQ4GUCCKSaAnn2oiSZv2g", myEO2.getNIM3_Referral().getReferenceFax());
				form.setField("Hand Carry CD?_GQQ-*9eyfgLvfUzn1PvzlA", translateNumberYesNo(myEO2.getNIM3_Encounter().getRequiresHandCarryCD()));
			}
			else if(spNotice == SP_SEND_RX_REF_MD){
				//TODO;will plug in fields to be set
			}
			else if(spNotice == SP_NOTICE_APPOINTMENT_DETAILS_REF_MD){
				//TODO;will plug in fields to be set
			}
			else if(spNotice == SP_FINAL_READY_TO_VIEW_REF_MD){
				//TODO;will plug in fields to be set
			}
			
			//email
			//write_To_Comtrack_SP
			
			
			System.out.println(form.getFields());
			stamper.close();
		}
		
		public static void write_To_Comtrack_SP(Integer iCurrentUserID, Integer iEncounterID) throws SQLException{
			bltUserAccount CurrentUserAccount = new bltUserAccount(iCurrentUserID);
			bltNIM3_PayerMaster myPM = new bltNIM3_PayerMaster(CurrentUserAccount.getPayerID());
			bltNIM3_Encounter myEnc = new bltNIM3_Encounter(iEncounterID);
			bltNIM3_Referral myRef = new bltNIM3_Referral(myEnc.getReferralID());
			
			
			bltNIM3_CommTrack        myCT = new bltNIM3_CommTrack();
			
			myCT.setEncounterID(iEncounterID);
		    myCT.setReferralID(myEnc.getReferralID());
		    myCT.setCaseID(myRef.getCaseID());
		    myCT.setMessageName(CurrentUserAccount.getContactFirstName()+" "+CurrentUserAccount.getContactLastName());
		    myCT.setMessageCompany(myPM.getPayerName());
		    myCT.setUniqueCreateDate(PLCUtils.getNowDate(false));
		    myCT.setUniqueModifyDate(PLCUtils.getNowDate(false));
		    myCT.setComments("ScanPass sent out on " + PLCUtils.getNowDate(false));
		    myCT.commitData();
		}		
		
		public static void email(String emailTo, String emailFrom, String theSubject, String theBody, String path) throws SQLException, IOException{
			bltEmailTransaction email = new bltEmailTransaction();
					            
			email.setEmailTo(emailTo);
			email.setEmailFrom(emailFrom);
			email.setEmailSubject(theSubject);
			email.setEmailBody(theBody);
			email.setEmailBodyType(emailType_V3.HTML_TYPE);
			email.setVirtualAttachment(path);				
			email.setTransactionDate(new java.util.Date());
			email.commitData();				
			}
		
		public static String translateNumberYesNo(int in){
			if (in ==2) return "Yes";
			
			return "No";
		}
		/**
		 * This method is used to generate a random number to be the file name
		 * @param encounterID will be part of the random string to create a unique Pdf file name
		 * @param spNotice will determine which ScanPass to fire off
		 * @return will be a alphanumeric String that will be used as the file name
		 */
		public static String random(int encounterID, int spNotice){
			DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");			
			Date date = new Date();
			String currentDate = dateFormat.format(date);
			
			if(spNotice == SP_NOTICE_CALL_TO_SCHEDULE_IW ){
				
			} else {}
			
			java.util.Random r = new java.util.Random();
		    int i = 1, n = 0;
		    char c;
		    String str="";
		    for (int t = 0; t < 3; t++) {
		        while (true) {
		            i = r.nextInt(10);
		            if (i > 5 && i < 10) {

		                if (i == 9) {
		                    i = 90;
		                    n = 90;
		                    break;
		                }
		                if (i != 90) {
		                    n = i * 10 + r.nextInt(10);
		                    while (n < 65) {
		                        n = i * 10 + r.nextInt(10);
		                    }
		                }

		                break;
		            }
		        }
		        c=(char)n;

		        str= String.valueOf(c)+str;
		    }
		    while(true){
		    i = r.nextInt(10000000);
		    if(i>999999)
		        break;
		    }
		    str=str+i;
		    String new_Pdf_Name = "NID" + "_" + str + "_" + encounterID + "_" +  currentDate + "_" + "SP_TO_IC.pdf";
		    System.out.println(new_Pdf_Name);
		    return new_Pdf_Name;
		}
		
	}
