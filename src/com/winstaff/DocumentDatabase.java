package com.winstaff;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Date;

import com.lowagie.text.Document;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.pdf.PdfWriter;

/**
 * This class can read and write pdf files to the database and can perform one
 * file format conversion. This class UPDATES an existing tnim3_documentblob table
 * with the document that is stored in a column named 'document'.
 * 
 * Current conversion is: TIF -> PDF.
 * 
 * This class does NOT convert Word documents to PDF.
 * 
 * Documents are read from the file system and stored into a postgresql table
 * column with the data type: BYTEA. While this allows for easy programming, it
 * does NOT allow for large files to be stored. If you need to store large files
 * you will need to re-work with class to incorporate LOB's.
 * 
 * This class was tested on a mock table created by this:
 * 
 * CREATE TABLE Files ( id SERIAL PRIMARY KEY, filename VARCHAR(64) NULL, data
 * BYTEA NULL);
 * 
 * NOTES:
 * 
 * Use this sql statement to populate the tnim3_documentblob table from filenames from the 
 * master tnim3_document table:
 * 
 * INSERT INTO tnim3_documentblob (filename)
 * 	(select filename FROM tnim3_document where filename is not null
 * 		EXCEPT SELECT filename FROM tnim3_documentblob where filename  is not null);
 * 
 */
public class DocumentDatabase {

	public static String baseDirectory = null;

	// Current unexplained memory, connection limitations forces us to 'cap' the files that this class will process all at once..
	// This int value is passed as an argument.
	
	public static final int maxFilesToProcess = 20000;
	public static int filesToProcess = 0;
	
	private String sClassIdentifier;

	ConfigurationInformation config = new ConfigurationInformation();

	/**
	 * Write file to database. You need to supply fully qualified pathname.
	 * 
	 * @param filename
	 */
	public void writeFile(String path, String filename) {
		try {
			java.sql.Connection cCon = java.sql.DriverManager.getConnection(
					ConfigurationInformation.sURL,
					ConfigurationInformation.sUser,
					ConfigurationInformation.sPassword);
			java.sql.PreparedStatement ps = cCon
					.prepareStatement("UPDATE \"public\".\"tnim3_documentblob\" SET document = ? where filename = ?");
			this.sClassIdentifier = this.getClass().getName();
			DebugLogger.println(sClassIdentifier + ": Query: "
					+ "Updating tnim3_documentblob in DocumentDatabase class");
			File file = new File(path + filename);
			FileInputStream fis = new FileInputStream(file);
			ps.setBinaryStream(1, fis, (int) file.length());
			ps.setString(2, file.getName());
			ps.executeUpdate();
			ps.close();
			fis.close();
			cCon.close();
		} catch (Exception eeee) {
			System.out.println("File INSERT Error: " + eeee.toString());
		}

	}

	/**
	 * Write file to database.
	 * 
	 * @param filename
	 */
	public void writeFile(String filename) {
		try {
			java.sql.Connection cCon = java.sql.DriverManager.getConnection(
					ConfigurationInformation.sURL,
					ConfigurationInformation.sUser,
					ConfigurationInformation.sPassword);
			java.sql.PreparedStatement ps = cCon
					.prepareStatement("UPDATE \"public\".\"tnim3_documentblob\" SET document = ? where filename = ?");
			this.sClassIdentifier = this.getClass().getName();
			DebugLogger.println(sClassIdentifier + ": Query: "
					+ "Updating tnim3_documentblob in DocumentDatabase class");
			File file = new File(filename);
			FileInputStream fis = new FileInputStream(file);
			ps.setBinaryStream(1, fis, (int) file.length());
			ps.setString(2, file.getName());
			ps.executeUpdate();
			ps.close();
			fis.close();
			cCon.close();
		} catch (Exception eeee) {
			System.out.println("File INSERT Error: " + eeee.toString());
		}

	}

	/**
	 * Write file to database.
	 * 
	 * @param filename
	 */
	public void writeFileandName(String name, String filename) {
		try {
			java.sql.Connection cCon = java.sql.DriverManager.getConnection(
					ConfigurationInformation.sURL,
					ConfigurationInformation.sUser,
					ConfigurationInformation.sPassword);
			java.sql.PreparedStatement ps = cCon
					.prepareStatement("UPDATE \"public\".\"tnim3_documentblob\" SET document = ?, filename = ?, where filename = ?");
			this.sClassIdentifier = this.getClass().getName();
			DebugLogger.println(sClassIdentifier + ": Query: "
					+ "Updating tnim3_documentblob in DocumentDatabase class");
			
			File file = new File(filename);
			FileInputStream fis = new FileInputStream(file);
			ps.setBinaryStream(1, fis, (int) file.length());
			ps.setString(2, name);
			ps.setString(3, file.getName());
			DebugLogger.println("---- " + ps.toString());
			ps.executeUpdate();
			ps.close();
			fis.close();
			cCon.close();
		} catch (Exception eeee) {
			System.out.println("File INSERT Error: " + eeee.toString());
		}

	}

	/**
	 * 
	 * @param name
	 */
	public void insertFilename(String name) {
		
		java.sql.PreparedStatement stmt = null;
		
		try {
			System.out.println("1");
			java.sql.Connection cCon = java.sql.DriverManager.getConnection(
					ConfigurationInformation.sURL,
					ConfigurationInformation.sUser,
					ConfigurationInformation.sPassword);
			System.out.println("2");
			String sSQLString = "insert into \"public\".\"tnim3_documentblob\" (filename) values (?)";
			stmt = cCon.prepareStatement(sSQLString);
			stmt.setString(1, name);
		
			System.out.println("--> "+sSQLString);
			DebugLogger.println(sClassIdentifier + ": Query: "
					+ stmt.toString());
			
			stmt.executeUpdate();
			cCon.close();
		} catch (Exception eeee) {
			System.out.println("3");
			System.out.println("File INSERT Error: " + eeee.toString());
		}

	}
	/**
	 * 
	 * Read source file from database table and copy to target file on file system.
	 * 
	 * @param source
	 * @param target
	 */
	public void readFile(String source, String target) {
		try {
			java.sql.Connection cCon = java.sql.DriverManager.getConnection(
					ConfigurationInformation.sURL,
					ConfigurationInformation.sUser,
					ConfigurationInformation.sPassword);

			// Replace any single qoute with 2 single qoutes
			if (source.contains("'")) {
				source = source.replaceAll("'", "''");
			}

			String stmt = "SELECT document FROM tnim3_documentblob WHERE filename = '"
					+ source + "'";
			System.out.println("stmt= " + stmt);			
			PreparedStatement ps = cCon.prepareStatement(stmt);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				byte[] imgBytes = rs.getBytes(1);
				FileOutputStream out = new FileOutputStream(target);
				try {
					out.write(imgBytes);
				} finally {
					out.close();
				}
			}
			rs.close();
			ps.close();
			cCon.close();
		} catch (Exception eeee) {
			System.out.println("File INSERT Error: " + eeee.toString());
		}

	}

	/**
	 * 
	 * Read source file from database table and return contents in byte array.
	 * 
	 * @param source - filename of interest.
	 * 
	 */
	public byte[] readFile(String source) {
		byte[] imgBytes = null;
		try {
			java.sql.Connection cCon = java.sql.DriverManager.getConnection(
					ConfigurationInformation.sURL,
					ConfigurationInformation.sUser,
					ConfigurationInformation.sPassword);

			// Replace any single qoute with 2 single qoutes
			if (source.contains("'")) {
				source = source.replaceAll("'", "''");
			}

			String stmt = "SELECT document FROM tnim3_documentblob WHERE filename = '"
					+ source + "'";
			PreparedStatement ps = cCon.prepareStatement(stmt);
			ResultSet rs = ps.executeQuery();
			int i = 0;
			while (rs.next()) {
				imgBytes = rs.getBytes(1);
			}
			rs.close();
			ps.close();
			cCon.close();
		} catch (Exception eeee) {
			System.out.println("Error making byte array: " + eeee.toString());
		}
		
//		System.out.println(imgBytes[0]);
//		System.out.println(imgBytes[1]);
//		System.out.println(imgBytes[2]);
		
		return imgBytes;
	}
	
	/**
	 * Convert tif to pdf and then store in database.
	 * 
	 * @param inputFile
	 * @param outputFile
	 */
	public void tiff2Pdf2Database(String inputFile, String outputFile) {
		createPdf(inputFile, outputFile, true);
		writeFile(outputFile);
	}

	/**
	 * 
	 * Convenience method: convert TIF to PDF.
	 * 
	 * @param inputFile
	 * @param outputFile
	 */
	public void tiff2Pdf(String inputFile, String outputFile) {
		createPdf(inputFile, outputFile, true);
	}

	/**
	 * 
	 * General conversion method. Does NOT work correctly for doc., docx files.
	 * 
	 * @param inputFile
	 * @param outputFile
	 * @param isPictureFile
	 */
	public void createPdf(String inputFile, String outputFile,
			boolean isPictureFile) {

		Document pdfDocument = new Document(PageSize.A4, 0, 0, 0, 0);
		String pdfFilePath = outputFile;

		try {
			FileOutputStream fileOutputStream = new FileOutputStream(
					pdfFilePath);
			PdfWriter writer = null;
			writer = PdfWriter.getInstance(pdfDocument, fileOutputStream);
			writer.open();
			pdfDocument.open();

			if (isPictureFile) {
				System.out.println("IMAGE file");
				com.lowagie.text.Image img = com.lowagie.text.Image
						.getInstance(inputFile);

				if (img != null) { // scale if too big to fit on page
					if (img.scaledWidth() > PageSize.A4.width()
							|| img.scaledHeight() > PageSize.A4.height()) {
						// if images have a different resolution (dpi value) for
						// x and y the scaling considers it
						if (img.getDpiX() != 0 && img.getDpiY() != 0
								&& img.getDpiX() != img.getDpiY()) {
							img.scalePercent(100);
							float percentX = (PageSize.A4.width() * 100)
									/ img.scaledWidth();
							float percentY = ((PageSize.A4.height() * 100) / img
									.scaledHeight());
							img.scalePercent(percentX, percentY);
							img.setWidthPercentage(0);
						} else {
							// scaling the image if DPI values for x and y are
							// the same // no customized scaling i.e. different
							// values for x and y necessary
							img.scaleToFit(PageSize.A4.width(),
									PageSize.A4.height());
						}
					}
					pdfDocument.add(img);
				}
			}
			/**
			 * Proceed if the file given is (.txt,.html,.doc etc)
			 */
			else {
				File file = new File(inputFile);
				pdfDocument.add(new Paragraph(org.apache.commons.io.FileUtils
						.readFileToString(file)));
			}

			pdfDocument.close();
			writer.close();
		} catch (Exception exception) {
			System.out.println("Document Exception!" + exception);
		}
	}

	/**
	 * Method that returns TRUE if incoming infile exists in the incoming
	 * directory.
	 * 
	 * @param directory
	 * @return
	 */
	public boolean findFile(String directory, String infile) {

		boolean result = false;
		File dir = new File(directory);
		String[] children = dir.list();
		if (children == null) {
			// Either dir does not exist or is not a directory
		} else {
			for (int i = 0; i < children.length; i++) {
				// Get filename of file or directory
				String filename = children[i];
				if (filename.compareTo(infile) == 0) {
					result = true;
					break;
				}
				// DebugLogger.println(filename);
			}
		}
		return result;
	}

	/**
	 * Helper method to list files in a directory. Used really just for testing.
	 * 
	 * @param directory
	 *            - Directory that you want to have files listed in.
	 * 
	 */
	public void listFiles(String directory) {
		int count = 0;
		File dir = new File(directory);
		String[] children = dir.list();
		if (children == null) {
			// Either dir does not exist or is not a directory
		} else {
			for (int i = 0; i < children.length; i++) {
				// Get filename of file or directory
				String filename = children[i];
				DebugLogger.println(filename);
				count++;
			}
		}
		DebugLogger.println("\nThere are " + count + " files in " + directory);
	}

	/*
	 * This snippet complements of:
	 * 
	 * http://coreygoldberg.blogspot.com/2008/06/java-run-system-command-and-return
	 * .html
	 */
	public static String cmdExec(String cmdLine) {
		String line;
		String output = "";
		try {
			Process p = Runtime.getRuntime().exec(cmdLine);
			BufferedReader input = new BufferedReader(new InputStreamReader(
					p.getInputStream()));
			while ((line = input.readLine()) != null) {
				output += (line + '\n');
			}
			input.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return output;
	}

	/**
	 * This method is the main workhorse of this class.
	 * 
	 * Go through the nim3_documents table, read the filename, fetch that file
	 * from a directory and store it the nim3_documents table.
	 * 
	 * After we store the file in the database, we read it from the database to
	 * a newly created file on disk and do a differences between that file and
	 * the source. If the files are identical then the process was successful.
	 * This will then allow us to delete the files on the file system after we
	 * are all done here.
	 * 
	 * This table has had a column added to it to store that file taken from the
	 * file system.
	 * 
	 * @param update - Set TRUE if you want to load a document only if the 'document' column is empty.
	 * 				 - Set FALSE to process all records in the 'tnim3_documentblob' table.
	 * 
	 */
	public void insert(boolean update) {

		int count1 = 0;
		int count2 = 0;
		int count = 0;

		baseDirectory = config.sUploadFolderDirectory;

		searchDB2 mySDBA = new searchDB2();
		try {
			
			// Set update = TRUE if you have already loaded the 'tnim3_documentblob' table with BLOB's
			// Set to FALSE if you are loading it for the first time.
			String mySQLA = null;
			if (!update) {
				mySQLA = "SELECT filename FROM tnim3_documentblob WHERE filename != ''";
			} else {
				mySQLA = "SELECT filename FROM tnim3_documentblob WHERE document IS NULL AND filename != ''";
			}
			
			java.sql.ResultSet myRSA = mySDBA.executeStatement(mySQLA);
			//while (myRSA.next() & count < filesToProcess) {
				
			while (count < filesToProcess) {			
				//java.sql.ResultSet myRSA = mySDBA.executeStatement(mySQLA);
				if (!myRSA.next()) {
					break;
				}
				
				String tmp = myRSA.getString("filename");
				//DebugLogger.println("filename = "+tmp);
				
				// Look for file on file system.
				if (findFile(baseDirectory, tmp)) {
					DebugLogger.println(tmp + " exists.");
					count1++;

					// Now that we know the filename exists in the database and
					// on the file system
					// we can grab this file off disc and put it into the
					// database.

					DebugLogger.println("Writing file to database.");
					DebugLogger.println("count = "+count);
					String sourceFile = baseDirectory + tmp;
					writeFile(sourceFile);

//					DebugLogger
//							.println("\nRead file in database table and write out to file location on disk to compare.");
//					String targetFile = baseDirectory + "compare";
//					readFile(tmp, targetFile);
//
//					// Now compare the 2 files.
//					// Linux specific
//					String buffer = cmdExec("diff " + sourceFile + " "
//							+ targetFile);
//					if (buffer.length() == 0) {
//						DebugLogger.println("\nFiles are identical");
//						DebugLogger.println(buffer);
//					} else {
//						DebugLogger.println("\nFiles are DIFFERENT");
//						DebugLogger.println("\nFATAL ERROR!");
//						DebugLogger.println(buffer);
//					}

				} else {
					DebugLogger.println("count = "+count);					
					DebugLogger.println(tmp + " does not exist.");
					count2++;
				}
				count++;
			}
		} catch (Exception e) {
			System.out.println(e);
		}
		DebugLogger.println("\nProcessed " + count1
				+ " files that are in the database AND file system.");
		DebugLogger.println("\nProcessed " + count2
				+ " files that are in the database but not the file system.");
	}

	public void tester() {
		// System.out.println("\nConvert tif to pdf and store in database.");
		// String pdfFile = "/devel/resume/_CURRENT/x.pdf";
		// fd.tiff2Pdf2Database("/devel/resume/_CURRENT/c.TIF", pdfFile);
		// System.out.println("FINISHED.");
		//
		// System.out.println("\nRead file in database table and write out to file location on disk.");
		// String pdfTarget = "/devel/resume/_CURRENT/result.pdf";
		// fd.readFile(pdfFile, targetFile);
		// System.out.println("FINISHED.");
	}

	public static void main(String[] args) {

		long start = System.currentTimeMillis();

		boolean update = false;
		
		if (args.length < 2 || args[0].compareToIgnoreCase("all") != 0 && args[0].compareToIgnoreCase("update") != 0) {
			System.err.println("This script takes 2 arguments.");
			System.err.println("Use 'ALL' to overwrite all documents already in db.");
			System.err.println("Use 'UPDATE' to process documents not already loaded into db.");
			System.err.println("The second argument is the number of files to process.");
			System.err.println("This number needs do be <= 20,000");
			System.err.println("Example: $scriptName UPDATE 20000");
	        System.exit(1);
		}
	
		// 1st argument
		String cmp = args[0].toLowerCase();		
		if (cmp.compareTo("update")==0) {
			update = true;
		}
		
		// 2nd argument = number of files to process.
		int num = 0;
		try { num = new Integer(args[1]).intValue();	}
		catch (Exception e) {
			System.err.println("2nd argument needs to be an Integer");
	        System.exit(1);			
		}		
		if (num > maxFilesToProcess) {
			System.err.println("2nd argument exceeds "+maxFilesToProcess);
	        System.exit(1);			
		}
	
		filesToProcess = num;
		DocumentDatabase fd = new DocumentDatabase();		
		fd.insert(update);

		long end = System.currentTimeMillis();
		float elapsedTimeSec = (end - start) / 1000F;
		float elapsedTimeMin = (end - start) / (60 * 1000F);

		DebugLogger.println("\nElasped time (minutes): " + elapsedTimeMin);
		DebugLogger.println("\nElasped time (seconds): " + elapsedTimeSec);
	}
	
	
	public static void main_other_test(String[] args) {

		DocumentDatabase fd = new DocumentDatabase();		
		fd.insertFilename("asdfasdf");

	}
	
	public static void main_test(String[] args) {

		byte[] res = null;
		 
		DocumentDatabase fd = new DocumentDatabase();		
		
		res = fd.readFile("doc_gwuzwu_2012-03-21_CC177NN73FP_fw4.pdf");
		
		System.out.println(res);
		
	}

	
	public static void main_new_test(String[] args) {

		long start = System.currentTimeMillis();
		boolean update = true;	
		
		filesToProcess = 999999;
		DocumentDatabase fd = new DocumentDatabase();		
		fd.insert(update);

		long end = System.currentTimeMillis();
		float elapsedTimeSec = (end - start) / 1000F;
		float elapsedTimeMin = (end - start) / (60 * 1000F);

		DebugLogger.println("\nElasped time (minutes): " + elapsedTimeMin);
		DebugLogger.println("\nElasped time (seconds): " + elapsedTimeSec);		
		
	}
	
}
