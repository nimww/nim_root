package com.winstaff;


import com.winstaff.dbtProfessionalEducation;
import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public interface InttProfessionalEducation extends dbTableInterface
{

    public void setProfessionalEducationID(Integer newValue);
    public Integer getProfessionalEducationID();
    public void setUniqueCreateDate(Date newValue);
    public Date getUniqueCreateDate();
    public void setUniqueModifyDate(Date newValue);
    public Date getUniqueModifyDate();
    public void setUniqueModifyComments(String newValue);
    public String getUniqueModifyComments();
    public void setPhysicianID(Integer newValue);
    public Integer getPhysicianID();
    public void setDegreeID(Integer newValue);
    public Integer getDegreeID();
    public void setOther(String newValue);
    public String getOther();
    public void setStartDate(Date newValue);
    public Date getStartDate();
    public void setDateOfGraduation(Date newValue);
    public Date getDateOfGraduation();
    public void setEndDate(Date newValue);
    public Date getEndDate();
    public void setFocus(String newValue);
    public String getFocus();
    public void setSchoolName(String newValue);
    public String getSchoolName();
    public void setSchoolAddress1(String newValue);
    public String getSchoolAddress1();
    public void setSchoolAddress2(String newValue);
    public String getSchoolAddress2();
    public void setSchoolCity(String newValue);
    public String getSchoolCity();
    public void setSchoolStateID(Integer newValue);
    public Integer getSchoolStateID();
    public void setSchoolProvince(String newValue);
    public String getSchoolProvince();
    public void setSchoolZIP(String newValue);
    public String getSchoolZIP();
    public void setSchoolCountryID(Integer newValue);
    public Integer getSchoolCountryID();
    public void setSchoolPhone(String newValue);
    public String getSchoolPhone();
    public void setSchoolFax(String newValue);
    public String getSchoolFax();
    public void setContactName(String newValue);
    public String getContactName();
    public void setContactEmail(String newValue);
    public String getContactEmail();
    public void setComments(String newValue);
    public String getComments();
}    // End of bltProfessionalEducation class definition
