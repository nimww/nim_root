

package com.winstaff;

import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;
import java.util.Enumeration;

public class bltNIM3_Modality extends Object implements InttNIM3_Modality
{

    dbtNIM3_Modality    dbDB;

    public bltNIM3_Modality()
    {
        dbDB = new dbtNIM3_Modality();
        dbDB.setUniqueCreateDate(new java.util.Date());
        makeRequiredHash();
        makeExpiredHash();
        makeEnglishHash();

    }    // End of default constructor

    public bltNIM3_Modality( Integer iNewID )
    {        dbDB = new dbtNIM3_Modality( iNewID );
        makeRequiredHash();
        makeExpiredHash();
        makeEnglishHash();
        iSecurityCheck = new Integer(2);
    }    // End of Constructor knowing an ID


    public bltNIM3_Modality( Integer iNewID, Integer iGroupSecurityID )
    {
        this.GroupSecurityInit(iGroupSecurityID);
        dbDB = new dbtNIM3_Modality( iNewID );
        makeExpiredHash();
        makeEnglishHash();
        makeRequiredHash(iGroupSecurityID);
    }    // End of Constructor knowing an ID

    public bltNIM3_Modality( Integer iGroupSecurityID, boolean isTrue )
    {
        this.GroupSecurityInit(iGroupSecurityID);
        dbDB = new dbtNIM3_Modality(  );
        makeExpiredHash();
        makeEnglishHash();
        makeRequiredHash(iGroupSecurityID);
    }    // End of Security unknown ID

    public void GroupSecurityInit( Integer iGroupSecurityID )
    {
        bltSecurityGroupMaster mySGM = new bltSecurityGroupMaster(iGroupSecurityID);
        this.UserSecurityID = iGroupSecurityID;
        if (mySGM.getRunFieldSecurityID().intValue()==1)
        {
            this.runFieldSecurity = true;
            makeRequiredHash(iGroupSecurityID);
        }
        iSecurityCheck = SecurityCheck.CheckItem("nim1", iGroupSecurityID);
    }  

    public EventReferenceType getEventReference()
    {
        return EventUtils.getEventReference("tNIM3_Modality", this.getUniqueID());
    }  

    public String convertString2LogString(String normalString) 
    {
        String myVal = "";
        for (int i=0;i<normalString.length();i++)
        {
            if ( (normalString.charAt(i)!='\r')&&(normalString.charAt(i)!='\n')&&(normalString.charAt(i)!='\t') )
            {
                myVal+=normalString.charAt(i);
            }
            else
            {
                myVal+="<NLT>";
            }
        }
        return myVal;
    }
    public void runAudit()
    {
        String AuditString = new java.util.Date()+" \t " ;  
        AuditString += " tNIM3_Modality "; 
        AuditString += " ModalityID ="+this.getUniqueID(); 
        AuditString += " PracticeID ="+this.getPracticeID(); 
        for (int i=0;i<this.AuditVector.size();i++)
        {
            AuditString+="\t"+convertString2LogString((String)this.AuditVector.elementAt(i));
        }
        AuditLogger.printLine(AuditString);
    }
    public void commitData() throws SQLException
    {
        runAudit();
        dbDB.setModifyComments(this.getUniqueModifyComments());
        dbDB.setUniqueModifyDate(new java.util.Date());
        dbDB.commitData();
        DataControlUtils.dataChanged("tNIM3_Modality",this.getPracticeID() ,this.AuditVector.size(), "PracticeID");
        this.AuditVector.removeAllElements();
    }    // End of commitData()

    public void commitDataForced() throws SQLException
    {
        dbDB.commitDataForced();
    }    // End of commitData()

    public Integer getUniqueID()
    {
        return dbDB.getUniqueID();
    }    // End of getUniqueID()

    public void setUniqueID( Integer iNewID )
    {
        dbDB.setUniqueID( iNewID );
    }    // End of setUniqueID()


    public void setModalityID(Integer newValue)
    {
        dbDB.setModalityID(newValue);
    }

    public Integer getModalityID()
    {
        return dbDB.getModalityID();
    }

    java.util.Vector AuditVector = new java.util.Vector();

    public void setUniqueCreateDate(Date newValue)
    {
        this.setUniqueCreateDate(newValue,this.UserSecurityID);


    }
    public Date getUniqueCreateDate()
    {
        return this.getUniqueCreateDate(this.UserSecurityID);
    }

    public void setUniqueCreateDate(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='UniqueCreateDate' AND TableName='tNIM3_Modality'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[UniqueCreateDate]=["+newValue+"]");
                   dbDB.setUniqueCreateDate(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[UniqueCreateDate]=["+newValue+"]");
           dbDB.setUniqueCreateDate(newValue);
         }
    }
    public Date getUniqueCreateDate(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='UniqueCreateDate' AND TableName='tNIM3_Modality'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getUniqueCreateDate();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getUniqueCreateDate();
         }
        return myVal;
    }

    public void setUniqueModifyDate(Date newValue)
    {
        this.setUniqueModifyDate(newValue,this.UserSecurityID);


    }
    public Date getUniqueModifyDate()
    {
        return this.getUniqueModifyDate(this.UserSecurityID);
    }

    public void setUniqueModifyDate(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='UniqueModifyDate' AND TableName='tNIM3_Modality'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[UniqueModifyDate]=["+newValue+"]");
                   dbDB.setUniqueModifyDate(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[UniqueModifyDate]=["+newValue+"]");
           dbDB.setUniqueModifyDate(newValue);
         }
    }
    public Date getUniqueModifyDate(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='UniqueModifyDate' AND TableName='tNIM3_Modality'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getUniqueModifyDate();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getUniqueModifyDate();
         }
        return myVal;
    }

    public void setUniqueModifyComments(String newValue)
    {
        this.setUniqueModifyComments(newValue,this.UserSecurityID);


    }
    public String getUniqueModifyComments()
    {
        return this.getUniqueModifyComments(this.UserSecurityID);
    }

    public void setUniqueModifyComments(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='UniqueModifyComments' AND TableName='tNIM3_Modality'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[UniqueModifyComments]=["+newValue+"]");
                   dbDB.setUniqueModifyComments(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[UniqueModifyComments]=["+newValue+"]");
           dbDB.setUniqueModifyComments(newValue);
         }
    }
    public String getUniqueModifyComments(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='UniqueModifyComments' AND TableName='tNIM3_Modality'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getUniqueModifyComments();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getUniqueModifyComments();
         }
        return myVal;
    }

    public void setPracticeID(Integer newValue)
    {
        this.setPracticeID(newValue,this.UserSecurityID);


    }
    public Integer getPracticeID()
    {
        return this.getPracticeID(this.UserSecurityID);
    }

    public void setPracticeID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PracticeID' AND TableName='tNIM3_Modality'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[PracticeID]=["+newValue+"]");
                   dbDB.setPracticeID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[PracticeID]=["+newValue+"]");
           dbDB.setPracticeID(newValue);
         }
    }
    public Integer getPracticeID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PracticeID' AND TableName='tNIM3_Modality'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPracticeID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPracticeID();
         }
        return myVal;
    }

    public void setModalityTypeID(Integer newValue)
    {
        this.setModalityTypeID(newValue,this.UserSecurityID);


    }
    public Integer getModalityTypeID()
    {
        return this.getModalityTypeID(this.UserSecurityID);
    }

    public void setModalityTypeID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ModalityTypeID' AND TableName='tNIM3_Modality'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[ModalityTypeID]=["+newValue+"]");
                   dbDB.setModalityTypeID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[ModalityTypeID]=["+newValue+"]");
           dbDB.setModalityTypeID(newValue);
         }
    }
    public Integer getModalityTypeID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ModalityTypeID' AND TableName='tNIM3_Modality'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getModalityTypeID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getModalityTypeID();
         }
        return myVal;
    }

    public void setModalityModelID(Integer newValue)
    {
        this.setModalityModelID(newValue,this.UserSecurityID);


    }
    public Integer getModalityModelID()
    {
        return this.getModalityModelID(this.UserSecurityID);
    }

    public void setModalityModelID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ModalityModelID' AND TableName='tNIM3_Modality'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[ModalityModelID]=["+newValue+"]");
                   dbDB.setModalityModelID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[ModalityModelID]=["+newValue+"]");
           dbDB.setModalityModelID(newValue);
         }
    }
    public Integer getModalityModelID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ModalityModelID' AND TableName='tNIM3_Modality'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getModalityModelID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getModalityModelID();
         }
        return myVal;
    }

    public void setSoftwareVersion(String newValue)
    {
        this.setSoftwareVersion(newValue,this.UserSecurityID);


    }
    public String getSoftwareVersion()
    {
        return this.getSoftwareVersion(this.UserSecurityID);
    }

    public void setSoftwareVersion(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SoftwareVersion' AND TableName='tNIM3_Modality'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[SoftwareVersion]=["+newValue+"]");
                   dbDB.setSoftwareVersion(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[SoftwareVersion]=["+newValue+"]");
           dbDB.setSoftwareVersion(newValue);
         }
    }
    public String getSoftwareVersion(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SoftwareVersion' AND TableName='tNIM3_Modality'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getSoftwareVersion();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getSoftwareVersion();
         }
        return myVal;
    }

    public void setLastServiceDate(Date newValue)
    {
        this.setLastServiceDate(newValue,this.UserSecurityID);


    }
    public Date getLastServiceDate()
    {
        return this.getLastServiceDate(this.UserSecurityID);
    }

    public void setLastServiceDate(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='LastServiceDate' AND TableName='tNIM3_Modality'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[LastServiceDate]=["+newValue+"]");
                   dbDB.setLastServiceDate(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[LastServiceDate]=["+newValue+"]");
           dbDB.setLastServiceDate(newValue);
         }
    }
    public Date getLastServiceDate(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='LastServiceDate' AND TableName='tNIM3_Modality'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getLastServiceDate();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getLastServiceDate();
         }
        return myVal;
    }

    public void setCoilWrist(Integer newValue)
    {
        this.setCoilWrist(newValue,this.UserSecurityID);


    }
    public Integer getCoilWrist()
    {
        return this.getCoilWrist(this.UserSecurityID);
    }

    public void setCoilWrist(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CoilWrist' AND TableName='tNIM3_Modality'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[CoilWrist]=["+newValue+"]");
                   dbDB.setCoilWrist(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[CoilWrist]=["+newValue+"]");
           dbDB.setCoilWrist(newValue);
         }
    }
    public Integer getCoilWrist(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CoilWrist' AND TableName='tNIM3_Modality'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getCoilWrist();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getCoilWrist();
         }
        return myVal;
    }

    public void setCoilElbow(Integer newValue)
    {
        this.setCoilElbow(newValue,this.UserSecurityID);


    }
    public Integer getCoilElbow()
    {
        return this.getCoilElbow(this.UserSecurityID);
    }

    public void setCoilElbow(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CoilElbow' AND TableName='tNIM3_Modality'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[CoilElbow]=["+newValue+"]");
                   dbDB.setCoilElbow(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[CoilElbow]=["+newValue+"]");
           dbDB.setCoilElbow(newValue);
         }
    }
    public Integer getCoilElbow(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CoilElbow' AND TableName='tNIM3_Modality'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getCoilElbow();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getCoilElbow();
         }
        return myVal;
    }

    public void setCoilKnee(Integer newValue)
    {
        this.setCoilKnee(newValue,this.UserSecurityID);


    }
    public Integer getCoilKnee()
    {
        return this.getCoilKnee(this.UserSecurityID);
    }

    public void setCoilKnee(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CoilKnee' AND TableName='tNIM3_Modality'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[CoilKnee]=["+newValue+"]");
                   dbDB.setCoilKnee(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[CoilKnee]=["+newValue+"]");
           dbDB.setCoilKnee(newValue);
         }
    }
    public Integer getCoilKnee(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CoilKnee' AND TableName='tNIM3_Modality'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getCoilKnee();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getCoilKnee();
         }
        return myVal;
    }

    public void setCoilAnkle(Integer newValue)
    {
        this.setCoilAnkle(newValue,this.UserSecurityID);


    }
    public Integer getCoilAnkle()
    {
        return this.getCoilAnkle(this.UserSecurityID);
    }

    public void setCoilAnkle(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CoilAnkle' AND TableName='tNIM3_Modality'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[CoilAnkle]=["+newValue+"]");
                   dbDB.setCoilAnkle(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[CoilAnkle]=["+newValue+"]");
           dbDB.setCoilAnkle(newValue);
         }
    }
    public Integer getCoilAnkle(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CoilAnkle' AND TableName='tNIM3_Modality'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getCoilAnkle();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getCoilAnkle();
         }
        return myVal;
    }

    public void setCoilShoulder(Integer newValue)
    {
        this.setCoilShoulder(newValue,this.UserSecurityID);


    }
    public Integer getCoilShoulder()
    {
        return this.getCoilShoulder(this.UserSecurityID);
    }

    public void setCoilShoulder(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CoilShoulder' AND TableName='tNIM3_Modality'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[CoilShoulder]=["+newValue+"]");
                   dbDB.setCoilShoulder(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[CoilShoulder]=["+newValue+"]");
           dbDB.setCoilShoulder(newValue);
         }
    }
    public Integer getCoilShoulder(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CoilShoulder' AND TableName='tNIM3_Modality'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getCoilShoulder();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getCoilShoulder();
         }
        return myVal;
    }

    public void setCoilSpine(Integer newValue)
    {
        this.setCoilSpine(newValue,this.UserSecurityID);


    }
    public Integer getCoilSpine()
    {
        return this.getCoilSpine(this.UserSecurityID);
    }

    public void setCoilSpine(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CoilSpine' AND TableName='tNIM3_Modality'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[CoilSpine]=["+newValue+"]");
                   dbDB.setCoilSpine(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[CoilSpine]=["+newValue+"]");
           dbDB.setCoilSpine(newValue);
         }
    }
    public Integer getCoilSpine(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CoilSpine' AND TableName='tNIM3_Modality'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getCoilSpine();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getCoilSpine();
         }
        return myVal;
    }

    public void setIsACR(Integer newValue)
    {
        this.setIsACR(newValue,this.UserSecurityID);


    }
    public Integer getIsACR()
    {
        return this.getIsACR(this.UserSecurityID);
    }

    public void setIsACR(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='IsACR' AND TableName='tNIM3_Modality'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[IsACR]=["+newValue+"]");
                   dbDB.setIsACR(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[IsACR]=["+newValue+"]");
           dbDB.setIsACR(newValue);
         }
    }
    public Integer getIsACR(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='IsACR' AND TableName='tNIM3_Modality'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getIsACR();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getIsACR();
         }
        return myVal;
    }

    public void setACRExpiration(Date newValue)
    {
        this.setACRExpiration(newValue,this.UserSecurityID);


    }
    public Date getACRExpiration()
    {
        return this.getACRExpiration(this.UserSecurityID);
    }

    public void setACRExpiration(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ACRExpiration' AND TableName='tNIM3_Modality'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[ACRExpiration]=["+newValue+"]");
                   dbDB.setACRExpiration(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[ACRExpiration]=["+newValue+"]");
           dbDB.setACRExpiration(newValue);
         }
    }
    public Date getACRExpiration(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ACRExpiration' AND TableName='tNIM3_Modality'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getACRExpiration();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getACRExpiration();
         }
        return myVal;
    }

    public void setIsACRVerified(Integer newValue)
    {
        this.setIsACRVerified(newValue,this.UserSecurityID);


    }
    public Integer getIsACRVerified()
    {
        return this.getIsACRVerified(this.UserSecurityID);
    }

    public void setIsACRVerified(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='IsACRVerified' AND TableName='tNIM3_Modality'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[IsACRVerified]=["+newValue+"]");
                   dbDB.setIsACRVerified(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[IsACRVerified]=["+newValue+"]");
           dbDB.setIsACRVerified(newValue);
         }
    }
    public Integer getIsACRVerified(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='IsACRVerified' AND TableName='tNIM3_Modality'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getIsACRVerified();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getIsACRVerified();
         }
        return myVal;
    }

    public void setACRVerifiedDate(Date newValue)
    {
        this.setACRVerifiedDate(newValue,this.UserSecurityID);


    }
    public Date getACRVerifiedDate()
    {
        return this.getACRVerifiedDate(this.UserSecurityID);
    }

    public void setACRVerifiedDate(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ACRVerifiedDate' AND TableName='tNIM3_Modality'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[ACRVerifiedDate]=["+newValue+"]");
                   dbDB.setACRVerifiedDate(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[ACRVerifiedDate]=["+newValue+"]");
           dbDB.setACRVerifiedDate(newValue);
         }
    }
    public Date getACRVerifiedDate(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ACRVerifiedDate' AND TableName='tNIM3_Modality'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getACRVerifiedDate();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getACRVerifiedDate();
         }
        return myVal;
    }

    public void setComments(String newValue)
    {
        this.setComments(newValue,this.UserSecurityID);


    }
    public String getComments()
    {
        return this.getComments(this.UserSecurityID);
    }

    public void setComments(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Comments' AND TableName='tNIM3_Modality'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[Comments]=["+newValue+"]");
                   dbDB.setComments(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[Comments]=["+newValue+"]");
           dbDB.setComments(newValue);
         }
    }
    public String getComments(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Comments' AND TableName='tNIM3_Modality'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getComments();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getComments();
         }
        return myVal;
    }

    public void setRequiredFieldList(java.util.Hashtable requiredFieldList)
	 {

        this.requiredFieldList = requiredFieldList;
    }

    public java.util.Hashtable getRequiredFieldList()
    {
        return this.requiredFieldList;
    }
    public void setExpiredFieldList(java.util.Hashtable expiredFieldList)
    {
        this.expiredFieldList = expiredFieldList;
    }

    public void setEnglishFieldList(java.util.Hashtable englishFieldList)
    {
        this.englishFieldList = englishFieldList;
    }

    public java.util.Hashtable getExpiredFieldList()
    {
        return this.expiredFieldList;
    }
    public boolean isExpired(java.util.Date myDate, int daysVector)
    {
       return isExpired(myDate,new java.util.Date(),daysVector);
    }


    public boolean isRead(String fieldN, Integer GroupRefID)
    {
        boolean myVal = false;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='"+fieldN+"' AND TableName='tNIM3_Modality'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==1)
               {
                   myVal = true;
               }
            }
        }
        else
        {

           if (iSecurityCheck.intValue()>=1)
           {
                  myVal = true;
           }
        }
        return myVal;
    }


    public boolean isWrite(String fieldN, Integer GroupRefID)
    {
        boolean myVal = false;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='"+fieldN+"' AND TableName='tNIM3_Modality'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {
                   myVal = true;
               }
            }
        }
        else
        {

           if (iSecurityCheck.intValue()>=2)
           {
                  myVal = true;
           }
        }
        return myVal;
    }


    public boolean isExpired(java.util.Date myDate, java.util.Date refDate,int daysVector)
    {
        java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat("yyyy-MM-dd");
        boolean myVal = true;
        try
        {
            if (myDate.after(dbdf.parse("1800-01-10")))
            {
                if (myDate.after(dateChange(refDate,daysVector)))
                {
                    myVal=false;
                }
            }
            else
            {
                myVal = false;
            }
        }
        catch (Exception Datee)
        {
        }
        return myVal;
    }

    public java.util.Date dateChange(java.util.Date myD, int dayChange)
    {
        java.util.Calendar calendar = java.util.Calendar.getInstance();
        calendar.setTime(myD);
        calendar.add(java.util.Calendar.DATE, dayChange);
        java.util.Date resultAsDate = new java.util.Date(calendar.getTime().getTime());
        return resultAsDate;
    }

    public boolean isRequired(String fieldN)
    {
        return isRequired(fieldN, new Integer("1"));
    }

    public boolean isRequired(String fieldN, Integer GroupRefID)
    {
        boolean myVal = false;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='"+fieldN+"' AND TableName='tNIM3_Modality'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getRequiredID().intValue()==1)
               {
                   myVal = true;
               }
            }
        }
        else
        {
            if (this.getRequiredFieldList().containsKey(fieldN))
            {
                myVal = true;
            }
        }
        return myVal;
    }
    public boolean isExpiredCheck(String fieldN)
    {
        return isExpiredCheck(fieldN, new Integer ("1"));
    }

    public boolean isExpiredCheck(String fieldN, Integer GroupRefID)
    {
        boolean myVal = false;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='"+fieldN+"' AND TableName='tNIM3_Modality'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getTrackedID().intValue()==1)
               {
                   myVal = true;
               }
            }
        }
        else
        {
            if (this.getExpiredFieldList().containsKey(fieldN))
           {
                myVal = true;
            }
        }
        return myVal;
    }

    public void makeRequiredHash()
    {
       makeRequiredHash(new Integer(1));
    }

    public void makeRequiredHash(Integer GroupRefID)
    {

        java.util.Hashtable newHash = new java.util.Hashtable();
        if (runFieldSecurity)
        {
            java.util.Enumeration myKeys = englishFieldList.keys(); 
            while (myKeys.hasMoreElements()) 
            {
                 String myElement = (String) myKeys.nextElement();
                 bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='"+myElement+"' AND TableName='tNIM3_Modality'" ,"");
                 bltFieldSecurity        working_bltFieldSecurity;
                 ListElement         leCurrentElement;
                 Enumeration eList = myFSL.elements();
                 if (eList.hasMoreElements())
                 {
                     leCurrentElement    = (ListElement) eList.nextElement();
                     working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
                     if (working_bltFieldSecurity.getRequiredID().intValue()==1)
                     {
                         newHash.put(myElement,new Boolean(true));
                     }
                 }
            }
        }
        else
        {
        }
        this.setRequiredFieldList(newHash);
    }

    public void makeEnglishHash()
    {

        java.util.Hashtable newHash = new java.util.Hashtable();        newHash.put("UniqueCreateDate","UniqueCreateDate");
        newHash.put("UniqueModifyDate","UniqueModifyDate");
        newHash.put("UniqueModifyComments","Item Modification Comments");
        newHash.put("PracticeID","PracticeID");
        newHash.put("ModalityTypeID","ModalityTypeID");
        newHash.put("ModalityModelID","ModalityModelID");
        newHash.put("SoftwareVersion","SoftwareVersion");
        newHash.put("LastServiceDate","LastServiceDate");
        newHash.put("CoilWrist","CoilWrist");
        newHash.put("CoilElbow","CoilElbow");
        newHash.put("CoilKnee","CoilKnee");
        newHash.put("CoilAnkle","CoilAnkle");
        newHash.put("CoilShoulder","CoilShoulder");
        newHash.put("CoilSpine","CoilSpine");
        newHash.put("IsACR","IsACR");
        newHash.put("ACRExpiration","ACRExpiration");
        newHash.put("IsACRVerified","IsACRVerified");
        newHash.put("ACRVerifiedDate","ACRVerifiedDate");
        newHash.put("Comments","Comments");

        this.setEnglishFieldList(newHash);
    }
    public void makeExpiredHash()
    {

        java.util.Hashtable newHash = new java.util.Hashtable();
        this.setExpiredFieldList(newHash);
    }

    public java.util.Hashtable englishFieldList = new java.util.Hashtable();
    public java.util.Hashtable requiredFieldList = new java.util.Hashtable();
    public java.util.Hashtable expiredFieldList = new java.util.Hashtable();

    public boolean isComplete()
    {
	     boolean myVal = true;
	     java.util.Enumeration myKeys = this.getRequiredFieldList().keys(); 
	     while (myKeys.hasMoreElements()) 
	     {
	         String myElement = (String) myKeys.nextElement();
	         if (!isComplete(myElement))
	         {
	             myVal = false;
	         }
	     }
	     return myVal;
    }

    public String getEnglish(String fieldN)
    {
        String myVal = null;
        if (this.englishFieldList.containsKey(fieldN))
        {
            myVal = (String)this.englishFieldList.get(fieldN);
        }
        return myVal;
    }
    public java.util.Vector isComplete(int daysVector)
    {
	     boolean myVal = false;
	     java.util.Enumeration myKeys = this.getExpiredFieldList().keys(); 
	     java.util.Vector myExpiredList = new java.util.Vector(); 
	     while (myKeys.hasMoreElements()) 
	     {
	         String myElement = (String) myKeys.nextElement();
	         if (isExpired(myElement,daysVector))
	         {
	             myVal = true;
	             myExpiredList.addElement(myElement);
	         }
	     }
	     return myExpiredList;
    }

    public void setField(String fieldN, Object fieldV)
    {
     boolean myVal = false;
     try
     {
        java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat("yyyy-MM-dd");

        if (fieldN.equals("ModalityID"))
        {
             this.setModalityID((Integer)fieldV);
        }

        else if (fieldN.equals("UniqueCreateDate"))
        {
            this.setUniqueCreateDate((java.util.Date)fieldV);
        }

        else if (fieldN.equals("UniqueModifyDate"))
        {
            this.setUniqueModifyDate((java.util.Date)fieldV);
        }

        else if (fieldN.equals("UniqueModifyComments"))
        {
            this.setUniqueModifyComments((String)fieldV);
        }

        else if (fieldN.equals("PracticeID"))
        {
             this.setPracticeID((Integer)fieldV);
        }

        else if (fieldN.equals("ModalityTypeID"))
        {
             this.setModalityTypeID((Integer)fieldV);
        }

        else if (fieldN.equals("ModalityModelID"))
        {
             this.setModalityModelID((Integer)fieldV);
        }

        else if (fieldN.equals("SoftwareVersion"))
        {
            this.setSoftwareVersion((String)fieldV);
        }

        else if (fieldN.equals("LastServiceDate"))
        {
            this.setLastServiceDate((java.util.Date)fieldV);
        }

        else if (fieldN.equals("CoilWrist"))
        {
             this.setCoilWrist((Integer)fieldV);
        }

        else if (fieldN.equals("CoilElbow"))
        {
             this.setCoilElbow((Integer)fieldV);
        }

        else if (fieldN.equals("CoilKnee"))
        {
             this.setCoilKnee((Integer)fieldV);
        }

        else if (fieldN.equals("CoilAnkle"))
        {
             this.setCoilAnkle((Integer)fieldV);
        }

        else if (fieldN.equals("CoilShoulder"))
        {
             this.setCoilShoulder((Integer)fieldV);
        }

        else if (fieldN.equals("CoilSpine"))
        {
             this.setCoilSpine((Integer)fieldV);
        }

        else if (fieldN.equals("IsACR"))
        {
             this.setIsACR((Integer)fieldV);
        }

        else if (fieldN.equals("ACRExpiration"))
        {
            this.setACRExpiration((java.util.Date)fieldV);
        }

        else if (fieldN.equals("IsACRVerified"))
        {
             this.setIsACRVerified((Integer)fieldV);
        }

        else if (fieldN.equals("ACRVerifiedDate"))
        {
            this.setACRVerifiedDate((java.util.Date)fieldV);
        }

        else if (fieldN.equals("Comments"))
        {
            this.setComments((String)fieldV);
        }

	    }
	    catch(Exception e2)
	    {}
	    //return myVal;
	}

    public String getFieldType(String fieldN)
    {
     String  myVal = null;
     try
     {
        java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat("yyyy-MM-dd");

        if (fieldN.equals("ModalityID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("UniqueCreateDate"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("UniqueModifyDate"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("UniqueModifyComments"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("PracticeID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("ModalityTypeID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("ModalityModelID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("SoftwareVersion"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("LastServiceDate"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("CoilWrist"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("CoilElbow"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("CoilKnee"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("CoilAnkle"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("CoilShoulder"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("CoilSpine"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("IsACR"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("ACRExpiration"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("IsACRVerified"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("ACRVerifiedDate"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("Comments"))
        {
            myVal = "String";
        }

	    }
	    catch(Exception e2)
	    {}
	    return myVal;
	}

    public boolean isComplete(String fieldN)
    {
	     boolean myVal = false;
	     try
	     {
        java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat("yyyy-MM-dd");

	     if (fieldN.equals("ModalityID"))

	     {
                    if (this.getModalityID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("UniqueCreateDate"))

	     {
                    if (this.getUniqueCreateDate().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("UniqueModifyDate"))

	     {
                    if (this.getUniqueModifyDate().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("UniqueModifyComments"))

	     {
                    if (this.getUniqueModifyComments().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("PracticeID"))

	     {
                    if (this.getPracticeID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("ModalityTypeID"))

	     {
                    if (this.getModalityTypeID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("ModalityModelID"))

	     {
                    if (this.getModalityModelID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("SoftwareVersion"))

	     {
                    if (this.getSoftwareVersion().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("LastServiceDate"))

	     {
                    if (this.getLastServiceDate().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("CoilWrist"))

	     {
                    if (this.getCoilWrist().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("CoilElbow"))

	     {
                    if (this.getCoilElbow().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("CoilKnee"))

	     {
                    if (this.getCoilKnee().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("CoilAnkle"))

	     {
                    if (this.getCoilAnkle().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("CoilShoulder"))

	     {
                    if (this.getCoilShoulder().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("CoilSpine"))

	     {
                    if (this.getCoilSpine().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("IsACR"))

	     {
                    if (this.getIsACR().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("ACRExpiration"))

	     {
                    if (this.getACRExpiration().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("IsACRVerified"))

	     {
                    if (this.getIsACRVerified().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("ACRVerifiedDate"))

	     {
                    if (this.getACRVerifiedDate().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("Comments"))

	     {
                    if (this.getComments().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	    }
	    catch(Exception e2)
	    {}
	    return myVal;
	}

    public boolean isExpired(String fieldN,int daysVector)
    {
        return isExpired(fieldN,new java.util.Date(),daysVector);
    }

    public boolean isExpired(String fieldN,java.util.Date refDate,int daysVector)
    {
	     boolean myVal = false;
	     try
	     {
        java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat("yyyy-MM-dd");

        if (fieldN.equals("UniqueCreateDate"))

	     {
                    if (this.isExpired(this.getUniqueCreateDate(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

        if (fieldN.equals("UniqueModifyDate"))

	     {
                    if (this.isExpired(this.getUniqueModifyDate(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

        if (fieldN.equals("LastServiceDate"))

	     {
                    if (this.isExpired(this.getLastServiceDate(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

        if (fieldN.equals("ACRExpiration"))

	     {
                    if (this.isExpired(this.getACRExpiration(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

        if (fieldN.equals("ACRVerifiedDate"))

	     {
                    if (this.isExpired(this.getACRVerifiedDate(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

	    }
	    catch(Exception e2)
	    {}
	    return myVal;
	}

    public java.util.Date getDateField(String fieldN)
    {
	     java.util.Date myVal = null;
	     try
	     {
        java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat("yyyy-MM-dd");

        if (fieldN.equals("UniqueCreateDate"))

	     {
             myVal = this.getUniqueCreateDate();
        }

        if (fieldN.equals("UniqueModifyDate"))

	     {
             myVal = this.getUniqueModifyDate();
        }

        if (fieldN.equals("LastServiceDate"))

	     {
             myVal = this.getLastServiceDate();
        }

        if (fieldN.equals("ACRExpiration"))

	     {
             myVal = this.getACRExpiration();
        }

        if (fieldN.equals("ACRVerifiedDate"))

	     {
             myVal = this.getACRVerifiedDate();
        }

	    }
	    catch(Exception e2)
	    {}
	    return myVal;
	}
	public boolean runFieldSecurity = false;
	public Integer UserSecurityID = null;
   public Integer iSecurityCheck = null;
}    // End of bltNIM3_Modality class definition
