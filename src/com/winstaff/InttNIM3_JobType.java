

package com.winstaff;

import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public interface InttNIM3_JobType extends dbTableInterface
{

    public void setJobTypeID(Integer newValue);
    public Integer getJobTypeID();
    public void setUniqueCreateDate(Date newValue);
    public Date getUniqueCreateDate();
    public void setUniqueModifyDate(Date newValue);
    public Date getUniqueModifyDate();
    public void setUniqueModifyComments(String newValue);
    public String getUniqueModifyComments();
    public void setEmployerID(Integer newValue);
    public Integer getEmployerID();
    public void setModifiedWorkID(Integer newValue);
    public Integer getModifiedWorkID();
    public void setJobTitle(String newValue);
    public String getJobTitle();
    public void setJobDesc(String newValue);
    public String getJobDesc();
    public void setJobLinkPDF(String newValue);
    public String getJobLinkPDF();
    public void setJobLinkVideo(String newValue);
    public String getJobLinkVideo();
    public void setComments(String newValue);
    public String getComments();
    public void setAuditNotes(String newValue);
    public String getAuditNotes();
}    // End of bltNIM3_JobType class definition
