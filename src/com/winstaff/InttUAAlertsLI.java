

package com.winstaff;

import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public interface InttUAAlertsLI extends dbTableInterface
{

    public void setUAAlertsID(Integer newValue);
    public Integer getUAAlertsID();
    public void setUAAlertsLong(String newValue);
    public String getUAAlertsLong();
}    // End of bltUAAlertsLI class definition
