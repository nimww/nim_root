package com.winstaff;

/**
 * Created with IntelliJ IDEA.
 * User: Scott
 * Date: 7/1/13
 * Time: 2:54 PM
 * To change this template use File | Settings | File Templates.
 */
public class NIM3_ReferralSourceObject {

    private Integer referralSource;
    private Integer payerID;
    private Integer nidPromo;


    public NIM3_ReferralSourceObject(Integer payerID, Integer referralSource) {
        this.payerID = payerID;
        this.referralSource = referralSource;
    }

    public NIM3_ReferralSourceObject(Integer payerID, Integer referralSource, Integer nidPromo) {
        this.payerID = payerID;
        this.referralSource = referralSource;
        this.nidPromo = nidPromo;
    }

    public Integer getReferralSource() {
        if (referralSource == null || referralSource.intValue()==0 ){
            return NID_SubmitCaseObject.NID_REF_SOURCE;
        }
        return referralSource;
    }

    public void setReferralSource(Integer referralSource) {
        this.referralSource = referralSource;
    }

    public Integer getPayerID() {
        if (payerID == null || payerID.intValue()==0 ){
            return NID_SubmitCaseObject.NID_PAYER;
        }
        return payerID;
    }

    public void setPayerID(Integer payerID) {
        this.payerID = payerID;
    }


    public Integer getNidPromo() {
        return nidPromo;
    }

    public void setNidPromo(Integer nidPromo) {
        this.nidPromo = nidPromo;
    }
}
