package com.winstaff;


import com.winstaff.dbtCaseStatusLI;
import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public interface InttCaseStatusLI extends dbTableInterface
{

    public void setCaseStatusID(Integer newValue);
    public Integer getCaseStatusID();
    public void setCaseStatusShort(String newValue);
    public String getCaseStatusShort();
    public void setCaseStatusLong(String newValue);
    public String getCaseStatusLong();
}    // End of bltCaseStatusLI class definition
