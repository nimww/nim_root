

package com.winstaff;

import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public class dbtScheduledAppointmentStatusLI extends Object implements InttScheduledAppointmentStatusLI
{

        db_NewBase    dbnbDB;

    public dbtScheduledAppointmentStatusLI()
    {
        dbnbDB = new db_NewBase( "tScheduledAppointmentStatusLI", "ScheduledAppointmentStatusID" );

    }    // End of default constructor

    public dbtScheduledAppointmentStatusLI( Integer iNewID )
    {
        dbnbDB = new db_NewBase( "tScheduledAppointmentStatusLI", "ScheduledAppointmentStatusID" );
        setUniqueID( iNewID );
    }    // End of Constructor knowing an ID

    public void commitData() throws SQLException
    {
            //no longer trapping/ignoring errors 2011-03-17
            dbnbDB.commitData();
    }    // End of commitData()

    public void commitDataForced() throws SQLException
    {
        dbnbDB.commitDataForced();
    }    // End of commitData()

    public Integer getUniqueID()
    {
        return dbnbDB.getUniqueID();
    }    // End of getUniqueID()

    public void setUniqueID( Integer iNewID )
    {
        dbnbDB.setUniqueID( iNewID );
    }    // End of setUniqueID()

    public void setModifyComments( String sNew )
    {
        dbnbDB.setModifyComments( sNew );
    }    // End of setUniqueID()


    public void setScheduledAppointmentStatusID(Integer newValue)
    {
                dbnbDB.setFieldData( "ScheduledAppointmentStatusID", newValue.toString() );
    }

    public Integer getScheduledAppointmentStatusID()
    {
        String           sValue = dbnbDB.getFieldData( "ScheduledAppointmentStatusID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setField(String fieldN, Object fieldV)
    {DebugLogger.println("setField Method not implemented yet in class!");}

    public String getFieldType(String fieldN)
    {return null;}


    public void setStatusShort(String newValue)
    {
                dbnbDB.setFieldData( "StatusShort", newValue.toString() );
    }

    public String getStatusShort()
    {
        String           sValue = dbnbDB.getFieldData( "StatusShort" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setStatusLong(String newValue)
    {
                dbnbDB.setFieldData( "StatusLong", newValue.toString() );
    }

    public String getStatusLong()
    {
        String           sValue = dbnbDB.getFieldData( "StatusLong" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

}    // End of bltScheduledAppointmentStatusLI class definition
