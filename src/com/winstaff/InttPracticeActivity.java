

package com.winstaff;

import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public interface InttPracticeActivity extends dbTableInterface
{

    public void setPracticeActivityID(Integer newValue);
    public Integer getPracticeActivityID();
    public void setUniqueCreateDate(Date newValue);
    public Date getUniqueCreateDate();
    public void setUniqueModifyDate(Date newValue);
    public Date getUniqueModifyDate();
    public void setUniqueModifyComments(String newValue);
    public String getUniqueModifyComments();
    public void setPracticeID(Integer newValue);
    public Integer getPracticeID();
    public void setAdminID(Integer newValue);
    public Integer getAdminID();
    public void setName(String newValue);
    public String getName();
    public void setActivityType(String newValue);
    public String getActivityType();
    public void setisCompleted(Integer newValue);
    public Integer getisCompleted();
    public void setStartDate(Date newValue);
    public Date getStartDate();
    public void setEndDate(Date newValue);
    public Date getEndDate();
    public void setRemindDate(Date newValue);
    public Date getRemindDate();
    public void setDescription(String newValue);
    public String getDescription();
    public void setTaskFileReference(String newValue);
    public String getTaskFileReference();
    public void setTaskLogicReference(String newValue);
    public String getTaskLogicReference();
    public void setSummary(String newValue);
    public String getSummary();
    public void setComments(String newValue);
    public String getComments();
    public void setReferenceID(Integer newValue);
    public Integer getReferenceID();
    public void setDocuLinkID(Integer newValue);
    public Integer getDocuLinkID();
    public void setAssignedToID(Integer newValue);
    public Integer getAssignedToID();
}    // End of bltPracticeActivity class definition
