package com.winstaff;
import java.awt.*;
import java.util.Date;


public class DocumentManagerUtils extends java.lang.Object
{

	public DocumentManagerUtils()
	{
	}
	
	java.text.SimpleDateFormat mySDF = new java.text.SimpleDateFormat("MM/dd/yyyy");


	public static int ArchiveDocument(String theType, Integer DocID, int ChangeCode)
	{
	    //ChangeCode
            //1 = item deleted
            //2 = item ?
	    int myType = 0;
            bltDocumentManagement myDM = new bltDocumentManagement(DocID);
            if (myDM.isComplete("DocumentFileName"))
            {
	            myDM.setArchived(new Integer(1));
	            myDM.setComments("Original Data Record has been removed, and this document has been archived");
	    }
            else
            {
	            myDM.setPhysicianID(new Integer(0));
	            myDM.setComments("Original Data Record has been removed, and this document has been archived");
	    }
            try
            {
	            myDM.commitDataForced();
	    }
	    catch(Exception e43)
            {
                 DebugLogger.println("Error in DocumentManagerUtils:" + e43);
            }
            return myType;
        }







	public void setDocumentType(String documentType, Integer UniqueID)
	{
	    int myType = -1;
        if (documentType.equalsIgnoreCase("tBoardCertification"))
	    {
	        myType = this.TYPE_tBoardCertification;
	    }
        else if (documentType.equalsIgnoreCase("tAdditionalInformation"))
	    {
	        myType = this.TYPE_tAdditionalInformation;
	    }
        else if (documentType.equalsIgnoreCase("tContinuingEducation"))
	    {
	        myType = this.TYPE_tContinuingEducation;
	    }
        else if (documentType.equalsIgnoreCase("tExperience"))
	    {
	        myType = this.TYPE_tExperience;
	    }
        else if (documentType.equalsIgnoreCase("tFacilityAffiliation"))
	    {
	        myType = this.TYPE_tFacilityAffiliation;
	    }
        else if (documentType.equalsIgnoreCase("tLicenseRegistration"))
	    {
	        myType = this.TYPE_tLicenseRegistration;
	    }
        else if (documentType.equalsIgnoreCase("tMalpractice"))
	    {
	        myType = this.TYPE_tMalpractice;
	    }
        else if (documentType.equalsIgnoreCase("tManagedCarePlan"))
	    {
	        myType = this.TYPE_tManagedCarePlan;
	    }
        else if (documentType.equalsIgnoreCase("tOtherCertification"))
	    {
	        myType = this.TYPE_tOtherCertification;
	    }
        else if (documentType.equalsIgnoreCase("tPeerReference"))
	    {
	        myType = this.TYPE_tPeerReference;
	    }
        else if (documentType.equalsIgnoreCase("tProfessionalEducation"))
	    {
	        myType = this.TYPE_tProfessionalEducation;
	    }
        else if (documentType.equalsIgnoreCase("tProfessionalLiability"))
	    {
	        myType = this.TYPE_tProfessionalLiability;
	    }
        else if (documentType.equalsIgnoreCase("tProfessionalMisconduct"))
	    {
	        myType = this.TYPE_tProfessionalMisconduct;
	    }
        else if (documentType.equalsIgnoreCase("tProfessionalSociety"))
	    {
	        myType = this.TYPE_tProfessionalSociety;
	    }
        else if (documentType.equalsIgnoreCase("tWorkHistory"))
	    {
	        myType = this.TYPE_tWorkHistory;
	    }
        else if (documentType.equalsIgnoreCase("tCoveringPhysicians"))
	    {
	        myType = this.TYPE_tCoveringPhysicians;
	    }

	    this.setDocumentType(myType, UniqueID);

	
	
	}
	

	
	public void setDocumentType(int documentType, Integer UniqueID)
	{
		this.documentType = documentType;
		this.setClassUniqueID(UniqueID);
/*		if (documentType == this.TYPE_tAdditionalInformation)
		{
		    bltAdditionalInformation myClass = new bltAdditionalInformation(UniqueID);
		    this.setDocumentName(myClass.getSubject());
		    this.setDateOfExpiration(PLCUtils.getSubDate("na"));
	        this.setDocumentTypeID(new Integer(14));
		}
*/
		if (documentType == this.TYPE_tBoardCertification)
		{
		    bltBoardCertification myClass = new bltBoardCertification(UniqueID);
		    this.setDocumentName(myClass.getDocumentNumber() + "[" + myClass.getBoardName() +"]"+ "[Exp: " + mySDF.format(myClass.getExpirationDate()) +"]");
		    this.setDateOfExpiration(myClass.getExpirationDate());
		    if (myClass.getIsPrimary().intValue()==0)
		    {
		        this.setDocumentTypeID(new Integer(7));
		    }
		    else 
		    {
		        this.setDocumentTypeID(new Integer(1));
		    }
		}
/*		else if (documentType == this.TYPE_tContinuingEducation)
		{
		    bltContinuingEducation myClass = new bltContinuingEducation(UniqueID);
		    this.setDocumentName(myClass.getCourseName());
		    this.setDateOfExpiration(PLCUtils.getSubDate("na"));
	        this.setDocumentTypeID(new Integer(14));
	    }
*/		else if (documentType == this.TYPE_tExperience)
		{
		    bltExperience myClass = new bltExperience(UniqueID);
		    this.setDocumentName(myClass.getName() + " [Focus:" + myClass.getSpecialty() + "] [To: " + mySDF.format(myClass.getDateTo()) + "]" );
		    this.setDateOfExpiration(PLCUtils.getSubDate("na"));
	        this.setDocumentTypeID(new Integer(14));
	    }
		else if (documentType == this.TYPE_tFacilityAffiliation)
		{
		    bltFacilityAffiliation myClass = new bltFacilityAffiliation(UniqueID);
		    this.setDocumentName(myClass.getFacilityName() + " [Dept:" + myClass.getFacilityDepartment() + "] [To: " + mySDF.format(myClass.getEndDate()) + "]" );
		    this.setDateOfExpiration(PLCUtils.getSubDate("na"));
	        this.setDocumentTypeID(new Integer(14));
	    }
		else if (documentType == this.TYPE_tLicenseRegistration)
		{
		    bltLicenseRegistration myClass = new bltLicenseRegistration(UniqueID);
		    this.setDocumentName(myClass.getLicenseDocumentNumber() + "[Exp: " + mySDF.format(myClass.getExpirationDate()) +"]");
		    this.setDateOfExpiration(myClass.getExpirationDate());
		    if (myClass.getLicenseType().intValue()==1)
		    {
		        this.setDocumentTypeID(new Integer(7));
		    }
		    else if (myClass.getLicenseType().intValue()==2)
		    {
		        this.setDocumentTypeID(new Integer(2));
		    }
		    else if (myClass.getLicenseType().intValue()==3)
		    {
		        this.setDocumentTypeID(new Integer(4));
		    }
		    else if (myClass.getLicenseType().intValue()==4)
		    {
		        this.setDocumentTypeID(new Integer(12));
		    }
		    else if (myClass.getLicenseType().intValue()==5)
		    {
		        this.setDocumentTypeID(new Integer(11));
		    }
		    else if (myClass.getLicenseType().intValue()==6)
		    {
		        this.setDocumentTypeID(new Integer(13));
		    }
		}
/*		else if (documentType == this.TYPE_tMalpractice)
		{
		    bltMalpractice myClass = new bltMalpractice(UniqueID);
		    this.setDocumentName("Malpractice Suit: "+myClass.getSuitCity() + " [Date:" + mySDF.format( myClass.getSuitDate()) + "]" );
		    this.setDateOfExpiration(PLCUtils.getSubDate("na"));
	        this.setDocumentTypeID(new Integer(14));
	    }
		else if (documentType == this.TYPE_tManagedCarePlan)
		{
		    bltManagedCarePlan myClass = new bltManagedCarePlan(UniqueID);
		    this.setDocumentName("Managed Care Plan: "+myClass.getName() + " [Number:" + myClass.getPlanNumber() + "]" );
		    this.setDateOfExpiration(PLCUtils.getSubDate("na"));
	        this.setDocumentTypeID(new Integer(14));
	    }
*/		else if (documentType == this.TYPE_tOtherCertification)
		{
		    bltOtherCertification myClass = new bltOtherCertification(UniqueID);
		    this.setDocumentName(myClass.getCertificationNumber() + "[" + myClass.getCertificationType() +"]"+ "[Exp: " + mySDF.format(myClass.getExpirationDate()) +"]");
		    this.setDateOfExpiration(myClass.getExpirationDate());
	        this.setDocumentTypeID(new Integer(7));
		}
		else if (documentType == this.TYPE_tPeerReference)
		{
		    bltPeerReference myClass = new bltPeerReference(UniqueID);
		    this.setDocumentName("Peer Reference: "+myClass.getFirstName()+ " " + myClass.getLastName() );
		    this.setDateOfExpiration(PLCUtils.getSubDate("na"));
	        this.setDocumentTypeID(new Integer(14));
	    }
		else if (documentType == this.TYPE_tProfessionalEducation)
		{
		    bltProfessionalEducation myClass = new bltProfessionalEducation(UniqueID);
		    this.setDocumentName("Education: "+new bltDegreeTypeLI(myClass.getDegreeID()).getDegreeLong()+ " [" + mySDF.format(myClass.getDateOfGraduation()) + "]" );
		    this.setDateOfExpiration(PLCUtils.getSubDate("na"));
	        this.setDocumentTypeID(new Integer(14));
	    }
		else if (documentType == this.TYPE_tProfessionalLiability)
		{
		    bltProfessionalLiability myClass = new bltProfessionalLiability(UniqueID);
		    this.setDocumentName("Professional Liability: "+myClass.getPolicyNumber()+"" );
            this.setDateOfExpiration(myClass.getInsuredToDate());
		    try
		    {
		        if (myClass.getTerminationDate().after(mySDF.parse("1/1/1805")))
		        {
		            this.setDateOfExpiration(PLCUtils.getSubDate("na"));
		        }
		        else
		        {
		            this.setDateOfExpiration(myClass.getInsuredToDate());
		        }
		    }
		    catch(Exception s55)
		    {
		    }
	        this.setDocumentTypeID(new Integer(8));
	    }
/*		else if (documentType == this.TYPE_tProfessionalMisconduct)
		{
		    bltProfessionalMisconduct myClass = new bltProfessionalMisconduct(UniqueID);
		    this.setDocumentName("Misconduct: "+myClass.getCity() + " [Date:" + mySDF.format( myClass.getSuitDate()) + "]" );
		    this.setDateOfExpiration(PLCUtils.getSubDate("na"));
	        this.setDocumentTypeID(new Integer(14));
	    }
		else if (documentType == this.TYPE_tProfessionalSociety)
		{
		    bltProfessionalSociety myClass = new bltProfessionalSociety(UniqueID);
		    this.setDocumentName("Association: "+myClass.getName()+ " [Status:" + myClass.getMembershipStatus() + "]" );
		    this.setDateOfExpiration(PLCUtils.getSubDate("na"));
	        this.setDocumentTypeID(new Integer(14));
	    }
*/		else if (documentType == this.TYPE_tWorkHistory)
		{
		    bltWorkHistory myClass = new bltWorkHistory(UniqueID);
		    this.setDocumentName("Work: "+myClass.getName() + " [To:" + mySDF.format(myClass.getToDate()) + "]" );
		    this.setDateOfExpiration(PLCUtils.getSubDate("na"));
	        this.setDocumentTypeID(new Integer(14));
	    }
/*		else if (documentType == this.TYPE_tCoveringPhysicians)
		{
		    bltCoveringPhysicians myClass = new bltCoveringPhysicians(UniqueID);
		    this.setDocumentName("Covering Physician: "+myClass.getFirstName() + " " + myClass.getLastName());
		    this.setDateOfExpiration(PLCUtils.getSubDate("na"));
	        this.setDocumentTypeID(new Integer(14));
	    }
*/	}

	public int getDocumentType()
	{
		return this.documentType;
	}

	public void setDocumentTypeID(Integer documentTypeID)
	{
		this.documentTypeID = documentTypeID;
	}

	public Integer getDocumentTypeID()
	{
		return this.documentTypeID;
	}

	public void setDocumentName(String documentName)
	{
			this.documentName = documentName;
	}

	public String getDocumentName()
	{
		return this.documentName;
	}

	public void setDateOfExpiration(java.util.Date dateOfExpiration)
	{
		this.dateOfExpiration = dateOfExpiration;
	}

	public java.util.Date getDateOfExpiration()
	{
		return this.dateOfExpiration;
	}

	public void setClassUniqueID(Integer classUniqueID)
	{
		this.classUniqueID = classUniqueID;
	}

	public Integer getClassUniqueID()
	{
		return this.classUniqueID;
	}

	protected Integer classUniqueID;
	protected Integer documentTypeID = new Integer(0);
	protected java.util.Date dateOfExpiration;
    protected String documentName = "[Not Set]";
	protected int documentType;
    public static int  TYPE_tAdditionalInformation = 0;
    public static int  TYPE_tBoardCertification = 1;
    public static int  TYPE_tContinuingEducation = 2;
    public static int  TYPE_tExperience = 3;
    public static int  TYPE_tFacilityAffiliation = 4;
    public static int  TYPE_tLicenseRegistration = 5;
    public static int  TYPE_tMalpractice = 6;
    public static int  TYPE_tManagedCarePlan = 7;
    public static int  TYPE_tOtherCertification = 8;
    public static int  TYPE_tPeerReference = 9;
    public static int  TYPE_tProfessionalEducation = 10;
    public static int  TYPE_tProfessionalLiability = 11;
    public static int  TYPE_tProfessionalMisconduct = 12;
    public static int  TYPE_tProfessionalSociety = 13;
    public static int  TYPE_tWorkHistory = 14;
    public static int  TYPE_tCoveringPhysicians = 15;

}
