package com.winstaff;

import java.io.DataOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLEncoder;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.winstaff.*;

import com.google.gson.Gson;

public class AmerisysPerformance {

	public static void main(String[] args) throws IllegalArgumentException, SecurityException, IOException, IllegalAccessException, InvocationTargetException, NoSuchMethodException, ClassNotFoundException, InstantiationException, SQLException {
		
		DataOutputStream out = new DataOutputStream( new FileOutputStream( "/var/lib/tomcat7/webapps/ROOT/WEB-INF/classes/Amerisys_Performance_"+(new SimpleDateFormat("yyyyMMdd").format(new Date())+".txt") ) );
		
		boolean isHeader = true;
		
		for(apObject  apO : getQuery()){
			boolean isFirst = true;
			if (isHeader){
				for(Field f : apO.getClass().getFields()){
					if(isFirst){
						out.writeBytes(f.getName());
						isFirst = false;
					} else{
						out.writeBytes("|"+f.getName());
					}
				}
				out.writeBytes("\n");
				isHeader = false;
			}
			
			isFirst = true;
			Class cls = Class.forName("com.winstaff.apObject");
			
			for(Field f : apO.getClass().getFields()){
				if(isFirst){
					out.writeBytes((String) cls.getDeclaredMethod("get"+f.getName()).invoke(apO,null));
					isFirst = false;
				} else{
					out.writeBytes("|"+cls.getDeclaredMethod("get"+f.getName()).invoke(apO,null));
				}
			}
			out.writeBytes("\n");
		}
		out.close();
	}
	
	private static Distancematrix getDistanceMatrixJsonResponse(String origins, String destinations){
		Gson gson = new Gson(); 
		
		String URL = "http://maps.googleapis.com/maps/api/distancematrix/json?origins="+URLEncoder.encode(origins)+"&destinations="+URLEncoder.encode(destinations)+"&sensor=false&units=imperial";

		byte[] array = null;
		try{
		InputStream stream = new URL(URL).openStream();
		array = new byte[stream.available()];
		stream.read(array);
		} catch(Exception e){}
		
		String json = new String(array);
		
		Distancematrix distancematrix = gson.fromJson(json, Distancematrix.class); 
		
		return distancematrix;
	}
	
	public static List<apObject> getQuery() throws ClassNotFoundException, InstantiationException, IllegalAccessException, IllegalArgumentException, SecurityException, SQLException, InvocationTargetException, NoSuchMethodException {
		String query = "select * from \"amerisys_performance\"";
		searchDB2 conn = new searchDB2();
		ResultSet rs = conn.executeStatement(query);
		
		Class[] string = new Class[1];
		string[0] = String.class;
		List<apObject> apObjectList = new ArrayList<apObject>();
		
		while(rs.next()){
			Class cls = Class.forName("com.winstaff.apObject");
			Object obj = cls.newInstance();
			
			for(int x = 1;x<rs.getMetaData().getColumnCount()+1;x++){
				cls.getDeclaredMethod("set"+rs.getMetaData().getColumnName(x),string).invoke(obj, rs.getString(rs.getMetaData().getColumnName(x)));
			}
			apObjectList.add((apObject) cls.getDeclaredMethod("getThis").invoke(obj,null));
		}
		
		for(int x = 0; x<apObjectList.size();x++){
			
			//Set Mileage_from_Home_Work_Address
			try{
				apObjectList.get(x).setMileage_from_Home_Work_Address(getDistance(getDistanceMatrixJsonResponse(apObjectList.get(x).getHome_address_or_Work_address()+" "+apObjectList.get(x).getState()+" "+apObjectList.get(x).getZip_Code(),apObjectList.get(x).getFacility_Address_1()+" "+apObjectList.get(x).getFacility_State()+" "+apObjectList.get(x).getFacility_Zip_Code())));
			} catch(Exception e){
				apObjectList.get(x).setMileage_from_Home_Work_Address("");
			}
			
			//Set SSN
			NIM3_EncounterObject eo2 = new  NIM3_EncounterObject(new Integer(apObjectList.get(x).getInvoice().substring(2,7)),"");
			try{
				if(eo2.getNIM3_CaseAccount().getPatientSSN().equals("Not Given")||eo2.getNIM3_CaseAccount().getPatientSSN().trim().length()==4){
					apObjectList.get(x).setLast_Four_Digits_SS(eo2.getNIM3_CaseAccount().getPatientSSN());
				} if (eo2.getNIM3_CaseAccount().getPatientSSN().trim().length()==11){
					apObjectList.get(x).setLast_Four_Digits_SS(eo2.getNIM3_CaseAccount().getPatientSSN().substring(7));
				}
				
			} catch(Exception e){
				apObjectList.get(x).setMileage_from_Home_Work_Address("");
			}
		}

		return apObjectList;
	}

	
	
	private static String getDistance(Distancematrix distancematrix){
		return distancematrix.rows.get(0).elements.get(0).distance.text;
	}
	
	private class Distancematrix {
		private String destination_addresses;
		private String origin_addresses;
		private ArrayList<Rows> rows;
		private String status;
	}
	private class Rows{
		private ArrayList<Elements> elements;
	}
	private class Elements{
		private textValue distance;
		private textValue duration;
		private String status;
		
	}
	private class textValue{
		private String text;
		private String value;
	}
}
