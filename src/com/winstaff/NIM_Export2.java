package com.winstaff;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Vector;

/**
 * Created by IntelliJ IDEA.
 * User: ste
 * Date: 5/4/11
 * Time: 5:26 PM
 * To change this template use File | Settings | File Templates.
 */
public class NIM_Export2
{
	String sFileName = "export/Export2_CSR_TEMP.txt";

    static public void main(String args[])
    {
        NIM_Export2 testMe = new NIM_Export2();
        try
        {
            testMe.runMe_CostSavings();
            testMe.toDatabase();
        }
        catch (Exception e)
        {
            System.out.println("Error in Thread: "+ e );
        }
    }

    public void runMe_CostSavings()
    {
        String CS_Version = "Encounter 1.0";
        searchDB2 mySDBA = new searchDB2();
        try
        {
            System.out.println("==============================================================================");
            System.out.println("Running Cost Savings Export Version " + CS_Version);
            java.text.SimpleDateFormat displayDateSDF1Full = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1Full);
            System.out.println("Start: " + displayDateSDF1Full.format(PLCUtils.getNowDate(true)));

            String mySQLA = "SELECT\n" +
                    "\"public\".\"vMQ4_Encounter_NoVoid_BP\".\"EncounterID\",\n" +
                    "\"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Service_modality\"\n" +
                    "FROM\n" +
                    "\"public\".\"vMQ4_Encounter_NoVoid_BP\" "; 
            
            mySQLA ="select \"EncounterID\" from export2 join tnim3_encounter en on en.encounterid = export2.\"EncounterID\" join tnim3_referral rf on rf.referralid = en.referralid join tnim3_caseaccount ca on ca.caseid = rf.caseid where encounter_uniquemodifydate < en.uniquemodifydate or encounter_uniquemodifydate < rf.uniquemodifydate or encounter_uniquemodifydate < ca.uniquemodifydate UNION select \"EncounterID\" from export2 join tnim3_service sr on sr.serviceid = export2.serviceid where export2.service_uniquemodifydate > sr.uniquemodifydate UNION select \"EncounterID\" from \"vMQ4_Encounter_NoVoid_BP\" where \"EncounterID\" not in (select \"EncounterID\" from export2 )";
            
            java.sql.ResultSet myRSA = mySDBA.executeStatement(mySQLA);
            int totalCnt=0;
            int successCnt=0;
            java.util.Vector<String> myOut = new Vector<String>();
            String myHeader = ("ScanPass\t" +
                    "EncounterID\t" +
                    "ParentPayer\t" +
                    "PayerBranch\t" +
                    "PayerID\t" +
                    "DateOfService\t" +
                    "DateOfService_iMonth\t" +
                    "DateOfService_iYear\t" +
                    "ClaimNumber\t" +
                    "Adjuster_FullName\t" +
                    "AdjusterName_BranchName\t" +
                    "AdjusterID\t" +
                    "EncounterType\t" +
                    "IsCourtesy_Status\t" +
                    "UniqueCreateDate\t" +
                    "ReceiveDate\t" +
                    "TT_Scheduled\t" +
                    "TA_Scheduled_Days\t" +
                    "TA_Scheduled_Hours\t" +
                    "TA_Init_App_Days\t" +
                    "TA_Init_App_Hours\t" +
                    "Practice_Name\t" +
                    "Practice_City\t" +
                    "Practice_State\t" +
                    "Practice_Zip\t" +
                    "Payer_BillDate\t" +
                    "Provider_BilledAmount\t" +
                    "First_CPT\t" +
                    "Service_modality\t"+
                    "First_CPT_Qty\t" +
                    "Total_BillAmount\t" +
                    "Total_AllowAmount\t" +
                    "Total_AllowAmountAdjustment\t" +
                    "Total_AllowAmount_Net\t" +
                    "Total_AllowAmount_Unit_C2\t" +
                    "Total_AllowAmount_Total_C2\t" +
                    "Total_FeeSchedule_Unit_C2\t" +
                    "Total_FeeSchedule_Total_C2\t" +
                    "Total_Savings_FromFS_Net\t" +
                    "Total_Savings_FromFS_Percentage\t" +
                    "Total_PaidIn_Amount\t" +
                    "Total_PaidIn_Amount_CheckCalc\t" +
                    "Total_PaidOut_Amount_Expected\t" +
                    "Total_Profit_Net\t" +
                    "Client_Savings_Warning\t" +
                    "Profit_Warning\t" +
                    "Payment_Warning\t" +
                    "FS Zero\t" + 
                    "UC\t" + 
                    "Medicare\t" +
                    "serviceid\t" +
                    "service_uniquemodifydate\t" +
                    "encounter_uniquemodifydate\t" +
                    "");

            myHeader = ""; //no headers
            System.out.println("");
            System.out.println("Writing File");

            SimpleDateFormat    formatter1              = new SimpleDateFormat( "MM_dd_yyyy_hh_mm" );
            SimpleDateFormat formatter2              = new SimpleDateFormat( "yyyy-MM-dd_HH:mm" );
            String              newDateFileNameString   = formatter1.format( PLCUtils.getNowDate(true) );
//            String sFileName = "export/Export2_CSR" + newDateFileNameString + ".txt";
            
            DataOutputStream    out;
            out = new DataOutputStream( new FileOutputStream( sFileName ) );
            out.writeBytes( myHeader );
            //out.writeBytes( "\n" );



            while (myRSA.next())
            {
                totalCnt++;
                /*
                if (totalCnt%3==0)
                {
                    System.out.print("\\");
                }
                else if (totalCnt%2==0)
                {
                    System.out.print(".");
                }
                else
                {
                    System.out.print("/");
                }
                */
                NIM3_EncounterObject2 myEO2 = new NIM3_EncounterObject2(new Integer(myRSA.getString("encounterid")),"CSR Load");
                String myPracticeName = "EMPTY";
                if (myEO2.getAppointment_PracticeMaster()!=null)
                {
                    myPracticeName = myEO2.getAppointment_PracticeMaster().getPracticeName();
                }
                myEO2.getNIM3_Encounter().setUniqueModifyDate(new Date());
                myEO2.getNIM3_Encounter().setUniqueModifyComments("NIM_Export2");
                myEO2.getNIM3_Encounter().commitData();
                NIM3_AddressObject myPractM_AO = myEO2.getAppointmentPracticeAddressObject();
                String tempEncounter_String = myEO2.getScanPass() + "\t" +
                    myEO2.getNIM3_Encounter().getEncounterID() + "\t" +
                    myEO2.getNIM3_ParentPayerMaster().getPayerName().trim() + "\t" +
                    myEO2.getNIM3_PayerMaster().getPayerName().trim() + "\t" +
                    myEO2.getNIM3_PayerMaster().getPayerID() + "\t" +
                    PLCUtils.getDisplayDate(myEO2.getNIM3_Encounter().getDateOfService(),true)+ "\t" +
                    PLCUtils.getDisplayDate(myEO2.getNIM3_Encounter().getDateOfService(),true,PLCUtils.DATE_MONTH)+ "\t" +
                    PLCUtils.getDisplayDate(myEO2.getNIM3_Encounter().getDateOfService(), true, PLCUtils.DATE_YEAR)+ "\t" +
                    myEO2.getNIM3_CaseAccount().getCaseClaimNumber().trim() + "\t" +
                    myEO2.getCase_Adjuster().getContactFirstName().trim() + " " + myEO2.getCase_Adjuster().getContactLastName().trim()+ "\t" +
                    myEO2.getCase_Adjuster().getContactFirstName().trim() + " " + myEO2.getCase_Adjuster().getContactLastName().trim() + ", " + myEO2.getNIM3_PayerMaster().getPayerName() + "\t" +
                    myEO2.getCase_Adjuster().getUserID() + "\t" +
                    myEO2.getEncounterType_Display() + "\t" +
                    myEO2.getCourtesyStatus_Display() + "\t" +
                    PLCUtils.getDisplayDate(myEO2.getNIM3_Encounter().getUniqueCreateDate(),true,PLCUtils.DATE_FULLDATE) + "\t" +
                    PLCUtils.getDisplayDate(myEO2.getNIM3_Referral().getReceiveDate(),true,PLCUtils.DATE_FULLDATE) + "\t" +
                    PLCUtils.getDisplayDate(myEO2.getNIM3_Encounter().getTimeTrack_ReqSched(),true,PLCUtils.DATE_FULLDATE) + "\t" +
                    workingDays.getWorkingDays(myEO2.getNIM3_Referral().getReceiveDate(),myEO2.getNIM3_Encounter().getTimeTrack_ReqSched()) + "\t" +
                    workingDays.getWorkingHours(myEO2.getNIM3_Referral().getReceiveDate(),myEO2.getNIM3_Encounter().getTimeTrack_ReqSched()) + "\t" +
                    workingDays.getWorkingDays(myEO2.getNIM3_Referral().getReceiveDate(),myEO2.getNIM3_Encounter().getTimeTrack_ReqInitialAppointment()) + "\t" +
                    workingDays.getWorkingHours(myEO2.getNIM3_Referral().getReceiveDate(),myEO2.getNIM3_Encounter().getTimeTrack_ReqInitialAppointment()) + "\t" +
                    myPracticeName + "\t" +
                    myPractM_AO.getCity().trim() + "\t" +
                    myPractM_AO.getState() + "\t" +
                    myPractM_AO.getZIP().trim() + "\t" +
                    PLCUtils.getDisplayDate(myEO2.getNIM3_Encounter().getSentTo_Bill_Pay(),false,PLCUtils.DATE_FULLDATE) + "\t" +
                    myEO2.getNIM3_Encounter().getAmountBilledByProvider() + "\t" +
                    "";
       
                bltNIM3_Service        working_bltNIM3_Service;
                ListElement         leCurrentElement_Service;
                java.util.Enumeration eList_Service = myEO2.getNIM3_Service_List().elements();
                String temp_First_CPT = "";
                String temp_First_CPT_Qty = "";
                Double total_temp_Total_Allow =0.0;
                Double total_temp_Total_Bill =0.0;
                Double total_temp_Total_AllowAdj =0.0;
                Double total_temp_NetAllow =0.0;
                Double total_temp_Allow_C2 =0.0;
                Double total_temp_TotalAllow_C2 =0.0;
                Double total_temp_FeeSchedule =0.0;
                Double total_temp_TotalFS =0.0;
                Double total_temp_Savings_FromFS_Net =0.0;
                Double total_temp_ReceivedAmount =0.0;
                Double total_temp_ReceivedAmount_CheckCalc =0.0;
                Double total_temp_ExpectedPayOut =0.0;
                Double total_temp_NetProfit =0.0;
                boolean isFS_Zero = false;
                Date service_uniquemodifydate = null;
                int serviceid = 0;
                while (eList_Service.hasMoreElements())
                {
                    leCurrentElement_Service    = (ListElement) eList_Service.nextElement();
                    working_bltNIM3_Service  = (bltNIM3_Service) leCurrentElement_Service.getObject();
                    if ( working_bltNIM3_Service.getServiceStatusID()==1)
                    {
                        Double temp_FeeSchedule = NIMUtils.getFeeSchedulePrice(new Integer(1), new Double(1.00) , myPractM_AO.getZIP(),working_bltNIM3_Service.getCPT(),working_bltNIM3_Service.getCPTModifier(),myEO2.getNIM3_Encounter().getDateOfService());
                        if (temp_FeeSchedule<0.01)
                        {
                            isFS_Zero = true;
                        }
                        //System.out.println("**"+ myPractM_AO.getZIP()+"**["+working_bltNIM3_Service.getCPT()+"]*********$"+temp_FeeSchedule+"****")       ;
                        Double temp_Allow_C2 = PLCUtils.DoubleRound_Dollars(NIMUtils.getPayerAllowAmount(myEO2.getNIM3_PayerMaster().getPayerID(), myPractM_AO.getZIP(),working_bltNIM3_Service.getCPT(),"",myEO2.getNIM3_Encounter().getDateOfService()));
                        Double temp_TotalAllow_C2 = PLCUtils.DoubleRound_Dollars(temp_Allow_C2 * working_bltNIM3_Service.getCPTQty());
                        Double temp_NetAllow = PLCUtils.DoubleRound_Dollars(working_bltNIM3_Service.getAllowAmount() + working_bltNIM3_Service.getAllowAmountAdjustment());
                        Double temp_TotalFS = PLCUtils.DoubleRound_Dollars(temp_FeeSchedule * working_bltNIM3_Service.getCPTQty());

                        Double myNetProfit = 0.0;
                        if (working_bltNIM3_Service.getPaidOutAmount()>0)
                        {
                            myNetProfit = (temp_NetAllow - working_bltNIM3_Service.getPaidOutAmount() );
                        }
                        String FSWarning = "False";
                        if (working_bltNIM3_Service.getReceivedAmount()>0&& (temp_TotalFS-working_bltNIM3_Service.getReceivedAmount()<0))
                        {
                            FSWarning = "True";
                        }

                         temp_First_CPT = working_bltNIM3_Service.getCPT().trim();
                         temp_First_CPT_Qty = working_bltNIM3_Service.getCPTQty().toString();
                         total_temp_Total_Allow += working_bltNIM3_Service.getAllowAmount();
                         total_temp_Total_Bill += working_bltNIM3_Service.getBillAmount();
                         total_temp_Total_AllowAdj += working_bltNIM3_Service.getAllowAmountAdjustment();
                         total_temp_NetAllow += temp_NetAllow;
                         total_temp_Allow_C2 += temp_Allow_C2;
                         total_temp_TotalAllow_C2 += temp_TotalAllow_C2;
                         total_temp_FeeSchedule += temp_FeeSchedule;
                         total_temp_TotalFS += temp_TotalFS;
                         total_temp_Savings_FromFS_Net += ( temp_TotalFS - temp_NetAllow );
                         total_temp_ReceivedAmount += working_bltNIM3_Service.getReceivedAmount();
                         total_temp_ReceivedAmount_CheckCalc += (working_bltNIM3_Service.getReceivedCheck1Amount() + working_bltNIM3_Service.getReceivedCheck2Amount() + working_bltNIM3_Service.getReceivedCheck3Amount());
                         total_temp_ExpectedPayOut += working_bltNIM3_Service.getPaidOutAmount();
                         total_temp_NetProfit += myNetProfit;  //expected Profit
                         working_bltNIM3_Service.setUniqueModifyDate(new Date());
                         working_bltNIM3_Service.setUniqueModifyComments("NIM_Export2");
                         service_uniquemodifydate = working_bltNIM3_Service.getUniqueModifyDate();
                         serviceid = working_bltNIM3_Service.getServiceID();
                         working_bltNIM3_Service.commitData();
                    }
                }
                String Service_modality = "";
                String s_temp_total_FeeScheduleSavingsPercent = "Error";
                String s_see_profits = "Bad FS";
                String s_see_savings = "Bad FS";
                try{
	                if (temp_First_CPT.length()==5){
		                switch (Integer.parseInt(temp_First_CPT)){
			                case 70336: case 70540: case 70544: case 70547: case 70551: case 70557: case 71550: case 71555: case 72141: case 72146: case 72148: case 72195: case 73218: case 73221: case 73718: case 73721: case 74181:
			                	Service_modality = "mr_wo";
			                    break;
			                case 70545: case 70548: case 70552: case 70558: case 71551: case 72142: case 72147: case 72149: case 72196: case 73219: case 73222: case 73719: case 73722: case 73725: case 74182: case 74185: case 75151:
			                	Service_modality = "mr_w";
			                    break;
			                case 70543: case 70546: case 70549: case 70553: case 70559: case 71552: case 72156: case 72157: case 72158: case 72159: case 72197: case 72198: case 73220: case 73223: case 73720: case 73723: case 74183: case 75152:
			                	Service_modality = "mr_wwo";
			                    break;
			                case 70450: case 70480: case 70486: case 70490: case 71250: case 72125: case 72128: case 72131: case 72192: case 73200: case 73700: case 74150: case 75571:
			                	Service_modality = "ct_wo";
			                    break;
			                case 70460: case 70481: case 70487: case 70491: case 71260: case 72126: case 72129: case 72132: case 72193: case 73201: case 73701: case 74160: case 75572:
			                	Service_modality = "ct_w";
			                    break;
			                case 70470: case 70482: case 70488: case 70492: case 71270: case 72127: case 72133: case 72194: case 73202: case 73702: case 74170:
			                	Service_modality = "ct_wwo";
			                    break;
			                case 95860: case 95861: case 95863: case 95864: case 95867: case 95868: case 95869: case 95872: case 95900: case 95903: case 95904: case 95934: case 95936: 
			                	Service_modality = "emg_all";
			                    break;
		                }
	                }
                } catch (Exception e) {System.out.println("Switch error: "+e);}
                
                String ser_modA[] = {"mr_wo","mr_w","mr_wwo","ct_wo","ct_w","ct_wwo"};
                
                List<String> ser_modL = Arrays.asList(ser_modA);
                java.text.DecimalFormat DFCurrency = new java.text.DecimalFormat(PLCUtils.String_defDecFormatCurrency1);
                String UC = "";
                try{
                	if(ser_modL.contains(Service_modality)&&!Service_modality.equals("emg_all")){
                		UC = DFCurrency.format(NIMUtils.getPayerAllowAmount(1500,myPractM_AO.getZIP().trim().substring(0,5),Service_modality,"",new java.util.Date() ) ) ;
                	}
                } catch (Exception e){}
                
                if (total_temp_TotalFS>0)
                {
                    Double temp_total_FeeScheduleSavingsPercent = ((total_temp_TotalFS -total_temp_NetAllow)/total_temp_TotalFS);
                    s_temp_total_FeeScheduleSavingsPercent = "" + temp_total_FeeScheduleSavingsPercent;
                    s_see_savings = "Valid";
                    if (total_temp_Savings_FromFS_Net<0)
                    {
                        s_see_savings = "Warning";
                    }
                    s_see_profits = "Gain";
                    if (total_temp_NetProfit<=0)
                    {
                        s_see_profits = "Loss";
                    }
                }
                String FS_Zero = "OK";
                if (isFS_Zero)
                {
                    FS_Zero = "Warning";
                }
                String ShortPay = "Not Paid";
                if (total_temp_ReceivedAmount>0&& (total_temp_NetAllow-total_temp_ReceivedAmount>0))
                {
                    ShortPay = "Short Pay";
                }
                else if (total_temp_ReceivedAmount>0&& (total_temp_NetAllow-total_temp_ReceivedAmount<0))
                {
                    ShortPay = "Over Pay";
                }
                else if (total_temp_ReceivedAmount>0&&(total_temp_NetAllow-total_temp_ReceivedAmount)==0)
                {
                    ShortPay = "Equal Pay";
                }
                double medicare = 0;
                double contractPercent = 0;
                try {
                	
            		Class cls = Class.forName("com.winstaff.bltNIM3_PayerMaster");
            		Object obj = myEO2.getNIM3_PayerMaster();
            		
            		for(int x = 1; x<4;x++){
            			if ( (new Integer(cls.getDeclaredMethod("getContract"+x+"_FeeScheduleID").invoke(obj, null).toString()))==2){
            				contractPercent = (Double) cls.getDeclaredMethod("getContract"+x+"_FeePercentage").invoke(obj, null);
            			}
            		}
            		
            		if(contractPercent==0){
            			obj = myEO2.getNIM3_ParentPayerMaster();
            			for(int x = 1; x<4;x++){
            				if ( (new Integer(cls.getDeclaredMethod("getContract"+x+"_FeeScheduleID").invoke(obj, null).toString()))==2){
            					contractPercent = (Double) cls.getDeclaredMethod("getContract"+x+"_FeePercentage").invoke(obj, null);
            				}
                		}
            		}
                	
                	medicare = NIMUtils.getFeeSchedulePrice(2, contractPercent, myPractM_AO.getZIP().trim(), temp_First_CPT, "", new Date());
                } catch (Exception e){}
                
                String tempService_String = "" +
                    temp_First_CPT + "\t" +
                    Service_modality + "\t" +
                    temp_First_CPT_Qty + "\t" +
                    total_temp_Total_Bill + "\t" +
                    total_temp_Total_Allow + "\t" +
                    total_temp_Total_AllowAdj + "\t" +
                    total_temp_NetAllow + "\t" +
                    total_temp_Allow_C2 + "\t" +
                    total_temp_TotalAllow_C2   + "\t" +
                    total_temp_FeeSchedule + "\t" +
                    total_temp_TotalFS + "\t" +
                    total_temp_Savings_FromFS_Net + "\t" +
                    s_temp_total_FeeScheduleSavingsPercent + "\t" +
                    total_temp_ReceivedAmount + "\t" +
                    total_temp_ReceivedAmount_CheckCalc + "\t" +
                    total_temp_ExpectedPayOut + "\t" +   //expected
                    total_temp_NetProfit + "\t" +   //expected Profit
                    s_see_savings + "\t" +
                    s_see_profits + "\t" +
                    ShortPay + "\t" +
                    FS_Zero + "\t" +
                    UC + "\t" +
                    "$"+medicare + "\t" +
                    serviceid + "\t" +
                    service_uniquemodifydate + "\t" +
                    myEO2.getNIM3_Encounter().getUniqueModifyDate() + "\t" +
                    
                    "";
                out.writeBytes( tempEncounter_String + tempService_String );
                out.writeBytes( "\n" );

            }
            mySDBA.closeAll();
            out.flush();
            System.out.println("");
            System.out.println("Finish: " + displayDateSDF1Full.format(PLCUtils.getNowDate(true)));
            System.out.println("==============================================================================");
        }
        catch(Exception e)
        {
            System.out.println(e);
        } finally {
            mySDBA.closeAll();
        }

    }

    public void toDatabase() throws IOException{
    	BufferedReader br = new BufferedReader(new FileReader("/var/lib/tomcat7/webapps/ROOT/WEB-INF/classes/"+sFileName));
    	String line = "";
    	searchDB2 db = new searchDB2();
    	String query = "";
    	ResultSet rs = null;
    	List<Integer> int4 = Arrays.asList(1,50);
    	List<Integer> timestamp = Arrays.asList(14,51,52);
    	while ((line = br.readLine()) != null) {
				String[] s = line.split("\t");
				
				query = "insert into export2_temp values (";
				boolean	isFirst = true;
				for (int x = 0; x<53;x++){
					query += (isFirst ? "":",");
					
					if(int4.contains(x)){
						query += s[x];
					} else if(timestamp.contains(x)){
						query += "'"+s[x]+"'::timestamp";
					} else{
						query += "'"+s[x]+"'";
					}
					
					isFirst = false;
				}
				query += ")";
				
				db.executeUpdate(query);
    	}
    	db.executeUpdate("delete from export2 where \"EncounterID\" in (select \"EncounterID\" from export2_temp)");
    	db.executeUpdate("insert into export2 select * from export2_temp");
    	db.executeUpdate("DELETE FROM export2_temp;");
    	db.closeAll();
    }

}

