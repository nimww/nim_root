

package com.winstaff;

import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public interface InttAdminPracticeLU extends dbTableInterface
{

    public void setLookupID(Integer newValue);
    public Integer getLookupID();
    public void setUniqueCreateDate(Date newValue);
    public Date getUniqueCreateDate();
    public void setUniqueModifyDate(Date newValue);
    public Date getUniqueModifyDate();
    public void setUniqueModifyComments(String newValue);
    public String getUniqueModifyComments();
    public void setAdminID(Integer newValue);
    public Integer getAdminID();
    public void setPracticeID(Integer newValue);
    public Integer getPracticeID();
    public void setComments(String newValue);
    public String getComments();
    public void setRelationshipTypeID(Integer newValue);
    public Integer getRelationshipTypeID();
}    // End of bltAdminPracticeLU class definition
