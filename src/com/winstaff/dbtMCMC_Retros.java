

package com.winstaff;

import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public class dbtMCMC_Retros extends Object implements InttMCMC_Retros
{

        db_NewBase    dbnbDB;

    public dbtMCMC_Retros()
    {
        dbnbDB = new db_NewBase( "tMCMC_Retros", "MCMCID" );

    }    // End of default constructor

    public dbtMCMC_Retros( Integer iNewID )
    {
        dbnbDB = new db_NewBase( "tMCMC_Retros", "MCMCID" );
        setUniqueID( iNewID );
    }    // End of Constructor knowing an ID

    public void commitData() throws SQLException
    {
            //no longer trapping/ignoring errors 2011-03-17
            dbnbDB.commitData();
    }    // End of commitData()

    public void commitDataForced() throws SQLException
    {
        dbnbDB.commitDataForced();
    }    // End of commitData()

    public Integer getUniqueID()
    {
        return dbnbDB.getUniqueID();
    }    // End of getUniqueID()

    public void setUniqueID( Integer iNewID )
    {
        dbnbDB.setUniqueID( iNewID );
    }    // End of setUniqueID()

    public void setModifyComments( String sNew )
    {
        dbnbDB.setModifyComments( sNew );
    }    // End of setUniqueID()


    public void setMCMCID(Integer newValue)
    {
                dbnbDB.setFieldData( "MCMCID", newValue.toString() );
    }

    public Integer getMCMCID()
    {
        String           sValue = dbnbDB.getFieldData( "MCMCID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setField(String fieldN, Object fieldV)
    {DebugLogger.println("setField Method not implemented yet in classMCMC_Retros!");}

    public String getFieldType(String fieldN)
    {return null;}


    public void setUniqueCreateDate(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "UniqueCreateDate", formatter.format( newValue ) );
    }

    public Date getUniqueCreateDate()
    {
        String           sValue = dbnbDB.getFieldData( "UniqueCreateDate" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setUniqueModifyDate(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "UniqueModifyDate", formatter.format( newValue ) );
    }

    public Date getUniqueModifyDate()
    {
        String           sValue = dbnbDB.getFieldData( "UniqueModifyDate" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setUniqueModifyComments(String newValue)
    {
                dbnbDB.setFieldData( "UniqueModifyComments", newValue.toString() );
    }

    public String getUniqueModifyComments()
    {
        String           sValue = dbnbDB.getFieldData( "UniqueModifyComments" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setMCMCBillID(Integer newValue)
    {
                dbnbDB.setFieldData( "MCMCBillID", newValue.toString() );
    }

    public Integer getMCMCBillID()
    {
        String           sValue = dbnbDB.getFieldData( "MCMCBillID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setNetworkBillID(Integer newValue)
    {
                dbnbDB.setFieldData( "NetworkBillID", newValue.toString() );
    }

    public Integer getNetworkBillID()
    {
        String           sValue = dbnbDB.getFieldData( "NetworkBillID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setMCMCLocation(String newValue)
    {
                dbnbDB.setFieldData( "MCMCLocation", newValue.toString() );
    }

    public String getMCMCLocation()
    {
        String           sValue = dbnbDB.getFieldData( "MCMCLocation" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setRenderingProviderTaxID(String newValue)
    {
                dbnbDB.setFieldData( "RenderingProviderTaxID", newValue.toString() );
    }

    public String getRenderingProviderTaxID()
    {
        String           sValue = dbnbDB.getFieldData( "RenderingProviderTaxID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setRenderingProviderName(String newValue)
    {
                dbnbDB.setFieldData( "RenderingProviderName", newValue.toString() );
    }

    public String getRenderingProviderName()
    {
        String           sValue = dbnbDB.getFieldData( "RenderingProviderName" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setRenderingProviderAddress(String newValue)
    {
                dbnbDB.setFieldData( "RenderingProviderAddress", newValue.toString() );
    }

    public String getRenderingProviderAddress()
    {
        String           sValue = dbnbDB.getFieldData( "RenderingProviderAddress" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setRenderingProviderCity(String newValue)
    {
                dbnbDB.setFieldData( "RenderingProviderCity", newValue.toString() );
    }

    public String getRenderingProviderCity()
    {
        String           sValue = dbnbDB.getFieldData( "RenderingProviderCity" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setRenderingProviderState(String newValue)
    {
                dbnbDB.setFieldData( "RenderingProviderState", newValue.toString() );
    }

    public String getRenderingProviderState()
    {
        String           sValue = dbnbDB.getFieldData( "RenderingProviderState" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setRenderingProviderZip(String newValue)
    {
                dbnbDB.setFieldData( "RenderingProviderZip", newValue.toString() );
    }

    public String getRenderingProviderZip()
    {
        String           sValue = dbnbDB.getFieldData( "RenderingProviderZip" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setBillingProviderTaxID(String newValue)
    {
                dbnbDB.setFieldData( "BillingProviderTaxID", newValue.toString() );
    }

    public String getBillingProviderTaxID()
    {
        String           sValue = dbnbDB.getFieldData( "BillingProviderTaxID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setBillingProviderName(String newValue)
    {
                dbnbDB.setFieldData( "BillingProviderName", newValue.toString() );
    }

    public String getBillingProviderName()
    {
        String           sValue = dbnbDB.getFieldData( "BillingProviderName" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setBillingProviderAddress(String newValue)
    {
                dbnbDB.setFieldData( "BillingProviderAddress", newValue.toString() );
    }

    public String getBillingProviderAddress()
    {
        String           sValue = dbnbDB.getFieldData( "BillingProviderAddress" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setBillingProviderCity(String newValue)
    {
                dbnbDB.setFieldData( "BillingProviderCity", newValue.toString() );
    }

    public String getBillingProviderCity()
    {
        String           sValue = dbnbDB.getFieldData( "BillingProviderCity" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setBillingProviderState(String newValue)
    {
                dbnbDB.setFieldData( "BillingProviderState", newValue.toString() );
    }

    public String getBillingProviderState()
    {
        String           sValue = dbnbDB.getFieldData( "BillingProviderState" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setBillingProviderZip(String newValue)
    {
                dbnbDB.setFieldData( "BillingProviderZip", newValue.toString() );
    }

    public String getBillingProviderZip()
    {
        String           sValue = dbnbDB.getFieldData( "BillingProviderZip" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setClientName(String newValue)
    {
                dbnbDB.setFieldData( "ClientName", newValue.toString() );
    }

    public String getClientName()
    {
        String           sValue = dbnbDB.getFieldData( "ClientName" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setClaimNumber(String newValue)
    {
                dbnbDB.setFieldData( "ClaimNumber", newValue.toString() );
    }

    public String getClaimNumber()
    {
        String           sValue = dbnbDB.getFieldData( "ClaimNumber" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setPatientSSN(String newValue)
    {
                dbnbDB.setFieldData( "PatientSSN", newValue.toString() );
    }

    public String getPatientSSN()
    {
        String           sValue = dbnbDB.getFieldData( "PatientSSN" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setPatientFirstName(String newValue)
    {
                dbnbDB.setFieldData( "PatientFirstName", newValue.toString() );
    }

    public String getPatientFirstName()
    {
        String           sValue = dbnbDB.getFieldData( "PatientFirstName" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setPatientLastName(String newValue)
    {
                dbnbDB.setFieldData( "PatientLastName", newValue.toString() );
    }

    public String getPatientLastName()
    {
        String           sValue = dbnbDB.getFieldData( "PatientLastName" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setPatientAddress1(String newValue)
    {
                dbnbDB.setFieldData( "PatientAddress1", newValue.toString() );
    }

    public String getPatientAddress1()
    {
        String           sValue = dbnbDB.getFieldData( "PatientAddress1" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setPatientAddress2(String newValue)
    {
                dbnbDB.setFieldData( "PatientAddress2", newValue.toString() );
    }

    public String getPatientAddress2()
    {
        String           sValue = dbnbDB.getFieldData( "PatientAddress2" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setPatientCity(String newValue)
    {
                dbnbDB.setFieldData( "PatientCity", newValue.toString() );
    }

    public String getPatientCity()
    {
        String           sValue = dbnbDB.getFieldData( "PatientCity" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setPatientState(String newValue)
    {
                dbnbDB.setFieldData( "PatientState", newValue.toString() );
    }

    public String getPatientState()
    {
        String           sValue = dbnbDB.getFieldData( "PatientState" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setPatientZip(String newValue)
    {
                dbnbDB.setFieldData( "PatientZip", newValue.toString() );
    }

    public String getPatientZip()
    {
        String           sValue = dbnbDB.getFieldData( "PatientZip" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setEmployerName(String newValue)
    {
                dbnbDB.setFieldData( "EmployerName", newValue.toString() );
    }

    public String getEmployerName()
    {
        String           sValue = dbnbDB.getFieldData( "EmployerName" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setEmployerAddress1(String newValue)
    {
                dbnbDB.setFieldData( "EmployerAddress1", newValue.toString() );
    }

    public String getEmployerAddress1()
    {
        String           sValue = dbnbDB.getFieldData( "EmployerAddress1" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setEmployerAddress2(String newValue)
    {
                dbnbDB.setFieldData( "EmployerAddress2", newValue.toString() );
    }

    public String getEmployerAddress2()
    {
        String           sValue = dbnbDB.getFieldData( "EmployerAddress2" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setEmployerCity(String newValue)
    {
                dbnbDB.setFieldData( "EmployerCity", newValue.toString() );
    }

    public String getEmployerCity()
    {
        String           sValue = dbnbDB.getFieldData( "EmployerCity" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setEmployerState(String newValue)
    {
                dbnbDB.setFieldData( "EmployerState", newValue.toString() );
    }

    public String getEmployerState()
    {
        String           sValue = dbnbDB.getFieldData( "EmployerState" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setEmployerZip(String newValue)
    {
                dbnbDB.setFieldData( "EmployerZip", newValue.toString() );
    }

    public String getEmployerZip()
    {
        String           sValue = dbnbDB.getFieldData( "EmployerZip" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setDiagnosisCode1(String newValue)
    {
                dbnbDB.setFieldData( "DiagnosisCode1", newValue.toString() );
    }

    public String getDiagnosisCode1()
    {
        String           sValue = dbnbDB.getFieldData( "DiagnosisCode1" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setDiagnosisCode2(String newValue)
    {
                dbnbDB.setFieldData( "DiagnosisCode2", newValue.toString() );
    }

    public String getDiagnosisCode2()
    {
        String           sValue = dbnbDB.getFieldData( "DiagnosisCode2" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setDiagnosisCode3(String newValue)
    {
                dbnbDB.setFieldData( "DiagnosisCode3", newValue.toString() );
    }

    public String getDiagnosisCode3()
    {
        String           sValue = dbnbDB.getFieldData( "DiagnosisCode3" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setDiagnosisCode4(String newValue)
    {
                dbnbDB.setFieldData( "DiagnosisCode4", newValue.toString() );
    }

    public String getDiagnosisCode4()
    {
        String           sValue = dbnbDB.getFieldData( "DiagnosisCode4" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setDateOfBirth(String newValue)
    {
                dbnbDB.setFieldData( "DateOfBirth", newValue.toString() );
    }

    public String getDateOfBirth()
    {
        String           sValue = dbnbDB.getFieldData( "DateOfBirth" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setInjuryDate(String newValue)
    {
                dbnbDB.setFieldData( "InjuryDate", newValue.toString() );
    }

    public String getInjuryDate()
    {
        String           sValue = dbnbDB.getFieldData( "InjuryDate" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setReviewState(String newValue)
    {
                dbnbDB.setFieldData( "ReviewState", newValue.toString() );
    }

    public String getReviewState()
    {
        String           sValue = dbnbDB.getFieldData( "ReviewState" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setReviewZip(String newValue)
    {
                dbnbDB.setFieldData( "ReviewZip", newValue.toString() );
    }

    public String getReviewZip()
    {
        String           sValue = dbnbDB.getFieldData( "ReviewZip" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setLineID(Integer newValue)
    {
                dbnbDB.setFieldData( "LineID", newValue.toString() );
    }

    public Integer getLineID()
    {
        String           sValue = dbnbDB.getFieldData( "LineID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setFSType(String newValue)
    {
                dbnbDB.setFieldData( "FSType", newValue.toString() );
    }

    public String getFSType()
    {
        String           sValue = dbnbDB.getFieldData( "FSType" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setDateOfService(String newValue)
    {
                dbnbDB.setFieldData( "DateOfService", newValue.toString() );
    }

    public String getDateOfService()
    {
        String           sValue = dbnbDB.getFieldData( "DateOfService" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setCPTCode(String newValue)
    {
                dbnbDB.setFieldData( "CPTCode", newValue.toString() );
    }

    public String getCPTCode()
    {
        String           sValue = dbnbDB.getFieldData( "CPTCode" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setModifier1(String newValue)
    {
                dbnbDB.setFieldData( "Modifier1", newValue.toString() );
    }

    public String getModifier1()
    {
        String           sValue = dbnbDB.getFieldData( "Modifier1" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setModifier2(String newValue)
    {
                dbnbDB.setFieldData( "Modifier2", newValue.toString() );
    }

    public String getModifier2()
    {
        String           sValue = dbnbDB.getFieldData( "Modifier2" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setUnits(Double newValue)
    {
                java.text.DecimalFormat defDecFormat2 = new java.text.DecimalFormat(PLCUtils.String_defDecFormat2);
                dbnbDB.setFieldData( "Units", defDecFormat2.format(newValue) );
    }

    public Double getUnits()
    {
        String           sValue = dbnbDB.getFieldData( "Units" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDoubleValue : sValue;
        return new Double( sValue  ); 
    }

    public void setProviderCharge(Double newValue)
    {
                java.text.DecimalFormat defDecFormat2 = new java.text.DecimalFormat(PLCUtils.String_defDecFormat2);
                dbnbDB.setFieldData( "ProviderCharge", defDecFormat2.format(newValue) );
    }

    public Double getProviderCharge()
    {
        String           sValue = dbnbDB.getFieldData( "ProviderCharge" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDoubleValue : sValue;
        return new Double( sValue  ); 
    }

    public void setStateAllowance(Double newValue)
    {
                java.text.DecimalFormat defDecFormat2 = new java.text.DecimalFormat(PLCUtils.String_defDecFormat2);
                dbnbDB.setFieldData( "StateAllowance", defDecFormat2.format(newValue) );
    }

    public Double getStateAllowance()
    {
        String           sValue = dbnbDB.getFieldData( "StateAllowance" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDoubleValue : sValue;
        return new Double( sValue  ); 
    }

    public void setMCMCAllowance(Double newValue)
    {
                java.text.DecimalFormat defDecFormat2 = new java.text.DecimalFormat(PLCUtils.String_defDecFormat2);
                dbnbDB.setFieldData( "MCMCAllowance", defDecFormat2.format(newValue) );
    }

    public Double getMCMCAllowance()
    {
        String           sValue = dbnbDB.getFieldData( "MCMCAllowance" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDoubleValue : sValue;
        return new Double( sValue  ); 
    }

    public void setNetworkAllowance(Double newValue)
    {
                java.text.DecimalFormat defDecFormat2 = new java.text.DecimalFormat(PLCUtils.String_defDecFormat2);
                dbnbDB.setFieldData( "NetworkAllowance", defDecFormat2.format(newValue) );
    }

    public Double getNetworkAllowance()
    {
        String           sValue = dbnbDB.getFieldData( "NetworkAllowance" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDoubleValue : sValue;
        return new Double( sValue  ); 
    }

    public void setNetworkSavings(Double newValue)
    {
                java.text.DecimalFormat defDecFormat2 = new java.text.DecimalFormat(PLCUtils.String_defDecFormat2);
                dbnbDB.setFieldData( "NetworkSavings", defDecFormat2.format(newValue) );
    }

    public Double getNetworkSavings()
    {
        String           sValue = dbnbDB.getFieldData( "NetworkSavings" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDoubleValue : sValue;
        return new Double( sValue  ); 
    }

    public void setReasonCode1(String newValue)
    {
                dbnbDB.setFieldData( "ReasonCode1", newValue.toString() );
    }

    public String getReasonCode1()
    {
        String           sValue = dbnbDB.getFieldData( "ReasonCode1" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setReasonCode2(String newValue)
    {
                dbnbDB.setFieldData( "ReasonCode2", newValue.toString() );
    }

    public String getReasonCode2()
    {
        String           sValue = dbnbDB.getFieldData( "ReasonCode2" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setReasonCode3(String newValue)
    {
                dbnbDB.setFieldData( "ReasonCode3", newValue.toString() );
    }

    public String getReasonCode3()
    {
        String           sValue = dbnbDB.getFieldData( "ReasonCode3" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setReasonCode4(String newValue)
    {
                dbnbDB.setFieldData( "ReasonCode4", newValue.toString() );
    }

    public String getReasonCode4()
    {
        String           sValue = dbnbDB.getFieldData( "ReasonCode4" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setRejectDescription(String newValue)
    {
                dbnbDB.setFieldData( "RejectDescription", newValue.toString() );
    }

    public String getRejectDescription()
    {
        String           sValue = dbnbDB.getFieldData( "RejectDescription" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setStatusID(Integer newValue)
    {
                dbnbDB.setFieldData( "StatusID", newValue.toString() );
    }

    public Integer getStatusID()
    {
        String           sValue = dbnbDB.getFieldData( "StatusID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

}    // End of bltMCMC_Retros class definition
