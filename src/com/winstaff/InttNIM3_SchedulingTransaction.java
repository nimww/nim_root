

package com.winstaff;

import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public interface InttNIM3_SchedulingTransaction extends dbTableInterface
{

    public void setSchedulingTransactionID(Integer newValue);
    public Integer getSchedulingTransactionID();
    public void setUniqueCreateDate(Date newValue);
    public Date getUniqueCreateDate();
    public void setUniqueModifyDate(Date newValue);
    public Date getUniqueModifyDate();
    public void setUniqueModifyComments(String newValue);
    public String getUniqueModifyComments();
    public void setPracticeID(Integer newValue);
    public Integer getPracticeID();
    public void setTransactionDate(Date newValue);
    public Date getTransactionDate();
    public void setActionID(Integer newValue);
    public Integer getActionID();
    public void setUserID(Integer newValue);
    public Integer getUserID();
    public void setComments(String newValue);
    public String getComments();
    public void setCompareAddress1(String newValue);
    public String getCompareAddress1();
    public void setCompareAddress2(String newValue);
    public String getCompareAddress2();
    public void setCompareCity(String newValue);
    public String getCompareCity();
    public void setCompareStateLI(String newValue);
    public String getCompareStateLI();
    public void setCompareStateCode(String newValue);
    public String getCompareStateCode();
    public void setCompareZip(String newValue);
    public String getCompareZip();
    public void setCompareCounty(String newValue);
    public String getCompareCounty();
    public void setCompareEncounterRefID(Integer newValue);
    public Integer getCompareEncounterRefID();
    public void setTotalResults(Integer newValue);
    public Integer getTotalResults();
    public void setProcedureDescription(String newValue);
    public String getProcedureDescription();
}    // End of bltNIM3_SchedulingTransaction class definition
