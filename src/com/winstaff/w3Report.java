package com.winstaff;
/**
 * This pulls all the referrals from the W3 region and groups by Payer/Adjuster and type of modality
 */
import java.sql.SQLException;
import java.text.ParseException;

public class w3Report
{
	public static void email(String emailTo, String emailFrom, String theSubject, String theBody) throws SQLException{
		bltEmailTransaction email = new bltEmailTransaction();
		email.setEmailTo(emailTo);
		email.setEmailFrom(emailFrom);
		email.setEmailSubject(theSubject);
		email.setEmailBody(theBody);
		email.setEmailBodyType(emailType_V3.HTML_TYPE);
		email.setTransactionDate(new java.util.Date());
		email.commitData();		
	}
	
	public static void main(String[] args) throws SQLException, ParseException{
		
		//String query = "SELECT \"Payer_SalesDivision\", \"Referral_ReceiveDate_iMonth\", \"PayerName\"||' - '||\"UA_Adjuster_FirstName\"||' '||\"UA_Adjuster_LastName\" \"Payer/Adjuster\", case when \"Service_modality\" in ('mri_wo','mri_w','mri_wwo') then 'mr' when  \"Service_modality\" in ('ct_wo','ct_w','ct_wwo') then 'ct' when  \"Service_modality\" in ('emg_all') then 'emg' else 'other' end \"Service\",  Count(\"Encounter_ScanPass\") as Volume   FROM \"public\".\"vMQ4_Encounter_NoVoid_BP\"   WHERE \"Referral_ReceiveDate_iYear\" = EXTRACT(YEAR FROM CURRENT_DATE) AND \"Payer_SalesDivision\" !='' AND \"Payer_SalesDivision\" != '0' AND \"Payer_SalesDivision\" != 'NIM' AND \"Payer_SalesDivision\" != 'MSC'  and \"Payer_SalesDivision\" = 'W3'  GROUP BY \"Payer_SalesDivision\", \"Referral_ReceiveDate_iMonth\",\"PayerName\"||' - '||\"UA_Adjuster_FirstName\"||' '||\"UA_Adjuster_LastName\",\"Service\" order by \"Referral_ReceiveDate_iMonth\" desc";
		String query = "SELECT \"Payer_SalesDivision\", \"Referral_ReceiveDate_iMonth\", \"PayerName\"||' - '||\"UA_Adjuster_FirstName\"||' '||\"UA_Adjuster_LastName\" \"Payer/Adjuster\", case when \"Service_modality\" in ('mri_wo','mri_w','mri_wwo') then 'mr' when  \"Service_modality\" in ('ct_wo','ct_w','ct_wwo') then 'ct' when  \"Service_modality\" in ('emg_all') then 'emg' else 'other' end \"Service\", to_char(\"Referral_ReceiveDate\", 'mm/dd/yyyy hh12:mi AM') recievedate FROM \"public\".\"vMQ4_Encounter_NoVoid_BP\"   WHERE \"Referral_ReceiveDate_iYear\" = EXTRACT(YEAR FROM CURRENT_DATE) AND \"Payer_SalesDivision\" !='' AND \"Payer_SalesDivision\" != '0' AND \"Payer_SalesDivision\" != 'NIM' AND \"Payer_SalesDivision\" != 'MSC'  and \"Payer_SalesDivision\" = 'W3'  order by \"Referral_ReceiveDate_iMonth\" desc, \"Payer/Adjuster\" asc";
		String theBody ="";
		
		java.sql.ResultSet results = new searchDB2().executeStatement(query);
		try{
			theBody +="<table><tr>";
			theBody += "<th>Sales Division</th>";
			theBody += "<th>Month</th>";
			theBody += "<th>Payer/Adjuster</th>";
			theBody += "<th>Modality</th>";
			theBody += "<th>Receive Date</th></tr>";
			
			while (results!=null&&results.next()){
				theBody +="<tr>";
				theBody += "<td>"+results.getString("Payer_SalesDivision")+"</td>";
				theBody += "<td>"+results.getString("Referral_ReceiveDate_iMonth")+"</td>";
				theBody += "<td>"+results.getString("Payer/Adjuster")+"</td>";
				theBody += "<td>"+results.getString("Service")+"</td>";
				theBody += "<td>"+results.getString("recievedate")+"</td>";
				theBody +="</tr>";
			}
			//results.close();
			theBody +="</table>";
		}
		catch(Exception e){}
		
		String theSubject = "W3 Report";
		
		email("talesha.lee@nextimagemedical.com","support@nextimagemedical.com",theSubject,theBody);
		email("Vasanthi.Murulidhar@nextimagemedical.com","support@nextimagemedical.com",theSubject,theBody);
		
		System.out.print(theBody);
	}
    
    
    
}