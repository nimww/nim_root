

package com.winstaff;

import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public class dbtPhysicianMaster extends Object implements InttPhysicianMaster
{

        db_NewBase    dbnbDB;

    public dbtPhysicianMaster()
    {
        dbnbDB = new db_NewBase( "tPhysicianMaster", "PhysicianID" );

    }    // End of default constructor

    public dbtPhysicianMaster( Integer iNewID )
    {
        dbnbDB = new db_NewBase( "tPhysicianMaster", "PhysicianID" );
        setUniqueID( iNewID );
    }    // End of Constructor knowing an ID

    public void commitData() throws SQLException
    {
            //no longer trapping/ignoring errors 2011-03-17
            dbnbDB.commitData();
    }    // End of commitData()

    public void commitDataForced() throws SQLException
    {
        dbnbDB.commitDataForced();
    }    // End of commitData()

    public Integer getUniqueID()
    {
        return dbnbDB.getUniqueID();
    }    // End of getUniqueID()

    public void setUniqueID( Integer iNewID )
    {
        dbnbDB.setUniqueID( iNewID );
    }    // End of setUniqueID()

    public void setModifyComments( String sNew )
    {
        dbnbDB.setModifyComments( sNew );
    }    // End of setUniqueID()


    public void setPhysicianID(Integer newValue)
    {
                dbnbDB.setFieldData( "PhysicianID", newValue.toString() );
    }

    public Integer getPhysicianID()
    {
        String           sValue = dbnbDB.getFieldData( "PhysicianID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setField(String fieldN, Object fieldV)
    {DebugLogger.println("setField Method not implemented yet in classPhysicianMaster!");}

    public String getFieldType(String fieldN)
    {return null;}


    public void setUniqueCreateDate(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "UniqueCreateDate", formatter.format( newValue ) );
    }

    public Date getUniqueCreateDate()
    {
        String           sValue = dbnbDB.getFieldData( "UniqueCreateDate" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setUniqueModifyDate(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "UniqueModifyDate", formatter.format( newValue ) );
    }

    public Date getUniqueModifyDate()
    {
        String           sValue = dbnbDB.getFieldData( "UniqueModifyDate" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setUniqueModifyComments(String newValue)
    {
                dbnbDB.setFieldData( "UniqueModifyComments", newValue.toString() );
    }

    public String getUniqueModifyComments()
    {
        String           sValue = dbnbDB.getFieldData( "UniqueModifyComments" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setSalutation(Integer newValue)
    {
                dbnbDB.setFieldData( "Salutation", newValue.toString() );
    }

    public Integer getSalutation()
    {
        String           sValue = dbnbDB.getFieldData( "Salutation" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setTitle(String newValue)
    {
                dbnbDB.setFieldData( "Title", newValue.toString() );
    }

    public String getTitle()
    {
        String           sValue = dbnbDB.getFieldData( "Title" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setFirstName(String newValue)
    {
                dbnbDB.setFieldData( "FirstName", newValue.toString() );
    }

    public String getFirstName()
    {
        String           sValue = dbnbDB.getFieldData( "FirstName" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setMiddleName(String newValue)
    {
                dbnbDB.setFieldData( "MiddleName", newValue.toString() );
    }

    public String getMiddleName()
    {
        String           sValue = dbnbDB.getFieldData( "MiddleName" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setLastName(String newValue)
    {
                dbnbDB.setFieldData( "LastName", newValue.toString() );
    }

    public String getLastName()
    {
        String           sValue = dbnbDB.getFieldData( "LastName" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setSuffix(String newValue)
    {
                dbnbDB.setFieldData( "Suffix", newValue.toString() );
    }

    public String getSuffix()
    {
        String           sValue = dbnbDB.getFieldData( "Suffix" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setHomeEmail(String newValue)
    {
                dbnbDB.setFieldData( "HomeEmail", newValue.toString() );
    }

    public String getHomeEmail()
    {
        String           sValue = dbnbDB.getFieldData( "HomeEmail" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setHomeAddress1(String newValue)
    {
                dbnbDB.setFieldData( "HomeAddress1", newValue.toString() );
    }

    public String getHomeAddress1()
    {
        String           sValue = dbnbDB.getFieldData( "HomeAddress1" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setHomeAddress2(String newValue)
    {
                dbnbDB.setFieldData( "HomeAddress2", newValue.toString() );
    }

    public String getHomeAddress2()
    {
        String           sValue = dbnbDB.getFieldData( "HomeAddress2" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setHomeCity(String newValue)
    {
                dbnbDB.setFieldData( "HomeCity", newValue.toString() );
    }

    public String getHomeCity()
    {
        String           sValue = dbnbDB.getFieldData( "HomeCity" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setHomeStateID(Integer newValue)
    {
                dbnbDB.setFieldData( "HomeStateID", newValue.toString() );
    }

    public Integer getHomeStateID()
    {
        String           sValue = dbnbDB.getFieldData( "HomeStateID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setHomeProvince(String newValue)
    {
                dbnbDB.setFieldData( "HomeProvince", newValue.toString() );
    }

    public String getHomeProvince()
    {
        String           sValue = dbnbDB.getFieldData( "HomeProvince" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setHomeZIP(String newValue)
    {
                dbnbDB.setFieldData( "HomeZIP", newValue.toString() );
    }

    public String getHomeZIP()
    {
        String           sValue = dbnbDB.getFieldData( "HomeZIP" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setHomeCountryID(Integer newValue)
    {
                dbnbDB.setFieldData( "HomeCountryID", newValue.toString() );
    }

    public Integer getHomeCountryID()
    {
        String           sValue = dbnbDB.getFieldData( "HomeCountryID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setHomePhone(String newValue)
    {
                dbnbDB.setFieldData( "HomePhone", newValue.toString() );
    }

    public String getHomePhone()
    {
        String           sValue = dbnbDB.getFieldData( "HomePhone" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setHomeFax(String newValue)
    {
                dbnbDB.setFieldData( "HomeFax", newValue.toString() );
    }

    public String getHomeFax()
    {
        String           sValue = dbnbDB.getFieldData( "HomeFax" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setHomeMobile(String newValue)
    {
                dbnbDB.setFieldData( "HomeMobile", newValue.toString() );
    }

    public String getHomeMobile()
    {
        String           sValue = dbnbDB.getFieldData( "HomeMobile" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setHomePager(String newValue)
    {
                dbnbDB.setFieldData( "HomePager", newValue.toString() );
    }

    public String getHomePager()
    {
        String           sValue = dbnbDB.getFieldData( "HomePager" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setAlertEmail(String newValue)
    {
                dbnbDB.setFieldData( "AlertEmail", newValue.toString() );
    }

    public String getAlertEmail()
    {
        String           sValue = dbnbDB.getFieldData( "AlertEmail" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setAlertDays(Integer newValue)
    {
                dbnbDB.setFieldData( "AlertDays", newValue.toString() );
    }

    public Integer getAlertDays()
    {
        String           sValue = dbnbDB.getFieldData( "AlertDays" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setOtherName1(String newValue)
    {
                dbnbDB.setFieldData( "OtherName1", newValue.toString() );
    }

    public String getOtherName1()
    {
        String           sValue = dbnbDB.getFieldData( "OtherName1" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setOtherName1Start(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "OtherName1Start", formatter.format( newValue ) );
    }

    public Date getOtherName1Start()
    {
        String           sValue = dbnbDB.getFieldData( "OtherName1Start" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setOtherName1End(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "OtherName1End", formatter.format( newValue ) );
    }

    public Date getOtherName1End()
    {
        String           sValue = dbnbDB.getFieldData( "OtherName1End" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setOtherName2(String newValue)
    {
                dbnbDB.setFieldData( "OtherName2", newValue.toString() );
    }

    public String getOtherName2()
    {
        String           sValue = dbnbDB.getFieldData( "OtherName2" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setOtherName2Start(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "OtherName2Start", formatter.format( newValue ) );
    }

    public Date getOtherName2Start()
    {
        String           sValue = dbnbDB.getFieldData( "OtherName2Start" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setOtherName2End(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "OtherName2End", formatter.format( newValue ) );
    }

    public Date getOtherName2End()
    {
        String           sValue = dbnbDB.getFieldData( "OtherName2End" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setDateOfBirth(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "DateOfBirth", formatter.format( newValue ) );
    }

    public Date getDateOfBirth()
    {
        String           sValue = dbnbDB.getFieldData( "DateOfBirth" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setPlaceOfBirth(String newValue)
    {
                dbnbDB.setFieldData( "PlaceOfBirth", newValue.toString() );
    }

    public String getPlaceOfBirth()
    {
        String           sValue = dbnbDB.getFieldData( "PlaceOfBirth" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setSpouse(String newValue)
    {
                dbnbDB.setFieldData( "Spouse", newValue.toString() );
    }

    public String getSpouse()
    {
        String           sValue = dbnbDB.getFieldData( "Spouse" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setCitizenshipYN(Integer newValue)
    {
                dbnbDB.setFieldData( "CitizenshipYN", newValue.toString() );
    }

    public Integer getCitizenshipYN()
    {
        String           sValue = dbnbDB.getFieldData( "CitizenshipYN" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setCitizenship(String newValue)
    {
                dbnbDB.setFieldData( "Citizenship", newValue.toString() );
    }

    public String getCitizenship()
    {
        String           sValue = dbnbDB.getFieldData( "Citizenship" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setVisaNumber(String newValue)
    {
                dbnbDB.setFieldData( "VisaNumber", newValue.toString() );
    }

    public String getVisaNumber()
    {
        String           sValue = dbnbDB.getFieldData( "VisaNumber" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setVisaStatus(String newValue)
    {
                dbnbDB.setFieldData( "VisaStatus", newValue.toString() );
    }

    public String getVisaStatus()
    {
        String           sValue = dbnbDB.getFieldData( "VisaStatus" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setEligibleToWorkInUS(Integer newValue)
    {
                dbnbDB.setFieldData( "EligibleToWorkInUS", newValue.toString() );
    }

    public Integer getEligibleToWorkInUS()
    {
        String           sValue = dbnbDB.getFieldData( "EligibleToWorkInUS" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setSSN(String newValue)
    {
                dbnbDB.setFieldData( "SSN", newValue.toString() );
    }

    public String getSSN()
    {
        String           sValue = dbnbDB.getFieldData( "SSN" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setGender(Integer newValue)
    {
                dbnbDB.setFieldData( "Gender", newValue.toString() );
    }

    public Integer getGender()
    {
        String           sValue = dbnbDB.getFieldData( "Gender" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setMilitaryActive(Integer newValue)
    {
                dbnbDB.setFieldData( "MilitaryActive", newValue.toString() );
    }

    public Integer getMilitaryActive()
    {
        String           sValue = dbnbDB.getFieldData( "MilitaryActive" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setMilitaryBranch(String newValue)
    {
                dbnbDB.setFieldData( "MilitaryBranch", newValue.toString() );
    }

    public String getMilitaryBranch()
    {
        String           sValue = dbnbDB.getFieldData( "MilitaryBranch" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setMilitaryReserve(Integer newValue)
    {
                dbnbDB.setFieldData( "MilitaryReserve", newValue.toString() );
    }

    public Integer getMilitaryReserve()
    {
        String           sValue = dbnbDB.getFieldData( "MilitaryReserve" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setHospitalAdmitingPrivileges(Integer newValue)
    {
                dbnbDB.setFieldData( "HospitalAdmitingPrivileges", newValue.toString() );
    }

    public Integer getHospitalAdmitingPrivileges()
    {
        String           sValue = dbnbDB.getFieldData( "HospitalAdmitingPrivileges" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setHospitalAdmitingPrivilegesNo(String newValue)
    {
                dbnbDB.setFieldData( "HospitalAdmitingPrivilegesNo", newValue.toString() );
    }

    public String getHospitalAdmitingPrivilegesNo()
    {
        String           sValue = dbnbDB.getFieldData( "HospitalAdmitingPrivilegesNo" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setPhysicianCategoryID(Integer newValue)
    {
                dbnbDB.setFieldData( "PhysicianCategoryID", newValue.toString() );
    }

    public Integer getPhysicianCategoryID()
    {
        String           sValue = dbnbDB.getFieldData( "PhysicianCategoryID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setIPAMedicalAffiliation(Integer newValue)
    {
                dbnbDB.setFieldData( "IPAMedicalAffiliation", newValue.toString() );
    }

    public Integer getIPAMedicalAffiliation()
    {
        String           sValue = dbnbDB.getFieldData( "IPAMedicalAffiliation" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setAffiliationDesc1(String newValue)
    {
                dbnbDB.setFieldData( "AffiliationDesc1", newValue.toString() );
    }

    public String getAffiliationDesc1()
    {
        String           sValue = dbnbDB.getFieldData( "AffiliationDesc1" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setPhysicianLanguages(String newValue)
    {
                dbnbDB.setFieldData( "PhysicianLanguages", newValue.toString() );
    }

    public String getPhysicianLanguages()
    {
        String           sValue = dbnbDB.getFieldData( "PhysicianLanguages" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setECFMGNo(String newValue)
    {
                dbnbDB.setFieldData( "ECFMGNo", newValue.toString() );
    }

    public String getECFMGNo()
    {
        String           sValue = dbnbDB.getFieldData( "ECFMGNo" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setECFMGDateIssued(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "ECFMGDateIssued", formatter.format( newValue ) );
    }

    public Date getECFMGDateIssued()
    {
        String           sValue = dbnbDB.getFieldData( "ECFMGDateIssued" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setECFMGDateExpires(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "ECFMGDateExpires", formatter.format( newValue ) );
    }

    public Date getECFMGDateExpires()
    {
        String           sValue = dbnbDB.getFieldData( "ECFMGDateExpires" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setMedicareUPIN(String newValue)
    {
                dbnbDB.setFieldData( "MedicareUPIN", newValue.toString() );
    }

    public String getMedicareUPIN()
    {
        String           sValue = dbnbDB.getFieldData( "MedicareUPIN" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setUniqueNPI(String newValue)
    {
                dbnbDB.setFieldData( "UniqueNPI", newValue.toString() );
    }

    public String getUniqueNPI()
    {
        String           sValue = dbnbDB.getFieldData( "UniqueNPI" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setMedicareParticipation(Integer newValue)
    {
                dbnbDB.setFieldData( "MedicareParticipation", newValue.toString() );
    }

    public Integer getMedicareParticipation()
    {
        String           sValue = dbnbDB.getFieldData( "MedicareParticipation" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setMedicareNo(String newValue)
    {
                dbnbDB.setFieldData( "MedicareNo", newValue.toString() );
    }

    public String getMedicareNo()
    {
        String           sValue = dbnbDB.getFieldData( "MedicareNo" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setMediCaidParticipation(Integer newValue)
    {
                dbnbDB.setFieldData( "MediCaidParticipation", newValue.toString() );
    }

    public Integer getMediCaidParticipation()
    {
        String           sValue = dbnbDB.getFieldData( "MediCaidParticipation" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setMediCaidNo(String newValue)
    {
                dbnbDB.setFieldData( "MediCaidNo", newValue.toString() );
    }

    public String getMediCaidNo()
    {
        String           sValue = dbnbDB.getFieldData( "MediCaidNo" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setSupplementalIDNumber1(String newValue)
    {
                dbnbDB.setFieldData( "SupplementalIDNumber1", newValue.toString() );
    }

    public String getSupplementalIDNumber1()
    {
        String           sValue = dbnbDB.getFieldData( "SupplementalIDNumber1" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setSupplementalIDNumber2(String newValue)
    {
                dbnbDB.setFieldData( "SupplementalIDNumber2", newValue.toString() );
    }

    public String getSupplementalIDNumber2()
    {
        String           sValue = dbnbDB.getFieldData( "SupplementalIDNumber2" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setComments(String newValue)
    {
                dbnbDB.setFieldData( "Comments", newValue.toString() );
    }

    public String getComments()
    {
        String           sValue = dbnbDB.getFieldData( "Comments" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setAttestConsent(Integer newValue)
    {
                dbnbDB.setFieldData( "AttestConsent", newValue.toString() );
    }

    public Integer getAttestConsent()
    {
        String           sValue = dbnbDB.getFieldData( "AttestConsent" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setAttestationComments(String newValue)
    {
                dbnbDB.setFieldData( "AttestationComments", newValue.toString() );
    }

    public String getAttestationComments()
    {
        String           sValue = dbnbDB.getFieldData( "AttestationComments" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setIsViewable(Integer newValue)
    {
                dbnbDB.setFieldData( "IsViewable", newValue.toString() );
    }

    public Integer getIsViewable()
    {
        String           sValue = dbnbDB.getFieldData( "IsViewable" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setIsAttested(Integer newValue)
    {
                dbnbDB.setFieldData( "IsAttested", newValue.toString() );
    }

    public Integer getIsAttested()
    {
        String           sValue = dbnbDB.getFieldData( "IsAttested" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setAttestDatePending(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "AttestDatePending", formatter.format( newValue ) );
    }

    public Date getAttestDatePending()
    {
        String           sValue = dbnbDB.getFieldData( "AttestDatePending" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setIsAttestedPending(Integer newValue)
    {
                dbnbDB.setFieldData( "IsAttestedPending", newValue.toString() );
    }

    public Integer getIsAttestedPending()
    {
        String           sValue = dbnbDB.getFieldData( "IsAttestedPending" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setAttestDate(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "AttestDate", formatter.format( newValue ) );
    }

    public Date getAttestDate()
    {
        String           sValue = dbnbDB.getFieldData( "AttestDate" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setDeAttestDate(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "DeAttestDate", formatter.format( newValue ) );
    }

    public Date getDeAttestDate()
    {
        String           sValue = dbnbDB.getFieldData( "DeAttestDate" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setLastModifiedDate(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "LastModifiedDate", formatter.format( newValue ) );
    }

    public Date getLastModifiedDate()
    {
        String           sValue = dbnbDB.getFieldData( "LastModifiedDate" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setQuestionValidation(String newValue)
    {
                dbnbDB.setFieldData( "QuestionValidation", newValue.toString() );
    }

    public String getQuestionValidation()
    {
        String           sValue = dbnbDB.getFieldData( "QuestionValidation" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setUSMLEDatePassedStep1(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "USMLEDatePassedStep1", formatter.format( newValue ) );
    }

    public Date getUSMLEDatePassedStep1()
    {
        String           sValue = dbnbDB.getFieldData( "USMLEDatePassedStep1" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setUSMLEDatePassedStep2(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "USMLEDatePassedStep2", formatter.format( newValue ) );
    }

    public Date getUSMLEDatePassedStep2()
    {
        String           sValue = dbnbDB.getFieldData( "USMLEDatePassedStep2" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setUSMLEDatePassedStep3(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "USMLEDatePassedStep3", formatter.format( newValue ) );
    }

    public Date getUSMLEDatePassedStep3()
    {
        String           sValue = dbnbDB.getFieldData( "USMLEDatePassedStep3" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setHaveFLEX(Integer newValue)
    {
                dbnbDB.setFieldData( "HaveFLEX", newValue.toString() );
    }

    public Integer getHaveFLEX()
    {
        String           sValue = dbnbDB.getFieldData( "HaveFLEX" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setFLEXDatePassed(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "FLEXDatePassed", formatter.format( newValue ) );
    }

    public Date getFLEXDatePassed()
    {
        String           sValue = dbnbDB.getFieldData( "FLEXDatePassed" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setVisaSponsor(String newValue)
    {
                dbnbDB.setFieldData( "VisaSponsor", newValue.toString() );
    }

    public String getVisaSponsor()
    {
        String           sValue = dbnbDB.getFieldData( "VisaSponsor" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setVisaExpiration(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "VisaExpiration", formatter.format( newValue ) );
    }

    public Date getVisaExpiration()
    {
        String           sValue = dbnbDB.getFieldData( "VisaExpiration" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setCurrentVisaTemporary(Integer newValue)
    {
                dbnbDB.setFieldData( "CurrentVisaTemporary", newValue.toString() );
    }

    public Integer getCurrentVisaTemporary()
    {
        String           sValue = dbnbDB.getFieldData( "CurrentVisaTemporary" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setCurrentVisaExtended(Integer newValue)
    {
                dbnbDB.setFieldData( "CurrentVisaExtended", newValue.toString() );
    }

    public Integer getCurrentVisaExtended()
    {
        String           sValue = dbnbDB.getFieldData( "CurrentVisaExtended" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setHaveGreencard(Integer newValue)
    {
                dbnbDB.setFieldData( "HaveGreencard", newValue.toString() );
    }

    public Integer getHaveGreencard()
    {
        String           sValue = dbnbDB.getFieldData( "HaveGreencard" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setCountryOfIssueID(Integer newValue)
    {
                dbnbDB.setFieldData( "CountryOfIssueID", newValue.toString() );
    }

    public Integer getCountryOfIssueID()
    {
        String           sValue = dbnbDB.getFieldData( "CountryOfIssueID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setVisaTemporary5Years(Integer newValue)
    {
                dbnbDB.setFieldData( "VisaTemporary5Years", newValue.toString() );
    }

    public Integer getVisaTemporary5Years()
    {
        String           sValue = dbnbDB.getFieldData( "VisaTemporary5Years" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setPastVisa1DateFrom(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "PastVisa1DateFrom", formatter.format( newValue ) );
    }

    public Date getPastVisa1DateFrom()
    {
        String           sValue = dbnbDB.getFieldData( "PastVisa1DateFrom" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setPastVisa1DateTo(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "PastVisa1DateTo", formatter.format( newValue ) );
    }

    public Date getPastVisa1DateTo()
    {
        String           sValue = dbnbDB.getFieldData( "PastVisa1DateTo" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setPastVisa1Type(String newValue)
    {
                dbnbDB.setFieldData( "PastVisa1Type", newValue.toString() );
    }

    public String getPastVisa1Type()
    {
        String           sValue = dbnbDB.getFieldData( "PastVisa1Type" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setPastVisa1Sponsor(String newValue)
    {
                dbnbDB.setFieldData( "PastVisa1Sponsor", newValue.toString() );
    }

    public String getPastVisa1Sponsor()
    {
        String           sValue = dbnbDB.getFieldData( "PastVisa1Sponsor" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setPastVisa2DateFrom(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "PastVisa2DateFrom", formatter.format( newValue ) );
    }

    public Date getPastVisa2DateFrom()
    {
        String           sValue = dbnbDB.getFieldData( "PastVisa2DateFrom" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setPastVisa2DateTo(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "PastVisa2DateTo", formatter.format( newValue ) );
    }

    public Date getPastVisa2DateTo()
    {
        String           sValue = dbnbDB.getFieldData( "PastVisa2DateTo" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setPastVisa2Type(String newValue)
    {
                dbnbDB.setFieldData( "PastVisa2Type", newValue.toString() );
    }

    public String getPastVisa2Type()
    {
        String           sValue = dbnbDB.getFieldData( "PastVisa2Type" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setPastVisa2Sponsor(String newValue)
    {
                dbnbDB.setFieldData( "PastVisa2Sponsor", newValue.toString() );
    }

    public String getPastVisa2Sponsor()
    {
        String           sValue = dbnbDB.getFieldData( "PastVisa2Sponsor" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setMilitaryRank(String newValue)
    {
                dbnbDB.setFieldData( "MilitaryRank", newValue.toString() );
    }

    public String getMilitaryRank()
    {
        String           sValue = dbnbDB.getFieldData( "MilitaryRank" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setMilitaryDutyStatus(String newValue)
    {
                dbnbDB.setFieldData( "MilitaryDutyStatus", newValue.toString() );
    }

    public String getMilitaryDutyStatus()
    {
        String           sValue = dbnbDB.getFieldData( "MilitaryDutyStatus" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setMilitaryCurrentAssignment(String newValue)
    {
                dbnbDB.setFieldData( "MilitaryCurrentAssignment", newValue.toString() );
    }

    public String getMilitaryCurrentAssignment()
    {
        String           sValue = dbnbDB.getFieldData( "MilitaryCurrentAssignment" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

}    // End of bltPhysicianMaster class definition
