package com.winstaff;

import java.io.BufferedReader;
import java.io.FileReader;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.StringTokenizer;

/**
 * Created with IntelliJ IDEA.
 * User: btdev
 * Date: 4/10/13
 * Time: 10:50 PM
 * To change this template use File | Settings | File Templates.
 */
public class EDI_RetroAnalysis {


    private String localDir = "/var/lib/" + ConfigurationInformation.sTomcat + "/webapps/EDI/retro/";

    static public void main(String args[]) {
        EDI_RetroAnalysis retro = new EDI_RetroAnalysis();
        System.out.println("> NIM_EDI(Java): EDI Retro Analysis");
        if (true) {
            try {
                if (args[0].equalsIgnoreCase("run")) {
                    System.out.println("-->  'run' command detected");
                    String use_file = args[1];
                    System.out.println("--> NIM translation 'import' command detected");
                    System.out.println("----> using folder [" + retro.localDir + "]");
                    System.out.println("----> using file [" + use_file + "]");
                    int c_total = retro.runFile_MCMC(use_file);
                    System.out.println("----> Total Source Records: " + c_total);
                }
            } catch (Exception e) {
                DebugLogger.e("EDI_RetroAnalysis:Thread Error",e.toString());
                System.out.println("Error in Thread: " + e);
            }
        }
    }


    public int runFile  (String in) throws Exception {
    System.out.println("--->  Starting Analysis");
        int countLines = 0;
        BufferedReader bufferedReader = null;
        bufferedReader = new BufferedReader(new FileReader(this.localDir+in));
        String line = null;
        while ((line = bufferedReader.readLine()) != null) {
            try {
                countLines++;

            }  catch (Exception e){
                System.out.println("--->  General Error in while loop (" + e + ")");
            }

        }
        System.out.println("--->  Ending Analysis");
        return countLines;
    }


    public int runFile_MCMC  (String in) throws Exception {
        System.out.println("--->  Starting Analysis [Type: MCMC]");
        int countLines = 0;
        BufferedReader bufferedReader = null;
        bufferedReader = new BufferedReader(new FileReader(this.localDir+in));
        String line = null;
//        SimpleDateFormat translateDate = new SimpleDateFormat("MMddyyyy");
        SimpleDateFormat translateDate = new SimpleDateFormat("yyyyMMdd");
        while ((line = bufferedReader.readLine()) != null) {
            try {
                countLines++;
                //MCMC uses the | to delimit
                String[] result = line.split("\\|");
                bltNIM3_RetroWorklist myRW = new bltNIM3_RetroWorklist();
                String sNotes = "Started on " + PLCUtils.getDisplayDate(PLCUtils.getToday(), true);
                myRW.setPayerID(1375); //Production MCMC
                myRW.setNetwork_UserField1(result[0]); //0 - MCMC Bill ID (int:10) [MCMC's unique identifier]
                myRW.setNetwork_UserField2(result[1]); //1 - Network Bill ID (int:30) [Network's unique identifier]
                myRW.setNetwork_UserField3(result[2]); //2 - MCMC Location (char:20) [MCMC's internal office code]
                myRW.setRenderingProviderTaxID(result[3]); //3 - Rendering Provider Tax ID (char:9) []
                myRW.setRenderingProviderLastName(result[4]); //4 - Rendering Provider Name (char:100) []
                myRW.setRenderingProviderAddress1(result[5]); //5 - Rendering Provider Address (char:50) []
                myRW.setRenderingProviderCity(result[6]); //6 - Rendering Provider City (char:50) []
                myRW.setRenderingProviderState(result[7]); //7 - Rendering Provider State (char:2) []
                myRW.setRenderingProviderZip(result[8]); //8 - Rendering Provider Zip (char:5) []
                myRW.setBillingProviderTaxID(result[9]); //9 - Billing Provider Tax ID (char:9) []
                myRW.setBillingProviderLastName(result[10]); //10 - Billing Provider Name (char:100) []
                myRW.setBillingProviderAddress1(result[11]); //11 - Billing Provider Address (char:50) []
                myRW.setBillingProviderCity(result[12]); //12 - Billing Provider City (char:50) []
                myRW.setBillingProviderState(result[13]); //13 - Billing Provider State (char:2) []
                myRW.setBillingProviderZip(result[14]); //14 - Billing Provider Zip (char:5) []
                myRW.setClientName(result[15]); //15 - Client Name (char:75) [MCMC's client name]
                myRW.setClientClaimNum(result[16]); //16 - Claim Number (char:35) []
                myRW.setPatientSSN(result[17]); //17 - Patient SSN (char:9) []
                myRW.setPatientFirstName(result[18]); //18 - Patient First Name (char:50) []
                myRW.setPatientLastName(result[19]); //19 - Patient Last Name (char:50) []
                myRW.setPatientAddress1(result[20]); //20 - Patient Address1 (char:100) []
                myRW.setPatientAddress2(result[21]); //21 - Patient Address2 (char:100) []
                myRW.setPatientCity(result[22]); //22 - Patient City (char:50) []
                myRW.setPatientState(result[23]); //23 - Patient State (char:2) []
                myRW.setPatientZip(result[24]); //24 - Patient Zip (char:5) []
                myRW.setEmployerName(result[25]); //25 - Employer Name (char:100) []
                myRW.setEmployerAddress1(result[26]); //26 - Employer Address1 (char:100) []
                myRW.setEmployerAddress2(result[27]); //27 - Employer Address2 (char:100) []
                myRW.setEmployerCity(result[28]); //28 - Employer City (char:50) []
                myRW.setEmployerState(result[29]); //29 - Employer State (char:2) []
                myRW.setEmployerZip(result[30]); //30 - Employer Zip (char:5) []
                myRW.setDiagCode1(result[31]); //31 - Diagnosis Code 1 (char:5) []
                myRW.setDiagCode2(result[32]); //32 - Diagnosis Code 2 (char:5) []
                myRW.setDiagCode3(result[33]); //33 - Diagnosis Code 3 (char:5) []
                myRW.setDiagCode4(result[34]); //34 - Diagnosis Code 4  (char:5) []
                try {
                    myRW.setPatientDOB(translateDate.parse(result[35]));     //35 - Date Of Birth (char:8) [yyyymmdd]
                } catch (java.text.ParseException pe){
                    sNotes+= "\nInvalid Date of Birth";
                    System.out.println("Warning L2[^^]: Invalid Date of Birth: skipping this one field");
                }
                try {
                    myRW.setDateOfInjury(translateDate.parse(result[36]));   //36 - Injury Date (char:8) [yyyymmdd]
                } catch (java.text.ParseException pe){
                    sNotes+= "\nInvalid Date of Injury";
                    System.out.println("Warning L2[^^]: Invalid Date of Injury: skipping this one field");
                }
                myRW.setJurisdicationState(result[37]); //37 - Review State   (char:2) [State Jurisdiction]
                myRW.setJurisdicationZip(result[38]); //38 - Review Zip   (char:5) [Zip Code where services were rendered]

                try {
                    myRW.setLineID(new Integer(result[39])); //39 - Line ID   (int:5) [Line Number]
                } catch (Exception pe){
                    sNotes+= "\nInvalid Line ID";
                    System.out.println("Warning L2[^^]: Invalid Line ID skipping this one field");
                }
                //skipping      40 - FS Type (a/b)  (char:1) [Not Applicable - Ignore]
                try {
                    myRW.setDateofService(translateDate.parse(result[41]));   //36 - Injury Date (char:8) [yyyymmdd]
                } catch (java.text.ParseException pe){
                    sNotes+= "\nInvalid DOS";
                    System.out.println("Warning L2[^^]: Invalid Date of Service: skipping this one field");
                }
                myRW.setCPTCode(result[42]); //42 - CPT Code (char:5) []
                myRW.setModifier1(result[43]); //43 - Modifier 1 (char:2) []
                myRW.setModifier2(result[44]); //44 - Modifier 2 (char:2) []
                try {
                    myRW.setUnits(new Double(result[45])); //45 - Units    (double:5) []
                } catch (Exception pe){
                    sNotes+= "\nInvalid Units";
                    System.out.println("Warning L2[^^]: Invalid Units: skipping this one field");
                }
                try {
                    myRW.setProviderCharge(new Double(result[46])); //46 - Provider Charge    (double:10) [Explicit decimal]
                } catch (Exception pe){
                    sNotes+= "\nInvalid Provider Charge";
                    System.out.println("Warning L2[^^]: Invalid Provider Charge: skipping this one field");
                }
                try {
                    myRW.setStateAllowance(new Double(result[47])); //47 - State Allowance (double:10) [Explicit decimal]
                } catch (Exception pe){
                    sNotes+= "\nInvalid State Allowance";
                    System.out.println("Warning L2[^^]: Invalid State Allowance: skipping this one field");
                }
                try {
                    myRW.setPayerAllowance(new Double(result[48])); //48 - MCMC Allowance (double:10) [Allowance after fee schedule and MCMC's Bill Review rules are applied - Explicit decimal]
                } catch (Exception pe){
                    sNotes+= "\nInvalid Payer Allowance";
                    System.out.println("Warning L2[^^]: Invalid Payer Allowance: skipping this one field");
                }
                try {
                    myRW.setNetworkAllowance(new Double(result[49])); //49 - Network Allowance (double:10) [Returned by the network - Explicit decimal]
                } catch (Exception pe){
                    sNotes+= "\nInvalid Network Allowance";
                    System.out.println("Warning L2[^^]: Invalid Network Allowance: skipping this one field");
                }
                try {
                    myRW.setNetworkSavings(new Double(result[50])); //50 - Network Savings (double:10) [Returned by the network - Explicit decimal]
                } catch (Exception pe){
                    sNotes+= "\nInvalid Network Savings";
                    System.out.println("Warning L2[^^]: Invalid Network Savings: skipping this one field");
                }

                myRW.setNotes("Notes:\n" + sNotes);
//                myRW.setReasonCode1(result[51]); //51 - Reason Code  1 (char:5) [Ignore]
//                myRW.setReasonCode2(result[52]); //52 - Reason Code 2 (char:5) [Ignore]
//                myRW.setReasonCode3(result[53]); //53 - Reason Code 3 (char:5) [Ignore]
//                myRW.setReasonCode4(result[54]); //54 - Reason Code 4 (char:5) [Ignore]
//                myRW.setRejectDescription(result[55]); //55 - Reject Description (char:100) [Used by the Network to describe why a bill was rejected, if applicable]

                //TODO: do we need to check for existing records/ dups?

                //Let's look for matches
                //first try billing provider
                try {
                    bltPracticeMaster myMatch = EDI_RetroAnalysis.findMatch(myRW.getBillingProviderTaxID(), myRW.getBillingProviderZip());
                    if (myMatch==null){
                        myMatch = EDI_RetroAnalysis.findMatch(myRW.getRenderingProviderTaxID(), myRW.getRenderingProviderZip());
                    }
                    if (myMatch==null){
                        //this means neither matched
                        myRW.setNIM_ProviderMatchFound(2);
                        myRW.setNIM_ProviderMatchAcceptsRetro(3); ;
                        myRW.setNIM_ProviderMatchPositiveSavings(3);
                        myRW.setNIM_AcceptClaim(3);
                    } else {
                        //this means a match was found
                        myRW.setNIM_ProviderMatchFound(1);
                        //TODO: how do we know this?
                        myRW.setNIM_ProviderMatchAcceptsRetro(3);

                        //let's find provider cost
                        Double providerCost = NIMUtils.getCPTPayPriceforPracticeMaster(myMatch, new CPTObject(myRW.getCPTCode(), myRW.getModifier1(), 1), myRW.getDateofService(), NIMUtils.PAYMENT_TYPE_WORKCOMP);
                        myRW.setNIM_ContractProviderCost(providerCost);
                        Double requiredMinDollars = 30.0;
                        Double requiredMinSavings = 10.0;
                        Double targetDollars = 150.0;
                        Double temp_Savings = myRW.getPayerAllowance() - providerCost;
                        System.out.println("Temp Savings: " + temp_Savings);
                        if (temp_Savings>(requiredMinDollars+requiredMinSavings)){
                            //we can save them money
                            System.out.println("We can save");
                            myRW.setNIM_ProviderMatchPositiveSavings(1);
                            Double actual_Savings = temp_Savings;
                            Double actual_Price = providerCost + targetDollars;
                            if (actual_Price < (myRW.getPayerAllowance() - requiredMinSavings)){
                                //we can save target
                            } else {
                                actual_Price = (myRW.getPayerAllowance() - requiredMinSavings);
                            }
                            myRW.setNetworkSavings( (myRW.getPayerAllowance() - actual_Price) ) ;
                            myRW.setNetworkAllowance(actual_Price);
                            myRW.setNIM_AcceptClaim(0);
                        } else {
                            //we can't save them money
                            myRW.setNIM_ProviderMatchPositiveSavings(2);
                            myRW.setNIM_AcceptClaim(0);
                            myRW.setNIM_AcceptClaim(3);
                        }
                    }
                } catch (Exception ee){
                    //error in matching likely due to multiple results
                    sNotes+="\n" + ee;
                }

                myRW.commitData();


            }  catch (Exception e){
                System.out.println("--->  General Error in while loop (" + e + ")");
            }

        }
        System.out.println("--->  Ending Analysis");
        return countLines;
    }

    public static double round(double num, int multipleOf) {
        return Math.floor((num +  (double)multipleOf / 2) / multipleOf) * multipleOf;
    }

    public static bltPracticeMaster findMatch (String tin, String zip) throws Exception{
        bltPracticeMaster myFind = null;
        searchDB2 mySS = new searchDB2();
        java.sql.ResultSet myRS = null;
        try {
            String mySQL = "select practiceid from tpracticemaster where regexp_replace(officefederaltaxid, '[^0-9a-zA-Z ]', '') = regexp_replace(?, '[^0-9a-zA-Z ]', '') and (left(billingzip,5) = left(?,5) OR left(officezip,5)= left(?,5))";
            db_PreparedStatementObject myDPSO = new db_PreparedStatementObject(mySQL);
            myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,tin));
            myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,zip));
            myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,zip));
            myRS = mySS.executePreparedStatement(myDPSO);
            int returnCount=0;
            while (myRS!=null&&myRS.next()) {
                returnCount++;
                myFind = new bltPracticeMaster(myRS.getInt("practiceid"));
            }
            if (returnCount==1){
                return myFind;
            } else if (returnCount>1){
                throw new Exception ("Multiple Records Returned");
            }
            mySS.closeAll();
        }
        catch(Exception e){
            mySS.closeAll();
        } finally{
            mySS.closeAll();
        }
        return null;

    }


}
