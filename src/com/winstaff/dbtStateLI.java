

package com.winstaff;

import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public class dbtStateLI extends Object implements InttStateLI
{

        db_NewBase    dbnbDB;

    public dbtStateLI()
    {
        dbnbDB = new db_NewBase( "tStateLI", "StateID" );

    }    // End of default constructor

    public dbtStateLI( Integer iNewID )
    {
        dbnbDB = new db_NewBase( "tStateLI", "StateID" );
        setUniqueID( iNewID );
    }    // End of Constructor knowing an ID

    public void commitData() throws SQLException
    {
            //no longer trapping/ignoring errors 2011-03-17
            dbnbDB.commitData();
    }    // End of commitData()

    public void commitDataForced() throws SQLException
    {
        dbnbDB.commitDataForced();
    }    // End of commitData()

    public Integer getUniqueID()
    {
        return dbnbDB.getUniqueID();
    }    // End of getUniqueID()

    public void setUniqueID( Integer iNewID )
    {
        dbnbDB.setUniqueID( iNewID );
    }    // End of setUniqueID()

    public void setModifyComments( String sNew )
    {
        dbnbDB.setModifyComments( sNew );
    }    // End of setUniqueID()


    public void setStateID(Integer newValue)
    {
                dbnbDB.setFieldData( "StateID", newValue.toString() );
    }

    public Integer getStateID()
    {
        String           sValue = dbnbDB.getFieldData( "StateID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setField(String fieldN, Object fieldV)
    {DebugLogger.println("setField Method not implemented yet in class!");}

    public String getFieldType(String fieldN)
    {return null;}


    public void setShortState(String newValue)
    {
                dbnbDB.setFieldData( "ShortState", newValue.toString() );
    }

    public String getShortState()
    {
        String           sValue = dbnbDB.getFieldData( "ShortState" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setLongState(String newValue)
    {
                dbnbDB.setFieldData( "LongState", newValue.toString() );
    }

    public String getLongState()
    {
        String           sValue = dbnbDB.getFieldData( "LongState" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

}    // End of bltStateLI class definition
