

package com.winstaff;

import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public interface InttScheduledAppointmentStatusLI extends dbTableInterface
{

    public void setScheduledAppointmentStatusID(Integer newValue);
    public Integer getScheduledAppointmentStatusID();
    public void setStatusShort(String newValue);
    public String getStatusShort();
    public void setStatusLong(String newValue);
    public String getStatusLong();
}    // End of bltScheduledAppointmentStatusLI class definition
