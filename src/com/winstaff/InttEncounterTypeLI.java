

package com.winstaff;

import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public interface InttEncounterTypeLI extends dbTableInterface
{

    public void setEncounterTypeID(Integer newValue);
    public Integer getEncounterTypeID();
    public void setEncounterTypeShort(String newValue);
    public String getEncounterTypeShort();
    public void setEncounterTypeLong(String newValue);
    public String getEncounterTypeLong();
}    // End of bltEncounterTypeLI class definition
