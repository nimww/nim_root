

package com.winstaff;

import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public class dbtEmailTransaction extends Object implements InttEmailTransaction
{

        db_NewBase    dbnbDB;

    public dbtEmailTransaction()
    {
        dbnbDB = new db_NewBase( "tEmailTransaction", "EmailTransactionID" );

    }    // End of default constructor

    public dbtEmailTransaction( Integer iNewID )
    {
        dbnbDB = new db_NewBase( "tEmailTransaction", "EmailTransactionID" );
        setUniqueID( iNewID );
    }    // End of Constructor knowing an ID

    public void commitData() throws SQLException
    {
            //no longer trapping/ignoring errors 2011-03-17
            dbnbDB.commitData();
    }    // End of commitData()

    public void commitDataForced() throws SQLException
    {
        dbnbDB.commitDataForced();
    }    // End of commitData()

    public Integer getUniqueID()
    {
        return dbnbDB.getUniqueID();
    }    // End of getUniqueID()

    public void setUniqueID( Integer iNewID )
    {
        dbnbDB.setUniqueID( iNewID );
    }    // End of setUniqueID()

    public void setModifyComments( String sNew )
    {
        dbnbDB.setModifyComments( sNew );
    }    // End of setUniqueID()


    public void setEmailTransactionID(Integer newValue)
    {
                dbnbDB.setFieldData( "EmailTransactionID", newValue.toString() );
    }

    public Integer getEmailTransactionID()
    {
        String           sValue = dbnbDB.getFieldData( "EmailTransactionID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setField(String fieldN, Object fieldV)
    {DebugLogger.println("setField Method not implemented yet in classEmailTransaction!");}

    public String getFieldType(String fieldN)
    {return null;}


    public void setUniqueCreateDate(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "UniqueCreateDate", formatter.format( newValue ) );
    }

    public Date getUniqueCreateDate()
    {
        String           sValue = dbnbDB.getFieldData( "UniqueCreateDate" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setUniqueModifyDate(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "UniqueModifyDate", formatter.format( newValue ) );
    }

    public Date getUniqueModifyDate()
    {
        String           sValue = dbnbDB.getFieldData( "UniqueModifyDate" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setUniqueModifyComments(String newValue)
    {
                dbnbDB.setFieldData( "UniqueModifyComments", newValue.toString() );
    }

    public String getUniqueModifyComments()
    {
        String           sValue = dbnbDB.getFieldData( "UniqueModifyComments" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setTransactionDate(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "TransactionDate", formatter.format( newValue ) );
    }

    public Date getTransactionDate()
    {
        String           sValue = dbnbDB.getFieldData( "TransactionDate" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setActionID(Integer newValue)
    {
                dbnbDB.setFieldData( "ActionID", newValue.toString() );
    }

    public Integer getActionID()
    {
        String           sValue = dbnbDB.getFieldData( "ActionID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setEmailTo(String newValue)
    {
                dbnbDB.setFieldData( "EmailTo", newValue.toString() );
    }

    public String getEmailTo()
    {
        String           sValue = dbnbDB.getFieldData( "EmailTo" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setEmailFrom(String newValue)
    {
                dbnbDB.setFieldData( "EmailFrom", newValue.toString() );
    }

    public String getEmailFrom()
    {
        String           sValue = dbnbDB.getFieldData( "EmailFrom" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setEmailBody(String newValue)
    {
                dbnbDB.setFieldData( "EmailBody", newValue.toString() );
    }

    public String getEmailBody()
    {
        String           sValue = dbnbDB.getFieldData( "EmailBody" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setEmailBodyType(String newValue)
    {
                dbnbDB.setFieldData( "EmailBodyType", newValue.toString() );
    }

    public String getEmailBodyType()
    {
        String           sValue = dbnbDB.getFieldData( "EmailBodyType" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setEmailSubject(String newValue)
    {
                dbnbDB.setFieldData( "EmailSubject", newValue.toString() );
    }

    public String getEmailSubject()
    {
        String           sValue = dbnbDB.getFieldData( "EmailSubject" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setEmailImportance(Integer newValue)
    {
                dbnbDB.setFieldData( "EmailImportance", newValue.toString() );
    }

    public Integer getEmailImportance()
    {
        String           sValue = dbnbDB.getFieldData( "EmailImportance" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setComments(String newValue)
    {
                dbnbDB.setFieldData( "Comments", newValue.toString() );
    }

    public String getComments()
    {
        String           sValue = dbnbDB.getFieldData( "Comments" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setVirtualAttachment(String newValue)
    {
                dbnbDB.setFieldData( "VirtualAttachment", newValue.toString() );
    }

    public String getVirtualAttachment()
    {
        String           sValue = dbnbDB.getFieldData( "VirtualAttachment" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setEmailCC1(String newValue)
    {
                dbnbDB.setFieldData( "EmailCC1", newValue.toString() );
    }

    public String getEmailCC1()
    {
        String           sValue = dbnbDB.getFieldData( "EmailCC1" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setEmailCC2(String newValue)
    {
                dbnbDB.setFieldData( "EmailCC2", newValue.toString() );
    }

    public String getEmailCC2()
    {
        String           sValue = dbnbDB.getFieldData( "EmailCC2" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setEmailCC3(String newValue)
    {
                dbnbDB.setFieldData( "EmailCC3", newValue.toString() );
    }

    public String getEmailCC3()
    {
        String           sValue = dbnbDB.getFieldData( "EmailCC3" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setEmailCC4(String newValue)
    {
                dbnbDB.setFieldData( "EmailCC4", newValue.toString() );
    }

    public String getEmailCC4()
    {
        String           sValue = dbnbDB.getFieldData( "EmailCC4" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setEmailCC5(String newValue)
    {
                dbnbDB.setFieldData( "EmailCC5", newValue.toString() );
    }

    public String getEmailCC5()
    {
        String           sValue = dbnbDB.getFieldData( "EmailCC5" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

}    // End of bltEmailTransaction class definition
