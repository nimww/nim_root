package com.winstaff;

import java.io.DataOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;

public class CampusGardenFix {

	/**
	 * @param args
	 * @throws SQLException 
	 * @throws IOException 
	 */
	public static void main(String[] args) throws SQLException, IOException {
		ArrayList<String> uploadFileList = new ArrayList<String>();
		
		searchDB2 conn = new searchDB2();
		
		String query = "select \"EncounterID\" e from \"vMQ4_Encounter_NoVoid_BP\" where \"PracticeID\" = 7744 and to_char(\"Appointment_AppointmentTime\", 'yyyy-mm-dd') = to_char(now(), 'yyyy-mm-dd')";
		
		java.sql.ResultSet rs = conn.executeStatement(query);
		java.util.ArrayList<Integer> list = new java.util.ArrayList<Integer>();
		
		while(rs.next()){
			list.add(rs.getInt("e"));
		}
		
		if (list.size() != 0){
			//emailType_V3 getAlertEmail( NIM3_EncounterObject2 myEO2, String sFlag, String appendText, boolean custom)
			for(int i : list){
				emailType_V3 e = NIMUtils.getAlertEmail(i, "fax_sp_ic");
				
				String sFileName = "/acclaimrad/notice_"+i+".txt";
				uploadFileList.add(sFileName);
				
	            DataOutputStream out = new DataOutputStream( new FileOutputStream( sFileName ) );;
	            out.writeBytes( e.getBody() );
	            out.flush();
			}
			
		}
		uploadFile(uploadFileList);
		
		if(uploadFileList.size()>0){
			String theBody = "Notice went out to Campus Garden FTP for the following encounters:\n";
			
			for(String s : uploadFileList){
				theBody += s+"\n";
			}
			theBody +="\n-Administrator";
			email( "netdev.managers@nextimagemedical.com",  "support@nextimagemedical.com",  "Campus Garden Notice",  theBody);
			email( "scheduling.managers@nextimagemedical.com",  "support@nextimagemedical.com",  "Campus Garden Notice",  theBody);
			
			theBody = "Attention:\n\nNew notices have been delivered to the NextImage / Campus Garden FTP. \n\nThank you, \n\nNextImage Medical";
			
			email( "Wendy@acclaimrad.com",  "support@nextimagemedical.com",  "Campus Garden Notice",  theBody);
			email( "anelson@acclaimrad.com",  "support@nextimagemedical.com",  "Campus Garden Notice",  theBody);
			email( "Selene@acclaimrad.com",  "support@nextimagemedical.com",  "Campus Garden Notice",  theBody);
			email( "kathryn@acclaimrad.com",  "support@nextimagemedical.com",  "Campus Garden Notice",  theBody);
			email( "po.le@nextimagemedical.com",  "support@nextimagemedical.com",  "Campus Garden Notice",  theBody);
		}
		
	}
	
	private static void email(String emailTo, String emailFrom, String theSubject, String theBody) throws SQLException{
		bltEmailTransaction email = new bltEmailTransaction();
		email.setEmailTo(emailTo);
		email.setEmailFrom(emailFrom);
		email.setEmailSubject(theSubject);
		email.setEmailBody(theBody);
		email.setEmailBodyType(emailType_V3.PLAIN_TYPE);
		email.setTransactionDate(new java.util.Date());
		email.commitData();		
	}
	
	private static void uploadFile(List<String> src) {

		String remoteDir = "/out/";
		String host = "173.204.89.27";
		String user = "acclaimrad";
		String password = "edEn8tmub5QF8pT2r";

		JSch jsch = new JSch();
		Session session = null;
		try {
			session = jsch.getSession(user, host, 22);
			session.setConfig("StrictHostKeyChecking", "no");
			session.setPassword(password);
			session.connect();

			Channel channel = session.openChannel("sftp");
			channel.connect();
			ChannelSftp sftpChannel = (ChannelSftp) channel;
			
			for (String s : src){
				sftpChannel.put(s, remoteDir + s.replaceAll("/acclaimrad/", ""));
			}

			sftpChannel.exit();
			session.disconnect();
		} catch (JSchException e) {
			System.out.println("JSchException:");
			e.printStackTrace(); // To change body of catch statement use File |
			// Settings | File Templates.
		} catch (SftpException e) {
			System.out.println("SftpException:");
			e.printStackTrace();
		} finally{
			
		}

	}

}
