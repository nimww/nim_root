package com.winstaff;
/*
 * ListElement.java
 *
 * Created on April 17, 2001, 1:40 PM
 */


/**
 *
 * @author  Michael Million
 * @version 1.00
 */
public class ListElement extends Object {

    private Integer     iUniqueID;
    private Object      oListObject;
    
    /** Creates new ListElement */
    public ListElement ()
    {
        init( new Integer( "0" ), null );
    }   // End of default constructor

    public ListElement ( Integer iUniqueID, Object oValue)
    {
        init( iUniqueID, oValue );
    }   // End of constructor

    public Integer getUniqueID ()
    {
        return iUniqueID;
    }   // End of getUniqueID()
    
    public Object getObject ()
    {
        return oListObject;
    }   // End of getObject()

    /** stringize this object
    * @return String Value
    */     
    public String toString()
    {
        return "{" + getUniqueID().toString() + ", " + getObject().toString() + "}";
    }   // End of toString()
    
    private void init ( Integer iUniqueID, Object oValue )
    {
        this.iUniqueID      = iUniqueID;
        this.oListObject    = oValue;
    }
}   // End of ListElement class definition
