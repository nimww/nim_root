
package com.winstaff;
/*
 * bltNIM3_UserPracticeLU_List_LU_UserID.java
 *
 * Created: Wed Jul 28 15:43:30 PDT 2010
 */



import java.sql.*;




/**
 *
 * @author  Scott Ellis via listClassGenCode
 * @version 2.5
 */
public class bltNIM3_UserPracticeLU_List_LU_UserID extends db_BaseRelationTable
{
    /** Default constructor
    */    
    public bltNIM3_UserPracticeLU_List_LU_UserID ( Integer iUserID )
    {
        super( "tNIM3_UserPracticeLU", "LookupID", "LookupID", "UserID", iUserID );
    }   // End of bltNIM3_UserPracticeLU_List_LU_UserID()
    

    public void setField(String fieldN, Object fieldV)
    {DebugLogger.println("setField Method not implemented yet in classbltNIM3_UserPracticeLU_List_LU_UserID!");}

    public String getFieldType(String fieldN)
    {return null;}

    public bltNIM3_UserPracticeLU_List_LU_UserID ( Integer iUserID, String extraWhere, String OrderBy )
    {
        super( "tNIM3_UserPracticeLU", "LookupID", "LookupID", "UserID", iUserID, extraWhere,OrderBy );
    }   // End of bltNIM3_UserPracticeLU_List_LU_UserID()
     /** Method pull information from the result set and load into class attributes.
    * @param sFieldList Comma delimited list of fields for which data has been retrieved.
    * @param rs Result set from which to pull the data from.
    * @throws SQLException Passes on SQLException
    */      
    protected void transferData( ResultSet rs ) throws SQLException
    {
        while ( rs.next()  )
        {
            Integer         iValue              = new Integer( rs.getString( sLinkFieldName ) );
            Integer         iLinkRecordID       = new Integer( rs.getString( sUniqueIDFieldName ) );
            Object          oReferenceObject;
            ListElement     leNewElement;
            
            oReferenceObject    = new bltNIM3_UserPracticeLU( iValue );
            
            leNewElement        = new ListElement( iLinkRecordID, oReferenceObject );
            
            vList.add( leNewElement );

        }
    }   // End of transferData()
}   // End of bltNIM3_UserPracticeLU_List_LU_UserID class

