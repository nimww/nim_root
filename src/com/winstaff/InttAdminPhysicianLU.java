package com.winstaff;


import com.winstaff.dbtAdminPhysicianLU;
import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public interface InttAdminPhysicianLU extends dbTableInterface
{

    public void setLookupID(Integer newValue);
    public Integer getLookupID();
    public void setUniqueCreateDate(Date newValue);
    public Date getUniqueCreateDate();
    public void setUniqueModifyDate(Date newValue);
    public Date getUniqueModifyDate();
    public void setUniqueModifyComments(String newValue);
    public String getUniqueModifyComments();
    public void setAdminID(Integer newValue);
    public Integer getAdminID();
    public void setPhysicianID(Integer newValue);
    public Integer getPhysicianID();
    public void setComments(String newValue);
    public String getComments();
}    // End of bltAdminPhysicianLU class definition
