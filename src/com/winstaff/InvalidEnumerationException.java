package com.winstaff;
public class InvalidEnumerationException extends Exception
{

    public InvalidEnumerationException()
    {
        super();
    }

    public InvalidEnumerationException(String ee)
    {
        super(ee);
    }

}
