

package com.winstaff;

import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public class dbtAdminPracticeRelationshipTypeLI extends Object implements InttAdminPracticeRelationshipTypeLI
{

        db_NewBase    dbnbDB;

    public dbtAdminPracticeRelationshipTypeLI()
    {
        dbnbDB = new db_NewBase( "tAdminPracticeRelationshipTypeLI", "AdminPracticeRelationshipTypeID" );

    }    // End of default constructor

    public dbtAdminPracticeRelationshipTypeLI( Integer iNewID )
    {
        dbnbDB = new db_NewBase( "tAdminPracticeRelationshipTypeLI", "AdminPracticeRelationshipTypeID" );
        setUniqueID( iNewID );
    }    // End of Constructor knowing an ID

    public void commitData() throws SQLException
    {
            //no longer trapping/ignoring errors 2011-03-17
            dbnbDB.commitData();
    }    // End of commitData()

    public void commitDataForced() throws SQLException
    {
        dbnbDB.commitDataForced();
    }    // End of commitData()

    public Integer getUniqueID()
    {
        return dbnbDB.getUniqueID();
    }    // End of getUniqueID()

    public void setUniqueID( Integer iNewID )
    {
        dbnbDB.setUniqueID( iNewID );
    }    // End of setUniqueID()

    public void setModifyComments( String sNew )
    {
        dbnbDB.setModifyComments( sNew );
    }    // End of setUniqueID()


    public void setAdminPracticeRelationshipTypeID(Integer newValue)
    {
                dbnbDB.setFieldData( "AdminPracticeRelationshipTypeID", newValue.toString() );
    }

    public Integer getAdminPracticeRelationshipTypeID()
    {
        String           sValue = dbnbDB.getFieldData( "AdminPracticeRelationshipTypeID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setField(String fieldN, Object fieldV)
    {DebugLogger.println("setField Method not implemented yet in class!");}

    public String getFieldType(String fieldN)
    {return null;}


    public void setDescriptionLong(String newValue)
    {
                dbnbDB.setFieldData( "DescriptionLong", newValue.toString() );
    }

    public String getDescriptionLong()
    {
        String           sValue = dbnbDB.getFieldData( "DescriptionLong" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

}    // End of bltAdminPracticeRelationshipTypeLI class definition
