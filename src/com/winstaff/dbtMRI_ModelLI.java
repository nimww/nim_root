

package com.winstaff;

import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public class dbtMRI_ModelLI extends Object implements InttMRI_ModelLI
{

        db_NewBase    dbnbDB;

    public dbtMRI_ModelLI()
    {
        dbnbDB = new db_NewBase( "tMRI_ModelLI", "MRI_ModelID" );

    }    // End of default constructor

    public dbtMRI_ModelLI( Integer iNewID )
    {
        dbnbDB = new db_NewBase( "tMRI_ModelLI", "MRI_ModelID" );
        setUniqueID( iNewID );
    }    // End of Constructor knowing an ID

    public void commitData() throws SQLException
    {
            //no longer trapping/ignoring errors 2011-03-17
            dbnbDB.commitData();
    }    // End of commitData()

    public void commitDataForced() throws SQLException
    {
        dbnbDB.commitDataForced();
    }    // End of commitData()

    public Integer getUniqueID()
    {
        return dbnbDB.getUniqueID();
    }    // End of getUniqueID()

    public void setUniqueID( Integer iNewID )
    {
        dbnbDB.setUniqueID( iNewID );
    }    // End of setUniqueID()

    public void setModifyComments( String sNew )
    {
        dbnbDB.setModifyComments( sNew );
    }    // End of setUniqueID()


    public void setMRI_ModelID(Integer newValue)
    {
                dbnbDB.setFieldData( "MRI_ModelID", newValue.toString() );
    }

    public Integer getMRI_ModelID()
    {
        String           sValue = dbnbDB.getFieldData( "MRI_ModelID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setField(String fieldN, Object fieldV)
    {DebugLogger.println("setField Method not implemented yet in class!");}

    public String getFieldType(String fieldN)
    {return null;}


    public void setDescriptionLong(String newValue)
    {
                dbnbDB.setFieldData( "DescriptionLong", newValue.toString() );
    }

    public String getDescriptionLong()
    {
        String           sValue = dbnbDB.getFieldData( "DescriptionLong" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setIsOpen(Integer newValue)
    {
                dbnbDB.setFieldData( "IsOpen", newValue.toString() );
    }

    public Integer getIsOpen()
    {
        String           sValue = dbnbDB.getFieldData( "IsOpen" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setAccomodatesClaus(Integer newValue)
    {
                dbnbDB.setFieldData( "AccomodatesClaus", newValue.toString() );
    }

    public Integer getAccomodatesClaus()
    {
        String           sValue = dbnbDB.getFieldData( "AccomodatesClaus" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setTeslaStrength(String newValue)
    {
                dbnbDB.setFieldData( "TeslaStrength", newValue.toString() );
    }

    public String getTeslaStrength()
    {
        String           sValue = dbnbDB.getFieldData( "TeslaStrength" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setManufacturer(String newValue)
    {
                dbnbDB.setFieldData( "Manufacturer", newValue.toString() );
    }

    public String getManufacturer()
    {
        String           sValue = dbnbDB.getFieldData( "Manufacturer" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setModel(String newValue)
    {
                dbnbDB.setFieldData( "Model", newValue.toString() );
    }

    public String getModel()
    {
        String           sValue = dbnbDB.getFieldData( "Model" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setClassLevel(Integer newValue)
    {
                dbnbDB.setFieldData( "ClassLevel", newValue.toString() );
    }

    public Integer getClassLevel()
    {
        String           sValue = dbnbDB.getFieldData( "ClassLevel" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

}    // End of bltMRI_ModelLI class definition
