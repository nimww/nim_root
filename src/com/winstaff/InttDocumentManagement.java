package com.winstaff;


import com.winstaff.dbtDocumentManagement;
import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public interface InttDocumentManagement extends dbTableInterface
{

    public void setDocumentID(Integer newValue);
    public Integer getDocumentID();
    public void setUniqueCreateDate(Date newValue);
    public Date getUniqueCreateDate();
    public void setUniqueModifyDate(Date newValue);
    public Date getUniqueModifyDate();
    public void setUniqueModifyComments(String newValue);
    public String getUniqueModifyComments();
    public void setPhysicianID(Integer newValue);
    public Integer getPhysicianID();
    public void setDocumentTypeID(Integer newValue);
    public Integer getDocumentTypeID();
    public void setDocumentName(String newValue);
    public String getDocumentName();
    public void setDocumentFileName(String newValue);
    public String getDocumentFileName();
    public void setDateReceived(Date newValue);
    public Date getDateReceived();
    public void setDateOfExpiration(Date newValue);
    public Date getDateOfExpiration();
    public void setDateAlertSent(Date newValue);
    public Date getDateAlertSent();
    public void setArchived(Integer newValue);
    public Integer getArchived();
    public void setComments(String newValue);
    public String getComments();
}    // End of bltDocumentManagement class definition
