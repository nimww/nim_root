

package com.winstaff;

import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public class dbtStandardAccessTypeLI extends Object implements InttStandardAccessTypeLI
{

        db_NewBase    dbnbDB;

    public dbtStandardAccessTypeLI()
    {
        dbnbDB = new db_NewBase( "tStandardAccessTypeLI", "StandardAccessTypeID" );

    }    // End of default constructor

    public dbtStandardAccessTypeLI( Integer iNewID )
    {
        dbnbDB = new db_NewBase( "tStandardAccessTypeLI", "StandardAccessTypeID" );
        setUniqueID( iNewID );
    }    // End of Constructor knowing an ID

    public void commitData() throws SQLException
    {
            //no longer trapping/ignoring errors 2011-03-17
            dbnbDB.commitData();
    }    // End of commitData()

    public void commitDataForced() throws SQLException
    {
        dbnbDB.commitDataForced();
    }    // End of commitData()

    public Integer getUniqueID()
    {
        return dbnbDB.getUniqueID();
    }    // End of getUniqueID()

    public void setUniqueID( Integer iNewID )
    {
        dbnbDB.setUniqueID( iNewID );
    }    // End of setUniqueID()

    public void setModifyComments( String sNew )
    {
        dbnbDB.setModifyComments( sNew );
    }    // End of setUniqueID()


    public void setStandardAccessTypeID(Integer newValue)
    {
                dbnbDB.setFieldData( "StandardAccessTypeID", newValue.toString() );
    }

    public Integer getStandardAccessTypeID()
    {
        String           sValue = dbnbDB.getFieldData( "StandardAccessTypeID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setField(String fieldN, Object fieldV)
    {DebugLogger.println("setField Method not implemented yet in class!");}

    public String getFieldType(String fieldN)
    {return null;}


    public void setStandardAccessTypeLong(String newValue)
    {
                dbnbDB.setFieldData( "StandardAccessTypeLong", newValue.toString() );
    }

    public String getStandardAccessTypeLong()
    {
        String           sValue = dbnbDB.getFieldData( "StandardAccessTypeLong" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

}    // End of bltStandardAccessTypeLI class definition
