package com.winstaff;

/**
 * Created by IntelliJ IDEA.
 * User: ste
 * Date: 2/25/12
 * Time: 10:43 AM
 * To change this template use File | Settings | File Templates.
 */
public class NIMUtilsWorkflow {

    public static NIM3_BooleanAnalysisResultsObject getCaseWarnings (bltNIM3_CaseAccount NCA, String linkPrefix){
        NIM3_BooleanAnalysisResultsObject myReturn_BARO = new NIM3_BooleanAnalysisResultsObject();
        //check for bad Claim Number
        if (NCA.getCaseClaimNumber().toUpperCase().indexOf("TBD".toUpperCase())>=0){
            myReturn_BARO.setResult(true); //result is true of there is a warning
            myReturn_BARO.addArrayMessage("Claim Number contains TBD");
        }
        // only analyze if TBD is not found otherwise too many matches could occur
        if (!myReturn_BARO.getResult()){
            //check for duplicate Claim number
            //check for similar claim/last
            bltNIM3_PayerMaster myPayer = new bltNIM3_PayerMaster(NCA.getPayerID());
            searchDB2 mySSAct = new searchDB2();
            db_PreparedStatementObject myDPSO = null;
            java.sql.ResultSet myRSAct = null;
            try {
                String mySQL55 = "None";
                //mySQL55 = "SELECT \"CaseClaimNumber\", \"PatientFirstName\", \"PatientLastName\" from \"vMQ4_Encounter_NoVoid_BP\" where ( \"CaseID\"<>? AND \"Parent_PayerID\" = ? )  AND (  upper(\"PatientLastName\") % upper(?) AND upper(\"CaseClaimNumber\") % upper(?) )   GROUP BY \"CaseClaimNumber\", \"PatientFirstName\", \"PatientLastName\"";
                //mySQL55 = "select caseclaimnumber, patientfirstname, patientlastname from tnim3_caseaccount inner join tnim3_referral on tnim3_referral.caseid = tnim3_caseaccount.caseid inner join tnim3_encounter on tnim3_encounter.referralid = tnim3_referral.referralid inner join tnim3_payermaster on tnim3_caseaccount.payerid = tnim3_payermaster.payerid where ( tnim3_caseaccount.caseid<>? AND tnim3_payermaster.parentpayerid = ? AND tnim3_encounter.encounterstatusid in ("+NIMUtils.getSQL_Encounters_Active()+") )  AND (  upper(PatientLastName) % upper(?) AND upper(CaseClaimNumber) % upper(?) ) GROUP BY CaseClaimNumber, patientFirstName, PatientLastName";
                mySQL55 = "select caseclaimnumber, patientfirstname, patientlastname, patientZIP from tnim3_caseaccount inner join tnim3_referral on tnim3_referral.caseid = tnim3_caseaccount.caseid inner join tnim3_encounter on tnim3_encounter.referralid = tnim3_referral.referralid inner join tnim3_payermaster on tnim3_caseaccount.payerid = tnim3_payermaster.payerid where ( tnim3_caseaccount.caseid<>? AND tnim3_payermaster.parentpayerid = ? AND tnim3_encounter.encounterstatusid in ("+NIMUtils.getSQL_Encounters_Active()+") )  AND (  dmetaphone(upper(PatientLastName)) = dmetaphone(upper(?)) AND dmetaphone(upper(PatientFirstName)) = dmetaphone(upper(?)) AND levenshtein(upper(CaseClaimNumber), upper(?))<4  ) GROUP BY CaseClaimNumber, patientFirstName, PatientLastName, patientzip";
                myDPSO = new db_PreparedStatementObject(mySQL55);
                myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.INT_TYPE,NCA.getCaseID().toString()));
                myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.INT_TYPE,myPayer.getParentPayerID().toString()));
                myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,NCA.getPatientLastName()));
                myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,NCA.getPatientFirstName()));
                myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,NCA.getCaseClaimNumber()));
                myRSAct = mySSAct.executePreparedStatement(myDPSO);
            } catch(Exception e) {
                DebugLogger.println("ResultsSet:" + e);
            }
        	try {
	            if (myRSAct!=null) {
                    while (myRSAct.next()) {
                        myReturn_BARO.setResult(true); //result is true of there is a warning
                        String tempMSG = "";
                        if (NCA.getPatientZIP().equalsIgnoreCase(myRSAct.getString("patientZIP"))){
                            tempMSG += " Likely Dup: ";
                        } else {
                            tempMSG += " Possible Dup: ";
                        }
                        if (linkPrefix!=null && !linkPrefix.equalsIgnoreCase("") ) {
                            tempMSG +="<a href=\"" + linkPrefix + myRSAct.getString("CaseClaimNumber")  + "\">" + myRSAct.getString("CaseClaimNumber") + "</a> [" +  myRSAct.getString("PatientFirstName")  + " " +  myRSAct.getString("PatientLastName")  + "]" ;
                        } else {
                            tempMSG += myRSAct.getString("CaseClaimNumber") + " [" +  myRSAct.getString("PatientFirstName")  + " " +  myRSAct.getString("PatientLastName")  + "]" ;
                        }
                        myReturn_BARO.addArrayMessage(tempMSG);
                    }
                }
            } catch (java.sql.SQLException e2){

            }
        }
        return myReturn_BARO;

    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    

    
    
    
    
    
    
    
    
    

}
