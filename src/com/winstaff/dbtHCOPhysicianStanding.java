

package com.winstaff;

import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public class dbtHCOPhysicianStanding extends Object implements InttHCOPhysicianStanding
{

        db_NewBase    dbnbDB;

    public dbtHCOPhysicianStanding()
    {
        dbnbDB = new db_NewBase( "tHCOPhysicianStanding", "LookupID" );

    }    // End of default constructor

    public dbtHCOPhysicianStanding( Integer iNewID )
    {
        dbnbDB = new db_NewBase( "tHCOPhysicianStanding", "LookupID" );
        setUniqueID( iNewID );
    }    // End of Constructor knowing an ID

    public void commitData() throws SQLException
    {
            //no longer trapping/ignoring errors 2011-03-17
            dbnbDB.commitData();
    }    // End of commitData()

    public void commitDataForced() throws SQLException
    {
        dbnbDB.commitDataForced();
    }    // End of commitData()

    public Integer getUniqueID()
    {
        return dbnbDB.getUniqueID();
    }    // End of getUniqueID()

    public void setUniqueID( Integer iNewID )
    {
        dbnbDB.setUniqueID( iNewID );
    }    // End of setUniqueID()

    public void setModifyComments( String sNew )
    {
        dbnbDB.setModifyComments( sNew );
    }    // End of setUniqueID()


    public void setLookupID(Integer newValue)
    {
                dbnbDB.setFieldData( "LookupID", newValue.toString() );
    }

    public Integer getLookupID()
    {
        String           sValue = dbnbDB.getFieldData( "LookupID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setField(String fieldN, Object fieldV)
    {DebugLogger.println("setField Method not implemented yet in classHCOPhysicianStanding!");}

    public String getFieldType(String fieldN)
    {return null;}


    public void setUniqueCreateDate(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "UniqueCreateDate", formatter.format( newValue ) );
    }

    public Date getUniqueCreateDate()
    {
        String           sValue = dbnbDB.getFieldData( "UniqueCreateDate" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setUniqueModifyDate(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "UniqueModifyDate", formatter.format( newValue ) );
    }

    public Date getUniqueModifyDate()
    {
        String           sValue = dbnbDB.getFieldData( "UniqueModifyDate" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setUniqueModifyComments(String newValue)
    {
                dbnbDB.setFieldData( "UniqueModifyComments", newValue.toString() );
    }

    public String getUniqueModifyComments()
    {
        String           sValue = dbnbDB.getFieldData( "UniqueModifyComments" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setHCOID(Integer newValue)
    {
                dbnbDB.setFieldData( "HCOID", newValue.toString() );
    }

    public Integer getHCOID()
    {
        String           sValue = dbnbDB.getFieldData( "HCOID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setComments(String newValue)
    {
                dbnbDB.setFieldData( "Comments", newValue.toString() );
    }

    public String getComments()
    {
        String           sValue = dbnbDB.getFieldData( "Comments" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setFirstName(String newValue)
    {
                dbnbDB.setFieldData( "FirstName", newValue.toString() );
    }

    public String getFirstName()
    {
        String           sValue = dbnbDB.getFieldData( "FirstName" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setMiddleName(String newValue)
    {
                dbnbDB.setFieldData( "MiddleName", newValue.toString() );
    }

    public String getMiddleName()
    {
        String           sValue = dbnbDB.getFieldData( "MiddleName" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setLastName(String newValue)
    {
                dbnbDB.setFieldData( "LastName", newValue.toString() );
    }

    public String getLastName()
    {
        String           sValue = dbnbDB.getFieldData( "LastName" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setLicenseNumber(String newValue)
    {
                dbnbDB.setFieldData( "LicenseNumber", newValue.toString() );
    }

    public String getLicenseNumber()
    {
        String           sValue = dbnbDB.getFieldData( "LicenseNumber" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setLicenseState(String newValue)
    {
                dbnbDB.setFieldData( "LicenseState", newValue.toString() );
    }

    public String getLicenseState()
    {
        String           sValue = dbnbDB.getFieldData( "LicenseState" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setDepartment(String newValue)
    {
                dbnbDB.setFieldData( "Department", newValue.toString() );
    }

    public String getDepartment()
    {
        String           sValue = dbnbDB.getFieldData( "Department" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setFromDate(String newValue)
    {
                dbnbDB.setFieldData( "FromDate", newValue.toString() );
    }

    public String getFromDate()
    {
        String           sValue = dbnbDB.getFieldData( "FromDate" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setToDate(String newValue)
    {
                dbnbDB.setFieldData( "ToDate", newValue.toString() );
    }

    public String getToDate()
    {
        String           sValue = dbnbDB.getFieldData( "ToDate" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setStatus(String newValue)
    {
                dbnbDB.setFieldData( "Status", newValue.toString() );
    }

    public String getStatus()
    {
        String           sValue = dbnbDB.getFieldData( "Status" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setPriviledgeComments(String newValue)
    {
                dbnbDB.setFieldData( "PriviledgeComments", newValue.toString() );
    }

    public String getPriviledgeComments()
    {
        String           sValue = dbnbDB.getFieldData( "PriviledgeComments" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setIsArchived(Integer newValue)
    {
                dbnbDB.setFieldData( "IsArchived", newValue.toString() );
    }

    public Integer getIsArchived()
    {
        String           sValue = dbnbDB.getFieldData( "IsArchived" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setSpecialty(String newValue)
    {
                dbnbDB.setFieldData( "Specialty", newValue.toString() );
    }

    public String getSpecialty()
    {
        String           sValue = dbnbDB.getFieldData( "Specialty" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setSuffix(String newValue)
    {
                dbnbDB.setFieldData( "Suffix", newValue.toString() );
    }

    public String getSuffix()
    {
        String           sValue = dbnbDB.getFieldData( "Suffix" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setTitle(String newValue)
    {
                dbnbDB.setFieldData( "Title", newValue.toString() );
    }

    public String getTitle()
    {
        String           sValue = dbnbDB.getFieldData( "Title" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setPhotoFileName(String newValue)
    {
                dbnbDB.setFieldData( "PhotoFileName", newValue.toString() );
    }

    public String getPhotoFileName()
    {
        String           sValue = dbnbDB.getFieldData( "PhotoFileName" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setNPI(String newValue)
    {
                dbnbDB.setFieldData( "NPI", newValue.toString() );
    }

    public String getNPI()
    {
        String           sValue = dbnbDB.getFieldData( "NPI" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setFacilityName(String newValue)
    {
                dbnbDB.setFieldData( "FacilityName", newValue.toString() );
    }

    public String getFacilityName()
    {
        String           sValue = dbnbDB.getFieldData( "FacilityName" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setOrigDate(String newValue)
    {
                dbnbDB.setFieldData( "OrigDate", newValue.toString() );
    }

    public String getOrigDate()
    {
        String           sValue = dbnbDB.getFieldData( "OrigDate" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

}    // End of bltHCOPhysicianStanding class definition
