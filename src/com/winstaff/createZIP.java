package com.winstaff;
import java.util.Vector;
import java.awt.*;


public class createZIP extends java.lang.Object
{

	public createZIP()
	{
	}

	public void setFileList(java.util.Vector fileList)
	{
		this.fileList = fileList;
	}

	public java.util.Vector getFileList()
	{
		return this.fileList;
	}

	public void setInitialDirectory(String initialDirectory)
	{
			this.initialDirectory = initialDirectory;
	}

	public String getInitialDirectory()
	{
		return this.initialDirectory;
	}

	public void setOutputZIPFileName(String outputZIPFileName)
	{
			this.outputZIPFileName = outputZIPFileName;
	}

	public String getOutputZIPFileName()
	{
		return this.outputZIPFileName;
	}

    public void createZIP()
    {
        Vector myV = this.getFileList();
        String cmdLine = "pkzip "+this.getOutputZIPFileName()+".zip ";
        for (int i=0;i<myV.size(); i++)
        {
    		cmdLine += " " + (String) myV.elementAt(i) + " ";
	    }
        try
        {
            Process myProcess = Runtime.getRuntime().exec("cmd /c Cd" + this.getInitialDirectory() +" && " +cmdLine); 
            //java.io.BufferedReader myBR = new java.io.BufferedReader(new java.io.InputStreamReader(myProcess.getInputStream())); 
            //myBR.close();
            this.setOutputMessage("["+this.getOutputZIPFileName() + "] ZIP Successfully created");
        }
        catch (Exception e)
        {
            this.setOutputMessage("Error ZIP Creation Failed ["+e+"]");
        }
    }

	public void setOutputMessage(String outputMessage)
	{
			this.outputMessage = outputMessage;
	}

	public String getOutputMessage()
	{
		return this.outputMessage;
	}


	protected String outputMessage = "";	
	protected String outputZIPFileName = "";
	protected String initialDirectory = "";
	protected java.util.Vector fileList;

}
