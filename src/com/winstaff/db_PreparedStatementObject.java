package com.winstaff;

import java.util.Vector;

/**
 * Created by IntelliJ IDEA.
 * User: ste
 * Date: Jun 21, 2010
 * Time: 2:05:03 PM
 * To change this template use File | Settings | File Templates.
 */
public class db_PreparedStatementObject
{


    public db_PreparedStatementObject(String SQL)
    {
        this.SQL = SQL;
    }

    public String getSQL()
    {
        return SQL;
    }

    public void setSQL(String SQL) {
        this.SQL = SQL;
    }

    public Vector getSQLParameters() {
        return SQLParameters;
    }


    public void setSQLParameters(Vector SQLParameters) {
        this.SQLParameters = SQLParameters;
    }

    public void clearList()
    {
        SQLParameters.removeAllElements();
        SQLParameters.clear();
    }

    public void addSQLParameter (SQLParameterType SPT)
    {
        this.reset();
        if (hasNext())
        {
            SQLParameters.add(SPT);
        }
        else
        {
            this.SQLParameters = new Vector();
            SQLParameters.add(SPT);
        }
    }

    public SQLParameterType getNextElement()
    {
        if (hasNext())
        {
            SQLParameterType myVal = null;
            if (SQLParameters.size()>InternalCount)
            {
                myVal = (SQLParameterType) SQLParameters.elementAt(InternalCount);
                InternalCount++;
            }
            return myVal;
        }
        else
        {
            return null;
        }
    }

    public void reset()
    {
        InternalCount = 0;
    }

    public boolean hasNext()
    {
        if (SQLParameters==null)
        {
            return false;
        }
        else if (SQLParameters.size()>InternalCount)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    private int InternalCount = 0;
    private String SQL = "";
    private java.util.Vector SQLParameters = null;

}


