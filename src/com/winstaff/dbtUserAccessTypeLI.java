

package com.winstaff;

import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public class dbtUserAccessTypeLI extends Object implements InttUserAccessTypeLI
{

        db_NewBase    dbnbDB;

    public dbtUserAccessTypeLI()
    {
        dbnbDB = new db_NewBase( "tUserAccessTypeLI", "UserAccessTypeID" );

    }    // End of default constructor

    public dbtUserAccessTypeLI( Integer iNewID )
    {
        dbnbDB = new db_NewBase( "tUserAccessTypeLI", "UserAccessTypeID" );
        setUniqueID( iNewID );
    }    // End of Constructor knowing an ID

    public void commitData() throws SQLException
    {
            //no longer trapping/ignoring errors 2011-03-17
            dbnbDB.commitData();
    }    // End of commitData()

    public void commitDataForced() throws SQLException
    {
        dbnbDB.commitDataForced();
    }    // End of commitData()

    public Integer getUniqueID()
    {
        return dbnbDB.getUniqueID();
    }    // End of getUniqueID()

    public void setUniqueID( Integer iNewID )
    {
        dbnbDB.setUniqueID( iNewID );
    }    // End of setUniqueID()

    public void setModifyComments( String sNew )
    {
        dbnbDB.setModifyComments( sNew );
    }    // End of setUniqueID()


    public void setUserAccessTypeID(Integer newValue)
    {
                dbnbDB.setFieldData( "UserAccessTypeID", newValue.toString() );
    }

    public Integer getUserAccessTypeID()
    {
        String           sValue = dbnbDB.getFieldData( "UserAccessTypeID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setField(String fieldN, Object fieldV)
    {DebugLogger.println("setField Method not implemented yet in class!");}

    public String getFieldType(String fieldN)
    {return null;}


    public void setUserAccessTypeLong(String newValue)
    {
                dbnbDB.setFieldData( "UserAccessTypeLong", newValue.toString() );
    }

    public String getUserAccessTypeLong()
    {
        String           sValue = dbnbDB.getFieldData( "UserAccessTypeLong" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

}    // End of bltUserAccessTypeLI class definition
