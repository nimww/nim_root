

package com.winstaff;

import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public interface InttCommTrackTypeLI extends dbTableInterface
{

    public void setCommTrackTypeID(Integer newValue);
    public Integer getCommTrackTypeID();
    public void setCommTrackTypeShort(String newValue);
    public String getCommTrackTypeShort();
    public void setCommTrackTypeLong(String newValue);
    public String getCommTrackTypeLong();
}    // End of bltCommTrackTypeLI class definition
