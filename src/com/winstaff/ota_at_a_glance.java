package com.winstaff;

import java.sql.SQLException;

public class ota_at_a_glance{
	public static void email(String emailTo, String emailFrom, String theSubject, String theBody) throws SQLException{
		bltEmailTransaction et = new bltEmailTransaction();
		et.setEmailTo(emailTo);
		et.setEmailFrom(emailFrom);
		et.setEmailSubject(theSubject);
		et.setEmailBody(theBody);
		et.setEmailBodyType(emailType_V3.HTML_TYPE);
		et.setTransactionDate(new java.util.Date());
		et.commitData();
	}
	
	public static void main(String[]args) throws SQLException{
		String query = "SELECT * FROM \"ota_at_a_glance\"";
		String theBody = "";
		searchDB2 conn = new searchDB2();
		java.sql.ResultSet rs = conn.executeStatement(query);
		try{
			theBody += "<table border=2 style=\"font:Helvetica;border-collapse:collapse;\"><tr>";
			theBody += "<th align=\"left\" style=\"padding:2px 5px\">Scanpass</th>";
			theBody += "<th align=\"left\" style=\"padding:2px 5px\">Scheduler Name</th>";
			theBody += "<th align=\"left\" style=\"padding:2px 5px\">Courtesy Status</th>";
			theBody += "<th align=\"left\" style=\"padding:2px 5px\">Net-Dev Status</th>";
			theBody += "<th align=\"left\" style=\"padding:2px 5px\">Open Date</th>";
			theBody += "<th align=\"left\" style=\"padding:2px 5px\">Close Date</th>";
			theBody += "<th align=\"left\" style=\"padding:2px 5px\">Logged Modify Comments</th>";
			theBody += "<th align=\"left\" style=\"padding:2px 5px\">Patient Name</th>";
			theBody += "<th align=\"left\" style=\"padding:2px 5px\">Payer</th>";
			theBody += "<th align=\"left\" style=\"padding:2px 5px\">CPT Code</th>";
			theBody += "<th align=\"left\" style=\"padding:2px 5px\">Body Part</th>";
			theBody += "<th align=\"left\" style=\"padding:2px 5px\">Time In Net-Dev</th>";
			
			
			
			theBody += "</tr>";
			
			while(rs != null && rs.next()){
				theBody += "<tr>";
					theBody += "<td align=\"left\" style=\"padding:2px 5px\"><small>" + rs.getString("scanpass") + "</small></td>";
					theBody += "<td align=\"left\" style=\"padding:2px 5px\"><small>" + rs.getString("schedfirst") + " " +rs.getString("schedlast") +"</small></td>";
					theBody += "<td align=\"left\" style=\"padding:2px 5px\"><small>" + rs.getString("courtesy") + "</small></td>";
					theBody += "<td align=\"left\" style=\"padding:2px 5px\"><small>" + rs.getString("netdev_status") + "</small></td>";
					theBody += "<td align=\"left\" style=\"padding:2px 5px\"><small>" + rs.getString("opendate") + "</small></td>";
					theBody += "<td align=\"left\" style=\"padding:2px 5px\"><small>" + rs.getString("closedate") + "</small></td>";
					theBody += "<td align=\"left\" style=\"padding:2px 5px\"><small>" + rs.getString("comments") +  "</small></td>";
					theBody += "<td align=\"left\" style=\"padding:2px 5px\"><small>" + rs.getString("patientfirstname") + " " +rs.getString("patientlastname") +"</small></td>";
					theBody += "<td align=\"left\" style=\"padding:2px 5px\"><small>" + rs.getString("payername") + "</small></td>";
					theBody += "<td align=\"left\" style=\"padding:2px 5px\"><small>" + rs.getString("cpt") + "</small></td>";
					theBody += "<td align=\"left\" style=\"padding:2px 5px\"><small>" + rs.getString("body_part") +  "</small></td>";
					theBody += "<td align=\"left\" style=\"padding:2px 5px\"><small>" + rs.getString("days") + " days " + rs.getString("hours") + " hrs " + rs.getString("mins") + " min " +"</small></td>";
				theBody += "</tr>";
			}
			theBody += "</table> \n";
		}catch(Exception e){theBody = e.toString();}
		conn.closeAll();
		String theSubject = "OTA at a glance";
		email("otas.at.a.glance@nextimagemedical.com","support@nextimagemedical.com",theSubject,theBody);
		//System.out.print(theBody);
	}
}