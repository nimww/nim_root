

package com.winstaff;

import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public interface InttNIM3_BillingEntity extends dbTableInterface
{

    public void setBillingEntityID(Integer newValue);
    public Integer getBillingEntityID();
    public void setUniqueCreateDate(Date newValue);
    public Date getUniqueCreateDate();
    public void setUniqueModifyDate(Date newValue);
    public Date getUniqueModifyDate();
    public void setUniqueModifyComments(String newValue);
    public String getUniqueModifyComments();
    public void setEntityName(String newValue);
    public String getEntityName();
    public void setFeeScheduleRefID(Integer newValue);
    public Integer getFeeScheduleRefID();
    public void setFeePercentage(Double newValue);
    public Double getFeePercentage();
    public void setOverrideTypeID(Integer newValue);
    public Integer getOverrideTypeID();
    public void setOverrideScheduleRefID(Integer newValue);
    public Integer getOverrideScheduleRefID();
    public void setOverridePercentage(Double newValue);
    public Double getOverridePercentage();
}    // End of bltNIM3_BillingEntity class definition
