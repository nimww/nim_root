package com.winstaff;


import com.winstaff.dbtSupportTypeLI;
import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public interface InttSupportTypeLI extends dbTableInterface
{

    public void setSupportTypeID(Integer newValue);
    public Integer getSupportTypeID();
    public void setSupportTypeShort(String newValue);
    public String getSupportTypeShort();
    public void setSupportTypeLong(String newValue);
    public String getSupportTypeLong();
}    // End of bltSupportTypeLI class definition
