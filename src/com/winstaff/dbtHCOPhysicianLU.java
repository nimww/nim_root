

package com.winstaff;

import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public class dbtHCOPhysicianLU extends Object implements InttHCOPhysicianLU
{

        db_NewBase    dbnbDB;

    public dbtHCOPhysicianLU()
    {
        dbnbDB = new db_NewBase( "tHCOPhysicianLU", "LookupID" );

    }    // End of default constructor

    public dbtHCOPhysicianLU( Integer iNewID )
    {
        dbnbDB = new db_NewBase( "tHCOPhysicianLU", "LookupID" );
        setUniqueID( iNewID );
    }    // End of Constructor knowing an ID

    public void commitData() throws SQLException
    {
            //no longer trapping/ignoring errors 2011-03-17
            dbnbDB.commitData();
    }    // End of commitData()

    public void commitDataForced() throws SQLException
    {
        dbnbDB.commitDataForced();
    }    // End of commitData()

    public Integer getUniqueID()
    {
        return dbnbDB.getUniqueID();
    }    // End of getUniqueID()

    public void setUniqueID( Integer iNewID )
    {
        dbnbDB.setUniqueID( iNewID );
    }    // End of setUniqueID()

    public void setModifyComments( String sNew )
    {
        dbnbDB.setModifyComments( sNew );
    }    // End of setUniqueID()


    public void setLookupID(Integer newValue)
    {
                dbnbDB.setFieldData( "LookupID", newValue.toString() );
    }

    public Integer getLookupID()
    {
        String           sValue = dbnbDB.getFieldData( "LookupID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setField(String fieldN, Object fieldV)
    {DebugLogger.println("setField Method not implemented yet in classPhysicianMaster!");}

    public String getFieldType(String fieldN)
    {return null;}


    public void setUniqueCreateDate(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "UniqueCreateDate", formatter.format( newValue ) );
    }

    public Date getUniqueCreateDate()
    {
        String           sValue = dbnbDB.getFieldData( "UniqueCreateDate" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setUniqueModifyDate(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "UniqueModifyDate", formatter.format( newValue ) );
    }

    public Date getUniqueModifyDate()
    {
        String           sValue = dbnbDB.getFieldData( "UniqueModifyDate" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setUniqueModifyComments(String newValue)
    {
                dbnbDB.setFieldData( "UniqueModifyComments", newValue.toString() );
    }

    public String getUniqueModifyComments()
    {
        String           sValue = dbnbDB.getFieldData( "UniqueModifyComments" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setHCOID(Integer newValue)
    {
                dbnbDB.setFieldData( "HCOID", newValue.toString() );
    }

    public Integer getHCOID()
    {
        String           sValue = dbnbDB.getFieldData( "HCOID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setPhysicianID(Integer newValue)
    {
                dbnbDB.setFieldData( "PhysicianID", newValue.toString() );
    }

    public Integer getPhysicianID()
    {
        String           sValue = dbnbDB.getFieldData( "PhysicianID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setControlTypeID(Integer newValue)
    {
                dbnbDB.setFieldData( "ControlTypeID", newValue.toString() );
    }

    public Integer getControlTypeID()
    {
        String           sValue = dbnbDB.getFieldData( "ControlTypeID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setCredentialingStatus(Integer newValue)
    {
                dbnbDB.setFieldData( "CredentialingStatus", newValue.toString() );
    }

    public Integer getCredentialingStatus()
    {
        String           sValue = dbnbDB.getFieldData( "CredentialingStatus" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setApplicationReceiveDate(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "ApplicationReceiveDate", formatter.format( newValue ) );
    }

    public Date getApplicationReceiveDate()
    {
        String           sValue = dbnbDB.getFieldData( "ApplicationReceiveDate" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setApplicationSignDate(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "ApplicationSignDate", formatter.format( newValue ) );
    }

    public Date getApplicationSignDate()
    {
        String           sValue = dbnbDB.getFieldData( "ApplicationSignDate" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setCredentialingRequestDate(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "CredentialingRequestDate", formatter.format( newValue ) );
    }

    public Date getCredentialingRequestDate()
    {
        String           sValue = dbnbDB.getFieldData( "CredentialingRequestDate" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setInitialCredentialDate(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "InitialCredentialDate", formatter.format( newValue ) );
    }

    public Date getInitialCredentialDate()
    {
        String           sValue = dbnbDB.getFieldData( "InitialCredentialDate" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setLastCredentialDate(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "LastCredentialDate", formatter.format( newValue ) );
    }

    public Date getLastCredentialDate()
    {
        String           sValue = dbnbDB.getFieldData( "LastCredentialDate" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setNextRecredentialDate(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "NextRecredentialDate", formatter.format( newValue ) );
    }

    public Date getNextRecredentialDate()
    {
        String           sValue = dbnbDB.getFieldData( "NextRecredentialDate" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setEffectiveDate(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "EffectiveDate", formatter.format( newValue ) );
    }

    public Date getEffectiveDate()
    {
        String           sValue = dbnbDB.getFieldData( "EffectiveDate" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setTerminationDate(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "TerminationDate", formatter.format( newValue ) );
    }

    public Date getTerminationDate()
    {
        String           sValue = dbnbDB.getFieldData( "TerminationDate" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setCredentialer(Integer newValue)
    {
                dbnbDB.setFieldData( "Credentialer", newValue.toString() );
    }

    public Integer getCredentialer()
    {
        String           sValue = dbnbDB.getFieldData( "Credentialer" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setReviewer(Integer newValue)
    {
                dbnbDB.setFieldData( "Reviewer", newValue.toString() );
    }

    public Integer getReviewer()
    {
        String           sValue = dbnbDB.getFieldData( "Reviewer" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setDivision(String newValue)
    {
                dbnbDB.setFieldData( "Division", newValue.toString() );
    }

    public String getDivision()
    {
        String           sValue = dbnbDB.getFieldData( "Division" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setDistrict(String newValue)
    {
                dbnbDB.setFieldData( "District", newValue.toString() );
    }

    public String getDistrict()
    {
        String           sValue = dbnbDB.getFieldData( "District" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setContractManager(Integer newValue)
    {
                dbnbDB.setFieldData( "ContractManager", newValue.toString() );
    }

    public Integer getContractManager()
    {
        String           sValue = dbnbDB.getFieldData( "ContractManager" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setStatusType(String newValue)
    {
                dbnbDB.setFieldData( "StatusType", newValue.toString() );
    }

    public String getStatusType()
    {
        String           sValue = dbnbDB.getFieldData( "StatusType" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setUniqueProviderNumber(String newValue)
    {
                dbnbDB.setFieldData( "UniqueProviderNumber", newValue.toString() );
    }

    public String getUniqueProviderNumber()
    {
        String           sValue = dbnbDB.getFieldData( "UniqueProviderNumber" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setLaborAndIndustryNumber(String newValue)
    {
                dbnbDB.setFieldData( "LaborAndIndustryNumber", newValue.toString() );
    }

    public String getLaborAndIndustryNumber()
    {
        String           sValue = dbnbDB.getFieldData( "LaborAndIndustryNumber" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setUniqueBaselineNumber(String newValue)
    {
                dbnbDB.setFieldData( "UniqueBaselineNumber", newValue.toString() );
    }

    public String getUniqueBaselineNumber()
    {
        String           sValue = dbnbDB.getFieldData( "UniqueBaselineNumber" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setHoneywellNumber(String newValue)
    {
                dbnbDB.setFieldData( "HoneywellNumber", newValue.toString() );
    }

    public String getHoneywellNumber()
    {
        String           sValue = dbnbDB.getFieldData( "HoneywellNumber" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setContDepartment(String newValue)
    {
                dbnbDB.setFieldData( "ContDepartment", newValue.toString() );
    }

    public String getContDepartment()
    {
        String           sValue = dbnbDB.getFieldData( "ContDepartment" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setCertificationNotes(String newValue)
    {
                dbnbDB.setFieldData( "CertificationNotes", newValue.toString() );
    }

    public String getCertificationNotes()
    {
        String           sValue = dbnbDB.getFieldData( "CertificationNotes" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setTargetCPCDate(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "TargetCPCDate", formatter.format( newValue ) );
    }

    public Date getTargetCPCDate()
    {
        String           sValue = dbnbDB.getFieldData( "TargetCPCDate" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setMSOFileRequestDate(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "MSOFileRequestDate", formatter.format( newValue ) );
    }

    public Date getMSOFileRequestDate()
    {
        String           sValue = dbnbDB.getFieldData( "MSOFileRequestDate" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setMedicarePASDate(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "MedicarePASDate", formatter.format( newValue ) );
    }

    public Date getMedicarePASDate()
    {
        String           sValue = dbnbDB.getFieldData( "MedicarePASDate" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setUserField1(String newValue)
    {
                dbnbDB.setFieldData( "UserField1", newValue.toString() );
    }

    public String getUserField1()
    {
        String           sValue = dbnbDB.getFieldData( "UserField1" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setUserField2(String newValue)
    {
                dbnbDB.setFieldData( "UserField2", newValue.toString() );
    }

    public String getUserField2()
    {
        String           sValue = dbnbDB.getFieldData( "UserField2" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setUserField3(String newValue)
    {
                dbnbDB.setFieldData( "UserField3", newValue.toString() );
    }

    public String getUserField3()
    {
        String           sValue = dbnbDB.getFieldData( "UserField3" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setUserField4(String newValue)
    {
                dbnbDB.setFieldData( "UserField4", newValue.toString() );
    }

    public String getUserField4()
    {
        String           sValue = dbnbDB.getFieldData( "UserField4" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setUserField5(String newValue)
    {
                dbnbDB.setFieldData( "UserField5", newValue.toString() );
    }

    public String getUserField5()
    {
        String           sValue = dbnbDB.getFieldData( "UserField5" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setComments(String newValue)
    {
                dbnbDB.setFieldData( "Comments", newValue.toString() );
    }

    public String getComments()
    {
        String           sValue = dbnbDB.getFieldData( "Comments" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setSponsoringPractitionerPrimary(String newValue)
    {
                dbnbDB.setFieldData( "SponsoringPractitionerPrimary", newValue.toString() );
    }

    public String getSponsoringPractitionerPrimary()
    {
        String           sValue = dbnbDB.getFieldData( "SponsoringPractitionerPrimary" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setSponsoringPractitionerSecondary(String newValue)
    {
                dbnbDB.setFieldData( "SponsoringPractitionerSecondary", newValue.toString() );
    }

    public String getSponsoringPractitionerSecondary()
    {
        String           sValue = dbnbDB.getFieldData( "SponsoringPractitionerSecondary" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

}    // End of bltHCOPhysicianLU class definition
