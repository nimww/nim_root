

package com.winstaff;

import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public interface InttNIM3_CommReference extends dbTableInterface
{

    public void setCommReferenceID(Integer newValue);
    public Integer getCommReferenceID();
    public void setUniqueCreateDate(Date newValue);
    public Date getUniqueCreateDate();
    public void setUniqueModifyDate(Date newValue);
    public Date getUniqueModifyDate();
    public void setUniqueModifyComments(String newValue);
    public String getUniqueModifyComments();
    public void setCRID(Integer newValue);
    public Integer getCRID();
    public void setRefID(Integer newValue);
    public Integer getRefID();
    public void setCRTypeID(Integer newValue);
    public Integer getCRTypeID();
    public void setComments(String newValue);
    public String getComments();
}    // End of bltNIM3_CommReference class definition
