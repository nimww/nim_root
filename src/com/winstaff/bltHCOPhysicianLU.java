

package com.winstaff;

import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;
import java.util.Enumeration;

public class bltHCOPhysicianLU extends Object implements InttHCOPhysicianLU
{

    dbtHCOPhysicianLU    dbDB;

    public bltHCOPhysicianLU()
    {
        dbDB = new dbtHCOPhysicianLU();
        dbDB.setUniqueCreateDate(new java.util.Date());
        makeRequiredHash();
        makeExpiredHash();
        makeEnglishHash();

    }    // End of default constructor

    public bltHCOPhysicianLU( Integer iNewID )
    {        dbDB = new dbtHCOPhysicianLU( iNewID );
        makeRequiredHash();
        makeExpiredHash();
        makeEnglishHash();
    }    // End of Constructor knowing an ID


    public bltHCOPhysicianLU( Integer iNewID, Integer iGroupSecurityID )
    {
        this.GroupSecurityInit(iGroupSecurityID);
        dbDB = new dbtHCOPhysicianLU( iNewID );
        makeExpiredHash();
        makeEnglishHash();
        makeRequiredHash(iGroupSecurityID);
    }    // End of Constructor knowing an ID

    public bltHCOPhysicianLU( Integer iGroupSecurityID, boolean isTrue )
    {
        this.GroupSecurityInit(iGroupSecurityID);
        dbDB = new dbtHCOPhysicianLU(  );
        makeExpiredHash();
        makeEnglishHash();
        makeRequiredHash(iGroupSecurityID);
    }    // End of Security unknown ID

    public void GroupSecurityInit( Integer iGroupSecurityID )
    {
        bltSecurityGroupMaster mySGM = new bltSecurityGroupMaster(iGroupSecurityID);
        this.UserSecurityID = iGroupSecurityID;
        if (mySGM.getRunFieldSecurityID().intValue()==1)
        {
            this.runFieldSecurity = true;
            makeRequiredHash(iGroupSecurityID);
        }
    }  

    public EventReferenceType getEventReference()
    {
        return EventUtils.getEventReference("tHCOPhysicianLU", this.getUniqueID());
    }  

    public String convertString2LogString(String normalString) 
    {
        String myVal = "";
        for (int i=0;i<normalString.length();i++)
        {
            if ( (normalString.charAt(i)!='\r')&&(normalString.charAt(i)!='\n')&&(normalString.charAt(i)!='\t') )
            {
                myVal+=normalString.charAt(i);
            }
            else
            {
                myVal+="<NLT>";
            }
        }
        return myVal;
    }
    public void runAudit()
    {
        String AuditString = new java.util.Date()+" \t " ;  
        AuditString += " tHCOPhysicianLU "; 
        AuditString += " LookupID ="+this.getUniqueID(); 

        for (int i=0;i<this.AuditVector.size();i++)
        {
            AuditString+="\t"+convertString2LogString((String)this.AuditVector.elementAt(i));
        }
        AuditLogger.printLine(AuditString);
    }
    public void commitData() throws SQLException
    {
        runAudit();
        dbDB.setModifyComments(this.getUniqueModifyComments());
        dbDB.setUniqueModifyDate(new java.util.Date());
        dbDB.commitData();

        this.AuditVector.removeAllElements();
    }    // End of commitData()

    public void commitDataForced() throws SQLException
    {
        dbDB.commitDataForced();
    }    // End of commitData()

    public Integer getUniqueID()
    {
        return dbDB.getUniqueID();
    }    // End of getUniqueID()

    public void setUniqueID( Integer iNewID )
    {
        dbDB.setUniqueID( iNewID );
    }    // End of setUniqueID()


    public void setLookupID(Integer newValue)
    {
        dbDB.setLookupID(newValue);
    }

    public Integer getLookupID()
    {
        return dbDB.getLookupID();
    }

    java.util.Vector AuditVector = new java.util.Vector();

    public void setUniqueCreateDate(Date newValue)
    {        this.setUniqueCreateDate(newValue,this.UserSecurityID);

    }
    public Date getUniqueCreateDate()
    {
        return this.getUniqueCreateDate(this.UserSecurityID);
    }

    public void setUniqueCreateDate(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='UniqueCreateDate' AND TableName='tHCOPhysicianLU'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {
                   this.AuditVector.addElement("[UniqueCreateDate]=["+newValue+"]");
                   this.AuditVector.addElement("[UniqueCreateDate]=["+newValue+"]");
                   dbDB.setUniqueCreateDate(newValue);
               }
            }
        }
        else
        {
           this.AuditVector.addElement("[UniqueCreateDate]=["+newValue+"]");
           this.AuditVector.addElement("[UniqueCreateDate]=["+newValue+"]");
           dbDB.setUniqueCreateDate(newValue);
         }
    }
    public Date getUniqueCreateDate(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='UniqueCreateDate' AND TableName='tHCOPhysicianLU'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getUniqueCreateDate();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getUniqueCreateDate();
         }
        return myVal;
    }

    public void setUniqueModifyDate(Date newValue)
    {        this.setUniqueModifyDate(newValue,this.UserSecurityID);

    }
    public Date getUniqueModifyDate()
    {
        return this.getUniqueModifyDate(this.UserSecurityID);
    }

    public void setUniqueModifyDate(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='UniqueModifyDate' AND TableName='tHCOPhysicianLU'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {
                   this.AuditVector.addElement("[UniqueModifyDate]=["+newValue+"]");
                   this.AuditVector.addElement("[UniqueModifyDate]=["+newValue+"]");
                   dbDB.setUniqueModifyDate(newValue);
               }
            }
        }
        else
        {
           this.AuditVector.addElement("[UniqueModifyDate]=["+newValue+"]");
           this.AuditVector.addElement("[UniqueModifyDate]=["+newValue+"]");
           dbDB.setUniqueModifyDate(newValue);
         }
    }
    public Date getUniqueModifyDate(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='UniqueModifyDate' AND TableName='tHCOPhysicianLU'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getUniqueModifyDate();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getUniqueModifyDate();
         }
        return myVal;
    }

    public void setUniqueModifyComments(String newValue)
    {        this.setUniqueModifyComments(newValue,this.UserSecurityID);

    }
    public String getUniqueModifyComments()
    {
        return this.getUniqueModifyComments(this.UserSecurityID);
    }

    public void setUniqueModifyComments(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='UniqueModifyComments' AND TableName='tHCOPhysicianLU'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {
                   this.AuditVector.addElement("[UniqueModifyComments]=["+newValue+"]");
                   this.AuditVector.addElement("[UniqueModifyComments]=["+newValue+"]");
                   dbDB.setUniqueModifyComments(newValue);
               }
            }
        }
        else
        {
           this.AuditVector.addElement("[UniqueModifyComments]=["+newValue+"]");
           this.AuditVector.addElement("[UniqueModifyComments]=["+newValue+"]");
           dbDB.setUniqueModifyComments(newValue);
         }
    }
    public String getUniqueModifyComments(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='UniqueModifyComments' AND TableName='tHCOPhysicianLU'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getUniqueModifyComments();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getUniqueModifyComments();
         }
        return myVal;
    }

    public void setHCOID(Integer newValue)
    {        this.setHCOID(newValue,this.UserSecurityID);

    }
    public Integer getHCOID()
    {
        return this.getHCOID(this.UserSecurityID);
    }

    public void setHCOID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='HCOID' AND TableName='tHCOPhysicianLU'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {
                   this.AuditVector.addElement("[HCOID]=["+newValue+"]");
                   this.AuditVector.addElement("[HCOID]=["+newValue+"]");
                   dbDB.setHCOID(newValue);
               }
            }
        }
        else
        {
           this.AuditVector.addElement("[HCOID]=["+newValue+"]");
           this.AuditVector.addElement("[HCOID]=["+newValue+"]");
           dbDB.setHCOID(newValue);
         }
    }
    public Integer getHCOID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='HCOID' AND TableName='tHCOPhysicianLU'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getHCOID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getHCOID();
         }
        return myVal;
    }

    public void setPhysicianID(Integer newValue)
    {        this.setPhysicianID(newValue,this.UserSecurityID);

    }
    public Integer getPhysicianID()
    {
        return this.getPhysicianID(this.UserSecurityID);
    }

    public void setPhysicianID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PhysicianID' AND TableName='tHCOPhysicianLU'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {
                   this.AuditVector.addElement("[PhysicianID]=["+newValue+"]");
                   this.AuditVector.addElement("[PhysicianID]=["+newValue+"]");
                   dbDB.setPhysicianID(newValue);
               }
            }
        }
        else
        {
           this.AuditVector.addElement("[PhysicianID]=["+newValue+"]");
           this.AuditVector.addElement("[PhysicianID]=["+newValue+"]");
           dbDB.setPhysicianID(newValue);
         }
    }
    public Integer getPhysicianID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PhysicianID' AND TableName='tHCOPhysicianLU'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPhysicianID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPhysicianID();
         }
        return myVal;
    }

    public void setControlTypeID(Integer newValue)
    {        this.setControlTypeID(newValue,this.UserSecurityID);

    }
    public Integer getControlTypeID()
    {
        return this.getControlTypeID(this.UserSecurityID);
    }

    public void setControlTypeID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ControlTypeID' AND TableName='tHCOPhysicianLU'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {
                   this.AuditVector.addElement("[ControlTypeID]=["+newValue+"]");
                   this.AuditVector.addElement("[ControlTypeID]=["+newValue+"]");
                   dbDB.setControlTypeID(newValue);
               }
            }
        }
        else
        {
           this.AuditVector.addElement("[ControlTypeID]=["+newValue+"]");
           this.AuditVector.addElement("[ControlTypeID]=["+newValue+"]");
           dbDB.setControlTypeID(newValue);
         }
    }
    public Integer getControlTypeID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ControlTypeID' AND TableName='tHCOPhysicianLU'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getControlTypeID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getControlTypeID();
         }
        return myVal;
    }

    public void setCredentialingStatus(Integer newValue)
    {        this.setCredentialingStatus(newValue,this.UserSecurityID);

    }
    public Integer getCredentialingStatus()
    {
        return this.getCredentialingStatus(this.UserSecurityID);
    }

    public void setCredentialingStatus(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CredentialingStatus' AND TableName='tHCOPhysicianLU'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {
                   this.AuditVector.addElement("[CredentialingStatus]=["+newValue+"]");
                   this.AuditVector.addElement("[CredentialingStatus]=["+newValue+"]");
                   dbDB.setCredentialingStatus(newValue);
               }
            }
        }
        else
        {
           this.AuditVector.addElement("[CredentialingStatus]=["+newValue+"]");
           this.AuditVector.addElement("[CredentialingStatus]=["+newValue+"]");
           dbDB.setCredentialingStatus(newValue);
         }
    }
    public Integer getCredentialingStatus(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CredentialingStatus' AND TableName='tHCOPhysicianLU'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getCredentialingStatus();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getCredentialingStatus();
         }
        return myVal;
    }

    public void setApplicationReceiveDate(Date newValue)
    {        this.setApplicationReceiveDate(newValue,this.UserSecurityID);

    }
    public Date getApplicationReceiveDate()
    {
        return this.getApplicationReceiveDate(this.UserSecurityID);
    }

    public void setApplicationReceiveDate(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ApplicationReceiveDate' AND TableName='tHCOPhysicianLU'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {
                   this.AuditVector.addElement("[ApplicationReceiveDate]=["+newValue+"]");
                   this.AuditVector.addElement("[ApplicationReceiveDate]=["+newValue+"]");
                   dbDB.setApplicationReceiveDate(newValue);
               }
            }
        }
        else
        {
           this.AuditVector.addElement("[ApplicationReceiveDate]=["+newValue+"]");
           this.AuditVector.addElement("[ApplicationReceiveDate]=["+newValue+"]");
           dbDB.setApplicationReceiveDate(newValue);
         }
    }
    public Date getApplicationReceiveDate(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ApplicationReceiveDate' AND TableName='tHCOPhysicianLU'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getApplicationReceiveDate();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getApplicationReceiveDate();
         }
        return myVal;
    }

    public void setApplicationSignDate(Date newValue)
    {        this.setApplicationSignDate(newValue,this.UserSecurityID);

    }
    public Date getApplicationSignDate()
    {
        return this.getApplicationSignDate(this.UserSecurityID);
    }

    public void setApplicationSignDate(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ApplicationSignDate' AND TableName='tHCOPhysicianLU'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {
                   this.AuditVector.addElement("[ApplicationSignDate]=["+newValue+"]");
                   this.AuditVector.addElement("[ApplicationSignDate]=["+newValue+"]");
                   dbDB.setApplicationSignDate(newValue);
               }
            }
        }
        else
        {
           this.AuditVector.addElement("[ApplicationSignDate]=["+newValue+"]");
           this.AuditVector.addElement("[ApplicationSignDate]=["+newValue+"]");
           dbDB.setApplicationSignDate(newValue);
         }
    }
    public Date getApplicationSignDate(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ApplicationSignDate' AND TableName='tHCOPhysicianLU'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getApplicationSignDate();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getApplicationSignDate();
         }
        return myVal;
    }

    public void setCredentialingRequestDate(Date newValue)
    {        this.setCredentialingRequestDate(newValue,this.UserSecurityID);

    }
    public Date getCredentialingRequestDate()
    {
        return this.getCredentialingRequestDate(this.UserSecurityID);
    }

    public void setCredentialingRequestDate(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CredentialingRequestDate' AND TableName='tHCOPhysicianLU'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {
                   this.AuditVector.addElement("[CredentialingRequestDate]=["+newValue+"]");
                   this.AuditVector.addElement("[CredentialingRequestDate]=["+newValue+"]");
                   dbDB.setCredentialingRequestDate(newValue);
               }
            }
        }
        else
        {
           this.AuditVector.addElement("[CredentialingRequestDate]=["+newValue+"]");
           this.AuditVector.addElement("[CredentialingRequestDate]=["+newValue+"]");
           dbDB.setCredentialingRequestDate(newValue);
         }
    }
    public Date getCredentialingRequestDate(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CredentialingRequestDate' AND TableName='tHCOPhysicianLU'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getCredentialingRequestDate();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getCredentialingRequestDate();
         }
        return myVal;
    }

    public void setInitialCredentialDate(Date newValue)
    {        this.setInitialCredentialDate(newValue,this.UserSecurityID);

    }
    public Date getInitialCredentialDate()
    {
        return this.getInitialCredentialDate(this.UserSecurityID);
    }

    public void setInitialCredentialDate(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='InitialCredentialDate' AND TableName='tHCOPhysicianLU'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {
                   this.AuditVector.addElement("[InitialCredentialDate]=["+newValue+"]");
                   this.AuditVector.addElement("[InitialCredentialDate]=["+newValue+"]");
                   dbDB.setInitialCredentialDate(newValue);
               }
            }
        }
        else
        {
           this.AuditVector.addElement("[InitialCredentialDate]=["+newValue+"]");
           this.AuditVector.addElement("[InitialCredentialDate]=["+newValue+"]");
           dbDB.setInitialCredentialDate(newValue);
         }
    }
    public Date getInitialCredentialDate(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='InitialCredentialDate' AND TableName='tHCOPhysicianLU'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getInitialCredentialDate();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getInitialCredentialDate();
         }
        return myVal;
    }

    public void setLastCredentialDate(Date newValue)
    {        this.setLastCredentialDate(newValue,this.UserSecurityID);

    }
    public Date getLastCredentialDate()
    {
        return this.getLastCredentialDate(this.UserSecurityID);
    }

    public void setLastCredentialDate(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='LastCredentialDate' AND TableName='tHCOPhysicianLU'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {
                   this.AuditVector.addElement("[LastCredentialDate]=["+newValue+"]");
                   this.AuditVector.addElement("[LastCredentialDate]=["+newValue+"]");
                   dbDB.setLastCredentialDate(newValue);
               }
            }
        }
        else
        {
           this.AuditVector.addElement("[LastCredentialDate]=["+newValue+"]");
           this.AuditVector.addElement("[LastCredentialDate]=["+newValue+"]");
           dbDB.setLastCredentialDate(newValue);
         }
    }
    public Date getLastCredentialDate(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='LastCredentialDate' AND TableName='tHCOPhysicianLU'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getLastCredentialDate();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getLastCredentialDate();
         }
        return myVal;
    }

    public void setNextRecredentialDate(Date newValue)
    {        this.setNextRecredentialDate(newValue,this.UserSecurityID);

    }
    public Date getNextRecredentialDate()
    {
        return this.getNextRecredentialDate(this.UserSecurityID);
    }

    public void setNextRecredentialDate(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='NextRecredentialDate' AND TableName='tHCOPhysicianLU'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {
                   this.AuditVector.addElement("[NextRecredentialDate]=["+newValue+"]");
                   this.AuditVector.addElement("[NextRecredentialDate]=["+newValue+"]");
                   dbDB.setNextRecredentialDate(newValue);
               }
            }
        }
        else
        {
           this.AuditVector.addElement("[NextRecredentialDate]=["+newValue+"]");
           this.AuditVector.addElement("[NextRecredentialDate]=["+newValue+"]");
           dbDB.setNextRecredentialDate(newValue);
         }
    }
    public Date getNextRecredentialDate(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='NextRecredentialDate' AND TableName='tHCOPhysicianLU'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getNextRecredentialDate();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getNextRecredentialDate();
         }
        return myVal;
    }

    public void setEffectiveDate(Date newValue)
    {        this.setEffectiveDate(newValue,this.UserSecurityID);

    }
    public Date getEffectiveDate()
    {
        return this.getEffectiveDate(this.UserSecurityID);
    }

    public void setEffectiveDate(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='EffectiveDate' AND TableName='tHCOPhysicianLU'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {
                   this.AuditVector.addElement("[EffectiveDate]=["+newValue+"]");
                   this.AuditVector.addElement("[EffectiveDate]=["+newValue+"]");
                   dbDB.setEffectiveDate(newValue);
               }
            }
        }
        else
        {
           this.AuditVector.addElement("[EffectiveDate]=["+newValue+"]");
           this.AuditVector.addElement("[EffectiveDate]=["+newValue+"]");
           dbDB.setEffectiveDate(newValue);
         }
    }
    public Date getEffectiveDate(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='EffectiveDate' AND TableName='tHCOPhysicianLU'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getEffectiveDate();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getEffectiveDate();
         }
        return myVal;
    }

    public void setTerminationDate(Date newValue)
    {        this.setTerminationDate(newValue,this.UserSecurityID);

    }
    public Date getTerminationDate()
    {
        return this.getTerminationDate(this.UserSecurityID);
    }

    public void setTerminationDate(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='TerminationDate' AND TableName='tHCOPhysicianLU'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {
                   this.AuditVector.addElement("[TerminationDate]=["+newValue+"]");
                   this.AuditVector.addElement("[TerminationDate]=["+newValue+"]");
                   dbDB.setTerminationDate(newValue);
               }
            }
        }
        else
        {
           this.AuditVector.addElement("[TerminationDate]=["+newValue+"]");
           this.AuditVector.addElement("[TerminationDate]=["+newValue+"]");
           dbDB.setTerminationDate(newValue);
         }
    }
    public Date getTerminationDate(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='TerminationDate' AND TableName='tHCOPhysicianLU'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getTerminationDate();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getTerminationDate();
         }
        return myVal;
    }

    public void setCredentialer(Integer newValue)
    {        this.setCredentialer(newValue,this.UserSecurityID);

    }
    public Integer getCredentialer()
    {
        return this.getCredentialer(this.UserSecurityID);
    }

    public void setCredentialer(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Credentialer' AND TableName='tHCOPhysicianLU'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {
                   this.AuditVector.addElement("[Credentialer]=["+newValue+"]");
                   this.AuditVector.addElement("[Credentialer]=["+newValue+"]");
                   dbDB.setCredentialer(newValue);
               }
            }
        }
        else
        {
           this.AuditVector.addElement("[Credentialer]=["+newValue+"]");
           this.AuditVector.addElement("[Credentialer]=["+newValue+"]");
           dbDB.setCredentialer(newValue);
         }
    }
    public Integer getCredentialer(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Credentialer' AND TableName='tHCOPhysicianLU'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getCredentialer();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getCredentialer();
         }
        return myVal;
    }

    public void setReviewer(Integer newValue)
    {        this.setReviewer(newValue,this.UserSecurityID);

    }
    public Integer getReviewer()
    {
        return this.getReviewer(this.UserSecurityID);
    }

    public void setReviewer(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Reviewer' AND TableName='tHCOPhysicianLU'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {
                   this.AuditVector.addElement("[Reviewer]=["+newValue+"]");
                   this.AuditVector.addElement("[Reviewer]=["+newValue+"]");
                   dbDB.setReviewer(newValue);
               }
            }
        }
        else
        {
           this.AuditVector.addElement("[Reviewer]=["+newValue+"]");
           this.AuditVector.addElement("[Reviewer]=["+newValue+"]");
           dbDB.setReviewer(newValue);
         }
    }
    public Integer getReviewer(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Reviewer' AND TableName='tHCOPhysicianLU'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getReviewer();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getReviewer();
         }
        return myVal;
    }

    public void setDivision(String newValue)
    {        this.setDivision(newValue,this.UserSecurityID);

    }
    public String getDivision()
    {
        return this.getDivision(this.UserSecurityID);
    }

    public void setDivision(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Division' AND TableName='tHCOPhysicianLU'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {
                   this.AuditVector.addElement("[Division]=["+newValue+"]");
                   this.AuditVector.addElement("[Division]=["+newValue+"]");
                   dbDB.setDivision(newValue);
               }
            }
        }
        else
        {
           this.AuditVector.addElement("[Division]=["+newValue+"]");
           this.AuditVector.addElement("[Division]=["+newValue+"]");
           dbDB.setDivision(newValue);
         }
    }
    public String getDivision(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Division' AND TableName='tHCOPhysicianLU'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getDivision();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getDivision();
         }
        return myVal;
    }

    public void setDistrict(String newValue)
    {        this.setDistrict(newValue,this.UserSecurityID);

    }
    public String getDistrict()
    {
        return this.getDistrict(this.UserSecurityID);
    }

    public void setDistrict(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='District' AND TableName='tHCOPhysicianLU'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {
                   this.AuditVector.addElement("[District]=["+newValue+"]");
                   this.AuditVector.addElement("[District]=["+newValue+"]");
                   dbDB.setDistrict(newValue);
               }
            }
        }
        else
        {
           this.AuditVector.addElement("[District]=["+newValue+"]");
           this.AuditVector.addElement("[District]=["+newValue+"]");
           dbDB.setDistrict(newValue);
         }
    }
    public String getDistrict(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='District' AND TableName='tHCOPhysicianLU'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getDistrict();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getDistrict();
         }
        return myVal;
    }

    public void setContractManager(Integer newValue)
    {        this.setContractManager(newValue,this.UserSecurityID);

    }
    public Integer getContractManager()
    {
        return this.getContractManager(this.UserSecurityID);
    }

    public void setContractManager(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ContractManager' AND TableName='tHCOPhysicianLU'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {
                   this.AuditVector.addElement("[ContractManager]=["+newValue+"]");
                   this.AuditVector.addElement("[ContractManager]=["+newValue+"]");
                   dbDB.setContractManager(newValue);
               }
            }
        }
        else
        {
           this.AuditVector.addElement("[ContractManager]=["+newValue+"]");
           this.AuditVector.addElement("[ContractManager]=["+newValue+"]");
           dbDB.setContractManager(newValue);
         }
    }
    public Integer getContractManager(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ContractManager' AND TableName='tHCOPhysicianLU'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getContractManager();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getContractManager();
         }
        return myVal;
    }

    public void setStatusType(String newValue)
    {        this.setStatusType(newValue,this.UserSecurityID);

    }
    public String getStatusType()
    {
        return this.getStatusType(this.UserSecurityID);
    }

    public void setStatusType(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='StatusType' AND TableName='tHCOPhysicianLU'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {
                   this.AuditVector.addElement("[StatusType]=["+newValue+"]");
                   this.AuditVector.addElement("[StatusType]=["+newValue+"]");
                   dbDB.setStatusType(newValue);
               }
            }
        }
        else
        {
           this.AuditVector.addElement("[StatusType]=["+newValue+"]");
           this.AuditVector.addElement("[StatusType]=["+newValue+"]");
           dbDB.setStatusType(newValue);
         }
    }
    public String getStatusType(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='StatusType' AND TableName='tHCOPhysicianLU'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getStatusType();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getStatusType();
         }
        return myVal;
    }

    public void setUniqueProviderNumber(String newValue)
    {        this.setUniqueProviderNumber(newValue,this.UserSecurityID);

    }
    public String getUniqueProviderNumber()
    {
        return this.getUniqueProviderNumber(this.UserSecurityID);
    }

    public void setUniqueProviderNumber(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='UniqueProviderNumber' AND TableName='tHCOPhysicianLU'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {
                   this.AuditVector.addElement("[UniqueProviderNumber]=["+newValue+"]");
                   this.AuditVector.addElement("[UniqueProviderNumber]=["+newValue+"]");
                   dbDB.setUniqueProviderNumber(newValue);
               }
            }
        }
        else
        {
           this.AuditVector.addElement("[UniqueProviderNumber]=["+newValue+"]");
           this.AuditVector.addElement("[UniqueProviderNumber]=["+newValue+"]");
           dbDB.setUniqueProviderNumber(newValue);
         }
    }
    public String getUniqueProviderNumber(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='UniqueProviderNumber' AND TableName='tHCOPhysicianLU'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getUniqueProviderNumber();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getUniqueProviderNumber();
         }
        return myVal;
    }

    public void setLaborAndIndustryNumber(String newValue)
    {        this.setLaborAndIndustryNumber(newValue,this.UserSecurityID);

    }
    public String getLaborAndIndustryNumber()
    {
        return this.getLaborAndIndustryNumber(this.UserSecurityID);
    }

    public void setLaborAndIndustryNumber(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='LaborAndIndustryNumber' AND TableName='tHCOPhysicianLU'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {
                   this.AuditVector.addElement("[LaborAndIndustryNumber]=["+newValue+"]");
                   this.AuditVector.addElement("[LaborAndIndustryNumber]=["+newValue+"]");
                   dbDB.setLaborAndIndustryNumber(newValue);
               }
            }
        }
        else
        {
           this.AuditVector.addElement("[LaborAndIndustryNumber]=["+newValue+"]");
           this.AuditVector.addElement("[LaborAndIndustryNumber]=["+newValue+"]");
           dbDB.setLaborAndIndustryNumber(newValue);
         }
    }
    public String getLaborAndIndustryNumber(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='LaborAndIndustryNumber' AND TableName='tHCOPhysicianLU'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getLaborAndIndustryNumber();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getLaborAndIndustryNumber();
         }
        return myVal;
    }

    public void setUniqueBaselineNumber(String newValue)
    {        this.setUniqueBaselineNumber(newValue,this.UserSecurityID);

    }
    public String getUniqueBaselineNumber()
    {
        return this.getUniqueBaselineNumber(this.UserSecurityID);
    }

    public void setUniqueBaselineNumber(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='UniqueBaselineNumber' AND TableName='tHCOPhysicianLU'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {
                   this.AuditVector.addElement("[UniqueBaselineNumber]=["+newValue+"]");
                   this.AuditVector.addElement("[UniqueBaselineNumber]=["+newValue+"]");
                   dbDB.setUniqueBaselineNumber(newValue);
               }
            }
        }
        else
        {
           this.AuditVector.addElement("[UniqueBaselineNumber]=["+newValue+"]");
           this.AuditVector.addElement("[UniqueBaselineNumber]=["+newValue+"]");
           dbDB.setUniqueBaselineNumber(newValue);
         }
    }
    public String getUniqueBaselineNumber(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='UniqueBaselineNumber' AND TableName='tHCOPhysicianLU'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getUniqueBaselineNumber();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getUniqueBaselineNumber();
         }
        return myVal;
    }

    public void setHoneywellNumber(String newValue)
    {        this.setHoneywellNumber(newValue,this.UserSecurityID);

    }
    public String getHoneywellNumber()
    {
        return this.getHoneywellNumber(this.UserSecurityID);
    }

    public void setHoneywellNumber(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='HoneywellNumber' AND TableName='tHCOPhysicianLU'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {
                   this.AuditVector.addElement("[HoneywellNumber]=["+newValue+"]");
                   this.AuditVector.addElement("[HoneywellNumber]=["+newValue+"]");
                   dbDB.setHoneywellNumber(newValue);
               }
            }
        }
        else
        {
           this.AuditVector.addElement("[HoneywellNumber]=["+newValue+"]");
           this.AuditVector.addElement("[HoneywellNumber]=["+newValue+"]");
           dbDB.setHoneywellNumber(newValue);
         }
    }
    public String getHoneywellNumber(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='HoneywellNumber' AND TableName='tHCOPhysicianLU'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getHoneywellNumber();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getHoneywellNumber();
         }
        return myVal;
    }

    public void setContDepartment(String newValue)
    {        this.setContDepartment(newValue,this.UserSecurityID);

    }
    public String getContDepartment()
    {
        return this.getContDepartment(this.UserSecurityID);
    }

    public void setContDepartment(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ContDepartment' AND TableName='tHCOPhysicianLU'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {
                   this.AuditVector.addElement("[ContDepartment]=["+newValue+"]");
                   this.AuditVector.addElement("[ContDepartment]=["+newValue+"]");
                   dbDB.setContDepartment(newValue);
               }
            }
        }
        else
        {
           this.AuditVector.addElement("[ContDepartment]=["+newValue+"]");
           this.AuditVector.addElement("[ContDepartment]=["+newValue+"]");
           dbDB.setContDepartment(newValue);
         }
    }
    public String getContDepartment(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ContDepartment' AND TableName='tHCOPhysicianLU'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getContDepartment();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getContDepartment();
         }
        return myVal;
    }

    public void setCertificationNotes(String newValue)
    {        this.setCertificationNotes(newValue,this.UserSecurityID);

    }
    public String getCertificationNotes()
    {
        return this.getCertificationNotes(this.UserSecurityID);
    }

    public void setCertificationNotes(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CertificationNotes' AND TableName='tHCOPhysicianLU'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {
                   this.AuditVector.addElement("[CertificationNotes]=["+newValue+"]");
                   this.AuditVector.addElement("[CertificationNotes]=["+newValue+"]");
                   dbDB.setCertificationNotes(newValue);
               }
            }
        }
        else
        {
           this.AuditVector.addElement("[CertificationNotes]=["+newValue+"]");
           this.AuditVector.addElement("[CertificationNotes]=["+newValue+"]");
           dbDB.setCertificationNotes(newValue);
         }
    }
    public String getCertificationNotes(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CertificationNotes' AND TableName='tHCOPhysicianLU'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getCertificationNotes();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getCertificationNotes();
         }
        return myVal;
    }

    public void setTargetCPCDate(Date newValue)
    {        this.setTargetCPCDate(newValue,this.UserSecurityID);

    }
    public Date getTargetCPCDate()
    {
        return this.getTargetCPCDate(this.UserSecurityID);
    }

    public void setTargetCPCDate(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='TargetCPCDate' AND TableName='tHCOPhysicianLU'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {
                   this.AuditVector.addElement("[TargetCPCDate]=["+newValue+"]");
                   this.AuditVector.addElement("[TargetCPCDate]=["+newValue+"]");
                   dbDB.setTargetCPCDate(newValue);
               }
            }
        }
        else
        {
           this.AuditVector.addElement("[TargetCPCDate]=["+newValue+"]");
           this.AuditVector.addElement("[TargetCPCDate]=["+newValue+"]");
           dbDB.setTargetCPCDate(newValue);
         }
    }
    public Date getTargetCPCDate(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='TargetCPCDate' AND TableName='tHCOPhysicianLU'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getTargetCPCDate();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getTargetCPCDate();
         }
        return myVal;
    }

    public void setMSOFileRequestDate(Date newValue)
    {        this.setMSOFileRequestDate(newValue,this.UserSecurityID);

    }
    public Date getMSOFileRequestDate()
    {
        return this.getMSOFileRequestDate(this.UserSecurityID);
    }

    public void setMSOFileRequestDate(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='MSOFileRequestDate' AND TableName='tHCOPhysicianLU'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {
                   this.AuditVector.addElement("[MSOFileRequestDate]=["+newValue+"]");
                   this.AuditVector.addElement("[MSOFileRequestDate]=["+newValue+"]");
                   dbDB.setMSOFileRequestDate(newValue);
               }
            }
        }
        else
        {
           this.AuditVector.addElement("[MSOFileRequestDate]=["+newValue+"]");
           this.AuditVector.addElement("[MSOFileRequestDate]=["+newValue+"]");
           dbDB.setMSOFileRequestDate(newValue);
         }
    }
    public Date getMSOFileRequestDate(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='MSOFileRequestDate' AND TableName='tHCOPhysicianLU'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getMSOFileRequestDate();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getMSOFileRequestDate();
         }
        return myVal;
    }

    public void setMedicarePASDate(Date newValue)
    {        this.setMedicarePASDate(newValue,this.UserSecurityID);

    }
    public Date getMedicarePASDate()
    {
        return this.getMedicarePASDate(this.UserSecurityID);
    }

    public void setMedicarePASDate(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='MedicarePASDate' AND TableName='tHCOPhysicianLU'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {
                   this.AuditVector.addElement("[MedicarePASDate]=["+newValue+"]");
                   this.AuditVector.addElement("[MedicarePASDate]=["+newValue+"]");
                   dbDB.setMedicarePASDate(newValue);
               }
            }
        }
        else
        {
           this.AuditVector.addElement("[MedicarePASDate]=["+newValue+"]");
           this.AuditVector.addElement("[MedicarePASDate]=["+newValue+"]");
           dbDB.setMedicarePASDate(newValue);
         }
    }
    public Date getMedicarePASDate(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='MedicarePASDate' AND TableName='tHCOPhysicianLU'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getMedicarePASDate();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getMedicarePASDate();
         }
        return myVal;
    }

    public void setUserField1(String newValue)
    {        this.setUserField1(newValue,this.UserSecurityID);

    }
    public String getUserField1()
    {
        return this.getUserField1(this.UserSecurityID);
    }

    public void setUserField1(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='UserField1' AND TableName='tHCOPhysicianLU'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {
                   this.AuditVector.addElement("[UserField1]=["+newValue+"]");
                   this.AuditVector.addElement("[UserField1]=["+newValue+"]");
                   dbDB.setUserField1(newValue);
               }
            }
        }
        else
        {
           this.AuditVector.addElement("[UserField1]=["+newValue+"]");
           this.AuditVector.addElement("[UserField1]=["+newValue+"]");
           dbDB.setUserField1(newValue);
         }
    }
    public String getUserField1(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='UserField1' AND TableName='tHCOPhysicianLU'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getUserField1();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getUserField1();
         }
        return myVal;
    }

    public void setUserField2(String newValue)
    {        this.setUserField2(newValue,this.UserSecurityID);

    }
    public String getUserField2()
    {
        return this.getUserField2(this.UserSecurityID);
    }

    public void setUserField2(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='UserField2' AND TableName='tHCOPhysicianLU'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {
                   this.AuditVector.addElement("[UserField2]=["+newValue+"]");
                   this.AuditVector.addElement("[UserField2]=["+newValue+"]");
                   dbDB.setUserField2(newValue);
               }
            }
        }
        else
        {
           this.AuditVector.addElement("[UserField2]=["+newValue+"]");
           this.AuditVector.addElement("[UserField2]=["+newValue+"]");
           dbDB.setUserField2(newValue);
         }
    }
    public String getUserField2(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='UserField2' AND TableName='tHCOPhysicianLU'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getUserField2();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getUserField2();
         }
        return myVal;
    }

    public void setUserField3(String newValue)
    {        this.setUserField3(newValue,this.UserSecurityID);

    }
    public String getUserField3()
    {
        return this.getUserField3(this.UserSecurityID);
    }

    public void setUserField3(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='UserField3' AND TableName='tHCOPhysicianLU'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {
                   this.AuditVector.addElement("[UserField3]=["+newValue+"]");
                   this.AuditVector.addElement("[UserField3]=["+newValue+"]");
                   dbDB.setUserField3(newValue);
               }
            }
        }
        else
        {
           this.AuditVector.addElement("[UserField3]=["+newValue+"]");
           this.AuditVector.addElement("[UserField3]=["+newValue+"]");
           dbDB.setUserField3(newValue);
         }
    }
    public String getUserField3(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='UserField3' AND TableName='tHCOPhysicianLU'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getUserField3();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getUserField3();
         }
        return myVal;
    }

    public void setUserField4(String newValue)
    {        this.setUserField4(newValue,this.UserSecurityID);

    }
    public String getUserField4()
    {
        return this.getUserField4(this.UserSecurityID);
    }

    public void setUserField4(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='UserField4' AND TableName='tHCOPhysicianLU'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {
                   this.AuditVector.addElement("[UserField4]=["+newValue+"]");
                   this.AuditVector.addElement("[UserField4]=["+newValue+"]");
                   dbDB.setUserField4(newValue);
               }
            }
        }
        else
        {
           this.AuditVector.addElement("[UserField4]=["+newValue+"]");
           this.AuditVector.addElement("[UserField4]=["+newValue+"]");
           dbDB.setUserField4(newValue);
         }
    }
    public String getUserField4(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='UserField4' AND TableName='tHCOPhysicianLU'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getUserField4();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getUserField4();
         }
        return myVal;
    }

    public void setUserField5(String newValue)
    {        this.setUserField5(newValue,this.UserSecurityID);

    }
    public String getUserField5()
    {
        return this.getUserField5(this.UserSecurityID);
    }

    public void setUserField5(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='UserField5' AND TableName='tHCOPhysicianLU'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {
                   this.AuditVector.addElement("[UserField5]=["+newValue+"]");
                   this.AuditVector.addElement("[UserField5]=["+newValue+"]");
                   dbDB.setUserField5(newValue);
               }
            }
        }
        else
        {
           this.AuditVector.addElement("[UserField5]=["+newValue+"]");
           this.AuditVector.addElement("[UserField5]=["+newValue+"]");
           dbDB.setUserField5(newValue);
         }
    }
    public String getUserField5(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='UserField5' AND TableName='tHCOPhysicianLU'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getUserField5();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getUserField5();
         }
        return myVal;
    }

    public void setComments(String newValue)
    {        this.setComments(newValue,this.UserSecurityID);

    }
    public String getComments()
    {
        return this.getComments(this.UserSecurityID);
    }

    public void setComments(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Comments' AND TableName='tHCOPhysicianLU'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {
                   this.AuditVector.addElement("[Comments]=["+newValue+"]");
                   this.AuditVector.addElement("[Comments]=["+newValue+"]");
                   dbDB.setComments(newValue);
               }
            }
        }
        else
        {
           this.AuditVector.addElement("[Comments]=["+newValue+"]");
           this.AuditVector.addElement("[Comments]=["+newValue+"]");
           dbDB.setComments(newValue);
         }
    }
    public String getComments(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Comments' AND TableName='tHCOPhysicianLU'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getComments();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getComments();
         }
        return myVal;
    }

    public void setSponsoringPractitionerPrimary(String newValue)
    {        this.setSponsoringPractitionerPrimary(newValue,this.UserSecurityID);

    }
    public String getSponsoringPractitionerPrimary()
    {
        return this.getSponsoringPractitionerPrimary(this.UserSecurityID);
    }

    public void setSponsoringPractitionerPrimary(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SponsoringPractitionerPrimary' AND TableName='tHCOPhysicianLU'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {
                   this.AuditVector.addElement("[SponsoringPractitionerPrimary]=["+newValue+"]");
                   this.AuditVector.addElement("[SponsoringPractitionerPrimary]=["+newValue+"]");
                   dbDB.setSponsoringPractitionerPrimary(newValue);
               }
            }
        }
        else
        {
           this.AuditVector.addElement("[SponsoringPractitionerPrimary]=["+newValue+"]");
           this.AuditVector.addElement("[SponsoringPractitionerPrimary]=["+newValue+"]");
           dbDB.setSponsoringPractitionerPrimary(newValue);
         }
    }
    public String getSponsoringPractitionerPrimary(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SponsoringPractitionerPrimary' AND TableName='tHCOPhysicianLU'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getSponsoringPractitionerPrimary();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getSponsoringPractitionerPrimary();
         }
        return myVal;
    }

    public void setSponsoringPractitionerSecondary(String newValue)
    {        this.setSponsoringPractitionerSecondary(newValue,this.UserSecurityID);

    }
    public String getSponsoringPractitionerSecondary()
    {
        return this.getSponsoringPractitionerSecondary(this.UserSecurityID);
    }

    public void setSponsoringPractitionerSecondary(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SponsoringPractitionerSecondary' AND TableName='tHCOPhysicianLU'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {
                   this.AuditVector.addElement("[SponsoringPractitionerSecondary]=["+newValue+"]");
                   this.AuditVector.addElement("[SponsoringPractitionerSecondary]=["+newValue+"]");
                   dbDB.setSponsoringPractitionerSecondary(newValue);
               }
            }
        }
        else
        {
           this.AuditVector.addElement("[SponsoringPractitionerSecondary]=["+newValue+"]");
           this.AuditVector.addElement("[SponsoringPractitionerSecondary]=["+newValue+"]");
           dbDB.setSponsoringPractitionerSecondary(newValue);
         }
    }
    public String getSponsoringPractitionerSecondary(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SponsoringPractitionerSecondary' AND TableName='tHCOPhysicianLU'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getSponsoringPractitionerSecondary();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getSponsoringPractitionerSecondary();
         }
        return myVal;
    }

    public void setRequiredFieldList(java.util.Hashtable requiredFieldList)
	 {

        this.requiredFieldList = requiredFieldList;
    }

    public java.util.Hashtable getRequiredFieldList()
    {
        return this.requiredFieldList;
    }
    public void setExpiredFieldList(java.util.Hashtable expiredFieldList)
    {
        this.expiredFieldList = expiredFieldList;
    }

    public void setEnglishFieldList(java.util.Hashtable englishFieldList)
    {
        this.englishFieldList = englishFieldList;
    }

    public java.util.Hashtable getExpiredFieldList()
    {
        return this.expiredFieldList;
    }
    public boolean isExpired(java.util.Date myDate, int daysVector)
    {
       return isExpired(myDate,new java.util.Date(),daysVector);
    }


    public boolean isRead(String fieldN, Integer GroupRefID)
    {
        boolean myVal = false;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='"+fieldN+"' AND TableName='tHCOPhysicianLU'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==1)
               {
                   myVal = true;
               }
            }
        }
        else
        {
           Integer iSecurityCheck = SecurityCheck.CheckItem("HCO1", GroupRefID);
           if (iSecurityCheck.intValue()>=1)
           {
                  myVal = true;
           }
        }
        return myVal;
    }


    public boolean isWrite(String fieldN, Integer GroupRefID)
    {
        boolean myVal = false;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='"+fieldN+"' AND TableName='tHCOPhysicianLU'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {
                   myVal = true;
               }
            }
        }
        else
        {
           Integer iSecurityCheck = SecurityCheck.CheckItem("HCO1", GroupRefID);
           if (iSecurityCheck.intValue()>=2)
           {
                  myVal = true;
           }
        }
        return myVal;
    }


    public boolean isExpired(java.util.Date myDate, java.util.Date refDate,int daysVector)
    {
        java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat("yyyy-MM-dd");
        boolean myVal = true;
        try
        {
            if (myDate.after(dbdf.parse("1800-01-10")))
            {
                if (myDate.after(dateChange(refDate,daysVector)))
                {
                    myVal=false;
                }
            }
            else
            {
                myVal = false;
            }
        }
        catch (Exception Datee)
        {
        }
        return myVal;
    }

    public java.util.Date dateChange(java.util.Date myD, int dayChange)
    {
        java.util.Calendar calendar = java.util.Calendar.getInstance();
        calendar.setTime(myD);
        calendar.add(java.util.Calendar.DATE, dayChange);
        java.util.Date resultAsDate = new java.util.Date(calendar.getTime().getTime());
        return resultAsDate;
    }

    public boolean isRequired(String fieldN)
    {
        return isRequired(fieldN, new Integer("1"));
    }

    public boolean isRequired(String fieldN, Integer GroupRefID)
    {
        boolean myVal = false;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='"+fieldN+"' AND TableName='tHCOPhysicianLU'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getRequiredID().intValue()==1)
               {
                   myVal = true;
               }
            }
        }
        else
        {
            if (this.getRequiredFieldList().containsKey(fieldN))
            {
                myVal = true;
            }
        }
        return myVal;
    }
    public boolean isExpiredCheck(String fieldN)
    {
        return isExpiredCheck(fieldN, new Integer ("1"));
    }

    public boolean isExpiredCheck(String fieldN, Integer GroupRefID)
    {
        boolean myVal = false;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='"+fieldN+"' AND TableName='tHCOPhysicianLU'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getTrackedID().intValue()==1)
               {
                   myVal = true;
               }
            }
        }
        else
        {
            if (this.getExpiredFieldList().containsKey(fieldN))
           {
                myVal = true;
            }
        }
        return myVal;
    }

    public void makeRequiredHash()
    {
       makeRequiredHash(new Integer(1));
    }

    public void makeRequiredHash(Integer GroupRefID)
    {

        java.util.Hashtable newHash = new java.util.Hashtable();
        if (runFieldSecurity)
        {
            java.util.Enumeration myKeys = englishFieldList.keys(); 
            while (myKeys.hasMoreElements()) 
            {
                 String myElement = (String) myKeys.nextElement();
                 bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='"+myElement+"' AND TableName='tHCOPhysicianLU'" ,"");
                 bltFieldSecurity        working_bltFieldSecurity;
                 ListElement         leCurrentElement;
                 Enumeration eList = myFSL.elements();
                 if (eList.hasMoreElements())
                 {
                     leCurrentElement    = (ListElement) eList.nextElement();
                     working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
                     if (working_bltFieldSecurity.getRequiredID().intValue()==1)
                     {
                         newHash.put(myElement,new Boolean(true));
                     }
                 }
            }
        }
        else
        {
            newHash.put("HCOID",new Boolean(true));
            newHash.put("PhysicianID",new Boolean(true));
            newHash.put("ControlTypeID",new Boolean(true));
        }
        this.setRequiredFieldList(newHash);
    }

    public void makeEnglishHash()
    {

        java.util.Hashtable newHash = new java.util.Hashtable();        newHash.put("UniqueCreateDate","Item Create Date");
        newHash.put("UniqueModifyDate","Item Modify Date");
        newHash.put("UniqueModifyComments","Item Modification Comments");
        newHash.put("HCOID","HCOID");
        newHash.put("PhysicianID","PhysicianID");
        newHash.put("ControlTypeID","ControlTypeID");
        newHash.put("CredentialingStatus","CredentialingStatus");
        newHash.put("ApplicationReceiveDate","ApplicationReceiveDate");
        newHash.put("ApplicationSignDate","ApplicationSignDate");
        newHash.put("CredentialingRequestDate","CredentialingRequestDate");
        newHash.put("InitialCredentialDate","InitialCredentialDate");
        newHash.put("LastCredentialDate","LastCredentialDate");
        newHash.put("NextRecredentialDate","NextRecredentialDate");
        newHash.put("EffectiveDate","EffectiveDate");
        newHash.put("TerminationDate","TerminationDate");
        newHash.put("Credentialer","Credentialer");
        newHash.put("Reviewer","Reviewer");
        newHash.put("Division","Division");
        newHash.put("District","District");
        newHash.put("ContractManager","ContractManager");
        newHash.put("StatusType","StatusType");
        newHash.put("UniqueProviderNumber","UniqueProviderNumber");
        newHash.put("LaborAndIndustryNumber","LaborAndIndustryNumber");
        newHash.put("UniqueBaselineNumber","UniqueBaselineNumber");
        newHash.put("HoneywellNumber","HoneywellNumber");
        newHash.put("ContDepartment","ContDepartment");
        newHash.put("CertificationNotes","CertificationNotes");
        newHash.put("TargetCPCDate","TargetCPCDate");
        newHash.put("MSOFileRequestDate","MSOFileRequestDate");
        newHash.put("MedicarePASDate","MedicarePASDate");
        newHash.put("UserField1","User Field 1");
        newHash.put("UserField2","User Field 2");
        newHash.put("UserField3","User Field 3");
        newHash.put("UserField4","User Field 4");
        newHash.put("UserField5","User Field 5");
        newHash.put("Comments","Extra Comments");
        newHash.put("SponsoringPractitionerPrimary","Sponsoring Practitioner Primary");
        newHash.put("SponsoringPractitionerSecondary","Sponsoring Practitioner Secondary");

        this.setEnglishFieldList(newHash);
    }
    public void makeExpiredHash()
    {

        java.util.Hashtable newHash = new java.util.Hashtable();
        this.setExpiredFieldList(newHash);
    }

    public java.util.Hashtable englishFieldList = new java.util.Hashtable();
    public java.util.Hashtable requiredFieldList = new java.util.Hashtable();
    public java.util.Hashtable expiredFieldList = new java.util.Hashtable();

    public boolean isComplete()
    {
	     boolean myVal = true;
	     java.util.Enumeration myKeys = this.getRequiredFieldList().keys(); 
	     while (myKeys.hasMoreElements()) 
	     {
	         String myElement = (String) myKeys.nextElement();
	         if (!isComplete(myElement))
	         {
	             myVal = false;
	         }
	     }
	     return myVal;
    }

    public String getEnglish(String fieldN)
    {
        String myVal = null;
        if (this.englishFieldList.containsKey(fieldN))
        {
            myVal = (String)this.englishFieldList.get(fieldN);
        }
        return myVal;
    }
    public java.util.Vector isComplete(int daysVector)
    {
	     boolean myVal = false;
	     java.util.Enumeration myKeys = this.getExpiredFieldList().keys(); 
	     java.util.Vector myExpiredList = new java.util.Vector(); 
	     while (myKeys.hasMoreElements()) 
	     {
	         String myElement = (String) myKeys.nextElement();
	         if (isExpired(myElement,daysVector))
	         {
	             myVal = true;
	             myExpiredList.addElement(myElement);
	         }
	     }
	     return myExpiredList;
    }

    public void setField(String fieldN, Object fieldV)
    {
     boolean myVal = false;
     try
     {
        java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat("yyyy-MM-dd");

        if (fieldN.equals("LookupID"))
        {
             this.setLookupID((Integer)fieldV);
        }

        else if (fieldN.equals("UniqueCreateDate"))
        {
            this.setUniqueCreateDate((java.util.Date)fieldV);
        }

        else if (fieldN.equals("UniqueModifyDate"))
        {
            this.setUniqueModifyDate((java.util.Date)fieldV);
        }

        else if (fieldN.equals("UniqueModifyComments"))
        {
            this.setUniqueModifyComments((String)fieldV);
        }

        else if (fieldN.equals("HCOID"))
        {
             this.setHCOID((Integer)fieldV);
        }

        else if (fieldN.equals("PhysicianID"))
        {
             this.setPhysicianID((Integer)fieldV);
        }

        else if (fieldN.equals("ControlTypeID"))
        {
             this.setControlTypeID((Integer)fieldV);
        }

        else if (fieldN.equals("CredentialingStatus"))
        {
             this.setCredentialingStatus((Integer)fieldV);
        }

        else if (fieldN.equals("ApplicationReceiveDate"))
        {
            this.setApplicationReceiveDate((java.util.Date)fieldV);
        }

        else if (fieldN.equals("ApplicationSignDate"))
        {
            this.setApplicationSignDate((java.util.Date)fieldV);
        }

        else if (fieldN.equals("CredentialingRequestDate"))
        {
            this.setCredentialingRequestDate((java.util.Date)fieldV);
        }

        else if (fieldN.equals("InitialCredentialDate"))
        {
            this.setInitialCredentialDate((java.util.Date)fieldV);
        }

        else if (fieldN.equals("LastCredentialDate"))
        {
            this.setLastCredentialDate((java.util.Date)fieldV);
        }

        else if (fieldN.equals("NextRecredentialDate"))
        {
            this.setNextRecredentialDate((java.util.Date)fieldV);
        }

        else if (fieldN.equals("EffectiveDate"))
        {
            this.setEffectiveDate((java.util.Date)fieldV);
        }

        else if (fieldN.equals("TerminationDate"))
        {
            this.setTerminationDate((java.util.Date)fieldV);
        }

        else if (fieldN.equals("Credentialer"))
        {
             this.setCredentialer((Integer)fieldV);
        }

        else if (fieldN.equals("Reviewer"))
        {
             this.setReviewer((Integer)fieldV);
        }

        else if (fieldN.equals("Division"))
        {
            this.setDivision((String)fieldV);
        }

        else if (fieldN.equals("District"))
        {
            this.setDistrict((String)fieldV);
        }

        else if (fieldN.equals("ContractManager"))
        {
             this.setContractManager((Integer)fieldV);
        }

        else if (fieldN.equals("StatusType"))
        {
            this.setStatusType((String)fieldV);
        }

        else if (fieldN.equals("UniqueProviderNumber"))
        {
            this.setUniqueProviderNumber((String)fieldV);
        }

        else if (fieldN.equals("LaborAndIndustryNumber"))
        {
            this.setLaborAndIndustryNumber((String)fieldV);
        }

        else if (fieldN.equals("UniqueBaselineNumber"))
        {
            this.setUniqueBaselineNumber((String)fieldV);
        }

        else if (fieldN.equals("HoneywellNumber"))
        {
            this.setHoneywellNumber((String)fieldV);
        }

        else if (fieldN.equals("ContDepartment"))
        {
            this.setContDepartment((String)fieldV);
        }

        else if (fieldN.equals("CertificationNotes"))
        {
            this.setCertificationNotes((String)fieldV);
        }

        else if (fieldN.equals("TargetCPCDate"))
        {
            this.setTargetCPCDate((java.util.Date)fieldV);
        }

        else if (fieldN.equals("MSOFileRequestDate"))
        {
            this.setMSOFileRequestDate((java.util.Date)fieldV);
        }

        else if (fieldN.equals("MedicarePASDate"))
        {
            this.setMedicarePASDate((java.util.Date)fieldV);
        }

        else if (fieldN.equals("UserField1"))
        {
            this.setUserField1((String)fieldV);
        }

        else if (fieldN.equals("UserField2"))
        {
            this.setUserField2((String)fieldV);
        }

        else if (fieldN.equals("UserField3"))
        {
            this.setUserField3((String)fieldV);
        }

        else if (fieldN.equals("UserField4"))
        {
            this.setUserField4((String)fieldV);
        }

        else if (fieldN.equals("UserField5"))
        {
            this.setUserField5((String)fieldV);
        }

        else if (fieldN.equals("Comments"))
        {
            this.setComments((String)fieldV);
        }

        else if (fieldN.equals("SponsoringPractitionerPrimary"))
        {
            this.setSponsoringPractitionerPrimary((String)fieldV);
        }

        else if (fieldN.equals("SponsoringPractitionerSecondary"))
        {
            this.setSponsoringPractitionerSecondary((String)fieldV);
        }

	    }
	    catch(Exception e2)
	    {}
	    //return myVal;
	}

    public String getFieldType(String fieldN)
    {
     String  myVal = null;
     try
     {
        java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat("yyyy-MM-dd");

        if (fieldN.equals("LookupID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("UniqueCreateDate"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("UniqueModifyDate"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("UniqueModifyComments"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("HCOID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("PhysicianID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("ControlTypeID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("CredentialingStatus"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("ApplicationReceiveDate"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("ApplicationSignDate"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("CredentialingRequestDate"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("InitialCredentialDate"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("LastCredentialDate"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("NextRecredentialDate"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("EffectiveDate"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("TerminationDate"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("Credentialer"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("Reviewer"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("Division"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("District"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("ContractManager"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("StatusType"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("UniqueProviderNumber"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("LaborAndIndustryNumber"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("UniqueBaselineNumber"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("HoneywellNumber"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("ContDepartment"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("CertificationNotes"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("TargetCPCDate"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("MSOFileRequestDate"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("MedicarePASDate"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("UserField1"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("UserField2"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("UserField3"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("UserField4"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("UserField5"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("Comments"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("SponsoringPractitionerPrimary"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("SponsoringPractitionerSecondary"))
        {
            myVal = "String";
        }

	    }
	    catch(Exception e2)
	    {}
	    return myVal;
	}

    public boolean isComplete(String fieldN)
    {
	     boolean myVal = false;
	     try
	     {
        java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat("yyyy-MM-dd");

	     if (fieldN.equals("LookupID"))

	     {
                    if (this.getLookupID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("UniqueCreateDate"))

	     {
                    if (this.getUniqueCreateDate().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("UniqueModifyDate"))

	     {
                    if (this.getUniqueModifyDate().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("UniqueModifyComments"))

	     {
                    if (this.getUniqueModifyComments().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("HCOID"))

	     {
                    if (this.getHCOID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("PhysicianID"))

	     {
                    if (this.getPhysicianID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("ControlTypeID"))

	     {
                    if (this.getControlTypeID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("CredentialingStatus"))

	     {
                    if (this.getCredentialingStatus().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("ApplicationReceiveDate"))

	     {
                    if (this.getApplicationReceiveDate().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("ApplicationSignDate"))

	     {
                    if (this.getApplicationSignDate().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("CredentialingRequestDate"))

	     {
                    if (this.getCredentialingRequestDate().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("InitialCredentialDate"))

	     {
                    if (this.getInitialCredentialDate().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("LastCredentialDate"))

	     {
                    if (this.getLastCredentialDate().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("NextRecredentialDate"))

	     {
                    if (this.getNextRecredentialDate().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("EffectiveDate"))

	     {
                    if (this.getEffectiveDate().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("TerminationDate"))

	     {
                    if (this.getTerminationDate().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("Credentialer"))

	     {
                    if (this.getCredentialer().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("Reviewer"))

	     {
                    if (this.getReviewer().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("Division"))

	     {
                    if (this.getDivision().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("District"))

	     {
                    if (this.getDistrict().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("ContractManager"))

	     {
                    if (this.getContractManager().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("StatusType"))

	     {
                    if (this.getStatusType().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("UniqueProviderNumber"))

	     {
                    if (this.getUniqueProviderNumber().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("LaborAndIndustryNumber"))

	     {
                    if (this.getLaborAndIndustryNumber().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("UniqueBaselineNumber"))

	     {
                    if (this.getUniqueBaselineNumber().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("HoneywellNumber"))

	     {
                    if (this.getHoneywellNumber().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("ContDepartment"))

	     {
                    if (this.getContDepartment().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("CertificationNotes"))

	     {
                    if (this.getCertificationNotes().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("TargetCPCDate"))

	     {
                    if (this.getTargetCPCDate().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("MSOFileRequestDate"))

	     {
                    if (this.getMSOFileRequestDate().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("MedicarePASDate"))

	     {
                    if (this.getMedicarePASDate().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("UserField1"))

	     {
                    if (this.getUserField1().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("UserField2"))

	     {
                    if (this.getUserField2().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("UserField3"))

	     {
                    if (this.getUserField3().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("UserField4"))

	     {
                    if (this.getUserField4().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("UserField5"))

	     {
                    if (this.getUserField5().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("Comments"))

	     {
                    if (this.getComments().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("SponsoringPractitionerPrimary"))

	     {
                    if (this.getSponsoringPractitionerPrimary().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("SponsoringPractitionerSecondary"))

	     {
                    if (this.getSponsoringPractitionerSecondary().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	    }
	    catch(Exception e2)
	    {}
	    return myVal;
	}

    public boolean isExpired(String fieldN,int daysVector)
    {
        return isExpired(fieldN,new java.util.Date(),daysVector);
    }

    public boolean isExpired(String fieldN,java.util.Date refDate,int daysVector)
    {
	     boolean myVal = false;
	     try
	     {
        java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat("yyyy-MM-dd");

        if (fieldN.equals("UniqueCreateDate"))

	     {
                    if (this.isExpired(this.getUniqueCreateDate(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

        if (fieldN.equals("UniqueModifyDate"))

	     {
                    if (this.isExpired(this.getUniqueModifyDate(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

        if (fieldN.equals("ApplicationReceiveDate"))

	     {
                    if (this.isExpired(this.getApplicationReceiveDate(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

        if (fieldN.equals("ApplicationSignDate"))

	     {
                    if (this.isExpired(this.getApplicationSignDate(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

        if (fieldN.equals("CredentialingRequestDate"))

	     {
                    if (this.isExpired(this.getCredentialingRequestDate(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

        if (fieldN.equals("InitialCredentialDate"))

	     {
                    if (this.isExpired(this.getInitialCredentialDate(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

        if (fieldN.equals("LastCredentialDate"))

	     {
                    if (this.isExpired(this.getLastCredentialDate(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

        if (fieldN.equals("NextRecredentialDate"))

	     {
                    if (this.isExpired(this.getNextRecredentialDate(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

        if (fieldN.equals("EffectiveDate"))

	     {
                    if (this.isExpired(this.getEffectiveDate(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

        if (fieldN.equals("TerminationDate"))

	     {
                    if (this.isExpired(this.getTerminationDate(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

        if (fieldN.equals("TargetCPCDate"))

	     {
                    if (this.isExpired(this.getTargetCPCDate(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

        if (fieldN.equals("MSOFileRequestDate"))

	     {
                    if (this.isExpired(this.getMSOFileRequestDate(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

        if (fieldN.equals("MedicarePASDate"))

	     {
                    if (this.isExpired(this.getMedicarePASDate(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

	    }
	    catch(Exception e2)
	    {}
	    return myVal;
	}

    public java.util.Date getDateField(String fieldN)
    {
	     java.util.Date myVal = null;
	     try
	     {
        java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat("yyyy-MM-dd");

        if (fieldN.equals("UniqueCreateDate"))

	     {
             myVal = this.getUniqueCreateDate();
        }

        if (fieldN.equals("UniqueModifyDate"))

	     {
             myVal = this.getUniqueModifyDate();
        }

        if (fieldN.equals("ApplicationReceiveDate"))

	     {
             myVal = this.getApplicationReceiveDate();
        }

        if (fieldN.equals("ApplicationSignDate"))

	     {
             myVal = this.getApplicationSignDate();
        }

        if (fieldN.equals("CredentialingRequestDate"))

	     {
             myVal = this.getCredentialingRequestDate();
        }

        if (fieldN.equals("InitialCredentialDate"))

	     {
             myVal = this.getInitialCredentialDate();
        }

        if (fieldN.equals("LastCredentialDate"))

	     {
             myVal = this.getLastCredentialDate();
        }

        if (fieldN.equals("NextRecredentialDate"))

	     {
             myVal = this.getNextRecredentialDate();
        }

        if (fieldN.equals("EffectiveDate"))

	     {
             myVal = this.getEffectiveDate();
        }

        if (fieldN.equals("TerminationDate"))

	     {
             myVal = this.getTerminationDate();
        }

        if (fieldN.equals("TargetCPCDate"))

	     {
             myVal = this.getTargetCPCDate();
        }

        if (fieldN.equals("MSOFileRequestDate"))

	     {
             myVal = this.getMSOFileRequestDate();
        }

        if (fieldN.equals("MedicarePASDate"))

	     {
             myVal = this.getMedicarePASDate();
        }

	    }
	    catch(Exception e2)
	    {}
	    return myVal;
	}
	public boolean runFieldSecurity = false;
	public Integer UserSecurityID = null;

}    // End of bltHCOPhysicianLU class definition
