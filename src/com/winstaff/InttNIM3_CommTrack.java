

package com.winstaff;

import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public interface InttNIM3_CommTrack extends dbTableInterface
{

    public void setCommTrackID(Integer newValue);
    public Integer getCommTrackID();
    public void setUniqueCreateDate(Date newValue);
    public Date getUniqueCreateDate();
    public void setUniqueModifyDate(Date newValue);
    public Date getUniqueModifyDate();
    public void setUniqueModifyComments(String newValue);
    public String getUniqueModifyComments();
    public void setCaseID(Integer newValue);
    public Integer getCaseID();
    public void setEncounterID(Integer newValue);
    public Integer getEncounterID();
    public void setReferralID(Integer newValue);
    public Integer getReferralID();
    public void setServiceID(Integer newValue);
    public Integer getServiceID();
    public void setIntUserID(Integer newValue);
    public Integer getIntUserID();
    public void setExtUserID(Integer newValue);
    public Integer getExtUserID();
    public void setCommTypeID(Integer newValue);
    public Integer getCommTypeID();
    public void setCommStart(Date newValue);
    public Date getCommStart();
    public void setCommEnd(Date newValue);
    public Date getCommEnd();
    public void setMessageText(String newValue);
    public String getMessageText();
    public void setMessageName(String newValue);
    public String getMessageName();
    public void setMessageCompany(String newValue);
    public String getMessageCompany();
    public void setMessageEmail(String newValue);
    public String getMessageEmail();
    public void setMessagePhone(String newValue);
    public String getMessagePhone();
    public void setMessageFax(String newValue);
    public String getMessageFax();
    public void setAlertStatusCode(Integer newValue);
    public Integer getAlertStatusCode();
    public void setComments(String newValue);
    public String getComments();
    public void setCommReferenceID(Integer newValue);
    public Integer getCommReferenceID();
    public void setLink_UserID(Integer newValue);
    public Integer getLink_UserID();
    public void setMessageSubject(String newValue);
    public String getMessageSubject();
}    // End of bltNIM3_CommTrack class definition
