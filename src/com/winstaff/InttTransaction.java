package com.winstaff;


import com.winstaff.dbtTransaction;
import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public interface InttTransaction extends dbTableInterface
{

    public void setTransactionID(Integer newValue);
    public Integer getTransactionID();
    public void setUniqueCreateDate(Date newValue);
    public Date getUniqueCreateDate();
    public void setUniqueModifyDate(Date newValue);
    public Date getUniqueModifyDate();
    public void setUniqueModifyComments(String newValue);
    public String getUniqueModifyComments();
    public void setTransactionDate(Date newValue);
    public Date getTransactionDate();
    public void setAction(String newValue);
    public String getAction();
    public void setModifier(String newValue);
    public String getModifier();
    public void setComments(String newValue);
    public String getComments();
}    // End of bltTransaction class definition
