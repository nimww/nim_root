package com.winstaff;
/*
 * bltOtherCertification_List.java
 *
 * Created: Wed Apr 02 15:09:01 PST 2003
 */



import java.sql.*;
import com.winstaff.dbtOtherCertification;
import com.winstaff.ListElement;


/**
 *
 * @author  Scott Ellis via listClassGenCode
 * @version 2.5
 */
public class bltOtherCertification_List extends db_BaseRelationTable
{
    /** Default constructor
    */    
    public bltOtherCertification_List ( Integer iPhysicianID )
    {
        super( "tOtherCertification", "OtherCertID", "OtherCertID", "PhysicianID", iPhysicianID );
    }   // End of bltOtherCertification_List()
    

    public void setField(String fieldN, Object fieldV)
    {DebugLogger.println("setField Method not implemented yet in classbltOtherCertification_List!");}

    public String getFieldType(String fieldN)
    {return null;}

    public bltOtherCertification_List ( Integer iPhysicianID, String extraWhere, String OrderBy )
    {
        super( "tOtherCertification", "OtherCertID", "OtherCertID", "PhysicianID", iPhysicianID, extraWhere,OrderBy );
    }   // End of bltOtherCertification_List()
     /** Method pull information from the result set and load into class attributes.
    * @param sFieldList Comma delimited list of fields for which data has been retrieved.
    * @param rs Result set from which to pull the data from.
    * @throws SQLException Passes on SQLException
    */      
    protected void transferData( ResultSet rs ) throws SQLException
    {
        while ( rs.next()  )
        {
            Integer         iValue              = new Integer( rs.getString( sLinkFieldName ) );
            Integer         iLinkRecordID       = new Integer( rs.getString( sUniqueIDFieldName ) );
            Object          oReferenceObject;
            ListElement     leNewElement;
            
            oReferenceObject    = new bltOtherCertification( iValue );
            
            leNewElement        = new ListElement( iLinkRecordID, oReferenceObject );
            
            vList.add( leNewElement );

        }
    }   // End of transferData()
}   // End of bltOtherCertification_List class

