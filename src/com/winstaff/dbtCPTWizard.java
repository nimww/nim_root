

package com.winstaff;

import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public class dbtCPTWizard extends Object implements InttCPTWizard
{

        db_NewBase    dbnbDB;

    public dbtCPTWizard()
    {
        dbnbDB = new db_NewBase( "tCPTWizard", "CPTWizardID" );

    }    // End of default constructor

    public dbtCPTWizard( Integer iNewID )
    {
        dbnbDB = new db_NewBase( "tCPTWizard", "CPTWizardID" );
        setUniqueID( iNewID );
    }    // End of Constructor knowing an ID

    public void commitData() throws SQLException
    {
            //no longer trapping/ignoring errors 2011-03-17
            dbnbDB.commitData();
    }    // End of commitData()

    public void commitDataForced() throws SQLException
    {
        dbnbDB.commitDataForced();
    }    // End of commitData()

    public Integer getUniqueID()
    {
        return dbnbDB.getUniqueID();
    }    // End of getUniqueID()

    public void setUniqueID( Integer iNewID )
    {
        dbnbDB.setUniqueID( iNewID );
    }    // End of setUniqueID()

    public void setModifyComments( String sNew )
    {
        dbnbDB.setModifyComments( sNew );
    }    // End of setUniqueID()


    public void setCPTWizardID(Integer newValue)
    {
                dbnbDB.setFieldData( "CPTWizardID", newValue.toString() );
    }

    public Integer getCPTWizardID()
    {
        String           sValue = dbnbDB.getFieldData( "CPTWizardID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setField(String fieldN, Object fieldV)
    {DebugLogger.println("setField Method not implemented yet in class!");}

    public String getFieldType(String fieldN)
    {return null;}


    public void setUniqueCreateDate(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "UniqueCreateDate", formatter.format( newValue ) );
    }

    public Date getUniqueCreateDate()
    {
        String           sValue = dbnbDB.getFieldData( "UniqueCreateDate" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setUniqueModifyDate(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "UniqueModifyDate", formatter.format( newValue ) );
    }

    public Date getUniqueModifyDate()
    {
        String           sValue = dbnbDB.getFieldData( "UniqueModifyDate" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setUniqueModifyComments(String newValue)
    {
                dbnbDB.setFieldData( "UniqueModifyComments", newValue.toString() );
    }

    public String getUniqueModifyComments()
    {
        String           sValue = dbnbDB.getFieldData( "UniqueModifyComments" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setModality(String newValue)
    {
                dbnbDB.setFieldData( "Modality", newValue.toString() );
    }

    public String getModality()
    {
        String           sValue = dbnbDB.getFieldData( "Modality" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setSpecial(String newValue)
    {
                dbnbDB.setFieldData( "Special", newValue.toString() );
    }

    public String getSpecial()
    {
        String           sValue = dbnbDB.getFieldData( "Special" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setContrast(String newValue)
    {
                dbnbDB.setFieldData( "Contrast", newValue.toString() );
    }

    public String getContrast()
    {
        String           sValue = dbnbDB.getFieldData( "Contrast" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setBodyPart(String newValue)
    {
                dbnbDB.setFieldData( "BodyPart", newValue.toString() );
    }

    public String getBodyPart()
    {
        String           sValue = dbnbDB.getFieldData( "BodyPart" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setOrientation(String newValue)
    {
                dbnbDB.setFieldData( "Orientation", newValue.toString() );
    }

    public String getOrientation()
    {
        String           sValue = dbnbDB.getFieldData( "Orientation" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setCPT1(String newValue)
    {
                dbnbDB.setFieldData( "CPT1", newValue.toString() );
    }

    public String getCPT1()
    {
        String           sValue = dbnbDB.getFieldData( "CPT1" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setBP1(String newValue)
    {
                dbnbDB.setFieldData( "BP1", newValue.toString() );
    }

    public String getBP1()
    {
        String           sValue = dbnbDB.getFieldData( "BP1" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setCPT2(String newValue)
    {
                dbnbDB.setFieldData( "CPT2", newValue.toString() );
    }

    public String getCPT2()
    {
        String           sValue = dbnbDB.getFieldData( "CPT2" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setBP2(String newValue)
    {
                dbnbDB.setFieldData( "BP2", newValue.toString() );
    }

    public String getBP2()
    {
        String           sValue = dbnbDB.getFieldData( "BP2" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setCPT3(String newValue)
    {
                dbnbDB.setFieldData( "CPT3", newValue.toString() );
    }

    public String getCPT3()
    {
        String           sValue = dbnbDB.getFieldData( "CPT3" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setBP3(String newValue)
    {
                dbnbDB.setFieldData( "BP3", newValue.toString() );
    }

    public String getBP3()
    {
        String           sValue = dbnbDB.getFieldData( "BP3" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setCPT4(String newValue)
    {
                dbnbDB.setFieldData( "CPT4", newValue.toString() );
    }

    public String getCPT4()
    {
        String           sValue = dbnbDB.getFieldData( "CPT4" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setBP4(String newValue)
    {
                dbnbDB.setFieldData( "BP4", newValue.toString() );
    }

    public String getBP4()
    {
        String           sValue = dbnbDB.getFieldData( "BP4" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setCPT5(String newValue)
    {
                dbnbDB.setFieldData( "CPT5", newValue.toString() );
    }

    public String getCPT5()
    {
        String           sValue = dbnbDB.getFieldData( "CPT5" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setBP5(String newValue)
    {
                dbnbDB.setFieldData( "BP5", newValue.toString() );
    }

    public String getBP5()
    {
        String           sValue = dbnbDB.getFieldData( "BP5" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setAllowNIM(Integer newValue)
    {
                dbnbDB.setFieldData( "AllowNIM", newValue.toString() );
    }

    public Integer getAllowNIM()
    {
        String           sValue = dbnbDB.getFieldData( "AllowNIM" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setAllowNID(Integer newValue)
    {
                dbnbDB.setFieldData( "AllowNID", newValue.toString() );
    }

    public Integer getAllowNID()
    {
        String           sValue = dbnbDB.getFieldData( "AllowNID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

}    // End of bltCPTWizard class definition
