package com.winstaff;
import java.awt.*;
import java.util.Enumeration;


public class SecurityCheck extends java.lang.Object
{

	public SecurityCheck()
	{
	}

	public static Integer CheckItem (String ItemName,Integer iSecurityGroupID)
	{
	
		Integer myVal = null;
		if (ConfigurationInformation.bSecurityByGroup)
		{
			bltGroupSecurity_List        bltGroupSecurity_List        =    new    bltGroupSecurity_List(iSecurityGroupID,"ItemName='"+ItemName+"'","");
			bltGroupSecurity        working_bltGroupSecurity;
			ListElement         leCurrentElement;
			Enumeration eList = bltGroupSecurity_List.elements();
			if (eList.hasMoreElements())
			{
				leCurrentElement    = (ListElement) eList.nextElement();
				working_bltGroupSecurity  = (bltGroupSecurity) leCurrentElement.getObject();
				myVal = working_bltGroupSecurity.getAccessLevel();
			}
			else
			{
				myVal = new Integer("0");
			}
		}
		else
		{
			myVal = new Integer("2");
		}
		return myVal;
       }


}
