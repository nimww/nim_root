

package com.winstaff;

import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;
import java.util.Enumeration;

public class bltNIM3_PayerMaster extends Object implements InttNIM3_PayerMaster
{

    dbtNIM3_PayerMaster    dbDB;

    public bltNIM3_PayerMaster()
    {
        dbDB = new dbtNIM3_PayerMaster();
        dbDB.setUniqueCreateDate(new java.util.Date());
        makeRequiredHash();
        makeExpiredHash();
        makeEnglishHash();

    }    // End of default constructor

    public bltNIM3_PayerMaster( Integer iNewID )
    {        dbDB = new dbtNIM3_PayerMaster( iNewID );
        makeRequiredHash();
        makeExpiredHash();
        makeEnglishHash();
        iSecurityCheck = new Integer(2);
    }    // End of Constructor knowing an ID


    public bltNIM3_PayerMaster( Integer iNewID, Integer iGroupSecurityID )
    {
        this.GroupSecurityInit(iGroupSecurityID);
        dbDB = new dbtNIM3_PayerMaster( iNewID );
        makeExpiredHash();
        makeEnglishHash();
        makeRequiredHash(iGroupSecurityID);
    }    // End of Constructor knowing an ID

    public bltNIM3_PayerMaster( Integer iGroupSecurityID, boolean isTrue )
    {
        this.GroupSecurityInit(iGroupSecurityID);
        dbDB = new dbtNIM3_PayerMaster(  );
        makeExpiredHash();
        makeEnglishHash();
        makeRequiredHash(iGroupSecurityID);
    }    // End of Security unknown ID

    public void GroupSecurityInit( Integer iGroupSecurityID )
    {
        bltSecurityGroupMaster mySGM = new bltSecurityGroupMaster(iGroupSecurityID);
        this.UserSecurityID = iGroupSecurityID;
        if (mySGM.getRunFieldSecurityID().intValue()==1)
        {
            this.runFieldSecurity = true;
            makeRequiredHash(iGroupSecurityID);
        }
        iSecurityCheck = SecurityCheck.CheckItem("nim1", iGroupSecurityID);
    }  

    public EventReferenceType getEventReference()
    {
        return EventUtils.getEventReference("tNIM3_PayerMaster", this.getUniqueID());
    }  

    public String convertString2LogString(String normalString) 
    {
        String myVal = "";
        for (int i=0;i<normalString.length();i++)
        {
            if ( (normalString.charAt(i)!='\r')&&(normalString.charAt(i)!='\n')&&(normalString.charAt(i)!='\t') )
            {
                myVal+=normalString.charAt(i);
            }
            else
            {
                myVal+="<NLT>";
            }
        }
        return myVal;
    }
    public void runAudit()
    {
        String AuditString = new java.util.Date()+" \t " ;  
        AuditString += " tNIM3_PayerMaster "; 
        AuditString += " PayerID ="+this.getUniqueID(); 

        for (int i=0;i<this.AuditVector.size();i++)
        {
            AuditString+="\t"+convertString2LogString((String)this.AuditVector.elementAt(i));
        }
        AuditLogger.printLine(AuditString);
    }
    public void commitData() throws SQLException
    {
        runAudit();
        dbDB.setModifyComments(this.getUniqueModifyComments());
        dbDB.setUniqueModifyDate(new java.util.Date());
        dbDB.commitData();
        DataControlUtils.dataChanged("tNIM3_PayerMaster",this.getUniqueID() ,this.AuditVector.size(), "PayerID");
        this.AuditVector.removeAllElements();
    }    // End of commitData()

    public void commitDataForced() throws SQLException
    {
        dbDB.commitDataForced();
    }    // End of commitData()

    public Integer getUniqueID()
    {
        return dbDB.getUniqueID();
    }    // End of getUniqueID()

    public void setUniqueID( Integer iNewID )
    {
        dbDB.setUniqueID( iNewID );
    }    // End of setUniqueID()


    public void setPayerID(Integer newValue)
    {
        dbDB.setPayerID(newValue);
    }

    public Integer getPayerID()
    {
        return dbDB.getPayerID();
    }

    java.util.Vector AuditVector = new java.util.Vector();

    public void setAutoAuth(String newValue)
    {
        this.setAutoAuth(newValue,this.UserSecurityID);


    }
    public String getAutoAuth()
    {
        return this.getAutoAuth(this.UserSecurityID);
    }

    public void setAutoAuth(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AutoAuth' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[AutoAuth]=["+newValue+"]");
                   dbDB.setAutoAuth(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[AutoAuth]=["+newValue+"]");
           dbDB.setAutoAuth(newValue);
         }
    }
    public String getAutoAuth(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AutoAuth' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getAutoAuth();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getAutoAuth();
         }
        return myVal;
    }

    public void setUniqueCreateDate(Date newValue)
    {
        this.setUniqueCreateDate(newValue,this.UserSecurityID);


    }
    public Date getUniqueCreateDate()
    {
        return this.getUniqueCreateDate(this.UserSecurityID);
    }

    public void setUniqueCreateDate(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='UniqueCreateDate' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[UniqueCreateDate]=["+newValue+"]");
                   dbDB.setUniqueCreateDate(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[UniqueCreateDate]=["+newValue+"]");
           dbDB.setUniqueCreateDate(newValue);
         }
    }
    public Date getUniqueCreateDate(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='UniqueCreateDate' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getUniqueCreateDate();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getUniqueCreateDate();
         }
        return myVal;
    }

    public void setUniqueModifyDate(Date newValue)
    {
        this.setUniqueModifyDate(newValue,this.UserSecurityID);


    }
    public Date getUniqueModifyDate()
    {
        return this.getUniqueModifyDate(this.UserSecurityID);
    }

    public void setUniqueModifyDate(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='UniqueModifyDate' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[UniqueModifyDate]=["+newValue+"]");
                   dbDB.setUniqueModifyDate(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[UniqueModifyDate]=["+newValue+"]");
           dbDB.setUniqueModifyDate(newValue);
         }
    }
    public Date getUniqueModifyDate(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='UniqueModifyDate' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getUniqueModifyDate();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getUniqueModifyDate();
         }
        return myVal;
    }

    public void setUniqueModifyComments(String newValue)
    {
        this.setUniqueModifyComments(newValue,this.UserSecurityID);


    }
    public String getUniqueModifyComments()
    {
        return this.getUniqueModifyComments(this.UserSecurityID);
    }

    public void setUniqueModifyComments(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='UniqueModifyComments' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[UniqueModifyComments]=["+newValue+"]");
                   dbDB.setUniqueModifyComments(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[UniqueModifyComments]=["+newValue+"]");
           dbDB.setUniqueModifyComments(newValue);
         }
    }
    public String getUniqueModifyComments(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='UniqueModifyComments' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getUniqueModifyComments();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getUniqueModifyComments();
         }
        return myVal;
    }

    public void setPayerName(String newValue)
    {
        this.setPayerName(newValue,this.UserSecurityID);


    }
    public String getPayerName()
    {
        return this.getPayerName(this.UserSecurityID);
    }

    public void setPayerName(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PayerName' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[PayerName]=["+newValue+"]");
                   dbDB.setPayerName(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[PayerName]=["+newValue+"]");
           dbDB.setPayerName(newValue);
         }
    }
    public String getPayerName(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PayerName' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPayerName();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPayerName();
         }
        return myVal;
    }

    public void setContactName(String newValue)
    {
        this.setContactName(newValue,this.UserSecurityID);


    }
    public String getContactName()
    {
        return this.getContactName(this.UserSecurityID);
    }

    public void setContactName(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ContactName' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[ContactName]=["+newValue+"]");
                   dbDB.setContactName(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[ContactName]=["+newValue+"]");
           dbDB.setContactName(newValue);
         }
    }
    public String getContactName(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ContactName' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getContactName();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getContactName();
         }
        return myVal;
    }

    public void setBillingAddress1(String newValue)
    {
        this.setBillingAddress1(newValue,this.UserSecurityID);


    }
    public String getBillingAddress1()
    {
        return this.getBillingAddress1(this.UserSecurityID);
    }

    public void setBillingAddress1(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='BillingAddress1' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[BillingAddress1]=["+newValue+"]");
                   dbDB.setBillingAddress1(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[BillingAddress1]=["+newValue+"]");
           dbDB.setBillingAddress1(newValue);
         }
    }
    public String getBillingAddress1(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='BillingAddress1' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getBillingAddress1();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getBillingAddress1();
         }
        return myVal;
    }

    public void setBillingAddress2(String newValue)
    {
        this.setBillingAddress2(newValue,this.UserSecurityID);


    }
    public String getBillingAddress2()
    {
        return this.getBillingAddress2(this.UserSecurityID);
    }

    public void setBillingAddress2(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='BillingAddress2' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[BillingAddress2]=["+newValue+"]");
                   dbDB.setBillingAddress2(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[BillingAddress2]=["+newValue+"]");
           dbDB.setBillingAddress2(newValue);
         }
    }
    public String getBillingAddress2(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='BillingAddress2' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getBillingAddress2();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getBillingAddress2();
         }
        return myVal;
    }

    public void setBillingCity(String newValue)
    {
        this.setBillingCity(newValue,this.UserSecurityID);


    }
    public String getBillingCity()
    {
        return this.getBillingCity(this.UserSecurityID);
    }

    public void setBillingCity(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='BillingCity' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[BillingCity]=["+newValue+"]");
                   dbDB.setBillingCity(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[BillingCity]=["+newValue+"]");
           dbDB.setBillingCity(newValue);
         }
    }
    public String getBillingCity(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='BillingCity' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getBillingCity();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getBillingCity();
         }
        return myVal;
    }

    public void setBillingStateID(Integer newValue)
    {
        this.setBillingStateID(newValue,this.UserSecurityID);


    }
    public Integer getBillingStateID()
    {
        return this.getBillingStateID(this.UserSecurityID);
    }

    public void setBillingStateID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='BillingStateID' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[BillingStateID]=["+newValue+"]");
                   dbDB.setBillingStateID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[BillingStateID]=["+newValue+"]");
           dbDB.setBillingStateID(newValue);
         }
    }
    public Integer getBillingStateID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='BillingStateID' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getBillingStateID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getBillingStateID();
         }
        return myVal;
    }

    public void setBillingZIP(String newValue)
    {
        this.setBillingZIP(newValue,this.UserSecurityID);


    }
    public String getBillingZIP()
    {
        return this.getBillingZIP(this.UserSecurityID);
    }

    public void setBillingZIP(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='BillingZIP' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[BillingZIP]=["+newValue+"]");
                   dbDB.setBillingZIP(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[BillingZIP]=["+newValue+"]");
           dbDB.setBillingZIP(newValue);
         }
    }
    public String getBillingZIP(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='BillingZIP' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getBillingZIP();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getBillingZIP();
         }
        return myVal;
    }

    public void setBillingPhone(String newValue)
    {
        this.setBillingPhone(newValue,this.UserSecurityID);


    }
    public String getBillingPhone()
    {
        return this.getBillingPhone(this.UserSecurityID);
    }

    public void setBillingPhone(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='BillingPhone' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[BillingPhone]=["+newValue+"]");
                   dbDB.setBillingPhone(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[BillingPhone]=["+newValue+"]");
           dbDB.setBillingPhone(newValue);
         }
    }
    public String getBillingPhone(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='BillingPhone' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getBillingPhone();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getBillingPhone();
         }
        return myVal;
    }

    public void setBillingFax(String newValue)
    {
        this.setBillingFax(newValue,this.UserSecurityID);


    }
    public String getBillingFax()
    {
        return this.getBillingFax(this.UserSecurityID);
    }

    public void setBillingFax(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='BillingFax' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[BillingFax]=["+newValue+"]");
                   dbDB.setBillingFax(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[BillingFax]=["+newValue+"]");
           dbDB.setBillingFax(newValue);
         }
    }
    public String getBillingFax(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='BillingFax' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getBillingFax();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getBillingFax();
         }
        return myVal;
    }

    public void setBillingEmail(String newValue)
    {
        this.setBillingEmail(newValue,this.UserSecurityID);


    }
    public String getBillingEmail()
    {
        return this.getBillingEmail(this.UserSecurityID);
    }

    public void setBillingEmail(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='BillingEmail' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[BillingEmail]=["+newValue+"]");
                   dbDB.setBillingEmail(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[BillingEmail]=["+newValue+"]");
           dbDB.setBillingEmail(newValue);
         }
    }
    public String getBillingEmail(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='BillingEmail' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getBillingEmail();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getBillingEmail();
         }
        return myVal;
    }

    public void setComments(String newValue)
    {
        this.setComments(newValue,this.UserSecurityID);


    }
    public String getComments()
    {
        return this.getComments(this.UserSecurityID);
    }

    public void setComments(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Comments' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[Comments]=["+newValue+"]");
                   dbDB.setComments(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[Comments]=["+newValue+"]");
           dbDB.setComments(newValue);
         }
    }
    public String getComments(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Comments' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getComments();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getComments();
         }
        return myVal;
    }

    public void setBillingEntityID(Integer newValue)
    {
        this.setBillingEntityID(newValue,this.UserSecurityID);


    }
    public Integer getBillingEntityID()
    {
        return this.getBillingEntityID(this.UserSecurityID);
    }

    public void setBillingEntityID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='BillingEntityID' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[BillingEntityID]=["+newValue+"]");
                   dbDB.setBillingEntityID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[BillingEntityID]=["+newValue+"]");
           dbDB.setBillingEntityID(newValue);
         }
    }
    public Integer getBillingEntityID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='BillingEntityID' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getBillingEntityID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getBillingEntityID();
         }
        return myVal;
    }

    public void setPayerTypeID(Integer newValue)
    {
        this.setPayerTypeID(newValue,this.UserSecurityID);


    }
    public Integer getPayerTypeID()
    {
        return this.getPayerTypeID(this.UserSecurityID);
    }

    public void setPayerTypeID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PayerTypeID' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[PayerTypeID]=["+newValue+"]");
                   dbDB.setPayerTypeID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[PayerTypeID]=["+newValue+"]");
           dbDB.setPayerTypeID(newValue);
         }
    }
    public Integer getPayerTypeID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PayerTypeID' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPayerTypeID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPayerTypeID();
         }
        return myVal;
    }

    public void setImportantNotes(String newValue)
    {
        this.setImportantNotes(newValue,this.UserSecurityID);


    }
    public String getImportantNotes()
    {
        return this.getImportantNotes(this.UserSecurityID);
    }

    public void setImportantNotes(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ImportantNotes' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[ImportantNotes]=["+newValue+"]");
                   dbDB.setImportantNotes(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[ImportantNotes]=["+newValue+"]");
           dbDB.setImportantNotes(newValue);
         }
    }
    public String getImportantNotes(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ImportantNotes' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getImportantNotes();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getImportantNotes();
         }
        return myVal;
    }

    public void setPayableTo(String newValue)
    {
        this.setPayableTo(newValue,this.UserSecurityID);


    }
    public String getPayableTo()
    {
        return this.getPayableTo(this.UserSecurityID);
    }

    public void setPayableTo(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PayableTo' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[PayableTo]=["+newValue+"]");
                   dbDB.setPayableTo(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[PayableTo]=["+newValue+"]");
           dbDB.setPayableTo(newValue);
         }
    }
    public String getPayableTo(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PayableTo' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPayableTo();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPayableTo();
         }
        return myVal;
    }

    public void setPrice_Mod_MRI_WO(Double newValue)
    {
        this.setPrice_Mod_MRI_WO(newValue,this.UserSecurityID);


    }
    public Double getPrice_Mod_MRI_WO()
    {
        return this.getPrice_Mod_MRI_WO(this.UserSecurityID);
    }

    public void setPrice_Mod_MRI_WO(Double newValue, Integer GroupRefID)
    {
        Double myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Price_Mod_MRI_WO' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[Price_Mod_MRI_WO]=["+newValue+"]");
                   dbDB.setPrice_Mod_MRI_WO(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[Price_Mod_MRI_WO]=["+newValue+"]");
           dbDB.setPrice_Mod_MRI_WO(newValue);
         }
    }
    public Double getPrice_Mod_MRI_WO(Integer GroupRefID)
    {
        Double myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Price_Mod_MRI_WO' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPrice_Mod_MRI_WO();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPrice_Mod_MRI_WO();
         }
        return myVal;
    }

    public void setPrice_Mod_MRI_W(Double newValue)
    {
        this.setPrice_Mod_MRI_W(newValue,this.UserSecurityID);


    }
    public Double getPrice_Mod_MRI_W()
    {
        return this.getPrice_Mod_MRI_W(this.UserSecurityID);
    }

    public void setPrice_Mod_MRI_W(Double newValue, Integer GroupRefID)
    {
        Double myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Price_Mod_MRI_W' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[Price_Mod_MRI_W]=["+newValue+"]");
                   dbDB.setPrice_Mod_MRI_W(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[Price_Mod_MRI_W]=["+newValue+"]");
           dbDB.setPrice_Mod_MRI_W(newValue);
         }
    }
    public Double getPrice_Mod_MRI_W(Integer GroupRefID)
    {
        Double myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Price_Mod_MRI_W' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPrice_Mod_MRI_W();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPrice_Mod_MRI_W();
         }
        return myVal;
    }

    public void setPrice_Mod_MRI_WWO(Double newValue)
    {
        this.setPrice_Mod_MRI_WWO(newValue,this.UserSecurityID);


    }
    public Double getPrice_Mod_MRI_WWO()
    {
        return this.getPrice_Mod_MRI_WWO(this.UserSecurityID);
    }

    public void setPrice_Mod_MRI_WWO(Double newValue, Integer GroupRefID)
    {
        Double myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Price_Mod_MRI_WWO' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[Price_Mod_MRI_WWO]=["+newValue+"]");
                   dbDB.setPrice_Mod_MRI_WWO(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[Price_Mod_MRI_WWO]=["+newValue+"]");
           dbDB.setPrice_Mod_MRI_WWO(newValue);
         }
    }
    public Double getPrice_Mod_MRI_WWO(Integer GroupRefID)
    {
        Double myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Price_Mod_MRI_WWO' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPrice_Mod_MRI_WWO();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPrice_Mod_MRI_WWO();
         }
        return myVal;
    }

    public void setPrice_Mod_CT_WO(Double newValue)
    {
        this.setPrice_Mod_CT_WO(newValue,this.UserSecurityID);


    }
    public Double getPrice_Mod_CT_WO()
    {
        return this.getPrice_Mod_CT_WO(this.UserSecurityID);
    }

    public void setPrice_Mod_CT_WO(Double newValue, Integer GroupRefID)
    {
        Double myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Price_Mod_CT_WO' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[Price_Mod_CT_WO]=["+newValue+"]");
                   dbDB.setPrice_Mod_CT_WO(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[Price_Mod_CT_WO]=["+newValue+"]");
           dbDB.setPrice_Mod_CT_WO(newValue);
         }
    }
    public Double getPrice_Mod_CT_WO(Integer GroupRefID)
    {
        Double myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Price_Mod_CT_WO' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPrice_Mod_CT_WO();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPrice_Mod_CT_WO();
         }
        return myVal;
    }

    public void setPrice_Mod_CT_W(Double newValue)
    {
        this.setPrice_Mod_CT_W(newValue,this.UserSecurityID);


    }
    public Double getPrice_Mod_CT_W()
    {
        return this.getPrice_Mod_CT_W(this.UserSecurityID);
    }

    public void setPrice_Mod_CT_W(Double newValue, Integer GroupRefID)
    {
        Double myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Price_Mod_CT_W' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[Price_Mod_CT_W]=["+newValue+"]");
                   dbDB.setPrice_Mod_CT_W(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[Price_Mod_CT_W]=["+newValue+"]");
           dbDB.setPrice_Mod_CT_W(newValue);
         }
    }
    public Double getPrice_Mod_CT_W(Integer GroupRefID)
    {
        Double myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Price_Mod_CT_W' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPrice_Mod_CT_W();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPrice_Mod_CT_W();
         }
        return myVal;
    }

    public void setPrice_Mod_CT_WWO(Double newValue)
    {
        this.setPrice_Mod_CT_WWO(newValue,this.UserSecurityID);


    }
    public Double getPrice_Mod_CT_WWO()
    {
        return this.getPrice_Mod_CT_WWO(this.UserSecurityID);
    }

    public void setPrice_Mod_CT_WWO(Double newValue, Integer GroupRefID)
    {
        Double myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Price_Mod_CT_WWO' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[Price_Mod_CT_WWO]=["+newValue+"]");
                   dbDB.setPrice_Mod_CT_WWO(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[Price_Mod_CT_WWO]=["+newValue+"]");
           dbDB.setPrice_Mod_CT_WWO(newValue);
         }
    }
    public Double getPrice_Mod_CT_WWO(Integer GroupRefID)
    {
        Double myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Price_Mod_CT_WWO' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPrice_Mod_CT_WWO();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPrice_Mod_CT_WWO();
         }
        return myVal;
    }

    public void setBillPrice_Mod_MRI_WO(Double newValue)
    {
        this.setBillPrice_Mod_MRI_WO(newValue,this.UserSecurityID);


    }
    public Double getBillPrice_Mod_MRI_WO()
    {
        return this.getBillPrice_Mod_MRI_WO(this.UserSecurityID);
    }

    public void setBillPrice_Mod_MRI_WO(Double newValue, Integer GroupRefID)
    {
        Double myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='BillPrice_Mod_MRI_WO' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[BillPrice_Mod_MRI_WO]=["+newValue+"]");
                   dbDB.setBillPrice_Mod_MRI_WO(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[BillPrice_Mod_MRI_WO]=["+newValue+"]");
           dbDB.setBillPrice_Mod_MRI_WO(newValue);
         }
    }
    public Double getBillPrice_Mod_MRI_WO(Integer GroupRefID)
    {
        Double myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='BillPrice_Mod_MRI_WO' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getBillPrice_Mod_MRI_WO();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getBillPrice_Mod_MRI_WO();
         }
        return myVal;
    }

    public void setBillPrice_Mod_MRI_W(Double newValue)
    {
        this.setBillPrice_Mod_MRI_W(newValue,this.UserSecurityID);


    }
    public Double getBillPrice_Mod_MRI_W()
    {
        return this.getBillPrice_Mod_MRI_W(this.UserSecurityID);
    }

    public void setBillPrice_Mod_MRI_W(Double newValue, Integer GroupRefID)
    {
        Double myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='BillPrice_Mod_MRI_W' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[BillPrice_Mod_MRI_W]=["+newValue+"]");
                   dbDB.setBillPrice_Mod_MRI_W(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[BillPrice_Mod_MRI_W]=["+newValue+"]");
           dbDB.setBillPrice_Mod_MRI_W(newValue);
         }
    }
    public Double getBillPrice_Mod_MRI_W(Integer GroupRefID)
    {
        Double myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='BillPrice_Mod_MRI_W' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getBillPrice_Mod_MRI_W();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getBillPrice_Mod_MRI_W();
         }
        return myVal;
    }

    public void setBillPrice_Mod_MRI_WWO(Double newValue)
    {
        this.setBillPrice_Mod_MRI_WWO(newValue,this.UserSecurityID);


    }
    public Double getBillPrice_Mod_MRI_WWO()
    {
        return this.getBillPrice_Mod_MRI_WWO(this.UserSecurityID);
    }

    public void setBillPrice_Mod_MRI_WWO(Double newValue, Integer GroupRefID)
    {
        Double myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='BillPrice_Mod_MRI_WWO' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[BillPrice_Mod_MRI_WWO]=["+newValue+"]");
                   dbDB.setBillPrice_Mod_MRI_WWO(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[BillPrice_Mod_MRI_WWO]=["+newValue+"]");
           dbDB.setBillPrice_Mod_MRI_WWO(newValue);
         }
    }
    public Double getBillPrice_Mod_MRI_WWO(Integer GroupRefID)
    {
        Double myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='BillPrice_Mod_MRI_WWO' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getBillPrice_Mod_MRI_WWO();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getBillPrice_Mod_MRI_WWO();
         }
        return myVal;
    }

    public void setBillPrice_Mod_CT_WO(Double newValue)
    {
        this.setBillPrice_Mod_CT_WO(newValue,this.UserSecurityID);


    }
    public Double getBillPrice_Mod_CT_WO()
    {
        return this.getBillPrice_Mod_CT_WO(this.UserSecurityID);
    }

    public void setBillPrice_Mod_CT_WO(Double newValue, Integer GroupRefID)
    {
        Double myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='BillPrice_Mod_CT_WO' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[BillPrice_Mod_CT_WO]=["+newValue+"]");
                   dbDB.setBillPrice_Mod_CT_WO(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[BillPrice_Mod_CT_WO]=["+newValue+"]");
           dbDB.setBillPrice_Mod_CT_WO(newValue);
         }
    }
    public Double getBillPrice_Mod_CT_WO(Integer GroupRefID)
    {
        Double myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='BillPrice_Mod_CT_WO' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getBillPrice_Mod_CT_WO();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getBillPrice_Mod_CT_WO();
         }
        return myVal;
    }

    public void setBillPrice_Mod_CT_W(Double newValue)
    {
        this.setBillPrice_Mod_CT_W(newValue,this.UserSecurityID);


    }
    public Double getBillPrice_Mod_CT_W()
    {
        return this.getBillPrice_Mod_CT_W(this.UserSecurityID);
    }

    public void setBillPrice_Mod_CT_W(Double newValue, Integer GroupRefID)
    {
        Double myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='BillPrice_Mod_CT_W' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[BillPrice_Mod_CT_W]=["+newValue+"]");
                   dbDB.setBillPrice_Mod_CT_W(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[BillPrice_Mod_CT_W]=["+newValue+"]");
           dbDB.setBillPrice_Mod_CT_W(newValue);
         }
    }
    public Double getBillPrice_Mod_CT_W(Integer GroupRefID)
    {
        Double myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='BillPrice_Mod_CT_W' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getBillPrice_Mod_CT_W();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getBillPrice_Mod_CT_W();
         }
        return myVal;
    }

    public void setBillPrice_Mod_CT_WWO(Double newValue)
    {
        this.setBillPrice_Mod_CT_WWO(newValue,this.UserSecurityID);


    }
    public Double getBillPrice_Mod_CT_WWO()
    {
        return this.getBillPrice_Mod_CT_WWO(this.UserSecurityID);
    }

    public void setBillPrice_Mod_CT_WWO(Double newValue, Integer GroupRefID)
    {
        Double myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='BillPrice_Mod_CT_WWO' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[BillPrice_Mod_CT_WWO]=["+newValue+"]");
                   dbDB.setBillPrice_Mod_CT_WWO(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[BillPrice_Mod_CT_WWO]=["+newValue+"]");
           dbDB.setBillPrice_Mod_CT_WWO(newValue);
         }
    }
    public Double getBillPrice_Mod_CT_WWO(Integer GroupRefID)
    {
        Double myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='BillPrice_Mod_CT_WWO' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getBillPrice_Mod_CT_WWO();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getBillPrice_Mod_CT_WWO();
         }
        return myVal;
    }

    public void setFeeScheduleRefID(Integer newValue)
    {
        this.setFeeScheduleRefID(newValue,this.UserSecurityID);


    }
    public Integer getFeeScheduleRefID()
    {
        return this.getFeeScheduleRefID(this.UserSecurityID);
    }

    public void setFeeScheduleRefID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='FeeScheduleRefID' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[FeeScheduleRefID]=["+newValue+"]");
                   dbDB.setFeeScheduleRefID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[FeeScheduleRefID]=["+newValue+"]");
           dbDB.setFeeScheduleRefID(newValue);
         }
    }
    public Integer getFeeScheduleRefID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='FeeScheduleRefID' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getFeeScheduleRefID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getFeeScheduleRefID();
         }
        return myVal;
    }

    public void setFeePercentage(Double newValue)
    {
        this.setFeePercentage(newValue,this.UserSecurityID);


    }
    public Double getFeePercentage()
    {
        return this.getFeePercentage(this.UserSecurityID);
    }

    public void setFeePercentage(Double newValue, Integer GroupRefID)
    {
        Double myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='FeePercentage' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[FeePercentage]=["+newValue+"]");
                   dbDB.setFeePercentage(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[FeePercentage]=["+newValue+"]");
           dbDB.setFeePercentage(newValue);
         }
    }
    public Double getFeePercentage(Integer GroupRefID)
    {
        Double myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='FeePercentage' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getFeePercentage();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getFeePercentage();
         }
        return myVal;
    }

    public void setFeePercentageBill(Double newValue)
    {
        this.setFeePercentageBill(newValue,this.UserSecurityID);


    }
    public Double getFeePercentageBill()
    {
        return this.getFeePercentageBill(this.UserSecurityID);
    }

    public void setFeePercentageBill(Double newValue, Integer GroupRefID)
    {
        Double myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='FeePercentageBill' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[FeePercentageBill]=["+newValue+"]");
                   dbDB.setFeePercentageBill(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[FeePercentageBill]=["+newValue+"]");
           dbDB.setFeePercentageBill(newValue);
         }
    }
    public Double getFeePercentageBill(Integer GroupRefID)
    {
        Double myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='FeePercentageBill' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getFeePercentageBill();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getFeePercentageBill();
         }
        return myVal;
    }

    public void setFeeSchedule2RefID(Integer newValue)
    {
        this.setFeeSchedule2RefID(newValue,this.UserSecurityID);


    }
    public Integer getFeeSchedule2RefID()
    {
        return this.getFeeSchedule2RefID(this.UserSecurityID);
    }

    public void setFeeSchedule2RefID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='FeeSchedule2RefID' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[FeeSchedule2RefID]=["+newValue+"]");
                   dbDB.setFeeSchedule2RefID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[FeeSchedule2RefID]=["+newValue+"]");
           dbDB.setFeeSchedule2RefID(newValue);
         }
    }
    public Integer getFeeSchedule2RefID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='FeeSchedule2RefID' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getFeeSchedule2RefID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getFeeSchedule2RefID();
         }
        return myVal;
    }

    public void setFeePercentage2(Double newValue)
    {
        this.setFeePercentage2(newValue,this.UserSecurityID);


    }
    public Double getFeePercentage2()
    {
        return this.getFeePercentage2(this.UserSecurityID);
    }

    public void setFeePercentage2(Double newValue, Integer GroupRefID)
    {
        Double myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='FeePercentage2' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[FeePercentage2]=["+newValue+"]");
                   dbDB.setFeePercentage2(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[FeePercentage2]=["+newValue+"]");
           dbDB.setFeePercentage2(newValue);
         }
    }
    public Double getFeePercentage2(Integer GroupRefID)
    {
        Double myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='FeePercentage2' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getFeePercentage2();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getFeePercentage2();
         }
        return myVal;
    }

    public void setFeePercentage2Bill(Double newValue)
    {
        this.setFeePercentage2Bill(newValue,this.UserSecurityID);


    }
    public Double getFeePercentage2Bill()
    {
        return this.getFeePercentage2Bill(this.UserSecurityID);
    }

    public void setFeePercentage2Bill(Double newValue, Integer GroupRefID)
    {
        Double myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='FeePercentage2Bill' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[FeePercentage2Bill]=["+newValue+"]");
                   dbDB.setFeePercentage2Bill(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[FeePercentage2Bill]=["+newValue+"]");
           dbDB.setFeePercentage2Bill(newValue);
         }
    }
    public Double getFeePercentage2Bill(Integer GroupRefID)
    {
        Double myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='FeePercentage2Bill' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getFeePercentage2Bill();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getFeePercentage2Bill();
         }
        return myVal;
    }

    public void setBillingTypeID(Integer newValue)
    {
        this.setBillingTypeID(newValue,this.UserSecurityID);


    }
    public Integer getBillingTypeID()
    {
        return this.getBillingTypeID(this.UserSecurityID);
    }

    public void setBillingTypeID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='BillingTypeID' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[BillingTypeID]=["+newValue+"]");
                   dbDB.setBillingTypeID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[BillingTypeID]=["+newValue+"]");
           dbDB.setBillingTypeID(newValue);
         }
    }
    public Integer getBillingTypeID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='BillingTypeID' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getBillingTypeID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getBillingTypeID();
         }
        return myVal;
    }

    public void setAcceptsEMGCaseRate(Integer newValue)
    {
        this.setAcceptsEMGCaseRate(newValue,this.UserSecurityID);


    }
    public Integer getAcceptsEMGCaseRate()
    {
        return this.getAcceptsEMGCaseRate(this.UserSecurityID);
    }

    public void setAcceptsEMGCaseRate(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AcceptsEMGCaseRate' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[AcceptsEMGCaseRate]=["+newValue+"]");
                   dbDB.setAcceptsEMGCaseRate(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[AcceptsEMGCaseRate]=["+newValue+"]");
           dbDB.setAcceptsEMGCaseRate(newValue);
         }
    }
    public Integer getAcceptsEMGCaseRate(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AcceptsEMGCaseRate' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getAcceptsEMGCaseRate();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getAcceptsEMGCaseRate();
         }
        return myVal;
    }

    public void setParentPayerID(Integer newValue)
    {
        this.setParentPayerID(newValue,this.UserSecurityID);


    }
    public Integer getParentPayerID()
    {
        return this.getParentPayerID(this.UserSecurityID);
    }

    public void setParentPayerID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ParentPayerID' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[ParentPayerID]=["+newValue+"]");
                   dbDB.setParentPayerID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[ParentPayerID]=["+newValue+"]");
           dbDB.setParentPayerID(newValue);
         }
    }
    public Integer getParentPayerID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ParentPayerID' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getParentPayerID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getParentPayerID();
         }
        return myVal;
    }

    public void setSelectMRI_ID(Integer newValue)
    {
        this.setSelectMRI_ID(newValue,this.UserSecurityID);


    }
    public Integer getSelectMRI_ID()
    {
        return this.getSelectMRI_ID(this.UserSecurityID);
    }

    public void setSelectMRI_ID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SelectMRI_ID' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[SelectMRI_ID]=["+newValue+"]");
                   dbDB.setSelectMRI_ID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[SelectMRI_ID]=["+newValue+"]");
           dbDB.setSelectMRI_ID(newValue);
         }
    }
    public Integer getSelectMRI_ID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SelectMRI_ID' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getSelectMRI_ID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getSelectMRI_ID();
         }
        return myVal;
    }

    public void setSelectMRI_Notes(String newValue)
    {
        this.setSelectMRI_Notes(newValue,this.UserSecurityID);


    }
    public String getSelectMRI_Notes()
    {
        return this.getSelectMRI_Notes(this.UserSecurityID);
    }

    public void setSelectMRI_Notes(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SelectMRI_Notes' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[SelectMRI_Notes]=["+newValue+"]");
                   dbDB.setSelectMRI_Notes(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[SelectMRI_Notes]=["+newValue+"]");
           dbDB.setSelectMRI_Notes(newValue);
         }
    }
    public String getSelectMRI_Notes(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SelectMRI_Notes' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getSelectMRI_Notes();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getSelectMRI_Notes();
         }
        return myVal;
    }

    public void setSelectMRI_QBID(String newValue)
    {
        this.setSelectMRI_QBID(newValue,this.UserSecurityID);


    }
    public String getSelectMRI_QBID()
    {
        return this.getSelectMRI_QBID(this.UserSecurityID);
    }

    public void setSelectMRI_QBID(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SelectMRI_QBID' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[SelectMRI_QBID]=["+newValue+"]");
                   dbDB.setSelectMRI_QBID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[SelectMRI_QBID]=["+newValue+"]");
           dbDB.setSelectMRI_QBID(newValue);
         }
    }
    public String getSelectMRI_QBID(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SelectMRI_QBID' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getSelectMRI_QBID();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getSelectMRI_QBID();
         }
        return myVal;
    }

    public void setBillingName(String newValue)
    {
        this.setBillingName(newValue,this.UserSecurityID);


    }
    public String getBillingName()
    {
        return this.getBillingName(this.UserSecurityID);
    }

    public void setBillingName(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='BillingName' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[BillingName]=["+newValue+"]");
                   dbDB.setBillingName(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[BillingName]=["+newValue+"]");
           dbDB.setBillingName(newValue);
         }
    }
    public String getBillingName(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='BillingName' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getBillingName();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getBillingName();
         }
        return myVal;
    }

    public void setOfficeAddress1(String newValue)
    {
        this.setOfficeAddress1(newValue,this.UserSecurityID);


    }
    public String getOfficeAddress1()
    {
        return this.getOfficeAddress1(this.UserSecurityID);
    }

    public void setOfficeAddress1(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='OfficeAddress1' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[OfficeAddress1]=["+newValue+"]");
                   dbDB.setOfficeAddress1(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[OfficeAddress1]=["+newValue+"]");
           dbDB.setOfficeAddress1(newValue);
         }
    }
    public String getOfficeAddress1(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='OfficeAddress1' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getOfficeAddress1();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getOfficeAddress1();
         }
        return myVal;
    }

    public void setOfficeAddress2(String newValue)
    {
        this.setOfficeAddress2(newValue,this.UserSecurityID);


    }
    public String getOfficeAddress2()
    {
        return this.getOfficeAddress2(this.UserSecurityID);
    }

    public void setOfficeAddress2(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='OfficeAddress2' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[OfficeAddress2]=["+newValue+"]");
                   dbDB.setOfficeAddress2(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[OfficeAddress2]=["+newValue+"]");
           dbDB.setOfficeAddress2(newValue);
         }
    }
    public String getOfficeAddress2(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='OfficeAddress2' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getOfficeAddress2();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getOfficeAddress2();
         }
        return myVal;
    }

    public void setOfficeCity(String newValue)
    {
        this.setOfficeCity(newValue,this.UserSecurityID);


    }
    public String getOfficeCity()
    {
        return this.getOfficeCity(this.UserSecurityID);
    }

    public void setOfficeCity(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='OfficeCity' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[OfficeCity]=["+newValue+"]");
                   dbDB.setOfficeCity(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[OfficeCity]=["+newValue+"]");
           dbDB.setOfficeCity(newValue);
         }
    }
    public String getOfficeCity(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='OfficeCity' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getOfficeCity();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getOfficeCity();
         }
        return myVal;
    }

    public void setOfficeStateID(Integer newValue)
    {
        this.setOfficeStateID(newValue,this.UserSecurityID);


    }
    public Integer getOfficeStateID()
    {
        return this.getOfficeStateID(this.UserSecurityID);
    }

    public void setOfficeStateID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='OfficeStateID' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[OfficeStateID]=["+newValue+"]");
                   dbDB.setOfficeStateID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[OfficeStateID]=["+newValue+"]");
           dbDB.setOfficeStateID(newValue);
         }
    }
    public Integer getOfficeStateID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='OfficeStateID' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getOfficeStateID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getOfficeStateID();
         }
        return myVal;
    }

    public void setOfficeZIP(String newValue)
    {
        this.setOfficeZIP(newValue,this.UserSecurityID);


    }
    public String getOfficeZIP()
    {
        return this.getOfficeZIP(this.UserSecurityID);
    }

    public void setOfficeZIP(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='OfficeZIP' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[OfficeZIP]=["+newValue+"]");
                   dbDB.setOfficeZIP(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[OfficeZIP]=["+newValue+"]");
           dbDB.setOfficeZIP(newValue);
         }
    }
    public String getOfficeZIP(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='OfficeZIP' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getOfficeZIP();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getOfficeZIP();
         }
        return myVal;
    }

    public void setOfficePhone(String newValue)
    {
        this.setOfficePhone(newValue,this.UserSecurityID);


    }
    public String getOfficePhone()
    {
        return this.getOfficePhone(this.UserSecurityID);
    }

    public void setOfficePhone(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='OfficePhone' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[OfficePhone]=["+newValue+"]");
                   dbDB.setOfficePhone(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[OfficePhone]=["+newValue+"]");
           dbDB.setOfficePhone(newValue);
         }
    }
    public String getOfficePhone(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='OfficePhone' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getOfficePhone();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getOfficePhone();
         }
        return myVal;
    }

    public void setOfficeFax(String newValue)
    {
        this.setOfficeFax(newValue,this.UserSecurityID);


    }
    public String getOfficeFax()
    {
        return this.getOfficeFax(this.UserSecurityID);
    }

    public void setOfficeFax(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='OfficeFax' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[OfficeFax]=["+newValue+"]");
                   dbDB.setOfficeFax(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[OfficeFax]=["+newValue+"]");
           dbDB.setOfficeFax(newValue);
         }
    }
    public String getOfficeFax(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='OfficeFax' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getOfficeFax();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getOfficeFax();
         }
        return myVal;
    }

    public void setOfficeEmail(String newValue)
    {
        this.setOfficeEmail(newValue,this.UserSecurityID);


    }
    public String getOfficeEmail()
    {
        return this.getOfficeEmail(this.UserSecurityID);
    }

    public void setOfficeEmail(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='OfficeEmail' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[OfficeEmail]=["+newValue+"]");
                   dbDB.setOfficeEmail(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[OfficeEmail]=["+newValue+"]");
           dbDB.setOfficeEmail(newValue);
         }
    }
    public String getOfficeEmail(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='OfficeEmail' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getOfficeEmail();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getOfficeEmail();
         }
        return myVal;
    }

    public void setSalesDivision(String newValue)
    {
        this.setSalesDivision(newValue,this.UserSecurityID);


    }
    public String getSalesDivision()
    {
        return this.getSalesDivision(this.UserSecurityID);
    }

    public void setSalesDivision(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SalesDivision' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[SalesDivision]=["+newValue+"]");
                   dbDB.setSalesDivision(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[SalesDivision]=["+newValue+"]");
           dbDB.setSalesDivision(newValue);
         }
    }
    public String getSalesDivision(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SalesDivision' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getSalesDivision();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getSalesDivision();
         }
        return myVal;
    }

    public void setAcquisitionDivision(String newValue)
    {
        this.setAcquisitionDivision(newValue,this.UserSecurityID);


    }
    public String getAcquisitionDivision()
    {
        return this.getAcquisitionDivision(this.UserSecurityID);
    }

    public void setAcquisitionDivision(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AcquisitionDivision' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[AcquisitionDivision]=["+newValue+"]");
                   dbDB.setAcquisitionDivision(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[AcquisitionDivision]=["+newValue+"]");
           dbDB.setAcquisitionDivision(newValue);
         }
    }
    public String getAcquisitionDivision(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AcquisitionDivision' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getAcquisitionDivision();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getAcquisitionDivision();
         }
        return myVal;
    }

    public void setContract1_FeeScheduleID(Integer newValue)
    {
        this.setContract1_FeeScheduleID(newValue,this.UserSecurityID);


    }
    public Integer getContract1_FeeScheduleID()
    {
        return this.getContract1_FeeScheduleID(this.UserSecurityID);
    }

    public void setContract1_FeeScheduleID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Contract1_FeeScheduleID' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[Contract1_FeeScheduleID]=["+newValue+"]");
                   dbDB.setContract1_FeeScheduleID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[Contract1_FeeScheduleID]=["+newValue+"]");
           dbDB.setContract1_FeeScheduleID(newValue);
         }
    }
    public Integer getContract1_FeeScheduleID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Contract1_FeeScheduleID' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getContract1_FeeScheduleID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getContract1_FeeScheduleID();
         }
        return myVal;
    }

    public void setContract1_FeePercentage(Double newValue)
    {
        this.setContract1_FeePercentage(newValue,this.UserSecurityID);


    }
    public Double getContract1_FeePercentage()
    {
        return this.getContract1_FeePercentage(this.UserSecurityID);
    }

    public void setContract1_FeePercentage(Double newValue, Integer GroupRefID)
    {
        Double myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Contract1_FeePercentage' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[Contract1_FeePercentage]=["+newValue+"]");
                   dbDB.setContract1_FeePercentage(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[Contract1_FeePercentage]=["+newValue+"]");
           dbDB.setContract1_FeePercentage(newValue);
         }
    }
    public Double getContract1_FeePercentage(Integer GroupRefID)
    {
        Double myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Contract1_FeePercentage' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getContract1_FeePercentage();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getContract1_FeePercentage();
         }
        return myVal;
    }

    public void setContract2_FeeScheduleID(Integer newValue)
    {
        this.setContract2_FeeScheduleID(newValue,this.UserSecurityID);


    }
    public Integer getContract2_FeeScheduleID()
    {
        return this.getContract2_FeeScheduleID(this.UserSecurityID);
    }

    public void setContract2_FeeScheduleID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Contract2_FeeScheduleID' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[Contract2_FeeScheduleID]=["+newValue+"]");
                   dbDB.setContract2_FeeScheduleID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[Contract2_FeeScheduleID]=["+newValue+"]");
           dbDB.setContract2_FeeScheduleID(newValue);
         }
    }
    public Integer getContract2_FeeScheduleID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Contract2_FeeScheduleID' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getContract2_FeeScheduleID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getContract2_FeeScheduleID();
         }
        return myVal;
    }

    public void setContract2_FeePercentage(Double newValue)
    {
        this.setContract2_FeePercentage(newValue,this.UserSecurityID);


    }
    public Double getContract2_FeePercentage()
    {
        return this.getContract2_FeePercentage(this.UserSecurityID);
    }

    public void setContract2_FeePercentage(Double newValue, Integer GroupRefID)
    {
        Double myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Contract2_FeePercentage' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[Contract2_FeePercentage]=["+newValue+"]");
                   dbDB.setContract2_FeePercentage(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[Contract2_FeePercentage]=["+newValue+"]");
           dbDB.setContract2_FeePercentage(newValue);
         }
    }
    public Double getContract2_FeePercentage(Integer GroupRefID)
    {
        Double myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Contract2_FeePercentage' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getContract2_FeePercentage();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getContract2_FeePercentage();
         }
        return myVal;
    }

    public void setContract3_FeeScheduleID(Integer newValue)
    {
        this.setContract3_FeeScheduleID(newValue,this.UserSecurityID);


    }
    public Integer getContract3_FeeScheduleID()
    {
        return this.getContract3_FeeScheduleID(this.UserSecurityID);
    }

    public void setContract3_FeeScheduleID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Contract3_FeeScheduleID' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[Contract3_FeeScheduleID]=["+newValue+"]");
                   dbDB.setContract3_FeeScheduleID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[Contract3_FeeScheduleID]=["+newValue+"]");
           dbDB.setContract3_FeeScheduleID(newValue);
         }
    }
    public Integer getContract3_FeeScheduleID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Contract3_FeeScheduleID' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getContract3_FeeScheduleID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getContract3_FeeScheduleID();
         }
        return myVal;
    }

    public void setContract3_FeePercentage(Double newValue)
    {
        this.setContract3_FeePercentage(newValue,this.UserSecurityID);


    }
    public Double getContract3_FeePercentage()
    {
        return this.getContract3_FeePercentage(this.UserSecurityID);
    }

    public void setContract3_FeePercentage(Double newValue, Integer GroupRefID)
    {
        Double myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Contract3_FeePercentage' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[Contract3_FeePercentage]=["+newValue+"]");
                   dbDB.setContract3_FeePercentage(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[Contract3_FeePercentage]=["+newValue+"]");
           dbDB.setContract3_FeePercentage(newValue);
         }
    }
    public Double getContract3_FeePercentage(Integer GroupRefID)
    {
        Double myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Contract3_FeePercentage' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getContract3_FeePercentage();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getContract3_FeePercentage();
         }
        return myVal;
    }

    public void setBill1_FeeScheduleID(Integer newValue)
    {
        this.setBill1_FeeScheduleID(newValue,this.UserSecurityID);


    }
    public Integer getBill1_FeeScheduleID()
    {
        return this.getBill1_FeeScheduleID(this.UserSecurityID);
    }

    public void setBill1_FeeScheduleID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Bill1_FeeScheduleID' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[Bill1_FeeScheduleID]=["+newValue+"]");
                   dbDB.setBill1_FeeScheduleID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[Bill1_FeeScheduleID]=["+newValue+"]");
           dbDB.setBill1_FeeScheduleID(newValue);
         }
    }
    public Integer getBill1_FeeScheduleID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Bill1_FeeScheduleID' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getBill1_FeeScheduleID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getBill1_FeeScheduleID();
         }
        return myVal;
    }

    public void setBill1_FeePercentage(Double newValue)
    {
        this.setBill1_FeePercentage(newValue,this.UserSecurityID);


    }
    public Double getBill1_FeePercentage()
    {
        return this.getBill1_FeePercentage(this.UserSecurityID);
    }

    public void setBill1_FeePercentage(Double newValue, Integer GroupRefID)
    {
        Double myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Bill1_FeePercentage' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[Bill1_FeePercentage]=["+newValue+"]");
                   dbDB.setBill1_FeePercentage(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[Bill1_FeePercentage]=["+newValue+"]");
           dbDB.setBill1_FeePercentage(newValue);
         }
    }
    public Double getBill1_FeePercentage(Integer GroupRefID)
    {
        Double myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Bill1_FeePercentage' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getBill1_FeePercentage();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getBill1_FeePercentage();
         }
        return myVal;
    }

    public void setBill2_FeeScheduleID(Integer newValue)
    {
        this.setBill2_FeeScheduleID(newValue,this.UserSecurityID);


    }
    public Integer getBill2_FeeScheduleID()
    {
        return this.getBill2_FeeScheduleID(this.UserSecurityID);
    }

    public void setBill2_FeeScheduleID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Bill2_FeeScheduleID' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[Bill2_FeeScheduleID]=["+newValue+"]");
                   dbDB.setBill2_FeeScheduleID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[Bill2_FeeScheduleID]=["+newValue+"]");
           dbDB.setBill2_FeeScheduleID(newValue);
         }
    }
    public Integer getBill2_FeeScheduleID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Bill2_FeeScheduleID' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getBill2_FeeScheduleID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getBill2_FeeScheduleID();
         }
        return myVal;
    }

    public void setBill2_FeePercentage(Double newValue)
    {
        this.setBill2_FeePercentage(newValue,this.UserSecurityID);


    }
    public Double getBill2_FeePercentage()
    {
        return this.getBill2_FeePercentage(this.UserSecurityID);
    }

    public void setBill2_FeePercentage(Double newValue, Integer GroupRefID)
    {
        Double myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Bill2_FeePercentage' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[Bill2_FeePercentage]=["+newValue+"]");
                   dbDB.setBill2_FeePercentage(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[Bill2_FeePercentage]=["+newValue+"]");
           dbDB.setBill2_FeePercentage(newValue);
         }
    }
    public Double getBill2_FeePercentage(Integer GroupRefID)
    {
        Double myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Bill2_FeePercentage' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getBill2_FeePercentage();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getBill2_FeePercentage();
         }
        return myVal;
    }

    public void setBill3_FeeScheduleID(Integer newValue)
    {
        this.setBill3_FeeScheduleID(newValue,this.UserSecurityID);


    }
    public Integer getBill3_FeeScheduleID()
    {
        return this.getBill3_FeeScheduleID(this.UserSecurityID);
    }

    public void setBill3_FeeScheduleID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Bill3_FeeScheduleID' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[Bill3_FeeScheduleID]=["+newValue+"]");
                   dbDB.setBill3_FeeScheduleID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[Bill3_FeeScheduleID]=["+newValue+"]");
           dbDB.setBill3_FeeScheduleID(newValue);
         }
    }
    public Integer getBill3_FeeScheduleID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Bill3_FeeScheduleID' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getBill3_FeeScheduleID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getBill3_FeeScheduleID();
         }
        return myVal;
    }

    public void setBill3_FeePercentage(Double newValue)
    {
        this.setBill3_FeePercentage(newValue,this.UserSecurityID);


    }
    public Double getBill3_FeePercentage()
    {
        return this.getBill3_FeePercentage(this.UserSecurityID);
    }

    public void setBill3_FeePercentage(Double newValue, Integer GroupRefID)
    {
        Double myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Bill3_FeePercentage' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[Bill3_FeePercentage]=["+newValue+"]");
                   dbDB.setBill3_FeePercentage(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[Bill3_FeePercentage]=["+newValue+"]");
           dbDB.setBill3_FeePercentage(newValue);
         }
    }
    public Double getBill3_FeePercentage(Integer GroupRefID)
    {
        Double myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Bill3_FeePercentage' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getBill3_FeePercentage();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getBill3_FeePercentage();
         }
        return myVal;
    }

    public void setImportantNotes_Alert(String newValue)
    {
        this.setImportantNotes_Alert(newValue,this.UserSecurityID);


    }
    public String getImportantNotes_Alert()
    {
        return this.getImportantNotes_Alert(this.UserSecurityID);
    }

    public void setImportantNotes_Alert(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ImportantNotes_Alert' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[ImportantNotes_Alert]=["+newValue+"]");
                   dbDB.setImportantNotes_Alert(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[ImportantNotes_Alert]=["+newValue+"]");
           dbDB.setImportantNotes_Alert(newValue);
         }
    }
    public String getImportantNotes_Alert(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ImportantNotes_Alert' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getImportantNotes_Alert();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getImportantNotes_Alert();
         }
        return myVal;
    }

    public void setEmailAlertNotes_Alert(String newValue)
    {
        this.setEmailAlertNotes_Alert(newValue,this.UserSecurityID);


    }
    public String getEmailAlertNotes_Alert()
    {
        return this.getEmailAlertNotes_Alert(this.UserSecurityID);
    }

    public void setEmailAlertNotes_Alert(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='EmailAlertNotes_Alert' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[EmailAlertNotes_Alert]=["+newValue+"]");
                   dbDB.setEmailAlertNotes_Alert(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[EmailAlertNotes_Alert]=["+newValue+"]");
           dbDB.setEmailAlertNotes_Alert(newValue);
         }
    }
    public String getEmailAlertNotes_Alert(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='EmailAlertNotes_Alert' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getEmailAlertNotes_Alert();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getEmailAlertNotes_Alert();
         }
        return myVal;
    }

    public void setRequiresOnlineImage(Integer newValue)
    {
        this.setRequiresOnlineImage(newValue,this.UserSecurityID);


    }
    public Integer getRequiresOnlineImage()
    {
        return this.getRequiresOnlineImage(this.UserSecurityID);
    }

    public void setRequiresOnlineImage(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='RequiresOnlineImage' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[RequiresOnlineImage]=["+newValue+"]");
                   dbDB.setRequiresOnlineImage(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[RequiresOnlineImage]=["+newValue+"]");
           dbDB.setRequiresOnlineImage(newValue);
         }
    }
    public Integer getRequiresOnlineImage(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='RequiresOnlineImage' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getRequiresOnlineImage();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getRequiresOnlineImage();
         }
        return myVal;
    }

    public void setRequiresAging(Integer newValue)
    {
        this.setRequiresAging(newValue,this.UserSecurityID);


    }
    public Integer getRequiresAging()
    {
        return this.getRequiresAging(this.UserSecurityID);
    }

    public void setRequiresAging(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='RequiresAging' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[RequiresAging]=["+newValue+"]");
                   dbDB.setRequiresAging(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[RequiresAging]=["+newValue+"]");
           dbDB.setRequiresAging(newValue);
         }
    }
    public Integer getRequiresAging(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='RequiresAging' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getRequiresAging();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getRequiresAging();
         }
        return myVal;
    }

    public void setSales_Monthly_Target(Integer newValue)
    {
        this.setSales_Monthly_Target(newValue,this.UserSecurityID);


    }
    public Integer getSales_Monthly_Target()
    {
        return this.getSales_Monthly_Target(this.UserSecurityID);
    }

    public void setSales_Monthly_Target(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Sales_Monthly_Target' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[Sales_Monthly_Target]=["+newValue+"]");
                   dbDB.setSales_Monthly_Target(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[Sales_Monthly_Target]=["+newValue+"]");
           dbDB.setSales_Monthly_Target(newValue);
         }
    }
    public Integer getSales_Monthly_Target(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Sales_Monthly_Target' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getSales_Monthly_Target();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getSales_Monthly_Target();
         }
        return myVal;
    }

    public void setAcceptsTier2(Integer newValue)
    {
        this.setAcceptsTier2(newValue,this.UserSecurityID);


    }
    public Integer getAcceptsTier2()
    {
        return this.getAcceptsTier2(this.UserSecurityID);
    }

    public void setAcceptsTier2(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AcceptsTier2' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[AcceptsTier2]=["+newValue+"]");
                   dbDB.setAcceptsTier2(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[AcceptsTier2]=["+newValue+"]");
           dbDB.setAcceptsTier2(newValue);
         }
    }
    public Integer getAcceptsTier2(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AcceptsTier2' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getAcceptsTier2();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getAcceptsTier2();
         }
        return myVal;
    }

    public void setAcceptsTier3(Integer newValue)
    {
        this.setAcceptsTier3(newValue,this.UserSecurityID);


    }
    public Integer getAcceptsTier3()
    {
        return this.getAcceptsTier3(this.UserSecurityID);
    }

    public void setAcceptsTier3(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AcceptsTier3' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[AcceptsTier3]=["+newValue+"]");
                   dbDB.setAcceptsTier3(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[AcceptsTier3]=["+newValue+"]");
           dbDB.setAcceptsTier3(newValue);
         }
    }
    public Integer getAcceptsTier3(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AcceptsTier3' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getAcceptsTier3();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getAcceptsTier3();
         }
        return myVal;
    }

    public void setTier2_MinSavings(Double newValue)
    {
        this.setTier2_MinSavings(newValue,this.UserSecurityID);


    }
    public Double getTier2_MinSavings()
    {
        return this.getTier2_MinSavings(this.UserSecurityID);
    }

    public void setTier2_MinSavings(Double newValue, Integer GroupRefID)
    {
        Double myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Tier2_MinSavings' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[Tier2_MinSavings]=["+newValue+"]");
                   dbDB.setTier2_MinSavings(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[Tier2_MinSavings]=["+newValue+"]");
           dbDB.setTier2_MinSavings(newValue);
         }
    }
    public Double getTier2_MinSavings(Integer GroupRefID)
    {
        Double myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Tier2_MinSavings' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getTier2_MinSavings();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getTier2_MinSavings();
         }
        return myVal;
    }

    public void setTier2_TargetSavings(Double newValue)
    {
        this.setTier2_TargetSavings(newValue,this.UserSecurityID);


    }
    public Double getTier2_TargetSavings()
    {
        return this.getTier2_TargetSavings(this.UserSecurityID);
    }

    public void setTier2_TargetSavings(Double newValue, Integer GroupRefID)
    {
        Double myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Tier2_TargetSavings' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[Tier2_TargetSavings]=["+newValue+"]");
                   dbDB.setTier2_TargetSavings(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[Tier2_TargetSavings]=["+newValue+"]");
           dbDB.setTier2_TargetSavings(newValue);
         }
    }
    public Double getTier2_TargetSavings(Integer GroupRefID)
    {
        Double myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Tier2_TargetSavings' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getTier2_TargetSavings();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getTier2_TargetSavings();
         }
        return myVal;
    }

    public void setTier3_Fee(Double newValue)
    {
        this.setTier3_Fee(newValue,this.UserSecurityID);


    }
    public Double getTier3_Fee()
    {
        return this.getTier3_Fee(this.UserSecurityID);
    }

    public void setTier3_Fee(Double newValue, Integer GroupRefID)
    {
        Double myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Tier3_Fee' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[Tier3_Fee]=["+newValue+"]");
                   dbDB.setTier3_Fee(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[Tier3_Fee]=["+newValue+"]");
           dbDB.setTier3_Fee(newValue);
         }
    }
    public Double getTier3_Fee(Integer GroupRefID)
    {
        Double myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Tier3_Fee' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getTier3_Fee();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getTier3_Fee();
         }
        return myVal;
    }

    public void setIsNID(Integer newValue)
    {
        this.setIsNID(newValue,this.UserSecurityID);


    }
    public Integer getIsNID()
    {
        return this.getIsNID(this.UserSecurityID);
    }

    public void setIsNID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='IsNID' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[IsNID]=["+newValue+"]");
                   dbDB.setIsNID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[IsNID]=["+newValue+"]");
           dbDB.setIsNID(newValue);
         }
    }
    public Integer getIsNID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='IsNID' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getIsNID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getIsNID();
         }
        return myVal;
    }

    public void setNID_WebCode(String newValue)
    {
        this.setNID_WebCode(newValue,this.UserSecurityID);


    }
    public String getNID_WebCode()
    {
        return this.getNID_WebCode(this.UserSecurityID);
    }

    public void setNID_WebCode(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='NID_WebCode' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[NID_WebCode]=["+newValue+"]");
                   dbDB.setNID_WebCode(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[NID_WebCode]=["+newValue+"]");
           dbDB.setNID_WebCode(newValue);
         }
    }
    public String getNID_WebCode(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='NID_WebCode' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getNID_WebCode();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getNID_WebCode();
         }
        return myVal;
    }

    public void setNID_ShowLogo(Integer newValue)
    {
        this.setNID_ShowLogo(newValue,this.UserSecurityID);


    }
    public Integer getNID_ShowLogo()
    {
        return this.getNID_ShowLogo(this.UserSecurityID);
    }

    public void setNID_ShowLogo(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='NID_ShowLogo' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[NID_ShowLogo]=["+newValue+"]");
                   dbDB.setNID_ShowLogo(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[NID_ShowLogo]=["+newValue+"]");
           dbDB.setNID_ShowLogo(newValue);
         }
    }
    public Integer getNID_ShowLogo(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='NID_ShowLogo' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getNID_ShowLogo();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getNID_ShowLogo();
         }
        return myVal;
    }

    public void setNID_WebLogo(String newValue)
    {
        this.setNID_WebLogo(newValue,this.UserSecurityID);


    }
    public String getNID_WebLogo()
    {
        return this.getNID_WebLogo(this.UserSecurityID);
    }

    public void setNID_WebLogo(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='NID_WebLogo' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[NID_WebLogo]=["+newValue+"]");
                   dbDB.setNID_WebLogo(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[NID_WebLogo]=["+newValue+"]");
           dbDB.setNID_WebLogo(newValue);
         }
    }
    public String getNID_WebLogo(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='NID_WebLogo' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getNID_WebLogo();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getNID_WebLogo();
         }
        return myVal;
    }

    public void setNID_ReferralSource(Integer newValue)
    {
        this.setNID_ReferralSource(newValue,this.UserSecurityID);


    }
    public Integer getNID_ReferralSource()
    {
        return this.getNID_ReferralSource(this.UserSecurityID);
    }

    public void setNID_ReferralSource(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='NID_ReferralSource' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[NID_ReferralSource]=["+newValue+"]");
                   dbDB.setNID_ReferralSource(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[NID_ReferralSource]=["+newValue+"]");
           dbDB.setNID_ReferralSource(newValue);
         }
    }
    public Integer getNID_ReferralSource(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='NID_ReferralSource' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getNID_ReferralSource();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getNID_ReferralSource();
         }
        return myVal;
    }

    public void setCommissionHighPercentageSelf(Double newValue)
    {
        this.setCommissionHighPercentageSelf(newValue,this.UserSecurityID);


    }
    public Double getCommissionHighPercentageSelf()
    {
        return this.getCommissionHighPercentageSelf(this.UserSecurityID);
    }

    public void setCommissionHighPercentageSelf(Double newValue, Integer GroupRefID)
    {
        Double myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CommissionHighPercentageSelf' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[CommissionHighPercentageSelf]=["+newValue+"]");
                   dbDB.setCommissionHighPercentageSelf(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[CommissionHighPercentageSelf]=["+newValue+"]");
           dbDB.setCommissionHighPercentageSelf(newValue);
         }
    }
    public Double getCommissionHighPercentageSelf(Integer GroupRefID)
    {
        Double myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CommissionHighPercentageSelf' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getCommissionHighPercentageSelf();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getCommissionHighPercentageSelf();
         }
        return myVal;
    }

    public void setCommissionLowPercentageSelf(Double newValue)
    {
        this.setCommissionLowPercentageSelf(newValue,this.UserSecurityID);


    }
    public Double getCommissionLowPercentageSelf()
    {
        return this.getCommissionLowPercentageSelf(this.UserSecurityID);
    }

    public void setCommissionLowPercentageSelf(Double newValue, Integer GroupRefID)
    {
        Double myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CommissionLowPercentageSelf' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[CommissionLowPercentageSelf]=["+newValue+"]");
                   dbDB.setCommissionLowPercentageSelf(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[CommissionLowPercentageSelf]=["+newValue+"]");
           dbDB.setCommissionLowPercentageSelf(newValue);
         }
    }
    public Double getCommissionLowPercentageSelf(Integer GroupRefID)
    {
        Double myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CommissionLowPercentageSelf' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getCommissionLowPercentageSelf();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getCommissionLowPercentageSelf();
         }
        return myVal;
    }

    public void setCommissionHighPercentageParent(Double newValue)
    {
        this.setCommissionHighPercentageParent(newValue,this.UserSecurityID);


    }
    public Double getCommissionHighPercentageParent()
    {
        return this.getCommissionHighPercentageParent(this.UserSecurityID);
    }

    public void setCommissionHighPercentageParent(Double newValue, Integer GroupRefID)
    {
        Double myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CommissionHighPercentageParent' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[CommissionHighPercentageParent]=["+newValue+"]");
                   dbDB.setCommissionHighPercentageParent(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[CommissionHighPercentageParent]=["+newValue+"]");
           dbDB.setCommissionHighPercentageParent(newValue);
         }
    }
    public Double getCommissionHighPercentageParent(Integer GroupRefID)
    {
        Double myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CommissionHighPercentageParent' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getCommissionHighPercentageParent();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getCommissionHighPercentageParent();
         }
        return myVal;
    }

    public void setCommissionLowPercentageParent(Double newValue)
    {
        this.setCommissionLowPercentageParent(newValue,this.UserSecurityID);


    }
    public Double getCommissionLowPercentageParent()
    {
        return this.getCommissionLowPercentageParent(this.UserSecurityID);
    }

    public void setCommissionLowPercentageParent(Double newValue, Integer GroupRefID)
    {
        Double myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CommissionLowPercentageParent' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[CommissionLowPercentageParent]=["+newValue+"]");
                   dbDB.setCommissionLowPercentageParent(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[CommissionLowPercentageParent]=["+newValue+"]");
           dbDB.setCommissionLowPercentageParent(newValue);
         }
    }
    public Double getCommissionLowPercentageParent(Integer GroupRefID)
    {
        Double myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CommissionLowPercentageParent' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getCommissionLowPercentageParent();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getCommissionLowPercentageParent();
         }
        return myVal;
    }

    public void setCommissionNotes(String newValue)
    {
        this.setCommissionNotes(newValue,this.UserSecurityID);


    }
    public String getCommissionNotes()
    {
        return this.getCommissionNotes(this.UserSecurityID);
    }

    public void setCommissionNotes(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CommissionNotes' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[CommissionNotes]=["+newValue+"]");
                   dbDB.setCommissionNotes(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[CommissionNotes]=["+newValue+"]");
           dbDB.setCommissionNotes(newValue);
         }
    }
    public String getCommissionNotes(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CommissionNotes' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getCommissionNotes();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getCommissionNotes();
         }
        return myVal;
    }

    public void setDiscountHighAmount(Double newValue)
    {
        this.setDiscountHighAmount(newValue,this.UserSecurityID);


    }
    public Double getDiscountHighAmount()
    {
        return this.getDiscountHighAmount(this.UserSecurityID);
    }

    public void setDiscountHighAmount(Double newValue, Integer GroupRefID)
    {
        Double myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='DiscountHighAmount' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[DiscountHighAmount]=["+newValue+"]");
                   dbDB.setDiscountHighAmount(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[DiscountHighAmount]=["+newValue+"]");
           dbDB.setDiscountHighAmount(newValue);
         }
    }
    public Double getDiscountHighAmount(Integer GroupRefID)
    {
        Double myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='DiscountHighAmount' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getDiscountHighAmount();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getDiscountHighAmount();
         }
        return myVal;
    }

    public void setDiscountHighPercentage(Double newValue)
    {
        this.setDiscountHighPercentage(newValue,this.UserSecurityID);


    }
    public Double getDiscountHighPercentage()
    {
        return this.getDiscountHighPercentage(this.UserSecurityID);
    }

    public void setDiscountHighPercentage(Double newValue, Integer GroupRefID)
    {
        Double myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='DiscountHighPercentage' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[DiscountHighPercentage]=["+newValue+"]");
                   dbDB.setDiscountHighPercentage(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[DiscountHighPercentage]=["+newValue+"]");
           dbDB.setDiscountHighPercentage(newValue);
         }
    }
    public Double getDiscountHighPercentage(Integer GroupRefID)
    {
        Double myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='DiscountHighPercentage' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getDiscountHighPercentage();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getDiscountHighPercentage();
         }
        return myVal;
    }

    public void setDiscountLowAmount(Double newValue)
    {
        this.setDiscountLowAmount(newValue,this.UserSecurityID);


    }
    public Double getDiscountLowAmount()
    {
        return this.getDiscountLowAmount(this.UserSecurityID);
    }

    public void setDiscountLowAmount(Double newValue, Integer GroupRefID)
    {
        Double myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='DiscountLowAmount' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[DiscountLowAmount]=["+newValue+"]");
                   dbDB.setDiscountLowAmount(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[DiscountLowAmount]=["+newValue+"]");
           dbDB.setDiscountLowAmount(newValue);
         }
    }
    public Double getDiscountLowAmount(Integer GroupRefID)
    {
        Double myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='DiscountLowAmount' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getDiscountLowAmount();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getDiscountLowAmount();
         }
        return myVal;
    }

    public void setDiscountLowPercentage(Double newValue)
    {
        this.setDiscountLowPercentage(newValue,this.UserSecurityID);


    }
    public Double getDiscountLowPercentage()
    {
        return this.getDiscountLowPercentage(this.UserSecurityID);
    }

    public void setDiscountLowPercentage(Double newValue, Integer GroupRefID)
    {
        Double myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='DiscountLowPercentage' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[DiscountLowPercentage]=["+newValue+"]");
                   dbDB.setDiscountLowPercentage(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[DiscountLowPercentage]=["+newValue+"]");
           dbDB.setDiscountLowPercentage(newValue);
         }
    }
    public Double getDiscountLowPercentage(Integer GroupRefID)
    {
        Double myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='DiscountLowPercentage' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getDiscountLowPercentage();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getDiscountLowPercentage();
         }
        return myVal;
    }

    public void setCustomerDisplayName(String newValue)
    {
        this.setCustomerDisplayName(newValue,this.UserSecurityID);


    }
    public String getCustomerDisplayName()
    {
        return this.getCustomerDisplayName(this.UserSecurityID);
    }

    public void setCustomerDisplayName(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CustomerDisplayName' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[CustomerDisplayName]=["+newValue+"]");
                   dbDB.setCustomerDisplayName(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[CustomerDisplayName]=["+newValue+"]");
           dbDB.setCustomerDisplayName(newValue);
         }
    }
    public String getCustomerDisplayName(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CustomerDisplayName' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getCustomerDisplayName();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getCustomerDisplayName();
         }
        return myVal;
    }

    public void setNID_ShowCode(Integer newValue)
    {
        this.setNID_ShowCode(newValue,this.UserSecurityID);


    }
    public Integer getNID_ShowCode()
    {
        return this.getNID_ShowCode(this.UserSecurityID);
    }

    public void setNID_ShowCode(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='NID_ShowCode' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[NID_ShowCode]=["+newValue+"]");
                   dbDB.setNID_ShowCode(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[NID_ShowCode]=["+newValue+"]");
           dbDB.setNID_ShowCode(newValue);
         }
    }
    public Integer getNID_ShowCode(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='NID_ShowCode' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getNID_ShowCode();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getNID_ShowCode();
         }
        return myVal;
    }

    public void setNID_CodeDisplayName(String newValue)
    {
        this.setNID_CodeDisplayName(newValue,this.UserSecurityID);


    }
    public String getNID_CodeDisplayName()
    {
        return this.getNID_CodeDisplayName(this.UserSecurityID);
    }

    public void setNID_CodeDisplayName(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='NID_CodeDisplayName' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[NID_CodeDisplayName]=["+newValue+"]");
                   dbDB.setNID_CodeDisplayName(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[NID_CodeDisplayName]=["+newValue+"]");
           dbDB.setNID_CodeDisplayName(newValue);
         }
    }
    public String getNID_CodeDisplayName(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='NID_CodeDisplayName' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getNID_CodeDisplayName();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getNID_CodeDisplayName();
         }
        return myVal;
    }

    public void setNID_ShowSource(Integer newValue)
    {
        this.setNID_ShowSource(newValue,this.UserSecurityID);


    }
    public Integer getNID_ShowSource()
    {
        return this.getNID_ShowSource(this.UserSecurityID);
    }

    public void setNID_ShowSource(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='NID_ShowSource' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[NID_ShowSource]=["+newValue+"]");
                   dbDB.setNID_ShowSource(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[NID_ShowSource]=["+newValue+"]");
           dbDB.setNID_ShowSource(newValue);
         }
    }
    public Integer getNID_ShowSource(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='NID_ShowSource' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getNID_ShowSource();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getNID_ShowSource();
         }
        return myVal;
    }

    public void setNID_SourceDisplayName(String newValue)
    {
        this.setNID_SourceDisplayName(newValue,this.UserSecurityID);


    }
    public String getNID_SourceDisplayName()
    {
        return this.getNID_SourceDisplayName(this.UserSecurityID);
    }

    public void setNID_SourceDisplayName(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='NID_SourceDisplayName' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[NID_SourceDisplayName]=["+newValue+"]");
                   dbDB.setNID_SourceDisplayName(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[NID_SourceDisplayName]=["+newValue+"]");
           dbDB.setNID_SourceDisplayName(newValue);
         }
    }
    public String getNID_SourceDisplayName(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='NID_SourceDisplayName' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getNID_SourceDisplayName();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getNID_SourceDisplayName();
         }
        return myVal;
    }

    public void setNID_ShowBanner(Integer newValue)
    {
        this.setNID_ShowBanner(newValue,this.UserSecurityID);


    }
    public Integer getNID_ShowBanner()
    {
        return this.getNID_ShowBanner(this.UserSecurityID);
    }

    public void setNID_ShowBanner(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='NID_ShowBanner' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[NID_ShowBanner]=["+newValue+"]");
                   dbDB.setNID_ShowBanner(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[NID_ShowBanner]=["+newValue+"]");
           dbDB.setNID_ShowBanner(newValue);
         }
    }
    public Integer getNID_ShowBanner(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='NID_ShowBanner' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getNID_ShowBanner();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getNID_ShowBanner();
         }
        return myVal;
    }

    public void setNID_WelcomeMessage(String newValue)
    {
        this.setNID_WelcomeMessage(newValue,this.UserSecurityID);


    }
    public String getNID_WelcomeMessage()
    {
        return this.getNID_WelcomeMessage(this.UserSecurityID);
    }

    public void setNID_WelcomeMessage(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='NID_WelcomeMessage' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[NID_WelcomeMessage]=["+newValue+"]");
                   dbDB.setNID_WelcomeMessage(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[NID_WelcomeMessage]=["+newValue+"]");
           dbDB.setNID_WelcomeMessage(newValue);
         }
    }
    public String getNID_WelcomeMessage(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='NID_WelcomeMessage' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getNID_WelcomeMessage();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getNID_WelcomeMessage();
         }
        return myVal;
    }

    public void setNID_DiscountPercentageMessage(String newValue)
    {
        this.setNID_DiscountPercentageMessage(newValue,this.UserSecurityID);


    }
    public String getNID_DiscountPercentageMessage()
    {
        return this.getNID_DiscountPercentageMessage(this.UserSecurityID);
    }

    public void setNID_DiscountPercentageMessage(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='NID_DiscountPercentageMessage' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[NID_DiscountPercentageMessage]=["+newValue+"]");
                   dbDB.setNID_DiscountPercentageMessage(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[NID_DiscountPercentageMessage]=["+newValue+"]");
           dbDB.setNID_DiscountPercentageMessage(newValue);
         }
    }
    public String getNID_DiscountPercentageMessage(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='NID_DiscountPercentageMessage' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getNID_DiscountPercentageMessage();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getNID_DiscountPercentageMessage();
         }
        return myVal;
    }

    public void setInsideSalesDivision(String newValue)
    {
        this.setInsideSalesDivision(newValue,this.UserSecurityID);


    }
    public String getInsideSalesDivision()
    {
        return this.getInsideSalesDivision(this.UserSecurityID);
    }

    public void setInsideSalesDivision(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='InsideSalesDivision' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[InsideSalesDivision]=["+newValue+"]");
                   dbDB.setInsideSalesDivision(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[InsideSalesDivision]=["+newValue+"]");
           dbDB.setInsideSalesDivision(newValue);
         }
    }
    public String getInsideSalesDivision(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='InsideSalesDivision' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getInsideSalesDivision();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getInsideSalesDivision();
         }
        return myVal;
    }

    public void setRequiredFieldList(java.util.Hashtable requiredFieldList)
	 {

        this.requiredFieldList = requiredFieldList;
    }

    public java.util.Hashtable getRequiredFieldList()
    {
        return this.requiredFieldList;
    }
    public void setExpiredFieldList(java.util.Hashtable expiredFieldList)
    {
        this.expiredFieldList = expiredFieldList;
    }

    public void setEnglishFieldList(java.util.Hashtable englishFieldList)
    {
        this.englishFieldList = englishFieldList;
    }

    public java.util.Hashtable getExpiredFieldList()
    {
        return this.expiredFieldList;
    }
    public boolean isExpired(java.util.Date myDate, int daysVector)
    {
       return isExpired(myDate,new java.util.Date(),daysVector);
    }


    public boolean isRead(String fieldN, Integer GroupRefID)
    {
        boolean myVal = false;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='"+fieldN+"' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==1)
               {
                   myVal = true;
               }
            }
        }
        else
        {

           if (iSecurityCheck.intValue()>=1)
           {
                  myVal = true;
           }
        }
        return myVal;
    }


    public boolean isWrite(String fieldN, Integer GroupRefID)
    {
        boolean myVal = false;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='"+fieldN+"' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {
                   myVal = true;
               }
            }
        }
        else
        {

           if (iSecurityCheck.intValue()>=2)
           {
                  myVal = true;
           }
        }
        return myVal;
    }


    public boolean isExpired(java.util.Date myDate, java.util.Date refDate,int daysVector)
    {
        java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat("yyyy-MM-dd");
        boolean myVal = true;
        try
        {
            if (myDate.after(dbdf.parse("1800-01-10")))
            {
                if (myDate.after(dateChange(refDate,daysVector)))
                {
                    myVal=false;
                }
            }
            else
            {
                myVal = false;
            }
        }
        catch (Exception Datee)
        {
        }
        return myVal;
    }

    public java.util.Date dateChange(java.util.Date myD, int dayChange)
    {
        java.util.Calendar calendar = java.util.Calendar.getInstance();
        calendar.setTime(myD);
        calendar.add(java.util.Calendar.DATE, dayChange);
        java.util.Date resultAsDate = new java.util.Date(calendar.getTime().getTime());
        return resultAsDate;
    }

    public boolean isRequired(String fieldN)
    {
        return isRequired(fieldN, new Integer("1"));
    }

    public boolean isRequired(String fieldN, Integer GroupRefID)
    {
        boolean myVal = false;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='"+fieldN+"' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getRequiredID().intValue()==1)
               {
                   myVal = true;
               }
            }
        }
        else
        {
            if (this.getRequiredFieldList().containsKey(fieldN))
            {
                myVal = true;
            }
        }
        return myVal;
    }
    public boolean isExpiredCheck(String fieldN)
    {
        return isExpiredCheck(fieldN, new Integer ("1"));
    }

    public boolean isExpiredCheck(String fieldN, Integer GroupRefID)
    {
        boolean myVal = false;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='"+fieldN+"' AND TableName='tNIM3_PayerMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getTrackedID().intValue()==1)
               {
                   myVal = true;
               }
            }
        }
        else
        {
            if (this.getExpiredFieldList().containsKey(fieldN))
           {
                myVal = true;
            }
        }
        return myVal;
    }

    public void makeRequiredHash()
    {
       makeRequiredHash(new Integer(1));
    }

    public void makeRequiredHash(Integer GroupRefID)
    {

        java.util.Hashtable newHash = new java.util.Hashtable();
        if (runFieldSecurity)
        {
            java.util.Enumeration myKeys = englishFieldList.keys(); 
            while (myKeys.hasMoreElements()) 
            {
                 String myElement = (String) myKeys.nextElement();
                 bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='"+myElement+"' AND TableName='tNIM3_PayerMaster'" ,"");
                 bltFieldSecurity        working_bltFieldSecurity;
                 ListElement         leCurrentElement;
                 Enumeration eList = myFSL.elements();
                 if (eList.hasMoreElements())
                 {
                     leCurrentElement    = (ListElement) eList.nextElement();
                     working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
                     if (working_bltFieldSecurity.getRequiredID().intValue()==1)
                     {
                         newHash.put(myElement,new Boolean(true));
                     }
                 }
            }
        }
        else
        {
            newHash.put("PayerName",new Boolean(true));
            newHash.put("ContactName",new Boolean(true));
            newHash.put("BillingAddress1",new Boolean(true));
            newHash.put("BillingCity",new Boolean(true));
            newHash.put("BillingZIP",new Boolean(true));
            newHash.put("BillingPhone",new Boolean(true));
            newHash.put("BillingFax",new Boolean(true));
            newHash.put("BillingEmail",new Boolean(true));
            newHash.put("PayableTo",new Boolean(true));
            newHash.put("Price_Mod_MRI_WO",new Boolean(true));
            newHash.put("Price_Mod_MRI_W",new Boolean(true));
            newHash.put("Price_Mod_MRI_WWO",new Boolean(true));
            newHash.put("Price_Mod_CT_WO",new Boolean(true));
            newHash.put("Price_Mod_CT_W",new Boolean(true));
            newHash.put("Price_Mod_CT_WWO",new Boolean(true));
            newHash.put("BillPrice_Mod_MRI_WO",new Boolean(true));
            newHash.put("BillPrice_Mod_MRI_W",new Boolean(true));
            newHash.put("BillPrice_Mod_MRI_WWO",new Boolean(true));
            newHash.put("BillPrice_Mod_CT_WO",new Boolean(true));
            newHash.put("BillPrice_Mod_CT_W",new Boolean(true));
            newHash.put("BillPrice_Mod_CT_WWO",new Boolean(true));
            newHash.put("BillingName",new Boolean(true));
            newHash.put("OfficeAddress1",new Boolean(true));
            newHash.put("OfficeCity",new Boolean(true));
            newHash.put("OfficeZIP",new Boolean(true));
            newHash.put("OfficePhone",new Boolean(true));
            newHash.put("OfficeFax",new Boolean(true));
            newHash.put("OfficeEmail",new Boolean(true));
        }
        this.setRequiredFieldList(newHash);
    }

    public void makeEnglishHash()
    {

        java.util.Hashtable newHash = new java.util.Hashtable();        newHash.put("AutoAuth","AutoAuth");
        newHash.put("UniqueCreateDate","Item Create Date");
        newHash.put("UniqueModifyDate","Item Modify Date");
        newHash.put("UniqueModifyComments","Item Modification Comments");
        newHash.put("PayerName","Payer Name");
        newHash.put("ContactName","ContactName");
        newHash.put("BillingAddress1","BillingAddress1");
        newHash.put("BillingAddress2","BillingAddress2");
        newHash.put("BillingCity","BillingCity");
        newHash.put("BillingStateID","BillingStateID");
        newHash.put("BillingZIP","BillingZIP");
        newHash.put("BillingPhone","BillingPhone");
        newHash.put("BillingFax","BillingFax");
        newHash.put("BillingEmail","BillingEmail");
        newHash.put("Comments","Extra Comments");
        newHash.put("BillingEntityID","BillingEntityID");
        newHash.put("PayerTypeID","PayerTypeID");
        newHash.put("ImportantNotes","ImportantNotes");
        newHash.put("PayableTo","PayableTo");
        newHash.put("Price_Mod_MRI_WO","Price_Mod_MRI_WO");
        newHash.put("Price_Mod_MRI_W","Price_Mod_MRI_W");
        newHash.put("Price_Mod_MRI_WWO","Price_Mod_MRI_WWO");
        newHash.put("Price_Mod_CT_WO","Price_Mod_CT_WO");
        newHash.put("Price_Mod_CT_W","Price_Mod_CT_W");
        newHash.put("Price_Mod_CT_WWO","Price_Mod_CT_WWO");
        newHash.put("BillPrice_Mod_MRI_WO","BillPrice_Mod_MRI_WO");
        newHash.put("BillPrice_Mod_MRI_W","BillPrice_Mod_MRI_W");
        newHash.put("BillPrice_Mod_MRI_WWO","BillPrice_Mod_MRI_WWO");
        newHash.put("BillPrice_Mod_CT_WO","BillPrice_Mod_CT_WO");
        newHash.put("BillPrice_Mod_CT_W","BillPrice_Mod_CT_W");
        newHash.put("BillPrice_Mod_CT_WWO","BillPrice_Mod_CT_WWO");
        newHash.put("FeeScheduleRefID","FeeScheduleRefID");
        newHash.put("FeePercentage","FeePercentage");
        newHash.put("FeePercentageBill","FeePercentageBill");
        newHash.put("FeeSchedule2RefID","FeeSchedule2RefID");
        newHash.put("FeePercentage2","FeePercentage2");
        newHash.put("FeePercentage2Bill","FeePercentage2Bill");
        newHash.put("BillingTypeID","BillingTypeID");
        newHash.put("AcceptsEMGCaseRate","AcceptsEMGCaseRate");
        newHash.put("ParentPayerID","ParentPayerID");
        newHash.put("SelectMRI_ID","SelectMRI_ID");
        newHash.put("SelectMRI_Notes","SelectMRI_Notes");
        newHash.put("SelectMRI_QBID","SelectMRI_QBID");
        newHash.put("BillingName","BillingName");
        newHash.put("OfficeAddress1","OfficeAddress1");
        newHash.put("OfficeAddress2","OfficeAddress2");
        newHash.put("OfficeCity","OfficeCity");
        newHash.put("OfficeStateID","OfficeStateID");
        newHash.put("OfficeZIP","OfficeZIP");
        newHash.put("OfficePhone","OfficePhone");
        newHash.put("OfficeFax","OfficeFax");
        newHash.put("OfficeEmail","OfficeEmail");
        newHash.put("SalesDivision","SalesDivision");
        newHash.put("AcquisitionDivision","AcquisitionDivision");
        newHash.put("Contract1_FeeScheduleID","Contract1_FeeScheduleID");
        newHash.put("Contract1_FeePercentage","Contract1_FeePercentage");
        newHash.put("Contract2_FeeScheduleID","Contract2_FeeScheduleID");
        newHash.put("Contract2_FeePercentage","Contract2_FeePercentage");
        newHash.put("Contract3_FeeScheduleID","Contract3_FeeScheduleID");
        newHash.put("Contract3_FeePercentage","Contract3_FeePercentage");
        newHash.put("Bill1_FeeScheduleID","Bill1_FeeScheduleID");
        newHash.put("Bill1_FeePercentage","Bill1_FeePercentage");
        newHash.put("Bill2_FeeScheduleID","Bill2_FeeScheduleID");
        newHash.put("Bill2_FeePercentage","Bill2_FeePercentage");
        newHash.put("Bill3_FeeScheduleID","Bill3_FeeScheduleID");
        newHash.put("Bill3_FeePercentage","Bill3_FeePercentage");
        newHash.put("ImportantNotes_Alert","ImportantNotes_Alert");
        newHash.put("EmailAlertNotes_Alert","EmailAlertNotes_Alert");
        newHash.put("RequiresOnlineImage","RequiresOnlineImage");
        newHash.put("RequiresAging","RequiresAging");
        newHash.put("Sales_Monthly_Target","Sales_Monthly_Target");
        newHash.put("AcceptsTier2","AcceptsTier2");
        newHash.put("AcceptsTier3","AcceptsTier3");
        newHash.put("Tier2_MinSavings","Tier2_MinSavings");
        newHash.put("Tier2_TargetSavings","Tier2_TargetSavings");
        newHash.put("Tier3_Fee","Tier3_Fee");
        newHash.put("IsNID","IsNID");
        newHash.put("NID_WebCode","NID_WebCode");
        newHash.put("NID_ShowLogo","NID_ShowLogo");
        newHash.put("NID_WebLogo","NID_WebLogo");
        newHash.put("NID_ReferralSource","NID_ReferralSource");
        newHash.put("CommissionHighPercentageSelf","CommissionHighPercentageSelf");
        newHash.put("CommissionLowPercentageSelf","CommissionLowPercentageSelf");
        newHash.put("CommissionHighPercentageParent","CommissionHighPercentageParent");
        newHash.put("CommissionLowPercentageParent","CommissionLowPercentageParent");
        newHash.put("CommissionNotes","CommissionNotes");
        newHash.put("DiscountHighAmount","DiscountHighAmount");
        newHash.put("DiscountHighPercentage","DiscountHighPercentage");
        newHash.put("DiscountLowAmount","DiscountLowAmount");
        newHash.put("DiscountLowPercentage","DiscountLowPercentage");
        newHash.put("CustomerDisplayName","CustomerDisplayName");
        newHash.put("NID_ShowCode","NID_ShowCode");
        newHash.put("NID_CodeDisplayName","NID_CodeDisplayName");
        newHash.put("NID_ShowSource","NID_ShowSource");
        newHash.put("NID_SourceDisplayName","NID_SourceDisplayName");
        newHash.put("NID_ShowBanner","NID_ShowBanner");
        newHash.put("NID_WelcomeMessage","NID_WelcomeMessage");
        newHash.put("NID_DiscountPercentageMessage","NID_DiscountPercentageMessage");
        newHash.put("InsideSalesDivision","InsideSalesDivision");

        this.setEnglishFieldList(newHash);
    }
    public void makeExpiredHash()
    {

        java.util.Hashtable newHash = new java.util.Hashtable();
        this.setExpiredFieldList(newHash);
    }

    public java.util.Hashtable englishFieldList = new java.util.Hashtable();
    public java.util.Hashtable requiredFieldList = new java.util.Hashtable();
    public java.util.Hashtable expiredFieldList = new java.util.Hashtable();

    public boolean isComplete()
    {
	     boolean myVal = true;
	     java.util.Enumeration myKeys = this.getRequiredFieldList().keys(); 
	     while (myKeys.hasMoreElements()) 
	     {
	         String myElement = (String) myKeys.nextElement();
	         if (!isComplete(myElement))
	         {
	             myVal = false;
	         }
	     }
	     return myVal;
    }

    public String getEnglish(String fieldN)
    {
        String myVal = null;
        if (this.englishFieldList.containsKey(fieldN))
        {
            myVal = (String)this.englishFieldList.get(fieldN);
        }
        return myVal;
    }
    public java.util.Vector isComplete(int daysVector)
    {
	     boolean myVal = false;
	     java.util.Enumeration myKeys = this.getExpiredFieldList().keys(); 
	     java.util.Vector myExpiredList = new java.util.Vector(); 
	     while (myKeys.hasMoreElements()) 
	     {
	         String myElement = (String) myKeys.nextElement();
	         if (isExpired(myElement,daysVector))
	         {
	             myVal = true;
	             myExpiredList.addElement(myElement);
	         }
	     }
	     return myExpiredList;
    }

    public void setField(String fieldN, Object fieldV)
    {
     boolean myVal = false;
     try
     {
        java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat("yyyy-MM-dd");

        if (fieldN.equals("PayerID"))
        {
             this.setPayerID((Integer)fieldV);
        }

        else if (fieldN.equals("AutoAuth"))
        {
            this.setAutoAuth((String)fieldV);
        }

        else if (fieldN.equals("UniqueCreateDate"))
        {
            this.setUniqueCreateDate((java.util.Date)fieldV);
        }

        else if (fieldN.equals("UniqueModifyDate"))
        {
            this.setUniqueModifyDate((java.util.Date)fieldV);
        }

        else if (fieldN.equals("UniqueModifyComments"))
        {
            this.setUniqueModifyComments((String)fieldV);
        }

        else if (fieldN.equals("PayerName"))
        {
            this.setPayerName((String)fieldV);
        }

        else if (fieldN.equals("ContactName"))
        {
            this.setContactName((String)fieldV);
        }

        else if (fieldN.equals("BillingAddress1"))
        {
            this.setBillingAddress1((String)fieldV);
        }

        else if (fieldN.equals("BillingAddress2"))
        {
            this.setBillingAddress2((String)fieldV);
        }

        else if (fieldN.equals("BillingCity"))
        {
            this.setBillingCity((String)fieldV);
        }

        else if (fieldN.equals("BillingStateID"))
        {
             this.setBillingStateID((Integer)fieldV);
        }

        else if (fieldN.equals("BillingZIP"))
        {
            this.setBillingZIP((String)fieldV);
        }

        else if (fieldN.equals("BillingPhone"))
        {
            this.setBillingPhone((String)fieldV);
        }

        else if (fieldN.equals("BillingFax"))
        {
            this.setBillingFax((String)fieldV);
        }

        else if (fieldN.equals("BillingEmail"))
        {
            this.setBillingEmail((String)fieldV);
        }

        else if (fieldN.equals("Comments"))
        {
            this.setComments((String)fieldV);
        }

        else if (fieldN.equals("BillingEntityID"))
        {
             this.setBillingEntityID((Integer)fieldV);
        }

        else if (fieldN.equals("PayerTypeID"))
        {
             this.setPayerTypeID((Integer)fieldV);
        }

        else if (fieldN.equals("ImportantNotes"))
        {
            this.setImportantNotes((String)fieldV);
        }

        else if (fieldN.equals("PayableTo"))
        {
            this.setPayableTo((String)fieldV);
        }

        else if (fieldN.equals("Price_Mod_MRI_WO"))
        {
            this.setPrice_Mod_MRI_WO((Double)fieldV);
        }

        else if (fieldN.equals("Price_Mod_MRI_W"))
        {
            this.setPrice_Mod_MRI_W((Double)fieldV);
        }

        else if (fieldN.equals("Price_Mod_MRI_WWO"))
        {
            this.setPrice_Mod_MRI_WWO((Double)fieldV);
        }

        else if (fieldN.equals("Price_Mod_CT_WO"))
        {
            this.setPrice_Mod_CT_WO((Double)fieldV);
        }

        else if (fieldN.equals("Price_Mod_CT_W"))
        {
            this.setPrice_Mod_CT_W((Double)fieldV);
        }

        else if (fieldN.equals("Price_Mod_CT_WWO"))
        {
            this.setPrice_Mod_CT_WWO((Double)fieldV);
        }

        else if (fieldN.equals("BillPrice_Mod_MRI_WO"))
        {
            this.setBillPrice_Mod_MRI_WO((Double)fieldV);
        }

        else if (fieldN.equals("BillPrice_Mod_MRI_W"))
        {
            this.setBillPrice_Mod_MRI_W((Double)fieldV);
        }

        else if (fieldN.equals("BillPrice_Mod_MRI_WWO"))
        {
            this.setBillPrice_Mod_MRI_WWO((Double)fieldV);
        }

        else if (fieldN.equals("BillPrice_Mod_CT_WO"))
        {
            this.setBillPrice_Mod_CT_WO((Double)fieldV);
        }

        else if (fieldN.equals("BillPrice_Mod_CT_W"))
        {
            this.setBillPrice_Mod_CT_W((Double)fieldV);
        }

        else if (fieldN.equals("BillPrice_Mod_CT_WWO"))
        {
            this.setBillPrice_Mod_CT_WWO((Double)fieldV);
        }

        else if (fieldN.equals("FeeScheduleRefID"))
        {
             this.setFeeScheduleRefID((Integer)fieldV);
        }

        else if (fieldN.equals("FeePercentage"))
        {
            this.setFeePercentage((Double)fieldV);
        }

        else if (fieldN.equals("FeePercentageBill"))
        {
            this.setFeePercentageBill((Double)fieldV);
        }

        else if (fieldN.equals("FeeSchedule2RefID"))
        {
             this.setFeeSchedule2RefID((Integer)fieldV);
        }

        else if (fieldN.equals("FeePercentage2"))
        {
            this.setFeePercentage2((Double)fieldV);
        }

        else if (fieldN.equals("FeePercentage2Bill"))
        {
            this.setFeePercentage2Bill((Double)fieldV);
        }

        else if (fieldN.equals("BillingTypeID"))
        {
             this.setBillingTypeID((Integer)fieldV);
        }

        else if (fieldN.equals("AcceptsEMGCaseRate"))
        {
             this.setAcceptsEMGCaseRate((Integer)fieldV);
        }

        else if (fieldN.equals("ParentPayerID"))
        {
             this.setParentPayerID((Integer)fieldV);
        }

        else if (fieldN.equals("SelectMRI_ID"))
        {
             this.setSelectMRI_ID((Integer)fieldV);
        }

        else if (fieldN.equals("SelectMRI_Notes"))
        {
            this.setSelectMRI_Notes((String)fieldV);
        }

        else if (fieldN.equals("SelectMRI_QBID"))
        {
            this.setSelectMRI_QBID((String)fieldV);
        }

        else if (fieldN.equals("BillingName"))
        {
            this.setBillingName((String)fieldV);
        }

        else if (fieldN.equals("OfficeAddress1"))
        {
            this.setOfficeAddress1((String)fieldV);
        }

        else if (fieldN.equals("OfficeAddress2"))
        {
            this.setOfficeAddress2((String)fieldV);
        }

        else if (fieldN.equals("OfficeCity"))
        {
            this.setOfficeCity((String)fieldV);
        }

        else if (fieldN.equals("OfficeStateID"))
        {
             this.setOfficeStateID((Integer)fieldV);
        }

        else if (fieldN.equals("OfficeZIP"))
        {
            this.setOfficeZIP((String)fieldV);
        }

        else if (fieldN.equals("OfficePhone"))
        {
            this.setOfficePhone((String)fieldV);
        }

        else if (fieldN.equals("OfficeFax"))
        {
            this.setOfficeFax((String)fieldV);
        }

        else if (fieldN.equals("OfficeEmail"))
        {
            this.setOfficeEmail((String)fieldV);
        }

        else if (fieldN.equals("SalesDivision"))
        {
            this.setSalesDivision((String)fieldV);
        }

        else if (fieldN.equals("AcquisitionDivision"))
        {
            this.setAcquisitionDivision((String)fieldV);
        }

        else if (fieldN.equals("Contract1_FeeScheduleID"))
        {
             this.setContract1_FeeScheduleID((Integer)fieldV);
        }

        else if (fieldN.equals("Contract1_FeePercentage"))
        {
            this.setContract1_FeePercentage((Double)fieldV);
        }

        else if (fieldN.equals("Contract2_FeeScheduleID"))
        {
             this.setContract2_FeeScheduleID((Integer)fieldV);
        }

        else if (fieldN.equals("Contract2_FeePercentage"))
        {
            this.setContract2_FeePercentage((Double)fieldV);
        }

        else if (fieldN.equals("Contract3_FeeScheduleID"))
        {
             this.setContract3_FeeScheduleID((Integer)fieldV);
        }

        else if (fieldN.equals("Contract3_FeePercentage"))
        {
            this.setContract3_FeePercentage((Double)fieldV);
        }

        else if (fieldN.equals("Bill1_FeeScheduleID"))
        {
             this.setBill1_FeeScheduleID((Integer)fieldV);
        }

        else if (fieldN.equals("Bill1_FeePercentage"))
        {
            this.setBill1_FeePercentage((Double)fieldV);
        }

        else if (fieldN.equals("Bill2_FeeScheduleID"))
        {
             this.setBill2_FeeScheduleID((Integer)fieldV);
        }

        else if (fieldN.equals("Bill2_FeePercentage"))
        {
            this.setBill2_FeePercentage((Double)fieldV);
        }

        else if (fieldN.equals("Bill3_FeeScheduleID"))
        {
             this.setBill3_FeeScheduleID((Integer)fieldV);
        }

        else if (fieldN.equals("Bill3_FeePercentage"))
        {
            this.setBill3_FeePercentage((Double)fieldV);
        }

        else if (fieldN.equals("ImportantNotes_Alert"))
        {
            this.setImportantNotes_Alert((String)fieldV);
        }

        else if (fieldN.equals("EmailAlertNotes_Alert"))
        {
            this.setEmailAlertNotes_Alert((String)fieldV);
        }

        else if (fieldN.equals("RequiresOnlineImage"))
        {
             this.setRequiresOnlineImage((Integer)fieldV);
        }

        else if (fieldN.equals("RequiresAging"))
        {
             this.setRequiresAging((Integer)fieldV);
        }

        else if (fieldN.equals("Sales_Monthly_Target"))
        {
             this.setSales_Monthly_Target((Integer)fieldV);
        }

        else if (fieldN.equals("AcceptsTier2"))
        {
             this.setAcceptsTier2((Integer)fieldV);
        }

        else if (fieldN.equals("AcceptsTier3"))
        {
             this.setAcceptsTier3((Integer)fieldV);
        }

        else if (fieldN.equals("Tier2_MinSavings"))
        {
            this.setTier2_MinSavings((Double)fieldV);
        }

        else if (fieldN.equals("Tier2_TargetSavings"))
        {
            this.setTier2_TargetSavings((Double)fieldV);
        }

        else if (fieldN.equals("Tier3_Fee"))
        {
            this.setTier3_Fee((Double)fieldV);
        }

        else if (fieldN.equals("IsNID"))
        {
             this.setIsNID((Integer)fieldV);
        }

        else if (fieldN.equals("NID_WebCode"))
        {
            this.setNID_WebCode((String)fieldV);
        }

        else if (fieldN.equals("NID_ShowLogo"))
        {
             this.setNID_ShowLogo((Integer)fieldV);
        }

        else if (fieldN.equals("NID_WebLogo"))
        {
            this.setNID_WebLogo((String)fieldV);
        }

        else if (fieldN.equals("NID_ReferralSource"))
        {
             this.setNID_ReferralSource((Integer)fieldV);
        }

        else if (fieldN.equals("CommissionHighPercentageSelf"))
        {
            this.setCommissionHighPercentageSelf((Double)fieldV);
        }

        else if (fieldN.equals("CommissionLowPercentageSelf"))
        {
            this.setCommissionLowPercentageSelf((Double)fieldV);
        }

        else if (fieldN.equals("CommissionHighPercentageParent"))
        {
            this.setCommissionHighPercentageParent((Double)fieldV);
        }

        else if (fieldN.equals("CommissionLowPercentageParent"))
        {
            this.setCommissionLowPercentageParent((Double)fieldV);
        }

        else if (fieldN.equals("CommissionNotes"))
        {
            this.setCommissionNotes((String)fieldV);
        }

        else if (fieldN.equals("DiscountHighAmount"))
        {
            this.setDiscountHighAmount((Double)fieldV);
        }

        else if (fieldN.equals("DiscountHighPercentage"))
        {
            this.setDiscountHighPercentage((Double)fieldV);
        }

        else if (fieldN.equals("DiscountLowAmount"))
        {
            this.setDiscountLowAmount((Double)fieldV);
        }

        else if (fieldN.equals("DiscountLowPercentage"))
        {
            this.setDiscountLowPercentage((Double)fieldV);
        }

        else if (fieldN.equals("CustomerDisplayName"))
        {
            this.setCustomerDisplayName((String)fieldV);
        }

        else if (fieldN.equals("NID_ShowCode"))
        {
             this.setNID_ShowCode((Integer)fieldV);
        }

        else if (fieldN.equals("NID_CodeDisplayName"))
        {
            this.setNID_CodeDisplayName((String)fieldV);
        }

        else if (fieldN.equals("NID_ShowSource"))
        {
             this.setNID_ShowSource((Integer)fieldV);
        }

        else if (fieldN.equals("NID_SourceDisplayName"))
        {
            this.setNID_SourceDisplayName((String)fieldV);
        }

        else if (fieldN.equals("NID_ShowBanner"))
        {
             this.setNID_ShowBanner((Integer)fieldV);
        }

        else if (fieldN.equals("NID_WelcomeMessage"))
        {
            this.setNID_WelcomeMessage((String)fieldV);
        }

        else if (fieldN.equals("NID_DiscountPercentageMessage"))
        {
            this.setNID_DiscountPercentageMessage((String)fieldV);
        }

        else if (fieldN.equals("InsideSalesDivision"))
        {
            this.setInsideSalesDivision((String)fieldV);
        }

	    }
	    catch(Exception e2)
	    {}
	    //return myVal;
	}

    public String getFieldType(String fieldN)
    {
     String  myVal = null;
     try
     {
        java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat("yyyy-MM-dd");

        if (fieldN.equals("PayerID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("AutoAuth"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("UniqueCreateDate"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("UniqueModifyDate"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("UniqueModifyComments"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("PayerName"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("ContactName"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("BillingAddress1"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("BillingAddress2"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("BillingCity"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("BillingStateID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("BillingZIP"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("BillingPhone"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("BillingFax"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("BillingEmail"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("Comments"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("BillingEntityID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("PayerTypeID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("ImportantNotes"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("PayableTo"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("Price_Mod_MRI_WO"))
        {
            myVal = "Double";
        }

        else if (fieldN.equals("Price_Mod_MRI_W"))
        {
            myVal = "Double";
        }

        else if (fieldN.equals("Price_Mod_MRI_WWO"))
        {
            myVal = "Double";
        }

        else if (fieldN.equals("Price_Mod_CT_WO"))
        {
            myVal = "Double";
        }

        else if (fieldN.equals("Price_Mod_CT_W"))
        {
            myVal = "Double";
        }

        else if (fieldN.equals("Price_Mod_CT_WWO"))
        {
            myVal = "Double";
        }

        else if (fieldN.equals("BillPrice_Mod_MRI_WO"))
        {
            myVal = "Double";
        }

        else if (fieldN.equals("BillPrice_Mod_MRI_W"))
        {
            myVal = "Double";
        }

        else if (fieldN.equals("BillPrice_Mod_MRI_WWO"))
        {
            myVal = "Double";
        }

        else if (fieldN.equals("BillPrice_Mod_CT_WO"))
        {
            myVal = "Double";
        }

        else if (fieldN.equals("BillPrice_Mod_CT_W"))
        {
            myVal = "Double";
        }

        else if (fieldN.equals("BillPrice_Mod_CT_WWO"))
        {
            myVal = "Double";
        }

        else if (fieldN.equals("FeeScheduleRefID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("FeePercentage"))
        {
            myVal = "Double";
        }

        else if (fieldN.equals("FeePercentageBill"))
        {
            myVal = "Double";
        }

        else if (fieldN.equals("FeeSchedule2RefID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("FeePercentage2"))
        {
            myVal = "Double";
        }

        else if (fieldN.equals("FeePercentage2Bill"))
        {
            myVal = "Double";
        }

        else if (fieldN.equals("BillingTypeID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("AcceptsEMGCaseRate"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("ParentPayerID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("SelectMRI_ID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("SelectMRI_Notes"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("SelectMRI_QBID"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("BillingName"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("OfficeAddress1"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("OfficeAddress2"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("OfficeCity"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("OfficeStateID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("OfficeZIP"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("OfficePhone"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("OfficeFax"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("OfficeEmail"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("SalesDivision"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("AcquisitionDivision"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("Contract1_FeeScheduleID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("Contract1_FeePercentage"))
        {
            myVal = "Double";
        }

        else if (fieldN.equals("Contract2_FeeScheduleID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("Contract2_FeePercentage"))
        {
            myVal = "Double";
        }

        else if (fieldN.equals("Contract3_FeeScheduleID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("Contract3_FeePercentage"))
        {
            myVal = "Double";
        }

        else if (fieldN.equals("Bill1_FeeScheduleID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("Bill1_FeePercentage"))
        {
            myVal = "Double";
        }

        else if (fieldN.equals("Bill2_FeeScheduleID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("Bill2_FeePercentage"))
        {
            myVal = "Double";
        }

        else if (fieldN.equals("Bill3_FeeScheduleID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("Bill3_FeePercentage"))
        {
            myVal = "Double";
        }

        else if (fieldN.equals("ImportantNotes_Alert"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("EmailAlertNotes_Alert"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("RequiresOnlineImage"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("RequiresAging"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("Sales_Monthly_Target"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("AcceptsTier2"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("AcceptsTier3"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("Tier2_MinSavings"))
        {
            myVal = "Double";
        }

        else if (fieldN.equals("Tier2_TargetSavings"))
        {
            myVal = "Double";
        }

        else if (fieldN.equals("Tier3_Fee"))
        {
            myVal = "Double";
        }

        else if (fieldN.equals("IsNID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("NID_WebCode"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("NID_ShowLogo"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("NID_WebLogo"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("NID_ReferralSource"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("CommissionHighPercentageSelf"))
        {
            myVal = "Double";
        }

        else if (fieldN.equals("CommissionLowPercentageSelf"))
        {
            myVal = "Double";
        }

        else if (fieldN.equals("CommissionHighPercentageParent"))
        {
            myVal = "Double";
        }

        else if (fieldN.equals("CommissionLowPercentageParent"))
        {
            myVal = "Double";
        }

        else if (fieldN.equals("CommissionNotes"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("DiscountHighAmount"))
        {
            myVal = "Double";
        }

        else if (fieldN.equals("DiscountHighPercentage"))
        {
            myVal = "Double";
        }

        else if (fieldN.equals("DiscountLowAmount"))
        {
            myVal = "Double";
        }

        else if (fieldN.equals("DiscountLowPercentage"))
        {
            myVal = "Double";
        }

        else if (fieldN.equals("CustomerDisplayName"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("NID_ShowCode"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("NID_CodeDisplayName"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("NID_ShowSource"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("NID_SourceDisplayName"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("NID_ShowBanner"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("NID_WelcomeMessage"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("NID_DiscountPercentageMessage"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("InsideSalesDivision"))
        {
            myVal = "String";
        }

	    }
	    catch(Exception e2)
	    {}
	    return myVal;
	}

    public boolean isComplete(String fieldN)
    {
	     boolean myVal = false;
	     try
	     {
        java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat("yyyy-MM-dd");

	     if (fieldN.equals("PayerID"))

	     {
                    if (this.getPayerID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("AutoAuth"))

	     {
                    if (this.getAutoAuth().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("UniqueCreateDate"))

	     {
                    if (this.getUniqueCreateDate().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("UniqueModifyDate"))

	     {
                    if (this.getUniqueModifyDate().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("UniqueModifyComments"))

	     {
                    if (this.getUniqueModifyComments().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("PayerName"))

	     {
                    if (this.getPayerName().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("ContactName"))

	     {
                    if (this.getContactName().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("BillingAddress1"))

	     {
                    if (this.getBillingAddress1().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("BillingAddress2"))

	     {
                    if (this.getBillingAddress2().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("BillingCity"))

	     {
                    if (this.getBillingCity().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("BillingStateID"))

	     {
                    if (this.getBillingStateID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("BillingZIP"))

	     {
                    if (this.getBillingZIP().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("BillingPhone"))

	     {
                    if (this.getBillingPhone().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("BillingFax"))

	     {
                    if (this.getBillingFax().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("BillingEmail"))

	     {
                    if (this.getBillingEmail().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("Comments"))

	     {
                    if (this.getComments().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("BillingEntityID"))

	     {
                    if (this.getBillingEntityID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("PayerTypeID"))

	     {
                    if (this.getPayerTypeID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("ImportantNotes"))

	     {
                    if (this.getImportantNotes().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("PayableTo"))

	     {
                    if (this.getPayableTo().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("Price_Mod_MRI_WO"))

	     {
                    if (this.getPrice_Mod_MRI_WO().equals(new Double("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("Price_Mod_MRI_W"))

	     {
                    if (this.getPrice_Mod_MRI_W().equals(new Double("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("Price_Mod_MRI_WWO"))

	     {
                    if (this.getPrice_Mod_MRI_WWO().equals(new Double("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("Price_Mod_CT_WO"))

	     {
                    if (this.getPrice_Mod_CT_WO().equals(new Double("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("Price_Mod_CT_W"))

	     {
                    if (this.getPrice_Mod_CT_W().equals(new Double("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("Price_Mod_CT_WWO"))

	     {
                    if (this.getPrice_Mod_CT_WWO().equals(new Double("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("BillPrice_Mod_MRI_WO"))

	     {
                    if (this.getBillPrice_Mod_MRI_WO().equals(new Double("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("BillPrice_Mod_MRI_W"))

	     {
                    if (this.getBillPrice_Mod_MRI_W().equals(new Double("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("BillPrice_Mod_MRI_WWO"))

	     {
                    if (this.getBillPrice_Mod_MRI_WWO().equals(new Double("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("BillPrice_Mod_CT_WO"))

	     {
                    if (this.getBillPrice_Mod_CT_WO().equals(new Double("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("BillPrice_Mod_CT_W"))

	     {
                    if (this.getBillPrice_Mod_CT_W().equals(new Double("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("BillPrice_Mod_CT_WWO"))

	     {
                    if (this.getBillPrice_Mod_CT_WWO().equals(new Double("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("FeeScheduleRefID"))

	     {
                    if (this.getFeeScheduleRefID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("FeePercentage"))

	     {
                    if (this.getFeePercentage().equals(new Double("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("FeePercentageBill"))

	     {
                    if (this.getFeePercentageBill().equals(new Double("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("FeeSchedule2RefID"))

	     {
                    if (this.getFeeSchedule2RefID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("FeePercentage2"))

	     {
                    if (this.getFeePercentage2().equals(new Double("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("FeePercentage2Bill"))

	     {
                    if (this.getFeePercentage2Bill().equals(new Double("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("BillingTypeID"))

	     {
                    if (this.getBillingTypeID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("AcceptsEMGCaseRate"))

	     {
                    if (this.getAcceptsEMGCaseRate().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("ParentPayerID"))

	     {
                    if (this.getParentPayerID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("SelectMRI_ID"))

	     {
                    if (this.getSelectMRI_ID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("SelectMRI_Notes"))

	     {
                    if (this.getSelectMRI_Notes().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("SelectMRI_QBID"))

	     {
                    if (this.getSelectMRI_QBID().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("BillingName"))

	     {
                    if (this.getBillingName().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("OfficeAddress1"))

	     {
                    if (this.getOfficeAddress1().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("OfficeAddress2"))

	     {
                    if (this.getOfficeAddress2().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("OfficeCity"))

	     {
                    if (this.getOfficeCity().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("OfficeStateID"))

	     {
                    if (this.getOfficeStateID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("OfficeZIP"))

	     {
                    if (this.getOfficeZIP().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("OfficePhone"))

	     {
                    if (this.getOfficePhone().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("OfficeFax"))

	     {
                    if (this.getOfficeFax().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("OfficeEmail"))

	     {
                    if (this.getOfficeEmail().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("SalesDivision"))

	     {
                    if (this.getSalesDivision().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("AcquisitionDivision"))

	     {
                    if (this.getAcquisitionDivision().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("Contract1_FeeScheduleID"))

	     {
                    if (this.getContract1_FeeScheduleID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("Contract1_FeePercentage"))

	     {
                    if (this.getContract1_FeePercentage().equals(new Double("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("Contract2_FeeScheduleID"))

	     {
                    if (this.getContract2_FeeScheduleID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("Contract2_FeePercentage"))

	     {
                    if (this.getContract2_FeePercentage().equals(new Double("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("Contract3_FeeScheduleID"))

	     {
                    if (this.getContract3_FeeScheduleID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("Contract3_FeePercentage"))

	     {
                    if (this.getContract3_FeePercentage().equals(new Double("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("Bill1_FeeScheduleID"))

	     {
                    if (this.getBill1_FeeScheduleID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("Bill1_FeePercentage"))

	     {
                    if (this.getBill1_FeePercentage().equals(new Double("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("Bill2_FeeScheduleID"))

	     {
                    if (this.getBill2_FeeScheduleID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("Bill2_FeePercentage"))

	     {
                    if (this.getBill2_FeePercentage().equals(new Double("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("Bill3_FeeScheduleID"))

	     {
                    if (this.getBill3_FeeScheduleID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("Bill3_FeePercentage"))

	     {
                    if (this.getBill3_FeePercentage().equals(new Double("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("ImportantNotes_Alert"))

	     {
                    if (this.getImportantNotes_Alert().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("EmailAlertNotes_Alert"))

	     {
                    if (this.getEmailAlertNotes_Alert().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("RequiresOnlineImage"))

	     {
                    if (this.getRequiresOnlineImage().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("RequiresAging"))

	     {
                    if (this.getRequiresAging().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("Sales_Monthly_Target"))

	     {
                    if (this.getSales_Monthly_Target().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("AcceptsTier2"))

	     {
                    if (this.getAcceptsTier2().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("AcceptsTier3"))

	     {
                    if (this.getAcceptsTier3().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("Tier2_MinSavings"))

	     {
                    if (this.getTier2_MinSavings().equals(new Double("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("Tier2_TargetSavings"))

	     {
                    if (this.getTier2_TargetSavings().equals(new Double("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("Tier3_Fee"))

	     {
                    if (this.getTier3_Fee().equals(new Double("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("IsNID"))

	     {
                    if (this.getIsNID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("NID_WebCode"))

	     {
                    if (this.getNID_WebCode().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("NID_ShowLogo"))

	     {
                    if (this.getNID_ShowLogo().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("NID_WebLogo"))

	     {
                    if (this.getNID_WebLogo().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("NID_ReferralSource"))

	     {
                    if (this.getNID_ReferralSource().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("CommissionHighPercentageSelf"))

	     {
                    if (this.getCommissionHighPercentageSelf().equals(new Double("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("CommissionLowPercentageSelf"))

	     {
                    if (this.getCommissionLowPercentageSelf().equals(new Double("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("CommissionHighPercentageParent"))

	     {
                    if (this.getCommissionHighPercentageParent().equals(new Double("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("CommissionLowPercentageParent"))

	     {
                    if (this.getCommissionLowPercentageParent().equals(new Double("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("CommissionNotes"))

	     {
                    if (this.getCommissionNotes().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("DiscountHighAmount"))

	     {
                    if (this.getDiscountHighAmount().equals(new Double("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("DiscountHighPercentage"))

	     {
                    if (this.getDiscountHighPercentage().equals(new Double("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("DiscountLowAmount"))

	     {
                    if (this.getDiscountLowAmount().equals(new Double("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("DiscountLowPercentage"))

	     {
                    if (this.getDiscountLowPercentage().equals(new Double("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("CustomerDisplayName"))

	     {
                    if (this.getCustomerDisplayName().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("NID_ShowCode"))

	     {
                    if (this.getNID_ShowCode().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("NID_CodeDisplayName"))

	     {
                    if (this.getNID_CodeDisplayName().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("NID_ShowSource"))

	     {
                    if (this.getNID_ShowSource().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("NID_SourceDisplayName"))

	     {
                    if (this.getNID_SourceDisplayName().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("NID_ShowBanner"))

	     {
                    if (this.getNID_ShowBanner().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("NID_WelcomeMessage"))

	     {
                    if (this.getNID_WelcomeMessage().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("NID_DiscountPercentageMessage"))

	     {
                    if (this.getNID_DiscountPercentageMessage().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("InsideSalesDivision"))

	     {
                    if (this.getInsideSalesDivision().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	    }
	    catch(Exception e2)
	    {}
	    return myVal;
	}

    public boolean isExpired(String fieldN,int daysVector)
    {
        return isExpired(fieldN,new java.util.Date(),daysVector);
    }

    public boolean isExpired(String fieldN,java.util.Date refDate,int daysVector)
    {
	     boolean myVal = false;
	     try
	     {
        java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat("yyyy-MM-dd");

        if (fieldN.equals("UniqueCreateDate"))

	     {
                    if (this.isExpired(this.getUniqueCreateDate(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

        if (fieldN.equals("UniqueModifyDate"))

	     {
                    if (this.isExpired(this.getUniqueModifyDate(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

	    }
	    catch(Exception e2)
	    {}
	    return myVal;
	}

    public java.util.Date getDateField(String fieldN)
    {
	     java.util.Date myVal = null;
	     try
	     {
        java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat("yyyy-MM-dd");

        if (fieldN.equals("UniqueCreateDate"))

	     {
             myVal = this.getUniqueCreateDate();
        }

        if (fieldN.equals("UniqueModifyDate"))

	     {
             myVal = this.getUniqueModifyDate();
        }

	    }
	    catch(Exception e2)
	    {}
	    return myVal;
	}
	public boolean runFieldSecurity = false;
	public Integer UserSecurityID = null;
   public Integer iSecurityCheck = null;
}    // End of bltNIM3_PayerMaster class definition
