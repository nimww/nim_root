

package com.winstaff;

import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public interface InttNIM3_CaseAccountUserAccountLU extends dbTableInterface
{

    public void setLookupID(Integer newValue);
    public Integer getLookupID();
    public void setUniqueCreateDate(Date newValue);
    public Date getUniqueCreateDate();
    public void setUniqueModifyDate(Date newValue);
    public Date getUniqueModifyDate();
    public void setUniqueModifyComments(String newValue);
    public String getUniqueModifyComments();
    public void setCaseID(Integer newValue);
    public Integer getCaseID();
    public void setUserID(Integer newValue);
    public Integer getUserID();
    public void setStatusCode(Integer newValue);
    public Integer getStatusCode();
    public void setComments(String newValue);
    public String getComments();
}    // End of bltNIM3_CaseAccountUserAccountLU class definition
