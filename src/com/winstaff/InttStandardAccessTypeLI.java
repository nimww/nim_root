package com.winstaff;


import com.winstaff.dbtStandardAccessTypeLI;
import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public interface InttStandardAccessTypeLI extends dbTableInterface
{

    public void setStandardAccessTypeID(Integer newValue);
    public Integer getStandardAccessTypeID();
    public void setStandardAccessTypeLong(String newValue);
    public String getStandardAccessTypeLong();
}    // End of bltStandardAccessTypeLI class definition
