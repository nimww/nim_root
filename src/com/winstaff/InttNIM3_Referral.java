

package com.winstaff;

import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public interface InttNIM3_Referral extends dbTableInterface
{

    public void setReferralID(Integer newValue);
    public Integer getReferralID();
    public void setUniqueCreateDate(Date newValue);
    public Date getUniqueCreateDate();
    public void setUniqueModifyDate(Date newValue);
    public Date getUniqueModifyDate();
    public void setUniqueModifyComments(String newValue);
    public String getUniqueModifyComments();
    public void setCaseID(Integer newValue);
    public Integer getCaseID();
    public void setReferralTypeID(Integer newValue);
    public Integer getReferralTypeID();
    public void setReferralStatusID(Integer newValue);
    public Integer getReferralStatusID();
    public void setAttendingPhysicianID(Integer newValue);
    public Integer getAttendingPhysicianID();
    public void setReferringPhysicianID(Integer newValue);
    public Integer getReferringPhysicianID();
    public void setPreAuthorizationConfirmation(String newValue);
    public String getPreAuthorizationConfirmation();
    public void setProviderPatientID(String newValue);
    public String getProviderPatientID();
    public void setAuthorizationDate(Date newValue);
    public Date getAuthorizationDate();
    public void setReceiveDate(Date newValue);
    public Date getReceiveDate();
    public void setReferralDate(Date newValue);
    public Date getReferralDate();
    public void setNextActionDate(Date newValue);
    public Date getNextActionDate();
    public void setNextActionTaskID(Integer newValue);
    public Integer getNextActionTaskID();
    public void setPreparedBy(String newValue);
    public String getPreparedBy();
    public void setReferencePhone(String newValue);
    public String getReferencePhone();
    public void setReferenceFax(String newValue);
    public String getReferenceFax();
    public void setReferenceEmail(String newValue);
    public String getReferenceEmail();
    public void setURRecFileID(Integer newValue);
    public Integer getURRecFileID();
    public void setOrderFileID(Integer newValue);
    public Integer getOrderFileID();
    public void setRxFileID(Integer newValue);
    public Integer getRxFileID();
    public void setComments(String newValue);
    public String getComments();
    public void setAuditNotes(String newValue);
    public String getAuditNotes();
    public void setReferralMethod(String newValue);
    public String getReferralMethod();
    public void setReferredByContactID(Integer newValue);
    public Integer getReferredByContactID();
    public void setReferralWebLink(String newValue);
    public String getReferralWebLink();
    public void setReferralNIDPromoID(Integer newValue);
    public Integer getReferralNIDPromoID();
}    // End of bltNIM3_Referral class definition
