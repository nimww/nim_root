package com.winstaff;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.net.ftp.FTPReply;
import org.apache.commons.net.ftp.FTPSClient;
import org.apache.pdfbox.exceptions.COSVisitorException;
import org.apache.pdfbox.pdmodel.encryption.AccessPermission;
import org.apache.pdfbox.util.PDFMergerUtility;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;
/**
 * Tyson billing all in one. Pulls ready to bill cases, builds pdf from hcfa and unique reports per referral and stores record in
 * transaction table and the pdf on the file system. 
 * required TysonReadyToBill view and tysonbilling table.
 * @author Po Le
 *
 */
public class EDI_Tyson {

	/**
	 * Tyson hard coded parent ID
	 */
	static final int tysonParentID = 138;
	static Date date = new Date();

	/**
	 * Infamous main method
	 * 
	 * @param args
	 */
	static public void main(String args[]) {
		// testConnection();
		System.out.println("-- Start EDI_Tyson " + new Date() + " --\n");
		getReadyToBill();
		getUploadList();
		System.out.println("\n-- End EDI_Tyson " + new Date() + " --");
	}

	/**
	 * Imports cases that are ready into tysonbilling table for processing
	 */
	private static void getReadyToBill() {
		searchDB2 conn = new searchDB2();
		String query = "select * from \"TysonReadyToBill\"";
		java.sql.ResultSet rs = conn.executeStatement(query);
		System.out.println("-- Importing Ready to bill " + new Date() + " --\n");
		try {
			while (rs.next()) {
				bltysonbilling tb = new bltysonbilling();
				tb.setActionID(0);
				tb.setReferralID(rs.getInt("referralid"));
				tb.setUserApproveID(0);
				tb.setGeneratedBillName(generateBill(rs.getString("hcfa_filename"), rs.getString("referralid")));
				tb.commitData();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			conn.closeAll();
		}
		System.out.println("\n-- Finished Importing Ready to bill " + new Date() + " --\n");
	}

	/**
	 * Merges the hcfa and report to be sent to payer
	 * 
	 * @param hcfa
	 *            hcfa file name
	 * @param eid
	 *            encounter number to serialize
	 * @return generated file name
	 */
	private static String generateBill(String hcfa, String id) {
		String sourcePath = "/mnt/cloudstorage/nimdox/";
		String descPath = "/mnt/cloudstorage/tysonbill/";

		SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd");
		String destinationFileName = "NIM_" + df.format(date) + "_" + id + ".pdf";

		PDFMergerUtility ut = new PDFMergerUtility();
		ut.addSource(sourcePath + hcfa);
		
		for(String s : getReportFileNames(new Integer(id))){
			ut.addSource(sourcePath + s);
		}
		
		// Naming convention of those documents if any. NIM_yyyymmdd
		// (NIM_20130730). Date should be based on uploaded date.
		ut.setDestinationFileName(descPath + destinationFileName);

		try {
			ut.mergeDocuments();
		} catch (COSVisitorException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println("\t -" + destinationFileName);
		return destinationFileName;
	}

	/**
	 * Builds list to upload files to Tyson.
	 */
	public static List<TysonBillObject> getUploadList() {
		List<TysonBillObject> tbo = new ArrayList<TysonBillObject>();
		List<String> uploadFileList = new ArrayList<String>();
		searchDB2 conn = new searchDB2();
		String query = "select * from tysonbilling where actionid = 0";
		java.sql.ResultSet rs = conn.executeStatement(query);
		try {
			while (rs.next()) {
				searchDB2 conn2 = new searchDB2();
				String query2 = "select ca.patientfirstname, ca.patientlastname, to_char(ca.dateofinjury,'mm/dd/yyyy'), pm.payername from tnim3_referral ref join tnim3_caseaccount ca on ca.caseid = ref.caseid join tnim3_payermaster pm on pm.payerid = ca.payerid " +
						"where ref.referralid = " + rs.getInt(6);
				java.sql.ResultSet rs2 = conn2.executeStatement(query2);
				String ptFirst ="";
				String ptLast ="";
				String doi ="";
				String payerName ="";
				try {
					while (rs2.next()) {
						ptFirst = rs2.getString(1);
						ptLast = rs2.getString(2);
						doi = rs2.getString(3);
						payerName = rs2.getString(4);
					}
				} catch (SQLException e) {
					e.printStackTrace();
				} finally {
					conn2.closeAll();
				}
				uploadFileList.add(rs.getString("generatedbillname"));
				//tbo.add(new TysonBillObject(rs.getInt("referralid"), rs.getInt("userapproveid"), rs.getString("generatedbillname"), getReportFileNames(rs.getInt("referralid")), getEncounterIds(rs.getInt("referralid")),ptFirst,ptLast,doi,payerName));
				
				conn.executeStatement("update tysonbilling set actionid = 1 where tysonbillingid = " + rs.getString(1));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			conn.closeAll();
		}
		uploadFile(uploadFileList);
		System.out.println("-- Done uploadFile() " + new Date() + "  --");
		return tbo;
	}
	/**
	 * Generates a list of all report file names by referral id
	 * @param referralid
	 * @return
	 */
	private static List<String> getReportFileNames(int referralid) {
		List<String> reportfilename = new ArrayList<String>();
		List<String> uniquereportfilename = new ArrayList<String>();
		List<String> returnUniquereportfilename = new ArrayList<String>();
		String query = "select filename from tnim3_document where documentid in (select reportfileid from tnim3_encounter where encounterid in (select DISTINCT encounterid from tnim3_document where referralid = "+referralid+" and encounterid != 0) and encounterstatusid != 5)";
		searchDB2 conn = new searchDB2();
		ResultSet rs = conn.executeStatement(query);
		try {
			while (rs.next()) {
				reportfilename.add(rs.getString("filename"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			conn.closeAll();
		}
		System.out.println("In Files: "+reportfilename);
		for (String s : reportfilename){
			String tempTest[] = s.split("_");
			String reconstructed = "";
			boolean isFirst = true;
			for(int x = 4; x < tempTest.length; x++){
				if(isFirst){
					reconstructed += tempTest[x];
					isFirst = false;
				} else reconstructed += "_"+tempTest[x];
			}
			System.out.println("\t"+reconstructed);
			if (!uniquereportfilename.contains(reconstructed)){
				uniquereportfilename.add(reconstructed);
				returnUniquereportfilename.add(s);
			}
		}
		System.out.println("\t"+uniquereportfilename);
		return returnUniquereportfilename;
	}

	private static List<Integer> getEncounterIds(int referralid) {
		List<Integer> encounterids = new ArrayList<Integer>();
		String query = "select encounterid from tnim3_encounter where referralid = " + referralid;
		searchDB2 conn = new searchDB2();
		ResultSet rs = conn.executeStatement(query);
		try {
			while (rs.next()) {
				encounterids.add(rs.getInt(1));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			conn.closeAll();
		}

		return encounterids;
	}

	/**
	 * Uploads bill to payer from inputed src
	 * 
	 * @param src
	 *            name of file to be uploaded. path is hard coded.
	 */
	private static void uploadFile(List<String> src) {

		String testSRC = "TysonBill.pdf";
		String remoteDir = "/home/tynetncc/nextimage/in/";
		String localDir = "/mnt/cloudstorage/tysonbill/";
		String host = "ftp.tyson.com";
		String user = "nextimage";
		String password = "P3rf3ct!";

		JSch jsch = new JSch();
		Session session = null;
		try {
			session = jsch.getSession(user, host, 22);
			session.setConfig("StrictHostKeyChecking", "no");
			session.setPassword(password);
			session.connect();

			Channel channel = session.openChannel("sftp");
			channel.connect();
			ChannelSftp sftpChannel = (ChannelSftp) channel;
			
			for (String s : src){
				System.out.println("\t--Uploading: " + localDir + s);
				sftpChannel.put(localDir + s, remoteDir + s);
			}

			sftpChannel.exit();
			session.disconnect();
		} catch (JSchException e) {
			System.out.println("JSchException:");
			e.printStackTrace(); // To change body of catch statement use File |
			// Settings | File Templates.
		} catch (SftpException e) {
			System.out.println("SftpException:");
			e.printStackTrace();
		} finally{
			
		}

	}

	/**
	 * Test FTP connection to payer and list directory
	 */
	public static void testConnection() {
		System.out.println("-- Running testConnection " + new Date() + " --");

		String remoteDir = "/home/tynetncc/nextimage/in";
		String host = "ftp.tyson.com";
		String user = "nextimage";
		String password = "P3rf3ct!";

		System.out.println("Testing SFTP connection to " + host);

		JSch jsch = new JSch();
		Session session = null;
		try {
			session = jsch.getSession(user, host, 22);
			session.setConfig("StrictHostKeyChecking", "no");
			session.setPassword(password);
			session.connect();

			Channel channel = session.openChannel("sftp");
			channel.connect();
			ChannelSftp sftpChannel = (ChannelSftp) channel;
			// sftpChannel.cd(remoteDir);
			Vector<ChannelSftp.LsEntry> list = sftpChannel.ls(remoteDir);
			for (ChannelSftp.LsEntry entry : list) {
				System.out.println("Listing: " + entry.getFilename());
			}
			sftpChannel.exit();
			session.disconnect();
		} catch (JSchException e) {
			System.out.println("JSchException:");
			e.printStackTrace(); // To change body of catch statement use File |
			// Settings | File Templates.
		} catch (SftpException e) {
			System.out.println("SftpException:");
			e.printStackTrace();
		}
		System.out.println("-- Done testConnection() " + new Date() + "  --");
	}

	public static class TysonBillObject {

		private int referralid;
		private int userapproveid;
		private String generatedbillpath;
		private List<String> reportids;
		private List<Integer> encounterids;
		private String ptFirst;
		private String ptLast;
		private String doi;
		private String payerName;
		public TysonBillObject(int referralid, int userapproveid, String generatedbillpath, List<String> reportids, List<Integer> encounterids, String ptFirst, String ptLast, String doi, String payerName) {
			this.referralid = referralid;
			this.userapproveid = userapproveid;
			this.generatedbillpath = generatedbillpath;
			this.reportids = reportids;
			this.encounterids = encounterids;
			this.ptFirst = ptFirst;
			this.ptLast = ptLast;
			this.doi = doi;
			this.payerName = payerName;
		}
		public int getReferralid() {
			return referralid;
		}
		public void setReferralid(int referralid) {
			this.referralid = referralid;
		}
		public int getUserapproveid() {
			return userapproveid;
		}
		public void setUserapproveid(int userapproveid) {
			this.userapproveid = userapproveid;
		}
		public String getGeneratedbillpath() {
			return generatedbillpath;
		}
		public void setGeneratedbillpath(String generatedbillpath) {
			this.generatedbillpath = generatedbillpath;
		}
		public List<String> getReportids() {
			return reportids;
		}
		public void setReportids(List<String> reportids) {
			this.reportids = reportids;
		}
		public List<Integer> getEncounterids() {
			return encounterids;
		}
		public void setEncounterids(List<Integer> encounterids) {
			this.encounterids = encounterids;
		}
		public String getPtFirst() {
			return ptFirst;
		}
		public void setPtFirst(String ptFirst) {
			this.ptFirst = ptFirst;
		}
		public String getPtLast() {
			return ptLast;
		}
		public void setPtLast(String ptLast) {
			this.ptLast = ptLast;
		}
		public String getDoi() {
			return doi;
		}
		public void setDoi(String doi) {
			this.doi = doi;
		}
		public String getPayerName() {
			return payerName;
		}
		public void setPayerName(String payerName) {
			this.payerName = payerName;
		}
		
	}

}