

package com.winstaff;

import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public class dbtNIM3_PayerMaster extends Object implements InttNIM3_PayerMaster
{

        db_NewBase    dbnbDB;

    public dbtNIM3_PayerMaster()
    {
        dbnbDB = new db_NewBase( "tNIM3_PayerMaster", "PayerID" );

    }    // End of default constructor

    public dbtNIM3_PayerMaster( Integer iNewID )
    {
        dbnbDB = new db_NewBase( "tNIM3_PayerMaster", "PayerID" );
        setUniqueID( iNewID );
    }    // End of Constructor knowing an ID

    public void commitData() throws SQLException
    {
            //no longer trapping/ignoring errors 2011-03-17
            dbnbDB.commitData();
    }    // End of commitData()

    public void commitDataForced() throws SQLException
    {
        dbnbDB.commitDataForced();
    }    // End of commitData()

    public Integer getUniqueID()
    {
        return dbnbDB.getUniqueID();
    }    // End of getUniqueID()

    public void setUniqueID( Integer iNewID )
    {
        dbnbDB.setUniqueID( iNewID );
    }    // End of setUniqueID()

    public void setModifyComments( String sNew )
    {
        dbnbDB.setModifyComments( sNew );
    }    // End of setUniqueID()


    public void setPayerID(Integer newValue)
    {
                dbnbDB.setFieldData( "PayerID", newValue.toString() );
    }

    public Integer getPayerID()
    {
        String           sValue = dbnbDB.getFieldData( "PayerID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setField(String fieldN, Object fieldV)
    {DebugLogger.println("setField Method not implemented yet in classNIM3_PayerMaster!");}

    public String getFieldType(String fieldN)
    {return null;}


    public void setAutoAuth(String newValue)
    {
                dbnbDB.setFieldData( "AutoAuth", newValue.toString() );
    }

    public String getAutoAuth()
    {
        String           sValue = dbnbDB.getFieldData( "AutoAuth" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setUniqueCreateDate(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "UniqueCreateDate", formatter.format( newValue ) );
    }

    public Date getUniqueCreateDate()
    {
        String           sValue = dbnbDB.getFieldData( "UniqueCreateDate" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setUniqueModifyDate(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "UniqueModifyDate", formatter.format( newValue ) );
    }

    public Date getUniqueModifyDate()
    {
        String           sValue = dbnbDB.getFieldData( "UniqueModifyDate" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setUniqueModifyComments(String newValue)
    {
                dbnbDB.setFieldData( "UniqueModifyComments", newValue.toString() );
    }

    public String getUniqueModifyComments()
    {
        String           sValue = dbnbDB.getFieldData( "UniqueModifyComments" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setPayerName(String newValue)
    {
                dbnbDB.setFieldData( "PayerName", newValue.toString() );
    }

    public String getPayerName()
    {
        String           sValue = dbnbDB.getFieldData( "PayerName" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setContactName(String newValue)
    {
                dbnbDB.setFieldData( "ContactName", newValue.toString() );
    }

    public String getContactName()
    {
        String           sValue = dbnbDB.getFieldData( "ContactName" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setBillingAddress1(String newValue)
    {
                dbnbDB.setFieldData( "BillingAddress1", newValue.toString() );
    }

    public String getBillingAddress1()
    {
        String           sValue = dbnbDB.getFieldData( "BillingAddress1" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setBillingAddress2(String newValue)
    {
                dbnbDB.setFieldData( "BillingAddress2", newValue.toString() );
    }

    public String getBillingAddress2()
    {
        String           sValue = dbnbDB.getFieldData( "BillingAddress2" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setBillingCity(String newValue)
    {
                dbnbDB.setFieldData( "BillingCity", newValue.toString() );
    }

    public String getBillingCity()
    {
        String           sValue = dbnbDB.getFieldData( "BillingCity" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setBillingStateID(Integer newValue)
    {
                dbnbDB.setFieldData( "BillingStateID", newValue.toString() );
    }

    public Integer getBillingStateID()
    {
        String           sValue = dbnbDB.getFieldData( "BillingStateID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setBillingZIP(String newValue)
    {
                dbnbDB.setFieldData( "BillingZIP", newValue.toString() );
    }

    public String getBillingZIP()
    {
        String           sValue = dbnbDB.getFieldData( "BillingZIP" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setBillingPhone(String newValue)
    {
                dbnbDB.setFieldData( "BillingPhone", newValue.toString() );
    }

    public String getBillingPhone()
    {
        String           sValue = dbnbDB.getFieldData( "BillingPhone" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setBillingFax(String newValue)
    {
                dbnbDB.setFieldData( "BillingFax", newValue.toString() );
    }

    public String getBillingFax()
    {
        String           sValue = dbnbDB.getFieldData( "BillingFax" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setBillingEmail(String newValue)
    {
                dbnbDB.setFieldData( "BillingEmail", newValue.toString() );
    }

    public String getBillingEmail()
    {
        String           sValue = dbnbDB.getFieldData( "BillingEmail" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setComments(String newValue)
    {
                dbnbDB.setFieldData( "Comments", newValue.toString() );
    }

    public String getComments()
    {
        String           sValue = dbnbDB.getFieldData( "Comments" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setBillingEntityID(Integer newValue)
    {
                dbnbDB.setFieldData( "BillingEntityID", newValue.toString() );
    }

    public Integer getBillingEntityID()
    {
        String           sValue = dbnbDB.getFieldData( "BillingEntityID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setPayerTypeID(Integer newValue)
    {
                dbnbDB.setFieldData( "PayerTypeID", newValue.toString() );
    }

    public Integer getPayerTypeID()
    {
        String           sValue = dbnbDB.getFieldData( "PayerTypeID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setImportantNotes(String newValue)
    {
                dbnbDB.setFieldData( "ImportantNotes", newValue.toString() );
    }

    public String getImportantNotes()
    {
        String           sValue = dbnbDB.getFieldData( "ImportantNotes" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setPayableTo(String newValue)
    {
                dbnbDB.setFieldData( "PayableTo", newValue.toString() );
    }

    public String getPayableTo()
    {
        String           sValue = dbnbDB.getFieldData( "PayableTo" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setPrice_Mod_MRI_WO(Double newValue)
    {
                java.text.DecimalFormat defDecFormat2 = new java.text.DecimalFormat(PLCUtils.String_defDecFormat2);
                dbnbDB.setFieldData( "Price_Mod_MRI_WO", defDecFormat2.format(newValue) );
    }

    public Double getPrice_Mod_MRI_WO()
    {
        String           sValue = dbnbDB.getFieldData( "Price_Mod_MRI_WO" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDoubleValue : sValue;
        return new Double( sValue  ); 
    }

    public void setPrice_Mod_MRI_W(Double newValue)
    {
                java.text.DecimalFormat defDecFormat2 = new java.text.DecimalFormat(PLCUtils.String_defDecFormat2);
                dbnbDB.setFieldData( "Price_Mod_MRI_W", defDecFormat2.format(newValue) );
    }

    public Double getPrice_Mod_MRI_W()
    {
        String           sValue = dbnbDB.getFieldData( "Price_Mod_MRI_W" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDoubleValue : sValue;
        return new Double( sValue  ); 
    }

    public void setPrice_Mod_MRI_WWO(Double newValue)
    {
                java.text.DecimalFormat defDecFormat2 = new java.text.DecimalFormat(PLCUtils.String_defDecFormat2);
                dbnbDB.setFieldData( "Price_Mod_MRI_WWO", defDecFormat2.format(newValue) );
    }

    public Double getPrice_Mod_MRI_WWO()
    {
        String           sValue = dbnbDB.getFieldData( "Price_Mod_MRI_WWO" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDoubleValue : sValue;
        return new Double( sValue  ); 
    }

    public void setPrice_Mod_CT_WO(Double newValue)
    {
                java.text.DecimalFormat defDecFormat2 = new java.text.DecimalFormat(PLCUtils.String_defDecFormat2);
                dbnbDB.setFieldData( "Price_Mod_CT_WO", defDecFormat2.format(newValue) );
    }

    public Double getPrice_Mod_CT_WO()
    {
        String           sValue = dbnbDB.getFieldData( "Price_Mod_CT_WO" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDoubleValue : sValue;
        return new Double( sValue  ); 
    }

    public void setPrice_Mod_CT_W(Double newValue)
    {
                java.text.DecimalFormat defDecFormat2 = new java.text.DecimalFormat(PLCUtils.String_defDecFormat2);
                dbnbDB.setFieldData( "Price_Mod_CT_W", defDecFormat2.format(newValue) );
    }

    public Double getPrice_Mod_CT_W()
    {
        String           sValue = dbnbDB.getFieldData( "Price_Mod_CT_W" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDoubleValue : sValue;
        return new Double( sValue  ); 
    }

    public void setPrice_Mod_CT_WWO(Double newValue)
    {
                java.text.DecimalFormat defDecFormat2 = new java.text.DecimalFormat(PLCUtils.String_defDecFormat2);
                dbnbDB.setFieldData( "Price_Mod_CT_WWO", defDecFormat2.format(newValue) );
    }

    public Double getPrice_Mod_CT_WWO()
    {
        String           sValue = dbnbDB.getFieldData( "Price_Mod_CT_WWO" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDoubleValue : sValue;
        return new Double( sValue  ); 
    }

    public void setBillPrice_Mod_MRI_WO(Double newValue)
    {
                java.text.DecimalFormat defDecFormat2 = new java.text.DecimalFormat(PLCUtils.String_defDecFormat2);
                dbnbDB.setFieldData( "BillPrice_Mod_MRI_WO", defDecFormat2.format(newValue) );
    }

    public Double getBillPrice_Mod_MRI_WO()
    {
        String           sValue = dbnbDB.getFieldData( "BillPrice_Mod_MRI_WO" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDoubleValue : sValue;
        return new Double( sValue  ); 
    }

    public void setBillPrice_Mod_MRI_W(Double newValue)
    {
                java.text.DecimalFormat defDecFormat2 = new java.text.DecimalFormat(PLCUtils.String_defDecFormat2);
                dbnbDB.setFieldData( "BillPrice_Mod_MRI_W", defDecFormat2.format(newValue) );
    }

    public Double getBillPrice_Mod_MRI_W()
    {
        String           sValue = dbnbDB.getFieldData( "BillPrice_Mod_MRI_W" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDoubleValue : sValue;
        return new Double( sValue  ); 
    }

    public void setBillPrice_Mod_MRI_WWO(Double newValue)
    {
                java.text.DecimalFormat defDecFormat2 = new java.text.DecimalFormat(PLCUtils.String_defDecFormat2);
                dbnbDB.setFieldData( "BillPrice_Mod_MRI_WWO", defDecFormat2.format(newValue) );
    }

    public Double getBillPrice_Mod_MRI_WWO()
    {
        String           sValue = dbnbDB.getFieldData( "BillPrice_Mod_MRI_WWO" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDoubleValue : sValue;
        return new Double( sValue  ); 
    }

    public void setBillPrice_Mod_CT_WO(Double newValue)
    {
                java.text.DecimalFormat defDecFormat2 = new java.text.DecimalFormat(PLCUtils.String_defDecFormat2);
                dbnbDB.setFieldData( "BillPrice_Mod_CT_WO", defDecFormat2.format(newValue) );
    }

    public Double getBillPrice_Mod_CT_WO()
    {
        String           sValue = dbnbDB.getFieldData( "BillPrice_Mod_CT_WO" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDoubleValue : sValue;
        return new Double( sValue  ); 
    }

    public void setBillPrice_Mod_CT_W(Double newValue)
    {
                java.text.DecimalFormat defDecFormat2 = new java.text.DecimalFormat(PLCUtils.String_defDecFormat2);
                dbnbDB.setFieldData( "BillPrice_Mod_CT_W", defDecFormat2.format(newValue) );
    }

    public Double getBillPrice_Mod_CT_W()
    {
        String           sValue = dbnbDB.getFieldData( "BillPrice_Mod_CT_W" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDoubleValue : sValue;
        return new Double( sValue  ); 
    }

    public void setBillPrice_Mod_CT_WWO(Double newValue)
    {
                java.text.DecimalFormat defDecFormat2 = new java.text.DecimalFormat(PLCUtils.String_defDecFormat2);
                dbnbDB.setFieldData( "BillPrice_Mod_CT_WWO", defDecFormat2.format(newValue) );
    }

    public Double getBillPrice_Mod_CT_WWO()
    {
        String           sValue = dbnbDB.getFieldData( "BillPrice_Mod_CT_WWO" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDoubleValue : sValue;
        return new Double( sValue  ); 
    }

    public void setFeeScheduleRefID(Integer newValue)
    {
                dbnbDB.setFieldData( "FeeScheduleRefID", newValue.toString() );
    }

    public Integer getFeeScheduleRefID()
    {
        String           sValue = dbnbDB.getFieldData( "FeeScheduleRefID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setFeePercentage(Double newValue)
    {
                java.text.DecimalFormat defDecFormat2 = new java.text.DecimalFormat(PLCUtils.String_defDecFormat2);
                dbnbDB.setFieldData( "FeePercentage", defDecFormat2.format(newValue) );
    }

    public Double getFeePercentage()
    {
        String           sValue = dbnbDB.getFieldData( "FeePercentage" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDoubleValue : sValue;
        return new Double( sValue  ); 
    }

    public void setFeePercentageBill(Double newValue)
    {
                java.text.DecimalFormat defDecFormat2 = new java.text.DecimalFormat(PLCUtils.String_defDecFormat2);
                dbnbDB.setFieldData( "FeePercentageBill", defDecFormat2.format(newValue) );
    }

    public Double getFeePercentageBill()
    {
        String           sValue = dbnbDB.getFieldData( "FeePercentageBill" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDoubleValue : sValue;
        return new Double( sValue  ); 
    }

    public void setFeeSchedule2RefID(Integer newValue)
    {
                dbnbDB.setFieldData( "FeeSchedule2RefID", newValue.toString() );
    }

    public Integer getFeeSchedule2RefID()
    {
        String           sValue = dbnbDB.getFieldData( "FeeSchedule2RefID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setFeePercentage2(Double newValue)
    {
                java.text.DecimalFormat defDecFormat2 = new java.text.DecimalFormat(PLCUtils.String_defDecFormat2);
                dbnbDB.setFieldData( "FeePercentage2", defDecFormat2.format(newValue) );
    }

    public Double getFeePercentage2()
    {
        String           sValue = dbnbDB.getFieldData( "FeePercentage2" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDoubleValue : sValue;
        return new Double( sValue  ); 
    }

    public void setFeePercentage2Bill(Double newValue)
    {
                java.text.DecimalFormat defDecFormat2 = new java.text.DecimalFormat(PLCUtils.String_defDecFormat2);
                dbnbDB.setFieldData( "FeePercentage2Bill", defDecFormat2.format(newValue) );
    }

    public Double getFeePercentage2Bill()
    {
        String           sValue = dbnbDB.getFieldData( "FeePercentage2Bill" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDoubleValue : sValue;
        return new Double( sValue  ); 
    }

    public void setBillingTypeID(Integer newValue)
    {
                dbnbDB.setFieldData( "BillingTypeID", newValue.toString() );
    }

    public Integer getBillingTypeID()
    {
        String           sValue = dbnbDB.getFieldData( "BillingTypeID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setAcceptsEMGCaseRate(Integer newValue)
    {
                dbnbDB.setFieldData( "AcceptsEMGCaseRate", newValue.toString() );
    }

    public Integer getAcceptsEMGCaseRate()
    {
        String           sValue = dbnbDB.getFieldData( "AcceptsEMGCaseRate" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setParentPayerID(Integer newValue)
    {
                dbnbDB.setFieldData( "ParentPayerID", newValue.toString() );
    }

    public Integer getParentPayerID()
    {
        String           sValue = dbnbDB.getFieldData( "ParentPayerID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setSelectMRI_ID(Integer newValue)
    {
                dbnbDB.setFieldData( "SelectMRI_ID", newValue.toString() );
    }

    public Integer getSelectMRI_ID()
    {
        String           sValue = dbnbDB.getFieldData( "SelectMRI_ID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setSelectMRI_Notes(String newValue)
    {
                dbnbDB.setFieldData( "SelectMRI_Notes", newValue.toString() );
    }

    public String getSelectMRI_Notes()
    {
        String           sValue = dbnbDB.getFieldData( "SelectMRI_Notes" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setSelectMRI_QBID(String newValue)
    {
                dbnbDB.setFieldData( "SelectMRI_QBID", newValue.toString() );
    }

    public String getSelectMRI_QBID()
    {
        String           sValue = dbnbDB.getFieldData( "SelectMRI_QBID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setBillingName(String newValue)
    {
                dbnbDB.setFieldData( "BillingName", newValue.toString() );
    }

    public String getBillingName()
    {
        String           sValue = dbnbDB.getFieldData( "BillingName" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setOfficeAddress1(String newValue)
    {
                dbnbDB.setFieldData( "OfficeAddress1", newValue.toString() );
    }

    public String getOfficeAddress1()
    {
        String           sValue = dbnbDB.getFieldData( "OfficeAddress1" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setOfficeAddress2(String newValue)
    {
                dbnbDB.setFieldData( "OfficeAddress2", newValue.toString() );
    }

    public String getOfficeAddress2()
    {
        String           sValue = dbnbDB.getFieldData( "OfficeAddress2" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setOfficeCity(String newValue)
    {
                dbnbDB.setFieldData( "OfficeCity", newValue.toString() );
    }

    public String getOfficeCity()
    {
        String           sValue = dbnbDB.getFieldData( "OfficeCity" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setOfficeStateID(Integer newValue)
    {
                dbnbDB.setFieldData( "OfficeStateID", newValue.toString() );
    }

    public Integer getOfficeStateID()
    {
        String           sValue = dbnbDB.getFieldData( "OfficeStateID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setOfficeZIP(String newValue)
    {
                dbnbDB.setFieldData( "OfficeZIP", newValue.toString() );
    }

    public String getOfficeZIP()
    {
        String           sValue = dbnbDB.getFieldData( "OfficeZIP" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setOfficePhone(String newValue)
    {
                dbnbDB.setFieldData( "OfficePhone", newValue.toString() );
    }

    public String getOfficePhone()
    {
        String           sValue = dbnbDB.getFieldData( "OfficePhone" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setOfficeFax(String newValue)
    {
                dbnbDB.setFieldData( "OfficeFax", newValue.toString() );
    }

    public String getOfficeFax()
    {
        String           sValue = dbnbDB.getFieldData( "OfficeFax" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setOfficeEmail(String newValue)
    {
                dbnbDB.setFieldData( "OfficeEmail", newValue.toString() );
    }

    public String getOfficeEmail()
    {
        String           sValue = dbnbDB.getFieldData( "OfficeEmail" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setSalesDivision(String newValue)
    {
                dbnbDB.setFieldData( "SalesDivision", newValue.toString() );
    }

    public String getSalesDivision()
    {
        String           sValue = dbnbDB.getFieldData( "SalesDivision" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setAcquisitionDivision(String newValue)
    {
                dbnbDB.setFieldData( "AcquisitionDivision", newValue.toString() );
    }

    public String getAcquisitionDivision()
    {
        String           sValue = dbnbDB.getFieldData( "AcquisitionDivision" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setContract1_FeeScheduleID(Integer newValue)
    {
                dbnbDB.setFieldData( "Contract1_FeeScheduleID", newValue.toString() );
    }

    public Integer getContract1_FeeScheduleID()
    {
        String           sValue = dbnbDB.getFieldData( "Contract1_FeeScheduleID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setContract1_FeePercentage(Double newValue)
    {
                java.text.DecimalFormat defDecFormat2 = new java.text.DecimalFormat(PLCUtils.String_defDecFormat2);
                dbnbDB.setFieldData( "Contract1_FeePercentage", defDecFormat2.format(newValue) );
    }

    public Double getContract1_FeePercentage()
    {
        String           sValue = dbnbDB.getFieldData( "Contract1_FeePercentage" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDoubleValue : sValue;
        return new Double( sValue  ); 
    }

    public void setContract2_FeeScheduleID(Integer newValue)
    {
                dbnbDB.setFieldData( "Contract2_FeeScheduleID", newValue.toString() );
    }

    public Integer getContract2_FeeScheduleID()
    {
        String           sValue = dbnbDB.getFieldData( "Contract2_FeeScheduleID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setContract2_FeePercentage(Double newValue)
    {
                java.text.DecimalFormat defDecFormat2 = new java.text.DecimalFormat(PLCUtils.String_defDecFormat2);
                dbnbDB.setFieldData( "Contract2_FeePercentage", defDecFormat2.format(newValue) );
    }

    public Double getContract2_FeePercentage()
    {
        String           sValue = dbnbDB.getFieldData( "Contract2_FeePercentage" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDoubleValue : sValue;
        return new Double( sValue  ); 
    }

    public void setContract3_FeeScheduleID(Integer newValue)
    {
                dbnbDB.setFieldData( "Contract3_FeeScheduleID", newValue.toString() );
    }

    public Integer getContract3_FeeScheduleID()
    {
        String           sValue = dbnbDB.getFieldData( "Contract3_FeeScheduleID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setContract3_FeePercentage(Double newValue)
    {
                java.text.DecimalFormat defDecFormat2 = new java.text.DecimalFormat(PLCUtils.String_defDecFormat2);
                dbnbDB.setFieldData( "Contract3_FeePercentage", defDecFormat2.format(newValue) );
    }

    public Double getContract3_FeePercentage()
    {
        String           sValue = dbnbDB.getFieldData( "Contract3_FeePercentage" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDoubleValue : sValue;
        return new Double( sValue  ); 
    }

    public void setBill1_FeeScheduleID(Integer newValue)
    {
                dbnbDB.setFieldData( "Bill1_FeeScheduleID", newValue.toString() );
    }

    public Integer getBill1_FeeScheduleID()
    {
        String           sValue = dbnbDB.getFieldData( "Bill1_FeeScheduleID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setBill1_FeePercentage(Double newValue)
    {
                java.text.DecimalFormat defDecFormat2 = new java.text.DecimalFormat(PLCUtils.String_defDecFormat2);
                dbnbDB.setFieldData( "Bill1_FeePercentage", defDecFormat2.format(newValue) );
    }

    public Double getBill1_FeePercentage()
    {
        String           sValue = dbnbDB.getFieldData( "Bill1_FeePercentage" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDoubleValue : sValue;
        return new Double( sValue  ); 
    }

    public void setBill2_FeeScheduleID(Integer newValue)
    {
                dbnbDB.setFieldData( "Bill2_FeeScheduleID", newValue.toString() );
    }

    public Integer getBill2_FeeScheduleID()
    {
        String           sValue = dbnbDB.getFieldData( "Bill2_FeeScheduleID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setBill2_FeePercentage(Double newValue)
    {
                java.text.DecimalFormat defDecFormat2 = new java.text.DecimalFormat(PLCUtils.String_defDecFormat2);
                dbnbDB.setFieldData( "Bill2_FeePercentage", defDecFormat2.format(newValue) );
    }

    public Double getBill2_FeePercentage()
    {
        String           sValue = dbnbDB.getFieldData( "Bill2_FeePercentage" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDoubleValue : sValue;
        return new Double( sValue  ); 
    }

    public void setBill3_FeeScheduleID(Integer newValue)
    {
                dbnbDB.setFieldData( "Bill3_FeeScheduleID", newValue.toString() );
    }

    public Integer getBill3_FeeScheduleID()
    {
        String           sValue = dbnbDB.getFieldData( "Bill3_FeeScheduleID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setBill3_FeePercentage(Double newValue)
    {
                java.text.DecimalFormat defDecFormat2 = new java.text.DecimalFormat(PLCUtils.String_defDecFormat2);
                dbnbDB.setFieldData( "Bill3_FeePercentage", defDecFormat2.format(newValue) );
    }

    public Double getBill3_FeePercentage()
    {
        String           sValue = dbnbDB.getFieldData( "Bill3_FeePercentage" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDoubleValue : sValue;
        return new Double( sValue  ); 
    }

    public void setImportantNotes_Alert(String newValue)
    {
                dbnbDB.setFieldData( "ImportantNotes_Alert", newValue.toString() );
    }

    public String getImportantNotes_Alert()
    {
        String           sValue = dbnbDB.getFieldData( "ImportantNotes_Alert" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setEmailAlertNotes_Alert(String newValue)
    {
                dbnbDB.setFieldData( "EmailAlertNotes_Alert", newValue.toString() );
    }

    public String getEmailAlertNotes_Alert()
    {
        String           sValue = dbnbDB.getFieldData( "EmailAlertNotes_Alert" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setRequiresOnlineImage(Integer newValue)
    {
                dbnbDB.setFieldData( "RequiresOnlineImage", newValue.toString() );
    }

    public Integer getRequiresOnlineImage()
    {
        String           sValue = dbnbDB.getFieldData( "RequiresOnlineImage" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setRequiresAging(Integer newValue)
    {
                dbnbDB.setFieldData( "RequiresAging", newValue.toString() );
    }

    public Integer getRequiresAging()
    {
        String           sValue = dbnbDB.getFieldData( "RequiresAging" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setSales_Monthly_Target(Integer newValue)
    {
                dbnbDB.setFieldData( "Sales_Monthly_Target", newValue.toString() );
    }

    public Integer getSales_Monthly_Target()
    {
        String           sValue = dbnbDB.getFieldData( "Sales_Monthly_Target" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setAcceptsTier2(Integer newValue)
    {
                dbnbDB.setFieldData( "AcceptsTier2", newValue.toString() );
    }

    public Integer getAcceptsTier2()
    {
        String           sValue = dbnbDB.getFieldData( "AcceptsTier2" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setAcceptsTier3(Integer newValue)
    {
                dbnbDB.setFieldData( "AcceptsTier3", newValue.toString() );
    }

    public Integer getAcceptsTier3()
    {
        String           sValue = dbnbDB.getFieldData( "AcceptsTier3" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setTier2_MinSavings(Double newValue)
    {
                java.text.DecimalFormat defDecFormat2 = new java.text.DecimalFormat(PLCUtils.String_defDecFormat2);
                dbnbDB.setFieldData( "Tier2_MinSavings", defDecFormat2.format(newValue) );
    }

    public Double getTier2_MinSavings()
    {
        String           sValue = dbnbDB.getFieldData( "Tier2_MinSavings" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDoubleValue : sValue;
        return new Double( sValue  ); 
    }

    public void setTier2_TargetSavings(Double newValue)
    {
                java.text.DecimalFormat defDecFormat2 = new java.text.DecimalFormat(PLCUtils.String_defDecFormat2);
                dbnbDB.setFieldData( "Tier2_TargetSavings", defDecFormat2.format(newValue) );
    }

    public Double getTier2_TargetSavings()
    {
        String           sValue = dbnbDB.getFieldData( "Tier2_TargetSavings" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDoubleValue : sValue;
        return new Double( sValue  ); 
    }

    public void setTier3_Fee(Double newValue)
    {
                java.text.DecimalFormat defDecFormat2 = new java.text.DecimalFormat(PLCUtils.String_defDecFormat2);
                dbnbDB.setFieldData( "Tier3_Fee", defDecFormat2.format(newValue) );
    }

    public Double getTier3_Fee()
    {
        String           sValue = dbnbDB.getFieldData( "Tier3_Fee" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDoubleValue : sValue;
        return new Double( sValue  ); 
    }

    public void setIsNID(Integer newValue)
    {
                dbnbDB.setFieldData( "IsNID", newValue.toString() );
    }

    public Integer getIsNID()
    {
        String           sValue = dbnbDB.getFieldData( "IsNID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setNID_WebCode(String newValue)
    {
                dbnbDB.setFieldData( "NID_WebCode", newValue.toString() );
    }

    public String getNID_WebCode()
    {
        String           sValue = dbnbDB.getFieldData( "NID_WebCode" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setNID_ShowLogo(Integer newValue)
    {
                dbnbDB.setFieldData( "NID_ShowLogo", newValue.toString() );
    }

    public Integer getNID_ShowLogo()
    {
        String           sValue = dbnbDB.getFieldData( "NID_ShowLogo" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setNID_WebLogo(String newValue)
    {
                dbnbDB.setFieldData( "NID_WebLogo", newValue.toString() );
    }

    public String getNID_WebLogo()
    {
        String           sValue = dbnbDB.getFieldData( "NID_WebLogo" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setNID_ReferralSource(Integer newValue)
    {
                dbnbDB.setFieldData( "NID_ReferralSource", newValue.toString() );
    }

    public Integer getNID_ReferralSource()
    {
        String           sValue = dbnbDB.getFieldData( "NID_ReferralSource" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setCommissionHighPercentageSelf(Double newValue)
    {
                java.text.DecimalFormat defDecFormat2 = new java.text.DecimalFormat(PLCUtils.String_defDecFormat2);
                dbnbDB.setFieldData( "CommissionHighPercentageSelf", defDecFormat2.format(newValue) );
    }

    public Double getCommissionHighPercentageSelf()
    {
        String           sValue = dbnbDB.getFieldData( "CommissionHighPercentageSelf" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDoubleValue : sValue;
        return new Double( sValue  ); 
    }

    public void setCommissionLowPercentageSelf(Double newValue)
    {
                java.text.DecimalFormat defDecFormat2 = new java.text.DecimalFormat(PLCUtils.String_defDecFormat2);
                dbnbDB.setFieldData( "CommissionLowPercentageSelf", defDecFormat2.format(newValue) );
    }

    public Double getCommissionLowPercentageSelf()
    {
        String           sValue = dbnbDB.getFieldData( "CommissionLowPercentageSelf" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDoubleValue : sValue;
        return new Double( sValue  ); 
    }

    public void setCommissionHighPercentageParent(Double newValue)
    {
                java.text.DecimalFormat defDecFormat2 = new java.text.DecimalFormat(PLCUtils.String_defDecFormat2);
                dbnbDB.setFieldData( "CommissionHighPercentageParent", defDecFormat2.format(newValue) );
    }

    public Double getCommissionHighPercentageParent()
    {
        String           sValue = dbnbDB.getFieldData( "CommissionHighPercentageParent" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDoubleValue : sValue;
        return new Double( sValue  ); 
    }

    public void setCommissionLowPercentageParent(Double newValue)
    {
                java.text.DecimalFormat defDecFormat2 = new java.text.DecimalFormat(PLCUtils.String_defDecFormat2);
                dbnbDB.setFieldData( "CommissionLowPercentageParent", defDecFormat2.format(newValue) );
    }

    public Double getCommissionLowPercentageParent()
    {
        String           sValue = dbnbDB.getFieldData( "CommissionLowPercentageParent" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDoubleValue : sValue;
        return new Double( sValue  ); 
    }

    public void setCommissionNotes(String newValue)
    {
                dbnbDB.setFieldData( "CommissionNotes", newValue.toString() );
    }

    public String getCommissionNotes()
    {
        String           sValue = dbnbDB.getFieldData( "CommissionNotes" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setDiscountHighAmount(Double newValue)
    {
                java.text.DecimalFormat defDecFormat2 = new java.text.DecimalFormat(PLCUtils.String_defDecFormat2);
                dbnbDB.setFieldData( "DiscountHighAmount", defDecFormat2.format(newValue) );
    }

    public Double getDiscountHighAmount()
    {
        String           sValue = dbnbDB.getFieldData( "DiscountHighAmount" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDoubleValue : sValue;
        return new Double( sValue  ); 
    }

    public void setDiscountHighPercentage(Double newValue)
    {
                java.text.DecimalFormat defDecFormat2 = new java.text.DecimalFormat(PLCUtils.String_defDecFormat2);
                dbnbDB.setFieldData( "DiscountHighPercentage", defDecFormat2.format(newValue) );
    }

    public Double getDiscountHighPercentage()
    {
        String           sValue = dbnbDB.getFieldData( "DiscountHighPercentage" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDoubleValue : sValue;
        return new Double( sValue  ); 
    }

    public void setDiscountLowAmount(Double newValue)
    {
                java.text.DecimalFormat defDecFormat2 = new java.text.DecimalFormat(PLCUtils.String_defDecFormat2);
                dbnbDB.setFieldData( "DiscountLowAmount", defDecFormat2.format(newValue) );
    }

    public Double getDiscountLowAmount()
    {
        String           sValue = dbnbDB.getFieldData( "DiscountLowAmount" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDoubleValue : sValue;
        return new Double( sValue  ); 
    }

    public void setDiscountLowPercentage(Double newValue)
    {
                java.text.DecimalFormat defDecFormat2 = new java.text.DecimalFormat(PLCUtils.String_defDecFormat2);
                dbnbDB.setFieldData( "DiscountLowPercentage", defDecFormat2.format(newValue) );
    }

    public Double getDiscountLowPercentage()
    {
        String           sValue = dbnbDB.getFieldData( "DiscountLowPercentage" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDoubleValue : sValue;
        return new Double( sValue  ); 
    }

    public void setCustomerDisplayName(String newValue)
    {
                dbnbDB.setFieldData( "CustomerDisplayName", newValue.toString() );
    }

    public String getCustomerDisplayName()
    {
        String           sValue = dbnbDB.getFieldData( "CustomerDisplayName" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setNID_ShowCode(Integer newValue)
    {
                dbnbDB.setFieldData( "NID_ShowCode", newValue.toString() );
    }

    public Integer getNID_ShowCode()
    {
        String           sValue = dbnbDB.getFieldData( "NID_ShowCode" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setNID_CodeDisplayName(String newValue)
    {
                dbnbDB.setFieldData( "NID_CodeDisplayName", newValue.toString() );
    }

    public String getNID_CodeDisplayName()
    {
        String           sValue = dbnbDB.getFieldData( "NID_CodeDisplayName" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setNID_ShowSource(Integer newValue)
    {
                dbnbDB.setFieldData( "NID_ShowSource", newValue.toString() );
    }

    public Integer getNID_ShowSource()
    {
        String           sValue = dbnbDB.getFieldData( "NID_ShowSource" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setNID_SourceDisplayName(String newValue)
    {
                dbnbDB.setFieldData( "NID_SourceDisplayName", newValue.toString() );
    }

    public String getNID_SourceDisplayName()
    {
        String           sValue = dbnbDB.getFieldData( "NID_SourceDisplayName" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setNID_ShowBanner(Integer newValue)
    {
                dbnbDB.setFieldData( "NID_ShowBanner", newValue.toString() );
    }

    public Integer getNID_ShowBanner()
    {
        String           sValue = dbnbDB.getFieldData( "NID_ShowBanner" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setNID_WelcomeMessage(String newValue)
    {
                dbnbDB.setFieldData( "NID_WelcomeMessage", newValue.toString() );
    }

    public String getNID_WelcomeMessage()
    {
        String           sValue = dbnbDB.getFieldData( "NID_WelcomeMessage" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setNID_DiscountPercentageMessage(String newValue)
    {
                dbnbDB.setFieldData( "NID_DiscountPercentageMessage", newValue.toString() );
    }

    public String getNID_DiscountPercentageMessage()
    {
        String           sValue = dbnbDB.getFieldData( "NID_DiscountPercentageMessage" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setInsideSalesDivision(String newValue)
    {
                dbnbDB.setFieldData( "InsideSalesDivision", newValue.toString() );
    }

    public String getInsideSalesDivision()
    {
        String           sValue = dbnbDB.getFieldData( "InsideSalesDivision" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

}    // End of bltNIM3_PayerMaster class definition
