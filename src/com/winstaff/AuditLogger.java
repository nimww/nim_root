package com.winstaff;
import java.io.DataOutputStream;
import java.io.FileOutputStream;
import java.util.Date;
import java.text.SimpleDateFormat;

public class AuditLogger
{
    static String              sFileName        = "";
    static DataOutputStream    out;
    static boolean             firstTime        = true;
    static int                 linesOutputed    = 0;
    static int                 maxLinesOutput   = 75000;
    
    
  	/**
	 * <i>Default constructor.</i>
	 * 
	 */
    public AuditLogger()
    {
    }   // End of HistoryLogger()

  	/**
	 * <i>Output a line to the current log file.</i>
	 * 
	 * @param       String      Output string to send to log file.
	 */
    public void println( String s )
    {
        if ( (!s.equalsIgnoreCase(""))&&(!s.equalsIgnoreCase("     ")) )
        {
//            System.out.println(">>"+s);
            printLine(s);   
        }
    }
    public static void printLine( String s )
    {
        SimpleDateFormat    formatter1              = new SimpleDateFormat( "MM_dd_yyyy_hh_mm_ss" );
        String              newDateFileNameString   = formatter1.format( new Date() );
        
        
        try
        {
            if ( firstTime )
            {
                firstTime = false;
                
                linesOutputed = 0;                
            
                sFileName = ConfigurationInformation.sLogFolder + "AUDIT_" + newDateFileNameString + ".aud";
            
                out = new DataOutputStream( new FileOutputStream( sFileName ) );
            }

            out.writeBytes( s );
            out.writeBytes( "\n" );
            out.flush();
            
            if ( linesOutputed++ > maxLinesOutput )
            {
                out.close();
                firstTime = true;
            }            
        }
        catch ( Exception e )
        {
            System.out.println( "Audit Logger error: " + e.toString() );
        }
        
    }   // End of println()
    
}   // End of HistoryLogger()
