

package com.winstaff;

import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public class dbtYesNoLI extends Object implements InttYesNoLI
{

        db_NewBase    dbnbDB;

    public dbtYesNoLI()
    {
        dbnbDB = new db_NewBase( "tYesNoLI", "YesNoID" );

    }    // End of default constructor

    public dbtYesNoLI( Integer iNewID )
    {
        dbnbDB = new db_NewBase( "tYesNoLI", "YesNoID" );
        setUniqueID( iNewID );
    }    // End of Constructor knowing an ID

    public void commitData() throws SQLException
    {
            //no longer trapping/ignoring errors 2011-03-17
            dbnbDB.commitData();
    }    // End of commitData()

    public void commitDataForced() throws SQLException
    {
        dbnbDB.commitDataForced();
    }    // End of commitData()

    public Integer getUniqueID()
    {
        return dbnbDB.getUniqueID();
    }    // End of getUniqueID()

    public void setUniqueID( Integer iNewID )
    {
        dbnbDB.setUniqueID( iNewID );
    }    // End of setUniqueID()

    public void setModifyComments( String sNew )
    {
        dbnbDB.setModifyComments( sNew );
    }    // End of setUniqueID()


    public void setYesNoID(Integer newValue)
    {
                dbnbDB.setFieldData( "YesNoID", newValue.toString() );
    }

    public Integer getYesNoID()
    {
        String           sValue = dbnbDB.getFieldData( "YesNoID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setField(String fieldN, Object fieldV)
    {DebugLogger.println("setField Method not implemented yet in class!");}

    public String getFieldType(String fieldN)
    {return null;}


    public void setYNShort(String newValue)
    {
                dbnbDB.setFieldData( "YNShort", newValue.toString() );
    }

    public String getYNShort()
    {
        String           sValue = dbnbDB.getFieldData( "YNShort" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setYNLong(String newValue)
    {
                dbnbDB.setFieldData( "YNLong", newValue.toString() );
    }

    public String getYNLong()
    {
        String           sValue = dbnbDB.getFieldData( "YNLong" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

}    // End of bltYesNoLI class definition
