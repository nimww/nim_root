package com.winstaff;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Vector;

import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.net.ftp.FTPReply;
import org.apache.commons.net.ftp.FTPSClient;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;

public class EDI_Crawford {

    ConfigurationInformation config = new ConfigurationInformation();

    // Hard-coded value for Crawford Payer ID
    int crawfordPayerID = 326;

//	static String localDir = "/var/lib/" + ConfigurationInformation.sTomcat + "/webapps/EDI/crawford/";
//	static String localDir = "C:\\Users\\btdev\\Desktop\\NIM\\Crawford\\";
//	static String in = "C:\\Users\\btdev\\Desktop\\NIM\\Crawford\\Elg_20130127130232.txt";

    private String localDir = "/var/lib/" + ConfigurationInformation.sTomcat + "/webapps/EDI/crawford/";
//
//	static public void main_mytesting(String args[]) {
//
//		EDI_Crawford crawford = new EDI_Crawford();
//
//		try {
//			crawford.loadFile(localDir+in);
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//
//
//	}

    /**
     * - Open file.
     * - Read line by line.
     * - Insert into table.
     *
     * @param in
     */
    public int loadFile (String in) throws Exception {

        BufferedReader bufferedReader = null;
        bufferedReader = new BufferedReader(new FileReader(this.getLocalDir()+in));
        String line = null;

        String CLAIM_ID;
        String PATIENT_NUMBER;
        String CLAIM_SYSTEM;
        String PATIENT_ABBR;
        String PATIENT_ADDRESS_LINE1;
        String PATIENT_ADDRESS_LINE2;
        String PATIENT_AWW;
        String PATIENT_CITY;
        String PATIENT_COMP_RATE;
        String PATIENT_COUNTRY;
        String PATIENT_FIRST_NAME;
        String PATIENT_GENDER;
        String PATIENT_LAST_NAME;
        String PATIENT_MIDDLE_INITIAL;
        String PATIENT_MISC;
        String PATIENT_OCCUPATION;
        String PATIENT_PHONE;
        String PATIENT_PHONE_EXT;
        String PATIENT_SSN;
        String PATIENT_STATE;
        String PATIENT_SUFFIX;
        String PATIENT_WEEKLY_RATE;
        String PATIENT_WORK_PHONE;
        String PATIENT_WORK_PHONE_EXT;
        String PATIENT_ZIP_CODE;
        String PATIENT_ZIP_EXT;

        String PATIENT_DOB;
        String PATIENT_DOJ;
        String PATIENT_FIRST_LOST;
        String PATIENT_BENEFIT_STATE;
        String ACCIDENT_CAUSE;
        String ACCIDENT_DESC1;
        String ACCIDENT_DESC2;

        String ADJUSTER_EMAIL;
        String ADJUSTER_FIRST_NAME;
        String ADJUSTER_LAST_NAME;
        String ADJUSTER_ID;
        String ADJUSTER_PHONE;
        String ADJUSTER_PHONE_EXT;

        String BRANCH_ADDRESS_LINE1;
        String BRANCH_ADDRESS_LINE2;
        String BRANCH_CITY;
        String BRANCH_FAX;
        String BRANCH_STATE;
        String BRANCH_ZIP_CODE;

        String EMPLOYER_ADDRESS_LINE1;
        String EMPLOYER_ADDRESS_LINE2;
        String EMPLOYER_CITY;
        String EMPLOYER_NAME;
        String EMPLOYER_STATE;
        String EMPLOYER_ZIP_CODE;


        int count_total = 0;

        while ((line = bufferedReader.readLine()) != null) {
            try {
                //check for the end
                if (line.trim().equalsIgnoreCase(count_total+"")){
                    System.out.println("----> Reached EOF [lines=" + count_total + "]");
                    break;
                }
                count_total++;
                CLAIM_ID = line.substring(0, 20).trim();
                PATIENT_NUMBER = line.substring(20, 38).trim();
                CLAIM_SYSTEM = line.substring(38, 88).trim();
                PATIENT_ABBR = line.substring(88, 100).trim();
                PATIENT_ADDRESS_LINE1 = line.substring(100, 130).trim();
                PATIENT_ADDRESS_LINE2 = line.substring(130, 160).trim();
                PATIENT_AWW = line.substring(160, 169).trim();
                PATIENT_CITY = line.substring(169, 189).trim();
                PATIENT_COMP_RATE = line.substring(189, 198).trim();

                PATIENT_COUNTRY = line.substring(198, 228).trim();
                PATIENT_FIRST_NAME = line.substring(228, 258).trim();
                PATIENT_GENDER = line.substring(258, 259).trim();
                PATIENT_LAST_NAME = line.substring(259, 289).trim();
                PATIENT_MIDDLE_INITIAL = line.substring(289, 290).trim();

                PATIENT_MISC = line.substring(290, 296).trim();
                PATIENT_OCCUPATION = line.substring(296, 346).trim();
                PATIENT_PHONE = line.substring(346, 356).trim();
                PATIENT_PHONE_EXT = line.substring(356, 360).trim();
                PATIENT_SSN = line.substring(360, 369).trim();
                PATIENT_STATE = line.substring(369, 371).trim();
                PATIENT_SUFFIX = line.substring(371, 374).trim();
                PATIENT_WEEKLY_RATE = line.substring(374, 383).trim();
                PATIENT_WORK_PHONE = line.substring(383, 393).trim();
                PATIENT_WORK_PHONE_EXT = line.substring(393, 397).trim();
                PATIENT_ZIP_CODE = line.substring(397, 408).trim();
                PATIENT_ZIP_EXT = line.substring(406, 410).trim();

                //TODO: adding new stuff
                SimpleDateFormat translateDate = new SimpleDateFormat("MMddyyyy");
                PATIENT_DOB = line.substring(410, 418).trim();
                PATIENT_DOJ = line.substring(418, 426).trim();
                PATIENT_BENEFIT_STATE = line.substring(434, 436).trim();
                //Note: skipping accident address
                ACCIDENT_CAUSE = line.substring(496, 536).trim();
                ACCIDENT_DESC1 = line.substring(556, 596).trim();
                ACCIDENT_DESC2 = line.substring(596, 636).trim();

                ADJUSTER_EMAIL = line.substring(705, 785).trim();
                ADJUSTER_FIRST_NAME = line.substring(785, 815).trim();
                ADJUSTER_LAST_NAME = line.substring(825, 889).trim();
                ADJUSTER_ID = line.substring(815, 825).trim();
                ADJUSTER_PHONE = line.substring(889, 899).trim();
                ADJUSTER_PHONE_EXT = line.substring(899, 904).trim();

                BRANCH_ADDRESS_LINE1 = line.substring(904, 934).trim();
                BRANCH_ADDRESS_LINE2 = line.substring(934, 964).trim();
                BRANCH_CITY = line.substring(964, 984).trim();
                BRANCH_FAX = line.substring(984, 994).trim();
                BRANCH_STATE = line.substring(1074, 1076).trim();
                BRANCH_ZIP_CODE = line.substring(1076, 1086).trim();

                EMPLOYER_ADDRESS_LINE1 = line.substring(1184, 1214).trim();
                EMPLOYER_ADDRESS_LINE2 = line.substring(1214, 1244).trim();
                EMPLOYER_CITY = line.substring(1244, 1264).trim();
                EMPLOYER_NAME = line.substring(1313, 1353).trim();
                EMPLOYER_STATE = line.substring(1353, 1355).trim();
                EMPLOYER_ZIP_CODE = line.substring(1355, 1364).trim();



                //let's write to the DB using ORM
                System.out.print("------> " + count_total);
                bltNIM3_EligibilityReference temp_eligibility = this.getOrCreateEligibilityReference(CLAIM_ID, true);
                temp_eligibility.setUniqueModifyDate(PLCUtils.getNowDate(false));
                temp_eligibility.setUniqueModifyComments("Most Recently from file [" + in + "]");

                temp_eligibility.setPatientAccountNumber(PATIENT_NUMBER);
                temp_eligibility.setPatientAddress1(PATIENT_ADDRESS_LINE1);
                temp_eligibility.setPatientAddress2(PATIENT_ADDRESS_LINE2);
                temp_eligibility.setPatientCellPhone(PATIENT_PHONE);
                temp_eligibility.setPatientCity(PATIENT_CITY);
                temp_eligibility.setPatientFirstName(PATIENT_FIRST_NAME + " " + PATIENT_MIDDLE_INITIAL);
                temp_eligibility.setPatientGender(PATIENT_GENDER);
                try {
                    temp_eligibility.setDateOfInjury(translateDate.parse(PATIENT_DOJ));
                } catch (java.text.ParseException pe){
                    System.out.println("Warning L2[^^]: Invalid Date of Injury: skipping this one field");
                }
                try {
                    temp_eligibility.setPatientDOB(translateDate.parse(PATIENT_DOB));
                } catch (java.text.ParseException pe){
                    System.out.println("Warning L2[^^]: Invalid Date of Birth: skipping this one field");
                }
                String temp_pat_phone = PATIENT_PHONE;
                if (!PATIENT_PHONE_EXT.equalsIgnoreCase("")){
                    temp_pat_phone+= " ext. " + PATIENT_PHONE_EXT;
                }
                temp_eligibility.setPatientHomePhone(temp_pat_phone);
                temp_eligibility.setPatientLastName(PATIENT_LAST_NAME);
                temp_eligibility.setPatientSSN(PATIENT_SSN);
                temp_eligibility.setPatientState(PATIENT_STATE);
                String temp_work_phone = PATIENT_WORK_PHONE;
                if (!PATIENT_WORK_PHONE_EXT.equalsIgnoreCase("")){
                    temp_work_phone+= " ext. " + PATIENT_WORK_PHONE_EXT;
                }
                temp_eligibility.setPatientWorkPhone(temp_work_phone);
                temp_eligibility.setPatientZip(PATIENT_ZIP_CODE);

                temp_eligibility.setAdjusterEmail(ADJUSTER_EMAIL);
                temp_eligibility.setAdjusterFirstName(ADJUSTER_FIRST_NAME);
                temp_eligibility.setAdjusterLastName(ADJUSTER_LAST_NAME);
                temp_eligibility.setAdjusterReferenceID(ADJUSTER_ID);
                String temp_adj_phone = ADJUSTER_PHONE;
                if (!ADJUSTER_PHONE_EXT.equalsIgnoreCase("")){
                    temp_adj_phone+= " ext. " + ADJUSTER_PHONE_EXT;
                }
                temp_eligibility.setAdjusterPhoneNumber(temp_adj_phone);
                temp_eligibility.setAdjusterAddress1(BRANCH_ADDRESS_LINE1);
                temp_eligibility.setAdjusterAddress2(BRANCH_ADDRESS_LINE2);
                temp_eligibility.setAdjusterCity(BRANCH_CITY);
                temp_eligibility.setAdjusterState(BRANCH_STATE);
                temp_eligibility.setAdjusterZip(BRANCH_ZIP_CODE);
                temp_eligibility.setAdjusterFaxNumber(BRANCH_FAX);

                temp_eligibility.setEmployerAddress1(EMPLOYER_ADDRESS_LINE1);
                temp_eligibility.setEmployerAddress2(EMPLOYER_ADDRESS_LINE2);
                temp_eligibility.setEmployerCity(EMPLOYER_CITY);
                temp_eligibility.setEmployerName(EMPLOYER_NAME);
                temp_eligibility.setEmployerState(EMPLOYER_STATE);
                temp_eligibility.setEmployerZip(EMPLOYER_ZIP_CODE);

                temp_eligibility.setNotes(
                        "Last Updated On: " + PLCUtils.getDisplayDateWithTime(PLCUtils.getNowDate(false))
                                + "\nFrom File: [" + in + "]"
                                + "\nMiddle Initial: " + PATIENT_MIDDLE_INITIAL
                                + "\nAWW: " + PATIENT_AWW
                                + "\nAccident Cause: " + ACCIDENT_CAUSE
                                + "\nAccident Desc 1: " + ACCIDENT_DESC1
                                + "\nAccident Desc 2: " + ACCIDENT_DESC2
                                + "\nOccupation: " + PATIENT_OCCUPATION
                                + "\nMisc: " + PATIENT_MISC
                );
                temp_eligibility.commitDataDisableAudit();
    //            temp_eligibility.commitData();
            } catch (Exception eFull) {
                System.out.println("Error L4(^^^^): Bad record.  This can happen of the EOF Count from Crawford is not correct which we have seen before and can ignore. Line = [["+line.trim()+"]]");
            }
        }
        bufferedReader.close();
        
        return count_total;
    }


//	/**
//	 *
//	 * Examine the tCaseAccount table for records that has a payerId for which
//	 * Amerisys is the parent payer.
//	 *
//	 * If you can find matching records there with records from the
//	 * tnim3_eligibilityreference table then update an empty SSN and/or
//	 * EmployerName field (in the tCaseAccount table) with the non-null value
//	 * from the tnim3_eligibilityreference table.
//	 *
//	 */
//	public void updateCaseAccountInfo() {
//
//		ArrayList<Integer> payerArr = new ArrayList<Integer>();
//
//		//
//		// Get the payers who have 'Amerisys' as its parent payer .
//		// These are the ids in the case account table that we are interested
//		// in.
//		//
//		try {
//			java.sql.Connection cCon = java.sql.DriverManager.getConnection(
//					ConfigurationInformation.sURL,
//					ConfigurationInformation.sUser,
//					ConfigurationInformation.sPassword);
//
//			String stmt = "SELECT payerid FROM tnim3_payermaster WHERE parentpayerid = '"
//					+ crawfordPayerID + "'";
//			System.out.println("stmt= " + stmt);
//			PreparedStatement ps = cCon.prepareStatement(stmt);
//			ResultSet rs = ps.executeQuery();
//			while (rs.next()) {
//				payerArr.add(rs.getInt(1));
//			}
//			rs.close();
//			ps.close();
//			cCon.close();
//		} catch (Exception eeee) {
//			System.out.println("Table access Error: " + eeee.toString());
//		}
//
//		StringBuffer sb = new StringBuffer();
//		sb.append("(");
//		for (int i = 0; i < payerArr.size() - 1; i++) {
//			sb.append("'" + payerArr.get(i) + "',");
//		}
//		sb.append("'" + payerArr.get(payerArr.size() - 1) + "'");
//		sb.append(")");
//
//		//
//		// Get the records in the case account table with these child payer ids.
//		// Attempt to match based on first & last name, dob, & gender.
//		// with either blank SSN's and/or blank employer name. Join with the
//		// eligibility reference table.
//		//
//
//		String stmt1 = "SELECT er.patientssn, er.employername, ca.caseid FROM tnim3_caseaccount ca, tnim3_eligibilityreference er WHERE ca.payerid IN "
//				+ sb.toString() + " AND ";
//		String stmt2 = "ca.employername='' AND UPPER(TRIM(ca.patientfirstname)) = UPPER(TRIM(er.patientfirstname)) AND UPPER(TRIM(ca.patientlastname)) =  UPPER(TRIM(er.patientlastname)) AND ";
//		String stmt3 = "ca.patientdob = er.patientdob AND ca.patientgender = er.patientgender AND (";
//		String stmt4 = "ca.patientssn = 'bLBW9xM+GtW8ANm32hAL2Q==' OR ";
//		String stmt5 = "ca.employername ='')";
//
//		//
//		// Resolve SSN and employer name.
//		//
//		try {
//			java.sql.Connection cCon = java.sql.DriverManager.getConnection(
//					ConfigurationInformation.sURL,
//					ConfigurationInformation.sUser,
//					ConfigurationInformation.sPassword);
//
//			String ssnStmt = stmt1 + stmt2 + stmt3 + stmt4 + stmt5;
//			System.out.println("SSN statement = " + ssnStmt);
//			PreparedStatement ps = cCon.prepareStatement(ssnStmt);
//			ResultSet rs = ps.executeQuery();
//			while (rs.next()) {
//
//				// Update SSN field with value from tnim3_eligibilityreference,
//				// but you need to encode it.
//				int ssn = rs.getInt(1);
//				String employerName = rs.getString(2);
//				int caseId = rs.getInt(3);
//				System.out.println("\nSSN update value = " + ssn);
//				System.out.println("\nEmp Name update value = " + employerName);
//				System.out.println("CA caseid = " + caseId);
//
//				// Construct account table object and store ssn and employer
//				// name.
//				// Use current unique keys for accessing correct acount table
//				// record.
//				// We set both fields if either is blank. Not really optimal.
//				bltNIM3_CaseAccount ca = new bltNIM3_CaseAccount(caseId);
//				String caSSN = ca.getPatientSSN();
//				ca.setPatientSSN(ssn + "");
//				ca.setEmployerName(employerName);
//				ca.commitData();
//				System.out.println("Current CA ssn = " + caSSN);
//
//			}
//
//			rs.close();
//			ps.close();
//			cCon.close();
//		} catch (Exception eeee) {
//			System.out.println("Table access Error: " + eeee.toString());
//		}
//
//	}

    /**
     *
     * Used to clear out tnim3_eligibilityreference table between test runs.
     *
     */
    public void truncateEligibilityReferenceTable() {
        searchDB2 mySS = new searchDB2();
        StringBuffer sb = new StringBuffer();
        sb.append("DELETE FROM tnim3_eligibilityreference where hcoid = " + crawfordPayerID);
        String mySQL = sb.toString();
        java.sql.ResultSet myRS = mySS.executeStatement(mySQL);
    }

    private bltNIM3_EligibilityReference getOrCreateEligibilityReference(String claim_number, boolean isVerbose){
        //TODO: must put in a check to see if the claim already exists and then return that value
        String tag = "EDIT_Crawford.java:getOrCreateEligibilityReference(String claim_number)";
        Integer existingEligibilityReferenceID = null;
        searchDB2 mySS = new searchDB2();
        java.sql.ResultSet myRS = null;
        try
        {
            //myRS = mySS.executeStatement("SELECT * from tCPTLI where CPT = '" + sCPT.replace('\\','|') + "'");
//            String mySQL = "SELECT EligibilityReferenceID from tnim3_EligibilityReference";
            String mySQL = "SELECT * from tnim3_EligibilityReference where payerid=? and upper(CaseClaimNumber) = upper(?)";
            db_PreparedStatementObject myDPSO = new db_PreparedStatementObject(mySQL);
            myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.INT_TYPE,this.crawfordPayerID+""));
            myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,claim_number.trim()));
            myRS = mySS.executePreparedStatement(myDPSO);
            if (myRS!=null&&myRS.next())
            {
                existingEligibilityReferenceID = myRS.getInt("EligibilityReferenceID");
            }
            mySS.closeAll();
        }
        catch(Exception e)
        {
//			out.println("ResultsSet:"+e);
            System.out.println ( "********");
            System.out.println ( "Error searching for claim number ["+claim_number+"]:["+e+"]");
            System.out.println ( "********");
        } finally {
            mySS.closeAll();
        }
        if (existingEligibilityReferenceID!=null){
            //simply return the existing item, ready to go
            bltNIM3_EligibilityReference temp = new bltNIM3_EligibilityReference(existingEligibilityReferenceID);
            temp.setCaseClaimNumber(claim_number);
            temp.setUniqueCreateDate(PLCUtils.getNowDate(false));
            if (isVerbose){
                System.out.println("-----> Updating existing claim [ID=" + temp.getUniqueID() + "]: [" + claim_number + "]");
            }
            return temp;
        } else {
            bltNIM3_EligibilityReference temp = new bltNIM3_EligibilityReference();
            temp.setCaseClaimNumber(claim_number);
            temp.setPayerID(this.crawfordPayerID);
            temp.setUniqueCreateDate(PLCUtils.getNowDate(false));
            if (isVerbose){
                System.out.println("-----> Adding new claim: [" + claim_number + "]");
            }
            return temp;
        }
    }

//	public void tabFileReader(String inFile) throws Exception {
//
//		bltNIM3_EligibilityReference er = null;
//		BufferedReader bufferedReader = null;
//		bufferedReader = new BufferedReader(new FileReader(inFile));
//		String line = null;
//
//		// The variable 'addChangeFlag' signifies whether the record is a new
//		// record or an update.
//		// If 'A' (add) then the claim is new. If 'C' (change) it is an update
//		// to an existing claim.
//		String addChangeFlag = null;
//		String amerisysDatabaseID = null;
//
//		while ((line = bufferedReader.readLine()) != null) {
//
//			String dataval[] = line.split("\t");
//			addChangeFlag = dataval[0];
//
//			// We need to check for prior existence of records and use this
//			// logic as suggested by Amerisys (Yavette Griffith)
//			// in an email:
//			// "If I send you an A that already exists you can ignore it."
//			// "If I send you a C that does not exist then, yes treat it as an A."
//
//			searchDB2 mySS = new searchDB2();
//
//			// These next 3 fields compose a unique key for this record (as
//			// mandated by Amerisys).
//			String corrusClaimID = dataval[23]; // Corrus Claim Id - Corrus
//												// unique reference to Claim
//			String payerIndicator = dataval[38]; // Amerisys Payer Indicator
//			String dbID = dataval[41]; // Amerisys Database ID
//
//			StringBuffer sb = new StringBuffer();
//			sb.append("SELECT * FROM tnim3_eligibilityreference WHERE ");
//			sb.append("corrusclaimid = '" + corrusClaimID
//					+ "' AND databaseid = '" + dbID + "' AND ");
//			sb.append("payerindicator = '" + payerIndicator + "'");
//			String mySQL = sb.toString();
//
//			System.out.println("mySQL = " + mySQL);
//
//			System.out.println("corrusClaimID = " + corrusClaimID);
//			System.out.println("databaseid = " + dbID);
//			System.out.println("payerIndicator = " + payerIndicator);
//
//			java.sql.ResultSet myRS = mySS.executeStatement(mySQL);
//			int key = 0;
//			try {
//				// / record already there
//				if (myRS != null && myRS.next()) {
//					if (addChangeFlag.compareToIgnoreCase("a") == 0) {
//						System.out
//								.println("addChangeFlag.compareToIgnoreCase = a");
//						continue;
//					} else if (addChangeFlag.compareToIgnoreCase("c") == 0) {
//						// Record already exists.
//						key = myRS.getInt("eligibilityreferenceid");
//						er = new bltNIM3_EligibilityReference(key);
//					} else {
//						// Report an error if we get here.
//						System.out.println("*** FATAL ERROR ***");
//					}
//					// no record exists
//				} else {
//					// Add this record for the first time
//					System.out.println("Add this record for the first time");
//					er = new bltNIM3_EligibilityReference();
//				}
//				mySS.closeAll();
//			} catch (Exception e) {
//			} finally {
//				mySS.closeAll();
//			}
//
//			er.setPayerID(crawfordPayerID); // Currently this is hardcoded for
//											// Amerisys.
//			er.setPatientLastName(dataval[9]); // Claimant last name
//			er.setPatientFirstName(dataval[10]); // Claimant first name
//			er.setPatientAddress1(dataval[12]); // Claimant mailing address line
//												// 1
//			er.setPatientAddress2(dataval[13]); // Claimant address line 2
//			er.setPatientCity(dataval[14]); // Claimant City
//			er.setPatientState(dataval[15]); // Claimant State
//			er.setPatientZip(dataval[16]); // Claimant Zip
//			er.setClientPayerID(dataval[1]); // Corrus Payer Id - Unique Corrus
//												// Id to reference the
//												// Account/Payer of the claim
//
//			er.setEmployerName(dataval[32]);
//			er.setEmployerCounty(dataval[33]);
//			er.setControverted(dataval[39]); // Controverted.
//
//			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMdd");
//			if (dataval[24].compareTo("") == 0) {
//				Date tmp = simpleDateFormat.parse("00000000");
//				er.setDateOfInjury(tmp); // Date of Injury
//				System.out.println("Date of Injury set to 00000000");
//			} else {
//				Date doi = simpleDateFormat.parse(dataval[24]);
//				er.setDateOfInjury(doi); // Date of Injury
//			}
//
//			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
//			if (dataval[17].compareTo("") == 0) {
//				Date tmp = simpleDateFormat.parse("00000000");
//				er.setPatientDOB(tmp); // Claimant date of birth
//				System.out.println("DOB set to 00000000");
//			} else {
//				Date dob = dateFormat.parse(dataval[17]);
//				er.setPatientDOB(dob); // Claimant date of birth
//			}
//
//			er.setPatientSSN(dataval[18]); // Claimant social security name
//			er.setPatientCellPhone(dataval[19]); // Claimant phone number
//			er.setPatientGender(dataval[20]); // Claimant gender
//
//			er.setClientClaimNum(dataval[25]); // Client Claim Number
//			er.setAdjusterLastName(dataval[36]); // Last Name of the Adjuster
//													// assigned to the claim
//			er.setAdjusterFirstName(dataval[37]); // First Name of the Adjuster
//													// assigned to the claim
//
//			er.setCaseManagerFirstName(dataval[37]);
//			er.setCaseManagerLastName(dataval[36]);
//
//			// SimpleDateFormat mmiFormat = new SimpleDateFormat("yyyyMMdd");
//			// Date mmi = mmiFormat.parse(dataval[40]);
//			// er.setMMI(mmi);
//
//			er.setClaimstatus(dataval[29]); // Claim Status
//			er.setPrimaryDiagCode(dataval[26]); // Diagnostic codes.
//			er.setDiagCode2(dataval[27]);
//			er.setDiagCode3(dataval[28]);
//			//
//			// These next 3 fields compose a unique key for this record
//			//
//			er.setCorrusClaimId(dataval[23]); // Corrus Claim Id - Corrus unique
//												// reference to Claim
//			er.setPayerIndicator(dataval[38]); // Amerisys Payer Indicator
//			er.setDatabaseID(dataval[41]); // Amerisys Database ID
//
//			er.commitData();
//		}
//		bufferedReader.close();
//	}
//

//	/**
//	 *
//	 * Execute a bash command.
//	 *
//	 * @param cmd
//	 */
//	public void bashCommand(String cmd) {
//		try {
//			System.out.println("Executing " + cmd + " on remote host.");
//			Process proc = Runtime.getRuntime().exec(cmd);
//			BufferedReader read = new BufferedReader(new InputStreamReader(
//					proc.getInputStream()));
//			try {
//				proc.waitFor();
//			} catch (InterruptedException e) {
//				System.out.println(e.getMessage());
//			}
//			while (read.ready()) {
//				System.out.println(read.readLine());
//			}
//		} catch (IOException e) {
//			System.out.println(e.getMessage());
//		}
//	}

    static public void main(String args[]) {
        EDI_Crawford crawford = new EDI_Crawford();
        System.out.println("> NIM_EDI(Java): EDI Crawford/Broadspire");
        System.out.println("-> Using ParentPayerID="+crawford.crawfordPayerID);
        if (true) {
            try {
                if (args[0].equalsIgnoreCase("delete")) {
                    System.out.println("--> sFTP 'delete' command detected");
                    crawford.runCopy(true);
                } else if (args[0].equalsIgnoreCase("copy")) {
                    System.out.println("--> sFTP 'copy' command detected");
                    crawford.runCopy(false);
                } else if (args[0].equalsIgnoreCase("import")) {
                    String use_file = args[1];
                    System.out.println("--> NIM translation 'import' command detected");
                    System.out.println("----> using folder [" + crawford.getLocalDir() + "]");
                    System.out.println("----> using file [" + use_file + "]");
                    int c_total = crawford.loadFile(use_file);
                    System.out.println("----> Total Imported: " + c_total);

                }
            } catch (Exception e) {
                DebugLogger.e("EDI_Crawford:Thread Error",e.toString());
                System.out.println("Error in Thread: " + e);
            }
        }
    }

    public void runCopy(boolean delete) {
        System.out.println("Running Crawford/BS Backup of Eligibility");
        if (delete) {
            System.out.println("*** Delete Mode ***");
        } else {
            System.out.println("*** Copy Mode ***");
        }

        String remoteDir = "/home/sec013/Prod/Outbound/";
//		String host = "localhost";
//		String user = "sec013";
//		String password = "1234";
        String host = "secftp2.choosebroadspire.com";
        String user = "sec013";
        String password = "aJTurUhj";

        JSch jsch = new JSch();
        Session session = null;
        try {
            session = jsch.getSession(user, host, 22);
            session.setConfig("StrictHostKeyChecking", "no");
            session.setPassword(password);
            session.connect();

            Channel channel = session.openChannel("sftp");
            channel.connect();
            ChannelSftp sftpChannel = (ChannelSftp) channel;
            sftpChannel.cd(remoteDir);
            Vector<ChannelSftp.LsEntry> list = sftpChannel.ls("*.txt");
            for (ChannelSftp.LsEntry entry : list) {
                System.out.println("Copying: " + entry.getFilename());
                if (!delete) {
                    sftpChannel.get(entry.getFilename(),
                            localDir + entry.getFilename());
                } else {
                    sftpChannel.rm(entry.getFilename());
                }
            }
            sftpChannel.exit();
            session.disconnect();
        } catch (JSchException e) {
            e.printStackTrace(); // To change body of catch statement use File |
            // Settings | File Templates.
        } catch (SftpException e) {
            e.printStackTrace();
        }
        System.out.println("Done Crawford/BS Backup of Eligibility");
    }

    public String getLocalDir() {
        return localDir;
    }

    public void setLocalDir(String localDir) {
        this.localDir = localDir;
    }

    //	/**
//	 *
//	 * @param args
//	 *
//	 */
//	static public void main_former_main(String args[]) {
//
//		System.out.println("\nStarting EDI_Crawford -- ***");
//
////		EDI_Amerisys edi = new EDI_Amerisys();
////
////		edi.getRemoteOutBoundFiles();
////		edi.processFiles();
////
////		// Now go back and update some case account information.
////		edi.updateCaseAccountInfo();
//
//		System.out.println("\nFinished with EDI_Crawford.");
//
//	}

// Bash Script

//    echo "-------------------"
//    echo "Running EDI for Crawford/Broadspire"
//    echo "-------------------"
//    echo ""
//    echo "1) Download currents"
//            #$JAVA_HOME/bin/java -cp $CLASSPATH com.winstaff.EDI_Crawford copy
//
//    echo ""
//            #echo "2) Remove currents from sftp"
//            #$JAVA_HOME/bin/java -cp $CLASSPATH com.winstaff.EDI_Crawford delete
//
//    echo ""
//    echo "3) Running Import"
//            #files=(/var/lib/tomcat6/webapps/webapps/EDI/crawford/*.txt) newestSQL=${f[0]}
//for f in $(find /var/lib/tomcat6/webapps/webapps/EDI/crawford/ . -name "*.txt" ) ; do
//  echo "--> Importing File" $f
//  fname=$(basename $f)
//  echo "--->" $fname
//  $JAVA_HOME/bin/java -cp $CLASSPATH com.winstaff.EDI_Crawford import $fname
//  mv $fname crawford/processed/$fname
//done
//
//echo ""
//echo "---------------------"
//echo "Finished"
//echo "---------------------"
//
//exit 0



}