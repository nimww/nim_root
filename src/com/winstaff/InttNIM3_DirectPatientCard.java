

package com.winstaff;

import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public interface InttNIM3_DirectPatientCard extends dbTableInterface
{

    public void setDirectPatientCardID(Integer newValue);
    public Integer getDirectPatientCardID();
    public void setUniqueCreateDate(Date newValue);
    public Date getUniqueCreateDate();
    public void setUniqueModifyDate(Date newValue);
    public Date getUniqueModifyDate();
    public void setDirectReferralSourceID(Integer newValue);
    public Integer getDirectReferralSourceID();
    public void setUniqueModifyComments(String newValue);
    public String getUniqueModifyComments();
    public void setPatientFirstName(String newValue);
    public String getPatientFirstName();
    public void setPatientLastName(String newValue);
    public String getPatientLastName();
    public void setSignUpDate(Date newValue);
    public Date getSignUpDate();
    public void setSourceWebsite(String newValue);
    public String getSourceWebsite();
    public void setPatientMRN(String newValue);
    public String getPatientMRN();
}    // End of bltNIM3_DirectPatientCard class definition
