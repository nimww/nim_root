

package com.winstaff;

import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public class dbtAdminMaster extends Object implements InttAdminMaster
{

        db_NewBase    dbnbDB;

    public dbtAdminMaster()
    {
        dbnbDB = new db_NewBase( "tAdminMaster", "AdminID" );

    }    // End of default constructor

    public dbtAdminMaster( Integer iNewID )
    {
        dbnbDB = new db_NewBase( "tAdminMaster", "AdminID" );
        setUniqueID( iNewID );
    }    // End of Constructor knowing an ID

    public void commitData() throws SQLException
    {
            //no longer trapping/ignoring errors 2011-03-17
            dbnbDB.commitData();
    }    // End of commitData()

    public void commitDataForced() throws SQLException
    {
        dbnbDB.commitDataForced();
    }    // End of commitData()

    public Integer getUniqueID()
    {
        return dbnbDB.getUniqueID();
    }    // End of getUniqueID()

    public void setUniqueID( Integer iNewID )
    {
        dbnbDB.setUniqueID( iNewID );
    }    // End of setUniqueID()

    public void setModifyComments( String sNew )
    {
        dbnbDB.setModifyComments( sNew );
    }    // End of setUniqueID()


    public void setAdminID(Integer newValue)
    {
                dbnbDB.setFieldData( "AdminID", newValue.toString() );
    }

    public Integer getAdminID()
    {
        String           sValue = dbnbDB.getFieldData( "AdminID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setField(String fieldN, Object fieldV)
    {DebugLogger.println("setField Method not implemented yet in classAdminMaster!");}

    public String getFieldType(String fieldN)
    {return null;}


    public void setUniqueCreateDate(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "UniqueCreateDate", formatter.format( newValue ) );
    }

    public Date getUniqueCreateDate()
    {
        String           sValue = dbnbDB.getFieldData( "UniqueCreateDate" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setUniqueModifyDate(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "UniqueModifyDate", formatter.format( newValue ) );
    }

    public Date getUniqueModifyDate()
    {
        String           sValue = dbnbDB.getFieldData( "UniqueModifyDate" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setUniqueModifyComments(String newValue)
    {
                dbnbDB.setFieldData( "UniqueModifyComments", newValue.toString() );
    }

    public String getUniqueModifyComments()
    {
        String           sValue = dbnbDB.getFieldData( "UniqueModifyComments" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setName(String newValue)
    {
                dbnbDB.setFieldData( "Name", newValue.toString() );
    }

    public String getName()
    {
        String           sValue = dbnbDB.getFieldData( "Name" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setAddress1(String newValue)
    {
                dbnbDB.setFieldData( "Address1", newValue.toString() );
    }

    public String getAddress1()
    {
        String           sValue = dbnbDB.getFieldData( "Address1" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setAddress2(String newValue)
    {
                dbnbDB.setFieldData( "Address2", newValue.toString() );
    }

    public String getAddress2()
    {
        String           sValue = dbnbDB.getFieldData( "Address2" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setCity(String newValue)
    {
                dbnbDB.setFieldData( "City", newValue.toString() );
    }

    public String getCity()
    {
        String           sValue = dbnbDB.getFieldData( "City" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setStateID(Integer newValue)
    {
                dbnbDB.setFieldData( "StateID", newValue.toString() );
    }

    public Integer getStateID()
    {
        String           sValue = dbnbDB.getFieldData( "StateID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setProvince(String newValue)
    {
                dbnbDB.setFieldData( "Province", newValue.toString() );
    }

    public String getProvince()
    {
        String           sValue = dbnbDB.getFieldData( "Province" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setZIP(String newValue)
    {
                dbnbDB.setFieldData( "ZIP", newValue.toString() );
    }

    public String getZIP()
    {
        String           sValue = dbnbDB.getFieldData( "ZIP" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setCountryID(Integer newValue)
    {
                dbnbDB.setFieldData( "CountryID", newValue.toString() );
    }

    public Integer getCountryID()
    {
        String           sValue = dbnbDB.getFieldData( "CountryID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setPhone(String newValue)
    {
                dbnbDB.setFieldData( "Phone", newValue.toString() );
    }

    public String getPhone()
    {
        String           sValue = dbnbDB.getFieldData( "Phone" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setAlertEmail(String newValue)
    {
                dbnbDB.setFieldData( "AlertEmail", newValue.toString() );
    }

    public String getAlertEmail()
    {
        String           sValue = dbnbDB.getFieldData( "AlertEmail" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setAlertDays(Integer newValue)
    {
                dbnbDB.setFieldData( "AlertDays", newValue.toString() );
    }

    public Integer getAlertDays()
    {
        String           sValue = dbnbDB.getFieldData( "AlertDays" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setContactFirstName(String newValue)
    {
                dbnbDB.setFieldData( "ContactFirstName", newValue.toString() );
    }

    public String getContactFirstName()
    {
        String           sValue = dbnbDB.getFieldData( "ContactFirstName" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setContactLastName(String newValue)
    {
                dbnbDB.setFieldData( "ContactLastName", newValue.toString() );
    }

    public String getContactLastName()
    {
        String           sValue = dbnbDB.getFieldData( "ContactLastName" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setContactEmail(String newValue)
    {
                dbnbDB.setFieldData( "ContactEmail", newValue.toString() );
    }

    public String getContactEmail()
    {
        String           sValue = dbnbDB.getFieldData( "ContactEmail" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setContactAddress1(String newValue)
    {
                dbnbDB.setFieldData( "ContactAddress1", newValue.toString() );
    }

    public String getContactAddress1()
    {
        String           sValue = dbnbDB.getFieldData( "ContactAddress1" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setContactAddress2(String newValue)
    {
                dbnbDB.setFieldData( "ContactAddress2", newValue.toString() );
    }

    public String getContactAddress2()
    {
        String           sValue = dbnbDB.getFieldData( "ContactAddress2" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setContactCity(String newValue)
    {
                dbnbDB.setFieldData( "ContactCity", newValue.toString() );
    }

    public String getContactCity()
    {
        String           sValue = dbnbDB.getFieldData( "ContactCity" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setContactStateID(Integer newValue)
    {
                dbnbDB.setFieldData( "ContactStateID", newValue.toString() );
    }

    public Integer getContactStateID()
    {
        String           sValue = dbnbDB.getFieldData( "ContactStateID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setContactZIP(String newValue)
    {
                dbnbDB.setFieldData( "ContactZIP", newValue.toString() );
    }

    public String getContactZIP()
    {
        String           sValue = dbnbDB.getFieldData( "ContactZIP" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setContactPhone(String newValue)
    {
                dbnbDB.setFieldData( "ContactPhone", newValue.toString() );
    }

    public String getContactPhone()
    {
        String           sValue = dbnbDB.getFieldData( "ContactPhone" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setContactFax(String newValue)
    {
                dbnbDB.setFieldData( "ContactFax", newValue.toString() );
    }

    public String getContactFax()
    {
        String           sValue = dbnbDB.getFieldData( "ContactFax" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setLastImportDate(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "LastImportDate", formatter.format( newValue ) );
    }

    public Date getLastImportDate()
    {
        String           sValue = dbnbDB.getFieldData( "LastImportDate" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setDocuLinkID(Integer newValue)
    {
                dbnbDB.setFieldData( "DocuLinkID", newValue.toString() );
    }

    public Integer getDocuLinkID()
    {
        String           sValue = dbnbDB.getFieldData( "DocuLinkID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setComments(String newValue)
    {
                dbnbDB.setFieldData( "Comments", newValue.toString() );
    }

    public String getComments()
    {
        String           sValue = dbnbDB.getFieldData( "Comments" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setfax(String newValue)
    {
                dbnbDB.setFieldData( "fax", newValue.toString() );
    }

    public String getfax()
    {
        String           sValue = dbnbDB.getFieldData( "fax" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setAssignedToID(Integer newValue)
    {
                dbnbDB.setFieldData( "AssignedToID", newValue.toString() );
    }

    public Integer getAssignedToID()
    {
        String           sValue = dbnbDB.getFieldData( "AssignedToID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

}    // End of bltAdminMaster class definition
