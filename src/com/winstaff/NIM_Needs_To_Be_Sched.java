package com.winstaff;

import java.sql.SQLException;

public class NIM_Needs_To_Be_Sched{
	public static void email(String emailTo, String emailFrom, String theSubject, String theBody) throws SQLException{
		bltEmailTransaction et = new bltEmailTransaction();
		et.setEmailTo(emailTo);
		et.setEmailFrom(emailFrom);
		et.setEmailSubject(theSubject);
		et.setEmailBody(theBody);
		et.setEmailBodyType(emailType_V3.HTML_TYPE);
		et.setTransactionDate(new java.util.Date());
		et.commitData();
	}
	
	public static void main(String[]args) throws SQLException{
		String query = "SELECT * FROM \"all_active_needs_to_be_scheduled\"";
		String theBody = "";
		searchDB2 conn = new searchDB2();
		java.sql.ResultSet rs = conn.executeStatement(query);
		try{
			theBody += "<table border=2 style=\"font:Helvetica;border-collapse:collapse;\"><tr>";
			theBody += "<th align=\"left\" style=\"padding:2px 5px\">Scanpass</th>";
			theBody += "<th align=\"left\" style=\"padding:2px 5px\">Receive Date</th>";
			theBody += "<th align=\"left\" style=\"padding:2px 5px\">Scheduler Name</th>";
			theBody += "<th align=\"left\" style=\"padding:2px 5px\">Payer Name</th>";
			theBody += "<th align=\"left\" style=\"padding:2px 5px\">Adjuster Name</th>";
			theBody += "<th align=\"left\" style=\"padding:2px 5px\">Patient Name</th>";
			theBody += "<th align=\"left\" style=\"padding:2px 5px\">Patient State</th>";
			theBody += "<th align=\"left\" style=\"padding:2px 5px\">CPT Code</th>";
			theBody += "<th align=\"left\" style=\"padding:2px 5px\">Body Part</th>";
			
			
			
			theBody += "</tr>";
			
			while(rs != null && rs.next()){
				theBody += "<tr>";
					theBody += "<td align=\"left\" style=\"padding:2px 5px\"><small>" + rs.getString("scanpass") + "</small></td>";
					theBody += "<td align=\"left\" style=\"padding:2px 5px\"><small>" + rs.getString("uniquecreatedate") + "</small></td>";
					theBody += "<td align=\"left\" style=\"padding:2px 5px\"><small>" + rs.getString("sched_first") + " " + rs.getString("sched_last") +"</small></td>";
					theBody += "<td align=\"left\" style=\"padding:2px 5px\"><small>" + rs.getString("payername") + "</small></td>";
					theBody += "<td align=\"left\" style=\"padding:2px 5px\"><small>" + rs.getString("adjuster_first") + " " + rs.getString("adjuster_last") + "</small></td>";
					theBody += "<td align=\"left\" style=\"padding:2px 5px\"><small>" + rs.getString("patientfirstname") + " " + rs.getString("patientlastname") + "</small></td>";
					theBody += "<td align=\"left\" style=\"padding:2px 5px\"><small>" + rs.getString("patient_state") +  "</small></td>";
					theBody += "<td align=\"left\" style=\"padding:2px 5px\"><small>" + rs.getString("cpt_code") + "</small></td>";
					theBody += "<td align=\"left\" style=\"padding:2px 5px\"><small>" + rs.getString("body_part") +  "</small></td>";
					
					
					
				theBody += "</tr>";
			}
			theBody += "</table> \n";
		}catch(Exception e){theBody = e.toString();}
		conn.closeAll();
		String theSubject = "Needs To Be Scheduled";
		email("maryjane.demille@nextimagemedical.com","support@nextimagemedical.com",theSubject,theBody);
		//System.out.print(theBody);
	}
}