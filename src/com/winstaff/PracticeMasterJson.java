package com.winstaff;

public class PracticeMasterJson {

	private String practiceDistance;
	private String practiceCity;
	private int practiceId;
	private double macScore;
	private boolean badgeSuperHighField;
	private String sbadgeSuperHighField;
	private boolean badgeHighField;
	private String sbadgeHighField;
	private boolean badgeOpenMr;
	private String sbadgeOpenMr;
	private boolean badgeACR;
	private String sbadgeACR;
	private boolean badgeBestValue;
	private String sbadgeBestValue;
	private double priceUC;
	private double priceSaving;
	private int priceSavingPercent;
	private double priceCustomer;
	private int cptWizardId;
	private String zipCode;
	private String combinedBadge;
	public PracticeMasterJson(){};
	public PracticeMasterJson(String practiceDistance, String practiceCity, int practiceId, double macScore, boolean badgeSuperHighField, boolean badgeHighField, boolean badgeOpenMr, boolean badgeACR, boolean badgeBestValue, double priceUC, double priceSaving, int priceSavingPercent, double priceCustomer, int cptWizardId, String zipCode) {
		this.practiceDistance = practiceDistance;
		this.practiceCity = practiceCity;
		this.practiceId = practiceId;
		this.macScore = macScore;
		this.badgeSuperHighField = badgeSuperHighField;
		this.badgeHighField = badgeHighField;
		this.badgeOpenMr = badgeOpenMr;
		this.badgeACR = badgeACR;
		this.badgeBestValue = badgeBestValue;
		this.priceUC = priceUC;
		this.priceSaving = priceSaving;
		this.priceSavingPercent = priceSavingPercent;
		this.priceCustomer = priceCustomer;
		this.cptWizardId = cptWizardId;
		this.zipCode = zipCode;
		setSbadges(badgeSuperHighField, badgeHighField, badgeOpenMr, badgeACR, badgeBestValue);
	}
	public void setSbadges(boolean badgeSuperHighField, boolean badgeHighField, boolean badgeOpenMr, boolean badgeACR, boolean badgeBestValue){
		String temp ="";
		if (badgeSuperHighField){
			this.sbadgeSuperHighField = "Super High Field";
			temp +="Super High Field<br>";
		} else this.sbadgeSuperHighField = "";
		if (badgeHighField){
			this.sbadgeHighField = "High Field";
			temp +="High Field<br>";
		} else this.sbadgeHighField = "";
		if (badgeOpenMr){
			this.sbadgeOpenMr = "Open Mr";
			temp +="Open Mr<br>";
		} else this.sbadgeOpenMr = "";
		if (badgeACR){
			this.sbadgeACR = "ACR";
			temp +="ACR<br>";
		} else this.sbadgeACR = "";
		if (badgeBestValue){
			this.sbadgeBestValue = "Best Value";
			temp +="Best Value<br>";
		} else this.sbadgeBestValue = "";
		
		this.combinedBadge = temp;
	}
	public String getPracticeDistance() {
		return practiceDistance;
	}
	public void setPracticeDistance(String practiceDistance) {
		this.practiceDistance = practiceDistance;
	}
	public String getPracticeCity() {
		return practiceCity;
	}
	public void setPracticeCity(String practiceCity) {
		this.practiceCity = practiceCity;
	}
	public int getPracticeId() {
		return practiceId;
	}
	public void setPracticeId(int practiceId) {
		this.practiceId = practiceId;
	}
	public double getMacScore() {
		return macScore;
	}
	public void setMacScore(double macScore) {
		this.macScore = macScore;
	}
	public boolean isBadgeSuperHighField() {
		return badgeSuperHighField;
	}
	public void setBadgeSuperHighField(boolean badgeSuperHighField) {
		this.badgeSuperHighField = badgeSuperHighField;
	}
	public boolean isBadgeHighField() {
		return badgeHighField;
	}
	public void setBadgeHighField(boolean badgeHighField) {
		this.badgeHighField = badgeHighField;
	}
	public boolean isBadgeOpenMr() {
		return badgeOpenMr;
	}
	public void setBadgeOpenMr(boolean badgeOpenMr) {
		this.badgeOpenMr = badgeOpenMr;
	}
	public boolean isBadgeACR() {
		return badgeACR;
	}
	public void setBadgeACR(boolean badgeACR) {
		this.badgeACR = badgeACR;
	}
	public boolean isBadgeBestValue() {
		return badgeBestValue;
	}
	public void setBadgeBestValue(boolean badgeBestValue) {
		this.badgeBestValue = badgeBestValue;
	}
	public double getPriceUC() {
		return priceUC;
	}
	public void setPriceUC(double priceUC) {
		this.priceUC = priceUC;
	}
	public double getPriceSaving() {
		return priceSaving;
	}
	public void setPriceSaving(double priceSaving) {
		this.priceSaving = priceSaving;
	}
	public int getPriceSavingPercent() {
		return priceSavingPercent;
	}
	public void setPriceSavingPercent(int priceSavingPercent) {
		this.priceSavingPercent = priceSavingPercent;
	}
	public double getPriceCustomer() {
		return priceCustomer;
	}
	public void setPriceCustomer(double priceCustomer) {
		this.priceCustomer = priceCustomer;
	}
	public int getCptWizardId() {
		return cptWizardId;
	}
	public void setCptWizardId(int cptWizardId) {
		this.cptWizardId = cptWizardId;
	}
	public String getZipCode() {
		return zipCode;
	}
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}
	
	
	
	

}
