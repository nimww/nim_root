package com.winstaff;
import java.util.Enumeration;
import java.util.Vector;


/** Default constructor
 *
 */
public class VectorEnumeration implements Enumeration
{
    int     iCurrentValue = 0;
    int     iMaxValue;
    Vector  vVector;
    
    public VectorEnumeration( Vector vVect )
    {
        iMaxValue = vVect.size();
        vVector = vVect;
    }
    
    public boolean hasMoreElements()
    {
        return ( iCurrentValue < iMaxValue );
    }
    
    public Object nextElement()
    {
        return vVector.elementAt( iCurrentValue++ );
    }
}
