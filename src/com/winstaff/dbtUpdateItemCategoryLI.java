

package com.winstaff;

import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public class dbtUpdateItemCategoryLI extends Object implements InttUpdateItemCategoryLI
{

        db_NewBase    dbnbDB;

    public dbtUpdateItemCategoryLI()
    {
        dbnbDB = new db_NewBase( "tUpdateItemCategoryLI", "UpdateItemCategoryID" );

    }    // End of default constructor

    public dbtUpdateItemCategoryLI( Integer iNewID )
    {
        dbnbDB = new db_NewBase( "tUpdateItemCategoryLI", "UpdateItemCategoryID" );
        setUniqueID( iNewID );
    }    // End of Constructor knowing an ID

    public void commitData() throws SQLException
    {
            //no longer trapping/ignoring errors 2011-03-17
            dbnbDB.commitData();
    }    // End of commitData()

    public void commitDataForced() throws SQLException
    {
        dbnbDB.commitDataForced();
    }    // End of commitData()

    public Integer getUniqueID()
    {
        return dbnbDB.getUniqueID();
    }    // End of getUniqueID()

    public void setUniqueID( Integer iNewID )
    {
        dbnbDB.setUniqueID( iNewID );
    }    // End of setUniqueID()

    public void setModifyComments( String sNew )
    {
        dbnbDB.setModifyComments( sNew );
    }    // End of setUniqueID()


    public void setUpdateItemCategoryID(Integer newValue)
    {
                dbnbDB.setFieldData( "UpdateItemCategoryID", newValue.toString() );
    }

    public Integer getUpdateItemCategoryID()
    {
        String           sValue = dbnbDB.getFieldData( "UpdateItemCategoryID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setField(String fieldN, Object fieldV)
    {DebugLogger.println("setField Method not implemented yet in class!");}

    public String getFieldType(String fieldN)
    {return null;}


    public void setUpdateItemCategoryText(String newValue)
    {
                dbnbDB.setFieldData( "UpdateItemCategoryText", newValue.toString() );
    }

    public String getUpdateItemCategoryText()
    {
        String           sValue = dbnbDB.getFieldData( "UpdateItemCategoryText" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

}    // End of bltUpdateItemCategoryLI class definition
