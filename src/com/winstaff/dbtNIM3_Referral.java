

package com.winstaff;

import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public class dbtNIM3_Referral extends Object implements InttNIM3_Referral
{

        db_NewBase    dbnbDB;

    public dbtNIM3_Referral()
    {
        dbnbDB = new db_NewBase( "tNIM3_Referral", "ReferralID" );

    }    // End of default constructor

    public dbtNIM3_Referral( Integer iNewID )
    {
        dbnbDB = new db_NewBase( "tNIM3_Referral", "ReferralID" );
        setUniqueID( iNewID );
    }    // End of Constructor knowing an ID

    public void commitData() throws SQLException
    {
            //no longer trapping/ignoring errors 2011-03-17
            dbnbDB.commitData();
    }    // End of commitData()

    public void commitDataForced() throws SQLException
    {
        dbnbDB.commitDataForced();
    }    // End of commitData()

    public Integer getUniqueID()
    {
        return dbnbDB.getUniqueID();
    }    // End of getUniqueID()

    public void setUniqueID( Integer iNewID )
    {
        dbnbDB.setUniqueID( iNewID );
    }    // End of setUniqueID()

    public void setModifyComments( String sNew )
    {
        dbnbDB.setModifyComments( sNew );
    }    // End of setUniqueID()


    public void setReferralID(Integer newValue)
    {
                dbnbDB.setFieldData( "ReferralID", newValue.toString() );
    }

    public Integer getReferralID()
    {
        String           sValue = dbnbDB.getFieldData( "ReferralID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setField(String fieldN, Object fieldV)
    {DebugLogger.println("setField Method not implemented yet in classNIM3_Referral!");}

    public String getFieldType(String fieldN)
    {return null;}


    public void setUniqueCreateDate(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "UniqueCreateDate", formatter.format( newValue ) );
    }

    public Date getUniqueCreateDate()
    {
        String           sValue = dbnbDB.getFieldData( "UniqueCreateDate" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setUniqueModifyDate(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "UniqueModifyDate", formatter.format( newValue ) );
    }

    public Date getUniqueModifyDate()
    {
        String           sValue = dbnbDB.getFieldData( "UniqueModifyDate" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setUniqueModifyComments(String newValue)
    {
                dbnbDB.setFieldData( "UniqueModifyComments", newValue.toString() );
    }

    public String getUniqueModifyComments()
    {
        String           sValue = dbnbDB.getFieldData( "UniqueModifyComments" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setCaseID(Integer newValue)
    {
                dbnbDB.setFieldData( "CaseID", newValue.toString() );
    }

    public Integer getCaseID()
    {
        String           sValue = dbnbDB.getFieldData( "CaseID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setReferralTypeID(Integer newValue)
    {
                dbnbDB.setFieldData( "ReferralTypeID", newValue.toString() );
    }

    public Integer getReferralTypeID()
    {
        String           sValue = dbnbDB.getFieldData( "ReferralTypeID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setReferralStatusID(Integer newValue)
    {
                dbnbDB.setFieldData( "ReferralStatusID", newValue.toString() );
    }

    public Integer getReferralStatusID()
    {
        String           sValue = dbnbDB.getFieldData( "ReferralStatusID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setAttendingPhysicianID(Integer newValue)
    {
                dbnbDB.setFieldData( "AttendingPhysicianID", newValue.toString() );
    }

    public Integer getAttendingPhysicianID()
    {
        String           sValue = dbnbDB.getFieldData( "AttendingPhysicianID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setReferringPhysicianID(Integer newValue)
    {
                dbnbDB.setFieldData( "ReferringPhysicianID", newValue.toString() );
    }

    public Integer getReferringPhysicianID()
    {
        String           sValue = dbnbDB.getFieldData( "ReferringPhysicianID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setPreAuthorizationConfirmation(String newValue)
    {
                dbnbDB.setFieldData( "PreAuthorizationConfirmation", newValue.toString() );
    }

    public String getPreAuthorizationConfirmation()
    {
        String           sValue = dbnbDB.getFieldData( "PreAuthorizationConfirmation" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setProviderPatientID(String newValue)
    {
                dbnbDB.setFieldData( "ProviderPatientID", newValue.toString() );
    }

    public String getProviderPatientID()
    {
        String           sValue = dbnbDB.getFieldData( "ProviderPatientID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setAuthorizationDate(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "AuthorizationDate", formatter.format( newValue ) );
    }

    public Date getAuthorizationDate()
    {
        String           sValue = dbnbDB.getFieldData( "AuthorizationDate" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setReceiveDate(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "ReceiveDate", formatter.format( newValue ) );
    }

    public Date getReceiveDate()
    {
        String           sValue = dbnbDB.getFieldData( "ReceiveDate" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setReferralDate(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "ReferralDate", formatter.format( newValue ) );
    }

    public Date getReferralDate()
    {
        String           sValue = dbnbDB.getFieldData( "ReferralDate" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setNextActionDate(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "NextActionDate", formatter.format( newValue ) );
    }

    public Date getNextActionDate()
    {
        String           sValue = dbnbDB.getFieldData( "NextActionDate" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setNextActionTaskID(Integer newValue)
    {
                dbnbDB.setFieldData( "NextActionTaskID", newValue.toString() );
    }

    public Integer getNextActionTaskID()
    {
        String           sValue = dbnbDB.getFieldData( "NextActionTaskID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setPreparedBy(String newValue)
    {
                dbnbDB.setFieldData( "PreparedBy", newValue.toString() );
    }

    public String getPreparedBy()
    {
        String           sValue = dbnbDB.getFieldData( "PreparedBy" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setReferencePhone(String newValue)
    {
                dbnbDB.setFieldData( "ReferencePhone", newValue.toString() );
    }

    public String getReferencePhone()
    {
        String           sValue = dbnbDB.getFieldData( "ReferencePhone" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setReferenceFax(String newValue)
    {
                dbnbDB.setFieldData( "ReferenceFax", newValue.toString() );
    }

    public String getReferenceFax()
    {
        String           sValue = dbnbDB.getFieldData( "ReferenceFax" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setReferenceEmail(String newValue)
    {
                dbnbDB.setFieldData( "ReferenceEmail", newValue.toString() );
    }

    public String getReferenceEmail()
    {
        String           sValue = dbnbDB.getFieldData( "ReferenceEmail" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setURRecFileID(Integer newValue)
    {
                dbnbDB.setFieldData( "URRecFileID", newValue.toString() );
    }

    public Integer getURRecFileID()
    {
        String           sValue = dbnbDB.getFieldData( "URRecFileID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setOrderFileID(Integer newValue)
    {
                dbnbDB.setFieldData( "OrderFileID", newValue.toString() );
    }

    public Integer getOrderFileID()
    {
        String           sValue = dbnbDB.getFieldData( "OrderFileID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setRxFileID(Integer newValue)
    {
                dbnbDB.setFieldData( "RxFileID", newValue.toString() );
    }

    public Integer getRxFileID()
    {
        String           sValue = dbnbDB.getFieldData( "RxFileID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setComments(String newValue)
    {
                dbnbDB.setFieldData( "Comments", newValue.toString() );
    }

    public String getComments()
    {
        String           sValue = dbnbDB.getFieldData( "Comments" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setAuditNotes(String newValue)
    {
                dbnbDB.setFieldData( "AuditNotes", newValue.toString() );
    }

    public String getAuditNotes()
    {
        String           sValue = dbnbDB.getFieldData( "AuditNotes" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setReferralMethod(String newValue)
    {
                dbnbDB.setFieldData( "ReferralMethod", newValue.toString() );
    }

    public String getReferralMethod()
    {
        String           sValue = dbnbDB.getFieldData( "ReferralMethod" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setReferredByContactID(Integer newValue)
    {
                dbnbDB.setFieldData( "ReferredByContactID", newValue.toString() );
    }

    public Integer getReferredByContactID()
    {
        String           sValue = dbnbDB.getFieldData( "ReferredByContactID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setReferralWebLink(String newValue)
    {
                dbnbDB.setFieldData( "ReferralWebLink", newValue.toString() );
    }

    public String getReferralWebLink()
    {
        String           sValue = dbnbDB.getFieldData( "ReferralWebLink" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setReferralNIDPromoID(Integer newValue)
    {
                dbnbDB.setFieldData( "ReferralNIDPromoID", newValue.toString() );
    }

    public Integer getReferralNIDPromoID()
    {
        String           sValue = dbnbDB.getFieldData( "ReferralNIDPromoID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

}    // End of bltNIM3_Referral class definition
