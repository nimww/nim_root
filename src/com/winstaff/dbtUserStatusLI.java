

package com.winstaff;

import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public class dbtUserStatusLI extends Object implements InttUserStatusLI
{

        db_NewBase    dbnbDB;

    public dbtUserStatusLI()
    {
        dbnbDB = new db_NewBase( "tUserStatusLI", "UserStatusID" );

    }    // End of default constructor

    public dbtUserStatusLI( Integer iNewID )
    {
        dbnbDB = new db_NewBase( "tUserStatusLI", "UserStatusID" );
        setUniqueID( iNewID );
    }    // End of Constructor knowing an ID

    public void commitData() throws SQLException
    {
            //no longer trapping/ignoring errors 2011-03-17
            dbnbDB.commitData();
    }    // End of commitData()

    public void commitDataForced() throws SQLException
    {
        dbnbDB.commitDataForced();
    }    // End of commitData()

    public Integer getUniqueID()
    {
        return dbnbDB.getUniqueID();
    }    // End of getUniqueID()

    public void setUniqueID( Integer iNewID )
    {
        dbnbDB.setUniqueID( iNewID );
    }    // End of setUniqueID()

    public void setModifyComments( String sNew )
    {
        dbnbDB.setModifyComments( sNew );
    }    // End of setUniqueID()


    public void setUserStatusID(Integer newValue)
    {
                dbnbDB.setFieldData( "UserStatusID", newValue.toString() );
    }

    public Integer getUserStatusID()
    {
        String           sValue = dbnbDB.getFieldData( "UserStatusID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setField(String fieldN, Object fieldV)
    {DebugLogger.println("setField Method not implemented yet in class!");}

    public String getFieldType(String fieldN)
    {return null;}


    public void setUserStatusLong(String newValue)
    {
                dbnbDB.setFieldData( "UserStatusLong", newValue.toString() );
    }

    public String getUserStatusLong()
    {
        String           sValue = dbnbDB.getFieldData( "UserStatusLong" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

}    // End of bltUserStatusLI class definition
