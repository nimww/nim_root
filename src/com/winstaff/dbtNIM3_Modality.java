

package com.winstaff;

import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public class dbtNIM3_Modality extends Object implements InttNIM3_Modality
{

        db_NewBase    dbnbDB;

    public dbtNIM3_Modality()
    {
        dbnbDB = new db_NewBase( "tNIM3_Modality", "ModalityID" );

    }    // End of default constructor

    public dbtNIM3_Modality( Integer iNewID )
    {
        dbnbDB = new db_NewBase( "tNIM3_Modality", "ModalityID" );
        setUniqueID( iNewID );
    }    // End of Constructor knowing an ID

    public void commitData() throws SQLException
    {
            //no longer trapping/ignoring errors 2011-03-17
            dbnbDB.commitData();
    }    // End of commitData()

    public void commitDataForced() throws SQLException
    {
        dbnbDB.commitDataForced();
    }    // End of commitData()

    public Integer getUniqueID()
    {
        return dbnbDB.getUniqueID();
    }    // End of getUniqueID()

    public void setUniqueID( Integer iNewID )
    {
        dbnbDB.setUniqueID( iNewID );
    }    // End of setUniqueID()

    public void setModifyComments( String sNew )
    {
        dbnbDB.setModifyComments( sNew );
    }    // End of setUniqueID()


    public void setModalityID(Integer newValue)
    {
                dbnbDB.setFieldData( "ModalityID", newValue.toString() );
    }

    public Integer getModalityID()
    {
        String           sValue = dbnbDB.getFieldData( "ModalityID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setField(String fieldN, Object fieldV)
    {DebugLogger.println("setField Method not implemented yet in classNIM3_Modality!");}

    public String getFieldType(String fieldN)
    {return null;}


    public void setUniqueCreateDate(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "UniqueCreateDate", formatter.format( newValue ) );
    }

    public Date getUniqueCreateDate()
    {
        String           sValue = dbnbDB.getFieldData( "UniqueCreateDate" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setUniqueModifyDate(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "UniqueModifyDate", formatter.format( newValue ) );
    }

    public Date getUniqueModifyDate()
    {
        String           sValue = dbnbDB.getFieldData( "UniqueModifyDate" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setUniqueModifyComments(String newValue)
    {
                dbnbDB.setFieldData( "UniqueModifyComments", newValue.toString() );
    }

    public String getUniqueModifyComments()
    {
        String           sValue = dbnbDB.getFieldData( "UniqueModifyComments" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setPracticeID(Integer newValue)
    {
                dbnbDB.setFieldData( "PracticeID", newValue.toString() );
    }

    public Integer getPracticeID()
    {
        String           sValue = dbnbDB.getFieldData( "PracticeID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setModalityTypeID(Integer newValue)
    {
                dbnbDB.setFieldData( "ModalityTypeID", newValue.toString() );
    }

    public Integer getModalityTypeID()
    {
        String           sValue = dbnbDB.getFieldData( "ModalityTypeID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setModalityModelID(Integer newValue)
    {
                dbnbDB.setFieldData( "ModalityModelID", newValue.toString() );
    }

    public Integer getModalityModelID()
    {
        String           sValue = dbnbDB.getFieldData( "ModalityModelID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setSoftwareVersion(String newValue)
    {
                dbnbDB.setFieldData( "SoftwareVersion", newValue.toString() );
    }

    public String getSoftwareVersion()
    {
        String           sValue = dbnbDB.getFieldData( "SoftwareVersion" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setLastServiceDate(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "LastServiceDate", formatter.format( newValue ) );
    }

    public Date getLastServiceDate()
    {
        String           sValue = dbnbDB.getFieldData( "LastServiceDate" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setCoilWrist(Integer newValue)
    {
                dbnbDB.setFieldData( "CoilWrist", newValue.toString() );
    }

    public Integer getCoilWrist()
    {
        String           sValue = dbnbDB.getFieldData( "CoilWrist" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setCoilElbow(Integer newValue)
    {
                dbnbDB.setFieldData( "CoilElbow", newValue.toString() );
    }

    public Integer getCoilElbow()
    {
        String           sValue = dbnbDB.getFieldData( "CoilElbow" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setCoilKnee(Integer newValue)
    {
                dbnbDB.setFieldData( "CoilKnee", newValue.toString() );
    }

    public Integer getCoilKnee()
    {
        String           sValue = dbnbDB.getFieldData( "CoilKnee" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setCoilAnkle(Integer newValue)
    {
                dbnbDB.setFieldData( "CoilAnkle", newValue.toString() );
    }

    public Integer getCoilAnkle()
    {
        String           sValue = dbnbDB.getFieldData( "CoilAnkle" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setCoilShoulder(Integer newValue)
    {
                dbnbDB.setFieldData( "CoilShoulder", newValue.toString() );
    }

    public Integer getCoilShoulder()
    {
        String           sValue = dbnbDB.getFieldData( "CoilShoulder" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setCoilSpine(Integer newValue)
    {
                dbnbDB.setFieldData( "CoilSpine", newValue.toString() );
    }

    public Integer getCoilSpine()
    {
        String           sValue = dbnbDB.getFieldData( "CoilSpine" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setIsACR(Integer newValue)
    {
                dbnbDB.setFieldData( "IsACR", newValue.toString() );
    }

    public Integer getIsACR()
    {
        String           sValue = dbnbDB.getFieldData( "IsACR" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setACRExpiration(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "ACRExpiration", formatter.format( newValue ) );
    }

    public Date getACRExpiration()
    {
        String           sValue = dbnbDB.getFieldData( "ACRExpiration" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setIsACRVerified(Integer newValue)
    {
                dbnbDB.setFieldData( "IsACRVerified", newValue.toString() );
    }

    public Integer getIsACRVerified()
    {
        String           sValue = dbnbDB.getFieldData( "IsACRVerified" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setACRVerifiedDate(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "ACRVerifiedDate", formatter.format( newValue ) );
    }

    public Date getACRVerifiedDate()
    {
        String           sValue = dbnbDB.getFieldData( "ACRVerifiedDate" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setComments(String newValue)
    {
                dbnbDB.setFieldData( "Comments", newValue.toString() );
    }

    public String getComments()
    {
        String           sValue = dbnbDB.getFieldData( "Comments" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

}    // End of bltNIM3_Modality class definition
