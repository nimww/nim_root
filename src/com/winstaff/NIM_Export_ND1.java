package com.winstaff;

import java.io.DataOutputStream;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Vector;

/**
 * Created by IntelliJ IDEA.
 * User: ste
 * Date: 5/4/11
 * Time: 5:26 PM
 * To change this template use File | Settings | File Templates.
 */
public class NIM_Export_ND1
{

    static public void main(String args[])
    {
        NIM_Export_ND1 testMe = new NIM_Export_ND1();
        try
        {
            testMe.runMe_ND_Analysis();
        }
        catch (Exception e)
        {
            System.out.println("Error in Thread: "+ e );
        }
    }


    public static String getModality_Info(String myMod,Double d_price, String test_zip, String test_Code, double test_min,Integer test_nat_fsid)
    {
        String temp_string = "";
        try
        {
            //run modality: mr_wo
            temp_string+= myMod + "\t";
            temp_string+= "Good" + "\t";
            temp_string+= d_price + "\t";
            //fs
            Double temp_FeeSchedule = (NIMUtils.getFeeSchedulePrice(new Integer(1), new Double(1.00) ,test_zip,test_Code,"",PLCUtils.getToday()));
            boolean isFS_valid = false;
            if (temp_FeeSchedule>test_min)
            {
                temp_string+= temp_FeeSchedule + "\t";
                double myFS_Dif = temp_FeeSchedule - d_price;
                temp_string+= myFS_Dif + "\t";
                temp_string+= (temp_FeeSchedule - d_price)/temp_FeeSchedule + "\t";
                if (myFS_Dif>0)
                {
                    temp_string+= "Good" + "\t";
                }
                else if (myFS_Dif<0)
                {
                    temp_string+= "WARNING: Contract Over FS" + "\t";
                }
                else
                {
                    temp_string+= "WARNING: Contract Flat with FS" + "\t";
                }
                isFS_valid = true;
            }
            else
            {
                temp_string+= temp_FeeSchedule + "\t";
                double myFS_Dif = temp_FeeSchedule - d_price;
                temp_string+= myFS_Dif + "\t";
                temp_string+= (temp_FeeSchedule - d_price)/temp_FeeSchedule + "\t";
                temp_string+= "INVALID" + "\t";
                isFS_valid = false;
            }
            //nat pricing
            Double temp_Nat = (NIMUtils.getFeeSchedulePrice(test_nat_fsid, new Double(1.00) ,test_zip,test_Code,"",PLCUtils.getToday()));
            if (temp_Nat>test_min)
            {
                temp_string+= test_nat_fsid + "\t";
                temp_string+= temp_Nat + "\t";
                double myNat_Dif = temp_Nat - d_price;
                temp_string+= myNat_Dif + "\t";
                temp_string+= (temp_Nat - d_price)/temp_Nat + "\t";
                if (myNat_Dif>0)
                {
                    temp_string+= "Good" + "\t";
                }
                else if (myNat_Dif<0)
                {
                    temp_string+= "WARNING: Contract Over Nat" + "\t";
                }
                else
                {
                    temp_string+= "WARNING: Contract Flat with Nat" + "\t";
                }
                if (isFS_valid)
                {
                    if ( (temp_FeeSchedule - temp_Nat)<0 )
                    {
                        temp_string += "WARNING: Nat Over FS" + "\t";
                    }
                    else
                    {
                        temp_string += "Good" + "\t";
                    }

                }
                else
                {
                    temp_string += "INVALID" + "\t";
                }


            }
            else
            {
                temp_string+= test_nat_fsid + "\t";
                temp_string+= temp_Nat + "\t";
                double myNat_Dif = temp_Nat - d_price;
                temp_string+= myNat_Dif + "\t";
                temp_string+= (temp_Nat - d_price)/temp_Nat + "\t";
                temp_string+= "INVALID" + "\t";
                temp_string += "INVALID" + "\t";
            }
        }
        catch (Exception e)
        {
            System.out.println("Mod_info: ["+e+"]");
        }
        return temp_string;
    }



    public static void runMe_ND_Analysis()
    {
        String CS_Version = "ND 1.0";
        searchDB2 mySDBA = new searchDB2();
        try
        {
            System.out.println("==============================================================================");
            System.out.println("Running ND Export Version " + CS_Version);
            java.text.SimpleDateFormat displayDateSDF1Full = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1Full);
            System.out.println("Start: " + displayDateSDF1Full.format(PLCUtils.getNowDate(true)));

            String mySQLA = "SELECT\n" +
                    "\"public\".\"vMQ2_Practices\".practiceid,\n" +
                    "\"public\".\"vMQ2_Practices\".practicename,\n" +
                    "\"public\".\"vMQ2_Practices\".officeaddress1,\n" +
                    "\"public\".\"vMQ2_Practices\".officeaddress2,\n" +
                    "\"public\".\"vMQ2_Practices\".officecity,\n" +
                    "\"public\".\"vMQ2_Practices\".officestate,\n" +
                    "\"public\".\"vMQ2_Practices\".officezip,\n" +
                    "\"public\".\"vMQ2_Practices\".officecounty,\n" +
                    "\"public\".\"vMQ2_Practices\".officephone,\n" +
                    "\"public\".\"vMQ2_Practices\".officefaxno,\n" +
                    "\"public\".\"vMQ2_Practices\".officeemail,\n" +
                    "\"public\".\"vMQ2_Practices\".officefederaltaxid,\n" +
                    "\"public\".\"vMQ2_Practices\".officetaxidnameaffiliation,\n" +
                    "\"public\".\"vMQ2_Practices\".billingpayableto,\n" +
                    "\"public\".\"vMQ2_Practices\".billingaddress1,\n" +
                    "\"public\".\"vMQ2_Practices\".billingaddress2,\n" +
                    "\"public\".\"vMQ2_Practices\".billingcity,\n" +
                    "\"public\".\"vMQ2_Practices\".billingstate,\n" +
                    "\"public\".\"vMQ2_Practices\".billingzip,\n" +
                    "\"public\".\"vMQ2_Practices\".billingphone,\n" +
                    "\"public\".\"vMQ2_Practices\".billingfax,\n" +
                    "\"public\".\"vMQ2_Practices\".npinumber,\n" +
                    "\"public\".\"vMQ2_Practices\".statelicensenumber,\n" +
                    "\"public\".\"vMQ2_Practices\".medicarelicensenumber,\n" +
                    "\"public\".\"vMQ2_Practices\".adminid,\n" +
                    "\"public\".\"vMQ2_Practices\".admin_name,\n" +
                    "\"public\".\"vMQ2_Practices\".\"RelationshipType\",\n" +
                    "\"public\".\"vMQ2_Practices\".\"ContractStatus\",\n" +
                    "\"public\".\"vMQ2_Practices\".isbluestar,\n" +
                    "\"public\".\"vMQ2_Practices\".contractglobal,\n" +
                    "\"public\".\"vMQ2_Practices\".contracttechonly,\n" +
                    "\"public\".\"vMQ2_Practices\".contractprofessionalonly,\n" +
                    "\"public\".\"vMQ2_Practices\".price_mod_mri_wo,\n" +
                    "\"public\".\"vMQ2_Practices\".price_mod_mri_w,\n" +
                    "\"public\".\"vMQ2_Practices\".price_mod_mri_wwo,\n" +
                    "\"public\".\"vMQ2_Practices\".price_mod_ct_wo,\n" +
                    "\"public\".\"vMQ2_Practices\".price_mod_ct_w,\n" +
                    "\"public\".\"vMQ2_Practices\".price_mod_ct_wwo,\n" +
                    "\"public\".\"vMQ2_Practices\".feeschedulerefid,\n" +
                    "\"public\".\"vMQ2_Practices\".feepercentage,\n" +
                    "\"public\".\"vMQ2_Practices\".feescheduleoverrefid,\n" +
                    "\"public\".\"vMQ2_Practices\".feeoverpercentage,\n" +
                    "\"public\".\"vMQ2_Practices\".diagnosticmdarthrogram,\n" +
                    "\"public\".\"vMQ2_Practices\".diagnosticmdmyelogram,\n" +
                    "\"public\".\"vMQ2_Practices\".doesaging,\n" +
                    "\"public\".\"vMQ2_Practices\".hasmr,\n" +
                    "\"public\".\"vMQ2_Practices\".hasct,\n" +
                    "\"public\".\"vMQ2_Practices\".hasemg,\n" +
                    "\"public\".\"vMQ2_Practices\".hasus,\n" +
                    "\"public\".\"vMQ2_Practices\".hasfl,\n" +
                    "\"public\".\"vMQ2_Practices\".hasnm,\n" +
                    "\"public\".\"vMQ2_Practices\".hasxr,\n" +
                    "\"public\".\"vMQ2_Practices\".haspet\n" +
                    "FROM\n" +
                    "\"public\".\"vMQ2_Practices\"";
            java.sql.ResultSet myRSA = mySDBA.executeStatement(mySQLA);
            int totalCnt=0;
            int successCnt=0;
            java.util.Vector<String> myOut = new Vector<String>();
            String myHeader = ("" +
                    "data_status\t" +
                    "practiceid\t" +
                    "practicename\t" +
                    "officeaddress1\t" +
                    "officeaddress2\t" +
                    "officecity\t" +
                    "officestate\t" +
                    "officezip\t" +
                    "officezip5\t" +
                    "office_stateFS_RC\t" +
                    "officecounty\t" +
                    "officephone\t" +
                    "officefaxno\t" +
                    "officeemail\t" +
                    "officefederaltaxid\t" +
                    "officetaxidnameaffiliation\t" +
                    "billingpayableto\t" +
                    "billingaddress1\t" +
                    "billingaddress2\t" +
                    "billingcity\t" +
                    "billingstate\t" +
                    "billingzip\t" +
                    "billingphone\t" +
                    "billingfax\t" +
                    "npinumber\t" +
                    "statelicensenumber\t" +
                    "medicarelicensenumber\t" +
                    "adminid\t" +
                    "admin_name\t" +
                    "RelationshipType\t" +
                    "ContractStatus\t" +
                    "isbluestar\t" +
                    "contractglobal\t" +
                    "contracttechonly\t" +
                    "contractprofessionalonly\t" +
                    "feeschedulerefid\t" +
                    "feepercentage\t" +
                    "feescheduleoverrefid\t" +
                    "feeoverpercentage\t" +
                    "diagnosticmdarthrogram\t" +
                    "diagnosticmdmyelogram\t" +
                    "doesaging\t" +
                    "hasmr\t" +
                    "hasct\t" +
                    "hasemg\t" +
                    "hasus\t" +
                    "hasfl\t" +
                    "hasnm\t" +
                    "hasxr\t" +
                    "haspet\t" +
                    "modality\t" +
                    "contract_price_status\t" +
                    "contract_price\t" +
                    "state_fs_price\t" +
                    "state_fs_price_dif_$\t" +
                    "state_fs_price_dif_%\t" +
                    "state_fs_price_status\t" +
                    "compare_fs_fsid\t" +
                    "compare_fs_price\t" +
                    "compare_fs_price_dif_$\t" +
                    "compare_fs_price_dif_%\t" +
                    "compare_fs_price_status\t" +
                    "compare_fs_v_fs_price_status\t" +
                    "");
            System.out.println("");
            System.out.println("Writing File");

            SimpleDateFormat    formatter1              = new SimpleDateFormat( "MM_dd_yyyy_hh_mm" );
            SimpleDateFormat formatter2              = new SimpleDateFormat( "yyyy-MM-dd_HH:mm" );
            String              newDateFileNameString   = formatter1.format( PLCUtils.getNowDate(true) );
            String sFileName = "export/Export_ND1.txt";
//            String sFileName = "export/Export_ND1_" + newDateFileNameString + ".txt";
            DataOutputStream    out;
            out = new DataOutputStream( new FileOutputStream( sFileName ) );
            out.writeBytes( myHeader );
            out.writeBytes( "\n" );



            while (myRSA.next())
            {
                totalCnt++;
                String string_practiceid = myRSA.getString("practiceid");
                String string_practicename = myRSA.getString("practicename");
                String string_officeaddress1 = myRSA.getString("officeaddress1");
                String string_officeaddress2 = myRSA.getString("officeaddress2");
                String string_officecity = myRSA.getString("officecity");
                String string_officestate = myRSA.getString("officestate");
                String string_officezip = myRSA.getString("officezip");
                String string_officecounty = myRSA.getString("officecounty");
                String string_officephone = myRSA.getString("officephone");
                String string_officefaxno = myRSA.getString("officefaxno");
                String string_officeemail = myRSA.getString("officeemail");
                String string_officefederaltaxid = myRSA.getString("officefederaltaxid");
                String string_officetaxidnameaffiliation = myRSA.getString("officetaxidnameaffiliation");
                String string_billingpayableto = myRSA.getString("billingpayableto");
                String string_billingaddress1 = myRSA.getString("billingaddress1");
                String string_billingaddress2 = myRSA.getString("billingaddress2");
                String string_billingcity = myRSA.getString("billingcity");
                String string_billingstate = myRSA.getString("billingstate");
                String string_billingzip = myRSA.getString("billingzip");
                String string_billingphone = myRSA.getString("billingphone");
                String string_billingfax = myRSA.getString("billingfax");
                String string_npinumber = myRSA.getString("npinumber");
                String string_statelicensenumber = myRSA.getString("statelicensenumber");
                String string_medicarelicensenumber = myRSA.getString("medicarelicensenumber");
                String string_adminid = myRSA.getString("adminid");
                String string_admin_name = myRSA.getString("admin_name");
                String string_RelationshipType = myRSA.getString("RelationshipType");
                String string_ContractStatus = myRSA.getString("ContractStatus");
                String string_isbluestar = myRSA.getString("isbluestar");
                String string_contractglobal = myRSA.getString("contractglobal");
                String string_contracttechonly = myRSA.getString("contracttechonly");
                String string_contractprofessionalonly = myRSA.getString("contractprofessionalonly");
                String string_feeschedulerefid = myRSA.getString("feeschedulerefid");
                String string_feepercentage = myRSA.getString("feepercentage");
                String string_feescheduleoverrefid = myRSA.getString("feescheduleoverrefid");
                String string_feeoverpercentage = myRSA.getString("feeoverpercentage");
                String string_diagnosticmdarthrogram = myRSA.getString("diagnosticmdarthrogram");
                String string_diagnosticmdmyelogram = myRSA.getString("diagnosticmdmyelogram");
                String string_doesaging = myRSA.getString("doesaging");
                String string_hasmr = myRSA.getString("hasmr");
                String string_hasct = myRSA.getString("hasct");
                String string_hasemg = myRSA.getString("hasemg");
                String string_hasus = myRSA.getString("hasus");
                String string_hasfl = myRSA.getString("hasfl");
                String string_hasnm = myRSA.getString("hasnm");
                String string_hasxr = myRSA.getString("hasxr");
                String string_haspet = myRSA.getString("haspet");
                String string_price_mod_mri_wo = myRSA.getString("price_mod_mri_wo");
                String string_price_mod_mri_w = myRSA.getString("price_mod_mri_w");
                String string_price_mod_mri_wwo = myRSA.getString("price_mod_mri_wwo");
                String string_price_mod_ct_wo = myRSA.getString("price_mod_ct_wo");
                String string_price_mod_ct_w = myRSA.getString("price_mod_ct_w");
                String string_price_mod_ct_wwo = myRSA.getString("price_mod_ct_wwo");
                Double d_price_mod_mri_wo = null;
                Double d_price_mod_mri_w = null;
                Double d_price_mod_mri_wwo = null;
                Double d_price_mod_ct_wo = null;
                Double d_price_mod_ct_w = null;
                Double d_price_mod_ct_wwo = null;
                try
                {
                    d_price_mod_mri_wo = new Double(string_price_mod_mri_wo);
                    d_price_mod_mri_w = new Double(string_price_mod_mri_w);
                    d_price_mod_mri_wwo = new Double(string_price_mod_mri_wwo);
                    d_price_mod_ct_wo = new Double(string_price_mod_ct_wo);
                    d_price_mod_ct_w = new Double(string_price_mod_ct_w);
                    d_price_mod_ct_wwo = new Double(string_price_mod_ct_wwo);
                }
                catch (NumberFormatException nfe)
                {
                    System.out.println("Invalid Number ["+nfe+"]");
                }
                String string_officezip5 = "";
                String string_practice_status = "INVALID";
                String string_RC = "";
                if (string_officezip.length()>=5)
                {
                    string_officezip5 = string_officezip.trim().substring(0,5);
                    string_RC = NIMUtils.getRegionCode(1,string_officezip5,PLCUtils.getToday());
                    string_practice_status = "Good";
                }
                else
                {
                    string_practice_status = "INVALID: Bad Zip";
                }

                String temp_string = "" +
                    string_practice_status + "\t" +
                    string_practiceid + "\t" +
                    string_practicename + "\t" +
                    string_officeaddress1 + "\t" +
                    string_officeaddress2 + "\t" +
                    string_officecity + "\t" +
                    string_officestate + "\t" +
                    string_officezip + "\t" +
                    string_officezip5 + "\t" +
                    string_RC + "\t" +
                    string_officecounty + "\t" +
                    string_officephone + "\t" +
                    string_officefaxno + "\t" +
                    string_officeemail + "\t" +
                    string_officefederaltaxid + "\t" +
                    string_officetaxidnameaffiliation + "\t" +
                    string_billingpayableto + "\t" +
                    string_billingaddress1 + "\t" +
                    string_billingaddress2 + "\t" +
                    string_billingcity + "\t" +
                    string_billingstate + "\t" +
                    string_billingzip + "\t" +
                    string_billingphone + "\t" +
                    string_billingfax + "\t" +
                    string_npinumber + "\t" +
                    string_statelicensenumber + "\t" +
                    string_medicarelicensenumber + "\t" +
                    string_adminid + "\t" +
                    string_admin_name + "\t" +
                    string_RelationshipType + "\t" +
                    string_ContractStatus + "\t" +
                    string_isbluestar + "\t" +
                    string_contractglobal + "\t" +
                    string_contracttechonly + "\t" +
                    string_contractprofessionalonly + "\t" +
                    string_feeschedulerefid + "\t" +
                    string_feepercentage + "\t" +
                    string_feescheduleoverrefid + "\t" +
                    string_feeoverpercentage + "\t" +
                    string_diagnosticmdarthrogram + "\t" +
                    string_diagnosticmdmyelogram + "\t" +
                    string_doesaging + "\t" +
                    string_hasmr + "\t" +
                    string_hasct + "\t" +
                    string_hasemg + "\t" +
                    string_hasus + "\t" +
                    string_hasfl + "\t" +
                    string_hasnm + "\t" +
                    string_hasxr + "\t" +
                    string_haspet + "\t";
                double test_min = 100.00;
                String testCode_mr_wo = "73221";
                String testCode_mr_w = "72149";
                String testCode_mr_wwo = "72158";
                String testCode_ct_wo = "73200";
                String testCode_ct_w = "72132";
                String testCode_ct_wwo = "74170"; //"74150";
                String test_zip = string_officezip;
                boolean has_some_modality = false;
                String temp_mod_string = "";
                Integer test_nat_fsid = 140;

                //MRIs
                if (string_hasmr!=null&&!string_hasmr.equalsIgnoreCase("")
                        && d_price_mod_mri_wo!=null&&d_price_mod_mri_wo>test_min
                        && d_price_mod_mri_w!=null&&d_price_mod_mri_w>test_min
                        && d_price_mod_mri_wwo!=null&&d_price_mod_mri_wwo>test_min
                        )
                {

                    for (int temp_fsid=142; temp_fsid<=142;temp_fsid++)
                    {
                        temp_mod_string = getModality_Info("mr_wo",d_price_mod_mri_wo,test_zip,testCode_mr_wo, test_min,temp_fsid);
                        out.writeBytes( temp_string + temp_mod_string + "\n" );
                        temp_mod_string = getModality_Info("mr_w",d_price_mod_mri_w,test_zip,testCode_mr_w, test_min,temp_fsid);
                        out.writeBytes( temp_string + temp_mod_string + "\n" );
                        temp_mod_string = getModality_Info("mr_wwo",d_price_mod_mri_wwo,test_zip,testCode_mr_wwo, test_min,temp_fsid);
                        out.writeBytes( temp_string + temp_mod_string + "\n" );
                    }
                    has_some_modality = true;
                }

                //CTs
                if (string_hasct!=null&&!string_hasct.equalsIgnoreCase("")
                        && d_price_mod_ct_wo!=null&&d_price_mod_ct_wo>test_min
                        && d_price_mod_ct_w!=null&&d_price_mod_ct_w>test_min
                        && d_price_mod_ct_wwo!=null&&d_price_mod_ct_wwo>test_min
                        )
                {
                    for (int temp_fsid=142; temp_fsid<=142;temp_fsid++)
                    {
                        temp_mod_string = getModality_Info("ct_wo",d_price_mod_ct_wo,test_zip,testCode_ct_wo, test_min,temp_fsid);
                        out.writeBytes( temp_string + temp_mod_string + "\n" );
                        temp_mod_string = getModality_Info("ct_w",d_price_mod_ct_w,test_zip,testCode_ct_w, test_min,temp_fsid);
                        out.writeBytes( temp_string + temp_mod_string + "\n" );
                        temp_mod_string = getModality_Info("ct_wwo",d_price_mod_ct_wwo,test_zip,testCode_ct_wwo, test_min,temp_fsid);
                        out.writeBytes( temp_string + temp_mod_string + "\n" );
                    }
                    has_some_modality = true;
                }

                if (!has_some_modality)
                {
                    temp_string+= "EMPTY" + "\t";
                    temp_string+= "EMPTY" + "\t";
                    temp_string+= "EMPTY" + "\t";
                    temp_string+= "EMPTY" + "\t";
                    temp_string+= "EMPTY" + "\t";
                    temp_string+= "EMPTY" + "\t";
                    temp_string+= "EMPTY" + "\t";
                    temp_string+= "EMPTY" + "\t";
                    temp_string+= "EMPTY" + "\t";
                    temp_string+= "EMPTY" + "\t";
                    temp_string+= "EMPTY" + "\t";
                    temp_string+= "EMPTY" + "\t";
                }

            }
            mySDBA.closeAll();
            out.flush();
            System.out.println("");
            System.out.println("Finish: " + displayDateSDF1Full.format(PLCUtils.getNowDate(true)));
            System.out.println("==============================================================================");
        }
        catch(Exception e)
        {
            System.out.println("General Error: " + e);
        } finally {
            mySDBA.closeAll();

        }

    }




}

