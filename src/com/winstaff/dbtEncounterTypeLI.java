

package com.winstaff;

import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public class dbtEncounterTypeLI extends Object implements InttEncounterTypeLI
{

        db_NewBase    dbnbDB;

    public dbtEncounterTypeLI()
    {
        dbnbDB = new db_NewBase( "tEncounterTypeLI", "EncounterTypeID" );

    }    // End of default constructor

    public dbtEncounterTypeLI( Integer iNewID )
    {
        dbnbDB = new db_NewBase( "tEncounterTypeLI", "EncounterTypeID" );
        setUniqueID( iNewID );
    }    // End of Constructor knowing an ID

    public void commitData() throws SQLException
    {
            //no longer trapping/ignoring errors 2011-03-17
            dbnbDB.commitData();
    }    // End of commitData()

    public void commitDataForced() throws SQLException
    {
        dbnbDB.commitDataForced();
    }    // End of commitData()

    public Integer getUniqueID()
    {
        return dbnbDB.getUniqueID();
    }    // End of getUniqueID()

    public void setUniqueID( Integer iNewID )
    {
        dbnbDB.setUniqueID( iNewID );
    }    // End of setUniqueID()

    public void setModifyComments( String sNew )
    {
        dbnbDB.setModifyComments( sNew );
    }    // End of setUniqueID()


    public void setEncounterTypeID(Integer newValue)
    {
                dbnbDB.setFieldData( "EncounterTypeID", newValue.toString() );
    }

    public Integer getEncounterTypeID()
    {
        String           sValue = dbnbDB.getFieldData( "EncounterTypeID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setField(String fieldN, Object fieldV)
    {DebugLogger.println("setField Method not implemented yet in class!");}

    public String getFieldType(String fieldN)
    {return null;}


    public void setEncounterTypeShort(String newValue)
    {
                dbnbDB.setFieldData( "EncounterTypeShort", newValue.toString() );
    }

    public String getEncounterTypeShort()
    {
        String           sValue = dbnbDB.getFieldData( "EncounterTypeShort" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setEncounterTypeLong(String newValue)
    {
                dbnbDB.setFieldData( "EncounterTypeLong", newValue.toString() );
    }

    public String getEncounterTypeLong()
    {
        String           sValue = dbnbDB.getFieldData( "EncounterTypeLong" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

}    // End of bltEncounterTypeLI class definition
