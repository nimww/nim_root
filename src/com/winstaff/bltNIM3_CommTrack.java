

package com.winstaff;

import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;
import java.util.Enumeration;

public class bltNIM3_CommTrack extends Object implements InttNIM3_CommTrack
{

    dbtNIM3_CommTrack    dbDB;

    public bltNIM3_CommTrack()
    {
        dbDB = new dbtNIM3_CommTrack();
        dbDB.setUniqueCreateDate(new java.util.Date());
        makeRequiredHash();
        makeExpiredHash();
        makeEnglishHash();

    }    // End of default constructor

    public bltNIM3_CommTrack( Integer iNewID )
    {        dbDB = new dbtNIM3_CommTrack( iNewID );
        makeRequiredHash();
        makeExpiredHash();
        makeEnglishHash();
        iSecurityCheck = new Integer(2);
    }    // End of Constructor knowing an ID


    public bltNIM3_CommTrack( Integer iNewID, Integer iGroupSecurityID )
    {
        this.GroupSecurityInit(iGroupSecurityID);
        dbDB = new dbtNIM3_CommTrack( iNewID );
        makeExpiredHash();
        makeEnglishHash();
        makeRequiredHash(iGroupSecurityID);
    }    // End of Constructor knowing an ID

    public bltNIM3_CommTrack( Integer iGroupSecurityID, boolean isTrue )
    {
        this.GroupSecurityInit(iGroupSecurityID);
        dbDB = new dbtNIM3_CommTrack(  );
        makeExpiredHash();
        makeEnglishHash();
        makeRequiredHash(iGroupSecurityID);
    }    // End of Security unknown ID

    public void GroupSecurityInit( Integer iGroupSecurityID )
    {
        bltSecurityGroupMaster mySGM = new bltSecurityGroupMaster(iGroupSecurityID);
        this.UserSecurityID = iGroupSecurityID;
        if (mySGM.getRunFieldSecurityID().intValue()==1)
        {
            this.runFieldSecurity = true;
            makeRequiredHash(iGroupSecurityID);
        }
        iSecurityCheck = SecurityCheck.CheckItem("nim1", iGroupSecurityID);
    }  

    public EventReferenceType getEventReference()
    {
        return EventUtils.getEventReference("tNIM3_CommTrack", this.getUniqueID());
    }  

    public String convertString2LogString(String normalString) 
    {
        String myVal = "";
        for (int i=0;i<normalString.length();i++)
        {
            if ( (normalString.charAt(i)!='\r')&&(normalString.charAt(i)!='\n')&&(normalString.charAt(i)!='\t') )
            {
                myVal+=normalString.charAt(i);
            }
            else
            {
                myVal+="<NLT>";
            }
        }
        return myVal;
    }
    public void runAudit()
    {
        String AuditString = new java.util.Date()+" \t " ;  
        AuditString += " tNIM3_CommTrack "; 
        AuditString += " CommTrackID ="+this.getUniqueID(); 
        AuditString += " CaseID ="+this.getCaseID(); 
        for (int i=0;i<this.AuditVector.size();i++)
        {
            AuditString+="\t"+convertString2LogString((String)this.AuditVector.elementAt(i));
        }
        AuditLogger.printLine(AuditString);
    }
    public void commitData() throws SQLException
    {
        runAudit();
        dbDB.setModifyComments(this.getUniqueModifyComments());
        dbDB.setUniqueModifyDate(new java.util.Date());
        dbDB.commitData();
        DataControlUtils.dataChanged("tNIM3_CommTrack",this.getCaseID() ,this.AuditVector.size(), "CaseID");
        this.AuditVector.removeAllElements();
    }    // End of commitData()

    public void commitDataForced() throws SQLException
    {
        dbDB.commitDataForced();
    }    // End of commitData()

    public Integer getUniqueID()
    {
        return dbDB.getUniqueID();
    }    // End of getUniqueID()

    public void setUniqueID( Integer iNewID )
    {
        dbDB.setUniqueID( iNewID );
    }    // End of setUniqueID()


    public void setCommTrackID(Integer newValue)
    {
        dbDB.setCommTrackID(newValue);
    }

    public Integer getCommTrackID()
    {
        return dbDB.getCommTrackID();
    }

    java.util.Vector AuditVector = new java.util.Vector();

    public void setUniqueCreateDate(Date newValue)
    {
        this.setUniqueCreateDate(newValue,this.UserSecurityID);


    }
    public Date getUniqueCreateDate()
    {
        return this.getUniqueCreateDate(this.UserSecurityID);
    }

    public void setUniqueCreateDate(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='UniqueCreateDate' AND TableName='tNIM3_CommTrack'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[UniqueCreateDate]=["+newValue+"]");
                   dbDB.setUniqueCreateDate(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[UniqueCreateDate]=["+newValue+"]");
           dbDB.setUniqueCreateDate(newValue);
         }
    }
    public Date getUniqueCreateDate(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='UniqueCreateDate' AND TableName='tNIM3_CommTrack'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getUniqueCreateDate();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getUniqueCreateDate();
         }
        return myVal;
    }

    public void setUniqueModifyDate(Date newValue)
    {
        this.setUniqueModifyDate(newValue,this.UserSecurityID);


    }
    public Date getUniqueModifyDate()
    {
        return this.getUniqueModifyDate(this.UserSecurityID);
    }

    public void setUniqueModifyDate(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='UniqueModifyDate' AND TableName='tNIM3_CommTrack'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[UniqueModifyDate]=["+newValue+"]");
                   dbDB.setUniqueModifyDate(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[UniqueModifyDate]=["+newValue+"]");
           dbDB.setUniqueModifyDate(newValue);
         }
    }
    public Date getUniqueModifyDate(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='UniqueModifyDate' AND TableName='tNIM3_CommTrack'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getUniqueModifyDate();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getUniqueModifyDate();
         }
        return myVal;
    }

    public void setUniqueModifyComments(String newValue)
    {
        this.setUniqueModifyComments(newValue,this.UserSecurityID);


    }
    public String getUniqueModifyComments()
    {
        return this.getUniqueModifyComments(this.UserSecurityID);
    }

    public void setUniqueModifyComments(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='UniqueModifyComments' AND TableName='tNIM3_CommTrack'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[UniqueModifyComments]=["+newValue+"]");
                   dbDB.setUniqueModifyComments(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[UniqueModifyComments]=["+newValue+"]");
           dbDB.setUniqueModifyComments(newValue);
         }
    }
    public String getUniqueModifyComments(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='UniqueModifyComments' AND TableName='tNIM3_CommTrack'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getUniqueModifyComments();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getUniqueModifyComments();
         }
        return myVal;
    }

    public void setCaseID(Integer newValue)
    {
        this.setCaseID(newValue,this.UserSecurityID);


    }
    public Integer getCaseID()
    {
        return this.getCaseID(this.UserSecurityID);
    }

    public void setCaseID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CaseID' AND TableName='tNIM3_CommTrack'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[CaseID]=["+newValue+"]");
                   dbDB.setCaseID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[CaseID]=["+newValue+"]");
           dbDB.setCaseID(newValue);
         }
    }
    public Integer getCaseID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CaseID' AND TableName='tNIM3_CommTrack'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getCaseID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getCaseID();
         }
        return myVal;
    }

    public void setEncounterID(Integer newValue)
    {
        this.setEncounterID(newValue,this.UserSecurityID);


    }
    public Integer getEncounterID()
    {
        return this.getEncounterID(this.UserSecurityID);
    }

    public void setEncounterID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='EncounterID' AND TableName='tNIM3_CommTrack'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[EncounterID]=["+newValue+"]");
                   dbDB.setEncounterID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[EncounterID]=["+newValue+"]");
           dbDB.setEncounterID(newValue);
         }
    }
    public Integer getEncounterID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='EncounterID' AND TableName='tNIM3_CommTrack'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getEncounterID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getEncounterID();
         }
        return myVal;
    }

    public void setReferralID(Integer newValue)
    {
        this.setReferralID(newValue,this.UserSecurityID);


    }
    public Integer getReferralID()
    {
        return this.getReferralID(this.UserSecurityID);
    }

    public void setReferralID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ReferralID' AND TableName='tNIM3_CommTrack'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[ReferralID]=["+newValue+"]");
                   dbDB.setReferralID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[ReferralID]=["+newValue+"]");
           dbDB.setReferralID(newValue);
         }
    }
    public Integer getReferralID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ReferralID' AND TableName='tNIM3_CommTrack'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getReferralID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getReferralID();
         }
        return myVal;
    }

    public void setServiceID(Integer newValue)
    {
        this.setServiceID(newValue,this.UserSecurityID);


    }
    public Integer getServiceID()
    {
        return this.getServiceID(this.UserSecurityID);
    }

    public void setServiceID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ServiceID' AND TableName='tNIM3_CommTrack'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[ServiceID]=["+newValue+"]");
                   dbDB.setServiceID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[ServiceID]=["+newValue+"]");
           dbDB.setServiceID(newValue);
         }
    }
    public Integer getServiceID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ServiceID' AND TableName='tNIM3_CommTrack'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getServiceID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getServiceID();
         }
        return myVal;
    }

    public void setIntUserID(Integer newValue)
    {
        this.setIntUserID(newValue,this.UserSecurityID);


    }
    public Integer getIntUserID()
    {
        return this.getIntUserID(this.UserSecurityID);
    }

    public void setIntUserID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='IntUserID' AND TableName='tNIM3_CommTrack'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[IntUserID]=["+newValue+"]");
                   dbDB.setIntUserID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[IntUserID]=["+newValue+"]");
           dbDB.setIntUserID(newValue);
         }
    }
    public Integer getIntUserID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='IntUserID' AND TableName='tNIM3_CommTrack'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getIntUserID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getIntUserID();
         }
        return myVal;
    }

    public void setExtUserID(Integer newValue)
    {
        this.setExtUserID(newValue,this.UserSecurityID);


    }
    public Integer getExtUserID()
    {
        return this.getExtUserID(this.UserSecurityID);
    }

    public void setExtUserID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ExtUserID' AND TableName='tNIM3_CommTrack'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[ExtUserID]=["+newValue+"]");
                   dbDB.setExtUserID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[ExtUserID]=["+newValue+"]");
           dbDB.setExtUserID(newValue);
         }
    }
    public Integer getExtUserID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ExtUserID' AND TableName='tNIM3_CommTrack'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getExtUserID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getExtUserID();
         }
        return myVal;
    }

    public void setCommTypeID(Integer newValue)
    {
        this.setCommTypeID(newValue,this.UserSecurityID);


    }
    public Integer getCommTypeID()
    {
        return this.getCommTypeID(this.UserSecurityID);
    }

    public void setCommTypeID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CommTypeID' AND TableName='tNIM3_CommTrack'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[CommTypeID]=["+newValue+"]");
                   dbDB.setCommTypeID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[CommTypeID]=["+newValue+"]");
           dbDB.setCommTypeID(newValue);
         }
    }
    public Integer getCommTypeID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CommTypeID' AND TableName='tNIM3_CommTrack'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getCommTypeID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getCommTypeID();
         }
        return myVal;
    }

    public void setCommStart(Date newValue)
    {
        this.setCommStart(newValue,this.UserSecurityID);


    }
    public Date getCommStart()
    {
        return this.getCommStart(this.UserSecurityID);
    }

    public void setCommStart(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CommStart' AND TableName='tNIM3_CommTrack'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[CommStart]=["+newValue+"]");
                   dbDB.setCommStart(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[CommStart]=["+newValue+"]");
           dbDB.setCommStart(newValue);
         }
    }
    public Date getCommStart(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CommStart' AND TableName='tNIM3_CommTrack'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getCommStart();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getCommStart();
         }
        return myVal;
    }

    public void setCommEnd(Date newValue)
    {
        this.setCommEnd(newValue,this.UserSecurityID);


    }
    public Date getCommEnd()
    {
        return this.getCommEnd(this.UserSecurityID);
    }

    public void setCommEnd(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CommEnd' AND TableName='tNIM3_CommTrack'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[CommEnd]=["+newValue+"]");
                   dbDB.setCommEnd(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[CommEnd]=["+newValue+"]");
           dbDB.setCommEnd(newValue);
         }
    }
    public Date getCommEnd(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CommEnd' AND TableName='tNIM3_CommTrack'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getCommEnd();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getCommEnd();
         }
        return myVal;
    }

    public void setMessageText(String newValue)
    {
        this.setMessageText(newValue,this.UserSecurityID);


    }
    public String getMessageText()
    {
        return this.getMessageText(this.UserSecurityID);
    }

    public void setMessageText(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='MessageText' AND TableName='tNIM3_CommTrack'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[MessageText]=["+newValue+"]");
                   dbDB.setMessageText(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[MessageText]=["+newValue+"]");
           dbDB.setMessageText(newValue);
         }
    }
    public String getMessageText(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='MessageText' AND TableName='tNIM3_CommTrack'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getMessageText();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getMessageText();
         }
        return myVal;
    }

    public void setMessageName(String newValue)
    {
        this.setMessageName(newValue,this.UserSecurityID);


    }
    public String getMessageName()
    {
        return this.getMessageName(this.UserSecurityID);
    }

    public void setMessageName(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='MessageName' AND TableName='tNIM3_CommTrack'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[MessageName]=["+newValue+"]");
                   dbDB.setMessageName(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[MessageName]=["+newValue+"]");
           dbDB.setMessageName(newValue);
         }
    }
    public String getMessageName(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='MessageName' AND TableName='tNIM3_CommTrack'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getMessageName();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getMessageName();
         }
        return myVal;
    }

    public void setMessageCompany(String newValue)
    {
        this.setMessageCompany(newValue,this.UserSecurityID);


    }
    public String getMessageCompany()
    {
        return this.getMessageCompany(this.UserSecurityID);
    }

    public void setMessageCompany(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='MessageCompany' AND TableName='tNIM3_CommTrack'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[MessageCompany]=["+newValue+"]");
                   dbDB.setMessageCompany(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[MessageCompany]=["+newValue+"]");
           dbDB.setMessageCompany(newValue);
         }
    }
    public String getMessageCompany(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='MessageCompany' AND TableName='tNIM3_CommTrack'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getMessageCompany();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getMessageCompany();
         }
        return myVal;
    }

    public void setMessageEmail(String newValue)
    {
        this.setMessageEmail(newValue,this.UserSecurityID);


    }
    public String getMessageEmail()
    {
        return this.getMessageEmail(this.UserSecurityID);
    }

    public void setMessageEmail(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='MessageEmail' AND TableName='tNIM3_CommTrack'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[MessageEmail]=["+newValue+"]");
                   dbDB.setMessageEmail(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[MessageEmail]=["+newValue+"]");
           dbDB.setMessageEmail(newValue);
         }
    }
    public String getMessageEmail(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='MessageEmail' AND TableName='tNIM3_CommTrack'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getMessageEmail();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getMessageEmail();
         }
        return myVal;
    }

    public void setMessagePhone(String newValue)
    {
        this.setMessagePhone(newValue,this.UserSecurityID);


    }
    public String getMessagePhone()
    {
        return this.getMessagePhone(this.UserSecurityID);
    }

    public void setMessagePhone(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='MessagePhone' AND TableName='tNIM3_CommTrack'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[MessagePhone]=["+newValue+"]");
                   dbDB.setMessagePhone(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[MessagePhone]=["+newValue+"]");
           dbDB.setMessagePhone(newValue);
         }
    }
    public String getMessagePhone(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='MessagePhone' AND TableName='tNIM3_CommTrack'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getMessagePhone();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getMessagePhone();
         }
        return myVal;
    }

    public void setMessageFax(String newValue)
    {
        this.setMessageFax(newValue,this.UserSecurityID);


    }
    public String getMessageFax()
    {
        return this.getMessageFax(this.UserSecurityID);
    }

    public void setMessageFax(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='MessageFax' AND TableName='tNIM3_CommTrack'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[MessageFax]=["+newValue+"]");
                   dbDB.setMessageFax(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[MessageFax]=["+newValue+"]");
           dbDB.setMessageFax(newValue);
         }
    }
    public String getMessageFax(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='MessageFax' AND TableName='tNIM3_CommTrack'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getMessageFax();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getMessageFax();
         }
        return myVal;
    }

    public void setAlertStatusCode(Integer newValue)
    {
        this.setAlertStatusCode(newValue,this.UserSecurityID);


    }
    public Integer getAlertStatusCode()
    {
        return this.getAlertStatusCode(this.UserSecurityID);
    }

    public void setAlertStatusCode(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AlertStatusCode' AND TableName='tNIM3_CommTrack'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[AlertStatusCode]=["+newValue+"]");
                   dbDB.setAlertStatusCode(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[AlertStatusCode]=["+newValue+"]");
           dbDB.setAlertStatusCode(newValue);
         }
    }
    public Integer getAlertStatusCode(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AlertStatusCode' AND TableName='tNIM3_CommTrack'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getAlertStatusCode();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getAlertStatusCode();
         }
        return myVal;
    }

    public void setComments(String newValue)
    {
        this.setComments(newValue,this.UserSecurityID);


    }
    public String getComments()
    {
        return this.getComments(this.UserSecurityID);
    }

    public void setComments(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Comments' AND TableName='tNIM3_CommTrack'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[Comments]=["+newValue+"]");
                   dbDB.setComments(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[Comments]=["+newValue+"]");
           dbDB.setComments(newValue);
         }
    }
    public String getComments(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Comments' AND TableName='tNIM3_CommTrack'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getComments();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getComments();
         }
        return myVal;
    }

    public void setCommReferenceID(Integer newValue)
    {
        this.setCommReferenceID(newValue,this.UserSecurityID);


    }
    public Integer getCommReferenceID()
    {
        return this.getCommReferenceID(this.UserSecurityID);
    }

    public void setCommReferenceID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CommReferenceID' AND TableName='tNIM3_CommTrack'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[CommReferenceID]=["+newValue+"]");
                   dbDB.setCommReferenceID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[CommReferenceID]=["+newValue+"]");
           dbDB.setCommReferenceID(newValue);
         }
    }
    public Integer getCommReferenceID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CommReferenceID' AND TableName='tNIM3_CommTrack'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getCommReferenceID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getCommReferenceID();
         }
        return myVal;
    }

    public void setLink_UserID(Integer newValue)
    {
        this.setLink_UserID(newValue,this.UserSecurityID);


    }
    public Integer getLink_UserID()
    {
        return this.getLink_UserID(this.UserSecurityID);
    }

    public void setLink_UserID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Link_UserID' AND TableName='tNIM3_CommTrack'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[Link_UserID]=["+newValue+"]");
                   dbDB.setLink_UserID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[Link_UserID]=["+newValue+"]");
           dbDB.setLink_UserID(newValue);
         }
    }
    public Integer getLink_UserID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Link_UserID' AND TableName='tNIM3_CommTrack'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getLink_UserID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getLink_UserID();
         }
        return myVal;
    }

    public void setMessageSubject(String newValue)
    {
        this.setMessageSubject(newValue,this.UserSecurityID);


    }
    public String getMessageSubject()
    {
        return this.getMessageSubject(this.UserSecurityID);
    }

    public void setMessageSubject(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='MessageSubject' AND TableName='tNIM3_CommTrack'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[MessageSubject]=["+newValue+"]");
                   dbDB.setMessageSubject(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[MessageSubject]=["+newValue+"]");
           dbDB.setMessageSubject(newValue);
         }
    }
    public String getMessageSubject(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='MessageSubject' AND TableName='tNIM3_CommTrack'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getMessageSubject();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getMessageSubject();
         }
        return myVal;
    }

    public void setRequiredFieldList(java.util.Hashtable requiredFieldList)
	 {

        this.requiredFieldList = requiredFieldList;
    }

    public java.util.Hashtable getRequiredFieldList()
    {
        return this.requiredFieldList;
    }
    public void setExpiredFieldList(java.util.Hashtable expiredFieldList)
    {
        this.expiredFieldList = expiredFieldList;
    }

    public void setEnglishFieldList(java.util.Hashtable englishFieldList)
    {
        this.englishFieldList = englishFieldList;
    }

    public java.util.Hashtable getExpiredFieldList()
    {
        return this.expiredFieldList;
    }
    public boolean isExpired(java.util.Date myDate, int daysVector)
    {
       return isExpired(myDate,new java.util.Date(),daysVector);
    }


    public boolean isRead(String fieldN, Integer GroupRefID)
    {
        boolean myVal = false;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='"+fieldN+"' AND TableName='tNIM3_CommTrack'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==1)
               {
                   myVal = true;
               }
            }
        }
        else
        {

           if (iSecurityCheck.intValue()>=1)
           {
                  myVal = true;
           }
        }
        return myVal;
    }


    public boolean isWrite(String fieldN, Integer GroupRefID)
    {
        boolean myVal = false;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='"+fieldN+"' AND TableName='tNIM3_CommTrack'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {
                   myVal = true;
               }
            }
        }
        else
        {

           if (iSecurityCheck.intValue()>=2)
           {
                  myVal = true;
           }
        }
        return myVal;
    }


    public boolean isExpired(java.util.Date myDate, java.util.Date refDate,int daysVector)
    {
        java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat("yyyy-MM-dd");
        boolean myVal = true;
        try
        {
            if (myDate.after(dbdf.parse("1800-01-10")))
            {
                if (myDate.after(dateChange(refDate,daysVector)))
                {
                    myVal=false;
                }
            }
            else
            {
                myVal = false;
            }
        }
        catch (Exception Datee)
        {
        }
        return myVal;
    }

    public java.util.Date dateChange(java.util.Date myD, int dayChange)
    {
        java.util.Calendar calendar = java.util.Calendar.getInstance();
        calendar.setTime(myD);
        calendar.add(java.util.Calendar.DATE, dayChange);
        java.util.Date resultAsDate = new java.util.Date(calendar.getTime().getTime());
        return resultAsDate;
    }

    public boolean isRequired(String fieldN)
    {
        return isRequired(fieldN, new Integer("1"));
    }

    public boolean isRequired(String fieldN, Integer GroupRefID)
    {
        boolean myVal = false;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='"+fieldN+"' AND TableName='tNIM3_CommTrack'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getRequiredID().intValue()==1)
               {
                   myVal = true;
               }
            }
        }
        else
        {
            if (this.getRequiredFieldList().containsKey(fieldN))
            {
                myVal = true;
            }
        }
        return myVal;
    }
    public boolean isExpiredCheck(String fieldN)
    {
        return isExpiredCheck(fieldN, new Integer ("1"));
    }

    public boolean isExpiredCheck(String fieldN, Integer GroupRefID)
    {
        boolean myVal = false;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='"+fieldN+"' AND TableName='tNIM3_CommTrack'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getTrackedID().intValue()==1)
               {
                   myVal = true;
               }
            }
        }
        else
        {
            if (this.getExpiredFieldList().containsKey(fieldN))
           {
                myVal = true;
            }
        }
        return myVal;
    }

    public void makeRequiredHash()
    {
       makeRequiredHash(new Integer(1));
    }

    public void makeRequiredHash(Integer GroupRefID)
    {

        java.util.Hashtable newHash = new java.util.Hashtable();
        if (runFieldSecurity)
        {
            java.util.Enumeration myKeys = englishFieldList.keys(); 
            while (myKeys.hasMoreElements()) 
            {
                 String myElement = (String) myKeys.nextElement();
                 bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='"+myElement+"' AND TableName='tNIM3_CommTrack'" ,"");
                 bltFieldSecurity        working_bltFieldSecurity;
                 ListElement         leCurrentElement;
                 Enumeration eList = myFSL.elements();
                 if (eList.hasMoreElements())
                 {
                     leCurrentElement    = (ListElement) eList.nextElement();
                     working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
                     if (working_bltFieldSecurity.getRequiredID().intValue()==1)
                     {
                         newHash.put(myElement,new Boolean(true));
                     }
                 }
            }
        }
        else
        {
            newHash.put("MessageText",new Boolean(true));
            newHash.put("MessageName",new Boolean(true));
            newHash.put("MessageEmail",new Boolean(true));
            newHash.put("MessagePhone",new Boolean(true));
            newHash.put("MessageSubject",new Boolean(true));
        }
        this.setRequiredFieldList(newHash);
    }

    public void makeEnglishHash()
    {

        java.util.Hashtable newHash = new java.util.Hashtable();        newHash.put("UniqueCreateDate","UniqueCreateDate");
        newHash.put("UniqueModifyDate","UniqueModifyDate");
        newHash.put("UniqueModifyComments","UniqueModifyComments");
        newHash.put("CaseID","CaseID");
        newHash.put("EncounterID","EncounterID");
        newHash.put("ReferralID","ReferralID");
        newHash.put("ServiceID","ServiceID");
        newHash.put("IntUserID","IntUserID");
        newHash.put("ExtUserID","ExtUserID");
        newHash.put("CommTypeID","CommTypeID");
        newHash.put("CommStart","CommStart");
        newHash.put("CommEnd","CommEnd");
        newHash.put("MessageText","MessageText");
        newHash.put("MessageName","MessageName");
        newHash.put("MessageCompany","MessageCompany");
        newHash.put("MessageEmail","MessageEmail");
        newHash.put("MessagePhone","MessagePhone");
        newHash.put("MessageFax","MessageFax");
        newHash.put("AlertStatusCode","AlertStatusCode");
        newHash.put("Comments","Comments");
        newHash.put("CommReferenceID","CommReferenceID");
        newHash.put("Link_UserID","Link_UserID");
        newHash.put("MessageSubject","MessageSubject");

        this.setEnglishFieldList(newHash);
    }
    public void makeExpiredHash()
    {

        java.util.Hashtable newHash = new java.util.Hashtable();
        this.setExpiredFieldList(newHash);
    }

    public java.util.Hashtable englishFieldList = new java.util.Hashtable();
    public java.util.Hashtable requiredFieldList = new java.util.Hashtable();
    public java.util.Hashtable expiredFieldList = new java.util.Hashtable();

    public boolean isComplete()
    {
	     boolean myVal = true;
	     java.util.Enumeration myKeys = this.getRequiredFieldList().keys(); 
	     while (myKeys.hasMoreElements()) 
	     {
	         String myElement = (String) myKeys.nextElement();
	         if (!isComplete(myElement))
	         {
	             myVal = false;
	         }
	     }
	     return myVal;
    }

    public String getEnglish(String fieldN)
    {
        String myVal = null;
        if (this.englishFieldList.containsKey(fieldN))
        {
            myVal = (String)this.englishFieldList.get(fieldN);
        }
        return myVal;
    }
    public java.util.Vector isComplete(int daysVector)
    {
	     boolean myVal = false;
	     java.util.Enumeration myKeys = this.getExpiredFieldList().keys(); 
	     java.util.Vector myExpiredList = new java.util.Vector(); 
	     while (myKeys.hasMoreElements()) 
	     {
	         String myElement = (String) myKeys.nextElement();
	         if (isExpired(myElement,daysVector))
	         {
	             myVal = true;
	             myExpiredList.addElement(myElement);
	         }
	     }
	     return myExpiredList;
    }

    public void setField(String fieldN, Object fieldV)
    {
     boolean myVal = false;
     try
     {
        java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat("yyyy-MM-dd");

        if (fieldN.equals("CommTrackID"))
        {
             this.setCommTrackID((Integer)fieldV);
        }

        else if (fieldN.equals("UniqueCreateDate"))
        {
            this.setUniqueCreateDate((java.util.Date)fieldV);
        }

        else if (fieldN.equals("UniqueModifyDate"))
        {
            this.setUniqueModifyDate((java.util.Date)fieldV);
        }

        else if (fieldN.equals("UniqueModifyComments"))
        {
            this.setUniqueModifyComments((String)fieldV);
        }

        else if (fieldN.equals("CaseID"))
        {
             this.setCaseID((Integer)fieldV);
        }

        else if (fieldN.equals("EncounterID"))
        {
             this.setEncounterID((Integer)fieldV);
        }

        else if (fieldN.equals("ReferralID"))
        {
             this.setReferralID((Integer)fieldV);
        }

        else if (fieldN.equals("ServiceID"))
        {
             this.setServiceID((Integer)fieldV);
        }

        else if (fieldN.equals("IntUserID"))
        {
             this.setIntUserID((Integer)fieldV);
        }

        else if (fieldN.equals("ExtUserID"))
        {
             this.setExtUserID((Integer)fieldV);
        }

        else if (fieldN.equals("CommTypeID"))
        {
             this.setCommTypeID((Integer)fieldV);
        }

        else if (fieldN.equals("CommStart"))
        {
            this.setCommStart((java.util.Date)fieldV);
        }

        else if (fieldN.equals("CommEnd"))
        {
            this.setCommEnd((java.util.Date)fieldV);
        }

        else if (fieldN.equals("MessageText"))
        {
            this.setMessageText((String)fieldV);
        }

        else if (fieldN.equals("MessageName"))
        {
            this.setMessageName((String)fieldV);
        }

        else if (fieldN.equals("MessageCompany"))
        {
            this.setMessageCompany((String)fieldV);
        }

        else if (fieldN.equals("MessageEmail"))
        {
            this.setMessageEmail((String)fieldV);
        }

        else if (fieldN.equals("MessagePhone"))
        {
            this.setMessagePhone((String)fieldV);
        }

        else if (fieldN.equals("MessageFax"))
        {
            this.setMessageFax((String)fieldV);
        }

        else if (fieldN.equals("AlertStatusCode"))
        {
             this.setAlertStatusCode((Integer)fieldV);
        }

        else if (fieldN.equals("Comments"))
        {
            this.setComments((String)fieldV);
        }

        else if (fieldN.equals("CommReferenceID"))
        {
             this.setCommReferenceID((Integer)fieldV);
        }

        else if (fieldN.equals("Link_UserID"))
        {
             this.setLink_UserID((Integer)fieldV);
        }

        else if (fieldN.equals("MessageSubject"))
        {
            this.setMessageSubject((String)fieldV);
        }

	    }
	    catch(Exception e2)
	    {}
	    //return myVal;
	}

    public String getFieldType(String fieldN)
    {
     String  myVal = null;
     try
     {
        java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat("yyyy-MM-dd");

        if (fieldN.equals("CommTrackID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("UniqueCreateDate"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("UniqueModifyDate"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("UniqueModifyComments"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("CaseID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("EncounterID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("ReferralID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("ServiceID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("IntUserID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("ExtUserID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("CommTypeID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("CommStart"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("CommEnd"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("MessageText"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("MessageName"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("MessageCompany"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("MessageEmail"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("MessagePhone"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("MessageFax"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("AlertStatusCode"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("Comments"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("CommReferenceID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("Link_UserID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("MessageSubject"))
        {
            myVal = "String";
        }

	    }
	    catch(Exception e2)
	    {}
	    return myVal;
	}

    public boolean isComplete(String fieldN)
    {
	     boolean myVal = false;
	     try
	     {
        java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat("yyyy-MM-dd");

	     if (fieldN.equals("CommTrackID"))

	     {
                    if (this.getCommTrackID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("UniqueCreateDate"))

	     {
                    if (this.getUniqueCreateDate().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("UniqueModifyDate"))

	     {
                    if (this.getUniqueModifyDate().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("UniqueModifyComments"))

	     {
                    if (this.getUniqueModifyComments().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("CaseID"))

	     {
                    if (this.getCaseID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("EncounterID"))

	     {
                    if (this.getEncounterID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("ReferralID"))

	     {
                    if (this.getReferralID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("ServiceID"))

	     {
                    if (this.getServiceID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("IntUserID"))

	     {
                    if (this.getIntUserID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("ExtUserID"))

	     {
                    if (this.getExtUserID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("CommTypeID"))

	     {
                    if (this.getCommTypeID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("CommStart"))

	     {
                    if (this.getCommStart().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("CommEnd"))

	     {
                    if (this.getCommEnd().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("MessageText"))

	     {
                    if (this.getMessageText().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("MessageName"))

	     {
                    if (this.getMessageName().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("MessageCompany"))

	     {
                    if (this.getMessageCompany().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("MessageEmail"))

	     {
                    if (this.getMessageEmail().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("MessagePhone"))

	     {
                    if (this.getMessagePhone().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("MessageFax"))

	     {
                    if (this.getMessageFax().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("AlertStatusCode"))

	     {
                    if (this.getAlertStatusCode().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("Comments"))

	     {
                    if (this.getComments().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("CommReferenceID"))

	     {
                    if (this.getCommReferenceID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("Link_UserID"))

	     {
                    if (this.getLink_UserID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("MessageSubject"))

	     {
                    if (this.getMessageSubject().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	    }
	    catch(Exception e2)
	    {}
	    return myVal;
	}

    public boolean isExpired(String fieldN,int daysVector)
    {
        return isExpired(fieldN,new java.util.Date(),daysVector);
    }

    public boolean isExpired(String fieldN,java.util.Date refDate,int daysVector)
    {
	     boolean myVal = false;
	     try
	     {
        java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat("yyyy-MM-dd");

        if (fieldN.equals("UniqueCreateDate"))

	     {
                    if (this.isExpired(this.getUniqueCreateDate(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

        if (fieldN.equals("UniqueModifyDate"))

	     {
                    if (this.isExpired(this.getUniqueModifyDate(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

        if (fieldN.equals("CommStart"))

	     {
                    if (this.isExpired(this.getCommStart(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

        if (fieldN.equals("CommEnd"))

	     {
                    if (this.isExpired(this.getCommEnd(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

	    }
	    catch(Exception e2)
	    {}
	    return myVal;
	}

    public java.util.Date getDateField(String fieldN)
    {
	     java.util.Date myVal = null;
	     try
	     {
        java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat("yyyy-MM-dd");

        if (fieldN.equals("UniqueCreateDate"))

	     {
             myVal = this.getUniqueCreateDate();
        }

        if (fieldN.equals("UniqueModifyDate"))

	     {
             myVal = this.getUniqueModifyDate();
        }

        if (fieldN.equals("CommStart"))

	     {
             myVal = this.getCommStart();
        }

        if (fieldN.equals("CommEnd"))

	     {
             myVal = this.getCommEnd();
        }

	    }
	    catch(Exception e2)
	    {}
	    return myVal;
	}
	public boolean runFieldSecurity = false;
	public Integer UserSecurityID = null;
   public Integer iSecurityCheck = null;
}    // End of bltNIM3_CommTrack class definition
