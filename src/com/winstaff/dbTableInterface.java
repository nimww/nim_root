package com.winstaff;
/*
 * dbTableInterface.java
 *
 * Created on May 2, 2001, 3:40 PM
 */

import java.sql.SQLException;

/**
 *
 * @author  Michael Million
 * @version 1.00
 */
public interface dbTableInterface {
    public void     setUniqueID (Integer iUniqueID);
    public Integer  getUniqueID ();
    public String   getFieldType (String fieldN);
    public void setField(String fieldN, Object fieldV);
    public void     commitData  () throws SQLException;
}   // End of dbTableInterface interface definition

