

package com.winstaff;

import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public interface InttNIM3_CPTGroupList extends dbTableInterface
{

    public void setLookupID(Integer newValue);
    public Integer getLookupID();
    public void setUniqueCreateDate(Date newValue);
    public Date getUniqueCreateDate();
    public void setUniqueModifyDate(Date newValue);
    public Date getUniqueModifyDate();
    public void setUniqueModifyComments(String newValue);
    public String getUniqueModifyComments();
    public void setCPTGroupID(Integer newValue);
    public Integer getCPTGroupID();
    public void setCPT(String newValue);
    public String getCPT();
}    // End of bltNIM3_CPTGroupList class definition
