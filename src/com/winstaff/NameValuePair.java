package com.winstaff;
/*
 * NameValuePair.java
 *
 * Created on April 17, 2001, 1:40 PM
 */


/**
 *
 * @author  Michael Million
 * @version 1.00
 */

/*
 * NameValuePair.java
 *
 * Updated for SQL Injection 9/2009
 */


/**
 *
 * @author  Scott Ellis
 * @version 1.10
 */


public class NameValuePair extends Object {

    private String      sName           = "";
    private String      sValue          = "";
    private boolean     bIsModified     = false;
    
    /** Creates new NameValuePair */
    public NameValuePair ()
    {
    }   // End of default constructor

    /** Construct an NameValuePair object given default values
    * @param sName String Name value
    * @param sValue String Value
    */    
    public NameValuePair ( java.lang.String sName, java.lang.String sValue )
    {
        setName( sName );
        setValue( sValue );
    }   // End of constructor

    /** Construct an NameValuePair object given default values
    * @param sName String Name value
    * @param sValue String Value
    * @param bIsModified boolean Value
    */    
    public NameValuePair ( java.lang.String sName, java.lang.String sValue, boolean bIsModified )
    {
        setName( sName );
        setValue( sValue );
        setIsModified( bIsModified );
    }   // End of constructor

    /** Return name
    * @return String Current Name
    */    
    public String getName ()
    {
        return sName;
    }   // End of getName()
    
    /** Set given name
    * @param sValue String New name
    */    
    public void setName (java.lang.String sValue)
    {
        sName = sValue;
    }   // End of setName()

    /** Return value
    * @return String Value
    */    
    public String getValue ()
    {
	//insert escape code here - Method 2
        return sValue;
    }   // End of getValue()

    /** Set a new value
    * @param sValue String new value
    */    
    public void setValue (java.lang.String sValue)
    {
        this.sValue = sValue;
    }   // End of setValue()

    /** Set a new value
    * @param sValue String new value
    */    
    public void setValueWithModify (java.lang.String sValue)
    {
        this.sValue = sValue;
        setIsModified( true );
    }   // End of setValueWithModify()
    
    public boolean getIsModified()
    {
        return bIsModified;
    }   // End of getIsModified()
    
    public void setIsModified( boolean bIsModified )
    {
        this.bIsModified = bIsModified;
    }   // End of setIsModified()
    
    /** stringize this object
    * @return String Value
    */     
    public String toString()
    {
        return "{" + getName() + ", " + getValue() + ", " + ( getIsModified()?"Modified":"Not Modified" ) + "}";
    }   // End of toString()
}   // End of NameValuePair class definition
