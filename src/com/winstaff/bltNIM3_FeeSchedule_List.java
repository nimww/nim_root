
package com.winstaff;
/*
 * bltNIM3_FeeSchedule_List.java
 *
 * Created: Tue Feb 23 19:13:30 PST 2010
 */



import java.sql.*;




/**
 *
 * @author  Scott Ellis via listClassGenCode
 * @version 2.5
 */
public class bltNIM3_FeeSchedule_List extends db_BaseRelationTable
{
    /** Default constructor
    */    
    public bltNIM3_FeeSchedule_List ( Integer iFeeScheduleRefID )
    {
        super( "tNIM3_FeeSchedule", "FeeScheduleID", "FeeScheduleID", "FeeScheduleRefID", iFeeScheduleRefID );
    }   // End of bltNIM3_FeeSchedule_List()
    

    public void setField(String fieldN, Object fieldV)
    {DebugLogger.println("setField Method not implemented yet in classbltNIM3_FeeSchedule_List!");}

    public String getFieldType(String fieldN)
    {return null;}

    public bltNIM3_FeeSchedule_List ( Integer iFeeScheduleRefID, String extraWhere, String OrderBy )
    {
        super( "tNIM3_FeeSchedule", "FeeScheduleID", "FeeScheduleID", "FeeScheduleRefID", iFeeScheduleRefID, extraWhere,OrderBy );
    }   // End of bltNIM3_FeeSchedule_List()
     /** Method pull information from the result set and load into class attributes.
    * @param sFieldList Comma delimited list of fields for which data has been retrieved.
    * @param rs Result set from which to pull the data from.
    * @throws SQLException Passes on SQLException
    */      
    protected void transferData( ResultSet rs ) throws SQLException
    {
        while ( rs.next()  )
        {
            Integer         iValue              = new Integer( rs.getString( sLinkFieldName ) );
            Integer         iLinkRecordID       = new Integer( rs.getString( sUniqueIDFieldName ) );
            Object          oReferenceObject;
            ListElement     leNewElement;
            
            oReferenceObject    = new bltNIM3_FeeSchedule( iValue );
            
            leNewElement        = new ListElement( iLinkRecordID, oReferenceObject );
            
            vList.add( leNewElement );

        }
    }   // End of transferData()
}   // End of bltNIM3_FeeSchedule_List class

