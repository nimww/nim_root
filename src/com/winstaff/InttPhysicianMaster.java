

package com.winstaff;

import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public interface InttPhysicianMaster extends dbTableInterface
{

    public void setPhysicianID(Integer newValue);
    public Integer getPhysicianID();
    public void setUniqueCreateDate(Date newValue);
    public Date getUniqueCreateDate();
    public void setUniqueModifyDate(Date newValue);
    public Date getUniqueModifyDate();
    public void setUniqueModifyComments(String newValue);
    public String getUniqueModifyComments();
    public void setSalutation(Integer newValue);
    public Integer getSalutation();
    public void setTitle(String newValue);
    public String getTitle();
    public void setFirstName(String newValue);
    public String getFirstName();
    public void setMiddleName(String newValue);
    public String getMiddleName();
    public void setLastName(String newValue);
    public String getLastName();
    public void setSuffix(String newValue);
    public String getSuffix();
    public void setHomeEmail(String newValue);
    public String getHomeEmail();
    public void setHomeAddress1(String newValue);
    public String getHomeAddress1();
    public void setHomeAddress2(String newValue);
    public String getHomeAddress2();
    public void setHomeCity(String newValue);
    public String getHomeCity();
    public void setHomeStateID(Integer newValue);
    public Integer getHomeStateID();
    public void setHomeProvince(String newValue);
    public String getHomeProvince();
    public void setHomeZIP(String newValue);
    public String getHomeZIP();
    public void setHomeCountryID(Integer newValue);
    public Integer getHomeCountryID();
    public void setHomePhone(String newValue);
    public String getHomePhone();
    public void setHomeFax(String newValue);
    public String getHomeFax();
    public void setHomeMobile(String newValue);
    public String getHomeMobile();
    public void setHomePager(String newValue);
    public String getHomePager();
    public void setAlertEmail(String newValue);
    public String getAlertEmail();
    public void setAlertDays(Integer newValue);
    public Integer getAlertDays();
    public void setOtherName1(String newValue);
    public String getOtherName1();
    public void setOtherName1Start(Date newValue);
    public Date getOtherName1Start();
    public void setOtherName1End(Date newValue);
    public Date getOtherName1End();
    public void setOtherName2(String newValue);
    public String getOtherName2();
    public void setOtherName2Start(Date newValue);
    public Date getOtherName2Start();
    public void setOtherName2End(Date newValue);
    public Date getOtherName2End();
    public void setDateOfBirth(Date newValue);
    public Date getDateOfBirth();
    public void setPlaceOfBirth(String newValue);
    public String getPlaceOfBirth();
    public void setSpouse(String newValue);
    public String getSpouse();
    public void setCitizenshipYN(Integer newValue);
    public Integer getCitizenshipYN();
    public void setCitizenship(String newValue);
    public String getCitizenship();
    public void setVisaNumber(String newValue);
    public String getVisaNumber();
    public void setVisaStatus(String newValue);
    public String getVisaStatus();
    public void setEligibleToWorkInUS(Integer newValue);
    public Integer getEligibleToWorkInUS();
    public void setSSN(String newValue);
    public String getSSN();
    public void setGender(Integer newValue);
    public Integer getGender();
    public void setMilitaryActive(Integer newValue);
    public Integer getMilitaryActive();
    public void setMilitaryBranch(String newValue);
    public String getMilitaryBranch();
    public void setMilitaryReserve(Integer newValue);
    public Integer getMilitaryReserve();
    public void setHospitalAdmitingPrivileges(Integer newValue);
    public Integer getHospitalAdmitingPrivileges();
    public void setHospitalAdmitingPrivilegesNo(String newValue);
    public String getHospitalAdmitingPrivilegesNo();
    public void setPhysicianCategoryID(Integer newValue);
    public Integer getPhysicianCategoryID();
    public void setIPAMedicalAffiliation(Integer newValue);
    public Integer getIPAMedicalAffiliation();
    public void setAffiliationDesc1(String newValue);
    public String getAffiliationDesc1();
    public void setPhysicianLanguages(String newValue);
    public String getPhysicianLanguages();
    public void setECFMGNo(String newValue);
    public String getECFMGNo();
    public void setECFMGDateIssued(Date newValue);
    public Date getECFMGDateIssued();
    public void setECFMGDateExpires(Date newValue);
    public Date getECFMGDateExpires();
    public void setMedicareUPIN(String newValue);
    public String getMedicareUPIN();
    public void setUniqueNPI(String newValue);
    public String getUniqueNPI();
    public void setMedicareParticipation(Integer newValue);
    public Integer getMedicareParticipation();
    public void setMedicareNo(String newValue);
    public String getMedicareNo();
    public void setMediCaidParticipation(Integer newValue);
    public Integer getMediCaidParticipation();
    public void setMediCaidNo(String newValue);
    public String getMediCaidNo();
    public void setSupplementalIDNumber1(String newValue);
    public String getSupplementalIDNumber1();
    public void setSupplementalIDNumber2(String newValue);
    public String getSupplementalIDNumber2();
    public void setComments(String newValue);
    public String getComments();
    public void setAttestConsent(Integer newValue);
    public Integer getAttestConsent();
    public void setAttestationComments(String newValue);
    public String getAttestationComments();
    public void setIsViewable(Integer newValue);
    public Integer getIsViewable();
    public void setIsAttested(Integer newValue);
    public Integer getIsAttested();
    public void setAttestDatePending(Date newValue);
    public Date getAttestDatePending();
    public void setIsAttestedPending(Integer newValue);
    public Integer getIsAttestedPending();
    public void setAttestDate(Date newValue);
    public Date getAttestDate();
    public void setDeAttestDate(Date newValue);
    public Date getDeAttestDate();
    public void setLastModifiedDate(Date newValue);
    public Date getLastModifiedDate();
    public void setQuestionValidation(String newValue);
    public String getQuestionValidation();
    public void setUSMLEDatePassedStep1(Date newValue);
    public Date getUSMLEDatePassedStep1();
    public void setUSMLEDatePassedStep2(Date newValue);
    public Date getUSMLEDatePassedStep2();
    public void setUSMLEDatePassedStep3(Date newValue);
    public Date getUSMLEDatePassedStep3();
    public void setHaveFLEX(Integer newValue);
    public Integer getHaveFLEX();
    public void setFLEXDatePassed(Date newValue);
    public Date getFLEXDatePassed();
    public void setVisaSponsor(String newValue);
    public String getVisaSponsor();
    public void setVisaExpiration(Date newValue);
    public Date getVisaExpiration();
    public void setCurrentVisaTemporary(Integer newValue);
    public Integer getCurrentVisaTemporary();
    public void setCurrentVisaExtended(Integer newValue);
    public Integer getCurrentVisaExtended();
    public void setHaveGreencard(Integer newValue);
    public Integer getHaveGreencard();
    public void setCountryOfIssueID(Integer newValue);
    public Integer getCountryOfIssueID();
    public void setVisaTemporary5Years(Integer newValue);
    public Integer getVisaTemporary5Years();
    public void setPastVisa1DateFrom(Date newValue);
    public Date getPastVisa1DateFrom();
    public void setPastVisa1DateTo(Date newValue);
    public Date getPastVisa1DateTo();
    public void setPastVisa1Type(String newValue);
    public String getPastVisa1Type();
    public void setPastVisa1Sponsor(String newValue);
    public String getPastVisa1Sponsor();
    public void setPastVisa2DateFrom(Date newValue);
    public Date getPastVisa2DateFrom();
    public void setPastVisa2DateTo(Date newValue);
    public Date getPastVisa2DateTo();
    public void setPastVisa2Type(String newValue);
    public String getPastVisa2Type();
    public void setPastVisa2Sponsor(String newValue);
    public String getPastVisa2Sponsor();
    public void setMilitaryRank(String newValue);
    public String getMilitaryRank();
    public void setMilitaryDutyStatus(String newValue);
    public String getMilitaryDutyStatus();
    public void setMilitaryCurrentAssignment(String newValue);
    public String getMilitaryCurrentAssignment();
}    // End of bltPhysicianMaster class definition
