package com.winstaff;


import com.winstaff.dbtPhysicianPracticeLU;
import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;
import java.util.Enumeration;

public class bltPhysicianPracticeLU extends Object implements InttPhysicianPracticeLU
{

    dbtPhysicianPracticeLU    dbDB;

    public bltPhysicianPracticeLU()
    {
        dbDB = new dbtPhysicianPracticeLU();
        dbDB.setUniqueCreateDate(new java.util.Date());
        makeRequiredHash();
        makeExpiredHash();
        makeEnglishHash();

    }    // End of default constructor

    public bltPhysicianPracticeLU( Integer iNewID )
    {        dbDB = new dbtPhysicianPracticeLU( iNewID );
        makeRequiredHash();
        makeExpiredHash();
        makeEnglishHash();
    }    // End of Constructor knowing an ID


    public bltPhysicianPracticeLU( Integer iNewID, Integer iGroupSecurityID )
    {
        this.GroupSecurityInit(iGroupSecurityID);
        dbDB = new dbtPhysicianPracticeLU( iNewID );
        makeExpiredHash();
        makeEnglishHash();
        makeRequiredHash(iGroupSecurityID);
    }    // End of Constructor knowing an ID

    public bltPhysicianPracticeLU( Integer iGroupSecurityID, boolean isTrue )
    {
        this.GroupSecurityInit(iGroupSecurityID);
        dbDB = new dbtPhysicianPracticeLU(  );
        makeExpiredHash();
        makeEnglishHash();
        makeRequiredHash(iGroupSecurityID);
    }    // End of Security unknown ID

    public void GroupSecurityInit( Integer iGroupSecurityID )
    {
        bltSecurityGroupMaster mySGM = new bltSecurityGroupMaster(iGroupSecurityID);
        this.UserSecurityID = iGroupSecurityID;
        if (mySGM.getRunFieldSecurityID().intValue()==1)
        {
            this.runFieldSecurity = true;
            makeRequiredHash(iGroupSecurityID);
        }
    }  

    public EventReferenceType getEventReference()
    {
        return EventUtils.getEventReference("tPhysicianPracticeLU", this.getUniqueID());
    }  

    public String convertString2LogString(String normalString) 
    {
        String myVal = "";
        for (int i=0;i<normalString.length();i++)
        {
            if ( (normalString.charAt(i)!='\r')&&(normalString.charAt(i)!='\n')&&(normalString.charAt(i)!='\t') )
            {
                myVal+=normalString.charAt(i);
            }
            else
            {
                myVal+="<NLT>";
            }
        }
        return myVal;
    }
    public void runAudit()
    {
        String AuditString = new java.util.Date()+" \t " ;  
        AuditString += " tPhysicianPracticeLU "; 
        AuditString += " LookupID ="+this.getUniqueID(); 

        for (int i=0;i<this.AuditVector.size();i++)
        {
            AuditString+="\t"+convertString2LogString((String)this.AuditVector.elementAt(i));
        }
        AuditLogger.printLine(AuditString);
    }
    public void commitData() throws SQLException
    {
        runAudit();
        dbDB.setModifyComments(this.getUniqueModifyComments());
        dbDB.setUniqueModifyDate(new java.util.Date());
        dbDB.commitData();

        this.AuditVector.removeAllElements();
    }    // End of commitData()

    public void commitDataForced() throws SQLException
    {
        dbDB.commitDataForced();
    }    // End of commitData()

    public Integer getUniqueID()
    {
        return dbDB.getUniqueID();
    }    // End of getUniqueID()

    public void setUniqueID( Integer iNewID )
    {
        dbDB.setUniqueID( iNewID );
    }    // End of setUniqueID()


    public void setLookupID(Integer newValue)
    {
        dbDB.setLookupID(newValue);
    }

    public Integer getLookupID()
    {
        return dbDB.getLookupID();
    }

    java.util.Vector AuditVector = new java.util.Vector();

    public void setUniqueCreateDate(Date newValue)
    {        this.setUniqueCreateDate(newValue,this.UserSecurityID);

    }
    public Date getUniqueCreateDate()
    {
        return this.getUniqueCreateDate(this.UserSecurityID);
    }

    public void setUniqueCreateDate(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='UniqueCreateDate' AND TableName='tPhysicianPracticeLU'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {
                   this.AuditVector.addElement("[UniqueCreateDate]=["+newValue+"]");
                   this.AuditVector.addElement("[UniqueCreateDate]=["+newValue+"]");
                   dbDB.setUniqueCreateDate(newValue);
               }
            }
        }
        else
        {
           this.AuditVector.addElement("[UniqueCreateDate]=["+newValue+"]");
           this.AuditVector.addElement("[UniqueCreateDate]=["+newValue+"]");
           dbDB.setUniqueCreateDate(newValue);
         }
    }
    public Date getUniqueCreateDate(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='UniqueCreateDate' AND TableName='tPhysicianPracticeLU'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getUniqueCreateDate();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getUniqueCreateDate();
         }
        return myVal;
    }

    public void setUniqueModifyDate(Date newValue)
    {        this.setUniqueModifyDate(newValue,this.UserSecurityID);

    }
    public Date getUniqueModifyDate()
    {
        return this.getUniqueModifyDate(this.UserSecurityID);
    }

    public void setUniqueModifyDate(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='UniqueModifyDate' AND TableName='tPhysicianPracticeLU'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {
                   this.AuditVector.addElement("[UniqueModifyDate]=["+newValue+"]");
                   this.AuditVector.addElement("[UniqueModifyDate]=["+newValue+"]");
                   dbDB.setUniqueModifyDate(newValue);
               }
            }
        }
        else
        {
           this.AuditVector.addElement("[UniqueModifyDate]=["+newValue+"]");
           this.AuditVector.addElement("[UniqueModifyDate]=["+newValue+"]");
           dbDB.setUniqueModifyDate(newValue);
         }
    }
    public Date getUniqueModifyDate(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='UniqueModifyDate' AND TableName='tPhysicianPracticeLU'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getUniqueModifyDate();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getUniqueModifyDate();
         }
        return myVal;
    }

    public void setUniqueModifyComments(String newValue)
    {        this.setUniqueModifyComments(newValue,this.UserSecurityID);

    }
    public String getUniqueModifyComments()
    {
        return this.getUniqueModifyComments(this.UserSecurityID);
    }

    public void setUniqueModifyComments(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='UniqueModifyComments' AND TableName='tPhysicianPracticeLU'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {
                   this.AuditVector.addElement("[UniqueModifyComments]=["+newValue+"]");
                   this.AuditVector.addElement("[UniqueModifyComments]=["+newValue+"]");
                   dbDB.setUniqueModifyComments(newValue);
               }
            }
        }
        else
        {
           this.AuditVector.addElement("[UniqueModifyComments]=["+newValue+"]");
           this.AuditVector.addElement("[UniqueModifyComments]=["+newValue+"]");
           dbDB.setUniqueModifyComments(newValue);
         }
    }
    public String getUniqueModifyComments(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='UniqueModifyComments' AND TableName='tPhysicianPracticeLU'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getUniqueModifyComments();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getUniqueModifyComments();
         }
        return myVal;
    }

    public void setPhysicianID(Integer newValue)
    {        this.setPhysicianID(newValue,this.UserSecurityID);

    }
    public Integer getPhysicianID()
    {
        return this.getPhysicianID(this.UserSecurityID);
    }

    public void setPhysicianID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PhysicianID' AND TableName='tPhysicianPracticeLU'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {
                   this.AuditVector.addElement("[PhysicianID]=["+newValue+"]");
                   this.AuditVector.addElement("[PhysicianID]=["+newValue+"]");
                   dbDB.setPhysicianID(newValue);
               }
            }
        }
        else
        {
           this.AuditVector.addElement("[PhysicianID]=["+newValue+"]");
           this.AuditVector.addElement("[PhysicianID]=["+newValue+"]");
           dbDB.setPhysicianID(newValue);
         }
    }
    public Integer getPhysicianID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PhysicianID' AND TableName='tPhysicianPracticeLU'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPhysicianID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPhysicianID();
         }
        return myVal;
    }

    public void setPracticeID(Integer newValue)
    {        this.setPracticeID(newValue,this.UserSecurityID);

    }
    public Integer getPracticeID()
    {
        return this.getPracticeID(this.UserSecurityID);
    }

    public void setPracticeID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PracticeID' AND TableName='tPhysicianPracticeLU'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {
                   this.AuditVector.addElement("[PracticeID]=["+newValue+"]");
                   this.AuditVector.addElement("[PracticeID]=["+newValue+"]");
                   dbDB.setPracticeID(newValue);
               }
            }
        }
        else
        {
           this.AuditVector.addElement("[PracticeID]=["+newValue+"]");
           this.AuditVector.addElement("[PracticeID]=["+newValue+"]");
           dbDB.setPracticeID(newValue);
         }
    }
    public Integer getPracticeID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PracticeID' AND TableName='tPhysicianPracticeLU'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPracticeID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPracticeID();
         }
        return myVal;
    }

    public void setStartDate(Date newValue)
    {        this.setStartDate(newValue,this.UserSecurityID);

    }
    public Date getStartDate()
    {
        return this.getStartDate(this.UserSecurityID);
    }

    public void setStartDate(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='StartDate' AND TableName='tPhysicianPracticeLU'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {
                   this.AuditVector.addElement("[StartDate]=["+newValue+"]");
                   this.AuditVector.addElement("[StartDate]=["+newValue+"]");
                   dbDB.setStartDate(newValue);
               }
            }
        }
        else
        {
           this.AuditVector.addElement("[StartDate]=["+newValue+"]");
           this.AuditVector.addElement("[StartDate]=["+newValue+"]");
           dbDB.setStartDate(newValue);
         }
    }
    public Date getStartDate(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='StartDate' AND TableName='tPhysicianPracticeLU'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getStartDate();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getStartDate();
         }
        return myVal;
    }

    public void setEndDate(Date newValue)
    {        this.setEndDate(newValue,this.UserSecurityID);

    }
    public Date getEndDate()
    {
        return this.getEndDate(this.UserSecurityID);
    }

    public void setEndDate(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='EndDate' AND TableName='tPhysicianPracticeLU'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {
                   this.AuditVector.addElement("[EndDate]=["+newValue+"]");
                   this.AuditVector.addElement("[EndDate]=["+newValue+"]");
                   dbDB.setEndDate(newValue);
               }
            }
        }
        else
        {
           this.AuditVector.addElement("[EndDate]=["+newValue+"]");
           this.AuditVector.addElement("[EndDate]=["+newValue+"]");
           dbDB.setEndDate(newValue);
         }
    }
    public Date getEndDate(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='EndDate' AND TableName='tPhysicianPracticeLU'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getEndDate();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getEndDate();
         }
        return myVal;
    }

    public void setisPrimaryOffice(Integer newValue)
    {        this.setisPrimaryOffice(newValue,this.UserSecurityID);

    }
    public Integer getisPrimaryOffice()
    {
        return this.getisPrimaryOffice(this.UserSecurityID);
    }

    public void setisPrimaryOffice(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='isPrimaryOffice' AND TableName='tPhysicianPracticeLU'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {
                   this.AuditVector.addElement("[isPrimaryOffice]=["+newValue+"]");
                   this.AuditVector.addElement("[isPrimaryOffice]=["+newValue+"]");
                   dbDB.setisPrimaryOffice(newValue);
               }
            }
        }
        else
        {
           this.AuditVector.addElement("[isPrimaryOffice]=["+newValue+"]");
           this.AuditVector.addElement("[isPrimaryOffice]=["+newValue+"]");
           dbDB.setisPrimaryOffice(newValue);
         }
    }
    public Integer getisPrimaryOffice(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='isPrimaryOffice' AND TableName='tPhysicianPracticeLU'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getisPrimaryOffice();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getisPrimaryOffice();
         }
        return myVal;
    }

    public void setisAdministrativeOffice(Integer newValue)
    {        this.setisAdministrativeOffice(newValue,this.UserSecurityID);

    }
    public Integer getisAdministrativeOffice()
    {
        return this.getisAdministrativeOffice(this.UserSecurityID);
    }

    public void setisAdministrativeOffice(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='isAdministrativeOffice' AND TableName='tPhysicianPracticeLU'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {
                   this.AuditVector.addElement("[isAdministrativeOffice]=["+newValue+"]");
                   this.AuditVector.addElement("[isAdministrativeOffice]=["+newValue+"]");
                   dbDB.setisAdministrativeOffice(newValue);
               }
            }
        }
        else
        {
           this.AuditVector.addElement("[isAdministrativeOffice]=["+newValue+"]");
           this.AuditVector.addElement("[isAdministrativeOffice]=["+newValue+"]");
           dbDB.setisAdministrativeOffice(newValue);
         }
    }
    public Integer getisAdministrativeOffice(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='isAdministrativeOffice' AND TableName='tPhysicianPracticeLU'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getisAdministrativeOffice();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getisAdministrativeOffice();
         }
        return myVal;
    }

    public void setCoverageHours(String newValue)
    {        this.setCoverageHours(newValue,this.UserSecurityID);

    }
    public String getCoverageHours()
    {
        return this.getCoverageHours(this.UserSecurityID);
    }

    public void setCoverageHours(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CoverageHours' AND TableName='tPhysicianPracticeLU'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {
                   this.AuditVector.addElement("[CoverageHours]=["+newValue+"]");
                   this.AuditVector.addElement("[CoverageHours]=["+newValue+"]");
                   dbDB.setCoverageHours(newValue);
               }
            }
        }
        else
        {
           this.AuditVector.addElement("[CoverageHours]=["+newValue+"]");
           this.AuditVector.addElement("[CoverageHours]=["+newValue+"]");
           dbDB.setCoverageHours(newValue);
         }
    }
    public String getCoverageHours(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CoverageHours' AND TableName='tPhysicianPracticeLU'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getCoverageHours();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getCoverageHours();
         }
        return myVal;
    }

    public void setComments(String newValue)
    {        this.setComments(newValue,this.UserSecurityID);

    }
    public String getComments()
    {
        return this.getComments(this.UserSecurityID);
    }

    public void setComments(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Comments' AND TableName='tPhysicianPracticeLU'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {
                   this.AuditVector.addElement("[Comments]=["+newValue+"]");
                   this.AuditVector.addElement("[Comments]=["+newValue+"]");
                   dbDB.setComments(newValue);
               }
            }
        }
        else
        {
           this.AuditVector.addElement("[Comments]=["+newValue+"]");
           this.AuditVector.addElement("[Comments]=["+newValue+"]");
           dbDB.setComments(newValue);
         }
    }
    public String getComments(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Comments' AND TableName='tPhysicianPracticeLU'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getComments();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getComments();
         }
        return myVal;
    }

    public void setRequiredFieldList(java.util.Hashtable requiredFieldList)
	 {

        this.requiredFieldList = requiredFieldList;
    }

    public java.util.Hashtable getRequiredFieldList()
    {
        return this.requiredFieldList;
    }
    public void setExpiredFieldList(java.util.Hashtable expiredFieldList)
    {
        this.expiredFieldList = expiredFieldList;
    }

    public void setEnglishFieldList(java.util.Hashtable englishFieldList)
    {
        this.englishFieldList = englishFieldList;
    }

    public java.util.Hashtable getExpiredFieldList()
    {
        return this.expiredFieldList;
    }
    public boolean isExpired(java.util.Date myDate, int daysVector)
    {
       return isExpired(myDate,new java.util.Date(),daysVector);
    }


    public boolean isRead(String fieldN, Integer GroupRefID)
    {
        boolean myVal = false;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='"+fieldN+"' AND TableName='tPhysicianPracticeLU'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==1)
               {
                   myVal = true;
               }
            }
        }
        else
        {
           myVal = true;
        }
        return myVal;
    }


    public boolean isWrite(String fieldN, Integer GroupRefID)
    {
        boolean myVal = false;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='"+fieldN+"' AND TableName='tPhysicianPracticeLU'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {
                   myVal = true;
               }
            }
        }
        else
        {
           myVal = true;
        }
        return myVal;
    }


    public boolean isExpired(java.util.Date myDate, java.util.Date refDate,int daysVector)
    {
        java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat("yyyy-MM-dd");
        boolean myVal = true;
        try
        {
            if (myDate.after(dbdf.parse("1800-01-10")))
            {
                if (myDate.after(dateChange(refDate,daysVector)))
                {
                    myVal=false;
                }
            }
            else
            {
                myVal = false;
            }
        }
        catch (Exception Datee)
        {
        }
        return myVal;
    }

    public java.util.Date dateChange(java.util.Date myD, int dayChange)
    {
        java.util.Calendar calendar = java.util.Calendar.getInstance();
        calendar.setTime(myD);
        calendar.add(java.util.Calendar.DATE, dayChange);
        java.util.Date resultAsDate = new java.util.Date(calendar.getTime().getTime());
        return resultAsDate;
    }

    public boolean isRequired(String fieldN)
    {
        return isRequired(fieldN, new Integer("1"));
    }

    public boolean isRequired(String fieldN, Integer GroupRefID)
    {
        boolean myVal = false;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='"+fieldN+"' AND TableName='tPhysicianPracticeLU'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getRequiredID().intValue()==1)
               {
                   myVal = true;
               }
            }
        }
        else
        {
            if (this.getRequiredFieldList().containsKey(fieldN))
            {
                myVal = true;
            }
        }
        return myVal;
    }
    public boolean isExpiredCheck(String fieldN)
    {
        return isExpiredCheck(fieldN, new Integer ("1"));
    }

    public boolean isExpiredCheck(String fieldN, Integer GroupRefID)
    {
        boolean myVal = false;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='"+fieldN+"' AND TableName='tPhysicianPracticeLU'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getTrackedID().intValue()==1)
               {
                   myVal = true;
               }
            }
        }
        else
        {
            if (this.getExpiredFieldList().containsKey(fieldN))
           {
                myVal = true;
            }
        }
        return myVal;
    }

    public void makeRequiredHash()
    {
       makeRequiredHash(new Integer(1));
    }

    public void makeRequiredHash(Integer GroupRefID)
    {

        java.util.Hashtable newHash = new java.util.Hashtable();
        if (runFieldSecurity)
        {
            java.util.Enumeration myKeys = englishFieldList.keys(); 
            while (myKeys.hasMoreElements()) 
            {
                 String myElement = (String) myKeys.nextElement();
                 bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='"+myElement+"' AND TableName='tPhysicianPracticeLU'" ,"");
                 bltFieldSecurity        working_bltFieldSecurity;
                 ListElement         leCurrentElement;
                 Enumeration eList = myFSL.elements();
                 if (eList.hasMoreElements())
                 {
                     leCurrentElement    = (ListElement) eList.nextElement();
                     working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
                     if (working_bltFieldSecurity.getRequiredID().intValue()==1)
                     {
                         newHash.put(myElement,new Boolean(true));
                     }
                 }
            }
        }
        else
        {
            newHash.put("PhysicianID",new Boolean(true));
            newHash.put("PracticeID",new Boolean(true));
            newHash.put("StartDate",new Boolean(true));
            newHash.put("isPrimaryOffice",new Boolean(true));
            newHash.put("isAdministrativeOffice",new Boolean(true));
        }
        this.setRequiredFieldList(newHash);
    }

    public void makeEnglishHash()
    {

        java.util.Hashtable newHash = new java.util.Hashtable();        newHash.put("UniqueCreateDate","Item Create Date");
        newHash.put("UniqueModifyDate","Item Modify Date");
        newHash.put("UniqueModifyComments","Item Modification Comments");
        newHash.put("PhysicianID","PhysicianID");
        newHash.put("PracticeID","PracticeID");
        newHash.put("StartDate","Start Date");
        newHash.put("EndDate","End Date");
        newHash.put("isPrimaryOffice","Is this your primary office?");
        newHash.put("isAdministrativeOffice","Is this your administrative office?");
        newHash.put("CoverageHours","Coverage Hours");
        newHash.put("Comments","Extra Comments");

        this.setEnglishFieldList(newHash);
    }
    public void makeExpiredHash()
    {

        java.util.Hashtable newHash = new java.util.Hashtable();
        this.setExpiredFieldList(newHash);
    }

    public java.util.Hashtable englishFieldList = new java.util.Hashtable();
    public java.util.Hashtable requiredFieldList = new java.util.Hashtable();
    public java.util.Hashtable expiredFieldList = new java.util.Hashtable();

    public boolean isComplete()
    {
	     boolean myVal = true;
	     java.util.Enumeration myKeys = this.getRequiredFieldList().keys(); 
	     while (myKeys.hasMoreElements()) 
	     {
	         String myElement = (String) myKeys.nextElement();
	         if (!isComplete(myElement))
	         {
	             myVal = false;
	         }
	     }
	     return myVal;
    }

    public String getEnglish(String fieldN)
    {
        String myVal = null;
        if (this.englishFieldList.containsKey(fieldN))
        {
            myVal = (String)this.englishFieldList.get(fieldN);
        }
        return myVal;
    }
    public java.util.Vector isComplete(int daysVector)
    {
	     boolean myVal = false;
	     java.util.Enumeration myKeys = this.getExpiredFieldList().keys(); 
	     java.util.Vector myExpiredList = new java.util.Vector(); 
	     while (myKeys.hasMoreElements()) 
	     {
	         String myElement = (String) myKeys.nextElement();
	         if (isExpired(myElement,daysVector))
	         {
	             myVal = true;
	             myExpiredList.addElement(myElement);
	         }
	     }
	     return myExpiredList;
    }

    public void setField(String fieldN, Object fieldV)
    {
     boolean myVal = false;
     try
     {
        java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat("yyyy-MM-dd");

        if (fieldN.equals("LookupID"))
        {
             this.setLookupID((Integer)fieldV);
        }

        else if (fieldN.equals("UniqueCreateDate"))
        {
            this.setUniqueCreateDate((java.util.Date)fieldV);
        }

        else if (fieldN.equals("UniqueModifyDate"))
        {
            this.setUniqueModifyDate((java.util.Date)fieldV);
        }

        else if (fieldN.equals("UniqueModifyComments"))
        {
            this.setUniqueModifyComments((String)fieldV);
        }

        else if (fieldN.equals("PhysicianID"))
        {
             this.setPhysicianID((Integer)fieldV);
        }

        else if (fieldN.equals("PracticeID"))
        {
             this.setPracticeID((Integer)fieldV);
        }

        else if (fieldN.equals("StartDate"))
        {
            this.setStartDate((java.util.Date)fieldV);
        }

        else if (fieldN.equals("EndDate"))
        {
            this.setEndDate((java.util.Date)fieldV);
        }

        else if (fieldN.equals("isPrimaryOffice"))
        {
             this.setisPrimaryOffice((Integer)fieldV);
        }

        else if (fieldN.equals("isAdministrativeOffice"))
        {
             this.setisAdministrativeOffice((Integer)fieldV);
        }

        else if (fieldN.equals("CoverageHours"))
        {
            this.setCoverageHours((String)fieldV);
        }

        else if (fieldN.equals("Comments"))
        {
            this.setComments((String)fieldV);
        }

	    }
	    catch(Exception e2)
	    {}
	    //return myVal;
	}

    public String getFieldType(String fieldN)
    {
     String  myVal = null;
     try
     {
        java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat("yyyy-MM-dd");

        if (fieldN.equals("LookupID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("UniqueCreateDate"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("UniqueModifyDate"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("UniqueModifyComments"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("PhysicianID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("PracticeID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("StartDate"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("EndDate"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("isPrimaryOffice"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("isAdministrativeOffice"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("CoverageHours"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("Comments"))
        {
            myVal = "String";
        }

	    }
	    catch(Exception e2)
	    {}
	    return myVal;
	}

    public boolean isComplete(String fieldN)
    {
	     boolean myVal = false;
	     try
	     {
        java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat("yyyy-MM-dd");

	     if (fieldN.equals("LookupID"))

	     {
                    if (this.getLookupID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("UniqueCreateDate"))

	     {
                    if (this.getUniqueCreateDate().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("UniqueModifyDate"))

	     {
                    if (this.getUniqueModifyDate().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("UniqueModifyComments"))

	     {
                    if (this.getUniqueModifyComments().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("PhysicianID"))

	     {
                    if (this.getPhysicianID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("PracticeID"))

	     {
                    if (this.getPracticeID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("StartDate"))

	     {
                    if (this.getStartDate().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("EndDate"))

	     {
                    if (this.getEndDate().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("isPrimaryOffice"))

	     {
                    if (this.getisPrimaryOffice().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("isAdministrativeOffice"))

	     {
                    if (this.getisAdministrativeOffice().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("CoverageHours"))

	     {
                    if (this.getCoverageHours().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("Comments"))

	     {
                    if (this.getComments().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	    }
	    catch(Exception e2)
	    {}
	    return myVal;
	}

    public boolean isExpired(String fieldN,int daysVector)
    {
        return isExpired(fieldN,new java.util.Date(),daysVector);
    }

    public boolean isExpired(String fieldN,java.util.Date refDate,int daysVector)
    {
	     boolean myVal = false;
	     try
	     {
        java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat("yyyy-MM-dd");

        if (fieldN.equals("UniqueCreateDate"))

	     {
                    if (this.isExpired(this.getUniqueCreateDate(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

        if (fieldN.equals("UniqueModifyDate"))

	     {
                    if (this.isExpired(this.getUniqueModifyDate(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

        if (fieldN.equals("StartDate"))

	     {
                    if (this.isExpired(this.getStartDate(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

        if (fieldN.equals("EndDate"))

	     {
                    if (this.isExpired(this.getEndDate(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

	    }
	    catch(Exception e2)
	    {}
	    return myVal;
	}

    public java.util.Date getDateField(String fieldN)
    {
	     java.util.Date myVal = null;
	     try
	     {
        java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat("yyyy-MM-dd");

        if (fieldN.equals("UniqueCreateDate"))

	     {
             myVal = this.getUniqueCreateDate();
        }

        if (fieldN.equals("UniqueModifyDate"))

	     {
             myVal = this.getUniqueModifyDate();
        }

        if (fieldN.equals("StartDate"))

	     {
             myVal = this.getStartDate();
        }

        if (fieldN.equals("EndDate"))

	     {
             myVal = this.getEndDate();
        }

	    }
	    catch(Exception e2)
	    {}
	    return myVal;
	}
	public boolean runFieldSecurity = false;
	public Integer UserSecurityID = null;

}    // End of bltPhysicianPracticeLU class definition
