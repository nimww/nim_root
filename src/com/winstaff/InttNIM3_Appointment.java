

package com.winstaff;

import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public interface InttNIM3_Appointment extends dbTableInterface
{

    public void setAppointmentID(Integer newValue);
    public Integer getAppointmentID();
    public void setUniqueCreateDate(Date newValue);
    public Date getUniqueCreateDate();
    public void setUniqueModifyDate(Date newValue);
    public Date getUniqueModifyDate();
    public void setUniqueModifyComments(String newValue);
    public String getUniqueModifyComments();
    public void setProviderID(Integer newValue);
    public Integer getProviderID();
    public void setICID(Integer newValue);
    public Integer getICID();
    public void setiStatus(Integer newValue);
    public Integer getiStatus();
    public void setAppointmentTime(Date newValue);
    public Date getAppointmentTime();
    public void setActualTime(Date newValue);
    public Date getActualTime();
    public void setAppointmentConfirmation(String newValue);
    public String getAppointmentConfirmation();
    public void setScheduler(String newValue);
    public String getScheduler();
    public void setInitialEncounterID(Integer newValue);
    public Integer getInitialEncounterID();
    public void setScheduler_UserID(Integer newValue);
    public Integer getScheduler_UserID();
    public void setProviderConfirmSet(Integer newValue);
    public Integer getProviderConfirmSet();
    public void setProviderConfirmSet_UserID(Integer newValue);
    public Integer getProviderConfirmSet_UserID();
    public void setProviderConfirmDate(Date newValue);
    public Date getProviderConfirmDate();
    public void setProviderAppointmentConfirmation(String newValue);
    public String getProviderAppointmentConfirmation();
    public void setProviderConfirmShow(Integer newValue);
    public Integer getProviderConfirmShow();
    public void setProviderConfirmShow_UserID(Integer newValue);
    public Integer getProviderConfirmShow_UserID();
    public void setProviderConfirmShowDate(Date newValue);
    public Date getProviderConfirmShowDate();
    public void setProviderNotes(String newValue);
    public String getProviderNotes();
    public void setVoidNotes(String newValue);
    public String getVoidNotes();
    public void setPatientConfirmSet(Integer newValue);
    public Integer getPatientConfirmSet();
    public void setPatientConfirmSet_UserID(Integer newValue);
    public Integer getPatientConfirmSet_UserID();
    public void setPatientConfirmDate(Date newValue);
    public Date getPatientConfirmDate();
    public void setComments(String newValue);
    public String getComments();
}    // End of bltNIM3_Appointment class definition
