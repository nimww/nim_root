package com.winstaff;
/*
 * bltPhysicianPracticeLU_List_LU_PracticeID.java
 *
 * Created: Wed Apr 02 15:09:06 PST 2003
 */



import java.sql.*;
import com.winstaff.dbtPhysicianPracticeLU;
import com.winstaff.ListElement;


/**
 *
 * @author  Scott Ellis via listClassGenCode
 * @version 2.5
 */
public class bltPhysicianPracticeLU_List_LU_PracticeID extends db_BaseRelationTable
{
    /** Default constructor
    */    
    public bltPhysicianPracticeLU_List_LU_PracticeID ( Integer iPracticeID )
    {
        super( "tPhysicianPracticeLU", "LookupID", "LookupID", "PracticeID", iPracticeID );
    }   // End of bltPhysicianPracticeLU_List_LU_PracticeID()
    

    public void setField(String fieldN, Object fieldV)
    {DebugLogger.println("setField Method not implemented yet in classbltPhysicianPracticeLU_List_LU_PracticeID!");}

    public String getFieldType(String fieldN)
    {return null;}

    public bltPhysicianPracticeLU_List_LU_PracticeID ( Integer iPracticeID, String extraWhere, String OrderBy )
    {
        super( "tPhysicianPracticeLU", "LookupID", "LookupID", "PracticeID", iPracticeID, extraWhere,OrderBy );
    }   // End of bltPhysicianPracticeLU_List_LU_PracticeID()
     /** Method pull information from the result set and load into class attributes.
    * @param sFieldList Comma delimited list of fields for which data has been retrieved.
    * @param rs Result set from which to pull the data from.
    * @throws SQLException Passes on SQLException
    */      
    protected void transferData( ResultSet rs ) throws SQLException
    {
        while ( rs.next()  )
        {
            Integer         iValue              = new Integer( rs.getString( sLinkFieldName ) );
            Integer         iLinkRecordID       = new Integer( rs.getString( sUniqueIDFieldName ) );
            Object          oReferenceObject;
            ListElement     leNewElement;
            
            oReferenceObject    = new bltPhysicianPracticeLU( iValue );
            
            leNewElement        = new ListElement( iLinkRecordID, oReferenceObject );
            
            vList.add( leNewElement );

        }
    }   // End of transferData()
}   // End of bltPhysicianPracticeLU_List_LU_PracticeID class

