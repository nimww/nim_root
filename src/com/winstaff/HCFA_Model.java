package com.winstaff;

public class HCFA_Model {
	public String icnpi;
	public String nimnpi;
	public String servicerenderedaddress2;
	public String physiciansupplieraddress2;
	public String risingaddress;
	public String firstgroupid;
	public String datesofservicetodd6;
	public String datesofservicetodd4;
	public String datesofservicetodd5;
	public String authorizedsignature;
	public String federaltaxid;
	public String hospitalizedfrommm;
	public String insureddobmm;
	public String originalrefnum;
	public String dateofinjurymm;
	public String patientaccountnum;
	public String localuse6;
	public String icd2;
	public String icd3;
	public String localuse4;
	public String otherinsureddobmm;
	public String icd4;
	public String localuse5;
	public String localuse2;
	public String modifier3;
	public String modifier4;
	public String localuse3;
	public String datesofservicetomm1;
	public String modifier5;
	public String datesofservicetomm2;
	public String modifier6;
	public String datesofservicetomm3;
	public String icd1;
	public String localuse1;
	public String insuredinsuranceplanname;
	public String datesofservicetoyy6;
	public String datesofservicetoyy5;
	public String modifier1;
	public String datesofservicetoyy4;
	public String modifier2;
	public String unabletoworktodd;
	public String physicianfullname;
	public String patientrelationship;
	public String similarillnessdd;
	public String otherinsureddobdd;
	public String physiciansupplieraddress;
	public String cob4;
	public String cob5;
	public String cob2;
	public String groupnum;
	public String pinnum;
	public String cob3;
	public String cob1;
	public String patientdobdd3;
	public String datesofservicetodd1;
	public String datesofservicetodd3;
	public String datesofservicetodd2;
	public String otherhealthbenefitplan;
	public String Text160;
	public String dianosiscode3;
	public String dianosiscode4;
	public String localusebox;
	public String patientzip;
	public String dianosiscode1;
	public String dianosiscode2;
	public String insuredsex;
	public String amountpaid;
	public String daysorunits6;
	public String datesofservicefromdd4;
	public String datesofservicefromdd3;
	public String datesofservicefromdd6;
	public String dianosiscode5;
	public String dianosiscode6;
	public String datesofservicefromdd5;
	public String insuredphone;
	public String datesofservicefromdd1;
	public String datesofservicefromdd2;
	public String insuredzip;
	public String patientsex;
	public String datesofservicetomm6;
	public String datesofservicetomm5;
	public String unabletoworkfromyy;
	public String datesofservicetomm4;
	public String datesofservicetoyy1;
	public String datesofservicetoyy2;
	public String patientphone;
	public String datesofservicetoyy3;
	public String emg2;
	public String placestate;
	public String emg1;
	public String patientsignaturedate;
	public String emg6;
	public String emg5;
	public String emg4;
	public String emg3;
	public String patientdobmm;
	public String patientcity;
	public String similarillnessyy;
	public String priorauthnum;
	public String daysorunits1;
	public String daysorunits2;
	public String signature31;
	public String daysorunits3;
	public String dobinsuredyy;
	public String daysorunits4;
	public String acceptassignment;
	public String patientconditionauto;
	public String datesofservicefrommm6;
	public String patientconditionemployment;
	public String datesofservicefrommm5;
	public String datesofservicefrommm4;
	public String datesofservicefrommm3;
	public String dateofinjuryyy;
	public String insuredcity;
	public String ssn;
	public String placeofservice1;
	public String placeofservice2;
	public String dobinsureddd;
	public String Text128;
	public String Text129;
	public String Text126;
	public String epsdtfamplan2;
	public String Text127;
	public String epsdtfamplan1;
	public String epsdtfamplan3;
	public String epsdtfamplan4;
	public String patientemploymentstatus;
	public String hospitalizedfromyy;
	public String epsdtfamplan5;
	public String epsdtfamplan6;
	public String hospitalizedtomm;
	public String datesofservicefrommm1;
	public String datesofservicefrommm2;
	public String dateofinjurydd;
	public String servicerenderedaddress;
	public String Text133;
	public String insuredidnumber;
	public String charges;
	public String Text130;
	public String otherinsuredpolicynum;
	public String Text131;
	public String Text132;
	public String typeofservice2;
	public String hospitalizedtodd;
	public String outsidelab;
	public String typeofservice1;
	public String otherinsuredsex;
	public String insuranceplanname;
	public String madicaidresubmissioncode;
	public String datesofservicefromyy2;
	public String date31;
	public String datesofservicefromyy1;
	public String datesofservicefromyy3;
	public String datesofservicefromyy4;
	public String datesofservicefromyy5;
	public String datesofservicefromyy6;
	public String patientsignature;
	public String otherinsuredyy;
	public String patientaddress;
	public String balancedue;
	public String medicareradio;
	public String cob6;
	public String hospitalizedfromdd;
	public String insuredaddress;
	public String insurancepolicynum;
	public String insuredemployername;
	public String unabletoworkfrommm;
	public String unabletoworktoyy;
	public String unabletoworktomm;
	public String unabletoworkfromdd;
	public String similarillnessmm;
	public String patientdobyy3;
	public String patientconditionother;
	public String physicianidnum;
	public String reservedlocaluse;
	public String insuredfullname;
	public String patientfullname;
	public String patientstate;
	public String charges2;
	public String otherinsuredfullname;
	public String charges1;
	public String charges6;
	public String charges5;
	public String charges4;
	public String totalcharge;
	public String charges3;
	public String employerschoolname;
	public String cpt1;
	public String cpt2;
	public String insuredstate;
	public String cpt5;
	public String cpt6;
	public String cpt3;
	public String cpt4;
	public String patientrelationshipstatus;
	public String hospitalizedtoyy;
	
	
	public HCFA_Model(String servicerenderedaddress2, String physiciansupplieraddress2, String risingaddress, String firstgroupid, String icnpi, String nimnpi,
			String datesofservicetodd6, String datesofservicetodd4, 
			String datesofservicetodd5, String authorizedsignature,
			String federaltaxid, String hospitalizedfrommm,
			String insureddobmm, String originalrefnum, String dateofinjurymm,
			String patientaccountnum, String localuse6, String icd2,
			String icd3, String localuse4, String otherinsureddobmm,
			String icd4, String localuse5, String localuse2, String modifier3,
			String modifier4, String localuse3, String datesofservicetomm1,
			String modifier5, String datesofservicetomm2, String modifier6,
			String datesofservicetomm3, String icd1, String localuse1,
			String insuredinsuranceplanname, String datesofservicetoyy6,
			String datesofservicetoyy5, String modifier1,
			String datesofservicetoyy4, String modifier2,
			String unabletoworktodd, String physicianfullname,
			String patientrelationship, String similarillnessdd,
			String otherinsureddobdd, String physiciansupplieraddress,
			String cob4, String cob5, String cob2, String groupnum,
			String pinnum, String cob3, String cob1, String patientdobdd3,
			String datesofservicetodd1, String datesofservicetodd3,
			String datesofservicetodd2, String otherhealthbenefitplan,
			String text160, String dianosiscode3, String dianosiscode4,
			String localusebox, String patientzip, String dianosiscode1,
			String dianosiscode2, String insuredsex, String amountpaid,
			String daysorunits6, String datesofservicefromdd4,
			String datesofservicefromdd3, String datesofservicefromdd6,
			String dianosiscode5, String dianosiscode6,
			String datesofservicefromdd5, String insuredphone,
			String datesofservicefromdd1, String datesofservicefromdd2,
			String insuredzip, String patientsex, String datesofservicetomm6,
			String datesofservicetomm5, String unabletoworkfromyy,
			String datesofservicetomm4, String datesofservicetoyy1,
			String datesofservicetoyy2, String patientphone,
			String datesofservicetoyy3, String emg2, String placestate,
			String emg1, String patientsignaturedate, String emg6, String emg5,
			String emg4, String emg3, String patientdobmm, String patientcity,
			String similarillnessyy, String priorauthnum, String daysorunits1,
			String daysorunits2, String signature31, String daysorunits3,
			String dobinsuredyy, String daysorunits4, String acceptassignment,
			String patientconditionauto, String datesofservicefrommm6,
			String patientconditionemployment, String datesofservicefrommm5,
			String datesofservicefrommm4, String datesofservicefrommm3,
			String dateofinjuryyy, String insuredcity, String ssn,
			String placeofservice1, String placeofservice2,
			String dobinsureddd, String text128, String text129,
			String text126, String epsdtfamplan2, String text127,
			String epsdtfamplan1, String epsdtfamplan3, String epsdtfamplan4,
			String patientemploymentstatus, String hospitalizedfromyy,
			String epsdtfamplan5, String epsdtfamplan6,
			String hospitalizedtomm, String datesofservicefrommm1,
			String datesofservicefrommm2, String dateofinjurydd,
			String servicerenderedaddress, String text133,
			String insuredidnumber, String charges, String text130,
			String otherinsuredpolicynum, String text131, String text132,
			String typeofservice2, String hospitalizedtodd, String outsidelab,
			String typeofservice1, String otherinsuredsex,
			String insuranceplanname, String madicaidresubmissioncode,
			String datesofservicefromyy2, String date31,
			String datesofservicefromyy1, String datesofservicefromyy3,
			String datesofservicefromyy4, String datesofservicefromyy5,
			String datesofservicefromyy6, String patientsignature,
			String otherinsuredyy, String patientaddress, String balancedue,
			String medicareradio, String cob6, String hospitalizedfromdd,
			String insuredaddress, String insurancepolicynum,
			String insuredemployername, String unabletoworkfrommm,
			String unabletoworktoyy, String unabletoworktomm,
			String unabletoworkfromdd, String similarillnessmm,
			String patientdobyy3, String patientconditionother,
			String physicianidnum, String reservedlocaluse,
			String insuredfullname, String patientfullname,
			String patientstate, String charges2, String otherinsuredfullname,
			String charges1, String charges6, String charges5, String charges4,
			String totalcharge, String charges3, String employerschoolname,
			String cpt1, String cpt2, String insuredstate, String cpt5,
			String cpt6, String cpt3, String cpt4,
			String patientrelationshipstatus, String hospitalizedtoyy) {
		super();
		this.icnpi = icnpi;
		this.nimnpi = nimnpi;
		this.physiciansupplieraddress2 = physiciansupplieraddress2;
		this.servicerenderedaddress2 = servicerenderedaddress2;
		this.risingaddress = risingaddress;
		this.firstgroupid = firstgroupid;
		this.datesofservicetodd6 = datesofservicetodd6;
		this.datesofservicetodd4 = datesofservicetodd4;
		this.datesofservicetodd5 = datesofservicetodd5;
		this.authorizedsignature = authorizedsignature;
		this.federaltaxid = federaltaxid;
		this.hospitalizedfrommm = hospitalizedfrommm;
		this.insureddobmm = insureddobmm;
		this.originalrefnum = originalrefnum;
		this.dateofinjurymm = dateofinjurymm;
		this.patientaccountnum = patientaccountnum;
		this.localuse6 = localuse6;
		this.icd2 = icd2;
		this.icd3 = icd3;
		this.localuse4 = localuse4;
		this.otherinsureddobmm = otherinsureddobmm;
		this.icd4 = icd4;
		this.localuse5 = localuse5;
		this.localuse2 = localuse2;
		this.modifier3 = modifier3;
		this.modifier4 = modifier4;
		this.localuse3 = localuse3;
		this.datesofservicetomm1 = datesofservicetomm1;
		this.modifier5 = modifier5;
		this.datesofservicetomm2 = datesofservicetomm2;
		this.modifier6 = modifier6;
		this.datesofservicetomm3 = datesofservicetomm3;
		this.icd1 = icd1;
		this.localuse1 = localuse1;
		this.insuredinsuranceplanname = insuredinsuranceplanname;
		this.datesofservicetoyy6 = datesofservicetoyy6;
		this.datesofservicetoyy5 = datesofservicetoyy5;
		this.modifier1 = modifier1;
		this.datesofservicetoyy4 = datesofservicetoyy4;
		this.modifier2 = modifier2;
		this.unabletoworktodd = unabletoworktodd;
		this.physicianfullname = physicianfullname;
		this.patientrelationship = patientrelationship;
		this.similarillnessdd = similarillnessdd;
		this.otherinsureddobdd = otherinsureddobdd;
		this.physiciansupplieraddress = physiciansupplieraddress;
		this.cob4 = cob4;
		this.cob5 = cob5;
		this.cob2 = cob2;
		this.groupnum = groupnum;
		this.pinnum = pinnum;
		this.cob3 = cob3;
		this.cob1 = cob1;
		this.patientdobdd3 = patientdobdd3;
		this.datesofservicetodd1 = datesofservicetodd1;
		this.datesofservicetodd3 = datesofservicetodd3;
		this.datesofservicetodd2 = datesofservicetodd2;
		this.otherhealthbenefitplan = otherhealthbenefitplan;
		Text160 = text160;
		this.dianosiscode3 = dianosiscode3;
		this.dianosiscode4 = dianosiscode4;
		this.localusebox = localusebox;
		this.patientzip = patientzip;
		this.dianosiscode1 = dianosiscode1;
		this.dianosiscode2 = dianosiscode2;
		this.insuredsex = insuredsex;
		this.amountpaid = amountpaid;
		this.daysorunits6 = daysorunits6;
		this.datesofservicefromdd4 = datesofservicefromdd4;
		this.datesofservicefromdd3 = datesofservicefromdd3;
		this.datesofservicefromdd6 = datesofservicefromdd6;
		this.dianosiscode5 = dianosiscode5;
		this.dianosiscode6 = dianosiscode6;
		this.datesofservicefromdd5 = datesofservicefromdd5;
		this.insuredphone = insuredphone;
		this.datesofservicefromdd1 = datesofservicefromdd1;
		this.datesofservicefromdd2 = datesofservicefromdd2;
		this.insuredzip = insuredzip;
		this.patientsex = patientsex;
		this.datesofservicetomm6 = datesofservicetomm6;
		this.datesofservicetomm5 = datesofservicetomm5;
		this.unabletoworkfromyy = unabletoworkfromyy;
		this.datesofservicetomm4 = datesofservicetomm4;
		this.datesofservicetoyy1 = datesofservicetoyy1;
		this.datesofservicetoyy2 = datesofservicetoyy2;
		this.patientphone = patientphone;
		this.datesofservicetoyy3 = datesofservicetoyy3;
		this.emg2 = emg2;
		this.placestate = placestate;
		this.emg1 = emg1;
		this.patientsignaturedate = patientsignaturedate;
		this.emg6 = emg6;
		this.emg5 = emg5;
		this.emg4 = emg4;
		this.emg3 = emg3;
		this.patientdobmm = patientdobmm;
		this.patientcity = patientcity;
		this.similarillnessyy = similarillnessyy;
		this.priorauthnum = priorauthnum;
		this.daysorunits1 = daysorunits1;
		this.daysorunits2 = daysorunits2;
		this.signature31 = signature31;
		this.daysorunits3 = daysorunits3;
		this.dobinsuredyy = dobinsuredyy;
		this.daysorunits4 = daysorunits4;
		this.acceptassignment = acceptassignment;
		this.patientconditionauto = patientconditionauto;
		this.datesofservicefrommm6 = datesofservicefrommm6;
		this.patientconditionemployment = patientconditionemployment;
		this.datesofservicefrommm5 = datesofservicefrommm5;
		this.datesofservicefrommm4 = datesofservicefrommm4;
		this.datesofservicefrommm3 = datesofservicefrommm3;
		this.dateofinjuryyy = dateofinjuryyy;
		this.insuredcity = insuredcity;
		this.ssn = ssn;
		this.placeofservice1 = placeofservice1;
		this.placeofservice2 = placeofservice2;
		this.dobinsureddd = dobinsureddd;
		Text128 = text128;
		Text129 = text129;
		Text126 = text126;
		this.epsdtfamplan2 = epsdtfamplan2;
		Text127 = text127;
		this.epsdtfamplan1 = epsdtfamplan1;
		this.epsdtfamplan3 = epsdtfamplan3;
		this.epsdtfamplan4 = epsdtfamplan4;
		this.patientemploymentstatus = patientemploymentstatus;
		this.hospitalizedfromyy = hospitalizedfromyy;
		this.epsdtfamplan5 = epsdtfamplan5;
		this.epsdtfamplan6 = epsdtfamplan6;
		this.hospitalizedtomm = hospitalizedtomm;
		this.datesofservicefrommm1 = datesofservicefrommm1;
		this.datesofservicefrommm2 = datesofservicefrommm2;
		this.dateofinjurydd = dateofinjurydd;
		this.servicerenderedaddress = servicerenderedaddress;
		Text133 = text133;
		this.insuredidnumber = insuredidnumber;
		this.charges = charges;
		Text130 = text130;
		this.otherinsuredpolicynum = otherinsuredpolicynum;
		Text131 = text131;
		Text132 = text132;
		this.typeofservice2 = typeofservice2;
		this.hospitalizedtodd = hospitalizedtodd;
		this.outsidelab = outsidelab;
		this.typeofservice1 = typeofservice1;
		this.otherinsuredsex = otherinsuredsex;
		this.insuranceplanname = insuranceplanname;
		this.madicaidresubmissioncode = madicaidresubmissioncode;
		this.datesofservicefromyy2 = datesofservicefromyy2;
		this.date31 = date31;
		this.datesofservicefromyy1 = datesofservicefromyy1;
		this.datesofservicefromyy3 = datesofservicefromyy3;
		this.datesofservicefromyy4 = datesofservicefromyy4;
		this.datesofservicefromyy5 = datesofservicefromyy5;
		this.datesofservicefromyy6 = datesofservicefromyy6;
		this.patientsignature = patientsignature;
		this.otherinsuredyy = otherinsuredyy;
		this.patientaddress = patientaddress;
		this.balancedue = balancedue;
		this.medicareradio = medicareradio;
		this.cob6 = cob6;
		this.hospitalizedfromdd = hospitalizedfromdd;
		this.insuredaddress = insuredaddress;
		this.insurancepolicynum = insurancepolicynum;
		this.insuredemployername = insuredemployername;
		this.unabletoworkfrommm = unabletoworkfrommm;
		this.unabletoworktoyy = unabletoworktoyy;
		this.unabletoworktomm = unabletoworktomm;
		this.unabletoworkfromdd = unabletoworkfromdd;
		this.similarillnessmm = similarillnessmm;
		this.patientdobyy3 = patientdobyy3;
		this.patientconditionother = patientconditionother;
		this.physicianidnum = physicianidnum;
		this.reservedlocaluse = reservedlocaluse;
		this.insuredfullname = insuredfullname;
		this.patientfullname = patientfullname;
		this.patientstate = patientstate;
		this.charges2 = charges2;
		this.otherinsuredfullname = otherinsuredfullname;
		this.charges1 = charges1;
		this.charges6 = charges6;
		this.charges5 = charges5;
		this.charges4 = charges4;
		this.totalcharge = totalcharge;
		this.charges3 = charges3;
		this.employerschoolname = employerschoolname;
		this.cpt1 = cpt1;
		this.cpt2 = cpt2;
		this.insuredstate = insuredstate;
		this.cpt5 = cpt5;
		this.cpt6 = cpt6;
		this.cpt3 = cpt3;
		this.cpt4 = cpt4;
		this.patientrelationshipstatus = patientrelationshipstatus;
		this.hospitalizedtoyy = hospitalizedtoyy;
	}
	

	public HCFA_Model() {
		// TODO Auto-generated constructor stub
	}
	public String getIcnpi() {
		return icnpi;
	}
	public void setIcnpi(String icnpi) {
		this.icnpi = icnpi;
	}
	public String getNimnpi() {
		return nimnpi;
	}
	public void setNimnpi(String nimnpi) {
		this.nimnpi = nimnpi;
	}
	public String getServicerenderedaddress2() {
		return servicerenderedaddress2;
	}
	public void setServicerenderedaddress2(String servicerenderedaddress2) {
		this.servicerenderedaddress2 = servicerenderedaddress2;
	}

	public String getPhysiciansupplieraddress2() {
		return physiciansupplieraddress2;
	}

	public void setPhysiciansupplieraddress2(String physiciansupplieraddress2) {
		this.physiciansupplieraddress2 = physiciansupplieraddress2;
	}

	public String getRisingaddress() {
		return risingaddress;
	}

	public void setRisingaddress(String risingaddress) {
		this.risingaddress = risingaddress;
	}

	public String getFirstgroupid() {
		return firstgroupid;
	}
	public void setFirstgroupid(String firstgroupid) {
		this.firstgroupid = firstgroupid;
	}
	public String getDatesofservicetodd6() {
		return datesofservicetodd6;
	}
	public void setDatesofservicetodd6(String datesofservicetodd6) {
		this.datesofservicetodd6 = datesofservicetodd6;
	}
	public String getDatesofservicetodd4() {
		return datesofservicetodd4;
	}
	public void setDatesofservicetodd4(String datesofservicetodd4) {
		this.datesofservicetodd4 = datesofservicetodd4;
	}
	public String getDatesofservicetodd5() {
		return datesofservicetodd5;
	}
	public void setDatesofservicetodd5(String datesofservicetodd5) {
		this.datesofservicetodd5 = datesofservicetodd5;
	}
	public String getAuthorizedsignature() {
		return authorizedsignature;
	}
	public void setAuthorizedsignature(String authorizedsignature) {
		this.authorizedsignature = authorizedsignature;
	}
	public String getFederaltaxid() {
		return federaltaxid;
	}
	public void setFederaltaxid(String federaltaxid) {
		this.federaltaxid = federaltaxid;
	}
	public String getHospitalizedfrommm() {
		return hospitalizedfrommm;
	}
	public void setHospitalizedfrommm(String hospitalizedfrommm) {
		this.hospitalizedfrommm = hospitalizedfrommm;
	}
	public String getInsureddobmm() {
		return insureddobmm;
	}
	public void setInsureddobmm(String insureddobmm) {
		this.insureddobmm = insureddobmm;
	}
	public String getOriginalrefnum() {
		return originalrefnum;
	}
	public void setOriginalrefnum(String originalrefnum) {
		this.originalrefnum = originalrefnum;
	}
	public String getDateofinjurymm() {
		return dateofinjurymm;
	}
	public void setDateofinjurymm(String dateofinjurymm) {
		this.dateofinjurymm = dateofinjurymm;
	}
	public String getPatientaccountnum() {
		return patientaccountnum;
	}
	public void setPatientaccountnum(String patientaccountnum) {
		this.patientaccountnum = patientaccountnum;
	}
	public String getLocaluse6() {
		return localuse6;
	}
	public void setLocaluse6(String localuse6) {
		this.localuse6 = localuse6;
	}
	public String getIcd2() {
		return icd2;
	}
	public void setIcd2(String icd2) {
		this.icd2 = icd2;
	}
	public String getIcd3() {
		return icd3;
	}
	public void setIcd3(String icd3) {
		this.icd3 = icd3;
	}
	public String getLocaluse4() {
		return localuse4;
	}
	public void setLocaluse4(String localuse4) {
		this.localuse4 = localuse4;
	}
	public String getOtherinsureddobmm() {
		return otherinsureddobmm;
	}
	public void setOtherinsureddobmm(String otherinsureddobmm) {
		this.otherinsureddobmm = otherinsureddobmm;
	}
	public String getIcd4() {
		return icd4;
	}
	public void setIcd4(String icd4) {
		this.icd4 = icd4;
	}
	public String getLocaluse5() {
		return localuse5;
	}
	public void setLocaluse5(String localuse5) {
		this.localuse5 = localuse5;
	}
	public String getLocaluse2() {
		return localuse2;
	}
	public void setLocaluse2(String localuse2) {
		this.localuse2 = localuse2;
	}
	public String getModifier3() {
		return modifier3;
	}
	public void setModifier3(String modifier3) {
		this.modifier3 = modifier3;
	}
	public String getModifier4() {
		return modifier4;
	}
	public void setModifier4(String modifier4) {
		this.modifier4 = modifier4;
	}
	public String getLocaluse3() {
		return localuse3;
	}
	public void setLocaluse3(String localuse3) {
		this.localuse3 = localuse3;
	}
	public String getDatesofservicetomm1() {
		return datesofservicetomm1;
	}
	public void setDatesofservicetomm1(String datesofservicetomm1) {
		this.datesofservicetomm1 = datesofservicetomm1;
	}
	public String getModifier5() {
		return modifier5;
	}
	public void setModifier5(String modifier5) {
		this.modifier5 = modifier5;
	}
	public String getDatesofservicetomm2() {
		return datesofservicetomm2;
	}
	public void setDatesofservicetomm2(String datesofservicetomm2) {
		this.datesofservicetomm2 = datesofservicetomm2;
	}
	public String getModifier6() {
		return modifier6;
	}
	public void setModifier6(String modifier6) {
		this.modifier6 = modifier6;
	}
	public String getDatesofservicetomm3() {
		return datesofservicetomm3;
	}
	public void setDatesofservicetomm3(String datesofservicetomm3) {
		this.datesofservicetomm3 = datesofservicetomm3;
	}
	public String getIcd1() {
		return icd1;
	}
	public void setIcd1(String icd1) {
		this.icd1 = icd1;
	}
	public String getLocaluse1() {
		return localuse1;
	}
	public void setLocaluse1(String localuse1) {
		this.localuse1 = localuse1;
	}
	public String getInsuredinsuranceplanname() {
		return insuredinsuranceplanname;
	}
	public void setInsuredinsuranceplanname(String insuredinsuranceplanname) {
		this.insuredinsuranceplanname = insuredinsuranceplanname;
	}
	public String getDatesofservicetoyy6() {
		return datesofservicetoyy6;
	}
	public void setDatesofservicetoyy6(String datesofservicetoyy6) {
		this.datesofservicetoyy6 = datesofservicetoyy6;
	}
	public String getDatesofservicetoyy5() {
		return datesofservicetoyy5;
	}
	public void setDatesofservicetoyy5(String datesofservicetoyy5) {
		this.datesofservicetoyy5 = datesofservicetoyy5;
	}
	public String getModifier1() {
		return modifier1;
	}
	public void setModifier1(String modifier1) {
		this.modifier1 = modifier1;
	}
	public String getDatesofservicetoyy4() {
		return datesofservicetoyy4;
	}
	public void setDatesofservicetoyy4(String datesofservicetoyy4) {
		this.datesofservicetoyy4 = datesofservicetoyy4;
	}
	public String getModifier2() {
		return modifier2;
	}
	public void setModifier2(String modifier2) {
		this.modifier2 = modifier2;
	}
	public String getUnabletoworktodd() {
		return unabletoworktodd;
	}
	public void setUnabletoworktodd(String unabletoworktodd) {
		this.unabletoworktodd = unabletoworktodd;
	}
	public String getPhysicianfullname() {
		return physicianfullname;
	}
	public void setPhysicianfullname(String physicianfullname) {
		this.physicianfullname = physicianfullname;
	}
	public String getPatientrelationship() {
		return patientrelationship;
	}
	public void setPatientrelationship(String patientrelationship) {
		this.patientrelationship = patientrelationship;
	}
	public String getSimilarillnessdd() {
		return similarillnessdd;
	}
	public void setSimilarillnessdd(String similarillnessdd) {
		this.similarillnessdd = similarillnessdd;
	}
	public String getOtherinsureddobdd() {
		return otherinsureddobdd;
	}
	public void setOtherinsureddobdd(String otherinsureddobdd) {
		this.otherinsureddobdd = otherinsureddobdd;
	}
	public String getPhysiciansupplieraddress() {
		return physiciansupplieraddress;
	}
	public void setPhysiciansupplieraddress(String physiciansupplieraddress) {
		this.physiciansupplieraddress = physiciansupplieraddress;
	}
	public String getCob4() {
		return cob4;
	}
	public void setCob4(String cob4) {
		this.cob4 = cob4;
	}
	public String getCob5() {
		return cob5;
	}
	public void setCob5(String cob5) {
		this.cob5 = cob5;
	}
	public String getCob2() {
		return cob2;
	}
	public void setCob2(String cob2) {
		this.cob2 = cob2;
	}
	public String getGroupnum() {
		return groupnum;
	}
	public void setGroupnum(String groupnum) {
		this.groupnum = groupnum;
	}
	public String getPinnum() {
		return pinnum;
	}
	public void setPinnum(String pinnum) {
		this.pinnum = pinnum;
	}
	public String getCob3() {
		return cob3;
	}
	public void setCob3(String cob3) {
		this.cob3 = cob3;
	}
	public String getCob1() {
		return cob1;
	}
	public void setCob1(String cob1) {
		this.cob1 = cob1;
	}
	public String getPatientdobdd3() {
		return patientdobdd3;
	}
	public void setPatientdobdd3(String patientdobdd3) {
		this.patientdobdd3 = patientdobdd3;
	}
	public String getDatesofservicetodd1() {
		return datesofservicetodd1;
	}
	public void setDatesofservicetodd1(String datesofservicetodd1) {
		this.datesofservicetodd1 = datesofservicetodd1;
	}
	public String getDatesofservicetodd3() {
		return datesofservicetodd3;
	}
	public void setDatesofservicetodd3(String datesofservicetodd3) {
		this.datesofservicetodd3 = datesofservicetodd3;
	}
	public String getDatesofservicetodd2() {
		return datesofservicetodd2;
	}
	public void setDatesofservicetodd2(String datesofservicetodd2) {
		this.datesofservicetodd2 = datesofservicetodd2;
	}
	public String getOtherhealthbenefitplan() {
		return otherhealthbenefitplan;
	}
	public void setOtherhealthbenefitplan(String otherhealthbenefitplan) {
		this.otherhealthbenefitplan = otherhealthbenefitplan;
	}
	public String getText160() {
		return Text160;
	}
	public void setText160(String text160) {
		Text160 = text160;
	}
	public String getDianosiscode3() {
		return dianosiscode3;
	}
	public void setDianosiscode3(String dianosiscode3) {
		this.dianosiscode3 = dianosiscode3;
	}
	public String getDianosiscode4() {
		return dianosiscode4;
	}
	public void setDianosiscode4(String dianosiscode4) {
		this.dianosiscode4 = dianosiscode4;
	}
	public String getLocalusebox() {
		return localusebox;
	}
	public void setLocalusebox(String localusebox) {
		this.localusebox = localusebox;
	}
	public String getPatientzip() {
		return patientzip;
	}
	public void setPatientzip(String patientzip) {
		this.patientzip = patientzip;
	}
	public String getDianosiscode1() {
		return dianosiscode1;
	}
	public void setDianosiscode1(String dianosiscode1) {
		this.dianosiscode1 = dianosiscode1;
	}
	public String getDianosiscode2() {
		return dianosiscode2;
	}
	public void setDianosiscode2(String dianosiscode2) {
		this.dianosiscode2 = dianosiscode2;
	}
	public String getInsuredsex() {
		return insuredsex;
	}
	public void setInsuredsex(String insuredsex) {
		this.insuredsex = insuredsex;
	}
	public String getAmountpaid() {
		return amountpaid;
	}
	public void setAmountpaid(String amountpaid) {
		this.amountpaid = amountpaid;
	}
	public String getDaysorunits6() {
		return daysorunits6;
	}
	public void setDaysorunits6(String daysorunits6) {
		this.daysorunits6 = daysorunits6;
	}
	public String getDatesofservicefromdd4() {
		return datesofservicefromdd4;
	}
	public void setDatesofservicefromdd4(String datesofservicefromdd4) {
		this.datesofservicefromdd4 = datesofservicefromdd4;
	}
	public String getDatesofservicefromdd3() {
		return datesofservicefromdd3;
	}
	public void setDatesofservicefromdd3(String datesofservicefromdd3) {
		this.datesofservicefromdd3 = datesofservicefromdd3;
	}
	public String getDatesofservicefromdd6() {
		return datesofservicefromdd6;
	}
	public void setDatesofservicefromdd6(String datesofservicefromdd6) {
		this.datesofservicefromdd6 = datesofservicefromdd6;
	}
	public String getDianosiscode5() {
		return dianosiscode5;
	}
	public void setDianosiscode5(String dianosiscode5) {
		this.dianosiscode5 = dianosiscode5;
	}
	public String getDianosiscode6() {
		return dianosiscode6;
	}
	public void setDianosiscode6(String dianosiscode6) {
		this.dianosiscode6 = dianosiscode6;
	}
	public String getDatesofservicefromdd5() {
		return datesofservicefromdd5;
	}
	public void setDatesofservicefromdd5(String datesofservicefromdd5) {
		this.datesofservicefromdd5 = datesofservicefromdd5;
	}
	public String getInsuredphone() {
		return insuredphone;
	}
	public void setInsuredphone(String insuredphone) {
		this.insuredphone = insuredphone;
	}
	public String getDatesofservicefromdd1() {
		return datesofservicefromdd1;
	}
	public void setDatesofservicefromdd1(String datesofservicefromdd1) {
		this.datesofservicefromdd1 = datesofservicefromdd1;
	}
	public String getDatesofservicefromdd2() {
		return datesofservicefromdd2;
	}
	public void setDatesofservicefromdd2(String datesofservicefromdd2) {
		this.datesofservicefromdd2 = datesofservicefromdd2;
	}
	public String getInsuredzip() {
		return insuredzip;
	}
	public void setInsuredzip(String insuredzip) {
		this.insuredzip = insuredzip;
	}
	public String getPatientsex() {
		return patientsex;
	}
	public void setPatientsex(String patientsex) {
		this.patientsex = patientsex;
	}
	public String getDatesofservicetomm6() {
		return datesofservicetomm6;
	}
	public void setDatesofservicetomm6(String datesofservicetomm6) {
		this.datesofservicetomm6 = datesofservicetomm6;
	}
	public String getDatesofservicetomm5() {
		return datesofservicetomm5;
	}
	public void setDatesofservicetomm5(String datesofservicetomm5) {
		this.datesofservicetomm5 = datesofservicetomm5;
	}
	public String getUnabletoworkfromyy() {
		return unabletoworkfromyy;
	}
	public void setUnabletoworkfromyy(String unabletoworkfromyy) {
		this.unabletoworkfromyy = unabletoworkfromyy;
	}
	public String getDatesofservicetomm4() {
		return datesofservicetomm4;
	}
	public void setDatesofservicetomm4(String datesofservicetomm4) {
		this.datesofservicetomm4 = datesofservicetomm4;
	}
	public String getDatesofservicetoyy1() {
		return datesofservicetoyy1;
	}
	public void setDatesofservicetoyy1(String datesofservicetoyy1) {
		this.datesofservicetoyy1 = datesofservicetoyy1;
	}
	public String getDatesofservicetoyy2() {
		return datesofservicetoyy2;
	}
	public void setDatesofservicetoyy2(String datesofservicetoyy2) {
		this.datesofservicetoyy2 = datesofservicetoyy2;
	}
	public String getPatientphone() {
		return patientphone;
	}
	public void setPatientphone(String patientphone) {
		this.patientphone = patientphone;
	}
	public String getDatesofservicetoyy3() {
		return datesofservicetoyy3;
	}
	public void setDatesofservicetoyy3(String datesofservicetoyy3) {
		this.datesofservicetoyy3 = datesofservicetoyy3;
	}
	public String getEmg2() {
		return emg2;
	}
	public void setEmg2(String emg2) {
		this.emg2 = emg2;
	}
	public String getPlacestate() {
		return placestate;
	}
	public void setPlacestate(String placestate) {
		this.placestate = placestate;
	}
	public String getEmg1() {
		return emg1;
	}
	public void setEmg1(String emg1) {
		this.emg1 = emg1;
	}
	public String getPatientsignaturedate() {
		return patientsignaturedate;
	}
	public void setPatientsignaturedate(String patientsignaturedate) {
		this.patientsignaturedate = patientsignaturedate;
	}
	public String getEmg6() {
		return emg6;
	}
	public void setEmg6(String emg6) {
		this.emg6 = emg6;
	}
	public String getEmg5() {
		return emg5;
	}
	public void setEmg5(String emg5) {
		this.emg5 = emg5;
	}
	public String getEmg4() {
		return emg4;
	}
	public void setEmg4(String emg4) {
		this.emg4 = emg4;
	}
	public String getEmg3() {
		return emg3;
	}
	public void setEmg3(String emg3) {
		this.emg3 = emg3;
	}
	public String getPatientdobmm() {
		return patientdobmm;
	}
	public void setPatientdobmm(String patientdobmm) {
		this.patientdobmm = patientdobmm;
	}
	public String getPatientcity() {
		return patientcity;
	}
	public void setPatientcity(String patientcity) {
		this.patientcity = patientcity;
	}
	public String getSimilarillnessyy() {
		return similarillnessyy;
	}
	public void setSimilarillnessyy(String similarillnessyy) {
		this.similarillnessyy = similarillnessyy;
	}
	public String getPriorauthnum() {
		return priorauthnum;
	}
	public void setPriorauthnum(String priorauthnum) {
		this.priorauthnum = priorauthnum;
	}
	public String getDaysorunits1() {
		return daysorunits1;
	}
	public void setDaysorunits1(String daysorunits1) {
		this.daysorunits1 = daysorunits1;
	}
	public String getDaysorunits2() {
		return daysorunits2;
	}
	public void setDaysorunits2(String daysorunits2) {
		this.daysorunits2 = daysorunits2;
	}
	public String getSignature31() {
		return signature31;
	}
	public void setSignature31(String signature31) {
		this.signature31 = signature31;
	}
	public String getDaysorunits3() {
		return daysorunits3;
	}
	public void setDaysorunits3(String daysorunits3) {
		this.daysorunits3 = daysorunits3;
	}
	public String getDobinsuredyy() {
		return dobinsuredyy;
	}
	public void setDobinsuredyy(String dobinsuredyy) {
		this.dobinsuredyy = dobinsuredyy;
	}
	public String getDaysorunits4() {
		return daysorunits4;
	}
	public void setDaysorunits4(String daysorunits4) {
		this.daysorunits4 = daysorunits4;
	}
	public String getAcceptassignment() {
		return acceptassignment;
	}
	public void setAcceptassignment(String acceptassignment) {
		this.acceptassignment = acceptassignment;
	}
	public String getPatientconditionauto() {
		return patientconditionauto;
	}
	public void setPatientconditionauto(String patientconditionauto) {
		this.patientconditionauto = patientconditionauto;
	}
	public String getDatesofservicefrommm6() {
		return datesofservicefrommm6;
	}
	public void setDatesofservicefrommm6(String datesofservicefrommm6) {
		this.datesofservicefrommm6 = datesofservicefrommm6;
	}
	public String getPatientconditionemployment() {
		return patientconditionemployment;
	}
	public void setPatientconditionemployment(String patientconditionemployment) {
		this.patientconditionemployment = patientconditionemployment;
	}
	public String getDatesofservicefrommm5() {
		return datesofservicefrommm5;
	}
	public void setDatesofservicefrommm5(String datesofservicefrommm5) {
		this.datesofservicefrommm5 = datesofservicefrommm5;
	}
	public String getDatesofservicefrommm4() {
		return datesofservicefrommm4;
	}
	public void setDatesofservicefrommm4(String datesofservicefrommm4) {
		this.datesofservicefrommm4 = datesofservicefrommm4;
	}
	public String getDatesofservicefrommm3() {
		return datesofservicefrommm3;
	}
	public void setDatesofservicefrommm3(String datesofservicefrommm3) {
		this.datesofservicefrommm3 = datesofservicefrommm3;
	}
	public String getDateofinjuryyy() {
		return dateofinjuryyy;
	}
	public void setDateofinjuryyy(String dateofinjuryyy) {
		this.dateofinjuryyy = dateofinjuryyy;
	}
	public String getInsuredcity() {
		return insuredcity;
	}
	public void setInsuredcity(String insuredcity) {
		this.insuredcity = insuredcity;
	}
	public String getSsn() {
		return ssn;
	}
	public void setSsn(String ssn) {
		this.ssn = ssn;
	}
	public String getPlaceofservice1() {
		return placeofservice1;
	}
	public void setPlaceofservice1(String placeofservice1) {
		this.placeofservice1 = placeofservice1;
	}
	public String getPlaceofservice2() {
		return placeofservice2;
	}
	public void setPlaceofservice2(String placeofservice2) {
		this.placeofservice2 = placeofservice2;
	}
	public String getDobinsureddd() {
		return dobinsureddd;
	}
	public void setDobinsureddd(String dobinsureddd) {
		this.dobinsureddd = dobinsureddd;
	}
	public String getText128() {
		return Text128;
	}
	public void setText128(String text128) {
		Text128 = text128;
	}
	public String getText129() {
		return Text129;
	}
	public void setText129(String text129) {
		Text129 = text129;
	}
	public String getText126() {
		return Text126;
	}
	public void setText126(String text126) {
		Text126 = text126;
	}
	public String getEpsdtfamplan2() {
		return epsdtfamplan2;
	}
	public void setEpsdtfamplan2(String epsdtfamplan2) {
		this.epsdtfamplan2 = epsdtfamplan2;
	}
	public String getText127() {
		return Text127;
	}
	public void setText127(String text127) {
		Text127 = text127;
	}
	public String getEpsdtfamplan1() {
		return epsdtfamplan1;
	}
	public void setEpsdtfamplan1(String epsdtfamplan1) {
		this.epsdtfamplan1 = epsdtfamplan1;
	}
	public String getEpsdtfamplan3() {
		return epsdtfamplan3;
	}
	public void setEpsdtfamplan3(String epsdtfamplan3) {
		this.epsdtfamplan3 = epsdtfamplan3;
	}
	public String getEpsdtfamplan4() {
		return epsdtfamplan4;
	}
	public void setEpsdtfamplan4(String epsdtfamplan4) {
		this.epsdtfamplan4 = epsdtfamplan4;
	}
	public String getPatientemploymentstatus() {
		return patientemploymentstatus;
	}
	public void setPatientemploymentstatus(String patientemploymentstatus) {
		this.patientemploymentstatus = patientemploymentstatus;
	}
	public String getHospitalizedfromyy() {
		return hospitalizedfromyy;
	}
	public void setHospitalizedfromyy(String hospitalizedfromyy) {
		this.hospitalizedfromyy = hospitalizedfromyy;
	}
	public String getEpsdtfamplan5() {
		return epsdtfamplan5;
	}
	public void setEpsdtfamplan5(String epsdtfamplan5) {
		this.epsdtfamplan5 = epsdtfamplan5;
	}
	public String getEpsdtfamplan6() {
		return epsdtfamplan6;
	}
	public void setEpsdtfamplan6(String epsdtfamplan6) {
		this.epsdtfamplan6 = epsdtfamplan6;
	}
	public String getHospitalizedtomm() {
		return hospitalizedtomm;
	}
	public void setHospitalizedtomm(String hospitalizedtomm) {
		this.hospitalizedtomm = hospitalizedtomm;
	}
	public String getDatesofservicefrommm1() {
		return datesofservicefrommm1;
	}
	public void setDatesofservicefrommm1(String datesofservicefrommm1) {
		this.datesofservicefrommm1 = datesofservicefrommm1;
	}
	public String getDatesofservicefrommm2() {
		return datesofservicefrommm2;
	}
	public void setDatesofservicefrommm2(String datesofservicefrommm2) {
		this.datesofservicefrommm2 = datesofservicefrommm2;
	}
	public String getDateofinjurydd() {
		return dateofinjurydd;
	}
	public void setDateofinjurydd(String dateofinjurydd) {
		this.dateofinjurydd = dateofinjurydd;
	}
	public String getServicerenderedaddress() {
		return servicerenderedaddress;
	}
	public void setServicerenderedaddress(String servicerenderedaddress) {
		this.servicerenderedaddress = servicerenderedaddress;
	}
	public String getText133() {
		return Text133;
	}
	public void setText133(String text133) {
		Text133 = text133;
	}
	public String getInsuredidnumber() {
		return insuredidnumber;
	}
	public void setInsuredidnumber(String insuredidnumber) {
		this.insuredidnumber = insuredidnumber;
	}
	public String getCharges() {
		return charges;
	}
	public void setCharges(String charges) {
		this.charges = charges;
	}
	public String getText130() {
		return Text130;
	}
	public void setText130(String text130) {
		Text130 = text130;
	}
	public String getOtherinsuredpolicynum() {
		return otherinsuredpolicynum;
	}
	public void setOtherinsuredpolicynum(String otherinsuredpolicynum) {
		this.otherinsuredpolicynum = otherinsuredpolicynum;
	}
	public String getText131() {
		return Text131;
	}
	public void setText131(String text131) {
		Text131 = text131;
	}
	public String getText132() {
		return Text132;
	}
	public void setText132(String text132) {
		Text132 = text132;
	}
	public String getTypeofservice2() {
		return typeofservice2;
	}
	public void setTypeofservice2(String typeofservice2) {
		this.typeofservice2 = typeofservice2;
	}
	public String getHospitalizedtodd() {
		return hospitalizedtodd;
	}
	public void setHospitalizedtodd(String hospitalizedtodd) {
		this.hospitalizedtodd = hospitalizedtodd;
	}
	public String getOutsidelab() {
		return outsidelab;
	}
	public void setOutsidelab(String outsidelab) {
		this.outsidelab = outsidelab;
	}
	public String getTypeofservice1() {
		return typeofservice1;
	}
	public void setTypeofservice1(String typeofservice1) {
		this.typeofservice1 = typeofservice1;
	}
	public String getOtherinsuredsex() {
		return otherinsuredsex;
	}
	public void setOtherinsuredsex(String otherinsuredsex) {
		this.otherinsuredsex = otherinsuredsex;
	}
	public String getInsuranceplanname() {
		return insuranceplanname;
	}
	public void setInsuranceplanname(String insuranceplanname) {
		this.insuranceplanname = insuranceplanname;
	}
	public String getMadicaidresubmissioncode() {
		return madicaidresubmissioncode;
	}
	public void setMadicaidresubmissioncode(String madicaidresubmissioncode) {
		this.madicaidresubmissioncode = madicaidresubmissioncode;
	}
	public String getDatesofservicefromyy2() {
		return datesofservicefromyy2;
	}
	public void setDatesofservicefromyy2(String datesofservicefromyy2) {
		this.datesofservicefromyy2 = datesofservicefromyy2;
	}
	public String getDate31() {
		return date31;
	}
	public void setDate31(String date31) {
		this.date31 = date31;
	}
	public String getDatesofservicefromyy1() {
		return datesofservicefromyy1;
	}
	public void setDatesofservicefromyy1(String datesofservicefromyy1) {
		this.datesofservicefromyy1 = datesofservicefromyy1;
	}
	public String getDatesofservicefromyy3() {
		return datesofservicefromyy3;
	}
	public void setDatesofservicefromyy3(String datesofservicefromyy3) {
		this.datesofservicefromyy3 = datesofservicefromyy3;
	}
	public String getDatesofservicefromyy4() {
		return datesofservicefromyy4;
	}
	public void setDatesofservicefromyy4(String datesofservicefromyy4) {
		this.datesofservicefromyy4 = datesofservicefromyy4;
	}
	public String getDatesofservicefromyy5() {
		return datesofservicefromyy5;
	}
	public void setDatesofservicefromyy5(String datesofservicefromyy5) {
		this.datesofservicefromyy5 = datesofservicefromyy5;
	}
	public String getDatesofservicefromyy6() {
		return datesofservicefromyy6;
	}
	public void setDatesofservicefromyy6(String datesofservicefromyy6) {
		this.datesofservicefromyy6 = datesofservicefromyy6;
	}
	public String getPatientsignature() {
		return patientsignature;
	}
	public void setPatientsignature(String patientsignature) {
		this.patientsignature = patientsignature;
	}
	public String getOtherinsuredyy() {
		return otherinsuredyy;
	}
	public void setOtherinsuredyy(String otherinsuredyy) {
		this.otherinsuredyy = otherinsuredyy;
	}
	public String getPatientaddress() {
		return patientaddress;
	}
	public void setPatientaddress(String patientaddress) {
		this.patientaddress = patientaddress;
	}
	public String getBalancedue() {
		return balancedue;
	}
	public void setBalancedue(String balancedue) {
		this.balancedue = balancedue;
	}
	public String getMedicareradio() {
		return medicareradio;
	}
	public void setMedicareradio(String medicareradio) {
		this.medicareradio = medicareradio;
	}
	public String getCob6() {
		return cob6;
	}
	public void setCob6(String cob6) {
		this.cob6 = cob6;
	}
	public String getHospitalizedfromdd() {
		return hospitalizedfromdd;
	}
	public void setHospitalizedfromdd(String hospitalizedfromdd) {
		this.hospitalizedfromdd = hospitalizedfromdd;
	}
	public String getInsuredaddress() {
		return insuredaddress;
	}
	public void setInsuredaddress(String insuredaddress) {
		this.insuredaddress = insuredaddress;
	}
	public String getInsurancepolicynum() {
		return insurancepolicynum;
	}
	public void setInsurancepolicynum(String insurancepolicynum) {
		this.insurancepolicynum = insurancepolicynum;
	}
	public String getInsuredemployername() {
		return insuredemployername;
	}
	public void setInsuredemployername(String insuredemployername) {
		this.insuredemployername = insuredemployername;
	}
	public String getUnabletoworkfrommm() {
		return unabletoworkfrommm;
	}
	public void setUnabletoworkfrommm(String unabletoworkfrommm) {
		this.unabletoworkfrommm = unabletoworkfrommm;
	}
	public String getUnabletoworktoyy() {
		return unabletoworktoyy;
	}
	public void setUnabletoworktoyy(String unabletoworktoyy) {
		this.unabletoworktoyy = unabletoworktoyy;
	}
	public String getUnabletoworktomm() {
		return unabletoworktomm;
	}
	public void setUnabletoworktomm(String unabletoworktomm) {
		this.unabletoworktomm = unabletoworktomm;
	}
	public String getUnabletoworkfromdd() {
		return unabletoworkfromdd;
	}
	public void setUnabletoworkfromdd(String unabletoworkfromdd) {
		this.unabletoworkfromdd = unabletoworkfromdd;
	}
	public String getSimilarillnessmm() {
		return similarillnessmm;
	}
	public void setSimilarillnessmm(String similarillnessmm) {
		this.similarillnessmm = similarillnessmm;
	}
	public String getPatientdobyy3() {
		return patientdobyy3;
	}
	public void setPatientdobyy3(String patientdobyy3) {
		this.patientdobyy3 = patientdobyy3;
	}
	public String getPatientconditionother() {
		return patientconditionother;
	}
	public void setPatientconditionother(String patientconditionother) {
		this.patientconditionother = patientconditionother;
	}
	public String getPhysicianidnum() {
		return physicianidnum;
	}
	public void setPhysicianidnum(String physicianidnum) {
		this.physicianidnum = physicianidnum;
	}
	public String getReservedlocaluse() {
		return reservedlocaluse;
	}
	public void setReservedlocaluse(String reservedlocaluse) {
		this.reservedlocaluse = reservedlocaluse;
	}
	public String getInsuredfullname() {
		return insuredfullname;
	}
	public void setInsuredfullname(String insuredfullname) {
		this.insuredfullname = insuredfullname;
	}
	public String getPatientfullname() {
		return patientfullname;
	}
	public void setPatientfullname(String patientfullname) {
		this.patientfullname = patientfullname;
	}
	public String getPatientstate() {
		return patientstate;
	}
	public void setPatientstate(String patientstate) {
		this.patientstate = patientstate;
	}
	public String getCharges2() {
		return charges2;
	}
	public void setCharges2(String charges2) {
		this.charges2 = charges2;
	}
	public String getOtherinsuredfullname() {
		return otherinsuredfullname;
	}
	public void setOtherinsuredfullname(String otherinsuredfullname) {
		this.otherinsuredfullname = otherinsuredfullname;
	}
	public String getCharges1() {
		return charges1;
	}
	public void setCharges1(String charges1) {
		this.charges1 = charges1;
	}
	public String getCharges6() {
		return charges6;
	}
	public void setCharges6(String charges6) {
		this.charges6 = charges6;
	}
	public String getCharges5() {
		return charges5;
	}
	public void setCharges5(String charges5) {
		this.charges5 = charges5;
	}
	public String getCharges4() {
		return charges4;
	}
	public void setCharges4(String charges4) {
		this.charges4 = charges4;
	}
	public String getTotalcharge() {
		return totalcharge;
	}
	public void setTotalcharge(String totalcharge) {
		this.totalcharge = totalcharge;
	}
	public String getCharges3() {
		return charges3;
	}
	public void setCharges3(String charges3) {
		this.charges3 = charges3;
	}
	public String getEmployerschoolname() {
		return employerschoolname;
	}
	public void setEmployerschoolname(String employerschoolname) {
		this.employerschoolname = employerschoolname;
	}
	public String getCpt1() {
		return cpt1;
	}
	public void setCpt1(String cpt1) {
		this.cpt1 = cpt1;
	}
	public String getCpt2() {
		return cpt2;
	}
	public void setCpt2(String cpt2) {
		this.cpt2 = cpt2;
	}
	public String getInsuredstate() {
		return insuredstate;
	}
	public void setInsuredstate(String insuredstate) {
		this.insuredstate = insuredstate;
	}
	public String getCpt5() {
		return cpt5;
	}
	public void setCpt5(String cpt5) {
		this.cpt5 = cpt5;
	}
	public String getCpt6() {
		return cpt6;
	}
	public void setCpt6(String cpt6) {
		this.cpt6 = cpt6;
	}
	public String getCpt3() {
		return cpt3;
	}
	public void setCpt3(String cpt3) {
		this.cpt3 = cpt3;
	}
	public String getCpt4() {
		return cpt4;
	}
	public void setCpt4(String cpt4) {
		this.cpt4 = cpt4;
	}
	public String getPatientrelationshipstatus() {
		return patientrelationshipstatus;
	}
	public void setPatientrelationshipstatus(String patientrelationshipstatus) {
		this.patientrelationshipstatus = patientrelationshipstatus;
	}
	public String getHospitalizedtoyy() {
		return hospitalizedtoyy;
	}
	public void setHospitalizedtoyy(String hospitalizedtoyy) {
		this.hospitalizedtoyy = hospitalizedtoyy;
	}
	
	public HCFA_Model getSelf(){
		return this;
	}
}

