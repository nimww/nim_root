

package com.winstaff;

import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public interface InttCT_ModelLI extends dbTableInterface
{

    public void setCT_ModelID(Integer newValue);
    public Integer getCT_ModelID();
    public void setDescriptionLong(String newValue);
    public String getDescriptionLong();
    public void setManufacturer(String newValue);
    public String getManufacturer();
    public void setModel(String newValue);
    public String getModel();
    public void setSlices(Integer newValue);
    public Integer getSlices();
    public void setImages_Transferred(Integer newValue);
    public Integer getImages_Transferred();
    public void setIn_Seconds(Integer newValue);
    public Integer getIn_Seconds();
}    // End of bltCT_ModelLI class definition
