package com.winstaff;

public class apObject {

	public String Vendor_Company_Name;
	public String Month_Year_Activity;
	public String Location;
	public String AmeriSys_Payor_ID;
	public String AmeriSys_Payor_ID_Name;
	public String Referral_Source_Name;
	public String Description_of_Service;
	public String AmeriSys_Claim_ID;
	public String Date_of_Injury;
	public String IW_Last_Name;
	public String First_Name;
	public String Last_Four_Digits_SS;
	public String Date_of_Birth;
	public String Home_address_or_Work_address = "H";
	public String Home_Work_Address_1;
	public String Address_2;
	public String City;
	public String State;
	public String Zip_Code;
	public String Referring_NPI;
	public String Rendering_NPI;
	public String Diagnosis_ICD9;
	public String Diagnosis_Descriptor;
	public String Surgical_Status = "N";
	public String Which_Body_Part;
	public String Quantity;
	public String Facility_Address_1;
	public String Facility_Address_2;
	public String Facility_City;
	public String Facility_State;
	public String Facility_Zip_Code;
	public String Mileage_from_Home_Work_Address;
	public String Date_Service_Requested;
	public String Date_Initial_Contact_made_with_IW;
	public String Date_Initial_Appointment_Offered;
	public String Schedule_Date_of_Appointment;
	public String Schedule_Time_of_Appointment;
	public String Actual_Date_Service_Provided;
	public String Actual_Time_Service_Provided;
	public String Day_of_Final_Service_Delivery_Date;
	public String Date_Report_Provided_to_Referral_Source;
	public String Invoice;
	public String Usual_Customary_or_State_Fee_Schedule;
	public String Vendor_Contracted_Price;
	
	public apObject(){};
	
	public apObject getThis() {
		return this;
	}

	public String getVendor_Company_Name() {
		return Vendor_Company_Name;
	}

	public void setVendor_Company_Name(String vendor_Company_Name) {
		Vendor_Company_Name = vendor_Company_Name;
	}

	public String getMonth_Year_Activity() {
		return Month_Year_Activity;
	}

	public void setMonth_Year_Activity(String month_Year_Activity) {
		Month_Year_Activity = month_Year_Activity;
	}

	public String getLocation() {
		return Location;
	}

	public void setLocation(String location) {
		Location = location;
	}

	public String getAmeriSys_Payor_ID() {
		return AmeriSys_Payor_ID;
	}

	public void setAmeriSys_Payor_ID(String ameriSys_Payor_ID) {
		AmeriSys_Payor_ID = ameriSys_Payor_ID;
	}

	public String getAmeriSys_Payor_ID_Name() {
		return AmeriSys_Payor_ID_Name;
	}

	public void setAmeriSys_Payor_ID_Name(String ameriSys_Payor_ID_Name) {
		AmeriSys_Payor_ID_Name = ameriSys_Payor_ID_Name;
	}

	public String getReferral_Source_Name() {
		return Referral_Source_Name;
	}

	public void setReferral_Source_Name(String referral_Source_Name) {
		Referral_Source_Name = referral_Source_Name;
	}

	public String getDescription_of_Service() {
		return Description_of_Service;
	}

	public void setDescription_of_Service(String description_of_Service) {
		Description_of_Service = description_of_Service;
	}

	public String getAmeriSys_Claim_ID() {
		return AmeriSys_Claim_ID;
	}

	public void setAmeriSys_Claim_ID(String ameriSys_Claim_ID) {
		AmeriSys_Claim_ID = ameriSys_Claim_ID;
	}

	public String getDate_of_Injury() {
		return Date_of_Injury;
	}

	public void setDate_of_Injury(String date_of_Injury) {
		Date_of_Injury = date_of_Injury;
	}

	public String getIW_Last_Name() {
		return IW_Last_Name;
	}

	public void setIW_Last_Name(String iW_Last_Name) {
		IW_Last_Name = iW_Last_Name;
	}

	public String getFirst_Name() {
		return First_Name;
	}

	public void setFirst_Name(String first_Name) {
		First_Name = first_Name;
	}

	public String getLast_Four_Digits_SS() {
		return Last_Four_Digits_SS;
	}

	public void setLast_Four_Digits_SS(String last_Four_Digits_SS) {
		Last_Four_Digits_SS = last_Four_Digits_SS;
	}

	public String getDate_of_Birth() {
		return Date_of_Birth;
	}

	public void setDate_of_Birth(String date_of_Birth) {
		Date_of_Birth = date_of_Birth;
	}

	public String getHome_address_or_Work_address() {
		return Home_address_or_Work_address;
	}

	public void setHome_address_or_Work_address(String home_address_or_Work_address) {
		Home_address_or_Work_address = home_address_or_Work_address;
	}

	public String getHome_Work_Address_1() {
		return Home_Work_Address_1;
	}

	public void setHome_Work_Address_1(String home_Work_Address_1) {
		Home_Work_Address_1 = home_Work_Address_1;
	}

	public String getAddress_2() {
		return Address_2;
	}

	public void setAddress_2(String address_2) {
		Address_2 = address_2;
	}

	public String getCity() {
		return City;
	}

	public void setCity(String city) {
		City = city;
	}

	public String getState() {
		return State;
	}

	public void setState(String state) {
		State = state;
	}

	public String getZip_Code() {
		return Zip_Code;
	}

	public void setZip_Code(String zip_Code) {
		Zip_Code = zip_Code;
	}

	public String getReferring_NPI() {
		return Referring_NPI;
	}

	public void setReferring_NPI(String referring_NPI) {
		Referring_NPI = referring_NPI;
	}

	public String getRendering_NPI() {
		return Rendering_NPI;
	}

	public void setRendering_NPI(String rendering_NPI) {
		Rendering_NPI = rendering_NPI;
	}

	public String getDiagnosis_ICD9() {
		return Diagnosis_ICD9;
	}

	public void setDiagnosis_ICD9(String diagnosis_ICD9) {
		Diagnosis_ICD9 = diagnosis_ICD9;
	}

	public String getDiagnosis_Descriptor() {
		return Diagnosis_Descriptor;
	}

	public void setDiagnosis_Descriptor(String diagnosis_Descriptor) {
		Diagnosis_Descriptor = diagnosis_Descriptor;
	}

	public String getSurgical_Status() {
		return Surgical_Status;
	}

	public void setSurgical_Status(String surgical_Status) {
		Surgical_Status = surgical_Status;
	}

	public String getWhich_Body_Part() {
		return Which_Body_Part;
	}

	public void setWhich_Body_Part(String which_Body_Part) {
		Which_Body_Part = which_Body_Part;
	}

	public String getQuantity() {
		return Quantity;
	}

	public void setQuantity(String quantity) {
		Quantity = quantity;
	}

	public String getFacility_Address_1() {
		return Facility_Address_1;
	}

	public void setFacility_Address_1(String facility_Address_1) {
		Facility_Address_1 = facility_Address_1;
	}

	public String getFacility_Address_2() {
		return Facility_Address_2;
	}

	public void setFacility_Address_2(String facility_Address_2) {
		Facility_Address_2 = facility_Address_2;
	}

	public String getFacility_City() {
		return Facility_City;
	}

	public void setFacility_City(String facility_City) {
		Facility_City = facility_City;
	}

	public String getFacility_State() {
		return Facility_State;
	}

	public void setFacility_State(String facility_State) {
		Facility_State = facility_State;
	}

	public String getFacility_Zip_Code() {
		return Facility_Zip_Code;
	}

	public void setFacility_Zip_Code(String facility_Zip_Code) {
		Facility_Zip_Code = facility_Zip_Code;
	}

	public String getMileage_from_Home_Work_Address() {
		return Mileage_from_Home_Work_Address;
	}

	public void setMileage_from_Home_Work_Address(String mileage_from_Home_Work_Address) {
		Mileage_from_Home_Work_Address = mileage_from_Home_Work_Address;
	}

	public String getDate_Service_Requested() {
		return Date_Service_Requested;
	}

	public void setDate_Service_Requested(String date_Service_Requested) {
		Date_Service_Requested = date_Service_Requested;
	}

	public String getDate_Initial_Contact_made_with_IW() {
		return Date_Initial_Contact_made_with_IW;
	}

	public void setDate_Initial_Contact_made_with_IW(String date_Initial_Contact_made_with_IW) {
		Date_Initial_Contact_made_with_IW = date_Initial_Contact_made_with_IW;
	}

	public String getDate_Initial_Appointment_Offered() {
		return Date_Initial_Appointment_Offered;
	}

	public void setDate_Initial_Appointment_Offered(String date_Initial_Appointment_Offered) {
		Date_Initial_Appointment_Offered = date_Initial_Appointment_Offered;
	}

	public String getSchedule_Date_of_Appointment() {
		return Schedule_Date_of_Appointment;
	}

	public void setSchedule_Date_of_Appointment(String schedule_Date_of_Appointment) {
		Schedule_Date_of_Appointment = schedule_Date_of_Appointment;
	}

	public String getSchedule_Time_of_Appointment() {
		return Schedule_Time_of_Appointment;
	}

	public void setSchedule_Time_of_Appointment(String schedule_Time_of_Appointment) {
		Schedule_Time_of_Appointment = schedule_Time_of_Appointment;
	}

	public String getActual_Date_Service_Provided() {
		return Actual_Date_Service_Provided;
	}

	public void setActual_Date_Service_Provided(String actual_Date_Service_Provided) {
		Actual_Date_Service_Provided = actual_Date_Service_Provided;
	}

	public String getActual_Time_Service_Provided() {
		return Actual_Time_Service_Provided;
	}

	public void setActual_Time_Service_Provided(String actual_Time_Service_Provided) {
		Actual_Time_Service_Provided = actual_Time_Service_Provided;
	}

	public String getDay_of_Final_Service_Delivery_Date() {
		return Day_of_Final_Service_Delivery_Date;
	}

	public void setDay_of_Final_Service_Delivery_Date(String day_of_Final_Service_Delivery_Date) {
		Day_of_Final_Service_Delivery_Date = day_of_Final_Service_Delivery_Date;
	}

	public String getDate_Report_Provided_to_Referral_Source() {
		return Date_Report_Provided_to_Referral_Source;
	}

	public void setDate_Report_Provided_to_Referral_Source(String date_Report_Provided_to_Referral_Source) {
		Date_Report_Provided_to_Referral_Source = date_Report_Provided_to_Referral_Source;
	}

	public String getInvoice() {
		return Invoice;
	}

	public void setInvoice(String invoice) {
		Invoice = invoice;
	}

	public String getUsual_Customary_or_State_Fee_Schedule() {
		return Usual_Customary_or_State_Fee_Schedule;
	}

	public void setUsual_Customary_or_State_Fee_Schedule(String usual_Customary_or_State_Fee_Schedule) {
		Usual_Customary_or_State_Fee_Schedule = usual_Customary_or_State_Fee_Schedule;
	}

	public String getVendor_Contracted_Price() {
		return Vendor_Contracted_Price;
	}

	public void setVendor_Contracted_Price(String vendor_Contracted_Price) {
		Vendor_Contracted_Price = vendor_Contracted_Price;
	}

	public apObject(String vendor_Company_Name, String month_Year_Activity, String location, String ameriSys_Payor_ID, String ameriSys_Payor_ID_Name, String referral_Source_Name, String description_of_Service, String ameriSys_Claim_ID, String date_of_Injury, String iW_Last_Name, String first_Name, String last_Four_Digits_SS, String date_of_Birth, String home_address_or_Work_address,
			String home_Work_Address_1, String address_2, String city, String state, String zip_Code, String referring_NPI, String rendering_NPI, String diagnosis_ICD9, String diagnosis_Descriptor, String surgical_Status, String which_Body_Part, String quantity, String facility_Address_1, String facility_Address_2, String facility_City, String facility_State, String facility_Zip_Code,
			String mileage_from_Home_Work_Address, String date_Service_Requested, String date_Initial_Contact_made_with_IW, String date_Initial_Appointment_Offered, String schedule_Date_of_Appointment, String schedule_Time_of_Appointment, String actual_Date_Service_Provided, String actual_Time_Service_Provided, String day_of_Final_Service_Delivery_Date,
			String date_Report_Provided_to_Referral_Source, String invoice, String usual_Customary_or_State_Fee_Schedule, String vendor_Contracted_Price) {
		Vendor_Company_Name = vendor_Company_Name;
		Month_Year_Activity = month_Year_Activity;
		Location = location;
		AmeriSys_Payor_ID = ameriSys_Payor_ID;
		AmeriSys_Payor_ID_Name = ameriSys_Payor_ID_Name;
		Referral_Source_Name = referral_Source_Name;
		Description_of_Service = description_of_Service;
		AmeriSys_Claim_ID = ameriSys_Claim_ID;
		Date_of_Injury = date_of_Injury;
		IW_Last_Name = iW_Last_Name;
		First_Name = first_Name;
		Last_Four_Digits_SS = last_Four_Digits_SS;
		Date_of_Birth = date_of_Birth;
		Home_address_or_Work_address = home_address_or_Work_address;
		Home_Work_Address_1 = home_Work_Address_1;
		Address_2 = address_2;
		City = city;
		State = state;
		Zip_Code = zip_Code;
		Referring_NPI = referring_NPI;
		Rendering_NPI = rendering_NPI;
		Diagnosis_ICD9 = diagnosis_ICD9;
		Diagnosis_Descriptor = diagnosis_Descriptor;
		Surgical_Status = surgical_Status;
		Which_Body_Part = which_Body_Part;
		Quantity = quantity;
		Facility_Address_1 = facility_Address_1;
		Facility_Address_2 = facility_Address_2;
		Facility_City = facility_City;
		Facility_State = facility_State;
		Facility_Zip_Code = facility_Zip_Code;
		Mileage_from_Home_Work_Address = mileage_from_Home_Work_Address;
		Date_Service_Requested = date_Service_Requested;
		Date_Initial_Contact_made_with_IW = date_Initial_Contact_made_with_IW;
		Date_Initial_Appointment_Offered = date_Initial_Appointment_Offered;
		Schedule_Date_of_Appointment = schedule_Date_of_Appointment;
		Schedule_Time_of_Appointment = schedule_Time_of_Appointment;
		Actual_Date_Service_Provided = actual_Date_Service_Provided;
		Actual_Time_Service_Provided = actual_Time_Service_Provided;
		Day_of_Final_Service_Delivery_Date = day_of_Final_Service_Delivery_Date;
		Date_Report_Provided_to_Referral_Source = date_Report_Provided_to_Referral_Source;
		Invoice = invoice;
		Usual_Customary_or_State_Fee_Schedule = usual_Customary_or_State_Fee_Schedule;
		Vendor_Contracted_Price = vendor_Contracted_Price;
	}

}
