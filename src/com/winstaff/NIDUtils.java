package com.winstaff;

import java.sql.ResultSet;
import java.util.*;

/**
 * User: Scott
 * Date: 5/10/13
 * Time: 1:16 PM
 */
public class NIDUtils {

    public static double NID_MIN_MARK_UP = 1.428;   //removing in favor of method below


    public static double MAX_DISCOUNT_PERCENTAGE = 0.0;
    public static double MAX_DISCOUNT_AMOUNT = 0.0;
    public static double MAX_MARKETING_FEE_PERCENTAGE = 0.07;
    public static double ESTIMATED_BILLING_FEE_PERCENTAGE = 0.03;

    public static Double modifyPrice(Double inPrice){
        return new Double(Math.round(inPrice));

    }

    
    
    
    
    public static Double getNIDFinalPrice(double basePrice, bltPracticeMaster practiceMaster, bltNIM3_PayerMaster payerMaster, bltNIM3_NIDPromo nidPromo){
    	
    	int baseMarkup = 115;
    	System.out.println("getNIDFinalPrice(): "+payerMaster.getPayerID());
    	return basePrice + baseMarkup - (payerMaster.getDiscountHighPercentage() > 0 ? 15 : 0);
    }
    
    
    //Removed 03-03-2015. Pricing is not competitive. New model is base price + 100 dollars. 
    public static Double getNIDFinalPrkljklice(double basePrice, bltPracticeMaster practiceMaster, bltNIM3_PayerMaster payerMaster, bltNIM3_NIDPromo nidPromo){
        try{
            Double useMargin = getVaryingMargin(basePrice, practiceMaster);
            Double multiplyFactor = 1 / (1-useMargin);
            Double useNetNet = getVaryingNetNet(basePrice, practiceMaster);
            Double maxMarketingFee = MAX_MARKETING_FEE_PERCENTAGE;
            Double maxDiscountFee = MAX_DISCOUNT_PERCENTAGE;
            Double billingFee = ESTIMATED_BILLING_FEE_PERCENTAGE;

            Double FormulaA_FinalRetailPrice = basePrice * multiplyFactor;
            Double FormulaB_AddNIM = (basePrice + useNetNet);
            Double FormulaB_AddMarketing = FormulaB_AddNIM * (1/(1-maxMarketingFee));
            Double FormulaB_AddBilling = FormulaB_AddMarketing * (1/(1-billingFee));
            Double FormulaB_AddDiscount = FormulaB_AddBilling * (1/(1-maxDiscountFee));
            Double FormulaB_FinalRetailPrice = FormulaB_AddDiscount;

            // For retail pricing, we take whichever is greater.  The idea here is that Formula B gives us ONLY our min NetNet fee no more, no less. We want more, but can't accept less.  So by taking the greater of the two, we'll assure this.
            // For Partner pricing, we take Formula B.

            DebugLogger.e("Testing", "FormulaA_FinalRetailPrice = ["+FormulaA_FinalRetailPrice+"] FormulaB_FinalRetailPrice = ["+FormulaB_FinalRetailPrice+"]" );

            Double customerDiscountAmount = null;
            Double customerDiscountPercentage = null;
            if (nidPromo!=null){
                //nidPromo was supplied so we use it first
                if (nidPromo.getDiscountHighPercentage()!=null && nidPromo.getDiscountHighPercentage()>0.0){
                    customerDiscountPercentage = nidPromo.getDiscountHighPercentage();
                } else if (nidPromo.getDiscountHighAmount()!=null && nidPromo.getDiscountHighAmount()>0.0){
                    customerDiscountAmount = nidPromo.getDiscountHighAmount();
                }
            }
            //if discounts are still null, then we revert to the payer details
            if (customerDiscountAmount==null && customerDiscountPercentage==null){
                //nidPromo was supplied so we use it first
                if (payerMaster.getDiscountHighPercentage()!=null && payerMaster.getDiscountHighPercentage()>0.0){
                    customerDiscountPercentage = payerMaster.getDiscountHighPercentage();
                } else if (payerMaster.getDiscountHighAmount()!=null && payerMaster.getDiscountHighAmount()>0.0){
                    customerDiscountAmount = payerMaster.getDiscountHighAmount();
                }
            }

            if (customerDiscountAmount==null && customerDiscountPercentage==null){
                //if both are null or zero then customer does not get a discount
                if (FormulaA_FinalRetailPrice.doubleValue()>FormulaB_FinalRetailPrice.doubleValue()){
                    return modifyPrice(FormulaA_FinalRetailPrice);
                }
            } else {
                //if both are null or zero then customer does not get a discount
                return modifyPrice(FormulaB_FinalRetailPrice);
            }

        } catch (Exception e){
            DebugLogger.e("NIDUtils:getNIDFinalPrice","Error in calc for final price/discount ["+e+"]");

        }
        return modifyPrice(basePrice * NIDUtils.NID_MARK_UP(practiceMaster.getOfficeStateID()));
    }

    public static Double getVaryingNetNet(double basePrice, bltPracticeMaster practiceMaster){
        //ignoreing state for now just use arrage based on the basePrice (aka cost)
        if (basePrice<150){
            return 50.00;
        } else if (basePrice<200){
            return 65.00;
        } else if (basePrice<250){
            return 80.00;
        } else if (basePrice<300){
            return 85.00;
        } else if (basePrice<350){
            return 90.00;
        } else if (basePrice<400){
            return 90.00;
        } else if (basePrice<450){
            return 95.00;
        } else if (basePrice<500){
            return 95.00;
        } else if (basePrice<550){
            return 100.00;
        } else if (basePrice<600){
            return 100.00;
        } else if (basePrice<650){
            return 110.00;
        } else if (basePrice<700){
            return 110.00;
        } else if (basePrice<750){
            return 110.00;
        }
        return 125.00;
    }


    public static Double getVaryingMargin(double basePrice, bltPracticeMaster practiceMaster){
        //ignoreing state for now just use arrage based on the basePrice (aka cost)
        if (basePrice<150){
            return 0.428571429;
        } else if (basePrice<200){
            return 0.387755102;
        } else if (basePrice<250){
            return 0.375000000;
        } else if (basePrice<300){
            return 0.358974359;
        } else if (basePrice<350){
            return 0.347826087;
        } else if (basePrice<400){
            return 0.339622642;
        } else if (basePrice<450){
            return 0.333333333;
        } else if (basePrice<500){
            return 0.318181818;
        } else if (basePrice<550){
            return 0.305555556;
        } else if (basePrice<600){
            return 0.294871795;
        } else if (basePrice<650){
            return 0.285714286;
        } else if (basePrice<700){
            return 0.277777778;
        } else if (basePrice<750){
            return 0.270833333;
        }
        return 0.26;
    }


    public static Double getNIDFinalPrice_original(double basePrice, bltPracticeMaster practiceMaster, bltNIM3_PayerMaster payerMaster, bltNIM3_NIDPromo nidPromo){
        try{
            DebugLogger.e("payerMaster", ""+ payerMaster.getDiscountHighAmount());
            Double customerDiscountAmount = null;
            Double customerDiscountPercentage = null;
            if (nidPromo!=null){
                //nidPromo was supplied so we use it first
                if (nidPromo.getDiscountHighPercentage()!=null && nidPromo.getDiscountHighPercentage()>0.0){
                    customerDiscountPercentage = nidPromo.getDiscountHighPercentage();
                } else if (nidPromo.getDiscountHighAmount()!=null && nidPromo.getDiscountHighAmount()>0.0){
                    customerDiscountAmount = nidPromo.getDiscountHighAmount();
                }
            }
            //if discounts are still null, then we revert to the payer details
            if (customerDiscountAmount==null && customerDiscountPercentage==null){
                //nidPromo was supplied so we use it first
                if (payerMaster.getDiscountHighPercentage()!=null && payerMaster.getDiscountHighPercentage()>0.0){
                    customerDiscountPercentage = payerMaster.getDiscountHighPercentage();
                } else if (payerMaster.getDiscountHighAmount()!=null && payerMaster.getDiscountHighAmount()>0.0){
                    customerDiscountAmount = payerMaster.getDiscountHighAmount();
                }
            }

            Double finalPrice = basePrice * NIDUtils.NID_MARK_UP(practiceMaster.getOfficeStateID());
            if (customerDiscountPercentage!=null){
                finalPrice = finalPrice * (new Double(1)-customerDiscountPercentage);
            } else if (customerDiscountAmount!=null){
                finalPrice = finalPrice - customerDiscountAmount;
            }

            //check for 30% min margin
            double minMarginPrice = basePrice * NID_MIN_MARK_UP;
            if (finalPrice>minMarginPrice){
                return finalPrice;
            }
        } catch (Exception e){
            DebugLogger.e("NIDUtils:getNIDFinalPrice","Error in calc for final price/discount ["+e+"]");

        }
        return basePrice * NIDUtils.NID_MARK_UP(practiceMaster.getOfficeStateID());

    }

    public static double NID_MARK_UP(int stateid) {
		switch (stateid) {
		case 49:
			return 1.25; // AK
		case 30:
			return 1.66; // AL
		case 21:
			return 2.20; // AR
		case 8:
			return 2.25; // AZ
		case 1:
			return 1.73; // CA
		case 10:
			return 1.69; // CO
		case 45:
			return 1.60; // CT
		case 51:
			return 1.54; // DC
		case 47:
			return 1.54; // DE
		case 33:
			return 1.90; // FL
		case 32:
			return 1.58; // GA
		case 50:
			return 1.54; // HI
		case 19:
			return 1.67; // IA
		case 5:
			return 1.54; // ID
		case 24:
			return 1.94; // IL
		case 27:
			return 1.92; // IN
		case 15:
			return 2.31; // KS
		case 28:
			return 1.90; // KY
		case 22:
			return 1.54; // LA
		case 43:
			return 1.68; // MA
		case 48:
			return 1.66; // MD
		case 26:
			return 1.74; // MI
		case 18:
			return 1.80; // MN
		case 20:
			return 1.95; // MO
		case 25:
			return 1.68; // MS
		case 4:
			return 1.68; // MT
		case 41:
			return 1.71; // NC
		case 14:
			return 1.85; // NE
		case 35:
			return 1.60; // NH
		case 46:
			return 2.01; // NJ
		case 11:
			return 1.54; // NM
		case 6:
			return 1.77; // NV
		case 37:
			return 1.79; // NY
		case 31:
			return 1.73; // OH
		case 16:
			return 1.57; // OK
		case 3:
			return 1.50; // OR
		case 38:
			return 1.76; // PA
		case 44:
			return 1.54; // RI
		case 42:
			return 1.66; // SC
		case 13:
			return 1.63; // SD
		case 29:
			return 1.65; // TN
		case 17:
			return 1.81; // TX
		case 7:
			return 1.83; // UT
		case 40:
			return 1.71; // VA
		case 36:
			return 1.50; // VT
		case 2:
			return 1.71; // WA
		case 23:
			return 2.21; // WI
		}
		return 1.42;
    }
    
    public static void trackWebLink(String ip, String ua, Integer payerID, String webLink){
        bltNIM3_WebLinkTracker myWLT = new bltNIM3_WebLinkTracker();
        myWLT.setActionID(1);
        myWLT.setRemoteIP(ip);
        myWLT.setUserAgent(ua);
        myWLT.setWebLink(webLink);
        myWLT.setPayerID(payerID);
        myWLT.setTransactionDate(new Date());
        myWLT.setUniqueCreateDate(new Date());
        myWLT.setUniqueModifyDate(new Date());
        try{
            myWLT.commitData();
        } catch (Exception e){
            DebugLogger.e("NIDUtils:trackWebLink","Can't save tracker ["+e+"]");
        }
    }



    public static ImagingCenterListObject getListOfCenters(String sZIP, int iMiles, CPTGroupObject2 myCGO2_in, boolean limitModality, boolean requireOpen, int MaxResults, String webLink){
    //Note the final parameter is a boolean that tells whether the results should be limited only to centers containing all the CPT Group objects.  Default = True
    //  This means that if the user needs an US only centers that have US will show up.

        NIM3_ReferralSourceObject myRSO = NID_SubmitCaseObject.translateWebCodesToReferralSource ("","",webLink);
        bltNIM3_PayerMaster payerMaster = new bltNIM3_PayerMaster(myRSO.getPayerID());
        bltNIM3_NIDPromo nidPromo = null;
        if (myRSO.getNidPromo()!=null){
            nidPromo = new bltNIM3_NIDPromo(myRSO.getNidPromo());
        }

        Double ruralRatio = 0.0;
        int maxMiles = 200;
        Vector<String> searchNotes = new Vector<String>();
        sZIP = sZIP.trim();
//        CPTGroupObject2 myCGO2_dom = NIMUtils.getDominantCode(myCGO2_in);
        CPTGroupObject2 myCGO2 = myCGO2_in;
        String keyCheapest = null;
        Double valueCheapest = null;
        String keyClosest = null;
        Double valueClosest = null;
        String keyBestValue = null;

        Double theUC = 0.0;
        try {
            theUC = NIMUtils.getFreeSchedulePriceCPTGroup(3, 1.00, sZIP, myCGO2, new Date());
        } catch (java.sql.SQLException sql){

        }


        if (sZIP.equalsIgnoreCase("") || sZIP.length()!=5 || iMiles<=0){
            return null;
        } else {
            Hashtable<String, PracticeMaster_SearchResultsObject> myCenterList = new Hashtable<String, PracticeMaster_SearchResultsObject>();
//            Vector<PracticeMaster_SearchResultsObject> myCenterList = new Vector();
            ZIP_CODE_object myZCo = NIMUtils.get_zips_in_range(sZIP,iMiles);
            Vector myZIPVector = myZCo.getZipsInRange(iMiles);
            int i2cnt=0;
            String myZip = "";
            String myZipSQL = "";
            for (int i10=0;i10<myZIPVector.size();i10++)
            {
                if (i10>0)
                {
                    myZip += ", ";
                    myZipSQL += ",";
                }
                myZip += "'" + (String) myZIPVector.elementAt(i10) + "'";
                myZipSQL += "?";
            }
            if (myZip.equalsIgnoreCase(""))
            {
                //Possible unknown ZIP, this means that we may only find centers within the same ZIP
                myZip = "'" + sZIP + "'";
                myZipSQL = "?";
                DebugLogger.e("NIDUtils:getListOfCenters",":Invalid/New ZIP["+sZIP+"]");

            }
            //	searchDB2_Remote mySS = new searchDB2_Remote();
            searchDB2 mySS = new searchDB2();
            ResultSet myRS = null;
            try
            {
                //set these abitrarily high
                Double theCheapest = 10000.0;
                Double theClosest = 1000.0;

                String mySQL = null;
                //2011-06-15 updated process
                String addWhere = "";
                if (limitModality){
                    if (myCGO2.isHasMRI())
                    {
                        addWhere += " AND hasmr is not null ";
                    }
                    if (myCGO2.isHasCT())
                    {
                        addWhere += " AND hasct is not null ";
                    }
                    if (myCGO2.isHasEMG())
                    {
                        addWhere += " AND hasemg is not null ";
                    }
                    if (myCGO2.isHasXR())
                    {
                        addWhere += " AND hasxr is not null ";
                    }
                    if (myCGO2.isHasUS())
                    {
                        addWhere += " AND hasus is not null ";
                    }
                    if (myCGO2.isHasNM())
                    {
                        addWhere += " AND hasnm is not null ";
                    }
                    if (myCGO2.isHasFL())
                    {
                        addWhere += " AND hasfl is not null ";
                    }
                    if (myCGO2.isHasPET())
                    {
                        addWhere += " AND haspet is not null ";
                    }
                }
                if (requireOpen){
                    addWhere += " AND mr_open_class>0 ";
                }
                mySQL = ("SELECT practiceid, hasmr, mr_open_class, mr_open_tesla, mr_trad_class, mr_trad_tesla, hasmr_acr, hasmr_acrverified, hasmr_acrverifieddate, hasmr_acrexpirationdate, hasct_acr, hasct_acrverified, hasct_acrverifieddate, hasct_acrexpirationdate, hasus_acr, hasus_acrverified, hasus_acrverifieddate, hasus_acrexpirationdate   from \"vMQ3_Practice_Services\" where substr(officezip,0,6) in (" + myZipSQL + ") and contractglobal in (0, 1) and contractingstatusid in (2,8) " +addWhere);
                //SQL Prepared Statements Update 2011
                db_PreparedStatementObject myDPSO = new db_PreparedStatementObject(mySQL);
                if (myZIPVector.size()<=1)
                {
                    myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,sZIP));
                }
                else
                {
                    for (int i10=0;i10<myZIPVector.size();i10++)
                    {
                        myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,(String)myZIPVector.elementAt(i10)));
                    }
                }
                myRS = mySS.executePreparedStatement(myDPSO);

                GEOUtils.Location myGUL_Pat = null;
                GEOUtils.Location myGUL_Prac = null;
                try
                {
                    myGUL_Pat = NIMUtils.getLatLon(sZIP);
                }
                catch (Exception eeeeeee)
                {
                    DebugLogger.e ("NIMUtils:getHashOfCenters:","Invalid Patient Address.[" + eeeeeee.toString()+"]");
                }
                if (myGUL_Pat ==null)
                {
                    DebugLogger.e ("NIMUtils:getHashOfCenters:","Invalid Patient Address.");
                }
                while(myRS!=null&&myRS.next())
                {


//                    DebugLogger.d("NID Practice Search", "Looking at Practice: ["+myRS.getInt("practiceID")+"]");
                    i2cnt++;
                    //Pull record and set the badges and stats
                    PracticeMaster_SearchResultsObject myPracM_SRO = new PracticeMaster_SearchResultsObject(myRS.getInt("practiceID"));
                    
                    if (myRS.getInt("hasmr_acr")==1){
                        myPracM_SRO.setBadge_ACR(true);
                        myPracM_SRO.setBadge_ACR_MR(true);
                        if (myRS.getInt("hasmr_acrverified")==1){
                            myPracM_SRO.badge_ACR_notes.add("ACR verified for MR");
                        }
                    }

                    if (myRS.getInt("hasct_acr")==1){
                        myPracM_SRO.setBadge_ACR(true);
                        myPracM_SRO.setBadge_ACR_CT(true);
                        if (myRS.getInt("hasct_acrverified")==1){
                            myPracM_SRO.badge_ACR_notes.add("ACR verified for CT");
                        }
                    }

                    if (myRS.getInt("hasus_acr")==1){
                        myPracM_SRO.setBadge_ACR(true);
                        myPracM_SRO.setBadge_ACR_US(true);
                        if (myRS.getInt("hasus_acrverified")==1){
                            myPracM_SRO.badge_ACR_notes.add("ACR verified for US");
                        }
                    }

                    Boolean faxPass = true;
                    if (NIMUtils.fax2EmailFax(myPracM_SRO.getPracticeMaster().getOfficeFaxNo(), "").length()!=11){
                        DebugLogger.e("NIDUtils:ImagingCenterListObject","Bad Fax ["+myPracM_SRO.getPracticeMaster().getOfficeFaxNo()+"] for practice ["+myPracM_SRO.getPracticeMaster().getUniqueID()+"]");
                        faxPass = false;
                    }

                    //double check here to ensure that contract global is not set to "no" (2)
                    if (myPracM_SRO.getPracticeMaster().getContractGlobal()!=2 && faxPass){
                        if (myCGO2_in.isHasMRI()&&myRS.getString("mr_open_class")!=null){
                            int tempMR_OpenClass = new Integer(myRS.getString("mr_open_class"));
                            if (tempMR_OpenClass>0){
                                myPracM_SRO.setOpenMR_Tesla(new Double(myRS.getString("mr_open_tesla")));
                                myPracM_SRO.setOpenMR(true);
                                myPracM_SRO.setOpenMR_Class(tempMR_OpenClass);
                            } else {
                                myPracM_SRO.setOpenMR(false);
                            }
                        } else {
                            myPracM_SRO.setOpenMR(false);
                        }
                        if (myCGO2_in.isHasMRI()&&myRS.getString("mr_trad_class")!=null){
                            int tempMR_TradClass = new Integer(myRS.getString("mr_trad_class"));
                            if (tempMR_TradClass>0){
                                myPracM_SRO.setTradMR_Tesla(new Double(myRS.getString("mr_trad_tesla")));
                                myPracM_SRO.setTradMR(true);
                                myPracM_SRO.setTradMR_Class(tempMR_TradClass);
                            } else {
                                myPracM_SRO.setTradMR(false);
                            }
                        } else {
                            myPracM_SRO.setTradMR(false);
                        }
                        DebugLogger.d("POTEST: ","LOOKING AT ID: "+myPracM_SRO.getPracticeMaster().getPracticeID());
                        //get price
                        Double theCost = NIMUtils.getCPTGroupPayPriceforPracticeMaster(myPracM_SRO.practiceMaster,myCGO2, new Date(), NIMUtils.PAYMENT_TYPE_GROUPHEALTH);
                        if (theUC!=null){
                            myPracM_SRO.setPriceUC(theUC);
                        }
                        if (theCost!=null){
                            myPracM_SRO.setPrice(theCost);
                            myPracM_SRO.setCustomerPrice(NIDUtils.getNIDFinalPrice(theCost, myPracM_SRO.practiceMaster, payerMaster, nidPromo ));

                            try
                            {
    //                        myGUL_Prac = NIMUtils.getLatLon(myPracM.getOfficeAddress1() + " " + myPracM.getOfficeCity() + " " + new bltStateLI(myPracM.getOfficeStateID()).getShortState() + " " + myPracM.getOfficeZIP());
                                myGUL_Prac = NIMUtils.getLatLon(myPracM_SRO.practiceMaster.getOfficeZIP());
                            }
                            catch (Exception eeeeeee)
                            {
                                DebugLogger.e ("NIMUtils:getHashOfCenters", "Invalid Center in listing.  Please notify IT and NET DEV[" + eeeeeee.toString()+"]");
                            }

                            if (myGUL_Prac ==null)
                            {
                                DebugLogger.println ("NIMUtils:getHashOfCenters:Invalid Center Address.");
                            }

                            try
                            {
                                double myNewD = 0;
                                try
                                {
                                    myNewD = NIMUtils.calculate_mileage(new Double(myGUL_Pat.lat).doubleValue(),new Double(myGUL_Prac.lat).doubleValue(),new Double(myGUL_Pat.lon).doubleValue(),new Double(myGUL_Prac.lon).doubleValue());
                                }
                                catch (Exception e3ee)
                                {
                                    searchNotes.add("NIMUtils:getHashOfCenters:Error in Distance Calc for: "  + myPracM_SRO.practiceMaster.getPracticeName() + "[" + e3ee.toString()+"]");
                                }
                                myPracM_SRO.setDistance(myNewD);

                                Long score = getValueScore(myCGO2_in, myPracM_SRO, ruralRatio, requireOpen);
                                myPracM_SRO.setMACScore(new Double(score));

                                String key_price = String.format("%09d", score) + "_" + myPracM_SRO.getPracticeMaster().getPracticeID();

                                myPracM_SRO.setKeyScore(key_price);

                                //if keyBestValue has not yet been set or if the current value is greater than the keyBestValue, then make we want the KeyBestValue to be the current Key
                                if (keyBestValue==null || myPracM_SRO.getKeyScore().compareTo(keyBestValue)>0){
                                    keyBestValue = myPracM_SRO.getKeyScore();
                                }

                                //if valueCheapest has not yet been set or if the current value is less than than the valueCheapest, then make we want the keyCheapest to be the current Key
                                if (valueCheapest==null || myPracM_SRO.getPrice()<valueCheapest){
                                    keyCheapest = myPracM_SRO.getKeyScore();
                                }

                                //if valueClosest has not yet been set or if the current value is less than than the valueClosest, then make we want the keyClosest to be the current Key
                                if (valueClosest==null || myPracM_SRO.getDistance()<valueClosest){
                                    keyClosest = myPracM_SRO.getKeyScore();
                                }

                                myCenterList.put(key_price, myPracM_SRO);
                            } catch (Exception eeeeeee){
                                searchNotes.add("NIDUtils:getHashOfCenters:1x Error in Distance Calc for practice: "  + myPracM_SRO.practiceMaster.getPracticeName() + "[" + eeeeeee.toString()+"]");
                                DebugLogger.e("NIDUtils:getHashOfCenters", "1x Error in Distance Calc for practice: " + myPracM_SRO.practiceMaster.getPracticeName() + "[" + eeeeeee.toString() + "]");
                            }
                        }

                    } else {
                        // if Pricing fails do we need to do anything here?

                    }
                }
                mySS.closeAll();
            } catch (Exception eee3eeee) {
                DebugLogger.e ("NIDUtils:getHashOfCenters","Overall fail[" + eee3eeee.toString()+"]");
            } finally {
                mySS.closeAll();
            }


            try {
                DebugLogger.d("NID Debug", "Size = " + myCenterList.size());
                if (myCenterList.size()>0)
                {
                    //finalize a few badges
                    if (keyBestValue!=null){
                        DebugLogger.d("NID Debug", "Key = " + keyBestValue);
                        if (myCenterList.containsKey(keyBestValue)){
                            PracticeMaster_SearchResultsObject temp_SRO = myCenterList.get(keyBestValue);
                            temp_SRO.setBadge_BestValue(true);
                            myCenterList.put(keyBestValue,temp_SRO);
                        } else {
                            DebugLogger.e("NID Debug", "Key not in List?!");

                        }
                    }

                    Vector<Double> priceKeys = null;
                    priceKeys = new java.util.Vector(myCenterList.keySet());
                    java.util.Collections.sort(priceKeys, java.util.Collections.reverseOrder());
                    return new ImagingCenterListObject(NIMUtils.HashToVector(myCenterList, priceKeys, MaxResults),searchNotes);

                }
                else
                {
                    //let's try again but with wider range.

                    //first let's make sure we are not stuck in an infinite loop

                    if (iMiles>=maxMiles){
                        return null;
                    } else {
                        return getListOfCenters(sZIP, maxMiles, myCGO2_in, limitModality, requireOpen, MaxResults, webLink);
                    }
                }
            } catch (Exception final_ee) {
                DebugLogger.e ("NIDUtils:getHashOfCenters","Return Fail[" + final_ee.toString()+"]");
            }

        }
        return null;

    }
    
    public static ImagingCenterListObject getListOfCentersNIM(String sZIP, int iMiles, CPTGroupObject2 myCGO2_in, boolean limitModality, boolean requireOpen, int MaxResults, String webLink){
        //Note the final parameter is a boolean that tells whether the results should be limited only to centers containing all the CPT Group objects.  Default = True
        //  This means that if the user needs an US only centers that have US will show up.

            NIM3_ReferralSourceObject myRSO = NID_SubmitCaseObject.translateWebCodesToReferralSource ("","",webLink);
            bltNIM3_PayerMaster payerMaster = new bltNIM3_PayerMaster(myRSO.getPayerID());
            bltNIM3_NIDPromo nidPromo = null;
            if (myRSO.getNidPromo()!=null){
                nidPromo = new bltNIM3_NIDPromo(myRSO.getNidPromo());
            }

            Double ruralRatio = 0.0;
            int maxMiles = 200;
            Vector<String> searchNotes = new Vector<String>();
            sZIP = sZIP.trim();
//            CPTGroupObject2 myCGO2_dom = NIMUtils.getDominantCode(myCGO2_in);
            CPTGroupObject2 myCGO2 = myCGO2_in;
            String keyCheapest = null;
            Double valueCheapest = null;
            String keyClosest = null;
            Double valueClosest = null;
            String keyBestValue = null;

            Double theUC = 0.0;
            try {
                theUC = NIMUtils.getFreeSchedulePriceCPTGroup(3, 1.00, sZIP, myCGO2, new Date());
            } catch (java.sql.SQLException sql){

            }


            if (sZIP.equalsIgnoreCase("") || sZIP.length()!=5 || iMiles<=0){
                return null;
            } else {
                Hashtable<String, PracticeMaster_SearchResultsObject> myCenterList = new Hashtable<String, PracticeMaster_SearchResultsObject>();
//                Vector<PracticeMaster_SearchResultsObject> myCenterList = new Vector();
                ZIP_CODE_object myZCo = NIMUtils.get_zips_in_range(sZIP,iMiles);
                Vector myZIPVector = myZCo.getZipsInRange(iMiles);
                int i2cnt=0;
                String myZip = "";
                String myZipSQL = "";
                for (int i10=0;i10<myZIPVector.size();i10++)
                {
                    if (i10>0)
                    {
                        myZip += ", ";
                        myZipSQL += ",";
                    }
                    myZip += "'" + (String) myZIPVector.elementAt(i10) + "'";
                    myZipSQL += "?";
                }
                if (myZip.equalsIgnoreCase(""))
                {
                    //Possible unknown ZIP, this means that we may only find centers within the same ZIP
                    myZip = "'" + sZIP + "'";
                    myZipSQL = "?";
                    DebugLogger.e("NIDUtils:getListOfCenters",":Invalid/New ZIP["+sZIP+"]");

                }
                //	searchDB2_Remote mySS = new searchDB2_Remote();
                searchDB2 mySS = new searchDB2();
                ResultSet myRS = null;
                try
                {
                    //set these abitrarily high
                    Double theCheapest = 10000.0;
                    Double theClosest = 1000.0;

                    String mySQL = null;
                    //2011-06-15 updated process
                    String addWhere = "";
                    if (limitModality){
                        if (myCGO2.isHasMRI())
                        {
                            addWhere += " AND hasmr is not null ";
                        }
                        if (myCGO2.isHasCT())
                        {
                            addWhere += " AND hasct is not null ";
                        }
                        if (myCGO2.isHasEMG())
                        {
                            addWhere += " AND hasemg is not null ";
                        }
                        if (myCGO2.isHasXR())
                        {
                            addWhere += " AND hasxr is not null ";
                        }
                        if (myCGO2.isHasUS())
                        {
                            addWhere += " AND hasus is not null ";
                        }
                        if (myCGO2.isHasNM())
                        {
                            addWhere += " AND hasnm is not null ";
                        }
                        if (myCGO2.isHasFL())
                        {
                            addWhere += " AND hasfl is not null ";
                        }
                        if (myCGO2.isHasPET())
                        {
                            addWhere += " AND haspet is not null ";
                        }
                    }
                    if (requireOpen){
                        addWhere += " AND mr_open_class>0 ";
                    }
                    mySQL = ("SELECT practiceid, hasmr, mr_open_class, mr_open_tesla, mr_trad_class, mr_trad_tesla, hasmr_acr, hasmr_acrverified, hasmr_acrverifieddate, hasmr_acrexpirationdate, hasct_acr, hasct_acrverified, hasct_acrverifieddate, hasct_acrexpirationdate, hasus_acr, hasus_acrverified, hasus_acrverifieddate, hasus_acrexpirationdate   from \"vMQ3_Practice_Services\" where substr(officezip,0,6) in (" + myZipSQL + ") and contractglobal in (0, 1) and contractingstatusid in (2,8) " +addWhere);
                    
                    mySQL = "SELECT\n" +
                    		"		pm.practiceid,\n" +
                    		"		pc.descriptionlong,\n" +
                    		"		ps.hasmr,\n" +
                    		"		ps.mr_open_class,\n" +
                    		"		ps.mr_open_tesla,\n" +
                    		"		ps.mr_trad_class,\n" +
                    		"		ps.mr_trad_tesla,\n" +
                    		"		ps.hasmr_acr,\n" +
                    		"		ps.hasmr_acrverified,\n" +
                    		"		ps.hasmr_acrverifieddate,\n" +
                    		"		ps.hasmr_acrexpirationdate,\n" +
                    		"		ps.hasct_acr,\n" +
                    		"		ps.hasct_acrverified,\n" +
                    		"		ps.hasct_acrverifieddate,\n" +
                    		"		ps.hasct_acrexpirationdate,\n" +
                    		"		ps.hasus_acr,\n" +
                    		"		ps.hasus_acrverified,\n" +
                    		"		ps.hasus_acrverifieddate,\n" +
                    		"		ps.hasus_acrexpirationdate,\n" +
                    		"		getmodality (pm.practiceid) modality,\n" +
                    		"		st.shortstate \n" +
                    		"	FROM\n" +
                    		"		\"vMQ3_Practice_Services\" ps\n" +
                    		"	join tpracticemaster pm on pm.practiceid = ps.practiceid\n" +
                    		" join tpracticecontractstatusli pc on pc.practicecontractstatusid = pm.contractingstatusid\n" +
                    		" join tstateli st on st.stateid = pm.officestateid " +
                    		"	WHERE\n" +
                    		"		substr(pm.officezip, 0, 6) IN (" + myZipSQL + ")\n" +
                    		"	AND pm.contractglobal IN (0, 1)\n" +
                    		"	AND pm.contractingstatusid in (2,8) "+addWhere;
                    
                    //SQL Prepared Statements Update 2011
                    db_PreparedStatementObject myDPSO = new db_PreparedStatementObject(mySQL);
                    if (myZIPVector.size()<=1)
                    {
                        myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,sZIP));
                    }
                    else
                    {
                        for (int i10=0;i10<myZIPVector.size();i10++)
                        {
                            myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,(String)myZIPVector.elementAt(i10)));
                        }
                    }
                    myRS = mySS.executePreparedStatement(myDPSO);

                    GEOUtils.Location myGUL_Pat = null;
                    GEOUtils.Location myGUL_Prac = null;
                    try
                    {
                        myGUL_Pat = NIMUtils.getLatLon(sZIP);
                    }
                    catch (Exception eeeeeee)
                    {
                        DebugLogger.e ("NIMUtils:getHashOfCenters:","Invalid Patient Address.[" + eeeeeee.toString()+"]");
                    }
                    if (myGUL_Pat ==null)
                    {
                        DebugLogger.e ("NIMUtils:getHashOfCenters:","Invalid Patient Address.");
                    }
                    while(myRS!=null&&myRS.next())
                    {


//                        DebugLogger.d("NID Practice Search", "Looking at Practice: ["+myRS.getInt("practiceID")+"]");
                        i2cnt++;
                        //Pull record and set the badges and stats
                        PracticeMaster_SearchResultsObject myPracM_SRO = new PracticeMaster_SearchResultsObject(myRS.getInt("practiceID"));
                        //myPracM_SRO.setContractStatus(myRS.getString("descriptionlong"));
                        //myPracM_SRO.setModality(myRS.getString("modality"));
                        //myPracM_SRO.setState(myRS.getString("shortstate"));
                        
                        if (myRS.getInt("hasmr_acr")==1){
                            myPracM_SRO.setBadge_ACR(true);
                            myPracM_SRO.setBadge_ACR_MR(true);
                            if (myRS.getInt("hasmr_acrverified")==1){
                                myPracM_SRO.badge_ACR_notes.add("ACR verified for MR");
                            }
                        }

                        if (myRS.getInt("hasct_acr")==1){
                            myPracM_SRO.setBadge_ACR(true);
                            myPracM_SRO.setBadge_ACR_CT(true);
                            if (myRS.getInt("hasct_acrverified")==1){
                                myPracM_SRO.badge_ACR_notes.add("ACR verified for CT");
                            }
                        }

                        if (myRS.getInt("hasus_acr")==1){
                            myPracM_SRO.setBadge_ACR(true);
                            myPracM_SRO.setBadge_ACR_US(true);
                            if (myRS.getInt("hasus_acrverified")==1){
                                myPracM_SRO.badge_ACR_notes.add("ACR verified for US");
                            }
                        }

                        Boolean faxPass = true;
                        if (NIMUtils.fax2EmailFax(myPracM_SRO.getPracticeMaster().getOfficeFaxNo(), "").length()!=11){
                            DebugLogger.e("NIDUtils:ImagingCenterListObject","Bad Fax ["+myPracM_SRO.getPracticeMaster().getOfficeFaxNo()+"] for practice ["+myPracM_SRO.getPracticeMaster().getUniqueID()+"]");
                            faxPass = false;
                        }

                        //double check here to ensure that contract global is not set to "no" (2)
                        if (myPracM_SRO.getPracticeMaster().getContractGlobal()!=2 && faxPass){
                            if (myCGO2_in.isHasMRI()&&myRS.getString("mr_open_class")!=null){
                                int tempMR_OpenClass = new Integer(myRS.getString("mr_open_class"));
                                if (tempMR_OpenClass>0){
                                    myPracM_SRO.setOpenMR_Tesla(new Double(myRS.getString("mr_open_tesla")));
                                    myPracM_SRO.setOpenMR(true);
                                    myPracM_SRO.setOpenMR_Class(tempMR_OpenClass);
                                } else {
                                    myPracM_SRO.setOpenMR(false);
                                }
                            } else {
                                myPracM_SRO.setOpenMR(false);
                            }
                            if (myCGO2_in.isHasMRI()&&myRS.getString("mr_trad_class")!=null){
                                int tempMR_TradClass = new Integer(myRS.getString("mr_trad_class"));
                                if (tempMR_TradClass>0){
                                    myPracM_SRO.setTradMR_Tesla(new Double(myRS.getString("mr_trad_tesla")));
                                    myPracM_SRO.setTradMR(true);
                                    myPracM_SRO.setTradMR_Class(tempMR_TradClass);
                                } else {
                                    myPracM_SRO.setTradMR(false);
                                }
                            } else {
                                myPracM_SRO.setTradMR(false);
                            }

                            //get price
                            Double theCost = NIMUtils.getCPTGroupPayPriceforPracticeMaster(myPracM_SRO.practiceMaster,myCGO2, new Date(), NIMUtils.PAYMENT_TYPE_GROUPHEALTH);
                            if (theUC!=null){
                                myPracM_SRO.setPriceUC(theUC);
                            }
                            if (theCost!=null){
                                myPracM_SRO.setPrice(theCost);
                                myPracM_SRO.setCustomerPrice(NIDUtils.getNIDFinalPrice(theCost, myPracM_SRO.practiceMaster, payerMaster, nidPromo ));

                                try
                                {
        //                        myGUL_Prac = NIMUtils.getLatLon(myPracM.getOfficeAddress1() + " " + myPracM.getOfficeCity() + " " + new bltStateLI(myPracM.getOfficeStateID()).getShortState() + " " + myPracM.getOfficeZIP());
                                    myGUL_Prac = NIMUtils.getLatLon(myPracM_SRO.practiceMaster.getOfficeZIP());
                                }
                                catch (Exception eeeeeee)
                                {
                                    DebugLogger.e ("NIMUtils:getHashOfCenters", "Invalid Center in listing.  Please notify IT and NET DEV[" + eeeeeee.toString()+"]");
                                }

                                if (myGUL_Prac ==null)
                                {
                                    DebugLogger.println ("NIMUtils:getHashOfCenters:Invalid Center Address.");
                                }

                                try
                                {
                                    double myNewD = 0;
                                    try
                                    {
                                        myNewD = NIMUtils.calculate_mileage(new Double(myGUL_Pat.lat).doubleValue(),new Double(myGUL_Prac.lat).doubleValue(),new Double(myGUL_Pat.lon).doubleValue(),new Double(myGUL_Prac.lon).doubleValue());
                                    }
                                    catch (Exception e3ee)
                                    {
                                        searchNotes.add("NIMUtils:getHashOfCenters:Error in Distance Calc for: "  + myPracM_SRO.practiceMaster.getPracticeName() + "[" + e3ee.toString()+"]");
                                    }
                                    myPracM_SRO.setDistance(myNewD);

                                    Long score = getValueScore(myCGO2_in, myPracM_SRO, ruralRatio, requireOpen);
                                    myPracM_SRO.setMACScore(new Double(score));

                                    String key_price = String.format("%09d", score) + "_" + myPracM_SRO.getPracticeMaster().getPracticeID();

                                    myPracM_SRO.setKeyScore(key_price);

                                    //if keyBestValue has not yet been set or if the current value is greater than the keyBestValue, then make we want the KeyBestValue to be the current Key
                                    if (keyBestValue==null || myPracM_SRO.getKeyScore().compareTo(keyBestValue)>0){
                                        keyBestValue = myPracM_SRO.getKeyScore();
                                    }

                                    //if valueCheapest has not yet been set or if the current value is less than than the valueCheapest, then make we want the keyCheapest to be the current Key
                                    if (valueCheapest==null || myPracM_SRO.getPrice()<valueCheapest){
                                        keyCheapest = myPracM_SRO.getKeyScore();
                                    }

                                    //if valueClosest has not yet been set or if the current value is less than than the valueClosest, then make we want the keyClosest to be the current Key
                                    if (valueClosest==null || myPracM_SRO.getDistance()<valueClosest){
                                        keyClosest = myPracM_SRO.getKeyScore();
                                    }

                                    myCenterList.put(key_price, myPracM_SRO);
                                } catch (Exception eeeeeee){
                                    searchNotes.add("NIDUtils:getHashOfCenters:1x Error in Distance Calc for practice: "  + myPracM_SRO.practiceMaster.getPracticeName() + "[" + eeeeeee.toString()+"]");
                                    DebugLogger.e("NIDUtils:getHashOfCenters", "1x Error in Distance Calc for practice: " + myPracM_SRO.practiceMaster.getPracticeName() + "[" + eeeeeee.toString() + "]");
                                }
                            }

                        } else {
                            // if Pricing fails do we need to do anything here?

                        }
                    }
                    mySS.closeAll();
                } catch (Exception eee3eeee) {
                    DebugLogger.e ("NIDUtils:getHashOfCenters","Overall fail[" + eee3eeee.toString()+"]");
                } finally {
                    mySS.closeAll();
                }


                try {
                    DebugLogger.d("NID Debug", "Size = " + myCenterList.size());
                    if (myCenterList.size()>0)
                    {
                        //finalize a few badges
                        if (keyBestValue!=null){
                            DebugLogger.d("NID Debug", "Key = " + keyBestValue);
                            if (myCenterList.containsKey(keyBestValue)){
                                PracticeMaster_SearchResultsObject temp_SRO = myCenterList.get(keyBestValue);
                                temp_SRO.setBadge_BestValue(true);
                                myCenterList.put(keyBestValue,temp_SRO);
                            } else {
                                DebugLogger.e("NID Debug", "Key not in List?!");

                            }
                        }

                        Vector<Double> priceKeys = null;
                        priceKeys = new java.util.Vector(myCenterList.keySet());
                        java.util.Collections.sort(priceKeys, java.util.Collections.reverseOrder());
                        return new ImagingCenterListObject(NIMUtils.HashToVector(myCenterList, priceKeys, MaxResults),searchNotes);

                    }
                    else
                    {
                        //let's try again but with wider range.

                        //first let's make sure we are not stuck in an infinite loop

                        if (iMiles>=maxMiles){
                            return null;
                        } else {
                            return getListOfCenters(sZIP, maxMiles, myCGO2_in, limitModality, requireOpen, MaxResults, webLink);
                        }
                    }
                } catch (Exception final_ee) {
                    DebugLogger.e ("NIDUtils:getHashOfCenters","Return Fail[" + final_ee.toString()+"]");
                }

            }
            return null;

        }
    
    public static String getPracticeActiveStatus_SQLIN()
    {
//        return "1, 2, 8, 9, 5, 10, 11";
        //only Contracted
        return "2, 8, 10, 11";
    }

    private static Long getValueScore(CPTGroupObject2 myCGO2_in, PracticeMaster_SearchResultsObject myPracM_SRO, double ruralRatio, boolean requireOpen){
        //let's factor in the Modality Quality
        long modFactor = 0;

        //if the procedure involves an MRI
        if (myCGO2_in.isHasMRI() && !requireOpen){
            if (myPracM_SRO.getBadge_SuperHighField()){
                modFactor = 150;
            } else if (myPracM_SRO.getBadge_HighField()){
                modFactor = 100;
            } else {
                modFactor = 50;
            }
        }

        int priceMultiplier = 2;
        int distanceMultiplier = 1;
        int modFactorMultiplier = 2;

        long score = 5000 - (Math.round(myPracM_SRO.getPrice()*priceMultiplier) +  Math.round(myPracM_SRO.getDistance()*distanceMultiplier) ) + (modFactor*modFactorMultiplier);

        return score;
    }




}
