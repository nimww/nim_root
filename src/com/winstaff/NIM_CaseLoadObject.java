package com.winstaff;

/**
 * Created by IntelliJ IDEA.
 * User: ste
 * Date: 8/22/11
 * Time: 4:58 PM
 * To change this template use File | Settings | File Templates.
 */
public class NIM_CaseLoadObject {
    String name;
    public int totalActive;
    public int totalNotScheduled;
    public int totalAfterAppt;
    public int totalPastDue;

    public NIM_CaseLoadObject(String name) {
        this.name = name;
    }

    public NIM_CaseLoadObject(String name, int totalActive, int totalNotScheduled, int totalAfterAppt, int totalPastDue) {
        this.name = name;
        this.totalActive = totalActive;
        this.totalNotScheduled = totalNotScheduled;
        this.totalAfterAppt = totalAfterAppt;
        this.totalPastDue = totalPastDue;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getTotalActive() {
        return totalActive;
    }

    public void setTotalActive(int totalActive) {
        this.totalActive = totalActive;
    }

    public int getTotalNotScheduled() {
        return totalNotScheduled;
    }

    public void setTotalNotScheduled(int totalNotScheduled) {
        this.totalNotScheduled = totalNotScheduled;
    }

    public int getTotalAfterAppt() {
        return totalAfterAppt;
    }

    public void setTotalAfterAppt(int totalAfterAppt) {
        this.totalAfterAppt = totalAfterAppt;
    }

    public int getTotalPastDue() {
        return totalPastDue;
    }

    public void setTotalPastDue(int totalPastDue) {
        this.totalPastDue = totalPastDue;
    }
}
