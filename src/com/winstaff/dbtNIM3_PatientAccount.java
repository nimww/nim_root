

package com.winstaff;

import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public class dbtNIM3_PatientAccount extends Object implements InttNIM3_PatientAccount
{

        db_NewBase    dbnbDB;

    public dbtNIM3_PatientAccount()
    {
        dbnbDB = new db_NewBase( "tNIM3_PatientAccount", "PatientID" );

    }    // End of default constructor

    public dbtNIM3_PatientAccount( Integer iNewID )
    {
        dbnbDB = new db_NewBase( "tNIM3_PatientAccount", "PatientID" );
        setUniqueID( iNewID );
    }    // End of Constructor knowing an ID

    public void commitData() throws SQLException
    {
            //no longer trapping/ignoring errors 2011-03-17
            dbnbDB.commitData();
    }    // End of commitData()

    public void commitDataForced() throws SQLException
    {
        dbnbDB.commitDataForced();
    }    // End of commitData()

    public Integer getUniqueID()
    {
        return dbnbDB.getUniqueID();
    }    // End of getUniqueID()

    public void setUniqueID( Integer iNewID )
    {
        dbnbDB.setUniqueID( iNewID );
    }    // End of setUniqueID()

    public void setModifyComments( String sNew )
    {
        dbnbDB.setModifyComments( sNew );
    }    // End of setUniqueID()


    public void setPatientID(Integer newValue)
    {
                dbnbDB.setFieldData( "PatientID", newValue.toString() );
    }

    public Integer getPatientID()
    {
        String           sValue = dbnbDB.getFieldData( "PatientID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setField(String fieldN, Object fieldV)
    {DebugLogger.println("setField Method not implemented yet in classNIM3_PatientAccount!");}

    public String getFieldType(String fieldN)
    {return null;}


    public void setUniqueCreateDate(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "UniqueCreateDate", formatter.format( newValue ) );
    }

    public Date getUniqueCreateDate()
    {
        String           sValue = dbnbDB.getFieldData( "UniqueCreateDate" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setUniqueModifyDate(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "UniqueModifyDate", formatter.format( newValue ) );
    }

    public Date getUniqueModifyDate()
    {
        String           sValue = dbnbDB.getFieldData( "UniqueModifyDate" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setUniqueModifyComments(String newValue)
    {
                dbnbDB.setFieldData( "UniqueModifyComments", newValue.toString() );
    }

    public String getUniqueModifyComments()
    {
        String           sValue = dbnbDB.getFieldData( "UniqueModifyComments" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setPayerID(Integer newValue)
    {
                dbnbDB.setFieldData( "PayerID", newValue.toString() );
    }

    public Integer getPayerID()
    {
        String           sValue = dbnbDB.getFieldData( "PayerID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setPayerAccountIDNumber(String newValue)
    {
                dbnbDB.setFieldData( "PayerAccountIDNumber", newValue.toString() );
    }

    public String getPayerAccountIDNumber()
    {
        String           sValue = dbnbDB.getFieldData( "PayerAccountIDNumber" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setPatientFirstName(String newValue)
    {
                dbnbDB.setFieldData( "PatientFirstName", newValue.toString() );
    }

    public String getPatientFirstName()
    {
        String           sValue = dbnbDB.getFieldData( "PatientFirstName" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setPatientLastName(String newValue)
    {
                dbnbDB.setFieldData( "PatientLastName", newValue.toString() );
    }

    public String getPatientLastName()
    {
        String           sValue = dbnbDB.getFieldData( "PatientLastName" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setPatientAccountNumber(String newValue)
    {
                dbnbDB.setFieldData( "PatientAccountNumber", newValue.toString() );
    }

    public String getPatientAccountNumber()
    {
        String           sValue = dbnbDB.getFieldData( "PatientAccountNumber" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setPatientExpirationDate(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "PatientExpirationDate", formatter.format( newValue ) );
    }

    public Date getPatientExpirationDate()
    {
        String           sValue = dbnbDB.getFieldData( "PatientExpirationDate" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setPatientIsActive(Integer newValue)
    {
                dbnbDB.setFieldData( "PatientIsActive", newValue.toString() );
    }

    public Integer getPatientIsActive()
    {
        String           sValue = dbnbDB.getFieldData( "PatientIsActive" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setPatientDOB(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "PatientDOB", formatter.format( newValue ) );
    }

    public Date getPatientDOB()
    {
        String           sValue = dbnbDB.getFieldData( "PatientDOB" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setPatientSSN(String newValue)
    {
                dbnbDB.setFieldData( "PatientSSN", newValue.toString() );
    }

    public String getPatientSSN()
    {
        String           sValue = dbnbDB.getFieldData( "PatientSSN" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setPatientGender(String newValue)
    {
                dbnbDB.setFieldData( "PatientGender", newValue.toString() );
    }

    public String getPatientGender()
    {
        String           sValue = dbnbDB.getFieldData( "PatientGender" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setPatientAddress1(String newValue)
    {
                dbnbDB.setFieldData( "PatientAddress1", newValue.toString() );
    }

    public String getPatientAddress1()
    {
        String           sValue = dbnbDB.getFieldData( "PatientAddress1" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setPatientAddress2(String newValue)
    {
                dbnbDB.setFieldData( "PatientAddress2", newValue.toString() );
    }

    public String getPatientAddress2()
    {
        String           sValue = dbnbDB.getFieldData( "PatientAddress2" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setPatientCity(String newValue)
    {
                dbnbDB.setFieldData( "PatientCity", newValue.toString() );
    }

    public String getPatientCity()
    {
        String           sValue = dbnbDB.getFieldData( "PatientCity" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setPatientStateID(Integer newValue)
    {
                dbnbDB.setFieldData( "PatientStateID", newValue.toString() );
    }

    public Integer getPatientStateID()
    {
        String           sValue = dbnbDB.getFieldData( "PatientStateID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setPatientZIP(String newValue)
    {
                dbnbDB.setFieldData( "PatientZIP", newValue.toString() );
    }

    public String getPatientZIP()
    {
        String           sValue = dbnbDB.getFieldData( "PatientZIP" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setPatientHomePhone(String newValue)
    {
                dbnbDB.setFieldData( "PatientHomePhone", newValue.toString() );
    }

    public String getPatientHomePhone()
    {
        String           sValue = dbnbDB.getFieldData( "PatientHomePhone" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setPatientWorkPhone(String newValue)
    {
                dbnbDB.setFieldData( "PatientWorkPhone", newValue.toString() );
    }

    public String getPatientWorkPhone()
    {
        String           sValue = dbnbDB.getFieldData( "PatientWorkPhone" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setPatientCellPhone(String newValue)
    {
                dbnbDB.setFieldData( "PatientCellPhone", newValue.toString() );
    }

    public String getPatientCellPhone()
    {
        String           sValue = dbnbDB.getFieldData( "PatientCellPhone" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setPatientCCAccountFullName(String newValue)
    {
                dbnbDB.setFieldData( "PatientCCAccountFullName", newValue.toString() );
    }

    public String getPatientCCAccountFullName()
    {
        String           sValue = dbnbDB.getFieldData( "PatientCCAccountFullName" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setPatientCCAccountNumber1(String newValue)
    {
                dbnbDB.setFieldData( "PatientCCAccountNumber1", newValue.toString() );
    }

    public String getPatientCCAccountNumber1()
    {
        String           sValue = dbnbDB.getFieldData( "PatientCCAccountNumber1" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setPatientCCAccountNumber2(String newValue)
    {
                dbnbDB.setFieldData( "PatientCCAccountNumber2", newValue.toString() );
    }

    public String getPatientCCAccountNumber2()
    {
        String           sValue = dbnbDB.getFieldData( "PatientCCAccountNumber2" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setPatientCCAccountType(String newValue)
    {
                dbnbDB.setFieldData( "PatientCCAccountType", newValue.toString() );
    }

    public String getPatientCCAccountType()
    {
        String           sValue = dbnbDB.getFieldData( "PatientCCAccountType" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setPatientCCAccountExpMonth(String newValue)
    {
                dbnbDB.setFieldData( "PatientCCAccountExpMonth", newValue.toString() );
    }

    public String getPatientCCAccountExpMonth()
    {
        String           sValue = dbnbDB.getFieldData( "PatientCCAccountExpMonth" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setPatientCCAccountExpYear(String newValue)
    {
                dbnbDB.setFieldData( "PatientCCAccountExpYear", newValue.toString() );
    }

    public String getPatientCCAccountExpYear()
    {
        String           sValue = dbnbDB.getFieldData( "PatientCCAccountExpYear" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setEmployerID(Integer newValue)
    {
                dbnbDB.setFieldData( "EmployerID", newValue.toString() );
    }

    public Integer getEmployerID()
    {
        String           sValue = dbnbDB.getFieldData( "EmployerID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setJobID(Integer newValue)
    {
                dbnbDB.setFieldData( "JobID", newValue.toString() );
    }

    public Integer getJobID()
    {
        String           sValue = dbnbDB.getFieldData( "JobID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setJobNotes(String newValue)
    {
                dbnbDB.setFieldData( "JobNotes", newValue.toString() );
    }

    public String getJobNotes()
    {
        String           sValue = dbnbDB.getFieldData( "JobNotes" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setSupervisorName(String newValue)
    {
                dbnbDB.setFieldData( "SupervisorName", newValue.toString() );
    }

    public String getSupervisorName()
    {
        String           sValue = dbnbDB.getFieldData( "SupervisorName" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setSupervisorPhone(String newValue)
    {
                dbnbDB.setFieldData( "SupervisorPhone", newValue.toString() );
    }

    public String getSupervisorPhone()
    {
        String           sValue = dbnbDB.getFieldData( "SupervisorPhone" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setSupervisorFax(String newValue)
    {
                dbnbDB.setFieldData( "SupervisorFax", newValue.toString() );
    }

    public String getSupervisorFax()
    {
        String           sValue = dbnbDB.getFieldData( "SupervisorFax" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setComments(String newValue)
    {
                dbnbDB.setFieldData( "Comments", newValue.toString() );
    }

    public String getComments()
    {
        String           sValue = dbnbDB.getFieldData( "Comments" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setAuditNotes(String newValue)
    {
                dbnbDB.setFieldData( "AuditNotes", newValue.toString() );
    }

    public String getAuditNotes()
    {
        String           sValue = dbnbDB.getFieldData( "AuditNotes" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

}    // End of bltNIM3_PatientAccount class definition
