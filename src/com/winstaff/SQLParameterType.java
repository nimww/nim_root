package com.winstaff;

/**
 * Created by IntelliJ IDEA.
 * User: ste
 * Date: Jun 21, 2010
 * Time: 2:11:22 PM
 * To change this template use File | Settings | File Templates.
 */
public class SQLParameterType
{

    public SQLParameterType(String name, int itype, String value) {
        Name = name;
        iType = itype;
        Value = value;
    }

    public SQLParameterType(int itype, String value) {
        iType = itype;
        Value = value;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public int getType() {
        return iType;
    }

    public void setType(int itype) {
        iType = itype;
    }

    public String getValue() {
        return Value;
    }

    public void setValue(String value) {
        Value = value;
    }

    public String Name = null;
    public String Value = null;
    public int iType = 0;
    public static int STRING_TYPE = 1;
    public static int INT_TYPE = 2;
    public static int DATE_TYPE = 3;
    public static int DOUBLE_TYPE = 4;



}
