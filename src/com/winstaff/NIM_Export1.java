package com.winstaff;

import java.io.DataOutputStream;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Vector;

/**
 * Created by IntelliJ IDEA.
 * User: ste
 * Date: 5/4/11
 * Time: 5:26 PM
 * To change this template use File | Settings | File Templates.
 */
public class NIM_Export1
{

    static public void main(String args[])
    {
        NIM_Export1 testMe = new NIM_Export1();
        try
        {
            testMe.runMe_CostSavings();
        }
        catch (Exception e)
        {
            System.out.println("Error in Thread: "+ e );
        }
    }



    public static void runMe_CostSavings()
    {
        String CS_Version = "1.0";
        searchDB2 mySDBA = new searchDB2();
        try
        {
            System.out.println("==============================================================================");
            System.out.println("Running Cost Savings Export Version " + CS_Version);
            java.text.SimpleDateFormat displayDateSDF1Full = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1Full);
            System.out.println("Start: " + displayDateSDF1Full.format(PLCUtils.getNowDate(true)));

            String mySQLA = "SELECT\n" +
                    "\"public\".\"vMQ4_Encounter_NoVoid_BP\".\"EncounterID\"\n" +
                    "FROM\n" +
                    "\"public\".\"vMQ4_Encounter_NoVoid_BP\" ";
            java.sql.ResultSet myRSA = mySDBA.executeStatement(mySQLA);
            int totalCnt=0;
            int successCnt=0;
            java.util.Vector<String> myOut = new Vector<String>();
            String myHeader = ("ScanPass\t" +
                    "EncounterID\t" +
                    "ParentPayer\t" +
                    "PayerBranch\t" +
                    "PayerID\t" +
                    "DateOfService\t" +
                    "DateOfService_iMonth\t" +
                    "DateOfService_iYear\t" +
                    "ClaimNumber\t" +
                    "Adjuster_FullName\t" +
                    "AdjusterName_BranchName\t" +
                    "AdjusterID\t" +
                    "EncounterType\t" +
                    "IsCourtesy_Status\t" +
                    "UniqueCreateDate\t" +
                    "ReceiveDate\t" +
                    "TT_Scheduled\t" +
                    "Practice_City\t" +
                    "Practice_State\t" +
                    "Practice_Zip\t" +
                    "BillDate\t" +
                    "CPT\t" +
                    "CPT_Qty\t" +
                    "AllowAmount\t" +
                    "AllowAmountAdjustment\t" +
                    "AllowAmount_Net\t" +
                    "AllowAmount_Unit_C2\t" +
                    "AllowAmount_Total_C2\t" +
                    "FeeSchedule_Unit_C2\t" +
                    "FeeSchedule_Total_C2\t" +
                    "Savings_FromFS_Net\t" +
                    "Savings_FromFS_Percentage\t" +
                    "PaidIn_Date\t" +
                    "PaidIn_Amount\t" +
                    "PaidIn_Amount_CheckCalc\t" +
                    "PaidOut_Amount_Expected\t" +
                    "Profit_Net\t" +
                    "Payment_Status\t" +
                    "FS_Warning\t" +
                    "");


            System.out.println("");
            System.out.println("Writing File");

            SimpleDateFormat    formatter1              = new SimpleDateFormat( "MM_dd_yyyy_hh_mm" );
            SimpleDateFormat formatter2              = new SimpleDateFormat( "yyyy-MM-dd_HH:mm" );
            String              newDateFileNameString   = formatter1.format( PLCUtils.getNowDate(true) );
            String sFileName = "export/Export1_" + newDateFileNameString + ".txt";
            DataOutputStream    out;
            out = new DataOutputStream( new FileOutputStream( sFileName ) );
            out.writeBytes( myHeader );
            out.writeBytes( "\n" );



            while (myRSA.next())
            {
                totalCnt++;
                /*
                if (totalCnt%3==0)
                {
                    System.out.print("\\");
                }
                else if (totalCnt%2==0)
                {
                    System.out.print(".");
                }
                else
                {
                    System.out.print("/");
                }
                */
                NIM3_EncounterObject2 myEO2 = new NIM3_EncounterObject2(new Integer(myRSA.getString("encounterid")),"CSR Load");
                NIM3_AddressObject myPractM_AO = myEO2.getAppointmentPracticeAddressObject();
                String tempEncounter_String = myEO2.getScanPass() + "\t" +
                    myEO2.getNIM3_Encounter().getEncounterID() + "\t" +
                    myEO2.getNIM3_ParentPayerMaster().getPayerName().trim() + "\t" +
                    myEO2.getNIM3_PayerMaster().getPayerName().trim() + "\t" +
                    myEO2.getNIM3_PayerMaster().getPayerID() + "\t" +
                    PLCUtils.getDisplayDate(myEO2.getNIM3_Encounter().getDateOfService(),true)+ "\t" +
                    PLCUtils.getDisplayDate(myEO2.getNIM3_Encounter().getDateOfService(),true,PLCUtils.DATE_MONTH)+ "\t" +
                    PLCUtils.getDisplayDate(myEO2.getNIM3_Encounter().getDateOfService(), true, PLCUtils.DATE_YEAR)+ "\t" +
                    myEO2.getNIM3_CaseAccount().getCaseClaimNumber().trim() + "\t" +
                    myEO2.getCase_Adjuster().getContactFirstName().trim() + " " + myEO2.getCase_Adjuster().getContactLastName().trim()+ "\t" +
                    myEO2.getCase_Adjuster().getContactFirstName().trim() + " " + myEO2.getCase_Adjuster().getContactLastName().trim() + ", " + myEO2.getNIM3_PayerMaster().getPayerName() + "\t" +
                    myEO2.getCase_Adjuster().getUserID() + "\t" +
                    myEO2.getEncounterType_Display() + "\t" +
                    myEO2.getCourtesyStatus_Display() + "\t" +
                    PLCUtils.getDisplayDate(myEO2.getNIM3_Encounter().getUniqueCreateDate(),true,PLCUtils.DATE_FULLDATE) + "\t" +
                    PLCUtils.getDisplayDate(myEO2.getNIM3_Referral().getReceiveDate(),true,PLCUtils.DATE_FULLDATE) + "\t" +
                    PLCUtils.getDisplayDate(myEO2.getNIM3_Encounter().getTimeTrack_ReqSched(),true,PLCUtils.DATE_FULLDATE) + "\t" +
                    myPractM_AO.getCity().trim() + "\t" +
                    myPractM_AO.getState() + "\t" +
                    myPractM_AO.getZIP().trim() + "\t" +
                    PLCUtils.getDisplayDate(myEO2.getNIM3_Encounter().getSentTo_Bill_Pay(),false,PLCUtils.DATE_FULLDATE) + "\t" +
                    "";


                bltNIM3_Service        working_bltNIM3_Service;
                ListElement         leCurrentElement_Service;
                java.util.Enumeration eList_Service = myEO2.getNIM3_Service_List().elements();
                while (eList_Service.hasMoreElements())
                {
                    leCurrentElement_Service    = (ListElement) eList_Service.nextElement();
                    working_bltNIM3_Service  = (bltNIM3_Service) leCurrentElement_Service.getObject();
                    if ( working_bltNIM3_Service.getServiceStatusID().intValue()==1)
                    {
                        Double temp_FeeSchedule = (NIMUtils.getFeeSchedulePrice(new Integer(1), new Double(1.00) , myPractM_AO.getZIP(),working_bltNIM3_Service.getCPT(),working_bltNIM3_Service.getCPTModifier(),myEO2.getNIM3_Encounter().getDateOfService()));
                        //Double temp_FeeSchedule = PLCUtils.DoubleRound_Dollars(NIMUtils.getFeeSchedulePrice(1,1.00, myPractM_AO.getZIP(),working_bltNIM3_Service.getCPT(),"",myEO2.getNIM3_Encounter().getDateOfService()));
                        Double temp_Allow_C2 = PLCUtils.DoubleRound_Dollars(NIMUtils.getPayerAllowAmount(myEO2.getNIM3_PayerMaster().getPayerID(), myPractM_AO.getZIP(),working_bltNIM3_Service.getCPT(),"",myEO2.getNIM3_Encounter().getDateOfService()));
                        Double temp_TotalAllow_C2 = PLCUtils.DoubleRound_Dollars(temp_Allow_C2 * working_bltNIM3_Service.getCPTQty());
                        Double temp_NetAllow = PLCUtils.DoubleRound_Dollars(working_bltNIM3_Service.getAllowAmount() + working_bltNIM3_Service.getAllowAmountAdjustment());
                        Double temp_TotalFS = PLCUtils.DoubleRound_Dollars(temp_FeeSchedule * working_bltNIM3_Service.getCPTQty());

                        String myNetProfit = "Error";
                        if (working_bltNIM3_Service.getPaidOutAmount()>0)
                        {
                            myNetProfit = "" + (temp_NetAllow - working_bltNIM3_Service.getPaidOutAmount() );
                        }
                        String ShortPay = "Not Paid";
                        if (working_bltNIM3_Service.getReceivedAmount()>0&& (temp_NetAllow-working_bltNIM3_Service.getReceivedAmount()>0))
                        {
                            ShortPay = "Short Pay";
                        }
                        else if (working_bltNIM3_Service.getReceivedAmount()>0&& (temp_NetAllow-working_bltNIM3_Service.getReceivedAmount()<0))
                        {
                            ShortPay = "Over Pay";
                        }
                        else if (working_bltNIM3_Service.getReceivedAmount()>0&&(temp_NetAllow-working_bltNIM3_Service.getReceivedAmount())==0)
                        {
                            ShortPay = "Equal Pay";
                        }
                        String FSWarning = "False";
                        if (working_bltNIM3_Service.getReceivedAmount()>0&& (temp_TotalFS-working_bltNIM3_Service.getReceivedAmount()<0))
                        {
                            FSWarning = "True";
                        }

                        String tempService_String = "" +
                            working_bltNIM3_Service.getCPT().trim() + "\t" +
                            working_bltNIM3_Service.getCPTQty() + "\t" +
                            working_bltNIM3_Service.getAllowAmount() + "\t" +
                            working_bltNIM3_Service.getAllowAmountAdjustment() + "\t" +
                            temp_NetAllow + "\t" +
                            temp_Allow_C2 + "\t" +
                            temp_TotalAllow_C2   + "\t" +
                            (temp_FeeSchedule) + "\t" +
                            temp_TotalFS + "\t" +
                            ( temp_TotalFS - temp_NetAllow ) + "\t" +
                            ( (temp_TotalFS - temp_NetAllow)/temp_TotalFS ) + "\t" +
                            PLCUtils.getDisplayDate(myEO2.getNIM3_Encounter().getTimeTrack_ReqPaidIn(), false, PLCUtils.DATE_FULLDATE) + "\t" +
                            working_bltNIM3_Service.getReceivedAmount() + "\t" +
                            (working_bltNIM3_Service.getReceivedCheck1Amount() + working_bltNIM3_Service.getReceivedCheck2Amount() + working_bltNIM3_Service.getReceivedCheck3Amount()) + "\t" +
                            working_bltNIM3_Service.getPaidOutAmount() + "\t" +   //expected
                            myNetProfit + "\t" +   //expected Profit
                            ShortPay + "\t" +   //expected Profit
                            FSWarning + "\t" +   //expected Profit
                            "";
                        //myOut.add(tempEncounter_String + tempService_String);
                        out.writeBytes( tempEncounter_String + tempService_String );
                        out.writeBytes( "\n" );
                    }
                }

            }
            mySDBA.closeAll();
            out.flush();
            System.out.println("");
            System.out.println("Finish: " + displayDateSDF1Full.format(PLCUtils.getNowDate(true)));
            System.out.println("==============================================================================");
        }
        catch(Exception e)
        {
            System.out.println(e);
        }  finally {
            mySDBA.closeAll();
        }

    }




}
