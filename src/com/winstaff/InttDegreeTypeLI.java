package com.winstaff;


import com.winstaff.dbtDegreeTypeLI;
import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public interface InttDegreeTypeLI extends dbTableInterface
{

    public void setDegreeID(Integer newValue);
    public Integer getDegreeID();
    public void setDegreeLong(String newValue);
    public String getDegreeLong();
}    // End of bltDegreeTypeLI class definition
