package com.winstaff;


import com.winstaff.dbtUserStatusLI;
import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public interface InttUserStatusLI extends dbTableInterface
{

    public void setUserStatusID(Integer newValue);
    public Integer getUserStatusID();
    public void setUserStatusLong(String newValue);
    public String getUserStatusLong();
}    // End of bltUserStatusLI class definition
