package com.winstaff;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
public class EDI_MCMC_Mod{
	private static String ln;
	public EDI_MCMC_Mod(String ln){
		EDI_MCMC_Mod.ln = ln;
	}
	private int billid;
	private int status;
	private double networkallow;
	private double networksavings;
	public EDI_MCMC_Mod(int billid, int status, double networkallow, double networksavings){
		this.billid = billid;
		this.status = status;
		this.networkallow = networkallow;
		this.networksavings = networksavings;
	}
	
	public static void main(String[] args) {
		
	}
	public void updateMCMCRow(){
		String query = "update tmcmc_retros set statusid = '" + this.status + "', networkallowance = '" + this.networkallow + "', networksavings = '" + this.networksavings + "' where mcmcid = " + this.billid ;
		searchDB2 connection = new searchDB2();
		connection.executeUpdate(query);
		connection.closeAll();
	}
	public List<mcmcModel> getMCMCList(){
		List<mcmcModel> myMCMCList = new ArrayList<mcmcModel>();
		
		String query = "SELECT * FROM tmcmc_retros as retos WHERE statusid NOT IN ('0','1','3','5') ORDER BY mcmcid DESC";
		searchDB2 connection = new searchDB2();
		ResultSet rs = connection.executeStatement(query);
		try{
			while(rs.next()){
				int i = 1;
				myMCMCList.add(new mcmcModel(rs.getInt(i++), rs.getString(i++), rs.getString(i++), rs.getString(i++), rs.getInt(i++), rs.getInt(i++), rs.getString(i++), rs.getString(i++), rs.getString(i++), rs.getString(i++), 
						rs.getString(i++), rs.getString(i++), rs.getString(i++), rs.getString(i++), rs.getString(i++), rs.getString(i++), rs.getString(i++), rs.getString(i++), rs.getString(i++), rs.getString(i++), rs.getString(i++), 
						rs.getString(i++), rs.getString(i++), rs.getString(i++), rs.getString(i++), rs.getString(i++), rs.getString(i++), rs.getString(i++), rs.getString(i++), rs.getString(i++), rs.getString(i++), rs.getString(i++), 
						rs.getString(i++), rs.getString(i++), rs.getString(i++), rs.getString(i++), rs.getString(i++), rs.getString(i++), rs.getString(i++), rs.getString(i++), rs.getString(i++), rs.getString(i++), rs.getString(i++), 
						rs.getInt(i++), rs.getString(i++), rs.getString(i++), rs.getString(i++), rs.getString(i++), rs.getString(i++), rs.getDouble(i++), rs.getDouble(i++), rs.getDouble(i++), rs.getDouble(i++), rs.getDouble(i++), 
						rs.getDouble(i++), rs.getString(i++), rs.getString(i++), rs.getString(i++), rs.getString(i++), rs.getString(i++), rs.getInt(i++)));
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			connection.closeAll();
		}
		
		return myMCMCList;
	}
	public List<lnModel> getLNModel(){
		List<lnModel> myLNList = new ArrayList<lnModel>();
		String query = "SELECT mcmcid, statusid, claimnumber, uniquecreatedate , patientfirstname, patientlastname, fstype, providercharge, stateallowance, mcmcallowance, networkallowance, networksavings,cptcode, units FROM tmcmc_retros mcmc WHERE patientlastname ILIKE '" + ln + "%' ORDER BY mcmcid DESC";
		searchDB2 conn =new searchDB2();
		ResultSet rs = conn.executeStatement(query);
		try{
			while(rs.next()){
				int j = 1;
				myLNList.add(new lnModel(rs.getInt(j++), rs.getInt(j++), rs.getString(j++), rs.getString(j++), rs.getString(j++), rs.getString(j++), rs.getString(j++), rs.getDouble(j++), rs.getDouble(j++),rs.getDouble(j++),rs.getDouble(j++), rs.getDouble(j++), rs.getString(j++), rs.getInt(j++)));
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			conn.closeAll();
		}
		return myLNList;
	}
	public class lnModel{
		int billid;
		int status;
		String claimnumber;
		String receivedate;
		String ptfirst;
		String ptlast;
		String fs;
		double providerchrg;
		double stateallow;
		double mcmcallow;
		double networkallow;
		double networksavings;
		String cpt;
		int units;
		public lnModel(int billid, int status, String claimnumber,
				String receivedate, String ptfirst, String ptlast, String fs,
				double providerchrg, double stateallow, double mcmcallow,
				double networkallow, double networksavings, String cpt,
				int units) {
			super();
			this.billid = billid;
			this.status = status;
			this.claimnumber = claimnumber;
			this.receivedate = receivedate;
			this.ptfirst = ptfirst;
			this.ptlast = ptlast;
			this.fs = fs;
			this.providerchrg = providerchrg;
			this.stateallow = stateallow;
			this.mcmcallow = mcmcallow;
			this.networkallow = networkallow;
			this.networksavings = networksavings;
			this.cpt = cpt;
			this.units = units;
		}
		public int getBillid() {
			return billid;
		}
		public void setBillid(int billid) {
			this.billid = billid;
		}
		public int getStatus() {
			return status;
		}
		public void setStatus(int status) {
			this.status = status;
		}
		public String getClaimnumber() {
			return claimnumber;
		}
		public void setClaimnumber(String claimnumber) {
			this.claimnumber = claimnumber;
		}
		public String getReceivedate() {
			return receivedate;
		}
		public void setReceivedate(String receivedate) {
			this.receivedate = receivedate;
		}
		public String getPtfirst() {
			return ptfirst;
		}
		public void setPtfirst(String ptfirst) {
			this.ptfirst = ptfirst;
		}
		public String getPtlast() {
			return ptlast;
		}
		public void setPtlast(String ptlast) {
			this.ptlast = ptlast;
		}
		public String getFs() {
			return fs;
		}
		public void setFs(String fs) {
			this.fs = fs;
		}
		public double getProviderchrg() {
			return providerchrg;
		}
		public void setProviderchrg(double providerchrg) {
			this.providerchrg = providerchrg;
		}
		public double getStateallow() {
			return stateallow;
		}
		public void setStateallow(double stateallow) {
			this.stateallow = stateallow;
		}
		public double getMcmcallow() {
			return mcmcallow;
		}
		public void setMcmcallow(double mcmcallow) {
			this.mcmcallow = mcmcallow;
		}
		public double getNetworkallow() {
			return networkallow;
		}
		public void setNetworkallow(double networkallow) {
			this.networkallow = networkallow;
		}
		public double getNetworksavings() {
			return networksavings;
		}
		public void setNetworksavings(double networksavings) {
			this.networksavings = networksavings;
		}
		public String getCpt() {
			return cpt;
		}
		public void setCpt(String cpt) {
			this.cpt = cpt;
		}
		public int getUnits() {
			return units;
		}
		public void setUnits(int units) {
			this.units = units;
		}
		
	}
	
	public class mcmcModel{
		private int mcmcid;
		private String timeIn;
		private String modTime;
		private String uniqueModCom;
		private int billId;
		private int nwbid;
		private String mcmcLoc;
		private String renderProviderTaxid;
		private String renderProviderName;
		private String renderProviderAdrs;
		private String renderProviderCity;
		private String renderProviderSt;
		private String renderProviderZip;
		private String billingProviderTaxId;
		private String billingProviderName;
		private String billingProviderAdrs;
		private String billingProviderCity;
		private String billingProviderSt;
		private String billingProviderZip; 
		private String clientName;
		private String claimNumber;
		private String ssn;
		private String patientFirstName;
		private String patientLastName;
		private String patientAdrs;
		private String patientAdrs2;
		private String patientCity;
		private String patientSt;
		private String patientZip;
		private String employerName;
		private String employerAdrs;
		private String employerAdrs2;
		private String employerCity;
		private String employerSt;
		private String employerZip;
		private String diagnosis1;
		private String diagnosis2;
		private String diagnosis3;
		private String diagnosis4;
		private String dob;
		private String doi;
		private String reviewState;
		private String reviewZip;
		private int lineId;
		private String fsType;
		private String dateOfService;
		private String cptCode;
		private String modifier1;
		private String modifier2;
		private double units;
		private double providerCharge;
		private double stateAllow;
		private double mcmcAllow;
		private double networkAllow;
		private double networkSavings;
		private String reasonCode1;
		private String reasonCode2;
		private String reasonCode3;
		private String reasonCode4;
		private String rejectdescription;
		int statusid;
		public mcmcModel(int mcmcid, String timeIn, String modTime,
				String uniqueModCom, int billId, int nwbid, String mcmcLoc,
				String renderProviderTaxid, String renderProviderName,
				String renderProviderAdrs, String renderProviderCity,
				String renderProviderSt, String renderProviderZip,
				String billingProviderTaxId, String billingProviderName,
				String billingProviderAdrs, String billingProviderCity,
				String billingProviderSt, String billingProviderZip,
				String clientName, String claimNumber, String ssn,
				String patientFirstName, String patientLastName,
				String patientAdrs, String patientAdrs2, String patientCity,
				String patientSt, String patientZip, String employerName,
				String employerAdrs, String employerAdrs2, String employerCity,
				String employerSt, String employerZip, String diagnosis1,
				String diagnosis2, String diagnosis3, String diagnosis4,
				String dob, String doi, String reviewState, String reviewZip,
				int lineId, String fsType, String dateOfService,
				String cptCode, String modifier1, String modifier2,
				double units, double providerCharge, double stateAllow,
				double mcmcAllow, double networkAllow, double networkSavings,
				String reasonCode1, String reasonCode2, String reasonCode3,
				String reasonCode4, String rejectdescription, int statusid) {
			super();
			this.mcmcid = mcmcid;
			this.timeIn = timeIn;
			this.modTime = modTime;
			this.uniqueModCom = uniqueModCom;
			this.billId = billId;
			this.nwbid = nwbid;
			this.mcmcLoc = mcmcLoc;
			this.renderProviderTaxid = renderProviderTaxid;
			this.renderProviderName = renderProviderName;
			this.renderProviderAdrs = renderProviderAdrs;
			this.renderProviderCity = renderProviderCity;
			this.renderProviderSt = renderProviderSt;
			this.renderProviderZip = renderProviderZip;
			this.billingProviderTaxId = billingProviderTaxId;
			this.billingProviderName = billingProviderName;
			this.billingProviderAdrs = billingProviderAdrs;
			this.billingProviderCity = billingProviderCity;
			this.billingProviderSt = billingProviderSt;
			this.billingProviderZip = billingProviderZip;
			this.clientName = clientName;
			this.claimNumber = claimNumber;
			this.ssn = ssn;
			this.patientFirstName = patientFirstName;
			this.patientLastName = patientLastName;
			this.patientAdrs = patientAdrs;
			this.patientAdrs2 = patientAdrs2;
			this.patientCity = patientCity;
			this.patientSt = patientSt;
			this.patientZip = patientZip;
			this.employerName = employerName;
			this.employerAdrs = employerAdrs;
			this.employerAdrs2 = employerAdrs2;
			this.employerCity = employerCity;
			this.employerSt = employerSt;
			this.employerZip = employerZip;
			this.diagnosis1 = diagnosis1;
			this.diagnosis2 = diagnosis2;
			this.diagnosis3 = diagnosis3;
			this.diagnosis4 = diagnosis4;
			this.dob = dob;
			this.doi = doi;
			this.reviewState = reviewState;
			this.reviewZip = reviewZip;
			this.lineId = lineId;
			this.fsType = fsType;
			this.dateOfService = dateOfService;
			this.cptCode = cptCode;
			this.modifier1 = modifier1;
			this.modifier2 = modifier2;
			this.units = units;
			this.providerCharge = providerCharge;
			this.stateAllow = stateAllow;
			this.mcmcAllow = mcmcAllow;
			this.networkAllow = networkAllow;
			this.networkSavings = networkSavings;
			this.reasonCode1 = reasonCode1;
			this.reasonCode2 = reasonCode2;
			this.reasonCode3 = reasonCode3;
			this.reasonCode4 = reasonCode4;
			this.rejectdescription = rejectdescription;
			this.statusid = statusid;
		}
		public int getMcmcid() {
			return mcmcid;
		}
		public void setMcmcid(int mcmcid) {
			this.mcmcid = mcmcid;
		}
		public String getTimeIn() {
			return timeIn;
		}
		public void setTimeIn(String timeIn) {
			this.timeIn = timeIn;
		}
		public String getModTime() {
			return modTime;
		}
		public void setModTime(String modTime) {
			this.modTime = modTime;
		}
		public String getUniqueModCom() {
			return uniqueModCom;
		}
		public void setUniqueModCom(String uniqueModCom) {
			this.uniqueModCom = uniqueModCom;
		}
		public int getBillId() {
			return billId;
		}
		public void setBillId(int billId) {
			this.billId = billId;
		}
		public int getNwbid() {
			return nwbid;
		}
		public void setNwbid(int nwbid) {
			this.nwbid = nwbid;
		}
		public String getMcmcLoc() {
			return mcmcLoc;
		}
		public void setMcmcLoc(String mcmcLoc) {
			this.mcmcLoc = mcmcLoc;
		}
		public String getRenderProviderTaxid() {
			return renderProviderTaxid;
		}
		public void setRenderProviderTaxid(String renderProviderTaxid) {
			this.renderProviderTaxid = renderProviderTaxid;
		}
		public String getRenderProviderName() {
			return renderProviderName;
		}
		public void setRenderProviderName(String renderProviderName) {
			this.renderProviderName = renderProviderName;
		}
		public String getRenderProviderAdrs() {
			return renderProviderAdrs;
		}
		public void setRenderProviderAdrs(String renderProviderAdrs) {
			this.renderProviderAdrs = renderProviderAdrs;
		}
		public String getRenderProviderCity() {
			return renderProviderCity;
		}
		public void setRenderProviderCity(String renderProviderCity) {
			this.renderProviderCity = renderProviderCity;
		}
		public String getRenderProviderSt() {
			return renderProviderSt;
		}
		public void setRenderProviderSt(String renderProviderSt) {
			this.renderProviderSt = renderProviderSt;
		}
		public String getRenderProviderZip() {
			return renderProviderZip;
		}
		public void setRenderProviderZip(String renderProviderZip) {
			this.renderProviderZip = renderProviderZip;
		}
		public String getBillingProviderTaxId() {
			return billingProviderTaxId;
		}
		public void setBillingProviderTaxId(String billingProviderTaxId) {
			this.billingProviderTaxId = billingProviderTaxId;
		}
		public String getBillingProviderName() {
			return billingProviderName;
		}
		public void setBillingProviderName(String billingProviderName) {
			this.billingProviderName = billingProviderName;
		}
		public String getBillingProviderAdrs() {
			return billingProviderAdrs;
		}
		public void setBillingProviderAdrs(String billingProviderAdrs) {
			this.billingProviderAdrs = billingProviderAdrs;
		}
		public String getBillingProviderCity() {
			return billingProviderCity;
		}
		public void setBillingProviderCity(String billingProviderCity) {
			this.billingProviderCity = billingProviderCity;
		}
		public String getBillingProviderSt() {
			return billingProviderSt;
		}
		public void setBillingProviderSt(String billingProviderSt) {
			this.billingProviderSt = billingProviderSt;
		}
		public String getBillingProviderZip() {
			return billingProviderZip;
		}
		public void setBillingProviderZip(String billingProviderZip) {
			this.billingProviderZip = billingProviderZip;
		}
		public String getClientName() {
			return clientName;
		}
		public void setClientName(String clientName) {
			this.clientName = clientName;
		}
		public String getClaimNumber() {
			return claimNumber;
		}
		public void setClaimNumber(String claimNumber) {
			this.claimNumber = claimNumber;
		}
		public String getSsn() {
			return ssn;
		}
		public void setSsn(String ssn) {
			this.ssn = ssn;
		}
		public String getPatientFirstName() {
			return patientFirstName;
		}
		public void setPatientFirstName(String patientFirstName) {
			this.patientFirstName = patientFirstName;
		}
		public String getPatientLastName() {
			return patientLastName;
		}
		public void setPatientLastName(String patientLastName) {
			this.patientLastName = patientLastName;
		}
		public String getPatientAdrs() {
			return patientAdrs;
		}
		public void setPatientAdrs(String patientAdrs) {
			this.patientAdrs = patientAdrs;
		}
		public String getPatientAdrs2() {
			return patientAdrs2;
		}
		public void setPatientAdrs2(String patientAdrs2) {
			this.patientAdrs2 = patientAdrs2;
		}
		public String getPatientCity() {
			return patientCity;
		}
		public void setPatientCity(String patientCity) {
			this.patientCity = patientCity;
		}
		public String getPatientSt() {
			return patientSt;
		}
		public void setPatientSt(String patientSt) {
			this.patientSt = patientSt;
		}
		public String getPatientZip() {
			return patientZip;
		}
		public void setPatientZip(String patientZip) {
			this.patientZip = patientZip;
		}
		public String getEmployerName() {
			return employerName;
		}
		public void setEmployerName(String employerName) {
			this.employerName = employerName;
		}
		public String getEmployerAdrs() {
			return employerAdrs;
		}
		public void setEmployerAdrs(String employerAdrs) {
			this.employerAdrs = employerAdrs;
		}
		public String getEmployerAdrs2() {
			return employerAdrs2;
		}
		public void setEmployerAdrs2(String employerAdrs2) {
			this.employerAdrs2 = employerAdrs2;
		}
		public String getEmployerCity() {
			return employerCity;
		}
		public void setEmployerCity(String employerCity) {
			this.employerCity = employerCity;
		}
		public String getEmployerSt() {
			return employerSt;
		}
		public void setEmployerSt(String employerSt) {
			this.employerSt = employerSt;
		}
		public String getEmployerZip() {
			return employerZip;
		}
		public void setEmployerZip(String employerZip) {
			this.employerZip = employerZip;
		}
		public String getDiagnosis1() {
			return diagnosis1;
		}
		public void setDiagnosis1(String diagnosis1) {
			this.diagnosis1 = diagnosis1;
		}
		public String getDiagnosis2() {
			return diagnosis2;
		}
		public void setDiagnosis2(String diagnosis2) {
			this.diagnosis2 = diagnosis2;
		}
		public String getDiagnosis3() {
			return diagnosis3;
		}
		public void setDiagnosis3(String diagnosis3) {
			this.diagnosis3 = diagnosis3;
		}
		public String getDiagnosis4() {
			return diagnosis4;
		}
		public void setDiagnosis4(String diagnosis4) {
			this.diagnosis4 = diagnosis4;
		}
		public String getDob() {
			return dob;
		}
		public void setDob(String dob) {
			this.dob = dob;
		}
		public String getDoi() {
			return doi;
		}
		public void setDoi(String doi) {
			this.doi = doi;
		}
		public String getReviewState() {
			return reviewState;
		}
		public void setReviewState(String reviewState) {
			this.reviewState = reviewState;
		}
		public String getReviewZip() {
			return reviewZip;
		}
		public void setReviewZip(String reviewZip) {
			this.reviewZip = reviewZip;
		}
		public int getLineId() {
			return lineId;
		}
		public void setLineId(int lineId) {
			this.lineId = lineId;
		}
		public String getFsType() {
			return fsType;
		}
		public void setFsType(String fsType) {
			this.fsType = fsType;
		}
		public String getDateOfService() {
			return dateOfService;
		}
		public void setDateOfService(String dateOfService) {
			this.dateOfService = dateOfService;
		}
		public String getCptCode() {
			return cptCode;
		}
		public void setCptCode(String cptCode) {
			this.cptCode = cptCode;
		}
		public String getModifier1() {
			return modifier1;
		}
		public void setModifier1(String modifier1) {
			this.modifier1 = modifier1;
		}
		public String getModifier2() {
			return modifier2;
		}
		public void setModifier2(String modifier2) {
			this.modifier2 = modifier2;
		}
		public double getUnits() {
			return units;
		}
		public void setUnits(double units) {
			this.units = units;
		}
		public double getProviderCharge() {
			return providerCharge;
		}
		public void setProviderCharge(double providerCharge) {
			this.providerCharge = providerCharge;
		}
		public double getStateAllow() {
			return stateAllow;
		}
		public void setStateAllow(double stateAllow) {
			this.stateAllow = stateAllow;
		}
		public double getMcmcAllow() {
			return mcmcAllow;
		}
		public void setMcmcAllow(double mcmcAllow) {
			this.mcmcAllow = mcmcAllow;
		}
		public double getNetworkAllow() {
			return networkAllow;
		}
		public void setNetworkAllow(double networkAllow) {
			this.networkAllow = networkAllow;
		}
		public double getNetworkSavings() {
			return networkSavings;
		}
		public void setNetworkSavings(double networkSavings) {
			this.networkSavings = networkSavings;
		}
		public String getReasonCode1() {
			return reasonCode1;
		}
		public void setReasonCode1(String reasonCode1) {
			this.reasonCode1 = reasonCode1;
		}
		public String getReasonCode2() {
			return reasonCode2;
		}
		public void setReasonCode2(String reasonCode2) {
			this.reasonCode2 = reasonCode2;
		}
		public String getReasonCode3() {
			return reasonCode3;
		}
		public void setReasonCode3(String reasonCode3) {
			this.reasonCode3 = reasonCode3;
		}
		public String getReasonCode4() {
			return reasonCode4;
		}
		public void setReasonCode4(String reasonCode4) {
			this.reasonCode4 = reasonCode4;
		}
		public String getRejectdescription() {
			return rejectdescription;
		}
		public void setRejectdescription(String rejectdescription) {
			this.rejectdescription = rejectdescription;
		}
		public int getStatusid() {
			return statusid;
		}
		public void setStatusid(int statusid) {
			this.statusid = statusid;
		}
		public class getResultsByLn{
			int mcmcbillid;
			int statusid;
			String claimNumber;
			String uniqueCreateDate;
			String patientfirstname;
			String patientlastname;
			int fstype;
			double providercharge;
			double stateallowance;
			double mcmcallowance;
			double networkallowance;
			double networksavings;
			String cptcode;
			int units;
			public getResultsByLn(int mcmcbillid, int statusid,
					String claimNumber, String uniqueCreateDate,
					String patientfirstname, String patientlastname,
					int fstype, double providercharge, double stateallowance,
					double mcmcallowance, double networkallowance,
					double networksavings, String cptcode, int units) {
				super();
				this.mcmcbillid = mcmcbillid;
				this.statusid = statusid;
				this.claimNumber = claimNumber;
				this.uniqueCreateDate = uniqueCreateDate;
				this.patientfirstname = patientfirstname;
				this.patientlastname = patientlastname;
				this.fstype = fstype;
				this.providercharge = providercharge;
				this.stateallowance = stateallowance;
				this.mcmcallowance = mcmcallowance;
				this.networkallowance = networkallowance;
				this.networksavings = networksavings;
				this.cptcode = cptcode;
				this.units = units;
			}
			public int getMcmcbillid() {
				return mcmcbillid;
			}
			public void setMcmcbillid(int mcmcbillid) {
				this.mcmcbillid = mcmcbillid;
			}
			public int getStatusid() {
				return statusid;
			}
			public void setStatusid(int statusid) {
				this.statusid = statusid;
			}
			public String getClaimNumber() {
				return claimNumber;
			}
			public void setClaimNumber(String claimNumber) {
				this.claimNumber = claimNumber;
			}
			public String getUniqueCreateDate() {
				return uniqueCreateDate;
			}
			public void setUniqueCreateDate(String uniqueCreateDate) {
				this.uniqueCreateDate = uniqueCreateDate;
			}
			public String getPatientfirstname() {
				return patientfirstname;
			}
			public void setPatientfirstname(String patientfirstname) {
				this.patientfirstname = patientfirstname;
			}
			public String getPatientlastname() {
				return patientlastname;
			}
			public void setPatientlastname(String patientlastname) {
				this.patientlastname = patientlastname;
			}
			public int getFstype() {
				return fstype;
			}
			public void setFstype(int fstype) {
				this.fstype = fstype;
			}
			public double getProvidercharge() {
				return providercharge;
			}
			public void setProvidercharge(double providercharge) {
				this.providercharge = providercharge;
			}
			public double getStateallowance() {
				return stateallowance;
			}
			public void setStateallowance(double stateallowance) {
				this.stateallowance = stateallowance;
			}
			public double getMcmcallowance() {
				return mcmcallowance;
			}
			public void setMcmcallowance(double mcmcallowance) {
				this.mcmcallowance = mcmcallowance;
			}
			public double getNetworkallowance() {
				return networkallowance;
			}
			public void setNetworkallowance(double networkallowance) {
				this.networkallowance = networkallowance;
			}
			public double getNetworksavings() {
				return networksavings;
			}
			public void setNetworksavings(double networksavings) {
				this.networksavings = networksavings;
			}
			public String getCptcode() {
				return cptcode;
			}
			public void setCptcode(String cptcode) {
				this.cptcode = cptcode;
			}
			public int getUnits() {
				return units;
			}
			public void setUnits(int units) {
				this.units = units;
			}
		}
	}
}