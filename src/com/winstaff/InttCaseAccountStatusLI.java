

package com.winstaff;

import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public interface InttCaseAccountStatusLI extends dbTableInterface
{

    public void setCaseStatusID(Integer newValue);
    public Integer getCaseStatusID();
    public void setCaseStatusShort(String newValue);
    public String getCaseStatusShort();
    public void setCaseStatusLong(String newValue);
    public String getCaseStatusLong();
}    // End of bltCaseAccountStatusLI class definition
