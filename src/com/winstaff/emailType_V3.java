package com.winstaff;

import java.awt.*;
import java.net.*;
import java.util.*;
import java.io.*;
import java.text.*;
import javax.mail.*;
import javax.mail.internet.*;
import javax.activation.*;

public class emailType_V3 extends java.lang.Object {

	public emailType_V3() {
	}

	public void setTo(String to) {
		this.to = to;
	}

	public String getTo() {
		return this.to;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getFrom() {
		return this.from;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getSubject() {
		return this.subject;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public String getBody() {
		return this.body;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public String getHost() {
		return this.host;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getMessage() {
		return this.message;
	}

	public Integer isSendMail_Track() {
		Integer myValue = null;
		String myMessage = "";
		boolean debug = false;
		try {
			bltEmailTransaction myET = new bltEmailTransaction();
			myET.setEmailTo(this.to);
			myET.setEmailFrom(this.from);
			myET.setEmailSubject(this.subject);
			myET.setEmailBody(this.body);
			myET.setEmailBodyType(this.bodyType);
			myET.setEmailImportance(new Integer(1));
			myET.setActionID(new Integer(0));
			myET.setTransactionDate(new java.util.Date());
			myET.setVirtualAttachment(this.virtualattachment);
			if (this.cc.size() > 0) {
				myET.setEmailCC1((String) this.cc.elementAt(0));
			}
			if (this.cc.size() > 1) {
				myET.setEmailCC2((String) this.cc.elementAt(1));
			}
			if (this.cc.size() > 2) {
				myET.setEmailCC3((String) this.cc.elementAt(2));
			}
			if (this.cc.size() > 3) {
				myET.setEmailCC4((String) this.cc.elementAt(3));
			}
			if (this.cc.size() > 4) {
				myET.setEmailCC5((String) this.cc.elementAt(4));
			}
			myET.commitData();
			myValue = myET.getUniqueID();
		} catch (Exception eee) {
			DebugLogger.println("failed: Error: " + eee.toString());
			myValue = null;
		}
		return myValue;
	}
	
	public Integer prepFaxageAPI() {
		Integer myValue = null;
		String myMessage = "";
		boolean debug = false;
		try {
			bltEmailTransaction myET = new bltEmailTransaction();
			myET.setEmailTo(this.to);
			myET.setEmailFrom(this.from);
			myET.setEmailSubject(this.subject);
			myET.setEmailBody(this.body);
			myET.setEmailBodyType(this.bodyType);
			myET.setEmailImportance(new Integer(1));
			myET.setActionID(new Integer(33));
			myET.setTransactionDate(new java.util.Date());
			myET.setVirtualAttachment(this.virtualattachment);
			if (this.cc.size() > 0) {
				myET.setEmailCC1((String) this.cc.elementAt(0));
			}
			if (this.cc.size() > 1) {
				myET.setEmailCC2((String) this.cc.elementAt(1));
			}
			if (this.cc.size() > 2) {
				myET.setEmailCC3((String) this.cc.elementAt(2));
			}
			if (this.cc.size() > 3) {
				myET.setEmailCC4((String) this.cc.elementAt(3));
			}
			if (this.cc.size() > 4) {
				myET.setEmailCC5((String) this.cc.elementAt(4));
			}
			myET.commitData();
			myValue = myET.getUniqueID();
		} catch (Exception eee) {
			DebugLogger.println("failed: Error: " + eee.toString());
			myValue = null;
		}
		return myValue;
	}

	public boolean isSendMail() {
		boolean myValue = false;
		String myMessage = "";
		boolean debug = false;
		try {
			bltEmailTransaction myET = new bltEmailTransaction();
			myET.setEmailTo(this.to);
			myET.setEmailFrom(this.from);
			myET.setEmailSubject(this.subject);
			myET.setEmailBody(this.body);
			myET.setEmailBodyType(this.bodyType);
			myET.setEmailImportance(new Integer(1));
			myET.setActionID(new Integer(0));
			myET.setTransactionDate(new java.util.Date());
			myET.setVirtualAttachment(this.virtualattachment);

			if (this.cc.size() > 0) {
				myET.setEmailCC1((String) this.cc.elementAt(0));
			}
			if (this.cc.size() > 1) {
				myET.setEmailCC2((String) this.cc.elementAt(1));
			}
			if (this.cc.size() > 2) {
				myET.setEmailCC3((String) this.cc.elementAt(2));
			}
			if (this.cc.size() > 3) {
				myET.setEmailCC4((String) this.cc.elementAt(3));
			}
			if (this.cc.size() > 4) {
				myET.setEmailCC5((String) this.cc.elementAt(4));
			}
			myET.commitData();
			myValue = true;
		} catch (Exception eee) {
			DebugLogger.println("failed: Error: " + eee.toString());

			myValue = false;
		}
		return myValue;
	}

	public void setBodyType(String bodyType) {
		this.bodyType = bodyType;
	}

	public String getBodyType() {
		return this.bodyType;
	}

	public boolean isSendMail_Direct2() {
		try {
			// String host = "mail.nim.winstaff.com";
			// String from = "server@nim.winstaff.com";
			// String ReplyT = "scott@nextimagemedical.com";
			// String pass = "wNdpILe6";

			/*
			 * this.host = "mail.nextimagemedical.com"; this.user =
			 * "serversend"; this.pswd = "sS123456"; this.hostport="25";
			 */
			System.out.println("Sending mail...");
			Properties props = new Properties();
			props.setProperty("mail.transport.protocol", "smtp");
			props.setProperty("mail.smtp.host", this.host);
			props.setProperty("mail.smtp.user", this.user);
			props.setProperty("mail.smtp.password", this.pswd);
			props.setProperty("mail.smtp.port", this.hostport);
			// props.setProperty("mail.password", "wNdpILe6");
			props.setProperty("mail.smtp.auth", "true");
			// props.put("mail.smtp.starttls.enable", "true"); // added this
			// line
			String[] to = { this.to }; // added this line

			Session session = Session.getDefaultInstance(props, null);
			MimeMessage message = new MimeMessage(session);
			message.setFrom(new InternetAddress(this.from));

			InternetAddress[] toAddress = new InternetAddress[to.length];

			// To get the array of addresses
			for (int i = 0; i < to.length; i++) { // changed from a while loop
				toAddress[i] = new InternetAddress(to[i]);
			}
			// System.out.println(Message.RecipientType.TO);

			for (int i = 0; i < toAddress.length; i++) { // changed from a while
															// loop
				message.addRecipient(Message.RecipientType.TO, toAddress[i]);
			}

			// adding CC via Vector
			InternetAddress[] ccAddress = new InternetAddress[cc.size()];

			// To get the array of addresses
			for (int i = 0; i < cc.size(); i++) { // changed from a while loop
				ccAddress[i] = new InternetAddress((String) cc.elementAt(i));
			}
			// System.out.println(Message.RecipientType.TO);
			for (int i = 0; i < cc.size(); i++) { // changed from a while loop
				message.addRecipient(Message.RecipientType.CC, ccAddress[i]);
			}

			message.setSubject(this.subject);
			if (!this.virtualattachment.equalsIgnoreCase("")) {
				// create and fill the first message part
				MimeBodyPart mbp1 = new MimeBodyPart();
				mbp1.setText(this.getBody());

				// create the second message part
				MimeBodyPart mbp2 = new MimeBodyPart();

				// attach the file to the message

				if (ConfigurationInformation.storeDocumentsInDatabase) {

					// Get BLOB data from database for this specific filename.
					DocumentDatabase db = new DocumentDatabase();
					byte[] data = db.readFile(virtualattachment);
					mbp2.setDataHandler(new DataHandler(data,
							"application/pdf"));
					// Name the attachment for fax sending.  email attachments work without this statement.  (weird)
					mbp2.setFileName(virtualattachment);

				} else {

					FileDataSource fds = new FileDataSource(
							ConfigurationInformation.sUploadFolderDirectory
									+ this.virtualattachment);
					mbp2.setDataHandler(new DataHandler(fds));
					mbp2.setFileName(fds.getName());

				}

				// create the Multipart and add its parts to it
				Multipart mp = new MimeMultipart();
				mp.addBodyPart(mbp1);
				mp.addBodyPart(mbp2);
				message.setContent(mp);
			} else if (this.getBodyType() != null
					&& this.getBodyType().equalsIgnoreCase(this.HTML_TYPE)) {
				message.setContent(this.getBody(), this.getBodyType());
			} else {
				message.setText(this.getBody());
			}
			// message.setText(this.body);
			Transport transport = session.getTransport("smtp");
			transport.connect(this.host, this.user, this.pswd);
			transport.sendMessage(message, message.getAllRecipients());
			transport.close();

			/*
			 * Session mailSession = Session.getDefaultInstance(props, null);
			 * mailSession.setDebug(true); Transport transport =
			 * mailSession.getTransport();
			 * 
			 * MimeMessage message = new MimeMessage(mailSession);
			 * 
			 * message.setFrom( new InternetAddress( this.getFrom() ) );
			 * //InternetAddress[] address = { new InternetAddress( this.getTo()
			 * ) }; message.addRecipient(Message.RecipientType.TO, new
			 * InternetAddress(this.getTo())); message.setSubject(
			 * this.getSubject() ); message.setSentDate( new Date() ); if
			 * (this.getBodyType
			 * ()!=null&&this.getBodyType().equalsIgnoreCase(this.HTML_TYPE)) {
			 * message.setContent( this.getBody(),this.getBodyType()); } else {
			 * message.setText(this.getBody()); }
			 * 
			 * Transport transport = session.getTransport("smtp");
			 * transport.connect(host, from, pass);
			 * transport.sendMessage(message, message.getAllRecipients());
			 * transport.close();
			 * 
			 * 
			 * // transport.connect(); // transport.sendMessage(message,
			 * message.getRecipients(Message.RecipientType.TO)); //
			 * transport.close();
			 */

			return true;
		} catch (Exception eee) {
			DebugLogger.println("emailType_V3:DirectSend2: " + eee.toString());
			return false;
		}
	}

	
	public boolean isSendMail_Direct2_test() {
		try {
			// String host = "mail.nim.winstaff.com";
			// String from = "server@nim.winstaff.com";
			// String ReplyT = "scott@nextimagemedical.com";
			// String pass = "wNdpILe6";

			/*
			 * this.host = "mail.nextimagemedical.com"; this.user =
			 * "serversend"; this.pswd = "sS123456"; this.hostport="25";
			 */
			System.out.println("Sending mail...");
			Properties props = new Properties();
			props.setProperty("mail.transport.protocol", "smtp");
			props.setProperty("mail.smtp.host", this.host);
			props.setProperty("mail.smtp.user", this.user);
			props.setProperty("mail.smtp.password", this.pswd);
			props.setProperty("mail.smtp.port", this.hostport);
			// props.setProperty("mail.password", "wNdpILe6");
			props.setProperty("mail.smtp.auth", "true");
			// props.put("mail.smtp.starttls.enable", "true"); // added this
			// line
			String[] to = { this.to }; // added this line

			Session session = Session.getDefaultInstance(props, null);
			MimeMessage message = new MimeMessage(session);
			message.setFrom(new InternetAddress(this.from));

			InternetAddress[] toAddress = new InternetAddress[to.length];

			// To get the array of addresses
			for (int i = 0; i < to.length; i++) { // changed from a while loop
				toAddress[i] = new InternetAddress(to[i]);
			}
			// System.out.println(Message.RecipientType.TO);

			for (int i = 0; i < toAddress.length; i++) { // changed from a while
															// loop
				message.addRecipient(Message.RecipientType.TO, toAddress[i]);
			}

			// adding CC via Vector
			InternetAddress[] ccAddress = new InternetAddress[cc.size()];

			// To get the array of addresses
			for (int i = 0; i < cc.size(); i++) { // changed from a while loop
				ccAddress[i] = new InternetAddress((String) cc.elementAt(i));
			}
			// System.out.println(Message.RecipientType.TO);
			for (int i = 0; i < cc.size(); i++) { // changed from a while loop
				message.addRecipient(Message.RecipientType.CC, ccAddress[i]);
			}

			message.setSubject(this.subject);
			if (!this.virtualattachment.equalsIgnoreCase("")) {
				// create and fill the first message part
				MimeBodyPart mbp1 = new MimeBodyPart();
				mbp1.setText(this.getBody());

				// create the second message part
				MimeBodyPart mbp2 = new MimeBodyPart();

				// attach the file to the message

				if (ConfigurationInformation.storeDocumentsInDatabase) {

					// Get BLOB data from database for this specific filename.
					DocumentDatabase db = new DocumentDatabase();
					byte[] data = db.readFile(virtualattachment);
					mbp2.setDataHandler(new DataHandler(data,
							"application/pdf"));
//				"application/octet-stream"));

				} else {

					FileDataSource fds = new FileDataSource(
							ConfigurationInformation.sUploadFolderDirectory
									+ this.virtualattachment);
					mbp2.setDataHandler(new DataHandler(fds));
					mbp2.setFileName(fds.getName());

				}

				// create the Multipart and add its parts to it
				Multipart mp = new MimeMultipart();
				mp.addBodyPart(mbp1);
				mp.addBodyPart(mbp2);
				message.setContent(mp);
			} else if (this.getBodyType() != null
					&& this.getBodyType().equalsIgnoreCase(this.HTML_TYPE)) {
				message.setContent(this.getBody(), this.getBodyType());
			} else {
				message.setText(this.getBody());
			}
			// message.setText(this.body);
			Transport transport = session.getTransport("smtp");
			transport.connect(this.host, this.user, this.pswd);
			transport.sendMessage(message, message.getAllRecipients());
			transport.close();

			/*
			 * Session mailSession = Session.getDefaultInstance(props, null);
			 * mailSession.setDebug(true); Transport transport =
			 * mailSession.getTransport();
			 * 
			 * MimeMessage message = new MimeMessage(mailSession);
			 * 
			 * message.setFrom( new InternetAddress( this.getFrom() ) );
			 * //InternetAddress[] address = { new InternetAddress( this.getTo()
			 * ) }; message.addRecipient(Message.RecipientType.TO, new
			 * InternetAddress(this.getTo())); message.setSubject(
			 * this.getSubject() ); message.setSentDate( new Date() ); if
			 * (this.getBodyType
			 * ()!=null&&this.getBodyType().equalsIgnoreCase(this.HTML_TYPE)) {
			 * message.setContent( this.getBody(),this.getBodyType()); } else {
			 * message.setText(this.getBody()); }
			 * 
			 * Transport transport = session.getTransport("smtp");
			 * transport.connect(host, from, pass);
			 * transport.sendMessage(message, message.getAllRecipients());
			 * transport.close();
			 * 
			 * 
			 * // transport.connect(); // transport.sendMessage(message,
			 * message.getRecipients(Message.RecipientType.TO)); //
			 * transport.close();
			 */

			return true;
		} catch (Exception eee) {
			DebugLogger.println("emailType_V3:DirectSend2: " + eee.toString());
			return false;
		}
	}
	
	
	public boolean isSendMail_Direct3()// not currently working
	{
		try {
			// String host = "mail.nim.winstaff.com";
			// String from = "server@nim.winstaff.com";
			// String ReplyT = "scott@nextimagemedical.com";
			// String pass = "wNdpILe6";

			System.out.println("Sending mail...");
			Properties props = new Properties();
			props.setProperty("mail.transport.protocol", "smtp");
			this.host = "remote.nextimagemedical.com";
			this.user = "serversend";
			this.pswd = "sS123456";
			this.hostport = "25";
			props.setProperty("mail.smtp.host", this.host);
			props.setProperty("mail.smtp.user", this.user);
			props.setProperty("mail.smtp.password", this.pswd);
			props.setProperty("mail.smtp.port", this.hostport);
			// props.setProperty("mail.password", "wNdpILe6");
			props.setProperty("mail.smtp.auth", "true");

			props.put("mail.smtp.starttls.enable", "true"); // added this line
															// //for TLS
			props.put("mail.smtp.socketFactory.port", this.hostport); // for TLS
			props.put("mail.smtp.socketFactory.class",
					"javax.net.ssl.SSLSocketFactory"); // for TLS
			props.put("mail.smtp.socketFactory.fallback", "false"); // for TLS

			SecurityManager security = System.getSecurityManager(); // for TLS
			javax.mail.Authenticator auth = new SMTPAuthenticator(); // for TLS

			String[] to = { this.to }; // added this line

			// Session session = Session.getDefaultInstance(props, null);
			Session session = Session.getInstance(props, auth); // for TLS
			MimeMessage message = new MimeMessage(session);

			message.setFrom(new InternetAddress(this.from));

			InternetAddress[] toAddress = new InternetAddress[to.length];

			// To get the array of addresses
			for (int i = 0; i < to.length; i++) { // changed from a while loop
				toAddress[i] = new InternetAddress(to[i]);
			}
			System.out.println(Message.RecipientType.TO);

			for (int i = 0; i < toAddress.length; i++) { // changed from a while
															// loop
				message.addRecipient(Message.RecipientType.TO, toAddress[i]);
			}
			message.setSubject(this.subject);
			if (!this.virtualattachment.equalsIgnoreCase("")) {
				// create and fill the first message part
				MimeBodyPart mbp1 = new MimeBodyPart();
				mbp1.setText(this.getBody());

				// create the second message part
				MimeBodyPart mbp2 = new MimeBodyPart();

				// attach the file to the message
				FileDataSource fds = new FileDataSource(
						ConfigurationInformation.sUploadFolderDirectory
								+ this.virtualattachment);
				mbp2.setDataHandler(new DataHandler(fds));
				mbp2.setFileName(fds.getName());
				// create the Multipart and add its parts to it
				Multipart mp = new MimeMultipart();
				mp.addBodyPart(mbp1);
				mp.addBodyPart(mbp2);
				message.setContent(mp);
			} else if (this.getBodyType() != null
					&& this.getBodyType().equalsIgnoreCase(this.HTML_TYPE)) {
				message.setContent(this.getBody(), this.getBodyType());
			} else {
				message.setText(this.getBody());
			}
			// message.setText(this.body);
			Transport transport = session.getTransport("smtp");
			transport.connect(this.host, this.user, this.pswd);
			transport.sendMessage(message, message.getAllRecipients());
			transport.close();

			/*
			 * Session mailSession = Session.getDefaultInstance(props, null);
			 * mailSession.setDebug(true); Transport transport =
			 * mailSession.getTransport();
			 * 
			 * MimeMessage message = new MimeMessage(mailSession);
			 * 
			 * message.setFrom( new InternetAddress( this.getFrom() ) );
			 * //InternetAddress[] address = { new InternetAddress( this.getTo()
			 * ) }; message.addRecipient(Message.RecipientType.TO, new
			 * InternetAddress(this.getTo())); message.setSubject(
			 * this.getSubject() ); message.setSentDate( new Date() ); if
			 * (this.getBodyType
			 * ()!=null&&this.getBodyType().equalsIgnoreCase(this.HTML_TYPE)) {
			 * message.setContent( this.getBody(),this.getBodyType()); } else {
			 * message.setText(this.getBody()); }
			 * 
			 * Transport transport = session.getTransport("smtp");
			 * transport.connect(host, from, pass);
			 * transport.sendMessage(message, message.getAllRecipients());
			 * transport.close();
			 * 
			 * 
			 * // transport.connect(); // transport.sendMessage(message,
			 * message.getRecipients(Message.RecipientType.TO)); //
			 * transport.close();
			 */

			return true;
		} catch (Exception eee) {
			DebugLogger.println(eee.toString());
			return false;
		}
	}

	public boolean isSendMail_Direct() {
		boolean myValue = false;
		String myMessage = "";
		boolean debug = false;

		try {
			// create some properties and get the default Session
			Properties props = new Properties();
			// props.put( "mail.smtp.host", this.getHost() );
			props.put("mail.smtp.host", ConfigurationInformation.SMTPServer);
			if (debug) {
				props.put("mail.debug", "true");
			}
			Session ses = Session.getDefaultInstance(props, null);
			ses.setDebug(debug);
			try {
				MimeMessage msg = new MimeMessage(ses);
				msg.setFrom(new InternetAddress(this.getFrom()));
				InternetAddress[] address = { new InternetAddress(this.getTo()) };
				msg.setRecipients(Message.RecipientType.TO, address);
				msg.setSubject(this.getSubject());
				msg.setSentDate(new Date());
				if (this.getBodyType() != null
						&& this.getBodyType().equalsIgnoreCase(this.HTML_TYPE)) {
					msg.setContent(this.getBody(), this.getBodyType());
				} else {
					msg.setText(this.getBody());
				}
				Transport.send(msg);
				myMessage = ("SMTP Service reached and mail sent." + msg
						.getContentType());
				myValue = true;
			} catch (Exception e) {
				myMessage = ("failed: Error: " + e.toString());
				DebugLogger.printLine("failed: Error: " + e.toString());
				myValue = false;

				try {
					Message msg = new MimeMessage(ses);

					props.put("mail.smtp.host", "127.0.0.1");
					msg.setFrom(new InternetAddress("test25@emailalterego.com"));
					InternetAddress[] address = { new InternetAddress(
							"scottellis42@hotmail.com") };
					msg.setRecipients(Message.RecipientType.TO, address);
					msg.setSubject("Critical level 2");
					msg.setSentDate(new Date());
					msg.setText("email can't send");
					Transport.send(msg);
					myMessage = ("SMTP Service reached and mail sent.");
					myValue = true;
				} catch (Exception e545) {
					myMessage += ("failed: Error: " + e545.toString());
					myValue = false;
				}

			}
			this.setMessage(myMessage);
		} catch (Exception e) {
			myMessage = ("failed: Error: " + e.toString());
			myValue = false;
		}
		return myValue;
	}

	public String getVirtualAttachment() {
		return virtualattachment;
	}

	public void setVirtualAttachment(String virtualattachment) {
		this.virtualattachment = virtualattachment;
	}

	protected String body = "ALERT -- Body";
	protected String subject = "ALERT";
	protected String host = ConfigurationInformation.SMTPServerHost;
	protected String user = ConfigurationInformation.SMTPServerUser;
	protected String pswd = ConfigurationInformation.SMTPServerPass;
	protected String hostport = ConfigurationInformation.SMTPServerHostPort;
	protected String to = "scottellis42@hotmail.com";
	public java.util.Vector cc = new java.util.Vector();
	protected String from = "scott@nextimagemedical.com";
	protected String message = "Message";

//	public static String FAX_SUFFIX = "@efaxsend.com";
	public static String FAX_SUFFIX = "@faxage.com";
	protected String virtualattachment = "";
	public static String PLAIN_TYPE = "text/plain; charset=us-ascii";
	public static String HTML_TYPE = "text/html; charset=us-ascii";
	protected String bodyType = PLAIN_TYPE;

	private class SMTPAuthenticator extends javax.mail.Authenticator {
		public javax.mail.PasswordAuthentication getPasswordAuthentication() {
			return new javax.mail.PasswordAuthentication(user, pswd);
		}
	}

}
