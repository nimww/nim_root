package com.winstaff;

import java.sql.SQLException;

public class ReferralByDay{
	public static void email(String emailTo, String emailFrom, String theSubject, String theBody) throws SQLException{
		bltEmailTransaction et = new bltEmailTransaction();
		et.setEmailTo(emailTo);
		et.setEmailFrom(emailFrom);
		et.setEmailSubject(theSubject);
		et.setEmailBody(theBody);
		et.setEmailBodyType(emailType_V3.HTML_TYPE);
		et.setTransactionDate(new java.util.Date());
		et.commitData();
	}
	
	public static void main(String[]args) throws SQLException{
		String query = "SELECT * FROM \"dailyNumbers\"";
		String theBody = "";
		searchDB2 conn = new searchDB2();
		java.sql.ResultSet rs = conn.executeStatement(query);
		try{
			theBody += "<table border=2 style=\"font:Helvetica;border-collapse:collapse;\"><tr>";
			theBody += "<th align=\"left\" style=\"padding:2px 5px\">Receive Date</th>";
			theBody += "<th align=\"left\" style=\"padding:2px 5px\">Payer</th>";
			theBody += "<th align=\"left\" style=\"padding:2px 5px\">Referral Source First</th>";
			theBody += "<th align=\"left\" style=\"padding:2px 5px\">Referral Source Last</th>";
			theBody += "<th align=\"left\" style=\"padding:2px 5px\">Referral Source email</th>";
			theBody += "</tr>";
			
			while(rs != null && rs.next()){
				theBody += "<tr>";
					theBody += "<td align=\"left\" style=\"padding:2px 5px\"><small>" + rs.getString("Referral_ReceiveDate") + "</small></td>";
					theBody += "<td align=\"left\" style=\"padding:2px 5px\"><small>" + rs.getString("PayerName") + "</small></td>";
					theBody += "<td align=\"left\" style=\"padding:2px 5px\"><small>" + rs.getString("UA_ReferralSource_FirstName") + "</small></td>";
					theBody += "<td align=\"left\" style=\"padding:2px 5px\"><small>" + rs.getString("UA_ReferralSource_LastName") + "</small></td>";
					theBody += "<td align=\"left\" style=\"padding:2px 5px\"><small>" + rs.getString("UA_ReferralSource_Email") + "</small></td>";
				theBody += "</tr>";
			}
			theBody += "</table> \n";
		}catch(Exception e){theBody = e.toString();}
		conn.closeAll();
		String theSubject = "Referrals by Day";
		email("ReferralsByDay@nextimagemedical.com","support@nextimagemedical.com",theSubject,theBody);
		//System.out.print(theBody);
	}
}