

package com.winstaff;

import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public interface InttNIM3_EmployerAccount extends dbTableInterface
{

    public void setEmployerID(Integer newValue);
    public Integer getEmployerID();
    public void setUniqueCreateDate(Date newValue);
    public Date getUniqueCreateDate();
    public void setUniqueModifyDate(Date newValue);
    public Date getUniqueModifyDate();
    public void setUniqueModifyComments(String newValue);
    public String getUniqueModifyComments();
    public void setPayerID(Integer newValue);
    public Integer getPayerID();
    public void setEmployerName(String newValue);
    public String getEmployerName();
    public void setEmployerAddress1(String newValue);
    public String getEmployerAddress1();
    public void setEmployerAddress2(String newValue);
    public String getEmployerAddress2();
    public void setEmployerCity(String newValue);
    public String getEmployerCity();
    public void setEmployerStateID(Integer newValue);
    public Integer getEmployerStateID();
    public void setEmployerZIP(String newValue);
    public String getEmployerZIP();
    public void setEmployerFax(String newValue);
    public String getEmployerFax();
    public void setEmployerWorkPhone(String newValue);
    public String getEmployerWorkPhone();
    public void setEmployerCellPhone(String newValue);
    public String getEmployerCellPhone();
    public void setComments(String newValue);
    public String getComments();
    public void setAuditNotes(String newValue);
    public String getAuditNotes();
}    // End of bltNIM3_EmployerAccount class definition
