

package com.winstaff;

import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public interface InttAdminPracticeRelationshipTypeLI extends dbTableInterface
{

    public void setAdminPracticeRelationshipTypeID(Integer newValue);
    public Integer getAdminPracticeRelationshipTypeID();
    public void setDescriptionLong(String newValue);
    public String getDescriptionLong();
}    // End of bltAdminPracticeRelationshipTypeLI class definition
