package com.winstaff;

/**
 * Created by IntelliJ IDEA.
 * User: ste
 * Date: 1/8/12
 * Time: 1:23 PM
 * To change this template use File | Settings | File Templates.
 */
public class PracticeMaster_SearchResultsObject {

    public bltPracticeMaster  practiceMaster;
    private Double price;
    private Double customerPrice=0.0;
    private Double priceUC = 0.0;
    private Double distance;
    private Boolean modMR;

    private Boolean modCT;
    private Boolean modUS;
    private Boolean modFL;
    private Boolean modEMG;
    private Boolean modXR;
    private Boolean modPET;
    private Boolean openMR;
    private Boolean tradMR;
    private int openMR_Class = 0;
    private int tradMR_Class = 0;

    private Double openMR_Tesla = 0.0;
    private Double tradMR_Tesla = 0.0;

    private Boolean badge_Closest = false;
    private Boolean badge_Cheapest = false;
    private Boolean badge_Open = false;
    private Boolean badge_Traditional = false;
    private Boolean badge_HighField = false;
    private Boolean badge_SuperHighField = false;
    private Boolean badge_ACR = false;
    public java.util.Vector<String> badge_ACR_notes = new java.util.Vector<String>();
    private Boolean badge_ACR_MR = false;
    private Boolean badge_ACR_CT = false;
    private Boolean badge_ACR_US = false;
    private Boolean badge_ACR_PET = false;
    private Boolean badge_Featured = false;
    private Boolean badge_Specialize_MR= false;
    private Boolean badge_BestValue = false;

    private Double MACScore = 0.0;

    private String keyScore = "";

    public Double getCustomerPrice() {
        return customerPrice;
    }

    public void setCustomerPrice(Double customerPrice) {
        this.customerPrice = customerPrice;
    }

    public PracticeMaster_SearchResultsObject(Integer iPracticeID) {
        this.practiceMaster = new bltPracticeMaster(iPracticeID);
    }

    public bltPracticeMaster getPracticeMaster() {
        return practiceMaster;
    }

    public void setPracticeMaster(bltPracticeMaster practiceMaster) {
        this.practiceMaster = practiceMaster;
    }

    public Double getDistance() {
        return distance;
    }

    public void setDistance(Double distance) {
        this.distance = distance;
    }

    public Boolean getModMR() {
        return modMR;
    }

    public void setModMR(Boolean modMR) {
        this.modMR = modMR;
    }

    public Boolean getModCT() {
        return modCT;
    }

    public void setModCT(Boolean modCT) {
        this.modCT = modCT;
    }

    public Boolean getModUS() {
        return modUS;
    }

    public void setModUS(Boolean modUS) {
        this.modUS = modUS;
    }

    public Boolean getModFL() {
        return modFL;
    }

    public void setModFL(Boolean modFL) {
        this.modFL = modFL;
    }

    public Boolean getModEMG() {
        return modEMG;
    }

    public void setModEMG(Boolean modEMG) {
        this.modEMG = modEMG;
    }

    public Boolean getModXR() {
        return modXR;
    }

    public void setModXR(Boolean modXR) {
        this.modXR = modXR;
    }

    public Boolean getModPET() {
        return modPET;
    }

    public void setModPET(Boolean modPET) {
        this.modPET = modPET;
    }

    public Boolean getOpenMR() {
        return openMR;
    }

    public void setOpenMR(Boolean openMR) {
        this.openMR = openMR;
        if (openMR){
            this.badge_Open = true;
        }
    }

    public Boolean getTradMR() {
        return tradMR;
    }

    public void setTradMR(Boolean tradMR) {
        this.tradMR = tradMR;
        if (tradMR){
            this.badge_Traditional = true;
        }
    }

    public void setTradMR(boolean tradMR) {
        this.tradMR = tradMR;
    }

    public int getOpenMR_Class() {
        return openMR_Class;
    }

    public void setOpenMR_Class(int openMR_Class) {
        this.openMR_Class = openMR_Class;
    }

    public int getTradMR_Class() {
        return tradMR_Class;
    }

    public void setTradMR_Class(int tradMR_Class) {
        this.tradMR_Class = tradMR_Class;
    }

    public Double getMACScore() {
        return MACScore;
    }

    public void setMACScore(Double MACScore) {
        this.MACScore = MACScore;
    }

    public Double getOpenMR_Tesla() {
        return openMR_Tesla;
    }

    public void setOpenMR_Tesla(Double openMR_Tesla) {
        this.openMR_Tesla = openMR_Tesla;
        if (openMR_Tesla>1.1){
            this.setBadge_HighField(true);
        }
    }

    public Double getTradMR_Tesla() {
        return tradMR_Tesla;
    }

    public void setTradMR_Tesla(Double tradMR_Tesla) {
        this.tradMR_Tesla = tradMR_Tesla;
        if (tradMR_Tesla>1.6){
            this.setBadge_SuperHighField(true);
        }
        if (tradMR_Tesla>1.1){
            this.setBadge_HighField(true);
        }
    }

    public Boolean getBadge_Closest() {
        return badge_Closest;
    }

    public void setBadge_Closest(Boolean badge_Closest) {
        this.badge_Closest = badge_Closest;
    }

    public Boolean getBadge_Cheapest() {
        return badge_Cheapest;
    }

    public void setBadge_Cheapest(Boolean badge_Cheapest) {
        this.badge_Cheapest = badge_Cheapest;
    }

    public Boolean getBadge_Open() {
        return badge_Open;
    }

    public void setBadge_Open(Boolean badge_Open) {
        this.badge_Open = badge_Open;
    }

    public Boolean getBadge_Traditional() {
        return badge_Traditional;
    }

    public void setBadge_Traditional(Boolean badge_Traditional) {
        this.badge_Traditional = badge_Traditional;
    }

    public Boolean getBadge_HighField() {
        return badge_HighField;
    }

    public void setBadge_HighField(Boolean badge_HighField) {
        this.badge_HighField = badge_HighField;
    }

    public Boolean getBadge_SuperHighField() {
        return badge_SuperHighField;
    }

    public void setBadge_SuperHighField(Boolean badge_SuperHighField) {
        this.badge_SuperHighField = badge_SuperHighField;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Boolean getBadge_ACR() {
        return badge_ACR;
    }

    public void setBadge_ACR(Boolean badge_ACR) {
        this.badge_ACR = badge_ACR;
    }

    public Boolean getBadge_Featured() {
        return badge_Featured;
    }

    public void setBadge_Featured(Boolean badge_Featured) {
        this.badge_Featured = badge_Featured;
    }

    public Boolean getBadge_Specialize_MR() {
        return badge_Specialize_MR;
    }

    public void setBadge_Specialize_MR(Boolean badge_Specialize_MR) {
        this.badge_Specialize_MR = badge_Specialize_MR;
    }

    public Boolean getBadge_BestValue() {
        return badge_BestValue;
    }

    public void setBadge_BestValue(Boolean badge_BestValue) {
        this.badge_BestValue = badge_BestValue;
    }

    public String getKeyScore() {
        return keyScore;
    }

    public void setKeyScore(String keyScore) {
        this.keyScore = keyScore;
    }

    public Boolean getBadge_ACR_MR() {
        return badge_ACR_MR;
    }

    public void setBadge_ACR_MR(Boolean badge_ACR_MR) {
        this.badge_ACR_MR = badge_ACR_MR;
    }

    public Boolean getBadge_ACR_CT() {
        return badge_ACR_CT;
    }

    public void setBadge_ACR_CT(Boolean badge_ACR_CT) {
        this.badge_ACR_CT = badge_ACR_CT;
    }

    public Boolean getBadge_ACR_US() {
        return badge_ACR_US;
    }

    public void setBadge_ACR_US(Boolean badge_ACR_US) {
        this.badge_ACR_US = badge_ACR_US;
    }

    public Boolean getBadge_ACR_PET() {
        return badge_ACR_PET;
    }

    public void setBadge_ACR_PET(Boolean badge_ACR_PET) {
        this.badge_ACR_PET = badge_ACR_PET;
    }

    public String getBadge_ACR_Notes(String delim){
        String myReturn = "";
        int cnt = 0;
        for (String temp_note: this.badge_ACR_notes){
            cnt++;
            myReturn += temp_note;
            if (cnt<this.badge_ACR_notes.size()){
                myReturn += delim;
            }
        }
        return myReturn;

    }

    public Double getPriceUC() {
        return priceUC;
    }

    public void setPriceUC(Double priceUC) {
        this.priceUC = priceUC;
    }

    public long getSavings(){
        Double tempSavings = this.getPriceUC() - this.getCustomerPrice();
        try {
            if (tempSavings>0){
                return Math.round(tempSavings);
            }
        } catch (Exception e){
            DebugLogger.e("PracticeMaster_SearchResultsObject:getSavingsPercentage","Error calculating percent savings []" + e);
        }
        //if all else fails, return 15
        return 0;
    }


    public long getSavingsPercentage(){
        Double tempSavings = this.getPriceUC() - this.getCustomerPrice();
        try {
            if (tempSavings>0){
                return Math.round(tempSavings/this.getPriceUC()*100);
            }
        } catch (Exception e){
            DebugLogger.e("PracticeMaster_SearchResultsObject:getSavingsPercentage","Error calculating percent savings []" + e);
        }
        //if all else fails, return 15
        return 15;
    }
}
