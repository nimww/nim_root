

package com.winstaff;

import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public class dbtNIM3_Service extends Object implements InttNIM3_Service
{

        db_NewBase    dbnbDB;

    public dbtNIM3_Service()
    {
        dbnbDB = new db_NewBase( "tNIM3_Service", "ServiceID" );

    }    // End of default constructor

    public dbtNIM3_Service( Integer iNewID )
    {
        dbnbDB = new db_NewBase( "tNIM3_Service", "ServiceID" );
        setUniqueID( iNewID );
    }    // End of Constructor knowing an ID

    public void commitData() throws SQLException
    {
            //no longer trapping/ignoring errors 2011-03-17
            dbnbDB.commitData();
    }    // End of commitData()

    public void commitDataForced() throws SQLException
    {
        dbnbDB.commitDataForced();
    }    // End of commitData()

    public Integer getUniqueID()
    {
        return dbnbDB.getUniqueID();
    }    // End of getUniqueID()

    public void setUniqueID( Integer iNewID )
    {
        dbnbDB.setUniqueID( iNewID );
    }    // End of setUniqueID()

    public void setModifyComments( String sNew )
    {
        dbnbDB.setModifyComments( sNew );
    }    // End of setUniqueID()


    public void setServiceID(Integer newValue)
    {
                dbnbDB.setFieldData( "ServiceID", newValue.toString() );
    }

    public Integer getServiceID()
    {
        String           sValue = dbnbDB.getFieldData( "ServiceID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setField(String fieldN, Object fieldV)
    {DebugLogger.println("setField Method not implemented yet in classNIM3_Service!");}

    public String getFieldType(String fieldN)
    {return null;}


    public void setUniqueCreateDate(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "UniqueCreateDate", formatter.format( newValue ) );
    }

    public Date getUniqueCreateDate()
    {
        String           sValue = dbnbDB.getFieldData( "UniqueCreateDate" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setUniqueModifyDate(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "UniqueModifyDate", formatter.format( newValue ) );
    }

    public Date getUniqueModifyDate()
    {
        String           sValue = dbnbDB.getFieldData( "UniqueModifyDate" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setUniqueModifyComments(String newValue)
    {
                dbnbDB.setFieldData( "UniqueModifyComments", newValue.toString() );
    }

    public String getUniqueModifyComments()
    {
        String           sValue = dbnbDB.getFieldData( "UniqueModifyComments" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setEncounterID(Integer newValue)
    {
                dbnbDB.setFieldData( "EncounterID", newValue.toString() );
    }

    public Integer getEncounterID()
    {
        String           sValue = dbnbDB.getFieldData( "EncounterID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setServiceTypeID(Integer newValue)
    {
                dbnbDB.setFieldData( "ServiceTypeID", newValue.toString() );
    }

    public Integer getServiceTypeID()
    {
        String           sValue = dbnbDB.getFieldData( "ServiceTypeID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setDateOfService(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "DateOfService", formatter.format( newValue ) );
    }

    public Date getDateOfService()
    {
        String           sValue = dbnbDB.getFieldData( "DateOfService" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setAttendingPhysicianID(Integer newValue)
    {
                dbnbDB.setFieldData( "AttendingPhysicianID", newValue.toString() );
    }

    public Integer getAttendingPhysicianID()
    {
        String           sValue = dbnbDB.getFieldData( "AttendingPhysicianID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setReferringPhysicianID(Integer newValue)
    {
                dbnbDB.setFieldData( "ReferringPhysicianID", newValue.toString() );
    }

    public Integer getReferringPhysicianID()
    {
        String           sValue = dbnbDB.getFieldData( "ReferringPhysicianID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setCPT(String newValue)
    {
                dbnbDB.setFieldData( "CPT", newValue.toString() );
    }

    public String getCPT()
    {
        String           sValue = dbnbDB.getFieldData( "CPT" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setCPTModifier(String newValue)
    {
                dbnbDB.setFieldData( "CPTModifier", newValue.toString() );
    }

    public String getCPTModifier()
    {
        String           sValue = dbnbDB.getFieldData( "CPTModifier" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setCPTBodyPart(String newValue)
    {
                dbnbDB.setFieldData( "CPTBodyPart", newValue.toString() );
    }

    public String getCPTBodyPart()
    {
        String           sValue = dbnbDB.getFieldData( "CPTBodyPart" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setdCPT1(String newValue)
    {
                dbnbDB.setFieldData( "dCPT1", newValue.toString() );
    }

    public String getdCPT1()
    {
        String           sValue = dbnbDB.getFieldData( "dCPT1" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setdCPT2(String newValue)
    {
                dbnbDB.setFieldData( "dCPT2", newValue.toString() );
    }

    public String getdCPT2()
    {
        String           sValue = dbnbDB.getFieldData( "dCPT2" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setdCPT3(String newValue)
    {
                dbnbDB.setFieldData( "dCPT3", newValue.toString() );
    }

    public String getdCPT3()
    {
        String           sValue = dbnbDB.getFieldData( "dCPT3" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setdCPT4(String newValue)
    {
                dbnbDB.setFieldData( "dCPT4", newValue.toString() );
    }

    public String getdCPT4()
    {
        String           sValue = dbnbDB.getFieldData( "dCPT4" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setCPTText(String newValue)
    {
                dbnbDB.setFieldData( "CPTText", newValue.toString() );
    }

    public String getCPTText()
    {
        String           sValue = dbnbDB.getFieldData( "CPTText" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setBillAmount(Double newValue)
    {
                java.text.DecimalFormat defDecFormat2 = new java.text.DecimalFormat(PLCUtils.String_defDecFormat2);
                dbnbDB.setFieldData( "BillAmount", defDecFormat2.format(newValue) );
    }

    public Double getBillAmount()
    {
        String           sValue = dbnbDB.getFieldData( "BillAmount" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDoubleValue : sValue;
        return new Double( sValue  ); 
    }

    public void setAllowAmount(Double newValue)
    {
                java.text.DecimalFormat defDecFormat2 = new java.text.DecimalFormat(PLCUtils.String_defDecFormat2);
                dbnbDB.setFieldData( "AllowAmount", defDecFormat2.format(newValue) );
    }

    public Double getAllowAmount()
    {
        String           sValue = dbnbDB.getFieldData( "AllowAmount" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDoubleValue : sValue;
        return new Double( sValue  ); 
    }

    public void setReceivedAmount(Double newValue)
    {
                java.text.DecimalFormat defDecFormat2 = new java.text.DecimalFormat(PLCUtils.String_defDecFormat2);
                dbnbDB.setFieldData( "ReceivedAmount", defDecFormat2.format(newValue) );
    }

    public Double getReceivedAmount()
    {
        String           sValue = dbnbDB.getFieldData( "ReceivedAmount" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDoubleValue : sValue;
        return new Double( sValue  ); 
    }

    public void setPaidOutAmount(Double newValue)
    {
                java.text.DecimalFormat defDecFormat2 = new java.text.DecimalFormat(PLCUtils.String_defDecFormat2);
                dbnbDB.setFieldData( "PaidOutAmount", defDecFormat2.format(newValue) );
    }

    public Double getPaidOutAmount()
    {
        String           sValue = dbnbDB.getFieldData( "PaidOutAmount" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDoubleValue : sValue;
        return new Double( sValue  ); 
    }

    public void setReceivedCheck1Number(String newValue)
    {
                dbnbDB.setFieldData( "ReceivedCheck1Number", newValue.toString() );
    }

    public String getReceivedCheck1Number()
    {
        String           sValue = dbnbDB.getFieldData( "ReceivedCheck1Number" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setReceivedCheck1Amount(Double newValue)
    {
                java.text.DecimalFormat defDecFormat2 = new java.text.DecimalFormat(PLCUtils.String_defDecFormat2);
                dbnbDB.setFieldData( "ReceivedCheck1Amount", defDecFormat2.format(newValue) );
    }

    public Double getReceivedCheck1Amount()
    {
        String           sValue = dbnbDB.getFieldData( "ReceivedCheck1Amount" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDoubleValue : sValue;
        return new Double( sValue  ); 
    }

    public void setReceivedCheck1Date(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "ReceivedCheck1Date", formatter.format( newValue ) );
    }

    public Date getReceivedCheck1Date()
    {
        String           sValue = dbnbDB.getFieldData( "ReceivedCheck1Date" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setReceivedCheck2Number(String newValue)
    {
                dbnbDB.setFieldData( "ReceivedCheck2Number", newValue.toString() );
    }

    public String getReceivedCheck2Number()
    {
        String           sValue = dbnbDB.getFieldData( "ReceivedCheck2Number" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setReceivedCheck2Amount(Double newValue)
    {
                java.text.DecimalFormat defDecFormat2 = new java.text.DecimalFormat(PLCUtils.String_defDecFormat2);
                dbnbDB.setFieldData( "ReceivedCheck2Amount", defDecFormat2.format(newValue) );
    }

    public Double getReceivedCheck2Amount()
    {
        String           sValue = dbnbDB.getFieldData( "ReceivedCheck2Amount" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDoubleValue : sValue;
        return new Double( sValue  ); 
    }

    public void setReceivedCheck2Date(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "ReceivedCheck2Date", formatter.format( newValue ) );
    }

    public Date getReceivedCheck2Date()
    {
        String           sValue = dbnbDB.getFieldData( "ReceivedCheck2Date" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setReceivedCheck3Number(String newValue)
    {
                dbnbDB.setFieldData( "ReceivedCheck3Number", newValue.toString() );
    }

    public String getReceivedCheck3Number()
    {
        String           sValue = dbnbDB.getFieldData( "ReceivedCheck3Number" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setReceivedCheck3Amount(Double newValue)
    {
                java.text.DecimalFormat defDecFormat2 = new java.text.DecimalFormat(PLCUtils.String_defDecFormat2);
                dbnbDB.setFieldData( "ReceivedCheck3Amount", defDecFormat2.format(newValue) );
    }

    public Double getReceivedCheck3Amount()
    {
        String           sValue = dbnbDB.getFieldData( "ReceivedCheck3Amount" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDoubleValue : sValue;
        return new Double( sValue  ); 
    }

    public void setReceivedCheck3Date(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "ReceivedCheck3Date", formatter.format( newValue ) );
    }

    public Date getReceivedCheck3Date()
    {
        String           sValue = dbnbDB.getFieldData( "ReceivedCheck3Date" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setReportFileID(Integer newValue)
    {
                dbnbDB.setFieldData( "ReportFileID", newValue.toString() );
    }

    public Integer getReportFileID()
    {
        String           sValue = dbnbDB.getFieldData( "ReportFileID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setComments(String newValue)
    {
                dbnbDB.setFieldData( "Comments", newValue.toString() );
    }

    public String getComments()
    {
        String           sValue = dbnbDB.getFieldData( "Comments" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setAuditNotes(String newValue)
    {
                dbnbDB.setFieldData( "AuditNotes", newValue.toString() );
    }

    public String getAuditNotes()
    {
        String           sValue = dbnbDB.getFieldData( "AuditNotes" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setCPTWizardID(Integer newValue)
    {
                dbnbDB.setFieldData( "CPTWizardID", newValue.toString() );
    }

    public Integer getCPTWizardID()
    {
        String           sValue = dbnbDB.getFieldData( "CPTWizardID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setCPTQty(Integer newValue)
    {
                dbnbDB.setFieldData( "CPTQty", newValue.toString() );
    }

    public Integer getCPTQty()
    {
        String           sValue = dbnbDB.getFieldData( "CPTQty" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setAllowAmountAdjustment(Double newValue)
    {
                java.text.DecimalFormat defDecFormat2 = new java.text.DecimalFormat(PLCUtils.String_defDecFormat2);
                dbnbDB.setFieldData( "AllowAmountAdjustment", defDecFormat2.format(newValue) );
    }

    public Double getAllowAmountAdjustment()
    {
        String           sValue = dbnbDB.getFieldData( "AllowAmountAdjustment" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDoubleValue : sValue;
        return new Double( sValue  ); 
    }

    public void setServiceStatusID(Integer newValue)
    {
                dbnbDB.setFieldData( "ServiceStatusID", newValue.toString() );
    }

    public Integer getServiceStatusID()
    {
        String           sValue = dbnbDB.getFieldData( "ServiceStatusID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setDICOM_AccessionNumber(String newValue)
    {
                dbnbDB.setFieldData( "DICOM_AccessionNumber", newValue.toString() );
    }

    public String getDICOM_AccessionNumber()
    {
        String           sValue = dbnbDB.getFieldData( "DICOM_AccessionNumber" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setDICOM_ShowImage(Integer newValue)
    {
                dbnbDB.setFieldData( "DICOM_ShowImage", newValue.toString() );
    }

    public Integer getDICOM_ShowImage()
    {
        String           sValue = dbnbDB.getFieldData( "DICOM_ShowImage" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setDICOM_MRN(String newValue)
    {
                dbnbDB.setFieldData( "DICOM_MRN", newValue.toString() );
    }

    public String getDICOM_MRN()
    {
        String           sValue = dbnbDB.getFieldData( "DICOM_MRN" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setDICOM_PN(String newValue)
    {
                dbnbDB.setFieldData( "DICOM_PN", newValue.toString() );
    }

    public String getDICOM_PN()
    {
        String           sValue = dbnbDB.getFieldData( "DICOM_PN" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setDICOM_STUDYID(String newValue)
    {
                dbnbDB.setFieldData( "DICOM_STUDYID", newValue.toString() );
    }

    public String getDICOM_STUDYID()
    {
        String           sValue = dbnbDB.getFieldData( "DICOM_STUDYID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setReadingPhysicianID(Integer newValue)
    {
                dbnbDB.setFieldData( "ReadingPhysicianID", newValue.toString() );
    }

    public Integer getReadingPhysicianID()
    {
        String           sValue = dbnbDB.getFieldData( "ReadingPhysicianID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setReference_ProviderContractAmount(Double newValue)
    {
                java.text.DecimalFormat defDecFormat2 = new java.text.DecimalFormat(PLCUtils.String_defDecFormat2);
                dbnbDB.setFieldData( "Reference_ProviderContractAmount", defDecFormat2.format(newValue) );
    }

    public Double getReference_ProviderContractAmount()
    {
        String           sValue = dbnbDB.getFieldData( "Reference_ProviderContractAmount" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDoubleValue : sValue;
        return new Double( sValue  ); 
    }

    public void setReference_ProviderBilledAmount(Double newValue)
    {
                java.text.DecimalFormat defDecFormat2 = new java.text.DecimalFormat(PLCUtils.String_defDecFormat2);
                dbnbDB.setFieldData( "Reference_ProviderBilledAmount", defDecFormat2.format(newValue) );
    }

    public Double getReference_ProviderBilledAmount()
    {
        String           sValue = dbnbDB.getFieldData( "Reference_ProviderBilledAmount" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDoubleValue : sValue;
        return new Double( sValue  ); 
    }

    public void setReference_FeeScheduleAmount(Double newValue)
    {
                java.text.DecimalFormat defDecFormat2 = new java.text.DecimalFormat(PLCUtils.String_defDecFormat2);
                dbnbDB.setFieldData( "Reference_FeeScheduleAmount", defDecFormat2.format(newValue) );
    }

    public Double getReference_FeeScheduleAmount()
    {
        String           sValue = dbnbDB.getFieldData( "Reference_FeeScheduleAmount" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDoubleValue : sValue;
        return new Double( sValue  ); 
    }

    public void setReference_UCAmount(Double newValue)
    {
                java.text.DecimalFormat defDecFormat2 = new java.text.DecimalFormat(PLCUtils.String_defDecFormat2);
                dbnbDB.setFieldData( "Reference_UCAmount", defDecFormat2.format(newValue) );
    }

    public Double getReference_UCAmount()
    {
        String           sValue = dbnbDB.getFieldData( "Reference_UCAmount" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDoubleValue : sValue;
        return new Double( sValue  ); 
    }

    public void setReference_PayerBillAmount(Double newValue)
    {
                java.text.DecimalFormat defDecFormat2 = new java.text.DecimalFormat(PLCUtils.String_defDecFormat2);
                dbnbDB.setFieldData( "Reference_PayerBillAmount", defDecFormat2.format(newValue) );
    }

    public Double getReference_PayerBillAmount()
    {
        String           sValue = dbnbDB.getFieldData( "Reference_PayerBillAmount" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDoubleValue : sValue;
        return new Double( sValue  ); 
    }

    public void setReference_PayerContractAmount(Double newValue)
    {
                java.text.DecimalFormat defDecFormat2 = new java.text.DecimalFormat(PLCUtils.String_defDecFormat2);
                dbnbDB.setFieldData( "Reference_PayerContractAmount", defDecFormat2.format(newValue) );
    }

    public Double getReference_PayerContractAmount()
    {
        String           sValue = dbnbDB.getFieldData( "Reference_PayerContractAmount" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDoubleValue : sValue;
        return new Double( sValue  ); 
    }

    public void setBillingNotes(String newValue)
    {
                dbnbDB.setFieldData( "BillingNotes", newValue.toString() );
    }

    public String getBillingNotes()
    {
        String           sValue = dbnbDB.getFieldData( "BillingNotes" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setInvoiceNotes_Payer(String newValue)
    {
                dbnbDB.setFieldData( "InvoiceNotes_Payer", newValue.toString() );
    }

    public String getInvoiceNotes_Payer()
    {
        String           sValue = dbnbDB.getFieldData( "InvoiceNotes_Payer" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setInvoiceNotes_Provider(String newValue)
    {
                dbnbDB.setFieldData( "InvoiceNotes_Provider", newValue.toString() );
    }

    public String getInvoiceNotes_Provider()
    {
        String           sValue = dbnbDB.getFieldData( "InvoiceNotes_Provider" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setCPTQty_Bill(Integer newValue)
    {
                dbnbDB.setFieldData( "CPTQty_Bill", newValue.toString() );
    }

    public Integer getCPTQty_Bill()
    {
        String           sValue = dbnbDB.getFieldData( "CPTQty_Bill" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setCPTQty_Pay(Integer newValue)
    {
                dbnbDB.setFieldData( "CPTQty_Pay", newValue.toString() );
    }

    public Integer getCPTQty_Pay()
    {
        String           sValue = dbnbDB.getFieldData( "CPTQty_Pay" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

}    // End of bltNIM3_Service class definition
