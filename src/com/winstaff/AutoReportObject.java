package com.winstaff;

/**
 * Created by IntelliJ IDEA.
 * User: ste
 * Date: Jun 11, 2010
 * Time: 11:16:22 PM
 * To change this template use File | Settings | File Templates.
 */
public class AutoReportObject {

    private String title = null;
    private boolean displayChart = false;


    private boolean hideLabel = false;
    private String AutoReportCode = null;
    private String ChartType = null;
    private String ChartLabelField = null;
    private String ChartDataField = null;
    public static String CHART_BARCHART = "BarChart";
    public static String CHART_PIECHART = "PieChart";
    public static String CHART_COLCHART = "ColumnChart";

    public String getAutoReportCode()
    {
        return AutoReportCode;
    }

    public void setAutoReportCode(String autoReportCode) {
        AutoReportCode = autoReportCode;
    }

    public AutoReportObject(String title, String sSQL) {
        this.title = title;
        PreparedStatementObject = new db_PreparedStatementObject(sSQL);
    }

    public AutoReportObject() {
    }

    public AutoReportObject(String title, String sSQL, boolean displayChart, String chartType, String chartLabelField, String chartDataField) {
        this.title = title;
        PreparedStatementObject = new db_PreparedStatementObject(sSQL);
        this.displayChart = displayChart;
        ChartType = chartType;
        ChartLabelField = chartLabelField;
        ChartDataField = chartDataField;
    }


    public boolean isHideLabel() {
        return hideLabel;
    }

    public void setHideLabel(boolean hideLabel) {
        this.hideLabel = hideLabel;
    }


    public String getChartDataField() {
        return ChartDataField;
    }

    public void setChartDataField(String chartDataField) {
        ChartDataField = chartDataField;
    }

    public String getChartLabelField() {
        return ChartLabelField;
    }

    public void setChartLabelField(String chartLabelField) {
        ChartLabelField = chartLabelField;
    }

    public String getChartType() {
        return ChartType;
    }

    public void setChartType(String chartType) {
        ChartType = chartType;
    }

    public boolean isDisplayChart() {
        return displayChart;
    }

    public void setDisplayChart(boolean displayChart) {
        this.displayChart = displayChart;
    }

    public String getsSQL() {
        if (PreparedStatementObject==null)
        {
            return null;
        }
        else
        {
            return PreparedStatementObject.getSQL();
        }
    }

    public void setsSQL(String sSQL)
    {
        if (PreparedStatementObject==null)
        {
            PreparedStatementObject = new db_PreparedStatementObject(sSQL);
        }
        else
        {
            PreparedStatementObject.setSQL(sSQL);
        }
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public db_PreparedStatementObject getPreparedStatementObject() {
        return PreparedStatementObject;
    }

    public void setPreparedStatementObject(db_PreparedStatementObject myPSO) {
        this.PreparedStatementObject = myPSO;
    }

    private db_PreparedStatementObject PreparedStatementObject = null;


}
