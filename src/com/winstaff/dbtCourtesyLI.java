

package com.winstaff;

import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public class dbtCourtesyLI extends Object implements InttCourtesyLI
{

        db_NewBase    dbnbDB;

    public dbtCourtesyLI()
    {
        dbnbDB = new db_NewBase( "tCourtesyLI", "CourtesyID" );

    }    // End of default constructor

    public dbtCourtesyLI( Integer iNewID )
    {
        dbnbDB = new db_NewBase( "tCourtesyLI", "CourtesyID" );
        setUniqueID( iNewID );
    }    // End of Constructor knowing an ID

    public void commitData() throws SQLException
    {
            //no longer trapping/ignoring errors 2011-03-17
            dbnbDB.commitData();
    }    // End of commitData()

    public void commitDataForced() throws SQLException
    {
        dbnbDB.commitDataForced();
    }    // End of commitData()

    public Integer getUniqueID()
    {
        return dbnbDB.getUniqueID();
    }    // End of getUniqueID()

    public void setUniqueID( Integer iNewID )
    {
        dbnbDB.setUniqueID( iNewID );
    }    // End of setUniqueID()

    public void setModifyComments( String sNew )
    {
        dbnbDB.setModifyComments( sNew );
    }    // End of setUniqueID()


    public void setCourtesyID(Integer newValue)
    {
                dbnbDB.setFieldData( "CourtesyID", newValue.toString() );
    }

    public Integer getCourtesyID()
    {
        String           sValue = dbnbDB.getFieldData( "CourtesyID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setField(String fieldN, Object fieldV)
    {DebugLogger.println("setField Method not implemented yet in class!");}

    public String getFieldType(String fieldN)
    {return null;}


    public void setCourtesyShort(String newValue)
    {
                dbnbDB.setFieldData( "CourtesyShort", newValue.toString() );
    }

    public String getCourtesyShort()
    {
        String           sValue = dbnbDB.getFieldData( "CourtesyShort" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setCourtesyLong(String newValue)
    {
                dbnbDB.setFieldData( "CourtesyLong", newValue.toString() );
    }

    public String getCourtesyLong()
    {
        String           sValue = dbnbDB.getFieldData( "CourtesyLong" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

}    // End of bltCourtesyLI class definition
