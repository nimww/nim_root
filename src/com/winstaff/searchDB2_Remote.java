package com.winstaff;
/**
** quickDB defines a "quick" methodology to access a database without DB connection pooling
** 
** @author      Scott Tyler Ellis
** @version     1.0a
**
*/

import java.lang.String;
import java.sql.Connection;
import java.sql.Statement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.DriverManager;
import java.sql.DatabaseMetaData;
import java.sql.Timestamp;
import java.awt.*;
import java.util.Date;

public class searchDB2_Remote
{
    public searchDB2_Remote()
    {
    }
    
    Connection theConnection;
    Statement theStatement;
    ResultSet myRS;

    public int executeUpdate(String mySQL)
    {
	int DBAffected = 0;
	try
	{
	    Class.forName(this.dbForName);
	    theConnection = DriverManager.getConnection(this.dbConn,this.dbUserName,this.dbPassword);
	    theStatement = theConnection.createStatement();
	    DBAffected = theStatement.executeUpdate(mySQL);
	}
	catch (Exception e)
	{
            DebugLogger.println("QuickDB Degbug [No-ResultSet]: [" + e.toString() + "]");
	}
	return DBAffected;
    }

    public ResultSet executeStatement(String mySQL)
    {
	try
	{
	    Class.forName(this.dbForName);
//	    theConnection = DriverManager.getConnection(this.dbConn,this.dbUserName,this.dbPassword);
	    theConnection = DriverManager.getConnection(this.dbConn);
	    theStatement = theConnection.createStatement();
		DebugLogger.println("Select: " + mySQL);
	    myRS = theStatement.executeQuery(mySQL);
	}
	catch (Exception e)
	{
        DebugLogger.println("QuickDB Degbug [ResultSet]: [" + e.toString() + "]");
	}
	return myRS;
    }
    
    public void closeAll()
    {
		try
		{
		    theConnection.close();
		}
		catch (Exception e)
		{
		    DebugLogger.println("QuickDB Degbug: [ConnectionClose]" + e.toString());
		}
    }

	public void setDbUserName(String dbUserName)
	{
			this.dbUserName = dbUserName;
	}

	public String getDbUserName()
	{
		return this.dbUserName;
	}

	public void setDbPassword(String dbPassword)
	{
			this.dbPassword = dbPassword;
	}

	public String getDbPassword()
	{
		return this.dbPassword;
	}

	public void setDbForName(String dbForName)
	{
			this.dbForName = dbForName;
	}

	public String getDbForName()
	{
		return this.dbForName;
	}

	public void setDbConnPre(String dbConnPre)
	{
			this.dbConnPre = dbConnPre;
	}

	public String getDbConnPre()
	{
		return this.dbConnPre;
	}


	public void setDbConn(String dbConn)
	{
			this.dbConn = dbConn;
	}

	protected String dbForName = ConfigurationInformation_Remote.sJDBCDriverName;
	protected String dbPassword = ConfigurationInformation_Remote.sPassword;
	protected String dbUserName = ConfigurationInformation_Remote.sUser;
	protected String dbConnPre = "jdbc:odbc:";
	protected String dbConn = ConfigurationInformation_Remote.sURL;


	//{{DECLARE_CONTROLS
	//}}
}
