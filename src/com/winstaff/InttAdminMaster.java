

package com.winstaff;

import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public interface InttAdminMaster extends dbTableInterface
{

    public void setAdminID(Integer newValue);
    public Integer getAdminID();
    public void setUniqueCreateDate(Date newValue);
    public Date getUniqueCreateDate();
    public void setUniqueModifyDate(Date newValue);
    public Date getUniqueModifyDate();
    public void setUniqueModifyComments(String newValue);
    public String getUniqueModifyComments();
    public void setName(String newValue);
    public String getName();
    public void setAddress1(String newValue);
    public String getAddress1();
    public void setAddress2(String newValue);
    public String getAddress2();
    public void setCity(String newValue);
    public String getCity();
    public void setStateID(Integer newValue);
    public Integer getStateID();
    public void setProvince(String newValue);
    public String getProvince();
    public void setZIP(String newValue);
    public String getZIP();
    public void setCountryID(Integer newValue);
    public Integer getCountryID();
    public void setPhone(String newValue);
    public String getPhone();
    public void setAlertEmail(String newValue);
    public String getAlertEmail();
    public void setAlertDays(Integer newValue);
    public Integer getAlertDays();
    public void setContactFirstName(String newValue);
    public String getContactFirstName();
    public void setContactLastName(String newValue);
    public String getContactLastName();
    public void setContactEmail(String newValue);
    public String getContactEmail();
    public void setContactAddress1(String newValue);
    public String getContactAddress1();
    public void setContactAddress2(String newValue);
    public String getContactAddress2();
    public void setContactCity(String newValue);
    public String getContactCity();
    public void setContactStateID(Integer newValue);
    public Integer getContactStateID();
    public void setContactZIP(String newValue);
    public String getContactZIP();
    public void setContactPhone(String newValue);
    public String getContactPhone();
    public void setContactFax(String newValue);
    public String getContactFax();
    public void setLastImportDate(Date newValue);
    public Date getLastImportDate();
    public void setDocuLinkID(Integer newValue);
    public Integer getDocuLinkID();
    public void setComments(String newValue);
    public String getComments();
    public void setfax(String newValue);
    public String getfax();
    public void setAssignedToID(Integer newValue);
    public Integer getAssignedToID();
}    // End of bltAdminMaster class definition
