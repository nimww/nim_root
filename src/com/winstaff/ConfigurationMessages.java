package com.winstaff;
/*
 * ConfigurationInformation.java
 *
 * Created on April 14, 2001, 7:41 PM
 */


import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author  Scott Ellis
 * @version 1.00
 */
public class ConfigurationMessages
{
    public static String getMessage(String msgCode)
    {
         String myVal = "";
         if (msgCode.equalsIgnoreCase("NewDoc"))
         {
             myVal = "Would you like to create a document record so that you can attach a scanned/faxed document to this data entry?";
         }
         else if (msgCode.equalsIgnoreCase("ConfirmReqFieldsReturn"))
         {
             myVal = "We detect that you did not fill in a Required Field in the previous section.\\n\\nClick OK to go back and complete that field\\nClick Cancel to Continue the application.";
         }
         else if (msgCode.equalsIgnoreCase("NoPracticeAllowPhysEdit"))
         {
             myVal = "You are not allowed to modify this practice.  If you need to change information regarding this practice, please contact your Credentialing Administrator.";
         }
         else if (msgCode.equalsIgnoreCase("ModDoc"))
         {
             myVal = "You have updated this data, do you want to modify your linked Document Cover Sheet?\\n\\n*Note this may require you to send in a new document.";
         }
         else if (msgCode.equalsIgnoreCase("CreateDocHTML"))
         {
             myVal = "Your Document has been created.<br><br>Please make sure to print out the new coversheet and send your document in as soon as possible.";
         }
         else if (msgCode.equalsIgnoreCase("UpdatedDocHTML"))
         {
             myVal = "Your document was not yet processed, so the data record has simply been updated.<br><br>Please make sure to print out the new coversheet and send your document in as soon as possible.";
         }
         else if (msgCode.equalsIgnoreCase("ArchivedNewDocHTML"))
         {
             myVal = "Since your old document had already been processed, it has been Archived for Audit Purposes.<br><br>A new document record has now been created.  Please make sure to print out the new cover sheet and send in your document as soon as possible.";
         }
         else if (msgCode.equalsIgnoreCase("DeletedDocHTML"))
         {
             myVal = "Your Document has been Deleted.";
         }
         else if (msgCode.equalsIgnoreCase("DeletedArchivedDocHTML"))
         {
             myVal = "Since your document had already been processed, it has been Archived for Audit Purposes.";
         }
	 //Errors
         else if (msgCode.equalsIgnoreCase("ERRORInvalidQuery"))
         {
             myVal = "invalid where query";
         }
         else if (msgCode.equalsIgnoreCase("ERRORIllegal"))
         {
             myVal = "illegal";
         }
         else if (msgCode.equalsIgnoreCase("ERRORSecurityNoAccess"))
         {
             myVal = "Your Security Level does not permit you to View this.";
         }
         else if (msgCode.equalsIgnoreCase("FATALERROR"))
         {
		myVal = "<h2>Error</h2>There is an error in your data that is preventing it from saving. Or please contact us at <a href=\"mailto:support@nextimagemedical.com?subject=ScanPass-Error: \">support@nextimagemedical.com</a>.  <br><br> We apologize for the inconvenience.";
         }
         return myVal;



    }

    public static String getInterviewMessage(String msgCode, String dataCategory)
    {
	String myVal = "";
	if (msgCode.equalsIgnoreCase("INTERVIEWNoUnprocDocs"))
	{
             myVal = "You currently have one or more documents to attach.\\n\\nThese documents need to be faxed in so that they can be linked to your account. Each document needs its own unique cover sheet.\\nNote: If you have already printed and faxed in these documents, please allow 2-3 business days for them to be processed.\\n\\nWould you like to print out these cover sheets now?";
             myVal+= "\\n\\nClick OK for Yes";
             myVal+= "\\nClick CANCEL for No";
	}
	else if (msgCode.equalsIgnoreCase("INTERVIEWNoElements_PracticeSpecial"))
	{
		myVal = "You do not current have any existing practices to select from, would you like to create a new one?";
             myVal+= "\\n\\nClick OK for Yes";
             myVal+= "\\nClick CANCEL for No";
	}
	else if (msgCode.equalsIgnoreCase("INTERVIEWNoElements")&&dataCategory.equalsIgnoreCase("tNIM3_CaseAccount"))
	{

	     myVal = "You do not have any Cases, do you want to create one?";
             myVal+= "\\n\\nClick OK for Yes";
             myVal+= "\\nClick CANCEL for No";
	}
	else if (msgCode.equalsIgnoreCase("INTERVIEWNoElements")&&dataCategory.equalsIgnoreCase("tNIM3_Encounter"))
	{

	     myVal = "You do not have any Encounters, do you want to create one?";
             myVal+= "\\n\\nClick OK for Yes";
             myVal+= "\\nClick CANCEL for No";
	}
	else if (msgCode.equalsIgnoreCase("INTERVIEWNoElements")&&dataCategory.equalsIgnoreCase("tNIM3_Referral"))
	{

	     myVal = "You do not have any Referrals, do you want to create one?";
             myVal+= "\\n\\nClick OK for Yes";
             myVal+= "\\nClick CANCEL for No";
	}
	else if (msgCode.equalsIgnoreCase("INTERVIEWNoElements")&&dataCategory.equalsIgnoreCase("tNIM3_Service"))
	{

	     myVal = "You do not have any Services, do you want to create one?";
             myVal+= "\\n\\nClick OK for Yes";
             myVal+= "\\nClick CANCEL for No";
	}
	else if (msgCode.equalsIgnoreCase("INTERVIEWNoElements"))
	{

	     myVal = "Section " + getDataStepNumber(dataCategory) + " of 24 - " + getDataCategory(dataCategory) + "\\n\\nWould you like to enter " + getDataCategory(dataCategory) + " information?";
             myVal+= "\\n\\nClick OK for Yes";
             myVal+= "\\nClick CANCEL for No";
	}
	else if (msgCode.equalsIgnoreCase("INTERVIEWAnother"))
	{
		myVal = "Would you like to add another " + getDataCategory(dataCategory) + " record?";
             myVal+= "\\n\\nClick OK for Yes";
             myVal+= "\\nClick CANCEL for No";
	}
	else if (msgCode.equalsIgnoreCase("INTERVIEWNoMore"))
	{
		myVal = "I am done entering " + getDataCategory(dataCategory) + " records, just take me to the next step.";
	}
	else if (msgCode.equalsIgnoreCase("INTERVIEWAddMore"))
	{
		myVal = "Add another " + getDataCategory(dataCategory) + " record.";
	}
	return myVal;
    }


    public static int getExpressItemCount(String msgCode)
    {
	int myVal =4;

        if (msgCode.equalsIgnoreCase("tBoardCertification"))
	    {
	        myVal =  3;
	    }
        else if (msgCode.equalsIgnoreCase("tCoveringPhysicians"))
	    {
	        myVal =  3;
	    }
        else if (msgCode.equalsIgnoreCase("tAdditionalInformation"))
	    {
	        myVal =  2;
	    }
        else if (msgCode.equalsIgnoreCase("tContinuingEducation"))
	    {
	        myVal =  10;
	    }
        else if (msgCode.equalsIgnoreCase("tExperience"))
	    {
	        myVal =  5;
	    }
        else if (msgCode.equalsIgnoreCase("tFacilityAffiliation"))
	    {
	        myVal =  8;
	    }
        else if (msgCode.equalsIgnoreCase("tLicenseRegistration"))
	    {
	        myVal =  6;
	    }
        else if (msgCode.equalsIgnoreCase("tMalpractice"))
	    {
	        myVal =  3;
	    }
        else if (msgCode.equalsIgnoreCase("tManagedCarePlan"))
	    {
	        myVal =  0;
	    }
        else if (msgCode.equalsIgnoreCase("tOtherCertification"))
	    {
	        myVal =  5;
	    }
        else if (msgCode.equalsIgnoreCase("tPeerReference"))
	    {
	        myVal =  3;
	    }
        else if (msgCode.equalsIgnoreCase("tProfessionalEducation"))
	    {
	        myVal =  4;
	    }
        else if (msgCode.equalsIgnoreCase("tProfessionalLiability"))
	    {
	        myVal =  5;
	    }
        else if (msgCode.equalsIgnoreCase("tProfessionalMisconduct"))
	    {
	        myVal =  2;
	    }
        else if (msgCode.equalsIgnoreCase("tProfessionalSociety"))
	    {
	        myVal =  5;
	    }
        else if (msgCode.equalsIgnoreCase("tWorkHistory"))
	    {
	        myVal =  5;
	    }
        else if (msgCode.equalsIgnoreCase("tNIM3_Service"))
	    {
	        myVal =  10;
	    }
//correction
	return myVal-1;
    }



    public static String getDataCategory(String msgCode)
    {
	String myVal = "";

        if (msgCode.equalsIgnoreCase("tBoardCertification"))
	    {
	        myVal += "Specialty/Certification";
	    }
        else if (msgCode.equalsIgnoreCase("Misc"))
	    {
	        myVal += "Miscellaneous Items";
	    }
        else if (msgCode.equalsIgnoreCase("tCoveringPhysicians"))
	    {
	        myVal += "Covering Physicians";
	    }
        else if (msgCode.equalsIgnoreCase("tAdditionalInformation"))
	    {
	        myVal += "Additional Notes";
	    }
        else if (msgCode.equalsIgnoreCase("tContinuingEducation"))
	    {
	        myVal += "Continuing Education";
	    }
        else if (msgCode.equalsIgnoreCase("tExperience"))
	    {
	        myVal += "Training/Experience";
	    }
        else if (msgCode.equalsIgnoreCase("tFacilityAffiliation"))
	    {
	        myVal += "Hospital/Facility Affiliation";
	    }
        else if (msgCode.equalsIgnoreCase("tLicenseRegistration"))
	    {
	        myVal += "License/Registration";
	    }
        else if (msgCode.equalsIgnoreCase("tMalpractice"))
	    {
	        myVal += "Malpractice History";
	    }
        else if (msgCode.equalsIgnoreCase("tManagedCarePlan"))
	    {
	        myVal += "Managed Care Plan";
	    }
        else if (msgCode.equalsIgnoreCase("tOtherCertification"))
	    {
	        myVal += "Other Certification";
	    }
        else if (msgCode.equalsIgnoreCase("tPeerReference"))
	    {
	        myVal += "Peer Reference";
	    }
        else if (msgCode.equalsIgnoreCase("tProfessionalEducation"))
	    {
	        myVal += "Education";
	    }
        else if (msgCode.equalsIgnoreCase("tProfessionalLiability"))
	    {
	        myVal += "Professional Liability";
	    }
        else if (msgCode.equalsIgnoreCase("tProfessionalMisconduct"))
	    {
	        myVal += "Professional Misconduct History";
	    }
        else if (msgCode.equalsIgnoreCase("tProfessionalSociety"))
	    {
	        myVal += "Professional Association";
	    }
        else if (msgCode.equalsIgnoreCase("tWorkHistory"))
	    {
	        myVal += "Work History";
	    }
        else if (msgCode.equalsIgnoreCase("tAttestR_Form"))
	    {
	        myVal += "Disclosure Questions";
	    }
        else if (msgCode.equalsIgnoreCase("tPracticeMasterBasic"))
	    {
	        myVal += "Practice";
	    }
	//practice
        else if (msgCode.equalsIgnoreCase("tPracticeMaster1"))
	    {
	        myVal += "Practice (1a)";
	    }
        else if (msgCode.equalsIgnoreCase("tPracticeMaster2"))
	    {
	        myVal += "Practice (1b)";
	    }
        else if (msgCode.equalsIgnoreCase("tAlliedHealthProfessional"))
	    {
	        myVal += "Allied Health Professionals";
	    }
        else if (msgCode.equalsIgnoreCase("tPhysicianMaster"))
	    {
		myVal += "Identifying Information";
	    }
        else if (msgCode.equalsIgnoreCase("pro-file_Status"))
	    {
		myVal += "Practitioner Home";
	    }
        else if (msgCode.equalsIgnoreCase("practice_Status"))
	    {
		myVal += "Practice Home";
	    }
        else if (msgCode.equalsIgnoreCase("tPhysicianPracticeLU"))
	    {
		myVal += "Practice Information";
	    }
        else if (msgCode.equalsIgnoreCase("tPhysicianPracticeLU_Pract"))
	    {
		myVal += "Practice Information";
	    }
        else if (msgCode.equalsIgnoreCase("Add_Practice"))
	    {
		myVal += "Practice Information";
	    }
        else if (msgCode.equalsIgnoreCase("tPhysicianUserAccountLU"))
	    {
		myVal += "On-Line Signature Account";
	    }
        else if (msgCode.equalsIgnoreCase("pro-file_Attestation"))
	    {
		myVal += "Sign Your Application";
	    }
        else if (msgCode.equalsIgnoreCase("tDocumentManagement"))
	    {
		myVal += "Attach Documents";
	    }
        else if (msgCode.equalsIgnoreCase("FinishedCheckList_PhysicianID"))
	    {
		myVal += "Practitioner Final Checklist";
	    }
        else if (msgCode.equalsIgnoreCase("Supplemental_PracticeID"))
	    {
		myVal += "Practice Supplemental Forms";
	    }
        else if (msgCode.equalsIgnoreCase("Supplemental_PhysicianID"))
	    {
		myVal += "Supplemental Forms";
	    }
        else if (msgCode.equalsIgnoreCase("tSuppPractOptometry"))
	    {
		myVal += "Supplemental Practice Optometry";
	    }
        else if (msgCode.equalsIgnoreCase("tSuppUECN"))
	    {
		myVal += "Supplemental UECN Form";
	    }
        else if (msgCode.equalsIgnoreCase("tSuppInterplan"))
	    {
		myVal += "Supplemental Interplan Form";
	    }
        else if (msgCode.equalsIgnoreCase("cl_Practice_Physician_via_Admin.jsp"))
	    {
		myVal += "Adding a Practice";
	    }
        else if (msgCode.equalsIgnoreCase("Physician_HCOAuth"))
	    {
		myVal += "HCO Authorization Review";
	    }

	return myVal;
    }





    public static String getDataInstructions(String msgCode)
    {
	String myVal = "<br><span class=instructions>";

        if (msgCode.equalsIgnoreCase("tBoardCertification"))
	    {
	        myVal += "Enter all your specialties and designate the board certification status. Do not enter certifications such as CPR, QME, etc, use Section 5. Data fields in Red are Required Fields. Items labeled \"Not Complete\" can be completed by clicking on the Edit/View button.";
	    }
        else if (msgCode.equalsIgnoreCase("Physician_HCOAuth"))
	    {
	        myVal += "You have selected the healthcare organizations (HCOs) listed below to retrieve/access your application information. Please review this list to make sure it is accurate.";
	    }
        else if (msgCode.equalsIgnoreCase("tCoveringPhysicians"))
	    {
	        myVal += "List physicians covering your practice in your absence. Data fields in Red are Required Fields. Items labeled \"Not Complete\" can be completed by clicking on the Edit/View button.";
	    }
        else if (msgCode.equalsIgnoreCase("tAdditionalInformation"))
	    {
	        myVal += "Provide any information you want regarding your credentialing application that has not been previously addressed. Data fields in Red are Required Fields. Items labeled \"Not Complete\" can be completed by clicking on the Edit/View button.";
	    }
        else if (msgCode.equalsIgnoreCase("tContinuingEducation"))
	    {
	        myVal += "Enter all accredited continuing education courses for this practitioner in the past four years. As the practitioner completes additional courses, be sure to enter them here also. Data fields in Red are Required Fields. Items labeled \"Not Complete\" can be completed by clicking on the Edit/View button.";
	    }
        else if (msgCode.equalsIgnoreCase("tExperience"))
	    {
	        myVal += "Enter Internship, Residencies, Fellowships, and Preceptorships. Do not enter undergraduate or graduate studies here, use Section 6, Education. Data fields in Red are Required Fields. Items labeled \"Not Complete\" can be completed by clicking on the Edit/View button.";
	    }
        else if (msgCode.equalsIgnoreCase("tFacilityAffiliation"))
	    {
	        myVal += "Enter all hospitals and facilities with which this practitioner has an affiliation. This includes past and present affiliations. Data fields in Red are Required Fields. Items labeled \"Not Complete\" can be completed by clicking on the Edit/View button.";
	    }
        else if (msgCode.equalsIgnoreCase("tLicenseRegistration"))
	    {
	        myVal += "Enter all state licenses, DEA, CDS, TPA, etc., current and past, active and inactive. Data fields in Red are Required Fields. Items labeled \"Not Complete\" can be completed by clicking on the Edit/View button.";
	    }
        else if (msgCode.equalsIgnoreCase("tMalpractice"))
	    {
	        myVal += "Enter all Malpractice Cases for this practitioner for the last 10 years. Data fields in Red are Required Fields. Items labeled \"Not Complete\" can be completed by clicking on the Edit/View button.";
	    }
        else if (msgCode.equalsIgnoreCase("tManagedCarePlan"))
	    {
	        myVal += "This section is optional but may be required by some healthcare organizations. Enter managed care plans in which you participate. Data fields in Red are Required Fields. Items labeled \"Not Complete\" can be completed by clicking on the Edit/View button.";
	    }
        else if (msgCode.equalsIgnoreCase("tOtherCertification"))
	    {
	        myVal += "Enter all other certifications such as CPR, QME, BLS, etc. Data fields in Red are Required Fields. Items labeled \"Not Complete\" can be completed by clicking on the Edit/View button.";
	    }
        else if (msgCode.equalsIgnoreCase("tPeerReference"))
	    {
	        myVal += "You are required to enter at least three Peer References for Hospital credentialing and some health plans may require it. For completeness, we recommend you list at least three Peer References. Data fields in Red are Required Fields. Items labeled \"Not Complete\" can be completed by clicking on the Edit/View button.";
	    }
        else if (msgCode.equalsIgnoreCase("tProfessionalEducation"))
	    {
	        myVal += "Enter all levels of education starting with undergraduate studies. Do not enter Internships and Residencies here, use Section 7. Data fields in Red are Required Fields. Items labeled \"Not Complete\" can be completed by clicking on the Edit/View button.";
	    }
        else if (msgCode.equalsIgnoreCase("tProfessionalLiability"))
	    {
	        myVal += "Enter current and the past 10 years (if applicable) of professional liability coverage for this practitioner. Note: If a policy is terminated and no longer active, you must enter the Termination Date for that policy. Data fields in Red are Required Fields. Items labeled \"Not Complete\" can be completed by clicking on the Edit/View button.";
	    }
        else if (msgCode.equalsIgnoreCase("tProfessionalMisconduct"))
	    {
	        myVal += "Enter any documented professional misconduct cases. Data fields in Red are Required Fields. Items labeled \"Not Complete\" can be completed by clicking on the Edit/View button.";
	    }
        else if (msgCode.equalsIgnoreCase("tProfessionalSociety"))
	    {
	        myVal += "Enter all professional associations this practitioner is a member of such as medical societies, etc. Data fields in Red are Required Fields. Items labeled \"Not Complete\" can be completed by clicking on the Edit/View button.";
	    }
        else if (msgCode.equalsIgnoreCase("tWorkHistory"))
	    {
	        myVal += "Include all past and present work. If there are time gaps between jobs, you must explain in the Work Description box. Data fields in Red are Required Fields. Items labeled \"Not Complete\" can be completed by clicking on the Edit/View button.";
	    }
        else if (msgCode.equalsIgnoreCase("tAttestR_Form"))
	    {
	        myVal += "You must identify which health care organizations and/or application forms you will be working with in order for this application to present you with the correct questions.<br><br>Please complete the following two steps.";
	    }
        else if (msgCode.equalsIgnoreCase("tPracticeMasterBasic"))
	    {
	        myVal += "Each practitioner must have at least one set of Practice Information related to him or her. If the practitioner sees patients in more than one office, then this section should have the corresponding number of offices. If practitioner is hospital-based or does not have a physical office, this section is still required for administrative purposes.";
	    }
	//practice
        else if (msgCode.equalsIgnoreCase("tPracticeMaster1"))
	    {
	        myVal += "";
	    }
        else if (msgCode.equalsIgnoreCase("tPracticeMaster2"))
	    {
	        myVal += "";
	    }
        else if (msgCode.equalsIgnoreCase("tAlliedHealthProfessional"))
	    {
	        myVal += "";
	    }
        else if (msgCode.equalsIgnoreCase("tPhysicianMaster"))
	    {
		myVal += "All items in Red are Required. Answer every question.";
	    }
        else if (msgCode.equalsIgnoreCase("pro-file_Status"))
	    {
		myVal += "";
	    }
        else if (msgCode.equalsIgnoreCase("practice_Status"))
	    {
		myVal += "";
	    }
        else if (msgCode.equalsIgnoreCase("tPhysicianPracticeLU"))
	    {
		myVal += "Each practitioner must have at least one set of Practice Information related to him or her. If the practitioner sees patients in more than one office, then this section should have the corresponding number of offices. If practitioner is hospital-based or does not have a physical office, this section is still required for administrative purposes.";
	    }
        else if (msgCode.equalsIgnoreCase("Add_Practice"))
	    {
		myVal += "Each practitioner must have at least one set of Practice Information related to him or her. If the practitioner sees patients in more than one office, then this section should have the corresponding number of offices. If practitioner is hospital-based or does not have a physical office, this section is still required for administrative purposes.";
	    }
        else if (msgCode.equalsIgnoreCase("tPhysicianPracticeLU_Pract"))
	    {
		myVal += "";
	    }
        else if (msgCode.equalsIgnoreCase("tPhysicianUserAccountLU"))
	    {
		myVal += "Each Practitioner must have only one Signature Account to electronically sign the application.";
	    }
        else if (msgCode.equalsIgnoreCase("pro-file_Attestation"))
	    {
		myVal += "";
	    }
        else if (msgCode.equalsIgnoreCase("tDocumentManagement"))
	    {
		myVal += "";
	    }
        else if (msgCode.equalsIgnoreCase("FinishedCheckList_PhysicianID"))
	    {
		myVal += "";
	    }
        else if (msgCode.equalsIgnoreCase("Supplemental_PracticeID"))
	    {
		myVal += "";
	    }
        else if (msgCode.equalsIgnoreCase("Supplemental_PhysicianID"))
	    {
		myVal += "Particular healthcare organizations may require you to complete information in this section. You should check this list if any apply to you. Data fields in Red are Required Fields. Items labeled \"Not Complete\" can be completed by clicking on the Edit/View button.";
	    }
        else if (msgCode.equalsIgnoreCase("tSuppPractOptometry"))
	    {
		myVal += "";
	    }
        else if (msgCode.equalsIgnoreCase("tSuppUECN"))
	    {
		myVal += "";
	    }
        else if (msgCode.equalsIgnoreCase("tSuppInterplan"))
	    {
		myVal += "Complete the form below or to edit the existing form, click on the Edit button. Fields in Red are required fields.";
	    }
        else
	    {
		myVal = "";
	    }

        if (!myVal.equalsIgnoreCase(""))
	{
//		myVal += " Instructions go here</span>";
	}

	return myVal;
    }


    public static String getDataInstructionsExpress(String msgCode)
    {
	String myVal = "<br><span class=instructions>";

        if (msgCode.equalsIgnoreCase("tBoardCertification"))
	    {
	        myVal += "Enter all your specialties and designate the board certification status. Do not enter certifications such as CPR, QME, etc, use Section 5. Data fields in Red are Required Fields. If the number of items you have is less than what's shown on the form, just leave them blank. If you have more items than what is available on the form you will need to use the Advanced Mode to enter the additional items. When done, click on Continue.";
	    }
        else if (msgCode.equalsIgnoreCase("tPhysicianMaster"))
	    {
	        myVal += "Please answer every question possible. Data fields in Red are Required Fields.";
	    }
        else if (msgCode.equalsIgnoreCase("tCoveringPhysicians"))
	    {
	        myVal += "List physicians covering your practice in your absence. Data fields in Red are Required Fields. If the number of items you have is less than what's shown on the form, just leave them blank. If you have more items than what is available on the form you will need to use the Advanced Mode to enter the additional items. When done, click on Continue.";
	    }
        else if (msgCode.equalsIgnoreCase("tAdditionalInformation"))
	    {
	        myVal += "";
	    }
        else if (msgCode.equalsIgnoreCase("tContinuingEducation"))
	    {
	        myVal += "Enter all accredited continuing education courses for this practitioner in the past four years. As the practitioner completes additional courses, be sure to enter them here also. Data fields in Red are Required Fields. If the number of items you have is less than what's shown on the form, just leave them blank. If you have more items than what is available on the form you will need to use the Advanced Mode to enter the additional items. When done, click on Continue.";
	    }
        else if (msgCode.equalsIgnoreCase("tExperience"))
	    {
	        myVal += "Enter Internship, Residencies, Fellowships, and Preceptorships. Do not enter undergraduate or graduate studies here, use Section 6, Education. Data fields in Red are Required Fields. If the number of items you have is less than what's shown on the form, just leave them blank. If you have more items than what is available on the form you will need to use the Advanced Mode to enter the additional items. When done, click on Continue.";
	    }
        else if (msgCode.equalsIgnoreCase("tFacilityAffiliation"))
	    {
	        myVal += "Enter all hospitals and facilities with which this practitioner has an affiliation. This includes past and present affiliations. Data fields in Red are Required Fields. If the number of items you have is less than what's shown on the form, just leave them blank. If you have more items than what is available on the form you will need to use the Advanced Mode to enter the additional items. When done, click on Continue.";
	    }
        else if (msgCode.equalsIgnoreCase("tLicenseRegistration"))
	    {
	        myVal += "Enter all state licenses, DEA, CDS, TPA, etc., current and past, active and inactive. Data fields in Red are Required Fields. If the number of items you have is less than what's shown on the form, just leave them blank. If you have more items than what is available on the form you will need to use the Advanced Mode to enter the additional items. When done, click on Continue.";
	    }
        else if (msgCode.equalsIgnoreCase("tMalpractice"))
	    {
	        myVal += "Enter all Malpractice Cases for this practitioner for the last 10 years. Data fields in Red are Required Fields. If the number of items you have is less than what's shown on the form, just leave them blank. If you have more items than what is available on the form you will need to use the Advanced Mode to enter the additional items. When done, click on Continue.";
	    }
        else if (msgCode.equalsIgnoreCase("tManagedCarePlan"))
	    {
	        myVal += "";
	    }
        else if (msgCode.equalsIgnoreCase("tOtherCertification"))
	    {
	        myVal += "Enter all other certifications such as CPR, QME, BLS, etc. Data fields in Red are Required Fields. If the number of items you have is less than what's shown on the form, just leave them blank. If you have more items than what is available on the form you will need to use the Advanced Mode to enter the additional items. When done, click on Continue.";
	    }
        else if (msgCode.equalsIgnoreCase("tPeerReference"))
	    {
	        myVal += "You are required to enter at least three Peer References for Hospital credentialing and some health plans may require it. For completeness, we recommend you list at least three Peer References. Data fields in Red are Required Fields. If the number of items you have is less than what's shown on the form, just leave them blank. If you have more items than what is available on the form you will need to use the Advanced Mode to enter the additional items. When done, click on Continue.";
	    }
        else if (msgCode.equalsIgnoreCase("tProfessionalEducation"))
	    {
	        myVal += "Enter all levels of education starting with undergraduate studies. Do not enter Internships and Residencies here, use Section 7. Data fields in Red are Required Fields. If the number of items you have is less than what's shown on the form, just leave them blank. If you have more items than what is available on the form you will need to use the Advanced Mode to enter the additional items. When done, click on Continue.";
	    }
        else if (msgCode.equalsIgnoreCase("tProfessionalLiability"))
	    {
	        myVal += "Enter current and the past 10 years (if applicable) of professional liability coverage for this practitioner. Note: If a policy is terminated and no longer active, you must enter the Termination Date for that policy. Data fields in Red are Required Fields. If the number of items you have is less than what's shown on the form, just leave them blank. If you have more items than what is available on the form you will need to use the Advanced Mode to enter the additional items. When done, click on Continue.";
	    }
        else if (msgCode.equalsIgnoreCase("tProfessionalMisconduct"))
	    {
	        myVal += "Enter any documented professional misconduct cases. Data fields in Red are Required Fields. If the number of items you have is less than what's shown on the form, just leave them blank. If you have more items than what is available on the form you will need to use the Advanced Mode to enter the additional items. When done, click on Continue.";
	    }
        else if (msgCode.equalsIgnoreCase("tProfessionalSociety"))
	    {
	        myVal += "Enter all professional associations this practitioner is a member of such as medical societies, etc. Data fields in Red are Required Fields. If the number of items you have is less than what's shown on the form, just leave them blank. If you have more items than what is available on the form you will need to use the Advanced Mode to enter the additional items. When done, click on Continue.";
	    }
        else if (msgCode.equalsIgnoreCase("tWorkHistory"))
	    {
	        myVal += "Include all past and present work. If there are time gaps between jobs, you must explain in the Comment box. Data fields in Red are Required Fields. If the number of items you have is less than what's shown on the form, just leave them blank. If you have more items than what is available on the form you will need to use the Advanced Mode to enter the additional items. When done, click on Continue.";
	    }
        else if (msgCode.equalsIgnoreCase("tAttestR_Form"))
	    {
	        myVal += "You must identify which health care organizations and/or application forms you will be working with in order for this application to present you with the correct questions.<br><br>Please complete the following two steps.";
	    }
        else
	    {
		myVal = "";
	    }

	return myVal;
    }





    public static String getDataStepNumber(String msgCode)
    {
	String myVal = "";

        if (msgCode.equalsIgnoreCase("tBoardCertification"))
	    {
	        myVal += "4";
	    }
        else if (msgCode.equalsIgnoreCase("tCoveringPhysicians"))
	    {
	        myVal += "2";
	    }
        else if (msgCode.equalsIgnoreCase("Physician_HCOAuth"))
	    {
	        myVal += "19";
	    }
        else if (msgCode.equalsIgnoreCase("tContinuingEducation"))
	    {
	        myVal += "16";
	    }
        else if (msgCode.equalsIgnoreCase("tExperience"))
	    {
	        myVal += "7";
	    }
        else if (msgCode.equalsIgnoreCase("tFacilityAffiliation"))
	    {
	        myVal += "10";
	    }
        else if (msgCode.equalsIgnoreCase("tLicenseRegistration"))
	    {
	        myVal += "3";
	    }
        else if (msgCode.equalsIgnoreCase("tMalpractice"))
	    {
	        myVal += "14";
	    }
        else if (msgCode.equalsIgnoreCase("tManagedCarePlan"))
	    {
	        myVal += "17";
	    }
        else if (msgCode.equalsIgnoreCase("tOtherCertification"))
	    {
	        myVal += "5";
	    }
        else if (msgCode.equalsIgnoreCase("tPeerReference"))
	    {
	        myVal += "11";
	    }
        else if (msgCode.equalsIgnoreCase("tProfessionalEducation"))
	    {
	        myVal += "6";
	    }
        else if (msgCode.equalsIgnoreCase("tProfessionalLiability"))
	    {
	        myVal += "9";
	    }
        else if (msgCode.equalsIgnoreCase("tProfessionalMisconduct"))
	    {
	        myVal += "15";
	    }
        else if (msgCode.equalsIgnoreCase("tProfessionalSociety"))
	    {
	        myVal += "12";
	    }
        else if (msgCode.equalsIgnoreCase("tWorkHistory"))
	    {
	        myVal += "8";
	    }
        else if (msgCode.equalsIgnoreCase("tAttestR_Form"))
	    {
	        myVal += "13";
	    }
        else if (msgCode.equalsIgnoreCase("tPracticeMasterBasic"))
	    {
	        myVal += "20";
	    }
        else if (msgCode.equalsIgnoreCase("tPhysicianMaster"))
	    {
		myVal += "1";
	    }
        else if (msgCode.equalsIgnoreCase("tPhysicianPracticeLU"))
	    {
		myVal += "20";
	    }
        else if (msgCode.equalsIgnoreCase("tPhysicianUserAccountLU"))
	    {
		myVal += "21";
	    }
        else if (msgCode.equalsIgnoreCase("pro-file_Attestation"))
	    {
		myVal += "22";
	    }
        else if (msgCode.equalsIgnoreCase("tDocumentManagement"))
	    {
		myVal += "23";
	    }
        else if (msgCode.equalsIgnoreCase("FinishedCheckList_PhysicianID"))
	    {
		myVal += "24";
	    }
        else if (msgCode.equalsIgnoreCase("Supplemental_PhysicianID"))
	    {
		myVal += "18";
	    }
        else if (msgCode.equalsIgnoreCase("tSuppUECN"))
	    {
		myVal += "18";
	    }
        else if (msgCode.equalsIgnoreCase("tSuppInterplan"))
	    {
		myVal += "18";
	    }
        else if (msgCode.equalsIgnoreCase("cl_Practice_Physician_via_Admin.jsp"))
	    {
		myVal += "20";
	    }
        else
	    {
		myVal = "-1";
	    }
	   

	return myVal;
    }




    public static String getDataTitle(String msgCode)
    {
	String myVal = "";
	boolean includeDR = true;
	includeDR = false;

	myVal+= getDataCategory(msgCode);


	if (includeDR)
	{
		myVal+=" Records";
	}

	return myVal;

    }




    public static String getInterviewLink(String dataCategory, String Dir)
    {
	String myVal = "";

        if (Dir.equalsIgnoreCase("back"))
	{
		myVal += "        <a href=";
		String mylink = getInterviewLinkRaw(dataCategory, Dir);
		myVal += mylink;
		myVal += "><img src=images/back.gif border=0></a>";
		if (mylink.equalsIgnoreCase("#"))
		{
			myVal="";
		}
	}
        else if (Dir.equalsIgnoreCase("next"))
	{
		myVal += "        <a href=";
		String mylink = getInterviewLinkRaw(dataCategory, Dir);
		myVal += mylink;
		myVal += "><img src=images/continue.gif border=0></a>";
		if (mylink.equalsIgnoreCase("#"))
		{
			myVal="";
		}
	}
	return myVal;

    }



    public static String getExpressLink(String dataCategory, String Dir, Integer logicID)
    {
	String myVal = "";

        if (Dir.equalsIgnoreCase("back"))
	{
		myVal += "        <a href=";
		String mylink = getExpressLinkRaw(dataCategory, Dir,logicID);
		myVal += mylink;
		myVal += "><img src=images/back.gif border=0></a>";
		if (mylink.equalsIgnoreCase("#"))
		{
			myVal="";
		}
	}
        else if (Dir.equalsIgnoreCase("next"))
	{
		myVal += "        <a href=";
		String mylink = getExpressLinkRaw(dataCategory, Dir,logicID);
		myVal += mylink;
		myVal += "><img src=images/continue.gif border=0></a>";
		if (mylink.equalsIgnoreCase("#"))
		{
			myVal="";
		}
	}
	return myVal;

    }



    public static String getExpressLinkRaw(String dataCategory, String Dir, Integer logicID)
    {
	String myVal = "";
       if (logicID.intValue()==1)
       {
        if (Dir.equalsIgnoreCase("back"))
	{
        if (dataCategory.equalsIgnoreCase("tBoardCertification"))
	    {
		myVal += "tLicenseRegistration_main_LicenseRegistration_PhysicianID_Express.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("tCoveringPhysicians"))
	    {
		myVal += "tPhysicianMaster_form_Express.jsp?EDIT=edit";
	    }
        else if (dataCategory.equalsIgnoreCase("tAdditionalInformation"))
	    {
		myVal += "SupplementalData_main_PhysicianID_Express.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("tContinuingEducation"))
	    {
		myVal += "tProfessionalMisconduct_main_ProfessionalMisconduct_PhysicianID_Express.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("tExperience"))
	    {
		myVal += "tProfessionalEducation_main_ProfessionalEducation_PhysicianID_Express.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("tFacilityAffiliation"))
	    {
		myVal += "tProfessionalLiability_main_ProfessionalLiability_PhysicianID_Express.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("tLicenseRegistration"))
	    {
		myVal += "tCoveringPhysicians_main_CoveringPhysicians_PhysicianID_Express.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("tMalpractice"))
	    {
		myVal += "tAttestR_form2_Express.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("tManagedCarePlan"))
	    {
		myVal += "tContinuingEducation_main_ContinuingEducation_PhysicianID_Express.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("tOtherCertification"))
	    {
		myVal += "tBoardCertification_main_BoardCertification_PhysicianID_Express.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("tPeerReference"))
	    {
		myVal += "tFacilityAffiliation_main_FacilityAffiliation_PhysicianID_Express.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("tProfessionalEducation"))
	    {
		myVal += "tOtherCertification_main_OtherCertification_PhysicianID_Express.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("tProfessionalLiability"))
	    {
		myVal += "tWorkHistory_main_WorkHistory_PhysicianID_Express.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("tProfessionalMisconduct"))
	    {
		myVal += "tMalpractice_main_Malpractice_PhysicianID_Express.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("tProfessionalSociety"))
	    {
		myVal += "tPeerReference_main_PeerReference_PhysicianID_Express.jsp";
	    }
	else if (dataCategory.equalsIgnoreCase("tWorkHistory"))
	    {
		myVal += "tExperience_main_Experience_PhysicianID_Express.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("tAlliedHealthProfessional"))
	    {
		myVal += "tPracticeMaster_form2.jsp?EDIT=edit";
	    }
        else if (dataCategory.equalsIgnoreCase("tPhysicianMaster"))
	    {
		myVal += "pro-file_Status_Express.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("pro-file_Status"))
	    {
		myVal += "#";
	    }
        else if (dataCategory.equalsIgnoreCase("practice_Status"))
	    {
		myVal += "#";
	    }
        else if (dataCategory.equalsIgnoreCase("tPhysicianPracticeLU"))
	    {
		myVal += "Physician_HCOAuth.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("tPhysicianPracticeLU_Pract"))
	    {
		myVal += "tAlliedHealthProfessional_main_AlliedHealthProfessional_PracticeID.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("tPhysicianUserAccountLU"))
	    {
		myVal += "tPhysicianPracticeLU_PhysicianID_Express.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("pro-file_Attestation"))
	    {
		myVal += "tPhysicianUserAccountLU_main2_UserAccount_PhysicianID_Express.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("tDocumentManagement"))
	    {
		myVal += "pro-file_Attestation.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("FinishedCheckList_PhysicianID"))
	    {
		myVal += "tDocumentManagement_PhysicianID_Express.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("tAttestR_form"))
	    {
		myVal += "tProfessionalSociety_main_ProfessionalSociety_PhysicianID_Express.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("tPracticeMaster1"))
	    {
		myVal += "practice_Status.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("tPracticeMaster2"))
	    {
		myVal += "tPracticeMaster_form.jsp?EDIT=edit";
	    }
        else if (dataCategory.equalsIgnoreCase("Supplemental_PhysicianID"))
	    {
		myVal += "tManagedCarePlan_main_ManagedCarePlan_PhysicianID_Express.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("Supplemental_PracticeID"))
	    {
		myVal += "tPhysicianPracticeLU_main2_LU_PracticeID.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("tSuppUECN"))
	    {
		myVal += "SupplementalData_main_PhysicianID_Express.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("tSuppInterplan"))
	    {
		myVal += "SupplementalData_main_PhysicianID_Express.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("tSuppPractOptometry"))
	    {
		myVal += "SupplementalData_main_PracticeID.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("ExtraInformation_PhysicianID"))
	    {
		myVal += "tManagedCarePlan_main_ManagedCarePlan_PhysicianID_Express.jsp";
	    }
        else
	    {
		myVal += "#";
	    }
        }
	else if (Dir.equalsIgnoreCase("next"))
	{

        if (dataCategory.equalsIgnoreCase("tBoardCertification"))
	    {
		myVal += "tOtherCertification_main_OtherCertification_PhysicianID_Express.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("tCoveringPhysicians"))
	    {
		myVal += "tLicenseRegistration_main_LicenseRegistration_PhysicianID_Express.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("tAdditionalInformation"))
	    {
		myVal += "tPhysicianPracticeLU_PhysicianID_Express.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("tContinuingEducation"))
	    {
		myVal += "ExtraInformation_PhysicianID.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("tExperience"))
	    {
		myVal += "tWorkHistory_main_WorkHistory_PhysicianID_Express.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("tFacilityAffiliation"))
	    {
		myVal += "tPeerReference_main_PeerReference_PhysicianID_Express.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("tLicenseRegistration"))
	    {
		myVal += "tBoardCertification_main_BoardCertification_PhysicianID_Express.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("tMalpractice"))
	    {
		myVal += "tProfessionalMisconduct_main_ProfessionalMisconduct_PhysicianID_Express.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("tManagedCarePlan"))
	    {
		myVal += "ExtraInformation_PhysicianID.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("ExtraInformation_PhysicianID"))
	    {
		myVal += "SupplementalData_main_PhysicianID.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("tOtherCertification"))
	    {
		myVal += "tProfessionalEducation_main_ProfessionalEducation_PhysicianID_Express.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("tPeerReference"))
	    {
		myVal += "tProfessionalSociety_main_ProfessionalSociety_PhysicianID_Express.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("tProfessionalEducation"))
	    {
		myVal += "tExperience_main_Experience_PhysicianID_Express.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("tProfessionalLiability"))
	    {
		myVal += "tFacilityAffiliation_main_FacilityAffiliation_PhysicianID_Express.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("tProfessionalMisconduct"))
	    {
		myVal += "tContinuingEducation_main_ContinuingEducation_PhysicianID_Express.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("tProfessionalSociety"))
	    {
		myVal += "tAttestR_form2_Express.jsp";
	    }
	else if (dataCategory.equalsIgnoreCase("tWorkHistory"))
	    {
		myVal += "tProfessionalLiability_main_ProfessionalLiability_PhysicianID_Express.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("tAlliedHealthProfessional"))
	    {
		myVal += "tPhysicianPracticeLU_main2_LU_PracticeID.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("tPhysicianMaster"))
	    {
		myVal += "tCoveringPhysicians_main_CoveringPhysicians_PhysicianID_Express.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("pro-file_Status_Express"))
	    {
		myVal += "tPhysicianMaster_form_Express.jsp?EDIT=edit";
	    }
        else if (dataCategory.equalsIgnoreCase("practice_Status"))
	    {
		myVal += "tPracticeMaster_form.jsp?EDIT=edit";
	    }
        else if (dataCategory.equalsIgnoreCase("tPhysicianPracticeLU"))
	    {
		myVal += "tPhysicianUserAccountLU_main2_UserAccount_PhysicianID_Express.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("tPhysicianPracticeLU_Pract"))
	    {
		myVal += "SupplementalData_main_PracticeID.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("tPhysicianUserAccountLU"))
	    {
		myVal += "pro-file_Attestation.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("pro-file_Attestation"))
	    {
		myVal += "tDocumentManagement_PhysicianID_Express.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("tDocumentManagement"))
	    {
		myVal += "FinishedCheckList_PhysicianID_Express.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("FinishedCheckList_PhysicianID"))
	    {
		myVal += "pro-file_Status.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("tAttestR_form"))
	    {
		myVal += "tMalpractice_main_Malpractice_PhysicianID_Express.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("tPracticeMaster1"))
	    {
		myVal += "tPracticeMaster_form2.jsp?EDIT=edit";
	    }
        else if (dataCategory.equalsIgnoreCase("tPracticeMaster2"))
	    {
		myVal += "tAlliedHealthProfessional_main_AlliedHealthProfessional_PracticeID.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("Supplemental_PhysicianID"))
	    {
		myVal += "Physician_HCOAuth.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("Supplemental_PracticeID"))
	    {
		myVal += "practice_status.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("tGroupSecurity"))
	    {
		myVal += "tSecurityGroupMaster_form.jsp?nullParam=null&EDIT=edit";
	    }
        else if (dataCategory.equalsIgnoreCase("tSuppUECN"))
	    {
		myVal += "SupplementalData_main_PhysicianID_Express.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("tSuppInterplan"))
	    {
		myVal += "SupplementalData_main_PhysicianID_Express.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("tSuppPractOptometry"))
	    {
		myVal += "SupplementalData_main_PracticeID.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("tNIM3_CaseAccount"))
	    {
		myVal += "tNIM3_CaseAccount_main_NIM3_CaseAccount_PayerID.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("tNIM3_Referral"))
	    {
		myVal += "tNIM3_Referral_main_NIM3_Referral_CaseID.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("tNIM3_Encounter"))
	    {
		myVal += "tNIM3_Encounter_main_NIM3_Encounter_ReferralID.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("tNIM3_Service"))
	    {
		myVal += "tNIM3_Service_main_NIM3_Service_EncounterID.jsp";
	    }
        else
	    {
		myVal += "#";
	    }
        }
       }
       else if (logicID.intValue()==2)
       {
        if (Dir.equalsIgnoreCase("back"))
	{
        if (dataCategory.equalsIgnoreCase("tBoardCertification"))
	    {
		myVal += "tLicenseRegistration_main_LicenseRegistration_PhysicianID_Express.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("tCoveringPhysicians"))
	    {
		myVal += "tPhysicianMaster_form_Express.jsp?EDIT=edit";
	    }
        else if (dataCategory.equalsIgnoreCase("tAdditionalInformation"))
	    {
		myVal += "SupplementalData_main_PhysicianID_Express.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("tContinuingEducation"))
	    {
		myVal += "tProfessionalMisconduct_main_ProfessionalMisconduct_PhysicianID_Express.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("tExperience"))
	    {
		myVal += "tProfessionalEducation_main_ProfessionalEducation_PhysicianID_Express.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("tFacilityAffiliation"))
	    {
		myVal += "tProfessionalLiability_main_ProfessionalLiability_PhysicianID_Express.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("tLicenseRegistration"))
	    {
		myVal += "tCoveringPhysicians_main_CoveringPhysicians_PhysicianID_Express.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("tMalpractice"))
	    {
		myVal += "tAttestR_form2_Express.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("tManagedCarePlan"))
	    {
		myVal += "tContinuingEducation_main_ContinuingEducation_PhysicianID_Express.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("tOtherCertification"))
	    {
		myVal += "tBoardCertification_main_BoardCertification_PhysicianID_Express.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("tPeerReference"))
	    {
		myVal += "tFacilityAffiliation_main_FacilityAffiliation_PhysicianID_Express.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("tProfessionalEducation"))
	    {
		myVal += "tBoardCertification_main_BoardCertification_PhysicianID_Express.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("tProfessionalLiability"))
	    {
		myVal += "tWorkHistory_main_WorkHistory_PhysicianID_Express.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("tProfessionalMisconduct"))
	    {
		myVal += "tMalpractice_main_Malpractice_PhysicianID_Express.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("tProfessionalSociety"))
	    {
		myVal += "tFacilityAffiliation_main_FacilityAffiliation_PhysicianID_Express.jsp";
	    }
	else if (dataCategory.equalsIgnoreCase("tWorkHistory"))
	    {
		myVal += "tExperience_main_Experience_PhysicianID_Express.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("tAlliedHealthProfessional"))
	    {
		myVal += "tPracticeMaster_form2.jsp?EDIT=edit";
	    }
        else if (dataCategory.equalsIgnoreCase("tPhysicianMaster"))
	    {
		myVal += "pro-file_Status_Express.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("pro-file_Status"))
	    {
		myVal += "#";
	    }
        else if (dataCategory.equalsIgnoreCase("practice_Status"))
	    {
		myVal += "#";
	    }
        else if (dataCategory.equalsIgnoreCase("tPhysicianPracticeLU"))
	    {
		myVal += "Physician_HCOAuth.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("tPhysicianPracticeLU_Pract"))
	    {
		myVal += "tAlliedHealthProfessional_main_AlliedHealthProfessional_PracticeID.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("tPhysicianUserAccountLU"))
	    {
		myVal += "tPhysicianPracticeLU_PhysicianID_Express.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("pro-file_Attestation"))
	    {
		myVal += "tPhysicianUserAccountLU_main2_UserAccount_PhysicianID_Express.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("tDocumentManagement"))
	    {
		myVal += "pro-file_Attestation.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("FinishedCheckList_PhysicianID"))
	    {
		myVal += "tDocumentManagement_PhysicianID_Express.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("tAttestR_form"))
	    {
		myVal += "tFacilityAffiliation_main_FacilityAffiliation_PhysicianID_Express.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("tPracticeMaster1"))
	    {
		myVal += "practice_Status.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("tPracticeMaster2"))
	    {
		myVal += "tPracticeMaster_form.jsp?EDIT=edit";
	    }
        else if (dataCategory.equalsIgnoreCase("Supplemental_PhysicianID"))
	    {
		myVal += "tManagedCarePlan_main_ManagedCarePlan_PhysicianID_Express.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("Supplemental_PracticeID"))
	    {
		myVal += "tPhysicianPracticeLU_main2_LU_PracticeID.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("tSuppUECN"))
	    {
		myVal += "SupplementalData_main_PhysicianID_Express.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("tSuppInterplan"))
	    {
		myVal += "SupplementalData_main_PhysicianID_Express.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("tSuppPractOptometry"))
	    {
		myVal += "SupplementalData_main_PracticeID.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("ExtraInformation_PhysicianID"))
	    {
		myVal += "tManagedCarePlan_main_ManagedCarePlan_PhysicianID_Express.jsp";
	    }
        else
	    {
		myVal += "#";
	    }
        }
	else if (Dir.equalsIgnoreCase("next"))
	{

        if (dataCategory.equalsIgnoreCase("tBoardCertification"))
	    {
		myVal += "tProfessionalEducation_main_ProfessionalEducation_PhysicianID_Express.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("tCoveringPhysicians"))
	    {
		myVal += "tLicenseRegistration_main_LicenseRegistration_PhysicianID_Express.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("tAdditionalInformation"))
	    {
		myVal += "tPhysicianPracticeLU_PhysicianID_Express.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("tContinuingEducation"))
	    {
		myVal += "ExtraInformation_PhysicianID.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("tExperience"))
	    {
		myVal += "tWorkHistory_main_WorkHistory_PhysicianID_Express.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("tFacilityAffiliation"))
	    {
		myVal += "tAttestR_form2_Express.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("tLicenseRegistration"))
	    {
		myVal += "tBoardCertification_main_BoardCertification_PhysicianID_Express.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("tMalpractice"))
	    {
		myVal += "tProfessionalMisconduct_main_ProfessionalMisconduct_PhysicianID_Express.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("tManagedCarePlan"))
	    {
		myVal += "ExtraInformation_PhysicianID.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("ExtraInformation_PhysicianID"))
	    {
		myVal += "SupplementalData_main_PhysicianID.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("tOtherCertification"))
	    {
		myVal += "tProfessionalEducation_main_ProfessionalEducation_PhysicianID_Express.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("tPeerReference"))
	    {
		myVal += "tProfessionalSociety_main_ProfessionalSociety_PhysicianID_Express.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("tProfessionalEducation"))
	    {
		myVal += "tExperience_main_Experience_PhysicianID_Express.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("tProfessionalLiability"))
	    {
		myVal += "tFacilityAffiliation_main_FacilityAffiliation_PhysicianID_Express.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("tProfessionalMisconduct"))
	    {
		myVal += "ExtraInformation_PhysicianID.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("tProfessionalSociety"))
	    {
		myVal += "tAttestR_form2_Express.jsp";
	    }
	else if (dataCategory.equalsIgnoreCase("tWorkHistory"))
	    {
		myVal += "tProfessionalLiability_main_ProfessionalLiability_PhysicianID_Express.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("tAlliedHealthProfessional"))
	    {
		myVal += "tPhysicianPracticeLU_main2_LU_PracticeID.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("tPhysicianMaster"))
	    {
		myVal += "tCoveringPhysicians_main_CoveringPhysicians_PhysicianID_Express.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("pro-file_Status_Express"))
	    {
		myVal += "tPhysicianMaster_form_Express.jsp?EDIT=edit";
	    }
        else if (dataCategory.equalsIgnoreCase("practice_Status"))
	    {
		myVal += "tPracticeMaster_form.jsp?EDIT=edit";
	    }
        else if (dataCategory.equalsIgnoreCase("tPhysicianPracticeLU"))
	    {
		myVal += "tPhysicianUserAccountLU_main2_UserAccount_PhysicianID_Express.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("tPhysicianPracticeLU_Pract"))
	    {
		myVal += "SupplementalData_main_PracticeID.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("tPhysicianUserAccountLU"))
	    {
		myVal += "pro-file_Attestation.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("pro-file_Attestation"))
	    {
		myVal += "tDocumentManagement_PhysicianID_Express.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("tDocumentManagement"))
	    {
		myVal += "FinishedCheckList_PhysicianID_Express.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("FinishedCheckList_PhysicianID"))
	    {
		myVal += "pro-file_Status.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("tAttestR_form"))
	    {
		myVal += "tMalpractice_main_Malpractice_PhysicianID_Express.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("tPracticeMaster1"))
	    {
		myVal += "tPracticeMaster_form2.jsp?EDIT=edit";
	    }
        else if (dataCategory.equalsIgnoreCase("tPracticeMaster2"))
	    {
		myVal += "tAlliedHealthProfessional_main_AlliedHealthProfessional_PracticeID.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("Supplemental_PhysicianID"))
	    {
		myVal += "Physician_HCOAuth.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("Supplemental_PracticeID"))
	    {
		myVal += "practice_status.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("tGroupSecurity"))
	    {
		myVal += "tSecurityGroupMaster_form.jsp?nullParam=null&EDIT=edit";
	    }
        else if (dataCategory.equalsIgnoreCase("tSuppUECN"))
	    {
		myVal += "SupplementalData_main_PhysicianID_Express.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("tSuppInterplan"))
	    {
		myVal += "SupplementalData_main_PhysicianID_Express.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("tSuppPractOptometry"))
	    {
		myVal += "SupplementalData_main_PracticeID.jsp";
	    }
        else
	    {
		myVal += "#";
	    }
        }
       }



	return myVal;
    }


    public static String getDataCategoryLink(String dataCategory, String sKey)
    {
	String myVal = "";

        if (dataCategory.equalsIgnoreCase("tBoardCertification"))
	    {
		myVal += "tBoardCertification_main_BoardCertification_PhysicianID";
	    }
        else if (dataCategory.equalsIgnoreCase("tCoveringPhysicians"))
	    {
		myVal += "tCoveringPhysicians_main_CoveringPhysicians_PhysicianID";
	    }
        else if (dataCategory.equalsIgnoreCase("tAttestR_Form"))
	    {
		myVal += "tAttestR_form2";
	    }
        else if (dataCategory.equalsIgnoreCase("tAdditionalInformation"))
	    {
		myVal += "tAdditionalInformation_main_AdditionalInformation_PhysicianID";
	    }
        else if (dataCategory.equalsIgnoreCase("tContinuingEducation"))
	    {
		myVal += "tContinuingEducation_main_ContinuingEducation_PhysicianID";
	    }
        else if (dataCategory.equalsIgnoreCase("tExperience"))
	    {
		myVal += "tExperience_main_Experience_PhysicianID";
	    }
        else if (dataCategory.equalsIgnoreCase("tFacilityAffiliation"))
	    {
		myVal += "tFacilityAffiliation_main_FacilityAffiliation_PhysicianID";
	    }
        else if (dataCategory.equalsIgnoreCase("tLicenseRegistration"))
	    {
		myVal += "tLicenseRegistration_main_LicenseRegistration_PhysicianID";
	    }
        else if (dataCategory.equalsIgnoreCase("tMalpractice"))
	    {
		myVal += "tMalpractice_main_Malpractice_PhysicianID";
	    }
        else if (dataCategory.equalsIgnoreCase("tManagedCarePlan"))
	    {
		myVal += "tManagedCarePlan_main_ManagedCarePlan_PhysicianID";
	    }
        else if (dataCategory.equalsIgnoreCase("tOtherCertification"))
	    {
		myVal += "tOtherCertification_main_OtherCertification_PhysicianID";
	    }
        else if (dataCategory.equalsIgnoreCase("tPeerReference"))
	    {
		myVal += "tPeerReference_main_PeerReference_PhysicianID";
	    }
        else if (dataCategory.equalsIgnoreCase("tProfessionalEducation"))
	    {
		myVal += "tProfessionalEducation_main_ProfessionalEducation_PhysicianID";
	    }
        else if (dataCategory.equalsIgnoreCase("tProfessionalLiability"))
	    {
		myVal += "tProfessionalLiability_main_ProfessionalLiability_PhysicianID";
	    }
        else if (dataCategory.equalsIgnoreCase("tProfessionalMisconduct"))
	    {
		myVal += "tProfessionalMisconduct_main_ProfessionalMisconduct_PhysicianID";
	    }
        else if (dataCategory.equalsIgnoreCase("tProfessionalSociety"))
	    {
		myVal += "tProfessionalSociety_main_ProfessionalSociety_PhysicianID";
	    }
	else if (dataCategory.equalsIgnoreCase("tWorkHistory"))
	    {
		myVal += "tWorkHistory_main_WorkHistory_PhysicianID";
	    }
        else if (dataCategory.equalsIgnoreCase("tAlliedHealthProfessional"))
	    {
		myVal += "tAlliedHealthProfessional_main_AlliedHealthProfessional_PhysicianID";
	    }
        else if (dataCategory.equalsIgnoreCase("tPhysicianMaster"))
	    {
		myVal += "tPhysicianMaster_form";
	    }
        else if (dataCategory.equalsIgnoreCase("tNIM3_CaseAccount"))
	    {
		myVal += "tNIM3_CaseAccount_main_NIM3_CaseAccount_PayerID";
	    }
        else if (dataCategory.equalsIgnoreCase("tNIM3_Referral"))
	    {
		myVal += "tNIM3_Referral_main_NIM3_Referral_CaseID";
	    }
        else if (dataCategory.equalsIgnoreCase("tNIM3_Encounter"))
	    {
		myVal += "tNIM3_Encounter_main_NIM3_Encounter_ReferralID";
	    }
        else if (dataCategory.equalsIgnoreCase("tNIM3_Service"))
	    {
		myVal += "tNIM3_Service_main_NIM3_Service_EncounterID";
	    }
        else
	    {
		myVal += "#";
	    }

	if (!myVal.equalsIgnoreCase("#"))
	{
	    if (sKey.equalsIgnoreCase("Express"))
	    {
	        myVal+="_Express.jsp";
	    }
	    else
	    {
	        myVal+=".jsp";
	    }
	}
	return myVal;
    }








    public static String getInterviewLinkRaw(String dataCategory, String Dir)
    {
	String myVal = "";

        if (Dir.equalsIgnoreCase("back"))
	{
        if (dataCategory.equalsIgnoreCase("tBoardCertification"))
	    {
		myVal += "tLicenseRegistration_main_LicenseRegistration_PhysicianID.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("tCoveringPhysicians"))
	    {
		myVal += "tPhysicianMaster_form.jsp?EDIT=edit";
	    }
        else if (dataCategory.equalsIgnoreCase("Physician_HCOAuth"))
	    {
		myVal += "SupplementalData_main_PhysicianID.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("tContinuingEducation"))
	    {
		myVal += "tProfessionalMisconduct_main_ProfessionalMisconduct_PhysicianID.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("tExperience"))
	    {
		myVal += "tProfessionalEducation_main_ProfessionalEducation_PhysicianID.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("tFacilityAffiliation"))
	    {
		myVal += "tProfessionalLiability_main_ProfessionalLiability_PhysicianID.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("tLicenseRegistration"))
	    {
		myVal += "tCoveringPhysicians_main_CoveringPhysicians_PhysicianID.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("tMalpractice"))
	    {
		myVal += "tAttestR_form2.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("tManagedCarePlan"))
	    {
		myVal += "tContinuingEducation_main_ContinuingEducation_PhysicianID.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("tOtherCertification"))
	    {
		myVal += "tBoardCertification_main_BoardCertification_PhysicianID.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("tPeerReference"))
	    {
		myVal += "tFacilityAffiliation_main_FacilityAffiliation_PhysicianID.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("tProfessionalEducation"))
	    {
		myVal += "tOtherCertification_main_OtherCertification_PhysicianID.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("tProfessionalLiability"))
	    {
		myVal += "tWorkHistory_main_WorkHistory_PhysicianID.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("tProfessionalMisconduct"))
	    {
		myVal += "tMalpractice_main_Malpractice_PhysicianID.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("tProfessionalSociety"))
	    {
		myVal += "tPeerReference_main_PeerReference_PhysicianID.jsp";
	    }
	else if (dataCategory.equalsIgnoreCase("tWorkHistory"))
	    {
		myVal += "tExperience_main_Experience_PhysicianID.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("tAlliedHealthProfessional"))
	    {
		myVal += "tPracticeMaster_form2.jsp?EDIT=edit";
	    }
        else if (dataCategory.equalsIgnoreCase("tPhysicianMaster"))
	    {
		myVal += "pro-file_Status.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("pro-file_Status"))
	    {
		myVal += "#";
	    }
        else if (dataCategory.equalsIgnoreCase("practice_Status"))
	    {
		myVal += "#";
	    }
        else if (dataCategory.equalsIgnoreCase("tPhysicianPracticeLU"))
	    {
		myVal += "Physician_HCOAuth.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("tPhysicianPracticeLU_Pract"))
	    {
		myVal += "tAlliedHealthProfessional_main_AlliedHealthProfessional_PracticeID.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("tPhysicianUserAccountLU"))
	    {
		myVal += "tPhysicianPracticeLU_PhysicianID.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("pro-file_Attestation"))
	    {
		myVal += "tPhysicianUserAccountLU_main2_UserAccount_PhysicianID.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("tDocumentManagement"))
	    {
		myVal += "pro-file_Attestation.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("FinishedCheckList_PhysicianID"))
	    {
		myVal += "tDocumentManagement_PhysicianID.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("tAttestR_form"))
	    {
		myVal += "tProfessionalSociety_main_ProfessionalSociety_PhysicianID.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("tPracticeMaster1"))
	    {
		myVal += "practice_Status.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("tPracticeMaster2"))
	    {
		myVal += "tPracticeMaster_form.jsp?EDIT=edit";
	    }
        else if (dataCategory.equalsIgnoreCase("Supplemental_PhysicianID"))
	    {
		myVal += "tManagedCarePlan_main_ManagedCarePlan_PhysicianID.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("Supplemental_PracticeID"))
	    {
		myVal += "tPhysicianPracticeLU_main2_LU_PracticeID.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("tSuppUECN"))
	    {
		myVal += "SupplementalData_main_PhysicianID.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("tSuppInterplan"))
	    {
		myVal += "SupplementalData_main_PhysicianID.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("tSuppPractOptometry"))
	    {
		myVal += "SupplementalData_main_PracticeID.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("ExtraInformation_PhysicianID"))
	    {
		myVal += "tManagedCarePlan_main_ManagedCarePlan_PhysicianID.jsp";
	    }
        else
	    {
		myVal += "#";
	    }
        }
	else if (Dir.equalsIgnoreCase("next"))
	{

        if (dataCategory.equalsIgnoreCase("tBoardCertification"))
	    {
		myVal += "tOtherCertification_main_OtherCertification_PhysicianID.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("tCoveringPhysicians"))
	    {
		myVal += "tLicenseRegistration_main_LicenseRegistration_PhysicianID.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("Physician_HCOAuth"))
	    {
		myVal += "tPhysicianPracticeLU_PhysicianID.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("tContinuingEducation"))
	    {
		myVal += "tManagedCarePlan_main_ManagedCarePlan_PhysicianID.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("tExperience"))
	    {
		myVal += "tWorkHistory_main_WorkHistory_PhysicianID.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("tFacilityAffiliation"))
	    {
		myVal += "tPeerReference_main_PeerReference_PhysicianID.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("tLicenseRegistration"))
	    {
		myVal += "tBoardCertification_main_BoardCertification_PhysicianID.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("tMalpractice"))
	    {
		myVal += "tProfessionalMisconduct_main_ProfessionalMisconduct_PhysicianID.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("tManagedCarePlan"))
	    {
		myVal += "ExtraInformation_PhysicianID.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("ExtraInformation_PhysicianID"))
	    {
		myVal += "SupplementalData_main_PhysicianID.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("tOtherCertification"))
	    {
		myVal += "tProfessionalEducation_main_ProfessionalEducation_PhysicianID.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("tPeerReference"))
	    {
		myVal += "tProfessionalSociety_main_ProfessionalSociety_PhysicianID.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("tProfessionalEducation"))
	    {
		myVal += "tExperience_main_Experience_PhysicianID.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("tProfessionalLiability"))
	    {
		myVal += "tFacilityAffiliation_main_FacilityAffiliation_PhysicianID.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("tProfessionalMisconduct"))
	    {
		myVal += "tContinuingEducation_main_ContinuingEducation_PhysicianID.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("tProfessionalSociety"))
	    {
		myVal += "tAttestR_form2.jsp";
	    }
	else if (dataCategory.equalsIgnoreCase("tWorkHistory"))
	    {
		myVal += "tProfessionalLiability_main_ProfessionalLiability_PhysicianID.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("tAlliedHealthProfessional"))
	    {
		myVal += "tPhysicianPracticeLU_main2_LU_PracticeID.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("tPhysicianMaster"))
	    {
		myVal += "tCoveringPhysicians_main_CoveringPhysicians_PhysicianID.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("pro-file_Status"))
	    {
		myVal += "tPhysicianMaster_form.jsp?EDIT=edit";
	    }
        else if (dataCategory.equalsIgnoreCase("practice_Status"))
	    {
		myVal += "tPracticeMaster_form.jsp?EDIT=edit";
	    }
        else if (dataCategory.equalsIgnoreCase("tPhysicianPracticeLU"))
	    {
		myVal += "tPhysicianUserAccountLU_main2_UserAccount_PhysicianID.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("tPhysicianPracticeLU_Pract"))
	    {
		myVal += "SupplementalData_main_PracticeID.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("tPhysicianUserAccountLU"))
	    {
		myVal += "pro-file_Attestation.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("pro-file_Attestation"))
	    {
		myVal += "tDocumentManagement_PhysicianID.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("tDocumentManagement"))
	    {
		myVal += "FinishedCheckList_PhysicianID.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("FinishedCheckList_PhysicianID"))
	    {
		myVal += "pro-file_Status.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("tAttestR_form"))
	    {
		myVal += "tMalpractice_main_Malpractice_PhysicianID.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("tPracticeMaster1"))
	    {
		myVal += "tPracticeMaster_form2.jsp?EDIT=edit";
	    }
        else if (dataCategory.equalsIgnoreCase("tPracticeMaster2"))
	    {
		myVal += "tAlliedHealthProfessional_main_AlliedHealthProfessional_PracticeID.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("Supplemental_PhysicianID"))
	    {
		myVal += "Physician_HCOAuth.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("Supplemental_PracticeID"))
	    {
		myVal += "practice_status.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("tGroupSecurity"))
	    {
		myVal += "tSecurityGroupMaster_form.jsp?nullParam=null&EDIT=edit";
	    }
        else if (dataCategory.equalsIgnoreCase("tSuppUECN"))
	    {
		myVal += "SupplementalData_main_PhysicianID.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("tSuppInterplan"))
	    {
		myVal += "SupplementalData_main_PhysicianID.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("tSuppPractOptometry"))
	    {
		myVal += "SupplementalData_main_PracticeID.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("tNIM3_CaseAccount"))
	    {
		myVal += "tNIM3_CaseAccount_main_NIM3_CaseAccount_PayerID.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("tNIM3_Referral"))
	    {
		myVal += "tNIM3_Referral_main_NIM3_Referral_CaseID.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("tNIM3_Encounter"))
	    {
		myVal += "tNIM3_Encounter_main_NIM3_Encounter_ReferralID.jsp";
	    }
        else if (dataCategory.equalsIgnoreCase("tNIM3_Service"))
	    {
		myVal += "tNIM3_Service_main_NIM3_Service_EncounterID.jsp";
	    }
        else
	    {
		myVal += "#";
	    }
        }
	return myVal;
    }


    public static String getHTML(String msgCode, String dataCategory, Integer logicID)
    {
	String myVal = "";
	if (msgCode.equalsIgnoreCase("EXPRESSTopControl")&&dataCategory.substring(0,5).equalsIgnoreCase("tNIM3"))
	{
		if (dataCategory.equalsIgnoreCase("tNIM3_Service"))
		{
			myVal += "<p class=\"title\">Service</p>";
		}
		else if (dataCategory.equalsIgnoreCase("tNIM3_Encounter"))
		{
			myVal += "<p class=\"title\">Encounter</p>";
		}
		else if (dataCategory.equalsIgnoreCase("tNIM3_Referral"))
		{
			myVal += "<p class=\"title\">Referral</p>";
		}
		else if (dataCategory.equalsIgnoreCase("tNIM3_CaseAccount"))
		{
			myVal += "<p class=\"title\">Case</p>";
		}
	}
	else if (msgCode.equalsIgnoreCase("EXPRESSTopControl"))
	{
//		myVal += "<body onUnload=\"return Confirm ('You are now leavin express mode.  Please ensure that you have saved your data before proceededing\n\nIf you are not sure if you have saved your data, click CANCEL and then click the CONTINUE button before proceeding.\n\nTo continue click OK')\"  >";


		myVal += "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"690\">";
		myVal += "  <tr>";
		myVal += "   <td><img src=\"express/nav-images/spacer.gif\" width=\"468\" height=\"1\" border=\"0\"></td>";
		myVal += "   <td><img src=\"express/nav-images/spacer.gif\" width=\"211\" height=\"1\" border=\"0\"></td>";
		myVal += "   <td><img src=\"express/nav-images/spacer.gif\" width=\"11\" height=\"1\" border=\"0\"></td>";
		myVal += "   <td><img src=\"express/nav-images/spacer.gif\" width=\"1\" height=\"1\" border=\"0\"></td>";
		myVal += "  </tr>";
		myVal += "";
		myVal += "  <tr>";
		myVal += "   <td colspan=\"3\"><img name=\"expressnav_r1_c1\" src=\"express/nav-images/express-nav_r1_c1.gif\" width=\"690\" height=\"11\" border=\"0\"></td>";
		myVal += "   <td><img src=\"express/nav-images/spacer.gif\" width=\"1\" height=\"11\" border=\"0\"></td>";
		myVal += "  </tr>";
		myVal += "  <tr>";
		myVal += "   <td rowspan=\"2\"><img name=\"expressnav_r2_c1\" src=\"express/nav-images/express-nav_r2_c1.gif\" width=\"468\" height=\"39\" border=\"0\"></td>";
		myVal += "   <td bgcolor=\"#ffffff\" valign=\"middle\"><a href=# onClick=\"document.forms[0].nextPage.value='"+getDataCategoryLink(dataCategory,"Standard")+"';document.forms[0].submit();\" >switch to Advanced Mode</a></td>";
		myVal += "   <td rowspan=\"2\"><img name=\"expressnav_r2_c3\" src=\"express/nav-images/express-nav_r2_c3.gif\" width=\"11\" height=\"39\" border=\"0\"></td>";
		myVal += "   <td><img src=\"express/nav-images/spacer.gif\" width=\"1\" height=\"29\" border=\"0\"></td>";
		myVal += "  </tr>";
		myVal += "  <tr>";
		myVal += "   <td><img name=\"expressnav_r3_c2\" src=\"express/nav-images/express-nav_r3_c2.gif\" width=\"211\" height=\"10\" border=\"0\"></td>";
		myVal += "   <td><img src=\"express/nav-images/spacer.gif\" width=\"1\" height=\"10\" border=\"0\"></td>";
		myVal += "  </tr>";
		myVal += "</table>";

		myVal += "<table border=1 bordercolor=333333 cellpadding=0 cellspacing=0 width=100%>";
		myVal += " <tr>";
		myVal += "  <td>";
		myVal += "    <table class=wizardTable border=0 cellpadding=1 cellspacing=0 width=100%>";
		myVal += "     <tr>";
		myVal += "      <td class=title>";
		{
			myVal += "        Section " + getDataStepNumber(dataCategory) + ".&nbsp&nbsp" + getDataTitle(dataCategory) + "";
		}
		if (getDataInstructions(dataCategory).equalsIgnoreCase(""))
		{
		}
		else
		{
			myVal += getDataInstructionsExpress(dataCategory);
		}
		myVal += "<br><br>If any Date fields are not applicable, please enter \"na\"";

		myVal += "      </td>";
		myVal += "      <td class=title bgColor=FFFFFF width=10>";
		myVal += "        &nbsp;<a href=# onClick=\"document.forms[0].nextPage.value='"+getExpressLinkRaw(dataCategory,"back",logicID)+"';document.forms[0].submit();\" ><img src=images/back.gif border=0 ></a>";
		myVal += "      </td>";
		myVal += "      <td class=title bgColor=FFFFFF width=10>";
		myVal += "        &nbsp;<a href=# onClick=\"document.forms[0].nextPage.value='"+getExpressLinkRaw(dataCategory,"next",logicID)+"';document.forms[0].submit();\" ><img src=images/continue.gif border=0 ></a>";
		myVal += "      </td>";
//		myVal += "      <td class=title bgColor=FFFFFF width=10>";
//		myVal += "        &nbsp;"+getExpressLink(dataCategory,"next",logicID);
//		myVal += "      </td>";
		myVal += "     </tr>";
		myVal += "    </table>";
		myVal += "  </td>";
		myVal += " </tr>";
		myVal += "</table>";
	}
	return myVal;
    }




    public static String getHTML(String msgCode, String dataCategory)
    {
	String myVal = "";
	if (msgCode.equalsIgnoreCase("INTERVIEWTopControl")&&dataCategory.substring(0,5).equalsIgnoreCase("tNIM3"))
	{
		if (dataCategory.equalsIgnoreCase("tNIM3_Service"))
		{
			myVal += "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"90%\">";
			myVal += "  <tr >";
			myVal += "   <td colspan=4 class=title>Service</td>";
			myVal += "  </tr>";
			myVal += "  <tr>";
			myVal += "   <td><a href=\"tNIM3_CaseAccount_main_NIM3_CaseAccount_PayerID.jsp\">Cases</a>/<a href=\"tNIM3_Referral_main_NIM3_Referral_CaseID.jsp\">Referrals</a>/<a href=\"tNIM3_Encounter_main_NIM3_Encounter_ReferralID.jsp\">Encounters</a>/Services</td>";
			myVal += "   <td><a href=\"tNIM3_Service_main_NIM3_Service_EncounterID_Express.jsp\">Express Edit</a></td>";
			myVal += "   <td></td>";
			myVal += "   <td></td>";
			myVal += "  </tr>";
			myVal += "</table>";
		}
		else if (dataCategory.equalsIgnoreCase("tNIM3_Encounter"))
		{
			myVal += "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"90%\">";
			myVal += "  <tr >";
			myVal += "   <td colspan=4 class=title>Encounters</td>";
			myVal += "  </tr>";
			myVal += "  <tr>";
			myVal += "   <td><a href=\"tNIM3_CaseAccount_main_NIM3_CaseAccount_PayerID.jsp\">Cases</a>/<a href=\"tNIM3_Referral_main_NIM3_Referral_CaseID.jsp\">Referrals</a>/Encounters</td>";
			myVal += "   <td><a href=\"tNIM3_Encounter_main_NIM3_Encounter_ReferralID_Express.jsp\">Express Edit</a></td>";
			myVal += "   <td></td>";
			myVal += "   <td></td>";
			myVal += "  </tr>";
			myVal += "</table>";
		}
		else if (dataCategory.equalsIgnoreCase("tNIM3_Referral"))
		{
			myVal += "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"90%\">";
			myVal += "  <tr >";
			myVal += "   <td colspan=4 class=title>Referral</td>";
			myVal += "  </tr>";
			myVal += "  <tr>";
			myVal += "   <td><a href=\"tNIM3_CaseAccount_main_NIM3_CaseAccount_PayerID.jsp\">Cases</a>/Referrals</td>";
			myVal += "   <td></td>";
			myVal += "   <td></td>";
			myVal += "   <td></td>";
			myVal += "  </tr>";
			myVal += "</table>";
		}
		else if (dataCategory.equalsIgnoreCase("tNIM3_CaseAccount"))
		{
			myVal += "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"90%\">";
			myVal += "  <tr >";
			myVal += "   <td colspan=4 class=title>Cases</td>";
			myVal += "  </tr>";
			myVal += "  <tr>";
			myVal += "   <td></td>";
			myVal += "   <td></td>";
			myVal += "   <td></td>";
			myVal += "   <td></td>";
			myVal += "  </tr>";
			myVal += "</table>";
		}
	}
	else if (msgCode.equalsIgnoreCase("INTERVIEWTopControl"))
	{
		if (new Integer(getDataStepNumber(dataCategory)).intValue()<18)
		{		
			myVal += "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"690\">";
			myVal += "  <tr>";
			myVal += "   <td><img src=\"express/nav-images/spacer.gif\" width=\"468\" height=\"1\" border=\"0\"></td>";
			myVal += "   <td><img src=\"express/nav-images/spacer.gif\" width=\"211\" height=\"1\" border=\"0\"></td>";
			myVal += "   <td><img src=\"express/nav-images/spacer.gif\" width=\"11\" height=\"1\" border=\"0\"></td>";
			myVal += "   <td><img src=\"express/nav-images/spacer.gif\" width=\"1\" height=\"1\" border=\"0\"></td>";
			myVal += "  </tr>";
			myVal += "";
			myVal += "  <tr>";
			myVal += "   <td colspan=\"3\"><img name=\"expressnav_r1_c1\" src=\"express/nav-images/advanced-nav_r1_c1.gif\" width=\"690\" height=\"11\" border=\"0\"></td>";
			myVal += "   <td><img src=\"express/nav-images/spacer.gif\" width=\"1\" height=\"11\" border=\"0\"></td>";
			myVal += "  </tr>";
			myVal += "  <tr>";
			myVal += "   <td rowspan=\"2\"><img name=\"expressnav_r2_c1\" src=\"express/nav-images/advanced-nav_r2_c1.gif\" width=\"468\" height=\"39\" border=\"0\"></td>";
			myVal += "   <td bgcolor=\"#ffffff\" valign=\"middle\"><a href=\""+getDataCategoryLink(dataCategory,"Express")+"\">Switch to Express Mode</a><br><a href=\"ExtraInformation_PhysicianID.jsp\" ><font size=1>return to Review of Data Collection</font></a></td>";
			myVal += "   <td rowspan=\"2\"><img name=\"expressnav_r2_c3\" src=\"express/nav-images/advanced-nav_r2_c3.gif\" width=\"11\" height=\"39\" border=\"0\"></td>";
			myVal += "   <td><img src=\"express/nav-images/spacer.gif\" width=\"1\" height=\"29\" border=\"0\"></td>";
			myVal += "  </tr>";
			myVal += "  <tr>";
			myVal += "   <td><img name=\"expressnav_r3_c2\" src=\"express/nav-images/advanced-nav_r3_c2.gif\" width=\"211\" height=\"10\" border=\"0\"></td>";
			myVal += "   <td><img src=\"express/nav-images/spacer.gif\" width=\"1\" height=\"10\" border=\"0\"></td>";
			myVal += "  </tr>";
			myVal += "</table>";
		}
		else if (new Integer(getDataStepNumber(dataCategory)).intValue()<21)
		{		
			myVal += "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"690\">";
			myVal += "  <tr>";
			myVal += "   <td><img src=\"express/nav-images/spacer.gif\" width=\"468\" height=\"1\" border=\"0\"></td>";
			myVal += "   <td><img src=\"express/nav-images/spacer.gif\" width=\"211\" height=\"1\" border=\"0\"></td>";
			myVal += "   <td><img src=\"express/nav-images/spacer.gif\" width=\"11\" height=\"1\" border=\"0\"></td>";
			myVal += "   <td><img src=\"express/nav-images/spacer.gif\" width=\"1\" height=\"1\" border=\"0\"></td>";
			myVal += "  </tr>";
			myVal += "";
			myVal += "  <tr>";
			myVal += "   <td colspan=\"3\"><img name=\"expressnav_r1_c1\" src=\"express/nav-images/extra-nav_r1_c1.gif\" width=\"690\" height=\"11\" border=\"0\"></td>";
			myVal += "   <td><img src=\"express/nav-images/spacer.gif\" width=\"1\" height=\"11\" border=\"0\"></td>";
			myVal += "  </tr>";
			myVal += "  <tr>";
			myVal += "   <td rowspan=\"2\"><img name=\"expressnav_r2_c1\" src=\"express/nav-images/extra-nav_r2_c1.gif\" width=\"468\" height=\"39\" border=\"0\"></td>";
			myVal += "   <td bgcolor=\"#ffffff\" valign=\"middle\">&nbsp;</td>";
			myVal += "   <td rowspan=\"2\"><img name=\"expressnav_r2_c3\" src=\"express/nav-images/extra-nav_r2_c3.gif\" width=\"11\" height=\"39\" border=\"0\"></td>";
			myVal += "   <td><img src=\"express/nav-images/spacer.gif\" width=\"1\" height=\"29\" border=\"0\"></td>";
			myVal += "  </tr>";
			myVal += "  <tr>";
			myVal += "   <td><img name=\"expressnav_r3_c2\" src=\"express/nav-images/extra-nav_r3_c2.gif\" width=\"211\" height=\"10\" border=\"0\"></td>";
			myVal += "   <td><img src=\"express/nav-images/spacer.gif\" width=\"1\" height=\"10\" border=\"0\"></td>";
			myVal += "  </tr>";
			myVal += "</table>";
		}
		else if (new Integer(getDataStepNumber(dataCategory)).intValue()<23)
		{		
			myVal += "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"690\">";
			myVal += "  <tr>";
			myVal += "   <td><img src=\"express/nav-images/spacer.gif\" width=\"468\" height=\"1\" border=\"0\"></td>";
			myVal += "   <td><img src=\"express/nav-images/spacer.gif\" width=\"211\" height=\"1\" border=\"0\"></td>";
			myVal += "   <td><img src=\"express/nav-images/spacer.gif\" width=\"11\" height=\"1\" border=\"0\"></td>";
			myVal += "   <td><img src=\"express/nav-images/spacer.gif\" width=\"1\" height=\"1\" border=\"0\"></td>";
			myVal += "  </tr>";
			myVal += "";
			myVal += "  <tr>";
			myVal += "   <td colspan=\"3\"><img name=\"expressnav_r1_c1\" src=\"express/nav-images/sign-nav_r1_c1.gif\" width=\"690\" height=\"11\" border=\"0\"></td>";
			myVal += "   <td><img src=\"express/nav-images/spacer.gif\" width=\"1\" height=\"11\" border=\"0\"></td>";
			myVal += "  </tr>";
			myVal += "  <tr>";
			myVal += "   <td rowspan=\"2\"><img name=\"expressnav_r2_c1\" src=\"express/nav-images/sign-nav_r2_c1.gif\" width=\"468\" height=\"39\" border=\"0\"></td>";
			myVal += "   <td bgcolor=\"#ffffff\" valign=\"middle\">&nbsp;</td>";
			myVal += "   <td rowspan=\"2\"><img name=\"expressnav_r2_c3\" src=\"express/nav-images/sign-nav_r2_c3.gif\" width=\"11\" height=\"39\" border=\"0\"></td>";
			myVal += "   <td><img src=\"express/nav-images/spacer.gif\" width=\"1\" height=\"29\" border=\"0\"></td>";
			myVal += "  </tr>";
			myVal += "  <tr>";
			myVal += "   <td><img name=\"expressnav_r3_c2\" src=\"express/nav-images/sign-nav_r3_c2.gif\" width=\"211\" height=\"10\" border=\"0\"></td>";
			myVal += "   <td><img src=\"express/nav-images/spacer.gif\" width=\"1\" height=\"10\" border=\"0\"></td>";
			myVal += "  </tr>";
			myVal += "</table>";
		}
		else if (new Integer(getDataStepNumber(dataCategory)).intValue()<24)
		{		
			myVal += "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"690\">";
			myVal += "  <tr>";
			myVal += "   <td><img src=\"express/nav-images/spacer.gif\" width=\"468\" height=\"1\" border=\"0\"></td>";
			myVal += "   <td><img src=\"express/nav-images/spacer.gif\" width=\"211\" height=\"1\" border=\"0\"></td>";
			myVal += "   <td><img src=\"express/nav-images/spacer.gif\" width=\"11\" height=\"1\" border=\"0\"></td>";
			myVal += "   <td><img src=\"express/nav-images/spacer.gif\" width=\"1\" height=\"1\" border=\"0\"></td>";
			myVal += "  </tr>";
			myVal += "";
			myVal += "  <tr>";
			myVal += "   <td colspan=\"3\"><img name=\"expressnav_r1_c1\" src=\"express/nav-images/document-nav_r1_c1.gif\" width=\"690\" height=\"11\" border=\"0\"></td>";
			myVal += "   <td><img src=\"express/nav-images/spacer.gif\" width=\"1\" height=\"11\" border=\"0\"></td>";
			myVal += "  </tr>";
			myVal += "  <tr>";
			myVal += "   <td rowspan=\"2\"><img name=\"expressnav_r2_c1\" src=\"express/nav-images/document-nav_r2_c1.gif\" width=\"468\" height=\"39\" border=\"0\"></td>";
			myVal += "   <td bgcolor=\"#ffffff\" valign=\"middle\">&nbsp;</td>";
			myVal += "   <td rowspan=\"2\"><img name=\"expressnav_r2_c3\" src=\"express/nav-images/document-nav_r2_c3.gif\" width=\"11\" height=\"39\" border=\"0\"></td>";
			myVal += "   <td><img src=\"express/nav-images/spacer.gif\" width=\"1\" height=\"29\" border=\"0\"></td>";
			myVal += "  </tr>";
			myVal += "  <tr>";
			myVal += "   <td><img name=\"expressnav_r3_c2\" src=\"express/nav-images/document-nav_r3_c2.gif\" width=\"211\" height=\"10\" border=\"0\"></td>";
			myVal += "   <td><img src=\"express/nav-images/spacer.gif\" width=\"1\" height=\"10\" border=\"0\"></td>";
			myVal += "  </tr>";
			myVal += "</table>";
		}
		myVal += "<table border=1 bordercolor=333333 cellpadding=0 cellspacing=0 width=100%>";
		myVal += " <tr>";
		myVal += "  <td>";
		myVal += "    <table class=wizardTable border=0 cellpadding=1 cellspacing=0 width=100%>";
		myVal += "     <tr>";
		myVal += "      <td class=title>";
		if (getDataStepNumber(dataCategory).equalsIgnoreCase("-1"))
		{
			myVal += "        " + getDataTitle(dataCategory) + "";
		}
		else
		{
			myVal += "        Section " + getDataStepNumber(dataCategory) + " of 24 - " + getDataTitle(dataCategory) + "";
		}
		if (getDataInstructions(dataCategory).equalsIgnoreCase(""))
		{
		}
		else
		{
			myVal += getDataInstructions(dataCategory);
		}
		myVal += "      </td>";
		myVal += "      <td class=title bgColor=FFFFFF width=10>";
		myVal += "        &nbsp;"+getInterviewLink(dataCategory,"back");
		myVal += "      </td>";
		myVal += "      <td class=title bgColor=FFFFFF width=10>";
		myVal += "        &nbsp;"+getInterviewLink(dataCategory,"next");
		myVal += "      </td>";
		myVal += "     </tr>";
		myVal += "    </table>";
		myVal += "  </td>";
		myVal += " </tr>";
		myVal += "</table>";
	}
	else if (msgCode.equalsIgnoreCase("INTERVIEWTopControl_form"))
	{
		myVal += "<table border=1 bordercolor=333333 cellpadding=0 cellspacing=0 width=100%>";
		myVal += " <tr>";
		myVal += "  <td>";
		myVal += "    <table class=wizardTable border=0 cellpadding=1 cellspacing=0 width=100%>";
		myVal += "     <tr>";
		myVal += "      <td class=title >";
		if (getDataStepNumber(dataCategory).equalsIgnoreCase("-1"))
		{
			myVal += "        " + getDataTitle(dataCategory) + "";
		}
		else
		{
			myVal += "        Section " + getDataStepNumber(dataCategory) + " of 24 - " + getDataTitle(dataCategory) + "";
		}
		if (getDataInstructions(dataCategory).equalsIgnoreCase(""))
		{
		}
		else
		{
			myVal += getDataInstructions(dataCategory);
		}
		myVal += "<br><br>If any Date fields not applicable, please enter \"na\".";
		myVal += "      </td>";
		myVal += "      <td class=title>&nbsp;</td>";
		myVal += "     </tr>";
		myVal += "    </table>";
		myVal += "  </td>";
		myVal += " </tr>";
		myVal += "</table>";
	}
	return myVal;
    }




    public ConfigurationMessages () 
    {
    }   // End of default constructor

    /**
     *
     * @author  Scott Ellis via listClassGenCode
     * @version 2.5
     */
    public static class bltAdminPhysicianLU_List_LU_AdminID extends db_BaseRelationTable
    {
        /** Default constructor
        */
        public bltAdminPhysicianLU_List_LU_AdminID ( Integer iAdminID )
        {
            super( "tAdminPhysicianLU", "LookupID", "LookupID", "AdminID", iAdminID );
        }   // End of bltAdminPhysicianLU_List_LU_AdminID()


        public void setField(String fieldN, Object fieldV)
        {DebugLogger.println("setField Method not implemented yet in classbltAdminPhysicianLU_List_LU_AdminID!");}

        public String getFieldType(String fieldN)
        {return null;}

        public bltAdminPhysicianLU_List_LU_AdminID ( Integer iAdminID, String extraWhere, String OrderBy )
        {
            super( "tAdminPhysicianLU", "LookupID", "LookupID", "AdminID", iAdminID, extraWhere,OrderBy );
        }   // End of bltAdminPhysicianLU_List_LU_AdminID()
         /** Method pull information from the result set and load into class attributes.
        * @param sFieldList Comma delimited list of fields for which data has been retrieved.
        * @param rs Result set from which to pull the data from.
        * @throws java.sql.SQLException Passes on SQLException
        */
        protected void transferData( ResultSet rs ) throws SQLException
        {
            while ( rs.next()  )
            {
                Integer         iValue              = new Integer( rs.getString( sLinkFieldName ) );
                Integer         iLinkRecordID       = new Integer( rs.getString( sUniqueIDFieldName ) );
                Object          oReferenceObject;
                ListElement     leNewElement;

                oReferenceObject    = new bltAdminPhysicianLU( iValue );

                leNewElement        = new ListElement( iLinkRecordID, oReferenceObject );

                vList.add( leNewElement );

            }
        }   // End of transferData()
    }   // End of bltAdminPhysicianLU_List_LU_AdminID class
}   // End of ConfigurationInformation class
