package com.winstaff;


import com.winstaff.dbtCoverageTypeLI;
import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public interface InttCoverageTypeLI extends dbTableInterface
{

    public void setCoverageID(Integer newValue);
    public Integer getCoverageID();
    public void setCoverageLong(String newValue);
    public String getCoverageLong();
}    // End of bltCoverageTypeLI class definition
