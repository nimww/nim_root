

package com.winstaff;

import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public interface InttVoidLeakReasonLI extends dbTableInterface
{

    public void setVoidLeakReasonID(Integer newValue);
    public Integer getVoidLeakReasonID();
    public void setVoidLeakReasonShort(String newValue);
    public String getVoidLeakReasonShort();
    public void setVoidLeakReasonLong(String newValue);
    public String getVoidLeakReasonLong();
}    // End of bltVoidLeakReasonLI class definition
