
package com.winstaff;
/*
 * bltHCOPhysicianTransaction_List.java
 *
 * Created: Wed Apr 27 19:02:29 PDT 2011
 */



import java.sql.*;




/**
 *
 * @author  Scott Ellis via listClassGenCode
 * @version 2.5
 */
public class bltHCOPhysicianTransaction_List extends db_BaseRelationTable
{
    /** Default constructor
    */    
    public bltHCOPhysicianTransaction_List ( Integer iLookupID )
    {
        super( "tHCOPhysicianTransaction", "HCOPhysicianTransactionID", "HCOPhysicianTransactionID", "LookupID", iLookupID );
    }   // End of bltHCOPhysicianTransaction_List()
    

    public void setField(String fieldN, Object fieldV)
    {DebugLogger.println("setField Method not implemented yet in classbltHCOPhysicianTransaction_List!");}

    public String getFieldType(String fieldN)
    {return null;}

    public bltHCOPhysicianTransaction_List ( Integer iLookupID, String extraWhere, String OrderBy )
    {
        super( "tHCOPhysicianTransaction", "HCOPhysicianTransactionID", "HCOPhysicianTransactionID", "LookupID", iLookupID, extraWhere,OrderBy );
    }   // End of bltHCOPhysicianTransaction_List()
     /** Method pull information from the result set and load into class attributes.
    * @param sFieldList Comma delimited list of fields for which data has been retrieved.
    * @param rs Result set from which to pull the data from.
    * @throws SQLException Passes on SQLException
    */      
    protected void transferData( ResultSet rs ) throws SQLException
    {
        while ( rs.next()  )
        {
            Integer         iValue              = new Integer( rs.getString( sLinkFieldName ) );
            Integer         iLinkRecordID       = new Integer( rs.getString( sUniqueIDFieldName ) );
            Object          oReferenceObject;
            ListElement     leNewElement;
            
            oReferenceObject    = new bltHCOPhysicianTransaction( iValue );
            
            leNewElement        = new ListElement( iLinkRecordID, oReferenceObject );
            
            vList.add( leNewElement );

        }
    }   // End of transferData()
}   // End of bltHCOPhysicianTransaction_List class

