package com.winstaff;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.InvocationTargetException;
import java.security.GeneralSecurityException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.TimeZone;
import java.sql.ResultSet;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import com.winstaff.*;

public class DashBoardUtils {
	

	public static ArrayList<ServiceInfo> getServicesInfo(String services) {
		ArrayList<ServiceInfo> si = new ArrayList<ServiceInfo>();

		String[] line = services.split("\n");

		for (String item : line) {
			String[] service = item.split("\\|");
			int s = 0;
			si.add(new ServiceInfo(service[s++], service[s++], service[s++], service[s++], service[s++], service[s++], service[s++], service[s++], service[s++], service[s++], service[s++], service[s++], service[s++], service[s++], service[s++], service[s++], service[s++], service[s++], service[s++], service[s++], service[s++], service[s++], service[s++]));
		}
		return si;
	}

	@SuppressWarnings("unchecked")
	public static void saveService(int serviceId, int icdNum, String value) throws ClassNotFoundException, IllegalArgumentException, SecurityException, IllegalAccessException, InvocationTargetException, NoSuchMethodException {
		Class[] string = new Class[1];	
		string[0] = String.class;
		
		Class cls = Class.forName("com.winstaff.bltNIM3_Service");
		Object obj = new bltNIM3_Service(serviceId);
		
		cls.getDeclaredMethod("setdCPT"+icdNum,string).invoke(obj, value);
		cls.getDeclaredMethod("commitData").invoke(obj, null);
	}
	
	public static void email(String emailTo, String emailFrom, String theSubject, String theBody){
		bltEmailTransaction email = new bltEmailTransaction();
		email.setEmailTo(emailTo);
		email.setEmailFrom(emailFrom);
		email.setEmailSubject(theSubject);
		email.setEmailBody(theBody);
		email.setEmailBodyType("text/html; charset=us-ascii");
		email.setTransactionDate(new java.util.Date());
		
		try {
			email.commitData();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
	}
}
