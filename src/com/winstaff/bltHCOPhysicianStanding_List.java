
package com.winstaff;
/*
 * bltHCOPhysicianStanding_List.java
 *
 * Created: Sat Jun 18 18:27:46 PDT 2011
 */



import java.sql.*;




/**
 *
 * @author  Scott Ellis via listClassGenCode
 * @version 2.5
 */
public class bltHCOPhysicianStanding_List extends db_BaseRelationTable
{
    /** Default constructor
    */    
    public bltHCOPhysicianStanding_List ( Integer iHCOID )
    {
        super( "tHCOPhysicianStanding", "LookupID", "LookupID", "HCOID", iHCOID );
    }   // End of bltHCOPhysicianStanding_List()
    

    public void setField(String fieldN, Object fieldV)
    {DebugLogger.println("setField Method not implemented yet in classbltHCOPhysicianStanding_List!");}

    public String getFieldType(String fieldN)
    {return null;}

    public bltHCOPhysicianStanding_List ( Integer iHCOID, String extraWhere, String OrderBy )
    {
        super( "tHCOPhysicianStanding", "LookupID", "LookupID", "HCOID", iHCOID, extraWhere,OrderBy );
    }   // End of bltHCOPhysicianStanding_List()
     /** Method pull information from the result set and load into class attributes.
    * @param sFieldList Comma delimited list of fields for which data has been retrieved.
    * @param rs Result set from which to pull the data from.
    * @throws SQLException Passes on SQLException
    */      
    protected void transferData( ResultSet rs ) throws SQLException
    {
        while ( rs.next()  )
        {
            Integer         iValue              = new Integer( rs.getString( sLinkFieldName ) );
            Integer         iLinkRecordID       = new Integer( rs.getString( sUniqueIDFieldName ) );
            Object          oReferenceObject;
            ListElement     leNewElement;
            
            oReferenceObject    = new bltHCOPhysicianStanding( iValue );
            
            leNewElement        = new ListElement( iLinkRecordID, oReferenceObject );
            
            vList.add( leNewElement );

        }
    }   // End of transferData()
}   // End of bltHCOPhysicianStanding_List class

