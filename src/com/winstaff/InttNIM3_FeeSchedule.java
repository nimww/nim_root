

package com.winstaff;

import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public interface InttNIM3_FeeSchedule extends dbTableInterface
{

    public void setFeeScheduleID(Integer newValue);
    public Integer getFeeScheduleID();
    public void setUniqueCreateDate(Date newValue);
    public Date getUniqueCreateDate();
    public void setUniqueModifyDate(Date newValue);
    public Date getUniqueModifyDate();
    public void setUniqueModifyComments(String newValue);
    public String getUniqueModifyComments();
    public void setFeeScheduleRefID(Integer newValue);
    public Integer getFeeScheduleRefID();
    public void setCPT(String newValue);
    public String getCPT();
    public void setCPTModifier(String newValue);
    public String getCPTModifier();
    public void setBillAmount(Double newValue);
    public Double getBillAmount();
    public void setAllowAmount(Double newValue);
    public Double getAllowAmount();
    public void setStateID(Integer newValue);
    public Integer getStateID();
    public void setFeeScheduleRegionID(Integer newValue);
    public Integer getFeeScheduleRegionID();
}    // End of bltNIM3_FeeSchedule class definition
