package com.winstaff;

import java.util.Vector;

/**
 * Created by IntelliJ IDEA.
 * User: ste
 * Date: Jun 4, 2010
 * Time: 6:28:03 PM
 * To change this template use File | Settings | File Templates.
 */
public class CPTGroupObject2
{
    public CPTGroupObject2(Vector CPTList)
    {
        this.CPTList = CPTList;
    }

    public CPTGroupObject2()
    {
        CPTList = new Vector<CPTObject>();
    }

    public CPTGroupObject2(String sCPT1, String mCPT1, String sCPT2, String mCPT2, String sCPT3, String mCPT3,  String sCPT4, String mCPT4,  String sCPT5, String mCPT5)
    {
        CPTList = new Vector<CPTObject>();
        CPTList.add(new CPTObject(sCPT1, mCPT1));
        CPTList.add(new CPTObject(sCPT2, mCPT2));
        CPTList.add(new CPTObject(sCPT3, mCPT3));
        CPTList.add(new CPTObject(sCPT4, mCPT4));
        CPTList.add(new CPTObject(sCPT5, mCPT5));
    }

    public CPTGroupObject2(String sCPT1, String mCPT1, String sCPT2, String mCPT2, String sCPT3, String mCPT3, String sCPT4, String mCPT4)
    {
        CPTList = new Vector<CPTObject>();
        CPTList.add(new CPTObject(sCPT1, mCPT1));
        CPTList.add(new CPTObject(sCPT2, mCPT2));
        CPTList.add(new CPTObject(sCPT3, mCPT3));
        CPTList.add(new CPTObject(sCPT4, mCPT4));
    }

    public CPTGroupObject2(String sCPT1, String mCPT1, String sCPT2, String mCPT2, String sCPT3, String mCPT3)
    {
        CPTList = new Vector<CPTObject>();
        CPTList.add(new CPTObject(sCPT1, mCPT1));
        CPTList.add(new CPTObject(sCPT2, mCPT2));
        CPTList.add(new CPTObject(sCPT3, mCPT3));
    }

    public CPTGroupObject2(String sCPT1, String mCPT1, String sCPT2, String mCPT2)
    {
        CPTList = new Vector<CPTObject>();
        CPTList.add(new CPTObject(sCPT1, mCPT1));
        CPTList.add(new CPTObject(sCPT2, mCPT2));
    }

    public CPTGroupObject2(String sCPT1, String mCPT1)
    {
        CPTList = new Vector<CPTObject>();
        CPTList.add(new CPTObject(sCPT1, mCPT1));
    }

    public Vector<CPTObject> getCPTList()
    {
        return CPTList;
    }

    public void setCPTList(Vector<CPTObject> CPTList)
    {
        this.CPTList = CPTList;
    }

    public void addToCPTList(CPTObject sCPT)
    {
        if (sCPT != null && sCPT.getCPT_Code()!=null && !sCPT.getCPT_Code().equalsIgnoreCase("") && sCPT.getCPT_Code().length()>=4){
            this.getCPTList().add(sCPT);

        }
    }

    public boolean isEmpty()
    {
        return (this.getCPTList().isEmpty());
    }

    public String getSQLDB_ArrayList()
    {
        String myVal = "";
        try
        {
            for (int i=0;i<this.getCPTList().size();i++)
            {
                if (i!=0)
                {
                    myVal += ", ";
                }
                CPTObject myCPTO = this.getCPTList().elementAt(i);
                myVal += "'" + myCPTO.getCPT_Code() + "'";
            }
        }
        catch (Exception e)
        {
            myVal = null;
        }
        return myVal;
    }


    public CPTObject getFirstCPT()
    {
        CPTObject myCPTO = null;
        try
        {
             myCPTO = this.getCPTList().elementAt(0);
        }
        catch (Exception e)
        {
            myCPTO = null;
        }
        return myCPTO;
    }


    private boolean hasMRI = false;
    private boolean hasCT = false;
    private boolean hasEMG = false;
    private boolean hasFL = false;
    private boolean hasUS = false;
    private boolean hasNM = false;
    private boolean hasPET = false;
    private boolean hasXR = false;


    public boolean isHasMRI()
    {
        if (!DiagnosticAnalysisHasRun)
        {
            this.RunDiagnosticAnalysis();
        }
        return hasMRI;
    }

    public void setHasMRI(boolean hasMRI)
    {
        this.hasMRI = hasMRI;
    }

    public boolean isHasCT()
    {
        if (!DiagnosticAnalysisHasRun)
        {
            this.RunDiagnosticAnalysis();
        }
        return hasCT;
    }

    public void setHasCT(boolean hasCT)
    {
        this.hasCT = hasCT;
    }

    public boolean isHasEMG()
    {
        if (!DiagnosticAnalysisHasRun)
        {
            this.RunDiagnosticAnalysis();
        }
        return hasEMG;
    }

    public void setHasEMG(boolean hasEMG)
    {
        this.hasEMG = hasEMG;
    }

    public boolean isHasFL()
    {
        if (!DiagnosticAnalysisHasRun)
        {
            this.RunDiagnosticAnalysis();
        }
        return hasFL;
    }

    public void setHasFL(boolean hasFL)
    {
        this.hasFL = hasFL;
    }

    public boolean isHasUS()
    {
        if (!DiagnosticAnalysisHasRun)
        {
            this.RunDiagnosticAnalysis();
        }
        return hasUS;
    }

    public void setHasUS(boolean hasUS)
    {
        this.hasUS = hasUS;
    }

    public boolean isHasNM()
    {
        if (!DiagnosticAnalysisHasRun)
        {
            this.RunDiagnosticAnalysis();
        }
        return hasNM;
    }

    public void setHasNM(boolean hasNM)
    {
        this.hasNM = hasNM;
    }

    public boolean isHasPET()
    {
        if (!DiagnosticAnalysisHasRun)
        {
            this.RunDiagnosticAnalysis();
        }
        return hasPET;
    }

    public void setHasPET(boolean hasPET)
    {
        this.hasPET = hasPET;
    }

    public boolean isHasXR()
    {
        if (!DiagnosticAnalysisHasRun)
        {
            this.RunDiagnosticAnalysis();
        }
        return hasXR;
    }

    private void RunDiagnosticAnalysis()
    {
        try
        {
            for (int i=0;i<this.getCPTList().size();i++)
            {
                CPTObject myCPTO = this.getCPTList().elementAt(i);
                String sCPT = myCPTO.getCPT_Code();
				
				String myVal = "";
				searchDB2 mySS = new searchDB2();
				java.sql.ResultSet myRS = null;
				String mySQL = "select \"CPTGroup\" from \"CPTGroup\" where \"CPT\" = '"+sCPT+"'";
				try
				{
					db_PreparedStatementObject myDPSO = new db_PreparedStatementObject(mySQL);
					myRS = mySS.executePreparedStatement(myDPSO);
					if (myRS!=null&&myRS.next())
					{
						myVal=myRS.getString("CPTGroup");
					}
					mySS.closeAll();
				}
				catch(Exception e)
				{
					DebugLogger.println("NIMUtils:getRadiologyGroup:[MYSQL="+mySQL+"]:"+e);
				} finally {
					mySS.closeAll();
				}

                if ( !this.hasCT&&(myVal.equals("ct_w")||myVal.equals("ct_wo")||myVal.equals("ct_wwo")))
                {
                    this.setHasCT(true);
                }
                else if (!this.hasMRI&&(myVal.equals("mr_w")||myVal.equals("mr_wo")||myVal.equals("mr_wwo")))
                {
                    this.setHasMRI(true);
                }
                else if (!this.hasEMG&&(myVal.equals("emg")))
                {
                    this.setHasEMG(true);
                }
                else if (!this.hasFL&&(sCPT.equals("74270")||sCPT.equals("74280")||sCPT.equals("74220")||sCPT.equals("74400")||sCPT.equals("74250")||sCPT.equals("74245")||sCPT.equals("74249")||sCPT.equals("74241")||sCPT.equals("74247")||sCPT.equals("74455")))//12-3-12 CPTGroup table doesn't include FL cpts so left static for now
                {
                    this.setHasFL(true);
                }
                else if (!this.hasNM&&(myVal.equals("NM")))
                {
                    this.setHasNM(true);
                }
                else if (!this.hasUS&&(myVal.equals("US")))
                {
                    this.setHasUS(true);
                }
                else if (!this.hasXR&&(myVal.equals("XR")))
                {
                    this.setHasXR(true);
                }
                else if (!this.hasPET&&(myVal.equals("PET")))
                {
                    this.setHasPET(true);
                }
            }
        }
        catch (Exception e)
        {
        }
        this.DiagnosticAnalysisHasRun = true;
    }

    public void setHasXR(boolean hasXR)
    {
        this.hasXR = hasXR;
    }

    private boolean DiagnosticAnalysisHasRun = false;
    private Vector<CPTObject> CPTList = null;
}