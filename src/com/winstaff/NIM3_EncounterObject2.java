package com.winstaff;

import java.io.FileOutputStream;
import java.security.PrivateKey;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Enumeration;

import sun.tools.tree.ThisExpression;

import com.sun.org.apache.bcel.internal.generic.RETURN;
import com.sun.xml.internal.messaging.saaj.soap.name.NameImpl;
import com.winstaff.NIMUtils;

/**
 * Created by IntelliJ IDEA.
 * User: ste
 * Date: May 11, 2011
 * Time: 3:04:41 PM
 * To change this template use File | Settings | File Templates.
 */
public class NIM3_EncounterObject2
{

        public static String TAG = "NIM3_EncounterObject2:";

        public NIM3_EncounterObject2(Integer iEncounterID, String UMC)
        {
            this.setUniqueModifyComments(UMC);
            this.setNIM3_Encounter(new bltNIM3_Encounter(iEncounterID));
        }


    /*
        public NIM3_EncounterObject(Integer iEncounterID, String UMC)
        {
            NIM3_Encounter = new bltNIM3_Encounter(iEncounterID);
            NIM3_Encounter.setUniqueModifyComments(UMC);
            if (NIM3_Encounter.getAppointmentID()>0)
            {
                NIM3_Appointment = new bltNIM3_Appointment(NIM3_Encounter.getAppointmentID());
                NIM3_Appointment.setUniqueModifyComments(UMC);
                Appointment_PracticeMaster = new bltPracticeMaster(NIM3_Appointment.getProviderID());
            }
            NIM3_Referral = new bltNIM3_Referral (NIM3_Encounter.getReferralID());
            NIM3_Referral.setUniqueModifyComments(UMC);
            NIM3_CaseAccount = new bltNIM3_CaseAccount (NIM3_Referral.getCaseID());
            NIM3_CaseAccount.setUniqueModifyComments(UMC);
            NIM3_Service_List = new bltNIM3_Service_List(iEncounterID,"","ServiceID");
            this.Case_Adjuster = new bltUserAccount(NIM3_CaseAccount.getAdjusterID());
            this.Case_Adjuster.setUniqueModifyComments(UMC);
            this.Case_Admin= new bltUserAccount(NIM3_CaseAccount.getCaseAdministratorID());
            this.Case_Admin.setUniqueModifyComments(UMC);

            this.NIM3_PayerMaster = new bltNIM3_PayerMaster(NIM3_CaseAccount.getPayerID());
            this.NIM3_PayerMaster.setUniqueModifyComments(UMC);

            this.NIM3_ParentPayerMaster = new bltNIM3_PayerMaster(NIM3_PayerMaster.getParentPayerID());
            this.NIM3_ParentPayerMaster.setUniqueModifyComments(UMC);

            this.Case_BillTo= new bltUserAccount(NIM3_CaseAccount.getCaseAdministratorID());
            this.Case_BillTo.setUniqueModifyComments(UMC);

            this.Case_Admin2= new bltUserAccount(NIM3_CaseAccount.getCaseAdministrator2ID());
            this.Case_Admin2.setUniqueModifyComments(UMC);
            this.Case_Admin3= new bltUserAccount(NIM3_CaseAccount.getCaseAdministrator3ID());
            this.Case_Admin3.setUniqueModifyComments(UMC);
            this.Case_Admin4= new bltUserAccount(NIM3_CaseAccount.getCaseAdministrator4ID());
            this.Case_Admin4.setUniqueModifyComments(UMC);
            this.Case_NurseCaseManager = new bltUserAccount(NIM3_CaseAccount.getNurseCaseManagerID());
            this.Case_NurseCaseManager.setUniqueModifyComments(UMC);
            this.Referral_ReferringDoctor = new bltUserAccount(NIM3_Referral.getReferringPhysicianID());
            this.Referral_ReferringDoctor .setUniqueModifyComments(UMC);
            this.Case_AssignedTo = new bltUserAccount (NIM3_CaseAccount.getAssignedToID());
            this.Case_AssignedTo.setUniqueModifyComments(UMC);
            this.Case_AssignedTo_Payer = new bltNIM3_PayerMaster (Case_AssignedTo.getPayerID());
            this.Case_AssignedTo_Payer.setUniqueModifyComments(UMC);

            //pulling logic into central method
            isAlertSkip_Adj = NIMUtils.isAlertSkip(Case_Adjuster,NIMUtils.CASE_USER_ADJUSTER, 1);
            isAlertSkip_Adm = NIMUtils.isAlertSkip(Case_Admin, NIMUtils.CASE_USER_ADMIN, 1);
            isAlertSkip_Adm2 = NIMUtils.isAlertSkip(Case_Admin2, NIMUtils.CASE_USER_ADMIN2, 1);
            isAlertSkip_Adm3 = NIMUtils.isAlertSkip(Case_Admin3, NIMUtils.CASE_USER_ADMIN3, 1);
            isAlertSkip_Adm4 = NIMUtils.isAlertSkip(Case_Admin4, NIMUtils.CASE_USER_ADMIN4, 1);
            isAlertSkip_NCM = NIMUtils.isAlertSkip(Case_NurseCaseManager, NIMUtils.CASE_USER_NURSE_CASE_MANAGER, 1);
            isAlertSkip_RefDr = NIMUtils.isAlertSkip(Referral_ReferringDoctor, NIMUtils.CASE_USER_REFERRING_DOCTOR, 1);

            //pulling logic into central method
            isAlert2Skip_Adj = NIMUtils.isAlertSkip(Case_Adjuster,NIMUtils.CASE_USER_ADJUSTER, 2);
            isAlert2Skip_Adm = NIMUtils.isAlertSkip(Case_Admin, NIMUtils.CASE_USER_ADMIN, 2);
            isAlert2Skip_Adm2 = NIMUtils.isAlertSkip(Case_Admin2, NIMUtils.CASE_USER_ADMIN2, 2);
            isAlert2Skip_Adm3 = NIMUtils.isAlertSkip(Case_Admin3, NIMUtils.CASE_USER_ADMIN3, 2);
            isAlert2Skip_Adm4 = NIMUtils.isAlertSkip(Case_Admin4, NIMUtils.CASE_USER_ADMIN4, 2);
            isAlert2Skip_NCM = NIMUtils.isAlertSkip(Case_NurseCaseManager, NIMUtils.CASE_USER_NURSE_CASE_MANAGER, 2);
            isAlert2Skip_RefDr = NIMUtils.isAlertSkip(Referral_ReferringDoctor, NIMUtils.CASE_USER_REFERRING_DOCTOR, 2);

            //need to repeat logic
            isReportSkip_Adj = NIMUtils.isAlertSkip(Case_Adjuster,NIMUtils.CASE_USER_ADJUSTER, 3);
            isReportSkip_Adm = NIMUtils.isAlertSkip(Case_Admin, NIMUtils.CASE_USER_ADMIN, 3);
            isReportSkip_Adm2 = NIMUtils.isAlertSkip(Case_Admin2, NIMUtils.CASE_USER_ADMIN2, 3);
            isReportSkip_Adm3 = NIMUtils.isAlertSkip(Case_Admin3, NIMUtils.CASE_USER_ADMIN3, 3);
            isReportSkip_Adm4 = NIMUtils.isAlertSkip(Case_Admin4, NIMUtils.CASE_USER_ADMIN4, 3);
            isReportSkip_NCM = NIMUtils.isAlertSkip(Case_NurseCaseManager, NIMUtils.CASE_USER_NURSE_CASE_MANAGER, 3);
            isReportSkip_RefDr = NIMUtils.isAlertSkip(Referral_ReferringDoctor, NIMUtils.CASE_USER_REFERRING_DOCTOR, 3);
            isReportSkip_Adj = false;
            isReportSkip_RefDr = false;

            //skips alerts because case is not approved.
            if (NIM3_Referral.getReferralStatusID()!=1)
            {
                isAlertSkip_Adj = true;
                isAlertSkip_Adm = true;
                isAlertSkip_Adm2 = true;
                isAlertSkip_Adm3 = true;
                isAlertSkip_Adm4 = true;
                isAlertSkip_NCM = true;

                isAlert2Skip_Adj = true;
                isAlert2Skip_Adm = true;
                isAlert2Skip_Adm2 = true;
                isAlert2Skip_Adm3 = true;
                isAlert2Skip_Adm4 = true;
                isAlert2Skip_NCM = true;
            }


        }

    */

        public String getScanPass()
        {
            return this.getNIM3_Encounter().getScanPass();
        }

        public boolean isEscalatedSTAT()
        {
            if (this.getNIM3_Encounter().getisSTAT()==1)
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        public boolean isNextImageDirectCase(){
            return (this.getNIM3_PayerMaster().getIsNID()==1)||(this.getNIM3_PayerMaster().getParentPayerID()==ConfigurationInformation.NIM_DB_NID_PARENT_PAYER || this.getNIM3_PayerMaster().getPayerID()==ConfigurationInformation.NIM_DB_NID_PARENT_PAYER);
        }



        public String getFeeScheduleRateTablePayerMRI(String sDel)
        {
    //need to update this to use the actual appointment State/ZIP

            if (this.getNIM3_Encounter().getDateOfService().after(NIMUtils.getBeforeTime()))
            {
                return getFeeScheduleRateTablePayerMRI(this.getNIM3_CaseAccount().getPatientZIP(),this.getNIM3_Encounter().getDateOfService(),sDel);
            }
            else
            {
                return getFeeScheduleRateTablePayerMRI(this.getNIM3_CaseAccount().getPatientZIP(),PLCUtils.getToday(),sDel);
            }
        }


        public String getFeeScheduleRateTablePayerMRI(String sZIP,java.util.Date EffDate, String sDel)
        {
            java.text.DecimalFormat DFCurrency = new DecimalFormat(PLCUtils.String_defDecFormatCurrency1);
            return
                    "mr_wo = " + DFCurrency.format(NIMUtils.getPayerAllowAmount(this.getNIM3_PayerMaster().getPayerID(),sZIP,"mr_wo","",EffDate)) + sDel +
                            "mr_w = " + DFCurrency.format(NIMUtils.getPayerAllowAmount(this.getNIM3_PayerMaster().getPayerID(),sZIP,"mr_w","",EffDate)) + sDel +
                            "mr_wwo = " + DFCurrency.format(NIMUtils.getPayerAllowAmount(this.getNIM3_PayerMaster().getPayerID(),sZIP,"mr_wwo","",EffDate)) + sDel +
                            "ct_wo = " + DFCurrency.format(NIMUtils.getPayerAllowAmount(this.getNIM3_PayerMaster().getPayerID(),sZIP,"ct_wo","",EffDate)) + sDel +
                            "ct_w = " + DFCurrency.format(NIMUtils.getPayerAllowAmount(this.getNIM3_PayerMaster().getPayerID(),sZIP,"ct_w","",EffDate)) + sDel +
                            "ct_wwo = " + DFCurrency.format(NIMUtils.getPayerAllowAmount(this.getNIM3_PayerMaster().getPayerID(),sZIP,"ct_wwo","",EffDate)) + sDel;
        }


        public String getPayerFullName()
        {
            if (this.getNIM3_PayerMaster()!=null&&this.getNIM3_ParentPayerMaster()!=null)
            {
                return this.getNIM3_PayerMaster().getPayerName() + " (" + this.getNIM3_ParentPayerMaster().getPayerName() + ")";
            }
            else if (this.getNIM3_PayerMaster()!=null)
            {
                return this.getNIM3_PayerMaster().getPayerName() + " (No Parent)";
            }
            else
            {
               return "Invalid Payer";
            }
        }

            public boolean isEncounterVoid()
        {
            return NIMUtils.isEncounterVoid(this.getNIM3_Encounter());
        }




        public String getCaseDashboard_AlertCode_HTML()
        {
            /*
            <%if (myPM.getPayerName().indexOf("Tyson")>=0||myPM.getParentPayerID()==138){%><script language="javascript">alert("Note: This is a Tyson case.  Please do not call/email the Patient directly for any reason.\n\nIf you have any questions, please see Andy Lock")</script><%}else if (myPM.getPayerID()==214||myPM.getPayerID()==216||myPM.getPayerID()==523){%><script language="javascript">alert("Note: This is a Chartis/AIG case.  Please See NetDev for Scheduling to ensure that we can obtain a CD of the Scan.\n\nIf you have any questions, please see Andy Lock")</script><%}else if (myPM.getPayerID()==297){%><script language="javascript">alert("Note: This is a PMA case. Please send all reports as TIFF Documents.\n\nALSO: if auth is needed\nDO NOT CONTACT THE ADJUSTER\nCall the customer service line at: 800-476-2669\n\nIf you have any questions, please see Elizabeth Vidal or Andy Lock")</script><%}%>
             */
            String myVal = "";
            if (this.getNIM3_CaseAccount().getPayerID()==50)
            {
                myVal = "<script language=\"javascript\">alert(\"WARNING\\n\\nThis case is linked to NO PAYER, please fix this ASAP!\");</script>";
            }
            if (this.getCase_Adjuster().getPayerID()==50)
            {
                myVal = "<script language=\"javascript\">alert(\"WARNING\\n\\nThe Adjuster for this Case is linked to NO PAYER, please fix this ASAP or notify a manager!\");</script>";
            }


            if (this.getNIM3_PayerMaster().getImportantNotes_Alert().equalsIgnoreCase("")&&this.getNIM3_ParentPayerMaster().getImportantNotes_Alert().equalsIgnoreCase(""))
            {
                myVal += "<!-- No Payer Popup Alerts -->";
            }
            else
            {
                String tempVal = "";
                if (!this.getNIM3_PayerMaster().getImportantNotes_Alert().equalsIgnoreCase(""))
                {
                    tempVal += "Payer ["+this.getNIM3_PayerMaster().getPayerName()+"]: "+ DataControlUtils.Text2OneLine(this.getNIM3_PayerMaster().getImportantNotes_Alert());
                }
                if (!this.getNIM3_ParentPayerMaster().getImportantNotes_Alert().equalsIgnoreCase(""))
                {
                    if (!tempVal.equalsIgnoreCase(""))
                    {
                        tempVal+="\\n\\n";
                    }
                    tempVal += "Parent Payer ["+this.getNIM3_ParentPayerMaster().getPayerName()+"]: "+ DataControlUtils.Text2OneLine(this.getNIM3_ParentPayerMaster().getImportantNotes_Alert());
                }
                myVal += "<script language=\"javascript\">alert(\""+tempVal+"\");</script>";
            }

            String tempUserVal = "";
            if (!this.getCase_Adjuster().getImportantNotes_Alert().equalsIgnoreCase(""))
            {
                if (!tempUserVal.equalsIgnoreCase(""))
                {
                    tempUserVal+="\\n\\n";
                }
                tempUserVal += "Adjuster ["+this.getCase_Adjuster().getContactFirstName()+" "+this.getCase_Adjuster().getContactLastName()+"]: "+ DataControlUtils.Text2OneLine(this.getCase_Adjuster().getImportantNotes_Alert());
            }
            if (this.getCase_NurseCaseManager()!=null&&!this.getCase_NurseCaseManager().getImportantNotes_Alert().equalsIgnoreCase(""))
            {
                if (!tempUserVal.equalsIgnoreCase(""))
                {
                    tempUserVal+="\\n\\n";
                }
                tempUserVal += "Nurse Case Mgr ["+this.getCase_NurseCaseManager().getContactFirstName()+" "+this.getCase_NurseCaseManager().getContactLastName()+"]: "+ DataControlUtils.Text2OneLine(this.getCase_NurseCaseManager().getImportantNotes_Alert());
            }
            if (!this.getReferral_ReferringDoctor().getImportantNotes_Alert().equalsIgnoreCase(""))
            {
                if (!tempUserVal.equalsIgnoreCase(""))
                {
                    tempUserVal+="\\n\\n";
                }
                tempUserVal += "Ref Dr ["+this.getReferral_ReferringDoctor().getContactFirstName()+" "+this.getReferral_ReferringDoctor().getContactLastName()+"]: "+ DataControlUtils.Text2OneLine(this.getReferral_ReferringDoctor().getImportantNotes_Alert());
            }
            if (this.getCase_Admin()!=null&&!this.getCase_Admin().getImportantNotes_Alert().equalsIgnoreCase(""))
            {
                if (!tempUserVal.equalsIgnoreCase(""))
                {
                    tempUserVal+="\\n\\n";
                }
                tempUserVal += "Case Adm1 ["+this.getCase_Admin().getContactFirstName()+" "+this.getCase_Admin().getContactLastName()+"]: "+ DataControlUtils.Text2OneLine(this.getCase_Admin().getImportantNotes_Alert());
            }
            if (this.getCase_Admin2()!=null&&!this.Case_Admin2.getImportantNotes_Alert().equalsIgnoreCase(""))
            {
                if (!tempUserVal.equalsIgnoreCase(""))
                {
                    tempUserVal+="\\n\\n";
                }
                tempUserVal += "Case Adm2 ["+this.getCase_Admin2().getContactFirstName()+" "+this.getCase_Admin2().getContactLastName()+"]: "+ DataControlUtils.Text2OneLine(this.getCase_Admin2().getImportantNotes_Alert());
            }
            if (this.getCase_Admin3()!=null&&!this.Case_Admin3.getImportantNotes_Alert().equalsIgnoreCase(""))
            {
                if (!tempUserVal.equalsIgnoreCase(""))
                {
                    tempUserVal+="\\n\\n";
                }
                tempUserVal += "Case Adm3 ["+this.getCase_Admin3().getContactFirstName()+" "+this.getCase_Admin3().getContactLastName()+"]: "+ DataControlUtils.Text2OneLine(this.getCase_Admin3().getImportantNotes_Alert());
            }
            if (this.getCase_Admin4()!=null&&!this.Case_Admin4.getImportantNotes_Alert().equalsIgnoreCase(""))
            {
                if (!tempUserVal.equalsIgnoreCase(""))
                {
                    tempUserVal+="\\n\\n";
                }
                tempUserVal += "Case Adm4 ["+this.getCase_Admin4().getContactFirstName()+" "+this.getCase_Admin4().getContactLastName()+"]: "+ DataControlUtils.Text2OneLine(this.getCase_Admin4().getImportantNotes_Alert());
            }
            if (!tempUserVal.equalsIgnoreCase(""))
            {
                myVal += "<script language=\"javascript\">alert(\""+tempUserVal+"\");</script>";
            }

            return myVal;
        }

        public String getImportantNotes_All(bltUserAccount theUser, String lineBreak)
        {
            String myVal = "";
            if (!theUser.getImportantNotes().equalsIgnoreCase(""))
            {
                myVal += lineBreak + theUser.getImportantNotes();
            }
            if (!theUser.getImportantNotes_Alert().equalsIgnoreCase(""))
            {
                myVal += lineBreak  + theUser.getImportantNotes_Alert();
            }
            if (!theUser.getEmailAlertNotes_Alert().equalsIgnoreCase(""))
            {
                myVal += lineBreak  + theUser.getEmailAlertNotes_Alert();
            }
            return myVal;

        }

        public String getEmailImportantNotes_Payer(String lineBreak)
        {
            String myVal = "";
            if (!this.getNIM3_ParentPayerMaster().getEmailAlertNotes_Alert().equalsIgnoreCase(""))
            {
                if (!myVal.equalsIgnoreCase(""))
                {
                    myVal+=lineBreak;
                }
                myVal +=  this.getNIM3_ParentPayerMaster().getEmailAlertNotes_Alert();
            }
            if (!this.getNIM3_PayerMaster().getEmailAlertNotes_Alert().equalsIgnoreCase(""))
            {
                if (!myVal.equalsIgnoreCase(""))
                {
                    myVal+=lineBreak;
                }
                myVal +=  this.getNIM3_PayerMaster().getEmailAlertNotes_Alert();
            }
            return myVal;

        }


        public String getCaseDashboard_CombinedNotes(String lineBreak)
        {
            String myVal = "";
            if (!this.getNIM3_PayerMaster().getImportantNotes().equalsIgnoreCase(""))
            {
                if (!myVal.equalsIgnoreCase(""))
                {
                    myVal+=lineBreak;
                }
                myVal +=  "Case Notes: " + this.getNIM3_CaseAccount().getComments();
            }
            if (!this.getNIM3_PayerMaster().getImportantNotes().equalsIgnoreCase(""))
            {
                if (!myVal.equalsIgnoreCase(""))
                {
                    myVal+=lineBreak;
                }
                myVal +=  "Payer["+this.getNIM3_PayerMaster().getPayerName()+"]: " + this.getNIM3_PayerMaster().getImportantNotes();
            }
            if (!this.getNIM3_ParentPayerMaster().getImportantNotes().equalsIgnoreCase(""))
            {
                if (!myVal.equalsIgnoreCase(""))
                {
                    myVal+=lineBreak;
                }
                myVal +=  "Parent["+this.getNIM3_ParentPayerMaster().getPayerName()+"]: " + this.getNIM3_ParentPayerMaster().getImportantNotes();
            }
            if (!this.getCase_Adjuster().getImportantNotes().equalsIgnoreCase(""))
            {
                if (!myVal.equalsIgnoreCase(""))
                {
                    myVal+=lineBreak;
                }
                myVal +=  "Adj["+this.getCase_Adjuster().getContactFirstName()+" "+this.getCase_Adjuster().getContactLastName()+"]: " + this.getCase_Adjuster().getImportantNotes();
            }
            if (this.getCase_NurseCaseManager()!=null&&!this.getCase_NurseCaseManager().getImportantNotes().equalsIgnoreCase(""))
            {
                if (!myVal.equalsIgnoreCase(""))
                {
                    myVal+=lineBreak;
                }
                myVal +=  "NCM["+this.getCase_NurseCaseManager().getContactFirstName()+" "+this.getCase_NurseCaseManager().getContactLastName()+"]: " + this.getCase_NurseCaseManager().getImportantNotes();
            }
            if (this.getReferral_ReferringDoctor()!=null&&!this.getReferral_ReferringDoctor().getImportantNotes().equalsIgnoreCase(""))
            {
                if (!myVal.equalsIgnoreCase(""))
                {
                    myVal+=lineBreak;
                }
                myVal +=  "Ref Dr["+this.getReferral_ReferringDoctor().getContactFirstName()+" "+this.getReferral_ReferringDoctor().getContactLastName()+"]: " + this.getReferral_ReferringDoctor().getImportantNotes();
            }
            if (this.getCase_Admin()!=null&&!this.getCase_Admin().getImportantNotes().equalsIgnoreCase(""))
            {
                if (!myVal.equalsIgnoreCase(""))
                {
                    myVal+=lineBreak;
                }
                myVal +=  "Adm1["+this.getCase_Admin().getContactFirstName()+" "+this.getCase_Admin().getContactLastName()+"]: " + this.getCase_Admin().getImportantNotes();
            }
            if (this.getCase_Admin2()!=null&&!this.getCase_Admin2().getImportantNotes().equalsIgnoreCase(""))
            {
                if (!myVal.equalsIgnoreCase(""))
                {
                    myVal+=lineBreak;
                }
                myVal +=  "Adm2["+this.getCase_Admin2().getContactFirstName()+" "+this.Case_Admin2.getContactLastName()+"]: " + this.getCase_Admin2().getImportantNotes();
            }
            if (this.getCase_Admin3()!=null&&!this.getCase_Admin3().getImportantNotes().equalsIgnoreCase(""))
            {
                if (!myVal.equalsIgnoreCase(""))
                {
                    myVal+=lineBreak;
                }
                myVal +=  "Adm3["+this.getCase_Admin3().getContactFirstName()+" "+this.getCase_Admin3().getContactLastName()+"]: " + this.getCase_Admin3().getImportantNotes();
            }
            if (this.getCase_Admin4()!=null&&!this.getCase_Admin4().getImportantNotes().equalsIgnoreCase(""))
            {
                if (!myVal.equalsIgnoreCase(""))
                {
                    myVal+=lineBreak;
                }
                myVal +=  "Adm4["+this.getCase_Admin4().getContactFirstName()+" "+this.getCase_Admin4().getContactLastName()+"]: " + this.getCase_Admin4().getImportantNotes();
            }
            return myVal;
        }


        public String getCourtesyStatus_Display()
        {
            String myVal ="None-Error";
            if (this.getNIM3_Encounter().getisCourtesy()==0) {myVal = "NA";}
            else if (this.getNIM3_Encounter().getisCourtesy()==1) {myVal = "Yes";}
            else if (this.getNIM3_Encounter().getisCourtesy()==2) {myVal = "No";}
            else if (this.getNIM3_Encounter().getisCourtesy()==3) {myVal = "Decline";}
            else if (this.getNIM3_Encounter().getisCourtesy()==4) {myVal = "Pending";}
            return myVal;
        }


        public String getEncounterType_Display()
        {
            String myVal ="None-Error";
            if (this.getNIM3_Encounter().getEncounterTypeID()==0 ) {myVal = "Pending*";}
            else if (this.getNIM3_Encounter().getEncounterTypeID()==1 ) {myVal = "Other: Single";}
            else if (this.getNIM3_Encounter().getEncounterTypeID()==2 ) {myVal = "Other: Multiple";}
            else if (this.getNIM3_Encounter().getEncounterTypeID()==3 ) {myVal = "Other";}
            else if (this.getNIM3_Encounter().getEncounterTypeID()==4 ) {myVal = "Diag: MRI";}
            else if (this.getNIM3_Encounter().getEncounterTypeID()==5 ) {myVal = "Diag: CT";}
            else if (this.getNIM3_Encounter().getEncounterTypeID()==6 ) {myVal = "Diag: EMG";}
            else if (this.getNIM3_Encounter().getEncounterTypeID()==7 ) {myVal = "Diag: NCV";}
            else if (this.getNIM3_Encounter().getEncounterTypeID()==8 ) {myVal = "Diag: PET";}
            else if (this.getNIM3_Encounter().getEncounterTypeID()==9 ) {myVal = "Diag: Other";}
            else if (this.getNIM3_Encounter().getEncounterTypeID()==1 ) {myVal = "Lab";}
            return myVal;
        }



        public String getEncounterStory(String sDel)
        {
            String myVal = sDel;
            bltUserAccount myUA_TTReqCre = new bltUserAccount(this.getNIM3_Encounter().getTimeTrack_ReqCreated_UserID());
            myVal += "This referral was received on " + PLCUtils.getDisplayDateWithTime(this.getNIM3_Referral().getReceiveDate())
                    + " and initial creation was completed on " +PLCUtils.getDisplayDateWithTime(this.getNIM3_Encounter().getTimeTrack_ReqCreated())
                    + " by " + myUA_TTReqCre.getContactFirstName() + " " +  myUA_TTReqCre.getContactLastName() + "." + sDel ;
            myVal += "Receipt confirmations look like they were sent out as follows:" + sDel;
            if (this.getNIM3_Encounter().getSentTo_ReqRec_Adj().after(NIMUtils.getBeforeTime()))
            {
                myVal += " To the Adjuster (" + this.getCase_Adjuster().getContactFirstName() + " " + this.getCase_Adjuster().getContactLastName() + ") on "
                        + PLCUtils.getDisplayDateWithTime(this.getNIM3_Encounter().getSentTo_ReqRec_Adj()) + ";" + sDel;
            }
            if (this.getNIM3_Encounter().getSentTo_ReqRec_NCM().after(NIMUtils.getBeforeTime()))
            {
                myVal += " To the Nurse Case Manager (" +  this.getCase_NurseCaseManager().getContactFirstName() + " " + this.getCase_NurseCaseManager().getContactLastName() + ") on "
                        + PLCUtils.getDisplayDateWithTime(this.getNIM3_Encounter().getSentTo_ReqRec_NCM()) + ";" + sDel;
            }
            if (this.getNIM3_Encounter().getSentTo_ReqRec_Adm().after(NIMUtils.getBeforeTime()))
            {
                myVal += " To the Case Admin 1 (" +  this.getCase_Admin().getContactFirstName() + " " + this.getCase_Admin().getContactLastName() + ") on "
                        + PLCUtils.getDisplayDateWithTime(this.getNIM3_Encounter().getSentTo_ReqRec_Adm()) + ";" + sDel;
            }
            if (this.getNIM3_Encounter().getSentTo_ReqRec_Adm2().after(NIMUtils.getBeforeTime()))
            {
                myVal += " To the Case Admin 2 (" +  this.getCase_Admin2().getContactFirstName() + " " + this.getCase_Admin2().getContactLastName() + ") on "
                        + PLCUtils.getDisplayDateWithTime(this.getNIM3_Encounter().getSentTo_ReqRec_Adm2()) + ";" + sDel;
            }
            if (this.getNIM3_Encounter().getSentTo_ReqRec_Adm3().after(NIMUtils.getBeforeTime()))
            {
                myVal += " To the Case Admin 3 (" +  this.getCase_Admin3().getContactFirstName() + " " + this.getCase_Admin3().getContactLastName() + ") on "
                        + PLCUtils.getDisplayDateWithTime(this.getNIM3_Encounter().getSentTo_ReqRec_Adm3()) + ";" + sDel;
            }
            if (this.getNIM3_Encounter().getSentTo_ReqRec_Adm4().after(NIMUtils.getBeforeTime()))
            {
                myVal += " To the Case Admin 4 (" +  this.getCase_Admin4().getContactFirstName() + " " + this.getCase_Admin4().getContactLastName() + ") on "
                        + PLCUtils.getDisplayDateWithTime(this.getNIM3_Encounter().getSentTo_ReqRec_Adm4()) + ";" + sDel;
            }
            if (this.getNIM3_Encounter().getTimeTrack_ReqProc().after(NIMUtils.getBeforeTime()))
            {
                bltUserAccount myUA_TTReqPro = new bltUserAccount(this.getNIM3_Encounter().getTimeTrack_ReqProc_UserID());
                myVal += "This referral was processed on " +PLCUtils.getDisplayDateWithTime(this.getNIM3_Encounter().getTimeTrack_ReqProc())
                        + " by " + myUA_TTReqPro.getContactFirstName() + " " +  myUA_TTReqPro.getContactLastName() + "." + sDel ;
            }
            if (this.getNIM3_Encounter().getTimeTrack_ReqSched().after(NIMUtils.getBeforeTime()))
            {
                bltUserAccount myUA_TTReqSch = new bltUserAccount(this.getNIM3_Encounter().getTimeTrack_ReqSched_UserID());
                myVal += "Scheduling was completed on " +PLCUtils.getDisplayDateWithTime(this.getNIM3_Encounter().getTimeTrack_ReqSched())
                        + " by " + myUA_TTReqSch.getContactFirstName() + " " +  myUA_TTReqSch.getContactLastName()
                        + " with an initial appointment of  " +PLCUtils.getDisplayDateWithTime(this.getNIM3_Encounter().getTimeTrack_ReqInitialAppointment()) + sDel ;
            }
            if (this.getNIM3_Encounter().getSeeNetDev_Flagged()==1)
            {
                myVal += "Note: the NetDev department was involved in scheduling this case and had the following CommTracks:" + sDel;
                searchDB2 mySS_ct = new searchDB2();
                try
                {
                    java.sql.ResultSet myRS_ct = null;
                    String mySQL_ct = "SELECT commtrackid from tnim3_commtrack where encounterid=" + this.getNIM3_Encounter().getEncounterID() +" AND CommTypeID=4";
                    myRS_ct = mySS_ct.executeStatement(mySQL_ct);
                    int i2cnt_ct=0;
                    while(myRS_ct!=null&&myRS_ct.next())
                    {
                        bltNIM3_CommTrack myCT = new bltNIM3_CommTrack (new  Integer(myRS_ct.getString("commtrackid")));
                        if (myCT.getCommEnd().after(NIMUtils.getBeforeTime()))
                        {
                            myVal += " Request sent on " + PLCUtils.getDisplayDateWithTime(myCT.getCommStart()) + " with a duration of " + Math.round((myCT.getCommEnd().getTime()-myCT.getCommStart().getTime())/1000/60/60) + " hours";
                        }
                        else
                        {
                            myVal += " Request sent on " + PLCUtils.getDisplayDateWithTime(myCT.getCommStart()) + " and still open (" + PLCUtils.getHoursTill_Display(myCT.getCommStart(), false)+ " hours so far)";
                        }
                    }
                    mySS_ct.closeAll();
                }
                catch (Exception eee)
                {
                    DebugLogger.printLine("NIM3_EncounterObject:getEncounterStory:ParseCommTracks_NetDev:["+ eee +"]");
                } finally {
                    mySS_ct.closeAll();
                }

            }

            if (this.getNIM3_Encounter().getisCourtesy()==1)
            {
                myVal += "This case was designated to be completed as a Courtesy Schedule";
            }
            else if (this.getNIM3_Encounter().getisCourtesy()==4)
            {
                myVal += "This case is awaiting approval for a Courtesy Schedule.";
            }
            else if (this.getNIM3_Encounter().getisCourtesy()==3)
            {
                myVal += "This case has been declined as a Courtesy Schedule.";
            }

            //report
            if (NIM3_Encounter.getTimeTrack_ReqDelivered().after(NIMUtils.getBeforeTime()))
            {
                bltUserAccount myUA_TTReqDel = new bltUserAccount(this.getNIM3_Encounter().getTimeTrack_ReqDelivered_UserID());
                myVal += "Report was delivered on " +PLCUtils.getDisplayDateWithTime(this.getNIM3_Encounter().getTimeTrack_ReqDelivered())
                        + " (an estimated " + Math.round((this.getNIM3_Encounter().getTimeTrack_ReqDelivered().getTime()-this.getNIM3_Appointment().getAppointmentTime().getTime())/1000/60/60) + " hours after appointment)"
                        + " by " + myUA_TTReqDel.getContactFirstName() + " " +  myUA_TTReqDel.getContactLastName() + sDel;
            }
            myVal +="Billing:" + sDel;
            if (this.getNIM3_Encounter().getSentTo_Bill_Pay().before(NIMUtils.getBeforeTime()))
            {
                myVal += "We have not yet sent any bills to the Payer." + sDel;
            }
            else
            {
                myVal += "We have sent a bill to the Payer: " + PLCUtils.getDaysTill_Display(this.getNIM3_Encounter().getSentTo_Bill_Pay(),false) + "." + sDel;

            }
            if (this.getNIM3_Encounter().getRec_Bill_Pro().before(NIMUtils.getBeforeTime()))
            {
                myVal +="We have not yet received a bill (HFCA) from Provider." + sDel;
            }
            else
            {
                myVal +="We have received a bill (HFCA) from Provider:" + PLCUtils.getDaysTill_Display(this.getNIM3_Encounter().getRec_Bill_Pro(),false) + ".";
            }
            NIM3_BillingObject2 myBO2 = getBillingTotals2();
            myVal += myBO2.getStatus(sDel);


            return myVal;
        }


        public boolean isEncounterProcessed()
        {
            boolean myVal = false;
            if (!this.getNIM3_Encounter().getTimeTrack_ReqProc().before(NIMUtils.getBeforeTime()) && this.getNIM3_Encounter().getTimeTrack_ReqProc_UserID()>0)
            {
                myVal = true;
            }
            else
            {
            }
            return myVal;
        }

        public boolean ProcessEncounter(bltUserAccount CurrentUserAccount)
        {
            boolean myVal = false;
            try
            {
    //TODO: Must Update
                if ( (this.getNIM3_Encounter().getTimeTrack_ReqProc_UserID()==0||this.getNIM3_Encounter().getTimeTrack_ReqProc().before(NIMUtils.getBeforeTime())) &&NIMUtils.isEncounterReadyToSchedule(this.getNIM3_Encounter().getEncounterID(),false,false))
                {
                    this.getNIM3_Encounter().setTimeTrack_ReqProc(PLCUtils.getNowDate(true));
                    this.getNIM3_Encounter().setTimeTrack_ReqProc_UserID(CurrentUserAccount.getUserID());
                    this.getNIM3_Encounter().setUniqueModifyComments("SystemGenerated:EncounterProcessed:UserID:"+CurrentUserAccount.getUserID());
                    this.getNIM3_Encounter().commitData();
                    myVal = true;
                    //insert commTrack for processed.
                    bltNIM3_CommTrack myCT = new bltNIM3_CommTrack();
                    myCT.setCommStart(PLCUtils.getNowDate(false));
                    myCT.setCommEnd(PLCUtils.getNowDate(false));
                    myCT.setAlertStatusCode(new Integer(1));
                    myCT.setReferralID(this.getNIM3_Referral().getReferralID());
                    myCT.setEncounterID(this.getNIM3_Encounter().getEncounterID());
                    myCT.setMessageText("Encounter Processed on: " + PLCUtils.getNowDate(false)  );
                    myCT.setMessageName(CurrentUserAccount.getContactFirstName() + " " + CurrentUserAccount.getContactLastName());
                    myCT.setUniqueModifyComments("System Generated:Encounter Processed: UserID ["+ CurrentUserAccount.getUserID()+"]=" + CurrentUserAccount.getLogonUserName());
                    myCT.commitData();
                }
            }
            catch(Exception PEerror)
            {
                DebugLogger.printLine("NIM3_EncounterObject:ProcessEncounter:" + PEerror);
            }

            return myVal;
        }

        public NIM3_AddressObject getWhoShouldIBillTo()
        {
            return NIMUtils.getWhoShouldIBillTo(this.getNIM3_CaseAccount().getPayerID(),this.getNIM3_CaseAccount().getBillToContactID());
        }

        public NIM3_AddressObject getPatientAddressObject()
        {
            NIM3_AddressObject myAO = new NIM3_AddressObject();
            myAO.setAddress1(this.getNIM3_CaseAccount().getPatientAddress1());
            myAO.setAddress2(this.getNIM3_CaseAccount().getPatientAddress2());
            myAO.setCity(this.getNIM3_CaseAccount().getPatientCity());
            myAO.setStateID(this.getNIM3_CaseAccount().getPatientStateID());
            myAO.setZIP(this.getNIM3_CaseAccount().getPatientZIP());
            return myAO;
        }

        public NIM3_AddressObject getAppointmentPracticeAddressObject()
        {
            NIM3_AddressObject myAO = new NIM3_AddressObject();
            if (this.getNIM3_Appointment()!=null&&this.getAppointment_PracticeMaster()!=null)
            {
                myAO.setAddress1(this.getAppointment_PracticeMaster().getOfficeAddress1());
                myAO.setAddress2(this.getAppointment_PracticeMaster().getOfficeAddress2());
                myAO.setCity(this.getAppointment_PracticeMaster().getOfficeCity());
                myAO.setStateID(this.getAppointment_PracticeMaster().getOfficeStateID());
                myAO.setZIP(this.getAppointment_PracticeMaster().getOfficeZIP());
            }
            return myAO;
        }

        public java.util.Vector getPatientPreScreen()
        {
            return getPatientPreScreen(this.getNIM3_CaseAccount());
        }

       public boolean isPreScreenComplete()
        {
            boolean myVal = true;
            if (this.getNIM3_CaseAccount().getPatientHasImplants()==0)
            {
                myVal =false;
            }
            if (this.getNIM3_CaseAccount().getPatientHasMetalInBody()==0)
            {
                myVal =false;
            }
            if (this.getNIM3_CaseAccount().getPatientHasAllergies()==0)
            {
                myVal =false;
            }
            if (this.getNIM3_CaseAccount().getPatientHasRecentSurgery()==0)
            {
                myVal =false;
            }
            if (this.getNIM3_CaseAccount().getPatientHasPreviousMRIs()==0)
            {
                myVal =false;
            }
            if (this.getNIM3_CaseAccount().getPatientHasKidneyLiverHypertensionDiabeticConditions()==0)
            {
                myVal =false;
            }
            if (this.getNIM3_CaseAccount().getPatientIsPregnant()==0)
            {
                myVal =false;
            }
            if (this.getNIM3_CaseAccount().getPatientIsClaus()==0)
            {
                myVal =false;
            }
            return myVal;
        }

        public static java.util.Vector getPatientPreScreen(bltNIM3_CaseAccount myCA)
        {
            java.util.Vector<String> myVal = new java.util.Vector<String>();
            if (myCA.getPatientHasImplants()==1)
            {
                myVal.add("Has Implants: YES   [" + myCA.getPatientHasImplantsDesc() + "]");
            }
            if (myCA.getPatientHasMetalInBody()==1)
            {
                myVal.add("Has Metal in Body: YES   [" + myCA.getPatientHasMetalInBodyDesc() + "]");
            }
            if (myCA.getPatientHasAllergies()==1)
            {
                myVal.add("Has Allergies: YES   [" + myCA.getPatientHasAllergiesDesc() + "]");
            }
            if (myCA.getPatientHasRecentSurgery()==1)
            {
                myVal.add("Has Recent Surgery: YES   [" + myCA.getPatientHasRecentSurgeryDesc() + "]");
            }
            if (myCA.getPatientHasPreviousMRIs()==1)
            {
                myVal.add("Has Previous MRI: YES   [" + myCA.getPatientHasPreviousMRIsDesc() + "]");
            }
            if (myCA.getPatientHasKidneyLiverHypertensionDiabeticConditions()==1)
            {
                myVal.add("Has Kidney/Liver/Hypertension/Diabetic Condition: YES   [" + myCA.getPatientHasKidneyLiverHypertensionDiabeticConditionsDesc() + "]");
            }
            if (myCA.getPatientIsPregnant()==1)
            {
                myVal.add("Is Pregnant: YES   [" + myCA.getPatientIsPregnantDesc() + "]");
            }
            if (myCA.getPatientIsClaus()==1)
            {
                myVal.add("Severely Claustrophobic: YES ");
            }

            return myVal;
        }

        public boolean isRxReviewed()
        {
            boolean myVal = false;
            if (!getNIM3_Encounter().getTimeTrack_ReqRxReview().before(NIMUtils.getBeforeTime())&&getNIM3_Encounter().getTimeTrack_ReqRxReview_UserID()>0)
            {
                myVal = true;
            }
            return myVal;
        }

        public String getPatientPhoneDisplay()
        {
            String myVal = "";
            if (!getNIM3_CaseAccount().getPatientCellPhone().equalsIgnoreCase(""))
            {
                myVal +="Cell: " + getNIM3_CaseAccount().getPatientCellPhone();
            }
            else if (!getNIM3_CaseAccount().getPatientHomePhone().equalsIgnoreCase(""))
            {
                myVal +="Home: " + getNIM3_CaseAccount().getPatientHomePhone();
            }
            else if (!getNIM3_CaseAccount().getPatientWorkPhone().equalsIgnoreCase(""))
            {
                myVal +="Work: " + getNIM3_CaseAccount().getPatientWorkPhone();
            }

            return myVal;


        }


        public boolean isScanPassAlertsComplete(int AlertsType)
        {
            boolean myVal = false;
            if (AlertsType==1 &&
               (this.isAlertSkip_RefDr()||this.getNIM3_Encounter().getSentTo_ReqRec_RefDr().after(NIMUtils.getBeforeTime()))
             &&(this.isAlertSkip_Adj()||this.getNIM3_Encounter().getSentTo_ReqRec_Adj().after(NIMUtils.getBeforeTime()))
            &&(this.isAlertSkip_Adm()||this.getNIM3_Encounter().getSentTo_ReqRec_Adm().after(NIMUtils.getBeforeTime()))
            &&(this.isAlertSkip_Adm2()||this.getNIM3_Encounter().getSentTo_ReqRec_Adm2().after(NIMUtils.getBeforeTime()))
            &&(this.isAlertSkip_Adm3()||this.getNIM3_Encounter().getSentTo_ReqRec_Adm3().after(NIMUtils.getBeforeTime()))
            &&(this.isAlertSkip_Adm4()||this.getNIM3_Encounter().getSentTo_ReqRec_Adm4().after(NIMUtils.getBeforeTime()))
             &&(this.isAlertSkip_NCM()||this.getNIM3_Encounter().getSentTo_ReqRec_NCM().after(NIMUtils.getBeforeTime()))
                    )
            {
                myVal=true;
            }
            else if (AlertsType==2 &&
               (this.isAlert2Skip_RefDr()||this.getNIM3_Encounter().getSentTo_SP_RefDr().after(NIMUtils.getBeforeTime()))
             &&(this.isAlert2Skip_Adj()||this.getNIM3_Encounter().getSentTo_SP_Adj().after(NIMUtils.getBeforeTime()))
            &&(this.isAlert2Skip_Adm()||this.getNIM3_Encounter().getSentTo_SP_Adm().after(NIMUtils.getBeforeTime()))
            &&(this.isAlert2Skip_Adm2()||this.getNIM3_Encounter().getSentTo_SP_Adm2().after(NIMUtils.getBeforeTime()))
            &&(this.isAlert2Skip_Adm3()||this.getNIM3_Encounter().getSentTo_SP_Adm3().after(NIMUtils.getBeforeTime()))
            &&(this.isAlert2Skip_Adm4()||this.getNIM3_Encounter().getSentTo_SP_Adm4().after(NIMUtils.getBeforeTime()))
             &&(this.isAlert2Skip_NCM()||this.getNIM3_Encounter().getSentTo_SP_NCM().after(NIMUtils.getBeforeTime()))
                    )
            {
                myVal=true;
            }
            else if (AlertsType==3 &&
               (this.isReportSkip_RefDr()||this.getNIM3_Encounter().getSentToRefDr().after(NIMUtils.getBeforeTime()))
             &&(this.isReportSkip_Adj()||this.getNIM3_Encounter().getSentToAdj().after(NIMUtils.getBeforeTime()))
            &&(this.isReportSkip_Adm()||this.getNIM3_Encounter().getSentToAdm().after(NIMUtils.getBeforeTime()))
            &&(this.isReportSkip_Adm2()||this.getNIM3_Encounter().getSentTo_RP_Adm2().after(NIMUtils.getBeforeTime()))
            &&(this.isReportSkip_Adm3()||this.getNIM3_Encounter().getSentTo_RP_Adm3().after(NIMUtils.getBeforeTime()))
            &&(this.isReportSkip_Adm4()||this.getNIM3_Encounter().getSentTo_RP_Adm4().after(NIMUtils.getBeforeTime()))
             &&(this.isReportSkip_NCM()||this.getNIM3_Encounter().getSentTo_RP_NCM().after(NIMUtils.getBeforeTime()))
                    )
            {
                myVal=true;
            }
            return myVal;
        }


        public bltNIM3_CaseAccount getNIM3_CaseAccount()
        {
            if (NIM3_CaseAccount==null)
            {
                setNIM3_CaseAccount(new bltNIM3_CaseAccount(this.getNIM3_Referral().getCaseID()));
            }
            return this.NIM3_CaseAccount;
        }

        public void setNIM3_CaseAccount(bltNIM3_CaseAccount NIM3_CaseAccount)
        {
            this.NIM3_CaseAccount = NIM3_CaseAccount;
            this.NIM3_CaseAccount.setUniqueModifyComments(this.getUniqueModifyComments());
        }

        public bltNIM3_Referral getNIM3_Referral()
        {
            if (this.NIM3_Referral==null)
            {
                this.setNIM3_Referral(new bltNIM3_Referral(this.getNIM3_Encounter().getReferralID()));
            }
            return this.NIM3_Referral;
        }

        public void setNIM3_Referral(bltNIM3_Referral NIM3_Referral)
        {
            this.NIM3_Referral = NIM3_Referral;
            this.NIM3_Referral.setUniqueModifyComments(this.getUniqueModifyComments());
        }

        public bltNIM3_Encounter getNIM3_Encounter()
        {
            //always already loaded
            return NIM3_Encounter;
        }

        public void setNIM3_Encounter(bltNIM3_Encounter NIM3_Encounter)
        {
            this.NIM3_Encounter = NIM3_Encounter;
            this.NIM3_Encounter.setUniqueModifyComments(this.getUniqueModifyComments());
        }

        public bltNIM3_Appointment getNIM3_Appointment()
        {
            if (NIM3_Appointment==null)
            {
                if (this.getNIM3_Encounter().getAppointmentID()>0)
                {
                    setNIM3_Appointment(new bltNIM3_Appointment(this.getNIM3_Encounter().getAppointmentID()));
                }
            }
            return NIM3_Appointment;
        }

        public void setNIM3_Appointment(bltNIM3_Appointment NIM3_Appointment)
        {
            this.NIM3_Appointment = NIM3_Appointment;
            this.NIM3_Appointment.setUniqueModifyComments(this.getUniqueModifyComments());
        }

        public bltNIM3_PayerMaster getNIM3_PayerMaster()
        {
            if (NIM3_PayerMaster==null)
            {
                setNIM3_PayerMaster(new bltNIM3_PayerMaster(this.getNIM3_CaseAccount().getPayerID()));
            }
            return NIM3_PayerMaster;
        }

        public void setNIM3_PayerMaster(bltNIM3_PayerMaster NIM3_PayerMaster)
        {
            this.NIM3_PayerMaster = NIM3_PayerMaster;
            this.NIM3_PayerMaster.setUniqueModifyComments(this.getUniqueModifyComments());
        }

        public bltNIM3_PayerMaster getNIM3_ParentPayerMaster()
        {
            if (NIM3_ParentPayerMaster==null)
            {
                setNIM3_ParentPayerMaster(new bltNIM3_PayerMaster(this.getNIM3_PayerMaster().getParentPayerID()));
            }
            return NIM3_ParentPayerMaster;
        }

        public void setNIM3_ParentPayerMaster(bltNIM3_PayerMaster NIM3_ParentPayerMaster)
        {
            this.NIM3_ParentPayerMaster = NIM3_ParentPayerMaster;
            this.NIM3_ParentPayerMaster.setUniqueModifyComments(this.getUniqueModifyComments());
        }

        public bltPracticeMaster getAppointment_PracticeMaster()
        {
            if (Appointment_PracticeMaster==null)
            {
                if (this.getNIM3_Appointment()!=null)
                {
                    setAppointment_PracticeMaster(new bltPracticeMaster(this.getNIM3_Appointment().getProviderID()));
                }
            }
            return Appointment_PracticeMaster;
        }

        public void setAppointment_PracticeMaster(bltPracticeMaster appointment_PracticeMaster)
        {
            this.Appointment_PracticeMaster = appointment_PracticeMaster;
            this.Appointment_PracticeMaster.setUniqueModifyComments(this.getUniqueModifyComments());
        }

        public bltUserAccount getCase_BillTo()
        {
            if (Case_BillTo==null&&this.getNIM3_CaseAccount().getBillToContactID()>0)
            {
                setCase_BillTo(new bltUserAccount(this.getNIM3_CaseAccount().getBillToContactID()));
            }
            return Case_BillTo;
        }

        public void setCase_BillTo(bltUserAccount case_BillTo)
        {
            this.Case_BillTo = case_BillTo;
            this.Case_BillTo.setUniqueModifyComments(this.getUniqueModifyComments());
        }


        public void setCase_AssignedTo(bltUserAccount case_AssignedTo)
        {
            this.Case_AssignedTo = case_AssignedTo;
            this.Case_AssignedTo.setUniqueModifyComments(this.getUniqueModifyComments());
        }


        public bltNIM3_PayerMaster getCase_AssignedTo_Payer()
        {
            if (Case_AssignedTo_Payer==null)
            {
                setCase_AssignedTo_Payer(new bltNIM3_PayerMaster(this.getCase_AssignedTo().getPayerID()));
            }
            return Case_AssignedTo_Payer;
        }

        public void setCase_AssignedTo_Payer(bltNIM3_PayerMaster case_AssignedTo_Payer)
        {
            this.Case_AssignedTo_Payer = case_AssignedTo_Payer;
            this.Case_AssignedTo_Payer.setUniqueModifyComments(this.getUniqueModifyComments());
        }


        public bltUserAccount getCase_Adjuster()
        {
            if (Case_Adjuster==null)
            {
                this.setCase_Adjuster(new bltUserAccount(this.getNIM3_CaseAccount().getAdjusterID()));
                this.setAlertSkip_Adj(NIMUtils.isAlertSkip(this.getCase_Adjuster(),NIMUtils.CASE_USER_ADJUSTER, 1));
                this.setAlert2Skip_Adj(NIMUtils.isAlertSkip(this.getCase_Adjuster(),NIMUtils.CASE_USER_ADJUSTER, 2));
                //this.setReportSkip_Adj(NIMUtils.isAlertSkip(this.setCase_Adjuster(),NIMUtils.CASE_USER_ADJUSTER, 3));
                this.setReportSkip_Adj(false);
                this.resetAlertSkips_CaseNotApproved();
            }
            return Case_Adjuster;
        }

        public void setCase_Adjuster(bltUserAccount case_Adjuster)
        {
            this.Case_Adjuster = case_Adjuster;
            this.Case_Adjuster.setUniqueModifyComments(this.getUniqueModifyComments());
        }

        public bltUserAccount getCase_Admin()
        {
            if (Case_Admin==null&&this.getNIM3_CaseAccount().getCaseAdministratorID()>0)
            {
                setCase_Admin(new bltUserAccount(this.getNIM3_CaseAccount().getCaseAdministratorID()));
                this.setAlertSkip_Adm(NIMUtils.isAlertSkip(this.getCase_Admin(), NIMUtils.CASE_USER_ADMIN, 1));
                this.setAlert2Skip_Adm(NIMUtils.isAlertSkip(this.getCase_Admin(), NIMUtils.CASE_USER_ADMIN, 2));
                this.setReportSkip_Adm(NIMUtils.isAlertSkip(this.getCase_Admin(), NIMUtils.CASE_USER_ADMIN, 3));
                this.resetAlertSkips_CaseNotApproved();
            }
            return Case_Admin;
        }

        public void setCase_Admin(bltUserAccount case_Admin)
        {
            this.Case_Admin = case_Admin;
            this.Case_Admin.setUniqueModifyComments(this.getUniqueModifyComments());
        }

        public bltUserAccount getCase_NurseCaseManager()
        {
            if (Case_NurseCaseManager==null&&this.getNIM3_CaseAccount().getNurseCaseManagerID()>0)
            {
                setCase_NurseCaseManager(new bltUserAccount(this.getNIM3_CaseAccount().getNurseCaseManagerID()));
                this.setAlertSkip_NCM(NIMUtils.isAlertSkip(this.getCase_NurseCaseManager(), NIMUtils.CASE_USER_NURSE_CASE_MANAGER, 1));
                this.setAlert2Skip_NCM(NIMUtils.isAlertSkip(this.getCase_NurseCaseManager(), NIMUtils.CASE_USER_NURSE_CASE_MANAGER, 2));
                this.setReportSkip_NCM(NIMUtils.isAlertSkip(this.getCase_NurseCaseManager(), NIMUtils.CASE_USER_NURSE_CASE_MANAGER, 3));
                this.resetAlertSkips_CaseNotApproved();
            }
            return Case_NurseCaseManager;
        }

        public void setCase_NurseCaseManager(bltUserAccount case_NurseCaseManager)
        {
            this.Case_NurseCaseManager = case_NurseCaseManager;
            this.Case_NurseCaseManager.setUniqueModifyComments(this.getUniqueModifyComments());
        }


        public bltUserAccount getCase_Admin2()
        {
            if (this.Case_Admin2==null&&this.getNIM3_CaseAccount().getCaseAdministrator2ID()>0)
            {
                setCase_Admin2(new bltUserAccount(this.getNIM3_CaseAccount().getCaseAdministrator2ID()));
                this.setAlertSkip_Adm2(NIMUtils.isAlertSkip(this.getCase_Admin2(), NIMUtils.CASE_USER_ADMIN2, 1));
                this.setAlert2Skip_Adm2(NIMUtils.isAlertSkip(this.getCase_Admin2(), NIMUtils.CASE_USER_ADMIN2, 2));
                this.setReportSkip_Adm2(NIMUtils.isAlertSkip(this.getCase_Admin2(), NIMUtils.CASE_USER_ADMIN2, 3));
                this.resetAlertSkips_CaseNotApproved();
            }
            return Case_Admin2;
        }

        public void setCase_Admin2(bltUserAccount case_Admin2)
        {
            this.Case_Admin2 = case_Admin2;
            this.Case_Admin2.setUniqueModifyComments(this.getUniqueModifyComments());
        }

        public bltUserAccount getCase_Admin3()
        {
            if (Case_Admin3==null&&this.getNIM3_CaseAccount().getCaseAdministrator3ID()>0)
            {
                setCase_Admin3(new bltUserAccount(this.getNIM3_CaseAccount().getCaseAdministrator3ID()));
                this.setAlertSkip_Adm3(NIMUtils.isAlertSkip(this.getCase_Admin3(), NIMUtils.CASE_USER_ADMIN3, 1));
                this.setAlert2Skip_Adm3(NIMUtils.isAlertSkip(this.getCase_Admin3(), NIMUtils.CASE_USER_ADMIN3, 2));
                this.setReportSkip_Adm3(NIMUtils.isAlertSkip(this.getCase_Admin3(), NIMUtils.CASE_USER_ADMIN3, 3));
                this.resetAlertSkips_CaseNotApproved();
            }
            return Case_Admin3;
        }

        public void setCase_Admin3(bltUserAccount case_Admin3)
        {
            this.Case_Admin3 = case_Admin3;
            this.Case_Admin3.setUniqueModifyComments(this.getUniqueModifyComments());
        }

        public bltUserAccount getCase_Admin4()
        {
            if (Case_Admin4==null&&this.getNIM3_CaseAccount().getCaseAdministrator4ID()>0)
            {
                setCase_Admin4(new bltUserAccount(this.getNIM3_CaseAccount().getCaseAdministrator4ID()));
                this.setAlertSkip_Adm4(NIMUtils.isAlertSkip(this.getCase_Admin4(), NIMUtils.CASE_USER_ADMIN4, 1));
                this.setAlert2Skip_Adm4(NIMUtils.isAlertSkip(this.getCase_Admin4(), NIMUtils.CASE_USER_ADMIN4, 2));
                this.setReportSkip_Adm4(NIMUtils.isAlertSkip(this.getCase_Admin4(), NIMUtils.CASE_USER_ADMIN4, 3));
                this.resetAlertSkips_CaseNotApproved();
            }
            return Case_Admin4;
        }

        public void setCase_Admin4(bltUserAccount case_Admin4)
        {
            this.Case_Admin4 = case_Admin4;
            this.Case_Admin4.setUniqueModifyComments(this.getUniqueModifyComments());
        }

        public bltUserAccount getCase_AssignedTo()
        {
            if (Case_AssignedTo==null)
            {
                setCase_AssignedTo(new bltUserAccount(this.getNIM3_CaseAccount().getAssignedToID()));
            }
            return Case_AssignedTo;
        }




        public bltUserAccount getReferral_ReferringDoctor()
        {
            if (Referral_ReferringDoctor==null)
            {
                setReferral_ReferringDoctor(new bltUserAccount(this.getNIM3_Referral().getReferringPhysicianID()));
                this.setAlertSkip_RefDr(NIMUtils.isAlertSkip(this.getReferral_ReferringDoctor(), NIMUtils.CASE_USER_REFERRING_DOCTOR, 1));
                this.setAlert2Skip_RefDr(NIMUtils.isAlertSkip(this.getReferral_ReferringDoctor(), NIMUtils.CASE_USER_REFERRING_DOCTOR, 2));
                //isReportSkip_RefDr(NIMUtils.isAlertSkip(this.getReferral_ReferringDoctor(), NIMUtils.CASE_USER_REFERRING_DOCTOR, 3));
                this.setReportSkip_RefDr(false);
                this.resetAlertSkips_CaseNotApproved();
            }
            return Referral_ReferringDoctor;
        }

        public void setReferral_ReferringDoctor(bltUserAccount referral_ReferringDoctor)
        {
            this.Referral_ReferringDoctor = referral_ReferringDoctor;
            this.Referral_ReferringDoctor.setUniqueModifyComments(this.getUniqueModifyComments());
        }

        public bltNIM3_Service_List getNIM3_Service_List()
        {
            if (NIM3_Service_List==null)
            {
                NIM3_Service_List = new bltNIM3_Service_List(this.getNIM3_Encounter().getEncounterID(),"","ServiceID");
            }
            return NIM3_Service_List;
        }

        public void setNIM3_Service_List(bltNIM3_Service_List NIM3_Service_List)
        {
            this.NIM3_Service_List = NIM3_Service_List;
        }


        private String UniqueModifyComments = "[Not-Set]";

        private bltNIM3_CaseAccount NIM3_CaseAccount = null;
        private bltNIM3_Referral NIM3_Referral= null;
        private bltNIM3_Encounter NIM3_Encounter = null;
        private bltNIM3_Appointment NIM3_Appointment = null;
        private bltNIM3_PayerMaster NIM3_PayerMaster = null;
        private bltNIM3_PayerMaster NIM3_ParentPayerMaster = null;


        private bltNIM3_Service_List        NIM3_Service_List        =    null;

        private bltPracticeMaster Appointment_PracticeMaster = null;

        private bltUserAccount Case_Adjuster = null;
        private bltUserAccount Case_Admin = null;
        private bltUserAccount Case_BillTo = null;
        private bltUserAccount Case_Admin2 = null;
        private bltUserAccount Case_Admin3 = null;
        private bltUserAccount Case_Admin4 = null;
        private bltUserAccount Case_NurseCaseManager = null;
        private bltUserAccount Referral_ReferringDoctor = null;

        private bltNIM3_PayerMaster Case_AssignedTo_Payer = null;
        private bltUserAccount Case_AssignedTo = null;


        //pulling logic into central method
        private boolean isAlertSkip_Adj = true;
        private boolean isAlertSkip_Adm = true;
        private boolean isAlertSkip_Adm2 = true;
        private boolean isAlertSkip_Adm3 = true;
        private boolean isAlertSkip_Adm4 = true;
        private boolean isAlertSkip_NCM = true;
        private boolean isAlertSkip_RefDr = true;

        private boolean isAlert2Skip_Adj = true;
        private boolean isAlert2Skip_Adm = true;
        private boolean isAlert2Skip_Adm2 = true;
        private boolean isAlert2Skip_Adm3 = true;
        private boolean isAlert2Skip_Adm4 = true;
        private boolean isAlert2Skip_NCM = true;
        private boolean isAlert2Skip_RefDr = true;

        //need to repeat logic
        private boolean isReportSkip_Adj = true;
        private boolean isReportSkip_Adm = true;
        private boolean isReportSkip_Adm2 = true;
        private boolean isReportSkip_Adm3 = true;
        private boolean isReportSkip_Adm4 = true;
        private boolean isReportSkip_NCM = true;
        private boolean isReportSkip_RefDr = true;

        public boolean isAlertSkip_Adj()
        {
            this.getCase_Adjuster();
            return isAlertSkip_Adj;
        }

        public void setAlertSkip_Adj(boolean alertSkip_Adj)
        {
            isAlertSkip_Adj = alertSkip_Adj;
        }

        public boolean isAlertSkip_Adm()
        {
            this.getCase_Admin();
            return isAlertSkip_Adm;
        }

        public void setAlertSkip_Adm(boolean alertSkip_Adm)
        {
            isAlertSkip_Adm = alertSkip_Adm;
        }

        public boolean isAlertSkip_Adm2()
        {
            this.getCase_Admin2();
            return isAlertSkip_Adm2;
        }

        public void setAlertSkip_Adm2(boolean alertSkip_Adm2)
        {
            isAlertSkip_Adm2 = alertSkip_Adm2;
        }

        public boolean isAlertSkip_Adm3()
        {
            this.getCase_Admin3();
            return isAlertSkip_Adm3;
        }

        public void setAlertSkip_Adm3(boolean alertSkip_Adm3)
        {
            isAlertSkip_Adm3 = alertSkip_Adm3;
        }

        public boolean isAlertSkip_Adm4()
        {
            this.getCase_Admin4();
            return isAlertSkip_Adm4;
        }

        public void setAlertSkip_Adm4(boolean alertSkip_Adm4)
        {
            isAlertSkip_Adm4 = alertSkip_Adm4;
        }

        public boolean isAlertSkip_NCM()
        {
            this.getCase_NurseCaseManager();
            return isAlertSkip_NCM;
        }

        public void setAlertSkip_NCM(boolean alertSkip_NCM)
        {
            isAlertSkip_NCM = alertSkip_NCM;
        }

        public boolean isAlertSkip_RefDr()
        {
            this.getReferral_ReferringDoctor();
            return isAlertSkip_RefDr;
        }

        public void setAlertSkip_RefDr(boolean alertSkip_RefDr)
        {
            isAlertSkip_RefDr = alertSkip_RefDr;
        }

        public boolean isAlert2Skip_Adj()
        {
            this.getCase_Adjuster();
            return isAlert2Skip_Adj;
        }

        public void setAlert2Skip_Adj(boolean alert2Skip_Adj)
        {
            isAlert2Skip_Adj = alert2Skip_Adj;
        }

        public boolean isAlert2Skip_Adm()
        {
            this.getCase_Admin();
            return isAlert2Skip_Adm;
        }

        public void setAlert2Skip_Adm(boolean alert2Skip_Adm)
        {
            isAlert2Skip_Adm = alert2Skip_Adm;
        }

        public boolean isAlert2Skip_Adm2()
        {
            this.getCase_Admin2();
            return isAlert2Skip_Adm2;
        }

        public void setAlert2Skip_Adm2(boolean alert2Skip_Adm2)
        {
            isAlert2Skip_Adm2 = alert2Skip_Adm2;
        }

        public boolean isAlert2Skip_Adm3()
        {
            this.getCase_Admin3();
            return isAlert2Skip_Adm3;
        }

        public void setAlert2Skip_Adm3(boolean alert2Skip_Adm3)
        {
            isAlert2Skip_Adm3 = alert2Skip_Adm3;
        }

        public boolean isAlert2Skip_Adm4()
        {
            this.getCase_Admin4();
            return isAlert2Skip_Adm4;
        }

        public void setAlert2Skip_Adm4(boolean alert2Skip_Adm4)
        {
            isAlert2Skip_Adm4 = alert2Skip_Adm4;
        }

        public boolean isAlert2Skip_NCM()
        {
            this.getCase_NurseCaseManager();
            return isAlert2Skip_NCM;
        }

        public void setAlert2Skip_NCM(boolean alert2Skip_NCM)
        {
            isAlert2Skip_NCM = alert2Skip_NCM;
        }

        public boolean isAlert2Skip_RefDr()
        {
            this.getReferral_ReferringDoctor();
            return isAlert2Skip_RefDr;
        }

        public void setAlert2Skip_RefDr(boolean alert2Skip_RefDr)
        {
            isAlert2Skip_RefDr = alert2Skip_RefDr;
        }

        public boolean isReportSkip_Adj()
        {
            this.getCase_Adjuster();
            return isReportSkip_Adj;
        }

        public void setReportSkip_Adj(boolean reportSkip_Adj)
        {
            isReportSkip_Adj = reportSkip_Adj;
        }

        public boolean isReportSkip_Adm()
        {
            this.getCase_Admin();
            return isReportSkip_Adm;
        }

        public void setReportSkip_Adm(boolean reportSkip_Adm)
        {
            isReportSkip_Adm = reportSkip_Adm;
        }

        public boolean isReportSkip_Adm2()
        {
            this.getCase_Admin2();
            return isReportSkip_Adm2;
        }

        public void setReportSkip_Adm2(boolean reportSkip_Adm2)
        {
            isReportSkip_Adm2 = reportSkip_Adm2;
        }

        public boolean isReportSkip_Adm3()
        {
            this.getCase_Admin3();
            return isReportSkip_Adm3;
        }

        public void setReportSkip_Adm3(boolean reportSkip_Adm3)
        {
            isReportSkip_Adm3 = reportSkip_Adm3;
        }

        public boolean isReportSkip_Adm4()
        {
            this.getCase_Admin4();
            return isReportSkip_Adm4;
        }

        public void setReportSkip_Adm4(boolean reportSkip_Adm4)
        {
            isReportSkip_Adm4 = reportSkip_Adm4;
        }

        public boolean isReportSkip_NCM()
        {
            this.getCase_NurseCaseManager();
            return isReportSkip_NCM;
        }

        public void setReportSkip_NCM(boolean reportSkip_NCM)
        {
            isReportSkip_NCM = reportSkip_NCM;
        }

        public boolean isReportSkip_RefDr()
        {
            this.getReferral_ReferringDoctor();
            return isReportSkip_RefDr;
        }

        public void setReportSkip_RefDr(boolean reportSkip_RefDr)
        {
            isReportSkip_RefDr = reportSkip_RefDr;
        }

        public String getPracticeBillingTable(boolean isFull){
            String myVal = "No Practice Selected";
            if (this.getAppointment_PracticeMaster()!=null){
                myVal = "<table cellpadding=\"5\" cellspacing=\"0\" border=\"0\"><tr><td>" +
                        "<strong>"+this.getAppointment_PracticeMaster().getPracticeName()+" ("+this.getAppointment_PracticeMaster().getPracticeID()+")</strong><br>" +
                        "<strong>"+this.getAppointment_PracticeMaster().getOfficeCity()+", "+new bltStateLI(this.getAppointment_PracticeMaster().getOfficeStateID()).getShortState()+" "+this.getAppointment_PracticeMaster().getOfficeZIP()+"</strong><br>" +
                        "<br>TIN: <strong>"+this.getAppointment_PracticeMaster().getOfficeFederalTaxID()+"</strong><br>" +
                        "Status: <strong>"+new bltPracticeContractStatusLI(this.getAppointment_PracticeMaster().getContractingStatusID()).getDescriptionLong()+"</strong><br>" +
                        "BU FSID: <strong>"+this.getAppointment_PracticeMaster().getFeeScheduleRefID()+"</strong><br>" +
                        "BU FS %: <strong>"+PLCUtils.getDisplayDefaultDecimalFormatCurrency(this.getAppointment_PracticeMaster().getFeePercentage())+"</strong>" +
                        "</td><td>";

                myVal += "<table cellpadding=\"2\" cellspacing=\"0\" border=\"1\">";
                myVal += "<tr><th>Modality</th><th>Global</th><th>26</th><th>TC</th>";
                myVal += "<tr><td>mr_wo</td> <td><strong>$"+PLCUtils.getDisplayDefaultDecimalFormatCurrency(this.getAppointment_PracticeMaster().getPrice_Mod_MRI_WO())+"</strong></td><td><strong>$"+PLCUtils.getDisplayDefaultDecimalFormatCurrency(this.getAppointment_PracticeMaster().getPrice_Mod_MRI_WO_26())+"</strong></td><td><strong>$"+PLCUtils.getDisplayDefaultDecimalFormatCurrency(this.getAppointment_PracticeMaster().getPrice_Mod_MRI_WO_TC())+"</strong></td></tr>";
                myVal += "<tr><td>mr_w</td> <td><strong>$"+PLCUtils.getDisplayDefaultDecimalFormatCurrency(this.getAppointment_PracticeMaster().getPrice_Mod_MRI_W())+"</strong></td> <td><strong>$"+PLCUtils.getDisplayDefaultDecimalFormatCurrency(this.getAppointment_PracticeMaster().getPrice_Mod_MRI_W_26())+"</strong></td> <td><strong>$"+PLCUtils.getDisplayDefaultDecimalFormatCurrency(this.getAppointment_PracticeMaster().getPrice_Mod_MRI_W_TC())+"</strong></td></tr>";
                myVal += "<tr><td>mr_wwo</td> <td><strong>$"+PLCUtils.getDisplayDefaultDecimalFormatCurrency(this.getAppointment_PracticeMaster().getPrice_Mod_MRI_WWO())+"</strong></td> <td><strong>$"+PLCUtils.getDisplayDefaultDecimalFormatCurrency(this.getAppointment_PracticeMaster().getPrice_Mod_MRI_WWO_26())+"</strong></td> <td><strong>$"+PLCUtils.getDisplayDefaultDecimalFormatCurrency(this.getAppointment_PracticeMaster().getPrice_Mod_MRI_WWO_TC())+"</strong></td></tr>";
                myVal += "<tr><td>ct_wo</td> <td><strong>$"+PLCUtils.getDisplayDefaultDecimalFormatCurrency(this.getAppointment_PracticeMaster().getPrice_Mod_CT_WO())+"</strong></td> <td><strong>$"+PLCUtils.getDisplayDefaultDecimalFormatCurrency(this.getAppointment_PracticeMaster().getPrice_Mod_CT_WO_26())+"</strong></td> <td><strong>$"+PLCUtils.getDisplayDefaultDecimalFormatCurrency(this.getAppointment_PracticeMaster().getPrice_Mod_CT_WO_TC())+"</strong></td></tr>";
                myVal += "<tr><td>ct_w</td> <td><strong>$"+PLCUtils.getDisplayDefaultDecimalFormatCurrency(this.getAppointment_PracticeMaster().getPrice_Mod_CT_W())+"</strong></td><td><strong>$"+PLCUtils.getDisplayDefaultDecimalFormatCurrency(this.getAppointment_PracticeMaster().getPrice_Mod_CT_W_26())+"</strong></td><td><strong>$"+PLCUtils.getDisplayDefaultDecimalFormatCurrency(this.getAppointment_PracticeMaster().getPrice_Mod_CT_W_TC())+"</strong></td></tr>";
                myVal += "<tr><td>ct_wwo</td> <td><strong>$"+PLCUtils.getDisplayDefaultDecimalFormatCurrency(this.getAppointment_PracticeMaster().getPrice_Mod_CT_WWO())+"</strong></td> <td><strong>$"+PLCUtils.getDisplayDefaultDecimalFormatCurrency(this.getAppointment_PracticeMaster().getPrice_Mod_CT_WWO_26())+"</strong></td> <td><strong>$"+PLCUtils.getDisplayDefaultDecimalFormatCurrency(this.getAppointment_PracticeMaster().getPrice_Mod_CT_WWO_TC())+"</strong></td></tr>";
                myVal += "</table>";
                myVal += "</td></tr></table>";
            }

            return myVal;
        }


        public void synchronizeBilling_One_to_Two(){
            String cTAG_v = "v1.0";
            String cTAG = TAG + "SynchronizeBilling_One_to_Two:" + cTAG_v + ":";
            DebugLogger.i(cTAG,"Running Start");
            java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);

            //Parse through every service level item and make sure the Billing 2.0 fields sync with the data in Billing 1.0 fields
            //Some values are not in Billing 1.0 - these should be set first through runnign a Billing 2.0 Recalc.  We are not going to run that here is the purpose of this is to sync data from 1.0 to 2.0
            try{
                bltNIM3_Service        working_bltNIM3_Service;
                ListElement         leCurrentElement_Service;
                NIM3_Service_List = new bltNIM3_Service_List(this.getNIM3_Encounter().getEncounterID(),"","ServiceID");
                java.util.Enumeration eList_Service = NIM3_Service_List.elements();
                int temp_ServiceCount = 0;
                while (eList_Service.hasMoreElements())
                {
                    temp_ServiceCount++;
                    DebugLogger.i(cTAG,"Running Service #" + temp_ServiceCount);
                    leCurrentElement_Service    = (ListElement) eList_Service.nextElement();
                    working_bltNIM3_Service  = (bltNIM3_Service) leCurrentElement_Service.getObject();
                    //service status = 1 means that it's not void
                    if (working_bltNIM3_Service.getServiceStatusID()==1)
                    {
                        //First sync all QTY Fields
                        working_bltNIM3_Service.setCPTQty_Bill(working_bltNIM3_Service.getCPTQty());
                        working_bltNIM3_Service.setCPTQty_Pay(working_bltNIM3_Service.getCPTQty());

                        //Sync Reference Data (aka worksheet data).  In Billing 1.0 values are the total (for all QTYs) and in billing 2.0 reference data is the unit value
                        working_bltNIM3_Service.setReference_PayerContractAmount(working_bltNIM3_Service.getAllowAmount()/working_bltNIM3_Service.getCPTQty());
                        working_bltNIM3_Service.setReference_PayerBillAmount(working_bltNIM3_Service.getBillAmount()/working_bltNIM3_Service.getCPTQty());

                        //WARNING: We are pulling "vague" data and tryign to put it "detailed" data and there's no way to do this really.  So we are putting all the data in the first service item only
                        if (temp_ServiceCount==1){
                            working_bltNIM3_Service.setReference_ProviderBilledAmount(this.getNIM3_Encounter().getAmountBilledByProvider()/working_bltNIM3_Service.getCPTQty());
                        }
                        //getPaidOutAmount is really the Expected to Pay Out amount; The Actual Paid Out amount is done via checks at the Encounter Level in 1.0
                        working_bltNIM3_Service.setReference_ProviderContractAmount(working_bltNIM3_Service.getPaidOutAmount()/working_bltNIM3_Service.getCPTQty());

                        //the above should cover all reference data that can be synchronized.   FeeSchedule and U&C are not stored in 1.0 and thus are not sync'd here.
                        working_bltNIM3_Service.commitData();

                        //Next Steps: Now we need to take the "actual Paid Out" and "Received" & "Received Adjustments" and generate billing transactions accordingly
                        java.util.Date temp_CreateDate = new java.util.Date();
                        //WARNING: first we must delete all existing transactions for this service item.
                        bltNIM3_ServiceBillingTransaction_List        bltNIM3_ServiceBillingTransaction_List        =    new    bltNIM3_ServiceBillingTransaction_List(working_bltNIM3_Service.getServiceID());
                        //declaration of Enumeration
                        bltNIM3_ServiceBillingTransaction        working_bltNIM3_ServiceBillingTransaction;
                        ListElement         leCurrentElement_ServiceBillingTransaction;
                        Enumeration eList_ServiceBillingTransaction = bltNIM3_ServiceBillingTransaction_List.elements();
                        if (eList_ServiceBillingTransaction!=null&&eList_ServiceBillingTransaction.hasMoreElements())
                        {
                             while (eList_ServiceBillingTransaction.hasMoreElements())
                             {
                                 leCurrentElement_ServiceBillingTransaction    = (ListElement) eList_ServiceBillingTransaction.nextElement();
                                 working_bltNIM3_ServiceBillingTransaction  = (bltNIM3_ServiceBillingTransaction) leCurrentElement_ServiceBillingTransaction.getObject();
                                 //disassociates the item
                                 working_bltNIM3_ServiceBillingTransaction.setServiceID(0);
                                 working_bltNIM3_ServiceBillingTransaction.setUniqueModifyComments("removed during sync");
                                 working_bltNIM3_ServiceBillingTransaction.setUniqueModifyDate(temp_CreateDate);
                                 working_bltNIM3_ServiceBillingTransaction.commitData();
                             }
                        }

                        // only run this if the case has been billed:
                        
                        if (this.getNIM3_Encounter().getSentTo_Bill_Pay().after(NIMUtils.getBeforeTime())){
                            //create AR Record
                            bltNIM3_ServiceBillingTransaction        temp_bltNIM3_ServiceBillingTransaction = new bltNIM3_ServiceBillingTransaction();
                            temp_bltNIM3_ServiceBillingTransaction.setComments(cTAG + displayDateSDF1.format(temp_CreateDate));
                            temp_bltNIM3_ServiceBillingTransaction.setDetail_Item_Qty(working_bltNIM3_Service.getCPTQty_Bill());
                            temp_bltNIM3_ServiceBillingTransaction.setServiceID(working_bltNIM3_Service.getServiceID());
                            temp_bltNIM3_ServiceBillingTransaction.setUniqueCreateDate(temp_CreateDate);
                            //since we are using Billing 2.0 Reference data we must convert unit to sub-total using QTY
                            temp_bltNIM3_ServiceBillingTransaction.setTransactionAmount(working_bltNIM3_Service.getReference_PayerContractAmount() * working_bltNIM3_Service.getCPTQty_Bill());
                            temp_bltNIM3_ServiceBillingTransaction.setTransactionTitle("AR / Bill to Payer");
                            //using the originaly date that we sent the bill to the payer
                            //Note: setBilledDate() is not used as this is the date we send to eBilling if applicable.
                            temp_bltNIM3_ServiceBillingTransaction.setReferenceDate(this.getNIM3_Encounter().getSentTo_Bill_Pay());
                            temp_bltNIM3_ServiceBillingTransaction.setServiceBillingTransactionTypeID(NIM3_BillingObject2.REVENUE_TYPE);
                            temp_bltNIM3_ServiceBillingTransaction.commitData();

                            //create AR - Adjustment Record
                            if (working_bltNIM3_Service.getAllowAmountAdjustment()>0.0){
                                temp_bltNIM3_ServiceBillingTransaction = new bltNIM3_ServiceBillingTransaction();
                                temp_bltNIM3_ServiceBillingTransaction.setComments(cTAG + displayDateSDF1.format(temp_CreateDate));
                                temp_bltNIM3_ServiceBillingTransaction.setDetail_Item_Qty(working_bltNIM3_Service.getCPTQty_Bill());
                                temp_bltNIM3_ServiceBillingTransaction.setServiceID(working_bltNIM3_Service.getServiceID());
                                temp_bltNIM3_ServiceBillingTransaction.setUniqueCreateDate(temp_CreateDate);
                                //using Billing 1.0 field: no need to adjust for QTY here
                                temp_bltNIM3_ServiceBillingTransaction.setTransactionAmount(working_bltNIM3_Service.getAllowAmountAdjustment());
                                temp_bltNIM3_ServiceBillingTransaction.setTransactionTitle("AR:Adj / Bill to Payer");
                                //using the originaly date that we sent the bill to the payer.  This is an inherent flaw in Billing 1.0 in that adjustment dates are not tracked.
                                temp_bltNIM3_ServiceBillingTransaction.setReferenceDate(this.getNIM3_Encounter().getSentTo_Bill_Pay());
                                temp_bltNIM3_ServiceBillingTransaction.setServiceBillingTransactionTypeID(NIM3_BillingObject2.REVENUEADJUSTMENT_TYPE);
                                temp_bltNIM3_ServiceBillingTransaction.commitData();
                            }

                            //create Received Payment (could be up to 3 payments per service)
                            if (working_bltNIM3_Service.getReceivedCheck1Amount()>0.0){
                                temp_bltNIM3_ServiceBillingTransaction = new bltNIM3_ServiceBillingTransaction();
                                temp_bltNIM3_ServiceBillingTransaction.setComments(cTAG + displayDateSDF1.format(temp_CreateDate));
                                temp_bltNIM3_ServiceBillingTransaction.setServiceID(working_bltNIM3_Service.getServiceID());
                                temp_bltNIM3_ServiceBillingTransaction.setUniqueCreateDate(temp_CreateDate);
                                temp_bltNIM3_ServiceBillingTransaction.setTransactionAmount(working_bltNIM3_Service.getReceivedCheck1Amount());
                                temp_bltNIM3_ServiceBillingTransaction.setReferenceNumber(working_bltNIM3_Service.getReceivedCheck1Number());
                                temp_bltNIM3_ServiceBillingTransaction.setTransactionTitle("Received Payment/Check 1");
                                temp_bltNIM3_ServiceBillingTransaction.setReferenceDate(working_bltNIM3_Service.getReceivedCheck1Date());
                                temp_bltNIM3_ServiceBillingTransaction.setServiceBillingTransactionTypeID(NIM3_BillingObject2.ARPAYMENT_TYPE);
                                temp_bltNIM3_ServiceBillingTransaction.commitData();
                            }
                            if (working_bltNIM3_Service.getReceivedCheck2Amount()>0.0){
                                temp_bltNIM3_ServiceBillingTransaction = new bltNIM3_ServiceBillingTransaction();
                                temp_bltNIM3_ServiceBillingTransaction.setComments(cTAG + displayDateSDF1.format(temp_CreateDate));
                                temp_bltNIM3_ServiceBillingTransaction.setServiceID(working_bltNIM3_Service.getServiceID());
                                temp_bltNIM3_ServiceBillingTransaction.setUniqueCreateDate(temp_CreateDate);
                                temp_bltNIM3_ServiceBillingTransaction.setTransactionAmount(working_bltNIM3_Service.getReceivedCheck2Amount());
                                temp_bltNIM3_ServiceBillingTransaction.setReferenceNumber(working_bltNIM3_Service.getReceivedCheck2Number());
                                temp_bltNIM3_ServiceBillingTransaction.setTransactionTitle("Received Payment/Check 2");
                                temp_bltNIM3_ServiceBillingTransaction.setReferenceDate(working_bltNIM3_Service.getReceivedCheck2Date());
                                temp_bltNIM3_ServiceBillingTransaction.setServiceBillingTransactionTypeID(NIM3_BillingObject2.ARPAYMENT_TYPE);
                                temp_bltNIM3_ServiceBillingTransaction.commitData();
                            }
                            if (working_bltNIM3_Service.getReceivedCheck3Amount()>0.0){
                                temp_bltNIM3_ServiceBillingTransaction = new bltNIM3_ServiceBillingTransaction();
                                temp_bltNIM3_ServiceBillingTransaction.setComments(cTAG + displayDateSDF1.format(temp_CreateDate));
                                temp_bltNIM3_ServiceBillingTransaction.setServiceID(working_bltNIM3_Service.getServiceID());
                                temp_bltNIM3_ServiceBillingTransaction.setUniqueCreateDate(temp_CreateDate);
                                temp_bltNIM3_ServiceBillingTransaction.setTransactionAmount(working_bltNIM3_Service.getReceivedCheck3Amount());
                                temp_bltNIM3_ServiceBillingTransaction.setReferenceNumber(working_bltNIM3_Service.getReceivedCheck3Number());
                                temp_bltNIM3_ServiceBillingTransaction.setTransactionTitle("Received Payment/Check 3");
                                temp_bltNIM3_ServiceBillingTransaction.setReferenceDate(working_bltNIM3_Service.getReceivedCheck3Date());
                                temp_bltNIM3_ServiceBillingTransaction.setServiceBillingTransactionTypeID(NIM3_BillingObject2.ARPAYMENT_TYPE);
                                temp_bltNIM3_ServiceBillingTransaction.commitData();
                            }

                            //create AP Record
                            temp_bltNIM3_ServiceBillingTransaction = new bltNIM3_ServiceBillingTransaction();
                            temp_bltNIM3_ServiceBillingTransaction.setComments(cTAG + displayDateSDF1.format(temp_CreateDate));
                            temp_bltNIM3_ServiceBillingTransaction.setDetail_Item_Qty(working_bltNIM3_Service.getCPTQty_Pay());
                            temp_bltNIM3_ServiceBillingTransaction.setServiceID(working_bltNIM3_Service.getServiceID());
                            temp_bltNIM3_ServiceBillingTransaction.setUniqueCreateDate(temp_CreateDate);
                            //since we are using Billing 2.0 Reference data we must convert unit to sub-total using QTY
                            temp_bltNIM3_ServiceBillingTransaction.setTransactionAmount(working_bltNIM3_Service.getReference_ProviderContractAmount() * working_bltNIM3_Service.getCPTQty_Pay());
                            temp_bltNIM3_ServiceBillingTransaction.setTransactionTitle("AP/ Expectd to Provider");
                            //Currently we are using the date we bill to be the date we also incur AP, thus using the originaly date that we sent the bill to the payer
                            //Note: setBilledDate() is not used as this is the date we send to eBilling if applicable.
                            temp_bltNIM3_ServiceBillingTransaction.setReferenceDate(this.getNIM3_Encounter().getSentTo_Bill_Pay());
                            temp_bltNIM3_ServiceBillingTransaction.setServiceBillingTransactionTypeID(NIM3_BillingObject2.COS_TYPE);
                            temp_bltNIM3_ServiceBillingTransaction.commitData();

                            //create Check Paid
                            //WARNING: Check Paid was done at the encounter level and thus we have no idea what items it applies to.  For this reason we just have to put it all in service item 1
                            if (temp_ServiceCount==1){
                                if (this.getNIM3_Encounter().getPaidToProviderCheck1Amount()>0.0){
                                    temp_bltNIM3_ServiceBillingTransaction = new bltNIM3_ServiceBillingTransaction();
                                    temp_bltNIM3_ServiceBillingTransaction.setComments(cTAG + displayDateSDF1.format(temp_CreateDate));
                                    temp_bltNIM3_ServiceBillingTransaction.setServiceID(working_bltNIM3_Service.getServiceID());
                                    temp_bltNIM3_ServiceBillingTransaction.setUniqueCreateDate(temp_CreateDate);
                                    //since we are using Billing 2.0 Reference data we must convert unit to sub-total using QTY
                                    temp_bltNIM3_ServiceBillingTransaction.setTransactionAmount(this.getNIM3_Encounter().getPaidToProviderCheck1Amount());
                                    temp_bltNIM3_ServiceBillingTransaction.setReferenceNumber(this.getNIM3_Encounter().getPaidToProviderCheck1Number());
                                    temp_bltNIM3_ServiceBillingTransaction.setTransactionTitle("AP/ Payment to Provider");
                                    temp_bltNIM3_ServiceBillingTransaction.setReferenceDate(this.getNIM3_Encounter().getPaidToProviderCheck1Date());
                                    temp_bltNIM3_ServiceBillingTransaction.setServiceBillingTransactionTypeID(NIM3_BillingObject2.APPAYMENT_TYPE);
                                    temp_bltNIM3_ServiceBillingTransaction.commitData();
                                }
                                if (this.getNIM3_Encounter().getPaidToProviderCheck2Amount()>0.0){
                                    temp_bltNIM3_ServiceBillingTransaction = new bltNIM3_ServiceBillingTransaction();
                                    temp_bltNIM3_ServiceBillingTransaction.setComments(cTAG + displayDateSDF1.format(temp_CreateDate));
                                    temp_bltNIM3_ServiceBillingTransaction.setServiceID(working_bltNIM3_Service.getServiceID());
                                    temp_bltNIM3_ServiceBillingTransaction.setUniqueCreateDate(temp_CreateDate);
                                    //since we are using Billing 2.0 Reference data we must convert unit to sub-total using QTY
                                    temp_bltNIM3_ServiceBillingTransaction.setTransactionAmount(this.getNIM3_Encounter().getPaidToProviderCheck2Amount());
                                    temp_bltNIM3_ServiceBillingTransaction.setReferenceNumber(this.getNIM3_Encounter().getPaidToProviderCheck2Number());
                                    temp_bltNIM3_ServiceBillingTransaction.setTransactionTitle("AP/ Payment to Provider");
                                    temp_bltNIM3_ServiceBillingTransaction.setReferenceDate(this.getNIM3_Encounter().getPaidToProviderCheck2Date());
                                    temp_bltNIM3_ServiceBillingTransaction.setServiceBillingTransactionTypeID(NIM3_BillingObject2.APPAYMENT_TYPE);
                                    temp_bltNIM3_ServiceBillingTransaction.commitData();
                                }
                                if (this.getNIM3_Encounter().getPaidToProviderCheck3Amount()>0.0){
                                    temp_bltNIM3_ServiceBillingTransaction = new bltNIM3_ServiceBillingTransaction();
                                    temp_bltNIM3_ServiceBillingTransaction.setComments(cTAG + displayDateSDF1.format(temp_CreateDate));
                                    temp_bltNIM3_ServiceBillingTransaction.setServiceID(working_bltNIM3_Service.getServiceID());
                                    temp_bltNIM3_ServiceBillingTransaction.setUniqueCreateDate(temp_CreateDate);
                                    //since we are using Billing 2.0 Reference data we must convert unit to sub-total using QTY
                                    temp_bltNIM3_ServiceBillingTransaction.setTransactionAmount(this.getNIM3_Encounter().getPaidToProviderCheck3Amount());
                                    temp_bltNIM3_ServiceBillingTransaction.setReferenceNumber(this.getNIM3_Encounter().getPaidToProviderCheck3Number());
                                    temp_bltNIM3_ServiceBillingTransaction.setTransactionTitle("AP/ Payment to Provider");
                                    temp_bltNIM3_ServiceBillingTransaction.setReferenceDate(this.getNIM3_Encounter().getPaidToProviderCheck3Date());
                                    temp_bltNIM3_ServiceBillingTransaction.setServiceBillingTransactionTypeID(NIM3_BillingObject2.APPAYMENT_TYPE);
                                    temp_bltNIM3_ServiceBillingTransaction.commitData();
                                }
                            }
                        }
                    }
                }
            } catch (Exception eee){
                DebugLogger.e(TAG,eee.toString());
            }


        }

        public NIM3_BillingObject2 getBillingTotals2()
        {
            NIM3_BillingObject2 myBO2 = new NIM3_BillingObject2();
            try{
                bltNIM3_Service        working_bltNIM3_Service;
                ListElement         leCurrentElement_Service;
                NIM3_Service_List = new bltNIM3_Service_List(this.getNIM3_Encounter().getEncounterID(),"","ServiceID");
                java.util.Enumeration eList_Service = NIM3_Service_List.elements();
                while (eList_Service.hasMoreElements())
                {
                    leCurrentElement_Service    = (ListElement) eList_Service.nextElement();
                    working_bltNIM3_Service  = (bltNIM3_Service) leCurrentElement_Service.getObject();
                    if (working_bltNIM3_Service.getServiceStatusID()==1)
                    {
                        //Billing Object Records must be done by SubTotals with corresponding QTY and not the unit values
                        myBO2.Reference_FeeScheduleAmount += (working_bltNIM3_Service.getReference_FeeScheduleAmount()*working_bltNIM3_Service.getCPTQty());  //times actual
                        myBO2.Reference_Payer_AllowAmount += working_bltNIM3_Service.getReference_PayerContractAmount()*working_bltNIM3_Service.getCPTQty_Bill();  //times billed
                        myBO2.Reference_Payer_BillAmount += working_bltNIM3_Service.getReference_PayerBillAmount()*working_bltNIM3_Service.getCPTQty_Bill();  //times billed
                        myBO2.Reference_Provider_AllowAmount += working_bltNIM3_Service.getReference_ProviderContractAmount()*working_bltNIM3_Service.getCPTQty_Pay();  //times pay
                        myBO2.Reference_Provider_BillAmount += working_bltNIM3_Service.getReference_ProviderBilledAmount();  //NOTE: already subtotaled
                        myBO2.Reference_UCAmount += working_bltNIM3_Service.getReference_UCAmount()*working_bltNIM3_Service.getCPTQty();  //times actual

                        //get actual by parsing through service billing transaction level items
                        bltNIM3_ServiceBillingTransaction_List        bltNIM3_ServiceBillingTransaction_List        =    new    bltNIM3_ServiceBillingTransaction_List(working_bltNIM3_Service.getServiceID());
                        //declaration of Enumeration
                        bltNIM3_ServiceBillingTransaction        working_bltNIM3_ServiceBillingTransaction;
                        ListElement         leCurrentElement_ServiceBillingTransaction;
                        Enumeration eList_ServiceBillingTransaction = bltNIM3_ServiceBillingTransaction_List.elements();
                        if (eList_ServiceBillingTransaction!=null&&eList_ServiceBillingTransaction.hasMoreElements())
                        {
                             while (eList_ServiceBillingTransaction.hasMoreElements())
                             {
                                 leCurrentElement_ServiceBillingTransaction    = (ListElement) eList_ServiceBillingTransaction.nextElement();
                                 working_bltNIM3_ServiceBillingTransaction  = (bltNIM3_ServiceBillingTransaction) leCurrentElement_ServiceBillingTransaction.getObject();
                                 if (working_bltNIM3_ServiceBillingTransaction.getServiceBillingTransactionTypeID()==NIM3_BillingObject2.REVENUE_TYPE)
                                 {
                                     myBO2.Actual_Revenue += working_bltNIM3_ServiceBillingTransaction.getTransactionAmount();
                                 }
                                 else if (working_bltNIM3_ServiceBillingTransaction.getServiceBillingTransactionTypeID()==NIM3_BillingObject2.REVENUEADJUSTMENT_TYPE)
                                 {
                                     myBO2.Actual_RevenueAdjustment += working_bltNIM3_ServiceBillingTransaction.getTransactionAmount();
                                 }
                                 else if (working_bltNIM3_ServiceBillingTransaction.getServiceBillingTransactionTypeID()==NIM3_BillingObject2.ARPAYMENT_TYPE)
                                 {
                                     myBO2.Actual_ARPayment += working_bltNIM3_ServiceBillingTransaction.getTransactionAmount();
                                 }
                                 else if (working_bltNIM3_ServiceBillingTransaction.getServiceBillingTransactionTypeID()==NIM3_BillingObject2.COS_TYPE)
                                 {
                                     myBO2.Actual_COS += working_bltNIM3_ServiceBillingTransaction.getTransactionAmount();
                                 }
                                 else if (working_bltNIM3_ServiceBillingTransaction.getServiceBillingTransactionTypeID()==NIM3_BillingObject2.COSADJUSTMENT_TYPE)
                                 {
                                     myBO2.Actual_COSAdjustment += working_bltNIM3_ServiceBillingTransaction.getTransactionAmount();
                                 }
                                 else if (working_bltNIM3_ServiceBillingTransaction.getServiceBillingTransactionTypeID()==NIM3_BillingObject2.APPAYMENT_TYPE)
                                 {
                                     myBO2.Actual_APPayment += working_bltNIM3_ServiceBillingTransaction.getTransactionAmount();
                                 }
                             }
                        }
                    }
                }
                return myBO2;
            } catch (Exception eee){
                return new NIM3_BillingObject2();

            }

        }




        public NIM3_BillingObject getBillingTotals()
        {
            NIM3_BillingObject myBO = new NIM3_BillingObject();
            Double myNetExpected = 0.0;
            Double myNetActual = 0.0;
            bltNIM3_Service        working_bltNIM3_Service;
            ListElement         leCurrentElement_Service;
            NIM3_Service_List = new bltNIM3_Service_List(this.getNIM3_Encounter().getEncounterID(),"","ServiceID");
            java.util.Enumeration eList_Service = NIM3_Service_List.elements();
            while (eList_Service.hasMoreElements())
            {
                leCurrentElement_Service    = (ListElement) eList_Service.nextElement();
                working_bltNIM3_Service  = (bltNIM3_Service) leCurrentElement_Service.getObject();
                if (working_bltNIM3_Service.getServiceStatusID()==1)
                {
                    myBO.Payer_ActualAmount += working_bltNIM3_Service.getReceivedAmount();
                    myBO.Payer_AllowAmount += working_bltNIM3_Service.getAllowAmount();
                    myBO.Payer_AllowAmount_Adjustment += working_bltNIM3_Service.getAllowAmountAdjustment();
                    myBO.Provider_AllowAmount += working_bltNIM3_Service.getPaidOutAmount();

                }
            }
            myBO.Provider_ActualAmount = NIM3_Encounter.getPaidToProviderCheck1Amount() + NIM3_Encounter.getPaidToProviderCheck2Amount() + NIM3_Encounter.getPaidToProviderCheck3Amount();
            return myBO;

        }

        public void resetAlertSkips_CaseNotApproved()
        {
            if (this.getNIM3_Referral().getReferralStatusID()!=1)
            {
                isAlertSkip_Adj = true;
                isAlertSkip_Adm = true;
                isAlertSkip_Adm2 = true;
                isAlertSkip_Adm3 = true;
                isAlertSkip_Adm4 = true;
                isAlertSkip_NCM = true;

                isAlert2Skip_Adj = true;
                isAlert2Skip_Adm = true;
                isAlert2Skip_Adm2 = true;
                isAlert2Skip_Adm3 = true;
                isAlert2Skip_Adm4 = true;
                isAlert2Skip_NCM = true;
            }

        }

        public  String getServicePrintOut(boolean isHTML)
        {
            String theBody = "";
            bltNIM3_Service        working_bltNIM3_Service;
            ListElement         leCurrentElement_Service;
            java.util.Enumeration eList_Service = NIM3_Service_List.elements();
            int cnt55=0;
            while (eList_Service.hasMoreElements())
            {
                cnt55++;
                leCurrentElement_Service    = (ListElement) eList_Service.nextElement();
                working_bltNIM3_Service  = (bltNIM3_Service) leCurrentElement_Service.getObject();
                if ( working_bltNIM3_Service.getServiceStatusID()==1)
                {
                    theBody += "\n" ;
                    if ( !working_bltNIM3_Service.getCPT().equalsIgnoreCase(""))
                    {
                        if (isHTML)
                        {
                            theBody += "" + working_bltNIM3_Service.getCPT() + "[" + NIMUtils.getCPTText(working_bltNIM3_Service.getCPT()) + "]";
                        }
                        else
                        {
                            theBody += "\t" + working_bltNIM3_Service.getCPT() + "[" + NIMUtils.getCPTText(working_bltNIM3_Service.getCPT()) + "]";
                        }
                    }
                    if ( !working_bltNIM3_Service.getCPTBodyPart().equalsIgnoreCase(""))
                    {
                        if (isHTML)
                        {
                            theBody += "<br>&nbsp;&nbsp;&nbsp;Body Part:" + working_bltNIM3_Service.getCPTBodyPart();
                        }
                        else
                        {
                            theBody += "\tBP:" + working_bltNIM3_Service.getCPTBodyPart();
                        }
                    }
                    if ( !working_bltNIM3_Service.getCPTText().equalsIgnoreCase(""))
                    {
                        if (isHTML)
                        {
                            theBody += "<br>&nbsp;&nbsp;&nbsp;Diag/Rule-Out: " + working_bltNIM3_Service.getCPTText();
                        }
                        else
                        {
                            theBody += "\tDiag/Rule-Out:\t" + working_bltNIM3_Service.getCPTText();
                        }
                    }
                }

            }
            return theBody;

        }


        public String getUniqueModifyComments()
        {
            return UniqueModifyComments;
        }

        public void setUniqueModifyComments(String uniqueModifyComments) {
            UniqueModifyComments = uniqueModifyComments;
        }
        
        /**
         * 
         * Here are the rules for auto-approving based on the child and parent autoauth column value in the tnim3_payermaster table:
         * 
         * Parent	Child 	Result
         * 
         * Y		blank	Y
         * N		blank	N
         * blank	blank	Y
         * 
         * Y		N		N
         * blank	N		N
         *  
         * N		Y		Y
         * blank	Y		Y
         * 
         */
        public Integer getAutoApproveReferral() {
 
			String childAutoAuth = "";
			String parentAutoAuth = "";
			
			//
			// Get parent info
    		//
			try {
    			java.sql.Connection cCon = java.sql.DriverManager.getConnection(
    					ConfigurationInformation.sURL,
    					ConfigurationInformation.sUser,
    					ConfigurationInformation.sPassword);

    			String stmt = "select autoauth from tnim3_payermaster where payerid  =  (select parentpayerid from tnim3_payermaster where payerid = "
    					+ getNIM3_PayerMaster().getPayerID().intValue() + ")";
    			DebugLogger.printLine("stmt= " + stmt);			
    			PreparedStatement ps = cCon.prepareStatement(stmt);
    			ResultSet rs = ps.executeQuery();
    			while (rs.next()) {
    				parentAutoAuth = rs.getString("autoauth");
    			}
    			rs.close();
    			ps.close();
    			cCon.close();
    		} catch (Exception eeee) {
    			System.out.println("Connection error: " + eeee.toString());
    		}

    		//
			// Get child info
    		//
			try {
    			java.sql.Connection cCon = java.sql.DriverManager.getConnection(
    					ConfigurationInformation.sURL,
    					ConfigurationInformation.sUser,
    					ConfigurationInformation.sPassword);

    			String stmt = "select autoauth from tnim3_payermaster where payerid = "
    					+ getNIM3_PayerMaster().getPayerID().intValue();
    			DebugLogger.printLine("stmt= " + stmt);			
    			PreparedStatement ps = cCon.prepareStatement(stmt);
    			ResultSet rs = ps.executeQuery();
    			while (rs.next()) {
    				childAutoAuth = rs.getString("autoauth");
    			}
    			rs.close();
    			ps.close();
    			cCon.close();
    		} catch (Exception eeee) {
    			System.out.println("Connection error: " + eeee.toString());
    		}
        	
			// And now, the rules from above.
			
			Integer finalAnswer = 0;
			if (childAutoAuth.equalsIgnoreCase("")) {
				DebugLogger.printLine("****** childAutoAuth.equalsIgnoreCase() ******");	
				DebugLogger.printLine("child = "+childAutoAuth);	
				DebugLogger.printLine("parent = "+parentAutoAuth);	
				if ("Y".compareToIgnoreCase(parentAutoAuth) == 0 ) {
					finalAnswer = 1;
				} else if ("N".compareToIgnoreCase(parentAutoAuth) == 0 ) {
					finalAnswer = 6;
				} else if (parentAutoAuth.equalsIgnoreCase("")) {
					finalAnswer = 6;
				}
			} else if (childAutoAuth.equalsIgnoreCase("Y") ) {
				DebugLogger.printLine("****** childAutoAuth.equalsIgnoreCase(Y) ******");	
				DebugLogger.printLine("child = "+childAutoAuth);	
				DebugLogger.printLine("parent = "+parentAutoAuth);				
				finalAnswer = 1;				
			} else if (childAutoAuth.equalsIgnoreCase("N")) {
				DebugLogger.printLine("****** childAutoAuth.equalsIgnoreCase(N) ******");	
				finalAnswer = 6;				
				DebugLogger.printLine("child = "+childAutoAuth);	
				DebugLogger.printLine("parent = "+parentAutoAuth);				
			}
			
			return finalAnswer;			
        }
        
        public String upgradeTest(String sDel)
        {
            String myVal = "";
            try
            {
                myVal += sDel+"Init EO1 for " + this.getNIM3_Encounter().getEncounterID();
                NIM3_EncounterObject myEO = new NIM3_EncounterObject(this.getNIM3_Encounter().getEncounterID());
                myVal += sDel+"EO1 Loaded";

                if (this.getNIM3_Appointment()!=null)
                {
                    if (this.getNIM3_Appointment().getAppointmentID().intValue()==myEO.NIM3_Appointment.getAppointmentID().intValue())
                    {
                        myVal += sDel + "AppointmentID Matches [" + this.getNIM3_Appointment().getAppointmentID() + "]";
                    }
                    else
                    {
                        myVal += sDel + "FAIL: AppointmentID";
                    }
                    if (this.getNIM3_Appointment()!=null&&this.getAppointment_PracticeMaster().getPracticeName().equalsIgnoreCase(myEO.Appointment_PracticeMaster.getPracticeName()))
                    {
                        myVal += sDel + "Appointment_PracticeMaster Matches [" + this.getAppointment_PracticeMaster().getPracticeName() + "]";
                    }
                    else
                    {
                        myVal += sDel + "FAIL: Appointment_PracticeMaster";
                    }
                }
                else
                {
                    myVal += sDel + "NO: AppointmentID";
                }

                if (this.getNIM3_Referral().getReferralID().intValue()==myEO.NIM3_Referral.getReferralID().intValue())
                {
                    myVal += sDel + "Referral Matches";
                }
                else
                {
                    myVal += sDel + "FAIL: Referral Fails EO2["+this.getNIM3_Referral().getReferralID()+"] != ["+myEO.NIM3_Referral.getReferralID()+"]";
                }


                if (this.getNIM3_CaseAccount().getCaseID().intValue()==myEO.NIM3_CaseAccount.getCaseID().intValue())
                {
                    myVal += sDel + "NIM3_CaseAccount Matches";
                }
                else
                {
                    myVal += sDel + "FAIL: NIM3_CaseAccount Fails";
                }

                if (this.getCase_Adjuster().getUserID().intValue()==myEO.Case_Adjuster.getUserID().intValue())
                {
                    myVal += sDel + "Case_Adjuster Matches";
                }
                else
                {
                    myVal += sDel + "FAIL: Case_Adjuster Fails";
                }

                if (this.getCase_Admin()!=null)
                {
                    if (this.getCase_Admin().getUserID().intValue()==myEO.Case_Admin.getUserID().intValue())
                    {
                        myVal += sDel + "Case_Admin Matches";
                    }
                    else
                    {
                        myVal += sDel + "FAIL: Case_Admin Fails";
                    }
                }
                else
                {
                    myVal += sDel + "Case_Admin NO";
                }


                if (this.getCase_Admin2()!=null)
                {
                    if (this.getCase_Admin2().getUserID().intValue()==myEO.Case_Admin2.getUserID().intValue() )
                    {
                        myVal += sDel + "Case_Admin2 Matches";
                    }
                    else
                    {
                        myVal += sDel + "FAIL: Case_Admin2 Fails";
                    }
                }
                else
                {
                    myVal += sDel + "Case_Admin2 NO";
                }


                if (this.getCase_Admin3()!=null)
                {
                    if(this.getCase_Admin3().getUserID().intValue()==myEO.Case_Admin3.getUserID().intValue())
                    {
                        myVal += sDel + "Case_Admin3 Matches";
                    }
                    else
                    {
                        myVal += sDel + "FAIL: Case_Admin3 Fails";
                    }
                }
                else
                {
                    myVal += sDel + "Case_Admin3 NO";
                }


                if (this.getCase_Admin4()!=null)
                {
                    if (this.getCase_Admin4().getUserID().intValue()==myEO.Case_Admin4.getUserID().intValue())
                    {
                        myVal += sDel + "Case_Admin4 Matches";
                    }
                    else
                    {
                        myVal += sDel + "FAIL: Case_Admin4 Fails";
                    }
                }
                else
                {
                    myVal += sDel + "Case_Admin4 NO";
                }


                if (this.getCase_BillTo()!=null)
                {
                    if(this.getCase_BillTo().getUserID().intValue()==myEO.Case_BillTo.getUserID().intValue())
                    {
                        myVal += sDel + "Case_BillTo Matches";
                    }
                    else
                    {
                        myVal += sDel + "FAIL: Case_BillTo Fails";
                    }
                }
                else
                {
                    myVal += sDel + "Case_BillTo NO";
                }


                if (this.getCase_NurseCaseManager()!=null)
                {
                    if(this.getCase_NurseCaseManager().getUserID().intValue()==myEO.Case_NurseCaseManager.getUserID().intValue())
                    {
                        myVal += sDel + "Case_NurseCaseManager Matches";
                    }
                    else
                    {
                        myVal += sDel + "FAIL: Case_NurseCaseManager Fails";
                    }

                }
                else
                {
                    myVal += sDel + "Case_NurseCaseManager NO";
                }


                if (this.getReferral_ReferringDoctor().getUserID().intValue()==myEO.Referral_ReferringDoctor.getUserID().intValue())
                {
                    myVal += sDel + "Referral_ReferringDoctor Matches";
                }
                else
                {
                    myVal += sDel + "FAIL: Referral_ReferringDoctor Fails";
                }


                if (this.getCase_AssignedTo().getUserID().intValue()==myEO.Case_AssignedTo.getUserID().intValue())
                {
                    myVal += sDel + "Case_AssignedTo Matches";
                }
                else
                {
                    myVal += sDel + "FAIL: Case_AssignedTo Fails";
                }


                if (this.getNIM3_PayerMaster().getPayerID().intValue()==myEO.NIM3_PayerMaster.getPayerID().intValue())
                {
                    myVal += sDel + "NIM3_PayerMaster Matches";
                }
                else
                {
                    myVal += sDel + "FAIL: NIM3_PayerMaster Fails";
                }

                if (this.getNIM3_ParentPayerMaster().getPayerID().intValue()==myEO.NIM3_ParentPayerMaster.getPayerID().intValue())
                {
                    myVal += sDel + "NIM3_ParentPayerMaster Matches";
                }
                else
                {
                    myVal += sDel + "FAIL: NIM3_ParentPayerMaster Fails";
                }


                if (this.getCase_AssignedTo_Payer().getPayerID().intValue()==myEO.Case_AssignedTo_Payer.getPayerID().intValue())
                {
                    myVal += sDel + "Case_AssignedTo_Payer Matches";
                }
                else
                {
                    myVal += sDel + "FAIL: Case_AssignedTo_Payer Fails";
                }


                if (this.isAlertSkip_Adj()==myEO.isAlertSkip_Adj)
                {
                    myVal += sDel + "isAlertSkip_Adj Matches";
                }
                else
                {
                    myVal += sDel + "FAIL: isAlertSkip_Adj Fails";
                }

                if (this.isAlertSkip_Adm()==myEO.isAlertSkip_Adm)
                {
                    myVal += sDel + "isAlertSkip_Adm Matches";
                }
                else
                {
                    myVal += sDel + "FAIL: isAlertSkip_Adm Fails";
                }

                if (this.isAlertSkip_Adm2()==myEO.isAlertSkip_Adm2)
                {
                    myVal += sDel + "isAlertSkip_Adm2 Matches";
                }
                else
                {
                    myVal += sDel + "FAIL: isAlertSkip_Adm2 Fails";
                }

                if (this.isAlertSkip_Adm3()==myEO.isAlertSkip_Adm3)
                {
                    myVal += sDel + "isAlertSkip_Adm3 Matches";
                }
                else
                {
                    myVal += sDel + "FAIL: isAlertSkip_Adm3 Fails";
                }

                if (this.isAlertSkip_Adm4()==myEO.isAlertSkip_Adm4)
                {
                    myVal += sDel + "isAlertSkip_Adm4 Matches";
                }
                else
                {
                    myVal += sDel + "FAIL: isAlertSkip_Adm4 Fails";
                }

                if (this.isAlertSkip_NCM()==myEO.isAlertSkip_NCM)
                {
                    myVal += sDel + "isAlertSkip_NCM Matches";
                }
                else
                {
                    myVal += sDel + "FAIL: isAlertSkip_NCM Fails";
                }

                if (this.isAlertSkip_RefDr()==myEO.isAlertSkip_RefDr)
                {
                    myVal += sDel + "isAlertSkip_RefDr Matches";
                }
                else
                {
                    myVal += sDel + "FAIL: isAlertSkip_RefDr Fails";
                }


                if (this.isAlert2Skip_Adj()==myEO.isAlert2Skip_Adj)
                {
                    myVal += sDel + "isAlert2Skip_Adj Matches";
                }
                else
                {
                    myVal += sDel + "FAIL: isAlert2Skip_Adj Fails";
                }

                if (this.isAlert2Skip_Adm()==myEO.isAlert2Skip_Adm)
                {
                    myVal += sDel + "isAlert2Skip_Adm Matches";
                }
                else
                {
                    myVal += sDel + "FAIL: isAlert2Skip_Adm Fails";
                }

                if (this.isAlert2Skip_Adm2()==myEO.isAlert2Skip_Adm2)
                {
                    myVal += sDel + "isAlert2Skip_Adm2 Matches";
                }
                else
                {
                    myVal += sDel + "FAIL: isAlert2Skip_Adm2 Fails";
                }

                if (this.isAlert2Skip_Adm3()==myEO.isAlert2Skip_Adm3)
                {
                    myVal += sDel + "isAlert2Skip_Adm3 Matches";
                }
                else
                {
                    myVal += sDel + "FAIL: isAlert2Skip_Adm3 Fails";
                }

                if (this.isAlert2Skip_Adm4()==myEO.isAlert2Skip_Adm4)
                {
                    myVal += sDel + "isAlert2Skip_Adm4 Matches";
                }
                else
                {
                    myVal += sDel + "FAIL: isAlert2Skip_Adm4 Fails";
                }

                if (this.isAlert2Skip_NCM()==myEO.isAlert2Skip_NCM)
                {
                    myVal += sDel + "isAlert2Skip_NCM Matches";
                }
                else
                {
                    myVal += sDel + "FAIL: isAlert2Skip_NCM Fails";
                }

                if (this.isAlert2Skip_RefDr()==myEO.isAlert2Skip_RefDr)
                {
                    myVal += sDel + "isAlert2Skip_RefDr Matches";
                }
                else
                {
                    myVal += sDel + "FAIL: isAlert2Skip_RefDr Fails";
                }


                if (this.isReportSkip_Adj()==myEO.isReportSkip_Adj)
                {
                    myVal += sDel + "isReportSkip_Adj Matches";
                }
                else
                {
                    myVal += sDel + "FAIL: isReportSkip_Adj Fails";
                }

                if (this.isReportSkip_Adm()==myEO.isReportSkip_Adm)
                {
                    myVal += sDel + "isReportSkip_Adm Matches";
                }
                else
                {
                    myVal += sDel + "FAIL: isReportSkip_Adm Fails";
                }

                if (this.isReportSkip_Adm2()==myEO.isReportSkip_Adm2)
                {
                    myVal += sDel + "isReportSkip_Adm2 Matches";
                }
                else
                {
                    myVal += sDel + "FAIL: isReportSkip_Adm2 Fails";
                }

                if (this.isReportSkip_Adm3()==myEO.isReportSkip_Adm3)
                {
                    myVal += sDel + "isReportSkip_Adm3 Matches";
                }
                else
                {
                    myVal += sDel + "FAIL: isReportSkip_Adm3 Fails";
                }

                if (this.isReportSkip_Adm4()==myEO.isReportSkip_Adm4)
                {
                    myVal += sDel + "isReportSkip_Adm4 Matches";
                }
                else
                {
                    myVal += sDel + "FAIL: isReportSkip_Adm4 Fails";
                }

                if (this.isReportSkip_NCM()==myEO.isReportSkip_NCM)
                {
                    myVal += sDel + "isReportSkip_NCM Matches";
                }
                else
                {
                    myVal += sDel + "FAIL: isReportSkip_NCM Fails";
                }

                if (this.isReportSkip_RefDr()==myEO.isReportSkip_RefDr)
                {
                    myVal += sDel + "isReportSkip_RefDr Matches";
                }
                else
                {
                    myVal += sDel + "FAIL: isReportSkip_RefDr Fails";
                }




                if (this.isScanPassAlertsComplete(1)==myEO.isScanPassAlertsComplete(1)
                        &&this.isScanPassAlertsComplete(2)==myEO.isScanPassAlertsComplete(2)
                        &&this.isScanPassAlertsComplete(3)==myEO.isScanPassAlertsComplete(3))
                {
                    myVal += sDel + "isScanPassAlertsComplete Matches";
                }
                else
                {
                    myVal += sDel + "FAIL: isScanPassAlertsComplete Fails";
                }



                if (this.isPreScreenComplete()==myEO.isPreScreenComplete())
                {
                    myVal += sDel + "isPreScreenComplete Matches";
                }
                else
                {
                    myVal += sDel + "FAIL: isPreScreenComplete Fails";
                }


                if (this.isEncounterProcessed()==myEO.isEncounterProcessed())
                {
                    myVal += sDel + "isEncounterProcessed Matches";
                }
                else
                {
                    myVal += sDel + "FAIL: isEncounterProcessed Fails";
                }


                try
                {
                    if (this.getEncounterStory("|").equalsIgnoreCase(myEO.getEncounterStory("|")))
                    {
                        myVal += sDel + "getEncounterStory Matches";
                    }
                    else
                    {
                        myVal += sDel + "FAIL: getEncounterStory Fails";
                    }
                }
                catch(Exception e)
                {
                    myVal += sDel + "OK: getEncounterStory Fails";
                }





                if (this.getEncounterType_Display().equalsIgnoreCase(myEO.getEncounterType_Display()))
                {
                    myVal += sDel + "getEncounterType_Display Matches";
                }
                else
                {
                    myVal += sDel + "FAIL: getEncounterType_Display Fails";
                }



                if (this.getCourtesyStatus_Display().equalsIgnoreCase(myEO.getCourtesyStatus_Display()))
                {
                    myVal += sDel + "getCourtesyStatus_Display Matches";
                }
                else
                {
                    myVal += sDel + "FAIL: getCourtesyStatus_Display Fails";
                }


                if (this.getCaseDashboard_CombinedNotes("|").equalsIgnoreCase(myEO.getCaseDashboard_CombinedNotes("|")))
                {
                    myVal += sDel + "getCaseDashboard_CombinedNotes Matches";
                }
                else
                {
                    myVal += sDel + "FAIL: getCaseDashboard_CombinedNotes Fails";
                }

                if (this.getEmailImportantNotes_Payer("|").equalsIgnoreCase(myEO.getEmailImportantNotes_Payer("|")))
                {
                    myVal += sDel + "getEmailImportantNotes_Payer Matches";
                }
                else
                {
                    myVal += sDel + "FAIL: getEmailImportantNotes_Payer Fails";
                }

                if (this.getImportantNotes_All(this.getCase_Adjuster(),"|").equalsIgnoreCase(myEO.getImportantNotes_All(myEO.Case_Adjuster,"|")))
                {
                    myVal += sDel + "getImportantNotes_All Matches";
                }
                else
                {
                    myVal += sDel + "FAIL: getImportantNotes_All Fails";
                }

                if (this.getCaseDashboard_AlertCode_HTML().equalsIgnoreCase(myEO.getCaseDashboard_AlertCode_HTML()))
                {
                    myVal += sDel + "getCaseDashboard_AlertCode_HTML Matches";
                }
                else
                {
                    myVal += sDel + "FAIL: getCaseDashboard_AlertCode_HTML Fails";
                }


                if (this.getPayerFullName().equalsIgnoreCase(myEO.getPayerFullName()))
                {
                    myVal += sDel + "getPayerFullName Matches";
                }
                else
                {
                    myVal += sDel + "FAIL: getPayerFullName Fails";
                }


                if (this.getFeeScheduleRateTablePayerMRI("|").equalsIgnoreCase(myEO.getFeeScheduleRateTablePayerMRI("|")))
                {
                    myVal += sDel + "getFeeScheduleRateTablePayerMRI Matches";
                }
                else
                {
                    myVal += sDel + "FAIL: getFeeScheduleRateTablePayerMRI Fails";
                }
            }
            catch (Exception e)
            {
                myVal += sDel+sDel+sDel+"Error ["+e+"]";
            }

            return myVal;

        }
        /**
         * Checks if there is claim no., adj, payer, dob, doi, dos
         * @return false if any checks returns negative 
         */
        public boolean hasCritialInfomation(){
        	boolean hasCritialInfomation = true;
        	String messageText = "Critial Infomation Missing: ";
        	if(this.NIM3_CaseAccount.getCaseClaimNumber().isEmpty()){
        		hasCritialInfomation = false;
        		messageText+="Missing claim number. ";
        	}
        	if(this.NIM3_CaseAccount.getAdjusterID()==0){
        		hasCritialInfomation = false;
        		messageText+="Missing Adjuster. ";
        	}
        	if(this.NIM3_CaseAccount.getPayerID()==0){
        		hasCritialInfomation = false;
        		messageText+="Missing payer. ";
        	}
        	try {
				if(this.NIM3_CaseAccount.getPatientDOB().equals(new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").parse("1800-01-01 00:00:00"))){
					hasCritialInfomation = false;
					messageText+="Missing patient DOB. ";
				}
			} catch (ParseException e) {
				e.printStackTrace();
			}
        	try {
        		if(this.NIM3_CaseAccount.getDateOfInjury().equals(new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").parse("1800-01-01 00:00:00"))){
        			hasCritialInfomation = false;
        			messageText+="Missing date of injury. ";
        		}
        	} catch (ParseException e) {
        		e.printStackTrace();
        	}
        	try {
        		if(this.NIM3_Encounter.getDateOfService().equals(new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").parse("1800-01-01 00:00:00"))){
        			hasCritialInfomation = false;
        			messageText+="Missing date of service. ";
        		}
        	} catch (ParseException e) {
        		e.printStackTrace();
        	}
        	
        	if(!hasCritialInfomation){
        		bltNIM3_CommTrack ct = new bltNIM3_CommTrack();
        		ct.setUniqueCreateDate(new Date());
        		ct.setUniqueModifyDate(new Date());
        		ct.setUniqueModifyComments("NIM3_EncounterObject2.hasCritialInfomation()");
        		ct.setCaseID(this.getNIM3_CaseAccount().getCaseID());
        	    ct.setReferralID(this.getNIM3_Referral().getReferralID());
        	    ct.setEncounterID(this.getNIM3_Encounter().getEncounterID());
        	    ct.setMessageName("System Automation");
        	    ct.setMessageCompany("NextImage Medical");
        	    ct.setMessageText(messageText);
        	    ct.setCommTypeID(3);
        	    ct.setCommStart(new Date());
        	    ct.setCommEnd(new Date());
        	    ct.setAlertStatusCode(1);
        	    
        		try {
					ct.commitData();
				} catch (SQLException e) {
					e.printStackTrace();
				}
        	}
        	
        	return hasCritialInfomation;
        }

}
