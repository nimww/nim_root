package com.winstaff;

import java.sql.SQLException;

public class ActualsToPlanByRegion {

	private String query = "SELECT \"public\".\"vMQ4_Encounter_Void_BP\".\"Payer_SalesDivision\", \"public\".\"vMQ4_Encounter_Void_BP\".\"Referral_ReceiveDate_iMonth\", Count(\"public\".\"vMQ4_Encounter_Void_BP\".\"Encounter_ScanPass\") as Volume FROM \"public\".\"vMQ4_Encounter_Void_BP\"  WHERE \"Referral_ReceiveDate_iYear\" = EXTRACT(YEAR FROM CURRENT_DATE) AND \"Payer_SalesDivision\" !='' AND \"Payer_SalesDivision\" != '0' AND \"Payer_SalesDivision\" != 'NIM' AND \"Payer_SalesDivision\" != 'NID' AND \"Payer_SalesDivision\" != 'MSC' GROUP BY \"public\".\"vMQ4_Encounter_Void_BP\".\"Payer_SalesDivision\", \"public\".\"vMQ4_Encounter_Void_BP\".\"Referral_ReceiveDate_iMonth\" order by \"public\".\"vMQ4_Encounter_Void_BP\".\"Referral_ReceiveDate_iMonth\" desc";
	
	private String regions[];
	private int act2plan[][];
	private searchDB2 conn = new searchDB2();
	private java.sql.ResultSet rs;
	
	public ActualsToPlanByRegion() throws SQLException{
		setMapRegions();
		setRegions();
	}
	
	/**
	 * Sets up regions[] with data
	 * @throws SQLException
	 */
	private void setMapRegions() throws SQLException{
		
		String query ="select DISTINCT salesdivision from tnim3_payermaster where trim(salesdivision) not in ('IS2','0','','NID','PCS','NIM','MSC') ";
		rs = conn.executeStatement(query);
		rs.last();
		regions = new String[rs.getRow()];
		rs.first();
		int count = 0;
		while (rs.next()){
			regions[count++] = rs.getString("salesdivision");
		}
	}
	/**
	 * Translate region into int
	 * @param region
	 * @return
	 */
	public int regionToInt(String region){
		
		for (int count = 0; count<regions.length;count++){
			if (region.equalsIgnoreCase(regions[count])){
				return count;
			}
		}
		return 0;
	}
	
	public String[] getX(){
		return this.regions;
	}
	/**
	 * Maps regions 
	 * @throws SQLException 
	 */
	private void setRegions() throws SQLException{
		act2plan = new int[12][regions.length];
		rs = conn.executeStatement(query);
		
		while (rs.next()){
			act2plan[rs.getInt("Referral_ReceiveDate_iMonth")][regionToInt(rs.getString("Payer_SalesDivision"))] = rs.getInt("volume");
		}	
	}
	
	public int getRegions(int month, String region){
		
		return act2plan[month][regionToInt(region)];
	}
	
}
