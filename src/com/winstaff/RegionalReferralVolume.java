package com.winstaff;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map.Entry;
import java.sql.ResultSet;
import java.sql.SQLException;
public class RegionalReferralVolume {
	public final static Calendar cal = Calendar.getInstance();
	public static List<String> list = new ArrayList<String>();
	public static LinkedHashMap <String, LinkedHashMap<Integer, Integer>> lhm = new LinkedHashMap <String, LinkedHashMap<Integer, Integer>>();
	
	public static DecimalFormat df = new DecimalFormat("00");
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}
	
	public static List<String> fillData(){
		List<String> l = new ArrayList<String>();
		for (int referralDay = 1; referralDay < cal.get(Calendar.DAY_OF_MONTH)+2;referralDay++){
			l.add(getSDG(referralDay));
		}
		return l;
	}
	public static String getSDG(int referralDay){
		return getSDG(referralDay, cal.get(Calendar.MONTH)+1, cal.get(Calendar.YEAR));
	}
	
	public static String getSDG(int referralDay, int referralMonth, int referralYear){
		String query = "select pm.salesdivision, count(*) from tnim3_encounter en join tnim3_referral ref on ref.referralid = en.referralid join tnim3_caseaccount ca on ca.caseid = ref.caseid join tnim3_payermaster pm on pm.payerid = ca.payerid where en.encounterstatusid != 5 and " +
				"cast(receivedate as varchar) ~ '"+referralYear+"-"+df.format(referralMonth)+"-"+df.format(referralDay)+"' GROUP BY pm.salesdivision";
		searchDB2 conn = new searchDB2();
		ResultSet rs = conn.executeStatement(query);
		
		try {
			while (rs.next()){
				LinkedHashMap<Integer, Integer> ilhm = null;

				try{
					ilhm = lhm.get(rs.getString("salesdivision"));
					ilhm.put(referralDay, rs.getInt("count"));
				} catch(Exception e){
					ilhm = new LinkedHashMap<Integer, Integer>();
					ilhm.put(referralDay, rs.getInt("count"));
				}
				
				lhm.put(rs.getString("salesdivision"), ilhm);
				
				
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally{
			conn.closeAll();
		}
		return query;
	}
	
}

