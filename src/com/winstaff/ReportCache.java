package com.winstaff;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
/**
 * 
 * @author Po Le
 *
 */
public class ReportCache {

	public static void main(String[] args) {
		System.out.println("ReportCache start: "+new Date());

		ReportCache reportcache = new ReportCache();
		
		reportcache.getServicesUpdate();
		
		//reportcache.servicesAddFS("vmq4_services");
		System.out.println("ReportCache end: "+new Date());

	}
	/**
	 * Pulls updated/new service records, add fs and updates vmq4_services
	 */
	public void getServicesUpdate(){
		searchDB2 db = new searchDB2();
		
		String query = "TRUNCATE TABLE vmq4_services_temp";
		db.executeUpdate(query);
		
		query = "insert into vmq4_services_temp select * from \"vMQ4_Services\" where \"ServiceID\" in (select DISTINCT \"ServiceID\" from vmq4_services join tnim3_service sr on sr.serviceid = \"ServiceID\" join tnim3_encounter en on en.encounterid = sr.encounterid join tnim3_referral rf on rf.referralid = en.referralid join tnim3_caseaccount ca on ca.caseid = rf.caseid where sr.uniquemodifydate > now()-'1 day'::INTERVAL or en.uniquemodifydate > now()-'1 day'::INTERVAL or rf.uniquemodifydate > now()-'1 day'::INTERVAL or ca.uniquemodifydate > now()-'1 day'::INTERVAL union select mqcache.\"ServiceID\" from vmq4_services mqtable right join \"vMQ4_Services\" mqcache on mqcache.\"ServiceID\" = mqtable.\"ServiceID\" where mqtable.\"StateFS\" is null)";
		db.executeUpdate(query);
		
		servicesAddFS("vmq4_services_temp");
		
		query ="DELETE FROM vmq4_services WHERE \"ServiceID\" in (select \"ServiceID\" from vmq4_services_temp)";
		db.executeUpdate(query);
		
		query ="insert into vmq4_services select * from vmq4_services_temp";
		db.executeUpdate(query);
		
	}
	
	/**
	 * Adds FS to record where there is no FS. 
	 */
	public void servicesAddFS(String from){
		searchDB2 db = new searchDB2();
		
		String query = "select \"CaseID\", \"Service_CPT\", \"Practice_Zip\", \"Service_CPTModifier\", \"Patient_Zip\", \"Encounter_DateOfService\"  from "+from+" where \"StateFS\" is null";
		String query2 = "";
		ResultSet rs = db.executeStatement(query); 
		System.out.println("ReportCache.servicesAddFS() start: "+new Date());
		try {
			while(rs.next()){
				double statefs = 0;
				try{
					statefs = NIMUtils.getFeeSchedulePrice(1, 1d, (rs.getString("Practice_Zip")!= null ? rs.getString("Practice_Zip"):rs.getString("Patient_Zip")), rs.getString("Service_CPT"),  rs.getString("Service_CPTModifier"), rs.getDate("Encounter_DateOfService"));
				} catch (Exception e){
					System.out.println(e.toString());
				}
				double medicare = 0;
				try{
					statefs = NIMUtils.getFeeSchedulePrice(2, 1d, (rs.getString("Practice_Zip")!= null ? rs.getString("Practice_Zip"):rs.getString("Patient_Zip")), rs.getString("Service_CPT"),  rs.getString("Service_CPTModifier"), rs.getDate("Encounter_DateOfService"));
				} catch (Exception e){
					System.out.println(e.toString());
				}
				double walmart = 0;
				try{
					statefs = NIMUtils.getFeeSchedulePrice(165, 1d, (rs.getString("Practice_Zip")!= null ? rs.getString("Practice_Zip"):rs.getString("Patient_Zip")), rs.getString("Service_CPT"),  rs.getString("Service_CPTModifier"), rs.getDate("Encounter_DateOfService"));
				} catch (Exception e){
					System.out.println(e.toString());
				}
				double nsp = 0;
				try{
					statefs = NIMUtils.getFeeSchedulePrice(7, 1d, (rs.getString("Practice_Zip")!= null ? rs.getString("Practice_Zip"):rs.getString("Patient_Zip")), rs.getString("Service_CPT"),  rs.getString("Service_CPTModifier"), rs.getDate("Encounter_DateOfService"));
				} catch (Exception e){
					System.out.println(e.toString());
				}
				
				query2 = "update "+from+" set \"StateFS\" = "+statefs+", \"Medicare\" = "+medicare+", \"Walmart\" = "+walmart+", \"NSP\" = "+nsp+" where \"ServiceID\" = "+rs.getString("CaseID");
				System.out.println(query2);
				db.executeUpdate(query2);
			}
		} catch (SQLException e) {
		}
		System.out.println("ReportCache.servicesAddFS() end: "+new Date());
		db.closeAll();
		
	}
}
