

package com.winstaff;

import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public interface InttMRI_ModelLI extends dbTableInterface
{

    public void setMRI_ModelID(Integer newValue);
    public Integer getMRI_ModelID();
    public void setDescriptionLong(String newValue);
    public String getDescriptionLong();
    public void setIsOpen(Integer newValue);
    public Integer getIsOpen();
    public void setAccomodatesClaus(Integer newValue);
    public Integer getAccomodatesClaus();
    public void setTeslaStrength(String newValue);
    public String getTeslaStrength();
    public void setManufacturer(String newValue);
    public String getManufacturer();
    public void setModel(String newValue);
    public String getModel();
    public void setClassLevel(Integer newValue);
    public Integer getClassLevel();
}    // End of bltMRI_ModelLI class definition
