

package com.winstaff;

import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public interface InttNIM3_FeeScheduleRef extends dbTableInterface
{

    public void setFeeScheduleRefID(Integer newValue);
    public Integer getFeeScheduleRefID();
    public void setUniqueCreateDate(Date newValue);
    public Date getUniqueCreateDate();
    public void setUniqueModifyDate(Date newValue);
    public Date getUniqueModifyDate();
    public void setUniqueModifyComments(String newValue);
    public String getUniqueModifyComments();
    public void setRefName(String newValue);
    public String getRefName();
}    // End of bltNIM3_FeeScheduleRef class definition
