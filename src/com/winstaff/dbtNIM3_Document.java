

package com.winstaff;

import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public class dbtNIM3_Document extends Object implements InttNIM3_Document
{

        db_NewBase    dbnbDB;

    public dbtNIM3_Document()
    {
        dbnbDB = new db_NewBase( "tNIM3_Document", "DocumentID" );

    }    // End of default constructor

    public dbtNIM3_Document( Integer iNewID )
    {
        dbnbDB = new db_NewBase( "tNIM3_Document", "DocumentID" );
        setUniqueID( iNewID );
    }    // End of Constructor knowing an ID

    public void commitData() throws SQLException
    {
            //no longer trapping/ignoring errors 2011-03-17
            dbnbDB.commitData();
    }    // End of commitData()

    public void commitDataForced() throws SQLException
    {
        dbnbDB.commitDataForced();
    }    // End of commitData()

    public Integer getUniqueID()
    {
        return dbnbDB.getUniqueID();
    }    // End of getUniqueID()

    public void setUniqueID( Integer iNewID )
    {
        dbnbDB.setUniqueID( iNewID );
    }    // End of setUniqueID()

    public void setModifyComments( String sNew )
    {
        dbnbDB.setModifyComments( sNew );
    }    // End of setUniqueID()


    public void setDocumentID(Integer newValue)
    {
                dbnbDB.setFieldData( "DocumentID", newValue.toString() );
    }

    public Integer getDocumentID()
    {
        String           sValue = dbnbDB.getFieldData( "DocumentID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setField(String fieldN, Object fieldV)
    {DebugLogger.println("setField Method not implemented yet in classNIM3_Document!");}

    public String getFieldType(String fieldN)
    {return null;}


    public void setUniqueCreateDate(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "UniqueCreateDate", formatter.format( newValue ) );
    }

    public Date getUniqueCreateDate()
    {
        String           sValue = dbnbDB.getFieldData( "UniqueCreateDate" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setUniqueModifyDate(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "UniqueModifyDate", formatter.format( newValue ) );
    }

    public Date getUniqueModifyDate()
    {
        String           sValue = dbnbDB.getFieldData( "UniqueModifyDate" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setUniqueModifyComments(String newValue)
    {
                dbnbDB.setFieldData( "UniqueModifyComments", newValue.toString() );
    }

    public String getUniqueModifyComments()
    {
        String           sValue = dbnbDB.getFieldData( "UniqueModifyComments" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setCaseID(Integer newValue)
    {
                dbnbDB.setFieldData( "CaseID", newValue.toString() );
    }

    public Integer getCaseID()
    {
        String           sValue = dbnbDB.getFieldData( "CaseID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setReferralID(Integer newValue)
    {
                dbnbDB.setFieldData( "ReferralID", newValue.toString() );
    }

    public Integer getReferralID()
    {
        String           sValue = dbnbDB.getFieldData( "ReferralID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setEncounterID(Integer newValue)
    {
                dbnbDB.setFieldData( "EncounterID", newValue.toString() );
    }

    public Integer getEncounterID()
    {
        String           sValue = dbnbDB.getFieldData( "EncounterID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setServiceID(Integer newValue)
    {
                dbnbDB.setFieldData( "ServiceID", newValue.toString() );
    }

    public Integer getServiceID()
    {
        String           sValue = dbnbDB.getFieldData( "ServiceID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setUploadUserID(Integer newValue)
    {
                dbnbDB.setFieldData( "UploadUserID", newValue.toString() );
    }

    public Integer getUploadUserID()
    {
        String           sValue = dbnbDB.getFieldData( "UploadUserID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setReferenceDate(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "ReferenceDate", formatter.format( newValue ) );
    }

    public Date getReferenceDate()
    {
        String           sValue = dbnbDB.getFieldData( "ReferenceDate" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setDocType(Integer newValue)
    {
                dbnbDB.setFieldData( "DocType", newValue.toString() );
    }

    public Integer getDocType()
    {
        String           sValue = dbnbDB.getFieldData( "DocType" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setDocName(String newValue)
    {
                dbnbDB.setFieldData( "DocName", newValue.toString() );
    }

    public String getDocName()
    {
        String           sValue = dbnbDB.getFieldData( "DocName" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setFileName(String newValue)
    {
                dbnbDB.setFieldData( "FileName", newValue.toString() );
    }

    public String getFileName()
    {
        String           sValue = dbnbDB.getFieldData( "FileName" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setDCMFile(String newValue)
    {
                dbnbDB.setFieldData( "DCMFile", newValue.toString() );
    }

    public String getDCMFile()
    {
        String           sValue = dbnbDB.getFieldData( "DCMFile" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setReportSummary(String newValue)
    {
                dbnbDB.setFieldData( "ReportSummary", newValue.toString() );
    }

    public String getReportSummary()
    {
        String           sValue = dbnbDB.getFieldData( "ReportSummary" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setReportText(String newValue)
    {
                dbnbDB.setFieldData( "ReportText", newValue.toString() );
    }

    public String getReportText()
    {
        String           sValue = dbnbDB.getFieldData( "ReportText" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setReportDictationFile(String newValue)
    {
                dbnbDB.setFieldData( "ReportDictationFile", newValue.toString() );
    }

    public String getReportDictationFile()
    {
        String           sValue = dbnbDB.getFieldData( "ReportDictationFile" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setTreatingPhysician(String newValue)
    {
                dbnbDB.setFieldData( "TreatingPhysician", newValue.toString() );
    }

    public String getTreatingPhysician()
    {
        String           sValue = dbnbDB.getFieldData( "TreatingPhysician" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setDiagnosticPacs(String newValue)
    {
                dbnbDB.setFieldData( "DiagnosticPacs", newValue.toString() );
    }

    public String getDiagnosticPacs()
    {
        String           sValue = dbnbDB.getFieldData( "DiagnosticPacs" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setEMR(String newValue)
    {
                dbnbDB.setFieldData( "EMR", newValue.toString() );
    }

    public String getEMR()
    {
        String           sValue = dbnbDB.getFieldData( "EMR" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setReferringPhysician(String newValue)
    {
                dbnbDB.setFieldData( "ReferringPhysician", newValue.toString() );
    }

    public String getReferringPhysician()
    {
        String           sValue = dbnbDB.getFieldData( "ReferringPhysician" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setComments(String newValue)
    {
                dbnbDB.setFieldData( "Comments", newValue.toString() );
    }

    public String getComments()
    {
        String           sValue = dbnbDB.getFieldData( "Comments" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

}    // End of bltNIM3_Document class definition
