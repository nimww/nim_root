

package com.winstaff;

import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public interface InttHCOPhysicianTransaction extends dbTableInterface
{

    public void setHCOPhysicianTransactionID(Integer newValue);
    public Integer getHCOPhysicianTransactionID();
    public void setUniqueCreateDate(Date newValue);
    public Date getUniqueCreateDate();
    public void setUniqueModifyDate(Date newValue);
    public Date getUniqueModifyDate();
    public void setUniqueModifyComments(String newValue);
    public String getUniqueModifyComments();
    public void setLookupID(Integer newValue);
    public Integer getLookupID();
    public void setTransactionDate(Date newValue);
    public Date getTransactionDate();
    public void setActionID(Integer newValue);
    public Integer getActionID();
    public void setUserID(Integer newValue);
    public Integer getUserID();
    public void setTransactionComments(String newValue);
    public String getTransactionComments();
    public void setComments(String newValue);
    public String getComments();
}    // End of bltHCOPhysicianTransaction class definition
