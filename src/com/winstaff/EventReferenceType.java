package com.winstaff;
import com.winstaff.ContactReferenceType;
import java.awt.*;
import java.util.Date;


public class EventReferenceType extends java.lang.Object
{

	public EventReferenceType()
	{
	}

	public void setContactReference(ContactReferenceType contactReference)
	{
		this.contactReference = contactReference;
	}

	public ContactReferenceType getContactReference()
	{
		return this.contactReference;
	}

	public void setLastModifiedDate(java.util.Date lastModifiedDate)
	{
		this.lastModifiedDate = lastModifiedDate;
	}

	public java.util.Date getLastModifiedDate()
	{
		return this.lastModifiedDate;
	}

	public void setName(String name)
	{
			this.name = name;
	}

	public String getName()
	{
		return this.name;
	}

	public void setRefID(Integer refID)
	{
			this.refID = refID;
	}

	public Integer getRefID()
	{
		return this.refID;
	}


	public void setRefIDType(String refIDType)
	{
			this.refIDType = refIDType;
	}

	public String getRefIDType()
	{
		return this.refIDType;
	}

	public void setSummaryNotes(String summaryNotes)
	{
			this.summaryNotes = summaryNotes;
	}

	public String getSummaryNotes()
	{
		return this.summaryNotes;
	}

	public void setLastModifiedUser(String lastModifiedUser)
	{
			this.lastModifiedUser = lastModifiedUser;
	}

	public String getLastModifiedUser()
	{
		return this.lastModifiedUser;
	}


	public void setMergeGeneric1(String mergeGeneric1)
	{
			this.mergeGeneric1 = mergeGeneric1;
	}

	public String getMergeGeneric1()
	{
		return this.mergeGeneric1;
	}

	public void setMergeGeneric2(String mergeGeneric2)
	{
			this.mergeGeneric2 = mergeGeneric2;
	}

	public String getMergeGeneric2()
	{
		return this.mergeGeneric2;
	}

	public void setMergeGeneric3(String mergeGeneric3)
	{
			this.mergeGeneric3 = mergeGeneric3;
	}

	public String getMergeGeneric3()
	{
		return this.mergeGeneric3;
	}

	public void setMergeGeneric4(String mergeGeneric4)
	{
			this.mergeGeneric4 = mergeGeneric4;
	}

	public String getMergeGeneric4()
	{
		return this.mergeGeneric4;
	}

	public void setMergeGeneric5(String mergeGeneric5)
	{
			this.mergeGeneric5 = mergeGeneric5;
	}

	public String getMergeGeneric5()
	{
		return this.mergeGeneric5;
	}


	protected String mergeGeneric5 = "";
	protected String mergeGeneric4 = "";
	protected String mergeGeneric3 = "";
	protected String mergeGeneric2 = "";
	protected String mergeGeneric1 = "";
	protected java.util.Date lastModifiedDate;
	protected String name = "";
	protected ContactReferenceType contactReference;
	protected String summaryNotes = "";
	protected Integer refID = new Integer(0);
	protected String refIDType = "";
	protected String lastModifiedUser = "";

}
