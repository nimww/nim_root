package com.winstaff;
import java.awt.*;


public class ZIP_CODE_object extends java.lang.Object
{

	public ZIP_CODE_object()
	{
	}
	
	public java.util.TreeMap tmZip = new java.util.TreeMap();
	
	public java.util.Vector<String> getZipsInRange(double myRange)
	{
		java.util.Vector<String> myVal = new java.util.Vector<String>();
		java.util.Vector myValDist = new java.util.Vector();
		java.util.Iterator it = this.tmZip.keySet().iterator();
		while (it.hasNext()) 
		{
			String obj = (String) it.next();

			Double tempDistance = (Double) this.tmZip.get(obj);
			if (tempDistance.doubleValue()<=myRange)
			{
				boolean isPlaced = false;
				if (myValDist.size()==0)
				{
					myValDist.add(tempDistance);
					myVal.add(obj);
				}
				else
				{
					int i=0;
					while (i<myValDist.size() && !isPlaced)
					{
						Double myTestVal = (Double) myValDist.elementAt(i);
						if (tempDistance.doubleValue()<=myTestVal.doubleValue())
						{
							myValDist.insertElementAt(tempDistance,i);
							myVal.insertElementAt(obj,i);
							isPlaced = true;
						}
						i++;
						if (i==myValDist.size())
						{
							myValDist.add(tempDistance);
							myVal.add(obj);
							isPlaced=true;
						}
					}
				}
			}
			
		}
		return myVal;
	}
	
	
}	
