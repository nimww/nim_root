

package com.winstaff;

import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;
import java.util.Enumeration;

public class bltProfessionalEducation extends Object implements InttProfessionalEducation
{

    dbtProfessionalEducation    dbDB;

    public bltProfessionalEducation()
    {
        dbDB = new dbtProfessionalEducation();
        dbDB.setUniqueCreateDate(new java.util.Date());
        makeRequiredHash();
        makeExpiredHash();
        makeEnglishHash();

    }    // End of default constructor

    public bltProfessionalEducation( Integer iNewID )
    {        dbDB = new dbtProfessionalEducation( iNewID );
        makeRequiredHash();
        makeExpiredHash();
        makeEnglishHash();
    }    // End of Constructor knowing an ID


    public bltProfessionalEducation( Integer iNewID, Integer iGroupSecurityID )
    {
        this.GroupSecurityInit(iGroupSecurityID);
        dbDB = new dbtProfessionalEducation( iNewID );
        makeExpiredHash();
        makeEnglishHash();
        makeRequiredHash(iGroupSecurityID);
    }    // End of Constructor knowing an ID

    public bltProfessionalEducation( Integer iGroupSecurityID, boolean isTrue )
    {
        this.GroupSecurityInit(iGroupSecurityID);
        dbDB = new dbtProfessionalEducation(  );
        makeExpiredHash();
        makeEnglishHash();
        makeRequiredHash(iGroupSecurityID);
    }    // End of Security unknown ID

    public void GroupSecurityInit( Integer iGroupSecurityID )
    {
        bltSecurityGroupMaster mySGM = new bltSecurityGroupMaster(iGroupSecurityID);
        this.UserSecurityID = iGroupSecurityID;
        if (mySGM.getRunFieldSecurityID().intValue()==1)
        {
            this.runFieldSecurity = true;
            makeRequiredHash(iGroupSecurityID);
        }
    }  

    public EventReferenceType getEventReference()
    {
        return EventUtils.getEventReference("tProfessionalEducation", this.getUniqueID());
    }  

    public String convertString2LogString(String normalString) 
    {
        String myVal = "";
        for (int i=0;i<normalString.length();i++)
        {
            if ( (normalString.charAt(i)!='\r')&&(normalString.charAt(i)!='\n')&&(normalString.charAt(i)!='\t') )
            {
                myVal+=normalString.charAt(i);
            }
            else
            {
                myVal+="<NLT>";
            }
        }
        return myVal;
    }
    public void runAudit()
    {
        String AuditString = new java.util.Date()+" \t " ;  
        AuditString += " tProfessionalEducation "; 
        AuditString += " ProfessionalEducationID ="+this.getUniqueID(); 
        AuditString += " PhysicianID ="+this.getPhysicianID(); 
        for (int i=0;i<this.AuditVector.size();i++)
        {
            AuditString+="\t"+convertString2LogString((String)this.AuditVector.elementAt(i));
        }
        AuditLogger.printLine(AuditString);
    }
    public void commitData() throws SQLException
    {
        runAudit();
        dbDB.setModifyComments(this.getUniqueModifyComments());
        dbDB.setUniqueModifyDate(new java.util.Date());
        dbDB.commitData();
        DataControlUtils.dataChanged("tProfessionalEducation",this.getPhysicianID() ,this.AuditVector.size(), "PhysicianID");
        this.AuditVector.removeAllElements();
    }    // End of commitData()

    public void commitDataForced() throws SQLException
    {
        dbDB.commitDataForced();
    }    // End of commitData()

    public Integer getUniqueID()
    {
        return dbDB.getUniqueID();
    }    // End of getUniqueID()

    public void setUniqueID( Integer iNewID )
    {
        dbDB.setUniqueID( iNewID );
    }    // End of setUniqueID()


    public void setProfessionalEducationID(Integer newValue)
    {
        dbDB.setProfessionalEducationID(newValue);
    }

    public Integer getProfessionalEducationID()
    {
        return dbDB.getProfessionalEducationID();
    }

    java.util.Vector AuditVector = new java.util.Vector();

    public void setUniqueCreateDate(Date newValue)
    {        this.setUniqueCreateDate(newValue,this.UserSecurityID);

    }
    public Date getUniqueCreateDate()
    {
        return this.getUniqueCreateDate(this.UserSecurityID);
    }

    public void setUniqueCreateDate(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='UniqueCreateDate' AND TableName='tProfessionalEducation'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {
                   this.AuditVector.addElement("[UniqueCreateDate]=["+newValue+"]");
                   this.AuditVector.addElement("[UniqueCreateDate]=["+newValue+"]");
                   dbDB.setUniqueCreateDate(newValue);
               }
            }
        }
        else
        {
           this.AuditVector.addElement("[UniqueCreateDate]=["+newValue+"]");
           this.AuditVector.addElement("[UniqueCreateDate]=["+newValue+"]");
           dbDB.setUniqueCreateDate(newValue);
         }
    }
    public Date getUniqueCreateDate(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='UniqueCreateDate' AND TableName='tProfessionalEducation'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getUniqueCreateDate();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getUniqueCreateDate();
         }
        return myVal;
    }

    public void setUniqueModifyDate(Date newValue)
    {        this.setUniqueModifyDate(newValue,this.UserSecurityID);

    }
    public Date getUniqueModifyDate()
    {
        return this.getUniqueModifyDate(this.UserSecurityID);
    }

    public void setUniqueModifyDate(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='UniqueModifyDate' AND TableName='tProfessionalEducation'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {
                   this.AuditVector.addElement("[UniqueModifyDate]=["+newValue+"]");
                   this.AuditVector.addElement("[UniqueModifyDate]=["+newValue+"]");
                   dbDB.setUniqueModifyDate(newValue);
               }
            }
        }
        else
        {
           this.AuditVector.addElement("[UniqueModifyDate]=["+newValue+"]");
           this.AuditVector.addElement("[UniqueModifyDate]=["+newValue+"]");
           dbDB.setUniqueModifyDate(newValue);
         }
    }
    public Date getUniqueModifyDate(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='UniqueModifyDate' AND TableName='tProfessionalEducation'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getUniqueModifyDate();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getUniqueModifyDate();
         }
        return myVal;
    }

    public void setUniqueModifyComments(String newValue)
    {        this.setUniqueModifyComments(newValue,this.UserSecurityID);

    }
    public String getUniqueModifyComments()
    {
        return this.getUniqueModifyComments(this.UserSecurityID);
    }

    public void setUniqueModifyComments(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='UniqueModifyComments' AND TableName='tProfessionalEducation'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {
                   this.AuditVector.addElement("[UniqueModifyComments]=["+newValue+"]");
                   this.AuditVector.addElement("[UniqueModifyComments]=["+newValue+"]");
                   dbDB.setUniqueModifyComments(newValue);
               }
            }
        }
        else
        {
           this.AuditVector.addElement("[UniqueModifyComments]=["+newValue+"]");
           this.AuditVector.addElement("[UniqueModifyComments]=["+newValue+"]");
           dbDB.setUniqueModifyComments(newValue);
         }
    }
    public String getUniqueModifyComments(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='UniqueModifyComments' AND TableName='tProfessionalEducation'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getUniqueModifyComments();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getUniqueModifyComments();
         }
        return myVal;
    }

    public void setPhysicianID(Integer newValue)
    {        this.setPhysicianID(newValue,this.UserSecurityID);

    }
    public Integer getPhysicianID()
    {
        return this.getPhysicianID(this.UserSecurityID);
    }

    public void setPhysicianID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PhysicianID' AND TableName='tProfessionalEducation'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {
                   this.AuditVector.addElement("[PhysicianID]=["+newValue+"]");
                   this.AuditVector.addElement("[PhysicianID]=["+newValue+"]");
                   dbDB.setPhysicianID(newValue);
               }
            }
        }
        else
        {
           this.AuditVector.addElement("[PhysicianID]=["+newValue+"]");
           this.AuditVector.addElement("[PhysicianID]=["+newValue+"]");
           dbDB.setPhysicianID(newValue);
         }
    }
    public Integer getPhysicianID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PhysicianID' AND TableName='tProfessionalEducation'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPhysicianID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPhysicianID();
         }
        return myVal;
    }

    public void setDegreeID(Integer newValue)
    {        this.setDegreeID(newValue,this.UserSecurityID);

    }
    public Integer getDegreeID()
    {
        return this.getDegreeID(this.UserSecurityID);
    }

    public void setDegreeID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='DegreeID' AND TableName='tProfessionalEducation'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {
                   this.AuditVector.addElement("[DegreeID]=["+newValue+"]");
                   this.AuditVector.addElement("[DegreeID]=["+newValue+"]");
                   dbDB.setDegreeID(newValue);
               }
            }
        }
        else
        {
           this.AuditVector.addElement("[DegreeID]=["+newValue+"]");
           this.AuditVector.addElement("[DegreeID]=["+newValue+"]");
           dbDB.setDegreeID(newValue);
         }
    }
    public Integer getDegreeID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='DegreeID' AND TableName='tProfessionalEducation'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getDegreeID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getDegreeID();
         }
        return myVal;
    }

    public void setOther(String newValue)
    {        this.setOther(newValue,this.UserSecurityID);

    }
    public String getOther()
    {
        return this.getOther(this.UserSecurityID);
    }

    public void setOther(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Other' AND TableName='tProfessionalEducation'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {
                   this.AuditVector.addElement("[Other]=["+newValue+"]");
                   this.AuditVector.addElement("[Other]=["+newValue+"]");
                   dbDB.setOther(newValue);
               }
            }
        }
        else
        {
           this.AuditVector.addElement("[Other]=["+newValue+"]");
           this.AuditVector.addElement("[Other]=["+newValue+"]");
           dbDB.setOther(newValue);
         }
    }
    public String getOther(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Other' AND TableName='tProfessionalEducation'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getOther();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getOther();
         }
        return myVal;
    }

    public void setStartDate(Date newValue)
    {        this.setStartDate(newValue,this.UserSecurityID);

    }
    public Date getStartDate()
    {
        return this.getStartDate(this.UserSecurityID);
    }

    public void setStartDate(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='StartDate' AND TableName='tProfessionalEducation'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {
                   this.AuditVector.addElement("[StartDate]=["+newValue+"]");
                   this.AuditVector.addElement("[StartDate]=["+newValue+"]");
                   dbDB.setStartDate(newValue);
               }
            }
        }
        else
        {
           this.AuditVector.addElement("[StartDate]=["+newValue+"]");
           this.AuditVector.addElement("[StartDate]=["+newValue+"]");
           dbDB.setStartDate(newValue);
         }
    }
    public Date getStartDate(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='StartDate' AND TableName='tProfessionalEducation'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getStartDate();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getStartDate();
         }
        return myVal;
    }

    public void setDateOfGraduation(Date newValue)
    {        this.setDateOfGraduation(newValue,this.UserSecurityID);

    }
    public Date getDateOfGraduation()
    {
        return this.getDateOfGraduation(this.UserSecurityID);
    }

    public void setDateOfGraduation(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='DateOfGraduation' AND TableName='tProfessionalEducation'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {
                   this.AuditVector.addElement("[DateOfGraduation]=["+newValue+"]");
                   this.AuditVector.addElement("[DateOfGraduation]=["+newValue+"]");
                   dbDB.setDateOfGraduation(newValue);
               }
            }
        }
        else
        {
           this.AuditVector.addElement("[DateOfGraduation]=["+newValue+"]");
           this.AuditVector.addElement("[DateOfGraduation]=["+newValue+"]");
           dbDB.setDateOfGraduation(newValue);
         }
    }
    public Date getDateOfGraduation(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='DateOfGraduation' AND TableName='tProfessionalEducation'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getDateOfGraduation();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getDateOfGraduation();
         }
        return myVal;
    }

    public void setEndDate(Date newValue)
    {        this.setEndDate(newValue,this.UserSecurityID);

    }
    public Date getEndDate()
    {
        return this.getEndDate(this.UserSecurityID);
    }

    public void setEndDate(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='EndDate' AND TableName='tProfessionalEducation'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {
                   this.AuditVector.addElement("[EndDate]=["+newValue+"]");
                   this.AuditVector.addElement("[EndDate]=["+newValue+"]");
                   dbDB.setEndDate(newValue);
               }
            }
        }
        else
        {
           this.AuditVector.addElement("[EndDate]=["+newValue+"]");
           this.AuditVector.addElement("[EndDate]=["+newValue+"]");
           dbDB.setEndDate(newValue);
         }
    }
    public Date getEndDate(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='EndDate' AND TableName='tProfessionalEducation'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getEndDate();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getEndDate();
         }
        return myVal;
    }

    public void setFocus(String newValue)
    {        this.setFocus(newValue,this.UserSecurityID);

    }
    public String getFocus()
    {
        return this.getFocus(this.UserSecurityID);
    }

    public void setFocus(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Focus' AND TableName='tProfessionalEducation'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {
                   this.AuditVector.addElement("[Focus]=["+newValue+"]");
                   this.AuditVector.addElement("[Focus]=["+newValue+"]");
                   dbDB.setFocus(newValue);
               }
            }
        }
        else
        {
           this.AuditVector.addElement("[Focus]=["+newValue+"]");
           this.AuditVector.addElement("[Focus]=["+newValue+"]");
           dbDB.setFocus(newValue);
         }
    }
    public String getFocus(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Focus' AND TableName='tProfessionalEducation'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getFocus();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getFocus();
         }
        return myVal;
    }

    public void setSchoolName(String newValue)
    {        this.setSchoolName(newValue,this.UserSecurityID);

    }
    public String getSchoolName()
    {
        return this.getSchoolName(this.UserSecurityID);
    }

    public void setSchoolName(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SchoolName' AND TableName='tProfessionalEducation'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {
                   this.AuditVector.addElement("[SchoolName]=["+newValue+"]");
                   this.AuditVector.addElement("[SchoolName]=["+newValue+"]");
                   dbDB.setSchoolName(newValue);
               }
            }
        }
        else
        {
           this.AuditVector.addElement("[SchoolName]=["+newValue+"]");
           this.AuditVector.addElement("[SchoolName]=["+newValue+"]");
           dbDB.setSchoolName(newValue);
         }
    }
    public String getSchoolName(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SchoolName' AND TableName='tProfessionalEducation'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getSchoolName();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getSchoolName();
         }
        return myVal;
    }

    public void setSchoolAddress1(String newValue)
    {        this.setSchoolAddress1(newValue,this.UserSecurityID);

    }
    public String getSchoolAddress1()
    {
        return this.getSchoolAddress1(this.UserSecurityID);
    }

    public void setSchoolAddress1(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SchoolAddress1' AND TableName='tProfessionalEducation'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {
                   this.AuditVector.addElement("[SchoolAddress1]=["+newValue+"]");
                   this.AuditVector.addElement("[SchoolAddress1]=["+newValue+"]");
                   dbDB.setSchoolAddress1(newValue);
               }
            }
        }
        else
        {
           this.AuditVector.addElement("[SchoolAddress1]=["+newValue+"]");
           this.AuditVector.addElement("[SchoolAddress1]=["+newValue+"]");
           dbDB.setSchoolAddress1(newValue);
         }
    }
    public String getSchoolAddress1(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SchoolAddress1' AND TableName='tProfessionalEducation'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getSchoolAddress1();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getSchoolAddress1();
         }
        return myVal;
    }

    public void setSchoolAddress2(String newValue)
    {        this.setSchoolAddress2(newValue,this.UserSecurityID);

    }
    public String getSchoolAddress2()
    {
        return this.getSchoolAddress2(this.UserSecurityID);
    }

    public void setSchoolAddress2(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SchoolAddress2' AND TableName='tProfessionalEducation'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {
                   this.AuditVector.addElement("[SchoolAddress2]=["+newValue+"]");
                   this.AuditVector.addElement("[SchoolAddress2]=["+newValue+"]");
                   dbDB.setSchoolAddress2(newValue);
               }
            }
        }
        else
        {
           this.AuditVector.addElement("[SchoolAddress2]=["+newValue+"]");
           this.AuditVector.addElement("[SchoolAddress2]=["+newValue+"]");
           dbDB.setSchoolAddress2(newValue);
         }
    }
    public String getSchoolAddress2(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SchoolAddress2' AND TableName='tProfessionalEducation'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getSchoolAddress2();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getSchoolAddress2();
         }
        return myVal;
    }

    public void setSchoolCity(String newValue)
    {        this.setSchoolCity(newValue,this.UserSecurityID);

    }
    public String getSchoolCity()
    {
        return this.getSchoolCity(this.UserSecurityID);
    }

    public void setSchoolCity(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SchoolCity' AND TableName='tProfessionalEducation'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {
                   this.AuditVector.addElement("[SchoolCity]=["+newValue+"]");
                   this.AuditVector.addElement("[SchoolCity]=["+newValue+"]");
                   dbDB.setSchoolCity(newValue);
               }
            }
        }
        else
        {
           this.AuditVector.addElement("[SchoolCity]=["+newValue+"]");
           this.AuditVector.addElement("[SchoolCity]=["+newValue+"]");
           dbDB.setSchoolCity(newValue);
         }
    }
    public String getSchoolCity(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SchoolCity' AND TableName='tProfessionalEducation'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getSchoolCity();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getSchoolCity();
         }
        return myVal;
    }

    public void setSchoolStateID(Integer newValue)
    {        this.setSchoolStateID(newValue,this.UserSecurityID);

    }
    public Integer getSchoolStateID()
    {
        return this.getSchoolStateID(this.UserSecurityID);
    }

    public void setSchoolStateID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SchoolStateID' AND TableName='tProfessionalEducation'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {
                   this.AuditVector.addElement("[SchoolStateID]=["+newValue+"]");
                   this.AuditVector.addElement("[SchoolStateID]=["+newValue+"]");
                   dbDB.setSchoolStateID(newValue);
               }
            }
        }
        else
        {
           this.AuditVector.addElement("[SchoolStateID]=["+newValue+"]");
           this.AuditVector.addElement("[SchoolStateID]=["+newValue+"]");
           dbDB.setSchoolStateID(newValue);
         }
    }
    public Integer getSchoolStateID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SchoolStateID' AND TableName='tProfessionalEducation'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getSchoolStateID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getSchoolStateID();
         }
        return myVal;
    }

    public void setSchoolProvince(String newValue)
    {        this.setSchoolProvince(newValue,this.UserSecurityID);

    }
    public String getSchoolProvince()
    {
        return this.getSchoolProvince(this.UserSecurityID);
    }

    public void setSchoolProvince(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SchoolProvince' AND TableName='tProfessionalEducation'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {
                   this.AuditVector.addElement("[SchoolProvince]=["+newValue+"]");
                   this.AuditVector.addElement("[SchoolProvince]=["+newValue+"]");
                   dbDB.setSchoolProvince(newValue);
               }
            }
        }
        else
        {
           this.AuditVector.addElement("[SchoolProvince]=["+newValue+"]");
           this.AuditVector.addElement("[SchoolProvince]=["+newValue+"]");
           dbDB.setSchoolProvince(newValue);
         }
    }
    public String getSchoolProvince(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SchoolProvince' AND TableName='tProfessionalEducation'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getSchoolProvince();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getSchoolProvince();
         }
        return myVal;
    }

    public void setSchoolZIP(String newValue)
    {        this.setSchoolZIP(newValue,this.UserSecurityID);

    }
    public String getSchoolZIP()
    {
        return this.getSchoolZIP(this.UserSecurityID);
    }

    public void setSchoolZIP(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SchoolZIP' AND TableName='tProfessionalEducation'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {
                   this.AuditVector.addElement("[SchoolZIP]=["+newValue+"]");
                   this.AuditVector.addElement("[SchoolZIP]=["+newValue+"]");
                   dbDB.setSchoolZIP(newValue);
               }
            }
        }
        else
        {
           this.AuditVector.addElement("[SchoolZIP]=["+newValue+"]");
           this.AuditVector.addElement("[SchoolZIP]=["+newValue+"]");
           dbDB.setSchoolZIP(newValue);
         }
    }
    public String getSchoolZIP(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SchoolZIP' AND TableName='tProfessionalEducation'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getSchoolZIP();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getSchoolZIP();
         }
        return myVal;
    }

    public void setSchoolCountryID(Integer newValue)
    {        this.setSchoolCountryID(newValue,this.UserSecurityID);

    }
    public Integer getSchoolCountryID()
    {
        return this.getSchoolCountryID(this.UserSecurityID);
    }

    public void setSchoolCountryID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SchoolCountryID' AND TableName='tProfessionalEducation'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {
                   this.AuditVector.addElement("[SchoolCountryID]=["+newValue+"]");
                   this.AuditVector.addElement("[SchoolCountryID]=["+newValue+"]");
                   dbDB.setSchoolCountryID(newValue);
               }
            }
        }
        else
        {
           this.AuditVector.addElement("[SchoolCountryID]=["+newValue+"]");
           this.AuditVector.addElement("[SchoolCountryID]=["+newValue+"]");
           dbDB.setSchoolCountryID(newValue);
         }
    }
    public Integer getSchoolCountryID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SchoolCountryID' AND TableName='tProfessionalEducation'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getSchoolCountryID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getSchoolCountryID();
         }
        return myVal;
    }

    public void setSchoolPhone(String newValue)
    {        this.setSchoolPhone(newValue,this.UserSecurityID);

    }
    public String getSchoolPhone()
    {
        return this.getSchoolPhone(this.UserSecurityID);
    }

    public void setSchoolPhone(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SchoolPhone' AND TableName='tProfessionalEducation'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {
                   this.AuditVector.addElement("[SchoolPhone]=["+newValue+"]");
                   this.AuditVector.addElement("[SchoolPhone]=["+newValue+"]");
                   dbDB.setSchoolPhone(newValue);
               }
            }
        }
        else
        {
           this.AuditVector.addElement("[SchoolPhone]=["+newValue+"]");
           this.AuditVector.addElement("[SchoolPhone]=["+newValue+"]");
           dbDB.setSchoolPhone(newValue);
         }
    }
    public String getSchoolPhone(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SchoolPhone' AND TableName='tProfessionalEducation'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getSchoolPhone();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getSchoolPhone();
         }
        return myVal;
    }

    public void setSchoolFax(String newValue)
    {        this.setSchoolFax(newValue,this.UserSecurityID);

    }
    public String getSchoolFax()
    {
        return this.getSchoolFax(this.UserSecurityID);
    }

    public void setSchoolFax(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SchoolFax' AND TableName='tProfessionalEducation'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {
                   this.AuditVector.addElement("[SchoolFax]=["+newValue+"]");
                   this.AuditVector.addElement("[SchoolFax]=["+newValue+"]");
                   dbDB.setSchoolFax(newValue);
               }
            }
        }
        else
        {
           this.AuditVector.addElement("[SchoolFax]=["+newValue+"]");
           this.AuditVector.addElement("[SchoolFax]=["+newValue+"]");
           dbDB.setSchoolFax(newValue);
         }
    }
    public String getSchoolFax(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SchoolFax' AND TableName='tProfessionalEducation'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getSchoolFax();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getSchoolFax();
         }
        return myVal;
    }

    public void setContactName(String newValue)
    {        this.setContactName(newValue,this.UserSecurityID);

    }
    public String getContactName()
    {
        return this.getContactName(this.UserSecurityID);
    }

    public void setContactName(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ContactName' AND TableName='tProfessionalEducation'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {
                   this.AuditVector.addElement("[ContactName]=["+newValue+"]");
                   this.AuditVector.addElement("[ContactName]=["+newValue+"]");
                   dbDB.setContactName(newValue);
               }
            }
        }
        else
        {
           this.AuditVector.addElement("[ContactName]=["+newValue+"]");
           this.AuditVector.addElement("[ContactName]=["+newValue+"]");
           dbDB.setContactName(newValue);
         }
    }
    public String getContactName(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ContactName' AND TableName='tProfessionalEducation'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getContactName();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getContactName();
         }
        return myVal;
    }

    public void setContactEmail(String newValue)
    {        this.setContactEmail(newValue,this.UserSecurityID);

    }
    public String getContactEmail()
    {
        return this.getContactEmail(this.UserSecurityID);
    }

    public void setContactEmail(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ContactEmail' AND TableName='tProfessionalEducation'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {
                   this.AuditVector.addElement("[ContactEmail]=["+newValue+"]");
                   this.AuditVector.addElement("[ContactEmail]=["+newValue+"]");
                   dbDB.setContactEmail(newValue);
               }
            }
        }
        else
        {
           this.AuditVector.addElement("[ContactEmail]=["+newValue+"]");
           this.AuditVector.addElement("[ContactEmail]=["+newValue+"]");
           dbDB.setContactEmail(newValue);
         }
    }
    public String getContactEmail(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ContactEmail' AND TableName='tProfessionalEducation'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getContactEmail();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getContactEmail();
         }
        return myVal;
    }

    public void setComments(String newValue)
    {        this.setComments(newValue,this.UserSecurityID);

    }
    public String getComments()
    {
        return this.getComments(this.UserSecurityID);
    }

    public void setComments(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Comments' AND TableName='tProfessionalEducation'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {
                   this.AuditVector.addElement("[Comments]=["+newValue+"]");
                   this.AuditVector.addElement("[Comments]=["+newValue+"]");
                   dbDB.setComments(newValue);
               }
            }
        }
        else
        {
           this.AuditVector.addElement("[Comments]=["+newValue+"]");
           this.AuditVector.addElement("[Comments]=["+newValue+"]");
           dbDB.setComments(newValue);
         }
    }
    public String getComments(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Comments' AND TableName='tProfessionalEducation'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getComments();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getComments();
         }
        return myVal;
    }

    public void setRequiredFieldList(java.util.Hashtable requiredFieldList)
	 {

        this.requiredFieldList = requiredFieldList;
    }

    public java.util.Hashtable getRequiredFieldList()
    {
        return this.requiredFieldList;
    }
    public void setExpiredFieldList(java.util.Hashtable expiredFieldList)
    {
        this.expiredFieldList = expiredFieldList;
    }

    public void setEnglishFieldList(java.util.Hashtable englishFieldList)
    {
        this.englishFieldList = englishFieldList;
    }

    public java.util.Hashtable getExpiredFieldList()
    {
        return this.expiredFieldList;
    }
    public boolean isExpired(java.util.Date myDate, int daysVector)
    {
       return isExpired(myDate,new java.util.Date(),daysVector);
    }


    public boolean isRead(String fieldN, Integer GroupRefID)
    {
        boolean myVal = false;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='"+fieldN+"' AND TableName='tProfessionalEducation'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==1)
               {
                   myVal = true;
               }
            }
        }
        else
        {
           Integer iSecurityCheck = SecurityCheck.CheckItem("PractitionerSection6", GroupRefID);
           if (iSecurityCheck.intValue()>=1)
           {
                  myVal = true;
           }
        }
        return myVal;
    }


    public boolean isWrite(String fieldN, Integer GroupRefID)
    {
        boolean myVal = false;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='"+fieldN+"' AND TableName='tProfessionalEducation'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {
                   myVal = true;
               }
            }
        }
        else
        {
           Integer iSecurityCheck = SecurityCheck.CheckItem("PractitionerSection6", GroupRefID);
           if (iSecurityCheck.intValue()>=2)
           {
                  myVal = true;
           }
        }
        return myVal;
    }


    public boolean isExpired(java.util.Date myDate, java.util.Date refDate,int daysVector)
    {
        java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat("yyyy-MM-dd");
        boolean myVal = true;
        try
        {
            if (myDate.after(dbdf.parse("1800-01-10")))
            {
                if (myDate.after(dateChange(refDate,daysVector)))
                {
                    myVal=false;
                }
            }
            else
            {
                myVal = false;
            }
        }
        catch (Exception Datee)
        {
        }
        return myVal;
    }

    public java.util.Date dateChange(java.util.Date myD, int dayChange)
    {
        java.util.Calendar calendar = java.util.Calendar.getInstance();
        calendar.setTime(myD);
        calendar.add(java.util.Calendar.DATE, dayChange);
        java.util.Date resultAsDate = new java.util.Date(calendar.getTime().getTime());
        return resultAsDate;
    }

    public boolean isRequired(String fieldN)
    {
        return isRequired(fieldN, new Integer("1"));
    }

    public boolean isRequired(String fieldN, Integer GroupRefID)
    {
        boolean myVal = false;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='"+fieldN+"' AND TableName='tProfessionalEducation'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getRequiredID().intValue()==1)
               {
                   myVal = true;
               }
            }
        }
        else
        {
            if (this.getRequiredFieldList().containsKey(fieldN))
            {
                myVal = true;
            }
        }
        return myVal;
    }
    public boolean isExpiredCheck(String fieldN)
    {
        return isExpiredCheck(fieldN, new Integer ("1"));
    }

    public boolean isExpiredCheck(String fieldN, Integer GroupRefID)
    {
        boolean myVal = false;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='"+fieldN+"' AND TableName='tProfessionalEducation'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getTrackedID().intValue()==1)
               {
                   myVal = true;
               }
            }
        }
        else
        {
            if (this.getExpiredFieldList().containsKey(fieldN))
           {
                myVal = true;
            }
        }
        return myVal;
    }

    public void makeRequiredHash()
    {
       makeRequiredHash(new Integer(1));
    }

    public void makeRequiredHash(Integer GroupRefID)
    {

        java.util.Hashtable newHash = new java.util.Hashtable();
        if (runFieldSecurity)
        {
            java.util.Enumeration myKeys = englishFieldList.keys(); 
            while (myKeys.hasMoreElements()) 
            {
                 String myElement = (String) myKeys.nextElement();
                 bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='"+myElement+"' AND TableName='tProfessionalEducation'" ,"");
                 bltFieldSecurity        working_bltFieldSecurity;
                 ListElement         leCurrentElement;
                 Enumeration eList = myFSL.elements();
                 if (eList.hasMoreElements())
                 {
                     leCurrentElement    = (ListElement) eList.nextElement();
                     working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
                     if (working_bltFieldSecurity.getRequiredID().intValue()==1)
                     {
                         newHash.put(myElement,new Boolean(true));
                     }
                 }
            }
        }
        else
        {
            newHash.put("StartDate",new Boolean(true));
            newHash.put("Focus",new Boolean(true));
            newHash.put("SchoolName",new Boolean(true));
            newHash.put("SchoolAddress1",new Boolean(true));
            newHash.put("SchoolCity",new Boolean(true));
            newHash.put("SchoolZIP",new Boolean(true));
            newHash.put("SchoolCountryID",new Boolean(true));
        }
        this.setRequiredFieldList(newHash);
    }

    public void makeEnglishHash()
    {

        java.util.Hashtable newHash = new java.util.Hashtable();        newHash.put("UniqueCreateDate","Item Create Date");
        newHash.put("UniqueModifyDate","Item Modify Date");
        newHash.put("UniqueModifyComments","Item Modification Comments");
        newHash.put("PhysicianID","PhysicianID");
        newHash.put("DegreeID","Degree Type");
        newHash.put("Other","If Other, list here");
        newHash.put("StartDate","Start Date");
        newHash.put("DateOfGraduation","Graduation Date");
        newHash.put("EndDate","End Date (if not graduated)");
        newHash.put("Focus","Major/Focus");
        newHash.put("SchoolName","School Name");
        newHash.put("SchoolAddress1","Address");
        newHash.put("SchoolAddress2","Address 2");
        newHash.put("SchoolCity","City");
        newHash.put("SchoolStateID","State");
        newHash.put("SchoolProvince","Province, District, State");
        newHash.put("SchoolZIP","ZIP");
        newHash.put("SchoolCountryID","Country");
        newHash.put("SchoolPhone","School Phone (XXX-XXX-XXXX)");
        newHash.put("SchoolFax","Fax (XXX-XXX-XXXX)");
        newHash.put("ContactName","Contact Name");
        newHash.put("ContactEmail","Contact E-mail");
        newHash.put("Comments","Extra Comments");

        this.setEnglishFieldList(newHash);
    }
    public void makeExpiredHash()
    {

        java.util.Hashtable newHash = new java.util.Hashtable();
        this.setExpiredFieldList(newHash);
    }

    public java.util.Hashtable englishFieldList = new java.util.Hashtable();
    public java.util.Hashtable requiredFieldList = new java.util.Hashtable();
    public java.util.Hashtable expiredFieldList = new java.util.Hashtable();

    public boolean isComplete()
    {
	     boolean myVal = true;
	     java.util.Enumeration myKeys = this.getRequiredFieldList().keys(); 
	     while (myKeys.hasMoreElements()) 
	     {
	         String myElement = (String) myKeys.nextElement();
	         if (!isComplete(myElement))
	         {
	             myVal = false;
	         }
	     }
	     return myVal;
    }

    public String getEnglish(String fieldN)
    {
        String myVal = null;
        if (this.englishFieldList.containsKey(fieldN))
        {
            myVal = (String)this.englishFieldList.get(fieldN);
        }
        return myVal;
    }
    public java.util.Vector isComplete(int daysVector)
    {
	     boolean myVal = false;
	     java.util.Enumeration myKeys = this.getExpiredFieldList().keys(); 
	     java.util.Vector myExpiredList = new java.util.Vector(); 
	     while (myKeys.hasMoreElements()) 
	     {
	         String myElement = (String) myKeys.nextElement();
	         if (isExpired(myElement,daysVector))
	         {
	             myVal = true;
	             myExpiredList.addElement(myElement);
	         }
	     }
	     return myExpiredList;
    }

    public void setField(String fieldN, Object fieldV)
    {
     boolean myVal = false;
     try
     {
        java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat("yyyy-MM-dd");

        if (fieldN.equals("ProfessionalEducationID"))
        {
             this.setProfessionalEducationID((Integer)fieldV);
        }

        else if (fieldN.equals("UniqueCreateDate"))
        {
            this.setUniqueCreateDate((java.util.Date)fieldV);
        }

        else if (fieldN.equals("UniqueModifyDate"))
        {
            this.setUniqueModifyDate((java.util.Date)fieldV);
        }

        else if (fieldN.equals("UniqueModifyComments"))
        {
            this.setUniqueModifyComments((String)fieldV);
        }

        else if (fieldN.equals("PhysicianID"))
        {
             this.setPhysicianID((Integer)fieldV);
        }

        else if (fieldN.equals("DegreeID"))
        {
             this.setDegreeID((Integer)fieldV);
        }

        else if (fieldN.equals("Other"))
        {
            this.setOther((String)fieldV);
        }

        else if (fieldN.equals("StartDate"))
        {
            this.setStartDate((java.util.Date)fieldV);
        }

        else if (fieldN.equals("DateOfGraduation"))
        {
            this.setDateOfGraduation((java.util.Date)fieldV);
        }

        else if (fieldN.equals("EndDate"))
        {
            this.setEndDate((java.util.Date)fieldV);
        }

        else if (fieldN.equals("Focus"))
        {
            this.setFocus((String)fieldV);
        }

        else if (fieldN.equals("SchoolName"))
        {
            this.setSchoolName((String)fieldV);
        }

        else if (fieldN.equals("SchoolAddress1"))
        {
            this.setSchoolAddress1((String)fieldV);
        }

        else if (fieldN.equals("SchoolAddress2"))
        {
            this.setSchoolAddress2((String)fieldV);
        }

        else if (fieldN.equals("SchoolCity"))
        {
            this.setSchoolCity((String)fieldV);
        }

        else if (fieldN.equals("SchoolStateID"))
        {
             this.setSchoolStateID((Integer)fieldV);
        }

        else if (fieldN.equals("SchoolProvince"))
        {
            this.setSchoolProvince((String)fieldV);
        }

        else if (fieldN.equals("SchoolZIP"))
        {
            this.setSchoolZIP((String)fieldV);
        }

        else if (fieldN.equals("SchoolCountryID"))
        {
             this.setSchoolCountryID((Integer)fieldV);
        }

        else if (fieldN.equals("SchoolPhone"))
        {
            this.setSchoolPhone((String)fieldV);
        }

        else if (fieldN.equals("SchoolFax"))
        {
            this.setSchoolFax((String)fieldV);
        }

        else if (fieldN.equals("ContactName"))
        {
            this.setContactName((String)fieldV);
        }

        else if (fieldN.equals("ContactEmail"))
        {
            this.setContactEmail((String)fieldV);
        }

        else if (fieldN.equals("Comments"))
        {
            this.setComments((String)fieldV);
        }

	    }
	    catch(Exception e2)
	    {}
	    //return myVal;
	}

    public String getFieldType(String fieldN)
    {
     String  myVal = null;
     try
     {
        java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat("yyyy-MM-dd");

        if (fieldN.equals("ProfessionalEducationID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("UniqueCreateDate"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("UniqueModifyDate"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("UniqueModifyComments"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("PhysicianID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("DegreeID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("Other"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("StartDate"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("DateOfGraduation"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("EndDate"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("Focus"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("SchoolName"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("SchoolAddress1"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("SchoolAddress2"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("SchoolCity"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("SchoolStateID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("SchoolProvince"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("SchoolZIP"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("SchoolCountryID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("SchoolPhone"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("SchoolFax"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("ContactName"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("ContactEmail"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("Comments"))
        {
            myVal = "String";
        }

	    }
	    catch(Exception e2)
	    {}
	    return myVal;
	}

    public boolean isComplete(String fieldN)
    {
	     boolean myVal = false;
	     try
	     {
        java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat("yyyy-MM-dd");

	     if (fieldN.equals("ProfessionalEducationID"))

	     {
                    if (this.getProfessionalEducationID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("UniqueCreateDate"))

	     {
                    if (this.getUniqueCreateDate().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("UniqueModifyDate"))

	     {
                    if (this.getUniqueModifyDate().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("UniqueModifyComments"))

	     {
                    if (this.getUniqueModifyComments().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("PhysicianID"))

	     {
                    if (this.getPhysicianID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("DegreeID"))

	     {
                    if (this.getDegreeID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("Other"))

	     {
                    if (this.getOther().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("StartDate"))

	     {
                    if (this.getStartDate().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("DateOfGraduation"))

	     {
                    if (this.getDateOfGraduation().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("EndDate"))

	     {
                    if (this.getEndDate().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("Focus"))

	     {
                    if (this.getFocus().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("SchoolName"))

	     {
                    if (this.getSchoolName().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("SchoolAddress1"))

	     {
                    if (this.getSchoolAddress1().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("SchoolAddress2"))

	     {
                    if (this.getSchoolAddress2().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("SchoolCity"))

	     {
                    if (this.getSchoolCity().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("SchoolStateID"))

	     {
                    if (this.getSchoolStateID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("SchoolProvince"))

	     {
                    if (this.getSchoolProvince().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("SchoolZIP"))

	     {
                    if (this.getSchoolZIP().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("SchoolCountryID"))

	     {
                    if (this.getSchoolCountryID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("SchoolPhone"))

	     {
                    if (this.getSchoolPhone().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("SchoolFax"))

	     {
                    if (this.getSchoolFax().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("ContactName"))

	     {
                    if (this.getContactName().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("ContactEmail"))

	     {
                    if (this.getContactEmail().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("Comments"))

	     {
                    if (this.getComments().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	    }
	    catch(Exception e2)
	    {}
	    return myVal;
	}

    public boolean isExpired(String fieldN,int daysVector)
    {
        return isExpired(fieldN,new java.util.Date(),daysVector);
    }

    public boolean isExpired(String fieldN,java.util.Date refDate,int daysVector)
    {
	     boolean myVal = false;
	     try
	     {
        java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat("yyyy-MM-dd");

        if (fieldN.equals("UniqueCreateDate"))

	     {
                    if (this.isExpired(this.getUniqueCreateDate(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

        if (fieldN.equals("UniqueModifyDate"))

	     {
                    if (this.isExpired(this.getUniqueModifyDate(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

        if (fieldN.equals("StartDate"))

	     {
                    if (this.isExpired(this.getStartDate(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

        if (fieldN.equals("DateOfGraduation"))

	     {
                    if (this.isExpired(this.getDateOfGraduation(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

        if (fieldN.equals("EndDate"))

	     {
                    if (this.isExpired(this.getEndDate(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

	    }
	    catch(Exception e2)
	    {}
	    return myVal;
	}

    public java.util.Date getDateField(String fieldN)
    {
	     java.util.Date myVal = null;
	     try
	     {
        java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat("yyyy-MM-dd");

        if (fieldN.equals("UniqueCreateDate"))

	     {
             myVal = this.getUniqueCreateDate();
        }

        if (fieldN.equals("UniqueModifyDate"))

	     {
             myVal = this.getUniqueModifyDate();
        }

        if (fieldN.equals("StartDate"))

	     {
             myVal = this.getStartDate();
        }

        if (fieldN.equals("DateOfGraduation"))

	     {
             myVal = this.getDateOfGraduation();
        }

        if (fieldN.equals("EndDate"))

	     {
             myVal = this.getEndDate();
        }

	    }
	    catch(Exception e2)
	    {}
	    return myVal;
	}
	public boolean runFieldSecurity = false;
	public Integer UserSecurityID = null;

}    // End of bltProfessionalEducation class definition
