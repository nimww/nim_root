package com.winstaff;


import com.winstaff.dbtSupportBase;
import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public interface InttSupportBase extends dbTableInterface
{

    public void setSupportBaseID(Integer newValue);
    public Integer getSupportBaseID();
    public void setUniqueCreateDate(Date newValue);
    public Date getUniqueCreateDate();
    public void setUniqueModifyDate(Date newValue);
    public Date getUniqueModifyDate();
    public void setUniqueModifyComments(String newValue);
    public String getUniqueModifyComments();
    public void setCompanyID(Integer newValue);
    public Integer getCompanyID();
    public void setIsComplete(Integer newValue);
    public Integer getIsComplete();
    public void setPhysicianID(Integer newValue);
    public Integer getPhysicianID();
    public void setPLCID(Integer newValue);
    public Integer getPLCID();
    public void setAdminID(Integer newValue);
    public Integer getAdminID();
    public void setUserID(Integer newValue);
    public Integer getUserID();
    public void setContactName(String newValue);
    public String getContactName();
    public void setContactEmail(String newValue);
    public String getContactEmail();
    public void setCompanyName(String newValue);
    public String getCompanyName();
    public void setPhone(String newValue);
    public String getPhone();
    public void setFax(String newValue);
    public String getFax();
    public void setAddress1(String newValue);
    public String getAddress1();
    public void setAddress2(String newValue);
    public String getAddress2();
    public void setCity(String newValue);
    public String getCity();
    public void setStateID(Integer newValue);
    public Integer getStateID();
    public void setProvince(String newValue);
    public String getProvince();
    public void setZIP(String newValue);
    public String getZIP();
    public void setCountryID(Integer newValue);
    public Integer getCountryID();
    public void setSupportTypeID(Integer newValue);
    public Integer getSupportTypeID();
    public void setSubject(String newValue);
    public String getSubject();
    public void setProblem(String newValue);
    public String getProblem();
    public void setSolution(String newValue);
    public String getSolution();
    public void setComments(String newValue);
    public String getComments();
}    // End of bltSupportBase class definition
