

package com.winstaff;

import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public class dbtEncounterStatusLI extends Object implements InttEncounterStatusLI
{

        db_NewBase    dbnbDB;

    public dbtEncounterStatusLI()
    {
        dbnbDB = new db_NewBase( "tEncounterStatusLI", "EncounterStatusID" );

    }    // End of default constructor

    public dbtEncounterStatusLI( Integer iNewID )
    {
        dbnbDB = new db_NewBase( "tEncounterStatusLI", "EncounterStatusID" );
        setUniqueID( iNewID );
    }    // End of Constructor knowing an ID

    public void commitData() throws SQLException
    {
            //no longer trapping/ignoring errors 2011-03-17
            dbnbDB.commitData();
    }    // End of commitData()

    public void commitDataForced() throws SQLException
    {
        dbnbDB.commitDataForced();
    }    // End of commitData()

    public Integer getUniqueID()
    {
        return dbnbDB.getUniqueID();
    }    // End of getUniqueID()

    public void setUniqueID( Integer iNewID )
    {
        dbnbDB.setUniqueID( iNewID );
    }    // End of setUniqueID()

    public void setModifyComments( String sNew )
    {
        dbnbDB.setModifyComments( sNew );
    }    // End of setUniqueID()


    public void setEncounterStatusID(Integer newValue)
    {
                dbnbDB.setFieldData( "EncounterStatusID", newValue.toString() );
    }

    public Integer getEncounterStatusID()
    {
        String           sValue = dbnbDB.getFieldData( "EncounterStatusID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setField(String fieldN, Object fieldV)
    {DebugLogger.println("setField Method not implemented yet in class!");}

    public String getFieldType(String fieldN)
    {return null;}


    public void setDescriptionLong(String newValue)
    {
                dbnbDB.setFieldData( "DescriptionLong", newValue.toString() );
    }

    public String getDescriptionLong()
    {
        String           sValue = dbnbDB.getFieldData( "DescriptionLong" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

}    // End of bltEncounterStatusLI class definition
