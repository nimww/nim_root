

package com.winstaff;

import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public interface InttNIM3_NetDevDataTableSupport extends dbTableInterface
{

    public void setNetDevDataTableSupportID(Integer newValue);
    public Integer getNetDevDataTableSupportID();
    public void setUniqueCreateDate(Date newValue);
    public Date getUniqueCreateDate();
    public void setUniqueModifyDate(Date newValue);
    public Date getUniqueModifyDate();
    public void setKey(String newValue);
    public String getKey();
    public void setValue(String newValue);
    public String getValue();
    public void setNetDevDataTableID(Integer newValue);
    public Integer getNetDevDataTableID();
}    // End of bltNIM3_NetDevDataTableSupport class definition
