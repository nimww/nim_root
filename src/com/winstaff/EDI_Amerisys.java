package com.winstaff;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.net.ftp.FTPReply;
import org.apache.commons.net.ftp.FTPSClient;

/**
 * 
 * This file uses a different FTP connection methodology than the
 * 'EDI_Crawford.java' method. The difference is that EDI_Crawford uses SFTP and
 * Amerisys is using FTPS.
 * 
 * FTPS extends the FTP protocol with support for SSL and TLS. Explicit security
 * requires that the FTP client sends an explicit command (AUTH SSL or AUTH TLS)
 * to the FTP server to initiate a secure control connection.
 * 
 * The default FTP server port (21) is used. The data connection can also be
 * secured using the commands PBSZ and PROT.
 * 
 * See http://www.kochnielsen.dk/kurt/blog/?p=162 for more details.
 * 
 * 
 * 
 * NOTES:
 * 
 * The combination of the claim_id, database Id, and the AmeriSys Payer
 * Indicator fields will identify the unique claim to a given database.
 * 
 * Ancillary Database ID
 * 
 * Database Name Database ID 
 * ------------- ----------- 
 * Amerisys 		1 
 * USIS 			2 
 * PGCS 			4
 * 
 * 
 */
public class EDI_Amerisys {

	ConfigurationInformation config = new ConfigurationInformation();

	// Hard-coded value for Amerisys Payer ID
	int amerisysPayerID = 307;
	
	// Local file system
	String localIncomingDir = "/downloads/amerisys/incoming/";
	String localOutboundDir = "/downloads/amerisys/outgoing/";

	// Directories on Amerisys FTP site
	String remoteInboundDir = "/nextimage/Incoming/" +
			"";
	String remoteOutboundDir = "/nextimage/Outgoing/";

	// Some filenames for testing.
	String localOutboundFilename = localOutboundDir + "jose.txt";
	String remoteInboundFilename = remoteInboundDir + "to_amerisys.txt";

	String host = "ftp.amerisys-info.com";
	String user = "NextImage";
	String password = "Bptx#R*Np";
	int port = 21;

	public void putFile_test() {
		putFile(host, port, user, password, localOutboundFilename,
				remoteInboundFilename);
	}

	public void deleteRemoteOutBoundFile(String filename) {
		deleteRemoteOutBoundFile(host, port, user, password, remoteOutboundDir+filename);
	}

	public void getRemoteOutBoundFiles() {
		getRemoteOutBoundFiles(host, port, user, password, remoteOutboundDir);
	}

	public void runOutBoundTests() {
		putFile_test(); 					// Test for putting file up on Amerisys site.
		deleteRemoteOutBoundFile("note"); 	// Test for deleting a file up on Amerisys site.
	}


	static public void main_test(String args[]) {

		System.out.println("\n--------------------------------------");
		EDI_Amerisys edi = new EDI_Amerisys();		
		edi.updateCaseAccountInfo();
		System.out.println("\n-------------------------------------.");

	}
	
	/**
	 * 
	 * @param args
	 * 
	 */
	static public void main(String args[]) {

		System.out.println("\nStarting EDI_Amerisys -- ***");

		EDI_Amerisys edi = new EDI_Amerisys();
		
		edi.getRemoteOutBoundFiles();
		edi.processFiles();
		
		// Now go back and update some case account information.
		edi.updateCaseAccountInfo();
		
		System.out.println("\nFinished with EDI_Amerisys.");

	}

	/**
	 * 
	 * Put data into the eligibility table.
	 * 
	 * Go through the .txt files in the local directory & put data into the eligibility table.
	 * Move the files to another directory and go back and nuke the files on the Amerisys FTP site.
	 * 
	 */
	public void processFiles() {

		String path = localIncomingDir;
		String files;
		File folder = new File(path);
		
		// The array File[] will have non .txt files included as well.
		// Remember to ignore these files later on down in the method.
		
		File[] listOfFiles = folder.listFiles();

		// Counter used for testing.
		int testCounter = 0;

		for (int i = 0; i < listOfFiles.length; i++) {
			if (listOfFiles[i].isFile()) {
				files = listOfFiles[i].getName();
				if (files.endsWith(".txt") || files.endsWith(".TXT")) {
					System.out.println(files);
					try {
						tabFileReader(path + files);
						testCounter++;
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
			// if (testCounter==1) break;
		}
		
		// Now move these files into a sub-directory named after current month-day-year
		// Now delete these named files on the Amerisys FTP site.
		
        Calendar cal = Calendar.getInstance();
        int day = cal.get(Calendar.DATE);
        int month = cal.get(Calendar.MONTH) + 1;
        int year = cal.get(Calendar.YEAR);
        String dir = path+month+"-"+day+"-"+year;
        
        boolean exists = (new File(dir)).exists();
        if (exists) {
            // File or directory exists        	
        } else {
            // File or directory does not exist
    		bashCommand("mkdir "+dir);	        	
        }	
        
		for (int i = 0; i < listOfFiles.length; i++) {
			if (listOfFiles[i].isFile()) {
				files = listOfFiles[i].getName();
				
				// Ignore the .dat files.  Keep these files up on the server as our partner
				// and most beloved associate Pyramid Peak needs these files.  Let them do with them 
				// what they wish.
				if (files.endsWith("txt") || files.endsWith("TXT")) {
					// move the local files
					bashCommand("mv "+files +" " + dir);
					// move appears to copy (bug? 7/29/12)
					bashCommand("rm -f "+files);
					// delete the remote files
					deleteRemoteOutBoundFile(files);
				}
			}
		}
	
		
	}

	/**
	 * 
	 * Examine the tCaseAccount table for records that has a payerId for which Amerisys is the parent payer.
	 * 
	 * If you can find matching records there with records from the tnim3_eligibilityreference table
	 * then update an empty SSN and/or EmployerName field (in the tCaseAccount table)
	 * with the non-null value from the tnim3_eligibilityreference table.
	 * 
	 */
	public void updateCaseAccountInfo () {
		
		ArrayList<Integer> payerArr = new ArrayList<Integer>();

		//
		// Get the payers who have 'Amerisys' as its parent payer .
		// These are the ids in the case account table that we are interested in.
		//
		try {
			java.sql.Connection cCon = java.sql.DriverManager.getConnection(
					ConfigurationInformation.sURL,
					ConfigurationInformation.sUser,
					ConfigurationInformation.sPassword);

			String stmt = "SELECT payerid FROM tnim3_payermaster WHERE parentpayerid = '"
					+ amerisysPayerID + "'";
			System.out.println("stmt= " + stmt);			
			PreparedStatement ps = cCon.prepareStatement(stmt);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				payerArr.add(rs.getInt(1));
			}
			rs.close();
			ps.close();
			cCon.close();
		} catch (Exception eeee) {
			System.out.println("Table access Error: " + eeee.toString());
		}		
		
		StringBuffer sb = new StringBuffer();
		sb.append("(");
		for (int i=0;i<payerArr.size()-1;i++) {
			sb.append("'"+payerArr.get(i)+"',");
		}
		sb.append("'"+payerArr.get(payerArr.size()-1)+"'");		
		sb.append(")");
		
		//
		// Get the records in the case account table with these child payer ids.
		// Attempt to match based on first & last name, dob, & gender.
		// with either blank SSN's and/or blank employer name.  Join with the eligibility reference table.
		//
		
		String stmt1 = "SELECT er.patientssn, er.employername, ca.caseid FROM tnim3_caseaccount ca, tnim3_eligibilityreference er WHERE ca.payerid IN " + sb.toString() + " AND ";
		String stmt2 = "ca.employername='' AND UPPER(TRIM(ca.patientfirstname)) = UPPER(TRIM(er.patientfirstname)) AND UPPER(TRIM(ca.patientlastname)) =  UPPER(TRIM(er.patientlastname)) AND ";
		String stmt3 = "ca.patientdob = er.patientdob AND ca.patientgender = er.patientgender AND (";
		String stmt4 = "ca.patientssn = 'bLBW9xM+GtW8ANm32hAL2Q==' OR ";
		String stmt5 = "ca.employername ='')";
		
		//
		// Resolve SSN and employer name.
		//
		try {
			java.sql.Connection cCon = java.sql.DriverManager.getConnection(
					ConfigurationInformation.sURL,
					ConfigurationInformation.sUser,
					ConfigurationInformation.sPassword);

			String ssnStmt = stmt1+stmt2+stmt3+stmt4+stmt5;
			System.out.println("SSN statement = " + ssnStmt);			
			PreparedStatement ps = cCon.prepareStatement(ssnStmt);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				
				// Update SSN field with value from tnim3_eligibilityreference, but you need to encode it.
				int ssn = rs.getInt(1);
				String employerName = rs.getString(2);
				int caseId = rs.getInt(3);
				System.out.println("\nSSN update value = " + ssn);
				System.out.println("\nEmp Name update value = " + employerName);
				System.out.println("CA caseid = " + caseId);
				
				// Construct account table object and store ssn and employer name.
				// Use current unique keys for accessing correct acount table record.
				// We set both fields if either is blank.  Not really optimal.
				bltNIM3_CaseAccount ca = new bltNIM3_CaseAccount(caseId);
				String caSSN = ca.getPatientSSN();
				ca.setPatientSSN(ssn+"");
				ca.setEmployerName(employerName);
				ca.commitData();
				System.out.println("Current CA ssn = " + caSSN);				
				
			}
		
			rs.close();
			ps.close();
			cCon.close();
		} catch (Exception eeee) {
			System.out.println("Table access Error: " + eeee.toString());
		}
				
	}
	
	/**
	 * 
	 * Used to clear out tnim3_eligibilityreference table between test runs.
	 * 
	 */
	public void truncateEligibilityReferenceTable() {
		searchDB2 mySS = new searchDB2();
		StringBuffer sb = new StringBuffer();
		sb.append("DELETE FROM tnim3_eligibilityreference");
		String mySQL = sb.toString();
		java.sql.ResultSet myRS = mySS.executeStatement(mySQL);
	}

	public void tabFileReader(String inFile) throws Exception {

		bltNIM3_EligibilityReference er = null;
		BufferedReader bufferedReader = null;
		bufferedReader = new BufferedReader(new FileReader(inFile));
		String line = null;

		// The variable 'addChangeFlag' signifies whether the record is a new
		// record or an update.
		// If 'A' (add) then the claim is new. If 'C' (change) it is an update
		// to an existing claim.
		String addChangeFlag = null;
		String amerisysDatabaseID = null;

		while ((line = bufferedReader.readLine()) != null) {

			String dataval[] = line.split("\t");
			addChangeFlag = dataval[0];

			// We need to check for prior existence of records and use this
			// logic as suggested by Amerisys (Yavette Griffith)
			// in an email:
			// "If I send you an A that already exists you can ignore it."
			// "If I send you a C that does not exist then, yes treat it as an A."

			searchDB2 mySS = new searchDB2();

			// These next 3 fields compose a unique key for this record (as
			// mandated by Amerisys).
			String corrusClaimID = dataval[23]; // Corrus Claim Id - Corrus
												// unique reference to Claim
			String payerIndicator = dataval[38]; // Amerisys Payer Indicator
			String dbID = dataval[41]; // Amerisys Database ID

			StringBuffer sb = new StringBuffer();
			sb.append("SELECT * FROM tnim3_eligibilityreference WHERE ");
			sb.append("corrusclaimid = '" + corrusClaimID
					+ "' AND databaseid = '" + dbID + "' AND ");
			sb.append("payerindicator = '" + payerIndicator + "'");
			String mySQL = sb.toString();

			System.out.println("mySQL = "+mySQL);

			System.out.println("corrusClaimID = "+corrusClaimID);
			System.out.println("databaseid = "+dbID);
			System.out.println("payerIndicator = "+payerIndicator);
			
			java.sql.ResultSet myRS = mySS.executeStatement(mySQL);
			int key = 0;
			try {
				// / record already there
				if (myRS != null && myRS.next()) {
					if (addChangeFlag.compareToIgnoreCase("a") == 0) {
						System.out.println("addChangeFlag.compareToIgnoreCase = a");
						continue;
					} else if (addChangeFlag.compareToIgnoreCase("c") == 0) {
						// Record already exists.
						key = myRS.getInt("eligibilityreferenceid");
						er = new bltNIM3_EligibilityReference(key);
					} else {
						// Report an error if we get here.
						System.out.println("*** FATAL ERROR ***");
					}
					// no record exists
				} else {
					// Add this record for the first time
					System.out.println("Add this record for the first time");
					er = new bltNIM3_EligibilityReference();
				}
				mySS.closeAll();
			} catch (Exception e) {
			} finally {
				mySS.closeAll();
			}
			
			er.setPayerID(amerisysPayerID);			// Currently this is hardcoded for Amerisys.
			er.setPatientLastName(dataval[9]); 		// Claimant last name
			er.setPatientFirstName(dataval[10]); 	// Claimant first name
			er.setPatientAddress1(dataval[12]); 	// Claimant mailing address line 1
			er.setPatientAddress2(dataval[13]); 	// Claimant address line 2
			er.setPatientCity(dataval[14]); 		// Claimant City
			er.setPatientState(dataval[15]); 		// Claimant State
			er.setPatientZip(dataval[16]); 			// Claimant Zip
			er.setClientPayerID(dataval[1]); 		// Corrus Payer Id - Unique Corrus
													// Id to reference the
													// Account/Payer of the claim

			er.setEmployerName(dataval[32]);
			er.setEmployerCounty(dataval[33]);
			er.setControverted(dataval[39]);		// Controverted.
			
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMdd");
			if (dataval[24].compareTo("")==0) {
				Date tmp = simpleDateFormat.parse("00000000");
				er.setDateOfInjury(tmp);			// Date of Injury
				System.out.println("Date of Injury set to 00000000");				
			}
			else {
				Date doi = simpleDateFormat.parse(dataval[24]);
				er.setDateOfInjury(doi);			// Date of Injury
			}
			
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
			if (dataval[17].compareTo("")==0) {
				Date tmp = simpleDateFormat.parse("00000000");				
				er.setPatientDOB(tmp); 				// Claimant date of birth
				System.out.println("DOB set to 00000000");				
			}
			else {
				Date dob = dateFormat.parse(dataval[17]);
				er.setPatientDOB(dob); 				// Claimant date of birth
			}

			er.setPatientSSN(dataval[18]); 			// Claimant social security name
			er.setPatientCellPhone(dataval[19]); 	// Claimant phone number
			er.setPatientGender(dataval[20]); 		// Claimant gender
				
			er.setClientClaimNum(dataval[25]);		// Client Claim Number
			er.setAdjusterLastName(dataval[36]); 	// Last Name of the Adjuster assigned to the claim
			er.setAdjusterFirstName(dataval[37]); 	// First Name of the Adjuster assigned to the claim
			
			er.setCaseManagerFirstName(dataval[37]);
			er.setCaseManagerLastName(dataval[36]);
			
			//SimpleDateFormat mmiFormat = new SimpleDateFormat("yyyyMMdd");
			//Date mmi = mmiFormat.parse(dataval[40]);	
			//er.setMMI(mmi);
			
			er.setClaimstatus(dataval[29]);			// Claim Status
			er.setPrimaryDiagCode(dataval[26]);		// Diagnostic codes.
			er.setDiagCode2(dataval[27]);
			er.setDiagCode3(dataval[28]);
			//
			// These next 3 fields compose a unique key for this record
			//
			er.setCorrusClaimId(dataval[23]); 		// Corrus Claim Id - Corrus unique
													// reference to Claim
			er.setPayerIndicator(dataval[38]); 		// Amerisys Payer Indicator
			er.setDatabaseID(dataval[41]); 			// Amerisys Database ID

			er.commitData();
		}
		bufferedReader.close();
	}

	/**
	 * 
	 * 1. 	Read the directory on the Amerisys site where they place their files for
	 *		processing by us.
	 * 
	 * 2.	Copy these files to us.
	 * 
	 * 3.	When you are done with this delete the files on the Amerisys site.
	 * 
	 * 
	 * @param host
	 * @param port
	 * @param username
	 * @param password
	 * @param remoteDir
	 */
	public void getRemoteOutBoundFiles(String host, int port, String username,
			String password, String remoteDir) {
		try {
			FTPSClient ftpClient = new FTPSClient(false);

			// Connect to host
			ftpClient.connect(host, port);
			int reply = ftpClient.getReplyCode();
			if (FTPReply.isPositiveCompletion(reply)) {

				// Login
				if (ftpClient.login(username, password)) {

					// Set protection buffer size
					ftpClient.execPBSZ(0);
					// Set data channel protection to private
					ftpClient.execPROT("P");
					// Enter local passive mode
					ftpClient.enterLocalPassiveMode();

					// Get a list of the files in the remote directory.
					boolean bool = ftpClient.changeWorkingDirectory(remoteDir);
					System.out.println("Current dir: "
							+ ftpClient.printWorkingDirectory());

					ftpClient.listFiles();
					FTPFile files[] = ftpClient.listFiles();

					for (int i = 0; i < files.length; i++) {
						String filename = ((FTPFile) files[i]).getName();
						if (filename.compareTo(".") == 0
								|| filename.compareTo("..") == 0) {
							continue;
						}
						System.out.println("Remote File -> " + remoteDir
								+ filename);
						OutputStream local = new FileOutputStream(
								localIncomingDir + filename);
						ftpClient.retrieveFile(remoteDir + filename, local);
					}

					// Logout
					ftpClient.logout();

				} else {
					System.out.println("FTP login failed");
				}

				// Disconnect
				ftpClient.disconnect();

			} else {
				System.out.println("FTP connect to host failed");
			}
		} catch (IOException ioe) {
			System.out.println("FTP client received network error" + ioe);
		}
	}

	/**
	 * 
	 * This method deletes a named file on the remote server.
	 * 
	 * @param host
	 * @param port
	 * @param username
	 * @param password
	 * @param remoteFilename
	 */
	public void deleteRemoteOutBoundFile(String host, int port, String username,
			String password, String remoteFilename) {
		try {
			FTPSClient ftpClient = new FTPSClient(false);

			// Connect to host
			ftpClient.connect(host, port);
			int reply = ftpClient.getReplyCode();
			if (FTPReply.isPositiveCompletion(reply)) {

				// Login
				if (ftpClient.login(username, password)) {

					// Set protection buffer size
					ftpClient.execPBSZ(0);
					// Set data channel protection to private
					ftpClient.execPROT("P");
					// Enter local passive mode
					ftpClient.enterLocalPassiveMode();

					// Delete the remote file.
					if (!ftpClient.deleteFile(remoteFilename)) {
						System.out.println(remoteFilename
								+ " Could not be deleted.");
					}

					// Logout
					ftpClient.logout();

				} else {
					System.out.println("FTP login failed");
				}

				// Disconnect
				ftpClient.disconnect();

			} else {
				System.out.println("FTP connect to host failed");
			}
		} catch (IOException ioe) {
			System.out.println("FTP client received network error");
		}
	}

	/**
	 * 
	 * This method puts a file on the Amerisys FTP site in a designated folder.
	 * 
	 * 
	 * @param host
	 * @param port
	 * @param username
	 * @param password
	 * @param localFilename
	 * @param remoteFilename
	 */
	public void putFile(String host, int port, String username,
			String password, String localFilename, String remoteFilename) {
		try {
			FTPSClient ftpClient = new FTPSClient(false);

			// Connect to host
			ftpClient.connect(host, port);
			int reply = ftpClient.getReplyCode();
			if (FTPReply.isPositiveCompletion(reply)) {

				// Login
				if (ftpClient.login(username, password)) {

					// Set protection buffer size
					ftpClient.execPBSZ(0);
					// Set data channel protection to private
					ftpClient.execPROT("P");
					// Enter local passive mode
					ftpClient.enterLocalPassiveMode();

					// Store file on host
					InputStream is = new FileInputStream(localFilename);
					if (ftpClient.storeFile(remoteFilename, is)) {
						is.close();
					} else {
						System.out.println("Could not store file");
					}
					// Logout
					ftpClient.logout();

				} else {
					System.out.println("FTP login failed");
				}

				// Disconnect
				ftpClient.disconnect();

			} else {
				System.out.println("FTP connect to host failed");
			}
		} catch (IOException ioe) {
			System.out.println("FTP client received network error");
		}
	}

	/**
	 * 
	 * Execute a bash commmand.
	 * 
	 * @param cmd
	 */
	public void bashCommand(String cmd) {
		try {
			System.out.println("Executing " + cmd + " on remote host.");
			Process proc = Runtime.getRuntime().exec(cmd);
			BufferedReader read = new BufferedReader(new InputStreamReader(
					proc.getInputStream()));
			try {
				proc.waitFor();
			} catch (InterruptedException e) {
				System.out.println(e.getMessage());
			}
			while (read.ready()) {
				System.out.println(read.readLine());
			}
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
	}
	
}