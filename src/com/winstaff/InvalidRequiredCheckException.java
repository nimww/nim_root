package com.winstaff;
public class InvalidRequiredCheckException extends Exception
{

    public InvalidRequiredCheckException()
    {
        super();
    }

    public InvalidRequiredCheckException(String ee)
    {
        super(ee);
    }

}
