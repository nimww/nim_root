

package com.winstaff;

import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public interface InttUATransaction extends dbTableInterface
{

    public void setUATransactionID(Integer newValue);
    public Integer getUATransactionID();
    public void setUniqueCreateDate(Date newValue);
    public Date getUniqueCreateDate();
    public void setUniqueModifyDate(Date newValue);
    public Date getUniqueModifyDate();
    public void setUniqueModifyComments(String newValue);
    public String getUniqueModifyComments();
    public void setTransactionDate(Date newValue);
    public Date getTransactionDate();
    public void setActionID(Integer newValue);
    public Integer getActionID();
    public void setRemoteIP(String newValue);
    public String getRemoteIP();
    public void setUserID(Integer newValue);
    public Integer getUserID();
    public void setComments(String newValue);
    public String getComments();
}    // End of bltUATransaction class definition
