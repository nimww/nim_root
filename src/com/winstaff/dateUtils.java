package com.winstaff;
import java.awt.*;
import java.text.DateFormatSymbols;


public class dateUtils extends java.lang.Object
{

	public dateUtils()
	{
	}

    public static java.util.Date dateChange(java.util.Date myD, int dayChange)
    {
        java.util.Calendar calendar = java.util.Calendar.getInstance();
        calendar.setTime(myD);
        calendar.add(java.util.Calendar.DATE, dayChange);
        java.util.Date resultAsDate = new java.util.Date(calendar.getTime().getTime());
        return resultAsDate;
    }

    
    public static int findDate(java.util.Vector dateV, java.util.Date testDate, java.text.SimpleDateFormat sdf)
    {
        int myVal = -1;
        try
        {
            String testDateS = sdf.format(testDate);
            for (int i=0;i<=dateV.size();i++)
            {
                String incDateS = sdf.format((java.util.Date)dateV.elementAt(i));
                if (incDateS.equalsIgnoreCase(testDateS))
                {
                    myVal = i;
                }
            }
        }
        catch(Exception e)
        {
        }
        return myVal;
    }

    public static java.util.Date findMostRecentDate(java.util.Vector dateV, java.text.SimpleDateFormat sdf)
    {
        java.util.Date myVal = (java.util.Date)dateV.elementAt(0);
        try
        {
            for (int i=1;i<=dateV.size();i++)
            {
                java.util.Date incDate = (java.util.Date)dateV.elementAt(i);
                if (incDate.after(myVal))
                {
                    myVal = incDate;
                }
            }
        }
        catch(Exception e)
        {
        }
        return myVal;
    }

	public static String fixDate(java.sql.Timestamp myT)
	{
        String myReturn = ConfigurationInformation.sDefaultDateValue;
	    try
	    {
	        java.text.SimpleDateFormat mySDF2 = new java.text.SimpleDateFormat(ConfigurationInformation.sDateTimeFormat);
	        myReturn = mySDF2.format(new java.util.Date(myT.getTime()));
	    }
	    catch (Exception e)
	    {
	    }
	    return myReturn;
	}


    public static String getMonthAsText(int month) {
        return new DateFormatSymbols().getMonths()[month-1];
    }






}
