package com.winstaff;

public class ServiceInfo {
	private String servicestatusid;
	private String cpt;
	private String cptqty;
	private String cptmodifier;
	private String cptbodypart;
	private String dcpt1;
	private String dcpt2;
	private String dcpt3;
	private String dcpt4;
	private String cpttext;
	private String billamount;
	private String allowamount;
	private String receivedamount;
	private String paidoutamount;
	private String receivedcheck1number;
	private String receivedcheck1date;
	private String receivedcheck1amount;
	private String receivedcheck2number;
	private String receivedcheck2date;
	private String receivedcheck2amount;
	private String receivedcheck3number;
	private String receivedcheck3date;
	private String receivedcheck3amount;
	public ServiceInfo(String servicestatusid, String cpt, String cptqty, String cptmodifier, String cptbodypart, String dcpt1, String dcpt2, String dcpt3, String dcpt4, String cpttext, String billamount, String allowamount, String receivedamount, String paidoutamount, String receivedcheck1number, String receivedcheck1date, String receivedcheck1amount, String receivedcheck2number,
			String receivedcheck2date, String receivedcheck2amount, String receivedcheck3number, String receivedcheck3date, String receivedcheck3amount) {
		this.servicestatusid = servicestatusid;
		this.cpt = cpt;
		this.cptqty = cptqty;
		this.cptmodifier = cptmodifier;
		this.cptbodypart = cptbodypart;
		this.dcpt1 = dcpt1;
		this.dcpt2 = dcpt2;
		this.dcpt3 = dcpt3;
		this.dcpt4 = dcpt4;
		this.cpttext = cpttext;
		this.billamount = billamount;
		this.allowamount = allowamount;
		this.receivedamount = receivedamount;
		this.paidoutamount = paidoutamount;
		this.receivedcheck1number = receivedcheck1number;
		this.receivedcheck1date = receivedcheck1date;
		this.receivedcheck1amount = receivedcheck1amount;
		this.receivedcheck2number = receivedcheck2number;
		this.receivedcheck2date = receivedcheck2date;
		this.receivedcheck2amount = receivedcheck2amount;
		this.receivedcheck3number = receivedcheck3number;
		this.receivedcheck3date = receivedcheck3date;
		this.receivedcheck3amount = receivedcheck3amount;
	}
	public String getServicestatusid() {
		return servicestatusid;
	}
	public void setServicestatusid(String servicestatusid) {
		this.servicestatusid = servicestatusid;
	}
	public String getCpt() {
		return cpt;
	}
	public void setCpt(String cpt) {
		this.cpt = cpt;
	}
	public String getCptqty() {
		return cptqty;
	}
	public void setCptqty(String cptqty) {
		this.cptqty = cptqty;
	}
	public String getCptmodifier() {
		return cptmodifier;
	}
	public void setCptmodifier(String cptmodifier) {
		this.cptmodifier = cptmodifier;
	}
	public String getCptbodypart() {
		return cptbodypart;
	}
	public void setCptbodypart(String cptbodypart) {
		this.cptbodypart = cptbodypart;
	}
	public String getDcpt1() {
		return dcpt1;
	}
	public void setDcpt1(String dcpt1) {
		this.dcpt1 = dcpt1;
	}
	public String getDcpt2() {
		return dcpt2;
	}
	public void setDcpt2(String dcpt2) {
		this.dcpt2 = dcpt2;
	}
	public String getDcpt3() {
		return dcpt3;
	}
	public void setDcpt3(String dcpt3) {
		this.dcpt3 = dcpt3;
	}
	public String getDcpt4() {
		return dcpt4;
	}
	public void setDcpt4(String dcpt4) {
		this.dcpt4 = dcpt4;
	}
	public String getCpttext() {
		return cpttext;
	}
	public void setCpttext(String cpttext) {
		this.cpttext = cpttext;
	}
	public String getBillamount() {
		return billamount;
	}
	public void setBillamount(String billamount) {
		this.billamount = billamount;
	}
	public String getAllowamount() {
		return allowamount;
	}
	public void setAllowamount(String allowamount) {
		this.allowamount = allowamount;
	}
	public String getReceivedamount() {
		return receivedamount;
	}
	public void setReceivedamount(String receivedamount) {
		this.receivedamount = receivedamount;
	}
	public String getPaidoutamount() {
		return paidoutamount;
	}
	public void setPaidoutamount(String paidoutamount) {
		this.paidoutamount = paidoutamount;
	}
	public String getReceivedcheck1number() {
		return receivedcheck1number;
	}
	public void setReceivedcheck1number(String receivedcheck1number) {
		this.receivedcheck1number = receivedcheck1number;
	}
	public String getReceivedcheck1date() {
		return receivedcheck1date;
	}
	public void setReceivedcheck1date(String receivedcheck1date) {
		this.receivedcheck1date = receivedcheck1date;
	}
	public String getReceivedcheck1amount() {
		return receivedcheck1amount;
	}
	public void setReceivedcheck1amount(String receivedcheck1amount) {
		this.receivedcheck1amount = receivedcheck1amount;
	}
	public String getReceivedcheck2number() {
		return receivedcheck2number;
	}
	public void setReceivedcheck2number(String receivedcheck2number) {
		this.receivedcheck2number = receivedcheck2number;
	}
	public String getReceivedcheck2date() {
		return receivedcheck2date;
	}
	public void setReceivedcheck2date(String receivedcheck2date) {
		this.receivedcheck2date = receivedcheck2date;
	}
	public String getReceivedcheck2amount() {
		return receivedcheck2amount;
	}
	public void setReceivedcheck2amount(String receivedcheck2amount) {
		this.receivedcheck2amount = receivedcheck2amount;
	}
	public String getReceivedcheck3number() {
		return receivedcheck3number;
	}
	public void setReceivedcheck3number(String receivedcheck3number) {
		this.receivedcheck3number = receivedcheck3number;
	}
	public String getReceivedcheck3date() {
		return receivedcheck3date;
	}
	public void setReceivedcheck3date(String receivedcheck3date) {
		this.receivedcheck3date = receivedcheck3date;
	}
	public String getReceivedcheck3amount() {
		return receivedcheck3amount;
	}
	public void setReceivedcheck3amount(String receivedcheck3amount) {
		this.receivedcheck3amount = receivedcheck3amount;
	}

}
