

package com.winstaff;

import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public class dbtUpdateItem extends Object implements InttUpdateItem
{

        db_NewBase    dbnbDB;

    public dbtUpdateItem()
    {
        dbnbDB = new db_NewBase( "tUpdateItem", "UpdateItemID" );

    }    // End of default constructor

    public dbtUpdateItem( Integer iNewID )
    {
        dbnbDB = new db_NewBase( "tUpdateItem", "UpdateItemID" );
        setUniqueID( iNewID );
    }    // End of Constructor knowing an ID

    public void commitData() throws SQLException
    {
            //no longer trapping/ignoring errors 2011-03-17
            dbnbDB.commitData();
    }    // End of commitData()

    public void commitDataForced() throws SQLException
    {
        dbnbDB.commitDataForced();
    }    // End of commitData()

    public Integer getUniqueID()
    {
        return dbnbDB.getUniqueID();
    }    // End of getUniqueID()

    public void setUniqueID( Integer iNewID )
    {
        dbnbDB.setUniqueID( iNewID );
    }    // End of setUniqueID()

    public void setModifyComments( String sNew )
    {
        dbnbDB.setModifyComments( sNew );
    }    // End of setUniqueID()


    public void setUpdateItemID(Integer newValue)
    {
                dbnbDB.setFieldData( "UpdateItemID", newValue.toString() );
    }

    public Integer getUpdateItemID()
    {
        String           sValue = dbnbDB.getFieldData( "UpdateItemID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setField(String fieldN, Object fieldV)
    {DebugLogger.println("setField Method not implemented yet in classUpdateItem!");}

    public String getFieldType(String fieldN)
    {return null;}


    public void setUniqueCreateDate(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "UniqueCreateDate", formatter.format( newValue ) );
    }

    public Date getUniqueCreateDate()
    {
        String           sValue = dbnbDB.getFieldData( "UniqueCreateDate" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setUniqueModifyDate(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "UniqueModifyDate", formatter.format( newValue ) );
    }

    public Date getUniqueModifyDate()
    {
        String           sValue = dbnbDB.getFieldData( "UniqueModifyDate" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setUniqueModifyComments(String newValue)
    {
                dbnbDB.setFieldData( "UniqueModifyComments", newValue.toString() );
    }

    public String getUniqueModifyComments()
    {
        String           sValue = dbnbDB.getFieldData( "UniqueModifyComments" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setSubmissionDate(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "SubmissionDate", formatter.format( newValue ) );
    }

    public Date getSubmissionDate()
    {
        String           sValue = dbnbDB.getFieldData( "SubmissionDate" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setReleaseVersion(String newValue)
    {
                dbnbDB.setFieldData( "ReleaseVersion", newValue.toString() );
    }

    public String getReleaseVersion()
    {
        String           sValue = dbnbDB.getFieldData( "ReleaseVersion" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setCategoryID(Integer newValue)
    {
                dbnbDB.setFieldData( "CategoryID", newValue.toString() );
    }

    public Integer getCategoryID()
    {
        String           sValue = dbnbDB.getFieldData( "CategoryID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setPriorityID(Integer newValue)
    {
                dbnbDB.setFieldData( "PriorityID", newValue.toString() );
    }

    public Integer getPriorityID()
    {
        String           sValue = dbnbDB.getFieldData( "PriorityID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setStatusID(Integer newValue)
    {
                dbnbDB.setFieldData( "StatusID", newValue.toString() );
    }

    public Integer getStatusID()
    {
        String           sValue = dbnbDB.getFieldData( "StatusID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setItemTitle(String newValue)
    {
                dbnbDB.setFieldData( "ItemTitle", newValue.toString() );
    }

    public String getItemTitle()
    {
        String           sValue = dbnbDB.getFieldData( "ItemTitle" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setItemDescription(String newValue)
    {
                dbnbDB.setFieldData( "ItemDescription", newValue.toString() );
    }

    public String getItemDescription()
    {
        String           sValue = dbnbDB.getFieldData( "ItemDescription" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setDeveloperComments(String newValue)
    {
                dbnbDB.setFieldData( "DeveloperComments", newValue.toString() );
    }

    public String getDeveloperComments()
    {
        String           sValue = dbnbDB.getFieldData( "DeveloperComments" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setTesterComments(String newValue)
    {
                dbnbDB.setFieldData( "TesterComments", newValue.toString() );
    }

    public String getTesterComments()
    {
        String           sValue = dbnbDB.getFieldData( "TesterComments" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setDateTestedOnDevelopment(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "DateTestedOnDevelopment", formatter.format( newValue ) );
    }

    public Date getDateTestedOnDevelopment()
    {
        String           sValue = dbnbDB.getFieldData( "DateTestedOnDevelopment" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setDateTestedOnQA(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "DateTestedOnQA", formatter.format( newValue ) );
    }

    public Date getDateTestedOnQA()
    {
        String           sValue = dbnbDB.getFieldData( "DateTestedOnQA" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setDateTestedOnProduction(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "DateTestedOnProduction", formatter.format( newValue ) );
    }

    public Date getDateTestedOnProduction()
    {
        String           sValue = dbnbDB.getFieldData( "DateTestedOnProduction" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setRequesterInitials(String newValue)
    {
                dbnbDB.setFieldData( "RequesterInitials", newValue.toString() );
    }

    public String getRequesterInitials()
    {
        String           sValue = dbnbDB.getFieldData( "RequesterInitials" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setRemoteIP(String newValue)
    {
                dbnbDB.setFieldData( "RemoteIP", newValue.toString() );
    }

    public String getRemoteIP()
    {
        String           sValue = dbnbDB.getFieldData( "RemoteIP" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setHowToTestComments(String newValue)
    {
                dbnbDB.setFieldData( "HowToTestComments", newValue.toString() );
    }

    public String getHowToTestComments()
    {
        String           sValue = dbnbDB.getFieldData( "HowToTestComments" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setComments(String newValue)
    {
                dbnbDB.setFieldData( "Comments", newValue.toString() );
    }

    public String getComments()
    {
        String           sValue = dbnbDB.getFieldData( "Comments" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setPLCSecID(Integer newValue)
    {
                dbnbDB.setFieldData( "PLCSecID", newValue.toString() );
    }

    public Integer getPLCSecID()
    {
        String           sValue = dbnbDB.getFieldData( "PLCSecID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setTicketNumber(String newValue)
    {
                dbnbDB.setFieldData( "TicketNumber", newValue.toString() );
    }

    public String getTicketNumber()
    {
        String           sValue = dbnbDB.getFieldData( "TicketNumber" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setAssignedToID(Integer newValue)
    {
                dbnbDB.setFieldData( "AssignedToID", newValue.toString() );
    }

    public Integer getAssignedToID()
    {
        String           sValue = dbnbDB.getFieldData( "AssignedToID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setAssignedToRoleID(Integer newValue)
    {
                dbnbDB.setFieldData( "AssignedToRoleID", newValue.toString() );
    }

    public Integer getAssignedToRoleID()
    {
        String           sValue = dbnbDB.getFieldData( "AssignedToRoleID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

}    // End of bltUpdateItem class definition
