

package com.winstaff;

import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;
import java.util.Enumeration;

public class bltNIM3_SchedulingTransaction extends Object implements InttNIM3_SchedulingTransaction
{

    dbtNIM3_SchedulingTransaction    dbDB;

    public bltNIM3_SchedulingTransaction()
    {
        dbDB = new dbtNIM3_SchedulingTransaction();
        dbDB.setUniqueCreateDate(new java.util.Date());
        makeRequiredHash();
        makeExpiredHash();
        makeEnglishHash();

    }    // End of default constructor

    public bltNIM3_SchedulingTransaction( Integer iNewID )
    {        dbDB = new dbtNIM3_SchedulingTransaction( iNewID );
        makeRequiredHash();
        makeExpiredHash();
        makeEnglishHash();
        iSecurityCheck = new Integer(2);
    }    // End of Constructor knowing an ID


    public bltNIM3_SchedulingTransaction( Integer iNewID, Integer iGroupSecurityID )
    {
        this.GroupSecurityInit(iGroupSecurityID);
        dbDB = new dbtNIM3_SchedulingTransaction( iNewID );
        makeExpiredHash();
        makeEnglishHash();
        makeRequiredHash(iGroupSecurityID);
    }    // End of Constructor knowing an ID

    public bltNIM3_SchedulingTransaction( Integer iGroupSecurityID, boolean isTrue )
    {
        this.GroupSecurityInit(iGroupSecurityID);
        dbDB = new dbtNIM3_SchedulingTransaction(  );
        makeExpiredHash();
        makeEnglishHash();
        makeRequiredHash(iGroupSecurityID);
    }    // End of Security unknown ID

    public void GroupSecurityInit( Integer iGroupSecurityID )
    {
        bltSecurityGroupMaster mySGM = new bltSecurityGroupMaster(iGroupSecurityID);
        this.UserSecurityID = iGroupSecurityID;
        if (mySGM.getRunFieldSecurityID().intValue()==1)
        {
            this.runFieldSecurity = true;
            makeRequiredHash(iGroupSecurityID);
        }
        iSecurityCheck = SecurityCheck.CheckItem("Maint1", iGroupSecurityID);
    }  

    public EventReferenceType getEventReference()
    {
        return EventUtils.getEventReference("tNIM3_SchedulingTransaction", this.getUniqueID());
    }  

    public String convertString2LogString(String normalString) 
    {
        String myVal = "";
        for (int i=0;i<normalString.length();i++)
        {
            if ( (normalString.charAt(i)!='\r')&&(normalString.charAt(i)!='\n')&&(normalString.charAt(i)!='\t') )
            {
                myVal+=normalString.charAt(i);
            }
            else
            {
                myVal+="<NLT>";
            }
        }
        return myVal;
    }
    public void runAudit()
    {
        String AuditString = new java.util.Date()+" \t " ;  
        AuditString += " tNIM3_SchedulingTransaction "; 
        AuditString += " SchedulingTransactionID ="+this.getUniqueID(); 
        AuditString += " PracticeID ="+this.getPracticeID(); 
        for (int i=0;i<this.AuditVector.size();i++)
        {
            AuditString+="\t"+convertString2LogString((String)this.AuditVector.elementAt(i));
        }
        AuditLogger.printLine(AuditString);
    }
    public void commitData() throws SQLException
    {
        runAudit();
        dbDB.setModifyComments(this.getUniqueModifyComments());
        dbDB.setUniqueModifyDate(new java.util.Date());
        dbDB.commitData();
        DataControlUtils.dataChanged("tNIM3_SchedulingTransaction",this.getPracticeID() ,this.AuditVector.size(), "PracticeID");
        this.AuditVector.removeAllElements();
    }    // End of commitData()

    public void commitDataForced() throws SQLException
    {
        dbDB.commitDataForced();
    }    // End of commitData()

    public Integer getUniqueID()
    {
        return dbDB.getUniqueID();
    }    // End of getUniqueID()

    public void setUniqueID( Integer iNewID )
    {
        dbDB.setUniqueID( iNewID );
    }    // End of setUniqueID()


    public void setSchedulingTransactionID(Integer newValue)
    {
        dbDB.setSchedulingTransactionID(newValue);
    }

    public Integer getSchedulingTransactionID()
    {
        return dbDB.getSchedulingTransactionID();
    }

    java.util.Vector AuditVector = new java.util.Vector();

    public void setUniqueCreateDate(Date newValue)
    {
        this.setUniqueCreateDate(newValue,this.UserSecurityID);


    }
    public Date getUniqueCreateDate()
    {
        return this.getUniqueCreateDate(this.UserSecurityID);
    }

    public void setUniqueCreateDate(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='UniqueCreateDate' AND TableName='tNIM3_SchedulingTransaction'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[UniqueCreateDate]=["+newValue+"]");
                   dbDB.setUniqueCreateDate(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[UniqueCreateDate]=["+newValue+"]");
           dbDB.setUniqueCreateDate(newValue);
         }
    }
    public Date getUniqueCreateDate(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='UniqueCreateDate' AND TableName='tNIM3_SchedulingTransaction'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getUniqueCreateDate();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getUniqueCreateDate();
         }
        return myVal;
    }

    public void setUniqueModifyDate(Date newValue)
    {
        this.setUniqueModifyDate(newValue,this.UserSecurityID);


    }
    public Date getUniqueModifyDate()
    {
        return this.getUniqueModifyDate(this.UserSecurityID);
    }

    public void setUniqueModifyDate(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='UniqueModifyDate' AND TableName='tNIM3_SchedulingTransaction'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[UniqueModifyDate]=["+newValue+"]");
                   dbDB.setUniqueModifyDate(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[UniqueModifyDate]=["+newValue+"]");
           dbDB.setUniqueModifyDate(newValue);
         }
    }
    public Date getUniqueModifyDate(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='UniqueModifyDate' AND TableName='tNIM3_SchedulingTransaction'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getUniqueModifyDate();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getUniqueModifyDate();
         }
        return myVal;
    }

    public void setUniqueModifyComments(String newValue)
    {
        this.setUniqueModifyComments(newValue,this.UserSecurityID);


    }
    public String getUniqueModifyComments()
    {
        return this.getUniqueModifyComments(this.UserSecurityID);
    }

    public void setUniqueModifyComments(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='UniqueModifyComments' AND TableName='tNIM3_SchedulingTransaction'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[UniqueModifyComments]=["+newValue+"]");
                   dbDB.setUniqueModifyComments(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[UniqueModifyComments]=["+newValue+"]");
           dbDB.setUniqueModifyComments(newValue);
         }
    }
    public String getUniqueModifyComments(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='UniqueModifyComments' AND TableName='tNIM3_SchedulingTransaction'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getUniqueModifyComments();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getUniqueModifyComments();
         }
        return myVal;
    }

    public void setPracticeID(Integer newValue)
    {
        this.setPracticeID(newValue,this.UserSecurityID);


    }
    public Integer getPracticeID()
    {
        return this.getPracticeID(this.UserSecurityID);
    }

    public void setPracticeID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PracticeID' AND TableName='tNIM3_SchedulingTransaction'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[PracticeID]=["+newValue+"]");
                   dbDB.setPracticeID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[PracticeID]=["+newValue+"]");
           dbDB.setPracticeID(newValue);
         }
    }
    public Integer getPracticeID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PracticeID' AND TableName='tNIM3_SchedulingTransaction'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPracticeID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPracticeID();
         }
        return myVal;
    }

    public void setTransactionDate(Date newValue)
    {
        this.setTransactionDate(newValue,this.UserSecurityID);


    }
    public Date getTransactionDate()
    {
        return this.getTransactionDate(this.UserSecurityID);
    }

    public void setTransactionDate(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='TransactionDate' AND TableName='tNIM3_SchedulingTransaction'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[TransactionDate]=["+newValue+"]");
                   dbDB.setTransactionDate(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[TransactionDate]=["+newValue+"]");
           dbDB.setTransactionDate(newValue);
         }
    }
    public Date getTransactionDate(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='TransactionDate' AND TableName='tNIM3_SchedulingTransaction'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getTransactionDate();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getTransactionDate();
         }
        return myVal;
    }

    public void setActionID(Integer newValue)
    {
        this.setActionID(newValue,this.UserSecurityID);


    }
    public Integer getActionID()
    {
        return this.getActionID(this.UserSecurityID);
    }

    public void setActionID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ActionID' AND TableName='tNIM3_SchedulingTransaction'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[ActionID]=["+newValue+"]");
                   dbDB.setActionID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[ActionID]=["+newValue+"]");
           dbDB.setActionID(newValue);
         }
    }
    public Integer getActionID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ActionID' AND TableName='tNIM3_SchedulingTransaction'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getActionID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getActionID();
         }
        return myVal;
    }

    public void setUserID(Integer newValue)
    {
        this.setUserID(newValue,this.UserSecurityID);


    }
    public Integer getUserID()
    {
        return this.getUserID(this.UserSecurityID);
    }

    public void setUserID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='UserID' AND TableName='tNIM3_SchedulingTransaction'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[UserID]=["+newValue+"]");
                   dbDB.setUserID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[UserID]=["+newValue+"]");
           dbDB.setUserID(newValue);
         }
    }
    public Integer getUserID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='UserID' AND TableName='tNIM3_SchedulingTransaction'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getUserID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getUserID();
         }
        return myVal;
    }

    public void setComments(String newValue)
    {
        this.setComments(newValue,this.UserSecurityID);


    }
    public String getComments()
    {
        return this.getComments(this.UserSecurityID);
    }

    public void setComments(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Comments' AND TableName='tNIM3_SchedulingTransaction'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[Comments]=["+newValue+"]");
                   dbDB.setComments(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[Comments]=["+newValue+"]");
           dbDB.setComments(newValue);
         }
    }
    public String getComments(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Comments' AND TableName='tNIM3_SchedulingTransaction'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getComments();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getComments();
         }
        return myVal;
    }

    public void setCompareAddress1(String newValue)
    {
        this.setCompareAddress1(newValue,this.UserSecurityID);


    }
    public String getCompareAddress1()
    {
        return this.getCompareAddress1(this.UserSecurityID);
    }

    public void setCompareAddress1(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CompareAddress1' AND TableName='tNIM3_SchedulingTransaction'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[CompareAddress1]=["+newValue+"]");
                   dbDB.setCompareAddress1(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[CompareAddress1]=["+newValue+"]");
           dbDB.setCompareAddress1(newValue);
         }
    }
    public String getCompareAddress1(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CompareAddress1' AND TableName='tNIM3_SchedulingTransaction'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getCompareAddress1();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getCompareAddress1();
         }
        return myVal;
    }

    public void setCompareAddress2(String newValue)
    {
        this.setCompareAddress2(newValue,this.UserSecurityID);


    }
    public String getCompareAddress2()
    {
        return this.getCompareAddress2(this.UserSecurityID);
    }

    public void setCompareAddress2(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CompareAddress2' AND TableName='tNIM3_SchedulingTransaction'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[CompareAddress2]=["+newValue+"]");
                   dbDB.setCompareAddress2(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[CompareAddress2]=["+newValue+"]");
           dbDB.setCompareAddress2(newValue);
         }
    }
    public String getCompareAddress2(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CompareAddress2' AND TableName='tNIM3_SchedulingTransaction'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getCompareAddress2();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getCompareAddress2();
         }
        return myVal;
    }

    public void setCompareCity(String newValue)
    {
        this.setCompareCity(newValue,this.UserSecurityID);


    }
    public String getCompareCity()
    {
        return this.getCompareCity(this.UserSecurityID);
    }

    public void setCompareCity(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CompareCity' AND TableName='tNIM3_SchedulingTransaction'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[CompareCity]=["+newValue+"]");
                   dbDB.setCompareCity(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[CompareCity]=["+newValue+"]");
           dbDB.setCompareCity(newValue);
         }
    }
    public String getCompareCity(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CompareCity' AND TableName='tNIM3_SchedulingTransaction'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getCompareCity();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getCompareCity();
         }
        return myVal;
    }

    public void setCompareStateLI(String newValue)
    {
        this.setCompareStateLI(newValue,this.UserSecurityID);


    }
    public String getCompareStateLI()
    {
        return this.getCompareStateLI(this.UserSecurityID);
    }

    public void setCompareStateLI(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CompareStateLI' AND TableName='tNIM3_SchedulingTransaction'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[CompareStateLI]=["+newValue+"]");
                   dbDB.setCompareStateLI(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[CompareStateLI]=["+newValue+"]");
           dbDB.setCompareStateLI(newValue);
         }
    }
    public String getCompareStateLI(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CompareStateLI' AND TableName='tNIM3_SchedulingTransaction'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getCompareStateLI();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getCompareStateLI();
         }
        return myVal;
    }

    public void setCompareStateCode(String newValue)
    {
        this.setCompareStateCode(newValue,this.UserSecurityID);


    }
    public String getCompareStateCode()
    {
        return this.getCompareStateCode(this.UserSecurityID);
    }

    public void setCompareStateCode(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CompareStateCode' AND TableName='tNIM3_SchedulingTransaction'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[CompareStateCode]=["+newValue+"]");
                   dbDB.setCompareStateCode(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[CompareStateCode]=["+newValue+"]");
           dbDB.setCompareStateCode(newValue);
         }
    }
    public String getCompareStateCode(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CompareStateCode' AND TableName='tNIM3_SchedulingTransaction'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getCompareStateCode();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getCompareStateCode();
         }
        return myVal;
    }

    public void setCompareZip(String newValue)
    {
        this.setCompareZip(newValue,this.UserSecurityID);


    }
    public String getCompareZip()
    {
        return this.getCompareZip(this.UserSecurityID);
    }

    public void setCompareZip(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CompareZip' AND TableName='tNIM3_SchedulingTransaction'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[CompareZip]=["+newValue+"]");
                   dbDB.setCompareZip(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[CompareZip]=["+newValue+"]");
           dbDB.setCompareZip(newValue);
         }
    }
    public String getCompareZip(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CompareZip' AND TableName='tNIM3_SchedulingTransaction'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getCompareZip();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getCompareZip();
         }
        return myVal;
    }

    public void setCompareCounty(String newValue)
    {
        this.setCompareCounty(newValue,this.UserSecurityID);


    }
    public String getCompareCounty()
    {
        return this.getCompareCounty(this.UserSecurityID);
    }

    public void setCompareCounty(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CompareCounty' AND TableName='tNIM3_SchedulingTransaction'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[CompareCounty]=["+newValue+"]");
                   dbDB.setCompareCounty(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[CompareCounty]=["+newValue+"]");
           dbDB.setCompareCounty(newValue);
         }
    }
    public String getCompareCounty(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CompareCounty' AND TableName='tNIM3_SchedulingTransaction'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getCompareCounty();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getCompareCounty();
         }
        return myVal;
    }

    public void setCompareEncounterRefID(Integer newValue)
    {
        this.setCompareEncounterRefID(newValue,this.UserSecurityID);


    }
    public Integer getCompareEncounterRefID()
    {
        return this.getCompareEncounterRefID(this.UserSecurityID);
    }

    public void setCompareEncounterRefID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CompareEncounterRefID' AND TableName='tNIM3_SchedulingTransaction'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[CompareEncounterRefID]=["+newValue+"]");
                   dbDB.setCompareEncounterRefID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[CompareEncounterRefID]=["+newValue+"]");
           dbDB.setCompareEncounterRefID(newValue);
         }
    }
    public Integer getCompareEncounterRefID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CompareEncounterRefID' AND TableName='tNIM3_SchedulingTransaction'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getCompareEncounterRefID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getCompareEncounterRefID();
         }
        return myVal;
    }

    public void setTotalResults(Integer newValue)
    {
        this.setTotalResults(newValue,this.UserSecurityID);


    }
    public Integer getTotalResults()
    {
        return this.getTotalResults(this.UserSecurityID);
    }

    public void setTotalResults(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='TotalResults' AND TableName='tNIM3_SchedulingTransaction'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[TotalResults]=["+newValue+"]");
                   dbDB.setTotalResults(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[TotalResults]=["+newValue+"]");
           dbDB.setTotalResults(newValue);
         }
    }
    public Integer getTotalResults(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='TotalResults' AND TableName='tNIM3_SchedulingTransaction'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getTotalResults();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getTotalResults();
         }
        return myVal;
    }

    public void setProcedureDescription(String newValue)
    {
        this.setProcedureDescription(newValue,this.UserSecurityID);


    }
    public String getProcedureDescription()
    {
        return this.getProcedureDescription(this.UserSecurityID);
    }

    public void setProcedureDescription(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ProcedureDescription' AND TableName='tNIM3_SchedulingTransaction'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[ProcedureDescription]=["+newValue+"]");
                   dbDB.setProcedureDescription(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[ProcedureDescription]=["+newValue+"]");
           dbDB.setProcedureDescription(newValue);
         }
    }
    public String getProcedureDescription(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ProcedureDescription' AND TableName='tNIM3_SchedulingTransaction'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getProcedureDescription();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getProcedureDescription();
         }
        return myVal;
    }

    public void setRequiredFieldList(java.util.Hashtable requiredFieldList)
	 {

        this.requiredFieldList = requiredFieldList;
    }

    public java.util.Hashtable getRequiredFieldList()
    {
        return this.requiredFieldList;
    }
    public void setExpiredFieldList(java.util.Hashtable expiredFieldList)
    {
        this.expiredFieldList = expiredFieldList;
    }

    public void setEnglishFieldList(java.util.Hashtable englishFieldList)
    {
        this.englishFieldList = englishFieldList;
    }

    public java.util.Hashtable getExpiredFieldList()
    {
        return this.expiredFieldList;
    }
    public boolean isExpired(java.util.Date myDate, int daysVector)
    {
       return isExpired(myDate,new java.util.Date(),daysVector);
    }


    public boolean isRead(String fieldN, Integer GroupRefID)
    {
        boolean myVal = false;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='"+fieldN+"' AND TableName='tNIM3_SchedulingTransaction'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==1)
               {
                   myVal = true;
               }
            }
        }
        else
        {

           if (iSecurityCheck.intValue()>=1)
           {
                  myVal = true;
           }
        }
        return myVal;
    }


    public boolean isWrite(String fieldN, Integer GroupRefID)
    {
        boolean myVal = false;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='"+fieldN+"' AND TableName='tNIM3_SchedulingTransaction'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {
                   myVal = true;
               }
            }
        }
        else
        {

           if (iSecurityCheck.intValue()>=2)
           {
                  myVal = true;
           }
        }
        return myVal;
    }


    public boolean isExpired(java.util.Date myDate, java.util.Date refDate,int daysVector)
    {
        java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat("yyyy-MM-dd");
        boolean myVal = true;
        try
        {
            if (myDate.after(dbdf.parse("1800-01-10")))
            {
                if (myDate.after(dateChange(refDate,daysVector)))
                {
                    myVal=false;
                }
            }
            else
            {
                myVal = false;
            }
        }
        catch (Exception Datee)
        {
        }
        return myVal;
    }

    public java.util.Date dateChange(java.util.Date myD, int dayChange)
    {
        java.util.Calendar calendar = java.util.Calendar.getInstance();
        calendar.setTime(myD);
        calendar.add(java.util.Calendar.DATE, dayChange);
        java.util.Date resultAsDate = new java.util.Date(calendar.getTime().getTime());
        return resultAsDate;
    }

    public boolean isRequired(String fieldN)
    {
        return isRequired(fieldN, new Integer("1"));
    }

    public boolean isRequired(String fieldN, Integer GroupRefID)
    {
        boolean myVal = false;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='"+fieldN+"' AND TableName='tNIM3_SchedulingTransaction'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getRequiredID().intValue()==1)
               {
                   myVal = true;
               }
            }
        }
        else
        {
            if (this.getRequiredFieldList().containsKey(fieldN))
            {
                myVal = true;
            }
        }
        return myVal;
    }
    public boolean isExpiredCheck(String fieldN)
    {
        return isExpiredCheck(fieldN, new Integer ("1"));
    }

    public boolean isExpiredCheck(String fieldN, Integer GroupRefID)
    {
        boolean myVal = false;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='"+fieldN+"' AND TableName='tNIM3_SchedulingTransaction'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getTrackedID().intValue()==1)
               {
                   myVal = true;
               }
            }
        }
        else
        {
            if (this.getExpiredFieldList().containsKey(fieldN))
           {
                myVal = true;
            }
        }
        return myVal;
    }

    public void makeRequiredHash()
    {
       makeRequiredHash(new Integer(1));
    }

    public void makeRequiredHash(Integer GroupRefID)
    {

        java.util.Hashtable newHash = new java.util.Hashtable();
        if (runFieldSecurity)
        {
            java.util.Enumeration myKeys = englishFieldList.keys(); 
            while (myKeys.hasMoreElements()) 
            {
                 String myElement = (String) myKeys.nextElement();
                 bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='"+myElement+"' AND TableName='tNIM3_SchedulingTransaction'" ,"");
                 bltFieldSecurity        working_bltFieldSecurity;
                 ListElement         leCurrentElement;
                 Enumeration eList = myFSL.elements();
                 if (eList.hasMoreElements())
                 {
                     leCurrentElement    = (ListElement) eList.nextElement();
                     working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
                     if (working_bltFieldSecurity.getRequiredID().intValue()==1)
                     {
                         newHash.put(myElement,new Boolean(true));
                     }
                 }
            }
        }
        else
        {
            newHash.put("ActionID",new Boolean(true));
            newHash.put("UserID",new Boolean(true));
        }
        this.setRequiredFieldList(newHash);
    }

    public void makeEnglishHash()
    {

        java.util.Hashtable newHash = new java.util.Hashtable();        newHash.put("UniqueCreateDate","Item Create Date");
        newHash.put("UniqueModifyDate","Item Modify Date");
        newHash.put("UniqueModifyComments","Item Modification Comments");
        newHash.put("PracticeID","PracticeID");
        newHash.put("TransactionDate","TransactionDate");
        newHash.put("ActionID","ActionID");
        newHash.put("UserID","UserID");
        newHash.put("Comments","Comments");
        newHash.put("CompareAddress1","CompareAddress1");
        newHash.put("CompareAddress2","CompareAddress2");
        newHash.put("CompareCity","CompareCity");
        newHash.put("CompareStateLI","CompareStateLI");
        newHash.put("CompareStateCode","CompareStateCode");
        newHash.put("CompareZip","CompareZip");
        newHash.put("CompareCounty","CompareCounty");
        newHash.put("CompareEncounterRefID","CompareEncounterRefID");
        newHash.put("TotalResults","TotalResults");
        newHash.put("ProcedureDescription","ProcedureDescription");

        this.setEnglishFieldList(newHash);
    }
    public void makeExpiredHash()
    {

        java.util.Hashtable newHash = new java.util.Hashtable();
        this.setExpiredFieldList(newHash);
    }

    public java.util.Hashtable englishFieldList = new java.util.Hashtable();
    public java.util.Hashtable requiredFieldList = new java.util.Hashtable();
    public java.util.Hashtable expiredFieldList = new java.util.Hashtable();

    public boolean isComplete()
    {
	     boolean myVal = true;
	     java.util.Enumeration myKeys = this.getRequiredFieldList().keys(); 
	     while (myKeys.hasMoreElements()) 
	     {
	         String myElement = (String) myKeys.nextElement();
	         if (!isComplete(myElement))
	         {
	             myVal = false;
	         }
	     }
	     return myVal;
    }

    public String getEnglish(String fieldN)
    {
        String myVal = null;
        if (this.englishFieldList.containsKey(fieldN))
        {
            myVal = (String)this.englishFieldList.get(fieldN);
        }
        return myVal;
    }
    public java.util.Vector isComplete(int daysVector)
    {
	     boolean myVal = false;
	     java.util.Enumeration myKeys = this.getExpiredFieldList().keys(); 
	     java.util.Vector myExpiredList = new java.util.Vector(); 
	     while (myKeys.hasMoreElements()) 
	     {
	         String myElement = (String) myKeys.nextElement();
	         if (isExpired(myElement,daysVector))
	         {
	             myVal = true;
	             myExpiredList.addElement(myElement);
	         }
	     }
	     return myExpiredList;
    }

    public void setField(String fieldN, Object fieldV)
    {
     boolean myVal = false;
     try
     {
        java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat("yyyy-MM-dd");

        if (fieldN.equals("SchedulingTransactionID"))
        {
             this.setSchedulingTransactionID((Integer)fieldV);
        }

        else if (fieldN.equals("UniqueCreateDate"))
        {
            this.setUniqueCreateDate((java.util.Date)fieldV);
        }

        else if (fieldN.equals("UniqueModifyDate"))
        {
            this.setUniqueModifyDate((java.util.Date)fieldV);
        }

        else if (fieldN.equals("UniqueModifyComments"))
        {
            this.setUniqueModifyComments((String)fieldV);
        }

        else if (fieldN.equals("PracticeID"))
        {
             this.setPracticeID((Integer)fieldV);
        }

        else if (fieldN.equals("TransactionDate"))
        {
            this.setTransactionDate((java.util.Date)fieldV);
        }

        else if (fieldN.equals("ActionID"))
        {
             this.setActionID((Integer)fieldV);
        }

        else if (fieldN.equals("UserID"))
        {
             this.setUserID((Integer)fieldV);
        }

        else if (fieldN.equals("Comments"))
        {
            this.setComments((String)fieldV);
        }

        else if (fieldN.equals("CompareAddress1"))
        {
            this.setCompareAddress1((String)fieldV);
        }

        else if (fieldN.equals("CompareAddress2"))
        {
            this.setCompareAddress2((String)fieldV);
        }

        else if (fieldN.equals("CompareCity"))
        {
            this.setCompareCity((String)fieldV);
        }

        else if (fieldN.equals("CompareStateLI"))
        {
            this.setCompareStateLI((String)fieldV);
        }

        else if (fieldN.equals("CompareStateCode"))
        {
            this.setCompareStateCode((String)fieldV);
        }

        else if (fieldN.equals("CompareZip"))
        {
            this.setCompareZip((String)fieldV);
        }

        else if (fieldN.equals("CompareCounty"))
        {
            this.setCompareCounty((String)fieldV);
        }

        else if (fieldN.equals("CompareEncounterRefID"))
        {
             this.setCompareEncounterRefID((Integer)fieldV);
        }

        else if (fieldN.equals("TotalResults"))
        {
             this.setTotalResults((Integer)fieldV);
        }

        else if (fieldN.equals("ProcedureDescription"))
        {
            this.setProcedureDescription((String)fieldV);
        }

	    }
	    catch(Exception e2)
	    {}
	    //return myVal;
	}

    public String getFieldType(String fieldN)
    {
     String  myVal = null;
     try
     {
        java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat("yyyy-MM-dd");

        if (fieldN.equals("SchedulingTransactionID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("UniqueCreateDate"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("UniqueModifyDate"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("UniqueModifyComments"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("PracticeID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("TransactionDate"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("ActionID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("UserID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("Comments"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("CompareAddress1"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("CompareAddress2"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("CompareCity"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("CompareStateLI"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("CompareStateCode"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("CompareZip"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("CompareCounty"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("CompareEncounterRefID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("TotalResults"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("ProcedureDescription"))
        {
            myVal = "String";
        }

	    }
	    catch(Exception e2)
	    {}
	    return myVal;
	}

    public boolean isComplete(String fieldN)
    {
	     boolean myVal = false;
	     try
	     {
        java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat("yyyy-MM-dd");

	     if (fieldN.equals("SchedulingTransactionID"))

	     {
                    if (this.getSchedulingTransactionID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("UniqueCreateDate"))

	     {
                    if (this.getUniqueCreateDate().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("UniqueModifyDate"))

	     {
                    if (this.getUniqueModifyDate().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("UniqueModifyComments"))

	     {
                    if (this.getUniqueModifyComments().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("PracticeID"))

	     {
                    if (this.getPracticeID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("TransactionDate"))

	     {
                    if (this.getTransactionDate().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("ActionID"))

	     {
                    if (this.getActionID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("UserID"))

	     {
                    if (this.getUserID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("Comments"))

	     {
                    if (this.getComments().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("CompareAddress1"))

	     {
                    if (this.getCompareAddress1().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("CompareAddress2"))

	     {
                    if (this.getCompareAddress2().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("CompareCity"))

	     {
                    if (this.getCompareCity().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("CompareStateLI"))

	     {
                    if (this.getCompareStateLI().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("CompareStateCode"))

	     {
                    if (this.getCompareStateCode().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("CompareZip"))

	     {
                    if (this.getCompareZip().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("CompareCounty"))

	     {
                    if (this.getCompareCounty().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("CompareEncounterRefID"))

	     {
                    if (this.getCompareEncounterRefID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("TotalResults"))

	     {
                    if (this.getTotalResults().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("ProcedureDescription"))

	     {
                    if (this.getProcedureDescription().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	    }
	    catch(Exception e2)
	    {}
	    return myVal;
	}

    public boolean isExpired(String fieldN,int daysVector)
    {
        return isExpired(fieldN,new java.util.Date(),daysVector);
    }

    public boolean isExpired(String fieldN,java.util.Date refDate,int daysVector)
    {
	     boolean myVal = false;
	     try
	     {
        java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat("yyyy-MM-dd");

        if (fieldN.equals("UniqueCreateDate"))

	     {
                    if (this.isExpired(this.getUniqueCreateDate(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

        if (fieldN.equals("UniqueModifyDate"))

	     {
                    if (this.isExpired(this.getUniqueModifyDate(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

        if (fieldN.equals("TransactionDate"))

	     {
                    if (this.isExpired(this.getTransactionDate(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

	    }
	    catch(Exception e2)
	    {}
	    return myVal;
	}

    public java.util.Date getDateField(String fieldN)
    {
	     java.util.Date myVal = null;
	     try
	     {
        java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat("yyyy-MM-dd");

        if (fieldN.equals("UniqueCreateDate"))

	     {
             myVal = this.getUniqueCreateDate();
        }

        if (fieldN.equals("UniqueModifyDate"))

	     {
             myVal = this.getUniqueModifyDate();
        }

        if (fieldN.equals("TransactionDate"))

	     {
             myVal = this.getTransactionDate();
        }

	    }
	    catch(Exception e2)
	    {}
	    return myVal;
	}
	public boolean runFieldSecurity = false;
	public Integer UserSecurityID = null;
   public Integer iSecurityCheck = null;
}    // End of bltNIM3_SchedulingTransaction class definition
