package com.winstaff;

import javax.tools.JavaCompiler;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Map;
import java.util.List;
import java.util.Map.Entry;

/**
 * Created by IntelliJ IDEA.
 * User: ste
 * Date: 9/12/11
 * Time: 2:37 PM
 * To change this template use File | Settings | File Templates.
 */
public class NIM3_HCPProvider  implements com.berryworks.edireader.edit837.SegmentValuesProvider
{
    public ArrayList<String> build(Map<String, Object> arguments, String sLoop)
    {


        //2300 = claim
        //2400 = service

        /*
        int maxIndex = 0;
        for (String argument : arguments.keySet()) {
            if (argument.startsWith("HCP")) {
                int n = Integer.valueOf(argument.substring(3, 5)).intValue();
                if (n > maxIndex) maxIndex = n;
            }
        }
        */

        /*
            00 Zero Pricing (Not Covered Under Contract)
            01 Priced as Billed at 100%
            02 Priced at the Standard Fee Schedule
            03 Priced at a Contractual Percentage
            04 Bundled Pricing
            05 Peer Review Pricing
            07 Flat Rate Pricing
            08 Combination Pricing
            09 Maternity Pricing
            10 Other Pricing
            11 Lower of Cost
            12 Ratio of Cost
            13 Cost Reimbursed
            14 Adjustment Pricing
         */

        System.out.println("Using NIM3 HCP ["+sLoop+"]");
        if (sLoop.equalsIgnoreCase("2300")){
            System.out.println("Loop 2300]");

            ArrayList<String> services = (ArrayList<String>) arguments.get("services");

            EDI_HCPSegmentObject myEDIHCP = new EDI_HCPSegmentObject();
            for (Map.Entry entry : arguments.entrySet()) {
                String key = (String)entry.getKey();
                Object value = entry.getValue();
                if ((value instanceof String)) {
                    //Process String Values
                } else if ((value instanceof List)) {
                    //Process String Values
                    List list = (List)value;
                    for (int i = 0; i < list.size(); i++) {
                        Map o = (Map)list.get(i);
                        String temp_CPT = (String)o.get("cpt");
                        String temp_Price = (String) o.get("price");
                        String temp_amt_code = (String) o.get("amt_code");
                        String temp_amt_price = (String) o.get("amt_price");

                        BigDecimal temp_usePrice;
                        if (temp_amt_price!=null){
                            temp_usePrice = new BigDecimal(temp_amt_price);
                        } else {
                            temp_usePrice =  new BigDecimal(0.0);
                        }


                        System.out.println("*** temp_dPrice = " +temp_usePrice +" ***");
                        System.out.println("*** temp_CPT = " +temp_CPT +" ***");
                        System.out.println("*** amt_code  = " +temp_amt_code +" ***");
                        System.out.println("*** amt_price  = " +temp_amt_price +" ***");
                        myEDIHCP.addToCurrentAllow(temp_usePrice);
                        if (temp_CPT.compareTo("9999999")<0){
                            myEDIHCP.addToRepricedAmount(temp_usePrice.multiply(new BigDecimal("0.85")));
                        }
                        else {
                            myEDIHCP.addToRepricedAmount(temp_usePrice.multiply(new BigDecimal("1.00")));
                        }
                    }
                }
            }
            System.out.println("*** "+ myEDIHCP.getCurrentAllow()+ "|"+myEDIHCP.getRepricedAmount() + " = " + myEDIHCP.getRepricedSavingsAmount() +" ***");

            if (myEDIHCP.getRepricedSavingsAmount().compareTo(BigDecimal.ZERO)>0){
                myEDIHCP.setRepricingCode("10");
            }
            else if (myEDIHCP.getRepricedSavingsAmount().compareTo(BigDecimal.ZERO)<0){
                myEDIHCP.setRepricingCode("00");
            }
            else if (myEDIHCP.getRepricedSavingsAmount().compareTo(BigDecimal.ZERO)==0){
                myEDIHCP.setRepricingCode("01");
            }
            ArrayList result = new ArrayList();
            result.add("00");
            result.add(myEDIHCP.getRepricingCode());
            //result.add(myEDIHCP.getRepricedAmount().setScale(3, BigDecimal.ROUND_HALF_UP).doubleValue()+"");
            result.add(myEDIHCP.getRepricedAmount().toPlainString());
  //          result.add(myEDIHCP.getRepricedSavingsAmount().toPlainString());

            return result;


        } else if (sLoop.equalsIgnoreCase("2400")){
            ArrayList<String> services = (ArrayList<String>) arguments.get("service");

            EDI_HCPSegmentObject myEDIHCP = new EDI_HCPSegmentObject();
            for (Map.Entry entry : arguments.entrySet()) {
                String key = (String)entry.getKey();
                Object value = entry.getValue();
                if ((value instanceof String)) {
                    //Process String Values
                } else if ((value instanceof List)) {
                    //Process String Values
                    List list = (List)value;
                    for (int i = 0; i < list.size(); i++) {
                        Map o = (Map)list.get(i);
                        String temp_CPT = (String)o.get("cpt");
                        String temp_Price = (String) o.get("price");
                        String temp_amt_code = (String) o.get("amt_code");
                        String temp_amt_price = (String) o.get("amt_price");

                        BigDecimal temp_usePrice;
                        if (temp_amt_price!=null){
                            temp_usePrice = new BigDecimal(temp_amt_price);
                        } else {
                            temp_usePrice =  new BigDecimal(0.0);
                        }
                        System.out.println("*** temp_dPrice = " +temp_Price +" ***");
                        System.out.println("*** temp_CPT = " +temp_CPT +" ***");
                        System.out.println("*** amt_code  = " +temp_amt_code +" ***");
                        System.out.println("*** amt_price  = " +temp_amt_price +" ***");
                        System.out.println("**** USING  = " +temp_usePrice +" ****");
                        myEDIHCP.addToCurrentAllow(temp_usePrice);
                        if (temp_CPT.compareTo("999999")<0){
                            myEDIHCP.addToRepricedAmount(temp_usePrice.multiply(new BigDecimal("0.85")));
                        }
                        else {
                            myEDIHCP.addToRepricedAmount(temp_usePrice.multiply(new BigDecimal("1.00")));
                        }
                    }
                }
            }
            System.out.println("*** "+ myEDIHCP.getCurrentAllow()+ "|"+myEDIHCP.getRepricedAmount() + " = " + myEDIHCP.getRepricedSavingsAmount() +" ***");

            if (myEDIHCP.getRepricedSavingsAmount().compareTo(BigDecimal.ZERO)>0){
                myEDIHCP.setRepricingCode("10");
            }
            else if (myEDIHCP.getRepricedSavingsAmount().compareTo(BigDecimal.ZERO)<=0){
                myEDIHCP.setRepricingCode("00");
            }
            else if (myEDIHCP.getRepricedSavingsAmount().compareTo(BigDecimal.ZERO)==0){
                myEDIHCP.setRepricingCode("01");
            }
            ArrayList result = new ArrayList();
            result.add("00");
            result.add(myEDIHCP.getRepricingCode());
            //result.add(myEDIHCP.getRepricedAmount().setScale(3, BigDecimal.ROUND_HALF_UP).doubleValue()+"");
            result.add(myEDIHCP.getRepricedAmount().toPlainString());
//            result.add(myEDIHCP.getRepricedSavingsAmount().toPlainString());

            return result;
        } else {
            return null;
        }


    }
}