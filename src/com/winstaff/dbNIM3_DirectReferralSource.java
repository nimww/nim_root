

package com.winstaff;

import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public class dbNIM3_DirectReferralSource extends Object implements IntNIM3_DirectReferralSource
{

        db_NewBase    dbnbDB;

    public dbNIM3_DirectReferralSource()
    {
        dbnbDB = new db_NewBase( "NIM3_DirectReferralSource", "DirectReferralSourceID" );

    }    // End of default constructor

    public dbNIM3_DirectReferralSource( Integer iNewID )
    {
        dbnbDB = new db_NewBase( "NIM3_DirectReferralSource", "DirectReferralSourceID" );
        setUniqueID( iNewID );
    }    // End of Constructor knowing an ID

    public void commitData() throws SQLException
    {
            //no longer trapping/ignoring errors 2011-03-17
            dbnbDB.commitData();
    }    // End of commitData()

    public void commitDataForced() throws SQLException
    {
        dbnbDB.commitDataForced();
    }    // End of commitData()

    public Integer getUniqueID()
    {
        return dbnbDB.getUniqueID();
    }    // End of getUniqueID()

    public void setUniqueID( Integer iNewID )
    {
        dbnbDB.setUniqueID( iNewID );
    }    // End of setUniqueID()

    public void setModifyComments( String sNew )
    {
        dbnbDB.setModifyComments( sNew );
    }    // End of setUniqueID()


    public void setDirectReferralSourceID(Integer newValue)
    {
                dbnbDB.setFieldData( "DirectReferralSourceID", newValue.toString() );
    }

    public Integer getDirectReferralSourceID()
    {
        String           sValue = dbnbDB.getFieldData( "DirectReferralSourceID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setField(String fieldN, Object fieldV)
    {DebugLogger.println("setField Method not implemented yet in classNIM3_DirectReferralSource!");}

    public String getFieldType(String fieldN)
    {return null;}


    public void setUniqueCreateDate(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "UniqueCreateDate", formatter.format( newValue ) );
    }

    public Date getUniqueCreateDate()
    {
        String           sValue = dbnbDB.getFieldData( "UniqueCreateDate" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setUniqueModifyDate(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "UniqueModifyDate", formatter.format( newValue ) );
    }

    public Date getUniqueModifyDate()
    {
        String           sValue = dbnbDB.getFieldData( "UniqueModifyDate" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setUniqueModifyComments(String newValue)
    {
                dbnbDB.setFieldData( "UniqueModifyComments", newValue.toString() );
    }

    public String getUniqueModifyComments()
    {
        String           sValue = dbnbDB.getFieldData( "UniqueModifyComments" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setSourceName(String newValue)
    {
                dbnbDB.setFieldData( "SourceName", newValue.toString() );
    }

    public String getSourceName()
    {
        String           sValue = dbnbDB.getFieldData( "SourceName" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setSourcePhone(String newValue)
    {
                dbnbDB.setFieldData( "SourcePhone", newValue.toString() );
    }

    public String getSourcePhone()
    {
        String           sValue = dbnbDB.getFieldData( "SourcePhone" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setSourceFax(String newValue)
    {
                dbnbDB.setFieldData( "SourceFax", newValue.toString() );
    }

    public String getSourceFax()
    {
        String           sValue = dbnbDB.getFieldData( "SourceFax" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setSourceLandingWeb(String newValue)
    {
                dbnbDB.setFieldData( "SourceLandingWeb", newValue.toString() );
    }

    public String getSourceLandingWeb()
    {
        String           sValue = dbnbDB.getFieldData( "SourceLandingWeb" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setSourceWebsite(String newValue)
    {
                dbnbDB.setFieldData( "SourceWebsite", newValue.toString() );
    }

    public String getSourceWebsite()
    {
        String           sValue = dbnbDB.getFieldData( "SourceWebsite" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setSourceReferralCode(String newValue)
    {
                dbnbDB.setFieldData( "SourceReferralCode", newValue.toString() );
    }

    public String getSourceReferralCode()
    {
        String           sValue = dbnbDB.getFieldData( "SourceReferralCode" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setExternalID(String newValue)
    {
                dbnbDB.setFieldData( "ExternalID", newValue.toString() );
    }

    public String getExternalID()
    {
        String           sValue = dbnbDB.getFieldData( "ExternalID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setPayerID(Integer newValue)
    {
                dbnbDB.setFieldData( "PayerID", newValue.toString() );
    }

    public Integer getPayerID()
    {
        String           sValue = dbnbDB.getFieldData( "PayerID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

}    // End of blNIM3_DirectReferralSource class definition
