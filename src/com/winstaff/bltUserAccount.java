

package com.winstaff;

import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;
import java.util.Enumeration;

public class bltUserAccount extends Object implements InttUserAccount
{

    dbtUserAccount    dbDB;

    public bltUserAccount()
    {
        dbDB = new dbtUserAccount();
        dbDB.setUniqueCreateDate(new java.util.Date());
        makeRequiredHash();
        makeExpiredHash();
        makeEnglishHash();

    }    // End of default constructor

    public bltUserAccount( Integer iNewID )
    {        dbDB = new dbtUserAccount( iNewID );
        makeRequiredHash();
        makeExpiredHash();
        makeEnglishHash();
        iSecurityCheck = new Integer(2);
    }    // End of Constructor knowing an ID


    public bltUserAccount( Integer iNewID, Integer iGroupSecurityID )
    {
        this.GroupSecurityInit(iGroupSecurityID);
        dbDB = new dbtUserAccount( iNewID );
        makeExpiredHash();
        makeEnglishHash();
        makeRequiredHash(iGroupSecurityID);
    }    // End of Constructor knowing an ID

    public bltUserAccount( Integer iGroupSecurityID, boolean isTrue )
    {
        this.GroupSecurityInit(iGroupSecurityID);
        dbDB = new dbtUserAccount(  );
        makeExpiredHash();
        makeEnglishHash();
        makeRequiredHash(iGroupSecurityID);
    }    // End of Security unknown ID

    public void GroupSecurityInit( Integer iGroupSecurityID )
    {
        bltSecurityGroupMaster mySGM = new bltSecurityGroupMaster(iGroupSecurityID);
        this.UserSecurityID = iGroupSecurityID;
        if (mySGM.getRunFieldSecurityID().intValue()==1)
        {
            this.runFieldSecurity = true;
            makeRequiredHash(iGroupSecurityID);
        }
        iSecurityCheck = SecurityCheck.CheckItem("User1", iGroupSecurityID);
    }  

    public EventReferenceType getEventReference()
    {
        return EventUtils.getEventReference("tUserAccount", this.getUniqueID());
    }  

    public String convertString2LogString(String normalString) 
    {
        String myVal = "";
        for (int i=0;i<normalString.length();i++)
        {
            if ( (normalString.charAt(i)!='\r')&&(normalString.charAt(i)!='\n')&&(normalString.charAt(i)!='\t') )
            {
                myVal+=normalString.charAt(i);
            }
            else
            {
                myVal+="<NLT>";
            }
        }
        return myVal;
    }
    public void runAudit()
    {
        String AuditString = new java.util.Date()+" \t " ;  
        AuditString += " tUserAccount "; 
        AuditString += " UserID ="+this.getUniqueID(); 

        for (int i=0;i<this.AuditVector.size();i++)
        {
            AuditString+="\t"+convertString2LogString((String)this.AuditVector.elementAt(i));
        }
        AuditLogger.printLine(AuditString);
    }
    public void commitData() throws SQLException
    {
        runAudit();
        dbDB.setModifyComments(this.getUniqueModifyComments());
        dbDB.setUniqueModifyDate(new java.util.Date());
        dbDB.commitData();
        DataControlUtils.dataChanged("tUserAccount",this.getUniqueID() ,this.AuditVector.size(), "UserID");
        this.AuditVector.removeAllElements();
    }    // End of commitData()

    public void commitDataForced() throws SQLException
    {
        dbDB.commitDataForced();
    }    // End of commitData()

    public Integer getUniqueID()
    {
        return dbDB.getUniqueID();
    }    // End of getUniqueID()

    public void setUniqueID( Integer iNewID )
    {
        dbDB.setUniqueID( iNewID );
    }    // End of setUniqueID()


    public void setUserID(Integer newValue)
    {
        dbDB.setUserID(newValue);
    }

    public Integer getUserID()
    {
        return dbDB.getUserID();
    }

    java.util.Vector AuditVector = new java.util.Vector();

    public void setUniqueCreateDate(Date newValue)
    {
        this.setUniqueCreateDate(newValue,this.UserSecurityID);


    }
    public Date getUniqueCreateDate()
    {
        return this.getUniqueCreateDate(this.UserSecurityID);
    }

    public void setUniqueCreateDate(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='UniqueCreateDate' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[UniqueCreateDate]=["+newValue+"]");
                   dbDB.setUniqueCreateDate(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[UniqueCreateDate]=["+newValue+"]");
           dbDB.setUniqueCreateDate(newValue);
         }
    }
    public Date getUniqueCreateDate(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='UniqueCreateDate' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getUniqueCreateDate();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getUniqueCreateDate();
         }
        return myVal;
    }

    public void setUniqueModifyDate(Date newValue)
    {
        this.setUniqueModifyDate(newValue,this.UserSecurityID);


    }
    public Date getUniqueModifyDate()
    {
        return this.getUniqueModifyDate(this.UserSecurityID);
    }

    public void setUniqueModifyDate(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='UniqueModifyDate' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[UniqueModifyDate]=["+newValue+"]");
                   dbDB.setUniqueModifyDate(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[UniqueModifyDate]=["+newValue+"]");
           dbDB.setUniqueModifyDate(newValue);
         }
    }
    public Date getUniqueModifyDate(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='UniqueModifyDate' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getUniqueModifyDate();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getUniqueModifyDate();
         }
        return myVal;
    }

    public void setUniqueModifyComments(String newValue)
    {
        this.setUniqueModifyComments(newValue,this.UserSecurityID);


    }
    public String getUniqueModifyComments()
    {
        return this.getUniqueModifyComments(this.UserSecurityID);
    }

    public void setUniqueModifyComments(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='UniqueModifyComments' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[UniqueModifyComments]=["+newValue+"]");
                   dbDB.setUniqueModifyComments(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[UniqueModifyComments]=["+newValue+"]");
           dbDB.setUniqueModifyComments(newValue);
         }
    }
    public String getUniqueModifyComments(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='UniqueModifyComments' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getUniqueModifyComments();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getUniqueModifyComments();
         }
        return myVal;
    }

    public void setPLCID(Integer newValue)
    {
        this.setPLCID(newValue,this.UserSecurityID);


    }
    public Integer getPLCID()
    {
        return this.getPLCID(this.UserSecurityID);
    }

    public void setPLCID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PLCID' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[PLCID]=["+newValue+"]");
                   dbDB.setPLCID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[PLCID]=["+newValue+"]");
           dbDB.setPLCID(newValue);
         }
    }
    public Integer getPLCID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PLCID' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPLCID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPLCID();
         }
        return myVal;
    }

    public void setStartPage(String newValue)
    {
        this.setStartPage(newValue,this.UserSecurityID);


    }
    public String getStartPage()
    {
        return this.getStartPage(this.UserSecurityID);
    }

    public void setStartPage(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='StartPage' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[StartPage]=["+newValue+"]");
                   dbDB.setStartPage(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[StartPage]=["+newValue+"]");
           dbDB.setStartPage(newValue);
         }
    }
    public String getStartPage(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='StartPage' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getStartPage();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getStartPage();
         }
        return myVal;
    }

    public void setAccountType(String newValue)
    {
        this.setAccountType(newValue,this.UserSecurityID);


    }
    public String getAccountType()
    {
        return this.getAccountType(this.UserSecurityID);
    }

    public void setAccountType(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AccountType' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[AccountType]=["+newValue+"]");
                   dbDB.setAccountType(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[AccountType]=["+newValue+"]");
           dbDB.setAccountType(newValue);
         }
    }
    public String getAccountType(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AccountType' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getAccountType();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getAccountType();
         }
        return myVal;
    }

    public void setSecurityGroupID(Integer newValue)
    {
        this.setSecurityGroupID(newValue,this.UserSecurityID);


    }
    public Integer getSecurityGroupID()
    {
        return this.getSecurityGroupID(this.UserSecurityID);
    }

    public void setSecurityGroupID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SecurityGroupID' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[SecurityGroupID]=["+newValue+"]");
                   dbDB.setSecurityGroupID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[SecurityGroupID]=["+newValue+"]");
           dbDB.setSecurityGroupID(newValue);
         }
    }
    public Integer getSecurityGroupID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SecurityGroupID' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getSecurityGroupID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getSecurityGroupID();
         }
        return myVal;
    }

    public void setGenericSecurityGroupID(Integer newValue)
    {
        this.setGenericSecurityGroupID(newValue,this.UserSecurityID);


    }
    public Integer getGenericSecurityGroupID()
    {
        return this.getGenericSecurityGroupID(this.UserSecurityID);
    }

    public void setGenericSecurityGroupID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='GenericSecurityGroupID' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[GenericSecurityGroupID]=["+newValue+"]");
                   dbDB.setGenericSecurityGroupID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[GenericSecurityGroupID]=["+newValue+"]");
           dbDB.setGenericSecurityGroupID(newValue);
         }
    }
    public Integer getGenericSecurityGroupID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='GenericSecurityGroupID' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getGenericSecurityGroupID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getGenericSecurityGroupID();
         }
        return myVal;
    }

    public void setReferenceID(Integer newValue)
    {
        this.setReferenceID(newValue,this.UserSecurityID);


    }
    public Integer getReferenceID()
    {
        return this.getReferenceID(this.UserSecurityID);
    }

    public void setReferenceID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ReferenceID' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[ReferenceID]=["+newValue+"]");
                   dbDB.setReferenceID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[ReferenceID]=["+newValue+"]");
           dbDB.setReferenceID(newValue);
         }
    }
    public Integer getReferenceID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ReferenceID' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getReferenceID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getReferenceID();
         }
        return myVal;
    }

    public void setAccessType(Integer newValue)
    {
        this.setAccessType(newValue,this.UserSecurityID);


    }
    public Integer getAccessType()
    {
        return this.getAccessType(this.UserSecurityID);
    }

    public void setAccessType(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AccessType' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[AccessType]=["+newValue+"]");
                   dbDB.setAccessType(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[AccessType]=["+newValue+"]");
           dbDB.setAccessType(newValue);
         }
    }
    public Integer getAccessType(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AccessType' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getAccessType();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getAccessType();
         }
        return myVal;
    }

    public void setStatus(Integer newValue)
    {
        this.setStatus(newValue,this.UserSecurityID);


    }
    public Integer getStatus()
    {
        return this.getStatus(this.UserSecurityID);
    }

    public void setStatus(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Status' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[Status]=["+newValue+"]");
                   dbDB.setStatus(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[Status]=["+newValue+"]");
           dbDB.setStatus(newValue);
         }
    }
    public Integer getStatus(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Status' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getStatus();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getStatus();
         }
        return myVal;
    }

    public void setLogonUserName(String newValue)
    {
        this.setLogonUserName(newValue,this.UserSecurityID);


    }
    public String getLogonUserName()
    {
        return this.getLogonUserName(this.UserSecurityID);
    }

    public void setLogonUserName(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='LogonUserName' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[LogonUserName]=["+newValue+"]");
                   dbDB.setLogonUserName(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[LogonUserName]=["+newValue+"]");
           dbDB.setLogonUserName(newValue);
         }
    }
    public String getLogonUserName(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='LogonUserName' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getLogonUserName();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getLogonUserName();
         }
        return myVal;
    }

    public void setLogonUserPassword(String newValue)
    {
        this.setLogonUserPassword(newValue,this.UserSecurityID);


    }
    public String getLogonUserPassword()
    {
        return this.getLogonUserPassword(this.UserSecurityID);
    }

    public void setLogonUserPassword(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='LogonUserPassword' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[LogonUserPassword]=["+newValue+"]");
                   dbDB.setLogonUserPassword(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[LogonUserPassword]=["+newValue+"]");
           dbDB.setLogonUserPassword(newValue);
         }
    }
    public String getLogonUserPassword(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='LogonUserPassword' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getLogonUserPassword();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getLogonUserPassword();
         }
        return myVal;
    }

    public void setAttestKeyword1(String newValue)
    {
        this.setAttestKeyword1(newValue,this.UserSecurityID);


    }
    public String getAttestKeyword1()
    {
        return this.getAttestKeyword1(this.UserSecurityID);
    }

    public void setAttestKeyword1(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AttestKeyword1' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[AttestKeyword1]=["+newValue+"]");
                   dbDB.setAttestKeyword1(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[AttestKeyword1]=["+newValue+"]");
           dbDB.setAttestKeyword1(newValue);
         }
    }
    public String getAttestKeyword1(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AttestKeyword1' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getAttestKeyword1();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getAttestKeyword1();
         }
        return myVal;
    }

    public void setAttestKeyword2(String newValue)
    {
        this.setAttestKeyword2(newValue,this.UserSecurityID);


    }
    public String getAttestKeyword2()
    {
        return this.getAttestKeyword2(this.UserSecurityID);
    }

    public void setAttestKeyword2(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AttestKeyword2' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[AttestKeyword2]=["+newValue+"]");
                   dbDB.setAttestKeyword2(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[AttestKeyword2]=["+newValue+"]");
           dbDB.setAttestKeyword2(newValue);
         }
    }
    public String getAttestKeyword2(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AttestKeyword2' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getAttestKeyword2();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getAttestKeyword2();
         }
        return myVal;
    }

    public void setAttestKeywordTemp1(String newValue)
    {
        this.setAttestKeywordTemp1(newValue,this.UserSecurityID);


    }
    public String getAttestKeywordTemp1()
    {
        return this.getAttestKeywordTemp1(this.UserSecurityID);
    }

    public void setAttestKeywordTemp1(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AttestKeywordTemp1' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[AttestKeywordTemp1]=["+newValue+"]");
                   dbDB.setAttestKeywordTemp1(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[AttestKeywordTemp1]=["+newValue+"]");
           dbDB.setAttestKeywordTemp1(newValue);
         }
    }
    public String getAttestKeywordTemp1(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AttestKeywordTemp1' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getAttestKeywordTemp1();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getAttestKeywordTemp1();
         }
        return myVal;
    }

    public void setAttestKeywordTemp2(String newValue)
    {
        this.setAttestKeywordTemp2(newValue,this.UserSecurityID);


    }
    public String getAttestKeywordTemp2()
    {
        return this.getAttestKeywordTemp2(this.UserSecurityID);
    }

    public void setAttestKeywordTemp2(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AttestKeywordTemp2' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[AttestKeywordTemp2]=["+newValue+"]");
                   dbDB.setAttestKeywordTemp2(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[AttestKeywordTemp2]=["+newValue+"]");
           dbDB.setAttestKeywordTemp2(newValue);
         }
    }
    public String getAttestKeywordTemp2(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AttestKeywordTemp2' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getAttestKeywordTemp2();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getAttestKeywordTemp2();
         }
        return myVal;
    }

    public void setContactFirstName(String newValue)
    {
        this.setContactFirstName(newValue,this.UserSecurityID);


    }
    public String getContactFirstName()
    {
        return this.getContactFirstName(this.UserSecurityID);
    }

    public void setContactFirstName(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ContactFirstName' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[ContactFirstName]=["+newValue+"]");
                   dbDB.setContactFirstName(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[ContactFirstName]=["+newValue+"]");
           dbDB.setContactFirstName(newValue);
         }
    }
    public String getContactFirstName(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ContactFirstName' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getContactFirstName();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getContactFirstName();
         }
        return myVal;
    }

    public void setContactLastName(String newValue)
    {
        this.setContactLastName(newValue,this.UserSecurityID);


    }
    public String getContactLastName()
    {
        return this.getContactLastName(this.UserSecurityID);
    }

    public void setContactLastName(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ContactLastName' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[ContactLastName]=["+newValue+"]");
                   dbDB.setContactLastName(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[ContactLastName]=["+newValue+"]");
           dbDB.setContactLastName(newValue);
         }
    }
    public String getContactLastName(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ContactLastName' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getContactLastName();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getContactLastName();
         }
        return myVal;
    }

    public void setContactEmail(String newValue)
    {
        this.setContactEmail(newValue,this.UserSecurityID);


    }
    public String getContactEmail()
    {
        return this.getContactEmail(this.UserSecurityID);
    }

    public void setContactEmail(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ContactEmail' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[ContactEmail]=["+newValue+"]");
                   dbDB.setContactEmail(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[ContactEmail]=["+newValue+"]");
           dbDB.setContactEmail(newValue);
         }
    }
    public String getContactEmail(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ContactEmail' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getContactEmail();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getContactEmail();
         }
        return myVal;
    }

    public void setContactEmail2(String newValue)
    {
        this.setContactEmail2(newValue,this.UserSecurityID);


    }
    public String getContactEmail2()
    {
        return this.getContactEmail2(this.UserSecurityID);
    }

    public void setContactEmail2(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ContactEmail2' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[ContactEmail2]=["+newValue+"]");
                   dbDB.setContactEmail2(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[ContactEmail2]=["+newValue+"]");
           dbDB.setContactEmail2(newValue);
         }
    }
    public String getContactEmail2(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ContactEmail2' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getContactEmail2();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getContactEmail2();
         }
        return myVal;
    }

    public void setContactAddress1(String newValue)
    {
        this.setContactAddress1(newValue,this.UserSecurityID);


    }
    public String getContactAddress1()
    {
        return this.getContactAddress1(this.UserSecurityID);
    }

    public void setContactAddress1(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ContactAddress1' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[ContactAddress1]=["+newValue+"]");
                   dbDB.setContactAddress1(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[ContactAddress1]=["+newValue+"]");
           dbDB.setContactAddress1(newValue);
         }
    }
    public String getContactAddress1(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ContactAddress1' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getContactAddress1();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getContactAddress1();
         }
        return myVal;
    }

    public void setContactAddress2(String newValue)
    {
        this.setContactAddress2(newValue,this.UserSecurityID);


    }
    public String getContactAddress2()
    {
        return this.getContactAddress2(this.UserSecurityID);
    }

    public void setContactAddress2(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ContactAddress2' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[ContactAddress2]=["+newValue+"]");
                   dbDB.setContactAddress2(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[ContactAddress2]=["+newValue+"]");
           dbDB.setContactAddress2(newValue);
         }
    }
    public String getContactAddress2(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ContactAddress2' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getContactAddress2();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getContactAddress2();
         }
        return myVal;
    }

    public void setContactCity(String newValue)
    {
        this.setContactCity(newValue,this.UserSecurityID);


    }
    public String getContactCity()
    {
        return this.getContactCity(this.UserSecurityID);
    }

    public void setContactCity(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ContactCity' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[ContactCity]=["+newValue+"]");
                   dbDB.setContactCity(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[ContactCity]=["+newValue+"]");
           dbDB.setContactCity(newValue);
         }
    }
    public String getContactCity(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ContactCity' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getContactCity();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getContactCity();
         }
        return myVal;
    }

    public void setContactStateID(Integer newValue)
    {
        this.setContactStateID(newValue,this.UserSecurityID);


    }
    public Integer getContactStateID()
    {
        return this.getContactStateID(this.UserSecurityID);
    }

    public void setContactStateID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ContactStateID' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[ContactStateID]=["+newValue+"]");
                   dbDB.setContactStateID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[ContactStateID]=["+newValue+"]");
           dbDB.setContactStateID(newValue);
         }
    }
    public Integer getContactStateID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ContactStateID' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getContactStateID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getContactStateID();
         }
        return myVal;
    }

    public void setContactProvince(String newValue)
    {
        this.setContactProvince(newValue,this.UserSecurityID);


    }
    public String getContactProvince()
    {
        return this.getContactProvince(this.UserSecurityID);
    }

    public void setContactProvince(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ContactProvince' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[ContactProvince]=["+newValue+"]");
                   dbDB.setContactProvince(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[ContactProvince]=["+newValue+"]");
           dbDB.setContactProvince(newValue);
         }
    }
    public String getContactProvince(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ContactProvince' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getContactProvince();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getContactProvince();
         }
        return myVal;
    }

    public void setContactZIP(String newValue)
    {
        this.setContactZIP(newValue,this.UserSecurityID);


    }
    public String getContactZIP()
    {
        return this.getContactZIP(this.UserSecurityID);
    }

    public void setContactZIP(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ContactZIP' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[ContactZIP]=["+newValue+"]");
                   dbDB.setContactZIP(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[ContactZIP]=["+newValue+"]");
           dbDB.setContactZIP(newValue);
         }
    }
    public String getContactZIP(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ContactZIP' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getContactZIP();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getContactZIP();
         }
        return myVal;
    }

    public void setContactCountryID(Integer newValue)
    {
        this.setContactCountryID(newValue,this.UserSecurityID);


    }
    public Integer getContactCountryID()
    {
        return this.getContactCountryID(this.UserSecurityID);
    }

    public void setContactCountryID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ContactCountryID' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[ContactCountryID]=["+newValue+"]");
                   dbDB.setContactCountryID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[ContactCountryID]=["+newValue+"]");
           dbDB.setContactCountryID(newValue);
         }
    }
    public Integer getContactCountryID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ContactCountryID' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getContactCountryID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getContactCountryID();
         }
        return myVal;
    }

    public void setContactPhone(String newValue)
    {
        this.setContactPhone(newValue,this.UserSecurityID);


    }
    public String getContactPhone()
    {
        return this.getContactPhone(this.UserSecurityID);
    }

    public void setContactPhone(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ContactPhone' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[ContactPhone]=["+newValue+"]");
                   dbDB.setContactPhone(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[ContactPhone]=["+newValue+"]");
           dbDB.setContactPhone(newValue);
         }
    }
    public String getContactPhone(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ContactPhone' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getContactPhone();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getContactPhone();
         }
        return myVal;
    }

    public void setContactFax(String newValue)
    {
        this.setContactFax(newValue,this.UserSecurityID);


    }
    public String getContactFax()
    {
        return this.getContactFax(this.UserSecurityID);
    }

    public void setContactFax(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ContactFax' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[ContactFax]=["+newValue+"]");
                   dbDB.setContactFax(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[ContactFax]=["+newValue+"]");
           dbDB.setContactFax(newValue);
         }
    }
    public String getContactFax(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ContactFax' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getContactFax();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getContactFax();
         }
        return myVal;
    }

    public void setContactMobile(String newValue)
    {
        this.setContactMobile(newValue,this.UserSecurityID);


    }
    public String getContactMobile()
    {
        return this.getContactMobile(this.UserSecurityID);
    }

    public void setContactMobile(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ContactMobile' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[ContactMobile]=["+newValue+"]");
                   dbDB.setContactMobile(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[ContactMobile]=["+newValue+"]");
           dbDB.setContactMobile(newValue);
         }
    }
    public String getContactMobile(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ContactMobile' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getContactMobile();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getContactMobile();
         }
        return myVal;
    }

    public void setSecretQuestion(String newValue)
    {
        this.setSecretQuestion(newValue,this.UserSecurityID);


    }
    public String getSecretQuestion()
    {
        return this.getSecretQuestion(this.UserSecurityID);
    }

    public void setSecretQuestion(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SecretQuestion' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[SecretQuestion]=["+newValue+"]");
                   dbDB.setSecretQuestion(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[SecretQuestion]=["+newValue+"]");
           dbDB.setSecretQuestion(newValue);
         }
    }
    public String getSecretQuestion(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SecretQuestion' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getSecretQuestion();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getSecretQuestion();
         }
        return myVal;
    }

    public void setSecretAnswer(String newValue)
    {
        this.setSecretAnswer(newValue,this.UserSecurityID);


    }
    public String getSecretAnswer()
    {
        return this.getSecretAnswer(this.UserSecurityID);
    }

    public void setSecretAnswer(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SecretAnswer' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[SecretAnswer]=["+newValue+"]");
                   dbDB.setSecretAnswer(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[SecretAnswer]=["+newValue+"]");
           dbDB.setSecretAnswer(newValue);
         }
    }
    public String getSecretAnswer(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SecretAnswer' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getSecretAnswer();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getSecretAnswer();
         }
        return myVal;
    }

    public void setCreditCardFullName(String newValue)
    {
        this.setCreditCardFullName(newValue,this.UserSecurityID);


    }
    public String getCreditCardFullName()
    {
        return this.getCreditCardFullName(this.UserSecurityID);
    }

    public void setCreditCardFullName(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CreditCardFullName' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[CreditCardFullName]=["+newValue+"]");
                   dbDB.setCreditCardFullName(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[CreditCardFullName]=["+newValue+"]");
           dbDB.setCreditCardFullName(newValue);
         }
    }
    public String getCreditCardFullName(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CreditCardFullName' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getCreditCardFullName();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getCreditCardFullName();
         }
        return myVal;
    }

    public void setCreditCardNumber(String newValue)
    {
        this.setCreditCardNumber(newValue,this.UserSecurityID);


    }
    public String getCreditCardNumber()
    {
        return this.getCreditCardNumber(this.UserSecurityID);
    }

    public void setCreditCardNumber(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CreditCardNumber' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[CreditCardNumber]=["+newValue+"]");
                   dbDB.setCreditCardNumber(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[CreditCardNumber]=["+newValue+"]");
           dbDB.setCreditCardNumber(newValue);
         }
    }
    public String getCreditCardNumber(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CreditCardNumber' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getCreditCardNumber();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getCreditCardNumber();
         }
        return myVal;
    }

    public void setCreditCardType(String newValue)
    {
        this.setCreditCardType(newValue,this.UserSecurityID);


    }
    public String getCreditCardType()
    {
        return this.getCreditCardType(this.UserSecurityID);
    }

    public void setCreditCardType(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CreditCardType' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[CreditCardType]=["+newValue+"]");
                   dbDB.setCreditCardType(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[CreditCardType]=["+newValue+"]");
           dbDB.setCreditCardType(newValue);
         }
    }
    public String getCreditCardType(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CreditCardType' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getCreditCardType();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getCreditCardType();
         }
        return myVal;
    }

    public void setCreditCardExpMonth(String newValue)
    {
        this.setCreditCardExpMonth(newValue,this.UserSecurityID);


    }
    public String getCreditCardExpMonth()
    {
        return this.getCreditCardExpMonth(this.UserSecurityID);
    }

    public void setCreditCardExpMonth(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CreditCardExpMonth' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[CreditCardExpMonth]=["+newValue+"]");
                   dbDB.setCreditCardExpMonth(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[CreditCardExpMonth]=["+newValue+"]");
           dbDB.setCreditCardExpMonth(newValue);
         }
    }
    public String getCreditCardExpMonth(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CreditCardExpMonth' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getCreditCardExpMonth();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getCreditCardExpMonth();
         }
        return myVal;
    }

    public void setCreditCardExpYear(String newValue)
    {
        this.setCreditCardExpYear(newValue,this.UserSecurityID);


    }
    public String getCreditCardExpYear()
    {
        return this.getCreditCardExpYear(this.UserSecurityID);
    }

    public void setCreditCardExpYear(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CreditCardExpYear' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[CreditCardExpYear]=["+newValue+"]");
                   dbDB.setCreditCardExpYear(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[CreditCardExpYear]=["+newValue+"]");
           dbDB.setCreditCardExpYear(newValue);
         }
    }
    public String getCreditCardExpYear(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CreditCardExpYear' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getCreditCardExpYear();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getCreditCardExpYear();
         }
        return myVal;
    }

    public void setCreditCardChargeDate(Date newValue)
    {
        this.setCreditCardChargeDate(newValue,this.UserSecurityID);


    }
    public Date getCreditCardChargeDate()
    {
        return this.getCreditCardChargeDate(this.UserSecurityID);
    }

    public void setCreditCardChargeDate(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CreditCardChargeDate' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[CreditCardChargeDate]=["+newValue+"]");
                   dbDB.setCreditCardChargeDate(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[CreditCardChargeDate]=["+newValue+"]");
           dbDB.setCreditCardChargeDate(newValue);
         }
    }
    public Date getCreditCardChargeDate(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CreditCardChargeDate' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getCreditCardChargeDate();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getCreditCardChargeDate();
         }
        return myVal;
    }

    public void setCreditCardPostDate(Date newValue)
    {
        this.setCreditCardPostDate(newValue,this.UserSecurityID);


    }
    public Date getCreditCardPostDate()
    {
        return this.getCreditCardPostDate(this.UserSecurityID);
    }

    public void setCreditCardPostDate(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CreditCardPostDate' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[CreditCardPostDate]=["+newValue+"]");
                   dbDB.setCreditCardPostDate(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[CreditCardPostDate]=["+newValue+"]");
           dbDB.setCreditCardPostDate(newValue);
         }
    }
    public Date getCreditCardPostDate(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CreditCardPostDate' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getCreditCardPostDate();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getCreditCardPostDate();
         }
        return myVal;
    }

    public void setAlertEmail(String newValue)
    {
        this.setAlertEmail(newValue,this.UserSecurityID);


    }
    public String getAlertEmail()
    {
        return this.getAlertEmail(this.UserSecurityID);
    }

    public void setAlertEmail(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AlertEmail' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[AlertEmail]=["+newValue+"]");
                   dbDB.setAlertEmail(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[AlertEmail]=["+newValue+"]");
           dbDB.setAlertEmail(newValue);
         }
    }
    public String getAlertEmail(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AlertEmail' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getAlertEmail();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getAlertEmail();
         }
        return myVal;
    }

    public void setAlertDays(Integer newValue)
    {
        this.setAlertDays(newValue,this.UserSecurityID);


    }
    public Integer getAlertDays()
    {
        return this.getAlertDays(this.UserSecurityID);
    }

    public void setAlertDays(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AlertDays' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[AlertDays]=["+newValue+"]");
                   dbDB.setAlertDays(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[AlertDays]=["+newValue+"]");
           dbDB.setAlertDays(newValue);
         }
    }
    public Integer getAlertDays(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AlertDays' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getAlertDays();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getAlertDays();
         }
        return myVal;
    }

    public void setBillingComments(String newValue)
    {
        this.setBillingComments(newValue,this.UserSecurityID);


    }
    public String getBillingComments()
    {
        return this.getBillingComments(this.UserSecurityID);
    }

    public void setBillingComments(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='BillingComments' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[BillingComments]=["+newValue+"]");
                   dbDB.setBillingComments(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[BillingComments]=["+newValue+"]");
           dbDB.setBillingComments(newValue);
         }
    }
    public String getBillingComments(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='BillingComments' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getBillingComments();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getBillingComments();
         }
        return myVal;
    }

    public void setPromoCode(String newValue)
    {
        this.setPromoCode(newValue,this.UserSecurityID);


    }
    public String getPromoCode()
    {
        return this.getPromoCode(this.UserSecurityID);
    }

    public void setPromoCode(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PromoCode' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[PromoCode]=["+newValue+"]");
                   dbDB.setPromoCode(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[PromoCode]=["+newValue+"]");
           dbDB.setPromoCode(newValue);
         }
    }
    public String getPromoCode(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PromoCode' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPromoCode();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPromoCode();
         }
        return myVal;
    }

    public void setCompanyType(Integer newValue)
    {
        this.setCompanyType(newValue,this.UserSecurityID);


    }
    public Integer getCompanyType()
    {
        return this.getCompanyType(this.UserSecurityID);
    }

    public void setCompanyType(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CompanyType' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[CompanyType]=["+newValue+"]");
                   dbDB.setCompanyType(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[CompanyType]=["+newValue+"]");
           dbDB.setCompanyType(newValue);
         }
    }
    public Integer getCompanyType(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CompanyType' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getCompanyType();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getCompanyType();
         }
        return myVal;
    }

    public void setCompanyName(String newValue)
    {
        this.setCompanyName(newValue,this.UserSecurityID);


    }
    public String getCompanyName()
    {
        return this.getCompanyName(this.UserSecurityID);
    }

    public void setCompanyName(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CompanyName' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[CompanyName]=["+newValue+"]");
                   dbDB.setCompanyName(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[CompanyName]=["+newValue+"]");
           dbDB.setCompanyName(newValue);
         }
    }
    public String getCompanyName(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CompanyName' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getCompanyName();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getCompanyName();
         }
        return myVal;
    }

    public void setCompanyAddress1(String newValue)
    {
        this.setCompanyAddress1(newValue,this.UserSecurityID);


    }
    public String getCompanyAddress1()
    {
        return this.getCompanyAddress1(this.UserSecurityID);
    }

    public void setCompanyAddress1(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CompanyAddress1' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[CompanyAddress1]=["+newValue+"]");
                   dbDB.setCompanyAddress1(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[CompanyAddress1]=["+newValue+"]");
           dbDB.setCompanyAddress1(newValue);
         }
    }
    public String getCompanyAddress1(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CompanyAddress1' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getCompanyAddress1();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getCompanyAddress1();
         }
        return myVal;
    }

    public void setCompanyAddress2(String newValue)
    {
        this.setCompanyAddress2(newValue,this.UserSecurityID);


    }
    public String getCompanyAddress2()
    {
        return this.getCompanyAddress2(this.UserSecurityID);
    }

    public void setCompanyAddress2(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CompanyAddress2' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[CompanyAddress2]=["+newValue+"]");
                   dbDB.setCompanyAddress2(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[CompanyAddress2]=["+newValue+"]");
           dbDB.setCompanyAddress2(newValue);
         }
    }
    public String getCompanyAddress2(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CompanyAddress2' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getCompanyAddress2();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getCompanyAddress2();
         }
        return myVal;
    }

    public void setCompanyCity(String newValue)
    {
        this.setCompanyCity(newValue,this.UserSecurityID);


    }
    public String getCompanyCity()
    {
        return this.getCompanyCity(this.UserSecurityID);
    }

    public void setCompanyCity(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CompanyCity' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[CompanyCity]=["+newValue+"]");
                   dbDB.setCompanyCity(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[CompanyCity]=["+newValue+"]");
           dbDB.setCompanyCity(newValue);
         }
    }
    public String getCompanyCity(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CompanyCity' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getCompanyCity();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getCompanyCity();
         }
        return myVal;
    }

    public void setCompanyStateID(Integer newValue)
    {
        this.setCompanyStateID(newValue,this.UserSecurityID);


    }
    public Integer getCompanyStateID()
    {
        return this.getCompanyStateID(this.UserSecurityID);
    }

    public void setCompanyStateID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CompanyStateID' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[CompanyStateID]=["+newValue+"]");
                   dbDB.setCompanyStateID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[CompanyStateID]=["+newValue+"]");
           dbDB.setCompanyStateID(newValue);
         }
    }
    public Integer getCompanyStateID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CompanyStateID' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getCompanyStateID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getCompanyStateID();
         }
        return myVal;
    }

    public void setCompanyZIP(String newValue)
    {
        this.setCompanyZIP(newValue,this.UserSecurityID);


    }
    public String getCompanyZIP()
    {
        return this.getCompanyZIP(this.UserSecurityID);
    }

    public void setCompanyZIP(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CompanyZIP' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[CompanyZIP]=["+newValue+"]");
                   dbDB.setCompanyZIP(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[CompanyZIP]=["+newValue+"]");
           dbDB.setCompanyZIP(newValue);
         }
    }
    public String getCompanyZIP(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CompanyZIP' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getCompanyZIP();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getCompanyZIP();
         }
        return myVal;
    }

    public void setCompanyPhone(String newValue)
    {
        this.setCompanyPhone(newValue,this.UserSecurityID);


    }
    public String getCompanyPhone()
    {
        return this.getCompanyPhone(this.UserSecurityID);
    }

    public void setCompanyPhone(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CompanyPhone' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[CompanyPhone]=["+newValue+"]");
                   dbDB.setCompanyPhone(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[CompanyPhone]=["+newValue+"]");
           dbDB.setCompanyPhone(newValue);
         }
    }
    public String getCompanyPhone(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CompanyPhone' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getCompanyPhone();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getCompanyPhone();
         }
        return myVal;
    }

    public void setCompanyFax(String newValue)
    {
        this.setCompanyFax(newValue,this.UserSecurityID);


    }
    public String getCompanyFax()
    {
        return this.getCompanyFax(this.UserSecurityID);
    }

    public void setCompanyFax(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CompanyFax' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[CompanyFax]=["+newValue+"]");
                   dbDB.setCompanyFax(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[CompanyFax]=["+newValue+"]");
           dbDB.setCompanyFax(newValue);
         }
    }
    public String getCompanyFax(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CompanyFax' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getCompanyFax();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getCompanyFax();
         }
        return myVal;
    }

    public void setComments(String newValue)
    {
        this.setComments(newValue,this.UserSecurityID);


    }
    public String getComments()
    {
        return this.getComments(this.UserSecurityID);
    }

    public void setComments(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Comments' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[Comments]=["+newValue+"]");
                   dbDB.setComments(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[Comments]=["+newValue+"]");
           dbDB.setComments(newValue);
         }
    }
    public String getComments(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Comments' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getComments();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getComments();
         }
        return myVal;
    }

    public void setBillingID(Integer newValue)
    {
        this.setBillingID(newValue,this.UserSecurityID);


    }
    public Integer getBillingID()
    {
        return this.getBillingID(this.UserSecurityID);
    }

    public void setBillingID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='BillingID' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[BillingID]=["+newValue+"]");
                   dbDB.setBillingID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[BillingID]=["+newValue+"]");
           dbDB.setBillingID(newValue);
         }
    }
    public Integer getBillingID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='BillingID' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getBillingID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getBillingID();
         }
        return myVal;
    }

    public void setPHDBAcknowledgementStatus(Integer newValue)
    {
        this.setPHDBAcknowledgementStatus(newValue,this.UserSecurityID);


    }
    public Integer getPHDBAcknowledgementStatus()
    {
        return this.getPHDBAcknowledgementStatus(this.UserSecurityID);
    }

    public void setPHDBAcknowledgementStatus(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PHDBAcknowledgementStatus' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[PHDBAcknowledgementStatus]=["+newValue+"]");
                   dbDB.setPHDBAcknowledgementStatus(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[PHDBAcknowledgementStatus]=["+newValue+"]");
           dbDB.setPHDBAcknowledgementStatus(newValue);
         }
    }
    public Integer getPHDBAcknowledgementStatus(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PHDBAcknowledgementStatus' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPHDBAcknowledgementStatus();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPHDBAcknowledgementStatus();
         }
        return myVal;
    }

    public void setPHDBLetterHeadStatus(Integer newValue)
    {
        this.setPHDBLetterHeadStatus(newValue,this.UserSecurityID);


    }
    public Integer getPHDBLetterHeadStatus()
    {
        return this.getPHDBLetterHeadStatus(this.UserSecurityID);
    }

    public void setPHDBLetterHeadStatus(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PHDBLetterHeadStatus' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[PHDBLetterHeadStatus]=["+newValue+"]");
                   dbDB.setPHDBLetterHeadStatus(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[PHDBLetterHeadStatus]=["+newValue+"]");
           dbDB.setPHDBLetterHeadStatus(newValue);
         }
    }
    public Integer getPHDBLetterHeadStatus(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PHDBLetterHeadStatus' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPHDBLetterHeadStatus();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPHDBLetterHeadStatus();
         }
        return myVal;
    }

    public void setTaxID(String newValue)
    {
        this.setTaxID(newValue,this.UserSecurityID);


    }
    public String getTaxID()
    {
        return this.getTaxID(this.UserSecurityID);
    }

    public void setTaxID(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='TaxID' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[TaxID]=["+newValue+"]");
                   dbDB.setTaxID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[TaxID]=["+newValue+"]");
           dbDB.setTaxID(newValue);
         }
    }
    public String getTaxID(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='TaxID' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getTaxID();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getTaxID();
         }
        return myVal;
    }

    public void setNotUsingPHDBID(Integer newValue)
    {
        this.setNotUsingPHDBID(newValue,this.UserSecurityID);


    }
    public Integer getNotUsingPHDBID()
    {
        return this.getNotUsingPHDBID(this.UserSecurityID);
    }

    public void setNotUsingPHDBID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='NotUsingPHDBID' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[NotUsingPHDBID]=["+newValue+"]");
                   dbDB.setNotUsingPHDBID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[NotUsingPHDBID]=["+newValue+"]");
           dbDB.setNotUsingPHDBID(newValue);
         }
    }
    public Integer getNotUsingPHDBID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='NotUsingPHDBID' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getNotUsingPHDBID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getNotUsingPHDBID();
         }
        return myVal;
    }

    public void setNotUsingPHDBComments(String newValue)
    {
        this.setNotUsingPHDBComments(newValue,this.UserSecurityID);


    }
    public String getNotUsingPHDBComments()
    {
        return this.getNotUsingPHDBComments(this.UserSecurityID);
    }

    public void setNotUsingPHDBComments(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='NotUsingPHDBComments' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[NotUsingPHDBComments]=["+newValue+"]");
                   dbDB.setNotUsingPHDBComments(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[NotUsingPHDBComments]=["+newValue+"]");
           dbDB.setNotUsingPHDBComments(newValue);
         }
    }
    public String getNotUsingPHDBComments(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='NotUsingPHDBComments' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getNotUsingPHDBComments();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getNotUsingPHDBComments();
         }
        return myVal;
    }

    public void setPayerID(Integer newValue)
    {
        this.setPayerID(newValue,this.UserSecurityID);


    }
    public Integer getPayerID()
    {
        return this.getPayerID(this.UserSecurityID);
    }

    public void setPayerID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PayerID' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[PayerID]=["+newValue+"]");
                   dbDB.setPayerID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[PayerID]=["+newValue+"]");
           dbDB.setPayerID(newValue);
         }
    }
    public Integer getPayerID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PayerID' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPayerID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPayerID();
         }
        return myVal;
    }

    public void setManagerID(Integer newValue)
    {
        this.setManagerID(newValue,this.UserSecurityID);


    }
    public Integer getManagerID()
    {
        return this.getManagerID(this.UserSecurityID);
    }

    public void setManagerID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ManagerID' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[ManagerID]=["+newValue+"]");
                   dbDB.setManagerID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[ManagerID]=["+newValue+"]");
           dbDB.setManagerID(newValue);
         }
    }
    public Integer getManagerID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ManagerID' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getManagerID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getManagerID();
         }
        return myVal;
    }

    public void setComm_Email_RequiresAttachement(Integer newValue)
    {
        this.setComm_Email_RequiresAttachement(newValue,this.UserSecurityID);


    }
    public Integer getComm_Email_RequiresAttachement()
    {
        return this.getComm_Email_RequiresAttachement(this.UserSecurityID);
    }

    public void setComm_Email_RequiresAttachement(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Comm_Email_RequiresAttachement' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[Comm_Email_RequiresAttachement]=["+newValue+"]");
                   dbDB.setComm_Email_RequiresAttachement(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[Comm_Email_RequiresAttachement]=["+newValue+"]");
           dbDB.setComm_Email_RequiresAttachement(newValue);
         }
    }
    public Integer getComm_Email_RequiresAttachement(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Comm_Email_RequiresAttachement' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getComm_Email_RequiresAttachement();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getComm_Email_RequiresAttachement();
         }
        return myVal;
    }

    public void setComm_Alerts_LevelUser(Integer newValue)
    {
        this.setComm_Alerts_LevelUser(newValue,this.UserSecurityID);


    }
    public Integer getComm_Alerts_LevelUser()
    {
        return this.getComm_Alerts_LevelUser(this.UserSecurityID);
    }

    public void setComm_Alerts_LevelUser(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Comm_Alerts_LevelUser' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[Comm_Alerts_LevelUser]=["+newValue+"]");
                   dbDB.setComm_Alerts_LevelUser(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[Comm_Alerts_LevelUser]=["+newValue+"]");
           dbDB.setComm_Alerts_LevelUser(newValue);
         }
    }
    public Integer getComm_Alerts_LevelUser(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Comm_Alerts_LevelUser' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getComm_Alerts_LevelUser();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getComm_Alerts_LevelUser();
         }
        return myVal;
    }

    public void setComm_Alerts_LevelManager(Integer newValue)
    {
        this.setComm_Alerts_LevelManager(newValue,this.UserSecurityID);


    }
    public Integer getComm_Alerts_LevelManager()
    {
        return this.getComm_Alerts_LevelManager(this.UserSecurityID);
    }

    public void setComm_Alerts_LevelManager(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Comm_Alerts_LevelManager' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[Comm_Alerts_LevelManager]=["+newValue+"]");
                   dbDB.setComm_Alerts_LevelManager(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[Comm_Alerts_LevelManager]=["+newValue+"]");
           dbDB.setComm_Alerts_LevelManager(newValue);
         }
    }
    public Integer getComm_Alerts_LevelManager(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Comm_Alerts_LevelManager' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getComm_Alerts_LevelManager();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getComm_Alerts_LevelManager();
         }
        return myVal;
    }

    public void setComm_Alerts_LevelBranch(Integer newValue)
    {
        this.setComm_Alerts_LevelBranch(newValue,this.UserSecurityID);


    }
    public Integer getComm_Alerts_LevelBranch()
    {
        return this.getComm_Alerts_LevelBranch(this.UserSecurityID);
    }

    public void setComm_Alerts_LevelBranch(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Comm_Alerts_LevelBranch' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[Comm_Alerts_LevelBranch]=["+newValue+"]");
                   dbDB.setComm_Alerts_LevelBranch(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[Comm_Alerts_LevelBranch]=["+newValue+"]");
           dbDB.setComm_Alerts_LevelBranch(newValue);
         }
    }
    public Integer getComm_Alerts_LevelBranch(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Comm_Alerts_LevelBranch' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getComm_Alerts_LevelBranch();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getComm_Alerts_LevelBranch();
         }
        return myVal;
    }

    public void setComm_Alerts2_LevelUser(Integer newValue)
    {
        this.setComm_Alerts2_LevelUser(newValue,this.UserSecurityID);


    }
    public Integer getComm_Alerts2_LevelUser()
    {
        return this.getComm_Alerts2_LevelUser(this.UserSecurityID);
    }

    public void setComm_Alerts2_LevelUser(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Comm_Alerts2_LevelUser' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[Comm_Alerts2_LevelUser]=["+newValue+"]");
                   dbDB.setComm_Alerts2_LevelUser(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[Comm_Alerts2_LevelUser]=["+newValue+"]");
           dbDB.setComm_Alerts2_LevelUser(newValue);
         }
    }
    public Integer getComm_Alerts2_LevelUser(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Comm_Alerts2_LevelUser' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getComm_Alerts2_LevelUser();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getComm_Alerts2_LevelUser();
         }
        return myVal;
    }

    public void setComm_Alerts2_LevelManager(Integer newValue)
    {
        this.setComm_Alerts2_LevelManager(newValue,this.UserSecurityID);


    }
    public Integer getComm_Alerts2_LevelManager()
    {
        return this.getComm_Alerts2_LevelManager(this.UserSecurityID);
    }

    public void setComm_Alerts2_LevelManager(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Comm_Alerts2_LevelManager' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[Comm_Alerts2_LevelManager]=["+newValue+"]");
                   dbDB.setComm_Alerts2_LevelManager(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[Comm_Alerts2_LevelManager]=["+newValue+"]");
           dbDB.setComm_Alerts2_LevelManager(newValue);
         }
    }
    public Integer getComm_Alerts2_LevelManager(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Comm_Alerts2_LevelManager' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getComm_Alerts2_LevelManager();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getComm_Alerts2_LevelManager();
         }
        return myVal;
    }

    public void setComm_Alerts2_LevelBranch(Integer newValue)
    {
        this.setComm_Alerts2_LevelBranch(newValue,this.UserSecurityID);


    }
    public Integer getComm_Alerts2_LevelBranch()
    {
        return this.getComm_Alerts2_LevelBranch(this.UserSecurityID);
    }

    public void setComm_Alerts2_LevelBranch(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Comm_Alerts2_LevelBranch' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[Comm_Alerts2_LevelBranch]=["+newValue+"]");
                   dbDB.setComm_Alerts2_LevelBranch(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[Comm_Alerts2_LevelBranch]=["+newValue+"]");
           dbDB.setComm_Alerts2_LevelBranch(newValue);
         }
    }
    public Integer getComm_Alerts2_LevelBranch(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Comm_Alerts2_LevelBranch' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getComm_Alerts2_LevelBranch();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getComm_Alerts2_LevelBranch();
         }
        return myVal;
    }

    public void setComm_Report_LevelUser(Integer newValue)
    {
        this.setComm_Report_LevelUser(newValue,this.UserSecurityID);


    }
    public Integer getComm_Report_LevelUser()
    {
        return this.getComm_Report_LevelUser(this.UserSecurityID);
    }

    public void setComm_Report_LevelUser(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Comm_Report_LevelUser' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[Comm_Report_LevelUser]=["+newValue+"]");
                   dbDB.setComm_Report_LevelUser(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[Comm_Report_LevelUser]=["+newValue+"]");
           dbDB.setComm_Report_LevelUser(newValue);
         }
    }
    public Integer getComm_Report_LevelUser(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Comm_Report_LevelUser' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getComm_Report_LevelUser();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getComm_Report_LevelUser();
         }
        return myVal;
    }

    public void setComm_Report_LevelManager(Integer newValue)
    {
        this.setComm_Report_LevelManager(newValue,this.UserSecurityID);


    }
    public Integer getComm_Report_LevelManager()
    {
        return this.getComm_Report_LevelManager(this.UserSecurityID);
    }

    public void setComm_Report_LevelManager(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Comm_Report_LevelManager' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[Comm_Report_LevelManager]=["+newValue+"]");
                   dbDB.setComm_Report_LevelManager(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[Comm_Report_LevelManager]=["+newValue+"]");
           dbDB.setComm_Report_LevelManager(newValue);
         }
    }
    public Integer getComm_Report_LevelManager(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Comm_Report_LevelManager' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getComm_Report_LevelManager();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getComm_Report_LevelManager();
         }
        return myVal;
    }

    public void setComm_Report_LevelBranch(Integer newValue)
    {
        this.setComm_Report_LevelBranch(newValue,this.UserSecurityID);


    }
    public Integer getComm_Report_LevelBranch()
    {
        return this.getComm_Report_LevelBranch(this.UserSecurityID);
    }

    public void setComm_Report_LevelBranch(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Comm_Report_LevelBranch' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[Comm_Report_LevelBranch]=["+newValue+"]");
                   dbDB.setComm_Report_LevelBranch(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[Comm_Report_LevelBranch]=["+newValue+"]");
           dbDB.setComm_Report_LevelBranch(newValue);
         }
    }
    public Integer getComm_Report_LevelBranch(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Comm_Report_LevelBranch' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getComm_Report_LevelBranch();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getComm_Report_LevelBranch();
         }
        return myVal;
    }

    public void setUserNPI(String newValue)
    {
        this.setUserNPI(newValue,this.UserSecurityID);


    }
    public String getUserNPI()
    {
        return this.getUserNPI(this.UserSecurityID);
    }

    public void setUserNPI(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='UserNPI' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[UserNPI]=["+newValue+"]");
                   dbDB.setUserNPI(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[UserNPI]=["+newValue+"]");
           dbDB.setUserNPI(newValue);
         }
    }
    public String getUserNPI(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='UserNPI' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getUserNPI();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getUserNPI();
         }
        return myVal;
    }

    public void setUserStateLicense(String newValue)
    {
        this.setUserStateLicense(newValue,this.UserSecurityID);


    }
    public String getUserStateLicense()
    {
        return this.getUserStateLicense(this.UserSecurityID);
    }

    public void setUserStateLicense(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='UserStateLicense' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[UserStateLicense]=["+newValue+"]");
                   dbDB.setUserStateLicense(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[UserStateLicense]=["+newValue+"]");
           dbDB.setUserStateLicense(newValue);
         }
    }
    public String getUserStateLicense(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='UserStateLicense' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getUserStateLicense();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getUserStateLicense();
         }
        return myVal;
    }

    public void setUserStateLicenseDesc(String newValue)
    {
        this.setUserStateLicenseDesc(newValue,this.UserSecurityID);


    }
    public String getUserStateLicenseDesc()
    {
        return this.getUserStateLicenseDesc(this.UserSecurityID);
    }

    public void setUserStateLicenseDesc(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='UserStateLicenseDesc' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[UserStateLicenseDesc]=["+newValue+"]");
                   dbDB.setUserStateLicenseDesc(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[UserStateLicenseDesc]=["+newValue+"]");
           dbDB.setUserStateLicenseDesc(newValue);
         }
    }
    public String getUserStateLicenseDesc(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='UserStateLicenseDesc' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getUserStateLicenseDesc();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getUserStateLicenseDesc();
         }
        return myVal;
    }

    public void setSelectMRI_ID(Integer newValue)
    {
        this.setSelectMRI_ID(newValue,this.UserSecurityID);


    }
    public Integer getSelectMRI_ID()
    {
        return this.getSelectMRI_ID(this.UserSecurityID);
    }

    public void setSelectMRI_ID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SelectMRI_ID' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[SelectMRI_ID]=["+newValue+"]");
                   dbDB.setSelectMRI_ID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[SelectMRI_ID]=["+newValue+"]");
           dbDB.setSelectMRI_ID(newValue);
         }
    }
    public Integer getSelectMRI_ID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SelectMRI_ID' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getSelectMRI_ID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getSelectMRI_ID();
         }
        return myVal;
    }

    public void setSelectMRI_Notes(String newValue)
    {
        this.setSelectMRI_Notes(newValue,this.UserSecurityID);


    }
    public String getSelectMRI_Notes()
    {
        return this.getSelectMRI_Notes(this.UserSecurityID);
    }

    public void setSelectMRI_Notes(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SelectMRI_Notes' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[SelectMRI_Notes]=["+newValue+"]");
                   dbDB.setSelectMRI_Notes(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[SelectMRI_Notes]=["+newValue+"]");
           dbDB.setSelectMRI_Notes(newValue);
         }
    }
    public String getSelectMRI_Notes(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SelectMRI_Notes' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getSelectMRI_Notes();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getSelectMRI_Notes();
         }
        return myVal;
    }

    public void setMELicense(String newValue)
    {
        this.setMELicense(newValue,this.UserSecurityID);


    }
    public String getMELicense()
    {
        return this.getMELicense(this.UserSecurityID);
    }

    public void setMELicense(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='MELicense' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[MELicense]=["+newValue+"]");
                   dbDB.setMELicense(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[MELicense]=["+newValue+"]");
           dbDB.setMELicense(newValue);
         }
    }
    public String getMELicense(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='MELicense' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getMELicense();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getMELicense();
         }
        return myVal;
    }

    public void setSelectMRI_UserType(String newValue)
    {
        this.setSelectMRI_UserType(newValue,this.UserSecurityID);


    }
    public String getSelectMRI_UserType()
    {
        return this.getSelectMRI_UserType(this.UserSecurityID);
    }

    public void setSelectMRI_UserType(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SelectMRI_UserType' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[SelectMRI_UserType]=["+newValue+"]");
                   dbDB.setSelectMRI_UserType(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[SelectMRI_UserType]=["+newValue+"]");
           dbDB.setSelectMRI_UserType(newValue);
         }
    }
    public String getSelectMRI_UserType(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SelectMRI_UserType' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getSelectMRI_UserType();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getSelectMRI_UserType();
         }
        return myVal;
    }

    public void setImportantNotes_Alert(String newValue)
    {
        this.setImportantNotes_Alert(newValue,this.UserSecurityID);


    }
    public String getImportantNotes_Alert()
    {
        return this.getImportantNotes_Alert(this.UserSecurityID);
    }

    public void setImportantNotes_Alert(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ImportantNotes_Alert' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[ImportantNotes_Alert]=["+newValue+"]");
                   dbDB.setImportantNotes_Alert(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[ImportantNotes_Alert]=["+newValue+"]");
           dbDB.setImportantNotes_Alert(newValue);
         }
    }
    public String getImportantNotes_Alert(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ImportantNotes_Alert' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getImportantNotes_Alert();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getImportantNotes_Alert();
         }
        return myVal;
    }

    public void setEmailAlertNotes_Alert(String newValue)
    {
        this.setEmailAlertNotes_Alert(newValue,this.UserSecurityID);


    }
    public String getEmailAlertNotes_Alert()
    {
        return this.getEmailAlertNotes_Alert(this.UserSecurityID);
    }

    public void setEmailAlertNotes_Alert(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='EmailAlertNotes_Alert' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[EmailAlertNotes_Alert]=["+newValue+"]");
                   dbDB.setEmailAlertNotes_Alert(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[EmailAlertNotes_Alert]=["+newValue+"]");
           dbDB.setEmailAlertNotes_Alert(newValue);
         }
    }
    public String getEmailAlertNotes_Alert(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='EmailAlertNotes_Alert' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getEmailAlertNotes_Alert();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getEmailAlertNotes_Alert();
         }
        return myVal;
    }

    public void setImportantNotes(String newValue)
    {
        this.setImportantNotes(newValue,this.UserSecurityID);


    }
    public String getImportantNotes()
    {
        return this.getImportantNotes(this.UserSecurityID);
    }

    public void setImportantNotes(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ImportantNotes' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[ImportantNotes]=["+newValue+"]");
                   dbDB.setImportantNotes(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[ImportantNotes]=["+newValue+"]");
           dbDB.setImportantNotes(newValue);
         }
    }
    public String getImportantNotes(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ImportantNotes' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getImportantNotes();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getImportantNotes();
         }
        return myVal;
    }

    public void setNIM_UserType(String newValue)
    {
        this.setNIM_UserType(newValue,this.UserSecurityID);


    }
    public String getNIM_UserType()
    {
        return this.getNIM_UserType(this.UserSecurityID);
    }

    public void setNIM_UserType(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='NIM_UserType' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[NIM_UserType]=["+newValue+"]");
                   dbDB.setNIM_UserType(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[NIM_UserType]=["+newValue+"]");
           dbDB.setNIM_UserType(newValue);
         }
    }
    public String getNIM_UserType(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='NIM_UserType' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getNIM_UserType();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getNIM_UserType();
         }
        return myVal;
    }

    public void setTotalMonthlyMRIs(Integer newValue)
    {
        this.setTotalMonthlyMRIs(newValue,this.UserSecurityID);


    }
    public Integer getTotalMonthlyMRIs()
    {
        return this.getTotalMonthlyMRIs(this.UserSecurityID);
    }

    public void setTotalMonthlyMRIs(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='TotalMonthlyMRIs' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[TotalMonthlyMRIs]=["+newValue+"]");
                   dbDB.setTotalMonthlyMRIs(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[TotalMonthlyMRIs]=["+newValue+"]");
           dbDB.setTotalMonthlyMRIs(newValue);
         }
    }
    public Integer getTotalMonthlyMRIs(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='TotalMonthlyMRIs' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getTotalMonthlyMRIs();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getTotalMonthlyMRIs();
         }
        return myVal;
    }

    public void setExpectedMonthlyMRIs(Integer newValue)
    {
        this.setExpectedMonthlyMRIs(newValue,this.UserSecurityID);


    }
    public Integer getExpectedMonthlyMRIs()
    {
        return this.getExpectedMonthlyMRIs(this.UserSecurityID);
    }

    public void setExpectedMonthlyMRIs(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ExpectedMonthlyMRIs' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[ExpectedMonthlyMRIs]=["+newValue+"]");
                   dbDB.setExpectedMonthlyMRIs(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[ExpectedMonthlyMRIs]=["+newValue+"]");
           dbDB.setExpectedMonthlyMRIs(newValue);
         }
    }
    public Integer getExpectedMonthlyMRIs(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ExpectedMonthlyMRIs' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getExpectedMonthlyMRIs();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getExpectedMonthlyMRIs();
         }
        return myVal;
    }

    public void setData_Rank(Integer newValue)
    {
        this.setData_Rank(newValue,this.UserSecurityID);


    }
    public Integer getData_Rank()
    {
        return this.getData_Rank(this.UserSecurityID);
    }

    public void setData_Rank(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Data_Rank' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[Data_Rank]=["+newValue+"]");
                   dbDB.setData_Rank(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[Data_Rank]=["+newValue+"]");
           dbDB.setData_Rank(newValue);
         }
    }
    public Integer getData_Rank(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Data_Rank' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getData_Rank();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getData_Rank();
         }
        return myVal;
    }

    public void setData_3MonthAverage(Integer newValue)
    {
        this.setData_3MonthAverage(newValue,this.UserSecurityID);


    }
    public Integer getData_3MonthAverage()
    {
        return this.getData_3MonthAverage(this.UserSecurityID);
    }

    public void setData_3MonthAverage(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Data_3MonthAverage' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[Data_3MonthAverage]=["+newValue+"]");
                   dbDB.setData_3MonthAverage(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[Data_3MonthAverage]=["+newValue+"]");
           dbDB.setData_3MonthAverage(newValue);
         }
    }
    public Integer getData_3MonthAverage(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Data_3MonthAverage' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getData_3MonthAverage();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getData_3MonthAverage();
         }
        return myVal;
    }

    public void setData_Referrals_AllTime(Integer newValue)
    {
        this.setData_Referrals_AllTime(newValue,this.UserSecurityID);


    }
    public Integer getData_Referrals_AllTime()
    {
        return this.getData_Referrals_AllTime(this.UserSecurityID);
    }

    public void setData_Referrals_AllTime(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Data_Referrals_AllTime' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[Data_Referrals_AllTime]=["+newValue+"]");
                   dbDB.setData_Referrals_AllTime(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[Data_Referrals_AllTime]=["+newValue+"]");
           dbDB.setData_Referrals_AllTime(newValue);
         }
    }
    public Integer getData_Referrals_AllTime(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Data_Referrals_AllTime' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getData_Referrals_AllTime();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getData_Referrals_AllTime();
         }
        return myVal;
    }

    public void setData_Referrals_AnalysisTotal(Integer newValue)
    {
        this.setData_Referrals_AnalysisTotal(newValue,this.UserSecurityID);


    }
    public Integer getData_Referrals_AnalysisTotal()
    {
        return this.getData_Referrals_AnalysisTotal(this.UserSecurityID);
    }

    public void setData_Referrals_AnalysisTotal(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Data_Referrals_AnalysisTotal' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[Data_Referrals_AnalysisTotal]=["+newValue+"]");
                   dbDB.setData_Referrals_AnalysisTotal(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[Data_Referrals_AnalysisTotal]=["+newValue+"]");
           dbDB.setData_Referrals_AnalysisTotal(newValue);
         }
    }
    public Integer getData_Referrals_AnalysisTotal(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Data_Referrals_AnalysisTotal' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getData_Referrals_AnalysisTotal();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getData_Referrals_AnalysisTotal();
         }
        return myVal;
    }

    public void setData_Referrals_AnalysisTurnAroundAverage_ToSched(Double newValue)
    {
        this.setData_Referrals_AnalysisTurnAroundAverage_ToSched(newValue,this.UserSecurityID);


    }
    public Double getData_Referrals_AnalysisTurnAroundAverage_ToSched()
    {
        return this.getData_Referrals_AnalysisTurnAroundAverage_ToSched(this.UserSecurityID);
    }

    public void setData_Referrals_AnalysisTurnAroundAverage_ToSched(Double newValue, Integer GroupRefID)
    {
        Double myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Data_Referrals_AnalysisTurnAroundAverage_ToSched' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[Data_Referrals_AnalysisTurnAroundAverage_ToSched]=["+newValue+"]");
                   dbDB.setData_Referrals_AnalysisTurnAroundAverage_ToSched(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[Data_Referrals_AnalysisTurnAroundAverage_ToSched]=["+newValue+"]");
           dbDB.setData_Referrals_AnalysisTurnAroundAverage_ToSched(newValue);
         }
    }
    public Double getData_Referrals_AnalysisTurnAroundAverage_ToSched(Integer GroupRefID)
    {
        Double myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Data_Referrals_AnalysisTurnAroundAverage_ToSched' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getData_Referrals_AnalysisTurnAroundAverage_ToSched();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getData_Referrals_AnalysisTurnAroundAverage_ToSched();
         }
        return myVal;
    }

    public void setData_Referrals_AnalysisCostSavings(Double newValue)
    {
        this.setData_Referrals_AnalysisCostSavings(newValue,this.UserSecurityID);


    }
    public Double getData_Referrals_AnalysisCostSavings()
    {
        return this.getData_Referrals_AnalysisCostSavings(this.UserSecurityID);
    }

    public void setData_Referrals_AnalysisCostSavings(Double newValue, Integer GroupRefID)
    {
        Double myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Data_Referrals_AnalysisCostSavings' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[Data_Referrals_AnalysisCostSavings]=["+newValue+"]");
                   dbDB.setData_Referrals_AnalysisCostSavings(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[Data_Referrals_AnalysisCostSavings]=["+newValue+"]");
           dbDB.setData_Referrals_AnalysisCostSavings(newValue);
         }
    }
    public Double getData_Referrals_AnalysisCostSavings(Integer GroupRefID)
    {
        Double myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Data_Referrals_AnalysisCostSavings' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getData_Referrals_AnalysisCostSavings();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getData_Referrals_AnalysisCostSavings();
         }
        return myVal;
    }

    public void setInternal_PrimaryContact(Integer newValue)
    {
        this.setInternal_PrimaryContact(newValue,this.UserSecurityID);


    }
    public Integer getInternal_PrimaryContact()
    {
        return this.getInternal_PrimaryContact(this.UserSecurityID);
    }

    public void setInternal_PrimaryContact(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Internal_PrimaryContact' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[Internal_PrimaryContact]=["+newValue+"]");
                   dbDB.setInternal_PrimaryContact(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[Internal_PrimaryContact]=["+newValue+"]");
           dbDB.setInternal_PrimaryContact(newValue);
         }
    }
    public Integer getInternal_PrimaryContact(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Internal_PrimaryContact' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getInternal_PrimaryContact();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getInternal_PrimaryContact();
         }
        return myVal;
    }

    public void setLastContacted(Date newValue)
    {
        this.setLastContacted(newValue,this.UserSecurityID);


    }
    public Date getLastContacted()
    {
        return this.getLastContacted(this.UserSecurityID);
    }

    public void setLastContacted(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='LastContacted' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[LastContacted]=["+newValue+"]");
                   dbDB.setLastContacted(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[LastContacted]=["+newValue+"]");
           dbDB.setLastContacted(newValue);
         }
    }
    public Date getLastContacted(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='LastContacted' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getLastContacted();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getLastContacted();
         }
        return myVal;
    }

    public void setDateOfBirth(Date newValue)
    {
        this.setDateOfBirth(newValue,this.UserSecurityID);


    }
    public Date getDateOfBirth()
    {
        return this.getDateOfBirth(this.UserSecurityID);
    }

    public void setDateOfBirth(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='DateOfBirth' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[DateOfBirth]=["+newValue+"]");
                   dbDB.setDateOfBirth(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[DateOfBirth]=["+newValue+"]");
           dbDB.setDateOfBirth(newValue);
         }
    }
    public Date getDateOfBirth(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='DateOfBirth' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getDateOfBirth();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getDateOfBirth();
         }
        return myVal;
    }

    public void setPersonalInsights(String newValue)
    {
        this.setPersonalInsights(newValue,this.UserSecurityID);


    }
    public String getPersonalInsights()
    {
        return this.getPersonalInsights(this.UserSecurityID);
    }

    public void setPersonalInsights(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PersonalInsights' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[PersonalInsights]=["+newValue+"]");
                   dbDB.setPersonalInsights(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[PersonalInsights]=["+newValue+"]");
           dbDB.setPersonalInsights(newValue);
         }
    }
    public String getPersonalInsights(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PersonalInsights' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPersonalInsights();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPersonalInsights();
         }
        return myVal;
    }

    public void setAllowAsPrimaryContact(Integer newValue)
    {
        this.setAllowAsPrimaryContact(newValue,this.UserSecurityID);


    }
    public Integer getAllowAsPrimaryContact()
    {
        return this.getAllowAsPrimaryContact(this.UserSecurityID);
    }

    public void setAllowAsPrimaryContact(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AllowAsPrimaryContact' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[AllowAsPrimaryContact]=["+newValue+"]");
                   dbDB.setAllowAsPrimaryContact(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[AllowAsPrimaryContact]=["+newValue+"]");
           dbDB.setAllowAsPrimaryContact(newValue);
         }
    }
    public Integer getAllowAsPrimaryContact(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AllowAsPrimaryContact' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getAllowAsPrimaryContact();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getAllowAsPrimaryContact();
         }
        return myVal;
    }

    public void setIsAdmin(Integer newValue)
    {
        this.setIsAdmin(newValue,this.UserSecurityID);


    }
    public Integer getIsAdmin()
    {
        return this.getIsAdmin(this.UserSecurityID);
    }

    public void setIsAdmin(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='IsAdmin' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[IsAdmin]=["+newValue+"]");
                   dbDB.setIsAdmin(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[IsAdmin]=["+newValue+"]");
           dbDB.setIsAdmin(newValue);
         }
    }
    public Integer getIsAdmin(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='IsAdmin' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getIsAdmin();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getIsAdmin();
         }
        return myVal;
    }

    public void setRequiredFieldList(java.util.Hashtable requiredFieldList)
	 {

        this.requiredFieldList = requiredFieldList;
    }

    public java.util.Hashtable getRequiredFieldList()
    {
        return this.requiredFieldList;
    }
    public void setExpiredFieldList(java.util.Hashtable expiredFieldList)
    {
        this.expiredFieldList = expiredFieldList;
    }

    public void setEnglishFieldList(java.util.Hashtable englishFieldList)
    {
        this.englishFieldList = englishFieldList;
    }

    public java.util.Hashtable getExpiredFieldList()
    {
        return this.expiredFieldList;
    }
    public boolean isExpired(java.util.Date myDate, int daysVector)
    {
       return isExpired(myDate,new java.util.Date(),daysVector);
    }


    public boolean isRead(String fieldN, Integer GroupRefID)
    {
        boolean myVal = false;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='"+fieldN+"' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==1)
               {
                   myVal = true;
               }
            }
        }
        else
        {

           if (iSecurityCheck.intValue()>=1)
           {
                  myVal = true;
           }
        }
        return myVal;
    }


    public boolean isWrite(String fieldN, Integer GroupRefID)
    {
        boolean myVal = false;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='"+fieldN+"' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {
                   myVal = true;
               }
            }
        }
        else
        {

           if (iSecurityCheck.intValue()>=2)
           {
                  myVal = true;
           }
        }
        return myVal;
    }


    public boolean isExpired(java.util.Date myDate, java.util.Date refDate,int daysVector)
    {
        java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat("yyyy-MM-dd");
        boolean myVal = true;
        try
        {
            if (myDate.after(dbdf.parse("1800-01-10")))
            {
                if (myDate.after(dateChange(refDate,daysVector)))
                {
                    myVal=false;
                }
            }
            else
            {
                myVal = false;
            }
        }
        catch (Exception Datee)
        {
        }
        return myVal;
    }

    public java.util.Date dateChange(java.util.Date myD, int dayChange)
    {
        java.util.Calendar calendar = java.util.Calendar.getInstance();
        calendar.setTime(myD);
        calendar.add(java.util.Calendar.DATE, dayChange);
        java.util.Date resultAsDate = new java.util.Date(calendar.getTime().getTime());
        return resultAsDate;
    }

    public boolean isRequired(String fieldN)
    {
        return isRequired(fieldN, new Integer("1"));
    }

    public boolean isRequired(String fieldN, Integer GroupRefID)
    {
        boolean myVal = false;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='"+fieldN+"' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getRequiredID().intValue()==1)
               {
                   myVal = true;
               }
            }
        }
        else
        {
            if (this.getRequiredFieldList().containsKey(fieldN))
            {
                myVal = true;
            }
        }
        return myVal;
    }
    public boolean isExpiredCheck(String fieldN)
    {
        return isExpiredCheck(fieldN, new Integer ("1"));
    }

    public boolean isExpiredCheck(String fieldN, Integer GroupRefID)
    {
        boolean myVal = false;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='"+fieldN+"' AND TableName='tUserAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getTrackedID().intValue()==1)
               {
                   myVal = true;
               }
            }
        }
        else
        {
            if (this.getExpiredFieldList().containsKey(fieldN))
           {
                myVal = true;
            }
        }
        return myVal;
    }

    public void makeRequiredHash()
    {
       makeRequiredHash(new Integer(1));
    }

    public void makeRequiredHash(Integer GroupRefID)
    {

        java.util.Hashtable newHash = new java.util.Hashtable();
        if (runFieldSecurity)
        {
            java.util.Enumeration myKeys = englishFieldList.keys(); 
            while (myKeys.hasMoreElements()) 
            {
                 String myElement = (String) myKeys.nextElement();
                 bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='"+myElement+"' AND TableName='tUserAccount'" ,"");
                 bltFieldSecurity        working_bltFieldSecurity;
                 ListElement         leCurrentElement;
                 Enumeration eList = myFSL.elements();
                 if (eList.hasMoreElements())
                 {
                     leCurrentElement    = (ListElement) eList.nextElement();
                     working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
                     if (working_bltFieldSecurity.getRequiredID().intValue()==1)
                     {
                         newHash.put(myElement,new Boolean(true));
                     }
                 }
            }
        }
        else
        {
            newHash.put("PLCID",new Boolean(true));
            newHash.put("StartPage",new Boolean(true));
            newHash.put("AccountType",new Boolean(true));
            newHash.put("SecurityGroupID",new Boolean(true));
            newHash.put("GenericSecurityGroupID",new Boolean(true));
            newHash.put("ReferenceID",new Boolean(true));
            newHash.put("AccessType",new Boolean(true));
            newHash.put("Status",new Boolean(true));
            newHash.put("LogonUserName",new Boolean(true));
            newHash.put("LogonUserPassword",new Boolean(true));
            newHash.put("BillingID",new Boolean(true));
        }
        this.setRequiredFieldList(newHash);
    }

    public void makeEnglishHash()
    {

        java.util.Hashtable newHash = new java.util.Hashtable();        newHash.put("UniqueCreateDate","Item Create Date");
        newHash.put("UniqueModifyDate","Item Modify Date");
        newHash.put("UniqueModifyComments","Item Modification Comments");
        newHash.put("PLCID","Hidden");
        newHash.put("StartPage","StartPage");
        newHash.put("AccountType","AccountType");
        newHash.put("SecurityGroupID","SecurityGroupID");
        newHash.put("GenericSecurityGroupID","GenericSecurityGroupID");
        newHash.put("ReferenceID","ReferenceID");
        newHash.put("AccessType","AccessType");
        newHash.put("Status","Status");
        newHash.put("LogonUserName","LogonUserName");
        newHash.put("LogonUserPassword","LogonUserPassword");
        newHash.put("AttestKeyword1","AttestKeyword1");
        newHash.put("AttestKeyword2","AttestKeyword2");
        newHash.put("AttestKeywordTemp1","AttestKeywordTemp1");
        newHash.put("AttestKeywordTemp2","AttestKeywordTemp2");
        newHash.put("ContactFirstName","Contact First Name");
        newHash.put("ContactLastName","Contact Last Name");
        newHash.put("ContactEmail","Contact E-mail");
        newHash.put("ContactEmail2","Contact E-mail 2");
        newHash.put("ContactAddress1","Contact Address");
        newHash.put("ContactAddress2","Contact Address 2");
        newHash.put("ContactCity","Contact City");
        newHash.put("ContactStateID","Contact State");
        newHash.put("ContactProvince","Contact Province, District, State");
        newHash.put("ContactZIP","Contact ZIP");
        newHash.put("ContactCountryID","Contact Country");
        newHash.put("ContactPhone","Contact Phone (XXX-XXX-XXXX)");
        newHash.put("ContactFax","Fax (XXX-XXX-XXXX)");
        newHash.put("ContactMobile","Mobile (XXX-XXX-XXXX)");
        newHash.put("SecretQuestion","Secret Question");
        newHash.put("SecretAnswer","Secret Answer");
        newHash.put("CreditCardFullName","CreditCardFullName");
        newHash.put("CreditCardNumber","CreditCardNumber");
        newHash.put("CreditCardType","CreditCardType");
        newHash.put("CreditCardExpMonth","CreditCardExpMonth");
        newHash.put("CreditCardExpYear","CreditCardExpYear");
        newHash.put("CreditCardChargeDate","CreditCardChargeDate");
        newHash.put("CreditCardPostDate","CreditCardPostDate");
        newHash.put("AlertEmail","Alert Email");
        newHash.put("AlertDays","Alert Days");
        newHash.put("BillingComments","BillingComments");
        newHash.put("PromoCode","PromoCode");
        newHash.put("CompanyType","Company Type");
        newHash.put("CompanyName","Company Name");
        newHash.put("CompanyAddress1","Company Address");
        newHash.put("CompanyAddress2","Company Address ");
        newHash.put("CompanyCity","Company City");
        newHash.put("CompanyStateID","Company State");
        newHash.put("CompanyZIP","Company ZIP");
        newHash.put("CompanyPhone","Company Phone");
        newHash.put("CompanyFax","Company Fax");
        newHash.put("Comments","Comments");
        newHash.put("BillingID","BillingID");
        newHash.put("PHDBAcknowledgementStatus","PHDBAcknowledgementStatus");
        newHash.put("PHDBLetterHeadStatus","PHDBLetterHeadStatus");
        newHash.put("TaxID","TaxID");
        newHash.put("NotUsingPHDBID","NotUsingPHDBID");
        newHash.put("NotUsingPHDBComments","NotUsingPHDBComments");
        newHash.put("PayerID","PayerID");
        newHash.put("ManagerID","ManagerID");
        newHash.put("Comm_Email_RequiresAttachement","Comm_Email_RequiresAttachement");
        newHash.put("Comm_Alerts_LevelUser","Comm_Alerts_LevelUser");
        newHash.put("Comm_Alerts_LevelManager","Comm_Alerts_LevelManager");
        newHash.put("Comm_Alerts_LevelBranch","Comm_Alerts_LevelBranch");
        newHash.put("Comm_Alerts2_LevelUser","Comm_Alerts2_LevelUser");
        newHash.put("Comm_Alerts2_LevelManager","Comm_Alerts2_LevelManager");
        newHash.put("Comm_Alerts2_LevelBranch","Comm_Alerts2_LevelBranch");
        newHash.put("Comm_Report_LevelUser","Comm_Report_LevelUser");
        newHash.put("Comm_Report_LevelManager","Comm_Report_LevelManager");
        newHash.put("Comm_Report_LevelBranch","Comm_Report_LevelBranch");
        newHash.put("UserNPI","UserNPI");
        newHash.put("UserStateLicense","UserStateLicense");
        newHash.put("UserStateLicenseDesc","UserStateLicenseDesc");
        newHash.put("SelectMRI_ID","SelectMRI_ID");
        newHash.put("SelectMRI_Notes","SelectMRI_Notes");
        newHash.put("MELicense","MELicense");
        newHash.put("SelectMRI_UserType","SelectMRI_UserType");
        newHash.put("ImportantNotes_Alert","ImportantNotes_Alert");
        newHash.put("EmailAlertNotes_Alert","EmailAlertNotes_Alert");
        newHash.put("ImportantNotes","ImportantNotes");
        newHash.put("NIM_UserType","NIM_UserType");
        newHash.put("TotalMonthlyMRIs","TotalMonthlyMRIs");
        newHash.put("ExpectedMonthlyMRIs","ExpectedMonthlyMRIs");
        newHash.put("Data_Rank","Data_Rank");
        newHash.put("Data_3MonthAverage","Data_3MonthAverage");
        newHash.put("Data_Referrals_AllTime","Data_Referrals_AllTime");
        newHash.put("Data_Referrals_AnalysisTotal","Data_Referrals_AnalysisTotal");
        newHash.put("Data_Referrals_AnalysisTurnAroundAverage_ToSched","Data_Referrals_AnalysisTurnAroundAverage_ToSched");
        newHash.put("Data_Referrals_AnalysisCostSavings","Data_Referrals_AnalysisCostSavings");
        newHash.put("Internal_PrimaryContact","Internal Primary Contact");
        newHash.put("LastContacted","Last Contacted");
        newHash.put("DateOfBirth","Date Of Birth");
        newHash.put("PersonalInsights","Personal Insights");
        newHash.put("AllowAsPrimaryContact","Allow As Primary Contact");
        newHash.put("IsAdmin","Is Admin");

        this.setEnglishFieldList(newHash);
    }
    public void makeExpiredHash()
    {

        java.util.Hashtable newHash = new java.util.Hashtable();
        this.setExpiredFieldList(newHash);
    }

    public java.util.Hashtable englishFieldList = new java.util.Hashtable();
    public java.util.Hashtable requiredFieldList = new java.util.Hashtable();
    public java.util.Hashtable expiredFieldList = new java.util.Hashtable();

    public boolean isComplete()
    {
	     boolean myVal = true;
	     java.util.Enumeration myKeys = this.getRequiredFieldList().keys(); 
	     while (myKeys.hasMoreElements()) 
	     {
	         String myElement = (String) myKeys.nextElement();
	         if (!isComplete(myElement))
	         {
	             myVal = false;
	         }
	     }
	     return myVal;
    }

    public String getEnglish(String fieldN)
    {
        String myVal = null;
        if (this.englishFieldList.containsKey(fieldN))
        {
            myVal = (String)this.englishFieldList.get(fieldN);
        }
        return myVal;
    }
    public java.util.Vector isComplete(int daysVector)
    {
	     boolean myVal = false;
	     java.util.Enumeration myKeys = this.getExpiredFieldList().keys(); 
	     java.util.Vector myExpiredList = new java.util.Vector(); 
	     while (myKeys.hasMoreElements()) 
	     {
	         String myElement = (String) myKeys.nextElement();
	         if (isExpired(myElement,daysVector))
	         {
	             myVal = true;
	             myExpiredList.addElement(myElement);
	         }
	     }
	     return myExpiredList;
    }

    public void setField(String fieldN, Object fieldV)
    {
     boolean myVal = false;
     try
     {
        java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat("yyyy-MM-dd");

        if (fieldN.equals("UserID"))
        {
             this.setUserID((Integer)fieldV);
        }

        else if (fieldN.equals("UniqueCreateDate"))
        {
            this.setUniqueCreateDate((java.util.Date)fieldV);
        }

        else if (fieldN.equals("UniqueModifyDate"))
        {
            this.setUniqueModifyDate((java.util.Date)fieldV);
        }

        else if (fieldN.equals("UniqueModifyComments"))
        {
            this.setUniqueModifyComments((String)fieldV);
        }

        else if (fieldN.equals("PLCID"))
        {
             this.setPLCID((Integer)fieldV);
        }

        else if (fieldN.equals("StartPage"))
        {
            this.setStartPage((String)fieldV);
        }

        else if (fieldN.equals("AccountType"))
        {
            this.setAccountType((String)fieldV);
        }

        else if (fieldN.equals("SecurityGroupID"))
        {
             this.setSecurityGroupID((Integer)fieldV);
        }

        else if (fieldN.equals("GenericSecurityGroupID"))
        {
             this.setGenericSecurityGroupID((Integer)fieldV);
        }

        else if (fieldN.equals("ReferenceID"))
        {
             this.setReferenceID((Integer)fieldV);
        }

        else if (fieldN.equals("AccessType"))
        {
             this.setAccessType((Integer)fieldV);
        }

        else if (fieldN.equals("Status"))
        {
             this.setStatus((Integer)fieldV);
        }

        else if (fieldN.equals("LogonUserName"))
        {
            this.setLogonUserName((String)fieldV);
        }

        else if (fieldN.equals("LogonUserPassword"))
        {
            this.setLogonUserPassword((String)fieldV);
        }

        else if (fieldN.equals("AttestKeyword1"))
        {
            this.setAttestKeyword1((String)fieldV);
        }

        else if (fieldN.equals("AttestKeyword2"))
        {
            this.setAttestKeyword2((String)fieldV);
        }

        else if (fieldN.equals("AttestKeywordTemp1"))
        {
            this.setAttestKeywordTemp1((String)fieldV);
        }

        else if (fieldN.equals("AttestKeywordTemp2"))
        {
            this.setAttestKeywordTemp2((String)fieldV);
        }

        else if (fieldN.equals("ContactFirstName"))
        {
            this.setContactFirstName((String)fieldV);
        }

        else if (fieldN.equals("ContactLastName"))
        {
            this.setContactLastName((String)fieldV);
        }

        else if (fieldN.equals("ContactEmail"))
        {
            this.setContactEmail((String)fieldV);
        }

        else if (fieldN.equals("ContactEmail2"))
        {
            this.setContactEmail2((String)fieldV);
        }

        else if (fieldN.equals("ContactAddress1"))
        {
            this.setContactAddress1((String)fieldV);
        }

        else if (fieldN.equals("ContactAddress2"))
        {
            this.setContactAddress2((String)fieldV);
        }

        else if (fieldN.equals("ContactCity"))
        {
            this.setContactCity((String)fieldV);
        }

        else if (fieldN.equals("ContactStateID"))
        {
             this.setContactStateID((Integer)fieldV);
        }

        else if (fieldN.equals("ContactProvince"))
        {
            this.setContactProvince((String)fieldV);
        }

        else if (fieldN.equals("ContactZIP"))
        {
            this.setContactZIP((String)fieldV);
        }

        else if (fieldN.equals("ContactCountryID"))
        {
             this.setContactCountryID((Integer)fieldV);
        }

        else if (fieldN.equals("ContactPhone"))
        {
            this.setContactPhone((String)fieldV);
        }

        else if (fieldN.equals("ContactFax"))
        {
            this.setContactFax((String)fieldV);
        }

        else if (fieldN.equals("ContactMobile"))
        {
            this.setContactMobile((String)fieldV);
        }

        else if (fieldN.equals("SecretQuestion"))
        {
            this.setSecretQuestion((String)fieldV);
        }

        else if (fieldN.equals("SecretAnswer"))
        {
            this.setSecretAnswer((String)fieldV);
        }

        else if (fieldN.equals("CreditCardFullName"))
        {
            this.setCreditCardFullName((String)fieldV);
        }

        else if (fieldN.equals("CreditCardNumber"))
        {
            this.setCreditCardNumber((String)fieldV);
        }

        else if (fieldN.equals("CreditCardType"))
        {
            this.setCreditCardType((String)fieldV);
        }

        else if (fieldN.equals("CreditCardExpMonth"))
        {
            this.setCreditCardExpMonth((String)fieldV);
        }

        else if (fieldN.equals("CreditCardExpYear"))
        {
            this.setCreditCardExpYear((String)fieldV);
        }

        else if (fieldN.equals("CreditCardChargeDate"))
        {
            this.setCreditCardChargeDate((java.util.Date)fieldV);
        }

        else if (fieldN.equals("CreditCardPostDate"))
        {
            this.setCreditCardPostDate((java.util.Date)fieldV);
        }

        else if (fieldN.equals("AlertEmail"))
        {
            this.setAlertEmail((String)fieldV);
        }

        else if (fieldN.equals("AlertDays"))
        {
             this.setAlertDays((Integer)fieldV);
        }

        else if (fieldN.equals("BillingComments"))
        {
            this.setBillingComments((String)fieldV);
        }

        else if (fieldN.equals("PromoCode"))
        {
            this.setPromoCode((String)fieldV);
        }

        else if (fieldN.equals("CompanyType"))
        {
             this.setCompanyType((Integer)fieldV);
        }

        else if (fieldN.equals("CompanyName"))
        {
            this.setCompanyName((String)fieldV);
        }

        else if (fieldN.equals("CompanyAddress1"))
        {
            this.setCompanyAddress1((String)fieldV);
        }

        else if (fieldN.equals("CompanyAddress2"))
        {
            this.setCompanyAddress2((String)fieldV);
        }

        else if (fieldN.equals("CompanyCity"))
        {
            this.setCompanyCity((String)fieldV);
        }

        else if (fieldN.equals("CompanyStateID"))
        {
             this.setCompanyStateID((Integer)fieldV);
        }

        else if (fieldN.equals("CompanyZIP"))
        {
            this.setCompanyZIP((String)fieldV);
        }

        else if (fieldN.equals("CompanyPhone"))
        {
            this.setCompanyPhone((String)fieldV);
        }

        else if (fieldN.equals("CompanyFax"))
        {
            this.setCompanyFax((String)fieldV);
        }

        else if (fieldN.equals("Comments"))
        {
            this.setComments((String)fieldV);
        }

        else if (fieldN.equals("BillingID"))
        {
             this.setBillingID((Integer)fieldV);
        }

        else if (fieldN.equals("PHDBAcknowledgementStatus"))
        {
             this.setPHDBAcknowledgementStatus((Integer)fieldV);
        }

        else if (fieldN.equals("PHDBLetterHeadStatus"))
        {
             this.setPHDBLetterHeadStatus((Integer)fieldV);
        }

        else if (fieldN.equals("TaxID"))
        {
            this.setTaxID((String)fieldV);
        }

        else if (fieldN.equals("NotUsingPHDBID"))
        {
             this.setNotUsingPHDBID((Integer)fieldV);
        }

        else if (fieldN.equals("NotUsingPHDBComments"))
        {
            this.setNotUsingPHDBComments((String)fieldV);
        }

        else if (fieldN.equals("PayerID"))
        {
             this.setPayerID((Integer)fieldV);
        }

        else if (fieldN.equals("ManagerID"))
        {
             this.setManagerID((Integer)fieldV);
        }

        else if (fieldN.equals("Comm_Email_RequiresAttachement"))
        {
             this.setComm_Email_RequiresAttachement((Integer)fieldV);
        }

        else if (fieldN.equals("Comm_Alerts_LevelUser"))
        {
             this.setComm_Alerts_LevelUser((Integer)fieldV);
        }

        else if (fieldN.equals("Comm_Alerts_LevelManager"))
        {
             this.setComm_Alerts_LevelManager((Integer)fieldV);
        }

        else if (fieldN.equals("Comm_Alerts_LevelBranch"))
        {
             this.setComm_Alerts_LevelBranch((Integer)fieldV);
        }

        else if (fieldN.equals("Comm_Alerts2_LevelUser"))
        {
             this.setComm_Alerts2_LevelUser((Integer)fieldV);
        }

        else if (fieldN.equals("Comm_Alerts2_LevelManager"))
        {
             this.setComm_Alerts2_LevelManager((Integer)fieldV);
        }

        else if (fieldN.equals("Comm_Alerts2_LevelBranch"))
        {
             this.setComm_Alerts2_LevelBranch((Integer)fieldV);
        }

        else if (fieldN.equals("Comm_Report_LevelUser"))
        {
             this.setComm_Report_LevelUser((Integer)fieldV);
        }

        else if (fieldN.equals("Comm_Report_LevelManager"))
        {
             this.setComm_Report_LevelManager((Integer)fieldV);
        }

        else if (fieldN.equals("Comm_Report_LevelBranch"))
        {
             this.setComm_Report_LevelBranch((Integer)fieldV);
        }

        else if (fieldN.equals("UserNPI"))
        {
            this.setUserNPI((String)fieldV);
        }

        else if (fieldN.equals("UserStateLicense"))
        {
            this.setUserStateLicense((String)fieldV);
        }

        else if (fieldN.equals("UserStateLicenseDesc"))
        {
            this.setUserStateLicenseDesc((String)fieldV);
        }

        else if (fieldN.equals("SelectMRI_ID"))
        {
             this.setSelectMRI_ID((Integer)fieldV);
        }

        else if (fieldN.equals("SelectMRI_Notes"))
        {
            this.setSelectMRI_Notes((String)fieldV);
        }

        else if (fieldN.equals("MELicense"))
        {
            this.setMELicense((String)fieldV);
        }

        else if (fieldN.equals("SelectMRI_UserType"))
        {
            this.setSelectMRI_UserType((String)fieldV);
        }

        else if (fieldN.equals("ImportantNotes_Alert"))
        {
            this.setImportantNotes_Alert((String)fieldV);
        }

        else if (fieldN.equals("EmailAlertNotes_Alert"))
        {
            this.setEmailAlertNotes_Alert((String)fieldV);
        }

        else if (fieldN.equals("ImportantNotes"))
        {
            this.setImportantNotes((String)fieldV);
        }

        else if (fieldN.equals("NIM_UserType"))
        {
            this.setNIM_UserType((String)fieldV);
        }

        else if (fieldN.equals("TotalMonthlyMRIs"))
        {
             this.setTotalMonthlyMRIs((Integer)fieldV);
        }

        else if (fieldN.equals("ExpectedMonthlyMRIs"))
        {
             this.setExpectedMonthlyMRIs((Integer)fieldV);
        }

        else if (fieldN.equals("Data_Rank"))
        {
             this.setData_Rank((Integer)fieldV);
        }

        else if (fieldN.equals("Data_3MonthAverage"))
        {
             this.setData_3MonthAverage((Integer)fieldV);
        }

        else if (fieldN.equals("Data_Referrals_AllTime"))
        {
             this.setData_Referrals_AllTime((Integer)fieldV);
        }

        else if (fieldN.equals("Data_Referrals_AnalysisTotal"))
        {
             this.setData_Referrals_AnalysisTotal((Integer)fieldV);
        }

        else if (fieldN.equals("Data_Referrals_AnalysisTurnAroundAverage_ToSched"))
        {
            this.setData_Referrals_AnalysisTurnAroundAverage_ToSched((Double)fieldV);
        }

        else if (fieldN.equals("Data_Referrals_AnalysisCostSavings"))
        {
            this.setData_Referrals_AnalysisCostSavings((Double)fieldV);
        }

        else if (fieldN.equals("Internal_PrimaryContact"))
        {
             this.setInternal_PrimaryContact((Integer)fieldV);
        }

        else if (fieldN.equals("LastContacted"))
        {
            this.setLastContacted((java.util.Date)fieldV);
        }

        else if (fieldN.equals("DateOfBirth"))
        {
            this.setDateOfBirth((java.util.Date)fieldV);
        }

        else if (fieldN.equals("PersonalInsights"))
        {
            this.setPersonalInsights((String)fieldV);
        }

        else if (fieldN.equals("AllowAsPrimaryContact"))
        {
             this.setAllowAsPrimaryContact((Integer)fieldV);
        }

        else if (fieldN.equals("IsAdmin"))
        {
             this.setIsAdmin((Integer)fieldV);
        }

	    }
	    catch(Exception e2)
	    {}
	    //return myVal;
	}

    public String getFieldType(String fieldN)
    {
     String  myVal = null;
     try
     {
        java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat("yyyy-MM-dd");

        if (fieldN.equals("UserID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("UniqueCreateDate"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("UniqueModifyDate"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("UniqueModifyComments"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("PLCID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("StartPage"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("AccountType"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("SecurityGroupID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("GenericSecurityGroupID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("ReferenceID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("AccessType"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("Status"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("LogonUserName"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("LogonUserPassword"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("AttestKeyword1"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("AttestKeyword2"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("AttestKeywordTemp1"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("AttestKeywordTemp2"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("ContactFirstName"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("ContactLastName"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("ContactEmail"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("ContactEmail2"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("ContactAddress1"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("ContactAddress2"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("ContactCity"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("ContactStateID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("ContactProvince"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("ContactZIP"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("ContactCountryID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("ContactPhone"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("ContactFax"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("ContactMobile"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("SecretQuestion"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("SecretAnswer"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("CreditCardFullName"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("CreditCardNumber"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("CreditCardType"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("CreditCardExpMonth"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("CreditCardExpYear"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("CreditCardChargeDate"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("CreditCardPostDate"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("AlertEmail"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("AlertDays"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("BillingComments"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("PromoCode"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("CompanyType"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("CompanyName"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("CompanyAddress1"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("CompanyAddress2"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("CompanyCity"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("CompanyStateID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("CompanyZIP"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("CompanyPhone"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("CompanyFax"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("Comments"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("BillingID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("PHDBAcknowledgementStatus"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("PHDBLetterHeadStatus"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("TaxID"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("NotUsingPHDBID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("NotUsingPHDBComments"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("PayerID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("ManagerID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("Comm_Email_RequiresAttachement"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("Comm_Alerts_LevelUser"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("Comm_Alerts_LevelManager"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("Comm_Alerts_LevelBranch"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("Comm_Alerts2_LevelUser"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("Comm_Alerts2_LevelManager"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("Comm_Alerts2_LevelBranch"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("Comm_Report_LevelUser"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("Comm_Report_LevelManager"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("Comm_Report_LevelBranch"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("UserNPI"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("UserStateLicense"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("UserStateLicenseDesc"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("SelectMRI_ID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("SelectMRI_Notes"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("MELicense"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("SelectMRI_UserType"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("ImportantNotes_Alert"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("EmailAlertNotes_Alert"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("ImportantNotes"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("NIM_UserType"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("TotalMonthlyMRIs"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("ExpectedMonthlyMRIs"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("Data_Rank"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("Data_3MonthAverage"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("Data_Referrals_AllTime"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("Data_Referrals_AnalysisTotal"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("Data_Referrals_AnalysisTurnAroundAverage_ToSched"))
        {
            myVal = "Double";
        }

        else if (fieldN.equals("Data_Referrals_AnalysisCostSavings"))
        {
            myVal = "Double";
        }

        else if (fieldN.equals("Internal_PrimaryContact"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("LastContacted"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("DateOfBirth"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("PersonalInsights"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("AllowAsPrimaryContact"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("IsAdmin"))
        {
            myVal = "Integer";
        }

	    }
	    catch(Exception e2)
	    {}
	    return myVal;
	}

    public boolean isComplete(String fieldN)
    {
	     boolean myVal = false;
	     try
	     {
        java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat("yyyy-MM-dd");

	     if (fieldN.equals("UserID"))

	     {
                    if (this.getUserID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("UniqueCreateDate"))

	     {
                    if (this.getUniqueCreateDate().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("UniqueModifyDate"))

	     {
                    if (this.getUniqueModifyDate().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("UniqueModifyComments"))

	     {
                    if (this.getUniqueModifyComments().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("PLCID"))

	     {
                    if (this.getPLCID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("StartPage"))

	     {
                    if (this.getStartPage().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("AccountType"))

	     {
                    if (this.getAccountType().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("SecurityGroupID"))

	     {
                    if (this.getSecurityGroupID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("GenericSecurityGroupID"))

	     {
                    if (this.getGenericSecurityGroupID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("ReferenceID"))

	     {
                    if (this.getReferenceID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("AccessType"))

	     {
                    if (this.getAccessType().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("Status"))

	     {
                    if (this.getStatus().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("LogonUserName"))

	     {
                    if (this.getLogonUserName().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("LogonUserPassword"))

	     {
                    if (this.getLogonUserPassword().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("AttestKeyword1"))

	     {
                    if (this.getAttestKeyword1().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("AttestKeyword2"))

	     {
                    if (this.getAttestKeyword2().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("AttestKeywordTemp1"))

	     {
                    if (this.getAttestKeywordTemp1().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("AttestKeywordTemp2"))

	     {
                    if (this.getAttestKeywordTemp2().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("ContactFirstName"))

	     {
                    if (this.getContactFirstName().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("ContactLastName"))

	     {
                    if (this.getContactLastName().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("ContactEmail"))

	     {
                    if (this.getContactEmail().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("ContactEmail2"))

	     {
                    if (this.getContactEmail2().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("ContactAddress1"))

	     {
                    if (this.getContactAddress1().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("ContactAddress2"))

	     {
                    if (this.getContactAddress2().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("ContactCity"))

	     {
                    if (this.getContactCity().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("ContactStateID"))

	     {
                    if (this.getContactStateID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("ContactProvince"))

	     {
                    if (this.getContactProvince().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("ContactZIP"))

	     {
                    if (this.getContactZIP().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("ContactCountryID"))

	     {
                    if (this.getContactCountryID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("ContactPhone"))

	     {
                    if (this.getContactPhone().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("ContactFax"))

	     {
                    if (this.getContactFax().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("ContactMobile"))

	     {
                    if (this.getContactMobile().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("SecretQuestion"))

	     {
                    if (this.getSecretQuestion().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("SecretAnswer"))

	     {
                    if (this.getSecretAnswer().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("CreditCardFullName"))

	     {
                    if (this.getCreditCardFullName().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("CreditCardNumber"))

	     {
                    if (this.getCreditCardNumber().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("CreditCardType"))

	     {
                    if (this.getCreditCardType().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("CreditCardExpMonth"))

	     {
                    if (this.getCreditCardExpMonth().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("CreditCardExpYear"))

	     {
                    if (this.getCreditCardExpYear().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("CreditCardChargeDate"))

	     {
                    if (this.getCreditCardChargeDate().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("CreditCardPostDate"))

	     {
                    if (this.getCreditCardPostDate().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("AlertEmail"))

	     {
                    if (this.getAlertEmail().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("AlertDays"))

	     {
                    if (this.getAlertDays().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("BillingComments"))

	     {
                    if (this.getBillingComments().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("PromoCode"))

	     {
                    if (this.getPromoCode().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("CompanyType"))

	     {
                    if (this.getCompanyType().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("CompanyName"))

	     {
                    if (this.getCompanyName().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("CompanyAddress1"))

	     {
                    if (this.getCompanyAddress1().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("CompanyAddress2"))

	     {
                    if (this.getCompanyAddress2().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("CompanyCity"))

	     {
                    if (this.getCompanyCity().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("CompanyStateID"))

	     {
                    if (this.getCompanyStateID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("CompanyZIP"))

	     {
                    if (this.getCompanyZIP().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("CompanyPhone"))

	     {
                    if (this.getCompanyPhone().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("CompanyFax"))

	     {
                    if (this.getCompanyFax().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("Comments"))

	     {
                    if (this.getComments().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("BillingID"))

	     {
                    if (this.getBillingID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("PHDBAcknowledgementStatus"))

	     {
                    if (this.getPHDBAcknowledgementStatus().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("PHDBLetterHeadStatus"))

	     {
                    if (this.getPHDBLetterHeadStatus().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("TaxID"))

	     {
                    if (this.getTaxID().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("NotUsingPHDBID"))

	     {
                    if (this.getNotUsingPHDBID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("NotUsingPHDBComments"))

	     {
                    if (this.getNotUsingPHDBComments().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("PayerID"))

	     {
                    if (this.getPayerID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("ManagerID"))

	     {
                    if (this.getManagerID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("Comm_Email_RequiresAttachement"))

	     {
                    if (this.getComm_Email_RequiresAttachement().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("Comm_Alerts_LevelUser"))

	     {
                    if (this.getComm_Alerts_LevelUser().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("Comm_Alerts_LevelManager"))

	     {
                    if (this.getComm_Alerts_LevelManager().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("Comm_Alerts_LevelBranch"))

	     {
                    if (this.getComm_Alerts_LevelBranch().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("Comm_Alerts2_LevelUser"))

	     {
                    if (this.getComm_Alerts2_LevelUser().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("Comm_Alerts2_LevelManager"))

	     {
                    if (this.getComm_Alerts2_LevelManager().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("Comm_Alerts2_LevelBranch"))

	     {
                    if (this.getComm_Alerts2_LevelBranch().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("Comm_Report_LevelUser"))

	     {
                    if (this.getComm_Report_LevelUser().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("Comm_Report_LevelManager"))

	     {
                    if (this.getComm_Report_LevelManager().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("Comm_Report_LevelBranch"))

	     {
                    if (this.getComm_Report_LevelBranch().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("UserNPI"))

	     {
                    if (this.getUserNPI().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("UserStateLicense"))

	     {
                    if (this.getUserStateLicense().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("UserStateLicenseDesc"))

	     {
                    if (this.getUserStateLicenseDesc().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("SelectMRI_ID"))

	     {
                    if (this.getSelectMRI_ID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("SelectMRI_Notes"))

	     {
                    if (this.getSelectMRI_Notes().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("MELicense"))

	     {
                    if (this.getMELicense().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("SelectMRI_UserType"))

	     {
                    if (this.getSelectMRI_UserType().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("ImportantNotes_Alert"))

	     {
                    if (this.getImportantNotes_Alert().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("EmailAlertNotes_Alert"))

	     {
                    if (this.getEmailAlertNotes_Alert().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("ImportantNotes"))

	     {
                    if (this.getImportantNotes().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("NIM_UserType"))

	     {
                    if (this.getNIM_UserType().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("TotalMonthlyMRIs"))

	     {
                    if (this.getTotalMonthlyMRIs().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("ExpectedMonthlyMRIs"))

	     {
                    if (this.getExpectedMonthlyMRIs().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("Data_Rank"))

	     {
                    if (this.getData_Rank().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("Data_3MonthAverage"))

	     {
                    if (this.getData_3MonthAverage().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("Data_Referrals_AllTime"))

	     {
                    if (this.getData_Referrals_AllTime().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("Data_Referrals_AnalysisTotal"))

	     {
                    if (this.getData_Referrals_AnalysisTotal().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("Data_Referrals_AnalysisTurnAroundAverage_ToSched"))

	     {
                    if (this.getData_Referrals_AnalysisTurnAroundAverage_ToSched().equals(new Double("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("Data_Referrals_AnalysisCostSavings"))

	     {
                    if (this.getData_Referrals_AnalysisCostSavings().equals(new Double("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("Internal_PrimaryContact"))

	     {
                    if (this.getInternal_PrimaryContact().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("LastContacted"))

	     {
                    if (this.getLastContacted().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("DateOfBirth"))

	     {
                    if (this.getDateOfBirth().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("PersonalInsights"))

	     {
                    if (this.getPersonalInsights().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("AllowAsPrimaryContact"))

	     {
                    if (this.getAllowAsPrimaryContact().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("IsAdmin"))

	     {
                    if (this.getIsAdmin().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	    }
	    catch(Exception e2)
	    {}
	    return myVal;
	}

    public boolean isExpired(String fieldN,int daysVector)
    {
        return isExpired(fieldN,new java.util.Date(),daysVector);
    }

    public boolean isExpired(String fieldN,java.util.Date refDate,int daysVector)
    {
	     boolean myVal = false;
	     try
	     {
        java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat("yyyy-MM-dd");

        if (fieldN.equals("UniqueCreateDate"))

	     {
                    if (this.isExpired(this.getUniqueCreateDate(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

        if (fieldN.equals("UniqueModifyDate"))

	     {
                    if (this.isExpired(this.getUniqueModifyDate(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

        if (fieldN.equals("CreditCardChargeDate"))

	     {
                    if (this.isExpired(this.getCreditCardChargeDate(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

        if (fieldN.equals("CreditCardPostDate"))

	     {
                    if (this.isExpired(this.getCreditCardPostDate(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

        if (fieldN.equals("LastContacted"))

	     {
                    if (this.isExpired(this.getLastContacted(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

        if (fieldN.equals("DateOfBirth"))

	     {
                    if (this.isExpired(this.getDateOfBirth(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

	    }
	    catch(Exception e2)
	    {}
	    return myVal;
	}

    public java.util.Date getDateField(String fieldN)
    {
	     java.util.Date myVal = null;
	     try
	     {
        java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat("yyyy-MM-dd");

        if (fieldN.equals("UniqueCreateDate"))

	     {
             myVal = this.getUniqueCreateDate();
        }

        if (fieldN.equals("UniqueModifyDate"))

	     {
             myVal = this.getUniqueModifyDate();
        }

        if (fieldN.equals("CreditCardChargeDate"))

	     {
             myVal = this.getCreditCardChargeDate();
        }

        if (fieldN.equals("CreditCardPostDate"))

	     {
             myVal = this.getCreditCardPostDate();
        }

        if (fieldN.equals("LastContacted"))

	     {
             myVal = this.getLastContacted();
        }

        if (fieldN.equals("DateOfBirth"))

	     {
             myVal = this.getDateOfBirth();
        }

	    }
	    catch(Exception e2)
	    {}
	    return myVal;
	}
	public boolean runFieldSecurity = false;
	public Integer UserSecurityID = null;
   public Integer iSecurityCheck = null;
}    // End of bltUserAccount class definition
