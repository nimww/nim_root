

package com.winstaff;

import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public interface InttPeerReference extends dbTableInterface
{

    public void setReferenceID(Integer newValue);
    public Integer getReferenceID();
    public void setUniqueCreateDate(Date newValue);
    public Date getUniqueCreateDate();
    public void setUniqueModifyDate(Date newValue);
    public Date getUniqueModifyDate();
    public void setUniqueModifyComments(String newValue);
    public String getUniqueModifyComments();
    public void setPhysicianID(Integer newValue);
    public Integer getPhysicianID();
    public void setSalutation(Integer newValue);
    public Integer getSalutation();
    public void setFirstName(String newValue);
    public String getFirstName();
    public void setLastName(String newValue);
    public String getLastName();
    public void setSpecialty(String newValue);
    public String getSpecialty();
    public void setAddress1(String newValue);
    public String getAddress1();
    public void setAddress2(String newValue);
    public String getAddress2();
    public void setCity(String newValue);
    public String getCity();
    public void setStateID(Integer newValue);
    public Integer getStateID();
    public void setProvince(String newValue);
    public String getProvince();
    public void setZIP(String newValue);
    public String getZIP();
    public void setCountryID(Integer newValue);
    public Integer getCountryID();
    public void setPhone(String newValue);
    public String getPhone();
    public void setFax(String newValue);
    public String getFax();
    public void setContactEmail(String newValue);
    public String getContactEmail();
    public void setYearsAssociated(String newValue);
    public String getYearsAssociated();
    public void setDocuLinkID(Integer newValue);
    public Integer getDocuLinkID();
    public void setComments(String newValue);
    public String getComments();
    public void setTitle(String newValue);
    public String getTitle();
    public void setHospitalAffiliation(String newValue);
    public String getHospitalAffiliation();
    public void setHospitalDepartment(String newValue);
    public String getHospitalDepartment();
}    // End of bltPeerReference class definition
