package com.winstaff;
import java.io.*;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

public class Practices_Active_New_Terminated_Cache{
	public static void main(String[]args){
		try {
			getProviderList();
			getNewProviderList();
			cleanTempTable();
			updateTable();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	public static void getProviderList() throws IOException{
		String active_query = "SELECT DISTINCT\n" +
				"	ON (pract.practiceid) pract.practiceid,\n" +
				"	'All_Active' status,\n" +
				"	pract.officeaddress1,\n" +
				"	pract.officeaddress2,\n" +
				"	pract.officecity,\n" +
				"	tsta.shortstate AS officestate,\n" +
				"	pract.officezip,\n" +
				"	pract.officefederaltaxid,\n" +
				"	pract.npinumber,\n" +
				"	CASE\n" +
				"WHEN pract.initialcontractdate = '1800-01-01 00:00:00' THEN\n" +
				"	'2010-01-01'\n" +
				"ELSE\n" +
				"	pract.initialcontractdate\n" +
				"END AS effective_date\n" +
				"FROM\n" +
				"	tpracticemaster AS pract\n" +
				"INNER JOIN tstateli tsta ON tsta.stateid = pract.officestateid\n" +
				"WHERE\n" +
				"	pract.contractingstatusid IN (2, 8, 10, 11, 12)\n" +
				"AND pract.officefederaltaxid != ''\n" +
				"AND pract.officefederaltaxid IS NOT NULL\n" +
				"ORDER BY\n" +
				"	pract.practiceid DESC";
		
		
		
		
		
		
		
		
		String term_query = "SELECT DISTINCT\n" +
				"	ON (prac.practiceid) prac.practiceid,\n" +
				"	'Terminated' status,\n" +
				"	prac.officeaddress1,\n" +
				"	prac.officeaddress2,\n" +
				"	prac.officecity,\n" +
				"	tsta.shortstate AS officestate,\n" +
				"	prac.officezip,\n" +
				"	prac.officefederaltaxid,\n" +
				"	prac.npinumber,\n" +
				"	CASE\n" +
				"WHEN prac.initialcontractdate = '1800-01-01 00:00:00' THEN\n" +
				"	'2010-01-01'\n" +
				"ELSE\n" +
				"	prac.initialcontractdate\n" +
				"END AS effective_date\n" +
				"FROM\n" +
				"	tpracticemaster AS prac\n" +
				"FULL OUTER JOIN tpracticemaster_temp USING (practiceid)\n" +
				"INNER JOIN tstateli tsta ON tsta.stateid = prac.officestateid\n" +
				"WHERE\n" +
				"	prac.officefederaltaxid != ''\n" +
				"AND prac.officefederaltaxid IS NOT NULL\n" +
				"AND tpracticemaster_temp.practiceid IS NULL\n" +
				"OR prac.practiceid IS NULL";
		
		
		
		
		
		
		
		String new_query = "SELECT DISTINCT\n" +
				"	ON (prac.practiceid) prac.practiceid,\n" +
				"	'New_Center' status,\n" +
				"	prac.officeaddress1,\n" +
				"	prac.officeaddress2,\n" +
				"	prac.officecity,\n" +
				"	prac.officezip AS officestate,\n" +
				"	prac.officezip,\n" +
				"	prac.officefederaltaxid,\n" +
				"	prac.npinumber,\n" +
				"	CASE\n" +
				"WHEN prac.initialcontractdate = '1800-01-01 00:00:00' THEN\n" +
				"	'2010-01-01'\n" +
				"ELSE\n" +
				"	prac.initialcontractdate\n" +
				"END AS effective_date\n" +
				"FROM\n" +
				"	tpracticemaster_temp AS prac\n" +
				"WHERE\n" +
				"	NOT EXISTS (\n" +
				"		SELECT\n" +
				"			tpracticemaster.practiceid\n" +
				"		FROM\n" +
				"			tpracticemaster\n" +
				"		INNER JOIN tstateli tsta ON tsta.stateid = prac.officestateid\n" +
				"		WHERE\n" +
				"			prac.practiceid = tpracticemaster.practiceid\n" +
				"	)\n" +
				"AND prac.officefederaltaxid != ''\n" +
				"AND prac.officefederaltaxid IS NOT NULL";
		
		searchDB2 ss = new searchDB2();
		ResultSet rs1 = ss.executeStatement(active_query);
		ResultSet rs2 = ss.executeStatement(term_query);
		ResultSet rs3 = ss.executeStatement(new_query);
		
		String npl = "export/nim3_tyson_provider_list.xls";
		HSSFWorkbook hwb = new HSSFWorkbook();
		
		HSSFSheet sheet1 = hwb.createSheet("Active List");
		HSSFSheet sheet2 = hwb.createSheet("Terminated Provider List");
		HSSFSheet sheet3 = hwb.createSheet("New Provider List");
		
		
		HSSFRow rh1 = sheet1.createRow((int) 0);
		HSSFRow rh2 = sheet2.createRow((int) 0);
		HSSFRow rh3 = sheet3.createRow((int) 0);
		
		rh1.createCell((int) 0).setCellValue("practiceid");
		rh1.createCell((int) 1).setCellValue("status");
		rh1.createCell((int) 2).setCellValue("officeaddress1");
		rh1.createCell((int) 3).setCellValue("officeaddress2");
		rh1.createCell((int) 4).setCellValue("officecity");
		rh1.createCell((int) 5).setCellValue("officestate");
		rh1.createCell((int) 6).setCellValue("officezip");
		rh1.createCell((int) 7).setCellValue("officefederaltaxid");
		rh1.createCell((int) 8).setCellValue("npinumber");
		rh1.createCell((int) 9).setCellValue("effective_date");
		int i = 1;
		try {
			while(rs1.next()){
					
				HSSFRow row1 = sheet1.createRow((int) i);
				row1.createCell((int) 0).setCellValue(rs1.getString("practiceid"));
				row1.createCell((int) 1).setCellValue(rs1.getString("status"));
				row1.createCell((int) 2).setCellValue(rs1.getString("officeaddress1"));
				row1.createCell((int) 3).setCellValue(rs1.getString("officeaddress2"));
				row1.createCell((int) 4).setCellValue(rs1.getString("officecity"));
				row1.createCell((int) 5).setCellValue(rs1.getString("officestate"));
				row1.createCell((int) 6).setCellValue(rs1.getString("officezip"));
				row1.createCell((int) 7).setCellValue(rs1.getString("officefederaltaxid"));
				row1.createCell((int) 8).setCellValue(rs1.getString("npinumber"));
				row1.createCell((int) 9).setCellValue(rs1.getString("effective_date"));
				i++;
			}
	}catch(Exception e){
		e.printStackTrace();
	}
		
		rh2.createCell((int) 0).setCellValue("practiceid");
		rh2.createCell((int) 1).setCellValue("status");
		rh2.createCell((int) 2).setCellValue("officeaddress1");
		rh2.createCell((int) 3).setCellValue("officeaddress2");
		rh2.createCell((int) 4).setCellValue("officecity");
		rh2.createCell((int) 5).setCellValue("officestate");
		rh2.createCell((int) 6).setCellValue("officezip");
		rh2.createCell((int) 7).setCellValue("officefederaltaxid");
		rh2.createCell((int) 8).setCellValue("npinumber");
		rh2.createCell((int) 9).setCellValue("effective_date");
		int j = 1;
		try {
			while(rs2.next()){
					
				HSSFRow row2 = sheet2.createRow((int) j);
				row2.createCell((int) 0).setCellValue(rs2.getString("practiceid"));
				row2.createCell((int) 1).setCellValue(rs2.getString("status"));
				row2.createCell((int) 2).setCellValue(rs2.getString("officeaddress1"));
				row2.createCell((int) 3).setCellValue(rs2.getString("officeaddress2"));
				row2.createCell((int) 4).setCellValue(rs2.getString("officecity"));
				row2.createCell((int) 5).setCellValue(rs2.getString("officestate"));
				row2.createCell((int) 6).setCellValue(rs2.getString("officezip"));
				row2.createCell((int) 7).setCellValue(rs2.getString("officefederaltaxid"));
				row2.createCell((int) 8).setCellValue(rs2.getString("npinumber"));
				row2.createCell((int) 9).setCellValue(rs2.getString("effective_date"));
				j++;
			}
	}catch(Exception e){
		e.printStackTrace();
	}
		rh3.createCell((int) 0).setCellValue("practiceid");
		rh3.createCell((int) 1).setCellValue("status");
		rh3.createCell((int) 2).setCellValue("officeaddress1");
		rh3.createCell((int) 3).setCellValue("officeaddress2");
		rh3.createCell((int) 4).setCellValue("officecity");
		rh3.createCell((int) 5).setCellValue("officestate");
		rh3.createCell((int) 6).setCellValue("officezip");
		rh3.createCell((int) 7).setCellValue("officefederaltaxid");
		rh3.createCell((int) 8).setCellValue("npinumber");
		rh3.createCell((int) 9).setCellValue("effective_date");
		int k = 1;
		try {
			while(rs3.next()){
					
				HSSFRow row3 = sheet3.createRow((int) k);
				row3.createCell((int) 0).setCellValue(rs3.getString("practiceid"));
				row3.createCell((int) 1).setCellValue(rs3.getString("status"));
				row3.createCell((int) 2).setCellValue(rs3.getString("officeaddress1"));
				row3.createCell((int) 3).setCellValue(rs3.getString("officeaddress2"));
				row3.createCell((int) 4).setCellValue(rs3.getString("officecity"));
				row3.createCell((int) 5).setCellValue(rs3.getString("officestate"));
				row3.createCell((int) 6).setCellValue(rs3.getString("officezip"));
				row3.createCell((int) 7).setCellValue(rs3.getString("officefederaltaxid"));
				row3.createCell((int) 8).setCellValue(rs3.getString("npinumber"));
				row3.createCell((int) 9).setCellValue(rs3.getString("effective_date"));
				k++;
			}
	}catch(Exception e){
		e.printStackTrace();
	}
		FileOutputStream fileout = null;
		try {
			fileout = new FileOutputStream(npl);
			hwb.write(fileout);
			fileout.close();
			System.out.println("Excel sheet 1 being generated via !!..");	
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}finally{
			ss.closeAll();
		}
	}
	
	public static void cleanTempTable(){
		String query = "truncate tpracticemaster_temp";
		searchDB2 ss = new searchDB2();
		ResultSet rs = null;
		ss.executeUpdate(query);
		ss.closeAll();
	}
	
	public static void updateTable(){
		String query = "insert into tpracticemaster_temp\n" +
				"select *\n" +
				"from tpracticemaster\n" +
				"where contractingstatusid in (2,8,10,11,12)";
		searchDB2 ss = new searchDB2();
		ResultSet rs = null;
		ss.executeUpdate(query);
		ss.closeAll();
	}
	
	public static void getNewProviderList() throws IOException{
		String query = "(\n" +
				"	SELECT\n" +
				"		active.practiceid ,'active' status, active.officeaddress1,active.officeaddress2, active.officecity, tsta.shortstate as officestate, active.officezip, active.officefederaltaxid, active.npinumber, to_date(active.initialcontractdate::text, 'YYYY-MM-DD') as effective_date\n" +
				"		\n" +
				"	FROM\n" +
				"		tpracticemaster AS active\n" +
				"	inner join tstateli tsta\n" +
				"on tsta.stateid = active.officestateid\n" +
				"	WHERE\n" +
				"		active.contractingstatusid IN (2, 8, 10, 11, 12)\n" +
				")\n" +
				"UNION\n" +
				"	(\n" +
				"		SELECT\n" +
				"			newP.practiceid, 'new' status, newP.officeaddress1,newP.officeaddress2, newP.officecity, tsta.shortstate as officestate, newP.officezip, newP.officefederaltaxid, newP.npinumber,to_date(newP.initialcontractdate::text, 'YYYY-MM-DD') as effective_date\n" +
				"			\n" +
				"		FROM\n" +
				"			tpracticemaster AS newP\n" +
				"inner join tstateli tsta\n" +
				"on tsta.stateid = newP.officestateid\n" +
				"		WHERE\n" +
				"			newP.practiceid NOT IN (\n" +
				"				SELECT\n" +
				"					newP.practiceid\n" +
				"				FROM\n" +
				"					tpracticemaster_temp AS newP\n" +
				"			)\n" +
				"		AND newP.contractingstatusid IN (2, 8, 10, 11, 12)\n" +
				"	)\n" +
				"UNION\n" +
				"	(\n" +
				"		(\n" +
				"			SELECT\n" +
				"				removed.practiceid, 'removed' status, removed.officeaddress1,removed.officeaddress2, removed.officecity, tsta.shortstate as officestate, removed.officezip, removed.officefederaltaxid, removed.npinumber, to_date(removed.initialcontractdate::text, 'YYYY-MM-DD') as effective_date\n" +
				"				\n" +
				"			FROM\n" +
				"				tpracticemaster_temp AS removed\n" +
				"			inner join tstateli tsta\n" +
				"			on tsta.stateid = removed.officestateid\n" +
				"			WHERE\n" +
				"				removed.practiceid NOT IN (\n" +
				"					SELECT\n" +
				"						removed.practiceid\n" +
				"					FROM\n" +
				"						tpracticemaster AS removed\n" +
				"						where removed.contractingstatusid IN (2, 8, 10, 11, 12)\n" +
				"				)\n" +
				"		)\n" +
				"	)\n" +
				"ORDER BY\n" +
				"	status";
		searchDB2 ss = new searchDB2();
		ResultSet rs = ss.executeStatement(query);
		String npl = "export/full_provider_detail_list.xls";
		HSSFWorkbook hwb = new HSSFWorkbook();
		HSSFSheet sheet = hwb.createSheet("Full Provider detail List");
		HSSFRow rh = sheet.createRow((int) 0);
		
		rh.createCell((int) 0).setCellValue("practiceid");
		rh.createCell((int) 1).setCellValue("status");
		rh.createCell((int) 2).setCellValue("officeaddress1");
		rh.createCell((int) 3).setCellValue("officeaddress2");
		rh.createCell((int) 4).setCellValue("officecity");
		rh.createCell((int) 5).setCellValue("officestate");
		rh.createCell((int) 6).setCellValue("officezip");
		rh.createCell((int) 7).setCellValue("officefederaltaxid");
		rh.createCell((int) 8).setCellValue("npinumber");
		rh.createCell((int) 9).setCellValue("effective_date");
		int i = 1;
		try {
			while(rs.next()){
					
				HSSFRow row = sheet.createRow((int) i);
				row.createCell((int) 0).setCellValue(rs.getString("practiceid"));
				row.createCell((int) 1).setCellValue(rs.getString("status"));
				row.createCell((int) 2).setCellValue(rs.getString("officeaddress1"));
				row.createCell((int) 3).setCellValue(rs.getString("officeaddress2"));
				row.createCell((int) 4).setCellValue(rs.getString("officecity"));
				row.createCell((int) 5).setCellValue(rs.getString("officestate"));
				row.createCell((int) 6).setCellValue(rs.getString("officezip"));
				row.createCell((int) 7).setCellValue(rs.getString("officefederaltaxid"));
				row.createCell((int) 8).setCellValue(rs.getString("npinumber"));
				row.createCell((int) 9).setCellValue(rs.getString("effective_date"));
				i++;
			}
			FileOutputStream fileout = new FileOutputStream(npl);
			hwb.write(fileout);
			fileout.close();
			System.out.println("Excel sheet 1 being generated via Union..");
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally{
			ss.closeAll();
		}
	}
}