

package com.winstaff;

import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public class dbtCommTrackTypeLI extends Object implements InttCommTrackTypeLI
{

        db_NewBase    dbnbDB;

    public dbtCommTrackTypeLI()
    {
        dbnbDB = new db_NewBase( "tCommTrackTypeLI", "CommTrackTypeID" );

    }    // End of default constructor

    public dbtCommTrackTypeLI( Integer iNewID )
    {
        dbnbDB = new db_NewBase( "tCommTrackTypeLI", "CommTrackTypeID" );
        setUniqueID( iNewID );
    }    // End of Constructor knowing an ID

    public void commitData() throws SQLException
    {
            //no longer trapping/ignoring errors 2011-03-17
            dbnbDB.commitData();
    }    // End of commitData()

    public void commitDataForced() throws SQLException
    {
        dbnbDB.commitDataForced();
    }    // End of commitData()

    public Integer getUniqueID()
    {
        return dbnbDB.getUniqueID();
    }    // End of getUniqueID()

    public void setUniqueID( Integer iNewID )
    {
        dbnbDB.setUniqueID( iNewID );
    }    // End of setUniqueID()

    public void setModifyComments( String sNew )
    {
        dbnbDB.setModifyComments( sNew );
    }    // End of setUniqueID()


    public void setCommTrackTypeID(Integer newValue)
    {
                dbnbDB.setFieldData( "CommTrackTypeID", newValue.toString() );
    }

    public Integer getCommTrackTypeID()
    {
        String           sValue = dbnbDB.getFieldData( "CommTrackTypeID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setField(String fieldN, Object fieldV)
    {DebugLogger.println("setField Method not implemented yet in class!");}

    public String getFieldType(String fieldN)
    {return null;}


    public void setCommTrackTypeShort(String newValue)
    {
                dbnbDB.setFieldData( "CommTrackTypeShort", newValue.toString() );
    }

    public String getCommTrackTypeShort()
    {
        String           sValue = dbnbDB.getFieldData( "CommTrackTypeShort" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setCommTrackTypeLong(String newValue)
    {
                dbnbDB.setFieldData( "CommTrackTypeLong", newValue.toString() );
    }

    public String getCommTrackTypeLong()
    {
        String           sValue = dbnbDB.getFieldData( "CommTrackTypeLong" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

}    // End of bltCommTrackTypeLI class definition
