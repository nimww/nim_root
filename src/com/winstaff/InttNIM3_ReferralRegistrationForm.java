

package com.winstaff;

import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public interface InttNIM3_ReferralRegistrationForm extends dbTableInterface
{

    public void setPortalRegisterID(Integer newValue);
    public Integer getPortalRegisterID();
    public void setUniqueCreateDate(Date newValue);
    public Date getUniqueCreateDate();
    public void setUniqueModifyDate(Date newValue);
    public Date getUniqueModifyDate();
    public void setUniqueModifyComments(String newValue);
    public String getUniqueModifyComments();
    public void setStatusID(Integer newValue);
    public Integer getStatusID();
    public void setFirstName(String newValue);
    public String getFirstName();
    public void setLastName(String newValue);
    public String getLastName();
    public void setPhone(String newValue);
    public String getPhone();
    public void setFax(String newValue);
    public String getFax();
    public void setEmail(String newValue);
    public String getEmail();
    public void setMessageText(String newValue);
    public String getMessageText();
    public void setEncounterID(Integer newValue);
    public Integer getEncounterID();
}    // End of bltNIM3_ReferralRegistrationForm class definition
