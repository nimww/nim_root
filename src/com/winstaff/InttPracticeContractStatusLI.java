

package com.winstaff;

import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public interface InttPracticeContractStatusLI extends dbTableInterface
{

    public void setPracticeContractStatusID(Integer newValue);
    public Integer getPracticeContractStatusID();
    public void setDescriptionLong(String newValue);
    public String getDescriptionLong();
}    // End of bltPracticeContractStatusLI class definition
