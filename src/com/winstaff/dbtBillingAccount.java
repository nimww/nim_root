

package com.winstaff;

import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public class dbtBillingAccount extends Object implements InttBillingAccount
{

        db_NewBase    dbnbDB;

    public dbtBillingAccount()
    {
        dbnbDB = new db_NewBase( "tBillingAccount", "BillingID" );

    }    // End of default constructor

    public dbtBillingAccount( Integer iNewID )
    {
        dbnbDB = new db_NewBase( "tBillingAccount", "BillingID" );
        setUniqueID( iNewID );
    }    // End of Constructor knowing an ID

    public void commitData() throws SQLException
    {
            //no longer trapping/ignoring errors 2011-03-17
            dbnbDB.commitData();
    }    // End of commitData()

    public void commitDataForced() throws SQLException
    {
        dbnbDB.commitDataForced();
    }    // End of commitData()

    public Integer getUniqueID()
    {
        return dbnbDB.getUniqueID();
    }    // End of getUniqueID()

    public void setUniqueID( Integer iNewID )
    {
        dbnbDB.setUniqueID( iNewID );
    }    // End of setUniqueID()

    public void setModifyComments( String sNew )
    {
        dbnbDB.setModifyComments( sNew );
    }    // End of setUniqueID()


    public void setBillingID(Integer newValue)
    {
                dbnbDB.setFieldData( "BillingID", newValue.toString() );
    }

    public Integer getBillingID()
    {
        String           sValue = dbnbDB.getFieldData( "BillingID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setField(String fieldN, Object fieldV)
    {DebugLogger.println("setField Method not implemented yet in classBillingAccount!");}

    public String getFieldType(String fieldN)
    {return null;}


    public void setUniqueCreateDate(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "UniqueCreateDate", formatter.format( newValue ) );
    }

    public Date getUniqueCreateDate()
    {
        String           sValue = dbnbDB.getFieldData( "UniqueCreateDate" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setUniqueModifyDate(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "UniqueModifyDate", formatter.format( newValue ) );
    }

    public Date getUniqueModifyDate()
    {
        String           sValue = dbnbDB.getFieldData( "UniqueModifyDate" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setUniqueModifyComments(String newValue)
    {
                dbnbDB.setFieldData( "UniqueModifyComments", newValue.toString() );
    }

    public String getUniqueModifyComments()
    {
        String           sValue = dbnbDB.getFieldData( "UniqueModifyComments" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setBillingType(String newValue)
    {
                dbnbDB.setFieldData( "BillingType", newValue.toString() );
    }

    public String getBillingType()
    {
        String           sValue = dbnbDB.getFieldData( "BillingType" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setStatus(Integer newValue)
    {
                dbnbDB.setFieldData( "Status", newValue.toString() );
    }

    public Integer getStatus()
    {
        String           sValue = dbnbDB.getFieldData( "Status" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setWSAccountNumber(String newValue)
    {
                dbnbDB.setFieldData( "WSAccountNumber", newValue.toString() );
    }

    public String getWSAccountNumber()
    {
        String           sValue = dbnbDB.getFieldData( "WSAccountNumber" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setContactFirstName(String newValue)
    {
                dbnbDB.setFieldData( "ContactFirstName", newValue.toString() );
    }

    public String getContactFirstName()
    {
        String           sValue = dbnbDB.getFieldData( "ContactFirstName" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setContactLastName(String newValue)
    {
                dbnbDB.setFieldData( "ContactLastName", newValue.toString() );
    }

    public String getContactLastName()
    {
        String           sValue = dbnbDB.getFieldData( "ContactLastName" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setContactEmail(String newValue)
    {
                dbnbDB.setFieldData( "ContactEmail", newValue.toString() );
    }

    public String getContactEmail()
    {
        String           sValue = dbnbDB.getFieldData( "ContactEmail" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setContactAddress1(String newValue)
    {
                dbnbDB.setFieldData( "ContactAddress1", newValue.toString() );
    }

    public String getContactAddress1()
    {
        String           sValue = dbnbDB.getFieldData( "ContactAddress1" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setContactAddress2(String newValue)
    {
                dbnbDB.setFieldData( "ContactAddress2", newValue.toString() );
    }

    public String getContactAddress2()
    {
        String           sValue = dbnbDB.getFieldData( "ContactAddress2" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setContactCity(String newValue)
    {
                dbnbDB.setFieldData( "ContactCity", newValue.toString() );
    }

    public String getContactCity()
    {
        String           sValue = dbnbDB.getFieldData( "ContactCity" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setContactStateID(Integer newValue)
    {
                dbnbDB.setFieldData( "ContactStateID", newValue.toString() );
    }

    public Integer getContactStateID()
    {
        String           sValue = dbnbDB.getFieldData( "ContactStateID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setContactProvince(String newValue)
    {
                dbnbDB.setFieldData( "ContactProvince", newValue.toString() );
    }

    public String getContactProvince()
    {
        String           sValue = dbnbDB.getFieldData( "ContactProvince" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setContactZIP(String newValue)
    {
                dbnbDB.setFieldData( "ContactZIP", newValue.toString() );
    }

    public String getContactZIP()
    {
        String           sValue = dbnbDB.getFieldData( "ContactZIP" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setContactCountryID(Integer newValue)
    {
                dbnbDB.setFieldData( "ContactCountryID", newValue.toString() );
    }

    public Integer getContactCountryID()
    {
        String           sValue = dbnbDB.getFieldData( "ContactCountryID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setContactPhone(String newValue)
    {
                dbnbDB.setFieldData( "ContactPhone", newValue.toString() );
    }

    public String getContactPhone()
    {
        String           sValue = dbnbDB.getFieldData( "ContactPhone" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setAccountFullName(String newValue)
    {
                dbnbDB.setFieldData( "AccountFullName", newValue.toString() );
    }

    public String getAccountFullName()
    {
        String           sValue = dbnbDB.getFieldData( "AccountFullName" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setAccountNumber1(String newValue)
    {
                dbnbDB.setFieldData( "AccountNumber1", newValue.toString() );
    }

    public String getAccountNumber1()
    {
        String           sValue = dbnbDB.getFieldData( "AccountNumber1" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setAccountNumber2(String newValue)
    {
                dbnbDB.setFieldData( "AccountNumber2", newValue.toString() );
    }

    public String getAccountNumber2()
    {
        String           sValue = dbnbDB.getFieldData( "AccountNumber2" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setAccountType(String newValue)
    {
                dbnbDB.setFieldData( "AccountType", newValue.toString() );
    }

    public String getAccountType()
    {
        String           sValue = dbnbDB.getFieldData( "AccountType" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setAccountExpMonth(String newValue)
    {
                dbnbDB.setFieldData( "AccountExpMonth", newValue.toString() );
    }

    public String getAccountExpMonth()
    {
        String           sValue = dbnbDB.getFieldData( "AccountExpMonth" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setAccountExpYear(String newValue)
    {
                dbnbDB.setFieldData( "AccountExpYear", newValue.toString() );
    }

    public String getAccountExpYear()
    {
        String           sValue = dbnbDB.getFieldData( "AccountExpYear" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setAccountChargeDate(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "AccountChargeDate", formatter.format( newValue ) );
    }

    public Date getAccountChargeDate()
    {
        String           sValue = dbnbDB.getFieldData( "AccountChargeDate" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setAccountPostDate(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "AccountPostDate", formatter.format( newValue ) );
    }

    public Date getAccountPostDate()
    {
        String           sValue = dbnbDB.getFieldData( "AccountPostDate" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setAlertEmail(String newValue)
    {
                dbnbDB.setFieldData( "AlertEmail", newValue.toString() );
    }

    public String getAlertEmail()
    {
        String           sValue = dbnbDB.getFieldData( "AlertEmail" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setAlertDays(Integer newValue)
    {
                dbnbDB.setFieldData( "AlertDays", newValue.toString() );
    }

    public Integer getAlertDays()
    {
        String           sValue = dbnbDB.getFieldData( "AlertDays" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setBillingComments(String newValue)
    {
                dbnbDB.setFieldData( "BillingComments", newValue.toString() );
    }

    public String getBillingComments()
    {
        String           sValue = dbnbDB.getFieldData( "BillingComments" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setDebitAmount(Double newValue)
    {
                java.text.DecimalFormat defDecFormat2 = new java.text.DecimalFormat(PLCUtils.String_defDecFormat2);
                dbnbDB.setFieldData( "DebitAmount", defDecFormat2.format(newValue) );
    }

    public Double getDebitAmount()
    {
        String           sValue = dbnbDB.getFieldData( "DebitAmount" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDoubleValue : sValue;
        return new Double( sValue  ); 
    }

    public void setComments(String newValue)
    {
                dbnbDB.setFieldData( "Comments", newValue.toString() );
    }

    public String getComments()
    {
        String           sValue = dbnbDB.getFieldData( "Comments" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setPromoCode(String newValue)
    {
                dbnbDB.setFieldData( "PromoCode", newValue.toString() );
    }

    public String getPromoCode()
    {
        String           sValue = dbnbDB.getFieldData( "PromoCode" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

}    // End of bltBillingAccount class definition
