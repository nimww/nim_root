package com.winstaff; 
//import com.sun.deploy.net.proxy.StaticProxyManager;
//import com.sun.org.apache.xalan.internal.xsltc.runtime.Parameter;

//import javax.mail.SendFailedException;
import java.sql.SQLException;
import java.util.*;

import JFlex.Out;

public class NIMUtils extends java.lang.Object
{

	public NIMUtils()
	{
	}

    public static String CreateTicketNumber (String prefix, Integer IDCode, int iLength)
    {
        return (prefix + IDCode + com.winstaff.password.RandomString.generateString(2).toUpperCase() + Math.round(Math.random()*100)+com.winstaff.password.RandomString.generateString(4).toUpperCase());
    }


	public static String getPassNumber (Integer iID, String sPrefix)
	{
		return (sPrefix + iID + com.winstaff.password.RandomString.generateString(2).toUpperCase() + Math.round(Math.random()*100)+com.winstaff.password.RandomString.generateString(2).toUpperCase());
	}

	public static boolean isValidEmail (String myEmail)
	{
		java.util.regex.Pattern p = java.util.regex.Pattern.compile(".+@.+\\.[a-z]+");
		java.util.regex.Matcher m = p.matcher(myEmail);
		boolean matchFound = m.matches();
		return matchFound;
	}

    public static boolean isValidPhone (String myPhone)
    {
        //java.util.regex.Pattern p = java.util.regex.Pattern.compile(".+@.+\\.[a-z]+");
        //java.util.regex.Matcher m = p.matcher(myPhone);
        //boolean matchFound = m.matches();
        //return matchFound;
        if (myPhone.length()>=10)
        {
            return true;
        }
        else
        {
            return false;
        }

    }

    public static bltNIM3_Document getRxForICID(Integer ICID, Integer RxFileID)
    {
        bltNIM3_Document myVal = null;
        searchDB2 mySS = new searchDB2();
        java.sql.ResultSet myRS = null;
        Integer doubleCheck = null;
        boolean isPass = false;
        String mySQL = "SELECT \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Referral_RxFileID\" FROM \"public\".\"vMQ4_Encounter_NoVoid_BP\" where \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"PracticeID\" = ? and \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Referral_RxFileID\" = ?";
        try
        {

            db_PreparedStatementObject myDPSO = new db_PreparedStatementObject(mySQL);
            myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.INT_TYPE,ICID.toString()));
            myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.INT_TYPE,RxFileID.toString()));
            myRS = mySS.executePreparedStatement(myDPSO);
            if (myRS!=null&&myRS.next())
            {
                doubleCheck = new Integer(myRS.getString("referral_rxfileid"));
                if (doubleCheck.intValue()==RxFileID.intValue())
                {
                    isPass=true;
                }
            }
            mySS.closeAll();
        }
        catch(Exception e)
        {
            DebugLogger.println("NIMUtils:bltNIM3_Document:[MYSQL="+mySQL+"]:"+e);
        } finally {
            mySS.closeAll();
        }
        if (isPass)
        {
            myVal = new bltNIM3_Document(RxFileID);
        }


        return myVal;
    }

    public static boolean isICIDAuthorizedToUploadReport(Integer ICID, Integer EncounterID)
    {
        return  isICIDAuthorizedToUploadReport(ICID,EncounterID,"R");
    }

    public static boolean isICIDAuthorizedToUploadReport(Integer ICID, Integer EncounterID, String uploadType)
    {
        boolean myVal = false;
        searchDB2 mySS = new searchDB2();
        java.sql.ResultSet myRS = null;
        String mySQL = "SELECT \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"EncounterID\" FROM \"public\".\"vMQ4_Encounter_NoVoid_BP\" where \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"PracticeID\" = ? AND \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"EncounterID\" = ?";
        DebugLogger.println("NIMUtils:isICIDAuthorizedToUploadReport:[MYSQL="+mySQL+"]:");
        try
        {
            db_PreparedStatementObject myDPSO = new db_PreparedStatementObject(mySQL);
            myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.INT_TYPE,ICID.toString()));
            myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.INT_TYPE,EncounterID.toString()));
            myRS = mySS.executePreparedStatement(myDPSO);
            if (myRS!=null&&myRS.next())
            {
                bltNIM3_Encounter myE = new bltNIM3_Encounter(EncounterID);
                if (uploadType.equalsIgnoreCase("R")&&myE.getReportFileID()==0)
                {
                    myVal=true;
                }
                else if (uploadType.equalsIgnoreCase("D"))
                {
                    myVal=true;
                }
            }
            mySS.closeAll();
        }
        catch(Exception e)
        {
            DebugLogger.println("NIMUtils:isICIDAuthorizedToUploadReport:[MYSQL="+mySQL+"]:"+e);
        } finally {
            mySS.closeAll();
        }
        return myVal;
    }



    public static boolean isAuthorizedCreateServiceBillingTransaction(Integer EncounterID,Integer ServiceID)
    {
        boolean myVal = false;
        searchDB2 mySS = new searchDB2();
        java.sql.ResultSet myRS = null;
        String mySQL = "SELECT \"public\".\"vMQ4_Services_NoVoid_BP\".\"ServiceID\" FROM \"public\".\"vMQ4_Services_NoVoid_BP\" where \"public\".\"vMQ4_Services_NoVoid_BP\".\"EncounterID\" = ? and \"public\".\"vMQ4_Services_NoVoid_BP\".\"ServiceID\"= ? ";
        try
        {
            db_PreparedStatementObject myDPSO = new db_PreparedStatementObject(mySQL);
            myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.INT_TYPE,EncounterID.toString()));
            myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.INT_TYPE,ServiceID.toString()));
            myRS = mySS.executePreparedStatement(myDPSO);
            if (myRS!=null&&myRS.next())
            {
                myVal=true;
            }
            mySS.closeAll();
        }
        catch(Exception e)
        {
            DebugLogger.println("NIMUtils:isAuthorizedCreateServiceBillingTransaction:[MYSQL="+mySQL+"]:"+e);
        } finally {
            mySS.closeAll();
        }
        return myVal;
    }






    public static void processAllEncounters(Integer iReferralID, bltUserAccount CurrentUserAccount)
    {
        bltNIM3_Encounter_List        bltNIM3_Encounter_List        =    new    bltNIM3_Encounter_List(iReferralID);
        //declaration of Enumeration
        bltNIM3_Encounter        working_bltNIM3_Encounter;
        ListElement         leCurrentElement;
        Enumeration eList = bltNIM3_Encounter_List.elements();
        if (eList.hasMoreElements())
        {
             while (eList.hasMoreElements())
             {
                 leCurrentElement    = (ListElement) eList.nextElement();
                 working_bltNIM3_Encounter  = (bltNIM3_Encounter) leCurrentElement.getObject();
                 NIM3_EncounterObject myEO = new NIM3_EncounterObject(working_bltNIM3_Encounter.getEncounterID());
                 myEO.ProcessEncounter(CurrentUserAccount);
             }
        }
    }
                              

//Note: new CPT codes must be added here as needed
	public static String getRadiologyGroup(String sCPT)
	{
		String myVal = "";
		
		searchDB2 mySS = new searchDB2();
        java.sql.ResultSet myRS = null;
        String mySQL = "select \"CPTGroup\" from \"CPTGroup\" where \"CPT\" = '"+sCPT+"'";
        try
        {
            db_PreparedStatementObject myDPSO = new db_PreparedStatementObject(mySQL);
            myRS = mySS.executePreparedStatement(myDPSO);
            if (myRS!=null&&myRS.next())
            {
                myVal=myRS.getString("CPTGroup");
            }
            mySS.closeAll();
        }
        catch(Exception e)
        {
            DebugLogger.println("NIMUtils:getRadiologyGroup:[MYSQL="+mySQL+"]:"+e);
        } finally {
            mySS.closeAll();
        }
		
		
		
		
		
		
		/*
		//ct wo
		if (sCPT.equals("74176")||sCPT.equals("74176")||sCPT.equals("70450")||sCPT.equals("72125")||sCPT.equals("71250")||sCPT.equals("70480")||sCPT.equals("70490")||sCPT.equals("73700")||sCPT.equals("73700")||sCPT.equals("73700")||sCPT.equals("73700")||sCPT.equals("73700")||sCPT.equals("73700")||sCPT.equals("73700")||sCPT.equals("73700")||sCPT.equals("73700")||sCPT.equals("73700")||sCPT.equals("73700")||sCPT.equals("73700")||sCPT.equals("72131")||sCPT.equals("70486")||sCPT.equals("72128")||sCPT.equals("73200")||sCPT.equals("73200")||sCPT.equals("73200")||sCPT.equals("73200")||sCPT.equals("73200")||sCPT.equals("73200"))
		{
			myVal = "ct_wo";
		}
		//ct w
		else if (sCPT.equals("74177")||sCPT.equals("74178")||sCPT.equals("74160")||sCPT.equals("70460")||sCPT.equals("72126")||sCPT.equals("71260")||sCPT.equals("70481")||sCPT.equals("70491")||sCPT.equals("73701")||sCPT.equals("73701")||sCPT.equals("73701")||sCPT.equals("73701")||sCPT.equals("73701")||sCPT.equals("73701")||sCPT.equals("73701")||sCPT.equals("73701")||sCPT.equals("73701")||sCPT.equals("73701")||sCPT.equals("73701")||sCPT.equals("73701")||sCPT.equals("72132")||sCPT.equals("70487")||sCPT.equals("72193")||sCPT.equals("72129")||sCPT.equals("73201")||sCPT.equals("73201")||sCPT.equals("73201")||sCPT.equals("73201")||sCPT.equals("73201")||sCPT.equals("73201"))
		{
			myVal = "ct_w";
		}		
		//ct wwo
		else if (sCPT.equals("74178")||sCPT.equals("74170")||sCPT.equals("70470")||sCPT.equals("72127")||sCPT.equals("71270")||sCPT.equals("70482")||sCPT.equals("70492")||sCPT.equals("73702")||sCPT.equals("73702")||sCPT.equals("73702")||sCPT.equals("73702")||sCPT.equals("73702")||sCPT.equals("73702")||sCPT.equals("73702")||sCPT.equals("73702")||sCPT.equals("73702")||sCPT.equals("73702")||sCPT.equals("73702")||sCPT.equals("73702")||sCPT.equals("72133")||sCPT.equals("70488")||sCPT.equals("72194")||sCPT.equals("72130")||sCPT.equals("73202")||sCPT.equals("73202")||sCPT.equals("73202")||sCPT.equals("73202")||sCPT.equals("73202")||sCPT.equals("73202"))
		{
			myVal = "ct_wwo";
		}		
		//MRI wo
		else if (sCPT.equals("74181")||sCPT.equals("70551")||sCPT.equals("77058")||sCPT.equals("77059")||sCPT.equals("72141")||sCPT.equals("71550")||sCPT.equals("73721")||sCPT.equals("73721")||sCPT.equals("73721")||sCPT.equals("73721")||sCPT.equals("73721")||sCPT.equals("73721")||sCPT.equals("73718")||sCPT.equals("73718")||sCPT.equals("73718")||sCPT.equals("73718")||sCPT.equals("73718")||sCPT.equals("73718")||sCPT.equals("72148")||sCPT.equals("70540")||sCPT.equals("72195")||sCPT.equals("72146")||sCPT.equals("70336")||sCPT.equals("73218")||sCPT.equals("73218")||sCPT.equals("73218")||sCPT.equals("73218")||sCPT.equals("73218")||sCPT.equals("73218")||sCPT.equals("73221")||sCPT.equals("73221")||sCPT.equals("73221")||sCPT.equals("73221")||sCPT.equals("73221")||sCPT.equals("73221"))
		{
			myVal = "mr_wo";
		}
		//MRI w
		else if (sCPT.equals("74182")||sCPT.equals("70552")||sCPT.equals("72142")||sCPT.equals("71551")||sCPT.equals("73722")||sCPT.equals("73722")||sCPT.equals("73722")||sCPT.equals("73722")||sCPT.equals("73722")||sCPT.equals("73722")||sCPT.equals("73719")||sCPT.equals("73719")||sCPT.equals("73719")||sCPT.equals("73719")||sCPT.equals("73719")||sCPT.equals("73719")||sCPT.equals("72149")||sCPT.equals("70542")||sCPT.equals("72196")||sCPT.equals("72147")||sCPT.equals("73219")||sCPT.equals("73219")||sCPT.equals("73219")||sCPT.equals("73219")||sCPT.equals("73219")||sCPT.equals("73219")||sCPT.equals("73222")||sCPT.equals("73222")||sCPT.equals("73222")||sCPT.equals("73222")||sCPT.equals("73222")||sCPT.equals("73222") )
		{
			myVal = "mr_w";
		}		
		//MRI wwo
        else if (sCPT.equals("74183")||sCPT.equals("70553")||sCPT.equals("72156")||sCPT.equals("71552")||sCPT.equals("73723")||sCPT.equals("73723")||sCPT.equals("73723")||sCPT.equals("73723")||sCPT.equals("73723")||sCPT.equals("73723")||sCPT.equals("73720")||sCPT.equals("73720")||sCPT.equals("73720")||sCPT.equals("73720")||sCPT.equals("73720")||sCPT.equals("73720")||sCPT.equals("72158")||sCPT.equals("70543")||sCPT.equals("72197")||sCPT.equals("72157")||sCPT.equals("73220")||sCPT.equals("73220")||sCPT.equals("73220")||sCPT.equals("73220")||sCPT.equals("73220")||sCPT.equals("73220")||sCPT.equals("73223")||sCPT.equals("73223")||sCPT.equals("73223")||sCPT.equals("73223")||sCPT.equals("73223")||sCPT.equals("73223"))
        {
            myVal = "mr_wwo";
        }
        else if (sCPT.equals("95900")||sCPT.equals("95903")||sCPT.equals("95904")||sCPT.equals("95934")||sCPT.equals("95936")||sCPT.equals("95937")||sCPT.equals("95860")||sCPT.equals("95861")||sCPT.equals("95863")||sCPT.equals("95864")||sCPT.equals("95867")||sCPT.equals("95868")||sCPT.equals("95869")||sCPT.equals("95999"))
        {
            myVal = "emg";
        }
        //xray

		 */






		return myVal;
	}

    //uses CPTWizard DB to look up Study Type 2-Digit Code
    public static String getDiagnosticStudy(String sCPT)
    {
        String myVal = "";
        return myVal;
    }


    public static String getBillingExport_PaymentsDue()
    {
        return     getBillingExport_PaymentsDue(false, null, null);
    }

    public static String getBillingExport_PaymentsDue(boolean includeAll, java.util.Date FromDate, java.util.Date toDate)
    {
        String myVal ="ERROR NOT IN USE";
           return myVal;
    }

    //scans a CPT Group Obj for any code that matches and returns the int of the one that ranks highest
    public static int getDiagnosticStudyType_NIMINT(com.winstaff.CPTGroupObject allCodes)
    {
        //Rank
        //EMG, MR, CT, Other

        int myVal = 0;
        if (!allCodes.isEmpty())
        {

            searchDB2 mySS = new searchDB2();
            java.sql.ResultSet myRS = null;
            try
            {
                //sql injection prepared statement fix - 2011
                String mySQL = "Select modality from tcptwizard where cpt1 in (?";
                for (int iSQL=1;iSQL<allCodes.getCPTList().size();iSQL++)
                {
                    mySQL+=",?";
                }
                mySQL +=")";
                db_PreparedStatementObject myDPSO = new db_PreparedStatementObject(mySQL);
                for (int iSQL=0;iSQL<allCodes.getCPTList().size();iSQL++)
                {
                    myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,(String)allCodes.getCPTList().elementAt(iSQL)));
                }
                myRS = mySS.executePreparedStatement(myDPSO);
                //myRS = mySS.executeStatement("select modality from tcptwizard where cpt1 in (" + allCodes.getSQLDB_ArrayList().replace('\\','|') + ")");

                //DebugLogger.printLine("^^^^^^^^^^^^^^^^^^^^^^^["+"select modality from tcptwizard where cpt1 in (" + allCodes.getSQLDB_ArrayList() + ")]");
                while(myRS!=null&&myRS.next())
                {
                    String myRG = myRS.getString("modality");
                    if (myRG==null||myRG.isEmpty()||myRG.length()<2)
                    {
                    }                   
                    else if (myRG.substring(0,2).equalsIgnoreCase("mr")&&myVal!=NIMUtils.ENCOUNTER_TYPE_EMG)
                    {
                        myVal = NIMUtils.ENCOUNTER_TYPE_MR;
                    }
                    else if (myRG.substring(0,2).equalsIgnoreCase("ct")&&myVal!=NIMUtils.ENCOUNTER_TYPE_MR&&myVal!=NIMUtils.ENCOUNTER_TYPE_EMG)
                    {
                        myVal = NIMUtils.ENCOUNTER_TYPE_CT;
                    }
                    else if (myRG.length()>2&&myRG.substring(0,3).equalsIgnoreCase("emg"))
                    {
                        myVal = NIMUtils.ENCOUNTER_TYPE_EMG;
                    }
                }
                mySS.closeAll();
            }
            catch (Exception eeee)
            {
                DebugLogger.printLine("NIMUtils:getDiagnosticStudyType_NIMINT:["+eeee+"]");
            } finally {
                mySS.closeAll();
            }
        }
        return myVal;


    }




    public static com.winstaff.CPTGroupObject getDominantCodes(com.winstaff.CPTGroupObject allCodes)
    {
        com.winstaff.CPTGroupObject DomCodes = new com.winstaff.CPTGroupObject();
        for (int i=0;i<allCodes.getCPTList().size();i++)
        {
            String myCode = (String) allCodes.getCPTList().elementAt(i);
            String myRG = getRadiologyGroup(myCode);
            if (myRG==null||myRG.isEmpty()||myRG.length()<2)
            {
            }
            else if (myRG.substring(0,2).equalsIgnoreCase("mr"))
            {
                DomCodes.addToCPTList(myCode);
            }
            else if (myRG.substring(0,2).equalsIgnoreCase("ct"))
            {
                DomCodes.addToCPTList(myCode);
            }

        }
        if (DomCodes.getCPTList().size()==0)
        {
            DomCodes.addToCPTList(allCodes.getFirstCPT());
        }
        return DomCodes;
    }


    public static com.winstaff.CPTGroupObject2 getDominantCode(com.winstaff.CPTGroupObject2 allCodes)
    {
        com.winstaff.CPTGroupObject2 DomCodes = new com.winstaff.CPTGroupObject2();
        boolean foundMRE = false;
        boolean foundCT = false;

        for (int i=0;i<allCodes.getCPTList().size();i++)
        {
            CPTObject myCPTO = allCodes.getCPTList().elementAt(i);
            String myCode = myCPTO.getCPT_Code();
            String myRG = getRadiologyGroup(myCode);
            if (myRG==null||myRG.isEmpty()||myRG.length()<2)
            {
            }
            else if (myRG.substring(0,2).equalsIgnoreCase("mr"))
            {
                if (foundCT)
                {
                    DomCodes = new com.winstaff.CPTGroupObject2();
                }
                if (!foundMRE)
                {
                    DomCodes.addToCPTList(myCPTO);
                    foundMRE = true;
                }
            }
            else if (myRG.substring(0,2).equalsIgnoreCase("ct"))
            {
                if (!foundMRE)
                {
                    DomCodes.addToCPTList(myCPTO);
                    foundCT = true;
                }
            }
            else if (myRG.substring(0,2).equalsIgnoreCase("em"))
            {
                if (foundCT)
                {
                    DomCodes = new com.winstaff.CPTGroupObject2();
                }
                if (!foundMRE)
                {
                    DomCodes.addToCPTList(myCPTO);
                    foundMRE = true;
                }
            }

        }
        if (DomCodes.getCPTList().size()==0)
        {
            DomCodes.addToCPTList(new CPTObject(allCodes.getFirstCPT().getCPT_Code(),"",1));
        }
        return DomCodes;
    }


    public static com.winstaff.CPTGroupObject2 getDominantCodes(com.winstaff.CPTGroupObject2 allCodes)
    {
        com.winstaff.CPTGroupObject2 DomCodes = new com.winstaff.CPTGroupObject2();
        for (int i=0;i<allCodes.getCPTList().size();i++)
        {
            CPTObject myCPTO = allCodes.getCPTList().elementAt(i);
            String myCode = myCPTO.getCPT_Code();
            String myRG = getRadiologyGroup(myCode);
            if (myRG==null||myRG.isEmpty()||myRG.length()<2)
            {
            }
            else if (myRG.substring(0,2).equalsIgnoreCase("mr"))
            {
                DomCodes.addToCPTList(myCPTO);
            }
            else if (myRG.substring(0,2).equalsIgnoreCase("ct"))
            {
                DomCodes.addToCPTList(myCPTO);
            }
            else if (myRG.substring(0,2).equalsIgnoreCase("em"))
            {
                DomCodes.addToCPTList(myCPTO);
            }

        }
        if (DomCodes.getCPTList().size()==0)
        {
            DomCodes.addToCPTList(new CPTObject(allCodes.getFirstCPT().getCPT_Code(),"",1));
        }
        return DomCodes;
    }


    public static com.winstaff.CPTGroupObject getCPTGroupObject(Integer iEncounterID)
    {
        com.winstaff.CPTGroupObject DomCodes = new com.winstaff.CPTGroupObject();
        bltNIM3_Service_List        bltNIM3_Service_List        =    new    bltNIM3_Service_List(iEncounterID);
//declaration of Enumeration
        bltNIM3_Service        working_bltNIM3_Service;
        ListElement         leCurrentElement;
        Enumeration eList = bltNIM3_Service_List.elements();
        while (eList.hasMoreElements())
        {
            leCurrentElement    = (ListElement) eList.nextElement();
            working_bltNIM3_Service  = (bltNIM3_Service) leCurrentElement.getObject();
            if (working_bltNIM3_Service.getServiceStatusID()==1)
            {
                DomCodes.addToCPTList(working_bltNIM3_Service.getCPT());
            }
        }
        return DomCodes;
    }

    public static com.winstaff.CPTGroupObject2 getCPTGroupObject2(Integer iEncounterID)
    {
        com.winstaff.CPTGroupObject2 DomCodes = new com.winstaff.CPTGroupObject2();
        bltNIM3_Service_List        bltNIM3_Service_List        =    new    bltNIM3_Service_List(iEncounterID);
//declaration of Enumeration
        bltNIM3_Service        working_bltNIM3_Service;
        ListElement         leCurrentElement;
        Enumeration eList = bltNIM3_Service_List.elements();
        while (eList.hasMoreElements())
        {
            leCurrentElement    = (ListElement) eList.nextElement();
            working_bltNIM3_Service  = (bltNIM3_Service) leCurrentElement.getObject();
            if (working_bltNIM3_Service.getServiceStatusID()==1)
            {
                DomCodes.addToCPTList(new CPTObject(working_bltNIM3_Service.getCPT(),working_bltNIM3_Service.getCPTModifier(),working_bltNIM3_Service.getCPTQty()));
            }
        }
        return DomCodes;
    }


    public static Double getPriceForCPTByPractice_dep(com.winstaff.bltPracticeMaster myPracM, String sCPT)
    {
        //uses new methods now, but still deprecated
        CPTObject myCPTO = new CPTObject(sCPT, "", 1);
        return getCPTPayPriceforPracticeMaster(myPracM, myCPTO, PLCUtils.getToday());
    }

//    public static Double getPriceForCPTByPractice_dep(com.winstaff.bltPracticeMaster myPracM, String sCPT)
//    {
          //deprecated
//        Double myVal = new Double(0);
//        String myRGroup = NIMUtils.getRadiologyGroup(sCPT);
//        if (myRGroup.equalsIgnoreCase("mr_w"))
//        {
//             myVal = myPracM.getPrice_Mod_MRI_W();
//        }
//        else if (myRGroup.equalsIgnoreCase("mr_wo"))
//        {
//             myVal = myPracM.getPrice_Mod_MRI_WO();
//        }
//        else if (myRGroup.equalsIgnoreCase("mr_wwo"))
//        {
//             myVal = myPracM.getPrice_Mod_MRI_WWO();
//        }
//        else if (myRGroup.equalsIgnoreCase("ct_w"))
//        {
//             myVal = myPracM.getPrice_Mod_CT_W();
//        }
//        else if (myRGroup.equalsIgnoreCase("ct_wo"))
//        {
//             myVal = myPracM.getPrice_Mod_CT_WO();
//        }
//        else if (myRGroup.equalsIgnoreCase("ct_wwo"))
//        {
//             myVal = myPracM.getPrice_Mod_CT_WWO();
//        }
//        return myVal;
//    }


    public static com.winstaff.CPTGroupObject getDominantCode(com.winstaff.CPTGroupObject allCodes)
    {
        com.winstaff.CPTGroupObject DomCodes = new com.winstaff.CPTGroupObject();
        for (int i=0;i<allCodes.getCPTList().size();i++)
        {
            if (DomCodes.getCPTList().isEmpty())
            {
                String myCode = (String) allCodes.getCPTList().elementAt(i);
                String myRG = getRadiologyGroup(myCode);
                if (myRG==null||myRG.isEmpty()||myRG.length()<2)
                {
                }
                else if (myRG.substring(0,2).equalsIgnoreCase("mr"))
                {
                    DomCodes.addToCPTList(myCode);
                }
                else if (myRG.substring(0,2).equalsIgnoreCase("ct"))
                {
                    DomCodes.addToCPTList(myCode);
                }
            }
            else
            {
                //all done for now
            }


        }

        if (DomCodes.getCPTList().isEmpty())
        {
            DomCodes.addToCPTList(allCodes.getFirstCPT());
        }

        return DomCodes;
    }


    public static String getMAC_Modality(com.winstaff.CPTGroupObject allCodes)
    {
        String myVal = "";
        //  mr = mri
        //  mr_md = mri with med
        //  ct = ct
        //  ct_md = ct with med
        //  emg
        //  us
        //  fl

        for (int i=0;i<allCodes.getCPTList().size();i++)
        {
            String myCode = (String) allCodes.getCPTList().elementAt(i);
            String myRG = getRadiologyGroup(myCode);
            if (myRG==null||myRG.isEmpty()||myRG.length()<2)
            {
            }
            else if (myRG.substring(0,2).equalsIgnoreCase("mr"))
            {
                myVal = NIMUtils.MAC_MOD_MR;
            }
            else if (myRG.substring(0,2).equalsIgnoreCase("ct"))
            {
                myVal = NIMUtils.MAC_MOD_CT;
            }

        }
        return myVal;
    }
    

    public static String MAC_MOD_MR = "mr";
    public static String MAC_MOD_MR_MD = "mr_md";
    public static String MAC_MOD_CT = "ct";
    public static String MAC_MOD_CT_MD = "ct_md";
    public static String MAC_MOD_US = "us";
    public static String MAC_MOD_EMG = "emg";
    public static String MAC_MOD_FL = "fl";
    public static String MAC_MOD_XR = "xr";

/*
    public static java.util.Vector getHashOfCenters(String sZIP, int iMiles, CPTGroupObject2 myCGO2, ContactAddressObject CAO_Compare)
    {
        return getListOfCenters(sZIP, iMiles, myCGO2, CAO_Compare, new Integer(0), new Double(10000.00));
    }
*/
    public static String getPracticeActiveStatus_SQLIN_And_OTA()
    {
        return "1, 2, 8, 9, 10, 11";
    }
    public static String getPracticeActiveStatus_SQLIN()
    {
//        return "1, 2, 8, 9, 5, 10, 11";
        //only Contracted and ota   (on 04-15-15 gio added 1 for ota)
        return "1, 2, 8, 9, 10, 11";
    }
    public static ImagingCenterListObject getListOfCenters(String sZIP, int iMiles, CPTGroupObject2 myCGO2_in, ContactAddressObject CAO_Compare, Integer iEncounter_Reference, Double MaxPrice, boolean isNetDev, Double MaxMACPrice, boolean priceOnlyMAC){
    	return getListOfCenters( sZIP,  iMiles,  myCGO2_in,  CAO_Compare,  iEncounter_Reference,  MaxPrice,  isNetDev,  MaxMACPrice,  priceOnlyMAC, false);
    }
    
    public static ImagingCenterListObject getListOfCenters(String sZIP, int iMiles, CPTGroupObject2 myCGO2_in, ContactAddressObject CAO_Compare, Integer iEncounter_Reference, Double MaxPrice, boolean isNetDev, Double MaxMACPrice, boolean priceOnlyMAC, boolean isPreSchedule )
    {
    	return getListOfCenters(sZIP, iMiles, myCGO2_in, CAO_Compare, iEncounter_Reference, MaxPrice, isNetDev, MaxMACPrice, priceOnlyMAC, isPreSchedule, null);
    }
    /**
     * 
     * @param sZIP center of search area
     * @param iMiles radius of search
     * @param myCGO2_in Vector of CPTObject. [{cpt,mod,qty}]
     * @param CAO_Compare Address object [{address,city, state, zip}]
     * @param iEncounter_Reference encouter id
     * @param MaxPrice total allows for cpts
     * @param isNetDev trigger for netdev. displays all providers 
     * @param MaxMACPrice same as MaxPrice unless null. then its 800, 9000 for NID.
     * @param priceOnlyMAC mods MAC score with default params
     * @param isPreSchedule includes OTA centers. 
     * @return ImagingCenterListObject: listOfCenters[{PracticeMaster_SearchResultsObject}] vector & searchNote[{string}] vector
     */
    public static ImagingCenterListObject getListOfCenters(String sZIP, int iMiles, CPTGroupObject2 myCGO2_in, ContactAddressObject CAO_Compare, Integer iEncounter_Reference, Double MaxPrice, boolean isNetDev, Double MaxMACPrice, boolean priceOnlyMAC, boolean isPreSchedule, bltNIM3_PayerMaster pm )
    {
        Vector<String> searchNotes = new Vector<String>();
        sZIP = sZIP.trim();
        CPTGroupObject2 myCGO2_dom = NIMUtils.getDominantCode(myCGO2_in);
        CPTGroupObject2 myCGO2 = myCGO2_in;
        if ( MaxPrice==null||MaxPrice<0.1)
        {
            return null;
        }
        else
        {
            java.util.Hashtable<Double,PracticeMaster_SearchResultsObject> myZIPHash = new java.util.Hashtable();
            ZIP_CODE_object myZCo = NIMUtils.get_zips_in_range(sZIP,iMiles);
            java.util.Vector myV = myZCo.getZipsInRange(iMiles);
            int i2cnt=0;
            String myZip = "";
            String myZipSQL = "";
            for (int i10=0;i10<myV.size();i10++)
            {
                if (i10>0)
                {
                    myZip += ", ";
                    myZipSQL += ",";
                }
                myZip += "'" + (String) myV.elementAt(i10) + "'";
                myZipSQL += "?";
    //start big loop
            }
            if (myZip.equalsIgnoreCase(""))
            {
                myZip = "'" + sZIP + "'";
                myZipSQL = "?";
                DebugLogger.println ("NIMUtils:getListOfCenters:Invalid/New ZIP["+sZIP+"]");

            }
    //	searchDB2_Remote mySS = new searchDB2_Remote();
            searchDB2 mySS = new searchDB2();
            java.sql.ResultSet myRS = null;
            try
            {
                //String mySQL = ("SELECT PracticeID from tpracticemaster where substr(officezip,0,6) in (" + myZip + ") and contractingstatusid in (1,2,5) order by case when contractingstatusid=2 then 1 when contractingstatusid=5 then 2 when contractingstatusid=1 then 3 END");
                //String mySQL = ("SELECT PracticeID from tpracticemaster INNER JOIN tNIM3_Modality on tNIM3_Modality.practiceid = tPracticeMaster.practiceid where substr(officezip,0,6) in (" + myZip + ") and contractingstatusid in (2, 8, 9, 5) ");
                String mySQL = null;
                //2011-06-15 updated process
                String addWhere = "";
                if (myCGO2_dom.isHasMRI())
                {
                    addWhere += " AND hasmr is not null ";
                }
                if (myCGO2_dom.isHasCT())
                {
                    addWhere += " AND hasct is not null ";
                }
                if (myCGO2_dom.isHasEMG())
                {
                    addWhere += " AND hasemg is not null ";
                }
                if (myCGO2_dom.isHasXR())
                {
                    addWhere += " AND hasxr is not null ";
                }
                if (myCGO2_dom.isHasUS())
                {
                    addWhere += " AND hasus is not null ";
                }
                if (myCGO2_dom.isHasNM())
                {
                    addWhere += " AND hasnm is not null ";
                }
                if (myCGO2_dom.isHasFL())
                {
                    addWhere += " AND hasfl is not null ";
                }
                if (isNetDev)
                {
                    mySQL = ("SELECT practiceid, hasmr, mr_open_class, mr_open_tesla, mr_trad_class, mr_trad_tesla  from \"vMQ2_Practice_Services\" where substr(officezip,0,6) in (" + myZipSQL + ") " +addWhere);
                }
                else
                {
                    mySQL = ("SELECT practiceid, hasmr, mr_open_class, mr_open_tesla, mr_trad_class, mr_trad_tesla  from \"vMQ2_Practice_Services\" where substr(officezip,0,6) in (" + myZipSQL + ") and contractingstatusid in ("+(isPreSchedule ? NIMUtils.getPracticeActiveStatus_SQLIN_And_OTA():NIMUtils.getPracticeActiveStatus_SQLIN())+") " +addWhere);
                }
                //SQL Prepared Statements Update 2011
                db_PreparedStatementObject myDPSO = new db_PreparedStatementObject(mySQL);
                if (myV.size()<=1)
                {
                    myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,sZIP));
                }
                else
                {
                    for (int i10=0;i10<myV.size();i10++)
                    {
                        myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,(String)myV.elementAt(i10)));
                    }
                }
                myRS = mySS.executePreparedStatement(myDPSO);


                while(myRS!=null&&myRS.next())
                {
                    i2cnt++;
                    PracticeMaster_SearchResultsObject myPracM_SRO = new PracticeMaster_SearchResultsObject(new  Integer(myRS.getString("practiceID")));
                    if (myRS.getString("mr_open_class")!=null){
                        int tempMR_OpenClass = new Integer(myRS.getString("mr_open_class"));
                        if (tempMR_OpenClass>0){
                            myPracM_SRO.setOpenMR(true);
                            myPracM_SRO.setOpenMR_Class(tempMR_OpenClass);
                        } else {
                            myPracM_SRO.setOpenMR(false);
                        }
                    } else {
                        myPracM_SRO.setOpenMR(false);
                    }
                    if (myRS.getString("mr_trad_class")!=null){
                        int tempMR_TradClass = new Integer(myRS.getString("mr_trad_class"));
                        if (tempMR_TradClass>0){
                            myPracM_SRO.setTradMR(true);
                            myPracM_SRO.setTradMR_Class(tempMR_TradClass);
                        } else {
                            myPracM_SRO.setTradMR(false);
                        }
                    } else {
                        myPracM_SRO.setTradMR(false);
                    }
                    com.winstaff.GEOUtils.Location myGUL_Pat = null;
                    com.winstaff.GEOUtils.Location myGUL_Prac = null;
                    try
                    {
//                        myGUL_Pat = NIMUtils.getLatLon(CAO_Compare.getAddress1() + " " + CAO_Compare.getCity() + " " + CAO_Compare.getState() + " " + CAO_Compare.getZip());
                        myGUL_Pat = NIMUtils.getLatLon(CAO_Compare.getZip());
                    }
                    catch (Exception eeeeeee)
                    {
                        DebugLogger.println ("NIMUtils:getHashOfCenters:Invalid Patient Address.[" + eeeeeee.toString()+"]");
                    }

                    try
                    {
//                        myGUL_Prac = NIMUtils.getLatLon(myPracM.getOfficeAddress1() + " " + myPracM.getOfficeCity() + " " + new bltStateLI(myPracM.getOfficeStateID()).getShortState() + " " + myPracM.getOfficeZIP());
                        myGUL_Prac = NIMUtils.getLatLon(myPracM_SRO.practiceMaster.getOfficeZIP());
                    }
                    catch (Exception eeeeeee)
                    {
                        DebugLogger.println ("NIMUtils:getHashOfCenters:Invalid Center in listing.  Please notify IT and NET DEV[" + eeeeeee.toString()+"]");
                    }

                    if (myGUL_Pat ==null)
                    {
                        DebugLogger.println ("NIMUtils:getHashOfCenters:Invalid Patient Address.");
                    }
                    if (myGUL_Prac ==null)
                    {
                        DebugLogger.println ("NIMUtils:getHashOfCenters:Invalid Center Address.");
                    }

                    try
                    {
                        double myNewD = 0;
                        try
                        {
                            myNewD = NIMUtils.calculate_mileage(new Double(myGUL_Pat.lat).doubleValue(),new Double(myGUL_Prac.lat).doubleValue(),new Double(myGUL_Pat.lon).doubleValue(),new Double(myGUL_Prac.lon).doubleValue());
                        }
                        catch (Exception e3ee)
                        {
                            searchNotes.add("NIMUtils:getHashOfCenters:Error in Distance Calc for: "  + myPracM_SRO.practiceMaster.getPracticeName() + "[" + e3ee.toString()+"]");
                        }

                        //DebugLogger.println("<br>["+myNewD + "]");
        //				DebugLogger.println("<br>" + myPracM.getPracticeName() + " - Null Distance   " + myNewD);
//                        PayerExceptionObject payerExceptionObject = new PayerExceptionObject();
                        myPracM_SRO.setDistance(myNewD);  //temp storage of disatnce
                        try
                        {
                            //DebugLogger.printLine("**** PID: " + myPracM_SRO.practiceMaster.getPracticeID() + "**** FSID:" + myPracM_SRO.practiceMaster.getFeePercentage() );
                            Double myPrice = NIMUtils.getCPTGroupPayPriceforPracticeMaster(myPracM_SRO.practiceMaster,myCGO2, PLCUtils.getToday());
                            //DebugLogger.printLine("**** PID: " + myPracM_SRO.practiceMaster.getPracticeID() + "**** FSID:" + myPracM_SRO.practiceMaster.getFeePercentage() + "****** = " + myPrice);
                            Double requiredMargin = 2.0;
                            
                            if(myCGO2_in.isHasCT()||myCGO2_in.isHasMRI()||myCGO2_in.isHasPET()){
                            	requiredMargin = 30.0;
                            }
                            //DebugLogger.printLine(requiredMargin.toString());
                            if (myPrice!=null)
                            {
                                //DebugLogger.e("Pricing", "my-price:" + myPrice + " | MaxPrice: " + MaxPrice);
                            	
                            	Double exceptionPrice = 0d;
//                            	if(pm!=null){
//                            		exceptionPrice = payerExceptionObject.checkExceptionPricing(pm, myPracM_SRO.practiceMaster, myCGO2_in);
//                            	}
                            	System.out.println("EXP PRICE: "+exceptionPrice+" "+(pm!=null?"null":""));
                                if ( (myPrice+requiredMargin)<(exceptionPrice>0 ? exceptionPrice : MaxPrice))
                                {
                                	System.out.println(" (myPrice+requiredMargin)<(exceptionPrice>0 ? exceptionPrice : MaxPrice)");
                                    Double MinPrice = 150.0;
                                    if (myCGO2_dom.isHasEMG()){
                                        MinPrice = 0.01;
                                        MaxMACPrice = (exceptionPrice>0 ? exceptionPrice : MaxPrice);
                                    }
                                    Double myMacScore = NIMUtils.getMACScore(new Double(myNewD), myPrice, myPracM_SRO.practiceMaster, (exceptionPrice>0 ? exceptionPrice : MaxMACPrice), MinPrice, priceOnlyMAC);
                                    //verify that center is not in hash
                                    if (myZIPHash.containsKey(myMacScore))
                                    {
                                        myMacScore = myMacScore+0.000001;
                                        if (myZIPHash.containsKey(myMacScore))
                                        {
                                            myMacScore = myMacScore+0.000001;
                                            if (!myZIPHash.containsKey(myMacScore))
                                            {
                                                myZIPHash.put(myMacScore,myPracM_SRO);
                                            }
                                        }
                                        else
                                        {
                                            myZIPHash.put (myMacScore,myPracM_SRO);
                                        }
                                    }
                                    else
                                    {
                                        myZIPHash.put (myMacScore,myPracM_SRO);
                                    }
                                }
                                else
                                {
                                    searchNotes.add("NIMUtils:getListOfCenters: Practice ["+myPracM_SRO.practiceMaster.getPracticeName()+": " + myPracM_SRO.practiceMaster.getPracticeID() + "] cost [$"+PLCUtils.getDisplayDefaultDecimalFormatCurrency(myPrice)+"] higher than Payer Allow [$"+PLCUtils.getDisplayDefaultDecimalFormatCurrency(MaxPrice)+"] [Distance: " + PLCUtils.getDisplayDefaultDecimalFormat2(myNewD) + " miles] ");
                                }
                            }
                            else
                            {
                                //attempt to use Fee Percentage as the Mac Score
                                if (myCGO2_dom.isHasEMG())
                                {
                                    Double EMG_Mac = NIMUtils.getMACScore(new Double(myNewD),myPracM_SRO.practiceMaster.getFeePercentage(), myPracM_SRO.practiceMaster, 0.0,1.0, priceOnlyMAC);
                                    if (myZIPHash.containsKey(EMG_Mac))
                                    {
                                        EMG_Mac = EMG_Mac+0.000001;
                                        if (myZIPHash.containsKey(EMG_Mac))
                                        {
                                            EMG_Mac = EMG_Mac+0.000001;
                                            if (!myZIPHash.containsKey(EMG_Mac))
                                            {
                                                myZIPHash.put (EMG_Mac,myPracM_SRO);
                                            }
                                        }
                                        else
                                        {
                                            myZIPHash.put (EMG_Mac,myPracM_SRO);
                                        }
                                    }
                                    else
                                    {
                                        myZIPHash.put (EMG_Mac,myPracM_SRO);
                                    }


//                                    myZIPHash.put (-1.0 * Math.random(),myPracM);
                                }
                                else
                                {
                                    searchNotes.add("NIMUtils:getListOfCenters: Practice ["+myPracM_SRO.practiceMaster.getPracticeName()+": " + myPracM_SRO.practiceMaster.getPracticeID() + "] Missing FSID or bad Code [Distance: " + PLCUtils.getDisplayDefaultDecimalFormat2(myNewD) + " miles]");
                                    //DebugLogger.println("NIMUtils:getHashOfCenters: Practice Missing FSID or bad Code [" + myPracM.getPracticeID() + "]");
                                }
                            }
                        }
                        catch (Exception eee)
                        {
//                            myZIPHash.put (new Double(0),myPracM);
                            searchNotes.add("NIMUtils:getHashOfCenters:Can't Hash: " + eee);
                            DebugLogger.println("NIMUtils:getHashOfCenters:Can't Hash: " + eee);
                        }
                    }
                    catch (Exception eeeeeee)
                    {
                        searchNotes.add("NIMUtils:getHashOfCenters:1x Error in Distance Calc for practice: "  + myPracM_SRO.practiceMaster.getPracticeName() + "[" + eeeeeee.toString()+"]");
                        DebugLogger.println ("NIMUtils:getHashOfCenters:1x Error in Distance Calc for practice: "  + myPracM_SRO.practiceMaster.getPracticeName() + "[" + eeeeeee.toString()+"]");
                    }
                }
                mySS.closeAll();
            }
            catch (Exception eee3eeee)
            {
                DebugLogger.println ("NIMUtils:getHashOfCenters:Overall fail[" + eee3eeee.toString()+"]");
            }
            finally {
                mySS.closeAll();
            }


            if (myZIPHash.size()>0)
            {
                Vector<Double> vZIP = null;
                java.util.Iterator itZIP = null;
                vZIP = new java.util.Vector(myZIPHash.keySet());
                java.util.Comparator comparator = java.util.Collections.reverseOrder();
                java.util.Collections.sort(vZIP,comparator);
                return new ImagingCenterListObject(HashToVector(myZIPHash, vZIP, 100),searchNotes);
            }
            else
            {
                return null;
            }
        }
    }

    /**
     * 
     * @param dDistance distance of IC to center point
     * @param dPrice total charge from IC
     * @param myPracM checks for blue star booster
     * @param MaxPrice_Cap total payer allows
     * @param MinPrice_Cap statically set to 150 unless EMG then its 0.01
     * @param priceOnlyMAC price is 100% of score
     * @return
     */
    public static Double getMACScore(Double dDistance, Double dPrice, com.winstaff.bltPracticeMaster myPracM, Double MaxPrice_Cap, Double MinPrice_Cap, boolean priceOnlyMAC)
    {
        Double myVal = new Double(0);
        double MaxDistance = 60;
        double MaxPrice = MaxPrice_Cap;
        double MinPrice = MinPrice_Cap;
        double Weight_Price = 8;
        double Weight_BlueStar = 5;
        double Weight_LocalCongestion = 3;

        //conditions
        if (dPrice<MinPrice&&dPrice>0)
        {
            dPrice = MinPrice;
        }
        if (dPrice>MaxPrice)
        {
            dPrice = MaxPrice;
        }
        if (dPrice<0.01)
        {
            dPrice = MaxPrice;
        }
        if (dPrice<1.0)
        {
            Weight_LocalCongestion = 1;
        }
        if (priceOnlyMAC){
            Weight_Price = 100;
            Weight_BlueStar = 0;
            Weight_LocalCongestion = 0;
        }

        if (dDistance<=10.0){

//            Weight_LocalCongestion = 1;
        }
        //double Weight_Distance = 1;

        //must normalize
        double myDistanceScore = 1-(dDistance.doubleValue()/MaxDistance);

        double myPriceScore = 1-((dPrice.doubleValue()-MinPrice)/(MaxPrice-MinPrice));
        double myBlueStarScore = 0;
        if (myPracM.getIsBlueStar().intValue()==1)
        {
            myBlueStarScore = 1;
        }

        double myScore = (myDistanceScore * Weight_LocalCongestion) + (myPriceScore * Weight_Price) + (myBlueStarScore * Weight_BlueStar);
        double BestScore = (1 * Weight_LocalCongestion) + (1 * Weight_Price) + (1 * Weight_BlueStar);
        double WorstScore = (0 * Weight_LocalCongestion) + (0 * Weight_Price) + (0 * Weight_BlueStar);
        if (false&&dPrice==0)
        {
            myScore = WorstScore;
        }
        double normMyScore = myScore/BestScore;
        myVal = new Double (normMyScore);

        //DebugLogger.printLine("MAC Score: Distance[" + myDistanceScore  + "] Price [" + dPrice.doubleValue() + "] PriceSc [" + myPriceScore + "] Blue [" + myBlueStarScore + "] Final [" + myScore + "] Norm [" + normMyScore + "]");
        return myVal;

    }


    public static java.util.Vector HashToVector(Hashtable myHash, java.util.Vector myVect, int MaxSize)
    {
        java.util.Vector myVal = new java.util.Vector();
        for (int iVect=0;iVect<myVect.size();iVect++)
        {
            if (iVect<MaxSize)
            {
                myVal.add(myHash.get(myVect.elementAt(iVect)));
                //DebugLogger.printLine("Adding:" + iVect);
            }
        }
        return myVal;
    }


    public static java.util.Vector HashMapToVector(HashMap myHash, java.util.Vector myVect, int MaxSize)
    {
        java.util.Vector myVal = new java.util.Vector();
        for (int iVect=0;iVect<myVect.size();iVect++)
        {
            if (iVect<MaxSize)
            {
                myVal.add(myHash.get(myVect.elementAt(iVect)));
                //DebugLogger.printLine("Adding:" + iVect);
            }
        }
        return myVal;
    }



    public static void sendFatalError(String sType, String sClassIdentifier, String sDebugString)
    {
        java.util.Date myD = new java.util.Date();
//        DebugLogger.println( sClassIdentifier + ": " + "[" + myD + "]" );
        emailType_V3 myEM = new emailType_V3();
        myEM.setTo("scott@nextimagemedical.com");
        myEM.setFrom("server@nextimagemedical.com");
        myEM.setSubject("FATAL ERRROR ["+ConfigurationInformation.serverName+"]");
        myEM.setBody("FATAL ERROR\n----------\n"+sType+"\n----------\n" + sClassIdentifier + "\n" + sDebugString  +"\n-----------\n" + "[" + myD + "]" );
        myEM.isSendMail();
    }

    public static String getCPTGroupToCPT(String sCPT)
    {
        String myVal = sCPT;
        if (sCPT.equalsIgnoreCase("mr_wo"))
        {
            myVal = "72148";
        }
        else if (sCPT.equalsIgnoreCase("mr_w"))
        {
            myVal = "72149";
        }
        else if (sCPT.equalsIgnoreCase("mr_wwo"))
        {
            myVal = "72156";
        }
        if (sCPT.equalsIgnoreCase("ct_wo"))
        {
            myVal = "70490";
        }
        else if (sCPT.equalsIgnoreCase("ct_w"))
        {
            myVal = "70491";
        }
        else if (sCPT.equalsIgnoreCase("ct_wwo"))
        {
            myVal = "70492";
        }
        return myVal;
    }





    public static String getPayerContractRateTable(Integer iPayerID, String sDel)
    {
        //scroll through PayerChild options
        String myVal="Rate Table" + sDel;
        bltNIM3_PayerMaster myPM = new bltNIM3_PayerMaster(iPayerID);
        try
        {
            if (myPM.getContract1_FeeScheduleID()!=0)
            {
                myVal += "Contract 1:" + myPM.getContract1_FeeScheduleID() +  " @ " + myPM.getContract1_FeePercentage() + "%";
            }
            if (myPM.getContract2_FeeScheduleID()!=0)
            {
                myVal += "Contract 2:" + myPM.getContract2_FeeScheduleID() +  " @ " + myPM.getContract2_FeePercentage() + "%";
            }
            if (myPM.getContract3_FeeScheduleID()!=0)
            {
                myVal += "Contract 3:" + myPM.getContract3_FeeScheduleID() +  " @ " + myPM.getContract3_FeePercentage() + "%";
            }
            bltNIM3_PayerMaster myPPM = new bltNIM3_PayerMaster(myPM.getParentPayerID());
            if (myPPM.getContract1_FeeScheduleID()!=0)
            {
                myVal += "Parent Contract 1:" + myPPM.getContract1_FeeScheduleID() +  " @ " + myPPM.getContract1_FeePercentage() + "%";
            }
            if (myPPM.getContract2_FeeScheduleID()!=0)
            {
                myVal += "Parent Contract 2:" + myPPM.getContract2_FeeScheduleID() +  " @ " + myPPM.getContract2_FeePercentage() + "%";
            }
            if (myPPM.getContract3_FeeScheduleID()!=0)
            {
                myVal += "Parent Contract 3:" + myPPM.getContract3_FeeScheduleID() +  " @ " + myPPM.getContract3_FeePercentage() + "%";
            }

        }
        catch(Exception e)
        {
            //do nothing
            DebugLogger.printLine("NIMUtils:getPayerRateTable:GeneralError: [" + e + "]");
        }
        return myVal;
    }



    public static Double getPayerAllowAmount(Integer iPayerID,String sZIP, String sCPT, String sMod, java.util.Date EffDate)
    {
    	DebugLogger.printLine("NIMUtils:getPayerAllowAmount: [" + iPayerID + "] "+ "[" + sZIP + "] "+"[" + sCPT + "] "+"[" + sMod + "] "+"[" + EffDate + "] ");
        Double myVal = 0.0;
        sCPT = NIMUtils.getCPTGroupToCPT(sCPT);
        bltNIM3_PayerMaster myPM = new bltNIM3_PayerMaster(iPayerID);
        
        	try
            {
                if (myPM.getContract1_FeeScheduleID()!=0)
                {
                    myVal = NIMUtils.getFeeSchedulePrice(myPM.getContract1_FeeScheduleID(),myPM.getContract1_FeePercentage(),sZIP,sCPT,sMod,EffDate);
                }
                if (myPM.getContract2_FeeScheduleID()!=0&&myVal<=0.1)
                {
                    myVal = NIMUtils.getFeeSchedulePrice(myPM.getContract2_FeeScheduleID(),myPM.getContract2_FeePercentage(),sZIP,sCPT,sMod,EffDate);
                }
                if (myPM.getContract3_FeeScheduleID()!=0&&myVal<=0.1)
                {
                    myVal = NIMUtils.getFeeSchedulePrice(myPM.getContract3_FeeScheduleID(),myPM.getContract3_FeePercentage(),sZIP,sCPT,sMod,EffDate);
                }
                if (myVal==0.0)
                {
                    bltNIM3_PayerMaster myPPM = new bltNIM3_PayerMaster(myPM.getParentPayerID());
                    if (myPPM.getContract1_FeeScheduleID()!=0)
                    {
                        myVal = NIMUtils.getFeeSchedulePrice(myPPM.getContract1_FeeScheduleID(),myPPM.getContract1_FeePercentage(),sZIP,sCPT,sMod,EffDate);
                    }
                    if (myPPM.getContract2_FeeScheduleID()!=0&&myVal<=0.1)
                    {
                        myVal = NIMUtils.getFeeSchedulePrice(myPPM.getContract2_FeeScheduleID(),myPPM.getContract2_FeePercentage(),sZIP,sCPT,sMod,EffDate);
                    }
                    if (myPPM.getContract3_FeeScheduleID()!=0&&myVal<=0.1)
                    {
                        myVal = NIMUtils.getFeeSchedulePrice(myPPM.getContract3_FeeScheduleID(),myPPM.getContract3_FeePercentage(),sZIP,sCPT,sMod,EffDate);
                    }
                }

            }
            catch(Exception e)
            {
                //do nothing
                DebugLogger.printLine("NIMUtils:getPayerAllowAmount:GeneralError: [" + e + "]");
                e.printStackTrace();
            }
        
        return myVal;
    }

    public static Double getPayerAllowAmount(Integer iPayerID,String sZIP, String sCPT, String sMod, java.util.Date EffDate, bltPracticeMaster pm)
    {
        //scroll through PayerChild options
    	DebugLogger.printLine("NIMUtils:getPayerAllowAmount: [" + iPayerID + "] "+ "[" + sZIP + "] "+"[" + sCPT + "] "+"[" + sMod + "] "+"[" + EffDate + "] ");
        Double myVal = 0.0;
        sCPT = NIMUtils.getCPTGroupToCPT(sCPT);
        bltNIM3_PayerMaster myPM = new bltNIM3_PayerMaster(iPayerID);
        
//        if(pm!=null){
//        	PayerExceptionObject PayerExceptionObject = new PayerExceptionObject();
//        	myVal = PayerExceptionObject.checkExceptionPricing(myPM, pm, sCPT);
//        }
        System.out.println("getPayerAllowAmount EX P"+myVal);
        DebugLogger.printLine("getPayerAllowAmount EX P"+myVal);
        	if(myVal<0.01){
        		try
                {
                    if (myPM.getContract1_FeeScheduleID()!=0)
                    {
                        myVal = NIMUtils.getFeeSchedulePrice(myPM.getContract1_FeeScheduleID(),myPM.getContract1_FeePercentage(),sZIP,sCPT,sMod,EffDate);
                    }
                    if (myPM.getContract2_FeeScheduleID()!=0&&myVal<=0.1)
                    {
                        myVal = NIMUtils.getFeeSchedulePrice(myPM.getContract2_FeeScheduleID(),myPM.getContract2_FeePercentage(),sZIP,sCPT,sMod,EffDate);
                    }
                    if (myPM.getContract3_FeeScheduleID()!=0&&myVal<=0.1)
                    {
                        myVal = NIMUtils.getFeeSchedulePrice(myPM.getContract3_FeeScheduleID(),myPM.getContract3_FeePercentage(),sZIP,sCPT,sMod,EffDate);
                    }
                    if (myVal==0.0)
                    {
                        bltNIM3_PayerMaster myPPM = new bltNIM3_PayerMaster(myPM.getParentPayerID());
                        if (myPPM.getContract1_FeeScheduleID()!=0)
                        {
                            myVal = NIMUtils.getFeeSchedulePrice(myPPM.getContract1_FeeScheduleID(),myPPM.getContract1_FeePercentage(),sZIP,sCPT,sMod,EffDate);
                        }
                        if (myPPM.getContract2_FeeScheduleID()!=0&&myVal<=0.1)
                        {
                            myVal = NIMUtils.getFeeSchedulePrice(myPPM.getContract2_FeeScheduleID(),myPPM.getContract2_FeePercentage(),sZIP,sCPT,sMod,EffDate);
                        }
                        if (myPPM.getContract3_FeeScheduleID()!=0&&myVal<=0.1)
                        {
                            myVal = NIMUtils.getFeeSchedulePrice(myPPM.getContract3_FeeScheduleID(),myPPM.getContract3_FeePercentage(),sZIP,sCPT,sMod,EffDate);
                        }
                    }

                }
                catch(Exception e)
                {
                    //do nothing
                    DebugLogger.printLine("NIMUtils:getPayerAllowAmount:GeneralError: [" + e + "]");
                    e.printStackTrace();
                }
        	}
        
        return myVal;
    }


    public static Double getPayerBillAmount(Integer iPayerID,String sZIP, String sCPT, String sMod, java.util.Date EffDate)
    {
        //scroll through PayerChild options
        Double myVal = 0.0;
        sCPT = NIMUtils.getCPTGroupToCPT(sCPT);
        bltNIM3_PayerMaster myPM = new bltNIM3_PayerMaster(iPayerID);
        try
        {
            if (myPM.getBill1_FeeScheduleID()!=0)
            {
                myVal = NIMUtils.getFeeSchedulePrice(myPM.getBill1_FeeScheduleID(),myPM.getBill1_FeePercentage(),sZIP,sCPT,sMod,EffDate);
            }
            if (myPM.getBill2_FeeScheduleID()!=0&&myVal<=0.1)
            {
                myVal = NIMUtils.getFeeSchedulePrice(myPM.getBill2_FeeScheduleID(),myPM.getBill2_FeePercentage(),sZIP,sCPT,sMod,EffDate);
            }
            if (myPM.getBill3_FeeScheduleID()!=0&&myVal<=0.1)
            {
                myVal = NIMUtils.getFeeSchedulePrice(myPM.getBill3_FeeScheduleID(),myPM.getBill3_FeePercentage(),sZIP,sCPT,sMod,EffDate);
            }
            if (myVal==0.0 && myPM.getParentPayerID()>0)
            {
                bltNIM3_PayerMaster myPPM = new bltNIM3_PayerMaster(myPM.getParentPayerID());
                if (myPPM.getBill1_FeeScheduleID()!=0)
                {
                    myVal = NIMUtils.getFeeSchedulePrice(myPPM.getBill1_FeeScheduleID(),myPPM.getBill1_FeePercentage(),sZIP,sCPT,sMod,EffDate);
                }
                if (myPPM.getBill2_FeeScheduleID()!=0&&myVal<=0.1)
                {
                    myVal = NIMUtils.getFeeSchedulePrice(myPPM.getBill2_FeeScheduleID(),myPPM.getBill2_FeePercentage(),sZIP,sCPT,sMod,EffDate);
                }
                if (myPPM.getBill3_FeeScheduleID()!=0&&myVal<=0.1)
                {
                    myVal = NIMUtils.getFeeSchedulePrice(myPPM.getBill3_FeeScheduleID(),myPPM.getBill3_FeePercentage(),sZIP,sCPT,sMod,EffDate);
                }
            }

        }
        catch(Exception e)
        {
            //do nothing
            DebugLogger.printLine("NIMUtils:getPayerAllowAmount:GeneralError: [" + e + "]");
        }
        return myVal;
    }


    public static String getRegionCode(Integer FSID, String sZIP, java.util.Date EffDate) throws java.sql.SQLException
    {
        String sRC = "";
        java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);

        searchDB2 mySS = new searchDB2();
        try {
            java.sql.ResultSet myRS = null;
            String sZIP5 = sZIP;
            if (sZIP.length()>5)
            {
                sZIP5 = sZIP.substring(0,5);
            }
            String mySQL = "not set yet";
            try
            {
                mySQL = "SELECT rc from in_fs_regioncode where zip = ? AND fsid= ? AND effdate < cast(? as Date) order by effdate desc";
                db_PreparedStatementObject myDPSO = new db_PreparedStatementObject(mySQL);
                myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,sZIP5));
                myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,FSID.toString()));
                myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,dbdf.format(EffDate)));
                myRS = mySS.executePreparedStatement(myDPSO);
                //myRS = mySS.executeStatement("select modality from tcptwizard where cpt1 in (" + allCodes.getSQLDB_ArrayList().replace('\\','|') + ")");
                if (myRS!=null&&myRS.next())
                {
                    sRC = myRS.getString("rc");
                }
                mySS.closeAll();
            }
            catch(Exception e)
            {
                DebugLogger.printLine("NIMUtils:getRegionCode:Look Up Region Code:["+e+"]");
                sendFatalError(e.toString(),"NIMUtils:getRegionCode:Look Up Region Code",mySQL);
                throw new SQLException("NIMUtils:getRegionCode:Look Up Region Code:["+e+"]")  ;
            }

            mySS = new searchDB2();
            if (sRC.equalsIgnoreCase(""))  //use generic 50 states
            {
                try
                {
                    mySQL = "SELECT state_prefix from " + DB_TABLE_ZIPCODE + " where zip_code = ?";
                    db_PreparedStatementObject myDPSO = new db_PreparedStatementObject(mySQL);
                    myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,sZIP5));
                    myRS = mySS.executePreparedStatement(myDPSO);
                    //myRS = mySS.executeStatement("select modality from tcptwizard where cpt1 in (" + allCodes.getSQLDB_ArrayList().replace('\\','|') + ")");
                    if (myRS!=null&&myRS.next())
                    {
                        sRC = myRS.getString("state_prefix");
                    }
                    mySS.closeAll();
                }
                catch(Exception e)
                {
                    DebugLogger.printLine("NIMUtils:getRegionCode:Look Up State Code:["+e+"]");
                    sendFatalError(e.toString(),"NIMUtils:getRegionCode:Look Up State Code",mySQL);
                    throw new SQLException("NIMUtils:getRegionCode:Look Up State Code:["+e+"]")  ;
                }
            }
        } catch(Exception e) {
            DebugLogger.printLine("NIMUtils:getRegionCode:Overall/Closing SS:["+e+"]");
        } finally {
            mySS.closeAll();
        }

        return sRC;


    }


    public static Double getFreeSchedulePriceCPTGroup(Integer FSID, Double FSID_Percent, String sZIP, CPTGroupObject2 CPTGO2, java.util.Date EffDate) throws java.sql.SQLException{
        Double myVal_Final = 0.0;
        boolean isFail = false;
        try
        {
            for (int i=0;i<CPTGO2.getCPTList().size();i++)
            {
                Double myVal = new Double(0.0);
                CPTObject myCPTO = CPTGO2.getCPTList().elementAt(i);
                if (myCPTO.getCPT_Code().length()>4)
                {
                    //Making Code DRYer
//                    DebugLogger.d("Group Pricing", "About to Price ["+myCPTO.getCPT_Code()+"]");
//                    myVal = getCPTPayPriceforPracticeMaster(myPracticeMaster, myCPTO, EffDate, paymentType );
                    try {
                        myVal = getFeeSchedulePrice(FSID, FSID_Percent, sZIP, myCPTO.getCPT_Code(), myCPTO.getCPT_Modifier(), EffDate);
                    } catch (SQLException sql){
                        DebugLogger.e("NIMUtils:getFreeSchedulePriceCPTGroup:","SQL Error ["+sql+"]");
                        myVal = 0.0;
                    }

                    if (myVal<=0.1)
                    {
                        isFail = true;
                    }
                    myVal_Final += myVal;
                }
            }
        }
        catch (Exception EEE)
        {
            DebugLogger.printLine("NIMUtils:getFreeSchedulePriceCPTGroup: " + EEE);
        }
        if (isFail||myVal_Final<0.01)
        {
            myVal_Final = null;
        }

        return myVal_Final;

    }
    private static String getFSTable(String zip){
    	searchDB2 conn = new searchDB2();
    	java.sql.ResultSet rs = conn.executeStatement("select lower(shortstate) from tstateli where shortstate = upper((select state_prefix from zip_code where zip_code='"+zip+"'))");
    	String results="";
    	try {
			
			while(rs.next()){
				results = "_"+rs.getString(1);
			}
		} catch (SQLException e) {
		}
    	
    	return results;
    }
    public static Double getFeeSchedulePrice(Integer FSID, Double FSID_Percent, String sZIP, String sCPT, String sMod, java.util.Date EffDate) throws java.sql.SQLException
    {
        //step 1 look up sZIP
        //Alter Modifier   Logic - remove all modifiers that are not TC, 26, or 51.  For 51 make end result 80%
        boolean is80 = false;
        boolean isSkipMod = false;
        if (sMod.equalsIgnoreCase("tc")||sMod.equalsIgnoreCase("26"))
        {
        //do nothing
        }
        else if (sMod.equalsIgnoreCase("51"))
        {
            is80 = true;
            isSkipMod = true;
        }
        else
        {
            isSkipMod = true;
        }
        
        String in_fs_table = getFSTable(sZIP);
        //String in_fs_table = "";
        
        java.text.SimpleDateFormat dbdft = new java.text.SimpleDateFormat(PLCUtils.String_dbdft);
        java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
        Double myVal = 0.0;
        String sRC = "";
        searchDB2 mySS = new searchDB2();
        try {
            java.sql.ResultSet myRS = null;
            String mySQL = "not set yet";
            sRC = getRegionCode(FSID,sZIP,EffDate) ;
            db_PreparedStatementObject myDPSO = null;

            try
            {
                if (isSkipMod)
                {
                    mySQL = "SELECT fee from in_fs_feeschedule"+in_fs_table+" where cpt = ? AND (mod = '' OR mod = ' ' OR mod = '  ' OR mod is null) AND fsid= ? AND rc= ? AND effdate < cast(? as Date) order by effdate desc";
                    myDPSO = new db_PreparedStatementObject(mySQL);
                    myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,sCPT));
    //            myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,sMod));
                    myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,FSID.toString()));
                    myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,sRC));
                    myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,dbdf.format(EffDate)));
                    myRS = mySS.executePreparedStatement(myDPSO);
                    DebugLogger.printLine(new Date()+" || NIMUtils:getFeeSchedulePrice:SQL: "+mySQL+"\nCPT: "+sCPT+"\n "+"\n FSID: "+FSID+"\n "+"\n sRC: "+sRC+"\n "+"\n date: "+dbdf.format(EffDate)+"\n ");
                }
                else
                {
                    mySQL = "SELECT fee from in_fs_feeschedule"+in_fs_table+" where cpt = ? AND mod = ? AND fsid= ? AND rc= ? AND effdate < cast(? as Date)  order by effdate desc";
                    myDPSO = new db_PreparedStatementObject(mySQL);
                    myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,sCPT));
                    myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,sMod));
                    myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,FSID.toString()));
                    myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,sRC));
                    myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,dbdf.format(EffDate)));
                    myRS = mySS.executePreparedStatement(myDPSO);
                    DebugLogger.printLine("NIMUtils:getFeeSchedulePrice:SQL: "+mySQL+"\nCPT: "+sCPT+"\n "+"\n FSID: "+FSID+"\n "+"\n sRC: "+sRC+"\n "+"\n date: "+dbdf.format(EffDate)+"\n ");
                }
                /*
                DebugLogger.printLine("NIMUtils:getFeeSchedulePrice:Look up FS:["+mySQL+"]");
                DebugLogger.printLine("NIMUtils:getFeeSchedulePrice:Look up FS:["+sCPT+"]");
                DebugLogger.printLine("NIMUtils:getFeeSchedulePrice:Look up FS:["+sMod+"]");
                DebugLogger.printLine("NIMUtils:getFeeSchedulePrice:Look up FS:["+FSID+"]");
                DebugLogger.printLine("NIMUtils:getFeeSchedulePrice:Look up FS:["+sRC+"]");
                DebugLogger.printLine("NIMUtils:getFeeSchedulePrice:Look up FS:["+dbdf.format(EffDate)+"]");
                 */

                if (myRS!=null&&myRS.next())
                {
                    myVal = myRS.getDouble("fee");
                    myVal = myVal * FSID_Percent;
                    DebugLogger.printLine("NIMUtils:getFeeSchedulePrice:SQL FEE: "+myVal +"\nfrom db:"+myRS.getString("fee"));
                }
                mySS.closeAll();
            }
            catch(Exception e)
            {
                DebugLogger.printLine("NIMUtils:getFeeSchedulePrice:Look up FS:["+e+"]");
                sendFatalError(e.toString(),"NIMUtils:getFeeSchedulePrice:Look up FS",mySQL);
                throw new SQLException("NIMUtils:getFeeSchedulePrice:Look up FS:["+e+"]")  ;
            }


        } catch(Exception e) {
            DebugLogger.printLine("NIMUtils:getFeeSchedulePrice:Overall/Closing SS:["+e+"]");
        } finally {
            mySS.closeAll();
        }
        if (is80)
        {
            myVal = myVal * 0.80;
        }
        DebugLogger.printLine("NIMUtils:getFeeSchedulePrice:FINAL VALUE:["+myVal+"]");
        return myVal;
    }

	public static Double getCPTPriceforPayer(Integer iPayerID,Integer iPracticeID, String sCPT, Integer CPTQty)
	{
		Double myVal = new Double(0.0);
		bltNIM3_PayerMaster myPayerMaster = new bltNIM3_PayerMaster(iPayerID);
		bltPracticeMaster myPracticeMaster = new bltPracticeMaster(iPracticeID);
        if (sCPT.length()<4)
        {
        }
        else
        {
            String RadGroup = getRadiologyGroup(sCPT);
            //ct wo
            if (RadGroup.equalsIgnoreCase("ct_wo"))
            {
                myVal = myPayerMaster.getPrice_Mod_CT_WO() * CPTQty;
            }
            //ct w
            else if (RadGroup.equalsIgnoreCase("ct_w"))
            {
                myVal = myPayerMaster.getPrice_Mod_CT_W() * CPTQty;
            }
            //ct wwo
            else if (RadGroup.equalsIgnoreCase("ct_wwo"))
            {
                myVal = myPayerMaster.getPrice_Mod_CT_WWO() * CPTQty;
            }
            //MRI wo
            else if (RadGroup.equalsIgnoreCase("mr_wo"))
            {
                myVal = myPayerMaster.getPrice_Mod_MRI_WO() * CPTQty;
            }
            //MRI w
            else if (RadGroup.equalsIgnoreCase("mr_w"))
            {
                myVal = myPayerMaster.getPrice_Mod_MRI_W() * CPTQty;
            }
            //MRI wwo
            else if (RadGroup.equalsIgnoreCase("mr_wwo"))
            {
                myVal = myPayerMaster.getPrice_Mod_MRI_WWO() * CPTQty;
            }
            if (myVal.doubleValue()<=0)
            {
                //run fee schedule ID look up
                //2010-02-23  Base look up on State and State WC Fee Sched
                searchDB2 mySS = new searchDB2();
                java.sql.ResultSet myRS = null;
                try
                {
//                	PayerExceptionObject payerExceptionObject = new PayerExceptionObject();
//                	
//                	myVal = payerExceptionObject.checkExceptionPricing(myPayerMaster, myPracticeMaster, sCPT);
//                	if(myVal<0){
                		//myRS = mySS.executeStatement("SELECT AllowAmount from tNIM3_FeeSchedule where CPT = '" + sCPT.replace('\\','|') + "' AND StateID = " + myPracticeMaster.getOfficeStateID() + " AND FeeScheduleRefID = '1'");
                        String mySQL = "SELECT AllowAmount from tNIM3_FeeSchedule where CPT = ? AND StateID = " + myPracticeMaster.getOfficeStateID() + " AND FeeScheduleRefID = '1'";
                        db_PreparedStatementObject myDPSO = new db_PreparedStatementObject(mySQL);
                        myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,sCPT));
                        myRS = mySS.executePreparedStatement(myDPSO);
                        //myRS = mySS.executeStatement("select modality from tcptwizard where cpt1 in (" + allCodes.getSQLDB_ArrayList().replace('\\','|') + ")");
                        if (myRS!=null&&myRS.next())
                        {
                            myVal = new Double(myRS.getString("AllowAmount")) * myPayerMaster.getFeePercentage() * CPTQty;
                        }
                        mySS.closeAll();
//                	}
                }
                catch(Exception e) {
                    DebugLogger.printLine("NIMUtils:getCPTPriceforPayer:["+e+"]");
                    myVal = new Double(0);
                } finally {
                    mySS.closeAll();
                }
            }
        }
		return myVal;
	}

	public static Double getCPTBillPriceforPayer(Integer iPayerID, Integer iPracticeID, String sCPT, Integer CPTQty)
	{
		Double myVal = new Double(0.0);
		bltNIM3_PayerMaster myPayerMaster = new bltNIM3_PayerMaster(iPayerID);
		bltPracticeMaster myPracticeMaster = new bltPracticeMaster(iPracticeID);
		
		String RadGroup = getRadiologyGroup(sCPT);
		//ct wo
        if (sCPT.length()<4)
        {
        }
        else
        {

            if (RadGroup.equalsIgnoreCase("ct_wo"))
            {
                myVal = myPayerMaster.getBillPrice_Mod_CT_WO() * CPTQty;
            }
            //ct w
            else if (RadGroup.equalsIgnoreCase("ct_w"))
            {
                myVal = myPayerMaster.getBillPrice_Mod_CT_W() * CPTQty;
            }
            //ct wwo
            else if (RadGroup.equalsIgnoreCase("ct_wwo"))
            {
                myVal = myPayerMaster.getBillPrice_Mod_CT_WWO() * CPTQty;
            }
            //MRI wo
            else if (RadGroup.equalsIgnoreCase("mr_wo"))
            {
                myVal = myPayerMaster.getBillPrice_Mod_MRI_WO() * CPTQty;
            }
            //MRI w
            else if (RadGroup.equalsIgnoreCase("mr_w"))
            {
                myVal = myPayerMaster.getBillPrice_Mod_MRI_W() * CPTQty;
            }
            //MRI wwo
            else if (RadGroup.equalsIgnoreCase("mr_wwo"))
            {
                myVal = myPayerMaster.getBillPrice_Mod_MRI_WWO() * CPTQty;
            }
            if (myVal.doubleValue()<=0)
            {
                //run fee schedule ID look up
                //2010-02-23  Base look up on State and State WC Fee Sched
                searchDB2 mySS = new searchDB2();
                java.sql.ResultSet myRS = null;
                try
                {
                    //myRS = mySS.executeStatement("SELECT AllowAmount from tNIM3_FeeSchedule where CPT = '" + sCPT.replace('\\','|')  + "' AND StateID = " + myPracticeMaster.getOfficeStateID() + " AND FeeScheduleRefID = '1'");
                    String mySQL = "SELECT AllowAmount from tNIM3_FeeSchedule where CPT = ? AND StateID = " + myPracticeMaster.getOfficeStateID() + " AND FeeScheduleRefID = '1'";
                    db_PreparedStatementObject myDPSO = new db_PreparedStatementObject(mySQL);
                    myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,sCPT));
                    myRS = mySS.executePreparedStatement(myDPSO);
                    if (myRS!=null&&myRS.next())
                    {
                        myVal = new Double(myRS.getString("AllowAmount")) * myPayerMaster.getFeePercentageBill() * CPTQty ;
                    }
                    mySS.closeAll();
                }
                catch(Exception e)
                {
                    DebugLogger.printLine("NIMUtils:getCPTBillPriceforPayer:["+e+"]");
                    myVal = new Double(0);
                } finally {
                    mySS.closeAll();
                }


            }
        }
		return myVal;
	}

    public static Double getCPTPayPriceforPracticeMaster(Integer iPracticeID, String sCPT, Integer CPTQty)
    {
        return getCPTPayPriceforPracticeMaster(iPracticeID,sCPT,CPTQty,"",PLCUtils.getToday());
    }

    public static Double getCPTPayPriceforPracticeMaster(Integer iPracticeID, String sCPT, Integer CPTQty, String sMod, java.util.Date EffDate)
    {
        CPTObject myCPTO = new CPTObject(sCPT, sMod, CPTQty);
        return getCPTPayPriceforPracticeMaster(new bltPracticeMaster(iPracticeID), myCPTO, EffDate);
    }


    public static Double getCPTPayPriceforPracticeMaster(bltPracticeMaster myPracticeMaster, CPTObject myCPTO, Date EffDate)
    {
        return getCPTPayPriceforPracticeMaster(myPracticeMaster, myCPTO, EffDate, PAYMENT_TYPE_WORKCOMP);
    }

    public static CPTGroupObject2 CPTWizardToCPTGroupObject(bltCPTWizard myCPTW){
        CPTGroupObject2 go2 = new CPTGroupObject2();
        go2.addToCPTList(new CPTObject(myCPTW.getCPT1(), "", 1, myCPTW.getBP1()));
        go2.addToCPTList(new CPTObject(myCPTW.getCPT2(), "", 1, myCPTW.getBP2()));
        go2.addToCPTList(new CPTObject(myCPTW.getCPT3(), "", 1, myCPTW.getBP3()));
        go2.addToCPTList(new CPTObject(myCPTW.getCPT4(), "", 1, myCPTW.getBP4()));
        go2.addToCPTList(new CPTObject(myCPTW.getCPT5(), "", 1, myCPTW.getBP5()));
        return go2;
    }

    public static Double getCPTPayPriceforPracticeMaster(bltPracticeMaster myPracticeMaster, CPTGroupObject2 myCPTGO2, Date EffDate, Integer paymentType)
    {
        //note this method returns a price for all items inside a CPT Group Object
        //If it can't price one object, then it will return Zero for the entire value
        Double returnValue = 0.0;
        for (CPTObject CPTO: myCPTGO2.getCPTList()){
            Double tempValue =  getCPTPayPriceforPracticeMaster(myPracticeMaster, CPTO, EffDate, paymentType);
            if (tempValue<=0.0){
                //return 0.0 indicating that it can't be priced
                return 0.0;
            }
            returnValue += tempValue;
        }
        return returnValue;
    }

    public static int PAYMENT_TYPE_WORKCOMP = 1;
    public static int PAYMENT_TYPE_GROUPHEALTH = 2;


    public static Double getCPTPayPriceforPracticeMaster(bltPracticeMaster myPracticeMaster, CPTObject myCPTO, Date EffDate, Integer paymentType)
    {
        Double myVal = new Double(0.0);
        try
        {
            if (myCPTO.getCPT_Code().length()==5)
            {
                // override is now used as "backup 2"
                if (myVal<0.01)
                {
                    String RadGroup = getRadiologyGroup(myCPTO.getCPT_Code());
                    //ct wo
                    if (RadGroup.equalsIgnoreCase("ct_wo"))
                    {
                        if (paymentType == PAYMENT_TYPE_WORKCOMP){
                            if (myCPTO.getCPT_Modifier().equalsIgnoreCase("26")){
                                myVal = myPracticeMaster.getPrice_Mod_CT_WO_26() * myCPTO.getCPT_Qty();
                            } else if (myCPTO.getCPT_Modifier().equalsIgnoreCase("TC")){
                                myVal = myPracticeMaster.getPrice_Mod_CT_WO_TC() * myCPTO.getCPT_Qty();
                            } else {
                                myVal = myPracticeMaster.getPrice_Mod_CT_WO() * myCPTO.getCPT_Qty();
                            }
                        } else {
                            if (myCPTO.getCPT_Modifier().equalsIgnoreCase("26")){

                                myVal = myPracticeMaster.getGH_Price_Mod_CT_WO_26() * myCPTO.getCPT_Qty();
                            } else if (myCPTO.getCPT_Modifier().equalsIgnoreCase("TC")){
                                myVal = myPracticeMaster.getGH_Price_Mod_CT_WO_TC() * myCPTO.getCPT_Qty();
                            } else {
                                myVal = myPracticeMaster.getGH_Price_Mod_CT_WO() * myCPTO.getCPT_Qty();
                            }
                        }
                    }
                    //ct w
                    else if (RadGroup.equalsIgnoreCase("ct_w"))
                    {
                        if (paymentType == PAYMENT_TYPE_WORKCOMP){
                            if (myCPTO.getCPT_Modifier().equalsIgnoreCase("26")){
                                myVal = myPracticeMaster.getPrice_Mod_CT_W_26() * myCPTO.getCPT_Qty();
                            } else if (myCPTO.getCPT_Modifier().equalsIgnoreCase("TC")){
                                myVal = myPracticeMaster.getPrice_Mod_CT_W_TC() * myCPTO.getCPT_Qty();
                            } else {
                                myVal = myPracticeMaster.getPrice_Mod_CT_W() * myCPTO.getCPT_Qty();
                            }
                        } else {
                            if (myCPTO.getCPT_Modifier().equalsIgnoreCase("26")){
                                myVal = myPracticeMaster.getGH_Price_Mod_CT_W_26() * myCPTO.getCPT_Qty();
                            } else if (myCPTO.getCPT_Modifier().equalsIgnoreCase("TC")){
                                myVal = myPracticeMaster.getGH_Price_Mod_CT_W_TC() * myCPTO.getCPT_Qty();
                            } else {
                                myVal = myPracticeMaster.getGH_Price_Mod_CT_W() * myCPTO.getCPT_Qty();
                            }
                        }
//                        myVal = myPracticeMaster.getPrice_Mod_CT_W() * myCPTO.getCPT_Qty();
                    }
                    //ct wwo
                    else if (RadGroup.equalsIgnoreCase("ct_wwo"))
                    {
                        if (paymentType == PAYMENT_TYPE_WORKCOMP){
                            if (myCPTO.getCPT_Modifier().equalsIgnoreCase("26")){
                                myVal = myPracticeMaster.getPrice_Mod_CT_WWO_26() * myCPTO.getCPT_Qty();
                            } else if (myCPTO.getCPT_Modifier().equalsIgnoreCase("TC")){
                                myVal = myPracticeMaster.getPrice_Mod_CT_WWO_TC() * myCPTO.getCPT_Qty();
                            } else {
                                myVal = myPracticeMaster.getPrice_Mod_CT_WWO() * myCPTO.getCPT_Qty();
                            }
                        } else {
                            if (myCPTO.getCPT_Modifier().equalsIgnoreCase("26")){
                                myVal = myPracticeMaster.getGH_Price_Mod_CT_WWO_26() * myCPTO.getCPT_Qty();
                            } else if (myCPTO.getCPT_Modifier().equalsIgnoreCase("TC")){
                                myVal = myPracticeMaster.getGH_Price_Mod_CT_WWO_TC() * myCPTO.getCPT_Qty();
                            } else {
                                myVal = myPracticeMaster.getGH_Price_Mod_CT_WWO() * myCPTO.getCPT_Qty();
                            }
                        }
//                        myVal = myPracticeMaster.getPrice_Mod_CT_WWO() * myCPTO.getCPT_Qty();
                    }
                    //MRI wo
                    else if (RadGroup.equalsIgnoreCase("mr_wo"))
                    {
                        if (paymentType == PAYMENT_TYPE_WORKCOMP){
                            if (myCPTO.getCPT_Modifier().equalsIgnoreCase("26")){
                                myVal = myPracticeMaster.getPrice_Mod_MRI_WO_26() * myCPTO.getCPT_Qty();
                            } else if (myCPTO.getCPT_Modifier().equalsIgnoreCase("TC")){
                                myVal = myPracticeMaster.getPrice_Mod_MRI_WO_TC() * myCPTO.getCPT_Qty();
                            } else {
                                myVal = myPracticeMaster.getPrice_Mod_MRI_WO() * myCPTO.getCPT_Qty();
                            }
                        } else {
                            if (myCPTO.getCPT_Modifier().equalsIgnoreCase("26")){
                                myVal = myPracticeMaster.getGH_Price_Mod_MRI_WO_26() * myCPTO.getCPT_Qty();
                            } else if (myCPTO.getCPT_Modifier().equalsIgnoreCase("TC")){
                                myVal = myPracticeMaster.getGH_Price_Mod_MRI_WO_TC() * myCPTO.getCPT_Qty();
                            } else {
                                myVal = myPracticeMaster.getGH_Price_Mod_MRI_WO() * myCPTO.getCPT_Qty();
                            }
                        }

//                        myVal = myPracticeMaster.getPrice_Mod_MRI_WO() * myCPTO.getCPT_Qty();
                    }
                    //MRI w
                    else if (RadGroup.equalsIgnoreCase("mr_w"))
                    {
                        if (paymentType == PAYMENT_TYPE_WORKCOMP){
                            if (myCPTO.getCPT_Modifier().equalsIgnoreCase("26")){
                                myVal = myPracticeMaster.getPrice_Mod_MRI_W_26() * myCPTO.getCPT_Qty();
                            } else if (myCPTO.getCPT_Modifier().equalsIgnoreCase("TC")){
                                myVal = myPracticeMaster.getPrice_Mod_MRI_W_TC() * myCPTO.getCPT_Qty();
                            } else {
                                myVal = myPracticeMaster.getPrice_Mod_MRI_W() * myCPTO.getCPT_Qty();
                            }
                        } else {
                            if (myCPTO.getCPT_Modifier().equalsIgnoreCase("26")){
                                myVal = myPracticeMaster.getGH_Price_Mod_MRI_W_26() * myCPTO.getCPT_Qty();
                            } else if (myCPTO.getCPT_Modifier().equalsIgnoreCase("TC")){
                                myVal = myPracticeMaster.getGH_Price_Mod_MRI_W_TC() * myCPTO.getCPT_Qty();
                            } else {
                                myVal = myPracticeMaster.getGH_Price_Mod_MRI_W() * myCPTO.getCPT_Qty();
                            }
                        }
//                        myVal = myPracticeMaster.getPrice_Mod_MRI_W() * myCPTO.getCPT_Qty();
                    }
                    //MRI wwo
                    else if (RadGroup.equalsIgnoreCase("mr_wwo"))
                    {
                        if (paymentType == PAYMENT_TYPE_WORKCOMP){
                            if (myCPTO.getCPT_Modifier().equalsIgnoreCase("26")){
                                myVal = myPracticeMaster.getPrice_Mod_MRI_WWO_26() * myCPTO.getCPT_Qty();
                            } else if (myCPTO.getCPT_Modifier().equalsIgnoreCase("TC")){
                                myVal = myPracticeMaster.getPrice_Mod_MRI_WWO_TC() * myCPTO.getCPT_Qty();
                            } else {
                                myVal = myPracticeMaster.getPrice_Mod_MRI_WWO() * myCPTO.getCPT_Qty();
                            }
                        } else {
                            if (myCPTO.getCPT_Modifier().equalsIgnoreCase("26")){
                                myVal = myPracticeMaster.getGH_Price_Mod_MRI_WWO_26() * myCPTO.getCPT_Qty();
                            } else if (myCPTO.getCPT_Modifier().equalsIgnoreCase("TC")){
                                myVal = myPracticeMaster.getGH_Price_Mod_MRI_WWO_TC() * myCPTO.getCPT_Qty();
                            } else {
                                myVal = myPracticeMaster.getGH_Price_Mod_MRI_WWO() * myCPTO.getCPT_Qty();
                            }
                        }
//                        myVal = myPracticeMaster.getPrice_Mod_MRI_WWO() * myCPTO.getCPT_Qty();
                    }
                    //MRI wwo
                    else if (RadGroup.equalsIgnoreCase("pet"))
                    {
                        if (paymentType == PAYMENT_TYPE_WORKCOMP){
                            if (myCPTO.getCPT_Modifier().equalsIgnoreCase("26")){
                                myVal = myPracticeMaster.getPrice_Mod_PETCT_WWO_26() * myCPTO.getCPT_Qty();
                            } else if (myCPTO.getCPT_Modifier().equalsIgnoreCase("TC")){
                                myVal = myPracticeMaster.getPrice_Mod_PETCT_WWO_TC() * myCPTO.getCPT_Qty();
                            } else {
                                myVal = myPracticeMaster.getPrice_Mod_PETCT_WWO() * myCPTO.getCPT_Qty();
                            }
                        } else {
                            if (myCPTO.getCPT_Modifier().equalsIgnoreCase("26")){
                                myVal = myPracticeMaster.getGH_Price_Mod_PETCT_WWO_26() * myCPTO.getCPT_Qty();
                            } else if (myCPTO.getCPT_Modifier().equalsIgnoreCase("TC")){
                                myVal = myPracticeMaster.getGH_Price_Mod_PETCT_WWO_TC() * myCPTO.getCPT_Qty();
                            } else {
                                myVal = myPracticeMaster.getGH_Price_Mod_PETCT_WWO() * myCPTO.getCPT_Qty();
                            }
                        }
//                        myVal = myPracticeMaster.getPrice_Mod_MRI_WWO() * myCPTO.getCPT_Qty();
                    }

                    if (paymentType == PAYMENT_TYPE_WORKCOMP){
                        //use backup lookup
                        if (myVal.doubleValue()<=0)
                        {
                            myVal = NIMUtils.getFeeSchedulePrice(myPracticeMaster.getFeeScheduleRefID(), myPracticeMaster.getFeePercentage(), (myPracticeMaster.getOfficeZIP().length()>5 ? myPracticeMaster.getOfficeZIP().substring(0, 5):myPracticeMaster.getOfficeZIP()), myCPTO.getCPT_Code(), myCPTO.getCPT_Modifier(), EffDate) * myCPTO.getCPT_Qty();
                            DebugLogger.printLine("\n\t["+myPracticeMaster.getPracticeID()+"]myPracticeMaster.getFeeScheduleRefID(): "+myPracticeMaster.getFeeScheduleRefID()+"\n\tmyPracticeMaster.getFeeOverPercentage(): "+myPracticeMaster.getFeeOverPercentage()+"\n\tmyPracticeMaster.getOfficeZIP(): "+myPracticeMaster.getOfficeZIP()+"\n\tmyCPTO.getCPT_Code(): "+myCPTO.getCPT_Code()+
                            		"\n\tmyCPTO.getCPT_Modifier(): "+myCPTO.getCPT_Modifier()+"\n\tEffDate: "+EffDate);
                            
                            DebugLogger.printLine(new Date()+" || getCPTPayPriceforPracticeMaster:  1 myval:"+myVal);
                        }
                        //use backup #2 lookup   uses the override fields
                        if (myVal.doubleValue()<=0)
                        {
                            myVal = NIMUtils.getFeeSchedulePrice(myPracticeMaster.getFeeScheduleOverRefID(), myPracticeMaster.getFeeOverPercentage(), (myPracticeMaster.getOfficeZIP().length()>5 ? myPracticeMaster.getOfficeZIP().substring(0, 5):myPracticeMaster.getOfficeZIP()), myCPTO.getCPT_Code(), myCPTO.getCPT_Modifier(), EffDate) * myCPTO.getCPT_Qty();
                            DebugLogger.printLine("\n\t["+myPracticeMaster.getPracticeID()+"]myPracticeMaster.getFeeScheduleOverRefID(): "+myPracticeMaster.getFeeScheduleOverRefID()+"\n\tmyPracticeMaster.getFeeOverPercentage(): "+myPracticeMaster.getFeeOverPercentage()+"\n\tmyPracticeMaster.getOfficeZIP(): "+myPracticeMaster.getOfficeZIP()+"\n\tmyCPTO.getCPT_Code(): "+myCPTO.getCPT_Code()+
                            		"\n\tmyCPTO.getCPT_Modifier(): "+myCPTO.getCPT_Modifier()+"\n\tEffDate: "+EffDate);
                            DebugLogger.printLine(new Date()+" || getCPTPayPriceforPracticeMaster:  2: myval:"+myVal);
                        }
                    } else {         //GH rates
                        //use backup lookup
                        if (myVal.doubleValue()<=0)
                        {
//                            DebugLogger.d("Get Price", "Practice [ID:"+myPracticeMaster.getPracticeID()+"]["+myPracticeMaster.getPracticeName()+"]["+myPracticeMaster.getGH_FeeScheduleRefID()+"]["+myPracticeMaster.getGH_FeeScheduleOverRefID()+"]");
                            myVal = NIMUtils.getFeeSchedulePrice(myPracticeMaster.getGH_FeeScheduleRefID(), myPracticeMaster.getGH_FeePercentage(), myPracticeMaster.getOfficeZIP(), myCPTO.getCPT_Code(), myCPTO.getCPT_Modifier(), EffDate) * myCPTO.getCPT_Qty();
                            DebugLogger.printLine(new Date()+" || getCPTPayPriceforPracticeMaster:  3");
                        }
                        //use backup #2 lookup   uses the override fields
                        if (myVal.doubleValue()<=0)
                        {
                            myVal = NIMUtils.getFeeSchedulePrice(myPracticeMaster.getGH_FeeScheduleOverRefID(), myPracticeMaster.getGH_FeeOverPercentage(), myPracticeMaster.getOfficeZIP(), myCPTO.getCPT_Code(), myCPTO.getCPT_Modifier(), EffDate) * myCPTO.getCPT_Qty();
                            DebugLogger.printLine(new Date()+" || getCPTPayPriceforPracticeMaster:  4");
                        }
                    }


                }
            }
        }
        catch (Exception EEE)
        {
            DebugLogger.printLine("NIMUtils:getCPTPayPriceforPracticeMaster: " + EEE);
        }
        DebugLogger.printLine("getCPTPayPriceforPracticeMaster:  final value: "+myVal);
        return myVal;
    }

    public static Double getCPTGroupPayPriceforPracticeMaster(bltPracticeMaster myPracticeMaster, CPTGroupObject2 CPTGO2, java.util.Date EffDate)
    {
        return getCPTGroupPayPriceforPracticeMaster(myPracticeMaster, CPTGO2, EffDate, NIMUtils.PAYMENT_TYPE_WORKCOMP);
    }
    
    public static Double getCPTGroupPayPriceforPracticeMaster(bltPracticeMaster myPracticeMaster, CPTGroupObject2 CPTGO2, java.util.Date EffDate, int paymentType)
    {
        Double myVal_Final = 0.0;
        boolean isFail = false;
        try
        {
            for (int i=0;i<CPTGO2.getCPTList().size();i++)
            {
                Double myVal = new Double(0.0);
                CPTObject myCPTO = CPTGO2.getCPTList().elementAt(i);
                if (myCPTO.getCPT_Code().length()>4)
                {
                    //Making Code DRYer
                    DebugLogger.d("Group Pricing", "About to Price ["+myCPTO.getCPT_Code()+"]");
                    	myVal = getCPTPayPriceforPracticeMaster(myPracticeMaster, myCPTO, EffDate, paymentType );
                    

                    if (myVal<=0.1)
                    {
                        isFail = true;
                    }
                    myVal_Final += myVal;
                }
            }
        }
        catch (Exception EEE)
        {
            DebugLogger.printLine("NIMUtils:getCPTGroupPayPriceforPracticeMaster: " + EEE);
        }
        if (isFail||myVal_Final<0.01)
        {
            myVal_Final = null;
        }

        return myVal_Final;
    }

    public static Double getCPTGroupPayPriceforPayer(bltNIM3_PayerMaster inPayer, String inZIP, CPTGroupObject2 CPTGO2, java.util.Date EffDate)
    {
        Double myVal_Final = 0.0;
        boolean isFail = false;
        if (inPayer==null)
        {
            isFail = true;
            DebugLogger.printLine("NIMUtils:getCPTGroupPayPriceforPayer: Null InPayer Parameter");
        }
        else
        {
            try
            {
                for (int i=0;i<CPTGO2.getCPTList().size();i++)
                {
                    Double myVal = new Double(0.0);
                    CPTObject myCPTO = CPTGO2.getCPTList().elementAt(i);
                    if (myCPTO.getCPT_Code()!=null&&myCPTO.getCPT_Code().length()>3)
                    {
                        myVal = NIMUtils.getPayerAllowAmount(inPayer.getPayerID(),inZIP.trim(),myCPTO.getCPT_Code().trim(),myCPTO.getCPT_Modifier(),EffDate);
                        if (myVal==null||myVal<=0.1)
                        {
                            DebugLogger.printLine("NIMUtils:getCPTGroupPayPriceforPayer: Code Failed " + myCPTO.getCPT_Code());
                            isFail = true;
                        }
                        else
                        {
                            myVal_Final += myVal * myCPTO.getCPT_Qty();
                        }
                    }
                }
            }
            catch (Exception EEE)
            {
                DebugLogger.printLine("NIMUtils:getCPTGroupPayPriceforPayer: " + EEE);
            }
        }
        if (isFail)
        {
            myVal_Final = null;
        }
        return myVal_Final;
    }

    public static double getTysonExceptionRate(int practiceID, String cptGroup){
    	LinkedHashMap<String, Double> muscatine7691 = new LinkedHashMap<String, Double>();
    	LinkedHashMap<String, Double> corridor8629 = new LinkedHashMap<String, Double>();
    	LinkedHashMap<String, Double> cnos7557 = new LinkedHashMap<String, Double>();
    	LinkedHashMap<String, Double> gardenCity7559 = new LinkedHashMap<String, Double>();
    	LinkedHashMap<String, Double> stCatherine7744 = new LinkedHashMap<String, Double>();
    	LinkedHashMap<String, Double> crisp7886 = new LinkedHashMap<String, Double>();
    	LinkedHashMap<String, Double> iaradiologyClive889 = new LinkedHashMap<String, Double>();
    	LinkedHashMap<String, Double> iaradiologyDTDesmoines865 = new LinkedHashMap<String, Double>();
    	LinkedHashMap<String, Double> capitalOrthopedicsAndSports10078 = new LinkedHashMap<String, Double>();
    	LinkedHashMap<Integer, LinkedHashMap<String, Double>> exceptionRate = new LinkedHashMap<Integer, LinkedHashMap<String, Double>>();
    	
    	muscatine7691.put("mr_wo", 990.00d);
    	muscatine7691.put("mr_w", 1280.00d);
    	muscatine7691.put("mr_wwo", 1925.00d);
    	muscatine7691.put("ct_wo", 536.00d);
    	muscatine7691.put("ct_w", 809.00d);
    	muscatine7691.put("ct_wwo", 1063.00d);
    	
    	corridor8629.put("mr_wo", 990.00d);
    	corridor8629.put("mr_w", 1280.00d);
    	corridor8629.put("mr_wwo", 1925.00d);
    	corridor8629.put("ct_wo", 536.00d);
    	corridor8629.put("ct_w", 809.00d);
    	corridor8629.put("ct_wwo", 1063.00d);
    	
    	cnos7557.put("mr_wo", 1050.00d);
    	cnos7557.put("mr_w", 1150.00d);
    	cnos7557.put("mr_wwo", 1250.00d);
    	cnos7557.put("ct_wo", 0.00d);
    	cnos7557.put("ct_w", 0.00d);
    	cnos7557.put("ct_wwo", 0.00d);
    	
    	gardenCity7559.put("mr_wo", 785.00d);
    	gardenCity7559.put("mr_w", 885.00d);
    	gardenCity7559.put("mr_wwo", 985.00d);
    	gardenCity7559.put("ct_wo", 350.00d);
    	gardenCity7559.put("ct_w", 400.00d);
    	gardenCity7559.put("ct_wwo", 450.00d);
    	
    	stCatherine7744.put("mr_wo", 785.00d);
    	stCatherine7744.put("mr_w", 885.00d);
    	stCatherine7744.put("mr_wwo", 985.00d);
    	stCatherine7744.put("ct_wo", 350.00d);
    	stCatherine7744.put("ct_w", 400.00d);
    	stCatherine7744.put("ct_wwo", 450.00d);
    	
    	crisp7886.put("mr_wo", 640.00d);
    	crisp7886.put("mr_w", 740.00d);
    	crisp7886.put("mr_wwo", 840.00d);
    	crisp7886.put("ct_wo", 465.00d);
    	crisp7886.put("ct_w", 490.00d);
    	crisp7886.put("ct_wwo", 510.00d);
    	
    	iaradiologyClive889.put("mr_wo", 890.00d);
    	iaradiologyClive889.put("mr_w", 990.00d);
    	iaradiologyClive889.put("mr_wwo", 1090.00d);
    	iaradiologyClive889.put("ct_wo", 490.00d);
    	iaradiologyClive889.put("ct_w", 540.00d);
    	iaradiologyClive889.put("ct_wwo", 590.00d);
    	
    	iaradiologyDTDesmoines865.put("ct_wo", 490.00d);
    	iaradiologyDTDesmoines865.put("ct_w", 540.00d);
    	iaradiologyDTDesmoines865.put("ct_wwo", 590.00d);
    	
    	capitalOrthopedicsAndSports10078.put("mr_wo", 990.00);
    	capitalOrthopedicsAndSports10078.put("mr_w", 1040.00);
    	capitalOrthopedicsAndSports10078.put("mr_wwo", 1090.00);
    	
    	exceptionRate.put(7691, muscatine7691);
    	exceptionRate.put(8629, corridor8629);
    	exceptionRate.put(7557, cnos7557);
    	exceptionRate.put(7559, gardenCity7559);
    	exceptionRate.put(7744, stCatherine7744);
    	exceptionRate.put(7886, crisp7886);
    	exceptionRate.put(889, iaradiologyClive889);
    	exceptionRate.put(865, iaradiologyDTDesmoines865);
    	exceptionRate.put(10078, capitalOrthopedicsAndSports10078);
    	
    	return exceptionRate.get(practiceID).get(cptGroup);
    }



    public static String  ReCalcServiceBilling (Integer iEncounterID)
    {
        String myVal = "Starting...";
        DebugLogger.printLine("NIMUtils:ReCalcServiceBilling: starting: "+iEncounterID);
        try
        {
        	List<Integer> tysonExceptionIC = Arrays.asList(7691,8629,7557,7559,7744,7886,889,865,10078);
        	int tysonParentID = 138;
        	NIM3_EncounterObject2 myEO2 = new NIM3_EncounterObject2(iEncounterID,"Load for Billing");
        	
            //we want to declare PracticeMaster so that we can pass the object into calculations
            bltPracticeMaster pm = new bltPracticeMaster(myEO2.getNIM3_Appointment().getProviderID());
            //we want to declare payerMaster so that we can access information about the payer and parent payer to determine if it is a Direct (NID) Case
            bltNIM3_PayerMaster payM = new bltNIM3_PayerMaster(myEO2.getNIM3_CaseAccount().getPayerID());
            bltNIM3_Service_List        bltNIM3_Service_List        =    new    bltNIM3_Service_List(iEncounterID);
            if (myEO2.getNIM3_Appointment()!=null)
            {

            //declaration of Enumeration
                bltNIM3_Service        working_bltNIM3_Service;
                ListElement         leCurrentElement;
                java.util.Enumeration eList = bltNIM3_Service_List.elements();
                while (eList.hasMoreElements())
                {
                    leCurrentElement    = (ListElement) eList.nextElement();
                    working_bltNIM3_Service  = (bltNIM3_Service) leCurrentElement.getObject();
                    working_bltNIM3_Service.setUniqueModifyComments("NIMUtils:ReCalcServiceBilling:System Generated");
                    working_bltNIM3_Service.GroupSecurityInit(new Integer(1));
                    if (working_bltNIM3_Service.getServiceStatusID().intValue()==1)
                    {

                        myVal +="<hr>For CPT: " + working_bltNIM3_Service.getCPT() + "";
                        if (working_bltNIM3_Service.getCPT().equalsIgnoreCase(""))
                        {
                            myVal += "[Warning: Blank CPT]";
                        }
                        if (working_bltNIM3_Service.getCPTQty().intValue()<=0)
                        {
                            working_bltNIM3_Service.setCPTQty(new Integer(1));
                            myVal +="<br>&nbsp;&nbsp;QTY: " + working_bltNIM3_Service.getCPTQty() + " [UPDATED: Auto-Changed from 0 to 1]";
                        }
                        else
                        {
                            myVal +="<br>&nbsp;&nbsp;QTY: " + working_bltNIM3_Service.getCPTQty() + " [NOT UPDATED]";
                        }

                        //Double tempPayOut_Calc = NIMUtils.getCPTPayPriceforPracticeMaster(myEO2.getNIM3_Appointment().getProviderID(),working_bltNIM3_Service.getCPT(),working_bltNIM3_Service.getCPTQty(),working_bltNIM3_Service.getCPTModifier(),myEO2.getNIM3_Encounter().getDateOfService());
                        //we default to Work Comp Pricing for the provider
                        Integer payment_type = NIMUtils.PAYMENT_TYPE_WORKCOMP;
                        //if the Payer or the Payer's Parent are the configured NIM NID Payer ID that means this is a NextImage Direct Case and should use GH pricing instead of work comp
                        if (myEO2.isNextImageDirectCase()){
                            payment_type = NIMUtils.PAYMENT_TYPE_GROUPHEALTH;
                        }
                        //now we send these into the new PayPrice caculator that takes a PAYMENT_TYPE
                        Double tempPayOut_Calc = NIMUtils.getCPTPayPriceforPracticeMaster(pm, new CPTObject(working_bltNIM3_Service.getCPT(),working_bltNIM3_Service.getCPTModifier(),working_bltNIM3_Service.getCPTQty()),myEO2.getNIM3_Encounter().getDateOfService(), payment_type);
                        //set Allow Amount
                        Double tempCalc = working_bltNIM3_Service.getCPTQty() * NIMUtils.getPayerAllowAmount(myEO2.getNIM3_CaseAccount().getPayerID(),myEO2.getAppointment_PracticeMaster().getOfficeZIP(),working_bltNIM3_Service.getCPT(),working_bltNIM3_Service.getCPTModifier(),myEO2.getNIM3_Encounter().getDateOfService(),pm);
                        Double tempFS_Calc = working_bltNIM3_Service.getCPTQty() *  NIMUtils.getFeeSchedulePrice(1,1.0,myEO2.getAppointment_PracticeMaster().getOfficeZIP(),working_bltNIM3_Service.getCPT(),working_bltNIM3_Service.getCPTModifier(),myEO2.getNIM3_Encounter().getDateOfService());
                        bltNIM3_NIDPromo temp_promo = null;
                        myVal +="<br>tempPayOut_Calc: "+tempPayOut_Calc+"<br>tempCalc: "+tempCalc+"<br>tempFS_Calc: "+tempFS_Calc;
                        
                        // Allow ammount
                        if (tempCalc<0.01)
                        {
                            tempCalc =(NIMUtils.getCPTPriceforPayer(myEO2.getNIM3_CaseAccount().getPayerID(),myEO2.getNIM3_Appointment().getProviderID(),working_bltNIM3_Service.getCPT(),working_bltNIM3_Service.getCPTQty()));
                        }
                        if (working_bltNIM3_Service.getAllowAmount()>0)
                        {
                            if (false )//12-03-14 Removed logic per Paul. This piece is checked during submit of allow amounts.  !myEO2.isNextImageDirectCase() && tempFS_Calc>0.0 && working_bltNIM3_Service.getAllowAmount()>tempFS_Calc)
                            {
//                                myVal +="<br>&nbsp;&nbsp;Allow Amount $" + working_bltNIM3_Service.getAllowAmount() + "  on file is greater than State Fee Schedule: $" + tempFS_Calc + " [UPDATED: Fee Schedule overrides amount on file]";
//                                working_bltNIM3_Service.setAllowAmount(tempFS_Calc);
                            }
                            else
                            {
                                myVal +="<br>&nbsp;&nbsp;Allow Amount: $" + working_bltNIM3_Service.getAllowAmount() + "  [NOT UPDATED]";
                            }
                        }
                        else
                        {
                            if (!myEO2.isNextImageDirectCase() && tempFS_Calc>0.0 && tempCalc>tempFS_Calc)
                            {
                                // 02-21-14 Ori logic State FS supersedes Payer Allow Amount. Logic is reversed now per Bob - Po
                            	myVal +="<br>&nbsp;&nbsp;Calculated Allow Amount $" + tempCalc + " is greater than State Fee Schedule: $" + tempFS_Calc + " [UPDATED: Calculated allow amount]";
                                working_bltNIM3_Service.setAllowAmount(tempCalc);
                            }
                            else if (myEO2.isNextImageDirectCase() )
                            {
                                if (temp_promo==null){
                                    temp_promo = new bltNIM3_NIDPromo(myEO2.getNIM3_Referral().getReferralNIDPromoID());
                                }
                                working_bltNIM3_Service.setAllowAmount(NIDUtils.getNIDFinalPrice(tempPayOut_Calc, pm, myEO2.getNIM3_PayerMaster(), temp_promo ));
                                myVal +="<br>&nbsp;&nbsp;Updated to Cost * NID Markup - discount: $" + (working_bltNIM3_Service.getAllowAmount()) + " ";
                            }
                            else
                            {
                                working_bltNIM3_Service.setAllowAmount(tempCalc);
                                myVal +="<br>&nbsp;&nbsp;No amount on file, Calculated Allow Amount: $" + working_bltNIM3_Service.getAllowAmount() + " Saved [UPDATED: Calculated and Updated]";
                            }
//                            if(payM.getParentPayerID().equals(138) || payM.getPayerID().equals(138) || payM.getPayerID().equals(139) || payM.getPayerID().equals(143) || payM.getPayerID().equals(147) || payM.getPayerID().equals(148) || payM.getPayerID().equals(149)){
//                            	working_bltNIM3_Service.setAllowAmount(tempCalc);//gio
//                        	}
                        }
                        
                        
//                        if(getTysonExceptionRate(pm.getPracticeID(),getRadiologyGroup(working_bltNIM3_Service.getCPT()))==0 && (payM.getParentPayerID()==tysonParentID || payM.getPayerID()==tysonParentID)){
//                        	working_bltNIM3_Service.setAllowAmount(tempCalc);
//                        	myVal +="<br>2093"; 
//                        } else if((payM.getParentPayerID()==tysonParentID || payM.getPayerID()==tysonParentID) && tysonExceptionIC.contains(pm.getPracticeID())){
//                        	working_bltNIM3_Service.setAllowAmount(getTysonExceptionRate(pm.getPracticeID(),getRadiologyGroup(working_bltNIM3_Service.getCPT())));
//                        	myVal +="<br>2096";
//                        }
//                        
//                        myVal +="<br>EXXBlaasdfhaslfkjal Rate: "+ getTysonExceptionRate(pm.getPracticeID(),getRadiologyGroup(working_bltNIM3_Service.getCPT()));
//                        myVal +="<br>&nbsp;&nbsp;No amount on file, Calculated Allow Amount: $" + working_bltNIM3_Service.getAllowAmount() + " Saved [UPDATED: Calculated and Updated]";
                        
                        //set Bill Amount
                        tempCalc = working_bltNIM3_Service.getCPTQty() * NIMUtils.getPayerBillAmount(myEO2.getNIM3_CaseAccount().getPayerID(),myEO2.getAppointment_PracticeMaster().getOfficeZIP(),working_bltNIM3_Service.getCPT(),working_bltNIM3_Service.getCPTModifier(),myEO2.getNIM3_Encounter().getDateOfService());
                        if (tempCalc<0.01)
                        {
                            tempCalc = (NIMUtils.getCPTBillPriceforPayer(myEO2.getNIM3_CaseAccount().getPayerID(),myEO2.getNIM3_Appointment().getProviderID(),working_bltNIM3_Service.getCPT(),working_bltNIM3_Service.getCPTQty()) );
                        }
                        if (myEO2.isNextImageDirectCase())
                        {
                            myVal +="<br>&nbsp;&nbsp;Bill Amount: $" + working_bltNIM3_Service.getBillAmount() + "  [NOT UPDATED]";
                        }
                        else if (working_bltNIM3_Service.getBillAmount()>0)
                        {
                            myVal +="<br>&nbsp;&nbsp;Bill Amount: $" + working_bltNIM3_Service.getBillAmount() + "  [NOT UPDATED: Compare to calculated amount of $" + tempCalc + "]";
                        }
                        else
                        {
                            working_bltNIM3_Service.setBillAmount(tempCalc);
                            myVal +="<br>&nbsp;&nbsp;Bill Amount: $" + working_bltNIM3_Service.getBillAmount() + " [UPDATED: Calculated and Updated]";
                        }
                        	

                        //set PaidOut    already has QTY
                        tempCalc = tempPayOut_Calc;
                        if (working_bltNIM3_Service.getPaidOutAmount()>0)
                        {
                            if (false)  //(false )//12-03-14 Removed logic per Paul. This piece is checked during submit of payout amounts.(!myEO2.isNextImageDirectCase() && tempFS_Calc>0.0 && working_bltNIM3_Service.getPaidOutAmount()>tempFS_Calc)
                            {
//                                myVal +="<br>&nbsp;&nbsp;Pay-Out $" + working_bltNIM3_Service.getPaidOutAmount() + "  on file is greater than State Fee Schedule: $" + tempFS_Calc + " [UPDATED: Fee Schedule overrides amount on file]";
//                                working_bltNIM3_Service.setPaidOutAmount(tempFS_Calc);
                            }
                            else
                            {
                                myVal +="<br>&nbsp;&nbsp;Pay-Out Amount: $" + working_bltNIM3_Service.getPaidOutAmount() + "  [NOT UPDATED: Compare to calculated amount of $" + tempCalc + "]";
                            }
                        }
                        else
                        {
                            if ( !myEO2.isNextImageDirectCase() && tempFS_Calc>0.0 && tempCalc>=(tempFS_Calc*pm.getFeePercentage()))//removed = after && logic
                            {
                            	working_bltNIM3_Service.setPaidOutAmount(tempFS_Calc*pm.getFeePercentage());
                            	myVal +="<br>&nbsp;&nbsp;Calculated Pay-Out Amount $" + tempCalc + " is greater than "+pm.getFeePercentage()*100+"% State Fee Schedule: $" + tempFS_Calc*pm.getFeePercentage() + " [UPDATED: Fee Schedule overrides calculated pay-out amount]";
                            	//System.out.println("2sffsdfdfsdfsdf  "+working_bltNIM3_Service.getPaidOutAmount());
                            }
                            else
                            {
                            	working_bltNIM3_Service.setPaidOutAmount(tempCalc);
                                myVal +="<br>&nbsp;&nbsp;No amount on file, Calculated Pay-Out Amount: $" + working_bltNIM3_Service.getPaidOutAmount() + " [UPDATED: Calculated and Updated]";
                            }
                        }
                        // per Silvia
                        if( pm.getPracticeID().equals(340) || pm.getPracticeID().equals(1538) ){
                        	String cptGroup = getRadiologyGroup(working_bltNIM3_Service.getCPT());
                        	if( cptGroup.equalsIgnoreCase("mr_wo") ){
                        		working_bltNIM3_Service.setPaidOutAmount(pm.getPrice_Mod_MRI_WO());
                        	}else if( cptGroup.equalsIgnoreCase("mr_w") ){
                        		working_bltNIM3_Service.setPaidOutAmount(pm.getPrice_Mod_MRI_W());
                        	}else if( cptGroup.equalsIgnoreCase("mr_wwo") ){
                        		working_bltNIM3_Service.setPaidOutAmount(pm.getPrice_Mod_MRI_WWO());
                        	}else if( cptGroup.equalsIgnoreCase("ct_wo") ){
                        		working_bltNIM3_Service.setPaidOutAmount(pm.getPrice_Mod_CT_WO());
                        	}else if( cptGroup.equalsIgnoreCase("ct_w") ){
                        		working_bltNIM3_Service.setPaidOutAmount(pm.getPrice_Mod_CT_W());
                        	}else if( cptGroup.equalsIgnoreCase("ct_wwo") ){
                        		working_bltNIM3_Service.setPaidOutAmount(pm.getPrice_Mod_CT_WWO());
                        	}
                        }
                        //per Brandon N
                        if(payM.getParentPayerID() == 1316){
                        	if(working_bltNIM3_Service.getCPT().equals("76140")){
                        		working_bltNIM3_Service.setPaidOutAmount(175.00);
                        	}
                        }
                        
                        if(working_bltNIM3_Service.getCPT().equals("70030")){
                        	working_bltNIM3_Service.setPaidOutAmount(15.00);
                        }
                        //Tyson Exception Rules:
	                         
	                        
	                        if((payM.getParentPayerID()==tysonParentID || payM.getPayerID()==tysonParentID) && tysonExceptionIC.contains(pm.getPracticeID())){
	                        	String cptGroup = getRadiologyGroup(working_bltNIM3_Service.getCPT());
	                      		  if(cptGroup.equals("mr_wo")){
	                          		working_bltNIM3_Service.setPaidOutAmount(pm.getPrice_Mod_MRI_WO());
	                          		working_bltNIM3_Service.setAllowAmount(NIMUtils.getTysonExceptionRate(pm.getPracticeID(), "mr_wo"));
	                          		working_bltNIM3_Service.setBillAmount(NIMUtils.getTysonExceptionRate(pm.getPracticeID(), "mr_wo"));
	                          	} else if(cptGroup.equals("mr_w")){
	                          		working_bltNIM3_Service.setPaidOutAmount(pm.getPrice_Mod_MRI_W());
	                          		working_bltNIM3_Service.setAllowAmount(NIMUtils.getTysonExceptionRate(pm.getPracticeID(), "mr_w"));
	                          		working_bltNIM3_Service.setBillAmount(NIMUtils.getTysonExceptionRate(pm.getPracticeID(), "mr_w"));
	                          	} else if(cptGroup.equals("mr_wwo")){
	                          		working_bltNIM3_Service.setPaidOutAmount(pm.getPrice_Mod_MRI_WWO());
	                          		working_bltNIM3_Service.setAllowAmount(NIMUtils.getTysonExceptionRate(pm.getPracticeID(), "mr_wwo"));
	                          		working_bltNIM3_Service.setBillAmount(NIMUtils.getTysonExceptionRate(pm.getPracticeID(), "mr_wwo"));
	                          	} else if(cptGroup.equals("ct_wo")){
	                          		working_bltNIM3_Service.setPaidOutAmount(pm.getPrice_Mod_CT_WO());
	                          		working_bltNIM3_Service.setAllowAmount(NIMUtils.getTysonExceptionRate(pm.getPracticeID(), "ct_wo"));
	                          		working_bltNIM3_Service.setBillAmount(NIMUtils.getTysonExceptionRate(pm.getPracticeID(), "ct_wo"));
	                          	} else if(cptGroup.equals("ct_w")){
	                          		working_bltNIM3_Service.setPaidOutAmount(pm.getPrice_Mod_CT_W());
	                          		working_bltNIM3_Service.setAllowAmount(NIMUtils.getTysonExceptionRate(pm.getPracticeID(), "ct_w"));
	                          		working_bltNIM3_Service.setBillAmount(NIMUtils.getTysonExceptionRate(pm.getPracticeID(), "ct_w"));
	                          	} else if(cptGroup.equals("ct_wwo")){
	                          		working_bltNIM3_Service.setPaidOutAmount(pm.getPrice_Mod_CT_WWO());
	                          		working_bltNIM3_Service.setAllowAmount(NIMUtils.getTysonExceptionRate(pm.getPracticeID(), "ct_wwo"));
	                          		working_bltNIM3_Service.setBillAmount(NIMUtils.getTysonExceptionRate(pm.getPracticeID(), "ct_wwo"));
	                          	}
	                        }
	                        
	                        if((payM.getParentPayerID()==tysonParentID || payM.getPayerID()==tysonParentID) && pm.getPracticeID().equals(889)){
		                        if(working_bltNIM3_Service.getCPT().equals("73722") || working_bltNIM3_Service.getCPT().equals("73222")){
		                        	working_bltNIM3_Service.setPaidOutAmount(1100.00);
		                        	working_bltNIM3_Service.setAllowAmount(1190.00);
		                        	working_bltNIM3_Service.setBillAmount(1100.00);
		                        }
		                        if(working_bltNIM3_Service.getCPT().equals("77002") || working_bltNIM3_Service.getCPT().equals("77003") && payM.getParentPayerID()==tysonParentID || payM.getPayerID()==tysonParentID){
		            				working_bltNIM3_Service.setPaidOutAmount(0.00);
		            				working_bltNIM3_Service.setAllowAmount(0.00);
		            				working_bltNIM3_Service.setBillAmount(0.00);
		            			}
		                        if(working_bltNIM3_Service.getCPT().equals("23350") || working_bltNIM3_Service.getCPT().equals("24220") || working_bltNIM3_Service.getCPT().equals("25246") || working_bltNIM3_Service.getCPT().equals("26989") || working_bltNIM3_Service.getCPT().equals("27093") || working_bltNIM3_Service.getCPT().equals("27095") || working_bltNIM3_Service.getCPT().equals("27096") || working_bltNIM3_Service.getCPT().equals("27370") || working_bltNIM3_Service.getCPT().equals("27648") || working_bltNIM3_Service.getCPT().equals("28899") && payM.getParentPayerID()==tysonParentID || payM.getPayerID()==tysonParentID){
		        					working_bltNIM3_Service.setPaidOutAmount(0.00);
		        					working_bltNIM3_Service.setAllowAmount(0.00);
		        					working_bltNIM3_Service.setBillAmount(0.00);
		        				}
	                        }
	                        
	                        
	                        
                        try
                        {
                            working_bltNIM3_Service.commitData();
                        }
                        catch (Exception e)
                        {
                            myVal +="<hr>**** ERROR: For CPT: " + working_bltNIM3_Service.getCPT() + " ****<hr>";
                        }

                    }

                }
            }
            else
            {
                myVal +=" No Provider Scheduled. Can't run calculation."  ;
            }
        }
        catch (Exception e)
        {
            DebugLogger.printLine("NIMUtils:ReCalcServiceBilling: "+e);
        }
        return myVal;
    }
    

    public static String  ReCalcServiceBilling2 (Integer iEncounterID)
    {
        String myVal = "<hr>Starting V2...";
        try
        {
            NIM3_EncounterObject2 myEO2 = new NIM3_EncounterObject2(iEncounterID,"Load for Billing");
            bltPracticeMaster pm = new bltPracticeMaster(myEO2.getNIM3_Appointment().getProviderID());
            bltNIM3_Service_List        bltNIM3_Service_List        =    new    bltNIM3_Service_List(iEncounterID);
            if (myEO2.getNIM3_Appointment()!=null)
            {

            //declaration of Enumeration
                bltNIM3_Service        working_bltNIM3_Service;
                ListElement         leCurrentElement;
                java.util.Enumeration eList = bltNIM3_Service_List.elements();
                while (eList.hasMoreElements())
                {
                    leCurrentElement    = (ListElement) eList.nextElement();
                    working_bltNIM3_Service  = (bltNIM3_Service) leCurrentElement.getObject();
                    working_bltNIM3_Service.setUniqueModifyComments("NIMUtils:ReCalcServiceBilling2:System Generated");
                    working_bltNIM3_Service.GroupSecurityInit(new Integer(1));
                    if (working_bltNIM3_Service.getServiceStatusID().intValue()==1)
                    {

                        myVal +="<hr>For CPT: " + working_bltNIM3_Service.getCPT() + "";
                        if (working_bltNIM3_Service.getCPT().equalsIgnoreCase(""))
                        {
                            myVal += "[Warning: Blank CPT]";
                        }
                        if (working_bltNIM3_Service.getCPTQty().intValue()<=0)
                        {
                            //working_bltNIM3_Service.setCPTQty(new Integer(1));
                            //myVal +="<br>&nbsp;&nbsp;QTY: " + working_bltNIM3_Service.getCPTQty() + " [UPDATED: Auto-Changed from 0 to 1]";
                        }
                        else
                        {
                            myVal +="<br>&nbsp;&nbsp;QTY: " + working_bltNIM3_Service.getCPTQty() + " [NOT UPDATED]";
                        }

                        //set Allow Amount
                        if (working_bltNIM3_Service.getReference_FeeScheduleAmount()<=0.001){
                            working_bltNIM3_Service.setReference_FeeScheduleAmount( NIMUtils.getFeeSchedulePrice(1,1.0,myEO2.getAppointment_PracticeMaster().getOfficeZIP(),working_bltNIM3_Service.getCPT(),working_bltNIM3_Service.getCPTModifier(),myEO2.getNIM3_Encounter().getDateOfService()));
                        }
                        if (working_bltNIM3_Service.getReference_UCAmount()<=0.001){
                            working_bltNIM3_Service.setReference_UCAmount( NIMUtils.getFeeSchedulePrice(3,1.0,myEO2.getAppointment_PracticeMaster().getOfficeZIP(),working_bltNIM3_Service.getCPT(),working_bltNIM3_Service.getCPTModifier(),myEO2.getNIM3_Encounter().getDateOfService()));
                        }
                        if (working_bltNIM3_Service.getReference_ProviderContractAmount()<=0.001){
                            //we default to Work Comp Pricing for the provider
                            Integer payment_type = NIMUtils.PAYMENT_TYPE_WORKCOMP;
                            if (myEO2.isNextImageDirectCase()){
                                    payment_type = NIMUtils.PAYMENT_TYPE_GROUPHEALTH;
                            }
                            //now we send these into the new PayPrice caculator that takes a PAYMENT_TYPE
                            working_bltNIM3_Service.setReference_ProviderContractAmount(NIMUtils.getCPTPayPriceforPracticeMaster(pm, new CPTObject(working_bltNIM3_Service.getCPT(),working_bltNIM3_Service.getCPTModifier(),working_bltNIM3_Service.getCPTQty()),myEO2.getNIM3_Encounter().getDateOfService(), payment_type));
                        }

                        bltNIM3_NIDPromo temp_promo = null;
                        if (working_bltNIM3_Service.getReference_PayerContractAmount()<=0.001){
                            if (myEO2.isNextImageDirectCase()){
                                //working_bltNIM3_Service.setReference_PayerContractAmount( NIDUtils.NID_MARK_UP * NIMUtils.getPayerAllowAmount(myEO2.getNIM3_CaseAccount().getPayerID(),myEO2.getAppointment_PracticeMaster().getOfficeZIP(),working_bltNIM3_Service.getCPT(),working_bltNIM3_Service.getCPTModifier(),myEO2.getNIM3_Encounter().getDateOfService()) );
//                                working_bltNIM3_Service.setReference_PayerContractAmount( NIDUtils.NID_MARK_UP(pm.getOfficeStateID()) * working_bltNIM3_Service.getReference_ProviderContractAmount());
                                if (temp_promo==null){
                                    temp_promo = new bltNIM3_NIDPromo(myEO2.getNIM3_Referral().getReferralNIDPromoID());
                                }
                                working_bltNIM3_Service.setReference_PayerBillAmount(NIDUtils.getNIDFinalPrice(working_bltNIM3_Service.getReference_ProviderContractAmount(), pm, myEO2.getNIM3_PayerMaster(), temp_promo ));

                            } else {
                                working_bltNIM3_Service.setReference_PayerContractAmount( NIMUtils.getPayerAllowAmount(myEO2.getNIM3_CaseAccount().getPayerID(),myEO2.getAppointment_PracticeMaster().getOfficeZIP(),working_bltNIM3_Service.getCPT(),working_bltNIM3_Service.getCPTModifier(),myEO2.getNIM3_Encounter().getDateOfService()) );
                            }
                        }
                        if (working_bltNIM3_Service.getReference_PayerBillAmount()<=0.001){
                            if (myEO2.isNextImageDirectCase()){
                                //should be the same as the Contract Amount since this is NID
                                if (temp_promo==null){
                                    temp_promo = new bltNIM3_NIDPromo(myEO2.getNIM3_Referral().getReferralNIDPromoID());
                                }
                                working_bltNIM3_Service.setReference_PayerBillAmount(NIDUtils.getNIDFinalPrice(working_bltNIM3_Service.getReference_ProviderContractAmount(), pm, myEO2.getNIM3_PayerMaster(), temp_promo ));
                            } else {
                                working_bltNIM3_Service.setReference_PayerBillAmount( NIMUtils.getPayerBillAmount(myEO2.getNIM3_CaseAccount().getPayerID(),myEO2.getAppointment_PracticeMaster().getOfficeZIP(),working_bltNIM3_Service.getCPT(),working_bltNIM3_Service.getCPTModifier(),myEO2.getNIM3_Encounter().getDateOfService()) );
                            }
                        }

                        try
                        {
                            working_bltNIM3_Service.commitData();
                        }
                        catch (Exception e)
                        {
                            myVal +="<hr>**** ERROR: For CPT: " + working_bltNIM3_Service.getCPT() + " ****<hr>";
                        }

                    }

                }
            }
            else
            {
                myVal +=" No Provider Scheduled. Can't run calculation."  ;
            }
        }
        catch (Exception e)
        {
            DebugLogger.printLine("*********"+e+"**********");
        }
        return myVal;
    }









    public static String getdCPTOptionList(String sCPT)
	{
		String myVal ="<option "; if (sCPT.equalsIgnoreCase("0")) {myVal += " selected ";} myVal += "value=0 >N/A</option>";
myVal +="<option "; if (sCPT.equalsIgnoreCase("Any_Abnormality")) {myVal += " selected ";} myVal += "value=Any_Abnormality>Any_Abnormality</option>";
myVal +="<option "; if (sCPT.equalsIgnoreCase("Anterior_Cruciate_Ligament_Tear")) {myVal += " selected ";} myVal += "value=Anterior_Cruciate_Ligament_Tear>Anterior_Cruciate_Ligament_Tear</option>";
myVal +="<option "; if (sCPT.equalsIgnoreCase("Arthritis")) {myVal += " selected ";} myVal += "value=Arthritis>Arthritis</option>";
myVal +="<option "; if (sCPT.equalsIgnoreCase("Bone_Compression")) {myVal += " selected ";} myVal += "value=Bone_Compression>Bone_Compression</option>";
myVal +="<option "; if (sCPT.equalsIgnoreCase("Carpal_Tunnel_Syndrome")) {myVal += " selected ";} myVal += "value=Carpal_Tunnel_Syndrome>Carpal_Tunnel_Syndrome</option>";
myVal +="<option "; if (sCPT.equalsIgnoreCase("Compressed_Nerve")) {myVal += " selected ";} myVal += "value=Compressed_Nerve>Compressed_Nerve</option>";
myVal +="<option "; if (sCPT.equalsIgnoreCase("Contusion")) {myVal += " selected ";} myVal += "value=Contusion>Contusion</option>";
myVal +="<option "; if (sCPT.equalsIgnoreCase("Crush_Injury")) {myVal += " selected ";} myVal += "value=Crush_Injury>Crush_Injury</option>";
myVal +="<option "; if (sCPT.equalsIgnoreCase("Degenerative_Disc_Disease")) {myVal += " selected ";} myVal += "value=Degenerative_Disc_Disease>Degenerative_Disc_Disease</option>";
myVal +="<option "; if (sCPT.equalsIgnoreCase("Degenerative_Joint_Disease")) {myVal += " selected ";} myVal += "value=Degenerative_Joint_Disease>Degenerative_Joint_Disease</option>";
myVal +="<option "; if (sCPT.equalsIgnoreCase("Displacement_of_Cervical_Disc")) {myVal += " selected ";} myVal += "value=Displacement_of_Cervical_Disc>Displacement_of_Cervical_Disc</option>";
myVal +="<option "; if (sCPT.equalsIgnoreCase("Displacement_of_Thoracic/Lumbar_Disc")) {myVal += " selected ";} myVal += "value=Displacement_of_Thoracic/Lumbar_Disc>Displacement_of_Thoracic/Lumbar_Disc</option>";
myVal +="<option "; if (sCPT.equalsIgnoreCase("Disc_Protrusion")) {myVal += " selected ";} myVal += "value=Disc_Protrusion>Disc_Protrusion</option>";
myVal +="<option "; if (sCPT.equalsIgnoreCase("Dizziness")) {myVal += " selected ";} myVal += "value=Dizziness>Dizziness</option>";
myVal +="<option "; if (sCPT.equalsIgnoreCase("Effusion_of_Joint")) {myVal += " selected ";} myVal += "value=Effusion_of_Joint>Effusion_of_Joint</option>";
myVal +="<option "; if (sCPT.equalsIgnoreCase("Fracture")) {myVal += " selected ";} myVal += "value=Fracture>Fracture</option>";
myVal +="<option "; if (sCPT.equalsIgnoreCase("Headache")) {myVal += " selected ";} myVal += "value=Headache>Headache</option>";
myVal +="<option "; if (sCPT.equalsIgnoreCase("Herniated_Disc")) {myVal += " selected ";} myVal += "value=Herniated_Disc>Herniated_Disc</option>";
myVal +="<option "; if (sCPT.equalsIgnoreCase("HNP")) {myVal += " selected ";} myVal += "value=HNP>HNP</option>";
myVal +="<option "; if (sCPT.equalsIgnoreCase("Internal_Derangement")) {myVal += " selected ";} myVal += "value=Internal_Derangement>Internal_Derangement</option>";
myVal +="<option "; if (sCPT.equalsIgnoreCase("Lateral_Meniscal_Tear")) {myVal += " selected ";} myVal += "value=Lateral_Meniscal_Tear>Lateral_Meniscal_Tear</option>";
myVal +="<option "; if (sCPT.equalsIgnoreCase("Medial_Meniscal_Tear")) {myVal += " selected ";} myVal += "value=Medial_Meniscal_Tear>Medial_Meniscal_Tear</option>";
myVal +="<option "; if (sCPT.equalsIgnoreCase("Myelopathy")) {myVal += " selected ";} myVal += "value=Myelopathy>Myelopathy</option>";
myVal +="<option "; if (sCPT.equalsIgnoreCase("Nerve_Impingement")) {myVal += " selected ";} myVal += "value=Nerve_Impingement>Nerve_Impingement</option>";
myVal +="<option "; if (sCPT.equalsIgnoreCase("Neuropathy")) {myVal += " selected ";} myVal += "value=Neuropathy>Neuropathy</option>";
myVal +="<option "; if (sCPT.equalsIgnoreCase("Numbness")) {myVal += " selected ";} myVal += "value=Numbness>Numbness</option>";
myVal +="<option "; if (sCPT.equalsIgnoreCase("Occult_Fracture")) {myVal += " selected ";} myVal += "value=Occult_Fracture>Occult_Fracture</option>";
myVal +="<option "; if (sCPT.equalsIgnoreCase("Osteomyelitis")) {myVal += " selected ";} myVal += "value=Osteomyelitis>Osteomyelitis</option>";
myVal +="<option "; if (sCPT.equalsIgnoreCase("Osteoporosis")) {myVal += " selected ";} myVal += "value=Osteoporosis>Osteoporosis</option>";
myVal +="<option "; if (sCPT.equalsIgnoreCase("Pain_in_Joint")) {myVal += " selected ";} myVal += "value=Pain_in_Joint>Pain_in_Joint</option>";
myVal +="<option "; if (sCPT.equalsIgnoreCase("Pain_in_Limb,_Arm,_or_Leg")) {myVal += " selected ";} myVal += "value=Pain_in_Limb,_Arm,_or_Leg>Pain_in_Limb,_Arm,_or_Leg</option>";
myVal +="<option "; if (sCPT.equalsIgnoreCase("Pain_in_Neck")) {myVal += " selected ";} myVal += "value=Pain_in_Neck>Pain_in_Neck</option>";
myVal +="<option "; if (sCPT.equalsIgnoreCase("Pain_in_Back")) {myVal += " selected ";} myVal += "value=Pain_in_Back>Pain_in_Back</option>";
myVal +="<option "; if (sCPT.equalsIgnoreCase("Posterior_Cruciate_Ligament_Tear")) {myVal += " selected ";} myVal += "value=Posterior_Cruciate_Ligament_Tear>Posterior_Cruciate_Ligament_Tear</option>";
myVal +="<option "; if (sCPT.equalsIgnoreCase("Radiculopathy")) {myVal += " selected ";} myVal += "value=Radiculopathy>Radiculopathy</option>";
myVal +="<option "; if (sCPT.equalsIgnoreCase("Recurrent_Disc_Disease")) {myVal += " selected ";} myVal += "value=Recurrent_Disc_Disease>Recurrent_Disc_Disease</option>";
myVal +="<option "; if (sCPT.equalsIgnoreCase("Rotator_Cuff_Tear")) {myVal += " selected ";} myVal += "value=Rotator_Cuff_Tear>Rotator_Cuff_Tear</option>";
myVal +="<option "; if (sCPT.equalsIgnoreCase("Sciatica")) {myVal += " selected ";} myVal += "value=Sciatica>Sciatica</option>";
myVal +="<option "; if (sCPT.equalsIgnoreCase("Spinal_Stenosis")) {myVal += " selected ";} myVal += "value=Spinal_Stenosis>Spinal_Stenosis</option>";
myVal +="<option "; if (sCPT.equalsIgnoreCase("Spondylosis")) {myVal += " selected ";} myVal += "value=Spondylosis>Spondylosis</option>";
myVal +="<option "; if (sCPT.equalsIgnoreCase("Sprain_/_Strain")) {myVal += " selected ";} myVal += "value=Sprain_/_Strain>Sprain_/_Strain</option>";
myVal +="<option "; if (sCPT.equalsIgnoreCase("Subdural_Hematoma")) {myVal += " selected ";} myVal += "value=Subdural_Hematoma>Subdural_Hematoma</option>";
myVal +="<option "; if (sCPT.equalsIgnoreCase("Subluxation")) {myVal += " selected ";} myVal += "value=Subluxation>Subluxation</option>";
myVal +="<option "; if (sCPT.equalsIgnoreCase("Tendonitis")) {myVal += " selected ";} myVal += "value=Tendonitis>Tendonitis</option>";
myVal +="<option "; if (sCPT.equalsIgnoreCase("Trefibrocartilage_Tear")) {myVal += " selected ";} myVal += "value=Trefibrocartilage_Tear>Trefibrocartilage_Tear</option>";
myVal +="<option "; if (sCPT.equalsIgnoreCase("Tumor")) {myVal += " selected ";} myVal += "value=Tumor>Tumor</option>";
myVal +="<option "; if (sCPT.equalsIgnoreCase("Weakness")) {myVal += " selected ";} myVal += "value=Weakness>Weakness</option>";
myVal +="<option "; if (sCPT.equalsIgnoreCase("Other")) {myVal += " selected ";} myVal += "value=Other>Other</option>";
		return myVal;
	}
		





    public static String getCPTOptionList(String sCPT)
	{
		String myVal ="<option "; if (sCPT.equalsIgnoreCase("0")) {myVal += " selected ";} myVal += "value=0 >N/A</option>";
		myVal +="<optgroup label=\"MRI/MRA\">";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("74181")) {myVal += " selected ";} myVal += "value=74181>MRI - Abdomen w/o contrast (74181)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("74182")) {myVal += " selected ";} myVal += "value=74182>MRI - Abdomen w/ contrast (74182)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("74183")) {myVal += " selected ";} myVal += "value=74183>MRI - Abdomen w_w/o contrast (74183)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("70551")) {myVal += " selected ";} myVal += "value=70551>MRI - Brain w/o contrast (70551)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("70552")) {myVal += " selected ";} myVal += "value=70552>MRI - Brain w/ contrast (70552)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("70553")) {myVal += " selected ";} myVal += "value=70553>MRI - Brain w_w/o contrast (70553)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("77058")) {myVal += " selected ";} myVal += "value=77058>MRI - Breast Unilateral (77058)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("77059")) {myVal += " selected ";} myVal += "value=77059>MRI - Breast Bilateral (77059)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("72141")) {myVal += " selected ";} myVal += "value=72141>MRI - Cervical w/o contrast (72141)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("72142")) {myVal += " selected ";} myVal += "value=72142>MRI - Cervical w/ contrast (72142)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("72156")) {myVal += " selected ";} myVal += "value=72156>MRI - Cervical w_w/o contrast (72156)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("71550")) {myVal += " selected ";} myVal += "value=71550>MRI - Chest w/o contrast (71550)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("71551")) {myVal += " selected ";} myVal += "value=71551>MRI - Chest w/ contrast (71551)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("71552")) {myVal += " selected ";} myVal += "value=71552>MRI - Chest w_w/o contrast (71552)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("73721")) {myVal += " selected ";} myVal += "value=73721>MRI - Hip w/o contrast (73721)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("73722")) {myVal += " selected ";} myVal += "value=73722>MRI - Hip w/ contrast (73722)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("73723")) {myVal += " selected ";} myVal += "value=73723>MRI - Hip w_w/o contrast (73723)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("73718")) {myVal += " selected ";} myVal += "value=73718>MRI - Lower Extremity w/o contrast (73718)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("73719")) {myVal += " selected ";} myVal += "value=73719>MRI - Lower Extremity w/ contrast (73719)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("73720")) {myVal += " selected ";} myVal += "value=73720>MRI - Lower Extremity w_w/o contrast (73720)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("73721")) {myVal += " selected ";} myVal += "value=73721>MRI - Lower Extremity Joint w/o contrast (73721)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("73722")) {myVal += " selected ";} myVal += "value=73722>MRI - Lower Extremity Joint w/ contrast (73722)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("73723")) {myVal += " selected ";} myVal += "value=73723>MRI - Lower Extremity Joint w_w/o contrast (73723)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("72148")) {myVal += " selected ";} myVal += "value=72148>MRI - Lumbar w/o contrast (72148)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("72149")) {myVal += " selected ";} myVal += "value=72149>MRI - Lumbar w/ contrast (72149)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("72158")) {myVal += " selected ";} myVal += "value=72158>MRI - Lumbar w_w/o contrast (72158)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("70540")) {myVal += " selected ";} myVal += "value=70540>MRI - Orbit Face/Neck w/o contrast (70540)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("70542")) {myVal += " selected ";} myVal += "value=70542>MRI - Orbit Face/Neck w/ contrast (70542)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("70543")) {myVal += " selected ";} myVal += "value=70543>MRI - Orbit Face/Neck w_w/o contrast (70543)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("72195")) {myVal += " selected ";} myVal += "value=72195>MRI - Pelvis w/o contrast (72195)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("72196")) {myVal += " selected ";} myVal += "value=72196>MRI - Pelvis w/ contrast (72196)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("72197")) {myVal += " selected ";} myVal += "value=72197>MRI - Pelvis w_w/o contrast (72197)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("72146")) {myVal += " selected ";} myVal += "value=72146>MRI - Thoracic w/o contrast (72146)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("72147")) {myVal += " selected ";} myVal += "value=72147>MRI - Thoracic w/ contrast (72147)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("72157")) {myVal += " selected ";} myVal += "value=72157>MRI - Thoracic w_w/o contrast (72157)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("70336")) {myVal += " selected ";} myVal += "value=70336>MRI - TMJ (70336)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("73218")) {myVal += " selected ";} myVal += "value=73218>MRI - Upper Extremity w/o contrast (73218)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("73219")) {myVal += " selected ";} myVal += "value=73219>MRI - Upper Extremity w/ contrast (73219)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("73220")) {myVal += " selected ";} myVal += "value=73220>MRI - Upper Extremity w_w/o contrast (73220)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("73221")) {myVal += " selected ";} myVal += "value=73221>MRI - Upper Extremity Joint w/o contrast (73221)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("73222")) {myVal += " selected ";} myVal += "value=73222>MRI - Upper Extremity Joint w/ contrast (73222)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("73223")) {myVal += " selected ";} myVal += "value=73223>MRI - Upper Extremity Joint w_w/o contrast (73223)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("74185")) {myVal += " selected ";} myVal += "value=74185>MRA - Abdomen (74185)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("71555")) {myVal += " selected ";} myVal += "value=71555>MRA - Chest (71555)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("70544")) {myVal += " selected ";} myVal += "value=70544>MRA - Head w/o contrast (70544)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("70545")) {myVal += " selected ";} myVal += "value=70545>MRA - Head w/ contrast (70545)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("70546")) {myVal += " selected ";} myVal += "value=70546>MRA - Head w_w/o contrast (70546)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("73725")) {myVal += " selected ";} myVal += "value=73725>MRA - Lower Extremity (73725)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("70547")) {myVal += " selected ";} myVal += "value=70547>MRA - Neck w/o contrast (70547)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("70548")) {myVal += " selected ";} myVal += "value=70548>MRA - Neck w/ contrast (70548)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("70549")) {myVal += " selected ";} myVal += "value=70549>MRA - Neck w_w/o contrast (70549)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("72198")) {myVal += " selected ";} myVal += "value=72198>MRA - Pelvis (72198)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("73225")) {myVal += " selected ";} myVal += "value=73225>MRA - Upper Extremity (73225)</option>";
		
		myVal +="</optgroup><optgroup label=\"CT\">";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("74176")) {myVal += " selected ";} myVal += "value=74176>CT - Abdomen and Pelvis w/o contrast (74176)</option>";
	    myVal +="<option "; if (sCPT.equalsIgnoreCase("74177")) {myVal += " selected ";} myVal += "value=74177>CT - Abdomen and Pelvis w contrast (74177)</option>";
	    myVal +="<option "; if (sCPT.equalsIgnoreCase("74178")) {myVal += " selected ";} myVal += "value=74178>CT - Abdomen and Pelvis w_wo contrast (74178)</option>";
	    myVal +="<option "; if (sCPT.equalsIgnoreCase("74150")) {myVal += " selected ";} myVal += "value=74150>CT - Abdomen w/o contrast (74150)</option>";
	    myVal +="<option "; if (sCPT.equalsIgnoreCase("74160")) {myVal += " selected ";} myVal += "value=74160>CT - Abdomen w/ contrast (74160)</option>";
	    myVal +="<option "; if (sCPT.equalsIgnoreCase("74170")) {myVal += " selected ";} myVal += "value=74170>CT - Abdomen w_w/o contrast (74170)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("70450")) {myVal += " selected ";} myVal += "value=70450>CT - Brain/Head w/o contrast (70450)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("70460")) {myVal += " selected ";} myVal += "value=70460>CT - Brain/Head w/ contrast (70460)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("70470")) {myVal += " selected ";} myVal += "value=70470>CT - Brain/Head w_ w/o contrast (70470)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("72125")) {myVal += " selected ";} myVal += "value=72125>CT - Cervical w/o contrast (72125)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("72126")) {myVal += " selected ";} myVal += "value=72126>CT - Cervical w/ contrast (72126)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("72127")) {myVal += " selected ";} myVal += "value=72127>CT - Cervical w_w/o contrast (72127)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("71250")) {myVal += " selected ";} myVal += "value=71250>CT - Chest w/o contrast (71250)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("71260")) {myVal += " selected ";} myVal += "value=71260>CT - Chest w/ contrast (71260)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("71270")) {myVal += " selected ";} myVal += "value=71270>CT - Chest w_w/o contrast (71270)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("0146T")) {myVal += " selected ";} myVal += "value=0146T>CT - CTA Coronary Arteries (0146T)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("0147T")) {myVal += " selected ";} myVal += "value=0147T>CT - CTA Coronary Arteries w/ Cal Scoring (0147T)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("0144T")) {myVal += " selected ";} myVal += "value=0144T>CT - CT Heart w/o contrast w/ Cal Scoring (0144T)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("70480")) {myVal += " selected ";} myVal += "value=70480>CT - orbit/ear/fossa w/o contrast (70480)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("70481")) {myVal += " selected ";} myVal += "value=70481>CT - orbit/ear/fossa w/ contrast (70481)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("70482")) {myVal += " selected ";} myVal += "value=70482>CT - orbit/ear/fossa w_w/o contrast (70482)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("70490")) {myVal += " selected ";} myVal += "value=70490>CT - Larynx/Neck w/o contrast (70490)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("70491")) {myVal += " selected ";} myVal += "value=70491>CT - Larynx/Neck w/ contrast (70491)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("70492")) {myVal += " selected ";} myVal += "value=70492>CT - Larynx/Neck w_w/o contrast (70492)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("73700")) {myVal += " selected ";} myVal += "value=73700>CT - Lower Extremity w/o contrast (73700)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("73701")) {myVal += " selected ";} myVal += "value=73701>CT - Lower Extremity w/ contrast (73701)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("73702")) {myVal += " selected ";} myVal += "value=73702>CT - Lower Extremity w_w/o contrast (73702)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("72131")) {myVal += " selected ";} myVal += "value=72131>CT - Lumbar w/o contrast (72131)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("72132")) {myVal += " selected ";} myVal += "value=72132>CT - Lumbar w/ contrast (72132)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("72133")) {myVal += " selected ";} myVal += "value=72133>CT - Lumbar w_w/o contrast (72133)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("70486")) {myVal += " selected ";} myVal += "value=70486>CT - Maxillofacial w/o contrast (70486)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("70487")) {myVal += " selected ";} myVal += "value=70487>CT - Maxillofacial w/ contrast (70487)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("70488")) {myVal += " selected ";} myVal += "value=70488>CT - Maxillofacial w_w/o contrast (70488)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("72192")) {myVal += " selected ";} myVal += "value=72192>CT - Pelvis w/o contrast (72192)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("72193")) {myVal += " selected ";} myVal += "value=72193>CT - Pelvis w/ contrast (72193)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("72194")) {myVal += " selected ";} myVal += "value=72194>CT - Pelvis w_w/o contrast (72194)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("72128")) {myVal += " selected ";} myVal += "value=72128>CT - Thoracic w/o contrast (72128)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("72129")) {myVal += " selected ";} myVal += "value=72129>CT - Thoracic w/ contrast (72129)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("72130")) {myVal += " selected ";} myVal += "value=72130>CT - Thoracic w_w/o contrast (72130)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("73200")) {myVal += " selected ";} myVal += "value=73200>CT - Upper Extremity w/o contrast (73200)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("73201")) {myVal += " selected ";} myVal += "value=73201>CT - Upper Extremity w/ contrast (73201)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("73202")) {myVal += " selected ";} myVal += "value=73202>CT - Upper Extremity w_w/o contrast (73202)</option>";
		
		myVal +="</optgroup><optgroup label=\"X-ray\">";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("73525")) {myVal += " selected ";} myVal += "value=73525>Contrast X-ray of hip (73525)</option>";
		
		myVal +="</optgroup><optgroup label=\"Arthrogram\">";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("77002")) {myVal += " selected ";} myVal += "value=77002>Fluoroscopic(X-Ray) Guidance for needle placement for all other joints (77002)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("76003")) {myVal += " selected ";} myVal += "value=76003>Fluoroscopic(X-Ray) Guidance for needle placement for Spinal procedures only (77003)</option>";
//		myVal +="<option "; if (sCPT.equalsIgnoreCase("76003")) {myVal += " selected ";} myVal += "value=76003>California Only - Fluoroscopic(X-Ray) Guidance for needle placement for all other joints (76003)</option>";
//		myVal +="<option "; if (sCPT.equalsIgnoreCase("76005")) {myVal += " selected ";} myVal += "value=76005>California Only - Fluoroscopic(X-Ray) Guidance for needle placement for Spinal procedures only (76005)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("77012")) {myVal += " selected ";} myVal += "value=77012>CT guidance (If no x-ray at center) (77012)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("23350")) {myVal += " selected ";} myVal += "value=23350>Injection procedure for shoulder arthrography (23350)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("24220")) {myVal += " selected ";} myVal += "value=24220>Injection procedure for elbow arthrography (24220)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("25246")) {myVal += " selected ";} myVal += "value=25246>Injection procedure for wrist arthrography (25246)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("26989")) {myVal += " selected ";} myVal += "value=26989>Injection procedure for hands or fingers arthrography (26989)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("20600")) {myVal += " selected ";} myVal += "value=20600>Injection procedure for hands, fingers or toes arthrography (20600)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("27093")) {myVal += " selected ";} myVal += "value=27093>Injection procedure for hip arthrography: without anesthesia (27093)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("27095")) {myVal += " selected ";} myVal += "value=27095>Injection procedure for hip arthrography: with anesthesia (27095)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("27096")) {myVal += " selected ";} myVal += "value=27096>Injection procedure for sacro-iliac joint arthrography (27096)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("27370")) {myVal += " selected ";} myVal += "value=27370>Injection procedure for knee arthrography (27370)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("27648")) {myVal += " selected ";} myVal += "value=27648>Injection procedure for ankle arthrography (27648)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("28899")) {myVal += " selected ";} myVal += "value=28899>Injection procedure for foot or toe arthrography (28899)</option>";
		
		myVal +="</optgroup><optgroup label=\"Myelogram\">";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("62284")) {myVal += " selected ";} myVal += "value=62284>Spinal Injection for myelography (62284)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("72240")) {myVal += " selected ";} myVal += "value=72240>Myelography Cervical supervision & interpretation (72240)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("72255")) {myVal += " selected ";} myVal += "value=72255>Myelography Thoracic supervision & interpretation (72255)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("72265")) {myVal += " selected ";} myVal += "value=72265>Myelography Lumbrosacral supervision & interpretation (72265)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("72270")) {myVal += " selected ";} myVal += "value=72270>Myelography 2+ regions supervision & interpretation (72270)</option>";
		
		myVal +="</optgroup><optgroup label=\"EMG\">";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("95885")) {myVal += " selected ";} myVal += "value=95885>EMG Extremity w/ NCS Limited >5 (95885)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("95886")) {myVal += " selected ";} myVal += "value=95886>EMG Extremity w/ NCS Complete 5+ (95886)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("95887")) {myVal += " selected ";} myVal += "value=95887>EMG Non-Extremity w/ NCS (95887)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("95860")) {myVal += " selected ";} myVal += "value=95860>EMG Only - Unilateral (95860)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("95861")) {myVal += " selected ";} myVal += "value=95861>EMG Only - Bilateral (95861)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("95864")) {myVal += " selected ";} myVal += "value=95864>EMG Only - All 4 Limbs (95864)</option>";
		
		myVal +="</optgroup><optgroup label=\"NCS\">";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("95903")) {myVal += " selected ";} myVal += "value=95903>1-2 Nerve conduction with F-wave study (95903)</option>";//Most Recent Change
		myVal +="<option "; if (sCPT.equalsIgnoreCase("95907")) {myVal += " selected ";} myVal += "value=95907>1-2 Nerve Conduction Studies (95907)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("95908")) {myVal += " selected ";} myVal += "value=95908>3-4 Nerve Conduction Studies (95908)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("95909")) {myVal += " selected ";} myVal += "value=95909>5-6 Nerve Conduction Studies (95909)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("95910")) {myVal += " selected ";} myVal += "value=95910>7-8 Nerve Conduction Studies (95910)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("95911")) {myVal += " selected ";} myVal += "value=95911>9-10 Nerve Conduction Studies (95911)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("95913")) {myVal += " selected ";} myVal += "value=95913>13 or more Nerve Conduction Studies (95913)</option>";
		
		myVal +="</optgroup><optgroup label=\"Old EMG\">";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("95860")) {myVal += " selected ";} myVal += "value=95860>EMG Unilateral Extremity (95860)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("95861")) {myVal += " selected ";} myVal += "value=95861>EMG Bilateral Extremities (95861)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("95864")) {myVal += " selected ";} myVal += "value=95864>EMG All 4 limbs (95864)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("95900")) {myVal += " selected ";} myVal += "value=95900>Motor NCV (95900)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("95904")) {myVal += " selected ";} myVal += "value=95904>Sensory Nerve Conduction (95904)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("95934")) {myVal += " selected ";} myVal += "value=95934>H Reflex (95934)</option>";

		
		myVal +="</optgroup><optgroup label=\"Consultation\">";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("76140")) {myVal += " selected ";} myVal += "value=76140>Age of Injury (76140)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("76140")) {myVal += " selected ";} myVal += "value=76140>Second Opinion (76140)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("76140")) {myVal += " selected ";} myVal += "value=76140>Comparison (76140)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("76140")) {myVal += " selected ";} myVal += "value=76140>Reread (76140)</option>";
		
		myVal +="</optgroup><optgroup label=\"DEXA\">";		
		myVal +="<option "; if (sCPT.equalsIgnoreCase("77080")) {myVal += " selected ";} myVal += "value=77080>DEXA - DEXA Scan (77080)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("77080")) {myVal += " selected ";} myVal += "value=77080>DEXA - DEXA w/Vertebral Height (77080)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("77082")) {myVal += " selected ";} myVal += "value=77082>DEXA - Vertebral Fracture Assessment (77082)</option>";
		
		myVal +="</optgroup><optgroup label=\"FL\">";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("74270")) {myVal += " selected ";} myVal += "value=74270>FL - Barium Enema (74270)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("74280")) {myVal += " selected ";} myVal += "value=74280>FL - Barium Enema A/C (74280)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("74220")) {myVal += " selected ";} myVal += "value=74220>FL - Esophagram (74220)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("74415")) {myVal += " selected ";} myVal += "value=74415>FL - IVP w/ Tomography (74415)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("74250")) {myVal += " selected ";} myVal += "value=74250>FL - Small Bowel Series (74250)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("74245")) {myVal += " selected ";} myVal += "value=74245>FL - UGI w/ Small Bowel Series (74245)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("74249")) {myVal += " selected ";} myVal += "value=74249>FL - UGI w/ Small Bowel Series DBL (74249)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("74241")) {myVal += " selected ";} myVal += "value=74241>FL - Upper GI (74241)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("74247")) {myVal += " selected ";} myVal += "value=74247>FL - Upper GI DBL (74247)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("74455")) {myVal += " selected ";} myVal += "value=74455>FL - Voiding Cystogram (74455)</option>";
		
		myVal +="</optgroup><optgroup label=\"NM\">";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("78300")) {myVal += " selected ";} myVal += "value=78300>NM - Limited Area Bone Scan (78300)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("78305")) {myVal += " selected ";} myVal += "value=78305>NM - Bone &/or Joint Scan_Multiple (78305)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("78306")) {myVal += " selected ";} myVal += "value=78306>NM - Bone &/or Joint Scan_Whole Body (78306)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("78315")) {myVal += " selected ";} myVal += "value=78315>NM - Three Phase Bone Scan (78315)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("78223")) {myVal += " selected ";} myVal += "value=78223>NM - Hida Scan (Hepatobiliary Ductual System) (78223)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("78007")) {myVal += " selected ";} myVal += "value=78007>NM - Thyroid Scan w/ Uptake (78007)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("78320")) {myVal += " selected ";} myVal += "value=78320>NM - Bone SPECT (78320)</option>";
		
		myVal +="</optgroup><optgroup label=\"PET\">";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("78811")) {myVal += " selected ";} myVal += "value=78811>PET - Limited Area (e.g., Chest, head, neck) (78811)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("78812")) {myVal += " selected ";} myVal += "value=78812>PET - Skull Base to Mid-Thigh (78812)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("78813")) {myVal += " selected ";} myVal += "value=78813>PET - Whole Body (78813)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("78814")) {myVal += " selected ";} myVal += "value=78814>PET - PET w/concurrent CT, Limited Area (78814)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("78815")) {myVal += " selected ";} myVal += "value=78815>PET - PET w/concurrent CT, Skull base-mid thigh (78815)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("78816")) {myVal += " selected ";} myVal += "value=78816>PET - PET w/concurrent CT, Whole Body (78816)</option>";
		
		myVal +="</optgroup><optgroup label=\"Special\">";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("73615")) {myVal += " selected ";} myVal += "value=73615>Special - Arthrogram Ankle (73615)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("73115")) {myVal += " selected ";} myVal += "value=73115>Special - Arthrogram Wrist (73115)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("73040")) {myVal += " selected ";} myVal += "value=73040>Special - Arthrogram Shoulder (73040)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("73085")) {myVal += " selected ";} myVal += "value=73085>Special - Arthrogram Elbow (73085)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("74740")) {myVal += " selected ";} myVal += "value=74740>Special - HSG (Hysterosalpinogram) (74740)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("72240")) {myVal += " selected ";} myVal += "value=72240>Special - Myelogram Cervical 62284 (72240)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("72255")) {myVal += " selected ";} myVal += "value=72255>Special - Myelogram Thoracic 62284 (72255)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("72265")) {myVal += " selected ";} myVal += "value=72265>Special - Myelogram Lumbar 62284 (72265)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("72270")) {myVal += " selected ";} myVal += "value=72270>Special - Myelography (2 or more) 62284 (72270)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("75822")) {myVal += " selected ";} myVal += "value=75822>Special - Venogram Bilateral 36005 (75822)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("75820")) {myVal += " selected ";} myVal += "value=75820>Special - Venogram Unilateral 36005 (75820)</option>";
		
		myVal +="</optgroup><optgroup label=\"Ultrasound\">";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("76700")) {myVal += " selected ";} myVal += "value=76700>US - Abdomen Complete (76700)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("93922")) {myVal += " selected ";} myVal += "value=93922>US - Arterial Upper or Lower - ABI only (93922)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("93923")) {myVal += " selected ";} myVal += "value=93923>US - Arterial Rest/Stress upper or lower (93923)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("93925")) {myVal += " selected ";} myVal += "value=93925>US - Arterial Bilateral Lower Extremity (93925)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("93930")) {myVal += " selected ";} myVal += "value=93930>US - Arterial Bilateral Upper Extremity (93930)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("93990")) {myVal += " selected ";} myVal += "value=93990>US - Arterial Hemodialysis Access (93990)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("93926")) {myVal += " selected ";} myVal += "value=93926>US - Arterial Unilateral Lower Extremity (93926)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("93931")) {myVal += " selected ";} myVal += "value=93931>US - Arterial Unilateral Upper Extremity (93931)</option>";
		
		//myVal +="<option "; if (sCPT.equalsIgnoreCase("76645")) {myVal += " selected ";} myVal += "value=76645>US - Breast (76645)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("76642")) {myVal += " selected ";} myVal += "value=76642>US - Bilateral Breast (76642)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("76641")) {myVal += " selected ";} myVal += "value=76641>US - Breast Unilateral (76641)</option>";
		
		myVal +="<option "; if (sCPT.equalsIgnoreCase("93880")) {myVal += " selected ";} myVal += "value=93880>US - Carotid Bilateral (93880)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("76881")) {myVal += " selected ";} myVal += "value=76881>US - Extremity_Soft Tissue (76881)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("76705")) {myVal += " selected ";} myVal += "value=76705>US - Gallbladder (76705)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("76857")) {myVal += " selected ";} myVal += "value=76857>US - Kidneys/Renal/Bladder (76857)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("76705")) {myVal += " selected ";} myVal += "value=76705>US - Liver (76705)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("76856")) {myVal += " selected ";} myVal += "value=76856>US - Pelvic (Non Obstetric) (76856)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("76801")) {myVal += " selected ";} myVal += "value=76801>US - Pregnancy Single or < 14 weeks (76801)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("76802")) {myVal += " selected ";} myVal += "value=76802>US - Pregnancy Each Additional Gestation (76802)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("76805")) {myVal += " selected ";} myVal += "value=76805>US - Pregnancy > or = to 14 weeks (76805)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("76811")) {myVal += " selected ";} myVal += "value=76811>US - Pregnancy Complete, Single - Detail (76811)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("76812")) {myVal += " selected ";} myVal += "value=76812>US - Pregnancy Complete, Additional - Detail (76812)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("76817")) {myVal += " selected ";} myVal += "value=76817>US - Pregnancy Transvaginal (76817)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("76381")) {myVal += " selected ";} myVal += "value=76381>US - Sonohysterography (Saline Infusion) (76381)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("76870")) {myVal += " selected ";} myVal += "value=76870>US - Testicular (76870)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("76536")) {myVal += " selected ";} myVal += "value=76536>US - Thyroid (76536)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("76830")) {myVal += " selected ";} myVal += "value=76830>US - Transvaginal (76830)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("93970")) {myVal += " selected ";} myVal += "value=93970>US - Venous Extremity Bilateral (93970)</option>";
		myVal +="<option "; if (sCPT.equalsIgnoreCase("93971")) {myVal += " selected ";} myVal += "value=93971>US - Venous Extremity Unilateral (93971)</option>";
		myVal +="</optgroup>";
		
		return myVal;
	}
		


    public static String getWZ_CPT(String sWID)
	{
		String myVal="Invalid";
		if (sWID.equalsIgnoreCase("1")) {myVal= ("78305");}
		else if (sWID.equalsIgnoreCase("2")) {myVal= ("78306");}
		else if (sWID.equalsIgnoreCase("3")) {myVal= ("78315");}
		else if (sWID.equalsIgnoreCase("4")) {myVal= ("78223");}
		else if (sWID.equalsIgnoreCase("5")) {myVal= ("78007");}
		else if (sWID.equalsIgnoreCase("6")) {myVal= ("78811");}
		else if (sWID.equalsIgnoreCase("7")) {myVal= ("78812");}
		else if (sWID.equalsIgnoreCase("8")) {myVal= ("78813");}
		else if (sWID.equalsIgnoreCase("9")) {myVal= ("78814");}
		else if (sWID.equalsIgnoreCase("10")) {myVal= ("78815");}
		else if (sWID.equalsIgnoreCase("11")) {myVal= ("78816");}
		else if (sWID.equalsIgnoreCase("12")) {myVal= ("76700");}
		else if (sWID.equalsIgnoreCase("13")) {myVal= ("93922");}
		else if (sWID.equalsIgnoreCase("14")) {myVal= ("93923");}
		else if (sWID.equalsIgnoreCase("15")) {myVal= ("93925");}
		else if (sWID.equalsIgnoreCase("16")) {myVal= ("93930");}
		else if (sWID.equalsIgnoreCase("17")) {myVal= ("93990");}
		else if (sWID.equalsIgnoreCase("18")) {myVal= ("93926");}
		else if (sWID.equalsIgnoreCase("19")) {myVal= ("93931");}
		else if (sWID.equalsIgnoreCase("20")) {myVal= ("76642");}//changed
		else if (sWID.equalsIgnoreCase("21")) {myVal= ("93880");}
		else if (sWID.equalsIgnoreCase("22")) {myVal= ("76880");}
		else if (sWID.equalsIgnoreCase("23")) {myVal= ("76705");}
		else if (sWID.equalsIgnoreCase("24")) {myVal= ("76770");}
		else if (sWID.equalsIgnoreCase("25")) {myVal= ("76705");}
		else if (sWID.equalsIgnoreCase("26")) {myVal= ("76856");}
		else if (sWID.equalsIgnoreCase("27")) {myVal= ("76801");}
		else if (sWID.equalsIgnoreCase("28")) {myVal= ("76802");}
		else if (sWID.equalsIgnoreCase("29")) {myVal= ("76805");}
		else if (sWID.equalsIgnoreCase("30")) {myVal= ("76811");}
		else if (sWID.equalsIgnoreCase("31")) {myVal= ("76812");}
		else if (sWID.equalsIgnoreCase("32")) {myVal= ("76817");}
		else if (sWID.equalsIgnoreCase("33")) {myVal= ("76381");}
		else if (sWID.equalsIgnoreCase("34")) {myVal= ("76870");}
		else if (sWID.equalsIgnoreCase("35")) {myVal= ("76536");}
		else if (sWID.equalsIgnoreCase("36")) {myVal= ("76830");}
		else if (sWID.equalsIgnoreCase("37")) {myVal= ("93970");}
		else if (sWID.equalsIgnoreCase("38")) {myVal= ("93971");}
		else if (sWID.equalsIgnoreCase("39")) {myVal= ("73615");}
		else if (sWID.equalsIgnoreCase("40")) {myVal= ("73115");}
		else if (sWID.equalsIgnoreCase("41")) {myVal= ("73040");}
		else if (sWID.equalsIgnoreCase("42")) {myVal= ("73085");}
		else if (sWID.equalsIgnoreCase("43")) {myVal= ("73615");}
		else if (sWID.equalsIgnoreCase("44")) {myVal= ("73115");}
		else if (sWID.equalsIgnoreCase("45")) {myVal= ("73040");}
		else if (sWID.equalsIgnoreCase("46")) {myVal= ("73085");}
		else if (sWID.equalsIgnoreCase("47")) {myVal= ("74740");}
		else if (sWID.equalsIgnoreCase("48")) {myVal= ("72240");}
		else if (sWID.equalsIgnoreCase("49")) {myVal= ("72255");}
		else if (sWID.equalsIgnoreCase("50")) {myVal= ("72265");}
		else if (sWID.equalsIgnoreCase("51")) {myVal= ("72270");}
		else if (sWID.equalsIgnoreCase("52")) {myVal= ("75822");}
		else if (sWID.equalsIgnoreCase("53")) {myVal= ("75820");}
		else if (sWID.equalsIgnoreCase("54")) {myVal= ("74181");}
		else if (sWID.equalsIgnoreCase("55")) {myVal= ("74182");}
		else if (sWID.equalsIgnoreCase("56")) {myVal= ("74183");}
		else if (sWID.equalsIgnoreCase("57")) {myVal= ("70551");}
		else if (sWID.equalsIgnoreCase("58")) {myVal= ("70552");}
		else if (sWID.equalsIgnoreCase("59")) {myVal= ("70553");}
		else if (sWID.equalsIgnoreCase("60")) {myVal= ("77058");}
		else if (sWID.equalsIgnoreCase("61")) {myVal= ("77059");}
		else if (sWID.equalsIgnoreCase("62")) {myVal= ("72141");}
		else if (sWID.equalsIgnoreCase("63")) {myVal= ("72142");}
		else if (sWID.equalsIgnoreCase("64")) {myVal= ("72156");}
		else if (sWID.equalsIgnoreCase("65")) {myVal= ("71550");}
		else if (sWID.equalsIgnoreCase("66")) {myVal= ("71551");}
		else if (sWID.equalsIgnoreCase("67")) {myVal= ("71552");}
		else if (sWID.equalsIgnoreCase("68")) {myVal= ("73721");}
		else if (sWID.equalsIgnoreCase("69")) {myVal= ("73722");}
		else if (sWID.equalsIgnoreCase("70")) {myVal= ("73723");}
		else if (sWID.equalsIgnoreCase("71")) {myVal= ("73721");}
		else if (sWID.equalsIgnoreCase("72")) {myVal= ("73722");}
		else if (sWID.equalsIgnoreCase("73")) {myVal= ("73723");}
		else if (sWID.equalsIgnoreCase("74")) {myVal= ("73721");}
		else if (sWID.equalsIgnoreCase("75")) {myVal= ("73722");}
		else if (sWID.equalsIgnoreCase("76")) {myVal= ("73723");}
		else if (sWID.equalsIgnoreCase("77")) {myVal= ("73721");}
		else if (sWID.equalsIgnoreCase("78")) {myVal= ("73722");}
		else if (sWID.equalsIgnoreCase("79")) {myVal= ("73723");}
		else if (sWID.equalsIgnoreCase("80")) {myVal= ("73721");}
		else if (sWID.equalsIgnoreCase("81")) {myVal= ("73722");}
		else if (sWID.equalsIgnoreCase("82")) {myVal= ("73723");}
		else if (sWID.equalsIgnoreCase("83")) {myVal= ("73721");}
		else if (sWID.equalsIgnoreCase("84")) {myVal= ("73722");}
		else if (sWID.equalsIgnoreCase("85")) {myVal= ("73723");}
		else if (sWID.equalsIgnoreCase("86")) {myVal= ("73718");}
		else if (sWID.equalsIgnoreCase("87")) {myVal= ("73719");}
		else if (sWID.equalsIgnoreCase("88")) {myVal= ("73720");}
		else if (sWID.equalsIgnoreCase("89")) {myVal= ("73718");}
		else if (sWID.equalsIgnoreCase("90")) {myVal= ("73719");}
		else if (sWID.equalsIgnoreCase("91")) {myVal= ("73720");}
		else if (sWID.equalsIgnoreCase("92")) {myVal= ("73718");}
		else if (sWID.equalsIgnoreCase("93")) {myVal= ("73719");}
		else if (sWID.equalsIgnoreCase("94")) {myVal= ("73720");}
		else if (sWID.equalsIgnoreCase("95")) {myVal= ("73718");}
		else if (sWID.equalsIgnoreCase("96")) {myVal= ("73719");}
		else if (sWID.equalsIgnoreCase("97")) {myVal= ("73720");}
		else if (sWID.equalsIgnoreCase("98")) {myVal= ("73718");}
		else if (sWID.equalsIgnoreCase("99")) {myVal= ("73719");}
		else if (sWID.equalsIgnoreCase("100")) {myVal= ("73720");}
		else if (sWID.equalsIgnoreCase("101")) {myVal= ("73718");}
		else if (sWID.equalsIgnoreCase("102")) {myVal= ("73719");}
		else if (sWID.equalsIgnoreCase("103")) {myVal= ("73720");}
		else if (sWID.equalsIgnoreCase("104")) {myVal= ("72148");}
		else if (sWID.equalsIgnoreCase("105")) {myVal= ("72149");}
		else if (sWID.equalsIgnoreCase("106")) {myVal= ("72158");}
		else if (sWID.equalsIgnoreCase("107")) {myVal= ("70540");}
		else if (sWID.equalsIgnoreCase("108")) {myVal= ("70542");}
		else if (sWID.equalsIgnoreCase("109")) {myVal= ("70543");}
		else if (sWID.equalsIgnoreCase("110")) {myVal= ("72195");}
		else if (sWID.equalsIgnoreCase("111")) {myVal= ("72196");}
		else if (sWID.equalsIgnoreCase("112")) {myVal= ("72197");}
		else if (sWID.equalsIgnoreCase("113")) {myVal= ("72146");}
		else if (sWID.equalsIgnoreCase("114")) {myVal= ("72147");}
		else if (sWID.equalsIgnoreCase("115")) {myVal= ("72157");}
		else if (sWID.equalsIgnoreCase("116")) {myVal= ("70336");}
		else if (sWID.equalsIgnoreCase("117")) {myVal= ("73218");}
		else if (sWID.equalsIgnoreCase("118")) {myVal= ("73219");}
		else if (sWID.equalsIgnoreCase("119")) {myVal= ("73220");}
		else if (sWID.equalsIgnoreCase("120")) {myVal= ("73218");}
		else if (sWID.equalsIgnoreCase("121")) {myVal= ("73219");}
		else if (sWID.equalsIgnoreCase("122")) {myVal= ("73220");}
		else if (sWID.equalsIgnoreCase("123")) {myVal= ("73218");}
		else if (sWID.equalsIgnoreCase("124")) {myVal= ("73219");}
		else if (sWID.equalsIgnoreCase("125")) {myVal= ("73220");}
		else if (sWID.equalsIgnoreCase("126")) {myVal= ("73218");}
		else if (sWID.equalsIgnoreCase("127")) {myVal= ("73219");}
		else if (sWID.equalsIgnoreCase("128")) {myVal= ("73220");}
		else if (sWID.equalsIgnoreCase("129")) {myVal= ("73218");}
		else if (sWID.equalsIgnoreCase("130")) {myVal= ("73219");}
		else if (sWID.equalsIgnoreCase("131")) {myVal= ("73220");}
		else if (sWID.equalsIgnoreCase("132")) {myVal= ("73218");}
		else if (sWID.equalsIgnoreCase("133")) {myVal= ("73219");}
		else if (sWID.equalsIgnoreCase("134")) {myVal= ("73220");}
		else if (sWID.equalsIgnoreCase("135")) {myVal= ("73221");}
		else if (sWID.equalsIgnoreCase("136")) {myVal= ("73222");}
		else if (sWID.equalsIgnoreCase("137")) {myVal= ("73223");}
		else if (sWID.equalsIgnoreCase("138")) {myVal= ("73221");}
		else if (sWID.equalsIgnoreCase("139")) {myVal= ("73222");}
		else if (sWID.equalsIgnoreCase("140")) {myVal= ("73223");}
		else if (sWID.equalsIgnoreCase("141")) {myVal= ("73221");}
		else if (sWID.equalsIgnoreCase("142")) {myVal= ("73222");}
		else if (sWID.equalsIgnoreCase("143")) {myVal= ("73223");}
		else if (sWID.equalsIgnoreCase("144")) {myVal= ("73221");}
		else if (sWID.equalsIgnoreCase("145")) {myVal= ("73222");}
		else if (sWID.equalsIgnoreCase("146")) {myVal= ("73223");}
		else if (sWID.equalsIgnoreCase("147")) {myVal= ("73221");}
		else if (sWID.equalsIgnoreCase("148")) {myVal= ("73222");}
		else if (sWID.equalsIgnoreCase("149")) {myVal= ("73223");}
		else if (sWID.equalsIgnoreCase("150")) {myVal= ("73221");}
		else if (sWID.equalsIgnoreCase("151")) {myVal= ("73222");}
		else if (sWID.equalsIgnoreCase("152")) {myVal= ("73223");}
		else if (sWID.equalsIgnoreCase("153")) {myVal= ("74185");}
		else if (sWID.equalsIgnoreCase("154")) {myVal= ("71555");}
		else if (sWID.equalsIgnoreCase("155")) {myVal= ("70544");}
		else if (sWID.equalsIgnoreCase("156")) {myVal= ("70545");}
		else if (sWID.equalsIgnoreCase("157")) {myVal= ("70546");}
		else if (sWID.equalsIgnoreCase("158")) {myVal= ("73725");}
		else if (sWID.equalsIgnoreCase("159")) {myVal= ("70547");}
		else if (sWID.equalsIgnoreCase("160")) {myVal= ("70548");}
		else if (sWID.equalsIgnoreCase("161")) {myVal= ("70549");}
		else if (sWID.equalsIgnoreCase("162")) {myVal= ("72198");}
		else if (sWID.equalsIgnoreCase("163")) {myVal= ("73225");}
		else if (sWID.equalsIgnoreCase("164")) {myVal= ("74150");}
		else if (sWID.equalsIgnoreCase("165")) {myVal= ("74160");}
		else if (sWID.equalsIgnoreCase("166")) {myVal= ("74170");}
		else if (sWID.equalsIgnoreCase("167")) {myVal= ("70450");}
		else if (sWID.equalsIgnoreCase("168")) {myVal= ("70460");}
		else if (sWID.equalsIgnoreCase("169")) {myVal= ("70470");}
		else if (sWID.equalsIgnoreCase("170")) {myVal= ("72125");}
		else if (sWID.equalsIgnoreCase("171")) {myVal= ("72126");}
		else if (sWID.equalsIgnoreCase("172")) {myVal= ("72127");}
		else if (sWID.equalsIgnoreCase("173")) {myVal= ("71250");}
		else if (sWID.equalsIgnoreCase("174")) {myVal= ("71260");}
		else if (sWID.equalsIgnoreCase("175")) {myVal= ("71270");}
		else if (sWID.equalsIgnoreCase("176")) {myVal= ("0146T");}
		else if (sWID.equalsIgnoreCase("177")) {myVal= ("0147T");}
		else if (sWID.equalsIgnoreCase("178")) {myVal= ("0144T");}
		else if (sWID.equalsIgnoreCase("179")) {myVal= ("70480");}
		else if (sWID.equalsIgnoreCase("180")) {myVal= ("70481");}
		else if (sWID.equalsIgnoreCase("181")) {myVal= ("70482");}
		else if (sWID.equalsIgnoreCase("182")) {myVal= ("70490");}
		else if (sWID.equalsIgnoreCase("183")) {myVal= ("70491");}
		else if (sWID.equalsIgnoreCase("184")) {myVal= ("70492");}
		else if (sWID.equalsIgnoreCase("185")) {myVal= ("73700");}
		else if (sWID.equalsIgnoreCase("186")) {myVal= ("73701");}
		else if (sWID.equalsIgnoreCase("187")) {myVal= ("73702");}
		else if (sWID.equalsIgnoreCase("188")) {myVal= ("73700");}
		else if (sWID.equalsIgnoreCase("189")) {myVal= ("73701");}
		else if (sWID.equalsIgnoreCase("190")) {myVal= ("73702");}
		else if (sWID.equalsIgnoreCase("191")) {myVal= ("73700");}
		else if (sWID.equalsIgnoreCase("192")) {myVal= ("73701");}
		else if (sWID.equalsIgnoreCase("193")) {myVal= ("73702");}
		else if (sWID.equalsIgnoreCase("194")) {myVal= ("73700");}
		else if (sWID.equalsIgnoreCase("195")) {myVal= ("73701");}
		else if (sWID.equalsIgnoreCase("196")) {myVal= ("73702");}
		else if (sWID.equalsIgnoreCase("197")) {myVal= ("73700");}
		else if (sWID.equalsIgnoreCase("198")) {myVal= ("73701");}
		else if (sWID.equalsIgnoreCase("199")) {myVal= ("73702");}
		else if (sWID.equalsIgnoreCase("200")) {myVal= ("73700");}
		else if (sWID.equalsIgnoreCase("201")) {myVal= ("73701");}
		else if (sWID.equalsIgnoreCase("202")) {myVal= ("73702");}
		else if (sWID.equalsIgnoreCase("203")) {myVal= ("73700");}
		else if (sWID.equalsIgnoreCase("204")) {myVal= ("73701");}
		else if (sWID.equalsIgnoreCase("205")) {myVal= ("73702");}
		else if (sWID.equalsIgnoreCase("206")) {myVal= ("73700");}
		else if (sWID.equalsIgnoreCase("207")) {myVal= ("73701");}
		else if (sWID.equalsIgnoreCase("208")) {myVal= ("73702");}
		else if (sWID.equalsIgnoreCase("209")) {myVal= ("73700");}
		else if (sWID.equalsIgnoreCase("210")) {myVal= ("73701");}
		else if (sWID.equalsIgnoreCase("211")) {myVal= ("73702");}
		else if (sWID.equalsIgnoreCase("212")) {myVal= ("73700");}
		else if (sWID.equalsIgnoreCase("213")) {myVal= ("73701");}
		else if (sWID.equalsIgnoreCase("214")) {myVal= ("73702");}
		else if (sWID.equalsIgnoreCase("215")) {myVal= ("73700");}
		else if (sWID.equalsIgnoreCase("216")) {myVal= ("73701");}
		else if (sWID.equalsIgnoreCase("217")) {myVal= ("73702");}
		else if (sWID.equalsIgnoreCase("218")) {myVal= ("73700");}
		else if (sWID.equalsIgnoreCase("219")) {myVal= ("73701");}
		else if (sWID.equalsIgnoreCase("220")) {myVal= ("73702");}
		else if (sWID.equalsIgnoreCase("221")) {myVal= ("73700");}
		else if (sWID.equalsIgnoreCase("222")) {myVal= ("73701");}
		else if (sWID.equalsIgnoreCase("223")) {myVal= ("73702");}
		else if (sWID.equalsIgnoreCase("224")) {myVal= ("72131");}
		else if (sWID.equalsIgnoreCase("225")) {myVal= ("72132");}
		else if (sWID.equalsIgnoreCase("226")) {myVal= ("72133");}
		else if (sWID.equalsIgnoreCase("227")) {myVal= ("70486");}
		else if (sWID.equalsIgnoreCase("228")) {myVal= ("70487");}
		else if (sWID.equalsIgnoreCase("229")) {myVal= ("70488");}
		else if (sWID.equalsIgnoreCase("230")) {myVal= ("72192");}
		else if (sWID.equalsIgnoreCase("231")) {myVal= ("72193");}
		else if (sWID.equalsIgnoreCase("232")) {myVal= ("72194");}
		else if (sWID.equalsIgnoreCase("233")) {myVal= ("72128");}
		else if (sWID.equalsIgnoreCase("234")) {myVal= ("72129");}
		else if (sWID.equalsIgnoreCase("235")) {myVal= ("72130");}
		else if (sWID.equalsIgnoreCase("236")) {myVal= ("73200");}
		else if (sWID.equalsIgnoreCase("237")) {myVal= ("73201");}
		else if (sWID.equalsIgnoreCase("238")) {myVal= ("73202");}
		else if (sWID.equalsIgnoreCase("239")) {myVal= ("73200");}
		else if (sWID.equalsIgnoreCase("240")) {myVal= ("73201");}
		else if (sWID.equalsIgnoreCase("241")) {myVal= ("73202");}
		else if (sWID.equalsIgnoreCase("242")) {myVal= ("73200");}
		else if (sWID.equalsIgnoreCase("243")) {myVal= ("73201");}
		else if (sWID.equalsIgnoreCase("244")) {myVal= ("73202");}
		else if (sWID.equalsIgnoreCase("245")) {myVal= ("73200");}
		else if (sWID.equalsIgnoreCase("246")) {myVal= ("73201");}
		else if (sWID.equalsIgnoreCase("247")) {myVal= ("73202");}
		else if (sWID.equalsIgnoreCase("248")) {myVal= ("73200");}
		else if (sWID.equalsIgnoreCase("249")) {myVal= ("73201");}
		else if (sWID.equalsIgnoreCase("250")) {myVal= ("73202");}
		else if (sWID.equalsIgnoreCase("251")) {myVal= ("73200");}
		else if (sWID.equalsIgnoreCase("252")) {myVal= ("73201");}
		else if (sWID.equalsIgnoreCase("253")) {myVal= ("73202");}
		else if (sWID.equalsIgnoreCase("254")) {myVal= ("77080");}
		else if (sWID.equalsIgnoreCase("255")) {myVal= ("77080");}
		else if (sWID.equalsIgnoreCase("256")) {myVal= ("77082");}
		else if (sWID.equalsIgnoreCase("257")) {myVal= ("74270");}
		else if (sWID.equalsIgnoreCase("258")) {myVal= ("74280");}
		else if (sWID.equalsIgnoreCase("259")) {myVal= ("74220");}
		else if (sWID.equalsIgnoreCase("260")) {myVal= ("74415");}
		else if (sWID.equalsIgnoreCase("261")) {myVal= ("74250");}
		else if (sWID.equalsIgnoreCase("262")) {myVal= ("74245");}
		else if (sWID.equalsIgnoreCase("263")) {myVal= ("74249");}
		else if (sWID.equalsIgnoreCase("264")) {myVal= ("74241");}
		else if (sWID.equalsIgnoreCase("265")) {myVal= ("74247");}
		else if (sWID.equalsIgnoreCase("266")) {myVal= ("74455");}
		return myVal;
	}

    public static String getWZ_BP(String sWID)
	{
		String myVal="Invalid";
		if (sWID.equalsIgnoreCase("1")) {myVal= ("Multiple");}
		else if (sWID.equalsIgnoreCase("2")) {myVal= ("Body");}
		else if (sWID.equalsIgnoreCase("3")) {myVal= ("");}
		else if (sWID.equalsIgnoreCase("4")) {myVal= ("");}
		else if (sWID.equalsIgnoreCase("5")) {myVal= ("Thyroid");}
		else if (sWID.equalsIgnoreCase("6")) {myVal= ("");}
		else if (sWID.equalsIgnoreCase("7")) {myVal= ("Head");}
		else if (sWID.equalsIgnoreCase("8")) {myVal= ("Body");}
		else if (sWID.equalsIgnoreCase("9")) {myVal= ("");}
		else if (sWID.equalsIgnoreCase("10")) {myVal= ("");}
		else if (sWID.equalsIgnoreCase("11")) {myVal= ("");}
		else if (sWID.equalsIgnoreCase("12")) {myVal= ("Abdomen");}
		else if (sWID.equalsIgnoreCase("13")) {myVal= ("");}
		else if (sWID.equalsIgnoreCase("14")) {myVal= ("");}
		else if (sWID.equalsIgnoreCase("15")) {myVal= ("");}
		else if (sWID.equalsIgnoreCase("16")) {myVal= ("");}
		else if (sWID.equalsIgnoreCase("17")) {myVal= ("");}
		else if (sWID.equalsIgnoreCase("18")) {myVal= ("");}
		else if (sWID.equalsIgnoreCase("19")) {myVal= ("");}
		else if (sWID.equalsIgnoreCase("20")) {myVal= ("");}
		else if (sWID.equalsIgnoreCase("21")) {myVal= ("");}
		else if (sWID.equalsIgnoreCase("22")) {myVal= ("");}
		else if (sWID.equalsIgnoreCase("23")) {myVal= ("");}
		else if (sWID.equalsIgnoreCase("24")) {myVal= ("");}
		else if (sWID.equalsIgnoreCase("25")) {myVal= ("");}
		else if (sWID.equalsIgnoreCase("26")) {myVal= ("");}
		else if (sWID.equalsIgnoreCase("27")) {myVal= ("");}
		else if (sWID.equalsIgnoreCase("28")) {myVal= ("");}
		else if (sWID.equalsIgnoreCase("29")) {myVal= ("");}
		else if (sWID.equalsIgnoreCase("30")) {myVal= ("");}
		else if (sWID.equalsIgnoreCase("31")) {myVal= ("");}
		else if (sWID.equalsIgnoreCase("32")) {myVal= ("");}
		else if (sWID.equalsIgnoreCase("33")) {myVal= ("");}
		else if (sWID.equalsIgnoreCase("34")) {myVal= ("");}
		else if (sWID.equalsIgnoreCase("35")) {myVal= ("");}
		else if (sWID.equalsIgnoreCase("36")) {myVal= ("");}
		else if (sWID.equalsIgnoreCase("37")) {myVal= ("");}
		else if (sWID.equalsIgnoreCase("38")) {myVal= ("");}
		else if (sWID.equalsIgnoreCase("39")) {myVal= ("RIGHT Ankle");}
		else if (sWID.equalsIgnoreCase("40")) {myVal= ("RIGTH Wrist");}
		else if (sWID.equalsIgnoreCase("41")) {myVal= ("RIGHT Shoulder");}
		else if (sWID.equalsIgnoreCase("42")) {myVal= ("RIGHT Elbow");}
		else if (sWID.equalsIgnoreCase("43")) {myVal= ("LEFT Ankle");}
		else if (sWID.equalsIgnoreCase("44")) {myVal= ("LEFT Wrist");}
		else if (sWID.equalsIgnoreCase("45")) {myVal= ("LEFT Shoulder");}
		else if (sWID.equalsIgnoreCase("46")) {myVal= ("LEFT Elbow");}
		else if (sWID.equalsIgnoreCase("47")) {myVal= ("");}
		else if (sWID.equalsIgnoreCase("48")) {myVal= ("");}
		else if (sWID.equalsIgnoreCase("49")) {myVal= ("");}
		else if (sWID.equalsIgnoreCase("50")) {myVal= ("");}
		else if (sWID.equalsIgnoreCase("51")) {myVal= ("");}
		else if (sWID.equalsIgnoreCase("52")) {myVal= ("");}
		else if (sWID.equalsIgnoreCase("53")) {myVal= ("");}
		else if (sWID.equalsIgnoreCase("54")) {myVal= ("");}
		else if (sWID.equalsIgnoreCase("55")) {myVal= ("");}
		else if (sWID.equalsIgnoreCase("56")) {myVal= ("");}
		else if (sWID.equalsIgnoreCase("57")) {myVal= ("");}
		else if (sWID.equalsIgnoreCase("58")) {myVal= ("");}
		else if (sWID.equalsIgnoreCase("59")) {myVal= ("");}
		else if (sWID.equalsIgnoreCase("60")) {myVal= ("");}
		else if (sWID.equalsIgnoreCase("61")) {myVal= ("");}
		else if (sWID.equalsIgnoreCase("62")) {myVal= ("");}
		else if (sWID.equalsIgnoreCase("63")) {myVal= ("");}
		else if (sWID.equalsIgnoreCase("64")) {myVal= ("");}
		else if (sWID.equalsIgnoreCase("65")) {myVal= ("");}
		else if (sWID.equalsIgnoreCase("66")) {myVal= ("");}
		else if (sWID.equalsIgnoreCase("67")) {myVal= ("");}
		else if (sWID.equalsIgnoreCase("68")) {myVal= ("LEFT Hip");}
		else if (sWID.equalsIgnoreCase("69")) {myVal= ("LEFT Hip");}
		else if (sWID.equalsIgnoreCase("70")) {myVal= ("LEFT Hip");}
		else if (sWID.equalsIgnoreCase("71")) {myVal= ("LEFT Knee");}
		else if (sWID.equalsIgnoreCase("72")) {myVal= ("LEFT Knee");}
		else if (sWID.equalsIgnoreCase("73")) {myVal= ("LEFT Knee");}
		else if (sWID.equalsIgnoreCase("74")) {myVal= ("LEFT Ankle");}
		else if (sWID.equalsIgnoreCase("75")) {myVal= ("LEFT Ankle");}
		else if (sWID.equalsIgnoreCase("76")) {myVal= ("LEFT Ankle");}
		else if (sWID.equalsIgnoreCase("77")) {myVal= ("RIGHT Hip");}
		else if (sWID.equalsIgnoreCase("78")) {myVal= ("RIGHT Hip");}
		else if (sWID.equalsIgnoreCase("79")) {myVal= ("RIGHT Hip");}
		else if (sWID.equalsIgnoreCase("80")) {myVal= ("RIGHT Knee");}
		else if (sWID.equalsIgnoreCase("81")) {myVal= ("RIGHT Knee");}
		else if (sWID.equalsIgnoreCase("82")) {myVal= ("RIGHT Knee");}
		else if (sWID.equalsIgnoreCase("83")) {myVal= ("RIGHT Ankle");}
		else if (sWID.equalsIgnoreCase("84")) {myVal= ("RIGHT Ankle");}
		else if (sWID.equalsIgnoreCase("85")) {myVal= ("RIGHT Ankle");}
		else if (sWID.equalsIgnoreCase("86")) {myVal= ("LEFT Thigh");}
		else if (sWID.equalsIgnoreCase("87")) {myVal= ("LEFT Thigh");}
		else if (sWID.equalsIgnoreCase("88")) {myVal= ("LEFT Thigh");}
		else if (sWID.equalsIgnoreCase("89")) {myVal= ("LEFT Foot");}
		else if (sWID.equalsIgnoreCase("90")) {myVal= ("LEFT Foot");}
		else if (sWID.equalsIgnoreCase("91")) {myVal= ("LEFT Foot");}
		else if (sWID.equalsIgnoreCase("92")) {myVal= ("LEFT Lower Leg");}
		else if (sWID.equalsIgnoreCase("93")) {myVal= ("LEFT Lower Leg");}
		else if (sWID.equalsIgnoreCase("94")) {myVal= ("LEFT Lower Leg");}
		else if (sWID.equalsIgnoreCase("95")) {myVal= ("RIGHT Thigh");}
		else if (sWID.equalsIgnoreCase("96")) {myVal= ("RIGHT Thigh");}
		else if (sWID.equalsIgnoreCase("97")) {myVal= ("RIGHT Thigh");}
		else if (sWID.equalsIgnoreCase("98")) {myVal= ("RIGHT Foot");}
		else if (sWID.equalsIgnoreCase("99")) {myVal= ("RIGHT Foot");}
		else if (sWID.equalsIgnoreCase("100")) {myVal= ("RIGHT Foot");}
		else if (sWID.equalsIgnoreCase("101")) {myVal= ("RIGHT Lower Leg");}
		else if (sWID.equalsIgnoreCase("102")) {myVal= ("RIGHT Lower Leg");}
		else if (sWID.equalsIgnoreCase("103")) {myVal= ("RIGHT Lower Leg");}
		else if (sWID.equalsIgnoreCase("104")) {myVal= ("");}
		else if (sWID.equalsIgnoreCase("105")) {myVal= ("");}
		else if (sWID.equalsIgnoreCase("106")) {myVal= ("");}
		else if (sWID.equalsIgnoreCase("107")) {myVal= ("");}
		else if (sWID.equalsIgnoreCase("108")) {myVal= ("");}
		else if (sWID.equalsIgnoreCase("109")) {myVal= ("");}
		else if (sWID.equalsIgnoreCase("110")) {myVal= ("");}
		else if (sWID.equalsIgnoreCase("111")) {myVal= ("");}
		else if (sWID.equalsIgnoreCase("112")) {myVal= ("");}
		else if (sWID.equalsIgnoreCase("113")) {myVal= ("");}
		else if (sWID.equalsIgnoreCase("114")) {myVal= ("");}
		else if (sWID.equalsIgnoreCase("115")) {myVal= ("");}
		else if (sWID.equalsIgnoreCase("116")) {myVal= ("");}
		else if (sWID.equalsIgnoreCase("117")) {myVal= ("LEFT Forearm");}
		else if (sWID.equalsIgnoreCase("118")) {myVal= ("LEFT Forearm");}
		else if (sWID.equalsIgnoreCase("119")) {myVal= ("LEFT Forearm");}
		else if (sWID.equalsIgnoreCase("120")) {myVal= ("LEFT Humerus");}
		else if (sWID.equalsIgnoreCase("121")) {myVal= ("LEFT Humerus");}
		else if (sWID.equalsIgnoreCase("122")) {myVal= ("LEFT Humerus");}
		else if (sWID.equalsIgnoreCase("123")) {myVal= ("LEFT Hand");}
		else if (sWID.equalsIgnoreCase("124")) {myVal= ("LEFT Hand");}
		else if (sWID.equalsIgnoreCase("125")) {myVal= ("LEFT Hand");}
		else if (sWID.equalsIgnoreCase("126")) {myVal= ("RIGHT Forearm");}
		else if (sWID.equalsIgnoreCase("127")) {myVal= ("RIGHT Forearm");}
		else if (sWID.equalsIgnoreCase("128")) {myVal= ("RIGHT Forearm");}
		else if (sWID.equalsIgnoreCase("129")) {myVal= ("RIGHT Humerus");}
		else if (sWID.equalsIgnoreCase("130")) {myVal= ("RIGHT Humerus");}
		else if (sWID.equalsIgnoreCase("131")) {myVal= ("RIGHT Humerus");}
		else if (sWID.equalsIgnoreCase("132")) {myVal= ("RIGHT Hand");}
		else if (sWID.equalsIgnoreCase("133")) {myVal= ("RIGHT Hand");}
		else if (sWID.equalsIgnoreCase("134")) {myVal= ("RIGHT Hand");}
		else if (sWID.equalsIgnoreCase("135")) {myVal= ("LEFT Shoulder");}
		else if (sWID.equalsIgnoreCase("136")) {myVal= ("LEFT Shoulder");}
		else if (sWID.equalsIgnoreCase("137")) {myVal= ("LEFT Shoulder");}
		else if (sWID.equalsIgnoreCase("138")) {myVal= ("LEFT Elbow");}
		else if (sWID.equalsIgnoreCase("139")) {myVal= ("LEFT Elbow");}
		else if (sWID.equalsIgnoreCase("140")) {myVal= ("LEFT Elbow");}
		else if (sWID.equalsIgnoreCase("141")) {myVal= ("LEFT Wrist");}
		else if (sWID.equalsIgnoreCase("142")) {myVal= ("LEFT Wrist");}
		else if (sWID.equalsIgnoreCase("143")) {myVal= ("LEFT Wrist");}
		else if (sWID.equalsIgnoreCase("144")) {myVal= ("RIGHT Shoulder");}
		else if (sWID.equalsIgnoreCase("145")) {myVal= ("RIGHT Shoulder");}
		else if (sWID.equalsIgnoreCase("146")) {myVal= ("RIGHT Shoulder");}
		else if (sWID.equalsIgnoreCase("147")) {myVal= ("RIGHT Elbow");}
		else if (sWID.equalsIgnoreCase("148")) {myVal= ("RIGHT Elbow");}
		else if (sWID.equalsIgnoreCase("149")) {myVal= ("RIGHT Elbow");}
		else if (sWID.equalsIgnoreCase("150")) {myVal= ("RIGHT Wrist");}
		else if (sWID.equalsIgnoreCase("151")) {myVal= ("RIGHT Wrist");}
		else if (sWID.equalsIgnoreCase("152")) {myVal= ("RIGHT Wrist");}
		else if (sWID.equalsIgnoreCase("153")) {myVal= ("");}
		else if (sWID.equalsIgnoreCase("154")) {myVal= ("");}
		else if (sWID.equalsIgnoreCase("155")) {myVal= ("");}
		else if (sWID.equalsIgnoreCase("156")) {myVal= ("");}
		else if (sWID.equalsIgnoreCase("157")) {myVal= ("");}
		else if (sWID.equalsIgnoreCase("158")) {myVal= ("");}
		else if (sWID.equalsIgnoreCase("159")) {myVal= ("");}
		else if (sWID.equalsIgnoreCase("160")) {myVal= ("");}
		else if (sWID.equalsIgnoreCase("161")) {myVal= ("");}
		else if (sWID.equalsIgnoreCase("162")) {myVal= ("");}
		else if (sWID.equalsIgnoreCase("163")) {myVal= ("");}
		else if (sWID.equalsIgnoreCase("164")) {myVal= ("");}
		else if (sWID.equalsIgnoreCase("165")) {myVal= ("");}
		else if (sWID.equalsIgnoreCase("166")) {myVal= ("");}
		else if (sWID.equalsIgnoreCase("167")) {myVal= ("");}
		else if (sWID.equalsIgnoreCase("168")) {myVal= ("");}
		else if (sWID.equalsIgnoreCase("169")) {myVal= ("");}
		else if (sWID.equalsIgnoreCase("170")) {myVal= ("");}
		else if (sWID.equalsIgnoreCase("171")) {myVal= ("");}
		else if (sWID.equalsIgnoreCase("172")) {myVal= ("");}
		else if (sWID.equalsIgnoreCase("173")) {myVal= ("");}
		else if (sWID.equalsIgnoreCase("174")) {myVal= ("");}
		else if (sWID.equalsIgnoreCase("175")) {myVal= ("");}
		else if (sWID.equalsIgnoreCase("176")) {myVal= ("");}
		else if (sWID.equalsIgnoreCase("177")) {myVal= ("");}
		else if (sWID.equalsIgnoreCase("178")) {myVal= ("");}
		else if (sWID.equalsIgnoreCase("179")) {myVal= ("");}
		else if (sWID.equalsIgnoreCase("180")) {myVal= ("");}
		else if (sWID.equalsIgnoreCase("181")) {myVal= ("");}
		else if (sWID.equalsIgnoreCase("182")) {myVal= ("");}
		else if (sWID.equalsIgnoreCase("183")) {myVal= ("");}
		else if (sWID.equalsIgnoreCase("184")) {myVal= ("");}
		else if (sWID.equalsIgnoreCase("185")) {myVal= ("");}
		else if (sWID.equalsIgnoreCase("186")) {myVal= ("");}
		else if (sWID.equalsIgnoreCase("187")) {myVal= ("");}
		else if (sWID.equalsIgnoreCase("188")) {myVal= ("LEFT Hip");}
		else if (sWID.equalsIgnoreCase("189")) {myVal= ("LEFT Hip");}
		else if (sWID.equalsIgnoreCase("190")) {myVal= ("LEFT Hip");}
		else if (sWID.equalsIgnoreCase("191")) {myVal= ("LEFT Knee");}
		else if (sWID.equalsIgnoreCase("192")) {myVal= ("LEFT Knee");}
		else if (sWID.equalsIgnoreCase("193")) {myVal= ("LEFT Knee");}
		else if (sWID.equalsIgnoreCase("194")) {myVal= ("LEFT Thigh");}
		else if (sWID.equalsIgnoreCase("195")) {myVal= ("LEFT Thigh");}
		else if (sWID.equalsIgnoreCase("196")) {myVal= ("LEFT Thigh");}
		else if (sWID.equalsIgnoreCase("197")) {myVal= ("LEFT Lower Leg");}
		else if (sWID.equalsIgnoreCase("198")) {myVal= ("LEFT Lower Leg");}
		else if (sWID.equalsIgnoreCase("199")) {myVal= ("LEFT Lower Leg");}
		else if (sWID.equalsIgnoreCase("200")) {myVal= ("LEFT Foot");}
		else if (sWID.equalsIgnoreCase("201")) {myVal= ("LEFT Foot");}
		else if (sWID.equalsIgnoreCase("202")) {myVal= ("LEFT Foot");}
		else if (sWID.equalsIgnoreCase("203")) {myVal= ("LEFT Ankle");}
		else if (sWID.equalsIgnoreCase("204")) {myVal= ("LEFT Ankle");}
		else if (sWID.equalsIgnoreCase("205")) {myVal= ("LEFT Ankle");}
		else if (sWID.equalsIgnoreCase("206")) {myVal= ("RIGHT Hip");}
		else if (sWID.equalsIgnoreCase("207")) {myVal= ("RIGHT Hip");}
		else if (sWID.equalsIgnoreCase("208")) {myVal= ("RIGHT Hip");}
		else if (sWID.equalsIgnoreCase("209")) {myVal= ("RIGHT Knee");}
		else if (sWID.equalsIgnoreCase("210")) {myVal= ("RIGHT Knee");}
		else if (sWID.equalsIgnoreCase("211")) {myVal= ("RIGHT Knee");}
		else if (sWID.equalsIgnoreCase("212")) {myVal= ("RIGHT Thigh");}
		else if (sWID.equalsIgnoreCase("213")) {myVal= ("RIGHT Thigh");}
		else if (sWID.equalsIgnoreCase("214")) {myVal= ("RIGHT Thigh");}
		else if (sWID.equalsIgnoreCase("215")) {myVal= ("RIGHT Lower Leg");}
		else if (sWID.equalsIgnoreCase("216")) {myVal= ("RIGHT Lower Leg");}
		else if (sWID.equalsIgnoreCase("217")) {myVal= ("RIGHT Lower Leg");}
		else if (sWID.equalsIgnoreCase("218")) {myVal= ("RIGHT Foot");}
		else if (sWID.equalsIgnoreCase("219")) {myVal= ("RIGHT Foot");}
		else if (sWID.equalsIgnoreCase("220")) {myVal= ("RIGHT Foot");}
		else if (sWID.equalsIgnoreCase("221")) {myVal= ("RIGHT Ankle");}
		else if (sWID.equalsIgnoreCase("222")) {myVal= ("RIGHT Ankle");}
		else if (sWID.equalsIgnoreCase("223")) {myVal= ("RIGHT Ankle");}
		else if (sWID.equalsIgnoreCase("224")) {myVal= ("");}
		else if (sWID.equalsIgnoreCase("225")) {myVal= ("");}
		else if (sWID.equalsIgnoreCase("226")) {myVal= ("");}
		else if (sWID.equalsIgnoreCase("227")) {myVal= ("");}
		else if (sWID.equalsIgnoreCase("228")) {myVal= ("");}
		else if (sWID.equalsIgnoreCase("229")) {myVal= ("");}
		else if (sWID.equalsIgnoreCase("230")) {myVal= ("");}
		else if (sWID.equalsIgnoreCase("231")) {myVal= ("");}
		else if (sWID.equalsIgnoreCase("232")) {myVal= ("");}
		else if (sWID.equalsIgnoreCase("233")) {myVal= ("");}
		else if (sWID.equalsIgnoreCase("234")) {myVal= ("");}
		else if (sWID.equalsIgnoreCase("235")) {myVal= ("");}
		else if (sWID.equalsIgnoreCase("236")) {myVal= ("LEFT Shoulder");}
		else if (sWID.equalsIgnoreCase("237")) {myVal= ("LEFT Shoulder");}
		else if (sWID.equalsIgnoreCase("238")) {myVal= ("LEFT Shoulder");}
		else if (sWID.equalsIgnoreCase("239")) {myVal= ("LEFT Elbow");}
		else if (sWID.equalsIgnoreCase("240")) {myVal= ("LEFT Elbow");}
		else if (sWID.equalsIgnoreCase("241")) {myVal= ("LEFT Elbow");}
		else if (sWID.equalsIgnoreCase("242")) {myVal= ("LEFT Wrist");}
		else if (sWID.equalsIgnoreCase("243")) {myVal= ("LEFT Wrist");}
		else if (sWID.equalsIgnoreCase("244")) {myVal= ("LEFT Wrist");}
		else if (sWID.equalsIgnoreCase("245")) {myVal= ("LEFT Forearm");}
		else if (sWID.equalsIgnoreCase("246")) {myVal= ("LEFT Forearm");}
		else if (sWID.equalsIgnoreCase("247")) {myVal= ("LEFT Forearm");}
		else if (sWID.equalsIgnoreCase("248")) {myVal= ("LEFT Humerus");}
		else if (sWID.equalsIgnoreCase("249")) {myVal= ("LEFT Humerus");}
		else if (sWID.equalsIgnoreCase("250")) {myVal= ("LEFT Humerus");}
		else if (sWID.equalsIgnoreCase("251")) {myVal= ("LEFT Hand");}
		else if (sWID.equalsIgnoreCase("252")) {myVal= ("LEFT Hand");}
		else if (sWID.equalsIgnoreCase("253")) {myVal= ("LEFT Hand");}
		else if (sWID.equalsIgnoreCase("254")) {myVal= ("");}
		else if (sWID.equalsIgnoreCase("255")) {myVal= ("");}
		else if (sWID.equalsIgnoreCase("256")) {myVal= ("");}
		else if (sWID.equalsIgnoreCase("257")) {myVal= ("");}
		else if (sWID.equalsIgnoreCase("258")) {myVal= ("");}
		else if (sWID.equalsIgnoreCase("259")) {myVal= ("");}
		else if (sWID.equalsIgnoreCase("260")) {myVal= ("");}
		else if (sWID.equalsIgnoreCase("261")) {myVal= ("");}
		else if (sWID.equalsIgnoreCase("262")) {myVal= ("");}
		else if (sWID.equalsIgnoreCase("263")) {myVal= ("");}
		else if (sWID.equalsIgnoreCase("264")) {myVal= ("");}
		else if (sWID.equalsIgnoreCase("265")) {myVal= ("");}
		else if (sWID.equalsIgnoreCase("266")) {myVal= ("");}
		if (myVal.equalsIgnoreCase(""))
	    {
			myVal = "NA";
	    } 
		return myVal;
	}




    public static String getWZ_Desc(String sWID)
	{
		String myVal="Invalid";
		if (sWID.equalsIgnoreCase("1")) {myVal= ("NM - Bone &/or Joint Scan_Multiple");}
		else if (sWID.equalsIgnoreCase("2")) {myVal= ("NM - Bone &/or Joint Scan_Whole Body");}
		else if (sWID.equalsIgnoreCase("3")) {myVal= ("NM - Bone &/or Joint Scan_Phase Study");}
		else if (sWID.equalsIgnoreCase("4")) {myVal= ("NM - Hida Scan");}
		else if (sWID.equalsIgnoreCase("5")) {myVal= ("NM - Thyroid Scan w/ Uptake");}
		else if (sWID.equalsIgnoreCase("6")) {myVal= ("PET - Limited Area");}
		else if (sWID.equalsIgnoreCase("7")) {myVal= ("PET - Skull Base to Mid-Thigh");}
		else if (sWID.equalsIgnoreCase("8")) {myVal= ("PET - Whole Body");}
		else if (sWID.equalsIgnoreCase("9")) {myVal= ("PET - PET w/concurrent CT, Limited Area");}
		else if (sWID.equalsIgnoreCase("10")) {myVal= ("PET - PET w/concurrent CT, Skull base-mid thigh");}
		else if (sWID.equalsIgnoreCase("11")) {myVal= ("PET - PET w/concurrent CT, Whole Body");}
		else if (sWID.equalsIgnoreCase("12")) {myVal= ("US - Abdomen Complete");}
		else if (sWID.equalsIgnoreCase("13")) {myVal= ("US - Arterial Upper or Lower - ABI only");}
		else if (sWID.equalsIgnoreCase("14")) {myVal= ("US - Arterial Rest/Stress upper or lower");}
		else if (sWID.equalsIgnoreCase("15")) {myVal= ("US - Arterial Bilateral Lower Extremity");}
		else if (sWID.equalsIgnoreCase("16")) {myVal= ("US - Arterial Bilateral Upper Extremity");}
		else if (sWID.equalsIgnoreCase("17")) {myVal= ("US - Arterial Hemodialysis Access");}
		else if (sWID.equalsIgnoreCase("18")) {myVal= ("US - Arterial Unilateral Lower Extremity");}
		else if (sWID.equalsIgnoreCase("19")) {myVal= ("US - Arterial Unilateral Upper Extremity");}
		else if (sWID.equalsIgnoreCase("20")) {myVal= ("US - Breast");}
		else if (sWID.equalsIgnoreCase("21")) {myVal= ("US - Carotid Bilateral");}
		else if (sWID.equalsIgnoreCase("22")) {myVal= ("US - Extremity_Soft Tissue");}
		else if (sWID.equalsIgnoreCase("23")) {myVal= ("US - Gallbladder");}
		else if (sWID.equalsIgnoreCase("24")) {myVal= ("US - Kidneys/Renal/Bladder");}
		else if (sWID.equalsIgnoreCase("25")) {myVal= ("US - Liver");}
		else if (sWID.equalsIgnoreCase("26")) {myVal= ("US - Pelvic");}
		else if (sWID.equalsIgnoreCase("27")) {myVal= ("US - Pregnancy Single or < 14 weeks");}
		else if (sWID.equalsIgnoreCase("28")) {myVal= ("US - Pregnancy Each Additional Gestation");}
		else if (sWID.equalsIgnoreCase("29")) {myVal= ("US - Pregnancy > or = to 14 weeks");}
		else if (sWID.equalsIgnoreCase("30")) {myVal= ("US - Pregnancy Complete, Single - Detail");}
		else if (sWID.equalsIgnoreCase("31")) {myVal= ("US - Pregnancy Complete, Additional - Detail");}
		else if (sWID.equalsIgnoreCase("32")) {myVal= ("US - Pregnancy Transvaginal");}
		else if (sWID.equalsIgnoreCase("33")) {myVal= ("US - Sonohysterography");}
		else if (sWID.equalsIgnoreCase("34")) {myVal= ("US - Testicular");}
		else if (sWID.equalsIgnoreCase("35")) {myVal= ("US - Thyroid");}
		else if (sWID.equalsIgnoreCase("36")) {myVal= ("US - Transvaginal");}
		else if (sWID.equalsIgnoreCase("37")) {myVal= ("US - Venous Extremity Bilateral");}
		else if (sWID.equalsIgnoreCase("38")) {myVal= ("US - Venous Extremity Unilateral");}
		else if (sWID.equalsIgnoreCase("39")) {myVal= ("Special - Arthrogram Ankle - RIGHT");}
		else if (sWID.equalsIgnoreCase("40")) {myVal= ("Special - Arthrogram Wrist - RIGHT");}
		else if (sWID.equalsIgnoreCase("41")) {myVal= ("Special - Arthrogram Shoulder - RIGHT");}
		else if (sWID.equalsIgnoreCase("42")) {myVal= ("Special - Arthrogram Elbow - RIGHT");}
		else if (sWID.equalsIgnoreCase("43")) {myVal= ("Special - Arthrogram Ankle - LEFT");}
		else if (sWID.equalsIgnoreCase("44")) {myVal= ("Special - Arthrogram Wrist - LEFT");}
		else if (sWID.equalsIgnoreCase("45")) {myVal= ("Special - Arthrogram Shoulder - LEFT");}
		else if (sWID.equalsIgnoreCase("46")) {myVal= ("Special - Arthrogram Elbow - LEFT");}
		else if (sWID.equalsIgnoreCase("47")) {myVal= ("Special - HSG");}
		else if (sWID.equalsIgnoreCase("48")) {myVal= ("Special - Myelogram Cervical 62284");}
		else if (sWID.equalsIgnoreCase("49")) {myVal= ("Special - Myelogram Thoracic 62284");}
		else if (sWID.equalsIgnoreCase("50")) {myVal= ("Special - Myelogram Lumbar 62284");}
		else if (sWID.equalsIgnoreCase("51")) {myVal= ("Special - Myelography");}
		else if (sWID.equalsIgnoreCase("52")) {myVal= ("Special - Venogram Bilateral 36005");}
		else if (sWID.equalsIgnoreCase("53")) {myVal= ("Special - Venogram Unilateral 36005");}
		else if (sWID.equalsIgnoreCase("54")) {myVal= ("MRI - Abdomen w/o contrast");}
		else if (sWID.equalsIgnoreCase("55")) {myVal= ("MRI - Abdomen w/o contrast");}
		else if (sWID.equalsIgnoreCase("56")) {myVal= ("MRI - Abdomen w_w/o contrast");}
		else if (sWID.equalsIgnoreCase("57")) {myVal= ("MRI - Brain w/o contrast");}
		else if (sWID.equalsIgnoreCase("58")) {myVal= ("MRI - Brain w/ contrast");}
		else if (sWID.equalsIgnoreCase("59")) {myVal= ("MRI - Brain w_w/o contrast");}
		else if (sWID.equalsIgnoreCase("60")) {myVal= ("MRI - Breast Unilateral");}
		else if (sWID.equalsIgnoreCase("61")) {myVal= ("MRI - Breast Bilateral");}
		else if (sWID.equalsIgnoreCase("62")) {myVal= ("MRI - Cervical w/o contrast");}
		else if (sWID.equalsIgnoreCase("63")) {myVal= ("MRI - Cervical w/ contrast");}
		else if (sWID.equalsIgnoreCase("64")) {myVal= ("MRI - Cervical w_w/o contrast");}
		else if (sWID.equalsIgnoreCase("65")) {myVal= ("MRI - Chest w/o contrast");}
		else if (sWID.equalsIgnoreCase("66")) {myVal= ("MRI - Chest w/ contrast");}
		else if (sWID.equalsIgnoreCase("67")) {myVal= ("MRI - Chest w_w/o contrast");}
		else if (sWID.equalsIgnoreCase("68")) {myVal= ("MRI - Hip w/o contrast - LEFT");}
		else if (sWID.equalsIgnoreCase("69")) {myVal= ("MRI - Hip w/ contrast - LEFT");}
		else if (sWID.equalsIgnoreCase("70")) {myVal= ("MRI - Hip w_w/o contrast - LEFT");}
		else if (sWID.equalsIgnoreCase("71")) {myVal= ("MRI - Knee w/o contrast - LEFT");}
		else if (sWID.equalsIgnoreCase("72")) {myVal= ("MRI - Knee w/ contrast - LEFT");}
		else if (sWID.equalsIgnoreCase("73")) {myVal= ("MRI - Knee w_w/o contrast - LEFT");}
		else if (sWID.equalsIgnoreCase("74")) {myVal= ("MRI - Ankle w/o contrast - LEFT");}
		else if (sWID.equalsIgnoreCase("75")) {myVal= ("MRI - Ankle w/ contrast - LEFT");}
		else if (sWID.equalsIgnoreCase("76")) {myVal= ("MRI - Ankle w_w/o contrast - LEFT");}
		else if (sWID.equalsIgnoreCase("77")) {myVal= ("MRI - Hip w/o contrast - RIGHT");}
		else if (sWID.equalsIgnoreCase("78")) {myVal= ("MRI - Hip w/ contrast - RIGHT");}
		else if (sWID.equalsIgnoreCase("79")) {myVal= ("MRI - Hip w_w/o contrast - RIGHT");}
		else if (sWID.equalsIgnoreCase("80")) {myVal= ("MRI - Knee w/o contrast - RIGHT");}
		else if (sWID.equalsIgnoreCase("81")) {myVal= ("MRI - Knee w/ contrast - RIGHT");}
		else if (sWID.equalsIgnoreCase("82")) {myVal= ("MRI - Knee w_w/o contrast - RIGHT");}
		else if (sWID.equalsIgnoreCase("83")) {myVal= ("MRI - Ankle w/o contrast - RIGHT");}
		else if (sWID.equalsIgnoreCase("84")) {myVal= ("MRI - Ankle w/ contrast - RIGHT");}
		else if (sWID.equalsIgnoreCase("85")) {myVal= ("MRI - Ankle w_w/o contrast - RIGHT");}
		else if (sWID.equalsIgnoreCase("86")) {myVal= ("MRI - Thigh w/o contrast - LEFT");}
		else if (sWID.equalsIgnoreCase("87")) {myVal= ("MRI - Thigh w/ contrast - LEFT");}
		else if (sWID.equalsIgnoreCase("88")) {myVal= ("MRI - Thigh w_w/o contrast - LEFT");}
		else if (sWID.equalsIgnoreCase("89")) {myVal= ("MRI - Foot w/o contrast - LEFT");}
		else if (sWID.equalsIgnoreCase("90")) {myVal= ("MRI - Foot w/ contrast - LEFT");}
		else if (sWID.equalsIgnoreCase("91")) {myVal= ("MRI - Foot w_w/o contrast - LEFT");}
		else if (sWID.equalsIgnoreCase("92")) {myVal= ("MRI - Lower Leg w/o contrast - LEFT");}
		else if (sWID.equalsIgnoreCase("93")) {myVal= ("MRI - Lower Leg w/ contrast - LEFT");}
		else if (sWID.equalsIgnoreCase("94")) {myVal= ("MRI - Lower Leg w_w/o contrast - LEFT");}
		else if (sWID.equalsIgnoreCase("95")) {myVal= ("MRI - Thigh w/o contrast - RIGHT");}
		else if (sWID.equalsIgnoreCase("96")) {myVal= ("MRI - Thigh w/ contrast - RIGHT");}
		else if (sWID.equalsIgnoreCase("97")) {myVal= ("MRI - Thigh w_w/o contrast - RIGHT");}
		else if (sWID.equalsIgnoreCase("98")) {myVal= ("MRI - Foot w/o contrast - RIGHT");}
		else if (sWID.equalsIgnoreCase("99")) {myVal= ("MRI - Foot w/ contrast - RIGHT");}
		else if (sWID.equalsIgnoreCase("100")) {myVal= ("MRI - Foot w_w/o contrast - RIGHT");}
		else if (sWID.equalsIgnoreCase("101")) {myVal= ("MRI - Lower Leg w/o contrast - RIGHT");}
		else if (sWID.equalsIgnoreCase("102")) {myVal= ("MRI - Lower Leg w/ contrast - RIGHT");}
		else if (sWID.equalsIgnoreCase("103")) {myVal= ("MRI - Lower Leg w_w/o contrast - RIGHT");}
		else if (sWID.equalsIgnoreCase("104")) {myVal= ("MRI - Lumbar w/o contrast");}
		else if (sWID.equalsIgnoreCase("105")) {myVal= ("MRI - Lumbar w/ contrast");}
		else if (sWID.equalsIgnoreCase("106")) {myVal= ("MRI - Lumbar w_w/o contrast");}
		else if (sWID.equalsIgnoreCase("107")) {myVal= ("MRI - Orbit Face/Neck w/o contrast");}
		else if (sWID.equalsIgnoreCase("108")) {myVal= ("MRI - Orbit Face/Neck w/ contrast");}
		else if (sWID.equalsIgnoreCase("109")) {myVal= ("MRI - Orbit Face/Neck w_w/o contrast");}
		else if (sWID.equalsIgnoreCase("110")) {myVal= ("MRI - Pelvis w/o contrast");}
		else if (sWID.equalsIgnoreCase("111")) {myVal= ("MRI - Pelvis w/ contrast");}
		else if (sWID.equalsIgnoreCase("112")) {myVal= ("MRI - Pelvis w_w/o contrast");}
		else if (sWID.equalsIgnoreCase("113")) {myVal= ("MRI - Thoracic w/o contrast");}
		else if (sWID.equalsIgnoreCase("114")) {myVal= ("MRI - Thoracic w/ contrast");}
		else if (sWID.equalsIgnoreCase("115")) {myVal= ("MRI - Thoracic w_w/o contrast");}
		else if (sWID.equalsIgnoreCase("116")) {myVal= ("MRI - TMJ");}
		else if (sWID.equalsIgnoreCase("117")) {myVal= ("MRI - Forearm w/o contrast - LEFT");}
		else if (sWID.equalsIgnoreCase("118")) {myVal= ("MRI - Forearm w/ contrast - LEFT");}
		else if (sWID.equalsIgnoreCase("119")) {myVal= ("MRI - Forearm w_w/o contrast - LEFT");}
		else if (sWID.equalsIgnoreCase("120")) {myVal= ("MRI - Humerus w/o contrast - LEFT");}
		else if (sWID.equalsIgnoreCase("121")) {myVal= ("MRI - Humerus w/ contrast - LEFT");}
		else if (sWID.equalsIgnoreCase("122")) {myVal= ("MRI - Humerus w_w/o contrast - LEFT");}
		else if (sWID.equalsIgnoreCase("123")) {myVal= ("MRI - Hand w/o contrast - LEFT");}
		else if (sWID.equalsIgnoreCase("124")) {myVal= ("MRI - Hand w/ contrast - LEFT");}
		else if (sWID.equalsIgnoreCase("125")) {myVal= ("MRI - Hand w_w/o contrast - LEFT");}
		else if (sWID.equalsIgnoreCase("126")) {myVal= ("MRI - Forearm w/o contrast - RIGHT");}
		else if (sWID.equalsIgnoreCase("127")) {myVal= ("MRI - Forearm w/ contrast - RIGHT");}
		else if (sWID.equalsIgnoreCase("128")) {myVal= ("MRI - Forearm w_w/o contrast - RIGHT");}
		else if (sWID.equalsIgnoreCase("129")) {myVal= ("MRI - Humerus w/o contrast - RIGHT");}
		else if (sWID.equalsIgnoreCase("130")) {myVal= ("MRI - Humerus w/ contrast - RIGHT");}
		else if (sWID.equalsIgnoreCase("131")) {myVal= ("MRI - Humerus w_w/o contrast - RIGHT");}
		else if (sWID.equalsIgnoreCase("132")) {myVal= ("MRI - Hand w/o contrast - RIGHT");}
		else if (sWID.equalsIgnoreCase("133")) {myVal= ("MRI - Hand w/ contrast - RIGHT");}
		else if (sWID.equalsIgnoreCase("134")) {myVal= ("MRI - Hand w_w/o contrast - RIGHT");}
		else if (sWID.equalsIgnoreCase("135")) {myVal= ("MRI - Shoulder w/o contrast - LEFT");}
		else if (sWID.equalsIgnoreCase("136")) {myVal= ("MRI - Shoulder w/ contrast - LEFT");}
		else if (sWID.equalsIgnoreCase("137")) {myVal= ("MRI - Shoulder w_w/o contrast - LEFT");}
		else if (sWID.equalsIgnoreCase("138")) {myVal= ("MRI - Elbow w/o contrast - LEFT");}
		else if (sWID.equalsIgnoreCase("139")) {myVal= ("MRI - Elbow w/ contrast - LEFT");}
		else if (sWID.equalsIgnoreCase("140")) {myVal= ("MRI - Elbow w_w/o contrast - LEFT");}
		else if (sWID.equalsIgnoreCase("141")) {myVal= ("MRI - Wrist w/o contrast - LEFT");}
		else if (sWID.equalsIgnoreCase("142")) {myVal= ("MRI - Wrist w/ contrast - LEFT");}
		else if (sWID.equalsIgnoreCase("143")) {myVal= ("MRI - Wrist w_w/o contrast - LEFT");}
		else if (sWID.equalsIgnoreCase("144")) {myVal= ("MRI - Shoulder w/o contrast - RIGHT");}
		else if (sWID.equalsIgnoreCase("145")) {myVal= ("MRI - Shoulder w/ contrast - RIGHT");}
		else if (sWID.equalsIgnoreCase("146")) {myVal= ("MRI - Shoulder w_w/o contrast - RIGHT");}
		else if (sWID.equalsIgnoreCase("147")) {myVal= ("MRI - Elbow w/o contrast - RIGHT");}
		else if (sWID.equalsIgnoreCase("148")) {myVal= ("MRI - Elbow w/ contrast - RIGHT");}
		else if (sWID.equalsIgnoreCase("149")) {myVal= ("MRI - Elbow w_w/o contrast - RIGHT");}
		else if (sWID.equalsIgnoreCase("150")) {myVal= ("MRI - Wrist w/o contrast - RIGHT");}
		else if (sWID.equalsIgnoreCase("151")) {myVal= ("MRI - Wrist w/ contrast - RIGHT");}
		else if (sWID.equalsIgnoreCase("152")) {myVal= ("MRI - Wrist w_w/o contrast - RIGHT");}
		else if (sWID.equalsIgnoreCase("153")) {myVal= ("MRA - Abdomen");}
		else if (sWID.equalsIgnoreCase("154")) {myVal= ("MRA - Chest");}
		else if (sWID.equalsIgnoreCase("155")) {myVal= ("MRA - Head w/o contrast");}
		else if (sWID.equalsIgnoreCase("156")) {myVal= ("MRA - Head w/ contrast");}
		else if (sWID.equalsIgnoreCase("157")) {myVal= ("MRA - Head w_w/o contrast");}
		else if (sWID.equalsIgnoreCase("158")) {myVal= ("MRA - Lower Extremity");}
		else if (sWID.equalsIgnoreCase("159")) {myVal= ("MRA - Neck w/o contrast");}
		else if (sWID.equalsIgnoreCase("160")) {myVal= ("MRA - Neck w/ contrast");}
		else if (sWID.equalsIgnoreCase("161")) {myVal= ("MRA - Neck w_w/o contrast");}
		else if (sWID.equalsIgnoreCase("162")) {myVal= ("MRA - Pelvis");}
		else if (sWID.equalsIgnoreCase("163")) {myVal= ("MRA - Upper Extremity");}
		else if (sWID.equalsIgnoreCase("164")) {myVal= ("CT - Abdomen w/o contrast");}
		else if (sWID.equalsIgnoreCase("165")) {myVal= ("CT - Abdomen w/ contrast");}
		else if (sWID.equalsIgnoreCase("166")) {myVal= ("CT - Abdomen w_w/o contrast");}
		else if (sWID.equalsIgnoreCase("167")) {myVal= ("CT - Brain/Head w/o contrast");}
		else if (sWID.equalsIgnoreCase("168")) {myVal= ("CT - Brain/Head w/ contrast");}
		else if (sWID.equalsIgnoreCase("169")) {myVal= ("CT - Brain/Head w_ w/o contrast");}
		else if (sWID.equalsIgnoreCase("170")) {myVal= ("CT - Cervical w/o contrast");}
		else if (sWID.equalsIgnoreCase("171")) {myVal= ("CT - Cervical w/ contrast");}
		else if (sWID.equalsIgnoreCase("172")) {myVal= ("CT - Cervical w_w/o contrast");}
		else if (sWID.equalsIgnoreCase("173")) {myVal= ("CT - Chest w/o contrast");}
		else if (sWID.equalsIgnoreCase("174")) {myVal= ("CT - Chest w/ contrast");}
		else if (sWID.equalsIgnoreCase("175")) {myVal= ("CT - Chest w_w/o contrast");}
		else if (sWID.equalsIgnoreCase("176")) {myVal= ("CT - CTA Coronary Arteries");}
		else if (sWID.equalsIgnoreCase("177")) {myVal= ("CT - CTA Coronary Arteries w/ Cal Scoring");}
		else if (sWID.equalsIgnoreCase("178")) {myVal= ("CT - CT Heart w/o contrast w/ Cal Scoring");}
		else if (sWID.equalsIgnoreCase("179")) {myVal= ("CT - IAC w/o contrast");}
		else if (sWID.equalsIgnoreCase("180")) {myVal= ("CT - IAC w/ contrast");}
		else if (sWID.equalsIgnoreCase("181")) {myVal= ("CT - IAC w_w/o contrast");}
		else if (sWID.equalsIgnoreCase("182")) {myVal= ("CT - Larynx/Neck w/o contrast");}
		else if (sWID.equalsIgnoreCase("183")) {myVal= ("CT - Larynx/Neck w/ contrast");}
		else if (sWID.equalsIgnoreCase("184")) {myVal= ("CT - Larynx/Neck w_w/o contrast");}
		else if (sWID.equalsIgnoreCase("185")) {myVal= ("CT - Lower Extremity w/o contrast");}
		else if (sWID.equalsIgnoreCase("186")) {myVal= ("CT - Lower Extremity w/ contrast");}
		else if (sWID.equalsIgnoreCase("187")) {myVal= ("CT - Lower Extremity w_w/o contrast");}
		else if (sWID.equalsIgnoreCase("188")) {myVal= ("CT - Hip w/o contrast - LEFT");}
		else if (sWID.equalsIgnoreCase("189")) {myVal= ("CT - Hip w/ contrast - LEFT");}
		else if (sWID.equalsIgnoreCase("190")) {myVal= ("CT - Hip w_w/o contrast - LEFT");}
		else if (sWID.equalsIgnoreCase("191")) {myVal= ("CT - Knee w/o contrast - LEFT");}
		else if (sWID.equalsIgnoreCase("192")) {myVal= ("CT - Knee w/ contrast - LEFT");}
		else if (sWID.equalsIgnoreCase("193")) {myVal= ("CT - Knee w_w/o contrast - LEFT");}
		else if (sWID.equalsIgnoreCase("194")) {myVal= ("CT - Thigh w/o contrast - LEFT");}
		else if (sWID.equalsIgnoreCase("195")) {myVal= ("CT - Thigh w/ contrast - LEFT");}
		else if (sWID.equalsIgnoreCase("196")) {myVal= ("CT - Thigh w_w/o contrast - LEFT");}
		else if (sWID.equalsIgnoreCase("197")) {myVal= ("CT - Lower Leg w/o contrast - LEFT");}
		else if (sWID.equalsIgnoreCase("198")) {myVal= ("CT - Lower Leg w/ contrast - LEFT");}
		else if (sWID.equalsIgnoreCase("199")) {myVal= ("CT - Lower Leg w_w/o contrast - LEFT");}
		else if (sWID.equalsIgnoreCase("200")) {myVal= ("CT - Foot w/o contrast - LEFT");}
		else if (sWID.equalsIgnoreCase("201")) {myVal= ("CT - Foot w/ contrast - LEFT");}
		else if (sWID.equalsIgnoreCase("202")) {myVal= ("CT - Foot w_w/o contrast - LEFT");}
		else if (sWID.equalsIgnoreCase("203")) {myVal= ("CT - Ankle w/o contrast - LEFT");}
		else if (sWID.equalsIgnoreCase("204")) {myVal= ("CT - Ankle w/ contrast - LEFT");}
		else if (sWID.equalsIgnoreCase("205")) {myVal= ("CT - Ankle w_w/o contrast - LEFT");}
		else if (sWID.equalsIgnoreCase("206")) {myVal= ("CT - Hip w/o contrast - RIGHT");}
		else if (sWID.equalsIgnoreCase("207")) {myVal= ("CT - Hip w/ contrast - RIGHT");}
		else if (sWID.equalsIgnoreCase("208")) {myVal= ("CT - Hip w_w/o contrast - RIGHT");}
		else if (sWID.equalsIgnoreCase("209")) {myVal= ("CT - Knee w/o contrast - RIGHT");}
		else if (sWID.equalsIgnoreCase("210")) {myVal= ("CT - Knee w/ contrast - RIGHT");}
		else if (sWID.equalsIgnoreCase("211")) {myVal= ("CT - Knee w_w/o contrast - RIGHT");}
		else if (sWID.equalsIgnoreCase("212")) {myVal= ("CT - Thigh w/o contrast - RIGHT");}
		else if (sWID.equalsIgnoreCase("213")) {myVal= ("CT - Thigh w/ contrast - RIGHT");}
		else if (sWID.equalsIgnoreCase("214")) {myVal= ("CT - Thigh w_w/o contrast - RIGHT");}
		else if (sWID.equalsIgnoreCase("215")) {myVal= ("CT - Lower Leg w/o contrast - RIGHT");}
		else if (sWID.equalsIgnoreCase("216")) {myVal= ("CT - Lower Leg w/ contrast - RIGHT");}
		else if (sWID.equalsIgnoreCase("217")) {myVal= ("CT - Lower Leg w_w/o contrast - RIGHT");}
		else if (sWID.equalsIgnoreCase("218")) {myVal= ("CT - Foot w/o contrast - RIGHT");}
		else if (sWID.equalsIgnoreCase("219")) {myVal= ("CT - Foot w/ contrast - RIGHT");}
		else if (sWID.equalsIgnoreCase("220")) {myVal= ("CT - Foot w_w/o contrast - RIGHT");}
		else if (sWID.equalsIgnoreCase("221")) {myVal= ("CT - Ankle w/o contrast - RIGHT");}
		else if (sWID.equalsIgnoreCase("222")) {myVal= ("CT - Ankle w/ contrast - RIGHT");}
		else if (sWID.equalsIgnoreCase("223")) {myVal= ("CT - Ankle w_w/o contrast - RIGHT");}
		else if (sWID.equalsIgnoreCase("224")) {myVal= ("CT - Lumbar w/o contrast");}
		else if (sWID.equalsIgnoreCase("225")) {myVal= ("CT - Lumbar w/ contrast");}
		else if (sWID.equalsIgnoreCase("226")) {myVal= ("CT - Lumbar w_w/o contrast");}
		else if (sWID.equalsIgnoreCase("227")) {myVal= ("CT - Maxillofacial w/o contrast");}
		else if (sWID.equalsIgnoreCase("228")) {myVal= ("CT - Maxillofacial w/ contrast");}
		else if (sWID.equalsIgnoreCase("229")) {myVal= ("CT - Maxillofacial w_w/o contrast");}
		else if (sWID.equalsIgnoreCase("230")) {myVal= ("CT - Pelvis w/o contrast");}
		else if (sWID.equalsIgnoreCase("231")) {myVal= ("CT - Pelvis w/ contrast");}
		else if (sWID.equalsIgnoreCase("232")) {myVal= ("CT - Pelvis w_w/o contrast");}
		else if (sWID.equalsIgnoreCase("233")) {myVal= ("CT - Thoracic w/o contrast");}
		else if (sWID.equalsIgnoreCase("234")) {myVal= ("CT - Thoracic w/ contrast");}
		else if (sWID.equalsIgnoreCase("235")) {myVal= ("CT - Thoracic w_w/o contrast");}
		else if (sWID.equalsIgnoreCase("236")) {myVal= ("CT - Shoulder w/o contrast - LEFT");}
		else if (sWID.equalsIgnoreCase("237")) {myVal= ("CT - Shoulder w/ contrast - LEFT");}
		else if (sWID.equalsIgnoreCase("238")) {myVal= ("CT - Shoulder w_w/o contrast - LEFT");}
		else if (sWID.equalsIgnoreCase("239")) {myVal= ("CT - Elbow w/o contrast - LEFT");}
		else if (sWID.equalsIgnoreCase("240")) {myVal= ("CT - Elbow w/ contrast - LEFT");}
		else if (sWID.equalsIgnoreCase("241")) {myVal= ("CT - Elbow w_w/o contrast - LEFT");}
		else if (sWID.equalsIgnoreCase("242")) {myVal= ("CT - Wrist w/o contrast - LEFT");}
		else if (sWID.equalsIgnoreCase("243")) {myVal= ("CT - Wrist w/ contrast - LEFT");}
		else if (sWID.equalsIgnoreCase("244")) {myVal= ("CT - Wrist w_w/o contrast - LEFT");}
		else if (sWID.equalsIgnoreCase("245")) {myVal= ("CT - Forearm w/o contrast - LEFT");}
		else if (sWID.equalsIgnoreCase("246")) {myVal= ("CT - Forearm w/ contrast - LEFT");}
		else if (sWID.equalsIgnoreCase("247")) {myVal= ("CT - Forearm w_w/o contrast - LEFT");}
		else if (sWID.equalsIgnoreCase("248")) {myVal= ("CT - Humerus w/o contrast - LEFT");}
		else if (sWID.equalsIgnoreCase("249")) {myVal= ("CT - Humerus w/ contrast - LEFT");}
		else if (sWID.equalsIgnoreCase("250")) {myVal= ("CT - Humerus w_w/o contrast - LEFT");}
		else if (sWID.equalsIgnoreCase("251")) {myVal= ("CT - Hand w/o contrast - LEFT");}
		else if (sWID.equalsIgnoreCase("252")) {myVal= ("CT - Hand w/ contrast - LEFT");}
		else if (sWID.equalsIgnoreCase("253")) {myVal= ("CT - Hand w_w/o contrast - LEFT");}
		else if (sWID.equalsIgnoreCase("254")) {myVal= ("DEXA - DEXA Scan");}
		else if (sWID.equalsIgnoreCase("255")) {myVal= ("DEXA - DEXA w/Vertebral Height");}
		else if (sWID.equalsIgnoreCase("256")) {myVal= ("DEXA - Vertebral Fracture Assessment");}
		else if (sWID.equalsIgnoreCase("257")) {myVal= ("FL - Barium Enema");}
		else if (sWID.equalsIgnoreCase("258")) {myVal= ("FL - Barium Enema A/C");}
		else if (sWID.equalsIgnoreCase("259")) {myVal= ("FL - Esophagram");}
		else if (sWID.equalsIgnoreCase("260")) {myVal= ("FL - IVP w/ Tomography");}
		else if (sWID.equalsIgnoreCase("261")) {myVal= ("FL - Small Bowel Series");}
		else if (sWID.equalsIgnoreCase("262")) {myVal= ("FL - UGI w/ Small Bowel Series");}
		else if (sWID.equalsIgnoreCase("263")) {myVal= ("FL - UGI w/ Small Bowel Series DBL");}
		else if (sWID.equalsIgnoreCase("264")) {myVal= ("FL - Upper GI");}
		else if (sWID.equalsIgnoreCase("265")) {myVal= ("FL - Upper GI DBL");}
		else if (sWID.equalsIgnoreCase("266")) {myVal= ("FL - Voiding Cystogram");}
		return myVal;
	}



    public static String getCPTText5(String sCPT1,String sCPT2,String sCPT3,String sCPT4,String sCPT5)
	{
		String myVal = "";
		String rCPT1 = getCPTText(sCPT1);
		String rCPT2 = getCPTText(sCPT2);
		String rCPT3 = getCPTText(sCPT3);
		String rCPT4 = getCPTText(sCPT4);
		String rCPT5 = getCPTText(sCPT5);
		if (!rCPT1.equalsIgnoreCase(""))
		{
			if (myVal.equalsIgnoreCase(""))
			{
				myVal += rCPT1 + "(" + sCPT1 + ")";
			}
			else 
			{
				myVal += ", " + rCPT1 + "(" + sCPT1 + ")";
			}
		}
		if (!rCPT2.equalsIgnoreCase(""))
		{
			if (myVal.equalsIgnoreCase(""))
			{
				myVal += rCPT2 + "(" + sCPT2+ ")";
			}
			else 
			{
				myVal += ", " + rCPT2 + "(" + sCPT2 + ")";
			}
		}
		if (!rCPT3.equalsIgnoreCase(""))
		{
			if (myVal.equalsIgnoreCase(""))
			{
				myVal += rCPT3 + "(" + sCPT3 + ")";
			}
			else 
			{
				myVal += ", " + rCPT3 + "(" + sCPT3 + ")";
			}
		}
		if (!rCPT4.equalsIgnoreCase(""))
		{
			if (myVal.equalsIgnoreCase(""))
			{
				myVal += rCPT4 + "(" + sCPT4 + ")";
			}
			else 
			{
				myVal += ", " + rCPT4 + "(" + sCPT4 + ")";
			}
		}
		if (!rCPT5.equalsIgnoreCase(""))
		{
			if (myVal.equalsIgnoreCase(""))
			{
				myVal += rCPT5 + "(" + sCPT5 + ")";
			}
			else 
			{
				myVal += ", " + rCPT5 + "(" + sCPT5 + ")";
			}
		}
		return myVal;

	}


    public static String getCPTText(String sCPT)
	{
		String myVal = "";
		searchDB2 mySS = new searchDB2();
		java.sql.ResultSet myRS = null;
		try
		{
			//myRS = mySS.executeStatement("SELECT * from tCPTLI where CPT = '" + sCPT.replace('\\','|') + "'");
			String mySQL = "SELECT * from tCPTLI where CPT = ?";
            db_PreparedStatementObject myDPSO = new db_PreparedStatementObject(mySQL);
            myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,sCPT));
            myRS = mySS.executePreparedStatement(myDPSO);
			if (myRS!=null&&myRS.next())
			{
				myVal = myRS.getString("Description");
			}
            mySS.closeAll();

		}
		catch(Exception e)
		{
//			out.println("ResultsSet:"+e);
            DebugLogger.printLine("NIMUtils:getCPTText:for["+sCPT+"]:["+e+"]");
        } finally {
            mySS.closeAll();
        }

	    if (myVal.equalsIgnoreCase(""))
	    {
		myVal = sCPT;
	    }
	    else
	    {
		//myVal = sCPT + " (" + myVal +")";
	    }
	    return myVal;
	}

	//Analyzes a Referral to see what flags there are.  Returns HTML Status Bar
	public static String getButtonCode_MSG(String myID, String myVal, String myMSG, String myActionLink, String myClass)
	{
		String myValRet = "<input name=\"" + myID + "\" type=\"button\" class=\"" + myClass + "\" onClick=\"alert('" + myMSG + "');\" id=\"" + myID + "\" value=\"" + myVal + "\" > ";
		return myValRet;	
	}
	
	//Analyzes a Referral to see what flags there are.  Returns HTML Status Bar
	public static String aEncounterStatus_HTML_Payer(Integer iEncounterID)
	{
		String myVal = "<!--start referral status bar -->";
		bltNIM3_Encounter myE = new bltNIM3_Encounter(iEncounterID);
		bltNIM3_Referral myR = new bltNIM3_Referral(myE.getReferralID());
		bltNIM3_CaseAccount myC = new bltNIM3_CaseAccount(myR.getCaseID());
		myVal = "0";
		try
		{
			if (myC.isComplete()&& myR.isComplete()&&isEncounterReadyToSchedule(myE.getEncounterID())&&myR.getRxFileID().intValue()>0)
			{
				myVal = "1";
				if (myE.getAppointmentID().intValue()>0)
				{
					myVal = "2";
					if (myE.getReportFileID().intValue()>0)
					{
						myVal = "3";
					}
				}
			}
		}
		catch (Exception EEE)
		{
			myVal = "0";
		}
		return myVal;
	}

    public static boolean isCaseCreateCriticalDone(Integer iEncounterID)
    {
        return isCaseCreateCriticalDone(new NIM3_EncounterObject2(iEncounterID,"testing"));
    }



    //TODO: Depricated
    public static boolean isCaseCreateCriticalDone(NIM3_EncounterObject myEO)
    {
        boolean myVal = false;
        if (!myEO.NIM3_CaseAccount.getCaseClaimNumber().equalsIgnoreCase("")&&!myEO.NIM3_CaseAccount.getPatientFirstName().equalsIgnoreCase("")&&!myEO.NIM3_CaseAccount.getPatientLastName().equalsIgnoreCase("")&&myEO.NIM3_CaseAccount.getAdjusterID().intValue()>0&&myEO.NIM3_Referral.getReferringPhysicianID().intValue()>0&&myEO.NIM3_CaseAccount.getAssignedToID().intValue()>0)
        {
            myVal = true;
        }

        return myVal;

    }


    public static boolean isCaseCreateCriticalDone(NIM3_EncounterObject2 myEO2)
    {
        return isCaseCreateCriticalDone (myEO2, true);

    }

    public static boolean isCaseCreateCriticalDone(NIM3_EncounterObject2 myEO2, boolean requireRefDr)
    {
        boolean myVal = false;
        if (!myEO2.getNIM3_CaseAccount().getCaseClaimNumber().equalsIgnoreCase("")&&!myEO2.getNIM3_CaseAccount().getPatientFirstName().equalsIgnoreCase("")&&!myEO2.getNIM3_CaseAccount().getPatientLastName().equalsIgnoreCase("")&&myEO2.getNIM3_CaseAccount().getAdjusterID().intValue()>0&& (myEO2.getNIM3_Referral().getReferringPhysicianID().intValue()>0||!requireRefDr)&&myEO2.getNIM3_CaseAccount().getAssignedToID().intValue()>0)
        {
            myVal = true;
        }

        return myVal;

    }


    public static boolean isAlertSkip(bltUserAccount myUA, int CaseUserType, int AlertLevel)//1=1 2=sp 3=report     on user settings it's 1=SP 2=other Report no #
    {

        boolean myVal = false;
        if (AlertLevel==1)
        {
            if (CaseUserType==NIMUtils.CASE_USER_ADJUSTER||CaseUserType==NIMUtils.CASE_USER_REFERRING_DOCTOR)
            {
                if (myUA.getComm_Alerts2_LevelUser().intValue()==NIMUtils.ALERT_PREF_NO_ALERTS)  //note reversal of 1.2
                {
                    myVal = true;
                }
            }
            else if (CaseUserType==NIMUtils.CASE_USER_ADMIN||CaseUserType==NIMUtils.CASE_USER_ADMIN2||CaseUserType==NIMUtils.CASE_USER_ADMIN3||CaseUserType==NIMUtils.CASE_USER_ADMIN4||CaseUserType==NIMUtils.CASE_USER_NURSE_CASE_MANAGER)
            {
                if (myUA.getComm_Alerts2_LevelUser().intValue()==NIMUtils.ALERT_PREF_NO_ALERTS||myUA.getUniqueID().intValue()==0)
                {
                    myVal = true;
                }
            }
        }
        else if (AlertLevel==2)
        {
            if (CaseUserType==NIMUtils.CASE_USER_ADJUSTER||CaseUserType==NIMUtils.CASE_USER_REFERRING_DOCTOR)
            {
                if (myUA.getComm_Alerts_LevelUser().intValue()==NIMUtils.ALERT_PREF_NO_ALERTS)
                {
                    myVal = true;
                }
            }
            else if (CaseUserType==NIMUtils.CASE_USER_ADMIN||CaseUserType==NIMUtils.CASE_USER_ADMIN2||CaseUserType==NIMUtils.CASE_USER_ADMIN3||CaseUserType==NIMUtils.CASE_USER_ADMIN4||CaseUserType==NIMUtils.CASE_USER_NURSE_CASE_MANAGER)
            {
                if (myUA.getComm_Alerts_LevelUser().intValue()==NIMUtils.ALERT_PREF_NO_ALERTS||myUA.getUniqueID().intValue()==0)
                {
                    myVal = true;
                }
            }
        }
        else if (AlertLevel==3)
        {
            if (CaseUserType==NIMUtils.CASE_USER_ADJUSTER||CaseUserType==NIMUtils.CASE_USER_REFERRING_DOCTOR)
            {
                myVal = false; //do not ever skip reports - these users must exist
            }
            else if (CaseUserType==NIMUtils.CASE_USER_ADMIN||CaseUserType==NIMUtils.CASE_USER_ADMIN2||CaseUserType==NIMUtils.CASE_USER_ADMIN3||CaseUserType==NIMUtils.CASE_USER_ADMIN4||CaseUserType==NIMUtils.CASE_USER_NURSE_CASE_MANAGER)
            {
                if (myUA.getUniqueID().intValue()==0)
                {
                    myVal = true;//only skip a report if the user does not exist
                }
            }
        }

        return myVal;

    }


    //Analyzes a Referral to see what flags there are.  Returns HTML Status Bar
    public static String aEncounterStatus_HTML(Integer iEncounterID)
    {
        return aEncounterStatus_HTML(new NIM3_EncounterObject2(iEncounterID,"loading"));
    }



    //Analyzes a Referral to see what flags there are.  Returns HTML Status Bar
    public static String aEncounterStatus_HTML(NIM3_EncounterObject2 myEO2)
    {
        java.text.SimpleDateFormat dbdft = new java.text.SimpleDateFormat(PLCUtils.String_dbdft);
        java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
        java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);
        //myIsSendTrackID
        String myVal = "<!--start referral status bar -->";
        myVal += "<fieldset style=\"display:inline;border:2px solid; margin-right:10px;padding-top:0\"><legend style=\"font-weight:bold\">Data</legend>";
//        NIM3_EncounterObject2 myEO2 = new NIM3_EncounterObject2(iEncounterID,"loading");
        //Key Users


        try
        {
            myVal += getButtonCode_MSG("a_Ref_Complete","RQ:Rc","Request Received on\\n\\n" + myEO2.getNIM3_Encounter().getTimeTrack_ReqRec()+"","#","inputButton_sm_Default");
            if (!isCaseCreateCriticalDone(myEO2))
            {
                myVal += getButtonCode_MSG("a_Ref_Complete","RQ:Cr","Critical Case information required to fully create a case has not yet been entered.","#","inputButton_sm_Action2");
            }
            else
            {
                myVal += getButtonCode_MSG("a_Ref_Complete","RQ:Cr","Case has been created and Critical Data appears complete.\\n\\nUser ID: " + myEO2.getNIM3_Encounter().getTimeTrack_ReqCreated_UserID() + "" ,"#","inputButton_sm_Default");
            }

            /*
            if (!myEO.NIM3_Encounter.getSentTo_ReqRec_RefDr().after(PLCUtils.getDisplayDateSDF1().parse("1/1/1800")))
            {
                myVal += getButtonCode_MSG("a_Ref_Complete","RQc:Dr","Confirmation has not yet been sent to the Ref Dr letting them know that the file has been received.\\n\\nPlease send this confirmation .","#","inputButton_sm_Action1");
            }
            else
            {
                myVal += getButtonCode_MSG("a_Ref_Complete","RQc:Dr","Confirmation has been sent to Ref Dr. notifying them that the referral has been received.","#","inputButton_sm_Default");
            }

            if (!myEO.NIM3_Encounter.getSentTo_ReqRec_Adj().after(PLCUtils.getDisplayDateSDF1().parse("1/1/1800")))
            {
                myVal += getButtonCode_MSG("a_Ref_Complete","RQc:Ad","Confirmation has not yet been sent to the Adjuster letting them know that the file has been received.\\n\\nPlease send this confirmation .","#","inputButton_sm_Action1");
            }
            else
            {
                myVal += getButtonCode_MSG("a_Ref_Complete","RQc:Ad","Confirmation has been sent to Adjuster notifying them that the referral has been received.","#","inputButton_sm_Default");
            }
            */
            //new version checks to see if All confirms are sent out
            //still need to coordinate this with user preferences and that user is selected
            if (myEO2.isScanPassAlertsComplete(1) )
            {
                myVal += getButtonCode_MSG("a_Ref_Complete","RQc","Request Received Confirmations appear to be complete.","#","inputButton_sm_Default");
            }
            else
            {
                myVal += getButtonCode_MSG("a_Ref_Complete","RQc","Request Received Confirmations do not appear to have been completely sent out.\\n\\nPlease Review the Alert Panel for this Encounter.","#","inputButton_sm_Action1");
            }

            boolean isComplete_C = false;
            if (!myEO2.getNIM3_CaseAccount().isComplete())
            {
                myVal += getButtonCode_MSG("a_Ref_Complete","DA:CA","Missing required Case Data\\n\\nPlease click EDIT to correct this.","#","inputButton_sm_Action1");
            }
            else
            {
                myVal += getButtonCode_MSG("a_Ref_Complete","DA:CA","Case Data appears complete","#","inputButton_sm_Default");
                isComplete_C = true;
            }

            boolean isComplete_R = false;
            if (!myEO2.getNIM3_Referral().isComplete())
            {
                myVal += getButtonCode_MSG("a_Ref_Complete","DA:Rf","Missing required Referral Data\\n\\nPlease click EDIT to correct this.","#","inputButton_sm_Action1");
            }
            else
            {
                myVal += getButtonCode_MSG("a_Ref_Complete","DA:Rf","Referral Data appears complete","#","inputButton_sm_Default");
                isComplete_R = true;
            }

            boolean isComplete_E = false;
//			if (isComplete_C&&!isEncounterReadyToSchedule (myEO.NIM3_Encounter.getEncounterID()) )
            if (!myEO2.getNIM3_Encounter().isComplete() )
            {
                myVal += getButtonCode_MSG("a_Ref_Complete","DA:En","Missing required Encounter Data\\n\\nPlease click EDIT to correct this.","#","inputButton_sm_Action1");
            }
            else
            {
                myVal += getButtonCode_MSG("a_Ref_Complete","DA:En","Encounter Data appears complete","#","inputButton_sm_Default");
                isComplete_E = true;
            }

            boolean isComplete_S = false;
//			if (isComplete_C&&!isEncounterReadyToSchedule (myEO.NIM3_Encounter.getEncounterID()) )
            if (!NIMUtils.isEncounterServiceComplete(myEO2.getNIM3_Encounter().getEncounterID()) )
            {
                myVal += getButtonCode_MSG("a_Ref_Complete","DA:Sv","Missing required Services Data\\n\\nPlease click EDIT to correct this.","#","inputButton_sm_Action1");
            }
            else
            {
                myVal += getButtonCode_MSG("a_Ref_Complete","DA:Sv","Services Data appears complete","#","inputButton_sm_Default");
                isComplete_S = true;
            }
            myVal += "</fieldset><fieldset style=\"display:inline;border:2px solid; margin-right:10px;padding-top:0\"><legend style=\"font-weight:bold\">Documents</legend>";
            if (myEO2.getNIM3_Referral().getOrderFileID().intValue()==0)
            {
                myVal += getButtonCode_MSG("a_Ref_Complete","UP:Or","Missing Order\\n\\nPlease obtain this from the Adjuster or Referring Dr and uploaded it by clicking on the Upload Order button.","#","inputButton_sm_Action1");
            }
            else
            {
                myVal += getButtonCode_MSG("a_Ref_Complete","UP:Or","Order File has been uploaded.","#","inputButton_sm_Default");
            }

            if (myEO2.getNIM3_Referral().getRxFileID().intValue()==0)
            {
                myVal += getButtonCode_MSG("a_Ref_Complete","UP:Rx","Missing RX\\n\\nPlease obtain this from the Adjuster or Referring Dr and uploaded it by clicking on the Upload RX button.","#","inputButton_sm_Action1");
            }
            else
            {
                myVal += getButtonCode_MSG("a_Ref_Complete","UP:Rx","RX File has been uploaded.","#","inputButton_sm_Default");
            }

            if (myEO2.isEncounterProcessed())
            {
                myVal += getButtonCode_MSG("a_Ref_Complete","RQ:Pr","Case information has been processed.\\n\\nUser ID: " + myEO2.getNIM3_Encounter().getTimeTrack_ReqProc_UserID() + "" ,"#","inputButton_sm_Default");
            }
            else
            {
                myVal += getButtonCode_MSG("a_Ref_Complete","RQ:Pr","Case information has not yet been Processed.","#","inputButton_sm_Action1");
            }

            if (myEO2.getNIM3_Encounter().getEncounterStatusID().intValue()==8)
            {
                myVal += getButtonCode_MSG("a_Ref_Complete","Rv:Rx","Rx review has been escalated and needs a managers approval.","#","inputButton_sm_Action2");
            }
            else if (myEO2.getNIM3_Encounter().getTimeTrack_ReqRxReview_UserID().intValue()==0||!myEO2.getNIM3_Encounter().getTimeTrack_ReqRxReview().after(displayDateSDF1.parse("1/1/1800")))
            {
                myVal += getButtonCode_MSG("a_Ref_Complete","Rv:Rx","Rx has not yet been reviewed.","#","inputButton_sm_Action1");
            }
            else
            {
                myVal += getButtonCode_MSG("a_Ref_Complete","Rv:Rx","Rx appears to have been reviewed.\\n\\nUser ID: " + myEO2.getNIM3_Encounter().getTimeTrack_ReqRxReview_UserID() + "" ,"#","inputButton_sm_Default");
            }
            myVal += "</fieldset><fieldset style=\"display:inline;border:2px solid; margin-right:10px;padding-top:0\"><legend style=\"font-weight:bold\">Schedule</legend>";
/*
			if (!myEO.NIM3_Encounter.getSentTo_DataProc_Adj().after(PLCUtils.getDisplayDateSDF1().parse("1/1/1800")))
			{
				myVal += getButtonCode_MSG("a_Ref_Complete","DAc:Ad","Confirmation has not yet been sent to the Adjuster letting them know that the file has been received and processed.\\n\\nPlease send this confirmation .","#","inputButton_sm_Action1");
			}
			else
			{
				myVal += getButtonCode_MSG("a_Ref_Complete","DAc:Ad","Confirmation has been sent to Adjuster notifying them that the referral has been received and processed.","#","inputButton_sm_Default");
			}

			if (!myEO.NIM3_Encounter.getSentTo_DataProc_RefDr().after(PLCUtils.getDisplayDateSDF1().parse("1/1/1800")))
			{
				myVal += getButtonCode_MSG("a_Ref_Complete","DAc:Dr","Confirmation has not yet been sent to the Referring Dr letting them know that the file has been received and processed.\\n\\nPlease send this confirmation .","#","inputButton_sm_Action1");
			}
			else
			{
				myVal += getButtonCode_MSG("a_Ref_Complete","DAc:Dr","Confirmation has been sent to Referring Dr notifying them that the referral has been received and processed.","#","inputButton_sm_Default");
			}

*/
            if (myEO2.getNIM3_Encounter().getAppointmentID().intValue()==0)
            {
                myVal += getButtonCode_MSG("a_Ref_Complete","AC:Sc","Encounter has not yet been Scheduled","#","inputButton_sm_Action1");
            }
            else
            {
                myVal += getButtonCode_MSG("a_Ref_Complete","AC:Sc","Encounter has been Scheduled\\n\\nUser ID: " + myEO2.getNIM3_Encounter().getTimeTrack_ReqSched_UserID() + "","#","inputButton_sm_Default");
            }


            /*
            if (!myEO.NIM3_Encounter.getSentTo_SP_Adj().after(PLCUtils.getDisplayDateSDF1().parse("1/1/1800")))
            {
                myVal += getButtonCode_MSG("a_Ref_Complete","SPc:Ad","ScanPass has NOT been sent to Adjuster","#","inputButton_sm_Action1");
            }
            else
            {
                myVal += getButtonCode_MSG("a_Ref_Complete","SPc:Ad","ScanPass has been sent to Adjuster","#","inputButton_sm_Default");
            }

            if (!myEO.NIM3_Encounter.getSentTo_SP_RefDr().after(PLCUtils.getDisplayDateSDF1().parse("1/1/1800")))
            {
                myVal += getButtonCode_MSG("a_Ref_Complete","SPc:Dr","ScanPass has NOT been sent to Ref. Dr","#","inputButton_sm_Action1");
            }
            else
            {
                myVal += getButtonCode_MSG("a_Ref_Complete","SPc:Dr","ScanPass has been sent to Ref. Dr","#","inputButton_sm_Default");
            }
            */

            //new version checks to see if All confirms are sent out
            if (myEO2.isScanPassAlertsComplete(2) )
            {
                myVal += getButtonCode_MSG("a_Ref_Complete","SPc","ScanPass Confirmations appear to be complete.","#","inputButton_sm_Default");
            }
            else
            {
                myVal += getButtonCode_MSG("a_Ref_Complete","SPc","ScanPass Confirmations do not appear to have been completely sent out.\\n\\nPlease Review the Alert Panel for this Encounter.","#","inputButton_sm_Action1");
            }

            if (!myEO2.getNIM3_Encounter().getSentTo_SP_IC().after(NIMUtils.getBeforeTime()))
            {
                myVal += getButtonCode_MSG("a_Ref_Complete","SPc:IC","ScanPass has NOT been sent to Provider/Imaging Facility\\n\\nThis must be sent out!","#","inputButton_sm_Action1");
            }
            else
            {
                myVal += getButtonCode_MSG("a_Ref_Complete","SPc:IC","ScanPass appears to have been sent to Provider/Imaging Facility","#","inputButton_sm_Default");
            }
            myVal += "</fieldset><fieldset style=\"display:inline;border:2px solid; margin-right:10px;padding-top:0\"><legend style=\"font-weight:bold\">Report</legend>";
            if (myEO2.getNIM3_Encounter().getReportFileID().intValue()==0)
            {
                myVal += getButtonCode_MSG("a_Ref_Complete","UP:MR","A report has not yet been uploaded/Delivered to NIM","#","inputButton_sm_Action1");
            }
            else if (myEO2.getNIM3_Encounter().getAppointmentID().intValue()!=0)
            {
                myVal += getButtonCode_MSG("a_Ref_Complete","UP:MR","Report has been uploaded/Delivered to NIM\\n\\nUser ID: " + myEO2.getNIM3_Encounter().getTimeTrack_ReqDelivered_UserID() + "","#","inputButton_sm_Default");
            }

            if (myEO2.getNIM3_Encounter().getEncounterStatusID().intValue()==11)
            {
                myVal += getButtonCode_MSG("a_Ref_Complete","Rv:Rp","Report review has been escalated and needs a managers approval.","#","inputButton_sm_Action2");
            }
            else if (myEO2.getNIM3_Encounter().getTimeTrack_ReqRpReview_UserID().intValue()==0||!myEO2.getNIM3_Encounter().getTimeTrack_ReqRpReview().after(displayDateSDF1.parse("1/1/1800")))
            {
                myVal += getButtonCode_MSG("a_Ref_Complete","Rv:Rp","Report has not yet been reviewed.","#","inputButton_sm_Action1");
            }
            else
            {
                myVal += getButtonCode_MSG("a_Ref_Complete","Rv:Rp","Report appears to have been reviewed.\\n\\nUser ID: " + myEO2.getNIM3_Encounter().getTimeTrack_ReqRpReview_UserID() + "" ,"#","inputButton_sm_Default");
            }




            /*
            if (!myEO.NIM3_Encounter.getSentToAdj().after(PLCUtils.getDisplayDateSDF1().parse("1/1/1800")))
            {
                myVal += getButtonCode_MSG("a_Ref_Complete","MRc:Ad","Report notification has NOT been sent to Adjuster","#","inputButton_sm_Action1");
            }
            else
            {
                myVal += getButtonCode_MSG("a_Ref_Complete","MRc:Ad","Report notification has been sent to Adjuster","#","inputButton_sm_Default");
            }

            if (!myEO.NIM3_Encounter.getSentToRefDr().after(PLCUtils.getDisplayDateSDF1().parse("1/1/1800")))
            {
                myVal += getButtonCode_MSG("a_Ref_Complete","MRc:Dr","Report notification has NOT been sent to Ref. Dr","#","inputButton_sm_Action1");
            }
            else
            {
                myVal += getButtonCode_MSG("a_Ref_Complete","MRc:Dr","Report notification has been sent to Ref. Dr","#","inputButton_sm_Default");
            }
            */

            //new version checks to see if All confirms are sent out
            //still need to coordinate this with user preferences   - done
            if (myEO2.isScanPassAlertsComplete(3) )
            {
                myVal += getButtonCode_MSG("a_Ref_Complete","RPc","Final Report Alerts/Confirmations appear to be completely sent out.","#","inputButton_sm_Default");
            }
            else
            {
                myVal += getButtonCode_MSG("a_Ref_Complete","RPc","Final Reports Alerts/Confirmations do not appear to have been completely sent out.\\n\\nPlease Review the Alert Panel for this Encounter.","#","inputButton_sm_Action1");
            }
            myVal += "</fieldset><fieldset style=\"display:inline;border:2px solid; margin-right:10px;padding-top:0\"><legend style=\"font-weight:bold\">Billing</legend>";


            boolean isBillReady = true;
            boolean isBillPay = true;

            if (myEO2.getNIM3_Encounter().getReportFileID().intValue()>0)
            {
                bltNIM3_Service_List        bltNIM3_Service_List        =    new    bltNIM3_Service_List(myEO2.getNIM3_Encounter().getEncounterID());
                //declaration of Enumeration
                bltNIM3_Service        working_bltNIM3_Service;
                com.winstaff.ListElement         leCurrentElement;
                java.util.Enumeration eList = bltNIM3_Service_List.elements();
                if (eList.hasMoreElements())
                {
                     while (eList.hasMoreElements())
                     {
                        leCurrentElement    = (ListElement) eList.nextElement();
                        working_bltNIM3_Service  = (bltNIM3_Service) leCurrentElement.getObject();
                        if (working_bltNIM3_Service.getBillAmount()==0||working_bltNIM3_Service.getBillAmount()==0||working_bltNIM3_Service.getCPTBodyPart().equalsIgnoreCase("")||working_bltNIM3_Service.getCPT().equalsIgnoreCase(""))

                        {
                            isBillReady = false;
                        }
                        if (working_bltNIM3_Service.getPaidOutAmount()==0)

                        {
                            isBillReady = false;
                        }
                     }
                }
                else
                {
                    isBillReady = false;
                    isBillPay = false;
                }

            }

//ready to bill
            if (myEO2.getNIM3_Encounter().getReportFileID().intValue()>0 && !myEO2.getNIM3_Encounter().getRec_Bill_Pay().after(NIMUtils.getBeforeTime())&&isBillReady&& !myEO2.getNIM3_Encounter().getSentTo_Bill_Pay().after(NIMUtils.getBeforeTime()))
            {
                myVal += getButtonCode_MSG("a_Ref_Complete","B:RI","Ready to Bill but not yet Billed.","#","inputButton_sm_Action1");
            }
            else if (myEO2.getNIM3_Encounter().getReportFileID().intValue()>0&&!myEO2.getNIM3_Encounter().getRec_Bill_Pay().after(NIMUtils.getBeforeTime())&& !myEO2.getNIM3_Encounter().getSentTo_Bill_Pay().after(NIMUtils.getBeforeTime()))
            {
                myVal += getButtonCode_MSG("a_Ref_Complete","B:RI","Need to Bill, but NOT Ready to Bill","#","inputButton_sm_Action1");
            }
            else if (myEO2.getNIM3_Encounter().getReportFileID().intValue()==0)
            {
                myVal += getButtonCode_MSG("a_Ref_Complete","B:RI","Report not upload so not Ready to Bill","#","inputButton_sm_Action1");
            }
            else if (  myEO2.getNIM3_Encounter().getSentTo_Bill_Pay().after(NIMUtils.getBeforeTime()) )
            {
                myVal += getButtonCode_MSG("a_Ref_Complete","B:RI","We have sent Bill to payer","#","inputButton_sm_Default");
            }
            else
            {
                myVal += getButtonCode_MSG("a_Ref_Complete","B:RI","Error in Bill Sent calc.","#","inputButton_sm_Action2");
            }
//been paid
            if (!myEO2.getNIM3_Encounter().getTimeTrack_ReqPaidIn().after(NIMUtils.getBeforeTime()))
            {
                myVal += getButtonCode_MSG("a_Ref_Complete","B:PI","Payment has not been fully received by Payer\\n\\nThis can be caused by the received amount not matching the allow amount even if received is greater.\\nIf this is the case, Adjustments need to be entered.","#","inputButton_sm_Action1");
            }
            else if (isPayInComplete(myEO2.getNIM3_Encounter().getEncounterID())!=null)
            {
                myVal += getButtonCode_MSG("a_Ref_Complete","B:PI","Payment has been fully received by Payer and the amounts appear correct.\\n\\nUser ID: " + myEO2.getNIM3_Encounter().getTimeTrack_ReqPaidOut_UserID() + "","#","inputButton_sm_Default");
            }
            else
            {
                myVal += getButtonCode_MSG("a_Ref_Complete","B:PI","Payment has been received by Payer; but amounts do not appear correct.\\n\\nUser ID: " + myEO2.getNIM3_Encounter().getTimeTrack_ReqPaidIn_UserID() + "","#","inputButton_sm_Action2");
            }


//ready to pay
            java.util.Calendar cal = java.util.Calendar.getInstance();
            cal.add(java.util.Calendar.DATE, -50);//NetDev Payment Terms - Need to set this dynmamically
            if (myEO2.getNIM3_Encounter().getReportFileID().intValue()>0 && !myEO2.getNIM3_Encounter().getTimeTrack_ReqPaidOut().after(NIMUtils.getBeforeTime())&&isBillPay&&myEO2.getNIM3_Encounter().getRec_Bill_Pro().after(NIMUtils.getBeforeTime()) )
            {
                myVal += getButtonCode_MSG("a_Ref_Complete","B:RO","Payment is Due and Payment is Ready.","#","inputButton_sm_Action1");
            }
            else if ( !myEO2.getNIM3_Encounter().getTimeTrack_ReqPaidOut().after(NIMUtils.getBeforeTime())&&myEO2.getNIM3_Encounter().getRec_Bill_Pro().after(NIMUtils.getBeforeTime())  )
            {
                myVal += getButtonCode_MSG("a_Ref_Complete","B:RO","Payment is Due but NOT Ready - Please Check for Amounts","#","inputButton_sm_Action1");
            }
            else if (myEO2.getNIM3_Encounter().getTimeTrack_ReqPaidOut().after(NIMUtils.getBeforeTime())  )
            {
                myVal += getButtonCode_MSG("a_Ref_Complete","B:RO","We have sent Payment to provider","#","inputButton_sm_Default");
            }
            else if (!myEO2.getNIM3_Encounter().getRec_Bill_Pro().after(NIMUtils.getBeforeTime())   )
            {
                myVal += getButtonCode_MSG("a_Ref_Complete","B:RO","We have not received an invoice (HFCA-1550, etc.) from Provider","#","inputButton_sm_Action1");
            }
            else
            {
                myVal += getButtonCode_MSG("a_Ref_Complete","B:RO","Error in Provider Pay Out Calc.","#","inputButton_sm_Action2");
            }


//Is paid
            if (!myEO2.getNIM3_Encounter().getTimeTrack_ReqPaidOut().after(NIMUtils.getBeforeTime()))
            {
                myVal += getButtonCode_MSG("a_Ref_Complete","B:PO","Payment has not been sent to Provider.","#","inputButton_sm_Action1");
            }
            else if (isPayOutComplete(myEO2.getNIM3_Encounter().getEncounterID())!=null)
            {
                myVal += getButtonCode_MSG("a_Ref_Complete","B:PO","Payment has been sent to Provider and the amounts appear correct.\\n\\nUser ID: " + myEO2.getNIM3_Encounter().getTimeTrack_ReqPaidOut_UserID() + "","#","inputButton_sm_Default");
            }
            else
            {
                myVal += getButtonCode_MSG("a_Ref_Complete","B:PO","Payment appears to have been sent, but the numbers do not seem to add up correctly.\\n\\nUser ID: " + myEO2.getNIM3_Encounter().getTimeTrack_ReqPaidOut_UserID() + "","#","inputButton_sm_Action2");
            }
            myVal += "</fieldset>";
        }
        catch (Exception EEE)
        {
            myVal = "Error Processing: aEncounterStatus_HTML: " + EEE;
        }

            myVal += "<!--end referral status bar -->";
        return myVal;
    }



//this procedure validates whether payment to a center is complete
public static java.util.Date isPayInComplete(Integer iEncounterID)
{
  //  DebugLogger.printLine("NIMUtils:isPayInComplete: [Started]");
    java.util.Date myval = null;
    bltNIM3_Encounter NIME = new bltNIM3_Encounter(iEncounterID);
    bltNIM3_Service_List        bltNIM3_Service_List        =    new    bltNIM3_Service_List(iEncounterID);
    //declaration of Enumeration
    bltNIM3_Service        working_bltNIM3_Service;
    com.winstaff.ListElement         leCurrentElement;
    java.util.Enumeration eList = bltNIM3_Service_List.elements();
    java.util.Date EarliestDate = null;
    double myTotalExpectedIN = 0;
    double myTotalIN = 0;
    double myTotalAdjustment = 0;
     while (eList.hasMoreElements())
     {
        leCurrentElement    = (ListElement) eList.nextElement();
        working_bltNIM3_Service  = (bltNIM3_Service) leCurrentElement.getObject();
         if (working_bltNIM3_Service.getServiceStatusID().intValue()==1)
         {

            myTotalExpectedIN += working_bltNIM3_Service.getAllowAmount();
             myTotalIN += (working_bltNIM3_Service.getReceivedCheck1Amount()+working_bltNIM3_Service.getReceivedCheck2Amount()+working_bltNIM3_Service.getReceivedCheck3Amount());
             myTotalAdjustment += (working_bltNIM3_Service.getAllowAmountAdjustment());
            if (EarliestDate==null)
            {
                EarliestDate =  working_bltNIM3_Service.getReceivedCheck1Date();
            }
             if (working_bltNIM3_Service.getReceivedCheck1Date().after(NIMUtils.getBeforeTime())&&working_bltNIM3_Service.getReceivedCheck1Date().before(EarliestDate))
             {
                 EarliestDate =  working_bltNIM3_Service.getReceivedCheck1Date();
             }
             if (working_bltNIM3_Service.getReceivedCheck2Date().after(NIMUtils.getBeforeTime())&&working_bltNIM3_Service.getReceivedCheck2Date().before(EarliestDate))
             {
                 EarliestDate =  working_bltNIM3_Service.getReceivedCheck2Date();
             }
             if (working_bltNIM3_Service.getReceivedCheck3Date().after(NIMUtils.getBeforeTime())&&working_bltNIM3_Service.getReceivedCheck3Date().before(EarliestDate))
             {
                 EarliestDate =  working_bltNIM3_Service.getReceivedCheck3Date();
             }
         }

     }
//    DebugLogger.printLine("NIMUtils:isPayInComplete: [" + myTotalExpectedIN  + "] = [" + myTotalIN  + "] - [" + myTotalAdjustment  + "]");
//    DebugLogger.printLine("NIMUtils:isPayInComplete: [" + myTotalExpectedIN  + "] = [" + (myTotalIN  + myTotalAdjustment)  + "]");
    //todo: Fix for Round issues     @@@@@
    if (myTotalIN == (myTotalExpectedIN  + myTotalAdjustment)   )
    {
        myval = EarliestDate;
//        DebugLogger.printLine("NIMUtils:isPayInComplete: [\" + myval  + \"]");
    }
    else
    {
//        DebugLogger.printLine("NIMUtils:isPayInComplete: [Payments do NOT match]");
    }
    if (myTotalIN==0)
    {
        myval = null;
    }
    return myval;

}

//this procedure validates whether payment to a center is complete
public static java.util.Date isPayOutComplete(Integer iEncounterID)
{
    java.util.Date myval = null;
    bltNIM3_Encounter NIME = new bltNIM3_Encounter(iEncounterID);
    bltNIM3_Service_List        bltNIM3_Service_List        =    new    bltNIM3_Service_List(iEncounterID);
    //declaration of Enumeration
    bltNIM3_Service        working_bltNIM3_Service;
    com.winstaff.ListElement         leCurrentElement;
    java.util.Enumeration eList = bltNIM3_Service_List.elements();
    java.util.Date EarliestDate = null;
    double myTotalOut = 0;
     while (eList.hasMoreElements())
     {
        leCurrentElement    = (ListElement) eList.nextElement();
        working_bltNIM3_Service  = (bltNIM3_Service) leCurrentElement.getObject();
        if (working_bltNIM3_Service.getServiceStatusID().intValue()==1)
        {
            myTotalOut += working_bltNIM3_Service.getPaidOutAmount();
        }
     }
    if (EarliestDate==null)
    {
        EarliestDate =  NIME.getPaidToProviderCheck1Date();
    }
    if (NIME.getPaidToProviderCheck2Date().after(NIMUtils.getBeforeTime())&&NIME.getPaidToProviderCheck2Date().before(EarliestDate))
    {
        EarliestDate =  NIME.getPaidToProviderCheck2Date();
    }
    if (NIME.getPaidToProviderCheck3Date().after(NIMUtils.getBeforeTime())&&NIME.getPaidToProviderCheck3Date().before(EarliestDate))
    {
        EarliestDate =  NIME.getPaidToProviderCheck3Date();
    }
    //todo: Fix for Round issues     @@@@@
    if (myTotalOut == NIME.getPaidToProviderCheck1Amount()+NIME.getPaidToProviderCheck2Amount()+NIME.getPaidToProviderCheck3Amount())
    {
        myval = EarliestDate;
    }
    if (myTotalOut==0)
    {
        myval = null;
    }
    return myval;

}
    public static boolean sendNewUserAccountEmail(bltUserAccount UserAccount, String PWD, boolean isReset, boolean isDirectSend)
    {
        boolean myVal = false;
        try
        {
            if (NIMUtils.isValidEmail(UserAccount.getContactEmail()))
            {
                emailType_V3 myETSend = new emailType_V3();
                myETSend.setFrom("ClientSupport@nextimagemedical.com");
                myETSend.setSubject("NextImage Medical: Account Update");
                String theBody = UserAccount.getContactFirstName() + ", welcome to the NextImage Medical web portal. You have received this email as NextImage Medical is an approved radiology partner/provider for your company.  NextImage Medical features excellent quality MRIs, CT scans, X-Rays and EMGs at competitive prices, offering quick turn-around time for both scheduling and reports. We are pleased to introduce you to our uniquely provisioned web portal!\n" +
                        "\n" +
                        "Our website, http://www.NextImageMedical.com, is secure and HIPAA compliant. It is designed to help you make your job easier when you work with us. Once you log on, you will have these features at your fingertips:\n" +
                        "\n" +
                        "\t1.  Monitor the progress of your cases: Received, Scheduled, Completed.\n" +
                        "\t2.  Download scanned original documents such as the treating physician's prescription (Rx) and the final medical report (MR faxed or e-mailed to you but you can retrieve it on the portal if you wish to do so).\n" +
                        "\t3.  If available, the actual radiology image is viewable on-line.\n" +
                        "\t4.  Submit new cases on-line in a concise easy format for faster service; once logged-in, never have to type your name or number to submit a referral!\n" +
                        "\t5.  Change your contact information or how you want us to communicate with you (click User Settings box to access your profile and Save to update).  You can designate here whether you want reports sent by secure email which requires your ID to open, regular email attachment or fax. \n" +
                        "\n" +
                        "To begin, visit http://www.NextImageMedical.com.  Click on Secure Login in the top right corner so we will be able to link your contact information and save your cases for your review. NOTE: If you instead click on 'Refer a Case' you will have to enter contact information since we will not have a profile linked.\n" +
                        "  \n" +
                        "You may now login and customize your own password, simply go to User Settings and enter your current password to change. The new one will need to be 8 characters any number or alpha combination.\n" +
                        "\n" +
                        "Your username is:  " + UserAccount.getLogonUserName() + "\n" +
                        "\nYour password is:  " + PWD + "\n" +
                        "\n" +
                        "If you have any questions, " +
                        "please email us at ClientSupport@nextimagemedical.com or call us at 858.764.8285. " +
                        "Thank You for using NextImage Medical.\n" +
                        "\n" +
                        "NextImage Medical\n" +
                        "Client Support Team\n" +
                        "\n" +
                        "...Image is Everything\nV3.6h";
                myETSend.setBody(theBody);
                myETSend.setBodyType(myETSend.PLAIN_TYPE);
                if (isDirectSend)
                {
                    myETSend.setTo(UserAccount.getContactEmail()); //direct send
                    myETSend.isSendMail();
                }
                else
                {
                    myETSend.setSubject("COPY: NextImage Medical: Account Update");
                    myETSend.setTo("ClientSupport@nextimagemedical.com");
                    //myETSend.isSendMail();
                }
                myETSend.setSubject("COPY: NextImage Medical: Account Update");
                if (isReset)
                {
                    myETSend.setSubject("COPY-RESET: NextImage Medical: Account Update");
                }
                myETSend.setTo("scott@nextimagemedical.com");      //duplicate to Scott
                myETSend.isSendMail();
                myVal = true;
            }
        }
        catch(Exception e)
        {
            myVal = false;
            DebugLogger.println("NIMUtils:sendNewUserAccountEmail:"+e);
        }
        return myVal;

    }


    public static  boolean isEncounterVoid(Integer iEncounterID)
    {
        return isEncounterVoid(new bltNIM3_Encounter(iEncounterID));
    }

    public static  boolean isEncounterVoid(com.winstaff.bltNIM3_Encounter nimE)
    {
        boolean myVal = false;
        if (nimE.getEncounterStatusID().intValue()==5||nimE.getEncounterStatusID().intValue()==2||nimE.getEncounterStatusID().intValue()==3||nimE.getEncounterStatusID().intValue()==10)
        {
            myVal=true;
        }
        return myVal;
    }


    public static String getListOfValidUserByPayer_HTMLSelect(Integer iPayerID, Integer iUserID)
    {
        String myVal = "Blank";
        searchDB2 mySS = new searchDB2();
        java.sql.ResultSet myRS = null;
        if (!isValidPayerForGroups(iPayerID))
        {
            //special payers - always fail.
            myVal = "Invalid Payer";
        }
        else
        {
             myVal = "";
            try
            {
                String mySQL = "SELECT Userid, ContactFirstName, ContactLastName from tUserAccount where Payerid = ? and Payerid>0 order by ContactLastName";
                db_PreparedStatementObject myDPSO = new db_PreparedStatementObject(mySQL);
                myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.INT_TYPE,iPayerID.toString()));
                myRS = mySS.executePreparedStatement(myDPSO);
                while(myRS!=null&&myRS.next())
                {
                    myVal +="<option ";
                    if (new Integer (myRS.getString("userid"))==iUserID)
                    {
                        myVal +=" selected ";
                    }
                    myVal +=" value=\"" + myRS.getString("userid") + "\">" + myRS.getString("contactlastname") + ", " + myRS.getString("contactfirstname") + "</option>";
                }
                mySS.closeAll();
            } catch(Exception e) {
                DebugLogger.printLine("NIMUtils:getListOfValidUserByPayer_HTMLSelect:error:" + e);
            } finally {
                mySS.closeAll();
            }
        }
        return myVal;
    }

    public static boolean isValidPayerForGroups(Integer iPayerID)
    {
        boolean myVal = false;
        if (iPayerID==0||iPayerID==23||iPayerID==1||iPayerID==50)
        {
            //special payers - always fail.
            myVal = false;
        }
        else
        {
            myVal = true;
        }
        return myVal;
    }


    public static boolean isValidUserByPayer(Integer iUserID, Integer iPayerID)
    {
        boolean myVal = false;
        searchDB2 mySS = new searchDB2();
        java.sql.ResultSet myRS = null;
        if (!isValidPayerForGroups(iPayerID))
        {
            //special payers - always fail.
            myVal = false;
        }
        else
        {
            try
            {
                String mySQL = "SELECT userid from tUserAccount where Payerid = ? and UserID = ? and Payerid>0";
                db_PreparedStatementObject myDPSO = new db_PreparedStatementObject(mySQL);
                myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.INT_TYPE,iPayerID.toString()));
                myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.INT_TYPE,iUserID.toString()));
                myRS = mySS.executePreparedStatement(myDPSO);
                if(myRS!=null&&myRS.next())
                {
                    myVal=true;
                }
                mySS.closeAll();
            } catch(Exception e) {
                DebugLogger.printLine("NIMUtils:isValidUseByPayer:error:" + e);
            } finally {
                mySS.closeAll();
            }
        }
        return myVal;
    }


    public static boolean isEncounterServiceComplete(Integer iEncounterID)
    {
        bltNIM3_Service_List        bltNIM3_Service_List        =    new    bltNIM3_Service_List(iEncounterID);
            return isEncounterServiceComplete(bltNIM3_Service_List) ;
    }



    public static boolean isEncounterServiceComplete(bltNIM3_Service_List bltNIM3_Service_List )
    {
        boolean isReady = true;
        if (isReady)
        {
            //declaration of Enumeration
            bltNIM3_Service        working_bltNIM3_Service;
            com.winstaff.ListElement         leCurrentElement;
            java.util.Enumeration eList = bltNIM3_Service_List.elements();
            if (eList.hasMoreElements())
            {
                 while (eList.hasMoreElements()&&isReady)
                 {
                    leCurrentElement    = (ListElement) eList.nextElement();
                    working_bltNIM3_Service  = (bltNIM3_Service) leCurrentElement.getObject();
                     //if (working_bltNIM3_Service.getCPTBodyPart().equalsIgnoreCase(""))
                     //isComplete only works for some status
                     if ( (working_bltNIM3_Service.getServiceStatusID()==1||working_bltNIM3_Service.getServiceStatusID()==3||working_bltNIM3_Service.getServiceStatusID()==4)&&(!working_bltNIM3_Service.isComplete()))
                     {
                        isReady = false;
                     }
                     else if ( working_bltNIM3_Service.getServiceStatusID()==0)
                     {
                        isReady = false;
                     }
                 }
            }
            else
            {
                isReady = false;
            }
        }
        return isReady;
    }


//This Procedure tests whether or not a referral is ready to schedule. In order to schedule, req data must be completed and certain documents must be uploaded.
    public static boolean isEncounterReadyToSchedule(Integer iEncounterID)
        {
            return isEncounterReadyToSchedule(iEncounterID, false, true);
        }



//This Procedure tests whether or not a referral is ready to schedule. In order to schedule, req data must be completed and certain documents must be uploaded.
public static boolean isEncounterReadyToSchedule(Integer iEncounterID, boolean RequireRxReview, boolean RequirePreScreen)
{
    return (isEncounterReadyToSchedule(new NIM3_EncounterObject(iEncounterID), RequireRxReview, RequirePreScreen ));
}
/**
 * Used in the new Adjuster Portal. Informs Adjusters what stage their case is at. Added 07-01-2013
 * @param encounterID - ID of encounter to analyze 
 * @return Status Code 0) New 1) Processed 2) Scheduled 3) Has Report
 */
public static int getCaseStatus(int encounterID){
	
	if(new bltNIM3_Encounter(encounterID).getReportFileID()>0){
		return 3;
	} else if (new bltNIM3_Encounter(encounterID).getAppointmentID()>0){
		return 2;
	} else if(isEncounterReadyToSchedule(encounterID, false, false)){
		return 1;
	}
	
	return 0;
}
//TODO: Depricate
//This Procedure tests whether or not a referral is ready to schedule. In order to schedule, req data must be completed and certain documents must be uploaded.
    public static boolean isEncounterReadyToSchedule(NIM3_EncounterObject myEO, boolean RequireRxReview, boolean RequirePreScreen)
	{
        boolean isReady = true;
        if (NIMUtils.isEncounterVoid(myEO.NIM3_Encounter))
        {
            isReady = false;
        }
        if (myEO.NIM3_Referral.getReferringPhysicianID()==0)
        {
            isReady = false;
        }
/*
		else if (!myEO.NIM3_CaseAccount.getDateOfInjury().before(new java.util.Date(1)))
		{
			isReady = false;
		}
*/
		else if (myEO.NIM3_CaseAccount.getAdjusterID()==0)
		{
			isReady = false;
		}
		else if (myEO.NIM3_CaseAccount.getCaseClaimNumber().equalsIgnoreCase(""))
		{
			isReady = false;
		}
		else if (myEO.NIM3_CaseAccount.getPatientFirstName().equalsIgnoreCase("")||myEO.NIM3_CaseAccount.getPatientLastName().equalsIgnoreCase("")||myEO.NIM3_CaseAccount.getPatientZIP().equalsIgnoreCase(""))
		{
			isReady = false;
		}
        else if (RequirePreScreen&&!myEO.NIM3_CaseAccount.isComplete())  //not required for RX Review 2011-01-11
        {
            isReady = false;
        }
        else if (RequirePreScreen&&!myEO.isPreScreenComplete()) //not required for RX Review 2011-01-11
        {
            isReady = false;
        }
        else if (!isCaseCreateCriticalDone(myEO))
        {
            isReady = false;
        }
        else if (!myEO.NIM3_Encounter.isComplete())
        {
            isReady = false;
        }
        else if (!myEO.NIM3_Referral.isComplete())
        {
            isReady = false;
        }
        else if (RequireRxReview&&!myEO.NIM3_Encounter.getTimeTrack_ReqRxReview().after(NIMUtils.getBeforeTime())) 
        {
            isReady = false;
        }
        else if (RequireRxReview&&myEO.NIM3_Encounter.getTimeTrack_ReqRxReview_UserID().intValue()==0) 
        {
            isReady = false;
        }
        else if (myEO.NIM3_Referral.getRxFileID().intValue()==0)
        {
            isReady = false;
        }
        else if (myEO.NIM3_Referral.getOrderFileID().intValue()==0) 
        {
            isReady = false;
        }
        else if (!NIMUtils.isEncounterServiceComplete(myEO.NIM3_Encounter.getEncounterID()))
        {
            isReady = false;
        }

		return isReady;
		
	}


//This Procedure tests whether or not a referral is ready to schedule. In order to schedule, req data must be completed and certain documents must be uploaded.
public static boolean isEncounterReadyToSchedule(NIM3_EncounterObject2 myEO2, boolean RequireRxReview, boolean RequirePreScreen)
{
    boolean isReady = true;
    if (NIMUtils.isEncounterVoid(myEO2.getNIM3_Encounter()))
    {
        isReady = false;
    }
    if (myEO2.getNIM3_Referral().getReferringPhysicianID()==0)
    {
        isReady = false;
    }
/*
		else if (!myEO.NIM3_CaseAccount.getDateOfInjury().before(new java.util.Date(1)))
		{
			isReady = false;
		}
*/
    else if (myEO2.getNIM3_CaseAccount().getAdjusterID()==0)
    {
        isReady = false;
    }
    else if (myEO2.getNIM3_CaseAccount().getCaseClaimNumber().equalsIgnoreCase(""))
    {
        isReady = false;
    }
    else if (myEO2.getNIM3_CaseAccount().getPatientFirstName().equalsIgnoreCase("")||myEO2.getNIM3_CaseAccount().getPatientLastName().equalsIgnoreCase("")||myEO2.getNIM3_CaseAccount().getPatientZIP().equalsIgnoreCase(""))
    {
        isReady = false;
    }
    else if (RequirePreScreen&&!myEO2.getNIM3_CaseAccount().isComplete())  //not required for RX Review 2011-01-11
    {
        isReady = false;
    }
    else if (RequirePreScreen&&!myEO2.isPreScreenComplete()) //not required for RX Review 2011-01-11
    {
        isReady = false;
    }
    else if (!isCaseCreateCriticalDone(myEO2))
    {
        isReady = false;
    }
    else if (!myEO2.getNIM3_Encounter().isComplete())
    {
        isReady = false;
    }
    else if (!myEO2.getNIM3_Referral().isComplete())
    {
        isReady = false;
    }
    else if (RequireRxReview&&!myEO2.getNIM3_Encounter().getTimeTrack_ReqRxReview().after(NIMUtils.getBeforeTime()))
    {
        isReady = false;
    }
    else if (RequireRxReview&&myEO2.getNIM3_Encounter().getTimeTrack_ReqRxReview_UserID().intValue()==0)
    {
        isReady = false;
    }
    else if (myEO2.getNIM3_Referral().getRxFileID().intValue()==0)
    {
        isReady = false;
    }
    else if (myEO2.getNIM3_Referral().getOrderFileID().intValue()==0)
    {
        isReady = false;
    }
    else if (!NIMUtils.isEncounterServiceComplete(myEO2.getNIM3_Encounter().getEncounterID()))
    {
        isReady = false;
    }

    return isReady;

}




public static String returnEmailAsHTML(emailType_V3 myET, boolean includeTo)
{
    String myVal = "<h4>From: " + myET.getFrom() + "</h4>";
    if (includeTo)
    {
        myVal += "<h4>To: " + myET.getTo() + "</h4>";
        myVal += "<h5>CC: ";
        for (int i=0;i<myET.cc.size();i++)
        {
            myVal += (String)myET.cc.elementAt(i) + "; ";
        }
        myVal += "</h5>";
    }
    myVal += "<h3>Subject: " + myET.getSubject() + "</h3>";
    if (!myET.getVirtualAttachment().equalsIgnoreCase(""))
    {
        myVal += "<h4>Attachment: " + myET.getVirtualAttachment() + "</h4>";
    }
    myVal += "<textarea cols=80 rows=20 readonly>" + myET.getBody() + "</textarea>";
    return myVal;
}

public static String getEmailSignature(Integer iUserID)
{
    String myVal = "";
    bltUserAccount myUA = new bltUserAccount(iUserID);
    myVal +="\n\nIf you have any questions, please call us. Thank You,\n\n" + myUA.getContactFirstName() + " " + myUA.getContactLastName() + "\nPhone: " + myUA.getContactPhone() + "\nFax: " + myUA.getContactFax();
    return myVal;
}


    public static String fax2EmailFax(String myFaxNumber)
    {
        return fax2EmailFax(myFaxNumber, emailType_V3.FAX_SUFFIX);
    }

    public static String fax2EmailFax(String myFaxNumber, String suffix)
    {
        String myVal = myFaxNumber.replaceAll("[^0-9]", "");
        if (myVal.indexOf("1")!=0)
        {
            myVal = "1"+myVal;
        }
        myVal += suffix;
        return myVal;
    }



    public static String getAppointmentDetails(Integer iAppointmentID, boolean isHTML)
{
    String myVal = "Appointment Details:\n";
    if (iAppointmentID==0)
    {
        myVal +=" No Appointment Scheduled";
    }
    else
    {
        bltNIM3_Appointment myAppt = new bltNIM3_Appointment(iAppointmentID);
        bltPracticeMaster myPM = new bltPracticeMaster(myAppt.getProviderID());
        myVal += "Provider: " + myPM.getPracticeName();
        myVal += "\nPractice ID: " + myPM.getPracticeID();
        myVal += "\nCurrent Status: " + new bltPracticeContractStatusLI(myPM.getContractingStatusID()).getDescriptionLong();
        myVal += "\nAppointment Time: " + myAppt.getAppointmentTime();
        myVal += "\nAppointment ID: " + myAppt.getUniqueID();
    }
    return myVal;
}

public static NIM3_AddressObject getWhoShouldIBillTo(Integer iPayerID, Integer iBillTo)
{
    NIM3_AddressObject myVal = new NIM3_AddressObject();
    if (iBillTo>0)   //use billto
    {
        bltUserAccount UABillTo = new bltUserAccount(iBillTo);
        //use BillTo User?
        if (!UABillTo.getContactAddress1().equalsIgnoreCase("")&&!UABillTo.getContactCity().equalsIgnoreCase("")&&!UABillTo.getContactZIP().equalsIgnoreCase(""))
        {
            myVal.setAddress1(UABillTo.getContactAddress1());
            myVal.setAddress2(UABillTo.getContactAddress2());
            myVal.setCity(UABillTo.getContactCity());
            myVal.setStateID(UABillTo.getContactStateID());
            myVal.setZIP(UABillTo.getContactZIP());
            myVal.setPhone(UABillTo.getContactPhone());
            myVal.setFax(UABillTo.getContactFax());
            myVal.setAddressName(UABillTo.getCompanyName());
            return myVal;
        }
        else  //use Bill To Payer
        {
            return getPayerBillingAddress(UABillTo.getPayerID());
        }
    }
    else  //use payer
    {
        return getPayerBillingAddress(iPayerID);
    }

}

public static NIM3_AddressObject getPayerBillingAddress(Integer iPayerID)
{
    NIM3_AddressObject myVal = new NIM3_AddressObject();
    bltNIM3_PayerMaster myPM = new bltNIM3_PayerMaster(iPayerID);
    bltNIM3_PayerMaster myPPM = null;
    boolean hasParent = false;
    if (myPM.getParentPayerID()!=0)
    {
        myPPM = new bltNIM3_PayerMaster(myPM.getParentPayerID());
        hasParent = true;
    }
    if (!myPM.getBillingAddress1().equalsIgnoreCase("")&&!myPM.getBillingCity().equalsIgnoreCase("")&&!myPM.getBillingZIP().equalsIgnoreCase(""))
    {
        myVal.setAddress1(myPM.getBillingAddress1());
        myVal.setAddress2(myPM.getBillingAddress2());
        myVal.setCity(myPM.getBillingCity());
        myVal.setStateID(myPM.getBillingStateID());
        myVal.setZIP(myPM.getBillingZIP());
        myVal.setPhone(myPM.getBillingPhone());
        myVal.setFax(myPM.getBillingFax());
        if (myPM.getBillingName().equalsIgnoreCase(""))
        {
            myVal.setAddressName(myPM.getPayerName());
        }
        else
        {
            myVal.setAddressName(myPM.getBillingName());
        }
    }
    else if (hasParent&&!myPPM.getBillingAddress1().equalsIgnoreCase("")&&!myPPM.getBillingCity().equalsIgnoreCase("")&&!myPPM.getBillingZIP().equalsIgnoreCase(""))
    {
        myVal.setAddress1(myPPM.getBillingAddress1());
        myVal.setAddress2(myPPM.getBillingAddress2());
        myVal.setCity(myPPM.getBillingCity());
        myVal.setStateID(myPPM.getBillingStateID());
        myVal.setZIP(myPPM.getBillingZIP());
        myVal.setPhone(myPPM.getBillingPhone());
        myVal.setFax(myPPM.getBillingFax());
        myVal.setAddressName(myPPM.getBillingName());
        if (myPPM.getBillingName().equalsIgnoreCase(""))
        {
            myVal.setAddressName(myPPM.getPayerName());
        }
        else
        {
            myVal.setAddressName(myPPM.getBillingName());
        }
    }
    else if (!myPM.getOfficeAddress1().equalsIgnoreCase("")&&!myPM.getOfficeCity().equalsIgnoreCase("")&&!myPM.getOfficeZIP().equalsIgnoreCase(""))
    {
        myVal.setAddress1(myPM.getOfficeAddress1());
        myVal.setAddress2(myPM.getOfficeAddress2());
        myVal.setCity(myPM.getOfficeCity());
        myVal.setStateID(myPM.getOfficeStateID());
        myVal.setZIP(myPM.getOfficeZIP());
        myVal.setPhone(myPM.getOfficePhone());
        myVal.setFax(myPM.getOfficeFax());
        myVal.setAddressName(myPM.getPayerName());
    }
    else if (hasParent&&!myPPM.getOfficeAddress1().equalsIgnoreCase("")&&!myPPM.getOfficeCity().equalsIgnoreCase("")&&!myPPM.getOfficeZIP().equalsIgnoreCase(""))
    {
        myVal.setAddress1(myPPM.getOfficeAddress1());
        myVal.setAddress2(myPPM.getOfficeAddress2());
        myVal.setCity(myPPM.getOfficeCity());
        myVal.setStateID(myPPM.getOfficeStateID());
        myVal.setZIP(myPPM.getOfficeZIP());
        myVal.setPhone(myPPM.getOfficePhone());
        myVal.setFax(myPPM.getOfficeFax());
        myVal.setAddressName(myPPM.getPayerName());
    }
    else
    {
        myVal.setAddressName("No Address On File");
    }
    return myVal;

}


public static String processEncounterProblem(Integer iEncounterID, String sFlag, String sNote,  Integer iCurrentUserID, String UserLogonDescription, Integer iExtra_Code)
{
    String myVal = "";
    try
    {
        bltUserAccount CurrentUserAccount = new bltUserAccount(iCurrentUserID);
        bltNIM3_PayerMaster myPM = new bltNIM3_PayerMaster(CurrentUserAccount.getPayerID());
        bltNIM3_Encounter myEnc = new bltNIM3_Encounter(iEncounterID);
        myEnc.setUniqueModifyComments("NIMUtils:processEncounterProblem:System Generated [ULD:"+UserLogonDescription+"]");
        bltNIM3_Referral myRef = new bltNIM3_Referral(myEnc.getReferralID());
        myRef.setUniqueModifyComments("NIMUtils:processEncounterProblem:System Generated [ULD:"+UserLogonDescription+"]");
        bltNIM3_CaseAccount myCA = new bltNIM3_CaseAccount(myRef.getCaseID());

        bltNIM3_CommTrack        working_bltNIM3_CommTrack = new bltNIM3_CommTrack();
        working_bltNIM3_CommTrack.setUniqueModifyComments("NIMUtils:processEncounterProblem:System Generated [ULD:"+UserLogonDescription+"]");
        working_bltNIM3_CommTrack.setEncounterID(iEncounterID);
        working_bltNIM3_CommTrack.setReferralID(myEnc.getReferralID());
        working_bltNIM3_CommTrack.setCaseID(myRef.getCaseID());
        working_bltNIM3_CommTrack.setMessageName(CurrentUserAccount.getContactFirstName()+" "+CurrentUserAccount.getContactLastName());
        working_bltNIM3_CommTrack.setMessageCompany(myPM.getPayerName());
        working_bltNIM3_CommTrack.setUniqueCreateDate(PLCUtils.getNowDate(false));
        working_bltNIM3_CommTrack.setUniqueModifyDate(PLCUtils.getNowDate(false));
        working_bltNIM3_CommTrack.setCommStart(PLCUtils.getNowDate(false));
        working_bltNIM3_CommTrack.setCommEnd(PLCUtils.getNowDate(false));
        working_bltNIM3_CommTrack.setUniqueModifyComments(""+UserLogonDescription);
        working_bltNIM3_CommTrack.setAlertStatusCode(new Integer(1));
        
        String theMessage = sNote;
        theMessage += "\n\n---System Generated Notes:";
        theMessage += "\nCase Currently Assigned to: " + new bltUserAccount(myCA.getAssignedToID()).getLogonUserName() + " [" + myCA.getAssignedToID() + "]";
        bltNIM3_Appointment myAppt = new bltNIM3_Appointment(myEnc.getAppointmentID());
        myAppt.setUniqueModifyComments("NIMUtils:processEncounterProblem:System Generated [ULD:"+UserLogonDescription+"]");
        bltPracticeMaster myPract = new bltPracticeMaster(myAppt.getProviderID());
        theMessage += NIMUtils.getAppointmentDetails(myAppt.getAppointmentID(), false);

        if (sFlag.equalsIgnoreCase("problem_leak"))
        {
            working_bltNIM3_CommTrack.setMessageText("Problem: Leak\n\n"+theMessage);
            myEnc.setEncounterStatusID(new Integer(12));  //escalate to manager
            myEnc.commitData();
            emailType_V3 myEM = new emailType_V3();
            myEM.setTo("scheduling.managers@nextimagemedical.com");
            myEM.setFrom("server@nextimagemedical.com");
            myEM.setSubject("Encounter Leak: SP: " + myEnc.getScanPass() + "[" + myCA.getPatientFirstName() + "]");
            myEM.setBody("ScanPass [" + myEnc.getScanPass() + "] has been escalated.  Please review the note below as well as any appropriate CommTracks on the system\n\n---\nFrom: " + working_bltNIM3_CommTrack.getMessageName() + " @ " + working_bltNIM3_CommTrack.getMessageCompany() + "\nNote: " + theMessage + "\n" + NIMUtils.getAppointmentDetails(myAppt.getAppointmentID(), false) + "\n---");
            myEM.isSendMail();
            myVal = "Encounter Escalated to Manager and Email sent to Scheduling Managers re: Leaked Encounter";
        }
        else if (sFlag.equalsIgnoreCase("problem_noshow"))
        {
            myEnc.setAppointmentID(new Integer(0));
            myEnc.commitData();
            myAppt.setiStatus(new Integer(2));
            myAppt.setVoidNotes(sNote);
            myAppt.setInitialEncounterID(myEnc.getEncounterID());
            myAppt.commitData();
            working_bltNIM3_CommTrack.setMessageText("Problem: No-Show [ApptID:"+myAppt.getUniqueID()+"]\n\n"+theMessage);
            emailType_V3 myEM = new emailType_V3();
            myEM.setTo("netdev.managers@nextimagemedical.com");
            myEM.setFrom("server@nextimagemedical.com");
            myEM.setSubject("Encounter No-Show: SP: " + myEnc.getScanPass() + "[" + myCA.getPatientLastName() + "]");
            myEM.setBody("ScanPass [" + myEnc.getScanPass() + "] has been reported as a No-Show.  Please review the note below as well as any appropriate CommTracks on the system\n\n---\nFrom: " + working_bltNIM3_CommTrack.getMessageName() + " @ " + working_bltNIM3_CommTrack.getMessageCompany() + "\nNote: " + theMessage + "\n" + NIMUtils.getAppointmentDetails(myAppt.getAppointmentID(), false) + "\n---");
            myEM.isSendMail();
            myVal = "Appointment Archived and Email sent to NetDev Managers";
        }
        else if (sFlag.equalsIgnoreCase("problem_resched"))
        {
            working_bltNIM3_CommTrack.setMessageText("Problem: Reschedule [ApptID:"+myAppt.getUniqueID()+"]\n\n"+theMessage);
            myEnc.setAppointmentID(new Integer(0));
            myEnc.setReportFollowUpCounter(0);
            myEnc.commitData();
//            myEnc.setAppointmentID(new Integer(0));
//            myEnc.commitData();
            myAppt.setiStatus(new Integer(4));
            myAppt.setVoidNotes(sNote);
            myAppt.setInitialEncounterID(myEnc.getEncounterID());
            myAppt.commitData();
            myVal = "Appointment Archived";
        }
        else if (sFlag.equalsIgnoreCase("problem_bypassrx"))
        {
            int BypassRXID = EncounterBypassRXID;          //4835 for prod
            working_bltNIM3_CommTrack.setMessageText("Problem: Bypass Rx/RxReview]\n\n"+theMessage);
            myRef.setRxFileID(BypassRXID);
            myRef.commitData();
            myEnc.setTimeTrack_ReqRxReview(PLCUtils.getNowDate(true));
            myEnc.setTimeTrack_ReqRxReview_UserID(iCurrentUserID);
            myEnc.commitData();
            myVal = "Rx/Rx Review Bypassed";
        }
        else if (sFlag.equalsIgnoreCase("problem_escalate"))
        {
            working_bltNIM3_CommTrack.setMessageText("Problem: Escalate to Manager\n\n"+theMessage);
            myEnc.setEncounterStatusID(new Integer(12));  //escalate to manager
            myEnc.setProblemCategory(Integer.toString(iExtra_Code.intValue()));
            myEnc.setProblemDescription(sNote);
            myEnc.commitData();
            emailType_V3 myEM = new emailType_V3();
            myEM.setTo("scheduling.managers@nextimagemedical.com");
            //myEnc.setProblemCategory(Integer.toString(iExtra_Code));
            //myEnc.setProblemDescription(sNote);
            myEM.setFrom("server@nextimagemedical.com");
            myEM.setSubject("Encounter Escalated: SP: " + myEnc.getScanPass() + "[" + myCA.getPatientFirstName() + "]");
            myEM.setBody("ScanPass [" + myEnc.getScanPass() + "] has been escalated.  Please review the note below as well as any appropriate CommTracks on the system\n\n---\nFrom: " + working_bltNIM3_CommTrack.getMessageName() + " @ " + working_bltNIM3_CommTrack.getMessageCompany() + "\nNote: " + theMessage + "\n" + NIMUtils.getAppointmentDetails(myAppt.getAppointmentID(), false) + "\n---");
            myEM.isSendMail();
            myVal = "Encounter Escalated to Manager and Email sent to Scheduling Managers";
        }
        else if (sFlag.equalsIgnoreCase("problem_reactive"))
        {
            working_bltNIM3_CommTrack.setMessageText("Problem: Change to Active\n\n"+theMessage);
            myEnc.setEncounterStatusID(new Integer(1));  //Active
            myEnc.commitData();
            myVal = "Encounter moved to Status = Active";
        }
        else if (sFlag.equalsIgnoreCase("problem_close"))
        {
            working_bltNIM3_CommTrack.setMessageText("Problem: Change to Closed\n\n"+theMessage);
            myEnc.setEncounterStatusID(new Integer(6));  //Active
            myEnc.commitData();
            myVal = "Encounter moved to Status = Closed";
        }
        else if (sFlag.equalsIgnoreCase("problem_holdbill"))
        {
            working_bltNIM3_CommTrack.setMessageText("Problem: Change to Flag: Hold Bill\n\n"+theMessage);
            myEnc.setEncounterStatusID(new Integer(7));  //Active
            myEnc.commitData();
            myVal = "Encounter moved to Status = Flag: Hold Bill";
        }
        else if (sFlag.equalsIgnoreCase("problem_removecourtesy"))
        {
            working_bltNIM3_CommTrack.setMessageText("Problem: (NETDEV) Remove Courtesy\n\n"+theMessage);
            myEnc.setisCourtesy(0);
            myEnc.commitData();
            myVal = "Encounter (NETDEV) remove courtesy.";
        }
        else if (sFlag.equalsIgnoreCase("problem_tier2"))
        {
            working_bltNIM3_CommTrack.setMessageText("Problem: Change to Tier 2\n\nPrior Structure ["+myEnc.getPricing_Structure()+"]\n\n"+theMessage);
            myEnc.setPricing_Structure(2);  //Tier 2
            myEnc.commitData();
            myVal = "Encounter Changed to Tier 2";
        }
        else if (sFlag.equalsIgnoreCase("problem_tier3"))
        {
            working_bltNIM3_CommTrack.setMessageText("Problem: Change to Tier 3\n\nPrior Structure ["+myEnc.getPricing_Structure()+"]\n\n"+theMessage);
            myEnc.setPricing_Structure(3);  //Tier 3
            myEnc.commitData();
            myVal = "Encounter Changed to Tier 3";
        }
        else if (sFlag.equalsIgnoreCase("problem_billinginprogress"))
        {
            working_bltNIM3_CommTrack.setMessageText("Problem: Change to Billing: In Progress\n\n"+theMessage);
            myEnc.setEncounterStatusID(new Integer(9));  //Active
            myEnc.commitData();
            myVal = "Encounter moved to Status = Billing: In Progress";
        }
        else if (sFlag.equalsIgnoreCase("problem_courtesy"))
        {
            working_bltNIM3_CommTrack.setMessageText("Problem: Courtesy Schedule\n\n"+theMessage);
            myEnc.setisCourtesy(1);
            //myEnc.setEncounterStatusID(6);
            //myEnc.setEncounterStatusID(new Integer(12));  //escalate to manager
            myEnc.commitData();
            emailType_V3 myEM = new emailType_V3();
            myEM.setTo("scheduling.managers@nextimagemedical.com");
            myEM.setFrom("server@nextimagemedical.com");
            myEM.setSubject("Encounter Courtesy Schedule: SP: " + myEnc.getScanPass() + "[" + myCA.getPatientFirstName() + "]");
            myEM.setBody("ScanPass [" + myEnc.getScanPass() + "].  Please review the note below as well as any appropriate CommTracks on the system\n\n---\nFrom: " + working_bltNIM3_CommTrack.getMessageName() + " @ " + working_bltNIM3_CommTrack.getMessageCompany() + "\nNote: " + theMessage + "\n" + NIMUtils.getAppointmentDetails(myAppt.getAppointmentID(), false) + "\n---");
            myEM.isSendMail();
            //myVal = "Encounter Marked as Courtesy and has been Closed.  Email sent to Scheduling Managers";
            myVal = "Encounter Marked as Courtesy.  Email sent to Scheduling Managers";
        }
        else if (sFlag.equalsIgnoreCase("problem_void"))
        {
            working_bltNIM3_CommTrack.setMessageText("Problem: Void\n\n"+theMessage);
            myEnc.setEncounterStatusID(new Integer(5));  //void
            myEnc.setVoidLeakReasonID(iExtra_Code);
            myEnc.setVoid_UserID(iCurrentUserID);
            myEnc.commitData();
            emailType_V3 myEM = new emailType_V3();
            myEM.setTo("md.sales@nextimagemedical.com");
            myEM.setFrom("server@nextimagemedical.com");
            String myVoidReason = new bltVoidLeakReasonLI(myEnc.getVoidLeakReasonID()).getVoidLeakReasonLong();
            myEM.setSubject("Encounter Voided ["+myVoidReason+"]: SP: " + myEnc.getScanPass() + "[" + myCA.getPatientFirstName() + "]");
            myEM.setBody("ScanPass [" + myEnc.getScanPass() + "].  Please review the note below as well as any appropriate CommTracks on the system\n\n---\nFrom: " + working_bltNIM3_CommTrack.getMessageName() + " @ " + working_bltNIM3_CommTrack.getMessageCompany() + "\nNote: " + theMessage + "\n" + NIMUtils.getAppointmentDetails(myAppt.getAppointmentID(), false) + "\n\nVoid Reason: "+myVoidReason+"\n\n---");
            myEM.isSendMail();
            myVal = "Encounter Voided";
        }
        else
        {
            myVal = ("UnRecognized Problem: " + sFlag);
        }
        working_bltNIM3_CommTrack.commitData();
    }
    catch (Exception eee)
    {
        DebugLogger.printLine("NIMUtils:processEncounterProblem:[" + eee + "]");
    }

    return myVal;
}


public static emailType_V3 getAlertEmail(Integer iEncounterID, String sFlag)
{
    NIM3_EncounterObject2 myEO2 = new NIM3_EncounterObject2(iEncounterID,"loading alert email");
    return getAlertEmail(myEO2,sFlag,"",false);
}



public static emailType_V3 getAlertEmail( NIM3_EncounterObject2 myEO2, String sFlag, String appendText, boolean custom)
{
    emailType_V3 myVal = new emailType_V3();
    try
    {
        //NIM3_EncounterObject2 myEO2 = new NIM3_EncounterObject2(iEncounterID,"loading alert email");

        java.text.SimpleDateFormat displayDateSDF = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);
        java.text.SimpleDateFormat displayDateTimeSDF = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF2Full);
        boolean isFax = false;
        if (sFlag.indexOf("fax_")==0)
        {
            isFax = true;
        }
        if (sFlag.indexOf("_all")>0)
        {
            myVal.setTo(myEO2.getCase_Adjuster().getContactEmail());
            if (myEO2.getNIM3_CaseAccount().getNurseCaseManagerID()>0&&NIMUtils.isValidEmail(myEO2.getCase_NurseCaseManager().getContactEmail()))
            {
                myVal.cc.add(myEO2.getCase_NurseCaseManager().getContactEmail());
            }
            if (myEO2.getNIM3_CaseAccount().getCaseAdministratorID()>0&&NIMUtils.isValidEmail(myEO2.getCase_Admin().getContactEmail()))
            {
                myVal.cc.add(myEO2.getCase_Admin().getContactEmail());
            }
            if (myEO2.getNIM3_CaseAccount().getCaseAdministrator2ID()>0&&NIMUtils.isValidEmail(myEO2.getCase_Admin2().getContactEmail()))
            {
                myVal.cc.add(myEO2.getCase_Admin2().getContactEmail());
            }
            if (myEO2.getNIM3_CaseAccount().getCaseAdministrator3ID()>0&&NIMUtils.isValidEmail(myEO2.getCase_Admin3().getContactEmail()))
            {
                myVal.cc.add(myEO2.getCase_Admin3().getContactEmail());
            }
            if (myEO2.getNIM3_CaseAccount().getCaseAdministrator4ID()>0&&NIMUtils.isValidEmail(myEO2.getCase_Admin4().getContactEmail()))
            {
                myVal.cc.add(myEO2.getCase_Admin4().getContactEmail());
            }
        }
        else if (sFlag.indexOf("_adj")>0)
        {
            if (isFax)
            {
                myVal.setTo(NIMUtils.fax2EmailFax(myEO2.getCase_Adjuster().getContactFax()));
            }
            else
            {
                myVal.setTo(myEO2.getCase_Adjuster().getContactEmail());
            }
        }
        else if (sFlag.indexOf("_adm4")>0)
        {
            if (isFax)
            {
                myVal.setTo(NIMUtils.fax2EmailFax(myEO2.getCase_Admin4().getContactFax()));
            }
            else
            {
                myVal.setTo(myEO2.getCase_Admin4().getContactEmail());
            }
        }
        else if (sFlag.indexOf("_adm3")>0)
        {
            if (isFax)
            {
                myVal.setTo(NIMUtils.fax2EmailFax(myEO2.getCase_Admin3().getContactFax()));
            }
            else
            {
                myVal.setTo(myEO2.getCase_Admin3().getContactEmail());
            }
        }
        else if (sFlag.indexOf("_adm2")>0)
        {
            if (isFax)
            {
                myVal.setTo(NIMUtils.fax2EmailFax(myEO2.getCase_Admin2().getContactFax()));
            }
            else
            {
                myVal.setTo(myEO2.getCase_Admin2().getContactEmail());
            }
        }
        else if (sFlag.indexOf("_adm")>0)
        {
            if (isFax)
            {
                myVal.setTo(NIMUtils.fax2EmailFax(myEO2.getCase_Admin().getContactFax()));
            }
            else
            {
                myVal.setTo(myEO2.getCase_Admin().getContactEmail());
            }
        }
        else if (sFlag.indexOf("_ncm")>0)
        {
            if (isFax)
            {
                myVal.setTo(NIMUtils.fax2EmailFax(myEO2.getCase_NurseCaseManager().getContactFax()));
            }
            else
            {
                myVal.setTo(myEO2.getCase_NurseCaseManager().getContactEmail());
            }
        }
        else if (sFlag.indexOf("_refdr")>0)
        {
            if (isFax)
            {
                myVal.setTo(NIMUtils.fax2EmailFax(myEO2.getReferral_ReferringDoctor().getContactFax()));
            }
            else
            {
                myVal.setTo(myEO2.getReferral_ReferringDoctor().getContactEmail());
            }
            //remove to go live
            if (myEO2.isNextImageDirectCase()){
                //myVal.setTo("contact@nextimagedirect.com");
                myVal.setTo("contact@nextimagedirect.com");
//                myVal.setTo("scott@nextimagemedical.com");

            }

        }
        else if (sFlag.indexOf("_ic")>0)
        {
            if (isFax)
            {
                myVal.setTo(NIMUtils.fax2EmailFax(myEO2.getAppointment_PracticeMaster().getOfficeFaxNo()));
            }
            else
            {
                myVal.setTo("giovanni.hernandez@nextimagemedical.com");
            }
            //remove to go live
            if (myEO2.isNextImageDirectCase()){
                myVal.setTo(NIMUtils.fax2EmailFax("888-371-3302"));
            }
        } else if (sFlag.indexOf("_pt")>0)  {
            if (NIMUtils.isValidEmail(myEO2.getNIM3_CaseAccount().getPatientEmail())){
                myVal.setTo(myEO2.getNIM3_CaseAccount().getPatientEmail());
                //remove to go live
               // myVal.setTo("contact@nextimagedirect.com");
            } else {
                DebugLogger.e("NIMUtils:GetAlertEmail", "Invalid Patient Email ["+myEO2.getNIM3_CaseAccount().getPatientEmail()+"]");
                myVal.setTo("contact@nextimagedirect.com");
                myVal.cc.add("giovanni.hernandez@nextimagemedical.com");
            }

        } else if (sFlag.indexOf("_sr_vstar")>0)  {
            if (isFax)
            {
                myVal.setTo(NIMUtils.fax2EmailFax(myEO2.getAppointment_PracticeMaster().getOfficeFaxNo()));
            }
            else
            {
                myVal.setTo("giovanni.hernandez@nextimagemedical.com");
                //fill in with Vikrams
            }
        }

        if (sFlag.equalsIgnoreCase("email_rc_adj")||sFlag.equalsIgnoreCase("email_rc_adm")||sFlag.equalsIgnoreCase("email_rc_adm2")||sFlag.equalsIgnoreCase("email_rc_adm3")||sFlag.equalsIgnoreCase("email_rc_adm4")||sFlag.equalsIgnoreCase("email_rc_ncm")||sFlag.equalsIgnoreCase("fax_rc_adj")||sFlag.equalsIgnoreCase("fax_rc_adm")||sFlag.equalsIgnoreCase("fax_rc_adm2")||sFlag.equalsIgnoreCase("fax_rc_adm3")||sFlag.equalsIgnoreCase("fax_rc_adm4")||sFlag.equalsIgnoreCase("fax_rc_ncm")||sFlag.equalsIgnoreCase("emaila_rc_all"))
        {
        	java.text.SimpleDateFormat df = new java.text.SimpleDateFormat("MM-dd-yyyy");
            String theBody = "NextImage Medical/Workwell has received the Workers Comp referral for this injured worker:";
            theBody += "\n\n***Please Contact Us if this Referral is not authorized***\n";
            theBody += "\n\nPatient:\t\t\t" + myEO2.getNIM3_CaseAccount().getPatientLastName() + ", "  + myEO2.getNIM3_CaseAccount().getPatientFirstName() ;
            theBody += "\nDOB:\t\t\t"+df.format(myEO2.getNIM3_CaseAccount().getPatientDOB());
            theBody += "\nAdjuster:\t\t" + myEO2.getCase_Adjuster().getContactFirstName() + " " + myEO2.getCase_Adjuster().getContactLastName() + "   [" + myEO2.getNIM3_PayerMaster().getPayerName() + "]\n";
            theBody += "Claim Number:\t\t" +  myEO2.getNIM3_CaseAccount().getCaseClaimNumber() + "\n";
            theBody += "\nReferring Doctor:\t" + myEO2.getReferral_ReferringDoctor().getContactFirstName() + " " + myEO2.getReferral_ReferringDoctor().getContactLastName()+"\n";
            bltNIM3_Service        working_bltNIM3_Service;
            ListElement         leCurrentElement_Service;            
            java.util.Enumeration eList_Service = myEO2.getNIM3_Service_List().elements();   
                        
            while (eList_Service.hasMoreElements()) {
                leCurrentElement_Service    = (ListElement) eList_Service.nextElement();
                working_bltNIM3_Service  = (bltNIM3_Service) leCurrentElement_Service.getObject();
                if ( working_bltNIM3_Service.getServiceStatusID()==1) {
                    theBody += "Test Description:\t" +  working_bltNIM3_Service.getCPTBodyPart() + "\n";
                }
            }
            theBody += "\nThe following users have have been sent notifications:";
            try{
            	if(myEO2.getCase_Adjuster().getUserID()>0){
            		theBody+="\n"+ myEO2.getCase_Adjuster().getContactFirstName()+" "+myEO2.getCase_Adjuster().getContactLastName();
            	}else{
            		theBody+="";
            	}
            }catch(Exception e){}
            try{
            	if(myEO2.getCase_NurseCaseManager().getUserID()>0){
                	theBody += "\n" + myEO2.getCase_NurseCaseManager().getContactFirstName() + " " + myEO2.getCase_NurseCaseManager().getContactLastName();
                }else if(myEO2.getCase_NurseCaseManager().getUserID()<=0){
                	theBody+="";
                }
            }catch(Exception e){}
            try{
            	if(myEO2.getCase_Admin().getUserID()>0){
            		theBody += "\n" + myEO2.getCase_Admin().getContactFirstName() + " " + myEO2.getCase_Admin().getContactLastName();
            	}else{
            		theBody += "";
            	}
            }catch(Exception e){}
            try{
            	if(myEO2.getCase_Admin2().getUserID()>0){
            		theBody += "\n" + myEO2.getCase_Admin2().getContactFirstName() + " " + myEO2.getCase_Admin2().getContactLastName();
            	}else{
            		theBody += "";
            	}
            }catch(Exception e){}
            try{
            	if(myEO2.getCase_Admin3().getUserID()>0){
            		theBody += "\n" + myEO2.getCase_Admin3().getContactFirstName() + " " + myEO2.getCase_Admin3().getContactLastName();
            	}else{
            		theBody += "";
            	}
            }catch(Exception e){}
            try{
            	if(myEO2.getCase_Admin4().getUserID()>0){
            		theBody += "\n" + myEO2.getCase_Admin4().getContactFirstName() + " " + myEO2.getCase_Admin4().getContactLastName();
            	}else{
            		theBody += "";
            	}
            }catch(Exception e){}
            theBody += "\n\n**Did you know? We now offer Workwell Physical Therapy Services**";

            if (custom) {
            	theBody = "\n"+appendText;            	
            } else {                
            	theBody += "\n"+appendText;
            }
            
        	theBody += NIMUtils.getEmailSignature(myEO2.getNIM3_CaseAccount().getAssignedToID());            
            myVal.setBody(theBody);
            myVal.setSubject("NextImage Medical Update - Order Received - Case#" + myEO2.getNIM3_CaseAccount().getCaseClaimNumber());
        }
        else if (sFlag.equalsIgnoreCase("email_rc_pt"))
        {
        	java.text.SimpleDateFormat df = new java.text.SimpleDateFormat("MM-dd-yyyy");
            String theBody = "<strong>Thank you for choosing NextImage Direct for your imaging needs.</strong>" + "<br />\n";
            theBody += "<br />" + "<br />ScanPass: " + myEO2.getNIM3_Encounter().getScanPass();
            theBody += "<br>Date: " + PLCUtils.getNowDate(false) + "<br />\n";
            theBody += "<br />" + "<br />We are in the process of notifying the imaging center of your reservation. " + 
            		"<br>You should receive a call from them within 2 business days to set up your specific appointment. " + 
            		"<br />" + "<br />To expedite your order, please fax your prescription (or have your doctor's office fax it) to: " + myEO2.getAppointment_PracticeMaster().getOfficeFaxNo() + "<br />\n"
            + "<br />" + "<br /><strong style=\"color:red\">In order to expedite appointment contact imaging center at:</strong>" //added code gio
            + "<br />" + "<br />\n" +  myEO2.getAppointment_PracticeMaster().getPracticeName() 
            + "<br>" + myEO2.getAppointment_PracticeMaster().getOfficeAddress1() + " " + myEO2.getAppointment_PracticeMaster().getOfficeAddress2()
            + "<br>" + myEO2.getAppointment_PracticeMaster().getOfficeZIP() + "<br />"
            + "<br />" + "<br />\n<strong style=\"color:red\">Imaging Center Phone: " + myEO2.getAppointment_PracticeMaster().getOfficePhone() + "</strong>";//added code gio
            if (custom) {
            	theBody = "<br>"+appendText;              	
            } else {                
            	theBody += "<br>"+appendText;
            }
            myVal.setSubject("Thank you for your Diagnostic Imaging Reservation!  ["+myEO2.getNIM3_Encounter().getScanPass()+"] ");
        	//theBody += NIMUtils.getEmailSignature(myEO2.getNIM3_CaseAccount().getAssignedToID());
            theBody += "<br>Jenny Watkins"
            		+ "<br>Phone: 888-693-8867"
            		+ "<br>Fax:   888-596-8680<br>";
            myVal.setBodyType(emailType_V3.HTML_TYPE);
            myVal.setBody(theBody);

        }
        
        
        else if (sFlag.equalsIgnoreCase("email_rc_refdr")||sFlag.equalsIgnoreCase("fax_rc_refdr"))
        {
        	java.text.SimpleDateFormat df = new java.text.SimpleDateFormat("MM-dd-yyyy");
        	String theBody = "NextImage Medical has received the Workers Comp referral for this injured worker:";
            
        	bltNIM3_PayerMaster myPM = new bltNIM3_PayerMaster(new bltUserAccount(myEO2.getNIM3_CaseAccount().getAdjusterID()).getPayerID());
            boolean isNID = myEO2.isNextImageDirectCase();

        	if(isNID){
        		theBody ="NextImage Direct has received a referral for your patient for their diagnostic imaging scan.";
        	}
        	
        	theBody += "\n\n***          In order for us to schedule your patient, please fax us the RX/Order if you have not done so already.      ***" ;

        	if(isNID){
        		theBody +=   "\n***          Fax to: " + myEO2.getAppointment_PracticeMaster().getOfficeFaxNo() + "                       ***" ;
        	}
        	else {theBody +=   "\n***                     Fax to: 888-596-8680                         ***" ;}
            
            theBody += "\n\nPatient:\t" + myEO2.getNIM3_CaseAccount().getPatientLastName() + ", "  + myEO2.getNIM3_CaseAccount().getPatientFirstName() ;
            theBody += "\nDOB:\t\t"+df.format(myEO2.getNIM3_CaseAccount().getPatientDOB());
            if (isNID){
                theBody += "\nPhone:\t\t"+myEO2.getNIM3_CaseAccount().getPatientHomePhone();
                theBody += "\nScanPass#\t"+myEO2.getNIM3_Encounter().getScanPass() + "\n";
            } else {
                theBody += "\nAdjuster:\t\t" + myEO2.getCase_Adjuster().getContactFirstName() + " " + myEO2.getCase_Adjuster().getContactLastName() + "   [" + myEO2.getNIM3_PayerMaster().getPayerName() + "]\n";
                theBody += "Claim Number:\t\t" +  myEO2.getNIM3_CaseAccount().getCaseClaimNumber() + "\n";
                //this was removed at the request of Bob Olsen & Ops Meeting
                theBody += "\nReferring Doctor:\t" + myEO2.getReferral_ReferringDoctor().getContactFirstName() + " " + myEO2.getReferral_ReferringDoctor().getContactLastName()+"\n";

            }
            bltNIM3_Service        working_bltNIM3_Service;
            ListElement         leCurrentElement_Service;            
            java.util.Enumeration eList_Service = myEO2.getNIM3_Service_List().elements();
            theBody += "\nAuthorized Services:\n";

            while (eList_Service.hasMoreElements()) {
                leCurrentElement_Service    = (ListElement) eList_Service.nextElement();
                working_bltNIM3_Service  = (bltNIM3_Service) leCurrentElement_Service.getObject();
                if ( working_bltNIM3_Service.getServiceStatusID()==1) {
                    theBody += "\tDescription:\t" +  working_bltNIM3_Service.getCPTBodyPart() + "\n";
                    theBody += "\tCPT:\t" +  working_bltNIM3_Service.getCPT() + "\n";
                }
            }
            if (custom) {
            	theBody = "\n"+appendText;              	
            } else {                
            	theBody += "\n"+appendText;
            }

        	theBody += NIMUtils.getEmailSignature(myEO2.getNIM3_CaseAccount().getAssignedToID());
            myVal.setBody(theBody);
            
            if(isNID){
            	myVal.setSubject("NextImage Direct Update - Order Received - ScanPass#" + myEO2.getNIM3_Encounter().getScanPass());
        	}
            else{myVal.setSubject("NextImage Medical Update - Order Received - Case#" + myEO2.getNIM3_CaseAccount().getCaseClaimNumber());}
        }
        
        
        
        
        
        else if (sFlag.equalsIgnoreCase("email_sp_pt"))
        {
        	java.util.Date dbAppointment = myEO2.getNIM3_Appointment().getAppointmentTime();
        	String sAppointmentTime = displayDateTimeSDF.format((dbAppointment));
        	java.text.SimpleDateFormat displayDateDayWeek = new java.text.SimpleDateFormat(PLCUtils.String_displayDateDayWeek);
        	String sAppointmentTimeDayWeek = displayDateDayWeek.format((dbAppointment));
        	
        	java.text.SimpleDateFormat df = new java.text.SimpleDateFormat("MM-dd-yyyy");
            String theBody = "<strong>Thank you for choosing NextImage Direct for your imaging needs, the following are your appointment details.</strong><br>";
            theBody += "<br>ScanPass: " + myEO2.getNIM3_Encounter().getScanPass();
            //theBody += "<br>Date: " + PLCUtils.getNowDate(false);
            
            theBody += "<br><br>\n<strong style=\"color:red\">Facility and Appointment Date: ";
            theBody += "<br><br>Contact Imaging Center at " + myEO2.getAppointment_PracticeMaster().getOfficePhone() + "</strong>";
//            				"<br><br>\n" + sAppointmentTimeDayWeek + " - " + sAppointmentTime + 
            theBody += 		"<br>" + myEO2.getAppointment_PracticeMaster().getPracticeName() +
            				"<br>" + myEO2.getAppointment_PracticeMaster().getOfficeAddress1() + " " + myEO2.getAppointment_PracticeMaster().getOfficeAddress2() +
            				"<br>" + myEO2.getAppointment_PracticeMaster().getOfficeCity() +
            				"<br>" + myEO2.getAppointment_PracticeMaster().getOfficeZIP();
//            				"<br><br>" + myEO2.getAppointment_PracticeMaster().getOfficePhone();
            theBody += "<br><br>\nReferring Doctor:     " + myEO2.getReferral_ReferringDoctor().getContactFirstName() + " " + myEO2.getReferral_ReferringDoctor().getContactLastName()+"<br><br>";
            
            bltNIM3_Service  service;
    		ListElement	current_element;
    		java.util.Enumeration eList_Service = myEO2.getNIM3_Service_List().elements();
    		int cnt55=1;
    		while (eList_Service.hasMoreElements()){
    			current_element = (ListElement) eList_Service.nextElement();
    			service  = (bltNIM3_Service) current_element.getObject();
    			if (service.getServiceStatusID().intValue()!=5){
    				theBody += "<span style=\"margin-left:10px\">"+cnt55+") "+service.getCPTBodyPart()+"<br></span><br>";
    				theBody += "Diagnosis: "+service.getdCPT1()+" "+service.getdCPT2()+" "+service.getdCPT3()+" "+service.getdCPT4()+"<br>";
    				cnt55++;
    			}
    		}
    		theBody += "<br>\nThank you for your payment in the amount of ";
    		
    		eList_Service = myEO2.getNIM3_Service_List().elements();
    	
    		while (eList_Service.hasMoreElements()){
    			current_element = (ListElement) eList_Service.nextElement();
    			service  = (bltNIM3_Service) current_element.getObject();
    			if (service.getServiceStatusID().intValue()!=5){
    				theBody += " $"+(Math.round(service.getReceivedCheck1Amount()+service.getReceivedCheck2Amount()+service.getReceivedCheck3Amount()));
    			}
    		}
    		theBody += " paid directly to Next Image of CA.<br><br>";
    		theBody += "<br />\n<strong style=\"text-decoration:underline;\">Payment Confirmation:</strong><br>";
    		
    		bltCCardTransaction ccardtrans = new bltCCardTransaction(myEO2.getNIM3_Encounter().getCCTransactionID());
    		//bltCCardTransaction ccardtrans = new bltCCardTransaction(2);
    		
    		theBody += "Bill To:"+"<br>";
    		theBody += "<span style=\"text-transform:capitalize\">"+ccardtrans.getCName().toLowerCase()+"</span><br>";
    		theBody += "<span style=\"text-transform:capitalize\">"+ccardtrans.getCAddress().toLowerCase()+"</span><br>";
    		theBody += "<span style=\"text-transform:capitalize\">"+ccardtrans.getCCity().toLowerCase()+", "+ccardtrans.getCState()+" "+ccardtrans.getCZip()+"</span><br>";
    		theBody += "<br>";
    		theBody += "Account: *"+ccardtrans.getCCNumber()+"<br>";
    		theBody += "Transaction Type: Sale"+"<br>";
    		theBody += "Order: "+ccardtrans.getOrderNumber()+"<br>";
    		theBody += "Auth: "+ccardtrans.getAuthNumber()+"<br><br>";
    		theBody += "Amount: $"+ccardtrans.getAmountCharged()+".00<br>";
    		theBody += "Tax: $0.00<br>";
    		theBody += "Total: $"+ccardtrans.getAmountCharged()+".00<br><br>";
    		theBody += "<br>\nCardmember acknowledges receipt of goods and/or services in the amount of the total shown hereon and agrees to perform <br>the obligations set forth by the cardmember's agreement with the issuer.<br><br>";
    		
    		theBody += "<br>\n<span style=\"font-size:14px;\">Jenny Watkins<br>Phone: 888-693-8867 Fax: 888-371-3302<br>Email: orders@nextimagedirect.com</span>";
    		
    		//myETSend.setSubject("Thank you for your Diagnostic Imaging Reservation!  ["+myEO2.getNIM3_Encounter().getScanPass()+"] ");
    		myEO2.getNIM3_Encounter().setSentTo_SP_Pt(PLCUtils.getNowDate(true));
    		myEO2.getNIM3_Encounter().commitData();
    		
            
            if (custom) {
            	theBody = "<br>"+appendText;              	
            } else {                
            	theBody += "<br>"+appendText;
            }
            myVal.setSubject("Thank you for your Diagnostic Imaging Reservation!  ["+myEO2.getNIM3_Encounter().getScanPass()+"] ");
        	myVal.setBody(theBody);
        	myVal.setBodyType(emailType_V3.HTML_TYPE);
    		myVal.setFrom("support@nextimagemedical.com");
    		myVal.setSubject("NextImage Direct receipt for your radiology service/s and your appointment details");
            myVal.setBody(theBody);

        }
        
        
        
        
        
        
        
        
        
        
        
        
        else if (sFlag.equalsIgnoreCase("email_sp_refdr")||sFlag.equalsIgnoreCase("email_sp_adj")||sFlag.equalsIgnoreCase("email_sp_adm")||sFlag.equalsIgnoreCase("email_sp_adm2")||sFlag.equalsIgnoreCase("email_sp_adm3")||sFlag.equalsIgnoreCase("email_sp_adm4")||sFlag.equalsIgnoreCase("email_sp_ncm")||sFlag.equalsIgnoreCase("fax_sp_refdr")||sFlag.equalsIgnoreCase("fax_sp_adj")||sFlag.equalsIgnoreCase("fax_sp_adm")||sFlag.equalsIgnoreCase("fax_sp_adm2")||sFlag.equalsIgnoreCase("fax_sp_adm3")||sFlag.equalsIgnoreCase("fax_sp_adm4")||sFlag.equalsIgnoreCase("fax_sp_ncm")||sFlag.equalsIgnoreCase("emaila_sp_all"))
        {
            java.util.Date dbAppointment = myEO2.getNIM3_Appointment().getAppointmentTime();

            java.text.SimpleDateFormat displayDateDay = new java.text.SimpleDateFormat(PLCUtils.String_displayDateDay);
            java.text.SimpleDateFormat displayDateDayWeek = new java.text.SimpleDateFormat(PLCUtils.String_displayDateDayWeek);
            java.text.SimpleDateFormat displayDateMonth = new java.text.SimpleDateFormat(PLCUtils.String_displayDateMonth);
            java.text.SimpleDateFormat displayDateYear = new java.text.SimpleDateFormat(PLCUtils.String_displayDateYear);
            java.text.SimpleDateFormat displayDateHour = new java.text.SimpleDateFormat(PLCUtils.String_displayDateHour);
            String sAppointmentTime = displayDateTimeSDF.format((dbAppointment));
            String sAppointmentTimeDay = displayDateDay.format((dbAppointment));
            String sAppointmentTimeDayWeek = displayDateDayWeek.format((dbAppointment));
            String sAppointmentTimeMonth = displayDateMonth.format((dbAppointment));
            String sAppointmentTimeYear = displayDateYear.format((dbAppointment));
            String sAppointmentHour = displayDateHour.format((dbAppointment));
            String theBody = "NextImage Medical has scheduled the injured worker for the procedure requested:";
            
            bltNIM3_PayerMaster myPM = new bltNIM3_PayerMaster(new bltUserAccount(myEO2.getNIM3_CaseAccount().getAdjusterID()).getPayerID());
        	
        	if(myPM.getParentPayerID()==951){
        		theBody ="NextImage Direct has scheduled your patient for the procedure requested:";
        	}
            
            theBody += "\n\nPatient:\t\t" + myEO2.getNIM3_CaseAccount().getPatientLastName() + ", "  + myEO2.getNIM3_CaseAccount().getPatientFirstName() + "\tGender: " + myEO2.getNIM3_CaseAccount().getPatientGender();
            theBody += "\nAddress:\t\t" + myEO2.getNIM3_CaseAccount().getPatientAddress1() + " "  + myEO2.getNIM3_CaseAccount().getPatientAddress2() + "";
            theBody += "\n\t\t\t" + myEO2.getNIM3_CaseAccount().getPatientCity() + ", "  + new bltStateLI(myEO2.getNIM3_CaseAccount().getPatientStateID()).getShortState() +" " +  myEO2.getNIM3_CaseAccount().getPatientZIP() + "";
            theBody += "\nAdjuster:\t\t" + myEO2.getCase_Adjuster().getContactFirstName() + " " + myEO2.getCase_Adjuster().getContactLastName() + "   [" + myEO2.getNIM3_PayerMaster().getPayerName() + "]\n";
            if(myPM.getParentPayerID()!=951){
            	theBody += "Claim Number:\t\t" +  myEO2.getNIM3_CaseAccount().getCaseClaimNumber();
        	}          

            theBody += "\nDate of Injury:\t\t" + displayDateSDF.format(myEO2.getNIM3_CaseAccount().getDateOfInjury()) + "\n";
            
            theBody += "Referring Doctor:\t" + myEO2.getReferral_ReferringDoctor().getContactFirstName() + " " + myEO2.getReferral_ReferringDoctor().getContactLastName() + "\n";
            if ( !myEO2.getReferral_ReferringDoctor().getContactPhone().equalsIgnoreCase(""))
            {
    //            theBody += " Phone:" + myEO2.getReferral_ReferringDoctor().getContactPhone();
            }
            if ( !myEO2.getReferral_ReferringDoctor().getContactFax().equalsIgnoreCase(""))
            {
    //            theBody += " Fax:" + myEO2.getReferral_ReferringDoctor().getContactFax();
            }
            if (myEO2.getNIM3_Encounter().getHasBeenRescheduled()==1)
            {
            	theBody += "\n\n******************************";
                theBody +=   "\n* Has Been Rescheduled: Yes *";
                theBody +=   "\n******************************";
            }
            theBody += "\nAuthorized services:";
            bltNIM3_Service        working_bltNIM3_Service;
            ListElement         leCurrentElement_Service;
            java.util.Enumeration eList_Service = myEO2.getNIM3_Service_List().elements();
            int cnt55=0;
            while (eList_Service.hasMoreElements())
            {
                cnt55++;
                leCurrentElement_Service    = (ListElement) eList_Service.nextElement();
                working_bltNIM3_Service  = (bltNIM3_Service) leCurrentElement_Service.getObject();
                if ( working_bltNIM3_Service.getServiceStatusID()==1)
                {
                    theBody += "\n";
                    if ( !working_bltNIM3_Service.getCPT().equalsIgnoreCase(""))
                    {
                        theBody += "\t" + NIMUtils.getCPTText(working_bltNIM3_Service.getCPT()) + "";
                    }
                    if ( !working_bltNIM3_Service.getCPTBodyPart().equalsIgnoreCase(""))
                    {
                        theBody += "\t" + working_bltNIM3_Service.getCPTBodyPart();
                    }
                    if ( !working_bltNIM3_Service.getCPTText().equalsIgnoreCase(""))
                    {
                        theBody += "\t|Diag/Rule-Out:\t" + working_bltNIM3_Service.getCPTText();
                    }
                }

            }
            theBody += "\n\nProcedure Notes: " + myEO2.getNIM3_Encounter().getComments();
            if (myEO2.getNIM3_Encounter().getRequiresAgeInjury()==1)
            {
                theBody += "\nAGE INJURY:\t\tYES";
            }
            theBody += "\nAppointment Time:\t" + sAppointmentTimeDayWeek + " - " + sAppointmentTime;
            theBody += "\nProvider/Location:\t" + myEO2.getAppointment_PracticeMaster().getPracticeName() + "";
            theBody += "\n\t\t\t" + myEO2.getAppointment_PracticeMaster().getOfficeAddress1() ;
            theBody += "\n\t\t\t" + myEO2.getAppointment_PracticeMaster().getOfficeAddress2() ;
            theBody += "\n\t\t\t" + myEO2.getAppointment_PracticeMaster().getOfficeCity() + ", " + new bltStateLI(myEO2.getAppointment_PracticeMaster().getOfficeStateID()).getShortState() + " " + myEO2.getAppointment_PracticeMaster().getOfficeZIP();
            theBody += "\n\t\t\tPhone: " + myEO2.getAppointment_PracticeMaster().getOfficePhone() + "\tFax: " + myEO2.getAppointment_PracticeMaster().getOfficeFaxNo();
            theBody += "\n";
            
            if (custom) {
            	theBody = "\n"+appendText;              	
            } else {                
            	theBody += "\n"+appendText;
            }            
            theBody += "\nThe following users have have been sent notifications:";
            try{
            	if(myEO2.getCase_Adjuster().getUserID()>0){
            		theBody+="\n"+ myEO2.getCase_Adjuster().getContactFirstName()+" "+myEO2.getCase_Adjuster().getContactLastName();
            	}else{
            		theBody+="";
            	}
            }catch(Exception e){}
            try{
            	if(myEO2.getCase_NurseCaseManager().getUserID()>0){
                	theBody += "\n" + myEO2.getCase_NurseCaseManager().getContactFirstName() + " " + myEO2.getCase_NurseCaseManager().getContactLastName();
                }else if(myEO2.getCase_NurseCaseManager().getUserID()<=0){
                	theBody+="";
                }
            }catch(Exception e){}
            try{
            	if(myEO2.getCase_Admin().getUserID()>0){
            		theBody += "\n" + myEO2.getCase_Admin().getContactFirstName() + " " + myEO2.getCase_Admin().getContactLastName();
            	}else{
            		theBody += "";
            	}
            }catch(Exception e){}
            try{
            	if(myEO2.getCase_Admin2().getUserID()>0){
            		theBody += "\n" + myEO2.getCase_Admin2().getContactFirstName() + " " + myEO2.getCase_Admin2().getContactLastName();
            	}else{
            		theBody += "";
            	}
            }catch(Exception e){}
            try{
            	if(myEO2.getCase_Admin3().getUserID()>0){
            		theBody += "\n" + myEO2.getCase_Admin3().getContactFirstName() + " " + myEO2.getCase_Admin3().getContactLastName();
            	}else{
            		theBody += "";
            	}
            }catch(Exception e){}
            try{
            	if(myEO2.getCase_Admin4().getUserID()>0){
            		theBody += "\n" + myEO2.getCase_Admin4().getContactFirstName() + " " + myEO2.getCase_Admin4().getContactLastName();
            	}else{
            		theBody += "";
            	}
            }catch(Exception e){}
    //	theBody += "Upon completion, you will receive an email copy of the medical report.\n\nIf you have any questions, please call us at 888-318-5111.\n\nThank You\n\nNextImage Medical\n\nV2.01";
            theBody += NIMUtils.getEmailSignature(myEO2.getNIM3_CaseAccount().getAssignedToID());
            myVal.setBody(theBody);
            
            if(myPM.getParentPayerID()==951){
            	myVal.setSubject("NextImage Direct Update - Appointment Scheduled - Case#" + myEO2.getNIM3_CaseAccount().getCaseClaimNumber());
        	}
            else{myVal.setSubject("NextImage Medical Update - Appointment Scheduled - Case#" + myEO2.getNIM3_CaseAccount().getCaseClaimNumber());}
//            myVal.setFrom(ConfigurationInformation.FaxFromAddress);
            //myVal.setTo();   //to is not set until Alert send is called
        }
//TODO: start testing
        //else {
        else if (sFlag.equalsIgnoreCase("email_sp_ic") || sFlag.equalsIgnoreCase("fax_sp_ic")) {
                java.text.SimpleDateFormat displayDateDay = new java.text.SimpleDateFormat(PLCUtils.String_displayDateDay);
                java.text.SimpleDateFormat displayDateDayWeek = new java.text.SimpleDateFormat(PLCUtils.String_displayDateDayWeek);
                java.text.SimpleDateFormat displayDateMonth = new java.text.SimpleDateFormat(PLCUtils.String_displayDateMonth);
                java.text.SimpleDateFormat displayDateYear = new java.text.SimpleDateFormat(PLCUtils.String_displayDateYear);
                java.text.SimpleDateFormat displayDateHour = new java.text.SimpleDateFormat(PLCUtils.String_displayDateHour);

                Date dbAppointment = myEO2.getNIM3_Appointment().getAppointmentTime();
                String sAppointmentTime = displayDateTimeSDF.format((dbAppointment));
                String sAppointmentTimeDay = displayDateDay.format((dbAppointment));
                String sAppointmentTimeDayWeek = displayDateDayWeek.format((dbAppointment));
                String sAppointmentTimeMonth = displayDateMonth.format((dbAppointment));
                String sAppointmentTimeYear = displayDateYear.format((dbAppointment));
                String sAppointmentHour = displayDateHour.format((dbAppointment));
                boolean isNID = myEO2.isNextImageDirectCase();
                bltNIM3_Document myDoc = new bltNIM3_Document(myEO2.getNIM3_Referral().getRxFileID());
                if (isNID) {
                    if (myDoc.getUniqueID()!=NIMUtils.EncounterBypassRXID){
                        myVal.setVirtualAttachment(myDoc.getFileName());
                    }
                } else {
                    myVal.setVirtualAttachment(myDoc.getFileName());
                }
                //String theBody = "<font size=\\\"+4\\\">NextImage Medical has scheduled the injured worker for the procedure requested:";
                String theBody = "NextImage Medical has scheduled the injured worker for the procedure requested:";
                bltNIM3_PayerMaster myPM = new bltNIM3_PayerMaster(new bltUserAccount(myEO2.getNIM3_CaseAccount().getAdjusterID()).getPayerID());

                if (isNID) {
                    //theBody ="<font size=\\\"+4\\\">NextImage Direct has scheduled the patient for the procedure requested:";
                    theBody = "NextImage Direct has referred a patient to your facility for the procedure requested below.";
                    theBody += "\n\n______________________FOR SPEEDY 30 DAY PAYMENT GUARANTEE PLEASE FOLLOW THESE INSTRUCTIONS_____________________" +
                    "\n1. Please fax all medical reports to " + "888-371-3302 within 72 hours of services being rendered." +
                    "\n2. Discrepancies between the scheduled service and physician's script must be clarified before service is rendered. Any questions " +
                    "\n   please contact us at 888-693-8867. SERVICES RENDERED MUST MATCH THIS AUTHORIZATION." +
                    "\n3. Notify us of appointment time and date by fax at 888-371-3302 or please include appointment time and date on FINAL REPORT." +
                    "\n4. Fax HCFA to 888-371-3302.";
                }

                //theBody += "\n\n<u><strong>ScanPass Auth#:</strong></u>\t" + myEO2.getNIM3_Encounter().getScanPass();
                theBody += "\n\nScanPass Auth#:\t" + myEO2.getNIM3_Encounter().getScanPass();// gio removed 1 linebrk
                theBody += "\nPatient:\t" + myEO2.getNIM3_CaseAccount().getPatientLastName() + ", " + myEO2.getNIM3_CaseAccount().getPatientFirstName() + "\tGender: " + myEO2.getNIM3_CaseAccount().getPatientGender();//gio added 1 tab
                theBody += "\nAddress:\t" + myEO2.getNIM3_CaseAccount().getPatientAddress1() + " " + myEO2.getNIM3_CaseAccount().getPatientAddress2() + "";
                theBody += "\n\t\t" + myEO2.getNIM3_CaseAccount().getPatientCity() + ", " + new bltStateLI(myEO2.getNIM3_CaseAccount().getPatientStateID()).getShortState() + " " + myEO2.getNIM3_CaseAccount().getPatientZIP() + "";
                //theBody += "\nDOB:\t\t" + "<strong>"+PLCUtils.getDisplayDate(myEO2.getNIM3_CaseAccount().getPatientDOB(),false)+"</strong>";
                theBody += "\nDOB:\t\t" + PLCUtils.getDisplayDate(myEO2.getNIM3_CaseAccount().getPatientDOB(), false);
                if (!isNID) {
                    theBody += "\nDate of Injury:\t" + PLCUtils.getDisplayDate(myEO2.getNIM3_CaseAccount().getDateOfInjury(), false);
                }
                if (isNID){
                    theBody += "\nPhone:\t\t" + myEO2.getNIM3_CaseAccount().getPatientHomePhone() ;
                } else {
                    theBody += "\nHome Phone:\t" + myEO2.getNIM3_CaseAccount().getPatientHomePhone() + "\t\tCell:" + myEO2.getNIM3_CaseAccount().getPatientCellPhone();
                }
                theBody += "\nHeight:\t\t" + myEO2.getNIM3_CaseAccount().getPatientHeight();
                theBody += "\nWeight:\t\t" + myEO2.getNIM3_CaseAccount().getPatientWeight();

                if (!isNID) {
                    Vector myV = myEO2.getPatientPreScreen();
                    for (int iTemp = 0; iTemp < myV.size(); iTemp++) {
                        theBody += "\n" + myV.elementAt(iTemp);
                    }
                }

                if (isNID){
                    //because NID referrals are now automated, we can't rely on the doctor account to be correct so we must use the notes.
                    theBody += "\n\nReferring Physician:\t" + myEO2.getNIM3_Referral().getComments();
                } else {
                    theBody += "\nReferring Physician:\t" + myEO2.getReferral_ReferringDoctor().getContactFirstName() + " " + myEO2.getReferral_ReferringDoctor().getContactLastName();// gio rmvd 1 lnbrk
                    if (!myEO2.getReferral_ReferringDoctor().getContactPhone().equalsIgnoreCase("")) {
                        theBody += "\tPhone: " + myEO2.getReferral_ReferringDoctor().getContactPhone();
                    }
                    if (!myEO2.getReferral_ReferringDoctor().getContactFax().equalsIgnoreCase("")) {
                        theBody += "\tFax: " + myEO2.getReferral_ReferringDoctor().getContactFax();
                    }
                }
                //theBody += "\n\n<strong><u>Authorized services:</u></strong>";
                theBody += "\nAuthorized services:";// gio rmvd 1 lnbrk
                bltNIM3_Service working_bltNIM3_Service;
                ListElement leCurrentElement_Service;
                Enumeration eList_Service = myEO2.getNIM3_Service_List().elements();
                int cnt55 = 0;
                while (eList_Service.hasMoreElements()) {
                    cnt55++;
                    leCurrentElement_Service = (ListElement) eList_Service.nextElement();
                    working_bltNIM3_Service = (bltNIM3_Service) leCurrentElement_Service.getObject();
                    if (working_bltNIM3_Service.getServiceStatusID() == 1) {
                        theBody += "\n";
                        if (!working_bltNIM3_Service.getCPT().equalsIgnoreCase("")) {
                            if (isNID){
                                theBody += "\tCPT: " + working_bltNIM3_Service.getCPT() + "\n";
                            }
                            //                    theBody += "\t" + NIMUtils.getCPTText(working_bltNIM3_Service.getCPT()) + "";
                        }
                        if (!working_bltNIM3_Service.getCPTBodyPart().equalsIgnoreCase("")) {
                            theBody += "\t" + working_bltNIM3_Service.getCPTBodyPart();
                        }
                        if (!working_bltNIM3_Service.getCPTText().equalsIgnoreCase("")) {
                            theBody += "\t|Diag/Rule-Out:\t" + working_bltNIM3_Service.getCPTText();
                        }
                        // TECHNICAL COMPONENT ONLY

                        if (working_bltNIM3_Service.getCPTModifier().equalsIgnoreCase("TC")) {
                            theBody += "\n\n*************************************************************";
                            theBody += "\n* The ABOVE service is a TECHNICAL COMPONENT ONLY service.  *";
                            theBody += "\n*************************************************************";
                        }
                    }

                }

                //theBody += "\n\n<strong><u>Procedure Notes:</u></strong>\n " + myEO2.getNIM3_Encounter().getComments();
                if (isNID){
                    theBody += "\n\nImportant Notes:" +
                            "\n" +
                            "\n*** DO NOT TO BILL THE PATIENT!!! ***" +
                            "\n* Send bill to NextImage Medical and we will pay you promptly. SEE NOTE BELOW FOR SPEEDY PAYMENT GUARANTEE*" +
                            "\n*****" +
                            "\n" +
                            "\n* Call patient to schedule appointment *" +
                            "\n*****" +
                            "\n" +
                            "\n* Please fax all medical reports to " + myEO2.getReferral_ReferringDoctor().getContactFax().equalsIgnoreCase("") + "" +//myEO2.getNIM3_Referral().getReferenceFax()
                            "\n*****" +
                            "\n" +
                            "\n* Hand Carry CD: Yes *" +
                            "\n*****" +
                            "\n" +
                            "\n* To obtain Rx for this procedure please contact referring doctor" +
                            "\n* at "+ myEO2.getReferral_ReferringDoctor().getContactPhone()  + " (phone) or "+ myEO2.getReferral_ReferringDoctor().getContactFax() + " (fax)" +
                            "\n*****" +
                            "\n" +
                            "\nAppointment Time: Please call patient to schedule appointment" +
                            "\n" +
                            "\nSEND CLAIMS TO:" +
                            "\nNextImage Medical" +
                            "\nAttn: Medical Claims" +
                            "\n3390 Carmel Mountain Rd. Suite 150" +
                            "\nSan Diego, CA  92121" +
                            "\n" +
                            "\nIf you have any questions, please call us. Thank You," +
                            "\n" +
                            "\nJenny Watkins" +
                            "\nPhone: 888-693-8867" +
                            "\nFax  : 888-371-3302" +
                            "\n" ;
                } else {
                    theBody += "\n\nProcedure Notes:\n " + myEO2.getNIM3_Encounter().getComments();
                    theBody += "\n* Please verify all Procedure details with attached prescription";//gio rmvd lnbrk after prescription

                    if (myEO2.getNIM3_Encounter().getHasBeenRescheduled() == 1) {
                        theBody += "\n* Has Been Rescheduled: Yes *\n";
                    }
                    if (myEO2.getNIM3_Encounter().getisSTAT() == 1) {
                        theBody += "\n* STAT Order: Yes *\n";
                    }
                    if (myEO2.getNIM3_Encounter().getRequiresFilms() == 1) {
                        theBody += "\n* Hand Carry Films: Yes *\n";
                    }
                    if (myEO2.getNIM3_Encounter().getRequiresHandCarryCD() == 1) {
                        theBody += "\n* Hand Carry CD: Yes *\n";
                    }
                    if (myEO2.getNIM3_Encounter().getRequiresAgeInjury() == 1) {
                        theBody += "\n* Age Injury Comment: Yes *\n";
                    }
                    if (myEO2.getNIM3_Encounter().getRequiresFilms() == 2) {
                        theBody += "\n* Hand Carry Films: No *\n";
                    }
                    if (myEO2.getNIM3_Encounter().getRequiresAgeInjury() == 2) {
                        theBody += "\n* Age Injury Comment: No *\n";
                    }
                    if (myEO2.getNIM3_Encounter().getRequiresHandCarryCD() == 2) {
                        theBody += "\n* Hand Carry CD: No *\n";
                    }

                    if (myEO2.getNIM3_Encounter().getRequiresAgeInjury() == 1) {
                        theBody += "\n* AGE OF INJURY COMMENT REQURIED\n ***Note for Radiologist - PLEASE note if findings are ACUTE or CHRONIC at the end of the report as a separate line item";//gio rmvd lnbrk after item
                    }

                    theBody += "\nAppointment Time:\t" + sAppointmentTimeDayWeek + " - " + sAppointmentTime;
                    theBody += "\nProvider/Location:\t" + myEO2.getAppointment_PracticeMaster().getPracticeName() + "";
                    theBody += "\n\t\t\t" + myEO2.getAppointment_PracticeMaster().getOfficeAddress1();
                    theBody += "\n\t\t\t" + myEO2.getAppointment_PracticeMaster().getOfficeAddress2();
                    theBody += "\n\t\t\t" + myEO2.getAppointment_PracticeMaster().getOfficeCity() + ", " + new bltStateLI(myEO2.getAppointment_PracticeMaster().getOfficeStateID()).getShortState() + " " + myEO2.getAppointment_PracticeMaster().getOfficeZIP() + "\n";//gio rmvd lnbrk at end of zip


                    theBody += NIMUtils.getEmailSignature(myEO2.getNIM3_CaseAccount().getAssignedToID());
                    theBody += "\n-----------------------------------------------------\n";
                    if (myEO2.getNIM3_Encounter().getisCourtesy() == 1) {
                        NIM3_AddressObject myNAO = myEO2.getWhoShouldIBillTo();
                        theBody += "1.  To ensure prompt payment, please fax all medical reports to 1-888-596-8680 within 48 hours of services being rendered.\n" +
                                "2.  Discrepancies between the scheduled service and physician's script must be clarified before service is rendered.\n" +
                                "3.  Services rendered without prior authorization will be denied for payment.\n" +
                                "4.  DO NOT BILL PATIENT!!\n" +
                                "\n" +
                                "Send Claims To:\n" +
                                myNAO.getAddressName() + "\n" +
                                myNAO.getAddress1() + "\n" +
                                myNAO.getAddress2() + "\n" +
                                myNAO.getCity() + ", " + myNAO.getState() + " " + myNAO.getZIP() + "\n" +
                                "Adjuster: " + myEO2.getCase_Adjuster().getContactFirstName() + " " + myEO2.getCase_Adjuster().getContactLastName() + "\n" +
                                "Claim #" + myEO2.getNIM3_CaseAccount().getCaseClaimNumber() + "\n" +
                                "Patient SSN: " + myEO2.getNIM3_CaseAccount().getPatientSSN();
                    } else {
                        theBody += "1.  To ensure prompt payment, please fax all medical reports to 1-888-596-8680 within 48 hours of services being rendered.\n" +
                                "2.  Discrepancies between the scheduled service and physician's script must be clarified before service is rendered.\n" +
                                "3.  Services rendered without prior authorization will be denied for payment.\n" +
                                "4.  THIS IS A WORK COMP CASE. DO NOT BILL PATIENT!!\n" +
                                "\n" +
                                "Send Claims To:\n" +
                                //"</font><font size=\\\"+3\\\">NextImage Medical/" +
                                "NextImage Medical/n" +
                                "Attn: Medical Claims/n" +
                                "3390 Carmel Mountain Rd. Suite 150/n" +
                                "San Diego, CA  92121";
                    }



                }
                if (custom) {
                    theBody = "\n" + appendText;
                } else {
                    theBody += "\n" + appendText;
                }
                myVal.setBody(theBody);
                if (isNID) {
                    myVal.setSubject("NextImage Direct has referred a patient to you! ScanPass#" + myEO2.getNIM3_Encounter().getScanPass());
                } else {
                    myVal.setSubject("Fax: NextImage Medical Update - Appointment Scheduled - ScanPass#" + myEO2.getNIM3_Encounter().getScanPass());
                }
               
                //myVal.setTo();   //to is not set until Alert send is called
            }
//TODO end testing
            else if (sFlag.equalsIgnoreCase("email_sr_vstar")) {
                java.text.SimpleDateFormat displayDateDay = new java.text.SimpleDateFormat(PLCUtils.String_displayDateDay);
                java.text.SimpleDateFormat displayDateDayWeek = new java.text.SimpleDateFormat(PLCUtils.String_displayDateDayWeek);
                java.text.SimpleDateFormat displayDateMonth = new java.text.SimpleDateFormat(PLCUtils.String_displayDateMonth);
                java.text.SimpleDateFormat displayDateYear = new java.text.SimpleDateFormat(PLCUtils.String_displayDateYear);
                java.text.SimpleDateFormat displayDateHour = new java.text.SimpleDateFormat(PLCUtils.String_displayDateHour);

                //myVal.setVirtualAttachment(myDoc.getFileName());
                String theBody = "NextImage Medical Scheduling Request";
                //      theBody += "\n\nProcedure information:\n\n";
                if (myEO2.getNIM3_Encounter().getisSTAT() == 1) {
                    theBody += "\n\nSTAT Order:\tYes";
                }
                theBody += "\n\nScanPass Auth#:\t" + myEO2.getNIM3_Encounter().getScanPass();
                theBody += "\tGender: " + myEO2.getNIM3_CaseAccount().getPatientGender();
                theBody += "\nAddress:\t\t" + myEO2.getNIM3_CaseAccount().getPatientAddress1() + " " + myEO2.getNIM3_CaseAccount().getPatientAddress2() + "";
                theBody += "\n\t\t\t" + myEO2.getNIM3_CaseAccount().getPatientCity() + ", " + new bltStateLI(myEO2.getNIM3_CaseAccount().getPatientStateID()).getShortState() + " " + myEO2.getNIM3_CaseAccount().getPatientZIP() + "";
                //theBody += "\nDOB:\t\t" + PLCUtils.getDisplayDate(myEO2.getNIM3_CaseAccount().getPatientDOB(),false);
                theBody += "\nDate of Injury:\t" + PLCUtils.getDisplayDate(myEO2.getNIM3_CaseAccount().getDateOfInjury(), false);
                //theBody += "\nHome Phone:\t" + myEO2.getNIM3_CaseAccount().getPatientHomePhone() + "\t\tCell:" + myEO2.getNIM3_CaseAccount().getPatientCellPhone();
                theBody += "\nHeight:\t\t" + myEO2.getNIM3_CaseAccount().getPatientHeight();
                theBody += "\nWeight:\t\t" + myEO2.getNIM3_CaseAccount().getPatientWeight();
                theBody += "\n";
                theBody += "\nPatient Availability:\n" + myEO2.getNIM3_Encounter().getPatientAvailability() + "\n";
                Vector myV = myEO2.getPatientPreScreen();
                for (int iTemp = 0; iTemp < myV.size(); iTemp++) {
                    theBody += "\n" + myV.elementAt(iTemp);
                }

                theBody += "\n\nProcedures/Services:";
                bltNIM3_Service working_bltNIM3_Service;
                ListElement leCurrentElement_Service;
                Enumeration eList_Service = myEO2.getNIM3_Service_List().elements();
                int cnt55 = 0;
                while (eList_Service.hasMoreElements()) {
                    cnt55++;
                    leCurrentElement_Service = (ListElement) eList_Service.nextElement();
                    working_bltNIM3_Service = (bltNIM3_Service) leCurrentElement_Service.getObject();
                    if (working_bltNIM3_Service.getServiceStatusID() == 1) {
                        theBody += "\n";
                        if (!working_bltNIM3_Service.getCPT().equalsIgnoreCase("")) {
                            //                    theBody += "\t" + NIMUtils.getCPTText(working_bltNIM3_Service.getCPT()) + "";
                        }
                        if (!working_bltNIM3_Service.getCPTBodyPart().equalsIgnoreCase("")) {
                            theBody += "\t" + working_bltNIM3_Service.getCPTBodyPart();
                        }
                        if (!working_bltNIM3_Service.getCPTText().equalsIgnoreCase("")) {
                            theBody += "\t|Diag/Rule-Out:\t" + working_bltNIM3_Service.getCPTText();
                        }
                    }

                }
                theBody += "\nProcedure Notes: " + myEO2.getNIM3_Encounter().getComments();
                if (myEO2.getNIM3_Encounter().getRequiresFilms() == 1) {
                    theBody += "\nHand Carry Films:\tYes";
                }
                if (myEO2.getNIM3_Encounter().getRequiresAgeInjury() == 1) {
                    theBody += "\nAGE INJURY:\t\tYES";
                }

                if (custom) {
                    theBody = "\n" + appendText;
                } else {
                    theBody += "\n" + appendText;
                }

                theBody += NIMUtils.getEmailSignature(myEO2.getNIM3_CaseAccount().getAssignedToID());
                theBody += "\n-----------------------------------------------------\n";
                myVal.setBody(theBody);
                myVal.setSubject("NextImage Medical Appointment Request - ScanPass#" + myEO2.getNIM3_Encounter().getScanPass());
                //myVal.setTo();   //to is not set until Alert send is called
            } else if (sFlag.equalsIgnoreCase("email_rp_refdr") || sFlag.equalsIgnoreCase("email_rp_adj") || sFlag.equalsIgnoreCase("email_rp_adm") || sFlag.equalsIgnoreCase("email_rp_adm2") || sFlag.equalsIgnoreCase("email_rp_adm3") || sFlag.equalsIgnoreCase("email_rp_adm4") || sFlag.equalsIgnoreCase("email_rp_ncm") || sFlag.equalsIgnoreCase("emaila_rp_refdr") || sFlag.equalsIgnoreCase("emaila_rp_adj") || sFlag.equalsIgnoreCase("emaila_rp_adm") || sFlag.equalsIgnoreCase("emaila_rp_adm2") || sFlag.equalsIgnoreCase("emaila_rp_adm3") || sFlag.equalsIgnoreCase("emaila_rp_adm4") || sFlag.equalsIgnoreCase("emaila_rp_ncm") || sFlag.equalsIgnoreCase("fax_rp_refdr") || sFlag.equalsIgnoreCase("fax_rp_adj") || sFlag.equalsIgnoreCase("fax_rp_adm") || sFlag.equalsIgnoreCase("fax_rp_adm2") || sFlag.equalsIgnoreCase("fax_rp_adm3") || sFlag.equalsIgnoreCase("fax_rp_adm4") || sFlag.equalsIgnoreCase("fax_rp_ncm") || sFlag.equalsIgnoreCase("emaila_rp_all") || sFlag.equalsIgnoreCase("email_rp_adj_alt")) {
                java.text.SimpleDateFormat displayDateDay = new java.text.SimpleDateFormat(PLCUtils.String_displayDateDay);
                java.text.SimpleDateFormat displayDateDayWeek = new java.text.SimpleDateFormat(PLCUtils.String_displayDateDayWeek);
                java.text.SimpleDateFormat displayDateMonth = new java.text.SimpleDateFormat(PLCUtils.String_displayDateMonth);
                java.text.SimpleDateFormat displayDateYear = new java.text.SimpleDateFormat(PLCUtils.String_displayDateYear);
                java.text.SimpleDateFormat displayDateHour = new java.text.SimpleDateFormat(PLCUtils.String_displayDateHour);

                Date dbAppointment = myEO2.getNIM3_Appointment().getAppointmentTime();
                String sAppointmentTime = displayDateTimeSDF.format((dbAppointment));
                String sAppointmentTimeDay = displayDateDay.format((dbAppointment));
                String sAppointmentTimeDayWeek = displayDateDayWeek.format((dbAppointment));
                String sAppointmentTimeMonth = displayDateMonth.format((dbAppointment));
                String sAppointmentTimeYear = displayDateYear.format((dbAppointment));
                String sAppointmentHour = displayDateHour.format((dbAppointment));
                bltNIM3_PayerMaster myPM = new bltNIM3_PayerMaster(new bltUserAccount(myEO2.getNIM3_CaseAccount().getAdjusterID()).getPayerID());


                String theLink = "https://secure.nextimagemedical.com:8443/winstaff/nim3/Outside_View_report.jsp?sssp=" + myEO2.getNIM3_Encounter().getScanPass();
                String theBody = "";
                if (sFlag.equalsIgnoreCase("email_rp_refdr") || sFlag.equalsIgnoreCase("email_rp_adj") || sFlag.equalsIgnoreCase("email_rp_adm") || sFlag.equalsIgnoreCase("email_rp_adm2") || sFlag.equalsIgnoreCase("email_rp_adm3") || sFlag.equalsIgnoreCase("email_rp_adm4") || sFlag.equalsIgnoreCase("email_rp_ncm")) {
                    theBody += "NextImage Medical has a report ready for you to view:";
                    theBody += "\n" + theLink;
                } else if (sFlag.equalsIgnoreCase("emaila_rp_refdr") || sFlag.equalsIgnoreCase("emaila_rp_adj") || sFlag.equalsIgnoreCase("emaila_rp_adm") || sFlag.equalsIgnoreCase("emaila_rp_adm2") || sFlag.equalsIgnoreCase("emaila_rp_adm3") || sFlag.equalsIgnoreCase("emaila_rp_adm4") || sFlag.equalsIgnoreCase("emaila_rp_ncm") || sFlag.equalsIgnoreCase("fax_rp_refdr") || sFlag.equalsIgnoreCase("fax_rp_adj") || sFlag.equalsIgnoreCase("fax_rp_adm") || sFlag.equalsIgnoreCase("fax_rp_adm2") || sFlag.equalsIgnoreCase("fax_rp_adm3") || sFlag.equalsIgnoreCase("fax_rp_adm4") || sFlag.equalsIgnoreCase("fax_rp_ncm")) {

                    if (myPM.getParentPayerID() == 951) {
                        theBody += "NextImage Direct has a report ready for you to view. Please see attached report.\n";
                    } else {
                        theBody += "NextImage Medical has a report ready for you to view. Please see attached report.\n";
                    }
                    bltNIM3_Document myDoc = new bltNIM3_Document(myEO2.getNIM3_Encounter().getReportFileID());
                    myVal.setVirtualAttachment(myDoc.getFileName());
                }
                theBody += "\nThe following users have have been sent notifications:";
                try{
                	if(myEO2.getCase_Adjuster().getUserID()>0){
                		theBody+="\n"+ myEO2.getCase_Adjuster().getContactFirstName()+" "+myEO2.getCase_Adjuster().getContactLastName();
                	}else{
                		theBody+="";
                	}
                }catch(Exception e){}
                try{
                	if(myEO2.getCase_NurseCaseManager().getUserID()>0){
                    	theBody += "\n" + myEO2.getCase_NurseCaseManager().getContactFirstName() + " " + myEO2.getCase_NurseCaseManager().getContactLastName();
                    }else if(myEO2.getCase_NurseCaseManager().getUserID()<=0){
                    	theBody+="";
                    }
                }catch(Exception e){}
                try{
                	if(myEO2.getCase_Admin().getUserID()>0){
                		theBody += "\n" + myEO2.getCase_Admin().getContactFirstName() + " " + myEO2.getCase_Admin().getContactLastName();
                	}else{
                		theBody += "";
                	}
                }catch(Exception e){}
                try{
                	if(myEO2.getCase_Admin2().getUserID()>0){
                		theBody += "\n" + myEO2.getCase_Admin2().getContactFirstName() + " " + myEO2.getCase_Admin2().getContactLastName();
                	}else{
                		theBody += "";
                	}
                }catch(Exception e){}
                try{
                	if(myEO2.getCase_Admin3().getUserID()>0){
                		theBody += "\n" + myEO2.getCase_Admin3().getContactFirstName() + " " + myEO2.getCase_Admin3().getContactLastName();
                	}else{
                		theBody += "";
                	}
                }catch(Exception e){}
                try{
                	if(myEO2.getCase_Admin4().getUserID()>0){
                		theBody += "\n" + myEO2.getCase_Admin4().getContactFirstName() + " " + myEO2.getCase_Admin4().getContactLastName();
                	}else{
                		theBody += "";
                	}
                }catch(Exception e){}
                //theBody += "\n\nProcedure information:\n\n";
                theBody += "\nPatient:\t" + myEO2.getNIM3_CaseAccount().getPatientLastName() + ", " + myEO2.getNIM3_CaseAccount().getPatientFirstName() + "\tGender: " + myEO2.getNIM3_CaseAccount().getPatientGender();
                theBody += "\nAddress:\t\t" + myEO2.getNIM3_CaseAccount().getPatientAddress1() + " " + myEO2.getNIM3_CaseAccount().getPatientAddress2() + "";
                theBody += "\n\t\t\t" + myEO2.getNIM3_CaseAccount().getPatientCity() + ", " + new bltStateLI(myEO2.getNIM3_CaseAccount().getPatientStateID()).getShortState() + " " + myEO2.getNIM3_CaseAccount().getPatientZIP() + "";
                theBody += "\nAdjuster: \t\t" + myEO2.getCase_Adjuster().getContactFirstName() + " " + myEO2.getCase_Adjuster().getContactLastName() + "   [" + myEO2.getNIM3_PayerMaster().getPayerName() + "]\n";
                	
	                		
                if (myPM.getParentPayerID() != 951) {
                    theBody += "Claim Number:\t\t" + myEO2.getNIM3_CaseAccount().getCaseClaimNumber();
                }
                theBody += "\nDate of Injury:\t\t " + displayDateSDF.format(myEO2.getNIM3_CaseAccount().getDateOfInjury()) + "\n";


                theBody += "Referring Doctor: \t" + myEO2.getReferral_ReferringDoctor().getContactFirstName() + " " + myEO2.getReferral_ReferringDoctor().getContactLastName();
                if (!myEO2.getReferral_ReferringDoctor().getContactPhone().equalsIgnoreCase("")) {
                    //            theBody += " Phone:" + myEO2.getReferral_ReferringDoctor().getContactPhone();
                }
                if (!myEO2.getReferral_ReferringDoctor().getContactFax().equalsIgnoreCase("")) {
                    //            theBody += " Fax:" + myEO2.getReferral_ReferringDoctor().getContactFax();
                }


                theBody += "\nProcedure Notes: " + myEO2.getNIM3_Encounter().getComments();


                theBody += "\nService Requested/Performed: \n";
                searchDB2 mySS = new searchDB2();
                java.sql.ResultSet myRS = null;
                ;
                try {
                    String mySQL = ("select * from tNIM3_Service where EncounterID = " + myEO2.getNIM3_Encounter().getEncounterID() + " order by uniquecreatedate");
                    myRS = mySS.executeStatement(mySQL);
                    int endCount = 0;
                    int cnt = 0;
                    int cnt2 = 0;
                    while (myRS != null && myRS.next()) {
                        bltNIM3_Service working_bltNIM3_Service = new bltNIM3_Service(new Integer(myRS.getString("ServiceID")));
                        if (!working_bltNIM3_Service.getCPT().equalsIgnoreCase("")) {
                            theBody += "   CPT: " + working_bltNIM3_Service.getCPT() + "\n";
                            theBody += "      Description: " + NIMUtils.getCPTText(working_bltNIM3_Service.getCPT()) + "\n";
                        }
                        if (!working_bltNIM3_Service.getCPTBodyPart().equalsIgnoreCase("")) {
                            theBody += "      Body Part: " + working_bltNIM3_Service.getCPTBodyPart() + "\n";
                        }
                    }
                } catch (Exception e) {
                    //		out.println("ResultsSet:"+e);
                }

                mySS.closeAll();

                theBody += "\n\nAppointment Time:\t" + sAppointmentTimeDayWeek + " - " + sAppointmentTime;
                theBody += "\nProvider/Location:\t" + myEO2.getAppointment_PracticeMaster().getPracticeName() + "";
                theBody += "\n\t\t\t" + myEO2.getAppointment_PracticeMaster().getOfficeAddress1();
                theBody += "\n\t\t\t" + myEO2.getAppointment_PracticeMaster().getOfficeAddress2();
                theBody += "\n\t\t\t" + myEO2.getAppointment_PracticeMaster().getOfficeCity() + ", " + new bltStateLI(myEO2.getAppointment_PracticeMaster().getOfficeStateID()).getShortState() + " " + myEO2.getAppointment_PracticeMaster().getOfficeZIP();
                theBody += "\n\t\t\tPhone: " + myEO2.getAppointment_PracticeMaster().getOfficePhone() + "\tFax: " + myEO2.getAppointment_PracticeMaster().getOfficeFaxNo();
                theBody += "\n";

                if (custom) {
                    theBody = "\n" + appendText;
                } else {
                    theBody += "\n" + appendText;
                }

                //	theBody += "Upon completion, you will receive an email copy of the medical report.\n\nIf you have any questions, please call us at 888-318-5111.\n\nThank You\n\nNextImage Medical\n\nV2.01";
                theBody += NIMUtils.getEmailSignature(myEO2.getNIM3_CaseAccount().getAssignedToID());
                myVal.setBody(theBody);
                myVal.setSubject("NextImage Medical Update - Report Ready - Case#" + myEO2.getNIM3_CaseAccount().getCaseClaimNumber());
                if (myPM.getParentPayerID() == 951) {
                    myVal.setSubject("NextImage Direct Update - Report Ready - Patient: " + myEO2.getNIM3_CaseAccount().getPatientLastName() + ", " + myEO2.getNIM3_CaseAccount().getPatientFirstName());
                } else {
                    myVal.setSubject("NextImage Medical Update - Report Ready - Case#" + myEO2.getNIM3_CaseAccount().getCaseClaimNumber());
                }

                //myVal.setTo();   //to is not set until Alert send is called
            } else {
                myVal.setTo("giovanni.hernandez@nextimagemedical.com");
                myVal.setSubject("Invalid Alert Type: " + sFlag);
            }
       // }
        myVal.setBody(myVal.getBody()+"\n\n["+sFlag+"]");
        if (isFax)
        {
//            myVal.setFrom(ConfigurationInformation.FaxFromAddress);    // no longer override- just send FROM the CaseAssignedTo
            myVal.setFrom(new bltUserAccount(myEO2.getNIM3_CaseAccount().getAssignedToID()).getContactEmail());
        }
        else
        {
            myVal.setFrom(new bltUserAccount(myEO2.getNIM3_CaseAccount().getAssignedToID()).getContactEmail());
        }
    }
    
    
    
    catch (Exception emailAlertFail)
    {
        myVal.setBody("Alert Error - Contact IT");
        myVal.setTo("giovanni.hernandez@nextimagemedical.com");
        myVal.cc.clear();
        myVal.cc= new java.util.Vector();

        DebugLogger.printLine("NIMUtils:getAlertEmail: failed generating alert email for EncounterID("+myEO2.getNIM3_Encounter().getEncounterID()+"):["+emailAlertFail+"]");
        NIMUtils.emailFatalError("NIMUtils:getAlertEmail: failed generating alert email for EncounterID("+myEO2.getNIM3_Encounter().getEncounterID()+"):["+emailAlertFail+"]");
    }



	return myVal;
}


//send to all Users and All Managers, etc. based on iUser ID
public static String sendAlertEmail(Integer iEncounterID, String sFlag, String appendText, boolean customEmail, String sNIMUserName)
{
    return sendAlertEmail(new NIM3_EncounterObject2(iEncounterID,"NIMUtils:sendAlertEmail:System Generated"),  sFlag,  appendText,  customEmail,  sNIMUserName) ;
}
//send to all Users and All Managers, etc. based on iUser ID
public static String sendAlertEmail(NIM3_EncounterObject2 myEO2, String sFlag, String appendText, boolean customEmail, String sNIMUserName)
{
//	System.out.print("<h1>" + customEmail + "<h1>");

    boolean isSend = true;
    String myVal = "";

    bltPracticeMaster myPractice = null;
    if (myEO2.getNIM3_Appointment()!=null) 
    {
        myPractice = new bltPracticeMaster(myEO2.getNIM3_Appointment().getProviderID());
    }

    boolean isFailSend = false;
    emailType_V3 myEmail = NIMUtils.getAlertEmail(myEO2, sFlag, appendText, customEmail);
    //String sNIMUserName = "";
    String sTargetRec = "";
    boolean isFax = false;
    boolean isAttach = false;
    try
    {
        if (sFlag.equalsIgnoreCase("emaila_rc_all"))  
        {
            sTargetRec = "[CC RC Email Sent] Including: ";
            if (myEO2.getNIM3_CaseAccount().getAdjusterID().intValue()!=0&&NIMUtils.isValidEmail(myEO2.getCase_Adjuster().getContactEmail())&&!myEO2.getNIM3_Encounter().getSentTo_ReqRec_Adj().after( NIMUtils.getBeforeTime()))
            {
                myEO2.getNIM3_Encounter().setSentTo_ReqRec_Adj(PLCUtils.getNowDate(true));
                myEO2.getNIM3_Encounter().commitData();
                sTargetRec += "["+myEO2.getCase_Adjuster().getContactEmail()+"]";
            }
            if (myEO2.getNIM3_CaseAccount().getCaseAdministratorID().intValue()!=0&&NIMUtils.isValidEmail(myEO2.getCase_Admin().getContactEmail())&&!myEO2.getNIM3_Encounter().getSentTo_ReqRec_Adm().after( NIMUtils.getBeforeTime()))
            {
                myEO2.getNIM3_Encounter().setSentTo_ReqRec_Adm(PLCUtils.getNowDate(true));
                myEO2.getNIM3_Encounter().commitData();
                sTargetRec += "["+myEO2.getCase_Admin().getContactEmail()+"]";
            }
            if (myEO2.getNIM3_CaseAccount().getCaseAdministrator2ID().intValue()!=0&&NIMUtils.isValidEmail(myEO2.getCase_Admin2().getContactEmail())&&!myEO2.getNIM3_Encounter().getSentTo_ReqRec_Adm2().after( NIMUtils.getBeforeTime()))
            {
                myEO2.getNIM3_Encounter().setSentTo_ReqRec_Adm2(PLCUtils.getNowDate(true));
                myEO2.getNIM3_Encounter().commitData();
                sTargetRec += "["+myEO2.getCase_Admin2().getContactEmail()+"]";
            }
            if (myEO2.getNIM3_CaseAccount().getCaseAdministrator3ID().intValue()!=0&&NIMUtils.isValidEmail(myEO2.getCase_Admin3().getContactEmail())&&!myEO2.getNIM3_Encounter().getSentTo_ReqRec_Adm3().after( NIMUtils.getBeforeTime()))
            {
                myEO2.getNIM3_Encounter().setSentTo_ReqRec_Adm3(PLCUtils.getNowDate(true));
                myEO2.getNIM3_Encounter().commitData();
                sTargetRec += "["+myEO2.getCase_Admin3().getContactEmail()+"]";
            }
            if (myEO2.getNIM3_CaseAccount().getCaseAdministrator4ID().intValue()!=0&&NIMUtils.isValidEmail(myEO2.getCase_Admin4().getContactEmail())&&!myEO2.getNIM3_Encounter().getSentTo_ReqRec_Adm4().after( NIMUtils.getBeforeTime()))
            {
                myEO2.getNIM3_Encounter().setSentTo_ReqRec_Adm4(PLCUtils.getNowDate(true));
                myEO2.getNIM3_Encounter().commitData();
                sTargetRec += "["+myEO2.getCase_Admin4().getContactEmail()+"]";
            }
            if (myEO2.getNIM3_CaseAccount().getNurseCaseManagerID().intValue()!=0&&NIMUtils.isValidEmail(myEO2.getCase_NurseCaseManager().getContactEmail())&&!myEO2.getNIM3_Encounter().getSentTo_ReqRec_NCM().after( NIMUtils.getBeforeTime()))
            {
                myEO2.getNIM3_Encounter().setSentTo_ReqRec_NCM(PLCUtils.getNowDate(true));
                myEO2.getNIM3_Encounter().commitData();
                sTargetRec += "["+myEO2.getCase_NurseCaseManager().getContactEmail()+"]";
            }
        }
        else if (sFlag.equalsIgnoreCase("emaila_sp_all"))
        {
            sTargetRec = "[CC SP Email Sent] Including: ";
            if (myEO2.getNIM3_CaseAccount().getAdjusterID().intValue()!=0&&NIMUtils.isValidEmail(myEO2.getCase_Adjuster().getContactEmail())&&!myEO2.getNIM3_Encounter().getSentTo_SP_Adj().after( NIMUtils.getBeforeTime()))
            {
                myEO2.getNIM3_Encounter().setSentTo_SP_Adj(PLCUtils.getNowDate(true));
                myEO2.getNIM3_Encounter().commitData();
                sTargetRec += "["+myEO2.getCase_Adjuster().getContactEmail()+"]";
            }
            if (myEO2.getNIM3_CaseAccount().getCaseAdministratorID().intValue()!=0&&NIMUtils.isValidEmail(myEO2.getCase_Admin().getContactEmail())&&!myEO2.getNIM3_Encounter().getSentTo_SP_Adm().after( NIMUtils.getBeforeTime()))
            {
                myEO2.getNIM3_Encounter().setSentTo_SP_Adm(PLCUtils.getNowDate(true));
                myEO2.getNIM3_Encounter().commitData();
                sTargetRec += "["+myEO2.getCase_Admin().getContactEmail()+"]";
            }
            if (myEO2.getNIM3_CaseAccount().getCaseAdministrator2ID().intValue()!=0&&NIMUtils.isValidEmail(myEO2.getCase_Admin2().getContactEmail())&&!myEO2.getNIM3_Encounter().getSentTo_SP_Adm2().after( NIMUtils.getBeforeTime()))
            {
                myEO2.getNIM3_Encounter().setSentTo_SP_Adm2(PLCUtils.getNowDate(true));
                myEO2.getNIM3_Encounter().commitData();
                sTargetRec += "["+myEO2.getCase_Admin2().getContactEmail()+"]";
            }
            if (myEO2.getNIM3_CaseAccount().getCaseAdministrator3ID().intValue()!=0&&NIMUtils.isValidEmail(myEO2.getCase_Admin3().getContactEmail())&&!myEO2.getNIM3_Encounter().getSentTo_SP_Adm3().after( NIMUtils.getBeforeTime()))
            {
                myEO2.getNIM3_Encounter().setSentTo_SP_Adm3(PLCUtils.getNowDate(true));
                myEO2.getNIM3_Encounter().commitData();
                sTargetRec += "["+myEO2.getCase_Admin3().getContactEmail()+"]";
            }
            if (myEO2.getNIM3_CaseAccount().getCaseAdministrator4ID().intValue()!=0&&NIMUtils.isValidEmail(myEO2.getCase_Admin4().getContactEmail())&&!myEO2.getNIM3_Encounter().getSentTo_SP_Adm4().after( NIMUtils.getBeforeTime()))
            {
                myEO2.getNIM3_Encounter().setSentTo_SP_Adm4(PLCUtils.getNowDate(true));
                myEO2.getNIM3_Encounter().commitData();
                sTargetRec += "["+myEO2.getCase_Admin4().getContactEmail()+"]";
            }
            if (myEO2.getNIM3_CaseAccount().getNurseCaseManagerID().intValue()!=0&&NIMUtils.isValidEmail(myEO2.getCase_NurseCaseManager().getContactEmail())&&!myEO2.getNIM3_Encounter().getSentTo_SP_NCM().after( NIMUtils.getBeforeTime()))
            {
                myEO2.getNIM3_Encounter().setSentTo_SP_NCM(PLCUtils.getNowDate(true));
                myEO2.getNIM3_Encounter().commitData();
                sTargetRec += "["+myEO2.getCase_NurseCaseManager().getContactEmail()+"]";
            }
        }
        else if (sFlag.equalsIgnoreCase("emaila_rp_all"))
        {
            sTargetRec = "[CC RP Email Sent] Including: ";
            if (myEO2.getNIM3_CaseAccount().getAdjusterID().intValue()!=0&&NIMUtils.isValidEmail(myEO2.getCase_Adjuster().getContactEmail())&&!myEO2.getNIM3_Encounter().getSentToAdj().after( NIMUtils.getBeforeTime()))
            {
                myEO2.getNIM3_Encounter().setSentToAdj(PLCUtils.getNowDate(true));
                myEO2.getNIM3_Encounter().commitData();
                sTargetRec += "["+myEO2.getCase_Adjuster().getContactEmail()+"]";
            }
            if (myEO2.getNIM3_CaseAccount().getCaseAdministratorID().intValue()!=0&&NIMUtils.isValidEmail(myEO2.getCase_Admin().getContactEmail())&&!myEO2.getNIM3_Encounter().getSentToAdm().after( NIMUtils.getBeforeTime()))
            {
                myEO2.getNIM3_Encounter().setSentToAdm(PLCUtils.getNowDate(true));
                myEO2.getNIM3_Encounter().commitData();
                sTargetRec += "["+myEO2.getCase_Admin().getContactEmail()+"]";
            }
            if (myEO2.getNIM3_CaseAccount().getCaseAdministrator2ID().intValue()!=0&&NIMUtils.isValidEmail(myEO2.getCase_Admin2().getContactEmail())&&!myEO2.getNIM3_Encounter().getSentTo_RP_Adm2().after( NIMUtils.getBeforeTime()))
            {
                myEO2.getNIM3_Encounter().setSentTo_RP_Adm2(PLCUtils.getNowDate(true));
                myEO2.getNIM3_Encounter().commitData();
                sTargetRec += "["+myEO2.getCase_Admin2().getContactEmail()+"]";
            }
            if (myEO2.getNIM3_CaseAccount().getCaseAdministrator3ID().intValue()!=0&&NIMUtils.isValidEmail(myEO2.getCase_Admin3().getContactEmail())&&!myEO2.getNIM3_Encounter().getSentTo_RP_Adm3().after( NIMUtils.getBeforeTime()))
            {
                myEO2.getNIM3_Encounter().setSentTo_RP_Adm3(PLCUtils.getNowDate(true));
                myEO2.getNIM3_Encounter().commitData();
                sTargetRec += "["+myEO2.getCase_Admin3().getContactEmail()+"]";
            }
            if (myEO2.getNIM3_CaseAccount().getCaseAdministrator4ID().intValue()!=0&&NIMUtils.isValidEmail(myEO2.getCase_Admin4().getContactEmail())&&!myEO2.getNIM3_Encounter().getSentTo_RP_Adm4().after( NIMUtils.getBeforeTime()))
            {
                myEO2.getNIM3_Encounter().setSentTo_RP_Adm4(PLCUtils.getNowDate(true));
                myEO2.getNIM3_Encounter().commitData();
                sTargetRec += "["+myEO2.getCase_Admin4().getContactEmail()+"]";
            }
            if (myEO2.getNIM3_CaseAccount().getNurseCaseManagerID().intValue()!=0&&NIMUtils.isValidEmail(myEO2.getCase_NurseCaseManager().getContactEmail())&&!myEO2.getNIM3_Encounter().getSentTo_RP_NCM().after( NIMUtils.getBeforeTime()))
            {
                myEO2.getNIM3_Encounter().setSentTo_RP_NCM(PLCUtils.getNowDate(true));
                myEO2.getNIM3_Encounter().commitData();
                sTargetRec += "["+myEO2.getCase_NurseCaseManager().getContactEmail()+"]";
            }
        }
        else if (sFlag.equalsIgnoreCase("email_rc_refdr")||sFlag.equalsIgnoreCase("fax_rc_refdr"))
        {
            if (myEO2.getNIM3_Encounter().getSentTo_ReqRec_RefDr().before( new java.util.Date(1)))
            {
                myEO2.getNIM3_Encounter().setSentTo_ReqRec_RefDr(PLCUtils.getNowDate(true));
                myEO2.getNIM3_Encounter().commitData();
            }
            sTargetRec = "["+ myEO2.getReferral_ReferringDoctor().getContactEmail()+"] Ref Dr Email["+ myEO2.getReferral_ReferringDoctor().getContactLastName()+"]";
            if (sFlag.equalsIgnoreCase("fax_rc_refdr"))
            {
                isFax = true;
                sTargetRec = "["+ myEO2.getReferral_ReferringDoctor().getContactFax()+"] Ref Dr Email["+ myEO2.getReferral_ReferringDoctor().getContactLastName()+"]";
            }
        }
        else if (sFlag.equalsIgnoreCase("email_rc_adj")||sFlag.equalsIgnoreCase("fax_rc_adj"))
        {
            if (myEO2.getNIM3_Encounter().getSentTo_ReqRec_Adj().before( new java.util.Date(1)))
            {
                myEO2.getNIM3_Encounter().setSentTo_ReqRec_Adj(PLCUtils.getNowDate(true));
                myEO2.getNIM3_Encounter().commitData();
            }
            sTargetRec = "["+ myEO2.getCase_Adjuster().getContactEmail()+"] Adj Email["+ myEO2.getCase_Adjuster().getContactLastName()+"]";
            if (sFlag.equalsIgnoreCase("fax_rc_adj"))
            {
                isFax = true;
                sTargetRec = "["+ myEO2.getCase_Adjuster().getContactFax()+"] Adj Email["+ myEO2.getCase_Adjuster().getContactLastName()+"]";
            }
        }
        else if (sFlag.equalsIgnoreCase("email_rc_adm")||sFlag.equalsIgnoreCase("fax_rc_adm"))
        {
            if (myEO2.getNIM3_Encounter().getSentTo_ReqRec_Adm().before( new java.util.Date(1)))
            {
                myEO2.getNIM3_Encounter().setSentTo_ReqRec_Adm(PLCUtils.getNowDate(true));
                myEO2.getNIM3_Encounter().commitData();
            }
            sTargetRec = "["+ myEO2.getCase_Admin().getContactEmail()+"] Adm Email["+ myEO2.getCase_Admin().getContactLastName()+"]";
//            myEmail.setTo(Case_Admin.getContactEmail());
            if (sFlag.equalsIgnoreCase("fax_rc_adm"))
            {
                isFax = true;
//                myEmail.setTo(Case_Admin.getContactFax());
                sTargetRec = "["+ myEO2.getCase_Admin().getContactFax()+"] Adm Email["+ myEO2.getCase_Admin().getContactLastName()+"]";
            }
        }
        else if (sFlag.equalsIgnoreCase("email_rc_adm2")||sFlag.equalsIgnoreCase("fax_rc_adm2"))
        {
            if (myEO2.getNIM3_Encounter().getSentTo_ReqRec_Adm2().before( new java.util.Date(1)))
            {
                myEO2.getNIM3_Encounter().setSentTo_ReqRec_Adm2(PLCUtils.getNowDate(true));
                myEO2.getNIM3_Encounter().commitData();
            }
            sTargetRec = "["+ myEO2.getCase_Admin2().getContactEmail()+"] Adm2 Email["+ myEO2.getCase_Admin2().getContactLastName()+"]";
//            myEmail.setTo(Case_Admin2.getContactEmail());
            if (sFlag.equalsIgnoreCase("fax_rc_adm2"))
            {
                isFax = true;
//                myEmail.setTo(Case_Adm2in.getContactFax());
                sTargetRec = "["+ myEO2.getCase_Admin2().getContactFax()+"] Adm2 Email["+ myEO2.getCase_Admin2().getContactLastName()+"]";
            }
        }
        else if (sFlag.equalsIgnoreCase("email_rc_adm3")||sFlag.equalsIgnoreCase("fax_rc_adm3"))
        {
            if (myEO2.getNIM3_Encounter().getSentTo_ReqRec_Adm3().before( new java.util.Date(1)))
            {
                myEO2.getNIM3_Encounter().setSentTo_ReqRec_Adm3(PLCUtils.getNowDate(true));
                myEO2.getNIM3_Encounter().commitData();
            }
            sTargetRec = "["+ myEO2.getCase_Admin3().getContactEmail()+"] Adm3 Email["+ myEO2.getCase_Admin3().getContactLastName()+"]";
//            myEmail.setTo(Case_Adm3in.getContactEmail());
            if (sFlag.equalsIgnoreCase("fax_rc_adm3"))
            {
                isFax = true;
//                myEmail.setTo(Case_Admin3.getContactFax());
                sTargetRec = "["+ myEO2.getCase_Admin3().getContactFax()+"] Adm3 Email["+ myEO2.getCase_Admin3().getContactLastName()+"]";
            }
        }
        else if (sFlag.equalsIgnoreCase("email_rc_adm4")||sFlag.equalsIgnoreCase("fax_rc_adm4"))
        {
            if (myEO2.getNIM3_Encounter().getSentTo_ReqRec_Adm4().before( new java.util.Date(1)))
            {
                myEO2.getNIM3_Encounter().setSentTo_ReqRec_Adm4(PLCUtils.getNowDate(true));
                myEO2.getNIM3_Encounter().commitData();
            }
            sTargetRec = "["+ myEO2.getCase_Admin4().getContactEmail()+"] Adm4 Email["+ myEO2.getCase_Admin4().getContactLastName()+"]";
//            myEmail.setTo(Case_Adm4in.getContactEmail());
            if (sFlag.equalsIgnoreCase("fax_rc_adm4"))
            {
                isFax = true;
//                myEmail.setTo(Case_Admin4.getContactFax());
                sTargetRec = "["+ myEO2.getCase_Admin4().getContactFax()+"] Adm4 Email["+ myEO2.getCase_Admin4().getContactLastName()+"]";
            }
        }
        else if (sFlag.equalsIgnoreCase("email_rc_ncm")||sFlag.equalsIgnoreCase("fax_rc_ncm"))
        {
            if (myEO2.getNIM3_Encounter().getSentTo_ReqRec_NCM().before( new java.util.Date(1)))
            {
                myEO2.getNIM3_Encounter().setSentTo_ReqRec_NCM(PLCUtils.getNowDate(true));
                myEO2.getNIM3_Encounter().commitData();
            }
            sTargetRec = "["+ myEO2.getCase_NurseCaseManager().getContactEmail()+"] NCM Email["+ myEO2.getCase_NurseCaseManager().getContactLastName()+"]";
//            myEmail.setTo(Case_NurseCaseManager.getContactEmail());
            if (sFlag.equalsIgnoreCase("fax_rc_ncm"))
            {
                isFax = true;
//                myEmail.setTo(NIMUtils.fax2EmailFax(Case_NurseCaseManager.getContactFax()));
                sTargetRec = "["+ myEO2.getCase_NurseCaseManager().getContactFax()+"] NCM Email["+ myEO2.getCase_NurseCaseManager().getContactLastName()+"]";
            }
        }
        else if (sFlag.equalsIgnoreCase("email_sp_refdr")||sFlag.equalsIgnoreCase("fax_sp_refdr"))
        {
            if (myEO2.getNIM3_Encounter().getSentTo_SP_RefDr().before( new java.util.Date(1)))
            {
                myEO2.getNIM3_Encounter().setSentTo_SP_RefDr(PLCUtils.getNowDate(true));
                myEO2.getNIM3_Encounter().commitData();
            }
            sTargetRec = "["+ myEO2.getReferral_ReferringDoctor().getContactEmail()+"] Ref Dr Email["+ myEO2.getReferral_ReferringDoctor().getContactLastName()+"]";
//            myEmail.setTo(Referral_ReferringDoctor.getContactEmail());
            if (sFlag.equalsIgnoreCase("fax_sp_refdr"))
            {
                isFax = true;
//                myEmail.setTo(NIMUtils.fax2EmailFax(Referral_ReferringDoctor.getContactFax()));
                sTargetRec = "["+ myEO2.getReferral_ReferringDoctor().getContactFax()+"] Ref Dr Email["+ myEO2.getReferral_ReferringDoctor().getContactLastName()+"]";
            }
        }
        else if (sFlag.equalsIgnoreCase("email_sp_adj")||sFlag.equalsIgnoreCase("fax_sp_adj"))
        {
            if (myEO2.getNIM3_Encounter().getSentTo_SP_Adj().before( new java.util.Date(1)))
            {
                myEO2.getNIM3_Encounter().setSentTo_SP_Adj(PLCUtils.getNowDate(true));
                myEO2.getNIM3_Encounter().commitData();
            }
            sTargetRec = "["+ myEO2.getCase_Adjuster().getContactEmail()+"] Adj Email["+ myEO2.getCase_Adjuster().getContactLastName()+"]";
//            myEmail.setTo(Case_Adjuster.getContactEmail());
            if (sFlag.equalsIgnoreCase("fax_sp_adj"))
            {
                isFax = true;
//                myEmail.setTo(NIMUtils.fax2EmailFax(Case_Adjuster.getContactFax()));
                sTargetRec = "["+ myEO2.getCase_Adjuster().getContactFax()+"] Adj Email["+ myEO2.getCase_Adjuster().getContactLastName()+"]";
            }
        }
        else if (sFlag.equalsIgnoreCase("email_sp_adm")||sFlag.equalsIgnoreCase("fax_sp_adm"))
        {
            if (myEO2.getNIM3_Encounter().getSentTo_SP_Adm().before( new java.util.Date(1)))
            {
                myEO2.getNIM3_Encounter().setSentTo_SP_Adm(PLCUtils.getNowDate(true));
                myEO2.getNIM3_Encounter().commitData();
            }
            sTargetRec = "["+ myEO2.getCase_Admin().getContactEmail()+"] Adm Email["+ myEO2.getCase_Admin().getContactLastName()+"]";
//            myEmail.setTo(Case_Admin.getContactEmail());
            if (sFlag.equalsIgnoreCase("fax_sp_adm"))
            {
                isFax = true;
//                myEmail.setTo(NIMUtils.fax2EmailFax(Case_Admin.getContactFax()));
                sTargetRec = "["+ myEO2.getCase_Admin().getContactFax()+"] Adm Email["+ myEO2.getCase_Admin().getContactLastName()+"]";
            }
        }
        else if (sFlag.equalsIgnoreCase("email_sp_adm2")||sFlag.equalsIgnoreCase("fax_sp_adm2"))
        {
            if (myEO2.getNIM3_Encounter().getSentTo_SP_Adm2().before( new java.util.Date(1)))
            {
                myEO2.getNIM3_Encounter().setSentTo_SP_Adm2(PLCUtils.getNowDate(true));
                myEO2.getNIM3_Encounter().commitData();
            }
            sTargetRec = "["+ myEO2.getCase_Admin2().getContactEmail()+"] Adm Email["+ myEO2.getCase_Admin2().getContactLastName()+"]";
//            myEmail.setTo(Case_Admin2.getContactEmail());
            if (sFlag.equalsIgnoreCase("fax_sp_adm2"))
            {
                isFax = true;
//                myEmail.setTo(NIMUtils.fax2EmailFax(Case_Admin2.getContactFax()));
                sTargetRec = "["+ myEO2.getCase_Admin2().getContactFax()+"] Adm Email["+ myEO2.getCase_Admin2().getContactLastName()+"]";
            }
        }
        else if (sFlag.equalsIgnoreCase("email_sp_adm3")||sFlag.equalsIgnoreCase("fax_sp_adm3"))
        {
            if (myEO2.getNIM3_Encounter().getSentTo_SP_Adm3().before( new java.util.Date(1)))
            {
                myEO2.getNIM3_Encounter().setSentTo_SP_Adm3(PLCUtils.getNowDate(true));
                myEO2.getNIM3_Encounter().commitData();
            }
            sTargetRec = "["+ myEO2.getCase_Admin3().getContactEmail()+"] Adm Email["+ myEO2.getCase_Admin3().getContactLastName()+"]";
//            myEmail.setTo(Case_Admin3.getContactEmail());
            if (sFlag.equalsIgnoreCase("fax_sp_adm3"))
            {
                isFax = true;
//                myEmail.setTo(NIMUtils.fax2EmailFax(Case_Admin3.getContactFax()));
                sTargetRec = "["+ myEO2.getCase_Admin3().getContactFax()+"] Adm Email["+ myEO2.getCase_Admin3().getContactLastName()+"]";
            }
        }
        else if (sFlag.equalsIgnoreCase("email_sp_adm4")||sFlag.equalsIgnoreCase("fax_sp_adm4"))
        {
            if (myEO2.getNIM3_Encounter().getSentTo_SP_Adm4().before( new java.util.Date(1)))
            {
                myEO2.getNIM3_Encounter().setSentTo_SP_Adm4(PLCUtils.getNowDate(true));
                myEO2.getNIM3_Encounter().commitData();
            }
            sTargetRec = "["+ myEO2.getCase_Admin4().getContactEmail()+"] Adm Email["+ myEO2.getCase_Admin4().getContactLastName()+"]";
//            myEmail.setTo(Case_Admin4.getContactEmail());
            if (sFlag.equalsIgnoreCase("fax_sp_adm4"))
            {
                isFax = true;
//                myEmail.setTo(NIMUtils.fax2EmailFax(Case_Admin4.getContactFax()));
                sTargetRec = "["+ myEO2.getCase_Admin4().getContactFax()+"] Adm Email["+ myEO2.getCase_Admin4().getContactLastName()+"]";
            }
        }
        else if (sFlag.equalsIgnoreCase("email_sp_ncm")||sFlag.equalsIgnoreCase("fax_sp_ncm"))
        {
            if (myEO2.getNIM3_Encounter().getSentTo_SP_NCM().before( new java.util.Date(1)))
            {
                myEO2.getNIM3_Encounter().setSentTo_SP_NCM(PLCUtils.getNowDate(true));
                myEO2.getNIM3_Encounter().commitData();
            }
            sTargetRec = "["+ myEO2.getCase_NurseCaseManager().getContactEmail()+"] NCM Email["+ myEO2.getCase_NurseCaseManager().getContactLastName()+"]";
//            myEmail.setTo(Case_NurseCaseManager.getContactEmail());
            if (sFlag.equalsIgnoreCase("fax_sp_ncm"))
            {
                isFax = true;
//                myEmail.setTo(NIMUtils.fax2EmailFax(Case_NurseCaseManager.getContactFax()));
                sTargetRec = "["+ myEO2.getCase_NurseCaseManager().getContactFax()+"] NCM Email["+ myEO2.getCase_NurseCaseManager().getContactLastName()+"]";
            }
        }
        
        else if (sFlag.equalsIgnoreCase("email_sp_ic")||sFlag.equalsIgnoreCase("fax_sp_ic"))
        {
            if (myEmail.getTo().indexOf("@")<10)
            {
                isFailSend = true;
                DebugLogger.printLine("NIMUtils:sendAlertEmail:BadFax ["+myEmail.getTo()+"]");
                myVal += ("Invalid Fax Number");

            }
            else
            {
                if (myEO2.getNIM3_Encounter().getSentTo_SP_IC().before( new java.util.Date(1)))
                {
                    myEO2.getNIM3_Encounter().setSentTo_SP_IC(PLCUtils.getNowDate(true));
                    myEO2.getNIM3_Encounter().commitData();
                }
                //myEmail.setTo("scott@nextimagemedical.com");
                //myEmail.setSubject("FWIC: " + myEmail.getSubject());
                if (sFlag.equalsIgnoreCase("fax_sp_ic"))
                {
                    isFax = true;
    //                myEmail.setTo(NIMUtils.fax2EmailFax(myPractice.getOfficeFaxNo()));
                    sTargetRec = "["+myPractice.getOfficeFaxNo()+"] Practice Email["+ myPractice.getPracticeName()+"]";
                }
            }
        }
        else if (sFlag.equalsIgnoreCase("email_sr_vstar"))
        {
            sTargetRec = "[" + myEmail.getTo() + "] V-Star Email";
        }
        else if (sFlag.equalsIgnoreCase("email_rp_refdr")||sFlag.equalsIgnoreCase("emaila_rp_refdr")||sFlag.equalsIgnoreCase("fax_rp_refdr"))
        {
            if (myEO2.getNIM3_Encounter().getSentToRefDr().before( new java.util.Date(1)))
            {
                myEO2.getNIM3_Encounter().setSentToRefDr(PLCUtils.getNowDate(true));
                myEO2.getNIM3_Encounter().commitData();
            }
            sTargetRec = "["+ myEO2.getReferral_ReferringDoctor().getContactEmail()+"] Ref Dr Email["+ myEO2.getReferral_ReferringDoctor().getContactLastName()+"]";
//            myEmail.setTo(Referral_ReferringDoctor.getContactEmail());
            if (sFlag.equalsIgnoreCase("fax_rp_refdr"))
            {
                isFax = true;
//                myEmail.setTo(NIMUtils.fax2EmailFax(Referral_ReferringDoctor.getContactFax()));
                sTargetRec = "["+ myEO2.getReferral_ReferringDoctor().getContactFax()+"] Ref Dr Email["+ myEO2.getReferral_ReferringDoctor().getContactLastName()+"]";
            }
        }
        else if (sFlag.equalsIgnoreCase("email_rp_adj")||sFlag.equalsIgnoreCase("emaila_rp_adj")||sFlag.equalsIgnoreCase("fax_rp_adj"))
        {
            if (myEO2.getNIM3_Encounter().getSentToAdj().before( new java.util.Date(1)))
            {
                myEO2.getNIM3_Encounter().setSentToAdj(PLCUtils.getNowDate(true));
                myEO2.getNIM3_Encounter().commitData();
            }
            sTargetRec = "["+ myEO2.getCase_Adjuster().getContactEmail()+"] Adj Email["+ myEO2.getCase_Adjuster().getContactLastName()+"]";
//            myEmail.setTo(Case_Adjuster.getContactEmail());
            if (sFlag.equalsIgnoreCase("fax_rp_adj"))
            {
                isFax = true;
//                myEmail.setTo(NIMUtils.fax2EmailFax(Case_Adjuster.getContactFax()));
                sTargetRec = "["+ myEO2.getCase_Adjuster().getContactFax()+"] Adj Email["+ myEO2.getCase_Adjuster().getContactLastName()+"]";
            }
        }
        else if (sFlag.equalsIgnoreCase("email_rp_adm")||sFlag.equalsIgnoreCase("emaila_rp_adm")||sFlag.equalsIgnoreCase("fax_rp_adm"))
        {
            if (myEO2.getNIM3_Encounter().getSentToAdm().before( new java.util.Date(1)))
            {
                myEO2.getNIM3_Encounter().setSentToAdm(PLCUtils.getNowDate(true));
                myEO2.getNIM3_Encounter().commitData();
            }
            sTargetRec = "["+ myEO2.getCase_Admin().getContactEmail()+"] Adm Email["+ myEO2.getCase_Admin().getContactLastName()+"]";
//            myEmail.setTo(Case_Admin.getContactEmail());
            if (sFlag.equalsIgnoreCase("fax_rp_adm"))
            {
                isFax = true;
//                myEmail.setTo(NIMUtils.fax2EmailFax(Case_Admin.getContactFax()));
                sTargetRec = "["+ myEO2.getCase_Admin().getContactFax()+"] Adm Email["+ myEO2.getCase_Admin().getContactLastName()+"]";
            }
        }
        else if (sFlag.equalsIgnoreCase("email_rp_adm2")||sFlag.equalsIgnoreCase("emaila_rp_adm2")||sFlag.equalsIgnoreCase("fax_rp_adm2"))
        {
            if (myEO2.getNIM3_Encounter().getSentTo_RP_Adm2().before( new java.util.Date(1)))
            {
                myEO2.getNIM3_Encounter().setSentTo_RP_Adm2(PLCUtils.getNowDate(true));
                myEO2.getNIM3_Encounter().commitData();
            }
            sTargetRec = "["+ myEO2.getCase_Admin2().getContactEmail()+"] Adm2 Email["+ myEO2.getCase_Admin2().getContactLastName()+"]";
//            myEmail.setTo(Case_Adm2in2.getContactEmail());
            if (sFlag.equalsIgnoreCase("fax_rp_adm2"))
            {
                isFax = true;
//                myEmail.setTo(NIMUtils.fax2EmailFax(Case_Adm2in2.getContactFax()));
                sTargetRec = "["+ myEO2.getCase_Admin2().getContactFax()+"] Adm2 Email["+ myEO2.getCase_Admin2().getContactLastName()+"]";
            }
        }
        else if (sFlag.equalsIgnoreCase("email_rp_adm3")||sFlag.equalsIgnoreCase("emaila_rp_adm3")||sFlag.equalsIgnoreCase("fax_rp_adm3"))
        {
            if (myEO2.getNIM3_Encounter().getSentTo_RP_Adm3().before( new java.util.Date(1)))
            {
                myEO2.getNIM3_Encounter().setSentTo_RP_Adm3(PLCUtils.getNowDate(true));
                myEO2.getNIM3_Encounter().commitData();
            }
            sTargetRec = "["+ myEO2.getCase_Admin3().getContactEmail()+"] Adm3 Email["+ myEO2.getCase_Admin3().getContactLastName()+"]";
//            myEmail.setTo(Case_Adm3in2.getContactEmail());
            if (sFlag.equalsIgnoreCase("fax_rp_adm3"))
            {
                isFax = true;
//                myEmail.setTo(NIMUtils.fax2EmailFax(Case_Adm3in2.getContactFax()));
                sTargetRec = "["+ myEO2.getCase_Admin3().getContactFax()+"] Adm3 Email["+ myEO2.getCase_Admin3().getContactLastName()+"]";
            }
        }
        else if (sFlag.equalsIgnoreCase("email_rp_adm4")||sFlag.equalsIgnoreCase("emaila_rp_adm4")||sFlag.equalsIgnoreCase("fax_rp_adm4"))
        {
            if (myEO2.getNIM3_Encounter().getSentTo_RP_Adm4().before( new java.util.Date(1)))
            {
                myEO2.getNIM3_Encounter().setSentTo_RP_Adm4(PLCUtils.getNowDate(true));
                myEO2.getNIM3_Encounter().commitData();
            }
            sTargetRec = "["+ myEO2.getCase_Admin4().getContactEmail()+"] Adm4 Email["+ myEO2.getCase_Admin4().getContactLastName()+"]";
//            myEmail.setTo(Case_Adm4in2.getContactEmail());
            if (sFlag.equalsIgnoreCase("fax_rp_adm4"))
            {
                isFax = true;
//                myEmail.setTo(NIMUtils.fax2EmailFax(Case_Adm4in2.getContactFax()));
                sTargetRec = "["+ myEO2.getCase_Admin4().getContactFax()+"] Adm4 Email["+ myEO2.getCase_Admin4().getContactLastName()+"]";
            }
        }
        else if (sFlag.equalsIgnoreCase("email_rp_ncm")||sFlag.equalsIgnoreCase("emaila_rp_ncm")||sFlag.equalsIgnoreCase("fax_rp_ncm"))
        {
            if (myEO2.getNIM3_Encounter().getSentTo_RP_NCM().before( new java.util.Date(1)))
            {
                myEO2.getNIM3_Encounter().setSentTo_RP_NCM(PLCUtils.getNowDate(true));
                myEO2.getNIM3_Encounter().commitData();
            }
            sTargetRec = "["+ myEO2.getCase_NurseCaseManager().getContactEmail()+"] NCM Email["+ myEO2.getCase_NurseCaseManager().getContactLastName()+"]";
//            myEmail.setTo(Case_NurseCaseManager.getContactEmail());
            if (sFlag.equalsIgnoreCase("fax_rp_ncm"))
            {
                isFax = true;
//                myEmail.setTo(NIMUtils.fax2EmailFax(Case_NurseCaseManager.getContactFax()));
                sTargetRec = "["+ myEO2.getCase_NurseCaseManager().getContactFax()+"] NCM Email["+ myEO2.getCase_NurseCaseManager().getContactLastName()+"]";
            }
        }
        else if (sFlag.equalsIgnoreCase("email_rc_pt"))
        {
            if (myEO2.getNIM3_Encounter().getSentTo_ReqRec_Pt().before( new java.util.Date(1)))
            {
                myEO2.getNIM3_Encounter().setSentTo_ReqRec_Pt(PLCUtils.getNowDate(true));
                myEO2.getNIM3_Encounter().commitData();
            }
            sTargetRec = "["+ myEO2.getNIM3_CaseAccount().getPatientEmail()+"] Patient Email";

        }
        else if (sFlag.equalsIgnoreCase("email_sp_pt"))
        {
            if (myEO2.getNIM3_Encounter().getSentTo_SP_Pat().before( new java.util.Date(1)))
            {
                myEO2.getNIM3_Encounter().getSentTo_SP_Pat();
                myEO2.getNIM3_Encounter().commitData();
            }
            sTargetRec = "["+ myEO2.getNIM3_CaseAccount().getPatientEmail()+"] Patient Email["+ myEO2.getNIM3_CaseAccount().getPatientLastName()+"]";
        }
    }
    catch(Exception emailError1)
    {
        isSend=false;
        DebugLogger.printLine("NIMUtils.sendAlertEmail [" + emailError1.toString() + "]");
        myVal = ("NIMUtils.sendAlertEmail [" + emailError1.toString() + "]");
    }
    if (!isFailSend&&isSend)
    {     
        
    	if (sFlag.equalsIgnoreCase("fax_sp_ic")&&!myEmail.getVirtualAttachment().equalsIgnoreCase("")){
    		myEmail.setBodyType(emailType_V3.HTML_TYPE);
    	}
        
        Integer myIsSendTrackID = null;
        
//        if(isFax){
//        	myIsSendTrackID = myEmail.prepFaxageAPI();
//        } else{
//        	myIsSendTrackID = myEmail.isSendMail_Track();
//        }
        myIsSendTrackID = myEmail.isSendMail_Track();
        if (myIsSendTrackID!=null)
        {
            try
            {
                myVal = "1";
                bltNIM3_CommTrack myNCT = new bltNIM3_CommTrack();
                myNCT.setUniqueModifyComments("NIMUtils:sendAlertEmail:System Generated");
                myNCT.setCaseID(myEO2.getNIM3_CaseAccount().getCaseID());
                myNCT.setReferralID(myEO2.getNIM3_Referral().getReferralID());
                myNCT.setEncounterID(myEO2.getNIM3_Encounter().getEncounterID());
                myNCT.setCommStart(PLCUtils.getNowDate(false));
                myNCT.setCommEnd(PLCUtils.getNowDate(false));
                myNCT.setCommTypeID(new Integer(3));
                myNCT.setAlertStatusCode(new Integer(1));
                myNCT.setMessageName(sNIMUserName);
                if (isFax)
                {
                    myNCT.setMessageText("Fax ["+sFlag+"] Generated to " + sTargetRec);
                }
                else
                {
                    myNCT.setMessageText("Email ["+sFlag+"] Generated to " + sTargetRec);
                }
                bltNIM3_CommReference myNCR = new bltNIM3_CommReference();
                myNCR.setUniqueModifyComments("NIMUtils:sendAlertEmail:System Generated");
                myNCR.setCRTypeID(new Integer(1));  //1 = email   2 = fax email  3 = fax manual
                myNCR.setRefID(myIsSendTrackID);
                myNCR.commitData();
                myNCT.setCommReferenceID(myNCR.getUniqueID());
                myNCT.commitData();
            }
            catch (Exception eeeee)
            {
                DebugLogger.printLine("NIMUtils:sendAlertEmail:Error on Final Save ["+ eeeee +"]");
            }
        }
        //run status checker to see if case is done and should be moved to billing
        MoveToBillingIfReady(myEO2) ;
    }
    return myVal;
}


@SuppressWarnings("unused")
public static void MoveToBillingIfReady(NIM3_EncounterObject2 myEO2)
{
    boolean isReady = false;
    if(!myEO2.hasCritialInfomation()){
    	//Case is not ready for billing. Action is handled in NIM3_EncounterObject2.hasCritialInfomation()
    }
    else if (myEO2.getNIM3_Encounter().getReportFileID()!=0&&myEO2.getNIM3_Encounter().getTimeTrack_ReqRpReview().after(NIMUtils.getBeforeTime())&&myEO2.isScanPassAlertsComplete(3)&&myEO2.getNIM3_Encounter().getisCourtesy()!=1)
    {	
    	//JIRA SPBILLTWO-21 Request EMG no longer moved to hold-bill 2/4/13
        if (false){//myEO2.getNIM3_Encounter().getEncounterTypeID()==ENCOUNTER_TYPE_EMG && myEO2.getNIM3_Encounter().getBillingHCFA_FromProvider_FileID()==0){
            try {
                myEO2.getNIM3_Encounter().setEncounterStatusID(new Integer(7));
                myEO2.getNIM3_Encounter().commitData();
                ReCalcServiceBilling(myEO2.getNIM3_Encounter().getEncounterID());
                bltNIM3_CommTrack myCT = new bltNIM3_CommTrack();
                myCT.setUniqueModifyComments("NIMUtils:MoveToBillingIfReady:System Generated");
                myCT.setReferralID(myEO2.getNIM3_Referral().getReferralID());
                myCT.setEncounterID(myEO2.getNIM3_Encounter().getEncounterID());
                myCT.setCommStart(PLCUtils.getNowDate(false));
                myCT.setCommEnd(PLCUtils.getNowDate(false));
                myCT.setAlertStatusCode(new Integer(1));
                myCT.setMessageName("System Generated");
                myCT.setMessageText("Report Delivery appears complete. Encounter is type=EMG and Moved to Hold-Bill");
                myCT.commitData();
            } catch (Exception MoveError) {
                DebugLogger.printLine("NIMUtils:MoveToBillingIfReady:["+ MoveError+"]");
            }
        } else {
            try {
                myEO2.getNIM3_Encounter().setEncounterStatusID(new Integer(9));
                myEO2.getNIM3_Encounter().commitData();
                ReCalcServiceBilling(myEO2.getNIM3_Encounter().getEncounterID());
                bltNIM3_CommTrack myCT = new bltNIM3_CommTrack();
                myCT.setUniqueModifyComments("NIMUtils:MoveToBillingIfReady:System Generated");
                myCT.setReferralID(myEO2.getNIM3_Referral().getReferralID());
                myCT.setEncounterID(myEO2.getNIM3_Encounter().getEncounterID());
                myCT.setCommStart(PLCUtils.getNowDate(false));
                myCT.setCommEnd(PLCUtils.getNowDate(false));
                myCT.setAlertStatusCode(new Integer(1));
                myCT.setMessageName("System Generated");
                myCT.setMessageText("Report Delivery appears complete. Encounter Moved to Billing");
                myCT.commitData();
            } catch (Exception MoveError) {
                DebugLogger.printLine("NIMUtils:MoveToBillingIfReady:["+ MoveError+"]");
            }
        }
    }
    else if (myEO2.getNIM3_Encounter().getReportFileID().intValue()!=0&&myEO2.getNIM3_Encounter().getTimeTrack_ReqRpReview().after(NIMUtils.getBeforeTime())&&myEO2.isScanPassAlertsComplete(3)&&myEO2.getNIM3_Encounter().getisCourtesy()==1)
    {
        try
        {
            myEO2.getNIM3_Encounter().setEncounterStatusID(new Integer(6));
            myEO2.getNIM3_Encounter().commitData();
            bltNIM3_CommTrack myCT = new bltNIM3_CommTrack();
            myCT.setUniqueModifyComments("NIMUtils:MoveToBillingIfReady:System Generated");
            myCT.setReferralID(myEO2.getNIM3_Referral().getReferralID());
            myCT.setEncounterID(myEO2.getNIM3_Encounter().getEncounterID());
            myCT.setCommStart(PLCUtils.getNowDate(false));
            myCT.setCommEnd(PLCUtils.getNowDate(false));
            myCT.setAlertStatusCode(new Integer(1));
            myCT.setMessageName("System Generated");
            myCT.setMessageText("Report Delivery appears complete. Encounter Moved to Closed (Billing skipped since encounter is marked as Courtesy).");
            myCT.commitData();
        }
        catch (Exception MoveError)
        {
            DebugLogger.printLine("NIMUtils:MoveToBillingIfReady:["+ MoveError+"]");
        }
    }
}

public static boolean isPayerTyson(Integer iP)
{
    bltNIM3_PayerMaster myPM = new bltNIM3_PayerMaster(iP);
    boolean myVal = false;
    if (myPM.getPayerName().indexOf("Tyson")>=0)
    {
        myVal = true;
    }
    return myVal;

}




public static int getEncounterCount_ByReferralID(Integer iReferralID)
{
    int myVal = 0;
    searchDB2 mySS = new searchDB2();
    java.sql.ResultSet myRS = null;
    try
    {
        //not updated yet for Prepared Statements
        myRS = mySS.executeStatement("select count(*) as iCount from tNIM3_Referral INNER JOIN tNIM3_Encounter on (tNIM3_Encounter.ReferralID = tNIM3_Referral.ReferralID) where tNIM3_Encounter.EncounterStatusID in ("+NIMUtils.getSQL_Encounters_Active()+") and tNIM3_Referral.ReferralID=" + iReferralID );
        while(myRS!=null&&myRS.next())
        {
            myVal = new Integer(myRS.getString("iCount"));
        }
        mySS.closeAll();
    }
    catch (Exception eee)
    {
        myVal = -1;
    } finally {
        mySS.closeAll();
    }
    return myVal;


}

public static String getCounty (String myZIP){
    String myVal = "";
    searchDB2 mySS = new searchDB2();
    java.sql.ResultSet myRS = null;
    try
    {
        db_PreparedStatementObject myDPSO = new db_PreparedStatementObject("select county from zip_code where zip_code = ?" );
        myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,myZIP));
        myRS = mySS.executePreparedStatement(myDPSO);
        if (myRS!=null&&myRS.next())
        {
            myVal = myRS.getString("county");
        }
        mySS.closeAll();
    }
    catch (Exception eee)
    {
    } finally {
        mySS.closeAll();
    }
    return myVal;
}


public static void emailFatalError(String sDebugString)
{
	java.util.Date myD = new java.util.Date();
	emailType_V3 myEM = new emailType_V3();
	myEM.setTo("po.le@nextimagemedical.com");
	myEM.setFrom("server@nextimagemedical.com");
	myEM.setSubject("FATAL ERRROR ["+ConfigurationInformation.serverName+"]");
	myEM.setBody("FATAL ERROR\n----------\nDate: " + PLCUtils.getNow(false) + "\n----------\n[" + sDebugString + "]" );
	myEM.isSendMail();
//    myEM.setTo("scottellis42@hotmail.com");
//    myEM.isSendMail_Direct();
}




//start zip

/*******************************************************************************
 *                ZIP Code and Distance Claculation Class
 *******************************************************************************
 *      Author:     Micah Carrick
 *      Email:      email@micahcarrick.com
 *      Website:    http://www.micahcarrick.com
 *
 *      File:       zipcode.class.php
 *      Version:    1.2.0
 *      Copyright:  (c) 2005 - Micah Carrick 
 *                  You are free to use, distribute, and modify this software 
 *                  under the terms of the GNU General Public License.  See the
 *                  included license.txt file.
 *
 *******************************************************************************
 *  VERION HISTORY:
 *      v1.2.0 [Oct 22, 2006] - Using a completely new database based on user
                                contributions which resolves many data bugs.
                              - Added sorting to get_zips_in_range()
                              - Added ability to include/exclude the base zip
                                from get_zips_in_range()
                              
 *      v1.1.0 [Apr 30, 2005] - Added Jeff Bearer's code to make it MUCH faster!
 
 *      v1.0.1 [Apr 22, 2005] - Fixed a typo :)
 
 *      v1.0.0 [Apr 12, 2005] - Initial Version
 *
 *******************************************************************************
 *  DESCRIPTION:
 
 *    A PHP Class and MySQL table to find the distance between zip codes and 
 *    find all zip codes within a given mileage or kilometer range.
 *      
 *******************************************************************************
*/

/* constants for setting the $units data member
define('_UNIT_MILES', 'm');
define('_UNIT_KILOMETERS', 'k');

// constants for passing $sort to get_zips_in_range()
define('_ZIPS_SORT_BY_DISTANCE_ASC', 1);
define('_ZIPS_SORT_BY_DISTANCE_DESC', 2);
define('_ZIPS_SORT_BY_ZIP_ASC', 3);
define('_ZIPS_SORT_BY_ZIP_DESC', 4);

// constant for miles to kilometers conversion
define('_M2KM_FACTOR', 1.609344);
*/


   String slast_error = "";            // last error message set by this class
   int ilast_time = 0;              // last function execution time (debug info)
   String sunits = "m";        // miles or kilometers
   int idecimals = 2;               // decimal places for returned distance

   public static double get_distance(String zip1, String zip2) 
   {
	  double[] myZIP1 = new double[2];
	  double[] myZIP2 = new double[2];
      double myVal = 0.0;
	  // returns the distance between to zip codes.  If there is an error, the 
      // function will return false and set the $last_error variable.
      
      //$this->chronometer();         // start the clock
      
      if (zip1.equalsIgnoreCase(zip2)) 
	  {
	  	myVal = 0.0; // same zip code means 0 miles between. :)
	  }
   
   
      // get details from database about each zip and exit if there is an error
      
      // calculate the distance between the two points based on the lattitude
      // and longitude pulled out of the database.
	  myZIP1 = get_zip_point(zip1);
	  myZIP2 = get_zip_point(zip2);
	  
      myVal = calculate_mileage(myZIP1[0],myZIP2[0],myZIP1[1],myZIP2[1]);
	  return myVal;
      
   }   

/*   function get_zip_details($zip) {
      
      // This function pulls the details from the database for a 
      // given zip code.
 
      $sql = "SELECT lat AS lattitude, lon AS longitude, city, county, state_prefix, 
              state_name, area_code, time_zone
              FROM zip_code 
              WHERE zip_code='$zip'";
              
      $r = mysql_query($sql);
      if (!$r) {
         $this->last_error = mysql_error();
         return false;
      } else {
         $row = mysql_fetch_array($r, MYSQL_ASSOC);
         mysql_free_result($r);
         return $row;       
      }
   }
*/
   public static double[] get_zip_point(String sZip) {
   
      // This function pulls just the lattitude and longitude from the
      // database for a given zip code.
      	double[] myVal = new double[2];
		myVal[0]=-1;
		myVal[1]=-1;
		searchDB2 mySS = new searchDB2();
		java.sql.ResultSet myRS = null;
		try
		{
			//myRS = mySS.executeStatement("SELECT lat, lon from zip_code WHERE zip_code='" + DataControlUtils.fixApostrophe(sZip) + "'");
			String mySQL = "SELECT lat, lon from " + DB_TABLE_ZIPCODE + " WHERE zip_code=?";
            db_PreparedStatementObject myDPSO = new db_PreparedStatementObject(mySQL);
            myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,sZip));
            myRS = mySS.executePreparedStatement(myDPSO);

			if(myRS!=null&&myRS.next())
			{
				myVal[0] = (new Double (myRS.getString("lat"))).doubleValue();
				myVal[1] = (new Double (myRS.getString("lon"))).doubleValue();
			}
            mySS.closeAll();

		}
		catch(Exception e)
		{
        } finally {
            mySS.closeAll();
        }
		return myVal;
   }

   public static double calculate_mileage(double lat1, double lat2, double lon1, double lon2) {
 
      // used internally, this function actually performs that calculation to
      // determine the mileage between 2 points defined by lattitude and
      // longitude coordinates.  This calculation is based on the code found
      // at http://www.cryptnet.net/fsp/zipdy/
       
      // Convert lattitude/longitude (degrees) to radians for calculations
      lat1 = Math.toRadians(lat1);
      lon1 = Math.toRadians(lon1);
      lat2 = Math.toRadians(lat2);
      lon2 = Math.toRadians(lon2);
      
      // Find the deltas
      double delta_lat = lat2 - lat1;
      double delta_lon = lon2 - lon1;
	
      // Find the Great Circle distance 
      double temp = Math.pow(Math.sin(delta_lat/2.0),2) + Math.cos(lat1) * Math.cos(lat2) * Math.pow(Math.sin(delta_lon/2.0),2);
      double distance = 3956 * 2 * Math.atan2(Math.sqrt(temp),Math.sqrt(1-temp));

      return distance;
   }


   public static com.winstaff.ZIP_CODE_object get_zips_in_range(String in_zip, double range)
   {
      // returns an array of the zip codes within $range of $zip. Returns
      // an array with keys as zip codes and values as the distance from 
      // the zipcode defined in $zip.
      
      // This portion of the routine  calculates the minimum and maximum lat and
      // long within a given range.  This portion of the code was written
      // by Jeff Bearer (http://www.jeffbearer.com). This significanly decreases
      // the time it takes to execute a query.  My demo took 3.2 seconds in 
      // v1.0.0 and now executes in 0.4 seconds!  Greate job Jeff!
      String zip = in_zip.trim();
      if (zip.length()>5)
      {
          zip = zip.substring(0,5);
      }

	  double[] myZIP1 = new double[2];
	  myZIP1 = get_zip_point(zip);


      
      // Find Max - Min Lat / Long for Radius and zero point and query
      // only zips in that range.
      double lat_range = range/69.172;
      double lon_range = Math.abs(range/(Math.cos(myZIP1[0]) * 69.172));
	  java.text.DecimalFormat myDF = new java.text.DecimalFormat("#.0000");
      String min_lat = myDF.format(myZIP1[0] - lat_range);
      String max_lat = myDF.format(myZIP1[0] + lat_range);
      String min_lon = myDF.format(myZIP1[1] - lon_range);
      String max_lon = myDF.format(myZIP1[1] + lon_range);
	  
//	  java.util.Vector myVal = new java.util.Vector();
//      java.util.Vector[] myVal = new java.util.Vector[2];
	  com.winstaff.ZIP_CODE_object myVal = new com.winstaff.ZIP_CODE_object();
//	  java.util.TreeMap myVal = new java.util.TreeMap();

		searchDB2 mySS = new searchDB2();
		java.sql.ResultSet myRS = null;
		try
		{
			//not yet updated for prepared statements
            myRS = mySS.executeStatement("SELECT zip_code, lat, lon FROM " + DB_TABLE_ZIPCODE + " WHERE lat BETWEEN '" + min_lat + "' AND '" + max_lat + "' AND lon BETWEEN '" + min_lon + "' AND '" + max_lon + "'");
			while(myRS!=null&&myRS.next())
			{
				double distance = calculate_mileage(myZIP1[0],(new Double (myRS.getString("lat"))).doubleValue(),myZIP1[1],(new Double (myRS.getString("lon"))).doubleValue());
				if (distance<=range)
				{
					myVal.tmZip.put(myRS.getString("zip_code"),new Double(distance));
//					myVal[0].add(myRS.getString("zip_code"));
//					myVal[1].add(distance);
				}
			}
            mySS.closeAll();
		}
		catch(Exception e)
		{
//			out.println("ResultsSet:"+e);
        } finally {
            mySS.closeAll();
        }
		return  myVal;
   }

//end zip

    public static com.winstaff.GEOUtils.Location getLatLon(String inZip)
    {
        String Zip = inZip.trim();
        com.winstaff.GEOUtils.Location GULoc = new com.winstaff.GEOUtils.Location() ;
        searchDB2 mySS = new searchDB2();
        java.sql.ResultSet myRS = null;
        try
        {
            //myRS = mySS.executeStatement("select lat, lon from \"GEOCODES\" where UPPER(address) = UPPER('" + DataControlUtils.fixApostrophe(sGCAddress) + "')");
            String mySQL = "select lat, lon from " + DB_TABLE_ZIPCODE + " where zip_code = ?";
            db_PreparedStatementObject myDPSO = new db_PreparedStatementObject(mySQL);
            String mySearchZip = Zip;
            if (mySearchZip.length()>5)
            {
                mySearchZip = mySearchZip.substring(0,5);
            }
            myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,mySearchZip));
            myRS = mySS.executePreparedStatement(myDPSO);


            if(myRS!=null&&myRS.next())
            {
                GULoc.lat = myRS.getString("lat");
                GULoc.lon = myRS.getString("lon");
            }
            else
            {
                DebugLogger.println ("NIMUtils:getLatLon:ZIP ONLY: INVALID ZIP["+Zip+"]:" );
            }
            mySS.closeAll();

        }
        catch(Exception e)
        {
//			out.println("ResultsSet:"+e);
            DebugLogger.println ("NIMUtils:getLatLon:Address["+Zip+"]:" + e);

        } finally {
            mySS.closeAll();
        }
        return GULoc;

    }




	public static com.winstaff.GEOUtils.Location getLatLonFull(String sGCAddress)
	{
		com.winstaff.GEOUtils.Location GULoc = new com.winstaff.GEOUtils.Location() ;
		searchDB2 mySS = new searchDB2();
		java.sql.ResultSet myRS = null;
		try
		{
			//myRS = mySS.executeStatement("select lat, lon from \"GEOCODES\" where UPPER(address) = UPPER('" + DataControlUtils.fixApostrophe(sGCAddress) + "')");
            String mySQL = "select lat, lon from \"GEOCODES\" where UPPER(address) = UPPER(?)";
            db_PreparedStatementObject myDPSO = new db_PreparedStatementObject(mySQL);
            myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,sGCAddress));
            myRS = mySS.executePreparedStatement(myDPSO);


			if(myRS!=null&&myRS.next())
			{
				GULoc.lat = myRS.getString("lat");
				GULoc.lon = myRS.getString("lon");
			}
			else
			{
                try
                {
                    com.winstaff.GEOUtils myGU = new com.winstaff.GEOUtils();
                    GULoc = myGU.getLocation(sGCAddress);
                    myGU.cacheLocation(sGCAddress, GULoc.lat,GULoc.lon);
                }
                catch(Exception e)
                {
                    DebugLogger.println ("NIMUtils:getLatLon:Address - error from Google?["+sGCAddress+"]:" + e);
                    //use latlon from zip
//                    String mySQL = "select lat, lon from \"GEOCODES\" where UPPER(address) = UPPER(?)";
//                    db_PreparedStatementObject myDPSO = new db_PreparedStatementObject(mySQL);
//                    myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,sGCAddress));
//                    myRS = mySS.executePreparedStatement(myDPSO);

                }

			}
            mySS.closeAll();
		}
        catch(Exception e)
        {
//			out.println("ResultsSet:"+e);
            DebugLogger.println ("NIMUtils:getLatLon:Address["+sGCAddress+"]:" + e);

        } finally {
            mySS.closeAll();
        }
		return GULoc;
		
	}

    
    public static java.util.Date getCurrentTimeByUser(Integer iUserID)
    {
        //not done yet
        return new java.util.Date();
    }

    public static java.util.Date getBeforeTime()
    {
        java.util.Calendar myCal = java.util.Calendar.getInstance();
        //myCal.set(1805,1,2);
        myCal.set(1800,0,1,20,0);
        return myCal.getTime();
    }



    public static String getSQL_Encounters_Active()   //TODO: Merge with new method below
    {
        return "'0','1','4','6','7','8','9','11','12'";
    }

    public static String getSQL_Encounters_Active(boolean includeClose, boolean includeBilling)
    {
        if (includeClose&&includeBilling){
            return "'0','1','4','6','7','8','9','11','12'";
        }
        else if (includeBilling) {
            return "'0','1','4','7','8','9','11','12'";
        }
        else if (includeClose) {
            return "'0','1','4','6','7','8','11','12'";
        }
        else  {
            return "'0','1','4','7','8','11','12'";
        }
    }

    public static boolean getEncounterIsActive(bltNIM3_Encounter myE)
    {
        boolean myVal = false;
        if (myE.getEncounterStatusID().intValue()==0||myE.getEncounterStatusID().intValue()==1||myE.getEncounterStatusID().intValue()==4||myE.getEncounterStatusID().intValue()==7||myE.getEncounterStatusID().intValue()==8||myE.getEncounterStatusID().intValue()==9||myE.getEncounterStatusID().intValue()==11||myE.getEncounterStatusID().intValue()==12)
        {
            myVal=true;
        }
        return myVal;
    }

    public static java.util.Vector getReporting_UserActivity (bltUserAccount UserAccount, int YearCap){
        HashMap<String,NIM3_Reporting_UserActivity> myVal_Hash = new HashMap<String,NIM3_Reporting_UserActivity>();
        for (int i=1;i<=12;i++){
            NIM3_Reporting_UserActivity tempRUA = new NIM3_Reporting_UserActivity(2011,i) ;
            myVal_Hash.put((201100 + i)+"", tempRUA);
        }

        searchDB2 mySSAct = new searchDB2();
        java.sql.ResultSet myRSAct = null;;
        try
        {
            String mySQL55 = "None";
            if (UserAccount.getAccountType().equalsIgnoreCase("AdjusterID")||UserAccount.getAccountType().equalsIgnoreCase("AdjusterID2")||UserAccount.getAccountType().equalsIgnoreCase("AdjusterID3"))
            {
                mySQL55 = ("SELECT\n" +
                        "\"public\".\"vMQ4_Encounter_Void_BP\".\"UA_Adjuster_UserID\",\n" +
                        "\"public\".\"vMQ4_Encounter_Void_BP\".\"Referral_ReceiveDate_iYear\",\n" +
                        "\"public\".\"vMQ4_Encounter_Void_BP\".\"Referral_ReceiveDate_iMonth\",\n" +
                        "\"public\".\"vMQ4_Encounter_Void_BP\".\"Encounter_StatusID\",\n" +
                        "\"public\".\"vMQ4_Encounter_Void_BP\".\"Encounter_Status\",\n" +
                        "\"public\".\"vMQ4_Encounter_Void_BP\".\"Encounter_Type\",\n" +
                        "\"public\".\"vMQ4_Encounter_Void_BP\".\"Encounter_IsCourtesy\",\n" +
                        "Count(\"public\".\"vMQ4_Encounter_Void_BP\".\"Encounter_ScanPass\") AS icount\n" +
                        "FROM \"public\".\"vMQ4_Encounter_Void_BP\"\n" +
                        "where  \"public\".\"vMQ4_Encounter_Void_BP\".\"UA_Adjuster_UserID\" = "+UserAccount.getUserID()+" AND \"public\".\"vMQ4_Encounter_Void_BP\".\"Referral_ReceiveDate_iYear\"> 2010\n" +
                        "GROUP BY \"public\".\"vMQ4_Encounter_Void_BP\".\"UA_Adjuster_UserID\", \"public\".\"vMQ4_Encounter_Void_BP\".\"Referral_ReceiveDate_iMonth\", \"public\".\"vMQ4_Encounter_Void_BP\".\"Referral_ReceiveDate_iYear\", \"public\".\"vMQ4_Encounter_Void_BP\".\"Encounter_StatusID\", \"public\".\"vMQ4_Encounter_Void_BP\".\"Encounter_Status\", \"public\".\"vMQ4_Encounter_Void_BP\".\"Encounter_Type\", \"public\".\"vMQ4_Encounter_Void_BP\".\"Encounter_IsCourtesy\"\n" +
                        "\n" +
                        "ORDER BY \"public\".\"vMQ4_Encounter_Void_BP\".\"UA_Adjuster_UserID\", \"public\".\"vMQ4_Encounter_Void_BP\".\"Referral_ReceiveDate_iYear\", \"public\".\"vMQ4_Encounter_Void_BP\".\"Referral_ReceiveDate_iMonth\", \"public\".\"vMQ4_Encounter_Void_BP\".\"Encounter_Status\", \"public\".\"vMQ4_Encounter_Void_BP\".\"Encounter_Type\"");
            }
            else if (UserAccount.getAccountType().equalsIgnoreCase("PhysicianID"))
            {
                mySQL55 = ("SELECT\n" +
                        "\"public\".\"vMQ4_Encounter_Void_BP\".\"UA_ReferringDr_UserID\",\n" +
                        "\"public\".\"vMQ4_Encounter_Void_BP\".\"Referral_ReceiveDate_iYear\",\n" +
                        "\"public\".\"vMQ4_Encounter_Void_BP\".\"Referral_ReceiveDate_iMonth\",\n" +
                        "\"public\".\"vMQ4_Encounter_Void_BP\".\"Encounter_StatusID\",\n" +
                        "\"public\".\"vMQ4_Encounter_Void_BP\".\"Encounter_Status\",\n" +
                        "\"public\".\"vMQ4_Encounter_Void_BP\".\"Encounter_Type\",\n" +
                        "\"public\".\"vMQ4_Encounter_Void_BP\".\"Encounter_IsCourtesy\",\n" +
                        "Count(\"public\".\"vMQ4_Encounter_Void_BP\".\"Encounter_ScanPass\") AS icount\n" +
                        "FROM \"public\".\"vMQ4_Encounter_Void_BP\"\n" +
                        "where  \"public\".\"vMQ4_Encounter_Void_BP\".\"UA_ReferringDr_UserID\" = "+UserAccount.getUserID()+"\n" +
                        "GROUP BY \"public\".\"vMQ4_Encounter_Void_BP\".\"UA_ReferringDr_UserID\", \"public\".\"vMQ4_Encounter_Void_BP\".\"Referral_ReceiveDate_iMonth\", \"public\".\"vMQ4_Encounter_Void_BP\".\"Referral_ReceiveDate_iYear\", \"public\".\"vMQ4_Encounter_Void_BP\".\"Encounter_StatusID\", \"public\".\"vMQ4_Encounter_Void_BP\".\"Encounter_Status\", \"public\".\"vMQ4_Encounter_Void_BP\".\"Encounter_Type\", \"public\".\"vMQ4_Encounter_Void_BP\".\"Encounter_IsCourtesy\"\n" +
                        "\n" +
                        "ORDER BY \"public\".\"vMQ4_Encounter_Void_BP\".\"UA_ReferringDr_UserID\", \"public\".\"vMQ4_Encounter_Void_BP\".\"Referral_ReceiveDate_iYear\", \"public\".\"vMQ4_Encounter_Void_BP\".\"Referral_ReceiveDate_iMonth\", \"public\".\"vMQ4_Encounter_Void_BP\".\"Encounter_Status\", \"public\".\"vMQ4_Encounter_Void_BP\".\"Encounter_Type\"");
            }
    //		out.println(mySQL55);
            if (mySQL55.equalsIgnoreCase("None"))
            {
            }
            else
            {
                myRSAct = mySSAct.executeStatement(mySQL55);
            }
        }
        catch(Exception e)
        {
        }
	    try
        {
            if (myRSAct!=null)
            {
                while (myRSAct!=null&&myRSAct.next())
                {
                    try
                    {
                        DebugLogger.printLine("*****||*****");
                        NIM3_Reporting_UserActivity myNRUA;
                        int tempMonth = new Integer(myRSAct.getString("Referral_ReceiveDate_iMonth"));
                        int tempYear = new Integer(myRSAct.getString("Referral_ReceiveDate_iYear"));
                        int tempCount = new Integer(myRSAct.getString("icount"));
                        int tempCourtesy = new Integer(myRSAct.getString("Encounter_IsCourtesy"));
                        String tempType = myRSAct.getString("Encounter_Type");
                        String tempStatus = myRSAct.getString("Encounter_Status");
                        String tempKey = ((tempYear*100)+tempMonth) + "";
                        if (myVal_Hash.containsKey(tempKey)){
                            myNRUA = myVal_Hash.get(tempKey);
                        } else {
                            myNRUA = new NIM3_Reporting_UserActivity();
                        }
                        myNRUA.setMonth(tempMonth);
                        myNRUA.setYear(tempYear);
                        if (tempStatus.indexOf("Void")>=0){
                            myNRUA.addCountVoid(tempCount);
                        } else if (tempCourtesy==1){
                            myNRUA.addCountCourtesy(tempCount);
                        } else if (tempType.equalsIgnoreCase("mr")){
                            myNRUA.addCountMR(tempCount);
                        } else if (tempType.equalsIgnoreCase("ct")){
                            myNRUA.addCountCT(tempCount);
                        } else if (tempType.equalsIgnoreCase("emg")){
                            myNRUA.addCountEMG(tempCount);
                        } else {
                            myNRUA.addCountOther(tempCount);
                        }
                        if (tempYear>=YearCap){
                            myVal_Hash.put(tempKey,myNRUA);
                        }
                    }
                    catch(NumberFormatException e)
                    {
                    }
                }
            }
            else
            {
                DebugLogger.println("No Results");
            }
            mySSAct.closeAll();
        }
        catch(Exception e)
        {
            DebugLogger.println("ResultsSet:"+e);
        } finally {
            mySSAct.closeAll();
        }
        Vector<Double> vSort = null;
        java.util.Iterator itSort = null;
        vSort = new java.util.Vector(myVal_Hash.keySet());
        java.util.Comparator comparator = java.util.Collections.reverseOrder();
        java.util.Collections.sort(vSort,comparator);
        return HashMapToVector(myVal_Hash, vSort, 1000);
    }







    public static java.util.Vector getCaseLoad()
    {
        Hashtable<String, NIM_CaseLoadObject> myVal = new Hashtable();
        java.util.Vector<Integer> myEncounters = NIMUtils.getWorklist(NIMUtils.WORKLIST_CASE_LOAD,"");
        for (Integer iEncoutnerID: myEncounters)
        {
            NIM3_EncounterObject2 myEO2 = new NIM3_EncounterObject2(iEncoutnerID,"CaseLoad");
            NIM_CaseLoadObject myCLO;
            if (myVal.containsKey(myEO2.getCase_AssignedTo().getLogonUserName()))
            {
                myCLO = myVal.get(myEO2.getCase_AssignedTo().getLogonUserName());
                DebugLogger.printLine("Match");
            }
            else
            {
                myCLO = new NIM_CaseLoadObject(myEO2.getCase_AssignedTo().getContactFirstName() + " " + myEO2.getCase_AssignedTo().getContactLastName());
            }
            myCLO.totalActive++;
            //DebugLogger.printLine("****"+ myCLO.getName()  + " = " + myCLO.totalActive);
            if (myEO2.getNIM3_Encounter().getAppointmentID()>0)
            {
                if (myEO2.getNIM3_Appointment().getAppointmentTime().before(PLCUtils.getToday()))
                {
                    myCLO.totalAfterAppt++;
                }
            }
            else
            {
                myCLO.totalNotScheduled++;
                if (workingDays.getWorkingHours(myEO2.getNIM3_Referral().getReceiveDate(),PLCUtils.getToday())>24)
                {
                    myCLO.totalPastDue++;
                }
            }
            myVal.put(myEO2.getCase_AssignedTo().getLogonUserName(),myCLO);
        }

        Vector<String> vZIP = null;
        java.util.Iterator itZIP = null;
        vZIP = new java.util.Vector(myVal.keySet());
//        java.util.Comparator comparator = java.util.Collections.reverseOrder();
        java.util.Collections.sort(vZIP);
        return HashToVector(myVal, vZIP, 200);
    }

    public static java.util.Vector<Integer> getWorklist(int worklistType, String addWhere)
    {
        java.util.Vector<Integer> myVal = new java.util.Vector();
        searchDB2 mySS = new searchDB2();
        java.sql.ResultSet myRS = null;
        try
        {
            String mySQL = "null";
            if (worklistType==NIMUtils.WORKLIST_NAD)
            {
			    mySQL = "select encounterid from tNIM3_Encounter INNER JOIN tNIM3_Referral on tNIM3_Referral.referralid = tNIM3_Encounter.referralid INNER JOIN tNIM3_CaseAccount on tNIM3_CaseAccount.caseid = tNIM3_Referral.caseid where Date(tNIM3_Encounter.NextActionDate)<=Date(Now()) and tNIM3_Encounter.NextActionDate>'2000-01-01' " + addWhere + " AND tNIM3_Encounter.encounterstatusid in (" + NIMUtils.getSQL_Encounters_Active(false, true) + ")  order by tNIM3_Encounter.NextActionDate";
            }
            else if (worklistType==NIMUtils.WORKLIST_CASE_LOAD)
            {
			    //11/30: remove Hold Bills
                mySQL = "SELECT \"public\".\"vMQ4_Services_NoVoid_BP\".\"EncounterID\" FROM \"public\".\"vMQ4_Services_NoVoid_BP\" where  \"public\".\"vMQ4_Services_NoVoid_BP\".\"Encounter_StatusID\" in ('0','1','4','8','11','12')";
            }
            else if (worklistType==NIMUtils.WORKLIST_REPORTS_DUE)
            {
//			    mySQL = "select encounterid from tNIM3_Encounter INNER JOIN tNIM3_Referral on tNIM3_Referral.referralid = tNIM3_Encounter.referralid INNER JOIN tNIM3_CaseAccount on tNIM3_CaseAccount.caseid = tNIM3_Referral.caseid INNER JOIN tNIM3_Appointment on tNIM3_Appointment.appointmentid = tNIM3_Encounter.appointmentid where tNIM3_Encounter.AppointmentID>0  AND  ( ( (tNIM3_Appointment.appointmenttime + interval '2 hours')<Now() AND (tNIM3_Encounter.ReportFileID=0 OR tNIM3_Encounter.SentToRefDr<'2000-01-01' OR tNIM3_Encounter.SentToAdj<'2000-01-01' or (tNIM3_Encounter.SentToAdm<'2000-01-01' AND tNIM3_CaseAccount.CaseAdministratorID>0) or (tNIM3_Encounter.SentTo_RP_NCM<'2000-01-01' AND tNIM3_CaseAccount.NurseCaseManagerID>0)  ) ) OR  ( (tNIM3_Appointment.appointmenttime + interval '2 hours')<Now() AND (tNIM3_Encounter.ReportFileID=0 OR tNIM3_Encounter.SentTo_SP_RefDr<'2000-01-01' OR tNIM3_Encounter.SentTo_SP_Adj<'2000-01-01' or (tNIM3_Encounter.SentTo_SP_Adm<'2000-01-01' AND tNIM3_CaseAccount.CaseAdministratorID>0) or (tNIM3_Encounter.SentTo_SP_NCM<'2000-01-01' AND tNIM3_CaseAccount.NurseCaseManagerID>0)  ) )     )  " + addWhere + "  AND tNIM3_Encounter.encounterstatusid in (" + NIMUtils.getSQL_Encounters_Active() + ")  order by AppointmentTime";
			    mySQL = "select encounterid from tNIM3_Encounter INNER JOIN tNIM3_Referral on tNIM3_Referral.referralid = tNIM3_Encounter.referralid INNER JOIN tNIM3_CaseAccount on tNIM3_CaseAccount.caseid = tNIM3_Referral.caseid INNER JOIN tNIM3_Appointment on tNIM3_Appointment.appointmentid = tNIM3_Encounter.appointmentid   " +
                        "where (tNIM3_Encounter.AppointmentID>0 or tnim3_encounter.agecommentlock=1)  AND  " +
                        "(  (   ( tNIM3_Encounter.ReportFileID=0 OR  tNIM3_Encounter.SentToRefDr<'2000-01-01' OR tNIM3_Encounter.SentToAdj<'2000-01-01' or  (tNIM3_Encounter.SentToAdm<'2000-01-01' AND tNIM3_CaseAccount.CaseAdministratorID>0)  OR (tNIM3_Encounter.SentTo_RP_Adm2<'2000-01-01' AND tNIM3_CaseAccount.CaseAdministrator2ID>0)  OR (tNIM3_Encounter.SentTo_RP_Adm3<'2000-01-01' AND tNIM3_CaseAccount.CaseAdministrator3ID>0)  OR (tNIM3_Encounter.SentTo_RP_Adm4<'2000-01-01' AND tNIM3_CaseAccount.CaseAdministrator4ID>0)  OR (tNIM3_Encounter.SentTo_RP_NCM<'2000-01-01' AND tNIM3_CaseAccount.NurseCaseManagerID>0) )  )  " +
                        " OR " +
                        "( ( tNIM3_Encounter.SentTo_SP_RefDr<'2000-01-01' OR tNIM3_Encounter.SentTo_SP_Adj<'2000-01-01'  OR tNIM3_Encounter.SentTo_SP_IC<'2000-01-01'  or  (tNIM3_Encounter.SentTo_SP_Adm<'2000-01-01' AND tNIM3_CaseAccount.CaseAdministratorID>0) or (tNIM3_Encounter.SentTo_SP_Adm2<'2000-01-01' AND tNIM3_CaseAccount.CaseAdministrator2ID>0) or (tNIM3_Encounter.SentTo_SP_Adm3<'2000-01-01' AND tNIM3_CaseAccount.CaseAdministrator3ID>0) or (tNIM3_Encounter.SentTo_SP_Adm4<'2000-01-01' AND tNIM3_CaseAccount.CaseAdministrator4ID>0) or  (tNIM3_Encounter.SentTo_SP_NCM<'2000-01-01' AND tNIM3_CaseAccount.NurseCaseManagerID>0)  )  ))" +
                        "" + addWhere + "  AND tNIM3_Encounter.encounterstatusid in (" + NIMUtils.getSQL_Encounters_Active(false, true) + ") AND tNIM3_Encounter.encounterstatusid not in (6)  order by AppointmentTime";

            }
            else if (worklistType==NIMUtils.WORKLIST_NOT_SCHEDULED)
            {
                mySQL = "select encounterid from tNIM3_Encounter INNER JOIN tNIM3_Referral on tNIM3_Referral.referralid = tNIM3_Encounter.referralid INNER JOIN tNIM3_CaseAccount on tNIM3_CaseAccount.caseid = tNIM3_Referral.caseid where tNIM3_Encounter.AppointmentID=0 " + addWhere + " AND tNIM3_Encounter.encounterstatusid in (" + NIMUtils.getSQL_Encounters_Active(false, true) + ")  order by tNIM3_Referral.ReceiveDate";
            }
            else if (worklistType==NIMUtils.WORKLIST_RX_REVIEW)
            {
                mySQL = "select encounterid from tNIM3_Encounter INNER JOIN tNIM3_Referral on tNIM3_Referral.referralid = tNIM3_Encounter.referralid INNER JOIN tNIM3_CaseAccount on tNIM3_CaseAccount.caseid = tNIM3_Referral.caseid where tNIM3_Encounter.TimeTrack_ReqRxReview<'2000-01-01' AND tNIM3_Referral.RxFileID>0 " + addWhere + " AND tNIM3_Encounter.encounterstatusid in (" + NIMUtils.getSQL_Encounters_Active(false, true) + ") order by tNIM3_Referral.ReceiveDate";
            }
            else if (worklistType==NIMUtils.WORKLIST_COMMTRACK)
            {
			    mySQL = "select commTrackID from tNIM3_CommTrack where AlertStatusCode=0  AND CommTypeID<>4  order by CommStart";
            }
            myRS = mySS.executeStatement(mySQL);
        }
        catch(Exception e)
        {
            DebugLogger.println("NIMUtils:getWorklist:ResultsSet:"+e);
        }
        try
        {
            int cnt=0;
            while (myRS!=null&&myRS.next())
            {
                cnt++;
                if (worklistType==NIMUtils.WORKLIST_COMMTRACK)
                {
                    Integer iCommTrackID = new Integer(myRS.getString("commtrackid"));
                    myVal.add(iCommTrackID);
                }
                else
                {
                    Integer iEncounterID = new Integer(myRS.getString("encounterid"));
                    myVal.add(iEncounterID);
                }

            }
            mySS.closeAll();
        }
        catch(Exception e)
        {
            DebugLogger.println("NIMUtils:getWorklist:ResultsSet:"+e);
        } finally {
            mySS.closeAll();
        }
        return myVal;
    }

    public static boolean isValidNewUser(bltUserAccount testUA)
    {
        boolean myVal = false;
        searchDB2 mySS = new searchDB2();
        java.sql.ResultSet myRS = null;
        if (testUA.getLogonUserName().equalsIgnoreCase("declined"))
        {
            myVal = true;
        }
        else
        {
            try
            {
                //myRS = mySS.executeStatement("select lat, lon from \"GEOCODES\" where UPPER(address) = UPPER('" + DataControlUtils.fixApostrophe(sGCAddress) + "')");
                String mySQL = "select userid from tUserAccount where logonusername = ?";
                db_PreparedStatementObject myDPSO = new db_PreparedStatementObject(mySQL);
                myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,testUA.getLogonUserName()));
                myRS = mySS.executePreparedStatement(myDPSO);
                if(myRS!=null&&myRS.next())
                {
                    myVal = false;
                }
                else
                {
                    myVal = true;
                }
                mySS.closeAll();
            }
            catch(Exception e)
            {
                DebugLogger.println ("NIMUtils:isValidNewUser["+testUA.getLogonUserName()+"]:" + e);

            } finally {
                mySS.closeAll();
            }
        }
        return myVal;
    }

    public static int WORKLIST_NAD = 1;
    public static int WORKLIST_CASE_LOAD = 6;
    public static int WORKLIST_REPORTS_DUE = 2;
    public static int WORKLIST_NOT_SCHEDULED = 3;
    public static int WORKLIST_RX_REVIEW = 4;
    public static int WORKLIST_COMMTRACK = 5;

//Defines NIM User Alert Preferences

    public static int ALERT_PREF_SECURE_EMAIL = 2;
    public static int ALERT_PREF_EMAIL_ATTACHMENT = 3;
    public static int ALERT_PREF_FAX = 4;
    public static int ALERT_PREF_PHONE = 5;
    public static int ALERT_PREF_MAIL = 6;
    public static int ALERT_PREF_NO_ALERTS = 1;

   //defines NIM Encounter Types
    public static int ENCOUNTER_TYPE_NONE = 0;
    public static int ENCOUNTER_TYPE_DIAG_OTHER = 9;
    public static int ENCOUNTER_TYPE_OTHER_SINGLE = 1;
    public static int ENCOUNTER_TYPE_MR = 4;
    public static int ENCOUNTER_TYPE_CT = 5;   
    public static int ENCOUNTER_TYPE_EMG = 6;


    public static int CASE_USER_ADJUSTER = 1;
    public static int CASE_USER_ADMIN= 2;
    public static int CASE_USER_ADMIN2= 5;
    public static int CASE_USER_ADMIN3= 6;
    public static int CASE_USER_ADMIN4= 7;
    public static int CASE_USER_REFERRING_DOCTOR= 3;
    public static int CASE_USER_NURSE_CASE_MANAGER = 4;


    public static int EncounterBypassRXID=4835;

    public static String DB_TABLE_ZIPCODE = "zip_code";

	//{{DECLARE_CONTROLS
	//}}
}
