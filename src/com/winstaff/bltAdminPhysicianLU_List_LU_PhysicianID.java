package com.winstaff;
/*
 * bltAdminPhysicianLU_List_LU_PhysicianID.java
 *
 * Created: Wed Apr 02 15:08:54 PST 2003
 */



import java.sql.*;
import com.winstaff.dbtAdminPhysicianLU;
import com.winstaff.ListElement;


/**
 *
 * @author  Scott Ellis via listClassGenCode
 * @version 2.5
 */
public class bltAdminPhysicianLU_List_LU_PhysicianID extends db_BaseRelationTable
{
    /** Default constructor
    */    
    public bltAdminPhysicianLU_List_LU_PhysicianID ( Integer iPhysicianID )
    {
        super( "tAdminPhysicianLU", "LookupID", "LookupID", "PhysicianID", iPhysicianID );
    }   // End of bltAdminPhysicianLU_List_LU_PhysicianID()
    

    public void setField(String fieldN, Object fieldV)
    {DebugLogger.println("setField Method not implemented yet in classbltAdminPhysicianLU_List_LU_PhysicianID!");}

    public String getFieldType(String fieldN)
    {return null;}

    public bltAdminPhysicianLU_List_LU_PhysicianID ( Integer iPhysicianID, String extraWhere, String OrderBy )
    {
        super( "tAdminPhysicianLU", "LookupID", "LookupID", "PhysicianID", iPhysicianID, extraWhere,OrderBy );
    }   // End of bltAdminPhysicianLU_List_LU_PhysicianID()
     /** Method pull information from the result set and load into class attributes.
    * @param sFieldList Comma delimited list of fields for which data has been retrieved.
    * @param rs Result set from which to pull the data from.
    * @throws SQLException Passes on SQLException
    */      
    protected void transferData( ResultSet rs ) throws SQLException
    {
        while ( rs.next()  )
        {
            Integer         iValue              = new Integer( rs.getString( sLinkFieldName ) );
            Integer         iLinkRecordID       = new Integer( rs.getString( sUniqueIDFieldName ) );
            Object          oReferenceObject;
            ListElement     leNewElement;
            
            oReferenceObject    = new bltAdminPhysicianLU( iValue );
            
            leNewElement        = new ListElement( iLinkRecordID, oReferenceObject );
            
            vList.add( leNewElement );

        }
    }   // End of transferData()
}   // End of bltAdminPhysicianLU_List_LU_PhysicianID class

