package com.winstaff;


import com.winstaff.dbtPhysicianPracticeLU;
import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public interface InttPhysicianPracticeLU extends dbTableInterface
{

    public void setLookupID(Integer newValue);
    public Integer getLookupID();
    public void setUniqueCreateDate(Date newValue);
    public Date getUniqueCreateDate();
    public void setUniqueModifyDate(Date newValue);
    public Date getUniqueModifyDate();
    public void setUniqueModifyComments(String newValue);
    public String getUniqueModifyComments();
    public void setPhysicianID(Integer newValue);
    public Integer getPhysicianID();
    public void setPracticeID(Integer newValue);
    public Integer getPracticeID();
    public void setStartDate(Date newValue);
    public Date getStartDate();
    public void setEndDate(Date newValue);
    public Date getEndDate();
    public void setisPrimaryOffice(Integer newValue);
    public Integer getisPrimaryOffice();
    public void setisAdministrativeOffice(Integer newValue);
    public Integer getisAdministrativeOffice();
    public void setCoverageHours(String newValue);
    public String getCoverageHours();
    public void setComments(String newValue);
    public String getComments();
}    // End of bltPhysicianPracticeLU class definition
