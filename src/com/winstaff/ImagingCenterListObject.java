package com.winstaff;

import com.winstaff.password.RandomString;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Vector;

/**
 * Created by IntelliJ IDEA.
 * User: ste
 * Date: 8/17/11
 * Time: 8:44 PM
 * To change this template use File | Settings | File Templates.
 */
public class ImagingCenterListObject {

    private Vector<PracticeMaster_SearchResultsObject> listOfCenters;
    private Vector<String> searchNotes_List;

    public ImagingCenterListObject(Vector<PracticeMaster_SearchResultsObject> listOfCenters, Vector<String> searchNotes_List) {
        this.listOfCenters = listOfCenters;
        this.searchNotes_List = searchNotes_List;
    }

    public String getSearchNotes_Text(String breaker) {
        String returnVal="";
        for  (String myString :  this.getSearchNotes_List())
        {
            returnVal += myString + breaker;

        }
        return returnVal;
    }


    public Vector<PracticeMaster_SearchResultsObject> getListOfCenters() {
        return listOfCenters;
    }

    public void setListOfCenters(Vector<PracticeMaster_SearchResultsObject> listOfCenters) {
        this.listOfCenters = listOfCenters;
    }

    public Vector<String> getSearchNotes_List() {
        return searchNotes_List;
    }

    public void setSearchNotes_List(Vector<String> searchNotes_List) {
        this.searchNotes_List = searchNotes_List;
    }
}
