package com.winstaff;
/*
 * ConfigurationInformation.java
 *
 * Created on April 14, 2001, 7:41 PM
 */


/**
 *
 * @author  Scott Ellis
 * @version 1.00
 */
public class ConfigurationInformation_Remote
{
//            Class.forName("org.gjt.mm.mysql.Driver").newInstance();


    //SQLServer ws1
/*
    public static  String          sDBType                 = "ms"; //[mySQL = mySQL] [SQLServ = MS] [Access=access]
    public static  String          sJDBCDriverName         = "com.microsoft.jdbc.sqlserver.SQLServerDriver";
    public static  String          sURL                    = "jdbc:microsoft:sqlserver://winstaffdbghc:1433;user=v3_app;password=gjam2lc1";
    public static  String          sUser                   = "v3_app";
    public static  String          sPassword               = "gjam2lc1";



    public static  String          sURL                    = "jdbc:microsoft:sqlserver://xcube:1433;user=v3_app;password=xTIG39";
    public static  String          sUser                   = "v3_app";
    public static  String          sPassword               = "xTIG39";
*/

/*
	public static  String          sDBType                 = "ps"; //[mySQL = mySQL] [SQLServ = MS] [Access=access]
    public static  String          sJDBCDriverName         = "org.postgresql.Driver";
    public static  String          sURL                    = "jdbc:postgresql://localhost:5432/v3_1?user=v3_app&password=xTIG39";
    public static  String          sUser                   = "v3_app";
    public static  String          sPassword               = "xTIG39";
*/



    //mySQL ws2
    public static  String          sDBType                 = "mySQL"; //[mySQL = mySQL] [SQLServ = MS] [Access=access]
    public static  String          sJDBCDriverName         = "com.mysql.jdbc.Driver";
    public static  String          sURL                    = "jdbc:mysql://mysql.nextimagemedical.com/nimsales?user=nimremote&password=z6y32asGSKHGsrd";
    public static  String          sUser                   = "nimremote";
    public static  String          sPassword               = "z6y32asGSKHGsrd";

    public static  boolean         bRequiredByGroup	= true;
    public static  boolean         bExpiredByGroup	= true;
    public static  boolean         bSecurityByGroup	= true;



    public static  boolean         bRunAudit            = true;
    public static  boolean         bInDebugMode            = true;
    public static  boolean         bInDebugModeShowFldSets = false;
    public static  boolean         bCreateLogBasic = true;
    public static  boolean         bCreateLogFull = true;
    public static  String          sDisplayHidden		= "[hidden]";
    public static  String          sDateTimeFormat         = "yyyy-MM-dd hh:mm:ss a";
    public static  String          sDefaultDateValue       = "1800-01-01 00:00:00 AM";
    public static final String          sDefaultIntegerValue    = "0";
    public static final String          sDefaultBooleanValue    = "0";
    public static final String          sDefaultStringValue     = "";
    public static final String          sDefaultDoubleValue     = "0.0";
    


    public static  String          sMergeRootFolder   = "C:\\winstaff\\merge";
//    public static  String          sMergeRootFolderNoEscape   = "C:\winstaff\merge";
    public static  String          sLogFolder   = "C:\\web\\webapps\\logs";
    public static  String          sUploadFolderDirectory   = "c:\\phdb_upload\\";
    public static  String          sLinkedPDFDirectory   = "C:\\Program Files\\Apache Tomcat 4.0\\scanpdf";
    public static  String          sUnLinkedPDFDirectory   = "C:\\Program Files\\Apache Tomcat 4.0\\scanpdf";
    public static  String          sPHDBPhotoDirectory     = "C:\\Development\\work\\winstaff\\webapps\\photos";
    public static  String          serverName     		= "https://nmserver3";
//    public static  String          serverName     		= "http://scotte-laptop1";
//    public static  String          serverName     		= "https://pro-file.winstaff.com";
    public static  String          serverIP		= "127.0.0.1";
    public static  String          applicationFolder	= "winstaff";
    public static  String          applicationVersion	= "PF v4.0 (DEV)";
    public static  long            sessionTimeOutInMillis = 900000;

//    public static  String	SMTPServer = "smtp.abac.com";
//    public static  String	SMTPServer = "mail.pacbell.net";
//    public static  String	SMTPServer = "pro-file.winstaff.com";
    public static  String	SMTPServer = "127.0.0.1";
    public static  String	ReturnEmail = "support@winstaff.com";

    /** Creates new ConfigurationInformation */
    public ConfigurationInformation_Remote () 
    {
    }   // End of default constructor
 }   // End of ConfigurationInformation class
