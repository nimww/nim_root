package com.winstaff;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import com.google.gson.Gson;

public class MapsAPI {

	/**
	 * @param args
	 * @throws IOException 
	 * @throws SQLException 
	 */
	public static void main(String[] args) throws SQLException, IOException {
		// TODO Main Class is to generate a report for Stacy
		getStacy();
	}

	public static GoogleMaps getGeoCode(String address) throws IOException {
		String googleAPI = "http://maps.googleapis.com/maps/api/geocode/json?sensor=false&address=" + address.replaceAll(" ", "+");

		return new Gson().fromJson((new BufferedReader(new InputStreamReader((new URL(googleAPI)).openStream()))), GoogleMaps.class);
	}
	public static double getLat(GoogleMaps googlemaps){
		return googlemaps.results.get(0).geometry.location.lat;
	}
	
	public static double getLng(GoogleMaps googlemaps){
		return googlemaps.results.get(0).geometry.location.lng;
	}
	
	public static DistanceMatrix getDistanceMatrix(String origins, String destinations) throws IOException {
		String googleAPI = "http://maps.googleapis.com/maps/api/distancematrix/json?sensor=false&units=imperial&origins=" + origins.replaceAll(" ", "+") + "&destinations=" + destinations.replaceAll(" ", "+");

		return new Gson().fromJson((new BufferedReader(new InputStreamReader((new URL(googleAPI)).openStream()))), DistanceMatrix.class);
	}
	public static String getDistance(DistanceMatrix distancematrix){
		
		return distancematrix.getRows().get(0).getElements().get(0).distance.text;
	}
	public static String getStacy() throws SQLException, IOException {
		String query = "select  \"Encounter_ScanPass\"  \"Patient SP\", to_char(dateofservice, 'mm/dd/yyyy')  \"Date of service\", \"PatientAddress1\"||' '|| \"PatientCity\"||' '||\"PatientState\"||' '||ca.patientzip \"Patient Address\",\"PatientState\" \"Patient State\", en.pricing_structure \"Tier\", \"Practice_Name\" \"Practice Name\", pm.officeaddress1||' '||pm.officecity||' '||pmst.shortstate||' '||pm.officezip \"Practice Address\", pmst.shortstate \"Practice State\" from \"vMQ4_Encounter_NoVoid_BP\" main join tnim3_caseaccount ca on ca.caseid = main.\"CaseID\" join tnim3_encounter en on en.encounterid = main.\"EncounterID\" join tpracticemaster pm on pm.practiceid = main.\"PracticeID\" join tstateli pmst on pmst.stateid = pm.officestateid where \"Practice_State\" in ('GA','TN') and \"Referral_ReceiveDate\" > '2013-05-01' order by \"Referral_ReceiveDate\"";

		searchDB2 conn = new searchDB2();
		ResultSet rs = conn.executeStatement(query);

		String sFileName = "/var/lib/tomcat7/webapps/ROOT/WEB-INF/classes/export/random_report.csv";
//		DataOutputStream out = new DataOutputStream(new FileOutputStream(sFileName));
		
		String header = "Patient SP,Date of service,Patient Address,Patient State,Tier,Practice Name,Practice Address,Practice State,Driving Distance\n";
//		out.writeBytes(header);
		String temp = "";
		while (rs.next()) {
			 temp += rs.getString("Patient SP")+","+
					rs.getString("Date of service")+","+
					rs.getString("Patient Address")+","+
					rs.getString("Patient State")+","+
					rs.getString("Tier")+","+
					rs.getString("Practice Name")+","+
					rs.getString("Practice Address")+","+
					rs.getString("Practice State")+",";
			 try{
				 temp +=getDistance(getDistanceMatrix(rs.getString("Patient Address"),rs.getString("Practice Address")))+"\n";
			 } catch(Exception e){
				 temp +="--\n";
			 }
					
//			out.writeBytes(temp);
		}
//		out.flush();
//		out.close();
		return temp;
	}
	public static String getTest(){
		return "select  \"Encounter_ScanPass \"  \"Patient SP \", to_char(dateofservice, 'mm/dd/yyyy')  \"Date of service \", \"PatientAddress1 \"||' '|| \"PatientCity \"||' '|| \"PatientState \"||' '||ca.patientzip  \"Patient Address \", \"PatientState \"  \"Patient State \", en.pricing_structure  \"Tier \",  \"Practice_Name \"  \"Practice Name \", pm.officeaddress1||' '||pm.officecity||' '||pmst.shortstate||' '||pm.officezip  \"Practice Address \", pmst.shortstate  \"Practice State \" from  \"vMQ4_Encounter_NoVoid_BP \" main join tnim3_caseaccount ca on ca.caseid = main. \"CaseID \" join tnim3_encounter en on en.encounterid = main. \"EncounterID \" join tpracticemaster pm on pm.practiceid = main. \"PracticeID \" join tstateli pmst on pmst.stateid = pm.officestateid where  \"Practice_State \" in ('GA','TN') and  \"Referral_ReceiveDate \" > '2013-05-01' order by  \"Referral_ReceiveDate \"";

	}
}
