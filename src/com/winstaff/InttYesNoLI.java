package com.winstaff;


import com.winstaff.dbtYesNoLI;
import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public interface InttYesNoLI extends dbTableInterface
{

    public void setYesNoID(Integer newValue);
    public Integer getYesNoID();
    public void setYNShort(String newValue);
    public String getYNShort();
    public void setYNLong(String newValue);
    public String getYNLong();
}    // End of bltYesNoLI class definition
