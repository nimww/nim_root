package com.winstaff;
import java.io.*;
import java.sql.ResultSet;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

public class NIM_ProviderList_Orig{
	public static void main(String[]args) throws IOException{
		
		//String query = "SELECT * FROM \"provider_list\"";
		String query = "SELECT DISTINCT ON(mypm.practiceid) mypm.practiceid as provider_id, practicename as location_name, officeaddress1 as addr1, officeaddress2 as addr2, officecity as City, mystate.shortstate as prac_state, officezip as zip, officephone as phone, officefederaltaxid as taxid, contractdate as contract_date, cliaexpirationdate as term_date, npinumber as provider_npid, npinumber as location_npid, statelicensenumber as lic_statecode, modal.statuslong as specialty FROM tpracticemaster mypm INNER JOIN tstateli as mystate ON mystate.stateid = mypm.officestateid INNER JOIN tnim3_modality as mymodallist on mymodallist.practiceid = mypm.practiceid INNER JOIN tmodalitytypeli as modal ON modal.modalitytypeid = mymodallist.modalitytypeid WHERE contractingstatusid in (2,8) ORDER BY provider_id ASC";
		searchDB2 ss = new searchDB2();
		java.sql.ResultSet rs = ss.executeStatement(query);
		getProviderList(rs);
		ss.closeAll();
	}
	public static void getProviderList(ResultSet rs) throws IOException{
		String npl = "export/providerlist.xls";
		try {
			HSSFWorkbook hwb = new HSSFWorkbook();
			HSSFSheet sheet = hwb.createSheet("new sheet");
			HSSFRow rh = sheet.createRow((int) 0);
			
			rh.createCell((int) 0).setCellValue("ProviderID");
			rh.createCell((int) 1).setCellValue("LocationName");
			rh.createCell((int) 2).setCellValue("Addr1");
			rh.createCell((int) 3).setCellValue("Addr2");
			rh.createCell((int) 4).setCellValue("City");
			rh.createCell((int) 5).setCellValue("State");
			rh.createCell((int) 6).setCellValue("Zip");
			rh.createCell((int) 7).setCellValue("Phone");
			rh.createCell((int) 8).setCellValue("TaxID");
			rh.createCell((int) 9).setCellValue("ContractDate");
			rh.createCell((int) 10).setCellValue("TermDate");
			rh.createCell((int) 11).setCellValue("ProviderNPID");
			rh.createCell((int) 12).setCellValue("LocationNPID");
			rh.createCell((int) 13).setCellValue("LicID");
			rh.createCell((int) 14).setCellValue("Specialty");
		int i = 1;
		while(rs.next() && rs != null){
			HSSFRow row = sheet.createRow((int) i);
			row.createCell((int) 0).setCellValue(rs.getString("provider_id"));
			row.createCell((int) 1).setCellValue(rs.getString("location_name"));
			row.createCell((int) 2).setCellValue(rs.getString("addr1"));
			row.createCell((int) 3).setCellValue(rs.getString("addr2"));
			row.createCell((int) 4).setCellValue(rs.getString("City"));
			row.createCell((int) 5).setCellValue(rs.getString("prac_state"));
			row.createCell((int) 6).setCellValue(rs.getString("zip"));
			row.createCell((int) 7).setCellValue(rs.getString("phone"));
			row.createCell((int) 8).setCellValue(rs.getString("taxid"));
			row.createCell((int) 9).setCellValue(rs.getString("contract_date"));
			row.createCell((int) 10).setCellValue(rs.getString("term_date"));
			row.createCell((int) 11).setCellValue(rs.getString("provider_npid"));
			row.createCell((int) 12).setCellValue(rs.getString("location_npid"));
			row.createCell((int) 13).setCellValue(rs.getString("lic_statecode"));
			row.createCell((int) 14).setCellValue(rs.getString("specialty"));
			i++;
		}
		FileOutputStream fileout = new FileOutputStream(npl);
		hwb.write(fileout);
		fileout.close();
		System.out.println("Excel sheet being generated");
			}catch (Exception e) {
				System.out.println(e);
				e.printStackTrace();
			}
		
	}
}