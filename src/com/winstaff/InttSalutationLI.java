package com.winstaff;


import com.winstaff.dbtSalutationLI;
import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public interface InttSalutationLI extends dbTableInterface
{

    public void setSalutationID(Integer newValue);
    public Integer getSalutationID();
    public void setSalutationShort(String newValue);
    public String getSalutationShort();
    public void setSalutationLong(String newValue);
    public String getSalutationLong();
}    // End of bltSalutationLI class definition
