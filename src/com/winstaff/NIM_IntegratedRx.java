package com.winstaff;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Vector;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;

public class NIM_IntegratedRx{
	private String TOMCAT = "tomcat7";
	private String remotedir = "/in/incomingIRX";
	private String localDir = "/var/lib/" + TOMCAT + "/webapps/ROOT/WEB-INF/classes/IRX";
	private String host = "ftp.nextimagemedical.com";
	private String user = "integratedrx";
	private String password = "ub5Qs5U8QFsI10tHVesF1";
	
	public static void main(String[]args){
		File xmlfile = new File("");//get path
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		try {
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			try {
				Document doc = dBuilder.parse(xmlfile);
				doc.getDocumentElement().normalize();
				System.out.println("Root element :" + doc.getDocumentElement().getNodeName());
				NodeList nList = doc.getElementsByTagName("");//xml tag
				System.out.println("----------------------------");
				for(int temp = 0; temp < nList.getLength(); temp ++){
					Node nNode = nList.item(temp);
					System.out.println("\nCurrent Element :" + nNode.getNodeName());
					
				}
				
			} catch (SAXException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		}	
	}
	
	public void processXMLFiles(){
		String newFolder = new SimpleDateFormat("yyyMMdd").format(new Date());
		System.out.println("mkdir: " + localDir + "/" + newFolder);
		File file = new File(localDir);
		for (File s : file.listFiles()) {
			if (s.isFile()) {
				new File(localDir + "/" + newFolder).mkdir();
			}
		}
		for (File s : file.listFiles()) {
			if (s.isFile()) {
				System.out.println("Load to DB: " + localDir + "/" + newFolder + "/" + s.getName());
			}
			if (s.isFile() && loadToDB(s)) {
				System.out.println("\tOK - Move File: " + localDir + "/" + newFolder + "/" + s.getName());
				s.renameTo(new File(localDir + "/" + newFolder + "/" + s.getName()));
			}
		}
	}
	
	private boolean loadToDB(File s) {
		// TODO Auto-generated method stub
		return false;
	}

	private void email(String emailTo, String emailFrom, String theSubject, String theBody) throws SQLException {
		bltEmailTransaction email = new bltEmailTransaction();
		email.setEmailTo(emailTo);
		email.setEmailFrom(emailFrom);
		email.setEmailSubject(theSubject);
		email.setEmailBody(theBody);
		email.setEmailBodyType(emailType_V3.PLAIN_TYPE);
		email.setTransactionDate(new java.util.Date());
		email.commitData();
	
	}
}
