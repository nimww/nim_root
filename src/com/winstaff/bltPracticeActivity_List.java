
package com.winstaff;
/*
 * bltPracticeActivity_List.java
 *
 * Created: Mon Dec 14 08:49:31 PST 2009
 */



import java.sql.*;




/**
 *
 * @author  Scott Ellis via listClassGenCode
 * @version 2.5
 */
public class bltPracticeActivity_List extends db_BaseRelationTable
{
    /** Default constructor
    */    
    public bltPracticeActivity_List ( Integer iAdminID )
    {
        super( "tPracticeActivity", "PracticeActivityID", "PracticeActivityID", "AdminID", iAdminID );
    }   // End of bltPracticeActivity_List()
    

    public void setField(String fieldN, Object fieldV)
    {DebugLogger.println("setField Method not implemented yet in classbltPracticeActivity_List!");}

    public String getFieldType(String fieldN)
    {return null;}

    public bltPracticeActivity_List ( Integer iAdminID, String extraWhere, String OrderBy )
    {
        super( "tPracticeActivity", "PracticeActivityID", "PracticeActivityID", "PracticeID", iAdminID, extraWhere,OrderBy );
    }   // End of bltPracticeActivity_List()
     /** Method pull information from the result set and load into class attributes.
    * @param sFieldList Comma delimited list of fields for which data has been retrieved.
    * @param rs Result set from which to pull the data from.
    * @throws SQLException Passes on SQLException
    */      
    protected void transferData( ResultSet rs ) throws SQLException
    {
        while ( rs.next()  )
        {
            Integer         iValue              = new Integer( rs.getString( sLinkFieldName ) );
            Integer         iLinkRecordID       = new Integer( rs.getString( sUniqueIDFieldName ) );
            Object          oReferenceObject;
            ListElement     leNewElement;
            
            oReferenceObject    = new bltPracticeActivity( iValue );
            
            leNewElement        = new ListElement( iLinkRecordID, oReferenceObject );
            
            vList.add( leNewElement );

        }
    }   // End of transferData()
}   // End of bltPracticeActivity_List class

