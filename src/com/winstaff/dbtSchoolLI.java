

package com.winstaff;

import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public class dbtSchoolLI extends Object implements InttSchoolLI
{

        db_NewBase    dbnbDB;

    public dbtSchoolLI()
    {
        dbnbDB = new db_NewBase( "tSchoolLI", "SchoolID" );

    }    // End of default constructor

    public dbtSchoolLI( Integer iNewID )
    {
        dbnbDB = new db_NewBase( "tSchoolLI", "SchoolID" );
        setUniqueID( iNewID );
    }    // End of Constructor knowing an ID

    public void commitData() throws SQLException
    {
            //no longer trapping/ignoring errors 2011-03-17
            dbnbDB.commitData();
    }    // End of commitData()

    public void commitDataForced() throws SQLException
    {
        dbnbDB.commitDataForced();
    }    // End of commitData()

    public Integer getUniqueID()
    {
        return dbnbDB.getUniqueID();
    }    // End of getUniqueID()

    public void setUniqueID( Integer iNewID )
    {
        dbnbDB.setUniqueID( iNewID );
    }    // End of setUniqueID()

    public void setModifyComments( String sNew )
    {
        dbnbDB.setModifyComments( sNew );
    }    // End of setUniqueID()


    public void setSchoolID(Integer newValue)
    {
                dbnbDB.setFieldData( "SchoolID", newValue.toString() );
    }

    public Integer getSchoolID()
    {
        String           sValue = dbnbDB.getFieldData( "SchoolID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setField(String fieldN, Object fieldV)
    {DebugLogger.println("setField Method not implemented yet in classSchoolLI!");}

    public String getFieldType(String fieldN)
    {return null;}


    public void setUniqueCreateDate(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "UniqueCreateDate", formatter.format( newValue ) );
    }

    public Date getUniqueCreateDate()
    {
        String           sValue = dbnbDB.getFieldData( "UniqueCreateDate" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setUniqueModifyDate(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "UniqueModifyDate", formatter.format( newValue ) );
    }

    public Date getUniqueModifyDate()
    {
        String           sValue = dbnbDB.getFieldData( "UniqueModifyDate" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setUniqueModifyComments(String newValue)
    {
                dbnbDB.setFieldData( "UniqueModifyComments", newValue.toString() );
    }

    public String getUniqueModifyComments()
    {
        String           sValue = dbnbDB.getFieldData( "UniqueModifyComments" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setSecurityGroupID(Integer newValue)
    {
                dbnbDB.setFieldData( "SecurityGroupID", newValue.toString() );
    }

    public Integer getSecurityGroupID()
    {
        String           sValue = dbnbDB.getFieldData( "SecurityGroupID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setName(String newValue)
    {
                dbnbDB.setFieldData( "Name", newValue.toString() );
    }

    public String getName()
    {
        String           sValue = dbnbDB.getFieldData( "Name" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setAddress1(String newValue)
    {
                dbnbDB.setFieldData( "Address1", newValue.toString() );
    }

    public String getAddress1()
    {
        String           sValue = dbnbDB.getFieldData( "Address1" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setAddress2(String newValue)
    {
                dbnbDB.setFieldData( "Address2", newValue.toString() );
    }

    public String getAddress2()
    {
        String           sValue = dbnbDB.getFieldData( "Address2" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setCity(String newValue)
    {
                dbnbDB.setFieldData( "City", newValue.toString() );
    }

    public String getCity()
    {
        String           sValue = dbnbDB.getFieldData( "City" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setStateID(Integer newValue)
    {
                dbnbDB.setFieldData( "StateID", newValue.toString() );
    }

    public Integer getStateID()
    {
        String           sValue = dbnbDB.getFieldData( "StateID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setZIP(String newValue)
    {
                dbnbDB.setFieldData( "ZIP", newValue.toString() );
    }

    public String getZIP()
    {
        String           sValue = dbnbDB.getFieldData( "ZIP" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setCountryID(Integer newValue)
    {
                dbnbDB.setFieldData( "CountryID", newValue.toString() );
    }

    public Integer getCountryID()
    {
        String           sValue = dbnbDB.getFieldData( "CountryID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setPhone(String newValue)
    {
                dbnbDB.setFieldData( "Phone", newValue.toString() );
    }

    public String getPhone()
    {
        String           sValue = dbnbDB.getFieldData( "Phone" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setFax(String newValue)
    {
                dbnbDB.setFieldData( "Fax", newValue.toString() );
    }

    public String getFax()
    {
        String           sValue = dbnbDB.getFieldData( "Fax" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setContactName(String newValue)
    {
                dbnbDB.setFieldData( "ContactName", newValue.toString() );
    }

    public String getContactName()
    {
        String           sValue = dbnbDB.getFieldData( "ContactName" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

}    // End of bltSchoolLI class definition
