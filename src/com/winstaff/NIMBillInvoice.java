package com.winstaff;

import java.util.ArrayList;

import com.winstaff.NIM3_EncounterObject2;

public class NIMBillInvoice {
	private NIM3_EncounterObject2 eo2;
	private ArrayList<ServiceInfo> si;
	public NIMBillInvoice(NIM3_EncounterObject2 eo2, ArrayList<ServiceInfo> si) {
		this.eo2 = eo2;
		this.si = si;
	}
	public NIM3_EncounterObject2 getEo2() {
		return eo2;
	}
	public void setEo2(NIM3_EncounterObject2 eo2) {
		this.eo2 = eo2;
	}
	public ArrayList<ServiceInfo> getSi() {
		return si;
	}
	public void setSi(ArrayList<ServiceInfo> si) {
		this.si = si;
	}
	
}
