package com.winstaff;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

public class CasesCreated {

	public static void main(String[] args) throws SQLException {
		CasesCreated cc = new CasesCreated();
		LinkedHashMap<Integer, Integer> lhm = cc.getDaily();
		
		String emailBody = "Cases Created Today "+(new SimpleDateFormat("MM-dd-yyyy")).format(new Date())+"<br><br>";
		
		for (Map.Entry<Integer, Integer> entry : lhm.entrySet()) {
			emailBody+=(new bltUserAccount(entry.getKey()).getContactFirstName())+"["+entry.getKey()+"] "+entry.getValue()+"<br>";
		}
		
		w3Report.email("jessica.rodriguez@nextimagemedical.com", "po.le@nextimagemedical.com", "Cases Schedlued", emailBody);
	}
	
	private LinkedHashMap<Integer, Integer> getDaily() throws SQLException{
		LinkedHashMap<Integer, Integer> lhm = new LinkedHashMap<Integer, Integer>();
		
		String query = "select * from taudit where lower(tablename) = 'tnim3_caseaccount' and lower(fieldname) = 'uniquecreatedate' and auditdate > now()-'1 day'::INTERVAL";
		
		searchDB2 conn = new searchDB2();
		ResultSet rs = conn.executeStatement(query);
		
		while(rs.next()){
			try{
				if(rs.getString("modifier").indexOf("nim")>-1){
					int lhmTemp = new Integer((String)rs.getString("modifier").split("\\[")[1].subSequence(0, rs.getString("modifier").split("\\[")[1].length()-1));
					if (lhm.containsKey(lhmTemp)){
						lhm.put(lhmTemp, lhm.get(lhmTemp)+1);
					} else{
						lhm.put(lhmTemp, 1);
					}
				}
			} catch(Exception e){
			}
		}
		return lhm;
	}

}
