package com.winstaff;

/**
 * Created by IntelliJ IDEA.
 * User: ste
 * Date: 2/6/11
 * Time: 4:35 PM
 * To change this template use File | Settings | File Templates.
 */
public class NIM3_BillingObject
{

    public Double getPayer_BillAmount() {
        return Round2(Payer_BillAmount);
    }

    public void setPayer_BillAmount(Double payer_BillAmount) {
        Payer_BillAmount = payer_BillAmount;
    }

    public Double getPayer_AllowAmount() {
        return Round2(Payer_AllowAmount);
    }

    public void setPayer_AllowAmount(Double payer_AllowAmount) {
        Payer_AllowAmount = payer_AllowAmount;
    }

    public Double getProvider_BillAmount() {
        return Round2(Provider_BillAmount);
    }

    public void setProvider_BillAmount(Double provider_BillAmount) {
        Provider_BillAmount = provider_BillAmount;
    }

    public Double getProvider_AllowAmount() {
        return Round2(Provider_AllowAmount);
    }

    public void setProvider_AllowAmount(Double provider_AllowAmount) {
        Provider_AllowAmount = provider_AllowAmount;
    }

    public Double getNetExpected()
    {
        return Round2(Payer_AllowAmount + Payer_AllowAmount_Adjustment - Provider_AllowAmount);
    }

    public Double getNetActual()
    {
        return Round2(Payer_ActualAmount - Provider_ActualAmount);
    }

    public Double getPayer_ActualAmount() {
        return Round2(Payer_ActualAmount);
    }

    public void setPayer_ActualAmount(Double payer_ActualAmount) {
        Payer_ActualAmount = payer_ActualAmount;
    }

    public Double getProvider_ActualAmount() {
        return Round2(Provider_ActualAmount);
    }

    public void setProvider_ActualAmount(Double provider_ActualAmount) {
        Provider_ActualAmount = provider_ActualAmount;
    }

    public Double getPayer_AllowAmount_Adjustment() {
        return Round2(Payer_AllowAmount_Adjustment);
    }

    public void setPayer_AllowAmount_Adjustment(Double payer_AllowAmount_Adjustment) {
        Payer_AllowAmount_Adjustment = payer_AllowAmount_Adjustment;
    }

    Double Payer_BillAmount = 0.0;
    Double Payer_AllowAmount = 0.0;
    Double Payer_AllowAmount_Adjustment = 0.0;
    Double Payer_ActualAmount = 0.0;
    Double Provider_ActualAmount = 0.0;
    Double Provider_BillAmount = 0.0;
    Double Provider_AllowAmount = 0.0;

    public Double Round2 (Double inDouble)
    {
        return new Double(  Math.round(inDouble*100) / 100.0);
    }

}
