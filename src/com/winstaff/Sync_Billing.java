package com.winstaff;

/**
 * Created by IntelliJ IDEA.
 * User: ste
 * Date: 3/28/12
 * Time: 9:32 PM
 * To change this template use File | Settings | File Templates.
 */
public class Sync_Billing {
    
    //Currently this just runs a sync on one case.  The intent is to run it on every case that's not void

    public void runSync(){
        NIM3_EncounterObject2 myEO2 = new NIM3_EncounterObject2(16006, "Testing");
        myEO2.synchronizeBilling_One_to_Two();
    }
    
    public static void main(String[] args) {
        java.util.Date startDate = new java.util.Date();
        System.out.println(">> Starting Billing Sync ["+startDate+"].\n");
        Sync_Billing sync = new Sync_Billing();
        if (args[0].equalsIgnoreCase("--run")){
            sync.runSync();
        } else {
        }
        java.util.Date endDate = new java.util.Date();
        System.out.println("\n<< FINISHED ["+endDate+"].");
        System.out.println("\n-- Duration ["+((startDate.getTime() - endDate.getTime())/1000)+"].");
    }
    
    
    
    
}
