package com.winstaff;

import javax.tools.JavaCompiler;

import java.sql.SQLException;
import java.util.*;

/**
 * Created by IntelliJ IDEA. User: ste Date: Jun 28, 2010 Time: 2:37:44 PM To
 * change this template use File | Settings | File Templates.
 */
public class NIM_EmailDaily extends Thread {
	static String nimAlert = "NIMAlerts@nextimagemedical.com";
	public NIM_EmailDaily() {

	}

	private static java.text.SimpleDateFormat displayDateTimeSDF = new java.text.SimpleDateFormat("MM/dd/yyyy hh:mm a");

	static public void main(String args[]) {
		NIM_EmailDaily testMe = new NIM_EmailDaily();
		if (true) {
			try {
				// int mySleep = 300;
				if (args[0].equalsIgnoreCase("n=yt")) {
					testMe.runMeMQ4(false);
				} else if (args[0].equalsIgnoreCase("n=ya")) {
					testMe.runMeMQ4(true);
				} else if (args[0].equalsIgnoreCase("n=n")) {
					testMe.runMe();
				}
				// System.out.println(">>> sleeping for " +
				// Math.round(mySleep/60) + " Minutes...");
				// testMe.sleep(mySleep * 1000);
			} catch (Exception e) {
				System.out.println("Error in Thread: " + e);
			}
		}
	}

	public static String filterDisplay(String myIN) {
		String myVal = myIN;
		if (myVal == null || myVal.equalsIgnoreCase("null")) {
			myVal = "0";
		}
		return myVal;
	}

	public static void runMeMQ4(boolean sendAll) {
		System.out.println("==============================================================================");
		System.out.println(">>> Running NIM Daily Email Send " + new java.util.Date());
		searchDB2 mySDBA_divd = new searchDB2();
		searchDB2 mySS = new searchDB2();
		try {
			// String theBody =
			// "<h2>Testing: Daily report in Beta/Testing Mode! Please report any errors!</h2>Recent Change: We have added a breakdown by Sales Division. Note: if a Payer is not coded to a Sales Division, it will be grouped as \"~Other\"<br><br>Please help ensure that all numbers are accurate.<br><br>NextImage Daily Report for: <strong>"
			// + displayDateTimeSDF.format(new java.util.Date()) + "</strong>";
			String theBody = "NextImage Daily Report for: <strong>" + displayDateTimeSDF.format(new java.util.Date()) + "</strong>";
			int iTotal_Day_Sales = 0;
			int iTotal_Day_Acq = 0;
			int iTotal_Day_CaseLoad = 0;
			int iTotal_Month_Sales = 0;
			int iTotal_Month_Acq = 0;
			int iTotal_Month_CaseLoad = 0;
			int iTotal_lMonth_Sales = 0;
			int iTotal_lMonth_Acq = 0;
			int iTotal_lMonth_CaseLoad = 0;
			int iTotal_All_Sales = 0;
			int iTotal_All_Acq = 0;
			int iTotal_All_CaseLoad = 0;

			int iTotal_Day_MR = 0;
			int iTotal_Month_MR = 0;
			int iTotal_lMonth_MR = 0;
			int iTotal_Day_CT = 0;
			int iTotal_Month_CT = 0;
			int iTotal_lMonth_CT = 0;
			int iTotal_Day_EMG = 0;
			int iTotal_Month_EMG = 0;
			int iTotal_lMonth_EMG = 0;
			int iTotal_Day_Oth = 0;
			int iTotal_Month_Oth = 0;
			int iTotal_lMonth_Oth = 0;

			TreeMap<String, NIM_Report_DailyTotalsObject> SalesCounts = new TreeMap<String, NIM_Report_DailyTotalsObject>();

			int iTotal_Day_nc_Sales = 0;
			int iTotal_Month_nc_Sales = 0;
			int iTotal_lMonth_nc_Sales = 0;
			int iTotal_All_nc_Sales = 0;

			if (true) // run sales totals
			{
				System.out.println(">>> Running Sales Division Totals Day " + new java.util.Date());
				String mySQLA_divd = "SELECT\n" + "\"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Payer_SalesDivision\" as sdivision,\n" + "\"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Encounter_IsCourtesy\" as iCourtesy,\n" + "\"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Referral_ReceiveDate_iYear\" as iyear,\n" + "\"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Referral_ReceiveDate_iDay\" as iday,\n"
						+ "\"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Referral_ReceiveDate_iMonth\" as imonth,\n" + "Count(\"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Encounter_ScanPass\") As icount\n" + "FROM\n" + "\"public\".\"vMQ4_Encounter_NoVoid_BP\"\n" + "GROUP BY\n" + "\"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Payer_SalesDivision\",\n"
						+ "\"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Encounter_IsCourtesy\", \n" + "\"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Referral_ReceiveDate_iYear\",\n" + "\"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Referral_ReceiveDate_iDay\" ,\n" + "\"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Referral_ReceiveDate_iMonth\"\n" + "order by \n"
						+ "\"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Referral_ReceiveDate_iYear\" desc,\n" + "\"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Referral_ReceiveDate_iMonth\" desc,\n" + "\"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Referral_ReceiveDate_iDay\" Desc\n" + " ";
				// java.sql.ResultSet myRSA =
				// mySDBA.executePreparedStatement(new
				// db_PreparedStatementObject(mySQLA));
				java.sql.ResultSet myRSA_divd = mySDBA_divd.executeStatement((mySQLA_divd));
				// build email body
				Calendar cal = Calendar.getInstance();
				int nowDay = cal.get(Calendar.DATE);
				int nowMonth = cal.get(Calendar.MONTH) + 1;
				int nowYear = cal.get(Calendar.YEAR);
				while (myRSA_divd.next()) {
					String myDiv = myRSA_divd.getString("sdivision");
					int iCourtesy = 0;
					int iYear = 0;
					int iDay = 0;
					int iMonth = 0;
					int iCount = 0;
					try {
						iCourtesy = new Integer(myRSA_divd.getString("iCourtesy"));
						iYear = new Integer(myRSA_divd.getString("iyear"));
						iDay = new Integer(myRSA_divd.getString("iday"));
						iMonth = new Integer(myRSA_divd.getString("imonth"));
						iCount = new Integer(myRSA_divd.getString("icount"));
					} catch (Exception emini) {
						System.out.println("DB Read Error: " + emini);
					}

					NIM_Report_DailyTotalsObject tempR_DTO;
					if (myDiv == null || myDiv.equalsIgnoreCase("")) {
						myDiv = "~Other";
					}
					if (SalesCounts.containsKey(myDiv)) {
						tempR_DTO = SalesCounts.get(myDiv);
					} else {
						tempR_DTO = new NIM_Report_DailyTotalsObject();
					}
					tempR_DTO.total_All += iCount;
					iTotal_All_Sales += iCount;
					if (iCourtesy == 1) {
						tempR_DTO.total_nc_All += iCount;
						iTotal_All_nc_Sales += iCount;
					}
					if (iYear == nowYear) {
						if (iMonth == nowMonth) {
							tempR_DTO.total_Month += iCount;
							iTotal_Month_Sales += iCount;
							if (iCourtesy == 1) {
								tempR_DTO.total_nc_Month += iCount;
								iTotal_Month_nc_Sales += iCount;
							}
							if (iDay == nowDay) {
								tempR_DTO.total_Day += iCount;
								iTotal_Day_Sales += iCount;
								if (iCourtesy == 1) {
									tempR_DTO.total_nc_Day += iCount;
									iTotal_Day_nc_Sales += iCount;
								}
							}
						} else if (iMonth == nowMonth - 1) {
							tempR_DTO.total_lMonth += iCount;
							iTotal_lMonth_Sales += iCount;
							if (iCourtesy == 1) {
								tempR_DTO.total_nc_lMonth += iCount;
								iTotal_lMonth_nc_Sales += iCount;
							}
						}
					} else if ((iYear == nowYear - 1) && (nowMonth - 1) == 0 && iMonth == 12) {
						tempR_DTO.total_lMonth += iCount;
						iTotal_lMonth_Sales += iCount;
						if (iCourtesy == 1) {
							tempR_DTO.total_nc_lMonth += iCount;
							iTotal_lMonth_nc_Sales += iCount;
						}
					}
					SalesCounts.put(myDiv, tempR_DTO);
				}
				mySDBA_divd.closeAll();
			}

			int TotalAMSC_Day = 0;
			int TotalAMSC_Month = 0;
			int TotalAMSC_lMonth = 0;
			int TotalAMSC_All = 0;
			int TotalAOth_Day = 0;
			int TotalAOth_Month = 0;
			int TotalAOth_lMonth = 0;
			int TotalAOth_All = 0;

			mySDBA_divd = new searchDB2();
			if (true) // run acq totals
			{
				System.out.println(">>> Running Acq Division Totals Day " + new java.util.Date());
				String mySQLA_divd = "SELECT\n" + "\"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Payer_AcquisitionDivision\" as sdivision,\n" + "\"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Referral_ReceiveDate_iYear\" as iyear,\n" + "\"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Referral_ReceiveDate_iDay\" as iday,\n" + "\"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Referral_ReceiveDate_iMonth\" as imonth,\n"
						+ "Count(\"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Encounter_ScanPass\") As icount\n" + "FROM\n" + "\"public\".\"vMQ4_Encounter_NoVoid_BP\"\n" + "GROUP BY\n" + "\"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Payer_AcquisitionDivision\",\n" + "\"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Referral_ReceiveDate_iYear\",\n"
						+ "\"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Referral_ReceiveDate_iDay\" ,\n" + "\"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Referral_ReceiveDate_iMonth\"\n" + "order by \n" + "\"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Referral_ReceiveDate_iYear\" desc,\n" + "\"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Referral_ReceiveDate_iMonth\" desc,\n"
						+ "\"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Referral_ReceiveDate_iDay\" Desc\n" + " ";
				// java.sql.ResultSet myRSA =
				// mySDBA.executePreparedStatement(new
				// db_PreparedStatementObject(mySQLA));
				java.sql.ResultSet myRSA_divd = mySDBA_divd.executeStatement((mySQLA_divd));
				// build email body
				Calendar cal = Calendar.getInstance();
				int nowDay = cal.get(Calendar.DATE);
				int nowMonth = cal.get(Calendar.MONTH) + 1;
				int nowYear = cal.get(Calendar.YEAR);
				while (myRSA_divd.next()) {
					String myDiv = myRSA_divd.getString("sdivision");
					int iYear = 0;
					int iDay = 0;
					int iMonth = 0;
					int iCount = 0;
					try {
						iYear = new Integer(myRSA_divd.getString("iyear"));
						iDay = new Integer(myRSA_divd.getString("iday"));
						iMonth = new Integer(myRSA_divd.getString("imonth"));
						iCount = new Integer(myRSA_divd.getString("icount"));
					} catch (Exception emini) {
						System.out.println("DB Read Error: " + emini);
					}
					if (myDiv.equalsIgnoreCase("msc")) {
						TotalAMSC_All += iCount;
						iTotal_All_Acq += iCount;
						if (iYear == nowYear) {
							if (iMonth == nowMonth) {
								TotalAMSC_Month += iCount;
								iTotal_Month_Acq += iCount;
								if (iDay == nowDay) {
									TotalAMSC_Day += iCount;
									iTotal_Day_Acq += iCount;
								}
							} else if (iMonth == nowMonth - 1) {
								TotalAMSC_lMonth += iCount;
								iTotal_lMonth_Acq += iCount;
							}
						} else if ((iYear == nowYear - 1) && (nowMonth - 1) == 0 && iMonth == 12) {
							TotalAMSC_lMonth += iCount;
							iTotal_lMonth_Acq += iCount;
						}
					} else {
						TotalAOth_All += iCount;
						iTotal_All_Acq += iCount;
						if (iYear == nowYear) {
							if (iMonth == nowMonth) {
								TotalAOth_Month += iCount;
								iTotal_Month_Acq += iCount;
								if (iDay == nowDay) {
									TotalAOth_Day += iCount;
									iTotal_Day_Acq += iCount;
								}
							} else if (iMonth == nowMonth - 1) {
								TotalAOth_lMonth += iCount;
								iTotal_lMonth_Acq += iCount;
							}
						} else if ((iYear == nowYear - 1) && (nowMonth - 1) == 0 && iMonth == 12) {
							TotalAOth_lMonth += iCount;
							iTotal_lMonth_Acq += iCount;
						}
					}
				}
				mySDBA_divd.closeAll();
			}

			int TotalCA_Day = 0;
			int TotalCA_Month = 0;
			int TotalCA_lMonth = 0;
			int TotalCA_All = 0;
			int TotalFL_Day = 0;
			int TotalFL_Month = 0;
			int TotalFL_lMonth = 0;
			int TotalFL_All = 0;

			mySDBA_divd = new searchDB2();
			if (true) // run Case Load totals
			{
				System.out.println(">>> Running CaseLoad Division Totals Day " + new java.util.Date());
				String mySQLA_divd = "SELECT\n" + "CASE WHEN (\"vMQ4_Encounter_NoVoid_BP\".\"UA_AssignedTo_UserID\" = ANY (ARRAY[15278, 15279, 15280, 15281])) THEN 'FL'::text ELSE 'CA'::text END AS sdivision,\n" + "                            \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Referral_ReceiveDate_iYear\" as iyear,\n"
						+ "                            \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Referral_ReceiveDate_iDay\" as iday,\n" + "                            \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Referral_ReceiveDate_iMonth\" as imonth,\n" + "                            Count(\"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Encounter_ScanPass\") As icount\n"
						+ "                            FROM\n" + "                            \"public\".\"vMQ4_Encounter_NoVoid_BP\"\n" + "                            GROUP BY\n" + "                            sdivision,\n" + "                            \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Referral_ReceiveDate_iYear\",\n"
						+ "                            \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Referral_ReceiveDate_iDay\" ,\n" + "                            \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Referral_ReceiveDate_iMonth\"\n" + "                            order by \n" + "                            \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Referral_ReceiveDate_iYear\" desc,\n"
						+ "                            \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Referral_ReceiveDate_iMonth\" desc,\n" + "                            \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Referral_ReceiveDate_iDay\" Desc\n" + "";
				// java.sql.ResultSet myRSA =
				// mySDBA.executePreparedStatement(new
				// db_PreparedStatementObject(mySQLA));
				java.sql.ResultSet myRSA_divd = mySDBA_divd.executeStatement((mySQLA_divd));
				// build email body
				Calendar cal = Calendar.getInstance();
				int nowDay = cal.get(Calendar.DATE);
				int nowMonth = cal.get(Calendar.MONTH) + 1;
				int nowYear = cal.get(Calendar.YEAR);
				while (myRSA_divd.next()) {
					String myDiv = myRSA_divd.getString("sdivision");
					int iYear = 0;
					int iDay = 0;
					int iMonth = 0;
					int iCount = 0;
					try {
						iYear = new Integer(myRSA_divd.getString("iyear"));
						iDay = new Integer(myRSA_divd.getString("iday"));
						iMonth = new Integer(myRSA_divd.getString("imonth"));
						iCount = new Integer(myRSA_divd.getString("icount"));
					} catch (Exception emini) {
						System.out.println("DB Read Error: " + emini);
					}
					if (myDiv.equalsIgnoreCase("fl")) {
						TotalFL_All += iCount;
						iTotal_All_CaseLoad += iCount;
						if (iYear == nowYear) {
							if (iMonth == nowMonth) {
								TotalFL_Month += iCount;
								iTotal_Month_CaseLoad += iCount;
								if (iDay == nowDay) {
									TotalFL_Day += iCount;
									iTotal_Day_CaseLoad += iCount;
								}
							} else if (iMonth == nowMonth - 1) {
								TotalFL_lMonth += iCount;
								iTotal_lMonth_CaseLoad += iCount;
							}
						} else if ((iYear == nowYear - 1) && (nowMonth - 1) == 0 && iMonth == 12) {
							TotalFL_lMonth += iCount;
							iTotal_lMonth_CaseLoad += iCount;
						}
					} else {
						TotalCA_All += iCount;
						iTotal_All_CaseLoad += iCount;
						if (iYear == nowYear) {
							if (iMonth == nowMonth) {
								TotalCA_Month += iCount;
								iTotal_Month_CaseLoad += iCount;
								if (iDay == nowDay) {
									TotalCA_Day += iCount;
									iTotal_Day_CaseLoad += iCount;
								}
							} else if (iMonth == nowMonth - 1) {
								TotalCA_lMonth += iCount;
								iTotal_lMonth_CaseLoad += iCount;
							}
						} else if ((iYear == nowYear - 1) && (nowMonth - 1) == 0 && iMonth == 12) {
							TotalCA_lMonth += iCount;
							iTotal_lMonth_CaseLoad += iCount;
						}
					}
				}
				mySDBA_divd.closeAll();
			}

			mySDBA_divd = new searchDB2();
			if (true) // run Enc Type Breakout
			{
				System.out.println(">>> Running Breakout Totals Day " + new java.util.Date());
				String mySQLA_divd = "SELECT\n" + "\"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Encounter_Type\" AS \"Type\",\n" + "\"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Referral_ReceiveDate_iYear\" AS \"iYear\",\n" + "\"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Referral_ReceiveDate_iMonth\" AS \"iMonth\",\n" + "\"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Referral_ReceiveDate_iDay\" AS \"iDay\",\n"
						+ "Count(\"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Encounter_ScanPass\") AS \"iCount\"\n" + "FROM\n" + "\"public\".\"vMQ4_Encounter_NoVoid_BP\"\n" + "GROUP BY\n" + "\"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Encounter_Type\",\n" + "\"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Referral_ReceiveDate_iYear\",\n"
						+ "\"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Referral_ReceiveDate_iMonth\",\n" + "\"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Referral_ReceiveDate_iDay\"";
				// java.sql.ResultSet myRSA =
				// mySDBA.executePreparedStatement(new
				// db_PreparedStatementObject(mySQLA));
				java.sql.ResultSet myRSA_divd = mySDBA_divd.executeStatement((mySQLA_divd));
				// build email body
				Calendar cal = Calendar.getInstance();
				int nowDay = cal.get(Calendar.DATE);
				int nowMonth = cal.get(Calendar.MONTH) + 1;
				int nowYear = cal.get(Calendar.YEAR);
				while (myRSA_divd.next()) {
					String myDiv = myRSA_divd.getString("Type");
					int iYear = 0;
					int iDay = 0;
					int iMonth = 0;
					int iCount = 0;
					try {
						iYear = new Integer(myRSA_divd.getString("iyear"));
						iDay = new Integer(myRSA_divd.getString("iday"));
						iMonth = new Integer(myRSA_divd.getString("imonth"));
						iCount = new Integer(myRSA_divd.getString("icount"));
					} catch (Exception emini) {
						System.out.println("DB Read Error: " + emini);
					}
					if (myDiv.equalsIgnoreCase("mr")) {
						if (iYear == nowYear) {
							if (iMonth == nowMonth) {
								iTotal_Month_MR += iCount;
								if (iDay == nowDay) {
									iTotal_Day_MR += iCount;
								}
							} else if (iMonth == nowMonth - 1) {
								iTotal_lMonth_MR += iCount;
							}
						} else if ((iYear == nowYear - 1) && (nowMonth - 1) == 0 && iMonth == 12) {
							iTotal_lMonth_MR += iCount;
						}
					} else if (myDiv.equalsIgnoreCase("CT")) {
						if (iYear == nowYear) {
							if (iMonth == nowMonth) {
								iTotal_Month_CT += iCount;
								if (iDay == nowDay) {
									iTotal_Day_CT += iCount;
								}
							} else if (iMonth == nowMonth - 1) {
								iTotal_lMonth_CT += iCount;
							}
						} else if ((iYear == nowYear - 1) && (nowMonth - 1) == 0 && iMonth == 12) {
							iTotal_lMonth_CT += iCount;
						}
					} else if (myDiv.equalsIgnoreCase("EMG")) {
						if (iYear == nowYear) {
							if (iMonth == nowMonth) {
								iTotal_Month_EMG += iCount;
								if (iDay == nowDay) {
									iTotal_Day_EMG += iCount;
								}
							} else if (iMonth == nowMonth - 1) {
								iTotal_lMonth_EMG += iCount;
							}
						} else if ((iYear == nowYear - 1) && (nowMonth - 1) == 0 && iMonth == 12) {
							iTotal_lMonth_EMG += iCount;
						}
					} else {
						if (iYear == nowYear) {
							if (iMonth == nowMonth) {
								iTotal_Month_Oth += iCount;
								if (iDay == nowDay) {
									iTotal_Day_Oth += iCount;
								}
							} else if (iMonth == nowMonth - 1) {
								iTotal_lMonth_Oth += iCount;
							}
						} else if ((iYear == nowYear - 1) && (nowMonth - 1) == 0 && iMonth == 12) {
							iTotal_lMonth_Oth += iCount;
						}
					}

				}
				mySDBA_divd.closeAll();
			}

			theBody += "<table  border=\"1\" bordercolor=#999999 cellspacing=\"0\" cellpadding=\"1\">" + "  <tr>" + "    <td bgcolor=\"#D0FBD6\"><strong>Totals</strong></td>" + "    <td bgcolor=\"#D0FBD6\"><strong>Today</strong></td>" + "    <td bgcolor=\"#D0FBD6\"><strong>Month</strong></td>" + "    <td bgcolor=\"#D0FBD6\"><strong>Last Month</strong></td>" +
			// "    <td bgcolor=\"#D0FBD6\"><strong>All Time</strong></td>" +
					"  </tr>" + "  <tr>" + "    <td bgcolor=\"#D0FBD6\">&nbsp;&nbsp;&nbsp;&nbsp;Total (Sales)</td>" + "    <td bgcolor=\"#D0FBD6\">" + iTotal_Day_Sales + "</td>" + "    <td bgcolor=\"#D0FBD6\">" + iTotal_Month_Sales + "</td>" + "    <td bgcolor=\"#D0FBD6\">" + iTotal_lMonth_Sales + "</td>" +
			// "    <td bgcolor=\"#D0FBD6\">"+iTotal_All_Sales+"</td>" +
					"  </tr>" + "  <tr>" + "    <td bgcolor=\"#D0FBD6\">&nbsp;&nbsp;&nbsp;&nbsp;Courtesy Only (Sales)</td>" + "    <td bgcolor=\"#D0FBD6\">" + iTotal_Day_nc_Sales + "</td>" + "    <td bgcolor=\"#D0FBD6\">" + iTotal_Month_nc_Sales + "</td>" + "    <td bgcolor=\"#D0FBD6\">" + iTotal_lMonth_nc_Sales + "</td>" +
					// "    <td bgcolor=\"#D0FBD6\">"+iTotal_All_nc_Sales+"</td>"
					// +
					"  </tr>" + "  <tr>" + "    <td bgcolor=\"#D0FBD6\">&nbsp;&nbsp;&nbsp;&nbsp;Type: <i>MR</i></td>" + "    <td bgcolor=\"#D0FBD6\">" + iTotal_Day_MR + "</td>" + "    <td bgcolor=\"#D0FBD6\">" + iTotal_Month_MR + "</td>" + "    <td bgcolor=\"#D0FBD6\">" + iTotal_lMonth_MR + "</td>" +
					// "    <td bgcolor=\"#D0FBD6\">"+iTotal_All_nc_Sales+"</td>"
					// +
					"  </tr>" + "  <tr>" + "    <td bgcolor=\"#D0FBD6\">&nbsp;&nbsp;&nbsp;&nbsp;Type: <i>CT</i></td>" + "    <td bgcolor=\"#D0FBD6\">" + iTotal_Day_CT + "</td>" + "    <td bgcolor=\"#D0FBD6\">" + iTotal_Month_CT + "</td>" + "    <td bgcolor=\"#D0FBD6\">" + iTotal_lMonth_CT + "</td>" +
					// "    <td bgcolor=\"#D0FBD6\">"+iTotal_All_nc_Sales+"</td>"
					// +
					"  </tr>" + "  <tr>" + "    <td bgcolor=\"#D0FBD6\">&nbsp;&nbsp;&nbsp;&nbsp;Type: <i>EMG</i></td>" + "    <td bgcolor=\"#D0FBD6\">" + iTotal_Day_EMG + "</td>" + "    <td bgcolor=\"#D0FBD6\">" + iTotal_Month_EMG + "</td>" + "    <td bgcolor=\"#D0FBD6\">" + iTotal_lMonth_EMG + "</td>" +
					// "    <td bgcolor=\"#D0FBD6\">"+iTotal_All_nc_Sales+"</td>"
					// +
					"  </tr>" + "  <tr>" + "    <td bgcolor=\"#D0FBD6\">&nbsp;&nbsp;&nbsp;&nbsp;Type: <i>Oth</i></td>" + "    <td bgcolor=\"#D0FBD6\">" + iTotal_Day_Oth + "</td>" + "    <td bgcolor=\"#D0FBD6\">" + iTotal_Month_Oth + "</td>" + "    <td bgcolor=\"#D0FBD6\">" + iTotal_lMonth_Oth + "</td>" +
					// "    <td bgcolor=\"#D0FBD6\">"+iTotal_All_nc_Sales+"</td>"
					// +
					"  </tr>" +
					// "  <tr>" +
					// "    <td colspan=\"4\" bgcolor=\"#CFE0FC\"><em>Sales: A breakdown by sales divisions; a measure of the totals from the NIM/MSC branches</em></td>"
					// +
					// "  </tr>" +
					"  <tr>" + "    <td bgcolor=\"#CFE0FC\"><strong>Sales Division Breakdown</strong></td>" + "    <td bgcolor=\"#CFE0FC\"><strong>Today</strong></td>" + "    <td bgcolor=\"#CFE0FC\"><strong>Month</strong></td>" + "    <td bgcolor=\"#CFE0FC\"><strong>Last Month</strong></td>" +
					// "    <td bgcolor=\"#CFE0FC\"><strong>All Time</strong></td>"
					// +
					"  </tr>";

			for (String tempKey : SalesCounts.keySet()) {
				NIM_Report_DailyTotalsObject temp_RDTO = SalesCounts.get(tempKey);
				theBody += "  <tr>" + "    <td bgcolor=\"#CFE0FC\">&nbsp;&nbsp;&nbsp;&nbsp;<strong>" + tempKey + "</strong>: All (Courtesy)</td>" + "    <td bgcolor=\"#CFE0FC\">" + temp_RDTO.total_Day + " (" + temp_RDTO.total_nc_Day + ")</td>" + "    <td bgcolor=\"#CFE0FC\">" + temp_RDTO.total_Month + " (" + temp_RDTO.total_nc_Month + ")</td>" + "    <td bgcolor=\"#CFE0FC\">"
						+ temp_RDTO.total_lMonth + " (" + temp_RDTO.total_nc_lMonth + ")</td>" +
						// "    <td bgcolor=\"#CFE0FC\">"+TotalSNIM_All+" ("+TotalSNIM_nc_All+")</td>"
						// +
						"  </tr>";
			}
			theBody += "</table>";

			String BodyDetail = "";
			mySS = new searchDB2();
			if (true) // by payer
			{
				System.out.println(">>> Running Detail for " + new java.util.Date());
				String mySQL = ("SELECT \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"PayerID\" AS \"PID\", \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"PayerName\" AS \"Name\",\"public\".\"vMQ4_Encounter_NoVoid_BP\".\"PayerName\" AS \"PayerName\", \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Payer_SalesDivision\" AS \"Sales\", \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Referral_ReceiveDate_iYear\" AS \"iYear\", \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Referral_ReceiveDate_iMonth\" AS \"iMonth\", \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Referral_ReceiveDate_iDay\" AS \"iDay\", Count(\"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Encounter_ScanPass\") AS \"iCount\", \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Encounter_Type\" FROM \"public\".\"vMQ4_Encounter_NoVoid_BP\" GROUP BY \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"PayerID\", \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"PayerName\", \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Payer_SalesDivision\", \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Referral_ReceiveDate_iYear\", \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Referral_ReceiveDate_iMonth\", \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Referral_ReceiveDate_iDay\", \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Encounter_Type\" order by \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"PayerName\", \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Referral_ReceiveDate_iYear\" , \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Referral_ReceiveDate_iMonth\" , \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Referral_ReceiveDate_iDay\" ");
				java.sql.ResultSet result = null;
				result = mySS.executeStatement(mySQL);
				java.util.Hashtable myPayers = new java.util.Hashtable();
				int i5 = 0;
				java.util.Calendar cal = java.util.Calendar.getInstance();
				int nowDay = cal.get(java.util.Calendar.DATE);
				int nowMonth = cal.get(java.util.Calendar.MONTH) + 1;
				int nowYear = cal.get(java.util.Calendar.YEAR);

				// nowDay = 31;
				// nowMonth = 12;
				// nowYear = 2013;

				while (result.next()) {
					i5++;
					String IPID_ID = (result.getString("PID"));
					String SMOD = (result.getString("Encounter_Type"));
					String pNAME = (result.getString("Name"));
					String pPayerNAME = (result.getString("PayerName"));
					String pDIV = (result.getString("Sales"));
					String IPID = pNAME + " [" + IPID_ID + "]";
					java.util.Hashtable myNumbers = null;
					int[] tempARMonth_MR = new int[13];
					int[] tempARMonth_CT = new int[13];
					int[] tempARMonth_EMG = new int[13];
					int[] tempARMonth_Oth = new int[13];
					Integer tempATotal_MR = new Integer(0);
					Integer tempATotal_CT = new Integer(0);
					Integer tempATotal_EMG = new Integer(0);
					Integer tempATotal_Oth = new Integer(0);
					Integer tempTDay_MR = new Integer(0);
					Integer tempTDay_CT = new Integer(0);
					Integer tempTDay_EMG = new Integer(0);
					Integer tempTDay_Oth = new Integer(0);
					Integer tempLastMonth_MR = new Integer(0);
					Integer tempLastMonth_CT = new Integer(0);
					Integer tempLastMonth_EMG = new Integer(0);
					Integer tempLastMonth_Oth = new Integer(0);
					if (myPayers.containsKey(IPID)) {
						// out.print("<br>Using Hash for " + IPID);
						myNumbers = (java.util.Hashtable) myPayers.get(IPID);
						tempARMonth_MR = (int[]) myNumbers.get("arMonth_MR");
						tempARMonth_CT = (int[]) myNumbers.get("arMonth_CT");
						tempARMonth_EMG = (int[]) myNumbers.get("arMonth_EMG");
						tempARMonth_Oth = (int[]) myNumbers.get("arMonth_Oth");
						tempATotal_MR = (Integer) myNumbers.get("aTotal_MR");
						tempATotal_CT = (Integer) myNumbers.get("aTotal_CT");
						tempATotal_EMG = (Integer) myNumbers.get("aTotal_EMG");
						tempATotal_Oth = (Integer) myNumbers.get("aTotal_Oth");
						tempTDay_MR = (Integer) myNumbers.get("tDay_MR");
						tempTDay_CT = (Integer) myNumbers.get("tDay_CT");
						tempTDay_EMG = (Integer) myNumbers.get("tDay_EMG");
						tempTDay_Oth = (Integer) myNumbers.get("tDay_Oth");
						tempLastMonth_MR = (Integer) myNumbers.get("LastMonth_MR");
						tempLastMonth_CT = (Integer) myNumbers.get("LastMonth_CT");
						tempLastMonth_EMG = (Integer) myNumbers.get("LastMonth_EMG");
						tempLastMonth_Oth = (Integer) myNumbers.get("LastMonth_Oth");
					} else if (true) {
						// out.print("<br>Creating Hash for " + IPID);
						myNumbers = new java.util.Hashtable();
						myNumbers.put("Name", pNAME);
						myNumbers.put("Sales", pDIV);
						myNumbers.put("IPID_ID", IPID_ID);
						myNumbers.put("PayerName", pPayerNAME);
					}
					int iYear = 0;
					int iDay = 0;
					int iMonth = 0;
					int iCount = 0;
					try {
						iYear = new Integer(result.getString("iyear"));
						iDay = new Integer(result.getString("iday"));
						iMonth = new Integer(result.getString("imonth"));
						iCount = new Integer(result.getString("icount"));
					} catch (Exception eee) {
						// out.print("caught[use]:"+eee);
					}
					if (SMOD.equalsIgnoreCase("MR")) {
						if (iYear == nowYear) {
							tempATotal_MR += iCount;
							tempARMonth_MR[iMonth] += iCount;
							if (iMonth == nowMonth) {
								if (iDay == nowDay) {
									tempTDay_MR += iCount;
								}
							} else if (iMonth == nowMonth - 1) {
								tempLastMonth_MR += iCount;
							}
						} else if (iYear == nowYear - 1 && iMonth == 12) {
							tempLastMonth_MR += iCount;
						}
					} else if (SMOD.equalsIgnoreCase("CT")) {
						if (iYear == nowYear) {
							tempATotal_CT += iCount;
							tempARMonth_CT[iMonth] += iCount;
							if (iMonth == nowMonth) {
								if (iDay == nowDay) {
									tempTDay_CT += iCount;
								}
							} else if (iMonth == nowMonth - 1) {
								tempLastMonth_CT += iCount;
							}
						} else if (iYear == nowYear - 1 && iMonth == 12) {
							tempLastMonth_CT += iCount;
						}
					} else if (SMOD.equalsIgnoreCase("EMG")) {
						if (iYear == nowYear) {
							tempATotal_EMG += iCount;
							tempARMonth_EMG[iMonth] += iCount;
							if (iMonth == nowMonth) {
								if (iDay == nowDay) {
									tempTDay_EMG += iCount;
								}
							} else if (iMonth == nowMonth - 1) {
								tempLastMonth_EMG += iCount;
							}
						} else if (iYear == nowYear - 1 && iMonth == 12) {
							tempLastMonth_EMG += iCount;
						}
					} else {
						if (iYear == nowYear) {
							tempATotal_Oth += iCount;
							tempARMonth_Oth[iMonth] += iCount;
							if (iMonth == nowMonth) {
								if (iDay == nowDay) {
									tempTDay_Oth += iCount;
								}
							} else if (iMonth == nowMonth - 1) {
								tempLastMonth_Oth += iCount;
							}
						} else if (iYear == nowYear - 1 && iMonth == 12) {
							tempLastMonth_Oth += iCount;
						}
					}
					myNumbers.put("arMonth_MR", tempARMonth_MR);
					myNumbers.put("arMonth_CT", tempARMonth_CT);
					myNumbers.put("arMonth_EMG", tempARMonth_EMG);
					myNumbers.put("arMonth_Oth", tempARMonth_Oth);
					myNumbers.put("aTotal_MR", tempATotal_MR);
					myNumbers.put("aTotal_CT", tempATotal_CT);
					myNumbers.put("aTotal_EMG", tempATotal_EMG);
					myNumbers.put("aTotal_Oth", tempATotal_Oth);
					myNumbers.put("tDay_MR", tempTDay_MR);
					myNumbers.put("tDay_CT", tempTDay_CT);
					myNumbers.put("tDay_EMG", tempTDay_EMG);
					myNumbers.put("tDay_Oth", tempTDay_Oth);
					myNumbers.put("LastMonth_MR", tempLastMonth_MR);
					myNumbers.put("LastMonth_CT", tempLastMonth_CT);
					myNumbers.put("LastMonth_EMG", tempLastMonth_EMG);
					myNumbers.put("LastMonth_Oth", tempLastMonth_Oth);
					myPayers.put(IPID, myNumbers);
				}
				mySS.closeAll();
				// now write out one row for each entry in the database table
				java.util.Vector eList = new java.util.Vector(myPayers.keySet());
				java.util.Collections.sort(eList);
				java.util.Iterator it = eList.iterator();
				int cnt = 0;
				if (it.hasNext()) {
					BodyDetail += "<table border=1 cellpadding=0, cellspacing=2 style=\"font-size:12px;\" >";
					while (it.hasNext()) {
						String element = (String) it.next();
						java.util.Hashtable myNumbers = (java.util.Hashtable) myPayers.get(element);
						String tempIPID = (String) myNumbers.get("IPID_ID");
						String tempName = (String) myNumbers.get("Name");
						String tempDiv = (String) myNumbers.get("Sales");
						String tempPayerName = (String) myNumbers.get("PayerName");
						int[] tempARMonth_MR = (int[]) myNumbers.get("arMonth_MR");
						int[] tempARMonth_CT = (int[]) myNumbers.get("arMonth_CT");
						int[] tempARMonth_EMG = (int[]) myNumbers.get("arMonth_EMG");
						int[] tempARMonth_Oth = (int[]) myNumbers.get("arMonth_Oth");
						Integer tempATotal_MR = (Integer) myNumbers.get("aTotal_MR");
						Integer tempATotal_CT = (Integer) myNumbers.get("aTotal_CT");
						Integer tempATotal_EMG = (Integer) myNumbers.get("aTotal_EMG");
						Integer tempATotal_Oth = (Integer) myNumbers.get("aTotal_Oth");
						Integer tempTDay_MR = (Integer) myNumbers.get("tDay_MR");
						Integer tempTDay_CT = (Integer) myNumbers.get("tDay_CT");
						Integer tempTDay_EMG = (Integer) myNumbers.get("tDay_EMG");
						Integer tempTDay_Oth = (Integer) myNumbers.get("tDay_Oth");
						Integer tempLastMonth_MR = (Integer) myNumbers.get("LastMonth_MR");
						Integer tempLastMonth_CT = (Integer) myNumbers.get("LastMonth_CT");
						Integer tempLastMonth_EMG = (Integer) myNumbers.get("LastMonth_EMG");
						Integer tempLastMonth_Oth = (Integer) myNumbers.get("LastMonth_Oth");

						int todaytotal = tempTDay_MR + tempTDay_CT + tempTDay_EMG + tempTDay_Oth;
						int monthtotal = tempARMonth_MR[nowMonth] + tempARMonth_CT[nowMonth] + tempARMonth_EMG[nowMonth] + tempARMonth_Oth[nowMonth];
						
						//int lmonthtotal = tempARMonth_MR[nowMonth - 1] + tempARMonth_CT[nowMonth - 1] + tempARMonth_EMG[nowMonth - 1] + tempARMonth_Oth[nowMonth - 1];
						int lmonthtotal = getLastMonth(new Integer(tempIPID));
						
						// int lmonthtotal = tempLastMonth_MR + tempLastMonth_CT
						// + tempLastMonth_EMG + tempLastMonth_Oth;

						String addClass_td = "";
						if (todaytotal > 0) {
							addClass_td = " style=\" background-color:#99ff99\" ";
						}

						if (monthtotal > 0) {
							if (cnt % 10 == 0) {
								BodyDetail += "<tr style=\"font-size: 12; color: #006; font-weight: bold;\" ><td>Payer</td><td>Sales</td><td>Today</td><td>Month</td><td>Last Month</td></tr>";
							}
							cnt++;
							// BodyDetail +="<tr "+addClass_td+"><td>" +
							// tempPayerName + "</td><td>" + tempDiv +
							// "</td><td >MR:" + tempTDay_MR + "<br>CT:" +
							// tempTDay_CT + "<br>EMG:" + tempTDay_EMG +
							// "<br>Oth:" + tempTDay_Oth + "<br><strong>Tot:" +
							// todaytotal + "</strong></td><td>MR:" +
							// tempARMonth_MR[nowMonth] + "<br>CT:" +
							// tempARMonth_CT[nowMonth] + "<br>EMG:" +
							// tempARMonth_EMG[nowMonth] + "<br>Oth:" +
							// tempARMonth_Oth[nowMonth] + "<br><strong>Tot:" +
							// monthtotal + "</strong></td><td>MR:" +
							// tempLastMonth_MR + "<br>CT:" + tempLastMonth_CT +
							// "<br>EMG:" + tempLastMonth_EMG + "<br>Oth:" +
							// tempLastMonth_Oth + "<br><strong>Tot:" +
							// lmonthtotal + "</strong></td></tr>";
							// BodyDetail +="<tr><td>" + tempPayerName +
							// "</td><td>" + tempDiv +
							// "</td><td "+addClass_td+">MR:" + tempTDay_MR +
							// "&nbsp;|&nbsp;CT:" + tempTDay_CT +
							// "&nbsp;|&nbsp;EMG:" + tempTDay_EMG +
							// "&nbsp;|&nbsp;Oth:" + tempTDay_Oth +
							// "&nbsp;|&nbsp;<strong>Tot:" + todaytotal +
							// "</strong></td><td>MR:" +
							// tempARMonth_MR[nowMonth] + "&nbsp;|&nbsp;CT:" +
							// tempARMonth_CT[nowMonth] + "&nbsp;|&nbsp;EMG:" +
							// tempARMonth_EMG[nowMonth] + "&nbsp;|&nbsp;Oth:" +
							// tempARMonth_Oth[nowMonth] +
							// "&nbsp;|&nbsp;<strong>Tot:" + monthtotal +
							// "</strong></td><td>MR:" +
							// tempARMonth_MR[nowMonth-1] + "&nbsp;|&nbsp;CT:" +
							// tempARMonth_CT[nowMonth-1] + "&nbsp;|&nbsp;EMG:"
							// + tempARMonth_EMG[nowMonth-1] +
							// "&nbsp;|&nbsp;Oth:" + tempARMonth_Oth[nowMonth-1]
							// + "&nbsp;|&nbsp;<strong>Tot:" + lmonthtotal +
							// "</strong></td></tr>";
							BodyDetail += "<tr><td>" + tempPayerName + "</td><td>" + tempDiv + "</td><td " + addClass_td + ">" + todaytotal + "</td><td>" + monthtotal + "</td><td>" + lmonthtotal + "</td></tr>";
						}

					}
					BodyDetail += "</table>";
				}

			}

			String theBodyFinal = theBody + "<hr>" + BodyDetail + "<br><hr>MQ4.2[BETA]";
			if (theBodyFinal.length() > 29500) {
				theBodyFinal = theBodyFinal.substring(0, 29500);
			}
			String theSubject = "NextImage Daily Report for [" + displayDateTimeSDF.format(new java.util.Date()) + "]";
			bltEmailTransaction myET = new bltEmailTransaction();
			myET.setEmailTo("po.le@nextimagemedical.com");
			myET.setEmailFrom("support@nextimagemedical.com");
			myET.setEmailSubject(theSubject);
			myET.setEmailBody(theBodyFinal);
			myET.setEmailBodyType(emailType_V3.HTML_TYPE);
			// System.out.println("Body Type: " + myETSend.getBodyType());
			myET.commitData();

			if (sendAll) {
				myET = new bltEmailTransaction();
				myET.setEmailTo(nimAlert);
				myET.setEmailFrom("support@nextimagemedical.com");
				myET.setEmailSubject(theSubject);
				myET.setEmailBody(theBodyFinal);
				myET.setEmailBodyType(emailType_V3.HTML_TYPE);
				// System.out.println("Body Type: " + myETSend.getBodyType());
				myET.commitData();
			}

		} catch (Exception e) {
			System.out.println(e);
		} finally {
			mySS.closeAll();
			mySDBA_divd.closeAll();
		}

	}
	private static int getLastMonth(int pid) throws SQLException{
		String query = "SELECT \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"PayerID\" AS \"PID\", \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Referral_ReceiveDate_iYear\" AS \"iYear\", \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Referral_ReceiveDate_iMonth\" AS \"iMonth\", Count(\"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Encounter_ScanPass\") AS \"iCount\" " +
						" FROM \"public\".\"vMQ4_Encounter_NoVoid_BP\"  " +
						" where \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Referral_ReceiveDate_iYear\" = extract('year' from now()-'1 month'::interval) " +
						" and \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Referral_ReceiveDate_iMonth\" = extract('month' from now()-'1 month'::interval) " +
						" and \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"PayerID\" = " + pid + 
						" GROUP BY \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"PayerID\", \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Referral_ReceiveDate_iYear\", \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Referral_ReceiveDate_iMonth\" ";
						
		searchDB2 conn = new searchDB2();
		java.sql.ResultSet rs = conn.executeStatement(query);
		
		int temp = 0;
		
		while(rs.next()){
			temp = rs.getInt("iCount");
		}
		
		conn.closeAll();
		
		return temp;
	}
	public static void runMe() {
		System.out.println("==============================================================================");
		System.out.println(">>> Running NIM Daily Email Send " + new java.util.Date());
		try {
			String theBody2 = "";

			System.out.println(">>> Running Division Totals Day " + new java.util.Date());
			searchDB2 mySDBA_divd = new searchDB2();
			String mySQLA_divd = "SELECT \"public\".\"vNIM_Daily_Division\".\"iMonth_ReceiveDate\", \"public\".\"vNIM_Daily_Division\".\"iDay_ReceiveDate\", \"public\".\"vNIM_Daily_Division\".\"iYear_ReceiveDate\", Count(\"public\".\"vNIM_Daily_Division\".\"ScanPass\"), \"public\".\"vNIM_Daily_Division\".sdivision FROM \"public\".\"vNIM_Daily_Division\" where  \"public\".\"vNIM_Daily_Division\".\"iMonth_ReceiveDate\" =  date_part('month',now()) AND \"public\".\"vNIM_Daily_Division\".\"iDay_ReceiveDate\" =  date_part('day',now()) AND \"public\".\"vNIM_Daily_Division\".\"iYear_ReceiveDate\" =  date_part('year',now())  GROUP BY \"public\".\"vNIM_Daily_Division\".\"iMonth_ReceiveDate\", \"public\".\"vNIM_Daily_Division\".\"iDay_ReceiveDate\", \"public\".\"vNIM_Daily_Division\".\"iYear_ReceiveDate\", \"public\".\"vNIM_Daily_Division\".sdivision order by sdivision";
			// java.sql.ResultSet myRSA = mySDBA.executePreparedStatement(new
			// db_PreparedStatementObject(mySQLA));
			java.sql.ResultSet myRSA_divd = mySDBA_divd.executeStatement((mySQLA_divd));
			// build email body
			int TotalFL_Day = 0;
			int TotalCA_Day = 0;
			while (myRSA_divd.next()) {
				String myDiv = myRSA_divd.getString("sdivision");
				if (myDiv.equalsIgnoreCase("ca")) {
					TotalCA_Day = new Integer(myRSA_divd.getString("count"));
				} else if (myDiv.equalsIgnoreCase("fl")) {
					TotalFL_Day = new Integer(myRSA_divd.getString("count"));
				}
			}
			theBody2 += "<u>Total for Today:" + displayDateTimeSDF.format(new java.util.Date()) + "</u>";
			theBody2 += "<br>Total CA-Division:<strong>" + TotalCA_Day + "</strong>";
			theBody2 += "<br>Total FL-Division:<strong>" + TotalFL_Day + "</strong>";
			mySDBA_divd.closeAll();

			System.out.println(">>> Running Division Totals Month " + new java.util.Date());
			searchDB2 mySDBA_div = new searchDB2();
			String mySQLA_div = "SELECT \"public\".\"vNIM_Daily_Division\".\"iMonth_ReceiveDate\", \"public\".\"vNIM_Daily_Division\".\"iYear_ReceiveDate\", Count(\"public\".\"vNIM_Daily_Division\".\"ScanPass\"), \"public\".\"vNIM_Daily_Division\".sdivision FROM \"public\".\"vNIM_Daily_Division\" where  \"public\".\"vNIM_Daily_Division\".\"iMonth_ReceiveDate\" =  date_part('month',now()) AND \"public\".\"vNIM_Daily_Division\".\"iYear_ReceiveDate\" =  date_part('year',now())  GROUP BY \"public\".\"vNIM_Daily_Division\".\"iMonth_ReceiveDate\", \"public\".\"vNIM_Daily_Division\".\"iYear_ReceiveDate\", \"public\".\"vNIM_Daily_Division\".sdivision order by sdivision";
			// java.sql.ResultSet myRSA = mySDBA.executePreparedStatement(new
			// db_PreparedStatementObject(mySQLA));
			java.sql.ResultSet myRSA_div = mySDBA_div.executeStatement((mySQLA_div));
			// build email body
			int TotalFL_Month = 0;
			int TotalCA_Month = 0;
			while (myRSA_div.next()) {
				String myDiv = myRSA_div.getString("sdivision");
				if (myDiv.equalsIgnoreCase("ca")) {
					TotalCA_Month = new Integer(myRSA_div.getString("count"));
				} else if (myDiv.equalsIgnoreCase("fl")) {
					TotalFL_Month = new Integer(myRSA_div.getString("count"));
				}
			}
			theBody2 += "<br><br><u>Total for this Month</u>";
			theBody2 += "<br>Total CA-Division:<strong>" + TotalCA_Month + "</strong>";
			theBody2 += "<br>Total FL-Division:<strong>" + TotalFL_Month + "</strong>";
			mySDBA_div.closeAll();

			searchDB2 mySDBA = new searchDB2();
			String mySQLA = "SELECT \"public\".\"vRP_NIM-Daily\".payername, \"public\".\"vRP_NIM-Daily\".\"cTodayAll\", \"public\".\"vRP_NIM-Daily\".\"cTodayMR\", \"public\".\"vRP_NIM-Daily\".\"cTodayCT\", \"public\".\"vRP_NIM-Daily\".\"cTodayOther\", \"public\".\"vRP_NIM-Daily\".\"cMonth\", \"public\".\"vRP_NIM-Daily\".\"cMonthCT\", \"public\".\"vRP_NIM-Daily\".\"cMonthMR\", \"public\".\"vRP_NIM-Daily\".\"cMonthOther\", \"public\".\"vRP_NIM-Daily\".\"cAll\", \"public\".\"vRP_NIM-Daily\".\"cAll2\", \"public\".\"vRP_NIM-Daily\".\"cAllMR\", \"public\".\"vRP_NIM-Daily\".\"cAllCT\", \"public\".\"vRP_NIM-Daily\".\"cAllOther\" FROM \"public\".\"vRP_NIM-Daily\" order by \"public\".\"vRP_NIM-Daily\".payername";
			// java.sql.ResultSet myRSA = mySDBA.executePreparedStatement(new
			// db_PreparedStatementObject(mySQLA));
			java.sql.ResultSet myRSA = mySDBA.executeStatement((mySQLA));
			int totalCnt = 0;
			int successCnt = 0;
			// build email body
			String theSubject = "NextImage Daily Report (BETA/3) for: " + displayDateTimeSDF.format(new java.util.Date());
			String theBody1 = "NextImage Daily Report for: <strong>" + displayDateTimeSDF.format(new java.util.Date()) + "</strong>";
			String theBody = "<table border=1 cellspace=0 cellpadding=3><tr><td><strong>Payer Office</strong></td> <td><strong>Today</strong></td>  <td><strong>Monthly</strong></td> <td><strong>All Time</strong></td></tr>";
			int TodayTotal = 0;
			int MonthTotal = 0;
			while (myRSA.next()) {
				TodayTotal += new Integer(filterDisplay(myRSA.getString("cTodayAll"))).intValue();
				MonthTotal += new Integer(filterDisplay(myRSA.getString("cMonth"))).intValue();
				System.out.println("Generating");
				totalCnt++;
				theBody += "<tr>";
				theBody += "<td>";
				theBody += "<strong>" + myRSA.getString("payername") + "</strong>";
				theBody += "<br><br>Total Cases (All Time): " + myRSA.getString("cAll");
				theBody += "</td>";
				theBody += "<td>";
				theBody += "Total Today: <strong>" + filterDisplay(myRSA.getString("cTodayAll")) + "</strong>";
				theBody += "<br>&nbsp;MR: " + filterDisplay(myRSA.getString("cTodayMR"));
				theBody += "<br>&nbsp;CT: " + filterDisplay(myRSA.getString("cTodayCT"));
				theBody += "<br>&nbsp;Other: " + filterDisplay(myRSA.getString("cTodayOther"));
				theBody += "</td>";
				theBody += "<td>";
				theBody += "Total Month: <strong>" + filterDisplay(myRSA.getString("cMonth")) + "</strong>";
				theBody += "<br>&nbsp;MR: " + filterDisplay(myRSA.getString("cMonthMR"));
				theBody += "<br>&nbsp;CT: " + filterDisplay(myRSA.getString("cMonthCT"));
				theBody += "<br>&nbsp;Other: " + filterDisplay(myRSA.getString("cMonthOther"));
				theBody += "</td>";
				theBody += "<td>";
				theBody += "Total All-Time: <strong>" + filterDisplay(myRSA.getString("cAll")) + "</strong>";
				theBody += "<br>&nbsp;MR: " + filterDisplay(myRSA.getString("cAllMR"));
				theBody += "<br>&nbsp;CT: " + filterDisplay(myRSA.getString("cAllCT"));
				theBody += "<br>&nbsp;Other: " + filterDisplay(myRSA.getString("cAllOther"));
				theBody += "</td>";
				theBody += "</tr>";

				System.out.println("Done Saving");
			}
			mySDBA.closeAll();

			theBody += "</table>";
			theBody += "<hr>Note: 'Total' items listed above show total Cases/Encounters.  The service breakdown (MR/CT/Other) indicates total services done.  So it is possible that there are more services than there are cases (for example, if more than one MR is done in one appointment)";
			String theBodyFinal = theBody2 + "<hr>" + theBody1 + "<br>Today Total : <strong>" + TodayTotal + "</strong><br>Month Total: <strong>" + MonthTotal + "</strong><br><br>" + theBody;

			// Gen Emails
			bltEmailTransaction myET = new bltEmailTransaction();
			myET.setEmailTo("po.le@nextimagemedical.com");
			myET.setEmailFrom("support@nextimagemedical.com");
			myET.setEmailSubject(theSubject);
			myET.setEmailBody(theBodyFinal);
			myET.setEmailBodyType(emailType_V3.HTML_TYPE);
			// System.out.println("Body Type: " + myETSend.getBodyType());
			myET.commitData();

			myET = new bltEmailTransaction();
			// myET.setEmailTo(nimAlert);
			myET.setEmailTo("po.le@nextimagemedical.com");
			myET.setEmailFrom("support@nextimagemedical.com");
			myET.setEmailSubject(theSubject);
			myET.setEmailBody(theBodyFinal);
			myET.setEmailBodyType(emailType_V3.HTML_TYPE);
			// System.out.println("Body Type: " + myETSend.getBodyType());
			myET.commitData();

		} catch (Exception e) {
			System.out.println(e);
		}

	}

}
