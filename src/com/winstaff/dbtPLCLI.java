

package com.winstaff;

import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public class dbtPLCLI extends Object implements InttPLCLI
{

        db_NewBase    dbnbDB;

    public dbtPLCLI()
    {
        dbnbDB = new db_NewBase( "tPLCLI", "PLCID" );

    }    // End of default constructor

    public dbtPLCLI( Integer iNewID )
    {
        dbnbDB = new db_NewBase( "tPLCLI", "PLCID" );
        setUniqueID( iNewID );
    }    // End of Constructor knowing an ID

    public void commitData() throws SQLException
    {
            //no longer trapping/ignoring errors 2011-03-17
            dbnbDB.commitData();
    }    // End of commitData()

    public void commitDataForced() throws SQLException
    {
        dbnbDB.commitDataForced();
    }    // End of commitData()

    public Integer getUniqueID()
    {
        return dbnbDB.getUniqueID();
    }    // End of getUniqueID()

    public void setUniqueID( Integer iNewID )
    {
        dbnbDB.setUniqueID( iNewID );
    }    // End of setUniqueID()

    public void setModifyComments( String sNew )
    {
        dbnbDB.setModifyComments( sNew );
    }    // End of setUniqueID()


    public void setPLCID(Integer newValue)
    {
                dbnbDB.setFieldData( "PLCID", newValue.toString() );
    }

    public Integer getPLCID()
    {
        String           sValue = dbnbDB.getFieldData( "PLCID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setField(String fieldN, Object fieldV)
    {DebugLogger.println("setField Method not implemented yet in class!");}

    public String getFieldType(String fieldN)
    {return null;}


    public void setPLCShort(String newValue)
    {
                dbnbDB.setFieldData( "PLCShort", newValue.toString() );
    }

    public String getPLCShort()
    {
        String           sValue = dbnbDB.getFieldData( "PLCShort" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setPLCLong(String newValue)
    {
                dbnbDB.setFieldData( "PLCLong", newValue.toString() );
    }

    public String getPLCLong()
    {
        String           sValue = dbnbDB.getFieldData( "PLCLong" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setCompanyID(Integer newValue)
    {
                dbnbDB.setFieldData( "CompanyID", newValue.toString() );
    }

    public Integer getCompanyID()
    {
        String           sValue = dbnbDB.getFieldData( "CompanyID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setAdminID(Integer newValue)
    {
                dbnbDB.setFieldData( "AdminID", newValue.toString() );
    }

    public Integer getAdminID()
    {
        String           sValue = dbnbDB.getFieldData( "AdminID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setHCOID(Integer newValue)
    {
                dbnbDB.setFieldData( "HCOID", newValue.toString() );
    }

    public Integer getHCOID()
    {
        String           sValue = dbnbDB.getFieldData( "HCOID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

}    // End of bltPLCLI class definition
