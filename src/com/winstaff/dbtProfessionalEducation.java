

package com.winstaff;

import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public class dbtProfessionalEducation extends Object implements InttProfessionalEducation
{

        db_NewBase    dbnbDB;

    public dbtProfessionalEducation()
    {
        dbnbDB = new db_NewBase( "tProfessionalEducation", "ProfessionalEducationID" );

    }    // End of default constructor

    public dbtProfessionalEducation( Integer iNewID )
    {
        dbnbDB = new db_NewBase( "tProfessionalEducation", "ProfessionalEducationID" );
        setUniqueID( iNewID );
    }    // End of Constructor knowing an ID

    public void commitData() throws SQLException
    {
            //no longer trapping/ignoring errors 2011-03-17
            dbnbDB.commitData();
    }    // End of commitData()

    public void commitDataForced() throws SQLException
    {
        dbnbDB.commitDataForced();
    }    // End of commitData()

    public Integer getUniqueID()
    {
        return dbnbDB.getUniqueID();
    }    // End of getUniqueID()

    public void setUniqueID( Integer iNewID )
    {
        dbnbDB.setUniqueID( iNewID );
    }    // End of setUniqueID()

    public void setModifyComments( String sNew )
    {
        dbnbDB.setModifyComments( sNew );
    }    // End of setUniqueID()


    public void setProfessionalEducationID(Integer newValue)
    {
                dbnbDB.setFieldData( "ProfessionalEducationID", newValue.toString() );
    }

    public Integer getProfessionalEducationID()
    {
        String           sValue = dbnbDB.getFieldData( "ProfessionalEducationID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setField(String fieldN, Object fieldV)
    {DebugLogger.println("setField Method not implemented yet in classProfessionalEducation!");}

    public String getFieldType(String fieldN)
    {return null;}


    public void setUniqueCreateDate(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "UniqueCreateDate", formatter.format( newValue ) );
    }

    public Date getUniqueCreateDate()
    {
        String           sValue = dbnbDB.getFieldData( "UniqueCreateDate" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setUniqueModifyDate(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "UniqueModifyDate", formatter.format( newValue ) );
    }

    public Date getUniqueModifyDate()
    {
        String           sValue = dbnbDB.getFieldData( "UniqueModifyDate" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setUniqueModifyComments(String newValue)
    {
                dbnbDB.setFieldData( "UniqueModifyComments", newValue.toString() );
    }

    public String getUniqueModifyComments()
    {
        String           sValue = dbnbDB.getFieldData( "UniqueModifyComments" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setPhysicianID(Integer newValue)
    {
                dbnbDB.setFieldData( "PhysicianID", newValue.toString() );
    }

    public Integer getPhysicianID()
    {
        String           sValue = dbnbDB.getFieldData( "PhysicianID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setDegreeID(Integer newValue)
    {
                dbnbDB.setFieldData( "DegreeID", newValue.toString() );
    }

    public Integer getDegreeID()
    {
        String           sValue = dbnbDB.getFieldData( "DegreeID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setOther(String newValue)
    {
                dbnbDB.setFieldData( "Other", newValue.toString() );
    }

    public String getOther()
    {
        String           sValue = dbnbDB.getFieldData( "Other" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setStartDate(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "StartDate", formatter.format( newValue ) );
    }

    public Date getStartDate()
    {
        String           sValue = dbnbDB.getFieldData( "StartDate" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setDateOfGraduation(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "DateOfGraduation", formatter.format( newValue ) );
    }

    public Date getDateOfGraduation()
    {
        String           sValue = dbnbDB.getFieldData( "DateOfGraduation" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setEndDate(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "EndDate", formatter.format( newValue ) );
    }

    public Date getEndDate()
    {
        String           sValue = dbnbDB.getFieldData( "EndDate" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setFocus(String newValue)
    {
                dbnbDB.setFieldData( "Focus", newValue.toString() );
    }

    public String getFocus()
    {
        String           sValue = dbnbDB.getFieldData( "Focus" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setSchoolName(String newValue)
    {
                dbnbDB.setFieldData( "SchoolName", newValue.toString() );
    }

    public String getSchoolName()
    {
        String           sValue = dbnbDB.getFieldData( "SchoolName" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setSchoolAddress1(String newValue)
    {
                dbnbDB.setFieldData( "SchoolAddress1", newValue.toString() );
    }

    public String getSchoolAddress1()
    {
        String           sValue = dbnbDB.getFieldData( "SchoolAddress1" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setSchoolAddress2(String newValue)
    {
                dbnbDB.setFieldData( "SchoolAddress2", newValue.toString() );
    }

    public String getSchoolAddress2()
    {
        String           sValue = dbnbDB.getFieldData( "SchoolAddress2" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setSchoolCity(String newValue)
    {
                dbnbDB.setFieldData( "SchoolCity", newValue.toString() );
    }

    public String getSchoolCity()
    {
        String           sValue = dbnbDB.getFieldData( "SchoolCity" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setSchoolStateID(Integer newValue)
    {
                dbnbDB.setFieldData( "SchoolStateID", newValue.toString() );
    }

    public Integer getSchoolStateID()
    {
        String           sValue = dbnbDB.getFieldData( "SchoolStateID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setSchoolProvince(String newValue)
    {
                dbnbDB.setFieldData( "SchoolProvince", newValue.toString() );
    }

    public String getSchoolProvince()
    {
        String           sValue = dbnbDB.getFieldData( "SchoolProvince" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setSchoolZIP(String newValue)
    {
                dbnbDB.setFieldData( "SchoolZIP", newValue.toString() );
    }

    public String getSchoolZIP()
    {
        String           sValue = dbnbDB.getFieldData( "SchoolZIP" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setSchoolCountryID(Integer newValue)
    {
                dbnbDB.setFieldData( "SchoolCountryID", newValue.toString() );
    }

    public Integer getSchoolCountryID()
    {
        String           sValue = dbnbDB.getFieldData( "SchoolCountryID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setSchoolPhone(String newValue)
    {
                dbnbDB.setFieldData( "SchoolPhone", newValue.toString() );
    }

    public String getSchoolPhone()
    {
        String           sValue = dbnbDB.getFieldData( "SchoolPhone" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setSchoolFax(String newValue)
    {
                dbnbDB.setFieldData( "SchoolFax", newValue.toString() );
    }

    public String getSchoolFax()
    {
        String           sValue = dbnbDB.getFieldData( "SchoolFax" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setContactName(String newValue)
    {
                dbnbDB.setFieldData( "ContactName", newValue.toString() );
    }

    public String getContactName()
    {
        String           sValue = dbnbDB.getFieldData( "ContactName" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setContactEmail(String newValue)
    {
                dbnbDB.setFieldData( "ContactEmail", newValue.toString() );
    }

    public String getContactEmail()
    {
        String           sValue = dbnbDB.getFieldData( "ContactEmail" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setComments(String newValue)
    {
                dbnbDB.setFieldData( "Comments", newValue.toString() );
    }

    public String getComments()
    {
        String           sValue = dbnbDB.getFieldData( "Comments" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

}    // End of bltProfessionalEducation class definition
