

package com.winstaff;

import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public class dbtNIM3_SchedulingTransaction extends Object implements InttNIM3_SchedulingTransaction
{

        db_NewBase    dbnbDB;

    public dbtNIM3_SchedulingTransaction()
    {
        dbnbDB = new db_NewBase( "tNIM3_SchedulingTransaction", "SchedulingTransactionID" );

    }    // End of default constructor

    public dbtNIM3_SchedulingTransaction( Integer iNewID )
    {
        dbnbDB = new db_NewBase( "tNIM3_SchedulingTransaction", "SchedulingTransactionID" );
        setUniqueID( iNewID );
    }    // End of Constructor knowing an ID

    public void commitData() throws SQLException
    {
            //no longer trapping/ignoring errors 2011-03-17
            dbnbDB.commitData();
    }    // End of commitData()

    public void commitDataForced() throws SQLException
    {
        dbnbDB.commitDataForced();
    }    // End of commitData()

    public Integer getUniqueID()
    {
        return dbnbDB.getUniqueID();
    }    // End of getUniqueID()

    public void setUniqueID( Integer iNewID )
    {
        dbnbDB.setUniqueID( iNewID );
    }    // End of setUniqueID()

    public void setModifyComments( String sNew )
    {
        dbnbDB.setModifyComments( sNew );
    }    // End of setUniqueID()


    public void setSchedulingTransactionID(Integer newValue)
    {
                dbnbDB.setFieldData( "SchedulingTransactionID", newValue.toString() );
    }

    public Integer getSchedulingTransactionID()
    {
        String           sValue = dbnbDB.getFieldData( "SchedulingTransactionID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setField(String fieldN, Object fieldV)
    {DebugLogger.println("setField Method not implemented yet in classNIM3_SchedulingTransaction!");}

    public String getFieldType(String fieldN)
    {return null;}


    public void setUniqueCreateDate(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "UniqueCreateDate", formatter.format( newValue ) );
    }

    public Date getUniqueCreateDate()
    {
        String           sValue = dbnbDB.getFieldData( "UniqueCreateDate" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setUniqueModifyDate(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "UniqueModifyDate", formatter.format( newValue ) );
    }

    public Date getUniqueModifyDate()
    {
        String           sValue = dbnbDB.getFieldData( "UniqueModifyDate" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setUniqueModifyComments(String newValue)
    {
                dbnbDB.setFieldData( "UniqueModifyComments", newValue.toString() );
    }

    public String getUniqueModifyComments()
    {
        String           sValue = dbnbDB.getFieldData( "UniqueModifyComments" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setPracticeID(Integer newValue)
    {
                dbnbDB.setFieldData( "PracticeID", newValue.toString() );
    }

    public Integer getPracticeID()
    {
        String           sValue = dbnbDB.getFieldData( "PracticeID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setTransactionDate(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "TransactionDate", formatter.format( newValue ) );
    }

    public Date getTransactionDate()
    {
        String           sValue = dbnbDB.getFieldData( "TransactionDate" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setActionID(Integer newValue)
    {
                dbnbDB.setFieldData( "ActionID", newValue.toString() );
    }

    public Integer getActionID()
    {
        String           sValue = dbnbDB.getFieldData( "ActionID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setUserID(Integer newValue)
    {
                dbnbDB.setFieldData( "UserID", newValue.toString() );
    }

    public Integer getUserID()
    {
        String           sValue = dbnbDB.getFieldData( "UserID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setComments(String newValue)
    {
                dbnbDB.setFieldData( "Comments", newValue.toString() );
    }

    public String getComments()
    {
        String           sValue = dbnbDB.getFieldData( "Comments" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setCompareAddress1(String newValue)
    {
                dbnbDB.setFieldData( "CompareAddress1", newValue.toString() );
    }

    public String getCompareAddress1()
    {
        String           sValue = dbnbDB.getFieldData( "CompareAddress1" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setCompareAddress2(String newValue)
    {
                dbnbDB.setFieldData( "CompareAddress2", newValue.toString() );
    }

    public String getCompareAddress2()
    {
        String           sValue = dbnbDB.getFieldData( "CompareAddress2" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setCompareCity(String newValue)
    {
                dbnbDB.setFieldData( "CompareCity", newValue.toString() );
    }

    public String getCompareCity()
    {
        String           sValue = dbnbDB.getFieldData( "CompareCity" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setCompareStateLI(String newValue)
    {
                dbnbDB.setFieldData( "CompareStateLI", newValue.toString() );
    }

    public String getCompareStateLI()
    {
        String           sValue = dbnbDB.getFieldData( "CompareStateLI" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setCompareStateCode(String newValue)
    {
                dbnbDB.setFieldData( "CompareStateCode", newValue.toString() );
    }

    public String getCompareStateCode()
    {
        String           sValue = dbnbDB.getFieldData( "CompareStateCode" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setCompareZip(String newValue)
    {
                dbnbDB.setFieldData( "CompareZip", newValue.toString() );
    }

    public String getCompareZip()
    {
        String           sValue = dbnbDB.getFieldData( "CompareZip" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setCompareCounty(String newValue)
    {
                dbnbDB.setFieldData( "CompareCounty", newValue.toString() );
    }

    public String getCompareCounty()
    {
        String           sValue = dbnbDB.getFieldData( "CompareCounty" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setCompareEncounterRefID(Integer newValue)
    {
                dbnbDB.setFieldData( "CompareEncounterRefID", newValue.toString() );
    }

    public Integer getCompareEncounterRefID()
    {
        String           sValue = dbnbDB.getFieldData( "CompareEncounterRefID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setTotalResults(Integer newValue)
    {
                dbnbDB.setFieldData( "TotalResults", newValue.toString() );
    }

    public Integer getTotalResults()
    {
        String           sValue = dbnbDB.getFieldData( "TotalResults" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setProcedureDescription(String newValue)
    {
                dbnbDB.setFieldData( "ProcedureDescription", newValue.toString() );
    }

    public String getProcedureDescription()
    {
        String           sValue = dbnbDB.getFieldData( "ProcedureDescription" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

}    // End of bltNIM3_SchedulingTransaction class definition
