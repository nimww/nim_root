

package com.winstaff;

import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;
import java.util.Enumeration;

public class bltNIM3_Service extends Object implements InttNIM3_Service
{

    dbtNIM3_Service    dbDB;

    public bltNIM3_Service()
    {
        dbDB = new dbtNIM3_Service();
        dbDB.setUniqueCreateDate(new java.util.Date());
        makeRequiredHash();
        makeExpiredHash();
        makeEnglishHash();

    }    // End of default constructor

    public bltNIM3_Service( Integer iNewID )
    {        dbDB = new dbtNIM3_Service( iNewID );
        makeRequiredHash();
        makeExpiredHash();
        makeEnglishHash();
        iSecurityCheck = new Integer(2);
    }    // End of Constructor knowing an ID


    public bltNIM3_Service( Integer iNewID, Integer iGroupSecurityID )
    {
        this.GroupSecurityInit(iGroupSecurityID);
        dbDB = new dbtNIM3_Service( iNewID );
        makeExpiredHash();
        makeEnglishHash();
        makeRequiredHash(iGroupSecurityID);
    }    // End of Constructor knowing an ID

    public bltNIM3_Service( Integer iGroupSecurityID, boolean isTrue )
    {
        this.GroupSecurityInit(iGroupSecurityID);
        dbDB = new dbtNIM3_Service(  );
        makeExpiredHash();
        makeEnglishHash();
        makeRequiredHash(iGroupSecurityID);
    }    // End of Security unknown ID

    public void GroupSecurityInit( Integer iGroupSecurityID )
    {
        bltSecurityGroupMaster mySGM = new bltSecurityGroupMaster(iGroupSecurityID);
        this.UserSecurityID = iGroupSecurityID;
        if (mySGM.getRunFieldSecurityID().intValue()==1)
        {
            this.runFieldSecurity = true;
            makeRequiredHash(iGroupSecurityID);
        }
        iSecurityCheck = SecurityCheck.CheckItem("nim1", iGroupSecurityID);
    }  

    public EventReferenceType getEventReference()
    {
        return EventUtils.getEventReference("tNIM3_Service", this.getUniqueID());
    }  

    public String convertString2LogString(String normalString) 
    {
        String myVal = "";
        for (int i=0;i<normalString.length();i++)
        {
            if ( (normalString.charAt(i)!='\r')&&(normalString.charAt(i)!='\n')&&(normalString.charAt(i)!='\t') )
            {
                myVal+=normalString.charAt(i);
            }
            else
            {
                myVal+="<NLT>";
            }
        }
        return myVal;
    }
    public void runAudit()
    {
        String AuditString = new java.util.Date()+" \t " ;  
        AuditString += " tNIM3_Service "; 
        AuditString += " ServiceID ="+this.getUniqueID(); 
        AuditString += " EncounterID ="+this.getEncounterID(); 
        for (int i=0;i<this.AuditVector.size();i++)
        {
            AuditString+="\t"+convertString2LogString((String)this.AuditVector.elementAt(i));
        }
        AuditLogger.printLine(AuditString);
    }
    public void commitData() throws SQLException
    {
        runAudit();
        dbDB.setModifyComments(this.getUniqueModifyComments());
        dbDB.setUniqueModifyDate(new java.util.Date());
        dbDB.commitData();
        DataControlUtils.dataChanged("tNIM3_Service",this.getEncounterID() ,this.AuditVector.size(), "EncounterID");
        this.AuditVector.removeAllElements();
    }    // End of commitData()

    public void commitDataForced() throws SQLException
    {
        dbDB.commitDataForced();
    }    // End of commitData()

    public Integer getUniqueID()
    {
        return dbDB.getUniqueID();
    }    // End of getUniqueID()

    public void setUniqueID( Integer iNewID )
    {
        dbDB.setUniqueID( iNewID );
    }    // End of setUniqueID()


    public void setServiceID(Integer newValue)
    {
        dbDB.setServiceID(newValue);
    }

    public Integer getServiceID()
    {
        return dbDB.getServiceID();
    }

    java.util.Vector AuditVector = new java.util.Vector();

    public void setUniqueCreateDate(Date newValue)
    {
        this.setUniqueCreateDate(newValue,this.UserSecurityID);


    }
    public Date getUniqueCreateDate()
    {
        return this.getUniqueCreateDate(this.UserSecurityID);
    }

    public void setUniqueCreateDate(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='UniqueCreateDate' AND TableName='tNIM3_Service'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[UniqueCreateDate]=["+newValue+"]");
                   dbDB.setUniqueCreateDate(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[UniqueCreateDate]=["+newValue+"]");
           dbDB.setUniqueCreateDate(newValue);
         }
    }
    public Date getUniqueCreateDate(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='UniqueCreateDate' AND TableName='tNIM3_Service'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getUniqueCreateDate();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getUniqueCreateDate();
         }
        return myVal;
    }

    public void setUniqueModifyDate(Date newValue)
    {
        this.setUniqueModifyDate(newValue,this.UserSecurityID);


    }
    public Date getUniqueModifyDate()
    {
        return this.getUniqueModifyDate(this.UserSecurityID);
    }

    public void setUniqueModifyDate(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='UniqueModifyDate' AND TableName='tNIM3_Service'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[UniqueModifyDate]=["+newValue+"]");
                   dbDB.setUniqueModifyDate(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[UniqueModifyDate]=["+newValue+"]");
           dbDB.setUniqueModifyDate(newValue);
         }
    }
    public Date getUniqueModifyDate(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='UniqueModifyDate' AND TableName='tNIM3_Service'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getUniqueModifyDate();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getUniqueModifyDate();
         }
        return myVal;
    }

    public void setUniqueModifyComments(String newValue)
    {
        this.setUniqueModifyComments(newValue,this.UserSecurityID);


    }
    public String getUniqueModifyComments()
    {
        return this.getUniqueModifyComments(this.UserSecurityID);
    }

    public void setUniqueModifyComments(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='UniqueModifyComments' AND TableName='tNIM3_Service'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[UniqueModifyComments]=["+newValue+"]");
                   dbDB.setUniqueModifyComments(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[UniqueModifyComments]=["+newValue+"]");
           dbDB.setUniqueModifyComments(newValue);
         }
    }
    public String getUniqueModifyComments(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='UniqueModifyComments' AND TableName='tNIM3_Service'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getUniqueModifyComments();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getUniqueModifyComments();
         }
        return myVal;
    }

    public void setEncounterID(Integer newValue)
    {
        this.setEncounterID(newValue,this.UserSecurityID);


    }
    public Integer getEncounterID()
    {
        return this.getEncounterID(this.UserSecurityID);
    }

    public void setEncounterID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='EncounterID' AND TableName='tNIM3_Service'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[EncounterID]=["+newValue+"]");
                   dbDB.setEncounterID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[EncounterID]=["+newValue+"]");
           dbDB.setEncounterID(newValue);
         }
    }
    public Integer getEncounterID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='EncounterID' AND TableName='tNIM3_Service'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getEncounterID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getEncounterID();
         }
        return myVal;
    }

    public void setServiceTypeID(Integer newValue)
    {
        this.setServiceTypeID(newValue,this.UserSecurityID);


    }
    public Integer getServiceTypeID()
    {
        return this.getServiceTypeID(this.UserSecurityID);
    }

    public void setServiceTypeID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ServiceTypeID' AND TableName='tNIM3_Service'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[ServiceTypeID]=["+newValue+"]");
                   dbDB.setServiceTypeID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[ServiceTypeID]=["+newValue+"]");
           dbDB.setServiceTypeID(newValue);
         }
    }
    public Integer getServiceTypeID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ServiceTypeID' AND TableName='tNIM3_Service'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getServiceTypeID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getServiceTypeID();
         }
        return myVal;
    }

    public void setDateOfService(Date newValue)
    {
        this.setDateOfService(newValue,this.UserSecurityID);


    }
    public Date getDateOfService()
    {
        return this.getDateOfService(this.UserSecurityID);
    }

    public void setDateOfService(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='DateOfService' AND TableName='tNIM3_Service'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[DateOfService]=["+newValue+"]");
                   dbDB.setDateOfService(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[DateOfService]=["+newValue+"]");
           dbDB.setDateOfService(newValue);
         }
    }
    public Date getDateOfService(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='DateOfService' AND TableName='tNIM3_Service'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getDateOfService();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getDateOfService();
         }
        return myVal;
    }

    public void setAttendingPhysicianID(Integer newValue)
    {
        this.setAttendingPhysicianID(newValue,this.UserSecurityID);


    }
    public Integer getAttendingPhysicianID()
    {
        return this.getAttendingPhysicianID(this.UserSecurityID);
    }

    public void setAttendingPhysicianID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AttendingPhysicianID' AND TableName='tNIM3_Service'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[AttendingPhysicianID]=["+newValue+"]");
                   dbDB.setAttendingPhysicianID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[AttendingPhysicianID]=["+newValue+"]");
           dbDB.setAttendingPhysicianID(newValue);
         }
    }
    public Integer getAttendingPhysicianID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AttendingPhysicianID' AND TableName='tNIM3_Service'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getAttendingPhysicianID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getAttendingPhysicianID();
         }
        return myVal;
    }

    public void setReferringPhysicianID(Integer newValue)
    {
        this.setReferringPhysicianID(newValue,this.UserSecurityID);


    }
    public Integer getReferringPhysicianID()
    {
        return this.getReferringPhysicianID(this.UserSecurityID);
    }

    public void setReferringPhysicianID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ReferringPhysicianID' AND TableName='tNIM3_Service'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[ReferringPhysicianID]=["+newValue+"]");
                   dbDB.setReferringPhysicianID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[ReferringPhysicianID]=["+newValue+"]");
           dbDB.setReferringPhysicianID(newValue);
         }
    }
    public Integer getReferringPhysicianID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ReferringPhysicianID' AND TableName='tNIM3_Service'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getReferringPhysicianID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getReferringPhysicianID();
         }
        return myVal;
    }

    public void setCPT(String newValue)
    {
        this.setCPT(newValue,this.UserSecurityID);


    }
    public String getCPT()
    {
        return this.getCPT(this.UserSecurityID);
    }

    public void setCPT(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CPT' AND TableName='tNIM3_Service'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[CPT]=["+newValue+"]");
                   dbDB.setCPT(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[CPT]=["+newValue+"]");
           dbDB.setCPT(newValue);
         }
    }
    public String getCPT(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CPT' AND TableName='tNIM3_Service'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getCPT();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getCPT();
         }
        return myVal;
    }

    public void setCPTModifier(String newValue)
    {
        this.setCPTModifier(newValue,this.UserSecurityID);


    }
    
    public String getCPTModifier()
    {
        return this.getCPTModifier(this.UserSecurityID);
    }

    public void setCPTModifier(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CPTModifier' AND TableName='tNIM3_Service'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[CPTModifier]=["+newValue+"]");
                   dbDB.setCPTModifier(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[CPTModifier]=["+newValue+"]");
           dbDB.setCPTModifier(newValue);
         }
    }
    public String getCPTModifier(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CPTModifier' AND TableName='tNIM3_Service'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getCPTModifier();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getCPTModifier();
         }
        return myVal;
    }

    public void setCPTBodyPart(String newValue)
    {
        this.setCPTBodyPart(newValue,this.UserSecurityID);


    }
    public String getCPTBodyPart()
    {
        return this.getCPTBodyPart(this.UserSecurityID);
    }

    public void setCPTBodyPart(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CPTBodyPart' AND TableName='tNIM3_Service'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[CPTBodyPart]=["+newValue+"]");
                   dbDB.setCPTBodyPart(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[CPTBodyPart]=["+newValue+"]");
           dbDB.setCPTBodyPart(newValue);
         }
    }
    public String getCPTBodyPart(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CPTBodyPart' AND TableName='tNIM3_Service'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getCPTBodyPart();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getCPTBodyPart();
         }
        return myVal;
    }

    public void setdCPT1(String newValue)
    {
        this.setdCPT1(newValue,this.UserSecurityID);


    }
    public String getdCPT1()
    {
        return this.getdCPT1(this.UserSecurityID);
    }

    public void setdCPT1(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='dCPT1' AND TableName='tNIM3_Service'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[dCPT1]=["+newValue+"]");
                   dbDB.setdCPT1(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[dCPT1]=["+newValue+"]");
           dbDB.setdCPT1(newValue);
         }
    }
    public String getdCPT1(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='dCPT1' AND TableName='tNIM3_Service'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getdCPT1();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getdCPT1();
         }
        return myVal;
    }

    public void setdCPT2(String newValue)
    {
        this.setdCPT2(newValue,this.UserSecurityID);


    }
    public String getdCPT2()
    {
        return this.getdCPT2(this.UserSecurityID);
    }

    public void setdCPT2(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='dCPT2' AND TableName='tNIM3_Service'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[dCPT2]=["+newValue+"]");
                   dbDB.setdCPT2(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[dCPT2]=["+newValue+"]");
           dbDB.setdCPT2(newValue);
         }
    }
    public String getdCPT2(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='dCPT2' AND TableName='tNIM3_Service'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getdCPT2();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getdCPT2();
         }
        return myVal;
    }

    public void setdCPT3(String newValue)
    {
        this.setdCPT3(newValue,this.UserSecurityID);


    }
    public String getdCPT3()
    {
        return this.getdCPT3(this.UserSecurityID);
    }

    public void setdCPT3(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='dCPT3' AND TableName='tNIM3_Service'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[dCPT3]=["+newValue+"]");
                   dbDB.setdCPT3(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[dCPT3]=["+newValue+"]");
           dbDB.setdCPT3(newValue);
         }
    }
    public String getdCPT3(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='dCPT3' AND TableName='tNIM3_Service'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getdCPT3();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getdCPT3();
         }
        return myVal;
    }

    public void setdCPT4(String newValue)
    {
        this.setdCPT4(newValue,this.UserSecurityID);


    }
    public String getdCPT4()
    {
        return this.getdCPT4(this.UserSecurityID);
    }

    public void setdCPT4(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='dCPT4' AND TableName='tNIM3_Service'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[dCPT4]=["+newValue+"]");
                   dbDB.setdCPT4(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[dCPT4]=["+newValue+"]");
           dbDB.setdCPT4(newValue);
         }
    }
    public String getdCPT4(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='dCPT4' AND TableName='tNIM3_Service'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getdCPT4();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getdCPT4();
         }
        return myVal;
    }

    public void setCPTText(String newValue)
    {
        this.setCPTText(newValue,this.UserSecurityID);


    }
    public String getCPTText()
    {
        return this.getCPTText(this.UserSecurityID);
    }

    public void setCPTText(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CPTText' AND TableName='tNIM3_Service'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[CPTText]=["+newValue+"]");
                   dbDB.setCPTText(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[CPTText]=["+newValue+"]");
           dbDB.setCPTText(newValue);
         }
    }
    public String getCPTText(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CPTText' AND TableName='tNIM3_Service'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getCPTText();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getCPTText();
         }
        return myVal;
    }

    public void setBillAmount(Double newValue)
    {
        this.setBillAmount(newValue,this.UserSecurityID);


    }
    public Double getBillAmount()
    {
        return this.getBillAmount(this.UserSecurityID);
    }

    public void setBillAmount(Double newValue, Integer GroupRefID)
    {
        Double myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='BillAmount' AND TableName='tNIM3_Service'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[BillAmount]=["+newValue+"]");
                   dbDB.setBillAmount(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[BillAmount]=["+newValue+"]");
           dbDB.setBillAmount(newValue);
         }
    }
    public Double getBillAmount(Integer GroupRefID)
    {
        Double myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='BillAmount' AND TableName='tNIM3_Service'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getBillAmount();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getBillAmount();
         }
        return myVal;
    }

    public void setAllowAmount(Double newValue)
    {
        this.setAllowAmount(newValue,this.UserSecurityID);


    }
    public Double getAllowAmount()
    {
        return this.getAllowAmount(this.UserSecurityID);
    }

    public void setAllowAmount(Double newValue, Integer GroupRefID)
    {
        Double myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AllowAmount' AND TableName='tNIM3_Service'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[AllowAmount]=["+newValue+"]");
                   dbDB.setAllowAmount(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[AllowAmount]=["+newValue+"]");
           dbDB.setAllowAmount(newValue);
         }
    }
    public Double getAllowAmount(Integer GroupRefID)
    {
        Double myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AllowAmount' AND TableName='tNIM3_Service'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getAllowAmount();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getAllowAmount();
         }
        return myVal;
    }

    public void setReceivedAmount(Double newValue)
    {
        this.setReceivedAmount(newValue,this.UserSecurityID);


    }
    public Double getReceivedAmount()
    {
        return this.getReceivedAmount(this.UserSecurityID);
    }

    public void setReceivedAmount(Double newValue, Integer GroupRefID)
    {
        Double myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ReceivedAmount' AND TableName='tNIM3_Service'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[ReceivedAmount]=["+newValue+"]");
                   dbDB.setReceivedAmount(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[ReceivedAmount]=["+newValue+"]");
           dbDB.setReceivedAmount(newValue);
         }
    }
    public Double getReceivedAmount(Integer GroupRefID)
    {
        Double myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ReceivedAmount' AND TableName='tNIM3_Service'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getReceivedAmount();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getReceivedAmount();
         }
        return myVal;
    }

    public void setPaidOutAmount(Double newValue)
    {
        this.setPaidOutAmount(newValue,this.UserSecurityID);


    }
    public Double getPaidOutAmount()
    {
        return this.getPaidOutAmount(this.UserSecurityID);
    }

    public void setPaidOutAmount(Double newValue, Integer GroupRefID)
    {
        Double myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PaidOutAmount' AND TableName='tNIM3_Service'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[PaidOutAmount]=["+newValue+"]");
                   dbDB.setPaidOutAmount(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[PaidOutAmount]=["+newValue+"]");
           dbDB.setPaidOutAmount(newValue);
         }
    }
    public Double getPaidOutAmount(Integer GroupRefID)
    {
        Double myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PaidOutAmount' AND TableName='tNIM3_Service'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPaidOutAmount();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPaidOutAmount();
         }
        return myVal;
    }

    public void setReceivedCheck1Number(String newValue)
    {
        this.setReceivedCheck1Number(newValue,this.UserSecurityID);


    }
    public String getReceivedCheck1Number()
    {
        return this.getReceivedCheck1Number(this.UserSecurityID);
    }

    public void setReceivedCheck1Number(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ReceivedCheck1Number' AND TableName='tNIM3_Service'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[ReceivedCheck1Number]=["+newValue+"]");
                   dbDB.setReceivedCheck1Number(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[ReceivedCheck1Number]=["+newValue+"]");
           dbDB.setReceivedCheck1Number(newValue);
         }
    }
    public String getReceivedCheck1Number(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ReceivedCheck1Number' AND TableName='tNIM3_Service'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getReceivedCheck1Number();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getReceivedCheck1Number();
         }
        return myVal;
    }

    public void setReceivedCheck1Amount(Double newValue)
    {
        this.setReceivedCheck1Amount(newValue,this.UserSecurityID);


    }
    public Double getReceivedCheck1Amount()
    {
        return this.getReceivedCheck1Amount(this.UserSecurityID);
    }

    public void setReceivedCheck1Amount(Double newValue, Integer GroupRefID)
    {
        Double myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ReceivedCheck1Amount' AND TableName='tNIM3_Service'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[ReceivedCheck1Amount]=["+newValue+"]");
                   dbDB.setReceivedCheck1Amount(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[ReceivedCheck1Amount]=["+newValue+"]");
           dbDB.setReceivedCheck1Amount(newValue);
         }
    }
    public Double getReceivedCheck1Amount(Integer GroupRefID)
    {
        Double myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ReceivedCheck1Amount' AND TableName='tNIM3_Service'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getReceivedCheck1Amount();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getReceivedCheck1Amount();
         }
        return myVal;
    }

    public void setReceivedCheck1Date(Date newValue)
    {
        this.setReceivedCheck1Date(newValue,this.UserSecurityID);


    }
    public Date getReceivedCheck1Date()
    {
        return this.getReceivedCheck1Date(this.UserSecurityID);
    }

    public void setReceivedCheck1Date(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ReceivedCheck1Date' AND TableName='tNIM3_Service'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[ReceivedCheck1Date]=["+newValue+"]");
                   dbDB.setReceivedCheck1Date(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[ReceivedCheck1Date]=["+newValue+"]");
           dbDB.setReceivedCheck1Date(newValue);
         }
    }
    public Date getReceivedCheck1Date(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ReceivedCheck1Date' AND TableName='tNIM3_Service'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getReceivedCheck1Date();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getReceivedCheck1Date();
         }
        return myVal;
    }

    public void setReceivedCheck2Number(String newValue)
    {
        this.setReceivedCheck2Number(newValue,this.UserSecurityID);


    }
    public String getReceivedCheck2Number()
    {
        return this.getReceivedCheck2Number(this.UserSecurityID);
    }

    public void setReceivedCheck2Number(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ReceivedCheck2Number' AND TableName='tNIM3_Service'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[ReceivedCheck2Number]=["+newValue+"]");
                   dbDB.setReceivedCheck2Number(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[ReceivedCheck2Number]=["+newValue+"]");
           dbDB.setReceivedCheck2Number(newValue);
         }
    }
    public String getReceivedCheck2Number(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ReceivedCheck2Number' AND TableName='tNIM3_Service'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getReceivedCheck2Number();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getReceivedCheck2Number();
         }
        return myVal;
    }

    public void setReceivedCheck2Amount(Double newValue)
    {
        this.setReceivedCheck2Amount(newValue,this.UserSecurityID);


    }
    public Double getReceivedCheck2Amount()
    {
        return this.getReceivedCheck2Amount(this.UserSecurityID);
    }

    public void setReceivedCheck2Amount(Double newValue, Integer GroupRefID)
    {
        Double myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ReceivedCheck2Amount' AND TableName='tNIM3_Service'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[ReceivedCheck2Amount]=["+newValue+"]");
                   dbDB.setReceivedCheck2Amount(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[ReceivedCheck2Amount]=["+newValue+"]");
           dbDB.setReceivedCheck2Amount(newValue);
         }
    }
    public Double getReceivedCheck2Amount(Integer GroupRefID)
    {
        Double myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ReceivedCheck2Amount' AND TableName='tNIM3_Service'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getReceivedCheck2Amount();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getReceivedCheck2Amount();
         }
        return myVal;
    }

    public void setReceivedCheck2Date(Date newValue)
    {
        this.setReceivedCheck2Date(newValue,this.UserSecurityID);


    }
    public Date getReceivedCheck2Date()
    {
        return this.getReceivedCheck2Date(this.UserSecurityID);
    }

    public void setReceivedCheck2Date(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ReceivedCheck2Date' AND TableName='tNIM3_Service'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[ReceivedCheck2Date]=["+newValue+"]");
                   dbDB.setReceivedCheck2Date(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[ReceivedCheck2Date]=["+newValue+"]");
           dbDB.setReceivedCheck2Date(newValue);
         }
    }
    public Date getReceivedCheck2Date(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ReceivedCheck2Date' AND TableName='tNIM3_Service'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getReceivedCheck2Date();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getReceivedCheck2Date();
         }
        return myVal;
    }

    public void setReceivedCheck3Number(String newValue)
    {
        this.setReceivedCheck3Number(newValue,this.UserSecurityID);


    }
    public String getReceivedCheck3Number()
    {
        return this.getReceivedCheck3Number(this.UserSecurityID);
    }

    public void setReceivedCheck3Number(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ReceivedCheck3Number' AND TableName='tNIM3_Service'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[ReceivedCheck3Number]=["+newValue+"]");
                   dbDB.setReceivedCheck3Number(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[ReceivedCheck3Number]=["+newValue+"]");
           dbDB.setReceivedCheck3Number(newValue);
         }
    }
    public String getReceivedCheck3Number(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ReceivedCheck3Number' AND TableName='tNIM3_Service'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getReceivedCheck3Number();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getReceivedCheck3Number();
         }
        return myVal;
    }

    public void setReceivedCheck3Amount(Double newValue)
    {
        this.setReceivedCheck3Amount(newValue,this.UserSecurityID);


    }
    public Double getReceivedCheck3Amount()
    {
        return this.getReceivedCheck3Amount(this.UserSecurityID);
    }

    public void setReceivedCheck3Amount(Double newValue, Integer GroupRefID)
    {
        Double myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ReceivedCheck3Amount' AND TableName='tNIM3_Service'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[ReceivedCheck3Amount]=["+newValue+"]");
                   dbDB.setReceivedCheck3Amount(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[ReceivedCheck3Amount]=["+newValue+"]");
           dbDB.setReceivedCheck3Amount(newValue);
         }
    }
    public Double getReceivedCheck3Amount(Integer GroupRefID)
    {
        Double myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ReceivedCheck3Amount' AND TableName='tNIM3_Service'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getReceivedCheck3Amount();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getReceivedCheck3Amount();
         }
        return myVal;
    }

    public void setReceivedCheck3Date(Date newValue)
    {
        this.setReceivedCheck3Date(newValue,this.UserSecurityID);


    }
    public Date getReceivedCheck3Date()
    {
        return this.getReceivedCheck3Date(this.UserSecurityID);
    }

    public void setReceivedCheck3Date(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ReceivedCheck3Date' AND TableName='tNIM3_Service'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[ReceivedCheck3Date]=["+newValue+"]");
                   dbDB.setReceivedCheck3Date(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[ReceivedCheck3Date]=["+newValue+"]");
           dbDB.setReceivedCheck3Date(newValue);
         }
    }
    public Date getReceivedCheck3Date(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ReceivedCheck3Date' AND TableName='tNIM3_Service'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getReceivedCheck3Date();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getReceivedCheck3Date();
         }
        return myVal;
    }

    public void setReportFileID(Integer newValue)
    {
        this.setReportFileID(newValue,this.UserSecurityID);


    }
    public Integer getReportFileID()
    {
        return this.getReportFileID(this.UserSecurityID);
    }

    public void setReportFileID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ReportFileID' AND TableName='tNIM3_Service'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[ReportFileID]=["+newValue+"]");
                   dbDB.setReportFileID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[ReportFileID]=["+newValue+"]");
           dbDB.setReportFileID(newValue);
         }
    }
    public Integer getReportFileID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ReportFileID' AND TableName='tNIM3_Service'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getReportFileID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getReportFileID();
         }
        return myVal;
    }

    public void setComments(String newValue)
    {
        this.setComments(newValue,this.UserSecurityID);


    }
    public String getComments()
    {
        return this.getComments(this.UserSecurityID);
    }

    public void setComments(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Comments' AND TableName='tNIM3_Service'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[Comments]=["+newValue+"]");
                   dbDB.setComments(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[Comments]=["+newValue+"]");
           dbDB.setComments(newValue);
         }
    }
    public String getComments(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Comments' AND TableName='tNIM3_Service'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getComments();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getComments();
         }
        return myVal;
    }

    public void setAuditNotes(String newValue)
    {
        this.setAuditNotes(newValue,this.UserSecurityID);


    }
    public String getAuditNotes()
    {
        return this.getAuditNotes(this.UserSecurityID);
    }

    public void setAuditNotes(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AuditNotes' AND TableName='tNIM3_Service'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[AuditNotes]=["+newValue+"]");
                   dbDB.setAuditNotes(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[AuditNotes]=["+newValue+"]");
           dbDB.setAuditNotes(newValue);
         }
    }
    public String getAuditNotes(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AuditNotes' AND TableName='tNIM3_Service'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getAuditNotes();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getAuditNotes();
         }
        return myVal;
    }

    public void setCPTWizardID(Integer newValue)
    {
        this.setCPTWizardID(newValue,this.UserSecurityID);


    }
    public Integer getCPTWizardID()
    {
        return this.getCPTWizardID(this.UserSecurityID);
    }

    public void setCPTWizardID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CPTWizardID' AND TableName='tNIM3_Service'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[CPTWizardID]=["+newValue+"]");
                   dbDB.setCPTWizardID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[CPTWizardID]=["+newValue+"]");
           dbDB.setCPTWizardID(newValue);
         }
    }
    public Integer getCPTWizardID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CPTWizardID' AND TableName='tNIM3_Service'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getCPTWizardID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getCPTWizardID();
         }
        return myVal;
    }

    public void setCPTQty(Integer newValue)
    {
        this.setCPTQty(newValue,this.UserSecurityID);


    }
    public Integer getCPTQty()
    {
        return this.getCPTQty(this.UserSecurityID);
    }

    public void setCPTQty(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CPTQty' AND TableName='tNIM3_Service'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[CPTQty]=["+newValue+"]");
                   dbDB.setCPTQty(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[CPTQty]=["+newValue+"]");
           dbDB.setCPTQty(newValue);
         }
    }
    public Integer getCPTQty(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CPTQty' AND TableName='tNIM3_Service'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getCPTQty();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getCPTQty();
         }
        return myVal;
    }

    public void setAllowAmountAdjustment(Double newValue)
    {
        this.setAllowAmountAdjustment(newValue,this.UserSecurityID);


    }
    public Double getAllowAmountAdjustment()
    {
        return this.getAllowAmountAdjustment(this.UserSecurityID);
    }

    public void setAllowAmountAdjustment(Double newValue, Integer GroupRefID)
    {
        Double myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AllowAmountAdjustment' AND TableName='tNIM3_Service'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[AllowAmountAdjustment]=["+newValue+"]");
                   dbDB.setAllowAmountAdjustment(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[AllowAmountAdjustment]=["+newValue+"]");
           dbDB.setAllowAmountAdjustment(newValue);
         }
    }
    public Double getAllowAmountAdjustment(Integer GroupRefID)
    {
        Double myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AllowAmountAdjustment' AND TableName='tNIM3_Service'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getAllowAmountAdjustment();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getAllowAmountAdjustment();
         }
        return myVal;
    }

    public void setServiceStatusID(Integer newValue)
    {
        this.setServiceStatusID(newValue,this.UserSecurityID);


    }
    public Integer getServiceStatusID()
    {
        return this.getServiceStatusID(this.UserSecurityID);
    }

    public void setServiceStatusID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ServiceStatusID' AND TableName='tNIM3_Service'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[ServiceStatusID]=["+newValue+"]");
                   dbDB.setServiceStatusID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[ServiceStatusID]=["+newValue+"]");
           dbDB.setServiceStatusID(newValue);
         }
    }
    public Integer getServiceStatusID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ServiceStatusID' AND TableName='tNIM3_Service'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getServiceStatusID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getServiceStatusID();
         }
        return myVal;
    }

    public void setDICOM_AccessionNumber(String newValue)
    {
        this.setDICOM_AccessionNumber(newValue,this.UserSecurityID);


    }
    public String getDICOM_AccessionNumber()
    {
        return this.getDICOM_AccessionNumber(this.UserSecurityID);
    }

    public void setDICOM_AccessionNumber(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='DICOM_AccessionNumber' AND TableName='tNIM3_Service'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[DICOM_AccessionNumber]=["+newValue+"]");
                   dbDB.setDICOM_AccessionNumber(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[DICOM_AccessionNumber]=["+newValue+"]");
           dbDB.setDICOM_AccessionNumber(newValue);
         }
    }
    public String getDICOM_AccessionNumber(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='DICOM_AccessionNumber' AND TableName='tNIM3_Service'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getDICOM_AccessionNumber();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getDICOM_AccessionNumber();
         }
        return myVal;
    }

    public void setDICOM_ShowImage(Integer newValue)
    {
        this.setDICOM_ShowImage(newValue,this.UserSecurityID);


    }
    public Integer getDICOM_ShowImage()
    {
        return this.getDICOM_ShowImage(this.UserSecurityID);
    }

    public void setDICOM_ShowImage(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='DICOM_ShowImage' AND TableName='tNIM3_Service'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[DICOM_ShowImage]=["+newValue+"]");
                   dbDB.setDICOM_ShowImage(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[DICOM_ShowImage]=["+newValue+"]");
           dbDB.setDICOM_ShowImage(newValue);
         }
    }
    public Integer getDICOM_ShowImage(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='DICOM_ShowImage' AND TableName='tNIM3_Service'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getDICOM_ShowImage();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getDICOM_ShowImage();
         }
        return myVal;
    }

    public void setDICOM_MRN(String newValue)
    {
        this.setDICOM_MRN(newValue,this.UserSecurityID);


    }
    public String getDICOM_MRN()
    {
        return this.getDICOM_MRN(this.UserSecurityID);
    }

    public void setDICOM_MRN(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='DICOM_MRN' AND TableName='tNIM3_Service'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[DICOM_MRN]=["+newValue+"]");
                   dbDB.setDICOM_MRN(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[DICOM_MRN]=["+newValue+"]");
           dbDB.setDICOM_MRN(newValue);
         }
    }
    public String getDICOM_MRN(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='DICOM_MRN' AND TableName='tNIM3_Service'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getDICOM_MRN();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getDICOM_MRN();
         }
        return myVal;
    }

    public void setDICOM_PN(String newValue)
    {
        this.setDICOM_PN(newValue,this.UserSecurityID);


    }
    public String getDICOM_PN()
    {
        return this.getDICOM_PN(this.UserSecurityID);
    }

    public void setDICOM_PN(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='DICOM_PN' AND TableName='tNIM3_Service'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[DICOM_PN]=["+newValue+"]");
                   dbDB.setDICOM_PN(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[DICOM_PN]=["+newValue+"]");
           dbDB.setDICOM_PN(newValue);
         }
    }
    public String getDICOM_PN(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='DICOM_PN' AND TableName='tNIM3_Service'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getDICOM_PN();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getDICOM_PN();
         }
        return myVal;
    }

    public void setDICOM_STUDYID(String newValue)
    {
        this.setDICOM_STUDYID(newValue,this.UserSecurityID);


    }
    public String getDICOM_STUDYID()
    {
        return this.getDICOM_STUDYID(this.UserSecurityID);
    }

    public void setDICOM_STUDYID(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='DICOM_STUDYID' AND TableName='tNIM3_Service'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[DICOM_STUDYID]=["+newValue+"]");
                   dbDB.setDICOM_STUDYID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[DICOM_STUDYID]=["+newValue+"]");
           dbDB.setDICOM_STUDYID(newValue);
         }
    }
    public String getDICOM_STUDYID(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='DICOM_STUDYID' AND TableName='tNIM3_Service'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getDICOM_STUDYID();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getDICOM_STUDYID();
         }
        return myVal;
    }

    public void setReadingPhysicianID(Integer newValue)
    {
        this.setReadingPhysicianID(newValue,this.UserSecurityID);


    }
    public Integer getReadingPhysicianID()
    {
        return this.getReadingPhysicianID(this.UserSecurityID);
    }

    public void setReadingPhysicianID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ReadingPhysicianID' AND TableName='tNIM3_Service'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[ReadingPhysicianID]=["+newValue+"]");
                   dbDB.setReadingPhysicianID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[ReadingPhysicianID]=["+newValue+"]");
           dbDB.setReadingPhysicianID(newValue);
         }
    }
    public Integer getReadingPhysicianID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ReadingPhysicianID' AND TableName='tNIM3_Service'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getReadingPhysicianID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getReadingPhysicianID();
         }
        return myVal;
    }

    public void setReference_ProviderContractAmount(Double newValue)
    {
        this.setReference_ProviderContractAmount(newValue,this.UserSecurityID);


    }
    public Double getReference_ProviderContractAmount()
    {
        return this.getReference_ProviderContractAmount(this.UserSecurityID);
    }

    public void setReference_ProviderContractAmount(Double newValue, Integer GroupRefID)
    {
        Double myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Reference_ProviderContractAmount' AND TableName='tNIM3_Service'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[Reference_ProviderContractAmount]=["+newValue+"]");
                   dbDB.setReference_ProviderContractAmount(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[Reference_ProviderContractAmount]=["+newValue+"]");
           dbDB.setReference_ProviderContractAmount(newValue);
         }
    }
    public Double getReference_ProviderContractAmount(Integer GroupRefID)
    {
        Double myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Reference_ProviderContractAmount' AND TableName='tNIM3_Service'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getReference_ProviderContractAmount();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getReference_ProviderContractAmount();
         }
        return myVal;
    }

    public void setReference_ProviderBilledAmount(Double newValue)
    {
        this.setReference_ProviderBilledAmount(newValue,this.UserSecurityID);


    }
    public Double getReference_ProviderBilledAmount()
    {
        return this.getReference_ProviderBilledAmount(this.UserSecurityID);
    }

    public void setReference_ProviderBilledAmount(Double newValue, Integer GroupRefID)
    {
        Double myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Reference_ProviderBilledAmount' AND TableName='tNIM3_Service'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[Reference_ProviderBilledAmount]=["+newValue+"]");
                   dbDB.setReference_ProviderBilledAmount(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[Reference_ProviderBilledAmount]=["+newValue+"]");
           dbDB.setReference_ProviderBilledAmount(newValue);
         }
    }
    public Double getReference_ProviderBilledAmount(Integer GroupRefID)
    {
        Double myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Reference_ProviderBilledAmount' AND TableName='tNIM3_Service'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getReference_ProviderBilledAmount();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getReference_ProviderBilledAmount();
         }
        return myVal;
    }

    public void setReference_FeeScheduleAmount(Double newValue)
    {
        this.setReference_FeeScheduleAmount(newValue,this.UserSecurityID);


    }
    public Double getReference_FeeScheduleAmount()
    {
        return this.getReference_FeeScheduleAmount(this.UserSecurityID);
    }

    public void setReference_FeeScheduleAmount(Double newValue, Integer GroupRefID)
    {
        Double myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Reference_FeeScheduleAmount' AND TableName='tNIM3_Service'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[Reference_FeeScheduleAmount]=["+newValue+"]");
                   dbDB.setReference_FeeScheduleAmount(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[Reference_FeeScheduleAmount]=["+newValue+"]");
           dbDB.setReference_FeeScheduleAmount(newValue);
         }
    }
    public Double getReference_FeeScheduleAmount(Integer GroupRefID)
    {
        Double myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Reference_FeeScheduleAmount' AND TableName='tNIM3_Service'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getReference_FeeScheduleAmount();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getReference_FeeScheduleAmount();
         }
        return myVal;
    }

    public void setReference_UCAmount(Double newValue)
    {
        this.setReference_UCAmount(newValue,this.UserSecurityID);


    }
    public Double getReference_UCAmount()
    {
        return this.getReference_UCAmount(this.UserSecurityID);
    }

    public void setReference_UCAmount(Double newValue, Integer GroupRefID)
    {
        Double myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Reference_UCAmount' AND TableName='tNIM3_Service'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[Reference_UCAmount]=["+newValue+"]");
                   dbDB.setReference_UCAmount(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[Reference_UCAmount]=["+newValue+"]");
           dbDB.setReference_UCAmount(newValue);
         }
    }
    public Double getReference_UCAmount(Integer GroupRefID)
    {
        Double myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Reference_UCAmount' AND TableName='tNIM3_Service'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getReference_UCAmount();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getReference_UCAmount();
         }
        return myVal;
    }

    public void setReference_PayerBillAmount(Double newValue)
    {
        this.setReference_PayerBillAmount(newValue,this.UserSecurityID);


    }
    public Double getReference_PayerBillAmount()
    {
        return this.getReference_PayerBillAmount(this.UserSecurityID);
    }

    public void setReference_PayerBillAmount(Double newValue, Integer GroupRefID)
    {
        Double myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Reference_PayerBillAmount' AND TableName='tNIM3_Service'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[Reference_PayerBillAmount]=["+newValue+"]");
                   dbDB.setReference_PayerBillAmount(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[Reference_PayerBillAmount]=["+newValue+"]");
           dbDB.setReference_PayerBillAmount(newValue);
         }
    }
    public Double getReference_PayerBillAmount(Integer GroupRefID)
    {
        Double myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Reference_PayerBillAmount' AND TableName='tNIM3_Service'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getReference_PayerBillAmount();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getReference_PayerBillAmount();
         }
        return myVal;
    }

    public void setReference_PayerContractAmount(Double newValue)
    {
        this.setReference_PayerContractAmount(newValue,this.UserSecurityID);


    }
    public Double getReference_PayerContractAmount()
    {
        return this.getReference_PayerContractAmount(this.UserSecurityID);
    }

    public void setReference_PayerContractAmount(Double newValue, Integer GroupRefID)
    {
        Double myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Reference_PayerContractAmount' AND TableName='tNIM3_Service'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[Reference_PayerContractAmount]=["+newValue+"]");
                   dbDB.setReference_PayerContractAmount(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[Reference_PayerContractAmount]=["+newValue+"]");
           dbDB.setReference_PayerContractAmount(newValue);
         }
    }
    public Double getReference_PayerContractAmount(Integer GroupRefID)
    {
        Double myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Reference_PayerContractAmount' AND TableName='tNIM3_Service'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getReference_PayerContractAmount();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getReference_PayerContractAmount();
         }
        return myVal;
    }

    public void setBillingNotes(String newValue)
    {
        this.setBillingNotes(newValue,this.UserSecurityID);


    }
    public String getBillingNotes()
    {
        return this.getBillingNotes(this.UserSecurityID);
    }

    public void setBillingNotes(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='BillingNotes' AND TableName='tNIM3_Service'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[BillingNotes]=["+newValue+"]");
                   dbDB.setBillingNotes(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[BillingNotes]=["+newValue+"]");
           dbDB.setBillingNotes(newValue);
         }
    }
    public String getBillingNotes(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='BillingNotes' AND TableName='tNIM3_Service'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getBillingNotes();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getBillingNotes();
         }
        return myVal;
    }

    public void setInvoiceNotes_Payer(String newValue)
    {
        this.setInvoiceNotes_Payer(newValue,this.UserSecurityID);


    }
    public String getInvoiceNotes_Payer()
    {
        return this.getInvoiceNotes_Payer(this.UserSecurityID);
    }

    public void setInvoiceNotes_Payer(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='InvoiceNotes_Payer' AND TableName='tNIM3_Service'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[InvoiceNotes_Payer]=["+newValue+"]");
                   dbDB.setInvoiceNotes_Payer(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[InvoiceNotes_Payer]=["+newValue+"]");
           dbDB.setInvoiceNotes_Payer(newValue);
         }
    }
    public String getInvoiceNotes_Payer(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='InvoiceNotes_Payer' AND TableName='tNIM3_Service'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getInvoiceNotes_Payer();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getInvoiceNotes_Payer();
         }
        return myVal;
    }

    public void setInvoiceNotes_Provider(String newValue)
    {
        this.setInvoiceNotes_Provider(newValue,this.UserSecurityID);


    }
    public String getInvoiceNotes_Provider()
    {
        return this.getInvoiceNotes_Provider(this.UserSecurityID);
    }

    public void setInvoiceNotes_Provider(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='InvoiceNotes_Provider' AND TableName='tNIM3_Service'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[InvoiceNotes_Provider]=["+newValue+"]");
                   dbDB.setInvoiceNotes_Provider(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[InvoiceNotes_Provider]=["+newValue+"]");
           dbDB.setInvoiceNotes_Provider(newValue);
         }
    }
    public String getInvoiceNotes_Provider(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='InvoiceNotes_Provider' AND TableName='tNIM3_Service'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getInvoiceNotes_Provider();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getInvoiceNotes_Provider();
         }
        return myVal;
    }

    public void setCPTQty_Bill(Integer newValue)
    {
        this.setCPTQty_Bill(newValue,this.UserSecurityID);


    }
    public Integer getCPTQty_Bill()
    {
        return this.getCPTQty_Bill(this.UserSecurityID);
    }

    public void setCPTQty_Bill(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CPTQty_Bill' AND TableName='tNIM3_Service'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[CPTQty_Bill]=["+newValue+"]");
                   dbDB.setCPTQty_Bill(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[CPTQty_Bill]=["+newValue+"]");
           dbDB.setCPTQty_Bill(newValue);
         }
    }
    public Integer getCPTQty_Bill(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CPTQty_Bill' AND TableName='tNIM3_Service'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getCPTQty_Bill();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getCPTQty_Bill();
         }
        return myVal;
    }

    public void setCPTQty_Pay(Integer newValue)
    {
        this.setCPTQty_Pay(newValue,this.UserSecurityID);


    }
    public Integer getCPTQty_Pay()
    {
        return this.getCPTQty_Pay(this.UserSecurityID);
    }

    public void setCPTQty_Pay(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CPTQty_Pay' AND TableName='tNIM3_Service'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[CPTQty_Pay]=["+newValue+"]");
                   dbDB.setCPTQty_Pay(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[CPTQty_Pay]=["+newValue+"]");
           dbDB.setCPTQty_Pay(newValue);
         }
    }
    public Integer getCPTQty_Pay(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CPTQty_Pay' AND TableName='tNIM3_Service'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getCPTQty_Pay();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getCPTQty_Pay();
         }
        return myVal;
    }

    public void setRequiredFieldList(java.util.Hashtable requiredFieldList)
	 {

        this.requiredFieldList = requiredFieldList;
    }

    public java.util.Hashtable getRequiredFieldList()
    {
        return this.requiredFieldList;
    }
    public void setExpiredFieldList(java.util.Hashtable expiredFieldList)
    {
        this.expiredFieldList = expiredFieldList;
    }

    public void setEnglishFieldList(java.util.Hashtable englishFieldList)
    {
        this.englishFieldList = englishFieldList;
    }

    public java.util.Hashtable getExpiredFieldList()
    {
        return this.expiredFieldList;
    }
    public boolean isExpired(java.util.Date myDate, int daysVector)
    {
       return isExpired(myDate,new java.util.Date(),daysVector);
    }


    public boolean isRead(String fieldN, Integer GroupRefID)
    {
        boolean myVal = false;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='"+fieldN+"' AND TableName='tNIM3_Service'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==1)
               {
                   myVal = true;
               }
            }
        }
        else
        {

           if (iSecurityCheck.intValue()>=1)
           {
                  myVal = true;
           }
        }
        return myVal;
    }


    public boolean isWrite(String fieldN, Integer GroupRefID)
    {
        boolean myVal = false;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='"+fieldN+"' AND TableName='tNIM3_Service'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {
                   myVal = true;
               }
            }
        }
        else
        {

           if (iSecurityCheck.intValue()>=2)
           {
                  myVal = true;
           }
        }
        return myVal;
    }


    public boolean isExpired(java.util.Date myDate, java.util.Date refDate,int daysVector)
    {
        java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat("yyyy-MM-dd");
        boolean myVal = true;
        try
        {
            if (myDate.after(dbdf.parse("1800-01-10")))
            {
                if (myDate.after(dateChange(refDate,daysVector)))
                {
                    myVal=false;
                }
            }
            else
            {
                myVal = false;
            }
        }
        catch (Exception Datee)
        {
        }
        return myVal;
    }

    public java.util.Date dateChange(java.util.Date myD, int dayChange)
    {
        java.util.Calendar calendar = java.util.Calendar.getInstance();
        calendar.setTime(myD);
        calendar.add(java.util.Calendar.DATE, dayChange);
        java.util.Date resultAsDate = new java.util.Date(calendar.getTime().getTime());
        return resultAsDate;
    }

    public boolean isRequired(String fieldN)
    {
        return isRequired(fieldN, new Integer("1"));
    }

    public boolean isRequired(String fieldN, Integer GroupRefID)
    {
        boolean myVal = false;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='"+fieldN+"' AND TableName='tNIM3_Service'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getRequiredID().intValue()==1)
               {
                   myVal = true;
               }
            }
        }
        else
        {
            if (this.getRequiredFieldList().containsKey(fieldN))
            {
                myVal = true;
            }
        }
        return myVal;
    }
    public boolean isExpiredCheck(String fieldN)
    {
        return isExpiredCheck(fieldN, new Integer ("1"));
    }

    public boolean isExpiredCheck(String fieldN, Integer GroupRefID)
    {
        boolean myVal = false;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='"+fieldN+"' AND TableName='tNIM3_Service'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getTrackedID().intValue()==1)
               {
                   myVal = true;
               }
            }
        }
        else
        {
            if (this.getExpiredFieldList().containsKey(fieldN))
           {
                myVal = true;
            }
        }
        return myVal;
    }

    public void makeRequiredHash()
    {
       makeRequiredHash(new Integer(1));
    }

    public void makeRequiredHash(Integer GroupRefID)
    {

        java.util.Hashtable newHash = new java.util.Hashtable();
        if (runFieldSecurity)
        {
            java.util.Enumeration myKeys = englishFieldList.keys(); 
            while (myKeys.hasMoreElements()) 
            {
                 String myElement = (String) myKeys.nextElement();
                 bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='"+myElement+"' AND TableName='tNIM3_Service'" ,"");
                 bltFieldSecurity        working_bltFieldSecurity;
                 ListElement         leCurrentElement;
                 Enumeration eList = myFSL.elements();
                 if (eList.hasMoreElements())
                 {
                     leCurrentElement    = (ListElement) eList.nextElement();
                     working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
                     if (working_bltFieldSecurity.getRequiredID().intValue()==1)
                     {
                         newHash.put(myElement,new Boolean(true));
                     }
                 }
            }
        }
        else
        {
            newHash.put("EncounterID",new Boolean(true));
            newHash.put("ServiceTypeID",new Boolean(true));
            newHash.put("CPT",new Boolean(true));
            newHash.put("CPTBodyPart",new Boolean(true));
            newHash.put("CPTQty",new Boolean(true));
            newHash.put("ServiceStatusID",new Boolean(true));
        }
        this.setRequiredFieldList(newHash);
    }

    public void makeEnglishHash()
    {

        java.util.Hashtable newHash = new java.util.Hashtable();        newHash.put("UniqueCreateDate","UniqueCreateDate");
        newHash.put("UniqueModifyDate","UniqueModifyDate");
        newHash.put("UniqueModifyComments","UniqueModifyComments");
        newHash.put("EncounterID","EncounterID");
        newHash.put("ServiceTypeID","Service Type ID");
        newHash.put("DateOfService","Date Of Service");
        newHash.put("AttendingPhysicianID","Attending Physician");
        newHash.put("ReferringPhysicianID","Referring Physician");
        newHash.put("CPT","CPT");
        newHash.put("CPTModifier","CPT Modifier");
        newHash.put("CPTBodyPart","CPT BodyPart");
        newHash.put("dCPT1","Diag/ICD 1");
        newHash.put("dCPT2","Diag/ICD 2");
        newHash.put("dCPT3","Diag/ICD 3");
        newHash.put("dCPT4","Diag/ICD 4");
        newHash.put("CPTText","CPT Text");
        newHash.put("BillAmount","Bill Amount");
        newHash.put("AllowAmount","Allow Amount");
        newHash.put("ReceivedAmount","Received Amount");
        newHash.put("PaidOutAmount","PaidOut Amount");
        newHash.put("ReceivedCheck1Number","ReceivedCheck1Number");
        newHash.put("ReceivedCheck1Amount","ReceivedCheck1Amount");
        newHash.put("ReceivedCheck1Date","ReceivedCheck1Date");
        newHash.put("ReceivedCheck2Number","ReceivedCheck2Number");
        newHash.put("ReceivedCheck2Amount","ReceivedCheck2Amount");
        newHash.put("ReceivedCheck2Date","ReceivedCheck2Date");
        newHash.put("ReceivedCheck3Number","ReceivedCheck3Number");
        newHash.put("ReceivedCheck3Amount","ReceivedCheck3Amount");
        newHash.put("ReceivedCheck3Date","ReceivedCheck3Date");
        newHash.put("ReportFileID","ReportFileID");
        newHash.put("Comments","Comments");
        newHash.put("AuditNotes","Audit Notes");
        newHash.put("CPTWizardID","CPTWizardID");
        newHash.put("CPTQty","CPTQty");
        newHash.put("AllowAmountAdjustment","Allow Amount Adjustment");
        newHash.put("ServiceStatusID","ServiceStatusID");
        newHash.put("DICOM_AccessionNumber","DICOM_AccessionNumber");
        newHash.put("DICOM_ShowImage","DICOM_ShowImage");
        newHash.put("DICOM_MRN","DICOM_MRN");
        newHash.put("DICOM_PN","DICOM_PN");
        newHash.put("DICOM_STUDYID","DICOM_STUDYID");
        newHash.put("ReadingPhysicianID","ReadingPhysicianID");
        newHash.put("Reference_ProviderContractAmount","Reference_ProviderContractAmount");
        newHash.put("Reference_ProviderBilledAmount","Reference_ProviderBilledAmount");
        newHash.put("Reference_FeeScheduleAmount","Reference_FeeScheduleAmount");
        newHash.put("Reference_UCAmount","Reference_UCAmount");
        newHash.put("Reference_PayerBillAmount","Reference_PayerBillAmount");
        newHash.put("Reference_PayerContractAmount","Reference_PayerContractAmount");
        newHash.put("BillingNotes","BillingNotes");
        newHash.put("InvoiceNotes_Payer","InvoiceNotes_Payer");
        newHash.put("InvoiceNotes_Provider","InvoiceNotes_Provider");
        newHash.put("CPTQty_Bill","CPTQty_Bill");
        newHash.put("CPTQty_Pay","CPTQty_Pay");

        this.setEnglishFieldList(newHash);
    }
    public void makeExpiredHash()
    {

        java.util.Hashtable newHash = new java.util.Hashtable();
        this.setExpiredFieldList(newHash);
    }

    public java.util.Hashtable englishFieldList = new java.util.Hashtable();
    public java.util.Hashtable requiredFieldList = new java.util.Hashtable();
    public java.util.Hashtable expiredFieldList = new java.util.Hashtable();

    public boolean isComplete()
    {
	     boolean myVal = true;
	     java.util.Enumeration myKeys = this.getRequiredFieldList().keys(); 
	     while (myKeys.hasMoreElements()) 
	     {
	         String myElement = (String) myKeys.nextElement();
	         if (!isComplete(myElement))
	         {
	             myVal = false;
	         }
	     }
	     return myVal;
    }

    public String getEnglish(String fieldN)
    {
        String myVal = null;
        if (this.englishFieldList.containsKey(fieldN))
        {
            myVal = (String)this.englishFieldList.get(fieldN);
        }
        return myVal;
    }
    public java.util.Vector isComplete(int daysVector)
    {
	     boolean myVal = false;
	     java.util.Enumeration myKeys = this.getExpiredFieldList().keys(); 
	     java.util.Vector myExpiredList = new java.util.Vector(); 
	     while (myKeys.hasMoreElements()) 
	     {
	         String myElement = (String) myKeys.nextElement();
	         if (isExpired(myElement,daysVector))
	         {
	             myVal = true;
	             myExpiredList.addElement(myElement);
	         }
	     }
	     return myExpiredList;
    }

    public void setField(String fieldN, Object fieldV)
    {
     boolean myVal = false;
     try
     {
        java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat("yyyy-MM-dd");

        if (fieldN.equals("ServiceID"))
        {
             this.setServiceID((Integer)fieldV);
        }

        else if (fieldN.equals("UniqueCreateDate"))
        {
            this.setUniqueCreateDate((java.util.Date)fieldV);
        }

        else if (fieldN.equals("UniqueModifyDate"))
        {
            this.setUniqueModifyDate((java.util.Date)fieldV);
        }

        else if (fieldN.equals("UniqueModifyComments"))
        {
            this.setUniqueModifyComments((String)fieldV);
        }

        else if (fieldN.equals("EncounterID"))
        {
             this.setEncounterID((Integer)fieldV);
        }

        else if (fieldN.equals("ServiceTypeID"))
        {
             this.setServiceTypeID((Integer)fieldV);
        }

        else if (fieldN.equals("DateOfService"))
        {
            this.setDateOfService((java.util.Date)fieldV);
        }

        else if (fieldN.equals("AttendingPhysicianID"))
        {
             this.setAttendingPhysicianID((Integer)fieldV);
        }

        else if (fieldN.equals("ReferringPhysicianID"))
        {
             this.setReferringPhysicianID((Integer)fieldV);
        }

        else if (fieldN.equals("CPT"))
        {
            this.setCPT((String)fieldV);
        }

        else if (fieldN.equals("CPTModifier"))
        {
            this.setCPTModifier((String)fieldV);
        }

        else if (fieldN.equals("CPTBodyPart"))
        {
            this.setCPTBodyPart((String)fieldV);
        }

        else if (fieldN.equals("dCPT1"))
        {
            this.setdCPT1((String)fieldV);
        }

        else if (fieldN.equals("dCPT2"))
        {
            this.setdCPT2((String)fieldV);
        }

        else if (fieldN.equals("dCPT3"))
        {
            this.setdCPT3((String)fieldV);
        }

        else if (fieldN.equals("dCPT4"))
        {
            this.setdCPT4((String)fieldV);
        }

        else if (fieldN.equals("CPTText"))
        {
            this.setCPTText((String)fieldV);
        }

        else if (fieldN.equals("BillAmount"))
        {
            this.setBillAmount((Double)fieldV);
        }

        else if (fieldN.equals("AllowAmount"))
        {
            this.setAllowAmount((Double)fieldV);
        }

        else if (fieldN.equals("ReceivedAmount"))
        {
            this.setReceivedAmount((Double)fieldV);
        }

        else if (fieldN.equals("PaidOutAmount"))
        {
            this.setPaidOutAmount((Double)fieldV);
        }

        else if (fieldN.equals("ReceivedCheck1Number"))
        {
            this.setReceivedCheck1Number((String)fieldV);
        }

        else if (fieldN.equals("ReceivedCheck1Amount"))
        {
            this.setReceivedCheck1Amount((Double)fieldV);
        }

        else if (fieldN.equals("ReceivedCheck1Date"))
        {
            this.setReceivedCheck1Date((java.util.Date)fieldV);
        }

        else if (fieldN.equals("ReceivedCheck2Number"))
        {
            this.setReceivedCheck2Number((String)fieldV);
        }

        else if (fieldN.equals("ReceivedCheck2Amount"))
        {
            this.setReceivedCheck2Amount((Double)fieldV);
        }

        else if (fieldN.equals("ReceivedCheck2Date"))
        {
            this.setReceivedCheck2Date((java.util.Date)fieldV);
        }

        else if (fieldN.equals("ReceivedCheck3Number"))
        {
            this.setReceivedCheck3Number((String)fieldV);
        }

        else if (fieldN.equals("ReceivedCheck3Amount"))
        {
            this.setReceivedCheck3Amount((Double)fieldV);
        }

        else if (fieldN.equals("ReceivedCheck3Date"))
        {
            this.setReceivedCheck3Date((java.util.Date)fieldV);
        }

        else if (fieldN.equals("ReportFileID"))
        {
             this.setReportFileID((Integer)fieldV);
        }

        else if (fieldN.equals("Comments"))
        {
            this.setComments((String)fieldV);
        }

        else if (fieldN.equals("AuditNotes"))
        {
            this.setAuditNotes((String)fieldV);
        }

        else if (fieldN.equals("CPTWizardID"))
        {
             this.setCPTWizardID((Integer)fieldV);
        }

        else if (fieldN.equals("CPTQty"))
        {
             this.setCPTQty((Integer)fieldV);
        }

        else if (fieldN.equals("AllowAmountAdjustment"))
        {
            this.setAllowAmountAdjustment((Double)fieldV);
        }

        else if (fieldN.equals("ServiceStatusID"))
        {
             this.setServiceStatusID((Integer)fieldV);
        }

        else if (fieldN.equals("DICOM_AccessionNumber"))
        {
            this.setDICOM_AccessionNumber((String)fieldV);
        }

        else if (fieldN.equals("DICOM_ShowImage"))
        {
             this.setDICOM_ShowImage((Integer)fieldV);
        }

        else if (fieldN.equals("DICOM_MRN"))
        {
            this.setDICOM_MRN((String)fieldV);
        }

        else if (fieldN.equals("DICOM_PN"))
        {
            this.setDICOM_PN((String)fieldV);
        }

        else if (fieldN.equals("DICOM_STUDYID"))
        {
            this.setDICOM_STUDYID((String)fieldV);
        }

        else if (fieldN.equals("ReadingPhysicianID"))
        {
             this.setReadingPhysicianID((Integer)fieldV);
        }

        else if (fieldN.equals("Reference_ProviderContractAmount"))
        {
            this.setReference_ProviderContractAmount((Double)fieldV);
        }

        else if (fieldN.equals("Reference_ProviderBilledAmount"))
        {
            this.setReference_ProviderBilledAmount((Double)fieldV);
        }

        else if (fieldN.equals("Reference_FeeScheduleAmount"))
        {
            this.setReference_FeeScheduleAmount((Double)fieldV);
        }

        else if (fieldN.equals("Reference_UCAmount"))
        {
            this.setReference_UCAmount((Double)fieldV);
        }

        else if (fieldN.equals("Reference_PayerBillAmount"))
        {
            this.setReference_PayerBillAmount((Double)fieldV);
        }

        else if (fieldN.equals("Reference_PayerContractAmount"))
        {
            this.setReference_PayerContractAmount((Double)fieldV);
        }

        else if (fieldN.equals("BillingNotes"))
        {
            this.setBillingNotes((String)fieldV);
        }

        else if (fieldN.equals("InvoiceNotes_Payer"))
        {
            this.setInvoiceNotes_Payer((String)fieldV);
        }

        else if (fieldN.equals("InvoiceNotes_Provider"))
        {
            this.setInvoiceNotes_Provider((String)fieldV);
        }

        else if (fieldN.equals("CPTQty_Bill"))
        {
             this.setCPTQty_Bill((Integer)fieldV);
        }

        else if (fieldN.equals("CPTQty_Pay"))
        {
             this.setCPTQty_Pay((Integer)fieldV);
        }

	    }
	    catch(Exception e2)
	    {}
	    //return myVal;
	}

    public String getFieldType(String fieldN)
    {
     String  myVal = null;
     try
     {
        java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat("yyyy-MM-dd");

        if (fieldN.equals("ServiceID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("UniqueCreateDate"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("UniqueModifyDate"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("UniqueModifyComments"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("EncounterID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("ServiceTypeID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("DateOfService"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("AttendingPhysicianID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("ReferringPhysicianID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("CPT"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("CPTModifier"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("CPTBodyPart"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("dCPT1"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("dCPT2"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("dCPT3"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("dCPT4"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("CPTText"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("BillAmount"))
        {
            myVal = "Double";
        }

        else if (fieldN.equals("AllowAmount"))
        {
            myVal = "Double";
        }

        else if (fieldN.equals("ReceivedAmount"))
        {
            myVal = "Double";
        }

        else if (fieldN.equals("PaidOutAmount"))
        {
            myVal = "Double";
        }

        else if (fieldN.equals("ReceivedCheck1Number"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("ReceivedCheck1Amount"))
        {
            myVal = "Double";
        }

        else if (fieldN.equals("ReceivedCheck1Date"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("ReceivedCheck2Number"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("ReceivedCheck2Amount"))
        {
            myVal = "Double";
        }

        else if (fieldN.equals("ReceivedCheck2Date"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("ReceivedCheck3Number"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("ReceivedCheck3Amount"))
        {
            myVal = "Double";
        }

        else if (fieldN.equals("ReceivedCheck3Date"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("ReportFileID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("Comments"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("AuditNotes"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("CPTWizardID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("CPTQty"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("AllowAmountAdjustment"))
        {
            myVal = "Double";
        }

        else if (fieldN.equals("ServiceStatusID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("DICOM_AccessionNumber"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("DICOM_ShowImage"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("DICOM_MRN"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("DICOM_PN"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("DICOM_STUDYID"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("ReadingPhysicianID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("Reference_ProviderContractAmount"))
        {
            myVal = "Double";
        }

        else if (fieldN.equals("Reference_ProviderBilledAmount"))
        {
            myVal = "Double";
        }

        else if (fieldN.equals("Reference_FeeScheduleAmount"))
        {
            myVal = "Double";
        }

        else if (fieldN.equals("Reference_UCAmount"))
        {
            myVal = "Double";
        }

        else if (fieldN.equals("Reference_PayerBillAmount"))
        {
            myVal = "Double";
        }

        else if (fieldN.equals("Reference_PayerContractAmount"))
        {
            myVal = "Double";
        }

        else if (fieldN.equals("BillingNotes"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("InvoiceNotes_Payer"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("InvoiceNotes_Provider"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("CPTQty_Bill"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("CPTQty_Pay"))
        {
            myVal = "Integer";
        }

	    }
	    catch(Exception e2)
	    {}
	    return myVal;
	}

    public boolean isComplete(String fieldN)
    {
	     boolean myVal = false;
	     try
	     {
        java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat("yyyy-MM-dd");

	     if (fieldN.equals("ServiceID"))

	     {
                    if (this.getServiceID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("UniqueCreateDate"))

	     {
                    if (this.getUniqueCreateDate().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("UniqueModifyDate"))

	     {
                    if (this.getUniqueModifyDate().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("UniqueModifyComments"))

	     {
                    if (this.getUniqueModifyComments().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("EncounterID"))

	     {
                    if (this.getEncounterID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("ServiceTypeID"))

	     {
                    if (this.getServiceTypeID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("DateOfService"))

	     {
                    if (this.getDateOfService().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("AttendingPhysicianID"))

	     {
                    if (this.getAttendingPhysicianID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("ReferringPhysicianID"))

	     {
                    if (this.getReferringPhysicianID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("CPT"))

	     {
                    if (this.getCPT().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("CPTModifier"))

	     {
                    if (this.getCPTModifier().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("CPTBodyPart"))

	     {
                    if (this.getCPTBodyPart().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("dCPT1"))

	     {
                    if (this.getdCPT1().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("dCPT2"))

	     {
                    if (this.getdCPT2().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("dCPT3"))

	     {
                    if (this.getdCPT3().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("dCPT4"))

	     {
                    if (this.getdCPT4().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("CPTText"))

	     {
                    if (this.getCPTText().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("BillAmount"))

	     {
                    if (this.getBillAmount().equals(new Double("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("AllowAmount"))

	     {
                    if (this.getAllowAmount().equals(new Double("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("ReceivedAmount"))

	     {
                    if (this.getReceivedAmount().equals(new Double("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("PaidOutAmount"))

	     {
                    if (this.getPaidOutAmount().equals(new Double("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("ReceivedCheck1Number"))

	     {
                    if (this.getReceivedCheck1Number().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("ReceivedCheck1Amount"))

	     {
                    if (this.getReceivedCheck1Amount().equals(new Double("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("ReceivedCheck1Date"))

	     {
                    if (this.getReceivedCheck1Date().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("ReceivedCheck2Number"))

	     {
                    if (this.getReceivedCheck2Number().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("ReceivedCheck2Amount"))

	     {
                    if (this.getReceivedCheck2Amount().equals(new Double("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("ReceivedCheck2Date"))

	     {
                    if (this.getReceivedCheck2Date().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("ReceivedCheck3Number"))

	     {
                    if (this.getReceivedCheck3Number().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("ReceivedCheck3Amount"))

	     {
                    if (this.getReceivedCheck3Amount().equals(new Double("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("ReceivedCheck3Date"))

	     {
                    if (this.getReceivedCheck3Date().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("ReportFileID"))

	     {
                    if (this.getReportFileID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("Comments"))

	     {
                    if (this.getComments().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("AuditNotes"))

	     {
                    if (this.getAuditNotes().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("CPTWizardID"))

	     {
                    if (this.getCPTWizardID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("CPTQty"))

	     {
                    if (this.getCPTQty().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("AllowAmountAdjustment"))

	     {
                    if (this.getAllowAmountAdjustment().equals(new Double("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("ServiceStatusID"))

	     {
                    if (this.getServiceStatusID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("DICOM_AccessionNumber"))

	     {
                    if (this.getDICOM_AccessionNumber().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("DICOM_ShowImage"))

	     {
                    if (this.getDICOM_ShowImage().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("DICOM_MRN"))

	     {
                    if (this.getDICOM_MRN().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("DICOM_PN"))

	     {
                    if (this.getDICOM_PN().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("DICOM_STUDYID"))

	     {
                    if (this.getDICOM_STUDYID().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("ReadingPhysicianID"))

	     {
                    if (this.getReadingPhysicianID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("Reference_ProviderContractAmount"))

	     {
                    if (this.getReference_ProviderContractAmount().equals(new Double("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("Reference_ProviderBilledAmount"))

	     {
                    if (this.getReference_ProviderBilledAmount().equals(new Double("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("Reference_FeeScheduleAmount"))

	     {
                    if (this.getReference_FeeScheduleAmount().equals(new Double("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("Reference_UCAmount"))

	     {
                    if (this.getReference_UCAmount().equals(new Double("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("Reference_PayerBillAmount"))

	     {
                    if (this.getReference_PayerBillAmount().equals(new Double("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("Reference_PayerContractAmount"))

	     {
                    if (this.getReference_PayerContractAmount().equals(new Double("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("BillingNotes"))

	     {
                    if (this.getBillingNotes().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("InvoiceNotes_Payer"))

	     {
                    if (this.getInvoiceNotes_Payer().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("InvoiceNotes_Provider"))

	     {
                    if (this.getInvoiceNotes_Provider().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("CPTQty_Bill"))

	     {
                    if (this.getCPTQty_Bill().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("CPTQty_Pay"))

	     {
                    if (this.getCPTQty_Pay().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	    }
	    catch(Exception e2)
	    {}
	    return myVal;
	}

    public boolean isExpired(String fieldN,int daysVector)
    {
        return isExpired(fieldN,new java.util.Date(),daysVector);
    }

    public boolean isExpired(String fieldN,java.util.Date refDate,int daysVector)
    {
	     boolean myVal = false;
	     try
	     {
        java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat("yyyy-MM-dd");

        if (fieldN.equals("UniqueCreateDate"))

	     {
                    if (this.isExpired(this.getUniqueCreateDate(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

        if (fieldN.equals("UniqueModifyDate"))

	     {
                    if (this.isExpired(this.getUniqueModifyDate(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

        if (fieldN.equals("DateOfService"))

	     {
                    if (this.isExpired(this.getDateOfService(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

        if (fieldN.equals("ReceivedCheck1Date"))

	     {
                    if (this.isExpired(this.getReceivedCheck1Date(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

        if (fieldN.equals("ReceivedCheck2Date"))

	     {
                    if (this.isExpired(this.getReceivedCheck2Date(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

        if (fieldN.equals("ReceivedCheck3Date"))

	     {
                    if (this.isExpired(this.getReceivedCheck3Date(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

	    }
	    catch(Exception e2)
	    {}
	    return myVal;
	}

    public java.util.Date getDateField(String fieldN)
    {
	     java.util.Date myVal = null;
	     try
	     {
        java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat("yyyy-MM-dd");

        if (fieldN.equals("UniqueCreateDate"))

	     {
             myVal = this.getUniqueCreateDate();
        }

        if (fieldN.equals("UniqueModifyDate"))

	     {
             myVal = this.getUniqueModifyDate();
        }

        if (fieldN.equals("DateOfService"))

	     {
             myVal = this.getDateOfService();
        }

        if (fieldN.equals("ReceivedCheck1Date"))

	     {
             myVal = this.getReceivedCheck1Date();
        }

        if (fieldN.equals("ReceivedCheck2Date"))

	     {
             myVal = this.getReceivedCheck2Date();
        }

        if (fieldN.equals("ReceivedCheck3Date"))

	     {
             myVal = this.getReceivedCheck3Date();
        }

	    }
	    catch(Exception e2)
	    {}
	    return myVal;
	}
	public boolean runFieldSecurity = false;
	public Integer UserSecurityID = null;
   public Integer iSecurityCheck = null;
}    // End of bltNIM3_Service class definition
