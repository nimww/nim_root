package com.winstaff;
import java.util.Date;
public class CptWizard{
	private String cptwizardid;
	private Date createdate;
	private String mod;
	private String special;
	private String contrast;
	private String bodypart;
	private String orientation;
	private String cpt1;
	private String bp1;
	private String cpt2;
	private String bp2;
	private String cpt3;
	private String bp3;
	private String cpt4;
	private String bp4;
	private String cpt5;
	private String bp5;
	private String ALLOW_NIM;
	private String ALLOW_NID;
	public CptWizard(String cptwizardid, Date createdate, String mod, String special, String contrast, String bodypart, String orientation, String cpt1, String bp1, String cpt2, String bp2, String cpt3, String bp3, String cpt4, String bp4, String cpt5, String bp5, String ALLOW_NIM,
			String ALLOW_NID) {
		super();
		this.cptwizardid = cptwizardid;
		this.createdate = createdate;
		this.mod = mod;
		this.special = special;
		this.contrast = contrast;
		this.bodypart = bodypart;
		this.orientation = orientation;
		this.cpt1 = cpt1;
		this.bp1 = bp1;
		this.cpt2 = cpt2;
		this.bp2 = bp2;
		this.cpt3 = cpt3;
		this.bp3 = bp3;
		this.cpt4 = cpt4;
		this.bp4 = bp4;
		this.cpt5 = cpt5;
		this.bp5 = bp5;
		this.ALLOW_NIM = ALLOW_NIM;
		this.ALLOW_NID = ALLOW_NID;
	}
	
	public String getCptwizardid() {
		return cptwizardid;
	}
	public void setCptwizardid(String cptwizardid) {
		this.cptwizardid = cptwizardid;
	}
	public Date getCreatedate() {
		return createdate;
	}
	public void setCreatedate(Date createdate) {
		this.createdate = createdate;
	}
	public String getMod() {
		return mod;
	}
	public void setMod(String mod) {
		this.mod = mod;
	}
	public String getSpecial() {
		return special;
	}
	public void setSpecial(String special) {
		this.special = special;
	}
	public String getContrast() {
		return contrast;
	}
	public void setContrast(String contrast) {
		this.contrast = contrast;
	}
	public String getBodypart() {
		return bodypart;
	}
	public void setBodypart(String bodypart) {
		this.bodypart = bodypart;
	}
	public String getOrientation() {
		return orientation;
	}
	public void setOrientation(String orientation) {
		this.orientation = orientation;
	}
	public String getCpt1() {
		return cpt1;
	}
	public void setCpt1(String cpt1) {
		this.cpt1 = cpt1;
	}
	public String getBp1() {
		return bp1;
	}
	public void setBp1(String bp1) {
		this.bp1 = bp1;
	}
	public String getCpt2() {
		return cpt2;
	}
	public void setCpt2(String cpt2) {
		this.cpt2 = cpt2;
	}
	public String getBp2() {
		return bp2;
	}
	public void setBp2(String bp2) {
		this.bp2 = bp2;
	}
	public String getCpt3() {
		return cpt3;
	}
	public void setCpt3(String cpt3) {
		this.cpt3 = cpt3;
	}
	public String getBp3() {
		return bp3;
	}
	public void setBp3(String bp3) {
		this.bp3 = bp3;
	}
	public String getCpt4() {
		return cpt4;
	}
	public void setCpt4(String cpt4) {
		this.cpt4 = cpt4;
	}
	public String getBp4() {
		return bp4;
	}
	public void setBp4(String bp4) {
		this.bp4 = bp4;
	}
	public String getCpt5() {
		return cpt5;
	}
	public void setCpt5(String cpt5) {
		this.cpt5 = cpt5;
	}
	public String getBp5() {
		return bp5;
	}
	public void setBp5(String bp5) {
		this.bp5 = bp5;
	}
	public String getALLOW_NIM() {
		return ALLOW_NIM;
	}
	public void setALLOW_NIM(String ALLOW_NIM) {
		this.ALLOW_NIM = ALLOW_NIM;
	}
	public String getALLOW_NID() {
		return ALLOW_NID;
	}
	public void setALLOW_NID(String ALLOW_NID) {
		this.ALLOW_NID = ALLOW_NID;
	}
	
}
