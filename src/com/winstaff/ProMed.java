package com.winstaff;

import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;

import com.winstaff.*;

public class ProMed {
	private static String venderName = "NextImage Medical, Inc.";
	private static String venderAddress1 = "P.O. Box 749462";
	private static String venderAddress2 = "";
	private static String venderCity = "Los Angeles";
	private static String venderState = "CA";
	private static String venderZip = "90074";
	private static String venderPhone = "858-847-9185";
	private static String venderFax = "888-596-8680";
	private static String venderNPI = "1881861359";
	private static String venderTIN = "205673072";

	public static void main(String[] args) throws Exception{
		runJob();
	}
	
	
	public static void runJob() throws SQLException, IOException, ParseException {
		for (Integer rid : getReadyToBill()) {
			generateBill(getEncounterList(rid), rid);
		}
		uploadFile();
	}

	public static void generateBillwUpload(List<Integer> eidList, Integer rid) throws SQLException, IOException, ParseException {
		generateBill(eidList, rid);
		uploadFile();
	}
	/**
	 * method sucks
	 * @param eidList
	 * @param rid
	 * @throws SQLException
	 * @throws IOException
	 * @throws ParseException
	 */
	public static void generateBill(List<Integer> eidList, Integer rid) throws SQLException, IOException, ParseException {
		List<NIMBillInvoice> nbiList = new ArrayList<NIMBillInvoice>();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");

		for (Integer eid : eidList) {
			DebugLogger.printLine("generateBill: eid "+eid);
			

			bltNIM3_Service_List bltNIM3_Service_List = new bltNIM3_Service_List(eid);
			bltNIM3_Service working_bltNIM3_Service;
			ListElement leCurrentElement;
			searchDB2 db = new searchDB2();
			String query = "";
			java.util.Enumeration eList = bltNIM3_Service_List.elements();
			while (eList.hasMoreElements()) {
				leCurrentElement = (ListElement) eList.nextElement();
				working_bltNIM3_Service = (bltNIM3_Service) leCurrentElement.getObject();
				query = "update tnim3_service set billamount=0, allowamount=0, paidoutamount=0 where serviceid = "+working_bltNIM3_Service.getServiceID();
				
				db.executeUpdate(query);
			}
			db.closeAll();
			
			DebugLogger.printLine("recalc: "+NIMUtils.ReCalcServiceBilling(eid));
			DebugLogger.printLine("recalc2: "+NIMUtils.ReCalcServiceBilling2(eid));
			nbiList.add(new NIMBillInvoice( new NIM3_EncounterObject2(eid, ""), DashBoardUtils.getServicesInfo(getServiceInfo(eid))));

		}

		String fileName = "/Promed/NIM_Invoice_" + rid + ".txt";
		DataOutputStream out = new DataOutputStream(new FileOutputStream(fileName));

		for (NIMBillInvoice nbi : nbiList) {
			bltysonbilling tb = new bltysonbilling();
			tb.setReferralID(rid);
			tb.setGeneratedBillName(fileName);
			tb.commitData();

			out.writeBytes(nbi.getEo2().getNIM3_Encounter().getScanPass() + "|");
			out.writeBytes(nbi.getEo2().getNIM3_CaseAccount().getPatientFirstName() + "|");
			out.writeBytes(nbi.getEo2().getNIM3_CaseAccount().getPatientLastName() + "|");
			out.writeBytes(nbi.getEo2().getNIM3_CaseAccount().getPatientAddress1() + "|");
			out.writeBytes(nbi.getEo2().getNIM3_CaseAccount().getPatientAddress2() + "|");
			out.writeBytes(nbi.getEo2().getNIM3_CaseAccount().getPatientCity() + "|");
			out.writeBytes(new bltStateLI(nbi.getEo2().getNIM3_CaseAccount().getPatientStateID()).getShortState() + "|");
			out.writeBytes(nbi.getEo2().getNIM3_CaseAccount().getPatientZIP() + "|");
			out.writeBytes(nbi.getEo2().getNIM3_CaseAccount().getPatientGender() + "|");
			out.writeBytes(sdf.format(nbi.getEo2().getNIM3_CaseAccount().getPatientDOB()) + "|");
			out.writeBytes(sdf.format(nbi.getEo2().getNIM3_CaseAccount().getDateOfInjury()) + "|");
			out.writeBytes(nbi.getEo2().getNIM3_CaseAccount().getEmployerName() + "|");
			out.writeBytes(sdf.format(nbi.getEo2().getNIM3_Encounter().getDateOfService()) + "|");
			for (int x = 0; x < 4; x++) {
				boolean isFirst = true;
				if (x < nbi.getSi().size()) {
					out.writeBytes(nbi.getSi().get(x).getCpt() + "|");
					out.writeBytes(nbi.getSi().get(x).getCptmodifier() + "|");
					out.writeBytes(nbi.getSi().get(x).getBillamount() + "|");
					out.writeBytes(nbi.getSi().get(x).getAllowamount() + "|");
					if (!nbi.getSi().get(x).getDcpt1().trim().isEmpty()) {
						if (isFirst) {
							out.writeBytes(nbi.getSi().get(x).getDcpt1());
							isFirst = false;
						} else {
							out.writeBytes("," + nbi.getSi().get(x).getDcpt1());
						}
					}
					if (!nbi.getSi().get(x).getDcpt2().trim().isEmpty()) {
						if (isFirst) {
							out.writeBytes(nbi.getSi().get(x).getDcpt2());
							isFirst = false;
						} else {
							out.writeBytes("," + nbi.getSi().get(x).getDcpt2());
						}
					}
					if (!nbi.getSi().get(x).getDcpt3().trim().isEmpty()) {
						if (isFirst) {
							out.writeBytes(nbi.getSi().get(x).getDcpt3());
							isFirst = false;
						} else {
							out.writeBytes("," + nbi.getSi().get(x).getDcpt3());
						}
					}
					if (!nbi.getSi().get(x).getDcpt4().trim().isEmpty()) {
						if (isFirst) {
							out.writeBytes(nbi.getSi().get(x).getDcpt4());
							isFirst = false;
						} else {
							out.writeBytes("," + nbi.getSi().get(x).getDcpt4());
						}
					}
					out.writeBytes("|");
				} else {
					out.writeBytes("|||||");
				}
			}

			out.writeBytes(nbi.getEo2().getAppointment_PracticeMaster().getPracticeName() + "|");
			out.writeBytes(nbi.getEo2().getAppointment_PracticeMaster().getOfficeAddress1() + "|");
			out.writeBytes(nbi.getEo2().getAppointment_PracticeMaster().getOfficeAddress2() + "|");
			out.writeBytes(nbi.getEo2().getAppointment_PracticeMaster().getOfficeCity() + "|");
			out.writeBytes(new bltStateLI(nbi.getEo2().getAppointment_PracticeMaster().getOfficeStateID()).getShortState() + "|");
			out.writeBytes(nbi.getEo2().getAppointment_PracticeMaster().getOfficeZIP() + "|");
			out.writeBytes(nbi.getEo2().getAppointment_PracticeMaster().getOfficePhone() + "|");
			out.writeBytes(nbi.getEo2().getNIM3_PayerMaster().getPayerName() + "|");
			out.writeBytes(nbi.getEo2().getNIM3_PayerMaster().getBillingAddress1() + "|");
			out.writeBytes(nbi.getEo2().getNIM3_PayerMaster().getBillingAddress2() + "|");
			out.writeBytes(nbi.getEo2().getNIM3_PayerMaster().getBillingCity() + "|");
			out.writeBytes(new bltStateLI(nbi.getEo2().getNIM3_PayerMaster().getBillingStateID()).getShortState() + "|");
			out.writeBytes(nbi.getEo2().getNIM3_PayerMaster().getBillingZIP() + "|");
			out.writeBytes(nbi.getEo2().getNIM3_PayerMaster().getBillingPhone() + "|");
			out.writeBytes(nbi.getEo2().getNIM3_PayerMaster().getBillingFax() + "|");
			out.writeBytes(nbi.getEo2().getNIM3_CaseAccount().getCaseClaimNumber() + "|");
			out.writeBytes(nbi.getEo2().getNIM3_Referral().getPreAuthorizationConfirmation() + "|");
			out.writeBytes(nbi.getEo2().getReferral_ReferringDoctor().getContactFirstName() + "|");
			out.writeBytes(nbi.getEo2().getReferral_ReferringDoctor().getContactLastName() + "|");
			out.writeBytes(nbi.getEo2().getReferral_ReferringDoctor().getContactPhone() + "|");
			out.writeBytes(nbi.getEo2().getReferral_ReferringDoctor().getContactFax() + "|");
			out.writeBytes(venderName + "|");
			out.writeBytes(venderAddress1 + "|");
			out.writeBytes(venderAddress2 + "|");
			out.writeBytes(venderCity + "|");
			out.writeBytes(venderState + "|");
			out.writeBytes(venderZip + "|");
			out.writeBytes(venderPhone + "|");
			out.writeBytes(venderFax + "|");
			out.writeBytes(venderNPI + "|");
			out.writeBytes(venderTIN + "|");
			out.writeBytes(nbi.getEo2().getReferral_ReferringDoctor().getUserNPI() + "|");
			out.writeBytes(nbi.getEo2().getAppointment_PracticeMaster().getNPINumber() + "|");
			out.writeBytes("\n");

			if (nbi.getEo2().getNIM3_Encounter().getSentTo_Bill_Pay().before(new SimpleDateFormat("yyyy-MM-dd").parse("1900-01-01"))) {
				nbi.getEo2().getNIM3_Encounter().setSentTo_Bill_Pay(new Date());
			}

			nbi.getEo2().getNIM3_Encounter().commitData();
		}
		out.close();
	}

	private static void uploadFile() {
		List<String> src = new ArrayList<String>();
		List<Integer> tysonId = new ArrayList<Integer>();
		searchDB2 conn = new searchDB2();
		String query = "select tysonbillingid, generatedbillname from tysonbilling where actionid = 0";
		ResultSet rs = conn.executeStatement(query);

		try {
			while (rs.next()) {
				src.add(rs.getString("generatedbillname"));
				tysonId.add(rs.getInt("tysonbillingid"));
			}
		} catch (SQLException e1) {
			e1.printStackTrace();
		}

		String remoteDir = "/Claims/";
		String host = "ftp.mypromed.us";
		String user = "next";
		String password = "next13";

		int filecount = 0;

		try {
			FTPClient ftp = new FTPClient();
			ftp.connect(host);

			ftp.login(user, password);
			ftp.setFileType(FTP.BINARY_FILE_TYPE);
			ftp.enterLocalPassiveMode();
			ftp.setFileTransferMode(FTP.BINARY_FILE_TYPE);
			ftp.changeWorkingDirectory("/Claims/");
			for (String s : src) {
				ftp.storeFile(remoteDir + s.replaceAll("/Promed/", ""), new FileInputStream(s));
				query = "update tysonbilling set actionid = 1 where tysonbillingid = " + tysonId.get(filecount++);
				rs = conn.executeStatement(query);
			}
			ftp.disconnect();
		} catch (Exception e) {
			System.out.println("JSchException:");
			e.printStackTrace();
			DashBoardUtils.email("po.le@nextimagemedical.com", "support@nextimagemedical.com", "ProMed FTP", e.toString());
		}

		query = "select tysonbillingid, generatedbillname from tysonbilling where actionid = 0";
		rs = conn.executeStatement(query);
		String body = "Fail:<br>";
		try {
			while (rs.next()) {
				body += rs.getString("tysonbillingid") + " " + rs.getString("generatedbillname") + "<br>";
			}
		} catch (Exception e) {
		}

		if (body.length() > 9) {
			DashBoardUtils.email("po.le@nextimagemedical.com", "support@nextimagemedical.com", "ProMed FTP", body);
		}
	}

	public static ArrayList<Integer> getEncounterList(int referralid) throws SQLException {
		String query = "select encounterid from tnim3_encounter where referralid = " + referralid;
		ArrayList<Integer> eidList = new ArrayList<Integer>();
		searchDB2 conn = new searchDB2();
		ResultSet rs = conn.executeStatement(query);

		while (rs.next()) {
			eidList.add(rs.getInt("encounterid"));
		}
		return eidList;
	}

	private static ArrayList<Integer> getReadyToBill() throws SQLException {
		String query = "select distinct on (tnim3_encounter.referralid) tnim3_encounter.referralid from tnim3_caseaccount inner join tnim3_referral on tnim3_referral.caseid=tnim3_caseaccount.caseid left join tnim3_encounter on tnim3_encounter.referralid=tnim3_referral.referralid left join tnim3_appointment on tnim3_appointment.appointmentid = tnim3_encounter.appointmentid where tnim3_caseaccount.payerid >0 and reportfileid>0 and encounterstatusid in (9) and sentto_bill_pay<'2008-01-01' and encountertypeid >=0 and tnim3_encounter.referralid not in (select rf.referralid from tnim3_referral rf join tnim3_encounter en on en.referralid = rf.referralid where en.encounterstatusid not in  (5,9)) and payerid in (138,139,143,147,148,149)";
		ArrayList<Integer> referralList = new ArrayList<Integer>();
		searchDB2 conn = new searchDB2();
		ResultSet rs = conn.executeStatement(query);

		while (rs.next()) {
			referralList.add(rs.getInt("referralid"));
		}
		return referralList;
	}

	private static String getServiceInfo(int eid) throws SQLException {
		String query = "select getServices(" + eid + ")";
		searchDB2 conn = new searchDB2();
		ResultSet rs = conn.executeStatement(query);
		rs.next();

		return rs.getString(1);
	}
}
