

package com.winstaff;

import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public interface InttNID_WidgetTrack extends dbTableInterface
{

    public void setWidgetTrackID(Integer newValue);
    public Integer getWidgetTrackID();
    public void setUniqueCreateDate(Date newValue);
    public Date getUniqueCreateDate();
    public void setUniqueModifyDate(Date newValue);
    public Date getUniqueModifyDate();
    public void setUniqueModifyComments(String newValue);
    public String getUniqueModifyComments();
    public void setRequestingURL(String newValue);
    public String getRequestingURL();
    public void setKeyID(String newValue);
    public String getKeyID();
    public void setZipCode(String newValue);
    public String getZipCode();
    public void setCPTWizardID(Integer newValue);
    public Integer getCPTWizardID();
}    // End of bltNID_WidgetTrack class definition
