package com.winstaff;
import java.awt.*;


public class UserUtils extends java.lang.Object
{

	public UserUtils()
	{
	}


	public static boolean isBillingCreditCardExpired(Integer iUserID)
	{
		boolean myVal = false;
		searchDB2 mySS = new searchDB2();
		String mySQL = "SELECT * from tPHDB_UserBilling_ExpiredCC where userid = " + iUserID;
		java.sql.ResultSet myRS =  mySS.executeStatement(mySQL);
		try
		{
		    if (myRS!=null&&myRS.next())
		    {
    			myVal = true;
		    }
		    else
		    {
		    }
            mySS.closeAll();
		}
		catch (Exception e)		{
		} finally {
            mySS.closeAll();
        }
		return myVal;
	}


	public static int hasNewMessage(Integer iUserID)
	{
		int myVal = 0;
		searchDB2 mySS = new searchDB2();
		String mySQL = "SELECT * from tMessage where isRead = 0 And userid = " + iUserID + " order by EmailImportance";
		java.sql.ResultSet myRS =  mySS.executeStatement(mySQL);
		try
		{
		    if (myRS!=null&&myRS.next())
		    {
			myVal = Integer.parseInt(myRS.getString ("EmailImportance"));
		    }
		    else
		    {
		    }
            mySS.closeAll();
		}
		catch (Exception e)
		{
        } finally {
            mySS.closeAll();
        }
		return myVal;
	}




	public static boolean isUserinFreeState(Integer iStateID)
	{
		boolean myVal = false;
		if (iStateID.intValue()==1||iStateID.intValue()==6||iStateID.intValue()==30||iStateID.intValue()==15)
		{
			myVal = true;
		}
		return myVal;
	}



	public static boolean isUserFree(Integer iStateID, String sTaxID)
	{
		boolean myVal = false;
		if (sTaxID.length()>=8&&isUserinFreeState(iStateID))
		{
			myVal = true;
		}

		return myVal;
	}


	public static boolean isPrePaidNegative(Integer iBillingID)
	{
		boolean myVal = false;
		searchDB2 mySS = new searchDB2();
		String mySQL = "SELECT SUM(CASE WHEN tBillingTransaction.Status = 1 THEN tBillingTransaction.SettlementAmount ELSE - tBillingTransaction.SettlementAmount END) AS q1 FROM tBillingTransaction INNER JOIN tBillingAccount ON tBillingTransaction.BillingID = tBillingAccount.BillingID WHERE (tBillingTransaction.Status = 1 OR tBillingTransaction.Status = 2) AND LEFT(wsaccountnumber, 2) = '00' AND tBillingAccount.billingid = " + iBillingID + " GROUP BY tBillingAccount.ContactFirstName, tBillingAccount.ContactLastName, tBillingAccount.ContactAddress1, tBillingAccount.ContactAddress2, tBillingAccount.ContactCity, tBillingAccount.ContactStateID, tBillingAccount.ContactZIP, tBillingAccount.ContactPhone, tBillingAccount.AccountFullName, tBillingAccount.AccountNumber1, tBillingAccount.AccountNumber2, tBillingAccount.AccountType, tBillingAccount.AccountExpMonth, tBillingAccount.AccountExpYear, tbillingAccount.BillingID, tBillingAccount.WSAccountNumber ORDER BY q1";
		java.sql.ResultSet myRS =  mySS.executeStatement(mySQL);
		try
		{
		    if (myRS!=null&&myRS.next())
		    {
				Double myValue = new Double(myRS.getString("q1"));
				if (myValue.doubleValue()<0)
				{
					myVal = true;
//					System.out.println("-------Inside----");
				}
//					System.out.println("-------Inside DB ----");
		    }
		    else
		    {
		    }
            mySS.closeAll();
		}
		catch (Exception e)
		{
			System.out.println("--UserUtils - isNegative ----" + e + "----");
        } finally {
            mySS.closeAll();
        }
		return myVal;
	}
	


	//{{DECLARE_CONTROLS
	//}}
}


//SELECT     tBillingTransaction.BillingID, SUM (  (CASE WHEN tBillingTransaction.Status = 1 THEN tBillingTransaction.SettlementAmount ELSE - tBillingTransaction.SettlementAmount END)) AS SumAmount FROM v3_app.tBillingTransaction group by BillingID order by SumAmount