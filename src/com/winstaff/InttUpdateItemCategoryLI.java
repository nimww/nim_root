

package com.winstaff;

import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public interface InttUpdateItemCategoryLI extends dbTableInterface
{

    public void setUpdateItemCategoryID(Integer newValue);
    public Integer getUpdateItemCategoryID();
    public void setUpdateItemCategoryText(String newValue);
    public String getUpdateItemCategoryText();
}    // End of bltUpdateItemCategoryLI class definition
