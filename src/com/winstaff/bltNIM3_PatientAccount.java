

package com.winstaff;

import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;
import java.util.Enumeration;

public class bltNIM3_PatientAccount extends Object implements InttNIM3_PatientAccount
{

    dbtNIM3_PatientAccount    dbDB;

    public bltNIM3_PatientAccount()
    {
        dbDB = new dbtNIM3_PatientAccount();
        dbDB.setUniqueCreateDate(new java.util.Date());
        makeRequiredHash();
        makeExpiredHash();
        makeEnglishHash();

    }    // End of default constructor

    public bltNIM3_PatientAccount( Integer iNewID )
    {        dbDB = new dbtNIM3_PatientAccount( iNewID );
        makeRequiredHash();
        makeExpiredHash();
        makeEnglishHash();
        iSecurityCheck = new Integer(2);
    }    // End of Constructor knowing an ID


    public bltNIM3_PatientAccount( Integer iNewID, Integer iGroupSecurityID )
    {
        this.GroupSecurityInit(iGroupSecurityID);
        dbDB = new dbtNIM3_PatientAccount( iNewID );
        makeExpiredHash();
        makeEnglishHash();
        makeRequiredHash(iGroupSecurityID);
    }    // End of Constructor knowing an ID

    public bltNIM3_PatientAccount( Integer iGroupSecurityID, boolean isTrue )
    {
        this.GroupSecurityInit(iGroupSecurityID);
        dbDB = new dbtNIM3_PatientAccount(  );
        makeExpiredHash();
        makeEnglishHash();
        makeRequiredHash(iGroupSecurityID);
    }    // End of Security unknown ID

    public void GroupSecurityInit( Integer iGroupSecurityID )
    {
        bltSecurityGroupMaster mySGM = new bltSecurityGroupMaster(iGroupSecurityID);
        this.UserSecurityID = iGroupSecurityID;
        if (mySGM.getRunFieldSecurityID().intValue()==1)
        {
            this.runFieldSecurity = true;
            makeRequiredHash(iGroupSecurityID);
        }
        iSecurityCheck = SecurityCheck.CheckItem("nim1", iGroupSecurityID);
    }  

    public EventReferenceType getEventReference()
    {
        return EventUtils.getEventReference("tNIM3_PatientAccount", this.getUniqueID());
    }  

    public String convertString2LogString(String normalString) 
    {
        String myVal = "";
        for (int i=0;i<normalString.length();i++)
        {
            if ( (normalString.charAt(i)!='\r')&&(normalString.charAt(i)!='\n')&&(normalString.charAt(i)!='\t') )
            {
                myVal+=normalString.charAt(i);
            }
            else
            {
                myVal+="<NLT>";
            }
        }
        return myVal;
    }
    public void runAudit()
    {
        String AuditString = new java.util.Date()+" \t " ;  
        AuditString += " tNIM3_PatientAccount "; 
        AuditString += " PatientID ="+this.getUniqueID(); 
        AuditString += " PayerID ="+this.getPayerID(); 
        for (int i=0;i<this.AuditVector.size();i++)
        {
            AuditString+="\t"+convertString2LogString((String)this.AuditVector.elementAt(i));
        }
        AuditLogger.printLine(AuditString);
    }
    public void commitData() throws SQLException
    {
        runAudit();
        dbDB.setModifyComments(this.getUniqueModifyComments());
        dbDB.setUniqueModifyDate(new java.util.Date());
        dbDB.commitData();
        DataControlUtils.dataChanged("tNIM3_PatientAccount",this.getPayerID() ,this.AuditVector.size(), "PayerID");
        this.AuditVector.removeAllElements();
    }    // End of commitData()

    public void commitDataForced() throws SQLException
    {
        dbDB.commitDataForced();
    }    // End of commitData()

    public Integer getUniqueID()
    {
        return dbDB.getUniqueID();
    }    // End of getUniqueID()

    public void setUniqueID( Integer iNewID )
    {
        dbDB.setUniqueID( iNewID );
    }    // End of setUniqueID()


    public void setPatientID(Integer newValue)
    {
        dbDB.setPatientID(newValue);
    }

    public Integer getPatientID()
    {
        return dbDB.getPatientID();
    }

    java.util.Vector AuditVector = new java.util.Vector();

    public void setUniqueCreateDate(Date newValue)
    {
        this.setUniqueCreateDate(newValue,this.UserSecurityID);


    }
    public Date getUniqueCreateDate()
    {
        return this.getUniqueCreateDate(this.UserSecurityID);
    }

    public void setUniqueCreateDate(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='UniqueCreateDate' AND TableName='tNIM3_PatientAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[UniqueCreateDate]=["+newValue+"]");
                   dbDB.setUniqueCreateDate(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[UniqueCreateDate]=["+newValue+"]");
           dbDB.setUniqueCreateDate(newValue);
         }
    }
    public Date getUniqueCreateDate(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='UniqueCreateDate' AND TableName='tNIM3_PatientAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getUniqueCreateDate();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getUniqueCreateDate();
         }
        return myVal;
    }

    public void setUniqueModifyDate(Date newValue)
    {
        this.setUniqueModifyDate(newValue,this.UserSecurityID);


    }
    public Date getUniqueModifyDate()
    {
        return this.getUniqueModifyDate(this.UserSecurityID);
    }

    public void setUniqueModifyDate(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='UniqueModifyDate' AND TableName='tNIM3_PatientAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[UniqueModifyDate]=["+newValue+"]");
                   dbDB.setUniqueModifyDate(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[UniqueModifyDate]=["+newValue+"]");
           dbDB.setUniqueModifyDate(newValue);
         }
    }
    public Date getUniqueModifyDate(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='UniqueModifyDate' AND TableName='tNIM3_PatientAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getUniqueModifyDate();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getUniqueModifyDate();
         }
        return myVal;
    }

    public void setUniqueModifyComments(String newValue)
    {
        this.setUniqueModifyComments(newValue,this.UserSecurityID);


    }
    public String getUniqueModifyComments()
    {
        return this.getUniqueModifyComments(this.UserSecurityID);
    }

    public void setUniqueModifyComments(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='UniqueModifyComments' AND TableName='tNIM3_PatientAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[UniqueModifyComments]=["+newValue+"]");
                   dbDB.setUniqueModifyComments(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[UniqueModifyComments]=["+newValue+"]");
           dbDB.setUniqueModifyComments(newValue);
         }
    }
    public String getUniqueModifyComments(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='UniqueModifyComments' AND TableName='tNIM3_PatientAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getUniqueModifyComments();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getUniqueModifyComments();
         }
        return myVal;
    }

    public void setPayerID(Integer newValue)
    {
        this.setPayerID(newValue,this.UserSecurityID);


    }
    public Integer getPayerID()
    {
        return this.getPayerID(this.UserSecurityID);
    }

    public void setPayerID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PayerID' AND TableName='tNIM3_PatientAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[PayerID]=["+newValue+"]");
                   dbDB.setPayerID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[PayerID]=["+newValue+"]");
           dbDB.setPayerID(newValue);
         }
    }
    public Integer getPayerID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PayerID' AND TableName='tNIM3_PatientAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPayerID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPayerID();
         }
        return myVal;
    }

    public void setPayerAccountIDNumber(String newValue)
    {
        this.setPayerAccountIDNumber(newValue,this.UserSecurityID);


    }
    public String getPayerAccountIDNumber()
    {
        return this.getPayerAccountIDNumber(this.UserSecurityID);
    }

    public void setPayerAccountIDNumber(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PayerAccountIDNumber' AND TableName='tNIM3_PatientAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[PayerAccountIDNumber]=["+newValue+"]");
                   dbDB.setPayerAccountIDNumber(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[PayerAccountIDNumber]=["+newValue+"]");
           dbDB.setPayerAccountIDNumber(newValue);
         }
    }
    public String getPayerAccountIDNumber(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PayerAccountIDNumber' AND TableName='tNIM3_PatientAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPayerAccountIDNumber();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPayerAccountIDNumber();
         }
        return myVal;
    }

    public void setPatientFirstName(String newValue)
    {
        this.setPatientFirstName(newValue,this.UserSecurityID);


    }
    public String getPatientFirstName()
    {
        return this.getPatientFirstName(this.UserSecurityID);
    }

    public void setPatientFirstName(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientFirstName' AND TableName='tNIM3_PatientAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[PatientFirstName]=["+newValue+"]");
                   dbDB.setPatientFirstName(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[PatientFirstName]=["+newValue+"]");
           dbDB.setPatientFirstName(newValue);
         }
    }
    public String getPatientFirstName(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientFirstName' AND TableName='tNIM3_PatientAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPatientFirstName();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPatientFirstName();
         }
        return myVal;
    }

    public void setPatientLastName(String newValue)
    {
        this.setPatientLastName(newValue,this.UserSecurityID);


    }
    public String getPatientLastName()
    {
        return this.getPatientLastName(this.UserSecurityID);
    }

    public void setPatientLastName(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientLastName' AND TableName='tNIM3_PatientAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[PatientLastName]=["+newValue+"]");
                   dbDB.setPatientLastName(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[PatientLastName]=["+newValue+"]");
           dbDB.setPatientLastName(newValue);
         }
    }
    public String getPatientLastName(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientLastName' AND TableName='tNIM3_PatientAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPatientLastName();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPatientLastName();
         }
        return myVal;
    }

    public void setPatientAccountNumber(String newValue)
    {
        this.setPatientAccountNumber(newValue,this.UserSecurityID);


    }
    public String getPatientAccountNumber()
    {
        return this.getPatientAccountNumber(this.UserSecurityID);
    }

    public void setPatientAccountNumber(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientAccountNumber' AND TableName='tNIM3_PatientAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[PatientAccountNumber]=["+newValue+"]");
                   dbDB.setPatientAccountNumber(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[PatientAccountNumber]=["+newValue+"]");
           dbDB.setPatientAccountNumber(newValue);
         }
    }
    public String getPatientAccountNumber(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientAccountNumber' AND TableName='tNIM3_PatientAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPatientAccountNumber();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPatientAccountNumber();
         }
        return myVal;
    }

    public void setPatientExpirationDate(Date newValue)
    {
        this.setPatientExpirationDate(newValue,this.UserSecurityID);


    }
    public Date getPatientExpirationDate()
    {
        return this.getPatientExpirationDate(this.UserSecurityID);
    }

    public void setPatientExpirationDate(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientExpirationDate' AND TableName='tNIM3_PatientAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[PatientExpirationDate]=["+newValue+"]");
                   dbDB.setPatientExpirationDate(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[PatientExpirationDate]=["+newValue+"]");
           dbDB.setPatientExpirationDate(newValue);
         }
    }
    public Date getPatientExpirationDate(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientExpirationDate' AND TableName='tNIM3_PatientAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPatientExpirationDate();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPatientExpirationDate();
         }
        return myVal;
    }

    public void setPatientIsActive(Integer newValue)
    {
        this.setPatientIsActive(newValue,this.UserSecurityID);


    }
    public Integer getPatientIsActive()
    {
        return this.getPatientIsActive(this.UserSecurityID);
    }

    public void setPatientIsActive(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientIsActive' AND TableName='tNIM3_PatientAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[PatientIsActive]=["+newValue+"]");
                   dbDB.setPatientIsActive(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[PatientIsActive]=["+newValue+"]");
           dbDB.setPatientIsActive(newValue);
         }
    }
    public Integer getPatientIsActive(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientIsActive' AND TableName='tNIM3_PatientAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPatientIsActive();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPatientIsActive();
         }
        return myVal;
    }

    public void setPatientDOB(Date newValue)
    {
        this.setPatientDOB(newValue,this.UserSecurityID);


    }
    public Date getPatientDOB()
    {
        return this.getPatientDOB(this.UserSecurityID);
    }

    public void setPatientDOB(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientDOB' AND TableName='tNIM3_PatientAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[PatientDOB]=["+newValue+"]");
                   dbDB.setPatientDOB(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[PatientDOB]=["+newValue+"]");
           dbDB.setPatientDOB(newValue);
         }
    }
    public Date getPatientDOB(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientDOB' AND TableName='tNIM3_PatientAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPatientDOB();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPatientDOB();
         }
        return myVal;
    }

    public void setPatientSSN(String newValue)
    {
        this.setPatientSSN(newValue,this.UserSecurityID);


    }
    public String getPatientSSN()
    {
        return this.getPatientSSN(this.UserSecurityID);
    }

    public void setPatientSSN(String newValue, Integer GroupRefID)
    {
        newValue = com.winstaff.password.Encrypt.encrypt_DESEDE_String(newValue, DataControlUtils.ENC_KEY);
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientSSN' AND TableName='tNIM3_PatientAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[PatientSSN]=["+newValue+"]");
                   dbDB.setPatientSSN(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[PatientSSN]=["+newValue+"]");
           dbDB.setPatientSSN(newValue);
         }
    }
    public String getPatientSSN(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientSSN' AND TableName='tNIM3_PatientAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPatientSSN();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPatientSSN();
         }
        if (myVal != null&&!myVal.equalsIgnoreCase(""))
        {
             myVal = com.winstaff.password.Encrypt.decrypt_DESEDE_String(myVal, DataControlUtils.ENC_KEY);
        }
        return myVal;
    }

    public void setPatientGender(String newValue)
    {
        this.setPatientGender(newValue,this.UserSecurityID);


    }
    public String getPatientGender()
    {
        return this.getPatientGender(this.UserSecurityID);
    }

    public void setPatientGender(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientGender' AND TableName='tNIM3_PatientAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[PatientGender]=["+newValue+"]");
                   dbDB.setPatientGender(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[PatientGender]=["+newValue+"]");
           dbDB.setPatientGender(newValue);
         }
    }
    public String getPatientGender(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientGender' AND TableName='tNIM3_PatientAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPatientGender();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPatientGender();
         }
        return myVal;
    }

    public void setPatientAddress1(String newValue)
    {
        this.setPatientAddress1(newValue,this.UserSecurityID);


    }
    public String getPatientAddress1()
    {
        return this.getPatientAddress1(this.UserSecurityID);
    }

    public void setPatientAddress1(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientAddress1' AND TableName='tNIM3_PatientAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[PatientAddress1]=["+newValue+"]");
                   dbDB.setPatientAddress1(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[PatientAddress1]=["+newValue+"]");
           dbDB.setPatientAddress1(newValue);
         }
    }
    public String getPatientAddress1(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientAddress1' AND TableName='tNIM3_PatientAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPatientAddress1();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPatientAddress1();
         }
        return myVal;
    }

    public void setPatientAddress2(String newValue)
    {
        this.setPatientAddress2(newValue,this.UserSecurityID);


    }
    public String getPatientAddress2()
    {
        return this.getPatientAddress2(this.UserSecurityID);
    }

    public void setPatientAddress2(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientAddress2' AND TableName='tNIM3_PatientAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[PatientAddress2]=["+newValue+"]");
                   dbDB.setPatientAddress2(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[PatientAddress2]=["+newValue+"]");
           dbDB.setPatientAddress2(newValue);
         }
    }
    public String getPatientAddress2(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientAddress2' AND TableName='tNIM3_PatientAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPatientAddress2();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPatientAddress2();
         }
        return myVal;
    }

    public void setPatientCity(String newValue)
    {
        this.setPatientCity(newValue,this.UserSecurityID);


    }
    public String getPatientCity()
    {
        return this.getPatientCity(this.UserSecurityID);
    }

    public void setPatientCity(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientCity' AND TableName='tNIM3_PatientAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[PatientCity]=["+newValue+"]");
                   dbDB.setPatientCity(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[PatientCity]=["+newValue+"]");
           dbDB.setPatientCity(newValue);
         }
    }
    public String getPatientCity(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientCity' AND TableName='tNIM3_PatientAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPatientCity();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPatientCity();
         }
        return myVal;
    }

    public void setPatientStateID(Integer newValue)
    {
        this.setPatientStateID(newValue,this.UserSecurityID);


    }
    public Integer getPatientStateID()
    {
        return this.getPatientStateID(this.UserSecurityID);
    }

    public void setPatientStateID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientStateID' AND TableName='tNIM3_PatientAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[PatientStateID]=["+newValue+"]");
                   dbDB.setPatientStateID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[PatientStateID]=["+newValue+"]");
           dbDB.setPatientStateID(newValue);
         }
    }
    public Integer getPatientStateID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientStateID' AND TableName='tNIM3_PatientAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPatientStateID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPatientStateID();
         }
        return myVal;
    }

    public void setPatientZIP(String newValue)
    {
        this.setPatientZIP(newValue,this.UserSecurityID);


    }
    public String getPatientZIP()
    {
        return this.getPatientZIP(this.UserSecurityID);
    }

    public void setPatientZIP(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientZIP' AND TableName='tNIM3_PatientAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[PatientZIP]=["+newValue+"]");
                   dbDB.setPatientZIP(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[PatientZIP]=["+newValue+"]");
           dbDB.setPatientZIP(newValue);
         }
    }
    public String getPatientZIP(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientZIP' AND TableName='tNIM3_PatientAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPatientZIP();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPatientZIP();
         }
        return myVal;
    }

    public void setPatientHomePhone(String newValue)
    {
        this.setPatientHomePhone(newValue,this.UserSecurityID);


    }
    public String getPatientHomePhone()
    {
        return this.getPatientHomePhone(this.UserSecurityID);
    }

    public void setPatientHomePhone(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientHomePhone' AND TableName='tNIM3_PatientAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[PatientHomePhone]=["+newValue+"]");
                   dbDB.setPatientHomePhone(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[PatientHomePhone]=["+newValue+"]");
           dbDB.setPatientHomePhone(newValue);
         }
    }
    public String getPatientHomePhone(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientHomePhone' AND TableName='tNIM3_PatientAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPatientHomePhone();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPatientHomePhone();
         }
        return myVal;
    }

    public void setPatientWorkPhone(String newValue)
    {
        this.setPatientWorkPhone(newValue,this.UserSecurityID);


    }
    public String getPatientWorkPhone()
    {
        return this.getPatientWorkPhone(this.UserSecurityID);
    }

    public void setPatientWorkPhone(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientWorkPhone' AND TableName='tNIM3_PatientAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[PatientWorkPhone]=["+newValue+"]");
                   dbDB.setPatientWorkPhone(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[PatientWorkPhone]=["+newValue+"]");
           dbDB.setPatientWorkPhone(newValue);
         }
    }
    public String getPatientWorkPhone(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientWorkPhone' AND TableName='tNIM3_PatientAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPatientWorkPhone();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPatientWorkPhone();
         }
        return myVal;
    }

    public void setPatientCellPhone(String newValue)
    {
        this.setPatientCellPhone(newValue,this.UserSecurityID);


    }
    public String getPatientCellPhone()
    {
        return this.getPatientCellPhone(this.UserSecurityID);
    }

    public void setPatientCellPhone(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientCellPhone' AND TableName='tNIM3_PatientAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[PatientCellPhone]=["+newValue+"]");
                   dbDB.setPatientCellPhone(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[PatientCellPhone]=["+newValue+"]");
           dbDB.setPatientCellPhone(newValue);
         }
    }
    public String getPatientCellPhone(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientCellPhone' AND TableName='tNIM3_PatientAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPatientCellPhone();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPatientCellPhone();
         }
        return myVal;
    }

    public void setPatientCCAccountFullName(String newValue)
    {
        this.setPatientCCAccountFullName(newValue,this.UserSecurityID);


    }
    public String getPatientCCAccountFullName()
    {
        return this.getPatientCCAccountFullName(this.UserSecurityID);
    }

    public void setPatientCCAccountFullName(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientCCAccountFullName' AND TableName='tNIM3_PatientAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[PatientCCAccountFullName]=["+newValue+"]");
                   dbDB.setPatientCCAccountFullName(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[PatientCCAccountFullName]=["+newValue+"]");
           dbDB.setPatientCCAccountFullName(newValue);
         }
    }
    public String getPatientCCAccountFullName(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientCCAccountFullName' AND TableName='tNIM3_PatientAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPatientCCAccountFullName();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPatientCCAccountFullName();
         }
        return myVal;
    }

    public void setPatientCCAccountNumber1(String newValue)
    {
        this.setPatientCCAccountNumber1(newValue,this.UserSecurityID);


    }
    public String getPatientCCAccountNumber1()
    {
        return this.getPatientCCAccountNumber1(this.UserSecurityID);
    }

    public void setPatientCCAccountNumber1(String newValue, Integer GroupRefID)
    {
        newValue = com.winstaff.password.Encrypt.encrypt_DESEDE_String(newValue, DataControlUtils.ENC_KEY);
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientCCAccountNumber1' AND TableName='tNIM3_PatientAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[PatientCCAccountNumber1]=["+newValue+"]");
                   dbDB.setPatientCCAccountNumber1(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[PatientCCAccountNumber1]=["+newValue+"]");
           dbDB.setPatientCCAccountNumber1(newValue);
         }
    }
    public String getPatientCCAccountNumber1(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientCCAccountNumber1' AND TableName='tNIM3_PatientAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPatientCCAccountNumber1();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPatientCCAccountNumber1();
         }
        if (myVal != null&&!myVal.equalsIgnoreCase(""))
        {
             myVal = com.winstaff.password.Encrypt.decrypt_DESEDE_String(myVal, DataControlUtils.ENC_KEY);
        }
        return myVal;
    }

    public void setPatientCCAccountNumber2(String newValue)
    {
        this.setPatientCCAccountNumber2(newValue,this.UserSecurityID);


    }
    public String getPatientCCAccountNumber2()
    {
        return this.getPatientCCAccountNumber2(this.UserSecurityID);
    }

    public void setPatientCCAccountNumber2(String newValue, Integer GroupRefID)
    {
        newValue = com.winstaff.password.Encrypt.encrypt_DESEDE_String(newValue, DataControlUtils.ENC_KEY);
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientCCAccountNumber2' AND TableName='tNIM3_PatientAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[PatientCCAccountNumber2]=["+newValue+"]");
                   dbDB.setPatientCCAccountNumber2(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[PatientCCAccountNumber2]=["+newValue+"]");
           dbDB.setPatientCCAccountNumber2(newValue);
         }
    }
    public String getPatientCCAccountNumber2(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientCCAccountNumber2' AND TableName='tNIM3_PatientAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPatientCCAccountNumber2();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPatientCCAccountNumber2();
         }
        if (myVal != null&&!myVal.equalsIgnoreCase(""))
        {
             myVal = com.winstaff.password.Encrypt.decrypt_DESEDE_String(myVal, DataControlUtils.ENC_KEY);
        }
        return myVal;
    }

    public void setPatientCCAccountType(String newValue)
    {
        this.setPatientCCAccountType(newValue,this.UserSecurityID);


    }
    public String getPatientCCAccountType()
    {
        return this.getPatientCCAccountType(this.UserSecurityID);
    }

    public void setPatientCCAccountType(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientCCAccountType' AND TableName='tNIM3_PatientAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[PatientCCAccountType]=["+newValue+"]");
                   dbDB.setPatientCCAccountType(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[PatientCCAccountType]=["+newValue+"]");
           dbDB.setPatientCCAccountType(newValue);
         }
    }
    public String getPatientCCAccountType(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientCCAccountType' AND TableName='tNIM3_PatientAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPatientCCAccountType();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPatientCCAccountType();
         }
        return myVal;
    }

    public void setPatientCCAccountExpMonth(String newValue)
    {
        this.setPatientCCAccountExpMonth(newValue,this.UserSecurityID);


    }
    public String getPatientCCAccountExpMonth()
    {
        return this.getPatientCCAccountExpMonth(this.UserSecurityID);
    }

    public void setPatientCCAccountExpMonth(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientCCAccountExpMonth' AND TableName='tNIM3_PatientAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[PatientCCAccountExpMonth]=["+newValue+"]");
                   dbDB.setPatientCCAccountExpMonth(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[PatientCCAccountExpMonth]=["+newValue+"]");
           dbDB.setPatientCCAccountExpMonth(newValue);
         }
    }
    public String getPatientCCAccountExpMonth(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientCCAccountExpMonth' AND TableName='tNIM3_PatientAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPatientCCAccountExpMonth();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPatientCCAccountExpMonth();
         }
        return myVal;
    }

    public void setPatientCCAccountExpYear(String newValue)
    {
        this.setPatientCCAccountExpYear(newValue,this.UserSecurityID);


    }
    public String getPatientCCAccountExpYear()
    {
        return this.getPatientCCAccountExpYear(this.UserSecurityID);
    }

    public void setPatientCCAccountExpYear(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientCCAccountExpYear' AND TableName='tNIM3_PatientAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[PatientCCAccountExpYear]=["+newValue+"]");
                   dbDB.setPatientCCAccountExpYear(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[PatientCCAccountExpYear]=["+newValue+"]");
           dbDB.setPatientCCAccountExpYear(newValue);
         }
    }
    public String getPatientCCAccountExpYear(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientCCAccountExpYear' AND TableName='tNIM3_PatientAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPatientCCAccountExpYear();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPatientCCAccountExpYear();
         }
        return myVal;
    }

    public void setEmployerID(Integer newValue)
    {
        this.setEmployerID(newValue,this.UserSecurityID);


    }
    public Integer getEmployerID()
    {
        return this.getEmployerID(this.UserSecurityID);
    }

    public void setEmployerID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='EmployerID' AND TableName='tNIM3_PatientAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[EmployerID]=["+newValue+"]");
                   dbDB.setEmployerID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[EmployerID]=["+newValue+"]");
           dbDB.setEmployerID(newValue);
         }
    }
    public Integer getEmployerID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='EmployerID' AND TableName='tNIM3_PatientAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getEmployerID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getEmployerID();
         }
        return myVal;
    }

    public void setJobID(Integer newValue)
    {
        this.setJobID(newValue,this.UserSecurityID);


    }
    public Integer getJobID()
    {
        return this.getJobID(this.UserSecurityID);
    }

    public void setJobID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='JobID' AND TableName='tNIM3_PatientAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[JobID]=["+newValue+"]");
                   dbDB.setJobID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[JobID]=["+newValue+"]");
           dbDB.setJobID(newValue);
         }
    }
    public Integer getJobID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='JobID' AND TableName='tNIM3_PatientAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getJobID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getJobID();
         }
        return myVal;
    }

    public void setJobNotes(String newValue)
    {
        this.setJobNotes(newValue,this.UserSecurityID);


    }
    public String getJobNotes()
    {
        return this.getJobNotes(this.UserSecurityID);
    }

    public void setJobNotes(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='JobNotes' AND TableName='tNIM3_PatientAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[JobNotes]=["+newValue+"]");
                   dbDB.setJobNotes(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[JobNotes]=["+newValue+"]");
           dbDB.setJobNotes(newValue);
         }
    }
    public String getJobNotes(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='JobNotes' AND TableName='tNIM3_PatientAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getJobNotes();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getJobNotes();
         }
        return myVal;
    }

    public void setSupervisorName(String newValue)
    {
        this.setSupervisorName(newValue,this.UserSecurityID);


    }
    public String getSupervisorName()
    {
        return this.getSupervisorName(this.UserSecurityID);
    }

    public void setSupervisorName(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SupervisorName' AND TableName='tNIM3_PatientAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[SupervisorName]=["+newValue+"]");
                   dbDB.setSupervisorName(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[SupervisorName]=["+newValue+"]");
           dbDB.setSupervisorName(newValue);
         }
    }
    public String getSupervisorName(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SupervisorName' AND TableName='tNIM3_PatientAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getSupervisorName();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getSupervisorName();
         }
        return myVal;
    }

    public void setSupervisorPhone(String newValue)
    {
        this.setSupervisorPhone(newValue,this.UserSecurityID);


    }
    public String getSupervisorPhone()
    {
        return this.getSupervisorPhone(this.UserSecurityID);
    }

    public void setSupervisorPhone(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SupervisorPhone' AND TableName='tNIM3_PatientAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[SupervisorPhone]=["+newValue+"]");
                   dbDB.setSupervisorPhone(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[SupervisorPhone]=["+newValue+"]");
           dbDB.setSupervisorPhone(newValue);
         }
    }
    public String getSupervisorPhone(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SupervisorPhone' AND TableName='tNIM3_PatientAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getSupervisorPhone();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getSupervisorPhone();
         }
        return myVal;
    }

    public void setSupervisorFax(String newValue)
    {
        this.setSupervisorFax(newValue,this.UserSecurityID);


    }
    public String getSupervisorFax()
    {
        return this.getSupervisorFax(this.UserSecurityID);
    }

    public void setSupervisorFax(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SupervisorFax' AND TableName='tNIM3_PatientAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[SupervisorFax]=["+newValue+"]");
                   dbDB.setSupervisorFax(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[SupervisorFax]=["+newValue+"]");
           dbDB.setSupervisorFax(newValue);
         }
    }
    public String getSupervisorFax(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SupervisorFax' AND TableName='tNIM3_PatientAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getSupervisorFax();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getSupervisorFax();
         }
        return myVal;
    }

    public void setComments(String newValue)
    {
        this.setComments(newValue,this.UserSecurityID);


    }
    public String getComments()
    {
        return this.getComments(this.UserSecurityID);
    }

    public void setComments(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Comments' AND TableName='tNIM3_PatientAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[Comments]=["+newValue+"]");
                   dbDB.setComments(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[Comments]=["+newValue+"]");
           dbDB.setComments(newValue);
         }
    }
    public String getComments(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Comments' AND TableName='tNIM3_PatientAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getComments();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getComments();
         }
        return myVal;
    }

    public void setAuditNotes(String newValue)
    {
        this.setAuditNotes(newValue,this.UserSecurityID);


    }
    public String getAuditNotes()
    {
        return this.getAuditNotes(this.UserSecurityID);
    }

    public void setAuditNotes(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AuditNotes' AND TableName='tNIM3_PatientAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[AuditNotes]=["+newValue+"]");
                   dbDB.setAuditNotes(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[AuditNotes]=["+newValue+"]");
           dbDB.setAuditNotes(newValue);
         }
    }
    public String getAuditNotes(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AuditNotes' AND TableName='tNIM3_PatientAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getAuditNotes();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getAuditNotes();
         }
        return myVal;
    }

    public void setRequiredFieldList(java.util.Hashtable requiredFieldList)
	 {

        this.requiredFieldList = requiredFieldList;
    }

    public java.util.Hashtable getRequiredFieldList()
    {
        return this.requiredFieldList;
    }
    public void setExpiredFieldList(java.util.Hashtable expiredFieldList)
    {
        this.expiredFieldList = expiredFieldList;
    }

    public void setEnglishFieldList(java.util.Hashtable englishFieldList)
    {
        this.englishFieldList = englishFieldList;
    }

    public java.util.Hashtable getExpiredFieldList()
    {
        return this.expiredFieldList;
    }
    public boolean isExpired(java.util.Date myDate, int daysVector)
    {
       return isExpired(myDate,new java.util.Date(),daysVector);
    }


    public boolean isRead(String fieldN, Integer GroupRefID)
    {
        boolean myVal = false;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='"+fieldN+"' AND TableName='tNIM3_PatientAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==1)
               {
                   myVal = true;
               }
            }
        }
        else
        {

           if (iSecurityCheck.intValue()>=1)
           {
                  myVal = true;
           }
        }
        return myVal;
    }


    public boolean isWrite(String fieldN, Integer GroupRefID)
    {
        boolean myVal = false;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='"+fieldN+"' AND TableName='tNIM3_PatientAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {
                   myVal = true;
               }
            }
        }
        else
        {

           if (iSecurityCheck.intValue()>=2)
           {
                  myVal = true;
           }
        }
        return myVal;
    }


    public boolean isExpired(java.util.Date myDate, java.util.Date refDate,int daysVector)
    {
        java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat("yyyy-MM-dd");
        boolean myVal = true;
        try
        {
            if (myDate.after(dbdf.parse("1800-01-10")))
            {
                if (myDate.after(dateChange(refDate,daysVector)))
                {
                    myVal=false;
                }
            }
            else
            {
                myVal = false;
            }
        }
        catch (Exception Datee)
        {
        }
        return myVal;
    }

    public java.util.Date dateChange(java.util.Date myD, int dayChange)
    {
        java.util.Calendar calendar = java.util.Calendar.getInstance();
        calendar.setTime(myD);
        calendar.add(java.util.Calendar.DATE, dayChange);
        java.util.Date resultAsDate = new java.util.Date(calendar.getTime().getTime());
        return resultAsDate;
    }

    public boolean isRequired(String fieldN)
    {
        return isRequired(fieldN, new Integer("1"));
    }

    public boolean isRequired(String fieldN, Integer GroupRefID)
    {
        boolean myVal = false;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='"+fieldN+"' AND TableName='tNIM3_PatientAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getRequiredID().intValue()==1)
               {
                   myVal = true;
               }
            }
        }
        else
        {
            if (this.getRequiredFieldList().containsKey(fieldN))
            {
                myVal = true;
            }
        }
        return myVal;
    }
    public boolean isExpiredCheck(String fieldN)
    {
        return isExpiredCheck(fieldN, new Integer ("1"));
    }

    public boolean isExpiredCheck(String fieldN, Integer GroupRefID)
    {
        boolean myVal = false;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='"+fieldN+"' AND TableName='tNIM3_PatientAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getTrackedID().intValue()==1)
               {
                   myVal = true;
               }
            }
        }
        else
        {
            if (this.getExpiredFieldList().containsKey(fieldN))
           {
                myVal = true;
            }
        }
        return myVal;
    }

    public void makeRequiredHash()
    {
       makeRequiredHash(new Integer(1));
    }

    public void makeRequiredHash(Integer GroupRefID)
    {

        java.util.Hashtable newHash = new java.util.Hashtable();
        if (runFieldSecurity)
        {
            java.util.Enumeration myKeys = englishFieldList.keys(); 
            while (myKeys.hasMoreElements()) 
            {
                 String myElement = (String) myKeys.nextElement();
                 bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='"+myElement+"' AND TableName='tNIM3_PatientAccount'" ,"");
                 bltFieldSecurity        working_bltFieldSecurity;
                 ListElement         leCurrentElement;
                 Enumeration eList = myFSL.elements();
                 if (eList.hasMoreElements())
                 {
                     leCurrentElement    = (ListElement) eList.nextElement();
                     working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
                     if (working_bltFieldSecurity.getRequiredID().intValue()==1)
                     {
                         newHash.put(myElement,new Boolean(true));
                     }
                 }
            }
        }
        else
        {
            newHash.put("PayerID",new Boolean(true));
            newHash.put("PayerAccountIDNumber",new Boolean(true));
            newHash.put("PatientFirstName",new Boolean(true));
            newHash.put("PatientLastName",new Boolean(true));
            newHash.put("PatientDOB",new Boolean(true));
            newHash.put("PatientSSN",new Boolean(true));
            newHash.put("PatientGender",new Boolean(true));
            newHash.put("PatientAddress1",new Boolean(true));
            newHash.put("PatientCity",new Boolean(true));
            newHash.put("PatientStateID",new Boolean(true));
            newHash.put("PatientZIP",new Boolean(true));
            newHash.put("PatientHomePhone",new Boolean(true));
            newHash.put("PatientCellPhone",new Boolean(true));
        }
        this.setRequiredFieldList(newHash);
    }

    public void makeEnglishHash()
    {

        java.util.Hashtable newHash = new java.util.Hashtable();        newHash.put("UniqueCreateDate","UniqueCreateDate");
        newHash.put("UniqueModifyDate","UniqueModifyDate");
        newHash.put("UniqueModifyComments","UniqueModifyComments");
        newHash.put("PayerID","PayerID");
        newHash.put("PayerAccountIDNumber","Payer Account ID Number");
        newHash.put("PatientFirstName","Patient First Name");
        newHash.put("PatientLastName","PatientLastName");
        newHash.put("PatientAccountNumber","Patient Account Number");
        newHash.put("PatientExpirationDate","Patient Expiration Date");
        newHash.put("PatientIsActive","Patient Active");
        newHash.put("PatientDOB","Patient DOB");
        newHash.put("PatientSSN","Patient SSN");
        newHash.put("PatientGender","Patient Gender");
        newHash.put("PatientAddress1","Patient Address1");
        newHash.put("PatientAddress2","Patient Address2");
        newHash.put("PatientCity","Patient City");
        newHash.put("PatientStateID","Patient State");
        newHash.put("PatientZIP","Patient ZIP");
        newHash.put("PatientHomePhone","Patient Home Phone");
        newHash.put("PatientWorkPhone","Patient Work Phone");
        newHash.put("PatientCellPhone","Patient Cell Phone");
        newHash.put("PatientCCAccountFullName","Patient CC Full Name");
        newHash.put("PatientCCAccountNumber1","Patient CC Account Number1");
        newHash.put("PatientCCAccountNumber2","Patient CC Account Number2");
        newHash.put("PatientCCAccountType","Patient CC Account Type");
        newHash.put("PatientCCAccountExpMonth","Patient CC ExpMonth");
        newHash.put("PatientCCAccountExpYear","Patient CC ExpYear");
        newHash.put("EmployerID","EmployerID");
        newHash.put("JobID","JobID");
        newHash.put("JobNotes","JobNotes");
        newHash.put("SupervisorName","SupervisorName");
        newHash.put("SupervisorPhone","SupervisorPhone");
        newHash.put("SupervisorFax","SupervisorFax");
        newHash.put("Comments","Comments");
        newHash.put("AuditNotes","Audit Notes");

        this.setEnglishFieldList(newHash);
    }
    public void makeExpiredHash()
    {

        java.util.Hashtable newHash = new java.util.Hashtable();
        this.setExpiredFieldList(newHash);
    }

    public java.util.Hashtable englishFieldList = new java.util.Hashtable();
    public java.util.Hashtable requiredFieldList = new java.util.Hashtable();
    public java.util.Hashtable expiredFieldList = new java.util.Hashtable();

    public boolean isComplete()
    {
	     boolean myVal = true;
	     java.util.Enumeration myKeys = this.getRequiredFieldList().keys(); 
	     while (myKeys.hasMoreElements()) 
	     {
	         String myElement = (String) myKeys.nextElement();
	         if (!isComplete(myElement))
	         {
	             myVal = false;
	         }
	     }
	     return myVal;
    }

    public String getEnglish(String fieldN)
    {
        String myVal = null;
        if (this.englishFieldList.containsKey(fieldN))
        {
            myVal = (String)this.englishFieldList.get(fieldN);
        }
        return myVal;
    }
    public java.util.Vector isComplete(int daysVector)
    {
	     boolean myVal = false;
	     java.util.Enumeration myKeys = this.getExpiredFieldList().keys(); 
	     java.util.Vector myExpiredList = new java.util.Vector(); 
	     while (myKeys.hasMoreElements()) 
	     {
	         String myElement = (String) myKeys.nextElement();
	         if (isExpired(myElement,daysVector))
	         {
	             myVal = true;
	             myExpiredList.addElement(myElement);
	         }
	     }
	     return myExpiredList;
    }

    public void setField(String fieldN, Object fieldV)
    {
     boolean myVal = false;
     try
     {
        java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat("yyyy-MM-dd");

        if (fieldN.equals("PatientID"))
        {
             this.setPatientID((Integer)fieldV);
        }

        else if (fieldN.equals("UniqueCreateDate"))
        {
            this.setUniqueCreateDate((java.util.Date)fieldV);
        }

        else if (fieldN.equals("UniqueModifyDate"))
        {
            this.setUniqueModifyDate((java.util.Date)fieldV);
        }

        else if (fieldN.equals("UniqueModifyComments"))
        {
            this.setUniqueModifyComments((String)fieldV);
        }

        else if (fieldN.equals("PayerID"))
        {
             this.setPayerID((Integer)fieldV);
        }

        else if (fieldN.equals("PayerAccountIDNumber"))
        {
            this.setPayerAccountIDNumber((String)fieldV);
        }

        else if (fieldN.equals("PatientFirstName"))
        {
            this.setPatientFirstName((String)fieldV);
        }

        else if (fieldN.equals("PatientLastName"))
        {
            this.setPatientLastName((String)fieldV);
        }

        else if (fieldN.equals("PatientAccountNumber"))
        {
            this.setPatientAccountNumber((String)fieldV);
        }

        else if (fieldN.equals("PatientExpirationDate"))
        {
            this.setPatientExpirationDate((java.util.Date)fieldV);
        }

        else if (fieldN.equals("PatientIsActive"))
        {
             this.setPatientIsActive((Integer)fieldV);
        }

        else if (fieldN.equals("PatientDOB"))
        {
            this.setPatientDOB((java.util.Date)fieldV);
        }

        else if (fieldN.equals("PatientSSN"))
        {
            this.setPatientSSN((String)fieldV);
        }

        else if (fieldN.equals("PatientGender"))
        {
            this.setPatientGender((String)fieldV);
        }

        else if (fieldN.equals("PatientAddress1"))
        {
            this.setPatientAddress1((String)fieldV);
        }

        else if (fieldN.equals("PatientAddress2"))
        {
            this.setPatientAddress2((String)fieldV);
        }

        else if (fieldN.equals("PatientCity"))
        {
            this.setPatientCity((String)fieldV);
        }

        else if (fieldN.equals("PatientStateID"))
        {
             this.setPatientStateID((Integer)fieldV);
        }

        else if (fieldN.equals("PatientZIP"))
        {
            this.setPatientZIP((String)fieldV);
        }

        else if (fieldN.equals("PatientHomePhone"))
        {
            this.setPatientHomePhone((String)fieldV);
        }

        else if (fieldN.equals("PatientWorkPhone"))
        {
            this.setPatientWorkPhone((String)fieldV);
        }

        else if (fieldN.equals("PatientCellPhone"))
        {
            this.setPatientCellPhone((String)fieldV);
        }

        else if (fieldN.equals("PatientCCAccountFullName"))
        {
            this.setPatientCCAccountFullName((String)fieldV);
        }

        else if (fieldN.equals("PatientCCAccountNumber1"))
        {
            this.setPatientCCAccountNumber1((String)fieldV);
        }

        else if (fieldN.equals("PatientCCAccountNumber2"))
        {
            this.setPatientCCAccountNumber2((String)fieldV);
        }

        else if (fieldN.equals("PatientCCAccountType"))
        {
            this.setPatientCCAccountType((String)fieldV);
        }

        else if (fieldN.equals("PatientCCAccountExpMonth"))
        {
            this.setPatientCCAccountExpMonth((String)fieldV);
        }

        else if (fieldN.equals("PatientCCAccountExpYear"))
        {
            this.setPatientCCAccountExpYear((String)fieldV);
        }

        else if (fieldN.equals("EmployerID"))
        {
             this.setEmployerID((Integer)fieldV);
        }

        else if (fieldN.equals("JobID"))
        {
             this.setJobID((Integer)fieldV);
        }

        else if (fieldN.equals("JobNotes"))
        {
            this.setJobNotes((String)fieldV);
        }

        else if (fieldN.equals("SupervisorName"))
        {
            this.setSupervisorName((String)fieldV);
        }

        else if (fieldN.equals("SupervisorPhone"))
        {
            this.setSupervisorPhone((String)fieldV);
        }

        else if (fieldN.equals("SupervisorFax"))
        {
            this.setSupervisorFax((String)fieldV);
        }

        else if (fieldN.equals("Comments"))
        {
            this.setComments((String)fieldV);
        }

        else if (fieldN.equals("AuditNotes"))
        {
            this.setAuditNotes((String)fieldV);
        }

	    }
	    catch(Exception e2)
	    {}
	    //return myVal;
	}

    public String getFieldType(String fieldN)
    {
     String  myVal = null;
     try
     {
        java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat("yyyy-MM-dd");

        if (fieldN.equals("PatientID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("UniqueCreateDate"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("UniqueModifyDate"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("UniqueModifyComments"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("PayerID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("PayerAccountIDNumber"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("PatientFirstName"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("PatientLastName"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("PatientAccountNumber"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("PatientExpirationDate"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("PatientIsActive"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("PatientDOB"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("PatientSSN"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("PatientGender"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("PatientAddress1"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("PatientAddress2"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("PatientCity"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("PatientStateID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("PatientZIP"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("PatientHomePhone"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("PatientWorkPhone"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("PatientCellPhone"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("PatientCCAccountFullName"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("PatientCCAccountNumber1"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("PatientCCAccountNumber2"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("PatientCCAccountType"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("PatientCCAccountExpMonth"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("PatientCCAccountExpYear"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("EmployerID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("JobID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("JobNotes"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("SupervisorName"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("SupervisorPhone"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("SupervisorFax"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("Comments"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("AuditNotes"))
        {
            myVal = "String";
        }

	    }
	    catch(Exception e2)
	    {}
	    return myVal;
	}

    public boolean isComplete(String fieldN)
    {
	     boolean myVal = false;
	     try
	     {
        java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat("yyyy-MM-dd");

	     if (fieldN.equals("PatientID"))

	     {
                    if (this.getPatientID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("UniqueCreateDate"))

	     {
                    if (this.getUniqueCreateDate().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("UniqueModifyDate"))

	     {
                    if (this.getUniqueModifyDate().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("UniqueModifyComments"))

	     {
                    if (this.getUniqueModifyComments().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("PayerID"))

	     {
                    if (this.getPayerID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("PayerAccountIDNumber"))

	     {
                    if (this.getPayerAccountIDNumber().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("PatientFirstName"))

	     {
                    if (this.getPatientFirstName().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("PatientLastName"))

	     {
                    if (this.getPatientLastName().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("PatientAccountNumber"))

	     {
                    if (this.getPatientAccountNumber().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("PatientExpirationDate"))

	     {
                    if (this.getPatientExpirationDate().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("PatientIsActive"))

	     {
                    if (this.getPatientIsActive().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("PatientDOB"))

	     {
                    if (this.getPatientDOB().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("PatientSSN"))

	     {
                    if (this.getPatientSSN().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("PatientGender"))

	     {
                    if (this.getPatientGender().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("PatientAddress1"))

	     {
                    if (this.getPatientAddress1().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("PatientAddress2"))

	     {
                    if (this.getPatientAddress2().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("PatientCity"))

	     {
                    if (this.getPatientCity().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("PatientStateID"))

	     {
                    if (this.getPatientStateID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("PatientZIP"))

	     {
                    if (this.getPatientZIP().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("PatientHomePhone"))

	     {
                    if (this.getPatientHomePhone().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("PatientWorkPhone"))

	     {
                    if (this.getPatientWorkPhone().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("PatientCellPhone"))

	     {
                    if (this.getPatientCellPhone().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("PatientCCAccountFullName"))

	     {
                    if (this.getPatientCCAccountFullName().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("PatientCCAccountNumber1"))

	     {
                    if (this.getPatientCCAccountNumber1().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("PatientCCAccountNumber2"))

	     {
                    if (this.getPatientCCAccountNumber2().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("PatientCCAccountType"))

	     {
                    if (this.getPatientCCAccountType().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("PatientCCAccountExpMonth"))

	     {
                    if (this.getPatientCCAccountExpMonth().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("PatientCCAccountExpYear"))

	     {
                    if (this.getPatientCCAccountExpYear().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("EmployerID"))

	     {
                    if (this.getEmployerID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("JobID"))

	     {
                    if (this.getJobID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("JobNotes"))

	     {
                    if (this.getJobNotes().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("SupervisorName"))

	     {
                    if (this.getSupervisorName().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("SupervisorPhone"))

	     {
                    if (this.getSupervisorPhone().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("SupervisorFax"))

	     {
                    if (this.getSupervisorFax().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("Comments"))

	     {
                    if (this.getComments().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("AuditNotes"))

	     {
                    if (this.getAuditNotes().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	    }
	    catch(Exception e2)
	    {}
	    return myVal;
	}

    public boolean isExpired(String fieldN,int daysVector)
    {
        return isExpired(fieldN,new java.util.Date(),daysVector);
    }

    public boolean isExpired(String fieldN,java.util.Date refDate,int daysVector)
    {
	     boolean myVal = false;
	     try
	     {
        java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat("yyyy-MM-dd");

        if (fieldN.equals("UniqueCreateDate"))

	     {
                    if (this.isExpired(this.getUniqueCreateDate(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

        if (fieldN.equals("UniqueModifyDate"))

	     {
                    if (this.isExpired(this.getUniqueModifyDate(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

        if (fieldN.equals("PatientExpirationDate"))

	     {
                    if (this.isExpired(this.getPatientExpirationDate(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

        if (fieldN.equals("PatientDOB"))

	     {
                    if (this.isExpired(this.getPatientDOB(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

	    }
	    catch(Exception e2)
	    {}
	    return myVal;
	}

    public java.util.Date getDateField(String fieldN)
    {
	     java.util.Date myVal = null;
	     try
	     {
        java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat("yyyy-MM-dd");

        if (fieldN.equals("UniqueCreateDate"))

	     {
             myVal = this.getUniqueCreateDate();
        }

        if (fieldN.equals("UniqueModifyDate"))

	     {
             myVal = this.getUniqueModifyDate();
        }

        if (fieldN.equals("PatientExpirationDate"))

	     {
             myVal = this.getPatientExpirationDate();
        }

        if (fieldN.equals("PatientDOB"))

	     {
             myVal = this.getPatientDOB();
        }

	    }
	    catch(Exception e2)
	    {}
	    return myVal;
	}
	public boolean runFieldSecurity = false;
	public Integer UserSecurityID = null;
   public Integer iSecurityCheck = null;
}    // End of bltNIM3_PatientAccount class definition
