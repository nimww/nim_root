

package com.winstaff;

import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public class dbtNIM3_EmployerAccount extends Object implements InttNIM3_EmployerAccount
{

        db_NewBase    dbnbDB;

    public dbtNIM3_EmployerAccount()
    {
        dbnbDB = new db_NewBase( "tNIM3_EmployerAccount", "EmployerID" );

    }    // End of default constructor

    public dbtNIM3_EmployerAccount( Integer iNewID )
    {
        dbnbDB = new db_NewBase( "tNIM3_EmployerAccount", "EmployerID" );
        setUniqueID( iNewID );
    }    // End of Constructor knowing an ID

    public void commitData() throws SQLException
    {
            //no longer trapping/ignoring errors 2011-03-17
            dbnbDB.commitData();
    }    // End of commitData()

    public void commitDataForced() throws SQLException
    {
        dbnbDB.commitDataForced();
    }    // End of commitData()

    public Integer getUniqueID()
    {
        return dbnbDB.getUniqueID();
    }    // End of getUniqueID()

    public void setUniqueID( Integer iNewID )
    {
        dbnbDB.setUniqueID( iNewID );
    }    // End of setUniqueID()

    public void setModifyComments( String sNew )
    {
        dbnbDB.setModifyComments( sNew );
    }    // End of setUniqueID()


    public void setEmployerID(Integer newValue)
    {
                dbnbDB.setFieldData( "EmployerID", newValue.toString() );
    }

    public Integer getEmployerID()
    {
        String           sValue = dbnbDB.getFieldData( "EmployerID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setField(String fieldN, Object fieldV)
    {DebugLogger.println("setField Method not implemented yet in classNIM3_EmployerAccount!");}

    public String getFieldType(String fieldN)
    {return null;}


    public void setUniqueCreateDate(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "UniqueCreateDate", formatter.format( newValue ) );
    }

    public Date getUniqueCreateDate()
    {
        String           sValue = dbnbDB.getFieldData( "UniqueCreateDate" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setUniqueModifyDate(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "UniqueModifyDate", formatter.format( newValue ) );
    }

    public Date getUniqueModifyDate()
    {
        String           sValue = dbnbDB.getFieldData( "UniqueModifyDate" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setUniqueModifyComments(String newValue)
    {
                dbnbDB.setFieldData( "UniqueModifyComments", newValue.toString() );
    }

    public String getUniqueModifyComments()
    {
        String           sValue = dbnbDB.getFieldData( "UniqueModifyComments" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setPayerID(Integer newValue)
    {
                dbnbDB.setFieldData( "PayerID", newValue.toString() );
    }

    public Integer getPayerID()
    {
        String           sValue = dbnbDB.getFieldData( "PayerID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setEmployerName(String newValue)
    {
                dbnbDB.setFieldData( "EmployerName", newValue.toString() );
    }

    public String getEmployerName()
    {
        String           sValue = dbnbDB.getFieldData( "EmployerName" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setEmployerAddress1(String newValue)
    {
                dbnbDB.setFieldData( "EmployerAddress1", newValue.toString() );
    }

    public String getEmployerAddress1()
    {
        String           sValue = dbnbDB.getFieldData( "EmployerAddress1" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setEmployerAddress2(String newValue)
    {
                dbnbDB.setFieldData( "EmployerAddress2", newValue.toString() );
    }

    public String getEmployerAddress2()
    {
        String           sValue = dbnbDB.getFieldData( "EmployerAddress2" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setEmployerCity(String newValue)
    {
                dbnbDB.setFieldData( "EmployerCity", newValue.toString() );
    }

    public String getEmployerCity()
    {
        String           sValue = dbnbDB.getFieldData( "EmployerCity" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setEmployerStateID(Integer newValue)
    {
                dbnbDB.setFieldData( "EmployerStateID", newValue.toString() );
    }

    public Integer getEmployerStateID()
    {
        String           sValue = dbnbDB.getFieldData( "EmployerStateID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setEmployerZIP(String newValue)
    {
                dbnbDB.setFieldData( "EmployerZIP", newValue.toString() );
    }

    public String getEmployerZIP()
    {
        String           sValue = dbnbDB.getFieldData( "EmployerZIP" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setEmployerFax(String newValue)
    {
                dbnbDB.setFieldData( "EmployerFax", newValue.toString() );
    }

    public String getEmployerFax()
    {
        String           sValue = dbnbDB.getFieldData( "EmployerFax" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setEmployerWorkPhone(String newValue)
    {
                dbnbDB.setFieldData( "EmployerWorkPhone", newValue.toString() );
    }

    public String getEmployerWorkPhone()
    {
        String           sValue = dbnbDB.getFieldData( "EmployerWorkPhone" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setEmployerCellPhone(String newValue)
    {
                dbnbDB.setFieldData( "EmployerCellPhone", newValue.toString() );
    }

    public String getEmployerCellPhone()
    {
        String           sValue = dbnbDB.getFieldData( "EmployerCellPhone" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setComments(String newValue)
    {
                dbnbDB.setFieldData( "Comments", newValue.toString() );
    }

    public String getComments()
    {
        String           sValue = dbnbDB.getFieldData( "Comments" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setAuditNotes(String newValue)
    {
                dbnbDB.setFieldData( "AuditNotes", newValue.toString() );
    }

    public String getAuditNotes()
    {
        String           sValue = dbnbDB.getFieldData( "AuditNotes" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

}    // End of bltNIM3_EmployerAccount class definition
