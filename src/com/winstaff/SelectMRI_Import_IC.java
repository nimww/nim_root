/*
	A basic Java class stub for a Win32 Console Application.
 */
package com.winstaff;
import com.winstaff.*;

public class SelectMRI_Import_IC  extends Thread
{

	public SelectMRI_Import_IC () 
	{

	}
	
    static public void main(String args[]) 
    {
        SelectMRI_Import_IC testMe = new SelectMRI_Import_IC();
        if (true)
        {
            try
            {
		       // int mySleep = 300;
	            System.out.println(">>> Running Email Send " + new java.util.Date());
                testMe.runMe();
    	        //System.out.println(">>> sleeping for " + Math.round(mySleep/60) + " Minutes...");
                //testMe.sleep(mySleep * 1000);
            }
            catch (Exception e)
            {
                System.out.println("Error in Thread: "+ e );
            }
        }
    }	
	
	
    public static java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat("MM/dd/yyyy");
    public static java.text.SimpleDateFormat displayDateSDF2 = new java.text.SimpleDateFormat("MM-dd-yyyy");
    public static java.text.SimpleDateFormat displayDateSDF3 = new java.text.SimpleDateFormat("MMddyyyy");
    public static java.text.SimpleDateFormat displayDateSDF4 = new java.text.SimpleDateFormat("MM/dd/yy");
    //public static java.text.SimpleDateFormat displayDateSDF5 = new java.text.SimpleDateFormat("MMddyyyy");
    public static java.text.SimpleDateFormat displayDateSDF1Full = new java.text.SimpleDateFormat("MM/dd/yyyy hh:mm:ss a");
    public static java.text.SimpleDateFormat displayDateSDF2Full = new java.text.SimpleDateFormat("MM/dd/yyyy hh:mm a");
    public static java.text.SimpleDateFormat displayDateSDF3Full = new java.text.SimpleDateFormat("MM/dd/yyyy HH:mm");

    public static void runMe()
    {
        String batchGroup = "4";
        System.out.println("==============================================================================");
        System.out.println("Starting Import of Clients ==> tNIM3_PayerMaster");
        try
        {
            searchDB2 mySDBA = new searchDB2();
            String mySQLA = "SELECT \"public\".\"qSelectMRI_IC\".\"SelectMRI_ID\", \"public\".\"qSelectMRI_IC\".\"PracticeName\", \"public\".\"qSelectMRI_IC\".mergeme, \"public\".\"qSelectMRI_IC\".officeaddress1, \"public\".\"qSelectMRI_IC\".officeaddress2, \"public\".\"qSelectMRI_IC\".officecity, \"public\".\"qSelectMRI_IC\".officestateid, \"public\".\"qSelectMRI_IC\".officezip, \"public\".\"qSelectMRI_IC\".officephone, \"public\".\"qSelectMRI_IC\".officefederaltaxid, \"public\".\"qSelectMRI_IC\".price_mod_mri_wo, \"public\".\"qSelectMRI_IC\".price_mod_mri_w, \"public\".\"qSelectMRI_IC\".price_mod_mri_wwo, \"public\".\"qSelectMRI_IC\".price_mod_ct_wo, \"public\".\"qSelectMRI_IC\".price_mod_ct_w, \"public\".\"qSelectMRI_IC\".price_mod_ct_wwo, \"public\".\"qSelectMRI_IC\".feepercentage, \"public\".\"qSelectMRI_IC\".selectmri_notes, \"public\".\"qSelectMRI_IC\".contractingstatusid, \"public\".\"SelectMRI_FULL_facilities\".\"Fax\", \"public\".\"SelectMRI_FULL_facilities\".\"ContactName\", \"public\".\"SelectMRI_FULL_facilities\".\"ContactPhone\", \"public\".\"SelectMRI_FULL_facilities\".\"ContactPhoneExt\", \"public\".\"SelectMRI_FULL_facilities\".\"ContactEMail\", \"public\".\"SelectMRI_FULL_facilities\".\"BillingAddress1\", \"public\".\"SelectMRI_FULL_facilities\".\"BillingAddress2\", \"public\".\"SelectMRI_FULL_facilities\".\"BillingCity\", \"public\".tstateli.stateid, \"public\".\"SelectMRI_FULL_facilities\".\"BillingZipCode\", \"public\".\"SelectMRI_FULL_facilities\".\"BillingPhone\", \"public\".\"SelectMRI_FULL_facilities\".\"BillingFax\", \"public\".\"SelectMRI_FULL_facilities\".\"QuickBooksID\" FROM \"public\".\"SelectMRI_FULL_facilities\" Inner Join \"public\".\"qSelectMRI_IC\" ON \"public\".\"qSelectMRI_IC\".\"SelectMRI_ID\" = \"public\".\"SelectMRI_FULL_facilities\".\"FacilityID\" Left Join \"public\".tstateli ON \"public\".\"SelectMRI_FULL_facilities\".\"BillingState\" = \"public\".tstateli.shortstate";
            java.sql.ResultSet myRS = mySDBA.executeStatement(mySQLA);
            int totalCnt=0;
            int successCnt=0;
            int successNoCnt=0;
            int CountCT = 0;
            while (myRS.next())
            {
                String tempSelectMRI_ID = myRS.getString("SelectMRI_ID"); if (tempSelectMRI_ID==null) {  tempSelectMRI_ID = "" ;}
                String tempPracticeName = myRS.getString("PracticeName"); if (tempPracticeName==null) {  tempPracticeName = "" ;}
                String tempmergeme = myRS.getString("mergeme"); if (tempmergeme==null) {  tempmergeme = "" ;}
                String tempofficeaddress1 = myRS.getString("officeaddress1"); if (tempofficeaddress1==null) {  tempofficeaddress1 = "" ;}
                String tempofficeaddress2 = myRS.getString("officeaddress2"); if (tempofficeaddress2==null) {  tempofficeaddress2 = "" ;}
                String tempofficecity = myRS.getString("officecity"); if (tempofficecity==null) {  tempofficecity = "" ;}
                String tempofficestateid = myRS.getString("officestateid"); if (tempofficestateid==null) {  tempofficestateid = "" ;}
                String tempofficezip = myRS.getString("officezip"); if (tempofficezip==null) {  tempofficezip = "" ;}
                String tempofficephone = myRS.getString("officephone"); if (tempofficephone==null) {  tempofficephone = "" ;}
                String tempofficefederaltaxid = myRS.getString("officefederaltaxid"); if (tempofficefederaltaxid==null) {  tempofficefederaltaxid = "" ;}
                String tempprice_mod_mri_wo = myRS.getString("price_mod_mri_wo"); if (tempprice_mod_mri_wo==null) {  tempprice_mod_mri_wo = "" ;}
                String tempprice_mod_mri_w = myRS.getString("price_mod_mri_w"); if (tempprice_mod_mri_w==null) {  tempprice_mod_mri_w = "" ;}
                String tempprice_mod_mri_wwo = myRS.getString("price_mod_mri_wwo"); if (tempprice_mod_mri_wwo==null) {  tempprice_mod_mri_wwo = "" ;}
                String tempprice_mod_ct_wo = myRS.getString("price_mod_ct_wo"); if (tempprice_mod_ct_wo==null) {  tempprice_mod_ct_wo = "" ;}
                String tempprice_mod_ct_w = myRS.getString("price_mod_ct_w"); if (tempprice_mod_ct_w==null) {  tempprice_mod_ct_w = "" ;}
                String tempprice_mod_ct_wwo = myRS.getString("price_mod_ct_wwo"); if (tempprice_mod_ct_wwo==null) {  tempprice_mod_ct_wwo = "" ;}
                String tempfeepercentage = myRS.getString("feepercentage"); if (tempfeepercentage==null) {  tempfeepercentage = "" ;}
                String tempselectmri_notes = myRS.getString("selectmri_notes"); if (tempselectmri_notes==null) {  tempselectmri_notes = "" ;}
                String tempcontractingstatusid = myRS.getString("contractingstatusid"); if (tempcontractingstatusid==null) {  tempcontractingstatusid = "" ;}
                String tempFax = myRS.getString("Fax"); if (tempFax==null) {  tempFax = "" ;}
                String tempContactName = myRS.getString("ContactName"); if (tempContactName==null) {  tempContactName = "" ;}
                String tempContactPhone = myRS.getString("ContactPhone"); if (tempContactPhone==null) {  tempContactPhone = "" ;}
                String tempContactPhoneExt = myRS.getString("ContactPhoneExt"); if (tempContactPhoneExt==null) {  tempContactPhoneExt = "" ;}
                String tempContactEMail = myRS.getString("ContactEMail"); if (tempContactEMail==null) {  tempContactEMail = "" ;}
                String tempBillingAddress1 = myRS.getString("BillingAddress1"); if (tempBillingAddress1==null) {  tempBillingAddress1 = "" ;}
                String tempBillingAddress2 = myRS.getString("BillingAddress2"); if (tempBillingAddress2==null) {  tempBillingAddress2 = "" ;}
                String tempBillingCity = myRS.getString("BillingCity"); if (tempBillingCity==null) {  tempBillingCity = "" ;}
                String tempstateid = myRS.getString("stateid"); if (tempstateid==null) {  tempstateid = "" ;}
                String tempBillingZipCode = myRS.getString("BillingZipCode"); if (tempBillingZipCode==null) {  tempBillingZipCode = "" ;}
                String tempBillingPhone = myRS.getString("BillingPhone"); if (tempBillingPhone==null) {  tempBillingPhone = "" ;}
                String tempBillingFax = myRS.getString("BillingFax"); if (tempBillingFax==null) {  tempBillingFax = "" ;}
                String tempQuickBooksID = myRS.getString("QuickBooksID"); if (tempQuickBooksID==null) {  tempQuickBooksID = "" ;}

                int MasterAccountID = 3444;
                int MasterBrokerID = 3446;

                System.out.println("[][][]Testing SelectMRI MergeMe ["+tempmergeme+"]");


                totalCnt++;
                //find match
                boolean isMatch = false;
                searchDB2 mySDBA2 = new searchDB2();
                String mySQLA2 = "SELECT PracticeID from tpracticemaster where (substring(\"public\".tpracticemaster.officezip from 1 for 5) || '_' || substring( \"public\".tpracticemaster.officeaddress1 from 1 for 5))  = substring('" + tempmergeme + "' from 1 for 11) order by practiceid";
                java.sql.ResultSet myRS2 = mySDBA2.executeStatement(mySQLA2);
                int total2Cnt=0;
                Integer matchPracticeID = null;
                if (myRS2.next())
                {
                    matchPracticeID = new Integer(myRS2.getString("PracticeID"));
                    isMatch = true;
                }
                mySDBA2.closeAll();
                if (isMatch)
                {
                    successCnt++;
                    System.out.println("     //Match Found... Merging data");
                    System.out.println("     \\\\Data Merge Done");
                    bltPracticeMaster myPM = new bltPracticeMaster(matchPracticeID);
/*
                    DebugLogger.printLine("IC_IN1: [" + myPM.getPracticeName() + "] = [" + tempPracticeName + "]");
                    DebugLogger.printLine("IC_IN2: [" + myPM.getOfficeAddress1() + "] = [" + tempofficeaddress1 + "]");
                    String NoteAdd = "";
                    NoteAdd +="| NIM-$MRI_wo: ["+myPM.getPrice_Mod_MRI_WO()+"]";
                    NoteAdd +="| NIM-$MRI_w: ["+myPM.getPrice_Mod_MRI_W()+"]";
                    NoteAdd +="| NIM-$MRI_wwo: ["+myPM.getPrice_Mod_MRI_WWO()+"]";
                    NoteAdd +="| NIM-$CT_wo: ["+myPM.getPrice_Mod_CT_WO()+"]";
                    NoteAdd +="| NIM-$CT_w: ["+myPM.getPrice_Mod_CT_W()+"]";
                    NoteAdd +="| NIM-$CT_wwo: ["+myPM.getPrice_Mod_CT_WWO()+"]";
                    myPM.setSelectMRI_Notes("NIM-Name: [" + myPM.getPracticeName() + "]"  + " | " + "NIM-Add1: [" + myPM.getOfficeAddress1() + "] "  + NoteAdd + " | " + tempselectmri_notes );
                    myPM.setPracticeName(tempPracticeName);
                    try
                    {
                        myPM.setSelectMRI_ID(new Integer(tempSelectMRI_ID));
                    }
                    catch (Exception eeee)
                    {
                        System.out.println("      ******** Failed on tempSelectMRI_ID = " + tempSelectMRI_ID);
                    }
                    myPM.setOfficeAddress1(tempofficeaddress1);
                    myPM.setOfficeAddress2(tempofficeaddress2);
                    myPM.setOfficeCity(tempofficecity);
                    try
                    {
                        myPM.setOfficeStateID(new Integer(tempofficestateid));
                    }
                    catch (Exception eeee)
                    {
                        System.out.println("      ******** Failed on stateid = " + tempofficestateid);
                    }
                    myPM.setOfficeZIP(tempofficezip);
                    myPM.setOfficePhone(tempofficephone);
                    myPM.setOfficeFederalTaxID(tempofficefederaltaxid);
                    //import MRI
                    try
                    {
                        myPM.setPrice_Mod_MRI_WO(new Double(tempprice_mod_mri_wo));
                    }
                    catch (Exception eeee)
                    {
                        System.out.println("      ******** Failed on tempprice_mod_mri_wo = " + tempprice_mod_mri_wo);
                    }
                    try
                    {
                        myPM.setPrice_Mod_MRI_W(new Double(tempprice_mod_mri_w));
                    }
                    catch (Exception eeee)
                    {
                        System.out.println("      ******** Failed on tempprice_mod_mri_w = " + tempprice_mod_mri_w);
                    }
                    try
                    {
                        myPM.setPrice_Mod_MRI_WWO(new Double(tempprice_mod_mri_wwo ));
                    }
                    catch (Exception eeee)
                    {
                        System.out.println("      ******** Failed on tempprice_mod_mri_wwo  = " + tempprice_mod_mri_wwo );
                    }
                    //import CTs
                    try
                    {
                        myPM.setPrice_Mod_CT_WO(new Double(tempprice_mod_ct_wo));
                    }
                    catch (Exception eeee)
                    {
                        System.out.println("      ******** Failed on tempprice_mod_CT_wo = " + tempprice_mod_ct_wo);
                    }
                    try
                    {
                        myPM.setPrice_Mod_CT_W(new Double(tempprice_mod_ct_w));
                    }
                    catch (Exception eeee)
                    {
                        System.out.println("      ******** Failed on tempprice_mod_CT_w = " + tempprice_mod_ct_w);
                    }
                    try
                    {
                        myPM.setPrice_Mod_CT_WWO(new Double(tempprice_mod_ct_wwo ));
                    }
                    catch (Exception eeee)
                    {
                        System.out.println("      ******** Failed on tempprice_mod_CT_wwo  = " + tempprice_mod_ct_wwo );
                    }

                    try
                    {
                        myPM.setFeePercentage(new Double(tempfeepercentage  ));
                    }
                    catch (Exception eeee)
                    {
                        System.out.println("      ******** Failed on tempfeepercentage   = " + tempfeepercentage  );
                    }

                    myPM.setContractingStatusID(8);
                    myPM.setOfficeFaxNo(tempFax);
                    myPM.setOfficeManagerFirstName(tempContactName );
                    myPM.setOfficeManagerPhone(tempContactPhone + " " + tempContactPhoneExt );
                    myPM.setOfficeEmail(tempContactEMail );
                    myPM.setBillingAddress1(tempBillingAddress1 );
                    myPM.setBillingAddress2(tempBillingAddress2 );
                    myPM.setBillingCity(tempBillingCity );
                    try
                    {
                        myPM.setBillingStateID(new Integer(tempstateid ));
                    }
                    catch (Exception eeee)
                    {
                        System.out.println("      ******** Failed on tempstateid   = " + tempstateid  );
                    }

                    myPM.setBillingZIP(tempBillingZipCode );
                    myPM.setBillingPhone(tempBillingPhone );
                    myPM.setBillingFax(tempBillingFax );
                    myPM.setSelectMRI_QBID(tempQuickBooksID );

  */
                    if (myPM.getPrice_Mod_CT_WO()>0)
                    {
                        //add MRI Modality
                        bltNIM3_Modality myM = new bltNIM3_Modality();
                        myM.setComments("auto create on MSC/Select Import - Round 2");
                        myM.setPracticeID(myPM.getUniqueID());
                        myM.setModalityTypeID(2);
                        myM.commitData();
                        CountCT++;
                    }
                    if (successCnt==1||true)
                    {
                        myPM.commitData();
/*
                        bltAdminPracticeLU myAPU2 = new bltAdminPracticeLU();
                        myAPU2.setAdminID(MasterBrokerID);
                        myAPU2.setPracticeID(myPM.getUniqueID());
                        myAPU2.setRelationshipTypeID(3);//broker
                        myAPU2.commitData();
*/
                    }
                }
                else if (false)
                {
                    successNoCnt++;
                    System.out.println("     //No Match.... creating new record");
                    bltPracticeMaster myPM = new bltPracticeMaster();
                    myPM.setPracticeName(tempPracticeName);
                    try
                    {
                        myPM.setSelectMRI_ID(new Integer(tempSelectMRI_ID));
                    }
                    catch (Exception eeee)
                    {
                        System.out.println("      ******** Failed on tempSelectMRI_ID = " + tempSelectMRI_ID);
                    }
                    myPM.setOfficeAddress1(tempofficeaddress1);
                    myPM.setOfficeAddress2(tempofficeaddress2);
                    myPM.setOfficeCity(tempofficecity);
                    try
                    {
                        myPM.setOfficeStateID(new Integer(tempofficestateid));
                    }
                    catch (Exception eeee)
                    {
                        System.out.println("      ******** Failed on stateid = " + tempofficestateid);
                    }
                    myPM.setOfficeZIP(tempofficezip);
                    myPM.setOfficePhone(tempofficephone);
                    myPM.setOfficeFederalTaxID(tempofficefederaltaxid);
                    //import MRI
                    try
                    {
                        myPM.setPrice_Mod_MRI_WO(new Double(tempprice_mod_mri_wo));
                    }
                    catch (Exception eeee)
                    {
                        System.out.println("      ******** Failed on tempprice_mod_mri_wo = " + tempprice_mod_mri_wo);
                    }
                    try
                    {
                        myPM.setPrice_Mod_MRI_W(new Double(tempprice_mod_mri_w));
                    }
                    catch (Exception eeee)
                    {
                        System.out.println("      ******** Failed on tempprice_mod_mri_w = " + tempprice_mod_mri_w);
                    }
                    try
                    {
                        myPM.setPrice_Mod_MRI_WWO(new Double(tempprice_mod_mri_wwo ));
                    }
                    catch (Exception eeee)
                    {
                        System.out.println("      ******** Failed on tempprice_mod_mri_wwo  = " + tempprice_mod_mri_wwo );
                    }
                    //import CTs
                    try
                    {
                        myPM.setPrice_Mod_CT_WO(new Double(tempprice_mod_ct_wo));
                    }
                    catch (Exception eeee)
                    {
                        System.out.println("      ******** Failed on tempprice_mod_CT_wo = " + tempprice_mod_ct_wo);
                    }
                    try
                    {
                        myPM.setPrice_Mod_CT_W(new Double(tempprice_mod_ct_w));
                    }
                    catch (Exception eeee)
                    {
                        System.out.println("      ******** Failed on tempprice_mod_CT_w = " + tempprice_mod_ct_w);
                    }
                    try
                    {
                        myPM.setPrice_Mod_CT_WWO(new Double(tempprice_mod_ct_wwo ));
                    }
                    catch (Exception eeee)
                    {
                        System.out.println("      ******** Failed on tempprice_mod_CT_wwo  = " + tempprice_mod_ct_wwo );
                    }

                    try
                    {
                        myPM.setFeePercentage(new Double(tempfeepercentage  ));
                    }
                    catch (Exception eeee)
                    {
                        System.out.println("      ******** Failed on tempfeepercentage   = " + tempfeepercentage  );
                    }

                    myPM.setSelectMRI_Notes("NIM: [New Center - No Prior Match] | " + tempselectmri_notes );
                    myPM.setContractingStatusID(8);
                    myPM.setOfficeFaxNo(tempFax);
                    myPM.setOfficeManagerFirstName(tempContactName );
                    myPM.setOfficeManagerPhone(tempContactPhone + " " + tempContactPhoneExt );
                    myPM.setOfficeEmail(tempContactEMail );
                    myPM.setBillingAddress1(tempBillingAddress1 );
                    myPM.setBillingAddress2(tempBillingAddress2 );
                    myPM.setBillingCity(tempBillingCity );
                    try
                    {
                        myPM.setBillingStateID(new Integer(tempstateid ));
                    }
                    catch (Exception eeee)
                    {
                        System.out.println("      ******** Failed on tempstateid   = " + tempstateid  );
                    }

                    myPM.setBillingZIP(tempBillingZipCode );
                    myPM.setBillingPhone(tempBillingPhone );
                    myPM.setBillingFax(tempBillingFax );
                    myPM.setSelectMRI_QBID(tempQuickBooksID );

                    if (successNoCnt==1||true)
                    {
                        myPM.commitData();
                        bltAdminPracticeLU myAPU = new bltAdminPracticeLU();
                        myAPU.setAdminID(MasterAccountID);
                        myAPU.setPracticeID(myPM.getUniqueID());
                        myAPU.setRelationshipTypeID(2);//admin
                        myAPU.commitData();
                        bltAdminPracticeLU myAPU2 = new bltAdminPracticeLU();
                        myAPU2.setAdminID(MasterBrokerID);
                        myAPU2.setPracticeID(myPM.getUniqueID());
                        myAPU2.setRelationshipTypeID(3);//broker
                        myAPU2.commitData();
                        System.out.println("     \\\\Record Saved");

                        //add MRI Modality
                        bltNIM3_Modality myM = new bltNIM3_Modality();
                        myM.setComments("auto create on MSC/Select Import");
                        myM.setPracticeID(myPM.getUniqueID());
                        myM.setModalityTypeID(1);
                        myM.commitData();
                    }
                }
            }
    	    mySDBA.closeAll();
            System.out.println("[][][] Final - Done");
            System.out.println("[]Matched:" + successCnt);
            System.out.println("[]No-Match:" + successNoCnt);
            System.out.println("[]Added CT:" + CountCT);
        }
        catch(Exception e)
        {
            DebugLogger.println("Critical Failure in SelectMRI Import: " + e);
            System.out.println("Critical Failure in SelectMRI Import: " + e);
        }


    }

	//{{DECLARE_CONTROLS
	//}}
}

