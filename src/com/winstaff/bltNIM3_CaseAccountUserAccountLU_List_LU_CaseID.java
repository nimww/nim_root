
package com.winstaff;
/*
 * bltNIM3_CaseAccountUserAccountLU_List_LU_CaseID.java
 *
 * Created: Tue Jun 30 17:14:10 PDT 2009
 */



import java.sql.*;




/**
 *
 * @author  Scott Ellis via listClassGenCode
 * @version 2.5
 */
public class bltNIM3_CaseAccountUserAccountLU_List_LU_CaseID extends db_BaseRelationTable
{
    /** Default constructor
    */    
    public bltNIM3_CaseAccountUserAccountLU_List_LU_CaseID ( Integer iCaseID )
    {
        super( "tNIM3_CaseAccountUserAccountLU", "LookupID", "LookupID", "CaseID", iCaseID );
    }   // End of bltNIM3_CaseAccountUserAccountLU_List_LU_CaseID()
    

    public void setField(String fieldN, Object fieldV)
    {DebugLogger.println("setField Method not implemented yet in classbltNIM3_CaseAccountUserAccountLU_List_LU_CaseID!");}

    public String getFieldType(String fieldN)
    {return null;}

    public bltNIM3_CaseAccountUserAccountLU_List_LU_CaseID ( Integer iCaseID, String extraWhere, String OrderBy )
    {
        super( "tNIM3_CaseAccountUserAccountLU", "LookupID", "LookupID", "CaseID", iCaseID, extraWhere,OrderBy );
    }   // End of bltNIM3_CaseAccountUserAccountLU_List_LU_CaseID()
     /** Method pull information from the result set and load into class attributes.
    * @param sFieldList Comma delimited list of fields for which data has been retrieved.
    * @param rs Result set from which to pull the data from.
    * @throws SQLException Passes on SQLException
    */      
    protected void transferData( ResultSet rs ) throws SQLException
    {
        while ( rs.next()  )
        {
            Integer         iValue              = new Integer( rs.getString( sLinkFieldName ) );
            Integer         iLinkRecordID       = new Integer( rs.getString( sUniqueIDFieldName ) );
            Object          oReferenceObject;
            ListElement     leNewElement;
            
            oReferenceObject    = new bltNIM3_CaseAccountUserAccountLU( iValue );
            
            leNewElement        = new ListElement( iLinkRecordID, oReferenceObject );
            
            vList.add( leNewElement );

        }
    }   // End of transferData()
}   // End of bltNIM3_CaseAccountUserAccountLU_List_LU_CaseID class

