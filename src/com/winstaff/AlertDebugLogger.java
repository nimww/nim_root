package com.winstaff;
import java.io.DataOutputStream;
import java.io.FileOutputStream;
import java.util.Date;
import java.text.SimpleDateFormat;

public class AlertDebugLogger
{
    static String              sFileName        = "";
    static DataOutputStream    out;
    static boolean             firstTime        = true;
    static int                 linesOutputed    = 0;
    static int                 maxLinesOutput   = 75000;
    
    
  	/**
	 * <i>Default constructor.</i>
	 * 
	 */
    public AlertDebugLogger()
    {
    }   // End of HistoryLogger()

  	/**
	 * <i>Output a line to the current log file.</i>
	 * 
	 * @param       String      Output string to send to log file.
	 */
    public static void println( String s )
    {
           System.out.println(s);   
           printLine(s);   
    }

    public static void println( Integer s )
    {
        {
            System.out.println(">>"+s);
            println(s+"");   
        }
    }

    public static void println( Exception s )
    {
        {
            System.out.println(">>"+s);
            println(s+"");   
        }
    }

    public static void println( int s )
    {
        {
            System.out.println(">>"+s);
            println(s+"");   
        }

    }

    public static void println( long s )
    {
        {
            System.out.println(">>"+s);
            println(s+"");   
        }
    }

    public static void printLine( String s )
    {
        SimpleDateFormat    formatter1              = new SimpleDateFormat( "MM_dd_yyyy_hh_mm_ss" );
        SimpleDateFormat    formatter2              = new SimpleDateFormat( "hh:mm:ss" );
        String              newDateFileNameString   = formatter1.format( new Date() );
        
        
        try
        {
            if ( firstTime )
            {
                firstTime = false;
                
                linesOutputed = 0;                
            
                sFileName = ConfigurationInformation.sLogFolder + "\\AlertDebugQR_" + newDateFileNameString + ".txt";
            
                out = new DataOutputStream( new FileOutputStream( sFileName ) );
            }

            out.writeBytes( formatter2.format(new java.util.Date()) + ">" + s );
            out.writeBytes( "\n" );
            out.flush();
            
            if ( linesOutputed++ > maxLinesOutput )
            {
                out.close();
                firstTime = true;
            }            
        }
        catch ( Exception e )
        {
            System.out.println( "Logger error: " + e.toString() );
        }
        
    }   // End of println()
    
}   // End of HistoryLogger()
