package com.winstaff;


import com.winstaff.dbtStateLI;
import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public interface InttStateLI extends dbTableInterface
{

    public void setStateID(Integer newValue);
    public Integer getStateID();
    public void setShortState(String newValue);
    public String getShortState();
    public void setLongState(String newValue);
    public String getLongState();
}    // End of bltStateLI class definition
