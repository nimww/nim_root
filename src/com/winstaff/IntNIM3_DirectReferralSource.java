

package com.winstaff;

import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public interface IntNIM3_DirectReferralSource extends dbTableInterface
{

    public void setDirectReferralSourceID(Integer newValue);
    public Integer getDirectReferralSourceID();
    public void setUniqueCreateDate(Date newValue);
    public Date getUniqueCreateDate();
    public void setUniqueModifyDate(Date newValue);
    public Date getUniqueModifyDate();
    public void setUniqueModifyComments(String newValue);
    public String getUniqueModifyComments();
    public void setSourceName(String newValue);
    public String getSourceName();
    public void setSourcePhone(String newValue);
    public String getSourcePhone();
    public void setSourceFax(String newValue);
    public String getSourceFax();
    public void setSourceLandingWeb(String newValue);
    public String getSourceLandingWeb();
    public void setSourceWebsite(String newValue);
    public String getSourceWebsite();
    public void setSourceReferralCode(String newValue);
    public String getSourceReferralCode();
    public void setExternalID(String newValue);
    public String getExternalID();
    public void setPayerID(Integer newValue);
    public Integer getPayerID();
}    // End of blNIM3_DirectReferralSource class definition
