package com.winstaff;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.Date;
import java.lang.Integer;

public class quickSchedContest
{
	public static void email(String emailTo, String emailFrom, String theSubject, String theBody) throws SQLException{
		bltEmailTransaction email = new bltEmailTransaction();
		email.setEmailTo(emailTo);
		email.setEmailFrom(emailFrom);
		email.setEmailSubject(theSubject);
		email.setEmailBody(theBody);
		email.setEmailBodyType(emailType_V3.HTML_TYPE);
		email.setTransactionDate(new java.util.Date());
		email.commitData();		
	}
	
	public static String[] buildSchedArray() throws SQLException{
		String query = "select count(*) from tuseraccount where lower(accounttype) like '%scheduler%' and status = 2";			
		java.sql.ResultSet results = new searchDB2().executeStatement(query);
		
		int n = 0;
		
		while (results!=null&&results.next()){
			 n = results.getInt("count");
		}
		results.close();

		query = "select contactfirstname, contactlastname from tuseraccount where lower(accounttype) like '%scheduler%' and status = 2";
		results = new searchDB2().executeStatement(query);
		int count = 0;
		String[] value = new String[n];
		
		while (results!=null&&results.next()){
			value[count]=results.getString("contactfirstname")+" "+results.getString("contactlastname");;
			count++;
		}
		//results.close();
		
		return value;

	}
	
	public static int schedLessThanTwelve(String scheduler, String start, String end){
		//start and end date must be in yyyy-mm-dd
		int value=0;
		try{
		String query = "select count(encounter.scanpass) from tnim3_encounter as encounter left join tnim3_appointment as appointment on appointment.appointmentid = encounter.appointmentid left join tnim3_referral as referral on referral.referralid = encounter.referralid left join tnim3_caseaccount as caseaccount on caseaccount.caseid = referral.caseid left join tuseraccount as assignedto on assignedto.userid = caseaccount.assignedtoid left join tuseraccount as schedby on schedby.userid = appointment.scheduler_userid where encounter.encounterstatusid not in (5) and referral.receivedate >= '"+start+"' and referral.receivedate < '"+end+"' and schedby.contactfirstname||' '||schedby.contactlastname = '"+scheduler+"' and encounter.isretro != 1 and EXTRACT(EPOCH FROM appointment.uniquecreatedate-referral.receivedate)/3600 > 0 and EXTRACT(EPOCH FROM appointment.uniquecreatedate-referral.receivedate)/3600 <=12";			
		
		java.sql.ResultSet results = new searchDB2().executeStatement(query);

		while (results!=null&&results.next()){
			 value = results.getInt("count");
		}
		//results.close();
		}
		catch(Exception e){System.out.println("schedLessThanTwelve");}
		return value;
	}
	
	public static int schedTwelveToTwentyFour(String scheduler, String start, String end){
		//start and end date must be in yyyy-mm-dd
		int value=0;
		try{
		String query = "select count(encounter.scanpass) from tnim3_encounter as encounter left join tnim3_appointment as appointment on appointment.appointmentid = encounter.appointmentid left join tnim3_referral as referral on referral.referralid = encounter.referralid left join tnim3_caseaccount as caseaccount on caseaccount.caseid = referral.caseid left join tuseraccount as assignedto on assignedto.userid = caseaccount.assignedtoid left join tuseraccount as schedby on schedby.userid = appointment.scheduler_userid where encounter.encounterstatusid not in (5) and referral.receivedate >= '"+start+"' and referral.receivedate < '"+end+"' and schedby.contactfirstname||' '||schedby.contactlastname = '"+scheduler+"' and encounter.isretro != 1 and EXTRACT(EPOCH FROM appointment.uniquecreatedate-referral.receivedate)/3600 > 0 and (EXTRACT(EPOCH FROM appointment.uniquecreatedate-referral.receivedate)/3600 >12 and EXTRACT(EPOCH FROM appointment.uniquecreatedate-referral.receivedate)/3600 <= 24)";
		
		java.sql.ResultSet results = new searchDB2().executeStatement(query);

		while (results!=null&&results.next()){
			 value = results.getInt("count");
		}
		//results.close();
		}
		catch(Exception e){System.out.println("schedTwelveToTwentyFour");}
		return value;
	}
	
	
	
	public static void main(String[] args) throws SQLException, ParseException{
		
		String[] schedulers = buildSchedArray();
		java.text.SimpleDateFormat df = new java.text.SimpleDateFormat("MM/dd");
		Date date1 = new java.text.SimpleDateFormat("yyyy-MM-dd").parse(args[0]);
		Date date2 = new java.text.SimpleDateFormat("yyyy-MM-dd").parse(args[1]);
		String dataDump ="Data from: " + df.format(date1) +" to " + df.format(date2);
		
		dataDump += "<br><table border=2 style=\"font:Helvetica;border-collapse:collapse;\"><tr>"+
				"<td colspan=3 style=\"background:#00aeff;padding:2px 5px;\"><strong>Turnover<br>"+df.format(date1)+"-"+df.format(date2)+"</strong></td>" +
				"</tr><tr>" +
				"<td>&nbsp;</td><td style=\"padding:2px 5px\">Less Than 12 hrs</td><td style=\"padding:2px 5px\">Between 12-24 hrs</td>" +
				"</tr>";
		try{
			for (int count = 0; count<schedulers.length;count++){
				int schedLessThanTwelve = schedLessThanTwelve(schedulers[count], args[0], args[1]);
				int schedTwelveToTwentyFour = schedTwelveToTwentyFour(schedulers[count], args[0], args[1]);
				
				if (schedLessThanTwelve != 0 || schedTwelveToTwentyFour !=0){
					dataDump += "<tr>";
					dataDump += "<td align=\"center\">"+schedulers[count]+"</td>";
					dataDump += "<td align=\"right\" style=\"padding:2px 5px\">"+schedLessThanTwelve+"</td>";
					dataDump += "<td align=\"right\" style=\"padding:2px 5px\">"+schedTwelveToTwentyFour+"</td>";
					dataDump += "</tr>";
					count++;
				}
			}
		}
		catch(Exception e){System.out.println("Top fail");}
		
		dataDump += "</table><br><br>";
		/* Broken but not quite needed. 
		 * 
		 * try{
			String query = "select encounter.scanpass,  assignedto.contactfirstname||' '||assignedto.contactlastname assignedto,  schedby.contactfirstname||' '||schedby.contactlastname schedby,  to_char(referral.receivedate, 'mm/dd/yyyy hh12:mi pm') receivedate,  to_char(appointment.uniquecreatedate, 'mm/dd/yyyy hh12:mi pm') scheddate,  round( (EXTRACT(EPOCH FROM appointment.uniquecreatedate-referral.receivedate)/3600)::NUMERIC, 1) hours from tnim3_encounter as encounter left join tnim3_appointment as appointment on appointment.appointmentid = encounter.appointmentid left join tnim3_referral as referral on referral.referralid = encounter.referralid left join tnim3_caseaccount as caseaccount on caseaccount.caseid = referral.caseid left join tuseraccount as assignedto on assignedto.userid = caseaccount.assignedtoid left join tuseraccount as schedby on schedby.userid = appointment.scheduler_userid where encounter.encounterstatusid not in (5) and referral.receivedate >= '2012-12-01' and referral.receivedate < '2013-01-01' and encounter.isretro != 1 and EXTRACT(EPOCH FROM appointment.uniquecreatedate-referral.receivedate)/3600 > 0 and (EXTRACT(EPOCH FROM appointment.uniquecreatedate-referral.receivedate)/3600 <=12 or  (EXTRACT(EPOCH FROM appointment.uniquecreatedate-referral.receivedate)/3600 >12 and  EXTRACT(EPOCH FROM appointment.uniquecreatedate-referral.receivedate)/3600 <= 24 ))";
			java.sql.ResultSet results = new searchDB2().executeStatement(query);
			dataDump += "<table><tr>" +
					"<td>Scanpass</td>" +
					"<td>Assigned To</td>" +
					"<td>Scheduled By</td>" +
					"<td>Received Date</td>" +
					"<td>Scheduled Date</td>" +
					"<td>Hours</td>" +
					"</tr>";
			
			while (results!=null&&results.next())
			{
				dataDump += "<tr>";
				dataDump += "<td>"+results.getString("scanpass")+"</td>";
				dataDump += "<td>"+results.getString("assignedto")+"</td>";
				dataDump += "<td>"+results.getString("schedby")+"</td>";
				dataDump += "<td>"+results.getString("receivedate")+"</td>";
				dataDump += "<td>"+results.getString("scheddate")+"</td>";
				dataDump += "<td>"+results.getString("hours")+"</td>";
				dataDump += "</tr>";
			}
			//results.close();
			dataDump += "</table>"; 
		}
		catch(Exception e){System.out.println("Bottom fail");}*/
		String theSubject = "Turnaround time contest results";
		
		email("lizzie.dizon@nextimagemedical.com","support@nextimagemedical.com",theSubject,dataDump);
		email("po.le@nextimagemedical.com","support@nextimagemedical.com",theSubject,dataDump);
		email("Jessica.Rodriguez@nextimagemedical.com","support@nextimagemedical.com",theSubject,dataDump);
		email("ChristineDouheret@nextimagemedical.com","support@nextimagemedical.com",theSubject,dataDump);
		email("Vanessa.Saenz@nextimagemedical.com","support@nextimagemedical.com",theSubject,dataDump);
		System.out.print(dataDump);
	}
    
    
    
}