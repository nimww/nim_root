package com.winstaff;
/*
 * bltEventChild_List.java
 *
 * Created: Tue Jul 08 14:17:58 PDT 2003
 */



import java.sql.*;
import com.winstaff.dbtEventChild;
import com.winstaff.ListElement;


/**
 *
 * @author  Scott Ellis via listClassGenCode
 * @version 2.5
 */
public class bltEventChild_List extends db_BaseRelationTable
{
    /** Default constructor
    */    
    public bltEventChild_List ( Integer iEventID )
    {
        super( "tEventChild", "EventChildID", "EventChildID", "EventID", iEventID );
    }   // End of bltEventChild_List()
    

    public void setField(String fieldN, Object fieldV)
    {DebugLogger.println("setField Method not implemented yet in classbltEventChild_List!");}

    public String getFieldType(String fieldN)
    {return null;}

    public bltEventChild_List ( Integer iEventID, String extraWhere, String OrderBy )
    {
        super( "tEventChild", "EventChildID", "EventChildID", "EventID", iEventID, extraWhere,OrderBy );
    }   // End of bltEventChild_List()
     /** Method pull information from the result set and load into class attributes.
    * @param sFieldList Comma delimited list of fields for which data has been retrieved.
    * @param rs Result set from which to pull the data from.
    * @throws SQLException Passes on SQLException
    */      
    protected void transferData( ResultSet rs ) throws SQLException
    {
        while ( rs.next()  )
        {
            Integer         iValue              = new Integer( rs.getString( sLinkFieldName ) );
            Integer         iLinkRecordID       = new Integer( rs.getString( sUniqueIDFieldName ) );
            Object          oReferenceObject;
            ListElement     leNewElement;
            
            oReferenceObject    = new bltEventChild( iValue );
            
            leNewElement        = new ListElement( iLinkRecordID, oReferenceObject );
            
            vList.add( leNewElement );

        }
    }   // End of transferData()
}   // End of bltEventChild_List class

