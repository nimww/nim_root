

package com.winstaff;

import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public interface InttEncounterStatusLI extends dbTableInterface
{

    public void setEncounterStatusID(Integer newValue);
    public Integer getEncounterStatusID();
    public void setDescriptionLong(String newValue);
    public String getDescriptionLong();
}    // End of bltEncounterStatusLI class definition
