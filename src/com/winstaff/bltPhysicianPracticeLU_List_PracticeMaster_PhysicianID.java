
package com.winstaff;
/*
 * bltPhysicianPracticeLU_List_PracticeMaster_PhysicianID.java
 *
 * Created: Tue Nov 03 16:01:23 PST 2009
 */



import java.sql.*;




/**
 *
 * @author  Scott Ellis via listClassGenCode
 * @version 2.5
 */
public class bltPhysicianPracticeLU_List_PracticeMaster_PhysicianID extends db_BaseRelationTable
{
    /** Default constructor
    */    
    public bltPhysicianPracticeLU_List_PracticeMaster_PhysicianID ( Integer iPhysicianID )
    {
        super( "tPhysicianPracticeLU", "PracticeID", "LookupID", "PhysicianID", iPhysicianID );
    }   // End of bltPhysicianPracticeLU_List_PracticeMaster_PhysicianID()
    

    public void setField(String fieldN, Object fieldV)
    {DebugLogger.println("setField Method not implemented yet in classbltPhysicianPracticeLU_List_PracticeMaster_PhysicianID!");}

    public String getFieldType(String fieldN)
    {return null;}

    public bltPhysicianPracticeLU_List_PracticeMaster_PhysicianID ( Integer iPhysicianID, String extraWhere, String OrderBy )
    {
        super( "tPhysicianPracticeLU", "PracticeID", "LookupID", "PhysicianID", iPhysicianID, extraWhere,OrderBy );
    }   // End of bltPhysicianPracticeLU_List_PracticeMaster_PhysicianID()
     /** Method pull information from the result set and load into class attributes.
    * @param sFieldList Comma delimited list of fields for which data has been retrieved.
    * @param rs Result set from which to pull the data from.
    * @throws SQLException Passes on SQLException
    */      
    protected void transferData( ResultSet rs ) throws SQLException
    {
        while ( rs.next()  )
        {
            Integer         iValue              = new Integer( rs.getString( sLinkFieldName ) );
            Integer         iLinkRecordID       = new Integer( rs.getString( sUniqueIDFieldName ) );
            Object          oReferenceObject;
            ListElement     leNewElement;
            
            oReferenceObject    = new bltPracticeMaster( iValue );
            
            leNewElement        = new ListElement( iLinkRecordID, oReferenceObject );
            
            vList.add( leNewElement );

        }
    }   // End of transferData()
}   // End of bltPhysicianPracticeLU_List_PracticeMaster_PhysicianID class

