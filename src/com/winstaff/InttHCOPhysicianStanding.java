

package com.winstaff;

import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public interface InttHCOPhysicianStanding extends dbTableInterface
{

    public void setLookupID(Integer newValue);
    public Integer getLookupID();
    public void setUniqueCreateDate(Date newValue);
    public Date getUniqueCreateDate();
    public void setUniqueModifyDate(Date newValue);
    public Date getUniqueModifyDate();
    public void setUniqueModifyComments(String newValue);
    public String getUniqueModifyComments();
    public void setHCOID(Integer newValue);
    public Integer getHCOID();
    public void setComments(String newValue);
    public String getComments();
    public void setFirstName(String newValue);
    public String getFirstName();
    public void setMiddleName(String newValue);
    public String getMiddleName();
    public void setLastName(String newValue);
    public String getLastName();
    public void setLicenseNumber(String newValue);
    public String getLicenseNumber();
    public void setLicenseState(String newValue);
    public String getLicenseState();
    public void setDepartment(String newValue);
    public String getDepartment();
    public void setFromDate(String newValue);
    public String getFromDate();
    public void setToDate(String newValue);
    public String getToDate();
    public void setStatus(String newValue);
    public String getStatus();
    public void setPriviledgeComments(String newValue);
    public String getPriviledgeComments();
    public void setIsArchived(Integer newValue);
    public Integer getIsArchived();
    public void setSpecialty(String newValue);
    public String getSpecialty();
    public void setSuffix(String newValue);
    public String getSuffix();
    public void setTitle(String newValue);
    public String getTitle();
    public void setPhotoFileName(String newValue);
    public String getPhotoFileName();
    public void setNPI(String newValue);
    public String getNPI();
    public void setFacilityName(String newValue);
    public String getFacilityName();
    public void setOrigDate(String newValue);
    public String getOrigDate();
}    // End of bltHCOPhysicianStanding class definition
