package com.winstaff;
/**
** quickDB defines a "quick" methodology to access a database without DB connection pooling
**
** @author      Scott Tyler Ellis
** @version     1.0a
**
*/



import javax.tools.JavaCompiler;
import java.lang.String;
import java.sql.*;
import java.awt.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Vector;

public class searchDB2
{
    public searchDB2()
    {
    }

    Connection theConnection;
    Statement theStatement;
    ResultSet myRS;
    Statement   stmt;
    PreparedStatement stmt2 = null;

    public String executeBatchUpdate(java.util.Vector<String> myStatements)
    {
        String myVal = "";
        try {
            // Disable auto-commit
            theConnection.setAutoCommit(false);

            // Create a prepared statement
            Statement pstmt = theConnection.createStatement();

            // Insert 10 rows of data
            for (String temp_SQL : myStatements){
                pstmt.addBatch(temp_SQL);
            }

            // Execute the batch
            int [] updateCounts = pstmt.executeBatch();

            // All statements were successfully executed.
            // updateCounts contains one element for each batched statement.
            // updateCounts[i] contains the number of rows affected by that statement.
            myVal = processUpdateCounts(updateCounts,myStatements);
            // Since there were no errors, commit
            theConnection.commit();
            if (myVal.equalsIgnoreCase(""))
            {
                myVal = "success";
            }
        } catch (BatchUpdateException e) {
            DebugLogger.printLine("SearchDB2:BatchUpdateException:SQLException Error:["+e.toString()+"]");
            // Not all of the statements were successfully executed
            int[] updateCounts = e.getUpdateCounts();

            // Some databases will continue to execute after one fails.
            // If so, updateCounts.length will equal the number of batched statements.
            // If not, updateCounts.length will equal the number of successfully executed statements
            myVal += "\n****ERROR CAUGHT****\n";

            myVal += processUpdateCounts(updateCounts,myStatements);

            // Either commit the successfully executed statements or rollback the entire batch
            try {
                theConnection.rollback();
            }
            catch (SQLException e3){
                DebugLogger.printLine("SearchDB2:executeBatchUpdate:SQLException Error:["+e3.toString()+"]");
            }
        } catch (SQLException e2) {
            DebugLogger.printLine("SearchDB2:executeBatchUpdate2:SQLException Error:["+e2.toString()+"]");
        } finally {
            closeAll();
        }

        return myVal;
    }

    public String processUpdateCounts(int[] updateCounts, Vector<String> temp_StatementList) {
        String myVal = "";
        for (int i=0; i<updateCounts.length; i++) {
            if (updateCounts[i] >= 0) {
                // Successfully executed; the number represents number of affected rows
            } else if (updateCounts[i] == Statement.SUCCESS_NO_INFO) {
                // Successfully executed; number of affected rows not available
            } else if (updateCounts[i] == Statement.EXECUTE_FAILED) {
                // Failed to execute
                myVal += "\nStatement ["+i+"] failed: [SQL:"+temp_StatementList.elementAt(i)+"]";

            }
        }
        return myVal;
    }

    public int executeUpdate(String mySQL)
    {
        int DBAffected = 0;
        try
        {
            Class.forName(this.dbForName);
            theConnection = DriverManager.getConnection(this.dbConn,this.dbUserName,this.dbPassword);
            theStatement = theConnection.createStatement();
            DBAffected = theStatement.executeUpdate(mySQL);
        }
        catch (Exception e)
        {
                DebugLogger.println("QuickDB Degbug [No-ResultSet]: [" + e.toString() + "]");
        }
        return DBAffected;
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        try {
            this.closeAll();
        } finally {
            try
            {
                theStatement.close();
            }
            catch (Exception e)
            {
                //DebugLogger.println("QuickDB Degbug: [ConnectionClose]" + e.toString());
            }
            try
            {
                stmt.close();
            }
            catch (Exception e)
            {
                //DebugLogger.println("QuickDB Degbug: [ConnectionClose]" + e.toString());
            }
            try
            {
                stmt2.close();
            }
            catch (Exception e)
            {
                //DebugLogger.println("QuickDB Degbug: [ConnectionClose]" + e.toString());
            }
            try
            {
                myRS.close();
            }
            catch (Exception e)
            {
                //DebugLogger.println("QuickDB Degbug: [ConnectionClose]" + e.toString());
            }
            try
            {
                theConnection.close();
            }
            catch (Exception e)
            {
                //DebugLogger.println("searchDB:finalize(): [ConnectionClose]" + e.toString());
            }

        }//To change body of overridden methods use File | Settings | File Templates.
    }

    public ResultSet executeStatement(String mySQL)
    {
        try
        {
            Class.forName(this.dbForName);
    //	    theConnection = DriverManager.getConnection(this.dbConn,this.dbUserName,this.dbPassword);
            theConnection = DriverManager.getConnection(this.dbConn);
            theStatement = theConnection.createStatement();
            mySQL = mySQL.replace(';',':');
            if ( ConfigurationInformation.bInDebugMode )
            {
                DebugLogger.println("Select: " + mySQL);
            }
            myRS = theStatement.executeQuery(mySQL);
        }
        catch (Exception e)
        {
            DebugLogger.println("QuickDB: executeStatement[ResultSet]: [" + e.toString() + "] SQL=["+mySQL+"]");
            DebugLogger.sendFatalError("QuickDB: executeStatement[ResultSet]: [" + e.toString() + "]");
        }
        return myRS;
    }



    public ResultSet executePreparedStatement(db_PreparedStatementObject dbPSO)
    {
        SimpleDateFormat sdDefaultFormatter = new SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        try
        {
//            Connection      cCon;
            theConnection = DriverManager.getConnection( ConfigurationInformation.sURL );

            stmt = theConnection.createStatement();

//sqi change            sSQLString = "select * from " + sTableName + " where " + sKeyFieldName + "=" + iUniqueID.toString();
            String sSQLString = dbPSO.getSQL();
		    stmt2 = theConnection.prepareStatement(sSQLString);
            dbPSO.reset();
            int stmt2cnt = 0;
            while (dbPSO.hasNext())
            {
                stmt2cnt++;
                SQLParameterType tempSPT = (SQLParameterType) dbPSO.getNextElement();
                if (tempSPT.getType()==SQLParameterType.INT_TYPE)
                {
        			stmt2.setInt(stmt2cnt, new Integer(tempSPT.getValue()).intValue());
                }
                else if (tempSPT.getType()==SQLParameterType.DOUBLE_TYPE)
                {
                    stmt2.setObject(stmt2cnt, new Double(tempSPT.getValue()).doubleValue(),2);
                }
                else if (tempSPT.getType()==SQLParameterType.DATE_TYPE)
                {
                    stmt2.setTimestamp(    stmt2cnt, new Timestamp (  (sdDefaultFormatter.parse(tempSPT.getValue()) ).getTime() )    );
                }
                else if (tempSPT.getType()==SQLParameterType.STRING_TYPE)
                {
//                    stmt2.setObject(    stmt2cnt,  tempSPT.getValue(), 25  );
                    stmt2.setString(stmt2cnt, tempSPT.getValue());
                }
            }
//			stmt2.setInt(1, iUniqueID.intValue());
			stmt2.executeQuery();

//sqi change            ResultSet           rs = stmt.executeQuery( sSQLString );
            myRS = stmt2.executeQuery();
        }
        catch (Exception e)
        {
            DebugLogger.println("QuickDB: executePreparedStatement: [ResultSet]: [" + e.toString() + "] SQL=["+dbPSO.getSQL()+"];");
            DebugLogger.sendFatalError("QuickDB: executePreparedStatement: [ResultSet]: [" + e.toString() + "]");
        }
        return myRS;
    }



    public void closeAll()
    {
        try
        {
            theStatement.close();
        }
        catch (Exception e)
        {
            //DebugLogger.println("QuickDB Degbug: [ConnectionClose]" + e.toString());
        }
        try
        {
            stmt.close();
        }
        catch (Exception e)
        {
            //DebugLogger.println("QuickDB Degbug: [ConnectionClose]" + e.toString());
        }
        try
        {
            stmt2.close();
        }
        catch (Exception e)
        {
            //DebugLogger.println("QuickDB Degbug: [ConnectionClose]" + e.toString());
        }
        try
        {
            myRS.close();
        }
        catch (Exception e)
        {
            //DebugLogger.println("QuickDB Degbug: [ConnectionClose]" + e.toString());
        }
        try
        {
            theConnection.close();
        }
        catch (Exception e)
        {
            //DebugLogger.println("QuickDB Degbug: [ConnectionClose]" + e.toString());
        }
    }

	public void setDbUserName(String dbUserName)
	{
			this.dbUserName = dbUserName;
	}

	public String getDbUserName()
	{
		return this.dbUserName;
	}

	public void setDbPassword(String dbPassword)
	{
			this.dbPassword = dbPassword;
	}

	public String getDbPassword()
	{
		return this.dbPassword;
	}

	public void setDbForName(String dbForName)
	{
			this.dbForName = dbForName;
	}

	public String getDbForName()
	{
		return this.dbForName;
	}

	public void setDbConnPre(String dbConnPre)
	{
			this.dbConnPre = dbConnPre;
	}

	public String getDbConnPre()
	{
		return this.dbConnPre;
	}


	public void setDbConn(String dbConn)
	{
			this.dbConn = dbConn;
	}

	protected String dbForName = ConfigurationInformation.sJDBCDriverName;
	protected String dbPassword = ConfigurationInformation.sPassword;
	protected String dbUserName = ConfigurationInformation.sUser;
	protected String dbConnPre = "jdbc:odbc:";
	protected String dbConn = ConfigurationInformation.sURL;


	//{{DECLARE_CONTROLS
	//}}
}
