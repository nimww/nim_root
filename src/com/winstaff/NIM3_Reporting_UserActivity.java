package com.winstaff;

/**
 * Created by IntelliJ IDEA.
 * User: ste
 * Date: 11/10/11
 * Time: 2:54 PM
 * To change this template use File | Settings | File Templates.
 */
public class NIM3_Reporting_UserActivity {


    int year = 0;
    int month = 0;
    int countMR = 0;
    int countCT = 0;
    int countEMG = 0;
    int countOther = 0;
    int countVoid = 0;
    int countCourtesy = 0;


    public NIM3_Reporting_UserActivity(int year, int month) {
        this.year = year;
        this.month = month;
    }

    public NIM3_Reporting_UserActivity() {
    }


    public int getCountTotal(boolean includeVoids, boolean includeCourtesy){
        int myVal = getCountCT() + getCountEMG() + getCountMR() + getCountOther();
        if (includeVoids){
            myVal += getCountVoid();
        }
        if (includeCourtesy){
            myVal +=  getCountCourtesy();
        }
        return myVal;
    }

    public int getYear() {
        return year;
    }


    public void setYear(int year) {
        this.year = year;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public String getMonthYear() {
        return this.month + "/" + this.year;
    }

    public int getCountMR() {
        return countMR;
    }

    public void setCountMR(int countMR) {
        this.countMR = countMR;
    }

    public void addCountMR(int countMR) {
        this.countMR += countMR;
    }

    public int getCountCT() {
        return countCT;
    }

    public void setCountCT(int countCT) {
        this.countCT = countCT;
    }

    public void addCountCT(int countCT) {
        this.countCT += countCT;
    }

    public int getCountEMG() {
        return countEMG;
    }

    public void setCountEMG(int countEMG) {
        this.countEMG = countEMG;
    }

    public void addCountEMG(int countEMG) {
        this.countEMG += countEMG;
    }

    public int getCountOther() {
        return countOther;
    }

    public void setCountOther(int countOther) {
        this.countOther = countOther;
    }

    public void addCountOther(int countOther) {
        this.countOther += countOther;
    }

    public int getCountVoid() {
        return countVoid;
    }

    public void setCountVoid(int countVoid) {
        this.countVoid = countVoid;
    }

    public void addCountVoid(int countVoid) {
        this.countVoid += countVoid;
    }

    public int getCountCourtesy() {
        return countCourtesy;
    }

    public void setCountCourtesy(int countCourtesy) {
        this.countCourtesy = countCourtesy;
    }

    public void addCountCourtesy(int countCourtesy) {
        this.countCourtesy += countCourtesy;
    }
}
