package com.winstaff;
/*
 * bltDocumentManagement_List.java
 *
 * Created: Wed Apr 02 15:09:25 PST 2003
 */



import java.sql.*;
import com.winstaff.dbtDocumentManagement;
import com.winstaff.ListElement;


/**
 *
 * @author  Scott Ellis via listClassGenCode
 * @version 2.5
 */
public class bltDocumentManagement_List extends db_BaseRelationTable
{
    /** Default constructor
    */    
    public bltDocumentManagement_List ( Integer iPhysicianID )
    {
        super( "tDocumentManagement", "DocumentID", "DocumentID", "PhysicianID", iPhysicianID );
    }   // End of bltDocumentManagement_List()
    

    public void setField(String fieldN, Object fieldV)
    {DebugLogger.println("setField Method not implemented yet in classbltDocumentManagement_List!");}

    public String getFieldType(String fieldN)
    {return null;}

    public bltDocumentManagement_List ( Integer iPhysicianID, String extraWhere, String OrderBy )
    {
        super( "tDocumentManagement", "DocumentID", "DocumentID", "PhysicianID", iPhysicianID, extraWhere,OrderBy );
    }   // End of bltDocumentManagement_List()
     /** Method pull information from the result set and load into class attributes.
    * @param sFieldList Comma delimited list of fields for which data has been retrieved.
    * @param rs Result set from which to pull the data from.
    * @throws SQLException Passes on SQLException
    */      
    protected void transferData( ResultSet rs ) throws SQLException
    {
        while ( rs.next()  )
        {
            Integer         iValue              = new Integer( rs.getString( sLinkFieldName ) );
            Integer         iLinkRecordID       = new Integer( rs.getString( sUniqueIDFieldName ) );
            Object          oReferenceObject;
            ListElement     leNewElement;
            
            oReferenceObject    = new bltDocumentManagement( iValue );
            
            leNewElement        = new ListElement( iLinkRecordID, oReferenceObject );
            
            vList.add( leNewElement );

        }
    }   // End of transferData()
}   // End of bltDocumentManagement_List class

