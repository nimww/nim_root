

package com.winstaff;

import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;
import java.util.Enumeration;

public class bltNIM3_Appointment extends Object implements InttNIM3_Appointment
{

    dbtNIM3_Appointment    dbDB;

    public bltNIM3_Appointment()
    {
        dbDB = new dbtNIM3_Appointment();
        dbDB.setUniqueCreateDate(new java.util.Date());
        makeRequiredHash();
        makeExpiredHash();
        makeEnglishHash();

    }    // End of default constructor

    public bltNIM3_Appointment( Integer iNewID )
    {        dbDB = new dbtNIM3_Appointment( iNewID );
        makeRequiredHash();
        makeExpiredHash();
        makeEnglishHash();
        iSecurityCheck = new Integer(2);
    }    // End of Constructor knowing an ID


    public bltNIM3_Appointment( Integer iNewID, Integer iGroupSecurityID )
    {
        this.GroupSecurityInit(iGroupSecurityID);
        dbDB = new dbtNIM3_Appointment( iNewID );
        makeExpiredHash();
        makeEnglishHash();
        makeRequiredHash(iGroupSecurityID);
    }    // End of Constructor knowing an ID

    public bltNIM3_Appointment( Integer iGroupSecurityID, boolean isTrue )
    {
        this.GroupSecurityInit(iGroupSecurityID);
        dbDB = new dbtNIM3_Appointment(  );
        makeExpiredHash();
        makeEnglishHash();
        makeRequiredHash(iGroupSecurityID);
    }    // End of Security unknown ID

    public void GroupSecurityInit( Integer iGroupSecurityID )
    {
        bltSecurityGroupMaster mySGM = new bltSecurityGroupMaster(iGroupSecurityID);
        this.UserSecurityID = iGroupSecurityID;
        if (mySGM.getRunFieldSecurityID().intValue()==1)
        {
            this.runFieldSecurity = true;
            makeRequiredHash(iGroupSecurityID);
        }
        iSecurityCheck = SecurityCheck.CheckItem("nim1", iGroupSecurityID);
    }  

    public EventReferenceType getEventReference()
    {
        return EventUtils.getEventReference("tNIM3_Appointment", this.getUniqueID());
    }  

    public String convertString2LogString(String normalString) 
    {
        String myVal = "";
        for (int i=0;i<normalString.length();i++)
        {
            if ( (normalString.charAt(i)!='\r')&&(normalString.charAt(i)!='\n')&&(normalString.charAt(i)!='\t') )
            {
                myVal+=normalString.charAt(i);
            }
            else
            {
                myVal+="<NLT>";
            }
        }
        return myVal;
    }
    public void runAudit()
    {
        String AuditString = new java.util.Date()+" \t " ;  
        AuditString += " tNIM3_Appointment "; 
        AuditString += " AppointmentID ="+this.getUniqueID(); 
        AuditString += " ProviderID ="+this.getProviderID(); 
        for (int i=0;i<this.AuditVector.size();i++)
        {
            AuditString+="\t"+convertString2LogString((String)this.AuditVector.elementAt(i));
        }
        AuditLogger.printLine(AuditString);
    }
    public void commitData() throws SQLException
    {
        runAudit();
        dbDB.setModifyComments(this.getUniqueModifyComments());
        dbDB.setUniqueModifyDate(new java.util.Date());
        dbDB.commitData();
        DataControlUtils.dataChanged("tNIM3_Appointment",this.getProviderID() ,this.AuditVector.size(), "ProviderID");
        this.AuditVector.removeAllElements();
    }    // End of commitData()

    public void commitDataForced() throws SQLException
    {
        dbDB.commitDataForced();
    }    // End of commitData()

    public Integer getUniqueID()
    {
        return dbDB.getUniqueID();
    }    // End of getUniqueID()

    public void setUniqueID( Integer iNewID )
    {
        dbDB.setUniqueID( iNewID );
    }    // End of setUniqueID()


    public void setAppointmentID(Integer newValue)
    {
        dbDB.setAppointmentID(newValue);
    }

    public Integer getAppointmentID()
    {
        return dbDB.getAppointmentID();
    }

    java.util.Vector AuditVector = new java.util.Vector();

    public void setUniqueCreateDate(Date newValue)
    {
        this.setUniqueCreateDate(newValue,this.UserSecurityID);


    }
    public Date getUniqueCreateDate()
    {
        return this.getUniqueCreateDate(this.UserSecurityID);
    }

    public void setUniqueCreateDate(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='UniqueCreateDate' AND TableName='tNIM3_Appointment'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[UniqueCreateDate]=["+newValue+"]");
                   dbDB.setUniqueCreateDate(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[UniqueCreateDate]=["+newValue+"]");
           dbDB.setUniqueCreateDate(newValue);
         }
    }
    public Date getUniqueCreateDate(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='UniqueCreateDate' AND TableName='tNIM3_Appointment'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getUniqueCreateDate();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getUniqueCreateDate();
         }
        return myVal;
    }

    public void setUniqueModifyDate(Date newValue)
    {
        this.setUniqueModifyDate(newValue,this.UserSecurityID);


    }
    public Date getUniqueModifyDate()
    {
        return this.getUniqueModifyDate(this.UserSecurityID);
    }

    public void setUniqueModifyDate(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='UniqueModifyDate' AND TableName='tNIM3_Appointment'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[UniqueModifyDate]=["+newValue+"]");
                   dbDB.setUniqueModifyDate(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[UniqueModifyDate]=["+newValue+"]");
           dbDB.setUniqueModifyDate(newValue);
         }
    }
    public Date getUniqueModifyDate(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='UniqueModifyDate' AND TableName='tNIM3_Appointment'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getUniqueModifyDate();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getUniqueModifyDate();
         }
        return myVal;
    }

    public void setUniqueModifyComments(String newValue)
    {
        this.setUniqueModifyComments(newValue,this.UserSecurityID);


    }
    public String getUniqueModifyComments()
    {
        return this.getUniqueModifyComments(this.UserSecurityID);
    }

    public void setUniqueModifyComments(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='UniqueModifyComments' AND TableName='tNIM3_Appointment'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[UniqueModifyComments]=["+newValue+"]");
                   dbDB.setUniqueModifyComments(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[UniqueModifyComments]=["+newValue+"]");
           dbDB.setUniqueModifyComments(newValue);
         }
    }
    public String getUniqueModifyComments(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='UniqueModifyComments' AND TableName='tNIM3_Appointment'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getUniqueModifyComments();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getUniqueModifyComments();
         }
        return myVal;
    }

    public void setProviderID(Integer newValue)
    {
        this.setProviderID(newValue,this.UserSecurityID);


    }
    public Integer getProviderID()
    {
        return this.getProviderID(this.UserSecurityID);
    }

    public void setProviderID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ProviderID' AND TableName='tNIM3_Appointment'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[ProviderID]=["+newValue+"]");
                   dbDB.setProviderID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[ProviderID]=["+newValue+"]");
           dbDB.setProviderID(newValue);
         }
    }
    public Integer getProviderID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ProviderID' AND TableName='tNIM3_Appointment'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getProviderID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getProviderID();
         }
        return myVal;
    }

    public void setICID(Integer newValue)
    {
        this.setICID(newValue,this.UserSecurityID);


    }
    public Integer getICID()
    {
        return this.getICID(this.UserSecurityID);
    }

    public void setICID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ICID' AND TableName='tNIM3_Appointment'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[ICID]=["+newValue+"]");
                   dbDB.setICID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[ICID]=["+newValue+"]");
           dbDB.setICID(newValue);
         }
    }
    public Integer getICID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ICID' AND TableName='tNIM3_Appointment'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getICID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getICID();
         }
        return myVal;
    }

    public void setiStatus(Integer newValue)
    {
        this.setiStatus(newValue,this.UserSecurityID);


    }
    public Integer getiStatus()
    {
        return this.getiStatus(this.UserSecurityID);
    }

    public void setiStatus(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='iStatus' AND TableName='tNIM3_Appointment'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[iStatus]=["+newValue+"]");
                   dbDB.setiStatus(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[iStatus]=["+newValue+"]");
           dbDB.setiStatus(newValue);
         }
    }
    public Integer getiStatus(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='iStatus' AND TableName='tNIM3_Appointment'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getiStatus();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getiStatus();
         }
        return myVal;
    }

    public void setAppointmentTime(Date newValue)
    {
        this.setAppointmentTime(newValue,this.UserSecurityID);


    }
    public Date getAppointmentTime()
    {
        return this.getAppointmentTime(this.UserSecurityID);
    }

    public void setAppointmentTime(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AppointmentTime' AND TableName='tNIM3_Appointment'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[AppointmentTime]=["+newValue+"]");
                   dbDB.setAppointmentTime(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[AppointmentTime]=["+newValue+"]");
           dbDB.setAppointmentTime(newValue);
         }
    }
    public Date getAppointmentTime(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AppointmentTime' AND TableName='tNIM3_Appointment'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getAppointmentTime();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getAppointmentTime();
         }
        return myVal;
    }

    public void setActualTime(Date newValue)
    {
        this.setActualTime(newValue,this.UserSecurityID);


    }
    public Date getActualTime()
    {
        return this.getActualTime(this.UserSecurityID);
    }

    public void setActualTime(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ActualTime' AND TableName='tNIM3_Appointment'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[ActualTime]=["+newValue+"]");
                   dbDB.setActualTime(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[ActualTime]=["+newValue+"]");
           dbDB.setActualTime(newValue);
         }
    }
    public Date getActualTime(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ActualTime' AND TableName='tNIM3_Appointment'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getActualTime();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getActualTime();
         }
        return myVal;
    }

    public void setAppointmentConfirmation(String newValue)
    {
        this.setAppointmentConfirmation(newValue,this.UserSecurityID);


    }
    public String getAppointmentConfirmation()
    {
        return this.getAppointmentConfirmation(this.UserSecurityID);
    }

    public void setAppointmentConfirmation(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AppointmentConfirmation' AND TableName='tNIM3_Appointment'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[AppointmentConfirmation]=["+newValue+"]");
                   dbDB.setAppointmentConfirmation(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[AppointmentConfirmation]=["+newValue+"]");
           dbDB.setAppointmentConfirmation(newValue);
         }
    }
    public String getAppointmentConfirmation(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AppointmentConfirmation' AND TableName='tNIM3_Appointment'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getAppointmentConfirmation();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getAppointmentConfirmation();
         }
        return myVal;
    }

    public void setScheduler(String newValue)
    {
        this.setScheduler(newValue,this.UserSecurityID);


    }
    public String getScheduler()
    {
        return this.getScheduler(this.UserSecurityID);
    }

    public void setScheduler(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Scheduler' AND TableName='tNIM3_Appointment'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[Scheduler]=["+newValue+"]");
                   dbDB.setScheduler(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[Scheduler]=["+newValue+"]");
           dbDB.setScheduler(newValue);
         }
    }
    public String getScheduler(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Scheduler' AND TableName='tNIM3_Appointment'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getScheduler();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getScheduler();
         }
        return myVal;
    }

    public void setInitialEncounterID(Integer newValue)
    {
        this.setInitialEncounterID(newValue,this.UserSecurityID);


    }
    public Integer getInitialEncounterID()
    {
        return this.getInitialEncounterID(this.UserSecurityID);
    }

    public void setInitialEncounterID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='InitialEncounterID' AND TableName='tNIM3_Appointment'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[InitialEncounterID]=["+newValue+"]");
                   dbDB.setInitialEncounterID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[InitialEncounterID]=["+newValue+"]");
           dbDB.setInitialEncounterID(newValue);
         }
    }
    public Integer getInitialEncounterID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='InitialEncounterID' AND TableName='tNIM3_Appointment'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getInitialEncounterID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getInitialEncounterID();
         }
        return myVal;
    }

    public void setScheduler_UserID(Integer newValue)
    {
        this.setScheduler_UserID(newValue,this.UserSecurityID);


    }
    public Integer getScheduler_UserID()
    {
        return this.getScheduler_UserID(this.UserSecurityID);
    }

    public void setScheduler_UserID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Scheduler_UserID' AND TableName='tNIM3_Appointment'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[Scheduler_UserID]=["+newValue+"]");
                   dbDB.setScheduler_UserID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[Scheduler_UserID]=["+newValue+"]");
           dbDB.setScheduler_UserID(newValue);
         }
    }
    public Integer getScheduler_UserID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Scheduler_UserID' AND TableName='tNIM3_Appointment'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getScheduler_UserID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getScheduler_UserID();
         }
        return myVal;
    }

    public void setProviderConfirmSet(Integer newValue)
    {
        this.setProviderConfirmSet(newValue,this.UserSecurityID);


    }
    public Integer getProviderConfirmSet()
    {
        return this.getProviderConfirmSet(this.UserSecurityID);
    }

    public void setProviderConfirmSet(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ProviderConfirmSet' AND TableName='tNIM3_Appointment'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[ProviderConfirmSet]=["+newValue+"]");
                   dbDB.setProviderConfirmSet(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[ProviderConfirmSet]=["+newValue+"]");
           dbDB.setProviderConfirmSet(newValue);
         }
    }
    public Integer getProviderConfirmSet(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ProviderConfirmSet' AND TableName='tNIM3_Appointment'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getProviderConfirmSet();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getProviderConfirmSet();
         }
        return myVal;
    }

    public void setProviderConfirmSet_UserID(Integer newValue)
    {
        this.setProviderConfirmSet_UserID(newValue,this.UserSecurityID);


    }
    public Integer getProviderConfirmSet_UserID()
    {
        return this.getProviderConfirmSet_UserID(this.UserSecurityID);
    }

    public void setProviderConfirmSet_UserID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ProviderConfirmSet_UserID' AND TableName='tNIM3_Appointment'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[ProviderConfirmSet_UserID]=["+newValue+"]");
                   dbDB.setProviderConfirmSet_UserID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[ProviderConfirmSet_UserID]=["+newValue+"]");
           dbDB.setProviderConfirmSet_UserID(newValue);
         }
    }
    public Integer getProviderConfirmSet_UserID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ProviderConfirmSet_UserID' AND TableName='tNIM3_Appointment'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getProviderConfirmSet_UserID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getProviderConfirmSet_UserID();
         }
        return myVal;
    }

    public void setProviderConfirmDate(Date newValue)
    {
        this.setProviderConfirmDate(newValue,this.UserSecurityID);


    }
    public Date getProviderConfirmDate()
    {
        return this.getProviderConfirmDate(this.UserSecurityID);
    }

    public void setProviderConfirmDate(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ProviderConfirmDate' AND TableName='tNIM3_Appointment'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[ProviderConfirmDate]=["+newValue+"]");
                   dbDB.setProviderConfirmDate(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[ProviderConfirmDate]=["+newValue+"]");
           dbDB.setProviderConfirmDate(newValue);
         }
    }
    public Date getProviderConfirmDate(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ProviderConfirmDate' AND TableName='tNIM3_Appointment'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getProviderConfirmDate();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getProviderConfirmDate();
         }
        return myVal;
    }

    public void setProviderAppointmentConfirmation(String newValue)
    {
        this.setProviderAppointmentConfirmation(newValue,this.UserSecurityID);


    }
    public String getProviderAppointmentConfirmation()
    {
        return this.getProviderAppointmentConfirmation(this.UserSecurityID);
    }

    public void setProviderAppointmentConfirmation(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ProviderAppointmentConfirmation' AND TableName='tNIM3_Appointment'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[ProviderAppointmentConfirmation]=["+newValue+"]");
                   dbDB.setProviderAppointmentConfirmation(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[ProviderAppointmentConfirmation]=["+newValue+"]");
           dbDB.setProviderAppointmentConfirmation(newValue);
         }
    }
    public String getProviderAppointmentConfirmation(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ProviderAppointmentConfirmation' AND TableName='tNIM3_Appointment'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getProviderAppointmentConfirmation();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getProviderAppointmentConfirmation();
         }
        return myVal;
    }

    public void setProviderConfirmShow(Integer newValue)
    {
        this.setProviderConfirmShow(newValue,this.UserSecurityID);


    }
    public Integer getProviderConfirmShow()
    {
        return this.getProviderConfirmShow(this.UserSecurityID);
    }

    public void setProviderConfirmShow(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ProviderConfirmShow' AND TableName='tNIM3_Appointment'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[ProviderConfirmShow]=["+newValue+"]");
                   dbDB.setProviderConfirmShow(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[ProviderConfirmShow]=["+newValue+"]");
           dbDB.setProviderConfirmShow(newValue);
         }
    }
    public Integer getProviderConfirmShow(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ProviderConfirmShow' AND TableName='tNIM3_Appointment'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getProviderConfirmShow();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getProviderConfirmShow();
         }
        return myVal;
    }

    public void setProviderConfirmShow_UserID(Integer newValue)
    {
        this.setProviderConfirmShow_UserID(newValue,this.UserSecurityID);


    }
    public Integer getProviderConfirmShow_UserID()
    {
        return this.getProviderConfirmShow_UserID(this.UserSecurityID);
    }

    public void setProviderConfirmShow_UserID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ProviderConfirmShow_UserID' AND TableName='tNIM3_Appointment'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[ProviderConfirmShow_UserID]=["+newValue+"]");
                   dbDB.setProviderConfirmShow_UserID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[ProviderConfirmShow_UserID]=["+newValue+"]");
           dbDB.setProviderConfirmShow_UserID(newValue);
         }
    }
    public Integer getProviderConfirmShow_UserID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ProviderConfirmShow_UserID' AND TableName='tNIM3_Appointment'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getProviderConfirmShow_UserID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getProviderConfirmShow_UserID();
         }
        return myVal;
    }

    public void setProviderConfirmShowDate(Date newValue)
    {
        this.setProviderConfirmShowDate(newValue,this.UserSecurityID);


    }
    public Date getProviderConfirmShowDate()
    {
        return this.getProviderConfirmShowDate(this.UserSecurityID);
    }

    public void setProviderConfirmShowDate(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ProviderConfirmShowDate' AND TableName='tNIM3_Appointment'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[ProviderConfirmShowDate]=["+newValue+"]");
                   dbDB.setProviderConfirmShowDate(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[ProviderConfirmShowDate]=["+newValue+"]");
           dbDB.setProviderConfirmShowDate(newValue);
         }
    }
    public Date getProviderConfirmShowDate(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ProviderConfirmShowDate' AND TableName='tNIM3_Appointment'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getProviderConfirmShowDate();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getProviderConfirmShowDate();
         }
        return myVal;
    }

    public void setProviderNotes(String newValue)
    {
        this.setProviderNotes(newValue,this.UserSecurityID);


    }
    public String getProviderNotes()
    {
        return this.getProviderNotes(this.UserSecurityID);
    }

    public void setProviderNotes(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ProviderNotes' AND TableName='tNIM3_Appointment'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[ProviderNotes]=["+newValue+"]");
                   dbDB.setProviderNotes(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[ProviderNotes]=["+newValue+"]");
           dbDB.setProviderNotes(newValue);
         }
    }
    public String getProviderNotes(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ProviderNotes' AND TableName='tNIM3_Appointment'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getProviderNotes();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getProviderNotes();
         }
        return myVal;
    }

    public void setVoidNotes(String newValue)
    {
        this.setVoidNotes(newValue,this.UserSecurityID);


    }
    public String getVoidNotes()
    {
        return this.getVoidNotes(this.UserSecurityID);
    }

    public void setVoidNotes(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='VoidNotes' AND TableName='tNIM3_Appointment'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[VoidNotes]=["+newValue+"]");
                   dbDB.setVoidNotes(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[VoidNotes]=["+newValue+"]");
           dbDB.setVoidNotes(newValue);
         }
    }
    public String getVoidNotes(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='VoidNotes' AND TableName='tNIM3_Appointment'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getVoidNotes();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getVoidNotes();
         }
        return myVal;
    }

    public void setPatientConfirmSet(Integer newValue)
    {
        this.setPatientConfirmSet(newValue,this.UserSecurityID);


    }
    public Integer getPatientConfirmSet()
    {
        return this.getPatientConfirmSet(this.UserSecurityID);
    }

    public void setPatientConfirmSet(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientConfirmSet' AND TableName='tNIM3_Appointment'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[PatientConfirmSet]=["+newValue+"]");
                   dbDB.setPatientConfirmSet(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[PatientConfirmSet]=["+newValue+"]");
           dbDB.setPatientConfirmSet(newValue);
         }
    }
    public Integer getPatientConfirmSet(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientConfirmSet' AND TableName='tNIM3_Appointment'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPatientConfirmSet();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPatientConfirmSet();
         }
        return myVal;
    }

    public void setPatientConfirmSet_UserID(Integer newValue)
    {
        this.setPatientConfirmSet_UserID(newValue,this.UserSecurityID);


    }
    public Integer getPatientConfirmSet_UserID()
    {
        return this.getPatientConfirmSet_UserID(this.UserSecurityID);
    }

    public void setPatientConfirmSet_UserID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientConfirmSet_UserID' AND TableName='tNIM3_Appointment'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[PatientConfirmSet_UserID]=["+newValue+"]");
                   dbDB.setPatientConfirmSet_UserID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[PatientConfirmSet_UserID]=["+newValue+"]");
           dbDB.setPatientConfirmSet_UserID(newValue);
         }
    }
    public Integer getPatientConfirmSet_UserID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientConfirmSet_UserID' AND TableName='tNIM3_Appointment'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPatientConfirmSet_UserID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPatientConfirmSet_UserID();
         }
        return myVal;
    }

    public void setPatientConfirmDate(Date newValue)
    {
        this.setPatientConfirmDate(newValue,this.UserSecurityID);


    }
    public Date getPatientConfirmDate()
    {
        return this.getPatientConfirmDate(this.UserSecurityID);
    }

    public void setPatientConfirmDate(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientConfirmDate' AND TableName='tNIM3_Appointment'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[PatientConfirmDate]=["+newValue+"]");
                   dbDB.setPatientConfirmDate(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[PatientConfirmDate]=["+newValue+"]");
           dbDB.setPatientConfirmDate(newValue);
         }
    }
    public Date getPatientConfirmDate(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientConfirmDate' AND TableName='tNIM3_Appointment'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPatientConfirmDate();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPatientConfirmDate();
         }
        return myVal;
    }

    public void setComments(String newValue)
    {
        this.setComments(newValue,this.UserSecurityID);


    }
    public String getComments()
    {
        return this.getComments(this.UserSecurityID);
    }

    public void setComments(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Comments' AND TableName='tNIM3_Appointment'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[Comments]=["+newValue+"]");
                   dbDB.setComments(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[Comments]=["+newValue+"]");
           dbDB.setComments(newValue);
         }
    }
    public String getComments(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Comments' AND TableName='tNIM3_Appointment'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getComments();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getComments();
         }
        return myVal;
    }

    public void setRequiredFieldList(java.util.Hashtable requiredFieldList)
	 {

        this.requiredFieldList = requiredFieldList;
    }

    public java.util.Hashtable getRequiredFieldList()
    {
        return this.requiredFieldList;
    }
    public void setExpiredFieldList(java.util.Hashtable expiredFieldList)
    {
        this.expiredFieldList = expiredFieldList;
    }

    public void setEnglishFieldList(java.util.Hashtable englishFieldList)
    {
        this.englishFieldList = englishFieldList;
    }

    public java.util.Hashtable getExpiredFieldList()
    {
        return this.expiredFieldList;
    }
    public boolean isExpired(java.util.Date myDate, int daysVector)
    {
       return isExpired(myDate,new java.util.Date(),daysVector);
    }


    public boolean isRead(String fieldN, Integer GroupRefID)
    {
        boolean myVal = false;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='"+fieldN+"' AND TableName='tNIM3_Appointment'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==1)
               {
                   myVal = true;
               }
            }
        }
        else
        {

           if (iSecurityCheck.intValue()>=1)
           {
                  myVal = true;
           }
        }
        return myVal;
    }


    public boolean isWrite(String fieldN, Integer GroupRefID)
    {
        boolean myVal = false;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='"+fieldN+"' AND TableName='tNIM3_Appointment'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {
                   myVal = true;
               }
            }
        }
        else
        {

           if (iSecurityCheck.intValue()>=2)
           {
                  myVal = true;
           }
        }
        return myVal;
    }


    public boolean isExpired(java.util.Date myDate, java.util.Date refDate,int daysVector)
    {
        java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat("yyyy-MM-dd");
        boolean myVal = true;
        try
        {
            if (myDate.after(dbdf.parse("1800-01-10")))
            {
                if (myDate.after(dateChange(refDate,daysVector)))
                {
                    myVal=false;
                }
            }
            else
            {
                myVal = false;
            }
        }
        catch (Exception Datee)
        {
        }
        return myVal;
    }

    public java.util.Date dateChange(java.util.Date myD, int dayChange)
    {
        java.util.Calendar calendar = java.util.Calendar.getInstance();
        calendar.setTime(myD);
        calendar.add(java.util.Calendar.DATE, dayChange);
        java.util.Date resultAsDate = new java.util.Date(calendar.getTime().getTime());
        return resultAsDate;
    }

    public boolean isRequired(String fieldN)
    {
        return isRequired(fieldN, new Integer("1"));
    }

    public boolean isRequired(String fieldN, Integer GroupRefID)
    {
        boolean myVal = false;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='"+fieldN+"' AND TableName='tNIM3_Appointment'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getRequiredID().intValue()==1)
               {
                   myVal = true;
               }
            }
        }
        else
        {
            if (this.getRequiredFieldList().containsKey(fieldN))
            {
                myVal = true;
            }
        }
        return myVal;
    }
    public boolean isExpiredCheck(String fieldN)
    {
        return isExpiredCheck(fieldN, new Integer ("1"));
    }

    public boolean isExpiredCheck(String fieldN, Integer GroupRefID)
    {
        boolean myVal = false;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='"+fieldN+"' AND TableName='tNIM3_Appointment'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getTrackedID().intValue()==1)
               {
                   myVal = true;
               }
            }
        }
        else
        {
            if (this.getExpiredFieldList().containsKey(fieldN))
           {
                myVal = true;
            }
        }
        return myVal;
    }

    public void makeRequiredHash()
    {
       makeRequiredHash(new Integer(1));
    }

    public void makeRequiredHash(Integer GroupRefID)
    {

        java.util.Hashtable newHash = new java.util.Hashtable();
        if (runFieldSecurity)
        {
            java.util.Enumeration myKeys = englishFieldList.keys(); 
            while (myKeys.hasMoreElements()) 
            {
                 String myElement = (String) myKeys.nextElement();
                 bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='"+myElement+"' AND TableName='tNIM3_Appointment'" ,"");
                 bltFieldSecurity        working_bltFieldSecurity;
                 ListElement         leCurrentElement;
                 Enumeration eList = myFSL.elements();
                 if (eList.hasMoreElements())
                 {
                     leCurrentElement    = (ListElement) eList.nextElement();
                     working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
                     if (working_bltFieldSecurity.getRequiredID().intValue()==1)
                     {
                         newHash.put(myElement,new Boolean(true));
                     }
                 }
            }
        }
        else
        {
            newHash.put("ProviderID",new Boolean(true));
            newHash.put("ICID",new Boolean(true));
            newHash.put("iStatus",new Boolean(true));
            newHash.put("AppointmentConfirmation",new Boolean(true));
            newHash.put("Scheduler",new Boolean(true));
            newHash.put("Scheduler_UserID",new Boolean(true));
            newHash.put("Comments",new Boolean(true));
        }
        this.setRequiredFieldList(newHash);
    }

    public void makeEnglishHash()
    {

        java.util.Hashtable newHash = new java.util.Hashtable();        newHash.put("UniqueCreateDate","UniqueCreateDate");
        newHash.put("UniqueModifyDate","UniqueModifyDate");
        newHash.put("UniqueModifyComments","UniqueModifyComments");
        newHash.put("ProviderID","ProviderID");
        newHash.put("ICID","ICID");
        newHash.put("iStatus","iStatus");
        newHash.put("AppointmentTime","AppointmentTime");
        newHash.put("ActualTime","ActualTime");
        newHash.put("AppointmentConfirmation","AppointmentConfirmation");
        newHash.put("Scheduler","Scheduler");
        newHash.put("InitialEncounterID","InitialEncounterID");
        newHash.put("Scheduler_UserID","Scheduler_UserID");
        newHash.put("ProviderConfirmSet","ProviderConfirmSet");
        newHash.put("ProviderConfirmSet_UserID","ProviderConfirmSet_UserID");
        newHash.put("ProviderConfirmDate","ProviderConfirmDate");
        newHash.put("ProviderAppointmentConfirmation","ProviderAppointmentConfirmation");
        newHash.put("ProviderConfirmShow","ProviderConfirmShow");
        newHash.put("ProviderConfirmShow_UserID","ProviderConfirmShow_UserID");
        newHash.put("ProviderConfirmShowDate","ProviderConfirmShowDate");
        newHash.put("ProviderNotes","ProviderNotes");
        newHash.put("VoidNotes","VoidNotes");
        newHash.put("PatientConfirmSet","PatientConfirmSet");
        newHash.put("PatientConfirmSet_UserID","PatientConfirmSet_UserID");
        newHash.put("PatientConfirmDate","PatientConfirmDate");
        newHash.put("Comments","Comments");

        this.setEnglishFieldList(newHash);
    }
    public void makeExpiredHash()
    {

        java.util.Hashtable newHash = new java.util.Hashtable();
        this.setExpiredFieldList(newHash);
    }

    public java.util.Hashtable englishFieldList = new java.util.Hashtable();
    public java.util.Hashtable requiredFieldList = new java.util.Hashtable();
    public java.util.Hashtable expiredFieldList = new java.util.Hashtable();

    public boolean isComplete()
    {
	     boolean myVal = true;
	     java.util.Enumeration myKeys = this.getRequiredFieldList().keys(); 
	     while (myKeys.hasMoreElements()) 
	     {
	         String myElement = (String) myKeys.nextElement();
	         if (!isComplete(myElement))
	         {
	             myVal = false;
	         }
	     }
	     return myVal;
    }

    public String getEnglish(String fieldN)
    {
        String myVal = null;
        if (this.englishFieldList.containsKey(fieldN))
        {
            myVal = (String)this.englishFieldList.get(fieldN);
        }
        return myVal;
    }
    public java.util.Vector isComplete(int daysVector)
    {
	     boolean myVal = false;
	     java.util.Enumeration myKeys = this.getExpiredFieldList().keys(); 
	     java.util.Vector myExpiredList = new java.util.Vector(); 
	     while (myKeys.hasMoreElements()) 
	     {
	         String myElement = (String) myKeys.nextElement();
	         if (isExpired(myElement,daysVector))
	         {
	             myVal = true;
	             myExpiredList.addElement(myElement);
	         }
	     }
	     return myExpiredList;
    }

    public void setField(String fieldN, Object fieldV)
    {
     boolean myVal = false;
     try
     {
        java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat("yyyy-MM-dd");

        if (fieldN.equals("AppointmentID"))
        {
             this.setAppointmentID((Integer)fieldV);
        }

        else if (fieldN.equals("UniqueCreateDate"))
        {
            this.setUniqueCreateDate((java.util.Date)fieldV);
        }

        else if (fieldN.equals("UniqueModifyDate"))
        {
            this.setUniqueModifyDate((java.util.Date)fieldV);
        }

        else if (fieldN.equals("UniqueModifyComments"))
        {
            this.setUniqueModifyComments((String)fieldV);
        }

        else if (fieldN.equals("ProviderID"))
        {
             this.setProviderID((Integer)fieldV);
        }

        else if (fieldN.equals("ICID"))
        {
             this.setICID((Integer)fieldV);
        }

        else if (fieldN.equals("iStatus"))
        {
             this.setiStatus((Integer)fieldV);
        }

        else if (fieldN.equals("AppointmentTime"))
        {
            this.setAppointmentTime((java.util.Date)fieldV);
        }

        else if (fieldN.equals("ActualTime"))
        {
            this.setActualTime((java.util.Date)fieldV);
        }

        else if (fieldN.equals("AppointmentConfirmation"))
        {
            this.setAppointmentConfirmation((String)fieldV);
        }

        else if (fieldN.equals("Scheduler"))
        {
            this.setScheduler((String)fieldV);
        }

        else if (fieldN.equals("InitialEncounterID"))
        {
             this.setInitialEncounterID((Integer)fieldV);
        }

        else if (fieldN.equals("Scheduler_UserID"))
        {
             this.setScheduler_UserID((Integer)fieldV);
        }

        else if (fieldN.equals("ProviderConfirmSet"))
        {
             this.setProviderConfirmSet((Integer)fieldV);
        }

        else if (fieldN.equals("ProviderConfirmSet_UserID"))
        {
             this.setProviderConfirmSet_UserID((Integer)fieldV);
        }

        else if (fieldN.equals("ProviderConfirmDate"))
        {
            this.setProviderConfirmDate((java.util.Date)fieldV);
        }

        else if (fieldN.equals("ProviderAppointmentConfirmation"))
        {
            this.setProviderAppointmentConfirmation((String)fieldV);
        }

        else if (fieldN.equals("ProviderConfirmShow"))
        {
             this.setProviderConfirmShow((Integer)fieldV);
        }

        else if (fieldN.equals("ProviderConfirmShow_UserID"))
        {
             this.setProviderConfirmShow_UserID((Integer)fieldV);
        }

        else if (fieldN.equals("ProviderConfirmShowDate"))
        {
            this.setProviderConfirmShowDate((java.util.Date)fieldV);
        }

        else if (fieldN.equals("ProviderNotes"))
        {
            this.setProviderNotes((String)fieldV);
        }

        else if (fieldN.equals("VoidNotes"))
        {
            this.setVoidNotes((String)fieldV);
        }

        else if (fieldN.equals("PatientConfirmSet"))
        {
             this.setPatientConfirmSet((Integer)fieldV);
        }

        else if (fieldN.equals("PatientConfirmSet_UserID"))
        {
             this.setPatientConfirmSet_UserID((Integer)fieldV);
        }

        else if (fieldN.equals("PatientConfirmDate"))
        {
            this.setPatientConfirmDate((java.util.Date)fieldV);
        }

        else if (fieldN.equals("Comments"))
        {
            this.setComments((String)fieldV);
        }

	    }
	    catch(Exception e2)
	    {}
	    //return myVal;
	}

    public String getFieldType(String fieldN)
    {
     String  myVal = null;
     try
     {
        java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat("yyyy-MM-dd");

        if (fieldN.equals("AppointmentID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("UniqueCreateDate"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("UniqueModifyDate"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("UniqueModifyComments"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("ProviderID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("ICID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("iStatus"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("AppointmentTime"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("ActualTime"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("AppointmentConfirmation"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("Scheduler"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("InitialEncounterID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("Scheduler_UserID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("ProviderConfirmSet"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("ProviderConfirmSet_UserID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("ProviderConfirmDate"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("ProviderAppointmentConfirmation"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("ProviderConfirmShow"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("ProviderConfirmShow_UserID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("ProviderConfirmShowDate"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("ProviderNotes"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("VoidNotes"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("PatientConfirmSet"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("PatientConfirmSet_UserID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("PatientConfirmDate"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("Comments"))
        {
            myVal = "String";
        }

	    }
	    catch(Exception e2)
	    {}
	    return myVal;
	}

    public boolean isComplete(String fieldN)
    {
	     boolean myVal = false;
	     try
	     {
        java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat("yyyy-MM-dd");

	     if (fieldN.equals("AppointmentID"))

	     {
                    if (this.getAppointmentID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("UniqueCreateDate"))

	     {
                    if (this.getUniqueCreateDate().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("UniqueModifyDate"))

	     {
                    if (this.getUniqueModifyDate().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("UniqueModifyComments"))

	     {
                    if (this.getUniqueModifyComments().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("ProviderID"))

	     {
                    if (this.getProviderID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("ICID"))

	     {
                    if (this.getICID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("iStatus"))

	     {
                    if (this.getiStatus().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("AppointmentTime"))

	     {
                    if (this.getAppointmentTime().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("ActualTime"))

	     {
                    if (this.getActualTime().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("AppointmentConfirmation"))

	     {
                    if (this.getAppointmentConfirmation().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("Scheduler"))

	     {
                    if (this.getScheduler().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("InitialEncounterID"))

	     {
                    if (this.getInitialEncounterID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("Scheduler_UserID"))

	     {
                    if (this.getScheduler_UserID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("ProviderConfirmSet"))

	     {
                    if (this.getProviderConfirmSet().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("ProviderConfirmSet_UserID"))

	     {
                    if (this.getProviderConfirmSet_UserID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("ProviderConfirmDate"))

	     {
                    if (this.getProviderConfirmDate().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("ProviderAppointmentConfirmation"))

	     {
                    if (this.getProviderAppointmentConfirmation().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("ProviderConfirmShow"))

	     {
                    if (this.getProviderConfirmShow().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("ProviderConfirmShow_UserID"))

	     {
                    if (this.getProviderConfirmShow_UserID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("ProviderConfirmShowDate"))

	     {
                    if (this.getProviderConfirmShowDate().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("ProviderNotes"))

	     {
                    if (this.getProviderNotes().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("VoidNotes"))

	     {
                    if (this.getVoidNotes().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("PatientConfirmSet"))

	     {
                    if (this.getPatientConfirmSet().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("PatientConfirmSet_UserID"))

	     {
                    if (this.getPatientConfirmSet_UserID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("PatientConfirmDate"))

	     {
                    if (this.getPatientConfirmDate().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("Comments"))

	     {
                    if (this.getComments().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	    }
	    catch(Exception e2)
	    {}
	    return myVal;
	}

    public boolean isExpired(String fieldN,int daysVector)
    {
        return isExpired(fieldN,new java.util.Date(),daysVector);
    }

    public boolean isExpired(String fieldN,java.util.Date refDate,int daysVector)
    {
	     boolean myVal = false;
	     try
	     {
        java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat("yyyy-MM-dd");

        if (fieldN.equals("UniqueCreateDate"))

	     {
                    if (this.isExpired(this.getUniqueCreateDate(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

        if (fieldN.equals("UniqueModifyDate"))

	     {
                    if (this.isExpired(this.getUniqueModifyDate(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

        if (fieldN.equals("AppointmentTime"))

	     {
                    if (this.isExpired(this.getAppointmentTime(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

        if (fieldN.equals("ActualTime"))

	     {
                    if (this.isExpired(this.getActualTime(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

        if (fieldN.equals("ProviderConfirmDate"))

	     {
                    if (this.isExpired(this.getProviderConfirmDate(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

        if (fieldN.equals("ProviderConfirmShowDate"))

	     {
                    if (this.isExpired(this.getProviderConfirmShowDate(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

        if (fieldN.equals("PatientConfirmDate"))

	     {
                    if (this.isExpired(this.getPatientConfirmDate(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

	    }
	    catch(Exception e2)
	    {}
	    return myVal;
	}

    public java.util.Date getDateField(String fieldN)
    {
	     java.util.Date myVal = null;
	     try
	     {
        java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat("yyyy-MM-dd");

        if (fieldN.equals("UniqueCreateDate"))

	     {
             myVal = this.getUniqueCreateDate();
        }

        if (fieldN.equals("UniqueModifyDate"))

	     {
             myVal = this.getUniqueModifyDate();
        }

        if (fieldN.equals("AppointmentTime"))

	     {
             myVal = this.getAppointmentTime();
        }

        if (fieldN.equals("ActualTime"))

	     {
             myVal = this.getActualTime();
        }

        if (fieldN.equals("ProviderConfirmDate"))

	     {
             myVal = this.getProviderConfirmDate();
        }

        if (fieldN.equals("ProviderConfirmShowDate"))

	     {
             myVal = this.getProviderConfirmShowDate();
        }

        if (fieldN.equals("PatientConfirmDate"))

	     {
             myVal = this.getPatientConfirmDate();
        }

	    }
	    catch(Exception e2)
	    {}
	    return myVal;
	}
	public boolean runFieldSecurity = false;
	public Integer UserSecurityID = null;
   public Integer iSecurityCheck = null;
}    // End of bltNIM3_Appointment class definition
