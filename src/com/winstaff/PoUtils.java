package com.winstaff;

import java.sql.SQLException;
import java.util.List;

public class PoUtils {
	/**
	 * Logs a hidden CT when users views case page.
	 * @param eid
	 * @param rid
	 * @param cid
	 * @param ua
	 */
	public static void viewCaseLog(int eid,int rid, int cid, bltUserAccount ua) {
		
		bltNIM3_CommTrack ct = new bltNIM3_CommTrack();

		ct.setUniqueModifyComments("PoUtils.viewCaseLog()");
		ct.setEncounterID(eid);
		ct.setReferralID(rid);
		ct.setCaseID(cid);
		ct.setMessageName(ua.getContactFirstName() + " " + ua.getContactLastName());
		ct.setMessageCompany("NextImage");
		ct.setMessageText(ua.getContactFirstName()+" has viewed this case. ");
		ct.setUniqueCreateDate(PLCUtils.getNowDate(false));
		ct.setUniqueModifyDate(PLCUtils.getNowDate(false));
		ct.setCommStart(PLCUtils.getNowDate(false));
		ct.setCommEnd(PLCUtils.getNowDate(false));
		ct.setUniqueModifyComments("Hidden CT");
		ct.setAlertStatusCode(55);
		ct.setCommTypeID(55);
		
		try {
			ct.commitData();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	/**
	 * Tests a string for any match of values in given array
	 * @param testString
	 * @param array
	 * @return
	 */
	public static boolean arrayContains(String testString, List array){
		boolean hasString = false;
		if(testString!=null && array!=null){
			for(Object obj : array){
				if(testString.toString().contains(obj.toString())){
					hasString = true;
					break;
				}
			}
		}
		return hasString;
	}
	
	public static boolean arrayContainsForInteter(Integer testString, List array){
		boolean hasString = false;
		if(testString!=null && array!=null){
			for(Object obj : array){
				if(testString.toString().contains(obj.toString())){
					hasString = true;
					break;
				}
			}
		}
		return hasString;
	}
	
}
