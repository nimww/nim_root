package com.winstaff;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class SalesPortal {
	public final static int BarChartDataLabel = 1;
	public final static int BarChartDataData = 2;
	public final static int BarChartDataData2 = 3;
	public final static int BarChartDataData3 = 4;
	public final static int OpenCases = 5;
	public final static int HistoryTrack = 6;
	
	/**
	 * Queries the Database to list either active cases or cases with a DOS 2 months ago from tracked users in sale's portal table. 
	 * @param userId Current user logged in
	 * @param type accepts only static int 'OpenCases' or 'HistoryTrack'
	 * @return list of OpenCases.class
	 */
	public static List<OpenCases> getListCases(int userId,int type){
		List<OpenCases> opencases = new ArrayList<OpenCases>();
		List<Integer> trackID = new ArrayList<Integer>();
		searchDB2 conn = new searchDB2();
		String query = "select trackingid from tnim3_salesportal where userid = "+userId;
		ResultSet rs = conn.executeStatement(query);
		String allTrack = "";
		boolean isFirst = true;
		SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");
		try {
			while(rs.next()){
				if(isFirst){
					allTrack += "('"+rs.getString("trackingid")+"'";
					trackID.add(rs.getInt("trackingid"));
					isFirst = false;
				} else {
					allTrack += ",'"+rs.getString("trackingid")+"'";
					trackID.add(rs.getInt("trackingid"));
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally{
			conn.closeAll();
			allTrack +=")";
		}
		if(type==OpenCases){
			query = "select encounterid from tnim3_encounter join tnim3_referral on tnim3_referral.referralid = tnim3_encounter.referralid join tnim3_caseaccount on tnim3_caseaccount.caseid = tnim3_referral.caseid where " + 
					"(adjusterid in "+allTrack+" or nursecasemanagerid in "+allTrack+" or tnim3_caseaccount.referredbycontactid in "+allTrack+" or caseadministratorid in "+allTrack+" or caseadministrator2id in "+allTrack+" or caseadministrator3id in "+allTrack+" or caseadministrator4id in "+allTrack+") and encounterstatusid = 1";
		} else if(type==HistoryTrack){
			query = "select encounterid from tnim3_encounter join tnim3_referral on tnim3_referral.referralid = tnim3_encounter.referralid join tnim3_caseaccount on tnim3_caseaccount.caseid = tnim3_referral.caseid where " + 
					"(adjusterid in "+allTrack+" or nursecasemanagerid in "+allTrack+" or tnim3_caseaccount.referredbycontactid in "+allTrack+" or caseadministratorid in "+allTrack+" or caseadministrator2id in "+allTrack+" or caseadministrator3id in "+allTrack+" or caseadministrator4id in "+allTrack+") and encounterstatusid != 5 and receivedate > now() - '2 month'::INTERVAL";
		}
		
		rs = conn.executeStatement(query);
		
		try {
			while(rs.next()){
				NIM3_EncounterObject2 eo2 = new NIM3_EncounterObject2(rs.getInt("encounterid"),"");
				opencases.add(new OpenCases( eo2.getNIM3_CaseAccount().getPatientFirstName()+" "+eo2.getNIM3_CaseAccount().getPatientLastName(), getTrackName(trackID,eo2),  eo2.getPayerFullName(),  df.format(eo2.getNIM3_Referral().getReceiveDate()),  getUserName(eo2.getNIM3_CaseAccount().getAssignedToID()),  eo2.getScanPass(),df.format(eo2.getNIM3_Encounter().getDateOfService())));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally{
			conn.closeAll();
		}
		
		return opencases;
	}
	/**
	 * Supports getListCases method, provides data on who is associated to a case
	 * @param trackID user in question
	 * @param eo2 case encounter object in question
	 * @return
	 */
	private static String getTrackName(List<Integer> trackID,NIM3_EncounterObject2 eo2){
		if(trackID.contains(eo2.getNIM3_CaseAccount().getAdjusterID())){
			return getUserName(eo2.getNIM3_CaseAccount().getAdjusterID());
		} else if(trackID.contains(eo2.getNIM3_CaseAccount().getNurseCaseManagerID())){
			return getUserName(eo2.getNIM3_CaseAccount().getNurseCaseManagerID());
		} else if(trackID.contains(eo2.getNIM3_CaseAccount().getCaseAdministratorID())){
			return getUserName(eo2.getNIM3_CaseAccount().getCaseAdministratorID());
		} else if(trackID.contains(eo2.getNIM3_CaseAccount().getCaseAdministrator2ID())){
			return getUserName(eo2.getNIM3_CaseAccount().getCaseAdministrator2ID());
		} else if(trackID.contains(eo2.getNIM3_CaseAccount().getCaseAdministrator3ID())){
			return getUserName(eo2.getNIM3_CaseAccount().getCaseAdministrator3ID());
		} else if(trackID.contains(eo2.getNIM3_CaseAccount().getCaseAdministrator4ID())){
			return getUserName(eo2.getNIM3_CaseAccount().getCaseAdministrator4ID());
		} else if(trackID.contains(eo2.getNIM3_CaseAccount().getReferredByContactID())){
			return getUserName(eo2.getNIM3_CaseAccount().getReferredByContactID());
		} 

		return "--";
		
	}
	/**
	 * Provides data for open referral chart on clientTracker.jsp
	 * @param userId current user logged in
	 * @return Data of tracked clients for user
	 */
	public static List<BarChartData> getOpenReferrals(int userId){
		List<BarChartData> barchartdata = new ArrayList<BarChartData>();
		String query = "select trackingid from tnim3_salesportal where userid = "+userId;
		searchDB2 conn = new searchDB2();
		ResultSet rs = conn.executeStatement(query);
		
		try {
			while(rs.next()){
				barchartdata.add(new BarChartData(getUserName(rs.getInt("trackingid")),getOpenReferralsCount(rs.getInt("trackingid"))));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally{
			conn.closeAll();
		}
		
		return barchartdata;
	}
	/**
	 * Translate list data for uses of javascript plug-in on clienTracker.jsp
	 * @param barchartdata data to parse
	 * @param type accepts only static int 'BarChartDataLabel' or 'BarChartDataData';
	 * @return Formated string charts.js plug-in
	 */
	public static String parseBarChartData(List<BarChartData> barchartdata, int type){
		boolean isFirst = true;
		String parse = "";
		if(type == BarChartDataLabel){
			for(BarChartData b : barchartdata){
				if(isFirst) {
					parse +="\""+b.getLabel()+"\"";
					isFirst = false;
				} else
				parse +=",\""+b.getLabel()+"\"";
			}
		} else if (type==BarChartDataData){
			for(BarChartData b : barchartdata){
				if(isFirst) {
					parse +=b.getData();
					isFirst = false;
				} else
				parse +=","+b.getData();
			}
		}
		return parse;
	}
	/**
	 * Translate list data for uses of javascript plug-in on clienTracker.jsp
	 * @param barchartdata data to parse
	 * @param type accepts only static int 'BarChartDataLabel' or 'BarChartDataData' or 'BarChartDataData2' or 'BarChartDataData3';
	 * @return Formated string charts.js plug-in
	 */
	public static String parseBarChartData2(List<BarChartData2> barchartdata, int type){
		boolean isFirst = true;
		String parse = "";
		if(type == BarChartDataLabel){
			for(BarChartData2 b : barchartdata){
				if(isFirst) {
					parse +="\""+b.getLabel()+"\"";
					isFirst = false;
				} else
				parse +=",\""+b.getLabel()+"\"";
			}
		} else if (type==BarChartDataData){
			for(BarChartData2 b : barchartdata){
				if(isFirst) {
					parse +=b.getData();
					isFirst = false;
				} else
				parse +=","+b.getData();
			}
		} else if (type==BarChartDataData2){
			for(BarChartData2 b : barchartdata){
				if(isFirst) {
					parse +=b.getData2();
					isFirst = false;
				} else
				parse +=","+b.getData2();
			}
		} else if (type==BarChartDataData3){
			for(BarChartData2 b : barchartdata){
				if(isFirst) {
					parse +=b.getData3();
					isFirst = false;
				} else
				parse +=","+b.getData3();
			}
		}
		return parse;
	}
	/**
	 * Test method
	 * @return
	 */
	public static String test(){
		int trackingId = 3402;
		String query = "select count(*) \"count\" from tnim3_encounter join tnim3_referral on tnim3_referral.referralid = tnim3_encounter.referralid join tnim3_caseaccount on tnim3_caseaccount.caseid = tnim3_referral.caseid where " + 
		"(adjusterid = "+trackingId+" or nursecasemanagerid = "+trackingId+" or tnim3_caseaccount.referredbycontactid = "+trackingId+" or caseadministratorid = "+trackingId+" or caseadministrator2id = "+trackingId+" or caseadministrator3id = "+trackingId+" or caseadministrator4id = "+trackingId+") and encounterstatusid = 1";
//		searchDB2 conn = new searchDB2();
//		ResultSet rs = conn.executeStatement(query);
//		
//		try {
//			while(rs.next()){
//				count = rs.getInt("count");
//			}
//		} catch (SQLException e) {
//			e.printStackTrace();
//		} finally{
//			conn.closeAll();
//		}
		
		return query;
	}
	/**
	 * Provides data for volume count chart on clientTracker.jsp
	 * @param userId current user logged in
	 * @return Data of tracked clients for user
	 */
	public static List<BarChartData2> getVolume(int userId){
		List<BarChartData2> barchartdata = new ArrayList<BarChartData2>();
		String query = "select trackingid from tnim3_salesportal where userid = "+userId;
		searchDB2 conn = new searchDB2();
		ResultSet rs = conn.executeStatement(query);
		
		try {
			while(rs.next()){
				List<Integer> count = getVolumeCount(rs.getInt("trackingid"));
				barchartdata.add(new BarChartData2(getUserName(rs.getInt("trackingid")),count.get(0),count.get(1),count.get(2)));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally{
			conn.closeAll();
		}
		
		return barchartdata;
	}
	/**
	 * Supports getVolumeCount method, provides the meat of the data
	 * @param trackID user in question
	 * @return
	 */
	private static List<Integer> getVolumeCount(int trackingId){
		List<Integer> count = new ArrayList<Integer>();
		String query = "select (select count(*) from tnim3_encounter join tnim3_referral on tnim3_referral.referralid = tnim3_encounter.referralid join tnim3_caseaccount on tnim3_caseaccount.caseid = tnim3_referral.caseid where "+
		"(adjusterid = "+trackingId+" or nursecasemanagerid = "+trackingId+" or tnim3_caseaccount.referredbycontactid = "+trackingId+" or caseadministratorid = "+trackingId+" or caseadministrator2id = "+trackingId+" or caseadministrator3id = "+trackingId+" or caseadministrator4id = "+trackingId+") and encounterstatusid != 5 and receivedate > now() - '1 month'::INTERVAL) one, (select count(*) from tnim3_encounter join tnim3_referral on tnim3_referral.referralid = tnim3_encounter.referralid join tnim3_caseaccount on tnim3_caseaccount.caseid = tnim3_referral.caseid where "+
		"(adjusterid = "+trackingId+" or nursecasemanagerid = "+trackingId+" or tnim3_caseaccount.referredbycontactid = "+trackingId+" or caseadministratorid = "+trackingId+" or caseadministrator2id = "+trackingId+" or caseadministrator3id = "+trackingId+" or caseadministrator4id = "+trackingId+") and encounterstatusid != 5 and receivedate > now() - '2 month'::INTERVAL) two, (select count(*) from tnim3_encounter join tnim3_referral on tnim3_referral.referralid = tnim3_encounter.referralid join tnim3_caseaccount on tnim3_caseaccount.caseid = tnim3_referral.caseid where "+
		"(adjusterid = "+trackingId+" or nursecasemanagerid = "+trackingId+" or tnim3_caseaccount.referredbycontactid = "+trackingId+" or caseadministratorid = "+trackingId+" or caseadministrator2id = "+trackingId+" or caseadministrator3id = "+trackingId+" or caseadministrator4id = "+trackingId+") and encounterstatusid != 5 and receivedate > now() - '3 month'::INTERVAL) three";
		searchDB2 conn = new searchDB2();
		ResultSet rs = conn.executeStatement(query);
		
		try {
			while(rs.next()){
				count.add(rs.getInt("one"));
				count.add(rs.getInt("two"));
				count.add(rs.getInt("three"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally{
			conn.closeAll();
		}
		
		return count;
	}
	/**
	 * Supports getOpenReferrals method, provides the meat of the data
	 * @param trackID user in question
	 * @return
	 */
	private static int getOpenReferralsCount(int trackingId){
		int count = 0;
		String query = "select count(*) \"count\" from tnim3_encounter join tnim3_referral on tnim3_referral.referralid = tnim3_encounter.referralid join tnim3_caseaccount on tnim3_caseaccount.caseid = tnim3_referral.caseid where " + 
		"(adjusterid = "+trackingId+" or nursecasemanagerid = "+trackingId+" or tnim3_caseaccount.referredbycontactid = "+trackingId+" or caseadministratorid = "+trackingId+" or caseadministrator2id = "+trackingId+" or caseadministrator3id = "+trackingId+" or caseadministrator4id = "+trackingId+") and encounterstatusid = 1";
		searchDB2 conn = new searchDB2();
		ResultSet rs = conn.executeStatement(query);
		
		try {
			while(rs.next()){
				count = rs.getInt("count");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally{
			conn.closeAll();
		}
		
		return count;
	}
	/**
	 * Translate userIDs into First and Last names
	 * @param userid
	 * @return
	 */
	private static String getUserName(int userid){
		String query = "select contactfirstname||' '||contactlastname \"name\" from tuseraccount where userid = "+userid;
		searchDB2 conn = new searchDB2();
		ResultSet rs = conn.executeStatement(query);
		
		try {
			while(rs.next()){
				return rs.getString("name");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally{
			conn.closeAll();
		}
		return "Unknown";
	}
	/**
	 * Model of cases for clientTracker.jsp
	 * @author Po Le
	 *
	 */
	public static class  OpenCases{
		private String patientName;
		private String trackName;
		private String carrier;
		private String receiveDate;
		private String scheduler;
		private String scanpass;
		private String dateofservice;
		public OpenCases(String patientName, String trackName, String carrier, String receiveDate, String scheduler, String scanpass, String dateofservice) {
			this.patientName = patientName;
			this.trackName = trackName;
			this.carrier = carrier;
			this.receiveDate = receiveDate;
			this.scheduler = scheduler;
			this.scanpass = scanpass;
			this.dateofservice = dateofservice;
		}
		public String getPatientName() {
			return patientName;
		}
		public void setPatientName(String patientName) {
			this.patientName = patientName;
		}
		public String getTrackName() {
			return trackName;
		}
		public void setTrackName(String trackName) {
			this.trackName = trackName;
		}
		public String getCarrier() {
			return carrier;
		}
		public void setCarrier(String carrier) {
			this.carrier = carrier;
		}
		public String getReceiveDate() {
			return receiveDate;
		}
		public void setReceiveDate(String receiveDate) {
			this.receiveDate = receiveDate;
		}
		public String getScheduler() {
			return scheduler;
		}
		public void setScheduler(String scheduler) {
			this.scheduler = scheduler;
		}
		public String getScanpass() {
			return scanpass;
		}
		public void setScanpass(String scanpass) {
			this.scanpass = scanpass;
		}
		public String getDateofservice() {
			return dateofservice;
		}
		public void setDateofservice(String dateofservice) {
			this.dateofservice = dateofservice;
		}
		
	}
	/**
	 * For Charts.js on clientTracker.jsp
	 * @author Po Le
	 *
	 */
	public static class  BarChartData{
		private String label;
		private int data;
		public BarChartData(String label, int data) {
			this.label = label;
			this.data = data;
		}
		public String getLabel() {
			return label;
		}
		public void setLabel(String label) {
			this.label = label;
		}
		public int getData() {
			return data;
		}
		public void setData(int data) {
			this.data = data;
		}
		
	}
	/**
	 * For Charts.js on clientTracker.jsp
	 * @author Po Le
	 *
	 */
	public static class BarChartData2 extends BarChartData{
		private int data2;
		private int data3;
		public BarChartData2(String label, int data, int data2, int data3) {
			super(label, data);
			this.data2 = data2;
			this.data3 = data3;
		}
		public int getData2() {
			return data2;
		}
		public void setData2(int data2) {
			this.data2 = data2;
		}
		public int getData3() {
			return data3;
		}
		public void setData3(int data3) {
			this.data3 = data3;
		}
		
	}
	
}
