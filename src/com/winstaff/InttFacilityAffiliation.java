package com.winstaff;


import com.winstaff.dbtFacilityAffiliation;
import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public interface InttFacilityAffiliation extends dbTableInterface
{

    public void setAffiliationID(Integer newValue);
    public Integer getAffiliationID();
    public void setUniqueCreateDate(Date newValue);
    public Date getUniqueCreateDate();
    public void setUniqueModifyDate(Date newValue);
    public Date getUniqueModifyDate();
    public void setUniqueModifyComments(String newValue);
    public String getUniqueModifyComments();
    public void setPhysicianID(Integer newValue);
    public Integer getPhysicianID();
    public void setIsPrimary(Integer newValue);
    public Integer getIsPrimary();
    public void setAppointmentLevel(String newValue);
    public String getAppointmentLevel();
    public void setStartDate(Date newValue);
    public Date getStartDate();
    public void setEndDate(Date newValue);
    public Date getEndDate();
    public void setPendingDate(Date newValue);
    public Date getPendingDate();
    public void setFacilityName(String newValue);
    public String getFacilityName();
    public void setFacilityDepartment(String newValue);
    public String getFacilityDepartment();
    public void setFacilityAddress1(String newValue);
    public String getFacilityAddress1();
    public void setFacilityAddress2(String newValue);
    public String getFacilityAddress2();
    public void setFacilityCity(String newValue);
    public String getFacilityCity();
    public void setFacilityStateID(Integer newValue);
    public Integer getFacilityStateID();
    public void setFacilityProvince(String newValue);
    public String getFacilityProvince();
    public void setFacilityZIP(String newValue);
    public String getFacilityZIP();
    public void setFacilityCountryID(Integer newValue);
    public Integer getFacilityCountryID();
    public void setFacilityPhone(String newValue);
    public String getFacilityPhone();
    public void setFacilityFax(String newValue);
    public String getFacilityFax();
    public void setContactName(String newValue);
    public String getContactName();
    public void setContactEmail(String newValue);
    public String getContactEmail();
    public void setReasonForLeaving(String newValue);
    public String getReasonForLeaving();
    public void setAdmissionPriviledges(Integer newValue);
    public Integer getAdmissionPriviledges();
    public void setAdmissionArrangements(String newValue);
    public String getAdmissionArrangements();
    public void setUnrestrictedAdmission(Integer newValue);
    public Integer getUnrestrictedAdmission();
    public void setTempPriviledges(Integer newValue);
    public Integer getTempPriviledges();
    public void setInpatientCare(Integer newValue);
    public Integer getInpatientCare();
    public void setPercentAdmissions(String newValue);
    public String getPercentAdmissions();
    public void setComments(String newValue);
    public String getComments();
}    // End of bltFacilityAffiliation class definition
