
package com.winstaff;
/*
 * bltNIM3_CommReference_List.java
 *
 * Created: Tue Jan 12 16:24:40 PST 2010
 */



import java.sql.*;




/**
 *
 * @author  Scott Ellis via listClassGenCode
 * @version 2.5
 */
public class bltNIM3_CommReference_List extends db_BaseRelationTable
{
    /** Default constructor
    */    
    public bltNIM3_CommReference_List ( Integer iCRID )
    {
        super( "tNIM3_CommReference", "CommReferenceID", "CommReferenceID", "CRID", iCRID );
    }   // End of bltNIM3_CommReference_List()
    

    public void setField(String fieldN, Object fieldV)
    {DebugLogger.println("setField Method not implemented yet in classbltNIM3_CommReference_List!");}

    public String getFieldType(String fieldN)
    {return null;}

    public bltNIM3_CommReference_List ( Integer iCRID, String extraWhere, String OrderBy )
    {
        super( "tNIM3_CommReference", "CommReferenceID", "CommReferenceID", "CRID", iCRID, extraWhere,OrderBy );
    }   // End of bltNIM3_CommReference_List()
     /** Method pull information from the result set and load into class attributes.
    * @param sFieldList Comma delimited list of fields for which data has been retrieved.
    * @param rs Result set from which to pull the data from.
    * @throws SQLException Passes on SQLException
    */      
    protected void transferData( ResultSet rs ) throws SQLException
    {
        while ( rs.next()  )
        {
            Integer         iValue              = new Integer( rs.getString( sLinkFieldName ) );
            Integer         iLinkRecordID       = new Integer( rs.getString( sUniqueIDFieldName ) );
            Object          oReferenceObject;
            ListElement     leNewElement;
            
            oReferenceObject    = new bltNIM3_CommReference( iValue );
            
            leNewElement        = new ListElement( iLinkRecordID, oReferenceObject );
            
            vList.add( leNewElement );

        }
    }   // End of transferData()
}   // End of bltNIM3_CommReference_List class

