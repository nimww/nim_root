
package com.winstaff;
/*
 * bltSchoolLI_List.java
 *
 * Created: Tue Nov 23 11:32:47 PST 2004
 */



import java.sql.*;




/**
 *
 * @author  Scott Ellis via listClassGenCode
 * @version 2.5
 */
public class bltSchoolLI_List extends db_BaseRelationTable
{
    /** Default constructor
    */    
    public bltSchoolLI_List ( Integer iSecurityGroupID )
    {
        super( "tSchoolLI", "SchoolID", "SchoolID", "SecurityGroupID", iSecurityGroupID );
    }   // End of bltSchoolLI_List()
    

    public void setField(String fieldN, Object fieldV)
    {DebugLogger.println("setField Method not implemented yet in classbltSchoolLI_List!");}

    public String getFieldType(String fieldN)
    {return null;}

    public bltSchoolLI_List ( Integer iSecurityGroupID, String extraWhere, String OrderBy )
    {
        super( "tSchoolLI", "SchoolID", "SchoolID", "SecurityGroupID", iSecurityGroupID, extraWhere,OrderBy );
    }   // End of bltSchoolLI_List()
     /** Method pull information from the result set and load into class attributes.
    * @param sFieldList Comma delimited list of fields for which data has been retrieved.
    * @param rs Result set from which to pull the data from.
    * @throws SQLException Passes on SQLException
    */      
    protected void transferData( ResultSet rs ) throws SQLException
    {
        while ( rs.next()  )
        {
            Integer         iValue              = new Integer( rs.getString( sLinkFieldName ) );
            Integer         iLinkRecordID       = new Integer( rs.getString( sUniqueIDFieldName ) );
            Object          oReferenceObject;
            ListElement     leNewElement;
            
            oReferenceObject    = new bltSchoolLI( iValue );
            
            leNewElement        = new ListElement( iLinkRecordID, oReferenceObject );
            
            vList.add( leNewElement );

        }
    }   // End of transferData()
}   // End of bltSchoolLI_List class

