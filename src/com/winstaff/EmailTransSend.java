/*
	A basic Java class stub for a Win32 Console Application.
 */
package com.winstaff;
import com.winstaff.*;

public class EmailTransSend  extends Thread
{

	public EmailTransSend () 
	{

	}
	
    static public void main(String args[]) 
    {
        EmailTransSend testMe = new EmailTransSend();
        while (true)
        {
            try
            {
		        int mySleep = 300;
	            System.out.println(">>> Running Email Send " + new java.util.Date());
                testMe.runMe();
    	        System.out.println(">>> sleeping for " + Math.round(mySleep/60) + " Minutes...");
                testMe.sleep(mySleep * 1000);
            }
            catch (Exception e)
            {
                System.out.println("Error in Thread: "+ e );
            }
        }
    }	
	
	

    public static void runMe()
    {
        String batchGroup = "1";
            System.out.println("==============================================================================");

        java.text.SimpleDateFormat displayDateSDF1Full = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1Full);
        System.out.println("Starting Batch Send [Batch Group:"+batchGroup+"] at " + displayDateSDF1Full.format(new java.util.Date()));
        searchDB2 mySDBA = new searchDB2();
        try
        {
            String mySQLA = "select top " + batchGroup + " * from tEmailTransaction where ActionID=0";
            mySQLA = "select * from tEmailTransaction where ActionID=0 LIMIT " + batchGroup + "";
            java.sql.ResultSet myRSA = mySDBA.executeStatement(mySQLA);
            int totalCnt=0;
            int successCnt=0;
            while (myRSA.next())
            {
                System.out.println("Sending");
                totalCnt++;
                bltEmailTransaction myET = new bltEmailTransaction(new Integer(myRSA.getString("emailtransactionid")));
                emailType_V3 myETSend = new emailType_V3();
                myETSend.setTo(myET.getEmailTo());
                myETSend.setFrom(myET.getEmailFrom());
                myETSend.setSubject(myET.getEmailSubject());
                myETSend.setBody(myET.getEmailBody());
                myETSend.setBodyType(myET.getEmailBodyType());
                //System.out.println("Body Type: " + myETSend.getBodyType());
                if (myETSend.isSendMail_Direct2())
                {
                    myET.setActionID(new Integer(1));
                    myET.setUniqueModifyComments("WS EmailTransSend V2");
                    myET.setComments("Successful Send on ["+displayDateSDF1Full.format(new java.util.Date())+"]");
                    successCnt++;
                }
                else
                {
                    myET.setActionID(new Integer(2));
                    myET.setUniqueModifyComments("WS EmailTransSend V2");
                    myET.setComments("Failed on ["+displayDateSDF1Full.format(new java.util.Date())+"] - ["+myETSend.getMessage()+"]");
                }
                myET.commitData();
                System.out.println("Done Saving");
            }
	        mySDBA.closeAll();
            System.out.println("==============================================================================");
            System.out.println("Total Attempted: " + totalCnt);
            System.out.println("Total Sent: " + successCnt);
            System.out.println("Total Failed: " + (totalCnt-successCnt));
            System.out.println("==============================================================================");
        }
        catch(Exception e)
        {
            System.out.println(e);
        } finally {
            mySDBA.closeAll();
        }
        
    }

	//{{DECLARE_CONTROLS
	//}}
}

