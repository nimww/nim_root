

package com.winstaff;

import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public class dbtFieldSecurity extends Object implements InttFieldSecurity
{

        db_NewBase    dbnbDB;

    public dbtFieldSecurity()
    {
        dbnbDB = new db_NewBase( "tFieldSecurity", "FieldSecurityID" );

    }    // End of default constructor

    public dbtFieldSecurity( Integer iNewID )
    {
        dbnbDB = new db_NewBase( "tFieldSecurity", "FieldSecurityID" );
        setUniqueID( iNewID );
    }    // End of Constructor knowing an ID

    public void commitData() throws SQLException
    {
            //no longer trapping/ignoring errors 2011-03-17
            dbnbDB.commitData();
    }    // End of commitData()

    public void commitDataForced() throws SQLException
    {
        dbnbDB.commitDataForced();
    }    // End of commitData()

    public Integer getUniqueID()
    {
        return dbnbDB.getUniqueID();
    }    // End of getUniqueID()

    public void setUniqueID( Integer iNewID )
    {
        dbnbDB.setUniqueID( iNewID );
    }    // End of setUniqueID()

    public void setModifyComments( String sNew )
    {
        dbnbDB.setModifyComments( sNew );
    }    // End of setUniqueID()


    public void setFieldSecurityID(Integer newValue)
    {
                dbnbDB.setFieldData( "FieldSecurityID", newValue.toString() );
    }

    public Integer getFieldSecurityID()
    {
        String           sValue = dbnbDB.getFieldData( "FieldSecurityID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setField(String fieldN, Object fieldV)
    {DebugLogger.println("setField Method not implemented yet in classFieldSecurity!");}

    public String getFieldType(String fieldN)
    {return null;}


    public void setUniqueCreateDate(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "UniqueCreateDate", formatter.format( newValue ) );
    }

    public Date getUniqueCreateDate()
    {
        String           sValue = dbnbDB.getFieldData( "UniqueCreateDate" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setUniqueModifyDate(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "UniqueModifyDate", formatter.format( newValue ) );
    }

    public Date getUniqueModifyDate()
    {
        String           sValue = dbnbDB.getFieldData( "UniqueModifyDate" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setUniqueModifyComments(String newValue)
    {
                dbnbDB.setFieldData( "UniqueModifyComments", newValue.toString() );
    }

    public String getUniqueModifyComments()
    {
        String           sValue = dbnbDB.getFieldData( "UniqueModifyComments" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setSecurityGroupID(Integer newValue)
    {
                dbnbDB.setFieldData( "SecurityGroupID", newValue.toString() );
    }

    public Integer getSecurityGroupID()
    {
        String           sValue = dbnbDB.getFieldData( "SecurityGroupID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setTableName(String newValue)
    {
                dbnbDB.setFieldData( "TableName", newValue.toString() );
    }

    public String getTableName()
    {
        String           sValue = dbnbDB.getFieldData( "TableName" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setFieldName(String newValue)
    {
                dbnbDB.setFieldData( "FieldName", newValue.toString() );
    }

    public String getFieldName()
    {
        String           sValue = dbnbDB.getFieldData( "FieldName" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setRequiredID(Integer newValue)
    {
                dbnbDB.setFieldData( "RequiredID", newValue.toString() );
    }

    public Integer getRequiredID()
    {
        String           sValue = dbnbDB.getFieldData( "RequiredID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setTrackedID(Integer newValue)
    {
                dbnbDB.setFieldData( "TrackedID", newValue.toString() );
    }

    public Integer getTrackedID()
    {
        String           sValue = dbnbDB.getFieldData( "TrackedID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setAccessLevel(Integer newValue)
    {
                dbnbDB.setFieldData( "AccessLevel", newValue.toString() );
    }

    public Integer getAccessLevel()
    {
        String           sValue = dbnbDB.getFieldData( "AccessLevel" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

}    // End of bltFieldSecurity class definition
