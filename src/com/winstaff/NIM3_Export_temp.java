package com.winstaff;
import java.io.DataOutputStream;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Vector;

/**
 * Created by IntelliJ IDEA.
 * User: ste
 * Date: 10/3/11
 * Time: 5:17 PM
 * To change this template use File | Settings | File Templates.
 */
public class NIM3_Export_temp {


    static public void main(String args[])
    {
        NIM3_Export_temp testMe = new NIM3_Export_temp();
        try
        {
            testMe.runMe_Temp();
        }
        catch (Exception e)
        {
            System.out.println("Error in Thread: "+ e );
        }
    }



    public static void runMe_Temp()
    {
        String CS_Version = "Encounter 1.0";
        searchDB2 mySDBA = new searchDB2();
        try
        {
            System.out.println("==============================================================================");
            System.out.println("Running Cost Savings Export Version " + CS_Version);
            java.text.SimpleDateFormat displayDateSDF1Full = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1Full);
            System.out.println("Start: " + displayDateSDF1Full.format(PLCUtils.getNowDate(true)));

            String mySQLA = "SELECT\n" +
                    "\"public\".\"vMQ4_Encounter_NoVoid_BP\".\"CaseID\",\n" +
                    "\"public\".tnim3_caseaccount.patientssn\n" +
                    "FROM\n" +
                    "\"public\".\"vMQ4_Encounter_NoVoid_BP\"\n" +
                    "INNER JOIN \"public\".tnim3_caseaccount ON \"public\".tnim3_caseaccount.caseid = \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"CaseID\"\n" +
                    "where \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Parent_PayerID\" = '508'";
            java.sql.ResultSet myRSA = mySDBA.executeStatement(mySQLA);
            int totalCnt=0;
            int successCnt=0;
            java.util.Vector<String> myOut = new Vector<String>();
            String myHeader = ("" +
                    "caseid\t" +
                    "SSNe\t" +
                    "SSN\t" +
                    "");


            System.out.println("");
            System.out.println("Writing File");

            SimpleDateFormat    formatter1              = new SimpleDateFormat( "MM_dd_yyyy_hh_mm" );
            SimpleDateFormat formatter2              = new SimpleDateFormat( "yyyy-MM-dd_HH:mm" );
            String              newDateFileNameString   = formatter1.format( PLCUtils.getNowDate(true) );
            String sFileName = "export/Export_temp" + newDateFileNameString + ".txt";
            DataOutputStream    out;
            out = new DataOutputStream( new FileOutputStream( sFileName ) );
            out.writeBytes( myHeader );
            out.writeBytes( "\n" );



            while (myRSA.next())
            {
                bltNIM3_CaseAccount myCA = new bltNIM3_CaseAccount(new Integer(myRSA.getString("CaseID")));
                String tempRow = "" +
                        myCA.getCaseID() + "\t" +
                        //myRSA.getString("patientssn") + "\t" +
                        //myCA.getPatientSSN() + "\t" +
                        "";

                out.writeBytes( tempRow );
                out.writeBytes( "\n" );
                totalCnt++;
            }
            mySDBA.closeAll();
            out.flush();
            System.out.println("");
            System.out.println("Finish: " + displayDateSDF1Full.format(PLCUtils.getNowDate(true)));
            System.out.println("==============================================================================");
        }
        catch(Exception e)
        {
            System.out.println(e);
        }  finally {
            mySDBA.closeAll();
        }

    }








}
