package com.winstaff;

import java.io.File;
import java.util.Vector;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 * Created by IntelliJ IDEA.
 * User: ste
 * Date: 6/28/11
 * Time: 12:42 PM
 * To change this template use File | Settings | File Templates.
 */
public class EncounterToXML {

    public EncounterToXML(Vector<NIM3_EncounterObject2> myVectorOfEO2) {
        this.myVectorOfEO2 = myVectorOfEO2;
    }

    public EncounterToXML(NIM3_EncounterObject2 myEO2) {
        this.myVectorOfEO2 = new Vector<NIM3_EncounterObject2>();
        this.myVectorOfEO2.add(myEO2);
    }


    Vector<NIM3_EncounterObject2> myVectorOfEO2 = null;

    public Vector<NIM3_EncounterObject2> getMyVectorOfEO2() {
        return myVectorOfEO2;
    }

    public void setMyVectorOfEO2(Vector<NIM3_EncounterObject2> myVectorOfEO2) {
        this.myVectorOfEO2 = myVectorOfEO2;
    }


    public void WriteToXML(String theFileName){
        NIM3_EncounterObject2 myEO2 = this.myVectorOfEO2.elementAt(0);
        try{

            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

            //root elements
            Document doc = docBuilder.newDocument();
            Element caseElement = doc.createElement("case");
            doc.appendChild(caseElement);

            //set attribute to case
            caseElement.setAttribute("claimnumber",myEO2.getNIM3_CaseAccount().getCaseClaimNumber());

            //firstname elements
            Element firstname = doc.createElement("firstname");
            firstname.appendChild(doc.createTextNode(myEO2.getNIM3_CaseAccount().getPatientFirstName()));
            caseElement.appendChild(firstname);

            //lastname elements
            Element lastname = doc.createElement("lastname");
            lastname.appendChild(doc.createTextNode(myEO2.getNIM3_CaseAccount().getPatientLastName()));
            caseElement.appendChild(lastname);


            //write the content into xml file
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource source = new DOMSource(doc);
            StreamResult result =  new StreamResult(theFileName);
            transformer.transform(source, result);

            System.out.println("Done");

        }catch(ParserConfigurationException pce){
            pce.printStackTrace();
        }catch(TransformerException tfe){
            tfe.printStackTrace();
        }
    }
    public static void main(String argv[]) {
        EncounterToXML myETX = new EncounterToXML(new NIM3_EncounterObject2(342,"xml load test"));
        myETX.WriteToXML("/var/lib/tomcat6/webapps/edi/ips/outbound/test.xml");
    }
}
