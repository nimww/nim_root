package com.winstaff;
/*
 * db_BaseLookupTable.java
 *
 * This base class used to build lookup table classes.
 *
 * Created on March 30, 2001, 1:38 PM
 */
import java.sql.*;
import java.util.Vector;
import java.util.Enumeration;

/**
 *
 * @author      Michael Million
 * @version     1.00
 */
public abstract class db_BaseLookupTable
{
    // Attribute information
    protected Vector          vList             =       new Vector();
    
    // Identifier, sometimes unique, to retrieve records.
    private Integer         iUniqueID;
    private String          sTableName;
    private String          sKeyFieldName;
    
//    private String          sRelationKeyFieldName;
//    private Integer         iRelationUniqueID;

    // Class attribute stuff
    private String          sClassIdentifier;
    
    // Database stuff
    private Connection      cCon;

    /** Constructs new db_BaseLookupTable
     * @param sTableName String Name of table to set/get information from
     * @param sKeyFieldName String Comma delimited list of fields to set/get
    */
    public db_BaseLookupTable (String sTableName, String sKeyFieldName)
    {
        this.sTableName             = sTableName;
        this.sKeyFieldName          = sKeyFieldName;
        
        // Initialize and load data
        init();
    }   // End of default constructor

    private void init()
    {
        String  sDebugString = "";
        
        this.sClassIdentifier =  this.getClass().getName();
        
        try
        {
            sDebugString = ".forName";
            Class.forName( ConfigurationInformation.sJDBCDriverName );
        }
        catch ( Exception e )
        {
            DebugLogger.println ( sClassIdentifier + ": in db_Base Constuctor Error: " + e.toString () + "\n" + sDebugString );
        }
        
        loadFromDatabase();
    }    
    
    /** Return enumeration of projects
    * @return Enumeration Enumeration of Projects
    */    
    public Enumeration elements()
    {
        return new VectorEnumeration( vList );
    }   // End of elements()
    
    /** Method to be defined for each child class.  It is used to initialize and invoke the SQL query.
    */    
    protected final void loadFromDatabase ()
    {
        String  sDebugString = "";
        String  sSQLString;
        
        try
        {
            sDebugString = ".getConnection";
            cCon = DriverManager.getConnection( ConfigurationInformation.sURL );
            
            sDebugString = ".createStatement";
            Statement   stmt = cCon.createStatement();
            
            sDebugString = ".executeQuery";
            sSQLString = "select " + sKeyFieldName + " from " + sTableName;

            if ( ConfigurationInformation.bInDebugMode )
            {
                DebugLogger.println( "Query: " + sSQLString );
            }
            
            ResultSet rs = stmt.executeQuery( sSQLString );
             
            // Via polymorphism, allow class to load member attributes.  Note this could be a list of records.
            transferData( rs );

            sDebugString = "resultset close";
            rs.close();
            
            sDebugString = "statement close";
            stmt.close();
            
            sDebugString = "connection close";
            cCon.close ();
        }
        catch( Exception e )
        {
            DebugLogger.println(  sClassIdentifier + ": " + e.toString() + "\n" + sDebugString );
        }
        finally
        {
    	    try
            {
    	        cCon.close();
            }
            catch (SQLException SQLEx)
            {
                DebugLogger.println(  sClassIdentifier + ": Finally " + SQLEx.toString() + "\n" + sDebugString );
    	    }
        }
    }   // End of loadFromDatabase()

    /*
    * Abstract method to pull information from the result set and load into class attributes.
    * @param sFieldList Comma delimited list of fields for which data has been retrieved.
    * @param rs Result set from which to pull the data from.
    * @throws SQLException Passes on SQLException
    */    
    protected abstract void transferData( ResultSet rs )  throws SQLException;
}   // End of db_BaseLookupTable class definition
