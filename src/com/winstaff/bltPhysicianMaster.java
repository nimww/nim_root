

package com.winstaff;

import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;
import java.util.Enumeration;

public class bltPhysicianMaster extends Object implements InttPhysicianMaster
{

    dbtPhysicianMaster    dbDB;

    public bltPhysicianMaster()
    {
        dbDB = new dbtPhysicianMaster();
        dbDB.setUniqueCreateDate(new java.util.Date());
        makeRequiredHash();
        makeExpiredHash();
        makeEnglishHash();

    }    // End of default constructor

    public bltPhysicianMaster( Integer iNewID )
    {        dbDB = new dbtPhysicianMaster( iNewID );
        makeRequiredHash();
        makeExpiredHash();
        makeEnglishHash();
        iSecurityCheck = new Integer(2);
    }    // End of Constructor knowing an ID


    public bltPhysicianMaster( Integer iNewID, Integer iGroupSecurityID )
    {
        this.GroupSecurityInit(iGroupSecurityID);
        dbDB = new dbtPhysicianMaster( iNewID );
        makeExpiredHash();
        makeEnglishHash();
        makeRequiredHash(iGroupSecurityID);
    }    // End of Constructor knowing an ID

    public bltPhysicianMaster( Integer iGroupSecurityID, boolean isTrue )
    {
        this.GroupSecurityInit(iGroupSecurityID);
        dbDB = new dbtPhysicianMaster(  );
        makeExpiredHash();
        makeEnglishHash();
        makeRequiredHash(iGroupSecurityID);
    }    // End of Security unknown ID

    public void GroupSecurityInit( Integer iGroupSecurityID )
    {
        bltSecurityGroupMaster mySGM = new bltSecurityGroupMaster(iGroupSecurityID);
        this.UserSecurityID = iGroupSecurityID;
        if (mySGM.getRunFieldSecurityID().intValue()==1)
        {
            this.runFieldSecurity = true;
            makeRequiredHash(iGroupSecurityID);
        }
        iSecurityCheck = SecurityCheck.CheckItem("PractitionerSection1", iGroupSecurityID);
    }  

    public EventReferenceType getEventReference()
    {
        return EventUtils.getEventReference("tPhysicianMaster", this.getUniqueID());
    }  

    public String convertString2LogString(String normalString) 
    {
        String myVal = "";
        for (int i=0;i<normalString.length();i++)
        {
            if ( (normalString.charAt(i)!='\r')&&(normalString.charAt(i)!='\n')&&(normalString.charAt(i)!='\t') )
            {
                myVal+=normalString.charAt(i);
            }
            else
            {
                myVal+="<NLT>";
            }
        }
        return myVal;
    }
    public void runAudit()
    {
        String AuditString = new java.util.Date()+" \t " ;  
        AuditString += " tPhysicianMaster "; 
        AuditString += " PhysicianID ="+this.getUniqueID(); 

        for (int i=0;i<this.AuditVector.size();i++)
        {
            AuditString+="\t"+convertString2LogString((String)this.AuditVector.elementAt(i));
        }
        AuditLogger.printLine(AuditString);
    }
    public void commitData() throws SQLException
    {
        runAudit();
        dbDB.setModifyComments(this.getUniqueModifyComments());
        dbDB.setUniqueModifyDate(new java.util.Date());
        dbDB.commitData();
        DataControlUtils.dataChanged("tPhysicianMaster",this.getUniqueID() ,this.AuditVector.size(), "PhysicianID");
        this.AuditVector.removeAllElements();
    }    // End of commitData()

    public void commitDataForced() throws SQLException
    {
        dbDB.commitDataForced();
    }    // End of commitData()

    public Integer getUniqueID()
    {
        return dbDB.getUniqueID();
    }    // End of getUniqueID()

    public void setUniqueID( Integer iNewID )
    {
        dbDB.setUniqueID( iNewID );
    }    // End of setUniqueID()


    public void setPhysicianID(Integer newValue)
    {
        dbDB.setPhysicianID(newValue);
    }

    public Integer getPhysicianID()
    {
        return dbDB.getPhysicianID();
    }

    java.util.Vector AuditVector = new java.util.Vector();

    public void setUniqueCreateDate(Date newValue)
    {
        this.setUniqueCreateDate(newValue,this.UserSecurityID);


    }
    public Date getUniqueCreateDate()
    {
        return this.getUniqueCreateDate(this.UserSecurityID);
    }

    public void setUniqueCreateDate(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='UniqueCreateDate' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[UniqueCreateDate]=["+newValue+"]");
                   dbDB.setUniqueCreateDate(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[UniqueCreateDate]=["+newValue+"]");
           dbDB.setUniqueCreateDate(newValue);
         }
    }
    public Date getUniqueCreateDate(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='UniqueCreateDate' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getUniqueCreateDate();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getUniqueCreateDate();
         }
        return myVal;
    }

    public void setUniqueModifyDate(Date newValue)
    {
        this.setUniqueModifyDate(newValue,this.UserSecurityID);


    }
    public Date getUniqueModifyDate()
    {
        return this.getUniqueModifyDate(this.UserSecurityID);
    }

    public void setUniqueModifyDate(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='UniqueModifyDate' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[UniqueModifyDate]=["+newValue+"]");
                   dbDB.setUniqueModifyDate(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[UniqueModifyDate]=["+newValue+"]");
           dbDB.setUniqueModifyDate(newValue);
         }
    }
    public Date getUniqueModifyDate(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='UniqueModifyDate' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getUniqueModifyDate();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getUniqueModifyDate();
         }
        return myVal;
    }

    public void setUniqueModifyComments(String newValue)
    {
        this.setUniqueModifyComments(newValue,this.UserSecurityID);


    }
    public String getUniqueModifyComments()
    {
        return this.getUniqueModifyComments(this.UserSecurityID);
    }

    public void setUniqueModifyComments(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='UniqueModifyComments' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[UniqueModifyComments]=["+newValue+"]");
                   dbDB.setUniqueModifyComments(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[UniqueModifyComments]=["+newValue+"]");
           dbDB.setUniqueModifyComments(newValue);
         }
    }
    public String getUniqueModifyComments(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='UniqueModifyComments' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getUniqueModifyComments();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getUniqueModifyComments();
         }
        return myVal;
    }

    public void setSalutation(Integer newValue)
    {
        this.setSalutation(newValue,this.UserSecurityID);


    }
    public Integer getSalutation()
    {
        return this.getSalutation(this.UserSecurityID);
    }

    public void setSalutation(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Salutation' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[Salutation]=["+newValue+"]");
                   dbDB.setSalutation(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[Salutation]=["+newValue+"]");
           dbDB.setSalutation(newValue);
         }
    }
    public Integer getSalutation(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Salutation' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getSalutation();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getSalutation();
         }
        return myVal;
    }

    public void setTitle(String newValue)
    {
        this.setTitle(newValue,this.UserSecurityID);


    }
    public String getTitle()
    {
        return this.getTitle(this.UserSecurityID);
    }

    public void setTitle(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Title' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[Title]=["+newValue+"]");
                   dbDB.setTitle(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[Title]=["+newValue+"]");
           dbDB.setTitle(newValue);
         }
    }
    public String getTitle(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Title' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getTitle();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getTitle();
         }
        return myVal;
    }

    public void setFirstName(String newValue)
    {
        this.setFirstName(newValue,this.UserSecurityID);


    }
    public String getFirstName()
    {
        return this.getFirstName(this.UserSecurityID);
    }

    public void setFirstName(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='FirstName' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[FirstName]=["+newValue+"]");
                   dbDB.setFirstName(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[FirstName]=["+newValue+"]");
           dbDB.setFirstName(newValue);
         }
    }
    public String getFirstName(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='FirstName' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getFirstName();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getFirstName();
         }
        return myVal;
    }

    public void setMiddleName(String newValue)
    {
        this.setMiddleName(newValue,this.UserSecurityID);


    }
    public String getMiddleName()
    {
        return this.getMiddleName(this.UserSecurityID);
    }

    public void setMiddleName(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='MiddleName' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[MiddleName]=["+newValue+"]");
                   dbDB.setMiddleName(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[MiddleName]=["+newValue+"]");
           dbDB.setMiddleName(newValue);
         }
    }
    public String getMiddleName(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='MiddleName' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getMiddleName();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getMiddleName();
         }
        return myVal;
    }

    public void setLastName(String newValue)
    {
        this.setLastName(newValue,this.UserSecurityID);


    }
    public String getLastName()
    {
        return this.getLastName(this.UserSecurityID);
    }

    public void setLastName(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='LastName' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[LastName]=["+newValue+"]");
                   dbDB.setLastName(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[LastName]=["+newValue+"]");
           dbDB.setLastName(newValue);
         }
    }
    public String getLastName(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='LastName' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getLastName();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getLastName();
         }
        return myVal;
    }

    public void setSuffix(String newValue)
    {
        this.setSuffix(newValue,this.UserSecurityID);


    }
    public String getSuffix()
    {
        return this.getSuffix(this.UserSecurityID);
    }

    public void setSuffix(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Suffix' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[Suffix]=["+newValue+"]");
                   dbDB.setSuffix(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[Suffix]=["+newValue+"]");
           dbDB.setSuffix(newValue);
         }
    }
    public String getSuffix(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Suffix' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getSuffix();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getSuffix();
         }
        return myVal;
    }

    public void setHomeEmail(String newValue)
    {
        this.setHomeEmail(newValue,this.UserSecurityID);


    }
    public String getHomeEmail()
    {
        return this.getHomeEmail(this.UserSecurityID);
    }

    public void setHomeEmail(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='HomeEmail' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[HomeEmail]=["+newValue+"]");
                   dbDB.setHomeEmail(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[HomeEmail]=["+newValue+"]");
           dbDB.setHomeEmail(newValue);
         }
    }
    public String getHomeEmail(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='HomeEmail' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getHomeEmail();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getHomeEmail();
         }
        return myVal;
    }

    public void setHomeAddress1(String newValue)
    {
        this.setHomeAddress1(newValue,this.UserSecurityID);


    }
    public String getHomeAddress1()
    {
        return this.getHomeAddress1(this.UserSecurityID);
    }

    public void setHomeAddress1(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='HomeAddress1' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[HomeAddress1]=["+newValue+"]");
                   dbDB.setHomeAddress1(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[HomeAddress1]=["+newValue+"]");
           dbDB.setHomeAddress1(newValue);
         }
    }
    public String getHomeAddress1(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='HomeAddress1' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getHomeAddress1();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getHomeAddress1();
         }
        return myVal;
    }

    public void setHomeAddress2(String newValue)
    {
        this.setHomeAddress2(newValue,this.UserSecurityID);


    }
    public String getHomeAddress2()
    {
        return this.getHomeAddress2(this.UserSecurityID);
    }

    public void setHomeAddress2(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='HomeAddress2' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[HomeAddress2]=["+newValue+"]");
                   dbDB.setHomeAddress2(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[HomeAddress2]=["+newValue+"]");
           dbDB.setHomeAddress2(newValue);
         }
    }
    public String getHomeAddress2(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='HomeAddress2' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getHomeAddress2();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getHomeAddress2();
         }
        return myVal;
    }

    public void setHomeCity(String newValue)
    {
        this.setHomeCity(newValue,this.UserSecurityID);


    }
    public String getHomeCity()
    {
        return this.getHomeCity(this.UserSecurityID);
    }

    public void setHomeCity(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='HomeCity' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[HomeCity]=["+newValue+"]");
                   dbDB.setHomeCity(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[HomeCity]=["+newValue+"]");
           dbDB.setHomeCity(newValue);
         }
    }
    public String getHomeCity(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='HomeCity' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getHomeCity();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getHomeCity();
         }
        return myVal;
    }

    public void setHomeStateID(Integer newValue)
    {
        this.setHomeStateID(newValue,this.UserSecurityID);


    }
    public Integer getHomeStateID()
    {
        return this.getHomeStateID(this.UserSecurityID);
    }

    public void setHomeStateID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='HomeStateID' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[HomeStateID]=["+newValue+"]");
                   dbDB.setHomeStateID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[HomeStateID]=["+newValue+"]");
           dbDB.setHomeStateID(newValue);
         }
    }
    public Integer getHomeStateID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='HomeStateID' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getHomeStateID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getHomeStateID();
         }
        return myVal;
    }

    public void setHomeProvince(String newValue)
    {
        this.setHomeProvince(newValue,this.UserSecurityID);


    }
    public String getHomeProvince()
    {
        return this.getHomeProvince(this.UserSecurityID);
    }

    public void setHomeProvince(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='HomeProvince' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[HomeProvince]=["+newValue+"]");
                   dbDB.setHomeProvince(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[HomeProvince]=["+newValue+"]");
           dbDB.setHomeProvince(newValue);
         }
    }
    public String getHomeProvince(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='HomeProvince' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getHomeProvince();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getHomeProvince();
         }
        return myVal;
    }

    public void setHomeZIP(String newValue)
    {
        this.setHomeZIP(newValue,this.UserSecurityID);


    }
    public String getHomeZIP()
    {
        return this.getHomeZIP(this.UserSecurityID);
    }

    public void setHomeZIP(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='HomeZIP' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[HomeZIP]=["+newValue+"]");
                   dbDB.setHomeZIP(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[HomeZIP]=["+newValue+"]");
           dbDB.setHomeZIP(newValue);
         }
    }
    public String getHomeZIP(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='HomeZIP' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getHomeZIP();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getHomeZIP();
         }
        return myVal;
    }

    public void setHomeCountryID(Integer newValue)
    {
        this.setHomeCountryID(newValue,this.UserSecurityID);


    }
    public Integer getHomeCountryID()
    {
        return this.getHomeCountryID(this.UserSecurityID);
    }

    public void setHomeCountryID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='HomeCountryID' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[HomeCountryID]=["+newValue+"]");
                   dbDB.setHomeCountryID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[HomeCountryID]=["+newValue+"]");
           dbDB.setHomeCountryID(newValue);
         }
    }
    public Integer getHomeCountryID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='HomeCountryID' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getHomeCountryID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getHomeCountryID();
         }
        return myVal;
    }

    public void setHomePhone(String newValue)
    {
        this.setHomePhone(newValue,this.UserSecurityID);


    }
    public String getHomePhone()
    {
        return this.getHomePhone(this.UserSecurityID);
    }

    public void setHomePhone(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='HomePhone' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[HomePhone]=["+newValue+"]");
                   dbDB.setHomePhone(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[HomePhone]=["+newValue+"]");
           dbDB.setHomePhone(newValue);
         }
    }
    public String getHomePhone(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='HomePhone' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getHomePhone();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getHomePhone();
         }
        return myVal;
    }

    public void setHomeFax(String newValue)
    {
        this.setHomeFax(newValue,this.UserSecurityID);


    }
    public String getHomeFax()
    {
        return this.getHomeFax(this.UserSecurityID);
    }

    public void setHomeFax(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='HomeFax' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[HomeFax]=["+newValue+"]");
                   dbDB.setHomeFax(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[HomeFax]=["+newValue+"]");
           dbDB.setHomeFax(newValue);
         }
    }
    public String getHomeFax(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='HomeFax' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getHomeFax();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getHomeFax();
         }
        return myVal;
    }

    public void setHomeMobile(String newValue)
    {
        this.setHomeMobile(newValue,this.UserSecurityID);


    }
    public String getHomeMobile()
    {
        return this.getHomeMobile(this.UserSecurityID);
    }

    public void setHomeMobile(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='HomeMobile' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[HomeMobile]=["+newValue+"]");
                   dbDB.setHomeMobile(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[HomeMobile]=["+newValue+"]");
           dbDB.setHomeMobile(newValue);
         }
    }
    public String getHomeMobile(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='HomeMobile' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getHomeMobile();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getHomeMobile();
         }
        return myVal;
    }

    public void setHomePager(String newValue)
    {
        this.setHomePager(newValue,this.UserSecurityID);


    }
    public String getHomePager()
    {
        return this.getHomePager(this.UserSecurityID);
    }

    public void setHomePager(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='HomePager' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[HomePager]=["+newValue+"]");
                   dbDB.setHomePager(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[HomePager]=["+newValue+"]");
           dbDB.setHomePager(newValue);
         }
    }
    public String getHomePager(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='HomePager' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getHomePager();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getHomePager();
         }
        return myVal;
    }

    public void setAlertEmail(String newValue)
    {
        this.setAlertEmail(newValue,this.UserSecurityID);


    }
    public String getAlertEmail()
    {
        return this.getAlertEmail(this.UserSecurityID);
    }

    public void setAlertEmail(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AlertEmail' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[AlertEmail]=["+newValue+"]");
                   dbDB.setAlertEmail(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[AlertEmail]=["+newValue+"]");
           dbDB.setAlertEmail(newValue);
         }
    }
    public String getAlertEmail(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AlertEmail' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getAlertEmail();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getAlertEmail();
         }
        return myVal;
    }

    public void setAlertDays(Integer newValue)
    {
        this.setAlertDays(newValue,this.UserSecurityID);


    }
    public Integer getAlertDays()
    {
        return this.getAlertDays(this.UserSecurityID);
    }

    public void setAlertDays(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AlertDays' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[AlertDays]=["+newValue+"]");
                   dbDB.setAlertDays(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[AlertDays]=["+newValue+"]");
           dbDB.setAlertDays(newValue);
         }
    }
    public Integer getAlertDays(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AlertDays' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getAlertDays();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getAlertDays();
         }
        return myVal;
    }

    public void setOtherName1(String newValue)
    {
        this.setOtherName1(newValue,this.UserSecurityID);


    }
    public String getOtherName1()
    {
        return this.getOtherName1(this.UserSecurityID);
    }

    public void setOtherName1(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='OtherName1' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[OtherName1]=["+newValue+"]");
                   dbDB.setOtherName1(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[OtherName1]=["+newValue+"]");
           dbDB.setOtherName1(newValue);
         }
    }
    public String getOtherName1(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='OtherName1' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getOtherName1();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getOtherName1();
         }
        return myVal;
    }

    public void setOtherName1Start(Date newValue)
    {
        this.setOtherName1Start(newValue,this.UserSecurityID);


    }
    public Date getOtherName1Start()
    {
        return this.getOtherName1Start(this.UserSecurityID);
    }

    public void setOtherName1Start(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='OtherName1Start' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[OtherName1Start]=["+newValue+"]");
                   dbDB.setOtherName1Start(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[OtherName1Start]=["+newValue+"]");
           dbDB.setOtherName1Start(newValue);
         }
    }
    public Date getOtherName1Start(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='OtherName1Start' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getOtherName1Start();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getOtherName1Start();
         }
        return myVal;
    }

    public void setOtherName1End(Date newValue)
    {
        this.setOtherName1End(newValue,this.UserSecurityID);


    }
    public Date getOtherName1End()
    {
        return this.getOtherName1End(this.UserSecurityID);
    }

    public void setOtherName1End(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='OtherName1End' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[OtherName1End]=["+newValue+"]");
                   dbDB.setOtherName1End(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[OtherName1End]=["+newValue+"]");
           dbDB.setOtherName1End(newValue);
         }
    }
    public Date getOtherName1End(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='OtherName1End' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getOtherName1End();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getOtherName1End();
         }
        return myVal;
    }

    public void setOtherName2(String newValue)
    {
        this.setOtherName2(newValue,this.UserSecurityID);


    }
    public String getOtherName2()
    {
        return this.getOtherName2(this.UserSecurityID);
    }

    public void setOtherName2(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='OtherName2' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[OtherName2]=["+newValue+"]");
                   dbDB.setOtherName2(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[OtherName2]=["+newValue+"]");
           dbDB.setOtherName2(newValue);
         }
    }
    public String getOtherName2(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='OtherName2' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getOtherName2();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getOtherName2();
         }
        return myVal;
    }

    public void setOtherName2Start(Date newValue)
    {
        this.setOtherName2Start(newValue,this.UserSecurityID);


    }
    public Date getOtherName2Start()
    {
        return this.getOtherName2Start(this.UserSecurityID);
    }

    public void setOtherName2Start(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='OtherName2Start' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[OtherName2Start]=["+newValue+"]");
                   dbDB.setOtherName2Start(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[OtherName2Start]=["+newValue+"]");
           dbDB.setOtherName2Start(newValue);
         }
    }
    public Date getOtherName2Start(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='OtherName2Start' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getOtherName2Start();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getOtherName2Start();
         }
        return myVal;
    }

    public void setOtherName2End(Date newValue)
    {
        this.setOtherName2End(newValue,this.UserSecurityID);


    }
    public Date getOtherName2End()
    {
        return this.getOtherName2End(this.UserSecurityID);
    }

    public void setOtherName2End(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='OtherName2End' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[OtherName2End]=["+newValue+"]");
                   dbDB.setOtherName2End(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[OtherName2End]=["+newValue+"]");
           dbDB.setOtherName2End(newValue);
         }
    }
    public Date getOtherName2End(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='OtherName2End' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getOtherName2End();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getOtherName2End();
         }
        return myVal;
    }

    public void setDateOfBirth(Date newValue)
    {
        this.setDateOfBirth(newValue,this.UserSecurityID);


    }
    public Date getDateOfBirth()
    {
        return this.getDateOfBirth(this.UserSecurityID);
    }

    public void setDateOfBirth(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='DateOfBirth' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[DateOfBirth]=["+newValue+"]");
                   dbDB.setDateOfBirth(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[DateOfBirth]=["+newValue+"]");
           dbDB.setDateOfBirth(newValue);
         }
    }
    public Date getDateOfBirth(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='DateOfBirth' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getDateOfBirth();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getDateOfBirth();
         }
        return myVal;
    }

    public void setPlaceOfBirth(String newValue)
    {
        this.setPlaceOfBirth(newValue,this.UserSecurityID);


    }
    public String getPlaceOfBirth()
    {
        return this.getPlaceOfBirth(this.UserSecurityID);
    }

    public void setPlaceOfBirth(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PlaceOfBirth' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[PlaceOfBirth]=["+newValue+"]");
                   dbDB.setPlaceOfBirth(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[PlaceOfBirth]=["+newValue+"]");
           dbDB.setPlaceOfBirth(newValue);
         }
    }
    public String getPlaceOfBirth(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PlaceOfBirth' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPlaceOfBirth();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPlaceOfBirth();
         }
        return myVal;
    }

    public void setSpouse(String newValue)
    {
        this.setSpouse(newValue,this.UserSecurityID);


    }
    public String getSpouse()
    {
        return this.getSpouse(this.UserSecurityID);
    }

    public void setSpouse(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Spouse' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[Spouse]=["+newValue+"]");
                   dbDB.setSpouse(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[Spouse]=["+newValue+"]");
           dbDB.setSpouse(newValue);
         }
    }
    public String getSpouse(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Spouse' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getSpouse();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getSpouse();
         }
        return myVal;
    }

    public void setCitizenshipYN(Integer newValue)
    {
        this.setCitizenshipYN(newValue,this.UserSecurityID);


    }
    public Integer getCitizenshipYN()
    {
        return this.getCitizenshipYN(this.UserSecurityID);
    }

    public void setCitizenshipYN(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CitizenshipYN' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[CitizenshipYN]=["+newValue+"]");
                   dbDB.setCitizenshipYN(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[CitizenshipYN]=["+newValue+"]");
           dbDB.setCitizenshipYN(newValue);
         }
    }
    public Integer getCitizenshipYN(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CitizenshipYN' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getCitizenshipYN();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getCitizenshipYN();
         }
        return myVal;
    }

    public void setCitizenship(String newValue)
    {
        this.setCitizenship(newValue,this.UserSecurityID);


    }
    public String getCitizenship()
    {
        return this.getCitizenship(this.UserSecurityID);
    }

    public void setCitizenship(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Citizenship' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[Citizenship]=["+newValue+"]");
                   dbDB.setCitizenship(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[Citizenship]=["+newValue+"]");
           dbDB.setCitizenship(newValue);
         }
    }
    public String getCitizenship(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Citizenship' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getCitizenship();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getCitizenship();
         }
        return myVal;
    }

    public void setVisaNumber(String newValue)
    {
        this.setVisaNumber(newValue,this.UserSecurityID);


    }
    public String getVisaNumber()
    {
        return this.getVisaNumber(this.UserSecurityID);
    }

    public void setVisaNumber(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='VisaNumber' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[VisaNumber]=["+newValue+"]");
                   dbDB.setVisaNumber(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[VisaNumber]=["+newValue+"]");
           dbDB.setVisaNumber(newValue);
         }
    }
    public String getVisaNumber(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='VisaNumber' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getVisaNumber();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getVisaNumber();
         }
        return myVal;
    }

    public void setVisaStatus(String newValue)
    {
        this.setVisaStatus(newValue,this.UserSecurityID);


    }
    public String getVisaStatus()
    {
        return this.getVisaStatus(this.UserSecurityID);
    }

    public void setVisaStatus(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='VisaStatus' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[VisaStatus]=["+newValue+"]");
                   dbDB.setVisaStatus(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[VisaStatus]=["+newValue+"]");
           dbDB.setVisaStatus(newValue);
         }
    }
    public String getVisaStatus(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='VisaStatus' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getVisaStatus();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getVisaStatus();
         }
        return myVal;
    }

    public void setEligibleToWorkInUS(Integer newValue)
    {
        this.setEligibleToWorkInUS(newValue,this.UserSecurityID);


    }
    public Integer getEligibleToWorkInUS()
    {
        return this.getEligibleToWorkInUS(this.UserSecurityID);
    }

    public void setEligibleToWorkInUS(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='EligibleToWorkInUS' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[EligibleToWorkInUS]=["+newValue+"]");
                   dbDB.setEligibleToWorkInUS(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[EligibleToWorkInUS]=["+newValue+"]");
           dbDB.setEligibleToWorkInUS(newValue);
         }
    }
    public Integer getEligibleToWorkInUS(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='EligibleToWorkInUS' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getEligibleToWorkInUS();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getEligibleToWorkInUS();
         }
        return myVal;
    }

    public void setSSN(String newValue)
    {
        this.setSSN(newValue,this.UserSecurityID);


    }
    public String getSSN()
    {
        return this.getSSN(this.UserSecurityID);
    }

    public void setSSN(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SSN' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[SSN]=["+newValue+"]");
                   dbDB.setSSN(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[SSN]=["+newValue+"]");
           dbDB.setSSN(newValue);
         }
    }
    public String getSSN(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SSN' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getSSN();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getSSN();
         }
        return myVal;
    }

    public void setGender(Integer newValue)
    {
        this.setGender(newValue,this.UserSecurityID);


    }
    public Integer getGender()
    {
        return this.getGender(this.UserSecurityID);
    }

    public void setGender(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Gender' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[Gender]=["+newValue+"]");
                   dbDB.setGender(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[Gender]=["+newValue+"]");
           dbDB.setGender(newValue);
         }
    }
    public Integer getGender(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Gender' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getGender();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getGender();
         }
        return myVal;
    }

    public void setMilitaryActive(Integer newValue)
    {
        this.setMilitaryActive(newValue,this.UserSecurityID);


    }
    public Integer getMilitaryActive()
    {
        return this.getMilitaryActive(this.UserSecurityID);
    }

    public void setMilitaryActive(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='MilitaryActive' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[MilitaryActive]=["+newValue+"]");
                   dbDB.setMilitaryActive(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[MilitaryActive]=["+newValue+"]");
           dbDB.setMilitaryActive(newValue);
         }
    }
    public Integer getMilitaryActive(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='MilitaryActive' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getMilitaryActive();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getMilitaryActive();
         }
        return myVal;
    }

    public void setMilitaryBranch(String newValue)
    {
        this.setMilitaryBranch(newValue,this.UserSecurityID);


    }
    public String getMilitaryBranch()
    {
        return this.getMilitaryBranch(this.UserSecurityID);
    }

    public void setMilitaryBranch(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='MilitaryBranch' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[MilitaryBranch]=["+newValue+"]");
                   dbDB.setMilitaryBranch(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[MilitaryBranch]=["+newValue+"]");
           dbDB.setMilitaryBranch(newValue);
         }
    }
    public String getMilitaryBranch(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='MilitaryBranch' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getMilitaryBranch();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getMilitaryBranch();
         }
        return myVal;
    }

    public void setMilitaryReserve(Integer newValue)
    {
        this.setMilitaryReserve(newValue,this.UserSecurityID);


    }
    public Integer getMilitaryReserve()
    {
        return this.getMilitaryReserve(this.UserSecurityID);
    }

    public void setMilitaryReserve(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='MilitaryReserve' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[MilitaryReserve]=["+newValue+"]");
                   dbDB.setMilitaryReserve(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[MilitaryReserve]=["+newValue+"]");
           dbDB.setMilitaryReserve(newValue);
         }
    }
    public Integer getMilitaryReserve(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='MilitaryReserve' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getMilitaryReserve();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getMilitaryReserve();
         }
        return myVal;
    }

    public void setHospitalAdmitingPrivileges(Integer newValue)
    {
        this.setHospitalAdmitingPrivileges(newValue,this.UserSecurityID);


    }
    public Integer getHospitalAdmitingPrivileges()
    {
        return this.getHospitalAdmitingPrivileges(this.UserSecurityID);
    }

    public void setHospitalAdmitingPrivileges(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='HospitalAdmitingPrivileges' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[HospitalAdmitingPrivileges]=["+newValue+"]");
                   dbDB.setHospitalAdmitingPrivileges(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[HospitalAdmitingPrivileges]=["+newValue+"]");
           dbDB.setHospitalAdmitingPrivileges(newValue);
         }
    }
    public Integer getHospitalAdmitingPrivileges(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='HospitalAdmitingPrivileges' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getHospitalAdmitingPrivileges();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getHospitalAdmitingPrivileges();
         }
        return myVal;
    }

    public void setHospitalAdmitingPrivilegesNo(String newValue)
    {
        this.setHospitalAdmitingPrivilegesNo(newValue,this.UserSecurityID);


    }
    public String getHospitalAdmitingPrivilegesNo()
    {
        return this.getHospitalAdmitingPrivilegesNo(this.UserSecurityID);
    }

    public void setHospitalAdmitingPrivilegesNo(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='HospitalAdmitingPrivilegesNo' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[HospitalAdmitingPrivilegesNo]=["+newValue+"]");
                   dbDB.setHospitalAdmitingPrivilegesNo(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[HospitalAdmitingPrivilegesNo]=["+newValue+"]");
           dbDB.setHospitalAdmitingPrivilegesNo(newValue);
         }
    }
    public String getHospitalAdmitingPrivilegesNo(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='HospitalAdmitingPrivilegesNo' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getHospitalAdmitingPrivilegesNo();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getHospitalAdmitingPrivilegesNo();
         }
        return myVal;
    }

    public void setPhysicianCategoryID(Integer newValue)
    {
        this.setPhysicianCategoryID(newValue,this.UserSecurityID);


    }
    public Integer getPhysicianCategoryID()
    {
        return this.getPhysicianCategoryID(this.UserSecurityID);
    }

    public void setPhysicianCategoryID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PhysicianCategoryID' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[PhysicianCategoryID]=["+newValue+"]");
                   dbDB.setPhysicianCategoryID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[PhysicianCategoryID]=["+newValue+"]");
           dbDB.setPhysicianCategoryID(newValue);
         }
    }
    public Integer getPhysicianCategoryID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PhysicianCategoryID' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPhysicianCategoryID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPhysicianCategoryID();
         }
        return myVal;
    }

    public void setIPAMedicalAffiliation(Integer newValue)
    {
        this.setIPAMedicalAffiliation(newValue,this.UserSecurityID);


    }
    public Integer getIPAMedicalAffiliation()
    {
        return this.getIPAMedicalAffiliation(this.UserSecurityID);
    }

    public void setIPAMedicalAffiliation(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='IPAMedicalAffiliation' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[IPAMedicalAffiliation]=["+newValue+"]");
                   dbDB.setIPAMedicalAffiliation(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[IPAMedicalAffiliation]=["+newValue+"]");
           dbDB.setIPAMedicalAffiliation(newValue);
         }
    }
    public Integer getIPAMedicalAffiliation(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='IPAMedicalAffiliation' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getIPAMedicalAffiliation();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getIPAMedicalAffiliation();
         }
        return myVal;
    }

    public void setAffiliationDesc1(String newValue)
    {
        this.setAffiliationDesc1(newValue,this.UserSecurityID);


    }
    public String getAffiliationDesc1()
    {
        return this.getAffiliationDesc1(this.UserSecurityID);
    }

    public void setAffiliationDesc1(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AffiliationDesc1' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[AffiliationDesc1]=["+newValue+"]");
                   dbDB.setAffiliationDesc1(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[AffiliationDesc1]=["+newValue+"]");
           dbDB.setAffiliationDesc1(newValue);
         }
    }
    public String getAffiliationDesc1(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AffiliationDesc1' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getAffiliationDesc1();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getAffiliationDesc1();
         }
        return myVal;
    }

    public void setPhysicianLanguages(String newValue)
    {
        this.setPhysicianLanguages(newValue,this.UserSecurityID);


    }
    public String getPhysicianLanguages()
    {
        return this.getPhysicianLanguages(this.UserSecurityID);
    }

    public void setPhysicianLanguages(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PhysicianLanguages' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[PhysicianLanguages]=["+newValue+"]");
                   dbDB.setPhysicianLanguages(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[PhysicianLanguages]=["+newValue+"]");
           dbDB.setPhysicianLanguages(newValue);
         }
    }
    public String getPhysicianLanguages(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PhysicianLanguages' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPhysicianLanguages();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPhysicianLanguages();
         }
        return myVal;
    }

    public void setECFMGNo(String newValue)
    {
        this.setECFMGNo(newValue,this.UserSecurityID);


    }
    public String getECFMGNo()
    {
        return this.getECFMGNo(this.UserSecurityID);
    }

    public void setECFMGNo(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ECFMGNo' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[ECFMGNo]=["+newValue+"]");
                   dbDB.setECFMGNo(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[ECFMGNo]=["+newValue+"]");
           dbDB.setECFMGNo(newValue);
         }
    }
    public String getECFMGNo(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ECFMGNo' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getECFMGNo();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getECFMGNo();
         }
        return myVal;
    }

    public void setECFMGDateIssued(Date newValue)
    {
        this.setECFMGDateIssued(newValue,this.UserSecurityID);


    }
    public Date getECFMGDateIssued()
    {
        return this.getECFMGDateIssued(this.UserSecurityID);
    }

    public void setECFMGDateIssued(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ECFMGDateIssued' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[ECFMGDateIssued]=["+newValue+"]");
                   dbDB.setECFMGDateIssued(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[ECFMGDateIssued]=["+newValue+"]");
           dbDB.setECFMGDateIssued(newValue);
         }
    }
    public Date getECFMGDateIssued(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ECFMGDateIssued' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getECFMGDateIssued();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getECFMGDateIssued();
         }
        return myVal;
    }

    public void setECFMGDateExpires(Date newValue)
    {
        this.setECFMGDateExpires(newValue,this.UserSecurityID);


    }
    public Date getECFMGDateExpires()
    {
        return this.getECFMGDateExpires(this.UserSecurityID);
    }

    public void setECFMGDateExpires(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ECFMGDateExpires' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[ECFMGDateExpires]=["+newValue+"]");
                   dbDB.setECFMGDateExpires(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[ECFMGDateExpires]=["+newValue+"]");
           dbDB.setECFMGDateExpires(newValue);
         }
    }
    public Date getECFMGDateExpires(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ECFMGDateExpires' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getECFMGDateExpires();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getECFMGDateExpires();
         }
        return myVal;
    }

    public void setMedicareUPIN(String newValue)
    {
        this.setMedicareUPIN(newValue,this.UserSecurityID);


    }
    public String getMedicareUPIN()
    {
        return this.getMedicareUPIN(this.UserSecurityID);
    }

    public void setMedicareUPIN(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='MedicareUPIN' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[MedicareUPIN]=["+newValue+"]");
                   dbDB.setMedicareUPIN(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[MedicareUPIN]=["+newValue+"]");
           dbDB.setMedicareUPIN(newValue);
         }
    }
    public String getMedicareUPIN(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='MedicareUPIN' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getMedicareUPIN();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getMedicareUPIN();
         }
        return myVal;
    }

    public void setUniqueNPI(String newValue)
    {
        this.setUniqueNPI(newValue,this.UserSecurityID);


    }
    public String getUniqueNPI()
    {
        return this.getUniqueNPI(this.UserSecurityID);
    }

    public void setUniqueNPI(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='UniqueNPI' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[UniqueNPI]=["+newValue+"]");
                   dbDB.setUniqueNPI(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[UniqueNPI]=["+newValue+"]");
           dbDB.setUniqueNPI(newValue);
         }
    }
    public String getUniqueNPI(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='UniqueNPI' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getUniqueNPI();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getUniqueNPI();
         }
        return myVal;
    }

    public void setMedicareParticipation(Integer newValue)
    {
        this.setMedicareParticipation(newValue,this.UserSecurityID);


    }
    public Integer getMedicareParticipation()
    {
        return this.getMedicareParticipation(this.UserSecurityID);
    }

    public void setMedicareParticipation(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='MedicareParticipation' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[MedicareParticipation]=["+newValue+"]");
                   dbDB.setMedicareParticipation(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[MedicareParticipation]=["+newValue+"]");
           dbDB.setMedicareParticipation(newValue);
         }
    }
    public Integer getMedicareParticipation(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='MedicareParticipation' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getMedicareParticipation();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getMedicareParticipation();
         }
        return myVal;
    }

    public void setMedicareNo(String newValue)
    {
        this.setMedicareNo(newValue,this.UserSecurityID);


    }
    public String getMedicareNo()
    {
        return this.getMedicareNo(this.UserSecurityID);
    }

    public void setMedicareNo(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='MedicareNo' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[MedicareNo]=["+newValue+"]");
                   dbDB.setMedicareNo(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[MedicareNo]=["+newValue+"]");
           dbDB.setMedicareNo(newValue);
         }
    }
    public String getMedicareNo(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='MedicareNo' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getMedicareNo();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getMedicareNo();
         }
        return myVal;
    }

    public void setMediCaidParticipation(Integer newValue)
    {
        this.setMediCaidParticipation(newValue,this.UserSecurityID);


    }
    public Integer getMediCaidParticipation()
    {
        return this.getMediCaidParticipation(this.UserSecurityID);
    }

    public void setMediCaidParticipation(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='MediCaidParticipation' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[MediCaidParticipation]=["+newValue+"]");
                   dbDB.setMediCaidParticipation(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[MediCaidParticipation]=["+newValue+"]");
           dbDB.setMediCaidParticipation(newValue);
         }
    }
    public Integer getMediCaidParticipation(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='MediCaidParticipation' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getMediCaidParticipation();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getMediCaidParticipation();
         }
        return myVal;
    }

    public void setMediCaidNo(String newValue)
    {
        this.setMediCaidNo(newValue,this.UserSecurityID);


    }
    public String getMediCaidNo()
    {
        return this.getMediCaidNo(this.UserSecurityID);
    }

    public void setMediCaidNo(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='MediCaidNo' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[MediCaidNo]=["+newValue+"]");
                   dbDB.setMediCaidNo(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[MediCaidNo]=["+newValue+"]");
           dbDB.setMediCaidNo(newValue);
         }
    }
    public String getMediCaidNo(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='MediCaidNo' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getMediCaidNo();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getMediCaidNo();
         }
        return myVal;
    }

    public void setSupplementalIDNumber1(String newValue)
    {
        this.setSupplementalIDNumber1(newValue,this.UserSecurityID);


    }
    public String getSupplementalIDNumber1()
    {
        return this.getSupplementalIDNumber1(this.UserSecurityID);
    }

    public void setSupplementalIDNumber1(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SupplementalIDNumber1' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[SupplementalIDNumber1]=["+newValue+"]");
                   dbDB.setSupplementalIDNumber1(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[SupplementalIDNumber1]=["+newValue+"]");
           dbDB.setSupplementalIDNumber1(newValue);
         }
    }
    public String getSupplementalIDNumber1(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SupplementalIDNumber1' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getSupplementalIDNumber1();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getSupplementalIDNumber1();
         }
        return myVal;
    }

    public void setSupplementalIDNumber2(String newValue)
    {
        this.setSupplementalIDNumber2(newValue,this.UserSecurityID);


    }
    public String getSupplementalIDNumber2()
    {
        return this.getSupplementalIDNumber2(this.UserSecurityID);
    }

    public void setSupplementalIDNumber2(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SupplementalIDNumber2' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[SupplementalIDNumber2]=["+newValue+"]");
                   dbDB.setSupplementalIDNumber2(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[SupplementalIDNumber2]=["+newValue+"]");
           dbDB.setSupplementalIDNumber2(newValue);
         }
    }
    public String getSupplementalIDNumber2(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SupplementalIDNumber2' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getSupplementalIDNumber2();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getSupplementalIDNumber2();
         }
        return myVal;
    }

    public void setComments(String newValue)
    {
        this.setComments(newValue,this.UserSecurityID);


    }
    public String getComments()
    {
        return this.getComments(this.UserSecurityID);
    }

    public void setComments(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Comments' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[Comments]=["+newValue+"]");
                   dbDB.setComments(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[Comments]=["+newValue+"]");
           dbDB.setComments(newValue);
         }
    }
    public String getComments(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Comments' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getComments();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getComments();
         }
        return myVal;
    }

    public void setAttestConsent(Integer newValue)
    {
        this.setAttestConsent(newValue,this.UserSecurityID);


    }
    public Integer getAttestConsent()
    {
        return this.getAttestConsent(this.UserSecurityID);
    }

    public void setAttestConsent(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AttestConsent' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[AttestConsent]=["+newValue+"]");
                   dbDB.setAttestConsent(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[AttestConsent]=["+newValue+"]");
           dbDB.setAttestConsent(newValue);
         }
    }
    public Integer getAttestConsent(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AttestConsent' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getAttestConsent();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getAttestConsent();
         }
        return myVal;
    }

    public void setAttestationComments(String newValue)
    {
        this.setAttestationComments(newValue,this.UserSecurityID);


    }
    public String getAttestationComments()
    {
        return this.getAttestationComments(this.UserSecurityID);
    }

    public void setAttestationComments(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AttestationComments' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[AttestationComments]=["+newValue+"]");
                   dbDB.setAttestationComments(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[AttestationComments]=["+newValue+"]");
           dbDB.setAttestationComments(newValue);
         }
    }
    public String getAttestationComments(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AttestationComments' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getAttestationComments();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getAttestationComments();
         }
        return myVal;
    }

    public void setIsViewable(Integer newValue)
    {
        this.setIsViewable(newValue,this.UserSecurityID);


    }
    public Integer getIsViewable()
    {
        return this.getIsViewable(this.UserSecurityID);
    }

    public void setIsViewable(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='IsViewable' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[IsViewable]=["+newValue+"]");
                   dbDB.setIsViewable(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[IsViewable]=["+newValue+"]");
           dbDB.setIsViewable(newValue);
         }
    }
    public Integer getIsViewable(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='IsViewable' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getIsViewable();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getIsViewable();
         }
        return myVal;
    }

    public void setIsAttested(Integer newValue)
    {
        this.setIsAttested(newValue,this.UserSecurityID);


    }
    public Integer getIsAttested()
    {
        return this.getIsAttested(this.UserSecurityID);
    }

    public void setIsAttested(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='IsAttested' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[IsAttested]=["+newValue+"]");
                   dbDB.setIsAttested(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[IsAttested]=["+newValue+"]");
           dbDB.setIsAttested(newValue);
         }
    }
    public Integer getIsAttested(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='IsAttested' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getIsAttested();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getIsAttested();
         }
        return myVal;
    }

    public void setAttestDatePending(Date newValue)
    {
        this.setAttestDatePending(newValue,this.UserSecurityID);


    }
    public Date getAttestDatePending()
    {
        return this.getAttestDatePending(this.UserSecurityID);
    }

    public void setAttestDatePending(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AttestDatePending' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[AttestDatePending]=["+newValue+"]");
                   dbDB.setAttestDatePending(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[AttestDatePending]=["+newValue+"]");
           dbDB.setAttestDatePending(newValue);
         }
    }
    public Date getAttestDatePending(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AttestDatePending' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getAttestDatePending();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getAttestDatePending();
         }
        return myVal;
    }

    public void setIsAttestedPending(Integer newValue)
    {
        this.setIsAttestedPending(newValue,this.UserSecurityID);


    }
    public Integer getIsAttestedPending()
    {
        return this.getIsAttestedPending(this.UserSecurityID);
    }

    public void setIsAttestedPending(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='IsAttestedPending' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[IsAttestedPending]=["+newValue+"]");
                   dbDB.setIsAttestedPending(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[IsAttestedPending]=["+newValue+"]");
           dbDB.setIsAttestedPending(newValue);
         }
    }
    public Integer getIsAttestedPending(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='IsAttestedPending' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getIsAttestedPending();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getIsAttestedPending();
         }
        return myVal;
    }

    public void setAttestDate(Date newValue)
    {
        this.setAttestDate(newValue,this.UserSecurityID);


    }
    public Date getAttestDate()
    {
        return this.getAttestDate(this.UserSecurityID);
    }

    public void setAttestDate(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AttestDate' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[AttestDate]=["+newValue+"]");
                   dbDB.setAttestDate(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[AttestDate]=["+newValue+"]");
           dbDB.setAttestDate(newValue);
         }
    }
    public Date getAttestDate(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AttestDate' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getAttestDate();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getAttestDate();
         }
        return myVal;
    }

    public void setDeAttestDate(Date newValue)
    {
        this.setDeAttestDate(newValue,this.UserSecurityID);


    }
    public Date getDeAttestDate()
    {
        return this.getDeAttestDate(this.UserSecurityID);
    }

    public void setDeAttestDate(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='DeAttestDate' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[DeAttestDate]=["+newValue+"]");
                   dbDB.setDeAttestDate(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[DeAttestDate]=["+newValue+"]");
           dbDB.setDeAttestDate(newValue);
         }
    }
    public Date getDeAttestDate(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='DeAttestDate' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getDeAttestDate();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getDeAttestDate();
         }
        return myVal;
    }

    public void setLastModifiedDate(Date newValue)
    {
        this.setLastModifiedDate(newValue,this.UserSecurityID);


    }
    public Date getLastModifiedDate()
    {
        return this.getLastModifiedDate(this.UserSecurityID);
    }

    public void setLastModifiedDate(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='LastModifiedDate' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[LastModifiedDate]=["+newValue+"]");
                   dbDB.setLastModifiedDate(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[LastModifiedDate]=["+newValue+"]");
           dbDB.setLastModifiedDate(newValue);
         }
    }
    public Date getLastModifiedDate(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='LastModifiedDate' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getLastModifiedDate();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getLastModifiedDate();
         }
        return myVal;
    }

    public void setQuestionValidation(String newValue)
    {
        this.setQuestionValidation(newValue,this.UserSecurityID);


    }
    public String getQuestionValidation()
    {
        return this.getQuestionValidation(this.UserSecurityID);
    }

    public void setQuestionValidation(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='QuestionValidation' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[QuestionValidation]=["+newValue+"]");
                   dbDB.setQuestionValidation(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[QuestionValidation]=["+newValue+"]");
           dbDB.setQuestionValidation(newValue);
         }
    }
    public String getQuestionValidation(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='QuestionValidation' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getQuestionValidation();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getQuestionValidation();
         }
        return myVal;
    }

    public void setUSMLEDatePassedStep1(Date newValue)
    {
        this.setUSMLEDatePassedStep1(newValue,this.UserSecurityID);


    }
    public Date getUSMLEDatePassedStep1()
    {
        return this.getUSMLEDatePassedStep1(this.UserSecurityID);
    }

    public void setUSMLEDatePassedStep1(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='USMLEDatePassedStep1' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[USMLEDatePassedStep1]=["+newValue+"]");
                   dbDB.setUSMLEDatePassedStep1(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[USMLEDatePassedStep1]=["+newValue+"]");
           dbDB.setUSMLEDatePassedStep1(newValue);
         }
    }
    public Date getUSMLEDatePassedStep1(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='USMLEDatePassedStep1' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getUSMLEDatePassedStep1();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getUSMLEDatePassedStep1();
         }
        return myVal;
    }

    public void setUSMLEDatePassedStep2(Date newValue)
    {
        this.setUSMLEDatePassedStep2(newValue,this.UserSecurityID);


    }
    public Date getUSMLEDatePassedStep2()
    {
        return this.getUSMLEDatePassedStep2(this.UserSecurityID);
    }

    public void setUSMLEDatePassedStep2(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='USMLEDatePassedStep2' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[USMLEDatePassedStep2]=["+newValue+"]");
                   dbDB.setUSMLEDatePassedStep2(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[USMLEDatePassedStep2]=["+newValue+"]");
           dbDB.setUSMLEDatePassedStep2(newValue);
         }
    }
    public Date getUSMLEDatePassedStep2(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='USMLEDatePassedStep2' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getUSMLEDatePassedStep2();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getUSMLEDatePassedStep2();
         }
        return myVal;
    }

    public void setUSMLEDatePassedStep3(Date newValue)
    {
        this.setUSMLEDatePassedStep3(newValue,this.UserSecurityID);


    }
    public Date getUSMLEDatePassedStep3()
    {
        return this.getUSMLEDatePassedStep3(this.UserSecurityID);
    }

    public void setUSMLEDatePassedStep3(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='USMLEDatePassedStep3' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[USMLEDatePassedStep3]=["+newValue+"]");
                   dbDB.setUSMLEDatePassedStep3(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[USMLEDatePassedStep3]=["+newValue+"]");
           dbDB.setUSMLEDatePassedStep3(newValue);
         }
    }
    public Date getUSMLEDatePassedStep3(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='USMLEDatePassedStep3' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getUSMLEDatePassedStep3();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getUSMLEDatePassedStep3();
         }
        return myVal;
    }

    public void setHaveFLEX(Integer newValue)
    {
        this.setHaveFLEX(newValue,this.UserSecurityID);


    }
    public Integer getHaveFLEX()
    {
        return this.getHaveFLEX(this.UserSecurityID);
    }

    public void setHaveFLEX(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='HaveFLEX' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[HaveFLEX]=["+newValue+"]");
                   dbDB.setHaveFLEX(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[HaveFLEX]=["+newValue+"]");
           dbDB.setHaveFLEX(newValue);
         }
    }
    public Integer getHaveFLEX(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='HaveFLEX' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getHaveFLEX();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getHaveFLEX();
         }
        return myVal;
    }

    public void setFLEXDatePassed(Date newValue)
    {
        this.setFLEXDatePassed(newValue,this.UserSecurityID);


    }
    public Date getFLEXDatePassed()
    {
        return this.getFLEXDatePassed(this.UserSecurityID);
    }

    public void setFLEXDatePassed(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='FLEXDatePassed' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[FLEXDatePassed]=["+newValue+"]");
                   dbDB.setFLEXDatePassed(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[FLEXDatePassed]=["+newValue+"]");
           dbDB.setFLEXDatePassed(newValue);
         }
    }
    public Date getFLEXDatePassed(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='FLEXDatePassed' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getFLEXDatePassed();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getFLEXDatePassed();
         }
        return myVal;
    }

    public void setVisaSponsor(String newValue)
    {
        this.setVisaSponsor(newValue,this.UserSecurityID);


    }
    public String getVisaSponsor()
    {
        return this.getVisaSponsor(this.UserSecurityID);
    }

    public void setVisaSponsor(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='VisaSponsor' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[VisaSponsor]=["+newValue+"]");
                   dbDB.setVisaSponsor(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[VisaSponsor]=["+newValue+"]");
           dbDB.setVisaSponsor(newValue);
         }
    }
    public String getVisaSponsor(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='VisaSponsor' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getVisaSponsor();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getVisaSponsor();
         }
        return myVal;
    }

    public void setVisaExpiration(Date newValue)
    {
        this.setVisaExpiration(newValue,this.UserSecurityID);


    }
    public Date getVisaExpiration()
    {
        return this.getVisaExpiration(this.UserSecurityID);
    }

    public void setVisaExpiration(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='VisaExpiration' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[VisaExpiration]=["+newValue+"]");
                   dbDB.setVisaExpiration(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[VisaExpiration]=["+newValue+"]");
           dbDB.setVisaExpiration(newValue);
         }
    }
    public Date getVisaExpiration(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='VisaExpiration' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getVisaExpiration();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getVisaExpiration();
         }
        return myVal;
    }

    public void setCurrentVisaTemporary(Integer newValue)
    {
        this.setCurrentVisaTemporary(newValue,this.UserSecurityID);


    }
    public Integer getCurrentVisaTemporary()
    {
        return this.getCurrentVisaTemporary(this.UserSecurityID);
    }

    public void setCurrentVisaTemporary(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CurrentVisaTemporary' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[CurrentVisaTemporary]=["+newValue+"]");
                   dbDB.setCurrentVisaTemporary(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[CurrentVisaTemporary]=["+newValue+"]");
           dbDB.setCurrentVisaTemporary(newValue);
         }
    }
    public Integer getCurrentVisaTemporary(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CurrentVisaTemporary' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getCurrentVisaTemporary();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getCurrentVisaTemporary();
         }
        return myVal;
    }

    public void setCurrentVisaExtended(Integer newValue)
    {
        this.setCurrentVisaExtended(newValue,this.UserSecurityID);


    }
    public Integer getCurrentVisaExtended()
    {
        return this.getCurrentVisaExtended(this.UserSecurityID);
    }

    public void setCurrentVisaExtended(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CurrentVisaExtended' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[CurrentVisaExtended]=["+newValue+"]");
                   dbDB.setCurrentVisaExtended(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[CurrentVisaExtended]=["+newValue+"]");
           dbDB.setCurrentVisaExtended(newValue);
         }
    }
    public Integer getCurrentVisaExtended(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CurrentVisaExtended' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getCurrentVisaExtended();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getCurrentVisaExtended();
         }
        return myVal;
    }

    public void setHaveGreencard(Integer newValue)
    {
        this.setHaveGreencard(newValue,this.UserSecurityID);


    }
    public Integer getHaveGreencard()
    {
        return this.getHaveGreencard(this.UserSecurityID);
    }

    public void setHaveGreencard(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='HaveGreencard' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[HaveGreencard]=["+newValue+"]");
                   dbDB.setHaveGreencard(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[HaveGreencard]=["+newValue+"]");
           dbDB.setHaveGreencard(newValue);
         }
    }
    public Integer getHaveGreencard(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='HaveGreencard' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getHaveGreencard();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getHaveGreencard();
         }
        return myVal;
    }

    public void setCountryOfIssueID(Integer newValue)
    {
        this.setCountryOfIssueID(newValue,this.UserSecurityID);


    }
    public Integer getCountryOfIssueID()
    {
        return this.getCountryOfIssueID(this.UserSecurityID);
    }

    public void setCountryOfIssueID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CountryOfIssueID' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[CountryOfIssueID]=["+newValue+"]");
                   dbDB.setCountryOfIssueID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[CountryOfIssueID]=["+newValue+"]");
           dbDB.setCountryOfIssueID(newValue);
         }
    }
    public Integer getCountryOfIssueID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CountryOfIssueID' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getCountryOfIssueID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getCountryOfIssueID();
         }
        return myVal;
    }

    public void setVisaTemporary5Years(Integer newValue)
    {
        this.setVisaTemporary5Years(newValue,this.UserSecurityID);


    }
    public Integer getVisaTemporary5Years()
    {
        return this.getVisaTemporary5Years(this.UserSecurityID);
    }

    public void setVisaTemporary5Years(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='VisaTemporary5Years' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[VisaTemporary5Years]=["+newValue+"]");
                   dbDB.setVisaTemporary5Years(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[VisaTemporary5Years]=["+newValue+"]");
           dbDB.setVisaTemporary5Years(newValue);
         }
    }
    public Integer getVisaTemporary5Years(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='VisaTemporary5Years' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getVisaTemporary5Years();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getVisaTemporary5Years();
         }
        return myVal;
    }

    public void setPastVisa1DateFrom(Date newValue)
    {
        this.setPastVisa1DateFrom(newValue,this.UserSecurityID);


    }
    public Date getPastVisa1DateFrom()
    {
        return this.getPastVisa1DateFrom(this.UserSecurityID);
    }

    public void setPastVisa1DateFrom(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PastVisa1DateFrom' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[PastVisa1DateFrom]=["+newValue+"]");
                   dbDB.setPastVisa1DateFrom(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[PastVisa1DateFrom]=["+newValue+"]");
           dbDB.setPastVisa1DateFrom(newValue);
         }
    }
    public Date getPastVisa1DateFrom(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PastVisa1DateFrom' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPastVisa1DateFrom();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPastVisa1DateFrom();
         }
        return myVal;
    }

    public void setPastVisa1DateTo(Date newValue)
    {
        this.setPastVisa1DateTo(newValue,this.UserSecurityID);


    }
    public Date getPastVisa1DateTo()
    {
        return this.getPastVisa1DateTo(this.UserSecurityID);
    }

    public void setPastVisa1DateTo(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PastVisa1DateTo' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[PastVisa1DateTo]=["+newValue+"]");
                   dbDB.setPastVisa1DateTo(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[PastVisa1DateTo]=["+newValue+"]");
           dbDB.setPastVisa1DateTo(newValue);
         }
    }
    public Date getPastVisa1DateTo(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PastVisa1DateTo' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPastVisa1DateTo();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPastVisa1DateTo();
         }
        return myVal;
    }

    public void setPastVisa1Type(String newValue)
    {
        this.setPastVisa1Type(newValue,this.UserSecurityID);


    }
    public String getPastVisa1Type()
    {
        return this.getPastVisa1Type(this.UserSecurityID);
    }

    public void setPastVisa1Type(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PastVisa1Type' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[PastVisa1Type]=["+newValue+"]");
                   dbDB.setPastVisa1Type(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[PastVisa1Type]=["+newValue+"]");
           dbDB.setPastVisa1Type(newValue);
         }
    }
    public String getPastVisa1Type(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PastVisa1Type' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPastVisa1Type();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPastVisa1Type();
         }
        return myVal;
    }

    public void setPastVisa1Sponsor(String newValue)
    {
        this.setPastVisa1Sponsor(newValue,this.UserSecurityID);


    }
    public String getPastVisa1Sponsor()
    {
        return this.getPastVisa1Sponsor(this.UserSecurityID);
    }

    public void setPastVisa1Sponsor(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PastVisa1Sponsor' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[PastVisa1Sponsor]=["+newValue+"]");
                   dbDB.setPastVisa1Sponsor(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[PastVisa1Sponsor]=["+newValue+"]");
           dbDB.setPastVisa1Sponsor(newValue);
         }
    }
    public String getPastVisa1Sponsor(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PastVisa1Sponsor' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPastVisa1Sponsor();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPastVisa1Sponsor();
         }
        return myVal;
    }

    public void setPastVisa2DateFrom(Date newValue)
    {
        this.setPastVisa2DateFrom(newValue,this.UserSecurityID);


    }
    public Date getPastVisa2DateFrom()
    {
        return this.getPastVisa2DateFrom(this.UserSecurityID);
    }

    public void setPastVisa2DateFrom(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PastVisa2DateFrom' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[PastVisa2DateFrom]=["+newValue+"]");
                   dbDB.setPastVisa2DateFrom(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[PastVisa2DateFrom]=["+newValue+"]");
           dbDB.setPastVisa2DateFrom(newValue);
         }
    }
    public Date getPastVisa2DateFrom(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PastVisa2DateFrom' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPastVisa2DateFrom();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPastVisa2DateFrom();
         }
        return myVal;
    }

    public void setPastVisa2DateTo(Date newValue)
    {
        this.setPastVisa2DateTo(newValue,this.UserSecurityID);


    }
    public Date getPastVisa2DateTo()
    {
        return this.getPastVisa2DateTo(this.UserSecurityID);
    }

    public void setPastVisa2DateTo(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PastVisa2DateTo' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[PastVisa2DateTo]=["+newValue+"]");
                   dbDB.setPastVisa2DateTo(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[PastVisa2DateTo]=["+newValue+"]");
           dbDB.setPastVisa2DateTo(newValue);
         }
    }
    public Date getPastVisa2DateTo(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PastVisa2DateTo' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPastVisa2DateTo();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPastVisa2DateTo();
         }
        return myVal;
    }

    public void setPastVisa2Type(String newValue)
    {
        this.setPastVisa2Type(newValue,this.UserSecurityID);


    }
    public String getPastVisa2Type()
    {
        return this.getPastVisa2Type(this.UserSecurityID);
    }

    public void setPastVisa2Type(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PastVisa2Type' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[PastVisa2Type]=["+newValue+"]");
                   dbDB.setPastVisa2Type(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[PastVisa2Type]=["+newValue+"]");
           dbDB.setPastVisa2Type(newValue);
         }
    }
    public String getPastVisa2Type(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PastVisa2Type' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPastVisa2Type();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPastVisa2Type();
         }
        return myVal;
    }

    public void setPastVisa2Sponsor(String newValue)
    {
        this.setPastVisa2Sponsor(newValue,this.UserSecurityID);


    }
    public String getPastVisa2Sponsor()
    {
        return this.getPastVisa2Sponsor(this.UserSecurityID);
    }

    public void setPastVisa2Sponsor(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PastVisa2Sponsor' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[PastVisa2Sponsor]=["+newValue+"]");
                   dbDB.setPastVisa2Sponsor(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[PastVisa2Sponsor]=["+newValue+"]");
           dbDB.setPastVisa2Sponsor(newValue);
         }
    }
    public String getPastVisa2Sponsor(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PastVisa2Sponsor' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPastVisa2Sponsor();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPastVisa2Sponsor();
         }
        return myVal;
    }

    public void setMilitaryRank(String newValue)
    {
        this.setMilitaryRank(newValue,this.UserSecurityID);


    }
    public String getMilitaryRank()
    {
        return this.getMilitaryRank(this.UserSecurityID);
    }

    public void setMilitaryRank(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='MilitaryRank' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[MilitaryRank]=["+newValue+"]");
                   dbDB.setMilitaryRank(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[MilitaryRank]=["+newValue+"]");
           dbDB.setMilitaryRank(newValue);
         }
    }
    public String getMilitaryRank(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='MilitaryRank' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getMilitaryRank();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getMilitaryRank();
         }
        return myVal;
    }

    public void setMilitaryDutyStatus(String newValue)
    {
        this.setMilitaryDutyStatus(newValue,this.UserSecurityID);


    }
    public String getMilitaryDutyStatus()
    {
        return this.getMilitaryDutyStatus(this.UserSecurityID);
    }

    public void setMilitaryDutyStatus(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='MilitaryDutyStatus' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[MilitaryDutyStatus]=["+newValue+"]");
                   dbDB.setMilitaryDutyStatus(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[MilitaryDutyStatus]=["+newValue+"]");
           dbDB.setMilitaryDutyStatus(newValue);
         }
    }
    public String getMilitaryDutyStatus(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='MilitaryDutyStatus' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getMilitaryDutyStatus();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getMilitaryDutyStatus();
         }
        return myVal;
    }

    public void setMilitaryCurrentAssignment(String newValue)
    {
        this.setMilitaryCurrentAssignment(newValue,this.UserSecurityID);


    }
    public String getMilitaryCurrentAssignment()
    {
        return this.getMilitaryCurrentAssignment(this.UserSecurityID);
    }

    public void setMilitaryCurrentAssignment(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='MilitaryCurrentAssignment' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[MilitaryCurrentAssignment]=["+newValue+"]");
                   dbDB.setMilitaryCurrentAssignment(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[MilitaryCurrentAssignment]=["+newValue+"]");
           dbDB.setMilitaryCurrentAssignment(newValue);
         }
    }
    public String getMilitaryCurrentAssignment(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='MilitaryCurrentAssignment' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getMilitaryCurrentAssignment();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getMilitaryCurrentAssignment();
         }
        return myVal;
    }

    public void setRequiredFieldList(java.util.Hashtable requiredFieldList)
	 {

        this.requiredFieldList = requiredFieldList;
    }

    public java.util.Hashtable getRequiredFieldList()
    {
        return this.requiredFieldList;
    }
    public void setExpiredFieldList(java.util.Hashtable expiredFieldList)
    {
        this.expiredFieldList = expiredFieldList;
    }

    public void setEnglishFieldList(java.util.Hashtable englishFieldList)
    {
        this.englishFieldList = englishFieldList;
    }

    public java.util.Hashtable getExpiredFieldList()
    {
        return this.expiredFieldList;
    }
    public boolean isExpired(java.util.Date myDate, int daysVector)
    {
       return isExpired(myDate,new java.util.Date(),daysVector);
    }


    public boolean isRead(String fieldN, Integer GroupRefID)
    {
        boolean myVal = false;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='"+fieldN+"' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==1)
               {
                   myVal = true;
               }
            }
        }
        else
        {

           if (iSecurityCheck.intValue()>=1)
           {
                  myVal = true;
           }
        }
        return myVal;
    }


    public boolean isWrite(String fieldN, Integer GroupRefID)
    {
        boolean myVal = false;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='"+fieldN+"' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {
                   myVal = true;
               }
            }
        }
        else
        {

           if (iSecurityCheck.intValue()>=2)
           {
                  myVal = true;
           }
        }
        return myVal;
    }


    public boolean isExpired(java.util.Date myDate, java.util.Date refDate,int daysVector)
    {
        java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat("yyyy-MM-dd");
        boolean myVal = true;
        try
        {
            if (myDate.after(dbdf.parse("1800-01-10")))
            {
                if (myDate.after(dateChange(refDate,daysVector)))
                {
                    myVal=false;
                }
            }
            else
            {
                myVal = false;
            }
        }
        catch (Exception Datee)
        {
        }
        return myVal;
    }

    public java.util.Date dateChange(java.util.Date myD, int dayChange)
    {
        java.util.Calendar calendar = java.util.Calendar.getInstance();
        calendar.setTime(myD);
        calendar.add(java.util.Calendar.DATE, dayChange);
        java.util.Date resultAsDate = new java.util.Date(calendar.getTime().getTime());
        return resultAsDate;
    }

    public boolean isRequired(String fieldN)
    {
        return isRequired(fieldN, new Integer("1"));
    }

    public boolean isRequired(String fieldN, Integer GroupRefID)
    {
        boolean myVal = false;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='"+fieldN+"' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getRequiredID().intValue()==1)
               {
                   myVal = true;
               }
            }
        }
        else
        {
            if (this.getRequiredFieldList().containsKey(fieldN))
            {
                myVal = true;
            }
        }
        return myVal;
    }
    public boolean isExpiredCheck(String fieldN)
    {
        return isExpiredCheck(fieldN, new Integer ("1"));
    }

    public boolean isExpiredCheck(String fieldN, Integer GroupRefID)
    {
        boolean myVal = false;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='"+fieldN+"' AND TableName='tPhysicianMaster'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getTrackedID().intValue()==1)
               {
                   myVal = true;
               }
            }
        }
        else
        {
            if (this.getExpiredFieldList().containsKey(fieldN))
           {
                myVal = true;
            }
        }
        return myVal;
    }

    public void makeRequiredHash()
    {
       makeRequiredHash(new Integer(1));
    }

    public void makeRequiredHash(Integer GroupRefID)
    {

        java.util.Hashtable newHash = new java.util.Hashtable();
        if (runFieldSecurity)
        {
            java.util.Enumeration myKeys = englishFieldList.keys(); 
            while (myKeys.hasMoreElements()) 
            {
                 String myElement = (String) myKeys.nextElement();
                 bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='"+myElement+"' AND TableName='tPhysicianMaster'" ,"");
                 bltFieldSecurity        working_bltFieldSecurity;
                 ListElement         leCurrentElement;
                 Enumeration eList = myFSL.elements();
                 if (eList.hasMoreElements())
                 {
                     leCurrentElement    = (ListElement) eList.nextElement();
                     working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
                     if (working_bltFieldSecurity.getRequiredID().intValue()==1)
                     {
                         newHash.put(myElement,new Boolean(true));
                     }
                 }
            }
        }
        else
        {
            newHash.put("Salutation",new Boolean(true));
            newHash.put("Title",new Boolean(true));
            newHash.put("FirstName",new Boolean(true));
            newHash.put("LastName",new Boolean(true));
            newHash.put("HomeEmail",new Boolean(true));
            newHash.put("HomeAddress1",new Boolean(true));
            newHash.put("HomeCity",new Boolean(true));
            newHash.put("HomeZIP",new Boolean(true));
            newHash.put("HomeCountryID",new Boolean(true));
            newHash.put("DateOfBirth",new Boolean(true));
            newHash.put("CitizenshipYN",new Boolean(true));
            newHash.put("EligibleToWorkInUS",new Boolean(true));
            newHash.put("SSN",new Boolean(true));
            newHash.put("Gender",new Boolean(true));
            newHash.put("MilitaryActive",new Boolean(true));
            newHash.put("MilitaryReserve",new Boolean(true));
            newHash.put("HospitalAdmitingPrivileges",new Boolean(true));
            newHash.put("PhysicianCategoryID",new Boolean(true));
        }
        this.setRequiredFieldList(newHash);
    }

    public void makeEnglishHash()
    {

        java.util.Hashtable newHash = new java.util.Hashtable();        newHash.put("UniqueCreateDate","Item Create Date");
        newHash.put("UniqueModifyDate","Item Modify Date");
        newHash.put("UniqueModifyComments","Item Modification Comments");
        newHash.put("Salutation","Salutation");
        newHash.put("Title","Title");
        newHash.put("FirstName","First Name");
        newHash.put("MiddleName","Middle Name");
        newHash.put("LastName","Last Name");
        newHash.put("Suffix","Suffix");
        newHash.put("HomeEmail","Email Address");
        newHash.put("HomeAddress1","Home Mailing Address");
        newHash.put("HomeAddress2","Address 2");
        newHash.put("HomeCity","City,Town,Province");
        newHash.put("HomeStateID","State");
        newHash.put("HomeProvince","Province, District, State");
        newHash.put("HomeZIP","ZIP");
        newHash.put("HomeCountryID","Country");
        newHash.put("HomePhone","Home Phone Number (XXX-XXX-XXXX)");
        newHash.put("HomeFax","HomeFax (XXX-XXX-XXXX)");
        newHash.put("HomeMobile","Mobile Phone Number (XXX-XXX-XXXX)");
        newHash.put("HomePager","Pager Number (XXX-XXX-XXXX)");
        newHash.put("AlertEmail","Alert Email");
        newHash.put("AlertDays","Alert Days");
        newHash.put("OtherName1","Other Name Used #1");
        newHash.put("OtherName1Start","Other name Used #1 From Date");
        newHash.put("OtherName1End","Other name Used #1 To Date");
        newHash.put("OtherName2","Other Name Used #2");
        newHash.put("OtherName2Start","Other name Used #2 From Date");
        newHash.put("OtherName2End","Other name Used #2 To Date");
        newHash.put("DateOfBirth","Date of Birth");
        newHash.put("PlaceOfBirth","Place of Birth");
        newHash.put("Spouse","Spouse Full Name (First Middle Last)");
        newHash.put("CitizenshipYN","Are you a US Citizen?");
        newHash.put("Citizenship","If not, list your citizenship");
        newHash.put("VisaNumber","Visa Number");
        newHash.put("VisaStatus","Visa Type/Status");
        newHash.put("EligibleToWorkInUS","Are you eligible to work in the US?");
        newHash.put("SSN","Social Security Number");
        newHash.put("Gender","Gender");
        newHash.put("MilitaryActive","Are you currently active in the Military?");
        newHash.put("MilitaryBranch","If yes, which branch?");
        newHash.put("MilitaryReserve","Are you currently in the Military Reserves?");
        newHash.put("HospitalAdmitingPrivileges","Do you have hospital admiting privileges?");
        newHash.put("HospitalAdmitingPrivilegesNo","If no, what admiting arrangements do you have?");
        newHash.put("PhysicianCategoryID","Physician Category");
        newHash.put("IPAMedicalAffiliation","Are you currently affiliated with any other IPAs or Medical Groups?");
        newHash.put("AffiliationDesc1","If YES, please list below");
        newHash.put("PhysicianLanguages","Please list any languages that you speak (other than English)");
        newHash.put("ECFMGNo","ECFMG Number (if applicable)");
        newHash.put("ECFMGDateIssued","ECFMG Issue Date");
        newHash.put("ECFMGDateExpires","ECFMG Expiration Date");
        newHash.put("MedicareUPIN","Medicare UPIN");
        newHash.put("UniqueNPI","National Identification Number or NPI (if applicable)");
        newHash.put("MedicareParticipation","Are you a Participating Medicare Provider?");
        newHash.put("MedicareNo","Medicare Provider Number(s)");
        newHash.put("MediCaidParticipation","Are you a Participating Medicaid Provider?");
        newHash.put("MediCaidNo","Medicaid Provider Number(s)");
        newHash.put("SupplementalIDNumber1","Supplemental ID #1");
        newHash.put("SupplementalIDNumber2","Supplemental ID #2");
        newHash.put("Comments","Comments");
        newHash.put("AttestConsent","AttestConsent");
        newHash.put("AttestationComments","AttestationComments");
        newHash.put("IsViewable","IsViewable");
        newHash.put("IsAttested","IsAttested");
        newHash.put("AttestDatePending","AttestDatePending");
        newHash.put("IsAttestedPending","IsAttestedPending");
        newHash.put("AttestDate","AttestDate");
        newHash.put("DeAttestDate","DeAttestDate");
        newHash.put("LastModifiedDate","LastModifiedDate");
        newHash.put("QuestionValidation","QuestionValidation");
        newHash.put("USMLEDatePassedStep1","USMLE Date Passed Step 1");
        newHash.put("USMLEDatePassedStep2","USMLE Date Passed Step 2");
        newHash.put("USMLEDatePassedStep3","USMLE Date Passed Step 3");
        newHash.put("HaveFLEX","FLEX?");
        newHash.put("FLEXDatePassed","FLEX Date Passed");
        newHash.put("VisaSponsor","Visa Sponsor");
        newHash.put("VisaExpiration","VisaExpiration");
        newHash.put("CurrentVisaTemporary","Are you currently in the United States on a Temporary Visa (i.e., J-1, H-1, F-1)?");
        newHash.put("CurrentVisaExtended","Is your visa being extended to cover period of appointment?");
        newHash.put("HaveGreencard","Do you hold permanent immigrant status in the United States?");
        newHash.put("CountryOfIssueID","Country of Issue");
        newHash.put("VisaTemporary5Years","If not currently in the United States, have you been in the United States on a temporary visa within the past five years?");
        newHash.put("PastVisa1DateFrom","Past Visa 1 Date From");
        newHash.put("PastVisa1DateTo","Past Visa 1 Date To");
        newHash.put("PastVisa1Type","Past Visa 1 Type");
        newHash.put("PastVisa1Sponsor","Past Visa 1 Sponsor");
        newHash.put("PastVisa2DateFrom","Past Visa 2 Date From");
        newHash.put("PastVisa2DateTo","Past Visa 2 Date To");
        newHash.put("PastVisa2Type","Past Visa 2 Type");
        newHash.put("PastVisa2Sponsor","Past Visa 2 Sponsor");
        newHash.put("MilitaryRank","Military Rank");
        newHash.put("MilitaryDutyStatus","Military Duty Status");
        newHash.put("MilitaryCurrentAssignment","Military Current Assignment");

        this.setEnglishFieldList(newHash);
    }
    public void makeExpiredHash()
    {

        java.util.Hashtable newHash = new java.util.Hashtable();
        newHash.put("ECFMGDateExpires",new Boolean(true));
        this.setExpiredFieldList(newHash);
    }

    public java.util.Hashtable englishFieldList = new java.util.Hashtable();
    public java.util.Hashtable requiredFieldList = new java.util.Hashtable();
    public java.util.Hashtable expiredFieldList = new java.util.Hashtable();

    public boolean isComplete()
    {
	     boolean myVal = true;
	     java.util.Enumeration myKeys = this.getRequiredFieldList().keys(); 
	     while (myKeys.hasMoreElements()) 
	     {
	         String myElement = (String) myKeys.nextElement();
	         if (!isComplete(myElement))
	         {
	             myVal = false;
	         }
	     }
	     return myVal;
    }

    public String getEnglish(String fieldN)
    {
        String myVal = null;
        if (this.englishFieldList.containsKey(fieldN))
        {
            myVal = (String)this.englishFieldList.get(fieldN);
        }
        return myVal;
    }
    public java.util.Vector isComplete(int daysVector)
    {
	     boolean myVal = false;
	     java.util.Enumeration myKeys = this.getExpiredFieldList().keys(); 
	     java.util.Vector myExpiredList = new java.util.Vector(); 
	     while (myKeys.hasMoreElements()) 
	     {
	         String myElement = (String) myKeys.nextElement();
	         if (isExpired(myElement,daysVector))
	         {
	             myVal = true;
	             myExpiredList.addElement(myElement);
	         }
	     }
	     return myExpiredList;
    }

    public void setField(String fieldN, Object fieldV)
    {
     boolean myVal = false;
     try
     {
        java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat("yyyy-MM-dd");

        if (fieldN.equals("PhysicianID"))
        {
             this.setPhysicianID((Integer)fieldV);
        }

        else if (fieldN.equals("UniqueCreateDate"))
        {
            this.setUniqueCreateDate((java.util.Date)fieldV);
        }

        else if (fieldN.equals("UniqueModifyDate"))
        {
            this.setUniqueModifyDate((java.util.Date)fieldV);
        }

        else if (fieldN.equals("UniqueModifyComments"))
        {
            this.setUniqueModifyComments((String)fieldV);
        }

        else if (fieldN.equals("Salutation"))
        {
             this.setSalutation((Integer)fieldV);
        }

        else if (fieldN.equals("Title"))
        {
            this.setTitle((String)fieldV);
        }

        else if (fieldN.equals("FirstName"))
        {
            this.setFirstName((String)fieldV);
        }

        else if (fieldN.equals("MiddleName"))
        {
            this.setMiddleName((String)fieldV);
        }

        else if (fieldN.equals("LastName"))
        {
            this.setLastName((String)fieldV);
        }

        else if (fieldN.equals("Suffix"))
        {
            this.setSuffix((String)fieldV);
        }

        else if (fieldN.equals("HomeEmail"))
        {
            this.setHomeEmail((String)fieldV);
        }

        else if (fieldN.equals("HomeAddress1"))
        {
            this.setHomeAddress1((String)fieldV);
        }

        else if (fieldN.equals("HomeAddress2"))
        {
            this.setHomeAddress2((String)fieldV);
        }

        else if (fieldN.equals("HomeCity"))
        {
            this.setHomeCity((String)fieldV);
        }

        else if (fieldN.equals("HomeStateID"))
        {
             this.setHomeStateID((Integer)fieldV);
        }

        else if (fieldN.equals("HomeProvince"))
        {
            this.setHomeProvince((String)fieldV);
        }

        else if (fieldN.equals("HomeZIP"))
        {
            this.setHomeZIP((String)fieldV);
        }

        else if (fieldN.equals("HomeCountryID"))
        {
             this.setHomeCountryID((Integer)fieldV);
        }

        else if (fieldN.equals("HomePhone"))
        {
            this.setHomePhone((String)fieldV);
        }

        else if (fieldN.equals("HomeFax"))
        {
            this.setHomeFax((String)fieldV);
        }

        else if (fieldN.equals("HomeMobile"))
        {
            this.setHomeMobile((String)fieldV);
        }

        else if (fieldN.equals("HomePager"))
        {
            this.setHomePager((String)fieldV);
        }

        else if (fieldN.equals("AlertEmail"))
        {
            this.setAlertEmail((String)fieldV);
        }

        else if (fieldN.equals("AlertDays"))
        {
             this.setAlertDays((Integer)fieldV);
        }

        else if (fieldN.equals("OtherName1"))
        {
            this.setOtherName1((String)fieldV);
        }

        else if (fieldN.equals("OtherName1Start"))
        {
            this.setOtherName1Start((java.util.Date)fieldV);
        }

        else if (fieldN.equals("OtherName1End"))
        {
            this.setOtherName1End((java.util.Date)fieldV);
        }

        else if (fieldN.equals("OtherName2"))
        {
            this.setOtherName2((String)fieldV);
        }

        else if (fieldN.equals("OtherName2Start"))
        {
            this.setOtherName2Start((java.util.Date)fieldV);
        }

        else if (fieldN.equals("OtherName2End"))
        {
            this.setOtherName2End((java.util.Date)fieldV);
        }

        else if (fieldN.equals("DateOfBirth"))
        {
            this.setDateOfBirth((java.util.Date)fieldV);
        }

        else if (fieldN.equals("PlaceOfBirth"))
        {
            this.setPlaceOfBirth((String)fieldV);
        }

        else if (fieldN.equals("Spouse"))
        {
            this.setSpouse((String)fieldV);
        }

        else if (fieldN.equals("CitizenshipYN"))
        {
             this.setCitizenshipYN((Integer)fieldV);
        }

        else if (fieldN.equals("Citizenship"))
        {
            this.setCitizenship((String)fieldV);
        }

        else if (fieldN.equals("VisaNumber"))
        {
            this.setVisaNumber((String)fieldV);
        }

        else if (fieldN.equals("VisaStatus"))
        {
            this.setVisaStatus((String)fieldV);
        }

        else if (fieldN.equals("EligibleToWorkInUS"))
        {
             this.setEligibleToWorkInUS((Integer)fieldV);
        }

        else if (fieldN.equals("SSN"))
        {
            this.setSSN((String)fieldV);
        }

        else if (fieldN.equals("Gender"))
        {
             this.setGender((Integer)fieldV);
        }

        else if (fieldN.equals("MilitaryActive"))
        {
             this.setMilitaryActive((Integer)fieldV);
        }

        else if (fieldN.equals("MilitaryBranch"))
        {
            this.setMilitaryBranch((String)fieldV);
        }

        else if (fieldN.equals("MilitaryReserve"))
        {
             this.setMilitaryReserve((Integer)fieldV);
        }

        else if (fieldN.equals("HospitalAdmitingPrivileges"))
        {
             this.setHospitalAdmitingPrivileges((Integer)fieldV);
        }

        else if (fieldN.equals("HospitalAdmitingPrivilegesNo"))
        {
            this.setHospitalAdmitingPrivilegesNo((String)fieldV);
        }

        else if (fieldN.equals("PhysicianCategoryID"))
        {
             this.setPhysicianCategoryID((Integer)fieldV);
        }

        else if (fieldN.equals("IPAMedicalAffiliation"))
        {
             this.setIPAMedicalAffiliation((Integer)fieldV);
        }

        else if (fieldN.equals("AffiliationDesc1"))
        {
            this.setAffiliationDesc1((String)fieldV);
        }

        else if (fieldN.equals("PhysicianLanguages"))
        {
            this.setPhysicianLanguages((String)fieldV);
        }

        else if (fieldN.equals("ECFMGNo"))
        {
            this.setECFMGNo((String)fieldV);
        }

        else if (fieldN.equals("ECFMGDateIssued"))
        {
            this.setECFMGDateIssued((java.util.Date)fieldV);
        }

        else if (fieldN.equals("ECFMGDateExpires"))
        {
            this.setECFMGDateExpires((java.util.Date)fieldV);
        }

        else if (fieldN.equals("MedicareUPIN"))
        {
            this.setMedicareUPIN((String)fieldV);
        }

        else if (fieldN.equals("UniqueNPI"))
        {
            this.setUniqueNPI((String)fieldV);
        }

        else if (fieldN.equals("MedicareParticipation"))
        {
             this.setMedicareParticipation((Integer)fieldV);
        }

        else if (fieldN.equals("MedicareNo"))
        {
            this.setMedicareNo((String)fieldV);
        }

        else if (fieldN.equals("MediCaidParticipation"))
        {
             this.setMediCaidParticipation((Integer)fieldV);
        }

        else if (fieldN.equals("MediCaidNo"))
        {
            this.setMediCaidNo((String)fieldV);
        }

        else if (fieldN.equals("SupplementalIDNumber1"))
        {
            this.setSupplementalIDNumber1((String)fieldV);
        }

        else if (fieldN.equals("SupplementalIDNumber2"))
        {
            this.setSupplementalIDNumber2((String)fieldV);
        }

        else if (fieldN.equals("Comments"))
        {
            this.setComments((String)fieldV);
        }

        else if (fieldN.equals("AttestConsent"))
        {
             this.setAttestConsent((Integer)fieldV);
        }

        else if (fieldN.equals("AttestationComments"))
        {
            this.setAttestationComments((String)fieldV);
        }

        else if (fieldN.equals("IsViewable"))
        {
             this.setIsViewable((Integer)fieldV);
        }

        else if (fieldN.equals("IsAttested"))
        {
             this.setIsAttested((Integer)fieldV);
        }

        else if (fieldN.equals("AttestDatePending"))
        {
            this.setAttestDatePending((java.util.Date)fieldV);
        }

        else if (fieldN.equals("IsAttestedPending"))
        {
             this.setIsAttestedPending((Integer)fieldV);
        }

        else if (fieldN.equals("AttestDate"))
        {
            this.setAttestDate((java.util.Date)fieldV);
        }

        else if (fieldN.equals("DeAttestDate"))
        {
            this.setDeAttestDate((java.util.Date)fieldV);
        }

        else if (fieldN.equals("LastModifiedDate"))
        {
            this.setLastModifiedDate((java.util.Date)fieldV);
        }

        else if (fieldN.equals("QuestionValidation"))
        {
            this.setQuestionValidation((String)fieldV);
        }

        else if (fieldN.equals("USMLEDatePassedStep1"))
        {
            this.setUSMLEDatePassedStep1((java.util.Date)fieldV);
        }

        else if (fieldN.equals("USMLEDatePassedStep2"))
        {
            this.setUSMLEDatePassedStep2((java.util.Date)fieldV);
        }

        else if (fieldN.equals("USMLEDatePassedStep3"))
        {
            this.setUSMLEDatePassedStep3((java.util.Date)fieldV);
        }

        else if (fieldN.equals("HaveFLEX"))
        {
             this.setHaveFLEX((Integer)fieldV);
        }

        else if (fieldN.equals("FLEXDatePassed"))
        {
            this.setFLEXDatePassed((java.util.Date)fieldV);
        }

        else if (fieldN.equals("VisaSponsor"))
        {
            this.setVisaSponsor((String)fieldV);
        }

        else if (fieldN.equals("VisaExpiration"))
        {
            this.setVisaExpiration((java.util.Date)fieldV);
        }

        else if (fieldN.equals("CurrentVisaTemporary"))
        {
             this.setCurrentVisaTemporary((Integer)fieldV);
        }

        else if (fieldN.equals("CurrentVisaExtended"))
        {
             this.setCurrentVisaExtended((Integer)fieldV);
        }

        else if (fieldN.equals("HaveGreencard"))
        {
             this.setHaveGreencard((Integer)fieldV);
        }

        else if (fieldN.equals("CountryOfIssueID"))
        {
             this.setCountryOfIssueID((Integer)fieldV);
        }

        else if (fieldN.equals("VisaTemporary5Years"))
        {
             this.setVisaTemporary5Years((Integer)fieldV);
        }

        else if (fieldN.equals("PastVisa1DateFrom"))
        {
            this.setPastVisa1DateFrom((java.util.Date)fieldV);
        }

        else if (fieldN.equals("PastVisa1DateTo"))
        {
            this.setPastVisa1DateTo((java.util.Date)fieldV);
        }

        else if (fieldN.equals("PastVisa1Type"))
        {
            this.setPastVisa1Type((String)fieldV);
        }

        else if (fieldN.equals("PastVisa1Sponsor"))
        {
            this.setPastVisa1Sponsor((String)fieldV);
        }

        else if (fieldN.equals("PastVisa2DateFrom"))
        {
            this.setPastVisa2DateFrom((java.util.Date)fieldV);
        }

        else if (fieldN.equals("PastVisa2DateTo"))
        {
            this.setPastVisa2DateTo((java.util.Date)fieldV);
        }

        else if (fieldN.equals("PastVisa2Type"))
        {
            this.setPastVisa2Type((String)fieldV);
        }

        else if (fieldN.equals("PastVisa2Sponsor"))
        {
            this.setPastVisa2Sponsor((String)fieldV);
        }

        else if (fieldN.equals("MilitaryRank"))
        {
            this.setMilitaryRank((String)fieldV);
        }

        else if (fieldN.equals("MilitaryDutyStatus"))
        {
            this.setMilitaryDutyStatus((String)fieldV);
        }

        else if (fieldN.equals("MilitaryCurrentAssignment"))
        {
            this.setMilitaryCurrentAssignment((String)fieldV);
        }

	    }
	    catch(Exception e2)
	    {}
	    //return myVal;
	}

    public String getFieldType(String fieldN)
    {
     String  myVal = null;
     try
     {
        java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat("yyyy-MM-dd");

        if (fieldN.equals("PhysicianID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("UniqueCreateDate"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("UniqueModifyDate"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("UniqueModifyComments"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("Salutation"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("Title"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("FirstName"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("MiddleName"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("LastName"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("Suffix"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("HomeEmail"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("HomeAddress1"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("HomeAddress2"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("HomeCity"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("HomeStateID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("HomeProvince"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("HomeZIP"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("HomeCountryID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("HomePhone"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("HomeFax"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("HomeMobile"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("HomePager"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("AlertEmail"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("AlertDays"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("OtherName1"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("OtherName1Start"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("OtherName1End"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("OtherName2"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("OtherName2Start"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("OtherName2End"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("DateOfBirth"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("PlaceOfBirth"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("Spouse"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("CitizenshipYN"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("Citizenship"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("VisaNumber"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("VisaStatus"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("EligibleToWorkInUS"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("SSN"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("Gender"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("MilitaryActive"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("MilitaryBranch"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("MilitaryReserve"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("HospitalAdmitingPrivileges"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("HospitalAdmitingPrivilegesNo"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("PhysicianCategoryID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("IPAMedicalAffiliation"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("AffiliationDesc1"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("PhysicianLanguages"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("ECFMGNo"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("ECFMGDateIssued"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("ECFMGDateExpires"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("MedicareUPIN"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("UniqueNPI"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("MedicareParticipation"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("MedicareNo"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("MediCaidParticipation"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("MediCaidNo"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("SupplementalIDNumber1"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("SupplementalIDNumber2"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("Comments"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("AttestConsent"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("AttestationComments"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("IsViewable"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("IsAttested"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("AttestDatePending"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("IsAttestedPending"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("AttestDate"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("DeAttestDate"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("LastModifiedDate"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("QuestionValidation"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("USMLEDatePassedStep1"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("USMLEDatePassedStep2"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("USMLEDatePassedStep3"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("HaveFLEX"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("FLEXDatePassed"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("VisaSponsor"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("VisaExpiration"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("CurrentVisaTemporary"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("CurrentVisaExtended"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("HaveGreencard"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("CountryOfIssueID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("VisaTemporary5Years"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("PastVisa1DateFrom"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("PastVisa1DateTo"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("PastVisa1Type"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("PastVisa1Sponsor"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("PastVisa2DateFrom"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("PastVisa2DateTo"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("PastVisa2Type"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("PastVisa2Sponsor"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("MilitaryRank"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("MilitaryDutyStatus"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("MilitaryCurrentAssignment"))
        {
            myVal = "String";
        }

	    }
	    catch(Exception e2)
	    {}
	    return myVal;
	}

    public boolean isComplete(String fieldN)
    {
	     boolean myVal = false;
	     try
	     {
        java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat("yyyy-MM-dd");

	     if (fieldN.equals("PhysicianID"))

	     {
                    if (this.getPhysicianID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("UniqueCreateDate"))

	     {
                    if (this.getUniqueCreateDate().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("UniqueModifyDate"))

	     {
                    if (this.getUniqueModifyDate().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("UniqueModifyComments"))

	     {
                    if (this.getUniqueModifyComments().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("Salutation"))

	     {
                    if (this.getSalutation().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("Title"))

	     {
                    if (this.getTitle().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("FirstName"))

	     {
                    if (this.getFirstName().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("MiddleName"))

	     {
                    if (this.getMiddleName().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("LastName"))

	     {
                    if (this.getLastName().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("Suffix"))

	     {
                    if (this.getSuffix().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("HomeEmail"))

	     {
                    if (this.getHomeEmail().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("HomeAddress1"))

	     {
                    if (this.getHomeAddress1().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("HomeAddress2"))

	     {
                    if (this.getHomeAddress2().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("HomeCity"))

	     {
                    if (this.getHomeCity().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("HomeStateID"))

	     {
                    if (this.getHomeStateID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("HomeProvince"))

	     {
                    if (this.getHomeProvince().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("HomeZIP"))

	     {
                    if (this.getHomeZIP().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("HomeCountryID"))

	     {
                    if (this.getHomeCountryID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("HomePhone"))

	     {
                    if (this.getHomePhone().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("HomeFax"))

	     {
                    if (this.getHomeFax().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("HomeMobile"))

	     {
                    if (this.getHomeMobile().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("HomePager"))

	     {
                    if (this.getHomePager().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("AlertEmail"))

	     {
                    if (this.getAlertEmail().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("AlertDays"))

	     {
                    if (this.getAlertDays().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("OtherName1"))

	     {
                    if (this.getOtherName1().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("OtherName1Start"))

	     {
                    if (this.getOtherName1Start().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("OtherName1End"))

	     {
                    if (this.getOtherName1End().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("OtherName2"))

	     {
                    if (this.getOtherName2().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("OtherName2Start"))

	     {
                    if (this.getOtherName2Start().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("OtherName2End"))

	     {
                    if (this.getOtherName2End().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("DateOfBirth"))

	     {
                    if (this.getDateOfBirth().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("PlaceOfBirth"))

	     {
                    if (this.getPlaceOfBirth().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("Spouse"))

	     {
                    if (this.getSpouse().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("CitizenshipYN"))

	     {
                    if (this.getCitizenshipYN().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("Citizenship"))

	     {
                    if (this.getCitizenship().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("VisaNumber"))

	     {
                    if (this.getVisaNumber().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("VisaStatus"))

	     {
                    if (this.getVisaStatus().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("EligibleToWorkInUS"))

	     {
                    if (this.getEligibleToWorkInUS().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("SSN"))

	     {
                    if (this.getSSN().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("Gender"))

	     {
                    if (this.getGender().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("MilitaryActive"))

	     {
                    if (this.getMilitaryActive().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("MilitaryBranch"))

	     {
                    if (this.getMilitaryBranch().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("MilitaryReserve"))

	     {
                    if (this.getMilitaryReserve().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("HospitalAdmitingPrivileges"))

	     {
                    if (this.getHospitalAdmitingPrivileges().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("HospitalAdmitingPrivilegesNo"))

	     {
                    if (this.getHospitalAdmitingPrivilegesNo().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("PhysicianCategoryID"))

	     {
                    if (this.getPhysicianCategoryID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("IPAMedicalAffiliation"))

	     {
                    if (this.getIPAMedicalAffiliation().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("AffiliationDesc1"))

	     {
                    if (this.getAffiliationDesc1().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("PhysicianLanguages"))

	     {
                    if (this.getPhysicianLanguages().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("ECFMGNo"))

	     {
                    if (this.getECFMGNo().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("ECFMGDateIssued"))

	     {
                    if (this.getECFMGDateIssued().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("ECFMGDateExpires"))

	     {
                    if (this.getECFMGDateExpires().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("MedicareUPIN"))

	     {
                    if (this.getMedicareUPIN().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("UniqueNPI"))

	     {
                    if (this.getUniqueNPI().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("MedicareParticipation"))

	     {
                    if (this.getMedicareParticipation().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("MedicareNo"))

	     {
                    if (this.getMedicareNo().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("MediCaidParticipation"))

	     {
                    if (this.getMediCaidParticipation().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("MediCaidNo"))

	     {
                    if (this.getMediCaidNo().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("SupplementalIDNumber1"))

	     {
                    if (this.getSupplementalIDNumber1().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("SupplementalIDNumber2"))

	     {
                    if (this.getSupplementalIDNumber2().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("Comments"))

	     {
                    if (this.getComments().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("AttestConsent"))

	     {
                    if (this.getAttestConsent().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("AttestationComments"))

	     {
                    if (this.getAttestationComments().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("IsViewable"))

	     {
                    if (this.getIsViewable().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("IsAttested"))

	     {
                    if (this.getIsAttested().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("AttestDatePending"))

	     {
                    if (this.getAttestDatePending().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("IsAttestedPending"))

	     {
                    if (this.getIsAttestedPending().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("AttestDate"))

	     {
                    if (this.getAttestDate().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("DeAttestDate"))

	     {
                    if (this.getDeAttestDate().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("LastModifiedDate"))

	     {
                    if (this.getLastModifiedDate().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("QuestionValidation"))

	     {
                    if (this.getQuestionValidation().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("USMLEDatePassedStep1"))

	     {
                    if (this.getUSMLEDatePassedStep1().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("USMLEDatePassedStep2"))

	     {
                    if (this.getUSMLEDatePassedStep2().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("USMLEDatePassedStep3"))

	     {
                    if (this.getUSMLEDatePassedStep3().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("HaveFLEX"))

	     {
                    if (this.getHaveFLEX().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("FLEXDatePassed"))

	     {
                    if (this.getFLEXDatePassed().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("VisaSponsor"))

	     {
                    if (this.getVisaSponsor().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("VisaExpiration"))

	     {
                    if (this.getVisaExpiration().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("CurrentVisaTemporary"))

	     {
                    if (this.getCurrentVisaTemporary().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("CurrentVisaExtended"))

	     {
                    if (this.getCurrentVisaExtended().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("HaveGreencard"))

	     {
                    if (this.getHaveGreencard().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("CountryOfIssueID"))

	     {
                    if (this.getCountryOfIssueID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("VisaTemporary5Years"))

	     {
                    if (this.getVisaTemporary5Years().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("PastVisa1DateFrom"))

	     {
                    if (this.getPastVisa1DateFrom().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("PastVisa1DateTo"))

	     {
                    if (this.getPastVisa1DateTo().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("PastVisa1Type"))

	     {
                    if (this.getPastVisa1Type().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("PastVisa1Sponsor"))

	     {
                    if (this.getPastVisa1Sponsor().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("PastVisa2DateFrom"))

	     {
                    if (this.getPastVisa2DateFrom().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("PastVisa2DateTo"))

	     {
                    if (this.getPastVisa2DateTo().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("PastVisa2Type"))

	     {
                    if (this.getPastVisa2Type().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("PastVisa2Sponsor"))

	     {
                    if (this.getPastVisa2Sponsor().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("MilitaryRank"))

	     {
                    if (this.getMilitaryRank().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("MilitaryDutyStatus"))

	     {
                    if (this.getMilitaryDutyStatus().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("MilitaryCurrentAssignment"))

	     {
                    if (this.getMilitaryCurrentAssignment().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	    }
	    catch(Exception e2)
	    {}
	    return myVal;
	}

    public boolean isExpired(String fieldN,int daysVector)
    {
        return isExpired(fieldN,new java.util.Date(),daysVector);
    }

    public boolean isExpired(String fieldN,java.util.Date refDate,int daysVector)
    {
	     boolean myVal = false;
	     try
	     {
        java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat("yyyy-MM-dd");

        if (fieldN.equals("UniqueCreateDate"))

	     {
                    if (this.isExpired(this.getUniqueCreateDate(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

        if (fieldN.equals("UniqueModifyDate"))

	     {
                    if (this.isExpired(this.getUniqueModifyDate(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

        if (fieldN.equals("OtherName1Start"))

	     {
                    if (this.isExpired(this.getOtherName1Start(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

        if (fieldN.equals("OtherName1End"))

	     {
                    if (this.isExpired(this.getOtherName1End(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

        if (fieldN.equals("OtherName2Start"))

	     {
                    if (this.isExpired(this.getOtherName2Start(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

        if (fieldN.equals("OtherName2End"))

	     {
                    if (this.isExpired(this.getOtherName2End(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

        if (fieldN.equals("DateOfBirth"))

	     {
                    if (this.isExpired(this.getDateOfBirth(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

        if (fieldN.equals("ECFMGDateIssued"))

	     {
                    if (this.isExpired(this.getECFMGDateIssued(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

        if (fieldN.equals("ECFMGDateExpires"))

	     {
                    if (this.isExpired(this.getECFMGDateExpires(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

        if (fieldN.equals("AttestDatePending"))

	     {
                    if (this.isExpired(this.getAttestDatePending(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

        if (fieldN.equals("AttestDate"))

	     {
                    if (this.isExpired(this.getAttestDate(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

        if (fieldN.equals("DeAttestDate"))

	     {
                    if (this.isExpired(this.getDeAttestDate(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

        if (fieldN.equals("LastModifiedDate"))

	     {
                    if (this.isExpired(this.getLastModifiedDate(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

        if (fieldN.equals("USMLEDatePassedStep1"))

	     {
                    if (this.isExpired(this.getUSMLEDatePassedStep1(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

        if (fieldN.equals("USMLEDatePassedStep2"))

	     {
                    if (this.isExpired(this.getUSMLEDatePassedStep2(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

        if (fieldN.equals("USMLEDatePassedStep3"))

	     {
                    if (this.isExpired(this.getUSMLEDatePassedStep3(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

        if (fieldN.equals("FLEXDatePassed"))

	     {
                    if (this.isExpired(this.getFLEXDatePassed(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

        if (fieldN.equals("VisaExpiration"))

	     {
                    if (this.isExpired(this.getVisaExpiration(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

        if (fieldN.equals("PastVisa1DateFrom"))

	     {
                    if (this.isExpired(this.getPastVisa1DateFrom(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

        if (fieldN.equals("PastVisa1DateTo"))

	     {
                    if (this.isExpired(this.getPastVisa1DateTo(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

        if (fieldN.equals("PastVisa2DateFrom"))

	     {
                    if (this.isExpired(this.getPastVisa2DateFrom(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

        if (fieldN.equals("PastVisa2DateTo"))

	     {
                    if (this.isExpired(this.getPastVisa2DateTo(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

	    }
	    catch(Exception e2)
	    {}
	    return myVal;
	}

    public java.util.Date getDateField(String fieldN)
    {
	     java.util.Date myVal = null;
	     try
	     {
        java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat("yyyy-MM-dd");

        if (fieldN.equals("UniqueCreateDate"))

	     {
             myVal = this.getUniqueCreateDate();
        }

        if (fieldN.equals("UniqueModifyDate"))

	     {
             myVal = this.getUniqueModifyDate();
        }

        if (fieldN.equals("OtherName1Start"))

	     {
             myVal = this.getOtherName1Start();
        }

        if (fieldN.equals("OtherName1End"))

	     {
             myVal = this.getOtherName1End();
        }

        if (fieldN.equals("OtherName2Start"))

	     {
             myVal = this.getOtherName2Start();
        }

        if (fieldN.equals("OtherName2End"))

	     {
             myVal = this.getOtherName2End();
        }

        if (fieldN.equals("DateOfBirth"))

	     {
             myVal = this.getDateOfBirth();
        }

        if (fieldN.equals("ECFMGDateIssued"))

	     {
             myVal = this.getECFMGDateIssued();
        }

        if (fieldN.equals("ECFMGDateExpires"))

	     {
             myVal = this.getECFMGDateExpires();
        }

        if (fieldN.equals("AttestDatePending"))

	     {
             myVal = this.getAttestDatePending();
        }

        if (fieldN.equals("AttestDate"))

	     {
             myVal = this.getAttestDate();
        }

        if (fieldN.equals("DeAttestDate"))

	     {
             myVal = this.getDeAttestDate();
        }

        if (fieldN.equals("LastModifiedDate"))

	     {
             myVal = this.getLastModifiedDate();
        }

        if (fieldN.equals("USMLEDatePassedStep1"))

	     {
             myVal = this.getUSMLEDatePassedStep1();
        }

        if (fieldN.equals("USMLEDatePassedStep2"))

	     {
             myVal = this.getUSMLEDatePassedStep2();
        }

        if (fieldN.equals("USMLEDatePassedStep3"))

	     {
             myVal = this.getUSMLEDatePassedStep3();
        }

        if (fieldN.equals("FLEXDatePassed"))

	     {
             myVal = this.getFLEXDatePassed();
        }

        if (fieldN.equals("VisaExpiration"))

	     {
             myVal = this.getVisaExpiration();
        }

        if (fieldN.equals("PastVisa1DateFrom"))

	     {
             myVal = this.getPastVisa1DateFrom();
        }

        if (fieldN.equals("PastVisa1DateTo"))

	     {
             myVal = this.getPastVisa1DateTo();
        }

        if (fieldN.equals("PastVisa2DateFrom"))

	     {
             myVal = this.getPastVisa2DateFrom();
        }

        if (fieldN.equals("PastVisa2DateTo"))

	     {
             myVal = this.getPastVisa2DateTo();
        }

	    }
	    catch(Exception e2)
	    {}
	    return myVal;
	}
	public boolean runFieldSecurity = false;
	public Integer UserSecurityID = null;
   public Integer iSecurityCheck = null;
}    // End of bltPhysicianMaster class definition
