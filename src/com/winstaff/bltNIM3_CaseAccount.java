

package com.winstaff;

import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;
import java.util.Enumeration;

public class bltNIM3_CaseAccount extends Object implements InttNIM3_CaseAccount
{

    dbtNIM3_CaseAccount    dbDB;

    public bltNIM3_CaseAccount()
    {
        dbDB = new dbtNIM3_CaseAccount();
        dbDB.setUniqueCreateDate(new java.util.Date());
        makeRequiredHash();
        makeExpiredHash();
        makeEnglishHash();

    }    // End of default constructor

    public bltNIM3_CaseAccount( Integer iNewID )
    {        dbDB = new dbtNIM3_CaseAccount( iNewID );
        makeRequiredHash();
        makeExpiredHash();
        makeEnglishHash();
        iSecurityCheck = new Integer(2);
    }    // End of Constructor knowing an ID


    public bltNIM3_CaseAccount( Integer iNewID, Integer iGroupSecurityID )
    {
        this.GroupSecurityInit(iGroupSecurityID);
        dbDB = new dbtNIM3_CaseAccount( iNewID );
        makeExpiredHash();
        makeEnglishHash();
        makeRequiredHash(iGroupSecurityID);
    }    // End of Constructor knowing an ID

    public bltNIM3_CaseAccount( Integer iGroupSecurityID, boolean isTrue )
    {
        this.GroupSecurityInit(iGroupSecurityID);
        dbDB = new dbtNIM3_CaseAccount(  );
        makeExpiredHash();
        makeEnglishHash();
        makeRequiredHash(iGroupSecurityID);
    }    // End of Security unknown ID

    public void GroupSecurityInit( Integer iGroupSecurityID )
    {
        bltSecurityGroupMaster mySGM = new bltSecurityGroupMaster(iGroupSecurityID);
        this.UserSecurityID = iGroupSecurityID;
        if (mySGM.getRunFieldSecurityID().intValue()==1)
        {
            this.runFieldSecurity = true;
            makeRequiredHash(iGroupSecurityID);
        }
        iSecurityCheck = SecurityCheck.CheckItem("nim1", iGroupSecurityID);
    }  

    public EventReferenceType getEventReference()
    {
        return EventUtils.getEventReference("tNIM3_CaseAccount", this.getUniqueID());
    }  

    public String convertString2LogString(String normalString) 
    {
        String myVal = "";
        for (int i=0;i<normalString.length();i++)
        {
            if ( (normalString.charAt(i)!='\r')&&(normalString.charAt(i)!='\n')&&(normalString.charAt(i)!='\t') )
            {
                myVal+=normalString.charAt(i);
            }
            else
            {
                myVal+="<NLT>";
            }
        }
        return myVal;
    }
    public void runAudit()
    {
        String AuditString = new java.util.Date()+" \t " ;  
        AuditString += " tNIM3_CaseAccount "; 
        AuditString += " CaseID ="+this.getUniqueID(); 
        AuditString += " PayerID ="+this.getPayerID(); 
        for (int i=0;i<this.AuditVector.size();i++)
        {
            AuditString+="\t"+convertString2LogString((String)this.AuditVector.elementAt(i));
        }
        AuditLogger.printLine(AuditString);
    }
    public void commitData() throws SQLException
    {
        runAudit();
        dbDB.setModifyComments(this.getUniqueModifyComments());
        dbDB.setUniqueModifyDate(new java.util.Date());
        dbDB.commitData();
        DataControlUtils.dataChanged("tNIM3_CaseAccount",this.getPayerID() ,this.AuditVector.size(), "PayerID");
        this.AuditVector.removeAllElements();
    }    // End of commitData()

    public void commitDataForced() throws SQLException
    {
        dbDB.commitDataForced();
    }    // End of commitData()

    public Integer getUniqueID()
    {
        return dbDB.getUniqueID();
    }    // End of getUniqueID()

    public void setUniqueID( Integer iNewID )
    {
        dbDB.setUniqueID( iNewID );
    }    // End of setUniqueID()


    public void setCaseID(Integer newValue)
    {
        dbDB.setCaseID(newValue);
    }

    public Integer getCaseID()
    {
        return dbDB.getCaseID();
    }

    java.util.Vector AuditVector = new java.util.Vector();

    public void setUniqueCreateDate(Date newValue)
    {
        this.setUniqueCreateDate(newValue,this.UserSecurityID);


    }
    public Date getUniqueCreateDate()
    {
        return this.getUniqueCreateDate(this.UserSecurityID);
    }

    public void setUniqueCreateDate(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='UniqueCreateDate' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[UniqueCreateDate]=["+newValue+"]");
                   dbDB.setUniqueCreateDate(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[UniqueCreateDate]=["+newValue+"]");
           dbDB.setUniqueCreateDate(newValue);
         }
    }
    public Date getUniqueCreateDate(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='UniqueCreateDate' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getUniqueCreateDate();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getUniqueCreateDate();
         }
        return myVal;
    }

    public void setUniqueModifyDate(Date newValue)
    {
        this.setUniqueModifyDate(newValue,this.UserSecurityID);


    }
    public Date getUniqueModifyDate()
    {
        return this.getUniqueModifyDate(this.UserSecurityID);
    }

    public void setUniqueModifyDate(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='UniqueModifyDate' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[UniqueModifyDate]=["+newValue+"]");
                   dbDB.setUniqueModifyDate(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[UniqueModifyDate]=["+newValue+"]");
           dbDB.setUniqueModifyDate(newValue);
         }
    }
    public Date getUniqueModifyDate(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='UniqueModifyDate' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getUniqueModifyDate();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getUniqueModifyDate();
         }
        return myVal;
    }

    public void setUniqueModifyComments(String newValue)
    {
        this.setUniqueModifyComments(newValue,this.UserSecurityID);


    }
    public String getUniqueModifyComments()
    {
        return this.getUniqueModifyComments(this.UserSecurityID);
    }

    public void setUniqueModifyComments(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='UniqueModifyComments' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[UniqueModifyComments]=["+newValue+"]");
                   dbDB.setUniqueModifyComments(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[UniqueModifyComments]=["+newValue+"]");
           dbDB.setUniqueModifyComments(newValue);
         }
    }
    public String getUniqueModifyComments(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='UniqueModifyComments' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getUniqueModifyComments();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getUniqueModifyComments();
         }
        return myVal;
    }

    public void setPayerID(Integer newValue)
    {
        this.setPayerID(newValue,this.UserSecurityID);


    }
    public Integer getPayerID()
    {
        return this.getPayerID(this.UserSecurityID);
    }

    public void setPayerID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PayerID' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[PayerID]=["+newValue+"]");
                   dbDB.setPayerID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[PayerID]=["+newValue+"]");
           dbDB.setPayerID(newValue);
         }
    }
    public Integer getPayerID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PayerID' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPayerID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPayerID();
         }
        return myVal;
    }

    public void setCaseClaimNumber(String newValue)
    {
        this.setCaseClaimNumber(newValue,this.UserSecurityID);


    }
    public String getCaseClaimNumber()
    {
        return this.getCaseClaimNumber(this.UserSecurityID);
    }

    public void setCaseClaimNumber(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CaseClaimNumber' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[CaseClaimNumber]=["+newValue+"]");
                   dbDB.setCaseClaimNumber(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[CaseClaimNumber]=["+newValue+"]");
           dbDB.setCaseClaimNumber(newValue);
         }
    }
    public String getCaseClaimNumber(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CaseClaimNumber' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getCaseClaimNumber();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getCaseClaimNumber();
         }
        return myVal;
    }

    public void setCaseCode(String newValue)
    {
        this.setCaseCode(newValue,this.UserSecurityID);


    }
    public String getCaseCode()
    {
        return this.getCaseCode(this.UserSecurityID);
    }

    public void setCaseCode(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CaseCode' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[CaseCode]=["+newValue+"]");
                   dbDB.setCaseCode(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[CaseCode]=["+newValue+"]");
           dbDB.setCaseCode(newValue);
         }
    }
    public String getCaseCode(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CaseCode' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getCaseCode();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getCaseCode();
         }
        return myVal;
    }

    public void setWCBNumber(String newValue)
    {
        this.setWCBNumber(newValue,this.UserSecurityID);


    }
    public String getWCBNumber()
    {
        return this.getWCBNumber(this.UserSecurityID);
    }

    public void setWCBNumber(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='WCBNumber' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[WCBNumber]=["+newValue+"]");
                   dbDB.setWCBNumber(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[WCBNumber]=["+newValue+"]");
           dbDB.setWCBNumber(newValue);
         }
    }
    public String getWCBNumber(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='WCBNumber' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getWCBNumber();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getWCBNumber();
         }
        return myVal;
    }

    public void setCaseStatusID(Integer newValue)
    {
        this.setCaseStatusID(newValue,this.UserSecurityID);


    }
    public Integer getCaseStatusID()
    {
        return this.getCaseStatusID(this.UserSecurityID);
    }

    public void setCaseStatusID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CaseStatusID' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[CaseStatusID]=["+newValue+"]");
                   dbDB.setCaseStatusID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[CaseStatusID]=["+newValue+"]");
           dbDB.setCaseStatusID(newValue);
         }
    }
    public Integer getCaseStatusID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CaseStatusID' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getCaseStatusID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getCaseStatusID();
         }
        return myVal;
    }

    public void setAdjusterID(Integer newValue)
    {
        this.setAdjusterID(newValue,this.UserSecurityID);


    }
    public Integer getAdjusterID()
    {
        return this.getAdjusterID(this.UserSecurityID);
    }

    public void setAdjusterID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AdjusterID' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[AdjusterID]=["+newValue+"]");
                   dbDB.setAdjusterID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[AdjusterID]=["+newValue+"]");
           dbDB.setAdjusterID(newValue);
         }
    }
    public Integer getAdjusterID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AdjusterID' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getAdjusterID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getAdjusterID();
         }
        return myVal;
    }

    public void setOccMedPatientID(String newValue)
    {
        this.setOccMedPatientID(newValue,this.UserSecurityID);


    }
    public String getOccMedPatientID()
    {
        return this.getOccMedPatientID(this.UserSecurityID);
    }

    public void setOccMedPatientID(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='OccMedPatientID' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[OccMedPatientID]=["+newValue+"]");
                   dbDB.setOccMedPatientID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[OccMedPatientID]=["+newValue+"]");
           dbDB.setOccMedPatientID(newValue);
         }
    }
    public String getOccMedPatientID(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='OccMedPatientID' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getOccMedPatientID();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getOccMedPatientID();
         }
        return myVal;
    }

    public void setEmployerID(Integer newValue)
    {
        this.setEmployerID(newValue,this.UserSecurityID);


    }
    public Integer getEmployerID()
    {
        return this.getEmployerID(this.UserSecurityID);
    }

    public void setEmployerID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='EmployerID' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[EmployerID]=["+newValue+"]");
                   dbDB.setEmployerID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[EmployerID]=["+newValue+"]");
           dbDB.setEmployerID(newValue);
         }
    }
    public Integer getEmployerID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='EmployerID' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getEmployerID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getEmployerID();
         }
        return myVal;
    }

    public void setEmployerName(String newValue)
    {
        this.setEmployerName(newValue,this.UserSecurityID);


    }
    public String getEmployerName()
    {
        return this.getEmployerName(this.UserSecurityID);
    }

    public void setEmployerName(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='EmployerName' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[EmployerName]=["+newValue+"]");
                   dbDB.setEmployerName(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[EmployerName]=["+newValue+"]");
           dbDB.setEmployerName(newValue);
         }
    }
    public String getEmployerName(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='EmployerName' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getEmployerName();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getEmployerName();
         }
        return myVal;
    }

    public void setEmployerPhone(String newValue)
    {
        this.setEmployerPhone(newValue,this.UserSecurityID);


    }
    public String getEmployerPhone()
    {
        return this.getEmployerPhone(this.UserSecurityID);
    }

    public void setEmployerPhone(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='EmployerPhone' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[EmployerPhone]=["+newValue+"]");
                   dbDB.setEmployerPhone(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[EmployerPhone]=["+newValue+"]");
           dbDB.setEmployerPhone(newValue);
         }
    }
    public String getEmployerPhone(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='EmployerPhone' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getEmployerPhone();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getEmployerPhone();
         }
        return myVal;
    }

    public void setEmployerFax(String newValue)
    {
        this.setEmployerFax(newValue,this.UserSecurityID);


    }
    public String getEmployerFax()
    {
        return this.getEmployerFax(this.UserSecurityID);
    }

    public void setEmployerFax(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='EmployerFax' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[EmployerFax]=["+newValue+"]");
                   dbDB.setEmployerFax(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[EmployerFax]=["+newValue+"]");
           dbDB.setEmployerFax(newValue);
         }
    }
    public String getEmployerFax(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='EmployerFax' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getEmployerFax();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getEmployerFax();
         }
        return myVal;
    }

    public void setJobID(Integer newValue)
    {
        this.setJobID(newValue,this.UserSecurityID);


    }
    public Integer getJobID()
    {
        return this.getJobID(this.UserSecurityID);
    }

    public void setJobID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='JobID' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[JobID]=["+newValue+"]");
                   dbDB.setJobID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[JobID]=["+newValue+"]");
           dbDB.setJobID(newValue);
         }
    }
    public Integer getJobID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='JobID' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getJobID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getJobID();
         }
        return myVal;
    }

    public void setJobNotes(String newValue)
    {
        this.setJobNotes(newValue,this.UserSecurityID);


    }
    public String getJobNotes()
    {
        return this.getJobNotes(this.UserSecurityID);
    }

    public void setJobNotes(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='JobNotes' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[JobNotes]=["+newValue+"]");
                   dbDB.setJobNotes(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[JobNotes]=["+newValue+"]");
           dbDB.setJobNotes(newValue);
         }
    }
    public String getJobNotes(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='JobNotes' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getJobNotes();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getJobNotes();
         }
        return myVal;
    }

    public void setPayerAccountIDNumber(String newValue)
    {
        this.setPayerAccountIDNumber(newValue,this.UserSecurityID);


    }
    public String getPayerAccountIDNumber()
    {
        return this.getPayerAccountIDNumber(this.UserSecurityID);
    }

    public void setPayerAccountIDNumber(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PayerAccountIDNumber' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[PayerAccountIDNumber]=["+newValue+"]");
                   dbDB.setPayerAccountIDNumber(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[PayerAccountIDNumber]=["+newValue+"]");
           dbDB.setPayerAccountIDNumber(newValue);
         }
    }
    public String getPayerAccountIDNumber(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PayerAccountIDNumber' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPayerAccountIDNumber();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPayerAccountIDNumber();
         }
        return myVal;
    }

    public void setPatientFirstName(String newValue)
    {
        this.setPatientFirstName(newValue,this.UserSecurityID);


    }
    public String getPatientFirstName()
    {
        return this.getPatientFirstName(this.UserSecurityID);
    }

    public void setPatientFirstName(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientFirstName' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[PatientFirstName]=["+newValue+"]");
                   dbDB.setPatientFirstName(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[PatientFirstName]=["+newValue+"]");
           dbDB.setPatientFirstName(newValue);
         }
    }
    public String getPatientFirstName(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientFirstName' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPatientFirstName();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPatientFirstName();
         }
        return myVal;
    }

    public void setPatientLastName(String newValue)
    {
        this.setPatientLastName(newValue,this.UserSecurityID);


    }
    public String getPatientLastName()
    {
        return this.getPatientLastName(this.UserSecurityID);
    }

    public void setPatientLastName(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientLastName' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[PatientLastName]=["+newValue+"]");
                   dbDB.setPatientLastName(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[PatientLastName]=["+newValue+"]");
           dbDB.setPatientLastName(newValue);
         }
    }
    public String getPatientLastName(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientLastName' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPatientLastName();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPatientLastName();
         }
        return myVal;
    }

    public void setPatientAccountNumber(String newValue)
    {
        this.setPatientAccountNumber(newValue,this.UserSecurityID);


    }
    public String getPatientAccountNumber()
    {
        return this.getPatientAccountNumber(this.UserSecurityID);
    }

    public void setPatientAccountNumber(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientAccountNumber' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[PatientAccountNumber]=["+newValue+"]");
                   dbDB.setPatientAccountNumber(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[PatientAccountNumber]=["+newValue+"]");
           dbDB.setPatientAccountNumber(newValue);
         }
    }
    public String getPatientAccountNumber(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientAccountNumber' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPatientAccountNumber();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPatientAccountNumber();
         }
        return myVal;
    }

    public void setPatientExpirationDate(Date newValue)
    {
        this.setPatientExpirationDate(newValue,this.UserSecurityID);


    }
    public Date getPatientExpirationDate()
    {
        return this.getPatientExpirationDate(this.UserSecurityID);
    }

    public void setPatientExpirationDate(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientExpirationDate' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[PatientExpirationDate]=["+newValue+"]");
                   dbDB.setPatientExpirationDate(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[PatientExpirationDate]=["+newValue+"]");
           dbDB.setPatientExpirationDate(newValue);
         }
    }
    public Date getPatientExpirationDate(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientExpirationDate' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPatientExpirationDate();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPatientExpirationDate();
         }
        return myVal;
    }

    public void setPatientIsActive(Integer newValue)
    {
        this.setPatientIsActive(newValue,this.UserSecurityID);


    }
    public Integer getPatientIsActive()
    {
        return this.getPatientIsActive(this.UserSecurityID);
    }

    public void setPatientIsActive(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientIsActive' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[PatientIsActive]=["+newValue+"]");
                   dbDB.setPatientIsActive(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[PatientIsActive]=["+newValue+"]");
           dbDB.setPatientIsActive(newValue);
         }
    }
    public Integer getPatientIsActive(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientIsActive' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPatientIsActive();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPatientIsActive();
         }
        return myVal;
    }

    public void setPatientDOB(Date newValue)
    {
        this.setPatientDOB(newValue,this.UserSecurityID);


    }
    public Date getPatientDOB()
    {
        return this.getPatientDOB(this.UserSecurityID);
    }

    public void setPatientDOB(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientDOB' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[PatientDOB]=["+newValue+"]");
                   dbDB.setPatientDOB(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[PatientDOB]=["+newValue+"]");
           dbDB.setPatientDOB(newValue);
         }
    }
    public Date getPatientDOB(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientDOB' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPatientDOB();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPatientDOB();
         }
        return myVal;
    }

    public void setPatientSSN(String newValue)
    {
        this.setPatientSSN(newValue,this.UserSecurityID);


    }
    public String getPatientSSN()
    {
        return this.getPatientSSN(this.UserSecurityID);
    }

    public void setPatientSSN(String newValue, Integer GroupRefID)
    {
        newValue = com.winstaff.password.Encrypt.encrypt_DESEDE_String(newValue, DataControlUtils.ENC_KEY);
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientSSN' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[PatientSSN]=["+newValue+"]");
                   dbDB.setPatientSSN(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[PatientSSN]=["+newValue+"]");
           dbDB.setPatientSSN(newValue);
         }
    }
    public String getPatientSSN(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientSSN' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPatientSSN();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPatientSSN();
         }
        if (myVal != null&&!myVal.equalsIgnoreCase(""))
        {
             myVal = com.winstaff.password.Encrypt.decrypt_DESEDE_String(myVal, DataControlUtils.ENC_KEY);
        }
        return myVal;
    }

    public void setPatientGender(String newValue)
    {
        this.setPatientGender(newValue,this.UserSecurityID);


    }
    public String getPatientGender()
    {
        return this.getPatientGender(this.UserSecurityID);
    }

    public void setPatientGender(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientGender' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[PatientGender]=["+newValue+"]");
                   dbDB.setPatientGender(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[PatientGender]=["+newValue+"]");
           dbDB.setPatientGender(newValue);
         }
    }
    public String getPatientGender(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientGender' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPatientGender();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPatientGender();
         }
        return myVal;
    }

    public void setPatientAddress1(String newValue)
    {
        this.setPatientAddress1(newValue,this.UserSecurityID);


    }
    public String getPatientAddress1()
    {
        return this.getPatientAddress1(this.UserSecurityID);
    }

    public void setPatientAddress1(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientAddress1' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[PatientAddress1]=["+newValue+"]");
                   dbDB.setPatientAddress1(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[PatientAddress1]=["+newValue+"]");
           dbDB.setPatientAddress1(newValue);
         }
    }
    public String getPatientAddress1(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientAddress1' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPatientAddress1();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPatientAddress1();
         }
        return myVal;
    }

    public void setPatientAddress2(String newValue)
    {
        this.setPatientAddress2(newValue,this.UserSecurityID);


    }
    public String getPatientAddress2()
    {
        return this.getPatientAddress2(this.UserSecurityID);
    }

    public void setPatientAddress2(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientAddress2' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[PatientAddress2]=["+newValue+"]");
                   dbDB.setPatientAddress2(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[PatientAddress2]=["+newValue+"]");
           dbDB.setPatientAddress2(newValue);
         }
    }
    public String getPatientAddress2(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientAddress2' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPatientAddress2();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPatientAddress2();
         }
        return myVal;
    }

    public void setPatientCity(String newValue)
    {
        this.setPatientCity(newValue,this.UserSecurityID);


    }
    public String getPatientCity()
    {
        return this.getPatientCity(this.UserSecurityID);
    }

    public void setPatientCity(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientCity' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[PatientCity]=["+newValue+"]");
                   dbDB.setPatientCity(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[PatientCity]=["+newValue+"]");
           dbDB.setPatientCity(newValue);
         }
    }
    public String getPatientCity(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientCity' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPatientCity();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPatientCity();
         }
        return myVal;
    }

    public void setPatientStateID(Integer newValue)
    {
        this.setPatientStateID(newValue,this.UserSecurityID);


    }
    public Integer getPatientStateID()
    {
        return this.getPatientStateID(this.UserSecurityID);
    }

    public void setPatientStateID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientStateID' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[PatientStateID]=["+newValue+"]");
                   dbDB.setPatientStateID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[PatientStateID]=["+newValue+"]");
           dbDB.setPatientStateID(newValue);
         }
    }
    public Integer getPatientStateID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientStateID' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPatientStateID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPatientStateID();
         }
        return myVal;
    }

    public void setPatientZIP(String newValue)
    {
        this.setPatientZIP(newValue,this.UserSecurityID);


    }
    public String getPatientZIP()
    {
        return this.getPatientZIP(this.UserSecurityID);
    }

    public void setPatientZIP(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientZIP' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[PatientZIP]=["+newValue+"]");
                   dbDB.setPatientZIP(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[PatientZIP]=["+newValue+"]");
           dbDB.setPatientZIP(newValue);
         }
    }
    public String getPatientZIP(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientZIP' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPatientZIP();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPatientZIP();
         }
        return myVal;
    }

    public void setPatientHomePhone(String newValue)
    {
        this.setPatientHomePhone(newValue,this.UserSecurityID);


    }
    public String getPatientHomePhone()
    {
        return this.getPatientHomePhone(this.UserSecurityID);
    }

    public void setPatientHomePhone(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientHomePhone' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[PatientHomePhone]=["+newValue+"]");
                   dbDB.setPatientHomePhone(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[PatientHomePhone]=["+newValue+"]");
           dbDB.setPatientHomePhone(newValue);
         }
    }
    public String getPatientHomePhone(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientHomePhone' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPatientHomePhone();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPatientHomePhone();
         }
        return myVal;
    }

    public void setPatientWorkPhone(String newValue)
    {
        this.setPatientWorkPhone(newValue,this.UserSecurityID);


    }
    public String getPatientWorkPhone()
    {
        return this.getPatientWorkPhone(this.UserSecurityID);
    }

    public void setPatientWorkPhone(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientWorkPhone' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[PatientWorkPhone]=["+newValue+"]");
                   dbDB.setPatientWorkPhone(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[PatientWorkPhone]=["+newValue+"]");
           dbDB.setPatientWorkPhone(newValue);
         }
    }
    public String getPatientWorkPhone(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientWorkPhone' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPatientWorkPhone();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPatientWorkPhone();
         }
        return myVal;
    }

    public void setPatientCellPhone(String newValue)
    {
        this.setPatientCellPhone(newValue,this.UserSecurityID);


    }
    public String getPatientCellPhone()
    {
        return this.getPatientCellPhone(this.UserSecurityID);
    }

    public void setPatientCellPhone(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientCellPhone' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[PatientCellPhone]=["+newValue+"]");
                   dbDB.setPatientCellPhone(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[PatientCellPhone]=["+newValue+"]");
           dbDB.setPatientCellPhone(newValue);
         }
    }
    public String getPatientCellPhone(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientCellPhone' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPatientCellPhone();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPatientCellPhone();
         }
        return myVal;
    }

    public void setPatientCCAccountFullName(String newValue)
    {
        this.setPatientCCAccountFullName(newValue,this.UserSecurityID);


    }
    public String getPatientCCAccountFullName()
    {
        return this.getPatientCCAccountFullName(this.UserSecurityID);
    }

    public void setPatientCCAccountFullName(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientCCAccountFullName' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[PatientCCAccountFullName]=["+newValue+"]");
                   dbDB.setPatientCCAccountFullName(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[PatientCCAccountFullName]=["+newValue+"]");
           dbDB.setPatientCCAccountFullName(newValue);
         }
    }
    public String getPatientCCAccountFullName(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientCCAccountFullName' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPatientCCAccountFullName();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPatientCCAccountFullName();
         }
        return myVal;
    }

    public void setPatientCCAccountNumber1(String newValue)
    {
        this.setPatientCCAccountNumber1(newValue,this.UserSecurityID);


    }
    public String getPatientCCAccountNumber1()
    {
        return this.getPatientCCAccountNumber1(this.UserSecurityID);
    }

    public void setPatientCCAccountNumber1(String newValue, Integer GroupRefID)
    {
        newValue = com.winstaff.password.Encrypt.encrypt_DESEDE_String(newValue, DataControlUtils.ENC_KEY);
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientCCAccountNumber1' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[PatientCCAccountNumber1]=["+newValue+"]");
                   dbDB.setPatientCCAccountNumber1(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[PatientCCAccountNumber1]=["+newValue+"]");
           dbDB.setPatientCCAccountNumber1(newValue);
         }
    }
    public String getPatientCCAccountNumber1(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientCCAccountNumber1' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPatientCCAccountNumber1();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPatientCCAccountNumber1();
         }
        if (myVal != null&&!myVal.equalsIgnoreCase(""))
        {
             myVal = com.winstaff.password.Encrypt.decrypt_DESEDE_String(myVal, DataControlUtils.ENC_KEY);
        }
        return myVal;
    }

    public void setPatientCCAccountNumber2(String newValue)
    {
        this.setPatientCCAccountNumber2(newValue,this.UserSecurityID);


    }
    public String getPatientCCAccountNumber2()
    {
        return this.getPatientCCAccountNumber2(this.UserSecurityID);
    }

    public void setPatientCCAccountNumber2(String newValue, Integer GroupRefID)
    {
        newValue = com.winstaff.password.Encrypt.encrypt_DESEDE_String(newValue, DataControlUtils.ENC_KEY);
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientCCAccountNumber2' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[PatientCCAccountNumber2]=["+newValue+"]");
                   dbDB.setPatientCCAccountNumber2(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[PatientCCAccountNumber2]=["+newValue+"]");
           dbDB.setPatientCCAccountNumber2(newValue);
         }
    }
    public String getPatientCCAccountNumber2(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientCCAccountNumber2' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPatientCCAccountNumber2();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPatientCCAccountNumber2();
         }
        if (myVal != null&&!myVal.equalsIgnoreCase(""))
        {
             myVal = com.winstaff.password.Encrypt.decrypt_DESEDE_String(myVal, DataControlUtils.ENC_KEY);
        }
        return myVal;
    }

    public void setPatientCCAccountType(String newValue)
    {
        this.setPatientCCAccountType(newValue,this.UserSecurityID);


    }
    public String getPatientCCAccountType()
    {
        return this.getPatientCCAccountType(this.UserSecurityID);
    }

    public void setPatientCCAccountType(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientCCAccountType' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[PatientCCAccountType]=["+newValue+"]");
                   dbDB.setPatientCCAccountType(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[PatientCCAccountType]=["+newValue+"]");
           dbDB.setPatientCCAccountType(newValue);
         }
    }
    public String getPatientCCAccountType(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientCCAccountType' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPatientCCAccountType();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPatientCCAccountType();
         }
        return myVal;
    }

    public void setPatientCCAccountExpMonth(String newValue)
    {
        this.setPatientCCAccountExpMonth(newValue,this.UserSecurityID);


    }
    public String getPatientCCAccountExpMonth()
    {
        return this.getPatientCCAccountExpMonth(this.UserSecurityID);
    }

    public void setPatientCCAccountExpMonth(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientCCAccountExpMonth' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[PatientCCAccountExpMonth]=["+newValue+"]");
                   dbDB.setPatientCCAccountExpMonth(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[PatientCCAccountExpMonth]=["+newValue+"]");
           dbDB.setPatientCCAccountExpMonth(newValue);
         }
    }
    public String getPatientCCAccountExpMonth(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientCCAccountExpMonth' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPatientCCAccountExpMonth();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPatientCCAccountExpMonth();
         }
        return myVal;
    }

    public void setPatientCCAccountExpYear(String newValue)
    {
        this.setPatientCCAccountExpYear(newValue,this.UserSecurityID);


    }
    public String getPatientCCAccountExpYear()
    {
        return this.getPatientCCAccountExpYear(this.UserSecurityID);
    }

    public void setPatientCCAccountExpYear(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientCCAccountExpYear' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[PatientCCAccountExpYear]=["+newValue+"]");
                   dbDB.setPatientCCAccountExpYear(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[PatientCCAccountExpYear]=["+newValue+"]");
           dbDB.setPatientCCAccountExpYear(newValue);
         }
    }
    public String getPatientCCAccountExpYear(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientCCAccountExpYear' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPatientCCAccountExpYear();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPatientCCAccountExpYear();
         }
        return myVal;
    }

    public void setSupervisorName(String newValue)
    {
        this.setSupervisorName(newValue,this.UserSecurityID);


    }
    public String getSupervisorName()
    {
        return this.getSupervisorName(this.UserSecurityID);
    }

    public void setSupervisorName(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SupervisorName' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[SupervisorName]=["+newValue+"]");
                   dbDB.setSupervisorName(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[SupervisorName]=["+newValue+"]");
           dbDB.setSupervisorName(newValue);
         }
    }
    public String getSupervisorName(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SupervisorName' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getSupervisorName();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getSupervisorName();
         }
        return myVal;
    }

    public void setSupervisorPhone(String newValue)
    {
        this.setSupervisorPhone(newValue,this.UserSecurityID);


    }
    public String getSupervisorPhone()
    {
        return this.getSupervisorPhone(this.UserSecurityID);
    }

    public void setSupervisorPhone(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SupervisorPhone' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[SupervisorPhone]=["+newValue+"]");
                   dbDB.setSupervisorPhone(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[SupervisorPhone]=["+newValue+"]");
           dbDB.setSupervisorPhone(newValue);
         }
    }
    public String getSupervisorPhone(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SupervisorPhone' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getSupervisorPhone();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getSupervisorPhone();
         }
        return myVal;
    }

    public void setSupervisorFax(String newValue)
    {
        this.setSupervisorFax(newValue,this.UserSecurityID);


    }
    public String getSupervisorFax()
    {
        return this.getSupervisorFax(this.UserSecurityID);
    }

    public void setSupervisorFax(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SupervisorFax' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[SupervisorFax]=["+newValue+"]");
                   dbDB.setSupervisorFax(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[SupervisorFax]=["+newValue+"]");
           dbDB.setSupervisorFax(newValue);
         }
    }
    public String getSupervisorFax(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SupervisorFax' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getSupervisorFax();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getSupervisorFax();
         }
        return myVal;
    }

    public void setDateOfInjury(Date newValue)
    {
        this.setDateOfInjury(newValue,this.UserSecurityID);


    }
    public Date getDateOfInjury()
    {
        return this.getDateOfInjury(this.UserSecurityID);
    }

    public void setDateOfInjury(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='DateOfInjury' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[DateOfInjury]=["+newValue+"]");
                   dbDB.setDateOfInjury(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[DateOfInjury]=["+newValue+"]");
           dbDB.setDateOfInjury(newValue);
         }
    }
    public Date getDateOfInjury(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='DateOfInjury' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getDateOfInjury();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getDateOfInjury();
         }
        return myVal;
    }

    public void setInjuryDescription(String newValue)
    {
        this.setInjuryDescription(newValue,this.UserSecurityID);


    }
    public String getInjuryDescription()
    {
        return this.getInjuryDescription(this.UserSecurityID);
    }

    public void setInjuryDescription(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='InjuryDescription' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[InjuryDescription]=["+newValue+"]");
                   dbDB.setInjuryDescription(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[InjuryDescription]=["+newValue+"]");
           dbDB.setInjuryDescription(newValue);
         }
    }
    public String getInjuryDescription(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='InjuryDescription' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getInjuryDescription();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getInjuryDescription();
         }
        return myVal;
    }

    public void setLODID(Integer newValue)
    {
        this.setLODID(newValue,this.UserSecurityID);


    }
    public Integer getLODID()
    {
        return this.getLODID(this.UserSecurityID);
    }

    public void setLODID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='LODID' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[LODID]=["+newValue+"]");
                   dbDB.setLODID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[LODID]=["+newValue+"]");
           dbDB.setLODID(newValue);
         }
    }
    public Integer getLODID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='LODID' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getLODID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getLODID();
         }
        return myVal;
    }

    public void setNurseCaseManagerID(Integer newValue)
    {
        this.setNurseCaseManagerID(newValue,this.UserSecurityID);


    }
    public Integer getNurseCaseManagerID()
    {
        return this.getNurseCaseManagerID(this.UserSecurityID);
    }

    public void setNurseCaseManagerID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='NurseCaseManagerID' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[NurseCaseManagerID]=["+newValue+"]");
                   dbDB.setNurseCaseManagerID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[NurseCaseManagerID]=["+newValue+"]");
           dbDB.setNurseCaseManagerID(newValue);
         }
    }
    public Integer getNurseCaseManagerID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='NurseCaseManagerID' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getNurseCaseManagerID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getNurseCaseManagerID();
         }
        return myVal;
    }

    public void setPatientAccountID(Integer newValue)
    {
        this.setPatientAccountID(newValue,this.UserSecurityID);


    }
    public Integer getPatientAccountID()
    {
        return this.getPatientAccountID(this.UserSecurityID);
    }

    public void setPatientAccountID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientAccountID' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[PatientAccountID]=["+newValue+"]");
                   dbDB.setPatientAccountID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[PatientAccountID]=["+newValue+"]");
           dbDB.setPatientAccountID(newValue);
         }
    }
    public Integer getPatientAccountID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientAccountID' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPatientAccountID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPatientAccountID();
         }
        return myVal;
    }

    public void setAttorneyFirstName(String newValue)
    {
        this.setAttorneyFirstName(newValue,this.UserSecurityID);


    }
    public String getAttorneyFirstName()
    {
        return this.getAttorneyFirstName(this.UserSecurityID);
    }

    public void setAttorneyFirstName(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AttorneyFirstName' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[AttorneyFirstName]=["+newValue+"]");
                   dbDB.setAttorneyFirstName(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[AttorneyFirstName]=["+newValue+"]");
           dbDB.setAttorneyFirstName(newValue);
         }
    }
    public String getAttorneyFirstName(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AttorneyFirstName' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getAttorneyFirstName();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getAttorneyFirstName();
         }
        return myVal;
    }

    public void setAttorneyLastName(String newValue)
    {
        this.setAttorneyLastName(newValue,this.UserSecurityID);


    }
    public String getAttorneyLastName()
    {
        return this.getAttorneyLastName(this.UserSecurityID);
    }

    public void setAttorneyLastName(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AttorneyLastName' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[AttorneyLastName]=["+newValue+"]");
                   dbDB.setAttorneyLastName(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[AttorneyLastName]=["+newValue+"]");
           dbDB.setAttorneyLastName(newValue);
         }
    }
    public String getAttorneyLastName(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AttorneyLastName' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getAttorneyLastName();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getAttorneyLastName();
         }
        return myVal;
    }

    public void setAttorneyAddress1(String newValue)
    {
        this.setAttorneyAddress1(newValue,this.UserSecurityID);


    }
    public String getAttorneyAddress1()
    {
        return this.getAttorneyAddress1(this.UserSecurityID);
    }

    public void setAttorneyAddress1(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AttorneyAddress1' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[AttorneyAddress1]=["+newValue+"]");
                   dbDB.setAttorneyAddress1(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[AttorneyAddress1]=["+newValue+"]");
           dbDB.setAttorneyAddress1(newValue);
         }
    }
    public String getAttorneyAddress1(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AttorneyAddress1' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getAttorneyAddress1();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getAttorneyAddress1();
         }
        return myVal;
    }

    public void setAttorneyAddress2(String newValue)
    {
        this.setAttorneyAddress2(newValue,this.UserSecurityID);


    }
    public String getAttorneyAddress2()
    {
        return this.getAttorneyAddress2(this.UserSecurityID);
    }

    public void setAttorneyAddress2(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AttorneyAddress2' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[AttorneyAddress2]=["+newValue+"]");
                   dbDB.setAttorneyAddress2(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[AttorneyAddress2]=["+newValue+"]");
           dbDB.setAttorneyAddress2(newValue);
         }
    }
    public String getAttorneyAddress2(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AttorneyAddress2' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getAttorneyAddress2();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getAttorneyAddress2();
         }
        return myVal;
    }

    public void setAttorneyCity(String newValue)
    {
        this.setAttorneyCity(newValue,this.UserSecurityID);


    }
    public String getAttorneyCity()
    {
        return this.getAttorneyCity(this.UserSecurityID);
    }

    public void setAttorneyCity(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AttorneyCity' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[AttorneyCity]=["+newValue+"]");
                   dbDB.setAttorneyCity(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[AttorneyCity]=["+newValue+"]");
           dbDB.setAttorneyCity(newValue);
         }
    }
    public String getAttorneyCity(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AttorneyCity' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getAttorneyCity();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getAttorneyCity();
         }
        return myVal;
    }

    public void setAttorneyStateID(Integer newValue)
    {
        this.setAttorneyStateID(newValue,this.UserSecurityID);


    }
    public Integer getAttorneyStateID()
    {
        return this.getAttorneyStateID(this.UserSecurityID);
    }

    public void setAttorneyStateID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AttorneyStateID' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[AttorneyStateID]=["+newValue+"]");
                   dbDB.setAttorneyStateID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[AttorneyStateID]=["+newValue+"]");
           dbDB.setAttorneyStateID(newValue);
         }
    }
    public Integer getAttorneyStateID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AttorneyStateID' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getAttorneyStateID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getAttorneyStateID();
         }
        return myVal;
    }

    public void setAttorneyZIP(String newValue)
    {
        this.setAttorneyZIP(newValue,this.UserSecurityID);


    }
    public String getAttorneyZIP()
    {
        return this.getAttorneyZIP(this.UserSecurityID);
    }

    public void setAttorneyZIP(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AttorneyZIP' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[AttorneyZIP]=["+newValue+"]");
                   dbDB.setAttorneyZIP(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[AttorneyZIP]=["+newValue+"]");
           dbDB.setAttorneyZIP(newValue);
         }
    }
    public String getAttorneyZIP(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AttorneyZIP' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getAttorneyZIP();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getAttorneyZIP();
         }
        return myVal;
    }

    public void setAttorneyFax(String newValue)
    {
        this.setAttorneyFax(newValue,this.UserSecurityID);


    }
    public String getAttorneyFax()
    {
        return this.getAttorneyFax(this.UserSecurityID);
    }

    public void setAttorneyFax(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AttorneyFax' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[AttorneyFax]=["+newValue+"]");
                   dbDB.setAttorneyFax(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[AttorneyFax]=["+newValue+"]");
           dbDB.setAttorneyFax(newValue);
         }
    }
    public String getAttorneyFax(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AttorneyFax' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getAttorneyFax();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getAttorneyFax();
         }
        return myVal;
    }

    public void setAttorneyWorkPhone(String newValue)
    {
        this.setAttorneyWorkPhone(newValue,this.UserSecurityID);


    }
    public String getAttorneyWorkPhone()
    {
        return this.getAttorneyWorkPhone(this.UserSecurityID);
    }

    public void setAttorneyWorkPhone(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AttorneyWorkPhone' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[AttorneyWorkPhone]=["+newValue+"]");
                   dbDB.setAttorneyWorkPhone(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[AttorneyWorkPhone]=["+newValue+"]");
           dbDB.setAttorneyWorkPhone(newValue);
         }
    }
    public String getAttorneyWorkPhone(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AttorneyWorkPhone' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getAttorneyWorkPhone();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getAttorneyWorkPhone();
         }
        return myVal;
    }

    public void setAttorneyCellPhone(String newValue)
    {
        this.setAttorneyCellPhone(newValue,this.UserSecurityID);


    }
    public String getAttorneyCellPhone()
    {
        return this.getAttorneyCellPhone(this.UserSecurityID);
    }

    public void setAttorneyCellPhone(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AttorneyCellPhone' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[AttorneyCellPhone]=["+newValue+"]");
                   dbDB.setAttorneyCellPhone(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[AttorneyCellPhone]=["+newValue+"]");
           dbDB.setAttorneyCellPhone(newValue);
         }
    }
    public String getAttorneyCellPhone(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AttorneyCellPhone' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getAttorneyCellPhone();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getAttorneyCellPhone();
         }
        return myVal;
    }

    public void setComments(String newValue)
    {
        this.setComments(newValue,this.UserSecurityID);


    }
    public String getComments()
    {
        return this.getComments(this.UserSecurityID);
    }

    public void setComments(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Comments' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[Comments]=["+newValue+"]");
                   dbDB.setComments(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[Comments]=["+newValue+"]");
           dbDB.setComments(newValue);
         }
    }
    public String getComments(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Comments' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getComments();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getComments();
         }
        return myVal;
    }

    public void setAuditNotes(String newValue)
    {
        this.setAuditNotes(newValue,this.UserSecurityID);


    }
    public String getAuditNotes()
    {
        return this.getAuditNotes(this.UserSecurityID);
    }

    public void setAuditNotes(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AuditNotes' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[AuditNotes]=["+newValue+"]");
                   dbDB.setAuditNotes(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[AuditNotes]=["+newValue+"]");
           dbDB.setAuditNotes(newValue);
         }
    }
    public String getAuditNotes(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AuditNotes' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getAuditNotes();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getAuditNotes();
         }
        return myVal;
    }

    public void setPatientIsClaus(Integer newValue)
    {
        this.setPatientIsClaus(newValue,this.UserSecurityID);


    }
    public Integer getPatientIsClaus()
    {
        return this.getPatientIsClaus(this.UserSecurityID);
    }

    public void setPatientIsClaus(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientIsClaus' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[PatientIsClaus]=["+newValue+"]");
                   dbDB.setPatientIsClaus(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[PatientIsClaus]=["+newValue+"]");
           dbDB.setPatientIsClaus(newValue);
         }
    }
    public Integer getPatientIsClaus(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientIsClaus' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPatientIsClaus();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPatientIsClaus();
         }
        return myVal;
    }

    public void setPatientIsHWRatio(Integer newValue)
    {
        this.setPatientIsHWRatio(newValue,this.UserSecurityID);


    }
    public Integer getPatientIsHWRatio()
    {
        return this.getPatientIsHWRatio(this.UserSecurityID);
    }

    public void setPatientIsHWRatio(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientIsHWRatio' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[PatientIsHWRatio]=["+newValue+"]");
                   dbDB.setPatientIsHWRatio(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[PatientIsHWRatio]=["+newValue+"]");
           dbDB.setPatientIsHWRatio(newValue);
         }
    }
    public Integer getPatientIsHWRatio(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientIsHWRatio' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPatientIsHWRatio();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPatientIsHWRatio();
         }
        return myVal;
    }

    public void setAssignedToID(Integer newValue)
    {
        this.setAssignedToID(newValue,this.UserSecurityID);


    }
    public Integer getAssignedToID()
    {
        return this.getAssignedToID(this.UserSecurityID);
    }

    public void setAssignedToID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AssignedToID' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[AssignedToID]=["+newValue+"]");
                   dbDB.setAssignedToID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[AssignedToID]=["+newValue+"]");
           dbDB.setAssignedToID(newValue);
         }
    }
    public Integer getAssignedToID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AssignedToID' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getAssignedToID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getAssignedToID();
         }
        return myVal;
    }

    public void setAssignedToRoleID(Integer newValue)
    {
        this.setAssignedToRoleID(newValue,this.UserSecurityID);


    }
    public Integer getAssignedToRoleID()
    {
        return this.getAssignedToRoleID(this.UserSecurityID);
    }

    public void setAssignedToRoleID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AssignedToRoleID' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[AssignedToRoleID]=["+newValue+"]");
                   dbDB.setAssignedToRoleID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[AssignedToRoleID]=["+newValue+"]");
           dbDB.setAssignedToRoleID(newValue);
         }
    }
    public Integer getAssignedToRoleID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AssignedToRoleID' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getAssignedToRoleID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getAssignedToRoleID();
         }
        return myVal;
    }

    public void setCaseAdministratorID(Integer newValue)
    {
        this.setCaseAdministratorID(newValue,this.UserSecurityID);


    }
    public Integer getCaseAdministratorID()
    {
        return this.getCaseAdministratorID(this.UserSecurityID);
    }

    public void setCaseAdministratorID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CaseAdministratorID' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[CaseAdministratorID]=["+newValue+"]");
                   dbDB.setCaseAdministratorID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[CaseAdministratorID]=["+newValue+"]");
           dbDB.setCaseAdministratorID(newValue);
         }
    }
    public Integer getCaseAdministratorID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CaseAdministratorID' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getCaseAdministratorID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getCaseAdministratorID();
         }
        return myVal;
    }

    public void setBillToContactID(Integer newValue)
    {
        this.setBillToContactID(newValue,this.UserSecurityID);


    }
    public Integer getBillToContactID()
    {
        return this.getBillToContactID(this.UserSecurityID);
    }

    public void setBillToContactID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='BillToContactID' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[BillToContactID]=["+newValue+"]");
                   dbDB.setBillToContactID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[BillToContactID]=["+newValue+"]");
           dbDB.setBillToContactID(newValue);
         }
    }
    public Integer getBillToContactID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='BillToContactID' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getBillToContactID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getBillToContactID();
         }
        return myVal;
    }

    public void setURContactID(Integer newValue)
    {
        this.setURContactID(newValue,this.UserSecurityID);


    }
    public Integer getURContactID()
    {
        return this.getURContactID(this.UserSecurityID);
    }

    public void setURContactID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='URContactID' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[URContactID]=["+newValue+"]");
                   dbDB.setURContactID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[URContactID]=["+newValue+"]");
           dbDB.setURContactID(newValue);
         }
    }
    public Integer getURContactID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='URContactID' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getURContactID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getURContactID();
         }
        return myVal;
    }

    public void setReferredByContactID(Integer newValue)
    {
        this.setReferredByContactID(newValue,this.UserSecurityID);


    }
    public Integer getReferredByContactID()
    {
        return this.getReferredByContactID(this.UserSecurityID);
    }

    public void setReferredByContactID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ReferredByContactID' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[ReferredByContactID]=["+newValue+"]");
                   dbDB.setReferredByContactID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[ReferredByContactID]=["+newValue+"]");
           dbDB.setReferredByContactID(newValue);
         }
    }
    public Integer getReferredByContactID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ReferredByContactID' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getReferredByContactID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getReferredByContactID();
         }
        return myVal;
    }

    public void setPatientHasImplants(Integer newValue)
    {
        this.setPatientHasImplants(newValue,this.UserSecurityID);


    }
    public Integer getPatientHasImplants()
    {
        return this.getPatientHasImplants(this.UserSecurityID);
    }

    public void setPatientHasImplants(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientHasImplants' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[PatientHasImplants]=["+newValue+"]");
                   dbDB.setPatientHasImplants(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[PatientHasImplants]=["+newValue+"]");
           dbDB.setPatientHasImplants(newValue);
         }
    }
    public Integer getPatientHasImplants(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientHasImplants' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPatientHasImplants();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPatientHasImplants();
         }
        return myVal;
    }

    public void setPatientHasMetalInBody(Integer newValue)
    {
        this.setPatientHasMetalInBody(newValue,this.UserSecurityID);


    }
    public Integer getPatientHasMetalInBody()
    {
        return this.getPatientHasMetalInBody(this.UserSecurityID);
    }

    public void setPatientHasMetalInBody(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientHasMetalInBody' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[PatientHasMetalInBody]=["+newValue+"]");
                   dbDB.setPatientHasMetalInBody(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[PatientHasMetalInBody]=["+newValue+"]");
           dbDB.setPatientHasMetalInBody(newValue);
         }
    }
    public Integer getPatientHasMetalInBody(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientHasMetalInBody' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPatientHasMetalInBody();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPatientHasMetalInBody();
         }
        return myVal;
    }

    public void setPatientHasAllergies(Integer newValue)
    {
        this.setPatientHasAllergies(newValue,this.UserSecurityID);


    }
    public Integer getPatientHasAllergies()
    {
        return this.getPatientHasAllergies(this.UserSecurityID);
    }

    public void setPatientHasAllergies(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientHasAllergies' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[PatientHasAllergies]=["+newValue+"]");
                   dbDB.setPatientHasAllergies(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[PatientHasAllergies]=["+newValue+"]");
           dbDB.setPatientHasAllergies(newValue);
         }
    }
    public Integer getPatientHasAllergies(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientHasAllergies' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPatientHasAllergies();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPatientHasAllergies();
         }
        return myVal;
    }

    public void setPatientHasRecentSurgery(Integer newValue)
    {
        this.setPatientHasRecentSurgery(newValue,this.UserSecurityID);


    }
    public Integer getPatientHasRecentSurgery()
    {
        return this.getPatientHasRecentSurgery(this.UserSecurityID);
    }

    public void setPatientHasRecentSurgery(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientHasRecentSurgery' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[PatientHasRecentSurgery]=["+newValue+"]");
                   dbDB.setPatientHasRecentSurgery(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[PatientHasRecentSurgery]=["+newValue+"]");
           dbDB.setPatientHasRecentSurgery(newValue);
         }
    }
    public Integer getPatientHasRecentSurgery(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientHasRecentSurgery' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPatientHasRecentSurgery();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPatientHasRecentSurgery();
         }
        return myVal;
    }

    public void setPatientHasPreviousMRIs(Integer newValue)
    {
        this.setPatientHasPreviousMRIs(newValue,this.UserSecurityID);


    }
    public Integer getPatientHasPreviousMRIs()
    {
        return this.getPatientHasPreviousMRIs(this.UserSecurityID);
    }

    public void setPatientHasPreviousMRIs(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientHasPreviousMRIs' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[PatientHasPreviousMRIs]=["+newValue+"]");
                   dbDB.setPatientHasPreviousMRIs(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[PatientHasPreviousMRIs]=["+newValue+"]");
           dbDB.setPatientHasPreviousMRIs(newValue);
         }
    }
    public Integer getPatientHasPreviousMRIs(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientHasPreviousMRIs' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPatientHasPreviousMRIs();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPatientHasPreviousMRIs();
         }
        return myVal;
    }

    public void setPatientHasKidneyLiverHypertensionDiabeticConditions(Integer newValue)
    {
        this.setPatientHasKidneyLiverHypertensionDiabeticConditions(newValue,this.UserSecurityID);


    }
    public Integer getPatientHasKidneyLiverHypertensionDiabeticConditions()
    {
        return this.getPatientHasKidneyLiverHypertensionDiabeticConditions(this.UserSecurityID);
    }

    public void setPatientHasKidneyLiverHypertensionDiabeticConditions(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientHasKidneyLiverHypertensionDiabeticConditions' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[PatientHasKidneyLiverHypertensionDiabeticConditions]=["+newValue+"]");
                   dbDB.setPatientHasKidneyLiverHypertensionDiabeticConditions(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[PatientHasKidneyLiverHypertensionDiabeticConditions]=["+newValue+"]");
           dbDB.setPatientHasKidneyLiverHypertensionDiabeticConditions(newValue);
         }
    }
    public Integer getPatientHasKidneyLiverHypertensionDiabeticConditions(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientHasKidneyLiverHypertensionDiabeticConditions' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPatientHasKidneyLiverHypertensionDiabeticConditions();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPatientHasKidneyLiverHypertensionDiabeticConditions();
         }
        return myVal;
    }

    public void setPatientIsPregnant(Integer newValue)
    {
        this.setPatientIsPregnant(newValue,this.UserSecurityID);


    }
    public Integer getPatientIsPregnant()
    {
        return this.getPatientIsPregnant(this.UserSecurityID);
    }

    public void setPatientIsPregnant(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientIsPregnant' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[PatientIsPregnant]=["+newValue+"]");
                   dbDB.setPatientIsPregnant(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[PatientIsPregnant]=["+newValue+"]");
           dbDB.setPatientIsPregnant(newValue);
         }
    }
    public Integer getPatientIsPregnant(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientIsPregnant' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPatientIsPregnant();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPatientIsPregnant();
         }
        return myVal;
    }

    public void setPatientHeight(String newValue)
    {
        this.setPatientHeight(newValue,this.UserSecurityID);


    }
    public String getPatientHeight()
    {
        return this.getPatientHeight(this.UserSecurityID);
    }

    public void setPatientHeight(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientHeight' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[PatientHeight]=["+newValue+"]");
                   dbDB.setPatientHeight(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[PatientHeight]=["+newValue+"]");
           dbDB.setPatientHeight(newValue);
         }
    }
    public String getPatientHeight(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientHeight' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPatientHeight();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPatientHeight();
         }
        return myVal;
    }

    public void setPatientWeight(Integer newValue)
    {
        this.setPatientWeight(newValue,this.UserSecurityID);


    }
    public Integer getPatientWeight()
    {
        return this.getPatientWeight(this.UserSecurityID);
    }

    public void setPatientWeight(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientWeight' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[PatientWeight]=["+newValue+"]");
                   dbDB.setPatientWeight(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[PatientWeight]=["+newValue+"]");
           dbDB.setPatientWeight(newValue);
         }
    }
    public Integer getPatientWeight(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientWeight' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPatientWeight();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPatientWeight();
         }
        return myVal;
    }

    public void setPatientHasImplantsDesc(String newValue)
    {
        this.setPatientHasImplantsDesc(newValue,this.UserSecurityID);


    }
    public String getPatientHasImplantsDesc()
    {
        return this.getPatientHasImplantsDesc(this.UserSecurityID);
    }

    public void setPatientHasImplantsDesc(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientHasImplantsDesc' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[PatientHasImplantsDesc]=["+newValue+"]");
                   dbDB.setPatientHasImplantsDesc(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[PatientHasImplantsDesc]=["+newValue+"]");
           dbDB.setPatientHasImplantsDesc(newValue);
         }
    }
    public String getPatientHasImplantsDesc(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientHasImplantsDesc' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPatientHasImplantsDesc();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPatientHasImplantsDesc();
         }
        return myVal;
    }

    public void setPatientHasMetalInBodyDesc(String newValue)
    {
        this.setPatientHasMetalInBodyDesc(newValue,this.UserSecurityID);


    }
    public String getPatientHasMetalInBodyDesc()
    {
        return this.getPatientHasMetalInBodyDesc(this.UserSecurityID);
    }

    public void setPatientHasMetalInBodyDesc(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientHasMetalInBodyDesc' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[PatientHasMetalInBodyDesc]=["+newValue+"]");
                   dbDB.setPatientHasMetalInBodyDesc(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[PatientHasMetalInBodyDesc]=["+newValue+"]");
           dbDB.setPatientHasMetalInBodyDesc(newValue);
         }
    }
    public String getPatientHasMetalInBodyDesc(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientHasMetalInBodyDesc' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPatientHasMetalInBodyDesc();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPatientHasMetalInBodyDesc();
         }
        return myVal;
    }

    public void setPatientHasAllergiesDesc(String newValue)
    {
        this.setPatientHasAllergiesDesc(newValue,this.UserSecurityID);


    }
    public String getPatientHasAllergiesDesc()
    {
        return this.getPatientHasAllergiesDesc(this.UserSecurityID);
    }

    public void setPatientHasAllergiesDesc(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientHasAllergiesDesc' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[PatientHasAllergiesDesc]=["+newValue+"]");
                   dbDB.setPatientHasAllergiesDesc(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[PatientHasAllergiesDesc]=["+newValue+"]");
           dbDB.setPatientHasAllergiesDesc(newValue);
         }
    }
    public String getPatientHasAllergiesDesc(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientHasAllergiesDesc' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPatientHasAllergiesDesc();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPatientHasAllergiesDesc();
         }
        return myVal;
    }

    public void setPatientHasRecentSurgeryDesc(String newValue)
    {
        this.setPatientHasRecentSurgeryDesc(newValue,this.UserSecurityID);


    }
    public String getPatientHasRecentSurgeryDesc()
    {
        return this.getPatientHasRecentSurgeryDesc(this.UserSecurityID);
    }

    public void setPatientHasRecentSurgeryDesc(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientHasRecentSurgeryDesc' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[PatientHasRecentSurgeryDesc]=["+newValue+"]");
                   dbDB.setPatientHasRecentSurgeryDesc(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[PatientHasRecentSurgeryDesc]=["+newValue+"]");
           dbDB.setPatientHasRecentSurgeryDesc(newValue);
         }
    }
    public String getPatientHasRecentSurgeryDesc(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientHasRecentSurgeryDesc' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPatientHasRecentSurgeryDesc();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPatientHasRecentSurgeryDesc();
         }
        return myVal;
    }

    public void setPatientHasPreviousMRIsDesc(String newValue)
    {
        this.setPatientHasPreviousMRIsDesc(newValue,this.UserSecurityID);


    }
    public String getPatientHasPreviousMRIsDesc()
    {
        return this.getPatientHasPreviousMRIsDesc(this.UserSecurityID);
    }

    public void setPatientHasPreviousMRIsDesc(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientHasPreviousMRIsDesc' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[PatientHasPreviousMRIsDesc]=["+newValue+"]");
                   dbDB.setPatientHasPreviousMRIsDesc(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[PatientHasPreviousMRIsDesc]=["+newValue+"]");
           dbDB.setPatientHasPreviousMRIsDesc(newValue);
         }
    }
    public String getPatientHasPreviousMRIsDesc(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientHasPreviousMRIsDesc' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPatientHasPreviousMRIsDesc();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPatientHasPreviousMRIsDesc();
         }
        return myVal;
    }

    public void setPatientHasKidneyLiverHypertensionDiabeticConditionsDesc(String newValue)
    {
        this.setPatientHasKidneyLiverHypertensionDiabeticConditionsDesc(newValue,this.UserSecurityID);


    }
    public String getPatientHasKidneyLiverHypertensionDiabeticConditionsDesc()
    {
        return this.getPatientHasKidneyLiverHypertensionDiabeticConditionsDesc(this.UserSecurityID);
    }

    public void setPatientHasKidneyLiverHypertensionDiabeticConditionsDesc(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientHasKidneyLiverHypertensionDiabeticConditionsDesc' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[PatientHasKidneyLiverHypertensionDiabeticConditionsDesc]=["+newValue+"]");
                   dbDB.setPatientHasKidneyLiverHypertensionDiabeticConditionsDesc(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[PatientHasKidneyLiverHypertensionDiabeticConditionsDesc]=["+newValue+"]");
           dbDB.setPatientHasKidneyLiverHypertensionDiabeticConditionsDesc(newValue);
         }
    }
    public String getPatientHasKidneyLiverHypertensionDiabeticConditionsDesc(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientHasKidneyLiverHypertensionDiabeticConditionsDesc' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPatientHasKidneyLiverHypertensionDiabeticConditionsDesc();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPatientHasKidneyLiverHypertensionDiabeticConditionsDesc();
         }
        return myVal;
    }

    public void setPatientIsPregnantDesc(String newValue)
    {
        this.setPatientIsPregnantDesc(newValue,this.UserSecurityID);


    }
    public String getPatientIsPregnantDesc()
    {
        return this.getPatientIsPregnantDesc(this.UserSecurityID);
    }

    public void setPatientIsPregnantDesc(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientIsPregnantDesc' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[PatientIsPregnantDesc]=["+newValue+"]");
                   dbDB.setPatientIsPregnantDesc(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[PatientIsPregnantDesc]=["+newValue+"]");
           dbDB.setPatientIsPregnantDesc(newValue);
         }
    }
    public String getPatientIsPregnantDesc(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientIsPregnantDesc' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPatientIsPregnantDesc();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPatientIsPregnantDesc();
         }
        return myVal;
    }

    public void setCaseAdministrator2ID(Integer newValue)
    {
        this.setCaseAdministrator2ID(newValue,this.UserSecurityID);


    }
    public Integer getCaseAdministrator2ID()
    {
        return this.getCaseAdministrator2ID(this.UserSecurityID);
    }

    public void setCaseAdministrator2ID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CaseAdministrator2ID' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[CaseAdministrator2ID]=["+newValue+"]");
                   dbDB.setCaseAdministrator2ID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[CaseAdministrator2ID]=["+newValue+"]");
           dbDB.setCaseAdministrator2ID(newValue);
         }
    }
    public Integer getCaseAdministrator2ID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CaseAdministrator2ID' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getCaseAdministrator2ID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getCaseAdministrator2ID();
         }
        return myVal;
    }

    public void setCaseAdministrator3ID(Integer newValue)
    {
        this.setCaseAdministrator3ID(newValue,this.UserSecurityID);


    }
    public Integer getCaseAdministrator3ID()
    {
        return this.getCaseAdministrator3ID(this.UserSecurityID);
    }

    public void setCaseAdministrator3ID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CaseAdministrator3ID' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[CaseAdministrator3ID]=["+newValue+"]");
                   dbDB.setCaseAdministrator3ID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[CaseAdministrator3ID]=["+newValue+"]");
           dbDB.setCaseAdministrator3ID(newValue);
         }
    }
    public Integer getCaseAdministrator3ID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CaseAdministrator3ID' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getCaseAdministrator3ID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getCaseAdministrator3ID();
         }
        return myVal;
    }

    public void setCaseAdministrator4ID(Integer newValue)
    {
        this.setCaseAdministrator4ID(newValue,this.UserSecurityID);


    }
    public Integer getCaseAdministrator4ID()
    {
        return this.getCaseAdministrator4ID(this.UserSecurityID);
    }

    public void setCaseAdministrator4ID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CaseAdministrator4ID' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[CaseAdministrator4ID]=["+newValue+"]");
                   dbDB.setCaseAdministrator4ID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[CaseAdministrator4ID]=["+newValue+"]");
           dbDB.setCaseAdministrator4ID(newValue);
         }
    }
    public Integer getCaseAdministrator4ID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CaseAdministrator4ID' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getCaseAdministrator4ID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getCaseAdministrator4ID();
         }
        return myVal;
    }

    public void setEmployerLocationID(Integer newValue)
    {
        this.setEmployerLocationID(newValue,this.UserSecurityID);


    }
    public Integer getEmployerLocationID()
    {
        return this.getEmployerLocationID(this.UserSecurityID);
    }

    public void setEmployerLocationID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='EmployerLocationID' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[EmployerLocationID]=["+newValue+"]");
                   dbDB.setEmployerLocationID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[EmployerLocationID]=["+newValue+"]");
           dbDB.setEmployerLocationID(newValue);
         }
    }
    public Integer getEmployerLocationID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='EmployerLocationID' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getEmployerLocationID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getEmployerLocationID();
         }
        return myVal;
    }

    public void setPatientEmail(String newValue)
    {
        this.setPatientEmail(newValue,this.UserSecurityID);


    }
    public String getPatientEmail()
    {
        return this.getPatientEmail(this.UserSecurityID);
    }

    public void setPatientEmail(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientEmail' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[PatientEmail]=["+newValue+"]");
                   dbDB.setPatientEmail(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[PatientEmail]=["+newValue+"]");
           dbDB.setPatientEmail(newValue);
         }
    }
    public String getPatientEmail(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientEmail' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPatientEmail();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPatientEmail();
         }
        return myVal;
    }

    public void setMobilePhoneCarrier(String newValue)
    {
        this.setMobilePhoneCarrier(newValue,this.UserSecurityID);


    }
    public String getMobilePhoneCarrier()
    {
        return this.getMobilePhoneCarrier(this.UserSecurityID);
    }

    public void setMobilePhoneCarrier(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='MobilePhoneCarrier' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[MobilePhoneCarrier]=["+newValue+"]");
                   dbDB.setMobilePhoneCarrier(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[MobilePhoneCarrier]=["+newValue+"]");
           dbDB.setMobilePhoneCarrier(newValue);
         }
    }
    public String getMobilePhoneCarrier(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='MobilePhoneCarrier' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getMobilePhoneCarrier();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getMobilePhoneCarrier();
         }
        return myVal;
    }

    public void setIsCurrentlyOnMeds(Integer newValue)
    {
        this.setIsCurrentlyOnMeds(newValue,this.UserSecurityID);


    }
    public Integer getIsCurrentlyOnMeds()
    {
        return this.getIsCurrentlyOnMeds(this.UserSecurityID);
    }

    public void setIsCurrentlyOnMeds(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='IsCurrentlyOnMeds' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[IsCurrentlyOnMeds]=["+newValue+"]");
                   dbDB.setIsCurrentlyOnMeds(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[IsCurrentlyOnMeds]=["+newValue+"]");
           dbDB.setIsCurrentlyOnMeds(newValue);
         }
    }
    public Integer getIsCurrentlyOnMeds(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='IsCurrentlyOnMeds' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getIsCurrentlyOnMeds();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getIsCurrentlyOnMeds();
         }
        return myVal;
    }

    public void setIsCurrentlyOnMedsDesc(String newValue)
    {
        this.setIsCurrentlyOnMedsDesc(newValue,this.UserSecurityID);


    }
    public String getIsCurrentlyOnMedsDesc()
    {
        return this.getIsCurrentlyOnMedsDesc(this.UserSecurityID);
    }

    public void setIsCurrentlyOnMedsDesc(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='IsCurrentlyOnMedsDesc' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[IsCurrentlyOnMedsDesc]=["+newValue+"]");
                   dbDB.setIsCurrentlyOnMedsDesc(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[IsCurrentlyOnMedsDesc]=["+newValue+"]");
           dbDB.setIsCurrentlyOnMedsDesc(newValue);
         }
    }
    public String getIsCurrentlyOnMedsDesc(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='IsCurrentlyOnMedsDesc' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getIsCurrentlyOnMedsDesc();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getIsCurrentlyOnMedsDesc();
         }
        return myVal;
    }

    public void setRequiredFieldList(java.util.Hashtable requiredFieldList)
	 {

        this.requiredFieldList = requiredFieldList;
    }

    public java.util.Hashtable getRequiredFieldList()
    {
        return this.requiredFieldList;
    }
    public void setExpiredFieldList(java.util.Hashtable expiredFieldList)
    {
        this.expiredFieldList = expiredFieldList;
    }

    public void setEnglishFieldList(java.util.Hashtable englishFieldList)
    {
        this.englishFieldList = englishFieldList;
    }

    public java.util.Hashtable getExpiredFieldList()
    {
        return this.expiredFieldList;
    }
    public boolean isExpired(java.util.Date myDate, int daysVector)
    {
       return isExpired(myDate,new java.util.Date(),daysVector);
    }


    public boolean isRead(String fieldN, Integer GroupRefID)
    {
        boolean myVal = false;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='"+fieldN+"' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==1)
               {
                   myVal = true;
               }
            }
        }
        else
        {

           if (iSecurityCheck.intValue()>=1)
           {
                  myVal = true;
           }
        }
        return myVal;
    }


    public boolean isWrite(String fieldN, Integer GroupRefID)
    {
        boolean myVal = false;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='"+fieldN+"' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {
                   myVal = true;
               }
            }
        }
        else
        {

           if (iSecurityCheck.intValue()>=2)
           {
                  myVal = true;
           }
        }
        return myVal;
    }


    public boolean isExpired(java.util.Date myDate, java.util.Date refDate,int daysVector)
    {
        java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat("yyyy-MM-dd");
        boolean myVal = true;
        try
        {
            if (myDate.after(dbdf.parse("1800-01-10")))
            {
                if (myDate.after(dateChange(refDate,daysVector)))
                {
                    myVal=false;
                }
            }
            else
            {
                myVal = false;
            }
        }
        catch (Exception Datee)
        {
        }
        return myVal;
    }

    public java.util.Date dateChange(java.util.Date myD, int dayChange)
    {
        java.util.Calendar calendar = java.util.Calendar.getInstance();
        calendar.setTime(myD);
        calendar.add(java.util.Calendar.DATE, dayChange);
        java.util.Date resultAsDate = new java.util.Date(calendar.getTime().getTime());
        return resultAsDate;
    }

    public boolean isRequired(String fieldN)
    {
        return isRequired(fieldN, new Integer("1"));
    }

    public boolean isRequired(String fieldN, Integer GroupRefID)
    {
        boolean myVal = false;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='"+fieldN+"' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getRequiredID().intValue()==1)
               {
                   myVal = true;
               }
            }
        }
        else
        {
            if (this.getRequiredFieldList().containsKey(fieldN))
            {
                myVal = true;
            }
        }
        return myVal;
    }
    public boolean isExpiredCheck(String fieldN)
    {
        return isExpiredCheck(fieldN, new Integer ("1"));
    }

    public boolean isExpiredCheck(String fieldN, Integer GroupRefID)
    {
        boolean myVal = false;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='"+fieldN+"' AND TableName='tNIM3_CaseAccount'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getTrackedID().intValue()==1)
               {
                   myVal = true;
               }
            }
        }
        else
        {
            if (this.getExpiredFieldList().containsKey(fieldN))
           {
                myVal = true;
            }
        }
        return myVal;
    }

    public void makeRequiredHash()
    {
       makeRequiredHash(new Integer(1));
    }

    public void makeRequiredHash(Integer GroupRefID)
    {

        java.util.Hashtable newHash = new java.util.Hashtable();
        if (runFieldSecurity)
        {
            java.util.Enumeration myKeys = englishFieldList.keys(); 
            while (myKeys.hasMoreElements()) 
            {
                 String myElement = (String) myKeys.nextElement();
                 bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='"+myElement+"' AND TableName='tNIM3_CaseAccount'" ,"");
                 bltFieldSecurity        working_bltFieldSecurity;
                 ListElement         leCurrentElement;
                 Enumeration eList = myFSL.elements();
                 if (eList.hasMoreElements())
                 {
                     leCurrentElement    = (ListElement) eList.nextElement();
                     working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
                     if (working_bltFieldSecurity.getRequiredID().intValue()==1)
                     {
                         newHash.put(myElement,new Boolean(true));
                     }
                 }
            }
        }
        else
        {
            newHash.put("PayerID",new Boolean(true));
            newHash.put("CaseClaimNumber",new Boolean(true));
            newHash.put("CaseCode",new Boolean(true));
            newHash.put("CaseStatusID",new Boolean(true));
            newHash.put("AdjusterID",new Boolean(true));
            newHash.put("PatientFirstName",new Boolean(true));
            newHash.put("PatientLastName",new Boolean(true));
            newHash.put("PatientDOB",new Boolean(true));
            newHash.put("PatientGender",new Boolean(true));
            newHash.put("PatientAddress1",new Boolean(true));
            newHash.put("PatientCity",new Boolean(true));
            newHash.put("PatientStateID",new Boolean(true));
            newHash.put("PatientZIP",new Boolean(true));
        }
        this.setRequiredFieldList(newHash);
    }

    public void makeEnglishHash()
    {

        java.util.Hashtable newHash = new java.util.Hashtable();        newHash.put("UniqueCreateDate","UniqueCreateDate");
        newHash.put("UniqueModifyDate","UniqueModifyDate");
        newHash.put("UniqueModifyComments","UniqueModifyComments");
        newHash.put("PayerID","PayerID");
        newHash.put("CaseClaimNumber","Claim Number");
        newHash.put("CaseCode","CaseCode");
        newHash.put("WCBNumber","WCBN Number");
        newHash.put("CaseStatusID","Status");
        newHash.put("AdjusterID","Adjuster");
        newHash.put("OccMedPatientID","Occ Med Patient ID");
        newHash.put("EmployerID","EmployerID");
        newHash.put("EmployerName","Employer");
        newHash.put("EmployerPhone","Employer Phone");
        newHash.put("EmployerFax","Employer Fax");
        newHash.put("JobID","JobID");
        newHash.put("JobNotes","JobNotes");
        newHash.put("PayerAccountIDNumber","Payer Account ID Number");
        newHash.put("PatientFirstName","Patient First Name");
        newHash.put("PatientLastName","PatientLastName");
        newHash.put("PatientAccountNumber","Patient Account Number");
        newHash.put("PatientExpirationDate","Patient Expiration Date");
        newHash.put("PatientIsActive","Patient Active");
        newHash.put("PatientDOB","Patient DOB");
        newHash.put("PatientSSN","Patient SSN");
        newHash.put("PatientGender","Patient Gender");
        newHash.put("PatientAddress1","Patient Address1");
        newHash.put("PatientAddress2","Patient Address2");
        newHash.put("PatientCity","Patient City");
        newHash.put("PatientStateID","Patient State");
        newHash.put("PatientZIP","Patient ZIP");
        newHash.put("PatientHomePhone","Patient Home Phone");
        newHash.put("PatientWorkPhone","Patient Work Phone");
        newHash.put("PatientCellPhone","Patient Cell Phone");
        newHash.put("PatientCCAccountFullName","Patient CC Full Name");
        newHash.put("PatientCCAccountNumber1","Patient CC Account Number1");
        newHash.put("PatientCCAccountNumber2","Patient CC Account Number2");
        newHash.put("PatientCCAccountType","Patient CC Account Type");
        newHash.put("PatientCCAccountExpMonth","Patient CC ExpMonth");
        newHash.put("PatientCCAccountExpYear","Patient CC ExpYear");
        newHash.put("SupervisorName","SupervisorName");
        newHash.put("SupervisorPhone","SupervisorPhone");
        newHash.put("SupervisorFax","SupervisorFax");
        newHash.put("DateOfInjury","Date Of Injury");
        newHash.put("InjuryDescription","Injury Description");
        newHash.put("LODID","Level of Disability");
        newHash.put("NurseCaseManagerID","Nurse Case Manager");
        newHash.put("PatientAccountID","Patient ID");
        newHash.put("AttorneyFirstName","Attorney First Name");
        newHash.put("AttorneyLastName","Attorney Last Name");
        newHash.put("AttorneyAddress1","Attorney Address1");
        newHash.put("AttorneyAddress2","Attorney Address2");
        newHash.put("AttorneyCity","Attorney City");
        newHash.put("AttorneyStateID","Attorney State");
        newHash.put("AttorneyZIP","Attorney ZIP");
        newHash.put("AttorneyFax","Attorney Fax");
        newHash.put("AttorneyWorkPhone","Attorney Work Phone");
        newHash.put("AttorneyCellPhone","Attorney Cell Phone");
        newHash.put("Comments","Comments");
        newHash.put("AuditNotes","Audit Notes");
        newHash.put("PatientIsClaus","PatientIsClaus");
        newHash.put("PatientIsHWRatio","PatientIsHWRatio");
        newHash.put("AssignedToID","AssignedToID");
        newHash.put("AssignedToRoleID","AssignedToRoleID");
        newHash.put("CaseAdministratorID","Case Administrator");
        newHash.put("BillToContactID","BillToContactID");
        newHash.put("URContactID","URContactID");
        newHash.put("ReferredByContactID","ReferredByContactID");
        newHash.put("PatientHasImplants","PatientHasImplants");
        newHash.put("PatientHasMetalInBody","PatientHasMetalInBody");
        newHash.put("PatientHasAllergies","PatientHasAllergies");
        newHash.put("PatientHasRecentSurgery","PatientHasRecentSurgery");
        newHash.put("PatientHasPreviousMRIs","PatientHasPreviousMRIs");
        newHash.put("PatientHasKidneyLiverHypertensionDiabeticConditions","PatientHasKidneyLiverHypertensionDiabeticConditions");
        newHash.put("PatientIsPregnant","PatientIsPregnant");
        newHash.put("PatientHeight","PatientHeight");
        newHash.put("PatientWeight","PatientWeight");
        newHash.put("PatientHasImplantsDesc","PatientHasImplantsDesc");
        newHash.put("PatientHasMetalInBodyDesc","PatientHasMetalInBodyDesc");
        newHash.put("PatientHasAllergiesDesc","PatientHasAllergiesDesc");
        newHash.put("PatientHasRecentSurgeryDesc","PatientHasRecentSurgeryDesc");
        newHash.put("PatientHasPreviousMRIsDesc","PatientHasPreviousMRIsDesc");
        newHash.put("PatientHasKidneyLiverHypertensionDiabeticConditionsDesc","PatientHasKidneyLiverHypertensionDiabeticConditionsDesc");
        newHash.put("PatientIsPregnantDesc","PatientIsPregnantDesc");
        newHash.put("CaseAdministrator2ID","Case Administrator 2");
        newHash.put("CaseAdministrator3ID","Case Administrator 3");
        newHash.put("CaseAdministrator4ID","Case Administrator 4");
        newHash.put("EmployerLocationID","Employer_Location");
        newHash.put("PatientEmail","Patient Email");
        newHash.put("MobilePhoneCarrier","Mobile Phone Carrier");
        newHash.put("IsCurrentlyOnMeds","IsCurrentlyOnMeds");
        newHash.put("IsCurrentlyOnMedsDesc","IsCurrentlyOnMedsDesc");

        this.setEnglishFieldList(newHash);
    }
    public void makeExpiredHash()
    {

        java.util.Hashtable newHash = new java.util.Hashtable();
        this.setExpiredFieldList(newHash);
    }

    public java.util.Hashtable englishFieldList = new java.util.Hashtable();
    public java.util.Hashtable requiredFieldList = new java.util.Hashtable();
    public java.util.Hashtable expiredFieldList = new java.util.Hashtable();

    public boolean isComplete()
    {
	     boolean myVal = true;
	     java.util.Enumeration myKeys = this.getRequiredFieldList().keys(); 
	     while (myKeys.hasMoreElements()) 
	     {
	         String myElement = (String) myKeys.nextElement();
	         if (!isComplete(myElement))
	         {
	             myVal = false;
	         }
	     }
	     return myVal;
    }

    public String getEnglish(String fieldN)
    {
        String myVal = null;
        if (this.englishFieldList.containsKey(fieldN))
        {
            myVal = (String)this.englishFieldList.get(fieldN);
        }
        return myVal;
    }
    public java.util.Vector isComplete(int daysVector)
    {
	     boolean myVal = false;
	     java.util.Enumeration myKeys = this.getExpiredFieldList().keys(); 
	     java.util.Vector myExpiredList = new java.util.Vector(); 
	     while (myKeys.hasMoreElements()) 
	     {
	         String myElement = (String) myKeys.nextElement();
	         if (isExpired(myElement,daysVector))
	         {
	             myVal = true;
	             myExpiredList.addElement(myElement);
	         }
	     }
	     return myExpiredList;
    }

    public void setField(String fieldN, Object fieldV)
    {
     boolean myVal = false;
     try
     {
        java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat("yyyy-MM-dd");

        if (fieldN.equals("CaseID"))
        {
             this.setCaseID((Integer)fieldV);
        }

        else if (fieldN.equals("UniqueCreateDate"))
        {
            this.setUniqueCreateDate((java.util.Date)fieldV);
        }

        else if (fieldN.equals("UniqueModifyDate"))
        {
            this.setUniqueModifyDate((java.util.Date)fieldV);
        }

        else if (fieldN.equals("UniqueModifyComments"))
        {
            this.setUniqueModifyComments((String)fieldV);
        }

        else if (fieldN.equals("PayerID"))
        {
             this.setPayerID((Integer)fieldV);
        }

        else if (fieldN.equals("CaseClaimNumber"))
        {
            this.setCaseClaimNumber((String)fieldV);
        }

        else if (fieldN.equals("CaseCode"))
        {
            this.setCaseCode((String)fieldV);
        }

        else if (fieldN.equals("WCBNumber"))
        {
            this.setWCBNumber((String)fieldV);
        }

        else if (fieldN.equals("CaseStatusID"))
        {
             this.setCaseStatusID((Integer)fieldV);
        }

        else if (fieldN.equals("AdjusterID"))
        {
             this.setAdjusterID((Integer)fieldV);
        }

        else if (fieldN.equals("OccMedPatientID"))
        {
            this.setOccMedPatientID((String)fieldV);
        }

        else if (fieldN.equals("EmployerID"))
        {
             this.setEmployerID((Integer)fieldV);
        }

        else if (fieldN.equals("EmployerName"))
        {
            this.setEmployerName((String)fieldV);
        }

        else if (fieldN.equals("EmployerPhone"))
        {
            this.setEmployerPhone((String)fieldV);
        }

        else if (fieldN.equals("EmployerFax"))
        {
            this.setEmployerFax((String)fieldV);
        }

        else if (fieldN.equals("JobID"))
        {
             this.setJobID((Integer)fieldV);
        }

        else if (fieldN.equals("JobNotes"))
        {
            this.setJobNotes((String)fieldV);
        }

        else if (fieldN.equals("PayerAccountIDNumber"))
        {
            this.setPayerAccountIDNumber((String)fieldV);
        }

        else if (fieldN.equals("PatientFirstName"))
        {
            this.setPatientFirstName((String)fieldV);
        }

        else if (fieldN.equals("PatientLastName"))
        {
            this.setPatientLastName((String)fieldV);
        }

        else if (fieldN.equals("PatientAccountNumber"))
        {
            this.setPatientAccountNumber((String)fieldV);
        }

        else if (fieldN.equals("PatientExpirationDate"))
        {
            this.setPatientExpirationDate((java.util.Date)fieldV);
        }

        else if (fieldN.equals("PatientIsActive"))
        {
             this.setPatientIsActive((Integer)fieldV);
        }

        else if (fieldN.equals("PatientDOB"))
        {
            this.setPatientDOB((java.util.Date)fieldV);
        }

        else if (fieldN.equals("PatientSSN"))
        {
            this.setPatientSSN((String)fieldV);
        }

        else if (fieldN.equals("PatientGender"))
        {
            this.setPatientGender((String)fieldV);
        }

        else if (fieldN.equals("PatientAddress1"))
        {
            this.setPatientAddress1((String)fieldV);
        }

        else if (fieldN.equals("PatientAddress2"))
        {
            this.setPatientAddress2((String)fieldV);
        }

        else if (fieldN.equals("PatientCity"))
        {
            this.setPatientCity((String)fieldV);
        }

        else if (fieldN.equals("PatientStateID"))
        {
             this.setPatientStateID((Integer)fieldV);
        }

        else if (fieldN.equals("PatientZIP"))
        {
            this.setPatientZIP((String)fieldV);
        }

        else if (fieldN.equals("PatientHomePhone"))
        {
            this.setPatientHomePhone((String)fieldV);
        }

        else if (fieldN.equals("PatientWorkPhone"))
        {
            this.setPatientWorkPhone((String)fieldV);
        }

        else if (fieldN.equals("PatientCellPhone"))
        {
            this.setPatientCellPhone((String)fieldV);
        }

        else if (fieldN.equals("PatientCCAccountFullName"))
        {
            this.setPatientCCAccountFullName((String)fieldV);
        }

        else if (fieldN.equals("PatientCCAccountNumber1"))
        {
            this.setPatientCCAccountNumber1((String)fieldV);
        }

        else if (fieldN.equals("PatientCCAccountNumber2"))
        {
            this.setPatientCCAccountNumber2((String)fieldV);
        }

        else if (fieldN.equals("PatientCCAccountType"))
        {
            this.setPatientCCAccountType((String)fieldV);
        }

        else if (fieldN.equals("PatientCCAccountExpMonth"))
        {
            this.setPatientCCAccountExpMonth((String)fieldV);
        }

        else if (fieldN.equals("PatientCCAccountExpYear"))
        {
            this.setPatientCCAccountExpYear((String)fieldV);
        }

        else if (fieldN.equals("SupervisorName"))
        {
            this.setSupervisorName((String)fieldV);
        }

        else if (fieldN.equals("SupervisorPhone"))
        {
            this.setSupervisorPhone((String)fieldV);
        }

        else if (fieldN.equals("SupervisorFax"))
        {
            this.setSupervisorFax((String)fieldV);
        }

        else if (fieldN.equals("DateOfInjury"))
        {
            this.setDateOfInjury((java.util.Date)fieldV);
        }

        else if (fieldN.equals("InjuryDescription"))
        {
            this.setInjuryDescription((String)fieldV);
        }

        else if (fieldN.equals("LODID"))
        {
             this.setLODID((Integer)fieldV);
        }

        else if (fieldN.equals("NurseCaseManagerID"))
        {
             this.setNurseCaseManagerID((Integer)fieldV);
        }

        else if (fieldN.equals("PatientAccountID"))
        {
             this.setPatientAccountID((Integer)fieldV);
        }

        else if (fieldN.equals("AttorneyFirstName"))
        {
            this.setAttorneyFirstName((String)fieldV);
        }

        else if (fieldN.equals("AttorneyLastName"))
        {
            this.setAttorneyLastName((String)fieldV);
        }

        else if (fieldN.equals("AttorneyAddress1"))
        {
            this.setAttorneyAddress1((String)fieldV);
        }

        else if (fieldN.equals("AttorneyAddress2"))
        {
            this.setAttorneyAddress2((String)fieldV);
        }

        else if (fieldN.equals("AttorneyCity"))
        {
            this.setAttorneyCity((String)fieldV);
        }

        else if (fieldN.equals("AttorneyStateID"))
        {
             this.setAttorneyStateID((Integer)fieldV);
        }

        else if (fieldN.equals("AttorneyZIP"))
        {
            this.setAttorneyZIP((String)fieldV);
        }

        else if (fieldN.equals("AttorneyFax"))
        {
            this.setAttorneyFax((String)fieldV);
        }

        else if (fieldN.equals("AttorneyWorkPhone"))
        {
            this.setAttorneyWorkPhone((String)fieldV);
        }

        else if (fieldN.equals("AttorneyCellPhone"))
        {
            this.setAttorneyCellPhone((String)fieldV);
        }

        else if (fieldN.equals("Comments"))
        {
            this.setComments((String)fieldV);
        }

        else if (fieldN.equals("AuditNotes"))
        {
            this.setAuditNotes((String)fieldV);
        }

        else if (fieldN.equals("PatientIsClaus"))
        {
             this.setPatientIsClaus((Integer)fieldV);
        }

        else if (fieldN.equals("PatientIsHWRatio"))
        {
             this.setPatientIsHWRatio((Integer)fieldV);
        }

        else if (fieldN.equals("AssignedToID"))
        {
             this.setAssignedToID((Integer)fieldV);
        }

        else if (fieldN.equals("AssignedToRoleID"))
        {
             this.setAssignedToRoleID((Integer)fieldV);
        }

        else if (fieldN.equals("CaseAdministratorID"))
        {
             this.setCaseAdministratorID((Integer)fieldV);
        }

        else if (fieldN.equals("BillToContactID"))
        {
             this.setBillToContactID((Integer)fieldV);
        }

        else if (fieldN.equals("URContactID"))
        {
             this.setURContactID((Integer)fieldV);
        }

        else if (fieldN.equals("ReferredByContactID"))
        {
             this.setReferredByContactID((Integer)fieldV);
        }

        else if (fieldN.equals("PatientHasImplants"))
        {
             this.setPatientHasImplants((Integer)fieldV);
        }

        else if (fieldN.equals("PatientHasMetalInBody"))
        {
             this.setPatientHasMetalInBody((Integer)fieldV);
        }

        else if (fieldN.equals("PatientHasAllergies"))
        {
             this.setPatientHasAllergies((Integer)fieldV);
        }

        else if (fieldN.equals("PatientHasRecentSurgery"))
        {
             this.setPatientHasRecentSurgery((Integer)fieldV);
        }

        else if (fieldN.equals("PatientHasPreviousMRIs"))
        {
             this.setPatientHasPreviousMRIs((Integer)fieldV);
        }

        else if (fieldN.equals("PatientHasKidneyLiverHypertensionDiabeticConditions"))
        {
             this.setPatientHasKidneyLiverHypertensionDiabeticConditions((Integer)fieldV);
        }

        else if (fieldN.equals("PatientIsPregnant"))
        {
             this.setPatientIsPregnant((Integer)fieldV);
        }

        else if (fieldN.equals("PatientHeight"))
        {
            this.setPatientHeight((String)fieldV);
        }

        else if (fieldN.equals("PatientWeight"))
        {
             this.setPatientWeight((Integer)fieldV);
        }

        else if (fieldN.equals("PatientHasImplantsDesc"))
        {
            this.setPatientHasImplantsDesc((String)fieldV);
        }

        else if (fieldN.equals("PatientHasMetalInBodyDesc"))
        {
            this.setPatientHasMetalInBodyDesc((String)fieldV);
        }

        else if (fieldN.equals("PatientHasAllergiesDesc"))
        {
            this.setPatientHasAllergiesDesc((String)fieldV);
        }

        else if (fieldN.equals("PatientHasRecentSurgeryDesc"))
        {
            this.setPatientHasRecentSurgeryDesc((String)fieldV);
        }

        else if (fieldN.equals("PatientHasPreviousMRIsDesc"))
        {
            this.setPatientHasPreviousMRIsDesc((String)fieldV);
        }

        else if (fieldN.equals("PatientHasKidneyLiverHypertensionDiabeticConditionsDesc"))
        {
            this.setPatientHasKidneyLiverHypertensionDiabeticConditionsDesc((String)fieldV);
        }

        else if (fieldN.equals("PatientIsPregnantDesc"))
        {
            this.setPatientIsPregnantDesc((String)fieldV);
        }

        else if (fieldN.equals("CaseAdministrator2ID"))
        {
             this.setCaseAdministrator2ID((Integer)fieldV);
        }

        else if (fieldN.equals("CaseAdministrator3ID"))
        {
             this.setCaseAdministrator3ID((Integer)fieldV);
        }

        else if (fieldN.equals("CaseAdministrator4ID"))
        {
             this.setCaseAdministrator4ID((Integer)fieldV);
        }

        else if (fieldN.equals("EmployerLocationID"))
        {
             this.setEmployerLocationID((Integer)fieldV);
        }

        else if (fieldN.equals("PatientEmail"))
        {
            this.setPatientEmail((String)fieldV);
        }

        else if (fieldN.equals("MobilePhoneCarrier"))
        {
            this.setMobilePhoneCarrier((String)fieldV);
        }

        else if (fieldN.equals("IsCurrentlyOnMeds"))
        {
             this.setIsCurrentlyOnMeds((Integer)fieldV);
        }

        else if (fieldN.equals("IsCurrentlyOnMedsDesc"))
        {
            this.setIsCurrentlyOnMedsDesc((String)fieldV);
        }

	    }
	    catch(Exception e2)
	    {}
	    //return myVal;
	}

    public String getFieldType(String fieldN)
    {
     String  myVal = null;
     try
     {
        java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat("yyyy-MM-dd");

        if (fieldN.equals("CaseID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("UniqueCreateDate"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("UniqueModifyDate"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("UniqueModifyComments"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("PayerID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("CaseClaimNumber"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("CaseCode"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("WCBNumber"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("CaseStatusID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("AdjusterID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("OccMedPatientID"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("EmployerID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("EmployerName"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("EmployerPhone"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("EmployerFax"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("JobID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("JobNotes"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("PayerAccountIDNumber"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("PatientFirstName"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("PatientLastName"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("PatientAccountNumber"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("PatientExpirationDate"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("PatientIsActive"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("PatientDOB"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("PatientSSN"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("PatientGender"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("PatientAddress1"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("PatientAddress2"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("PatientCity"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("PatientStateID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("PatientZIP"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("PatientHomePhone"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("PatientWorkPhone"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("PatientCellPhone"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("PatientCCAccountFullName"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("PatientCCAccountNumber1"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("PatientCCAccountNumber2"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("PatientCCAccountType"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("PatientCCAccountExpMonth"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("PatientCCAccountExpYear"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("SupervisorName"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("SupervisorPhone"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("SupervisorFax"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("DateOfInjury"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("InjuryDescription"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("LODID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("NurseCaseManagerID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("PatientAccountID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("AttorneyFirstName"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("AttorneyLastName"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("AttorneyAddress1"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("AttorneyAddress2"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("AttorneyCity"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("AttorneyStateID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("AttorneyZIP"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("AttorneyFax"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("AttorneyWorkPhone"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("AttorneyCellPhone"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("Comments"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("AuditNotes"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("PatientIsClaus"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("PatientIsHWRatio"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("AssignedToID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("AssignedToRoleID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("CaseAdministratorID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("BillToContactID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("URContactID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("ReferredByContactID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("PatientHasImplants"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("PatientHasMetalInBody"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("PatientHasAllergies"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("PatientHasRecentSurgery"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("PatientHasPreviousMRIs"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("PatientHasKidneyLiverHypertensionDiabeticConditions"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("PatientIsPregnant"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("PatientHeight"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("PatientWeight"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("PatientHasImplantsDesc"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("PatientHasMetalInBodyDesc"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("PatientHasAllergiesDesc"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("PatientHasRecentSurgeryDesc"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("PatientHasPreviousMRIsDesc"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("PatientHasKidneyLiverHypertensionDiabeticConditionsDesc"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("PatientIsPregnantDesc"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("CaseAdministrator2ID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("CaseAdministrator3ID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("CaseAdministrator4ID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("EmployerLocationID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("PatientEmail"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("MobilePhoneCarrier"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("IsCurrentlyOnMeds"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("IsCurrentlyOnMedsDesc"))
        {
            myVal = "String";
        }

	    }
	    catch(Exception e2)
	    {}
	    return myVal;
	}

    public boolean isComplete(String fieldN)
    {
	     boolean myVal = false;
	     try
	     {
        java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat("yyyy-MM-dd");

	     if (fieldN.equals("CaseID"))

	     {
                    if (this.getCaseID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("UniqueCreateDate"))

	     {
                    if (this.getUniqueCreateDate().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("UniqueModifyDate"))

	     {
                    if (this.getUniqueModifyDate().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("UniqueModifyComments"))

	     {
                    if (this.getUniqueModifyComments().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("PayerID"))

	     {
                    if (this.getPayerID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("CaseClaimNumber"))

	     {
                    if (this.getCaseClaimNumber().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("CaseCode"))

	     {
                    if (this.getCaseCode().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("WCBNumber"))

	     {
                    if (this.getWCBNumber().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("CaseStatusID"))

	     {
                    if (this.getCaseStatusID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("AdjusterID"))

	     {
                    if (this.getAdjusterID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("OccMedPatientID"))

	     {
                    if (this.getOccMedPatientID().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("EmployerID"))

	     {
                    if (this.getEmployerID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("EmployerName"))

	     {
                    if (this.getEmployerName().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("EmployerPhone"))

	     {
                    if (this.getEmployerPhone().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("EmployerFax"))

	     {
                    if (this.getEmployerFax().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("JobID"))

	     {
                    if (this.getJobID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("JobNotes"))

	     {
                    if (this.getJobNotes().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("PayerAccountIDNumber"))

	     {
                    if (this.getPayerAccountIDNumber().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("PatientFirstName"))

	     {
                    if (this.getPatientFirstName().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("PatientLastName"))

	     {
                    if (this.getPatientLastName().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("PatientAccountNumber"))

	     {
                    if (this.getPatientAccountNumber().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("PatientExpirationDate"))

	     {
                    if (this.getPatientExpirationDate().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("PatientIsActive"))

	     {
                    if (this.getPatientIsActive().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("PatientDOB"))

	     {
                    if (this.getPatientDOB().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("PatientSSN"))

	     {
                    if (this.getPatientSSN().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("PatientGender"))

	     {
                    if (this.getPatientGender().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("PatientAddress1"))

	     {
                    if (this.getPatientAddress1().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("PatientAddress2"))

	     {
                    if (this.getPatientAddress2().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("PatientCity"))

	     {
                    if (this.getPatientCity().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("PatientStateID"))

	     {
                    if (this.getPatientStateID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("PatientZIP"))

	     {
                    if (this.getPatientZIP().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("PatientHomePhone"))

	     {
                    if (this.getPatientHomePhone().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("PatientWorkPhone"))

	     {
                    if (this.getPatientWorkPhone().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("PatientCellPhone"))

	     {
                    if (this.getPatientCellPhone().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("PatientCCAccountFullName"))

	     {
                    if (this.getPatientCCAccountFullName().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("PatientCCAccountNumber1"))

	     {
                    if (this.getPatientCCAccountNumber1().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("PatientCCAccountNumber2"))

	     {
                    if (this.getPatientCCAccountNumber2().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("PatientCCAccountType"))

	     {
                    if (this.getPatientCCAccountType().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("PatientCCAccountExpMonth"))

	     {
                    if (this.getPatientCCAccountExpMonth().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("PatientCCAccountExpYear"))

	     {
                    if (this.getPatientCCAccountExpYear().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("SupervisorName"))

	     {
                    if (this.getSupervisorName().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("SupervisorPhone"))

	     {
                    if (this.getSupervisorPhone().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("SupervisorFax"))

	     {
                    if (this.getSupervisorFax().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("DateOfInjury"))

	     {
                    if (this.getDateOfInjury().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("InjuryDescription"))

	     {
                    if (this.getInjuryDescription().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("LODID"))

	     {
                    if (this.getLODID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("NurseCaseManagerID"))

	     {
                    if (this.getNurseCaseManagerID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("PatientAccountID"))

	     {
                    if (this.getPatientAccountID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("AttorneyFirstName"))

	     {
                    if (this.getAttorneyFirstName().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("AttorneyLastName"))

	     {
                    if (this.getAttorneyLastName().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("AttorneyAddress1"))

	     {
                    if (this.getAttorneyAddress1().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("AttorneyAddress2"))

	     {
                    if (this.getAttorneyAddress2().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("AttorneyCity"))

	     {
                    if (this.getAttorneyCity().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("AttorneyStateID"))

	     {
                    if (this.getAttorneyStateID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("AttorneyZIP"))

	     {
                    if (this.getAttorneyZIP().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("AttorneyFax"))

	     {
                    if (this.getAttorneyFax().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("AttorneyWorkPhone"))

	     {
                    if (this.getAttorneyWorkPhone().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("AttorneyCellPhone"))

	     {
                    if (this.getAttorneyCellPhone().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("Comments"))

	     {
                    if (this.getComments().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("AuditNotes"))

	     {
                    if (this.getAuditNotes().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("PatientIsClaus"))

	     {
                    if (this.getPatientIsClaus().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("PatientIsHWRatio"))

	     {
                    if (this.getPatientIsHWRatio().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("AssignedToID"))

	     {
                    if (this.getAssignedToID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("AssignedToRoleID"))

	     {
                    if (this.getAssignedToRoleID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("CaseAdministratorID"))

	     {
                    if (this.getCaseAdministratorID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("BillToContactID"))

	     {
                    if (this.getBillToContactID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("URContactID"))

	     {
                    if (this.getURContactID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("ReferredByContactID"))

	     {
                    if (this.getReferredByContactID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("PatientHasImplants"))

	     {
                    if (this.getPatientHasImplants().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("PatientHasMetalInBody"))

	     {
                    if (this.getPatientHasMetalInBody().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("PatientHasAllergies"))

	     {
                    if (this.getPatientHasAllergies().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("PatientHasRecentSurgery"))

	     {
                    if (this.getPatientHasRecentSurgery().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("PatientHasPreviousMRIs"))

	     {
                    if (this.getPatientHasPreviousMRIs().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("PatientHasKidneyLiverHypertensionDiabeticConditions"))

	     {
                    if (this.getPatientHasKidneyLiverHypertensionDiabeticConditions().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("PatientIsPregnant"))

	     {
                    if (this.getPatientIsPregnant().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("PatientHeight"))

	     {
                    if (this.getPatientHeight().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("PatientWeight"))

	     {
                    if (this.getPatientWeight().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("PatientHasImplantsDesc"))

	     {
                    if (this.getPatientHasImplantsDesc().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("PatientHasMetalInBodyDesc"))

	     {
                    if (this.getPatientHasMetalInBodyDesc().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("PatientHasAllergiesDesc"))

	     {
                    if (this.getPatientHasAllergiesDesc().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("PatientHasRecentSurgeryDesc"))

	     {
                    if (this.getPatientHasRecentSurgeryDesc().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("PatientHasPreviousMRIsDesc"))

	     {
                    if (this.getPatientHasPreviousMRIsDesc().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("PatientHasKidneyLiverHypertensionDiabeticConditionsDesc"))

	     {
                    if (this.getPatientHasKidneyLiverHypertensionDiabeticConditionsDesc().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("PatientIsPregnantDesc"))

	     {
                    if (this.getPatientIsPregnantDesc().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("CaseAdministrator2ID"))

	     {
                    if (this.getCaseAdministrator2ID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("CaseAdministrator3ID"))

	     {
                    if (this.getCaseAdministrator3ID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("CaseAdministrator4ID"))

	     {
                    if (this.getCaseAdministrator4ID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("EmployerLocationID"))

	     {
                    if (this.getEmployerLocationID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("PatientEmail"))

	     {
                    if (this.getPatientEmail().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("MobilePhoneCarrier"))

	     {
                    if (this.getMobilePhoneCarrier().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("IsCurrentlyOnMeds"))

	     {
                    if (this.getIsCurrentlyOnMeds().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("IsCurrentlyOnMedsDesc"))

	     {
                    if (this.getIsCurrentlyOnMedsDesc().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	    }
	    catch(Exception e2)
	    {}
	    return myVal;
	}

    public boolean isExpired(String fieldN,int daysVector)
    {
        return isExpired(fieldN,new java.util.Date(),daysVector);
    }

    public boolean isExpired(String fieldN,java.util.Date refDate,int daysVector)
    {
	     boolean myVal = false;
	     try
	     {
        java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat("yyyy-MM-dd");

        if (fieldN.equals("UniqueCreateDate"))

	     {
                    if (this.isExpired(this.getUniqueCreateDate(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

        if (fieldN.equals("UniqueModifyDate"))

	     {
                    if (this.isExpired(this.getUniqueModifyDate(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

        if (fieldN.equals("PatientExpirationDate"))

	     {
                    if (this.isExpired(this.getPatientExpirationDate(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

        if (fieldN.equals("PatientDOB"))

	     {
                    if (this.isExpired(this.getPatientDOB(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

        if (fieldN.equals("DateOfInjury"))

	     {
                    if (this.isExpired(this.getDateOfInjury(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

	    }
	    catch(Exception e2)
	    {}
	    return myVal;
	}

    public java.util.Date getDateField(String fieldN)
    {
	     java.util.Date myVal = null;
	     try
	     {
        java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat("yyyy-MM-dd");

        if (fieldN.equals("UniqueCreateDate"))

	     {
             myVal = this.getUniqueCreateDate();
        }

        if (fieldN.equals("UniqueModifyDate"))

	     {
             myVal = this.getUniqueModifyDate();
        }

        if (fieldN.equals("PatientExpirationDate"))

	     {
             myVal = this.getPatientExpirationDate();
        }

        if (fieldN.equals("PatientDOB"))

	     {
             myVal = this.getPatientDOB();
        }

        if (fieldN.equals("DateOfInjury"))

	     {
             myVal = this.getDateOfInjury();
        }

	    }
	    catch(Exception e2)
	    {}
	    return myVal;
	}
	public boolean runFieldSecurity = false;
	public Integer UserSecurityID = null;
   public Integer iSecurityCheck = null;
}    // End of bltNIM3_CaseAccount class definition
