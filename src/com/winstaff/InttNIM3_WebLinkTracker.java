

package com.winstaff;

import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public interface InttNIM3_WebLinkTracker extends dbTableInterface
{

    public void setWebLinkTrackerID(Integer newValue);
    public Integer getWebLinkTrackerID();
    public void setUniqueCreateDate(Date newValue);
    public Date getUniqueCreateDate();
    public void setUniqueModifyDate(Date newValue);
    public Date getUniqueModifyDate();
    public void setUniqueModifyComments(String newValue);
    public String getUniqueModifyComments();
    public void setTransactionDate(Date newValue);
    public Date getTransactionDate();
    public void setPayerID(Integer newValue);
    public Integer getPayerID();
    public void setActionID(Integer newValue);
    public Integer getActionID();
    public void setRemoteIP(String newValue);
    public String getRemoteIP();
    public void setWebLink(String newValue);
    public String getWebLink();
    public void setUserID(Integer newValue);
    public Integer getUserID();
    public void setComments(String newValue);
    public String getComments();
    public void setUserAgent(String newValue);
    public String getUserAgent();
    public void setUserHash(String newValue);
    public String getUserHash();
}    // End of bltNIM3_WebLinkTracker class definition
