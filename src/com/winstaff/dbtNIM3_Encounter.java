

package com.winstaff;

import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public class dbtNIM3_Encounter extends Object implements InttNIM3_Encounter
{

        db_NewBase    dbnbDB;

    public dbtNIM3_Encounter()
    {
        dbnbDB = new db_NewBase( "tNIM3_Encounter", "EncounterID" );

    }    // End of default constructor

    public dbtNIM3_Encounter( Integer iNewID )
    {
        dbnbDB = new db_NewBase( "tNIM3_Encounter", "EncounterID" );
        setUniqueID( iNewID );
    }    // End of Constructor knowing an ID

    public void commitData() throws SQLException
    {
            //no longer trapping/ignoring errors 2011-03-17
            dbnbDB.commitData();
    }    // End of commitData()

    public void commitDataForced() throws SQLException
    {
        dbnbDB.commitDataForced();
    }    // End of commitData()

    public Integer getUniqueID()
    {
        return dbnbDB.getUniqueID();
    }    // End of getUniqueID()

    public void setUniqueID( Integer iNewID )
    {
        dbnbDB.setUniqueID( iNewID );
    }    // End of setUniqueID()

    public void setModifyComments( String sNew )
    {
        dbnbDB.setModifyComments( sNew );
    }    // End of setUniqueID()


    public void setEncounterID(Integer newValue)
    {
                dbnbDB.setFieldData( "EncounterID", newValue.toString() );
    }

    public Integer getEncounterID()
    {
        String           sValue = dbnbDB.getFieldData( "EncounterID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setField(String fieldN, Object fieldV)
    {DebugLogger.println("setField Method not implemented yet in classNIM3_Encounter!");}

    public String getFieldType(String fieldN)
    {return null;}


    public void setUniqueCreateDate(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "UniqueCreateDate", formatter.format( newValue ) );
    }

    public Date getUniqueCreateDate()
    {
        String           sValue = dbnbDB.getFieldData( "UniqueCreateDate" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setUniqueModifyDate(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "UniqueModifyDate", formatter.format( newValue ) );
    }

    public Date getUniqueModifyDate()
    {
        String           sValue = dbnbDB.getFieldData( "UniqueModifyDate" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setUniqueModifyComments(String newValue)
    {
                dbnbDB.setFieldData( "UniqueModifyComments", newValue.toString() );
    }

    public String getUniqueModifyComments()
    {
        String           sValue = dbnbDB.getFieldData( "UniqueModifyComments" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setReferralID(Integer newValue)
    {
                dbnbDB.setFieldData( "ReferralID", newValue.toString() );
    }

    public Integer getReferralID()
    {
        String           sValue = dbnbDB.getFieldData( "ReferralID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setEncounterTypeID(Integer newValue)
    {
                dbnbDB.setFieldData( "EncounterTypeID", newValue.toString() );
    }

    public Integer getEncounterTypeID()
    {
        String           sValue = dbnbDB.getFieldData( "EncounterTypeID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setScanPass(String newValue)
    {
                dbnbDB.setFieldData( "ScanPass", newValue.toString() );
    }

    public String getScanPass()
    {
        String           sValue = dbnbDB.getFieldData( "ScanPass" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setAppointmentID(Integer newValue)
    {
                dbnbDB.setFieldData( "AppointmentID", newValue.toString() );
    }

    public Integer getAppointmentID()
    {
        String           sValue = dbnbDB.getFieldData( "AppointmentID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setAttendingPhysicianID(Integer newValue)
    {
                dbnbDB.setFieldData( "AttendingPhysicianID", newValue.toString() );
    }

    public Integer getAttendingPhysicianID()
    {
        String           sValue = dbnbDB.getFieldData( "AttendingPhysicianID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setReferringPhysicianID(Integer newValue)
    {
                dbnbDB.setFieldData( "ReferringPhysicianID", newValue.toString() );
    }

    public Integer getReferringPhysicianID()
    {
        String           sValue = dbnbDB.getFieldData( "ReferringPhysicianID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setDateOfService(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "DateOfService", formatter.format( newValue ) );
    }

    public Date getDateOfService()
    {
        String           sValue = dbnbDB.getFieldData( "DateOfService" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setNextActionDate(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "NextActionDate", formatter.format( newValue ) );
    }

    public Date getNextActionDate()
    {
        String           sValue = dbnbDB.getFieldData( "NextActionDate" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setNextActionTaskID(Integer newValue)
    {
                dbnbDB.setFieldData( "NextActionTaskID", newValue.toString() );
    }

    public Integer getNextActionTaskID()
    {
        String           sValue = dbnbDB.getFieldData( "NextActionTaskID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setSentToRefDr(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "SentToRefDr", formatter.format( newValue ) );
    }

    public Date getSentToRefDr()
    {
        String           sValue = dbnbDB.getFieldData( "SentToRefDr" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setSentToRefDr_CRID(Integer newValue)
    {
                dbnbDB.setFieldData( "SentToRefDr_CRID", newValue.toString() );
    }

    public Integer getSentToRefDr_CRID()
    {
        String           sValue = dbnbDB.getFieldData( "SentToRefDr_CRID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setSentToAdj(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "SentToAdj", formatter.format( newValue ) );
    }

    public Date getSentToAdj()
    {
        String           sValue = dbnbDB.getFieldData( "SentToAdj" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setSentToAdj_CRID(Integer newValue)
    {
                dbnbDB.setFieldData( "SentToAdj_CRID", newValue.toString() );
    }

    public Integer getSentToAdj_CRID()
    {
        String           sValue = dbnbDB.getFieldData( "SentToAdj_CRID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setSentToIC(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "SentToIC", formatter.format( newValue ) );
    }

    public Date getSentToIC()
    {
        String           sValue = dbnbDB.getFieldData( "SentToIC" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setSentToIC_CRID(Integer newValue)
    {
                dbnbDB.setFieldData( "SentToIC_CRID", newValue.toString() );
    }

    public Integer getSentToIC_CRID()
    {
        String           sValue = dbnbDB.getFieldData( "SentToIC_CRID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setSentTo_Appt_Conf_Pat(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "SentTo_Appt_Conf_Pat", formatter.format( newValue ) );
    }

    public Date getSentTo_Appt_Conf_Pat()
    {
        String           sValue = dbnbDB.getFieldData( "SentTo_Appt_Conf_Pat" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setSentTo_Appt_Conf_Pat_CRID(Integer newValue)
    {
                dbnbDB.setFieldData( "SentTo_Appt_Conf_Pat_CRID", newValue.toString() );
    }

    public Integer getSentTo_Appt_Conf_Pat_CRID()
    {
        String           sValue = dbnbDB.getFieldData( "SentTo_Appt_Conf_Pat_CRID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setSentTo_SP_Pat(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "SentTo_SP_Pat", formatter.format( newValue ) );
    }

    public Date getSentTo_SP_Pat()
    {
        String           sValue = dbnbDB.getFieldData( "SentTo_SP_Pat" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setSentTo_SP_Pat_CRID(Integer newValue)
    {
                dbnbDB.setFieldData( "SentTo_SP_Pat_CRID", newValue.toString() );
    }

    public Integer getSentTo_SP_Pat_CRID()
    {
        String           sValue = dbnbDB.getFieldData( "SentTo_SP_Pat_CRID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setSentTo_SP_RefDr(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "SentTo_SP_RefDr", formatter.format( newValue ) );
    }

    public Date getSentTo_SP_RefDr()
    {
        String           sValue = dbnbDB.getFieldData( "SentTo_SP_RefDr" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setSentTo_SP_RefDr_CRID(Integer newValue)
    {
                dbnbDB.setFieldData( "SentTo_SP_RefDr_CRID", newValue.toString() );
    }

    public Integer getSentTo_SP_RefDr_CRID()
    {
        String           sValue = dbnbDB.getFieldData( "SentTo_SP_RefDr_CRID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setSentTo_SP_Adj(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "SentTo_SP_Adj", formatter.format( newValue ) );
    }

    public Date getSentTo_SP_Adj()
    {
        String           sValue = dbnbDB.getFieldData( "SentTo_SP_Adj" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setSentTo_SP_Adj_CRID(Integer newValue)
    {
                dbnbDB.setFieldData( "SentTo_SP_Adj_CRID", newValue.toString() );
    }

    public Integer getSentTo_SP_Adj_CRID()
    {
        String           sValue = dbnbDB.getFieldData( "SentTo_SP_Adj_CRID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setSentTo_SP_IC(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "SentTo_SP_IC", formatter.format( newValue ) );
    }

    public Date getSentTo_SP_IC()
    {
        String           sValue = dbnbDB.getFieldData( "SentTo_SP_IC" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setSentTo_SP_IC_CRID(Integer newValue)
    {
                dbnbDB.setFieldData( "SentTo_SP_IC_CRID", newValue.toString() );
    }

    public Integer getSentTo_SP_IC_CRID()
    {
        String           sValue = dbnbDB.getFieldData( "SentTo_SP_IC_CRID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setSentTo_DataProc_RefDr(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "SentTo_DataProc_RefDr", formatter.format( newValue ) );
    }

    public Date getSentTo_DataProc_RefDr()
    {
        String           sValue = dbnbDB.getFieldData( "SentTo_DataProc_RefDr" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setSentTo_DataProc_RefDr_CRID(Integer newValue)
    {
                dbnbDB.setFieldData( "SentTo_DataProc_RefDr_CRID", newValue.toString() );
    }

    public Integer getSentTo_DataProc_RefDr_CRID()
    {
        String           sValue = dbnbDB.getFieldData( "SentTo_DataProc_RefDr_CRID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setSentTo_DataProc_Adj(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "SentTo_DataProc_Adj", formatter.format( newValue ) );
    }

    public Date getSentTo_DataProc_Adj()
    {
        String           sValue = dbnbDB.getFieldData( "SentTo_DataProc_Adj" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setSentTo_DataProc_Adj_CRID(Integer newValue)
    {
                dbnbDB.setFieldData( "SentTo_DataProc_Adj_CRID", newValue.toString() );
    }

    public Integer getSentTo_DataProc_Adj_CRID()
    {
        String           sValue = dbnbDB.getFieldData( "SentTo_DataProc_Adj_CRID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setSentTo_Bill_Pay(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "SentTo_Bill_Pay", formatter.format( newValue ) );
    }

    public Date getSentTo_Bill_Pay()
    {
        String           sValue = dbnbDB.getFieldData( "SentTo_Bill_Pay" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setSentTo_Bill_Pay_CRID(Integer newValue)
    {
                dbnbDB.setFieldData( "SentTo_Bill_Pay_CRID", newValue.toString() );
    }

    public Integer getSentTo_Bill_Pay_CRID()
    {
        String           sValue = dbnbDB.getFieldData( "SentTo_Bill_Pay_CRID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setRec_Bill_Pay(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "Rec_Bill_Pay", formatter.format( newValue ) );
    }

    public Date getRec_Bill_Pay()
    {
        String           sValue = dbnbDB.getFieldData( "Rec_Bill_Pay" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setRec_Bill_Pay_CRID(Integer newValue)
    {
                dbnbDB.setFieldData( "Rec_Bill_Pay_CRID", newValue.toString() );
    }

    public Integer getRec_Bill_Pay_CRID()
    {
        String           sValue = dbnbDB.getFieldData( "Rec_Bill_Pay_CRID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setSentTo_Bill_Pro(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "SentTo_Bill_Pro", formatter.format( newValue ) );
    }

    public Date getSentTo_Bill_Pro()
    {
        String           sValue = dbnbDB.getFieldData( "SentTo_Bill_Pro" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setSentTo_Bill_Pro_CRID(Integer newValue)
    {
                dbnbDB.setFieldData( "SentTo_Bill_Pro_CRID", newValue.toString() );
    }

    public Integer getSentTo_Bill_Pro_CRID()
    {
        String           sValue = dbnbDB.getFieldData( "SentTo_Bill_Pro_CRID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setRec_Bill_Pro(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "Rec_Bill_Pro", formatter.format( newValue ) );
    }

    public Date getRec_Bill_Pro()
    {
        String           sValue = dbnbDB.getFieldData( "Rec_Bill_Pro" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setRec_Bill_Pro_CRID(Integer newValue)
    {
                dbnbDB.setFieldData( "Rec_Bill_Pro_CRID", newValue.toString() );
    }

    public Integer getRec_Bill_Pro_CRID()
    {
        String           sValue = dbnbDB.getFieldData( "Rec_Bill_Pro_CRID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setReportFileID(Integer newValue)
    {
                dbnbDB.setFieldData( "ReportFileID", newValue.toString() );
    }

    public Integer getReportFileID()
    {
        String           sValue = dbnbDB.getFieldData( "ReportFileID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setCapabilityReportFileID(Integer newValue)
    {
                dbnbDB.setFieldData( "CapabilityReportFileID", newValue.toString() );
    }

    public Integer getCapabilityReportFileID()
    {
        String           sValue = dbnbDB.getFieldData( "CapabilityReportFileID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setDICOMFileID(Integer newValue)
    {
                dbnbDB.setFieldData( "DICOMFileID", newValue.toString() );
    }

    public Integer getDICOMFileID()
    {
        String           sValue = dbnbDB.getFieldData( "DICOMFileID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setLODID(Integer newValue)
    {
                dbnbDB.setFieldData( "LODID", newValue.toString() );
    }

    public Integer getLODID()
    {
        String           sValue = dbnbDB.getFieldData( "LODID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setComments(String newValue)
    {
                dbnbDB.setFieldData( "Comments", newValue.toString() );
    }

    public String getComments()
    {
        String           sValue = dbnbDB.getFieldData( "Comments" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setAuditNotes(String newValue)
    {
                dbnbDB.setFieldData( "AuditNotes", newValue.toString() );
    }

    public String getAuditNotes()
    {
        String           sValue = dbnbDB.getFieldData( "AuditNotes" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setEncounterStatusID(Integer newValue)
    {
                dbnbDB.setFieldData( "EncounterStatusID", newValue.toString() );
    }

    public Integer getEncounterStatusID()
    {
        String           sValue = dbnbDB.getFieldData( "EncounterStatusID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setPaidToProviderCheck1Number(String newValue)
    {
                dbnbDB.setFieldData( "PaidToProviderCheck1Number", newValue.toString() );
    }

    public String getPaidToProviderCheck1Number()
    {
        String           sValue = dbnbDB.getFieldData( "PaidToProviderCheck1Number" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setPaidToProviderCheck1Amount(Double newValue)
    {
                java.text.DecimalFormat defDecFormat2 = new java.text.DecimalFormat(PLCUtils.String_defDecFormat2);
                dbnbDB.setFieldData( "PaidToProviderCheck1Amount", defDecFormat2.format(newValue) );
    }

    public Double getPaidToProviderCheck1Amount()
    {
        String           sValue = dbnbDB.getFieldData( "PaidToProviderCheck1Amount" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDoubleValue : sValue;
        return new Double( sValue  ); 
    }

    public void setPaidToProviderCheck1Date(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "PaidToProviderCheck1Date", formatter.format( newValue ) );
    }

    public Date getPaidToProviderCheck1Date()
    {
        String           sValue = dbnbDB.getFieldData( "PaidToProviderCheck1Date" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setPaidToProviderCheck2Number(String newValue)
    {
                dbnbDB.setFieldData( "PaidToProviderCheck2Number", newValue.toString() );
    }

    public String getPaidToProviderCheck2Number()
    {
        String           sValue = dbnbDB.getFieldData( "PaidToProviderCheck2Number" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setPaidToProviderCheck2Amount(Double newValue)
    {
                java.text.DecimalFormat defDecFormat2 = new java.text.DecimalFormat(PLCUtils.String_defDecFormat2);
                dbnbDB.setFieldData( "PaidToProviderCheck2Amount", defDecFormat2.format(newValue) );
    }

    public Double getPaidToProviderCheck2Amount()
    {
        String           sValue = dbnbDB.getFieldData( "PaidToProviderCheck2Amount" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDoubleValue : sValue;
        return new Double( sValue  ); 
    }

    public void setPaidToProviderCheck2Date(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "PaidToProviderCheck2Date", formatter.format( newValue ) );
    }

    public Date getPaidToProviderCheck2Date()
    {
        String           sValue = dbnbDB.getFieldData( "PaidToProviderCheck2Date" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setPaidToProviderCheck3Number(String newValue)
    {
                dbnbDB.setFieldData( "PaidToProviderCheck3Number", newValue.toString() );
    }

    public String getPaidToProviderCheck3Number()
    {
        String           sValue = dbnbDB.getFieldData( "PaidToProviderCheck3Number" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setPaidToProviderCheck3Amount(Double newValue)
    {
                java.text.DecimalFormat defDecFormat2 = new java.text.DecimalFormat(PLCUtils.String_defDecFormat2);
                dbnbDB.setFieldData( "PaidToProviderCheck3Amount", defDecFormat2.format(newValue) );
    }

    public Double getPaidToProviderCheck3Amount()
    {
        String           sValue = dbnbDB.getFieldData( "PaidToProviderCheck3Amount" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDoubleValue : sValue;
        return new Double( sValue  ); 
    }

    public void setPaidToProviderCheck3Date(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "PaidToProviderCheck3Date", formatter.format( newValue ) );
    }

    public Date getPaidToProviderCheck3Date()
    {
        String           sValue = dbnbDB.getFieldData( "PaidToProviderCheck3Date" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setSentTo_ReqRec_RefDr(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "SentTo_ReqRec_RefDr", formatter.format( newValue ) );
    }

    public Date getSentTo_ReqRec_RefDr()
    {
        String           sValue = dbnbDB.getFieldData( "SentTo_ReqRec_RefDr" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setSentTo_ReqRec_RefDr_CRID(Integer newValue)
    {
                dbnbDB.setFieldData( "SentTo_ReqRec_RefDr_CRID", newValue.toString() );
    }

    public Integer getSentTo_ReqRec_RefDr_CRID()
    {
        String           sValue = dbnbDB.getFieldData( "SentTo_ReqRec_RefDr_CRID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setSentTo_ReqRec_Adj(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "SentTo_ReqRec_Adj", formatter.format( newValue ) );
    }

    public Date getSentTo_ReqRec_Adj()
    {
        String           sValue = dbnbDB.getFieldData( "SentTo_ReqRec_Adj" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setSentTo_ReqRec_Adj_CRID(Integer newValue)
    {
                dbnbDB.setFieldData( "SentTo_ReqRec_Adj_CRID", newValue.toString() );
    }

    public Integer getSentTo_ReqRec_Adj_CRID()
    {
        String           sValue = dbnbDB.getFieldData( "SentTo_ReqRec_Adj_CRID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setTimeTrack_ReqRec(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "TimeTrack_ReqRec", formatter.format( newValue ) );
    }

    public Date getTimeTrack_ReqRec()
    {
        String           sValue = dbnbDB.getFieldData( "TimeTrack_ReqRec" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setTimeTrack_ReqRec_UserID(Integer newValue)
    {
                dbnbDB.setFieldData( "TimeTrack_ReqRec_UserID", newValue.toString() );
    }

    public Integer getTimeTrack_ReqRec_UserID()
    {
        String           sValue = dbnbDB.getFieldData( "TimeTrack_ReqRec_UserID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setTimeTrack_ReqCreated(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "TimeTrack_ReqCreated", formatter.format( newValue ) );
    }

    public Date getTimeTrack_ReqCreated()
    {
        String           sValue = dbnbDB.getFieldData( "TimeTrack_ReqCreated" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setTimeTrack_ReqCreated_UserID(Integer newValue)
    {
                dbnbDB.setFieldData( "TimeTrack_ReqCreated_UserID", newValue.toString() );
    }

    public Integer getTimeTrack_ReqCreated_UserID()
    {
        String           sValue = dbnbDB.getFieldData( "TimeTrack_ReqCreated_UserID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setTimeTrack_ReqProc(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "TimeTrack_ReqProc", formatter.format( newValue ) );
    }

    public Date getTimeTrack_ReqProc()
    {
        String           sValue = dbnbDB.getFieldData( "TimeTrack_ReqProc" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setTimeTrack_ReqProc_UserID(Integer newValue)
    {
                dbnbDB.setFieldData( "TimeTrack_ReqProc_UserID", newValue.toString() );
    }

    public Integer getTimeTrack_ReqProc_UserID()
    {
        String           sValue = dbnbDB.getFieldData( "TimeTrack_ReqProc_UserID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setTimeTrack_ReqSched(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "TimeTrack_ReqSched", formatter.format( newValue ) );
    }

    public Date getTimeTrack_ReqSched()
    {
        String           sValue = dbnbDB.getFieldData( "TimeTrack_ReqSched" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setTimeTrack_ReqSched_UserID(Integer newValue)
    {
                dbnbDB.setFieldData( "TimeTrack_ReqSched_UserID", newValue.toString() );
    }

    public Integer getTimeTrack_ReqSched_UserID()
    {
        String           sValue = dbnbDB.getFieldData( "TimeTrack_ReqSched_UserID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setTimeTrack_ReqDelivered(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "TimeTrack_ReqDelivered", formatter.format( newValue ) );
    }

    public Date getTimeTrack_ReqDelivered()
    {
        String           sValue = dbnbDB.getFieldData( "TimeTrack_ReqDelivered" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setTimeTrack_ReqDelivered_UserID(Integer newValue)
    {
                dbnbDB.setFieldData( "TimeTrack_ReqDelivered_UserID", newValue.toString() );
    }

    public Integer getTimeTrack_ReqDelivered_UserID()
    {
        String           sValue = dbnbDB.getFieldData( "TimeTrack_ReqDelivered_UserID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setTimeTrack_ReqPaidIn(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "TimeTrack_ReqPaidIn", formatter.format( newValue ) );
    }

    public Date getTimeTrack_ReqPaidIn()
    {
        String           sValue = dbnbDB.getFieldData( "TimeTrack_ReqPaidIn" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setTimeTrack_ReqPaidIn_UserID(Integer newValue)
    {
                dbnbDB.setFieldData( "TimeTrack_ReqPaidIn_UserID", newValue.toString() );
    }

    public Integer getTimeTrack_ReqPaidIn_UserID()
    {
        String           sValue = dbnbDB.getFieldData( "TimeTrack_ReqPaidIn_UserID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setTimeTrack_ReqPaidOut(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "TimeTrack_ReqPaidOut", formatter.format( newValue ) );
    }

    public Date getTimeTrack_ReqPaidOut()
    {
        String           sValue = dbnbDB.getFieldData( "TimeTrack_ReqPaidOut" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setTimeTrack_ReqPaidOut_UserID(Integer newValue)
    {
                dbnbDB.setFieldData( "TimeTrack_ReqPaidOut_UserID", newValue.toString() );
    }

    public Integer getTimeTrack_ReqPaidOut_UserID()
    {
        String           sValue = dbnbDB.getFieldData( "TimeTrack_ReqPaidOut_UserID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setProviderInvoiceID(Integer newValue)
    {
                dbnbDB.setFieldData( "ProviderInvoiceID", newValue.toString() );
    }

    public Integer getProviderInvoiceID()
    {
        String           sValue = dbnbDB.getFieldData( "ProviderInvoiceID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setTimeTrack_ReqRec_RCodeID(Integer newValue)
    {
                dbnbDB.setFieldData( "TimeTrack_ReqRec_RCodeID", newValue.toString() );
    }

    public Integer getTimeTrack_ReqRec_RCodeID()
    {
        String           sValue = dbnbDB.getFieldData( "TimeTrack_ReqRec_RCodeID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setTimeTrack_ReqCreated_RCodeID(Integer newValue)
    {
                dbnbDB.setFieldData( "TimeTrack_ReqCreated_RCodeID", newValue.toString() );
    }

    public Integer getTimeTrack_ReqCreated_RCodeID()
    {
        String           sValue = dbnbDB.getFieldData( "TimeTrack_ReqCreated_RCodeID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setTimeTrack_ReqProc_RCodeID(Integer newValue)
    {
                dbnbDB.setFieldData( "TimeTrack_ReqProc_RCodeID", newValue.toString() );
    }

    public Integer getTimeTrack_ReqProc_RCodeID()
    {
        String           sValue = dbnbDB.getFieldData( "TimeTrack_ReqProc_RCodeID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setTimeTrack_ReqSched_RCodeID(Integer newValue)
    {
                dbnbDB.setFieldData( "TimeTrack_ReqSched_RCodeID", newValue.toString() );
    }

    public Integer getTimeTrack_ReqSched_RCodeID()
    {
        String           sValue = dbnbDB.getFieldData( "TimeTrack_ReqSched_RCodeID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setTimeTrack_ReqDelivered_RCodeID(Integer newValue)
    {
                dbnbDB.setFieldData( "TimeTrack_ReqDelivered_RCodeID", newValue.toString() );
    }

    public Integer getTimeTrack_ReqDelivered_RCodeID()
    {
        String           sValue = dbnbDB.getFieldData( "TimeTrack_ReqDelivered_RCodeID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setTimeTrack_ReqPaidIn_RCodeID(Integer newValue)
    {
                dbnbDB.setFieldData( "TimeTrack_ReqPaidIn_RCodeID", newValue.toString() );
    }

    public Integer getTimeTrack_ReqPaidIn_RCodeID()
    {
        String           sValue = dbnbDB.getFieldData( "TimeTrack_ReqPaidIn_RCodeID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setTimeTrack_ReqPaidOut_RCodeID(Integer newValue)
    {
                dbnbDB.setFieldData( "TimeTrack_ReqPaidOut_RCodeID", newValue.toString() );
    }

    public Integer getTimeTrack_ReqPaidOut_RCodeID()
    {
        String           sValue = dbnbDB.getFieldData( "TimeTrack_ReqPaidOut_RCodeID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setTimeTrack_ReqApproved_RCodeID(Integer newValue)
    {
                dbnbDB.setFieldData( "TimeTrack_ReqApproved_RCodeID", newValue.toString() );
    }

    public Integer getTimeTrack_ReqApproved_RCodeID()
    {
        String           sValue = dbnbDB.getFieldData( "TimeTrack_ReqApproved_RCodeID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setTimeTrack_ReqApproved(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "TimeTrack_ReqApproved", formatter.format( newValue ) );
    }

    public Date getTimeTrack_ReqApproved()
    {
        String           sValue = dbnbDB.getFieldData( "TimeTrack_ReqApproved" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setTimeTrack_ReqApproved_UserID(Integer newValue)
    {
                dbnbDB.setFieldData( "TimeTrack_ReqApproved_UserID", newValue.toString() );
    }

    public Integer getTimeTrack_ReqApproved_UserID()
    {
        String           sValue = dbnbDB.getFieldData( "TimeTrack_ReqApproved_UserID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setSentTo_ReqRec_Adm(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "SentTo_ReqRec_Adm", formatter.format( newValue ) );
    }

    public Date getSentTo_ReqRec_Adm()
    {
        String           sValue = dbnbDB.getFieldData( "SentTo_ReqRec_Adm" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setSentTo_ReqRec_Adm_CRID(Integer newValue)
    {
                dbnbDB.setFieldData( "SentTo_ReqRec_Adm_CRID", newValue.toString() );
    }

    public Integer getSentTo_ReqRec_Adm_CRID()
    {
        String           sValue = dbnbDB.getFieldData( "SentTo_ReqRec_Adm_CRID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setSentTo_SP_Adm(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "SentTo_SP_Adm", formatter.format( newValue ) );
    }

    public Date getSentTo_SP_Adm()
    {
        String           sValue = dbnbDB.getFieldData( "SentTo_SP_Adm" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setSentTo_SP_Adm_CRID(Integer newValue)
    {
                dbnbDB.setFieldData( "SentTo_SP_Adm_CRID", newValue.toString() );
    }

    public Integer getSentTo_SP_Adm_CRID()
    {
        String           sValue = dbnbDB.getFieldData( "SentTo_SP_Adm_CRID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setSentToAdm(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "SentToAdm", formatter.format( newValue ) );
    }

    public Date getSentToAdm()
    {
        String           sValue = dbnbDB.getFieldData( "SentToAdm" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setSentToAdm_CRID(Integer newValue)
    {
                dbnbDB.setFieldData( "SentToAdm_CRID", newValue.toString() );
    }

    public Integer getSentToAdm_CRID()
    {
        String           sValue = dbnbDB.getFieldData( "SentToAdm_CRID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setTimeTrack_ReqInitialAppointment_RCodeID(Integer newValue)
    {
                dbnbDB.setFieldData( "TimeTrack_ReqInitialAppointment_RCodeID", newValue.toString() );
    }

    public Integer getTimeTrack_ReqInitialAppointment_RCodeID()
    {
        String           sValue = dbnbDB.getFieldData( "TimeTrack_ReqInitialAppointment_RCodeID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setTimeTrack_ReqInitialAppointment(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "TimeTrack_ReqInitialAppointment", formatter.format( newValue ) );
    }

    public Date getTimeTrack_ReqInitialAppointment()
    {
        String           sValue = dbnbDB.getFieldData( "TimeTrack_ReqInitialAppointment" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setTimeTrack_ReqInitialAppointment_UserID(Integer newValue)
    {
                dbnbDB.setFieldData( "TimeTrack_ReqInitialAppointment_UserID", newValue.toString() );
    }

    public Integer getTimeTrack_ReqInitialAppointment_UserID()
    {
        String           sValue = dbnbDB.getFieldData( "TimeTrack_ReqInitialAppointment_UserID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setTimeTrack_ReqRxReview_RCodeID(Integer newValue)
    {
                dbnbDB.setFieldData( "TimeTrack_ReqRxReview_RCodeID", newValue.toString() );
    }

    public Integer getTimeTrack_ReqRxReview_RCodeID()
    {
        String           sValue = dbnbDB.getFieldData( "TimeTrack_ReqRxReview_RCodeID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setTimeTrack_ReqRxReview(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "TimeTrack_ReqRxReview", formatter.format( newValue ) );
    }

    public Date getTimeTrack_ReqRxReview()
    {
        String           sValue = dbnbDB.getFieldData( "TimeTrack_ReqRxReview" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setTimeTrack_ReqRxReview_UserID(Integer newValue)
    {
                dbnbDB.setFieldData( "TimeTrack_ReqRxReview_UserID", newValue.toString() );
    }

    public Integer getTimeTrack_ReqRxReview_UserID()
    {
        String           sValue = dbnbDB.getFieldData( "TimeTrack_ReqRxReview_UserID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setTimeTrack_ReqRpReview_RCodeID(Integer newValue)
    {
                dbnbDB.setFieldData( "TimeTrack_ReqRpReview_RCodeID", newValue.toString() );
    }

    public Integer getTimeTrack_ReqRpReview_RCodeID()
    {
        String           sValue = dbnbDB.getFieldData( "TimeTrack_ReqRpReview_RCodeID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setTimeTrack_ReqRpReview(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "TimeTrack_ReqRpReview", formatter.format( newValue ) );
    }

    public Date getTimeTrack_ReqRpReview()
    {
        String           sValue = dbnbDB.getFieldData( "TimeTrack_ReqRpReview" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setTimeTrack_ReqRpReview_UserID(Integer newValue)
    {
                dbnbDB.setFieldData( "TimeTrack_ReqRpReview_UserID", newValue.toString() );
    }

    public Integer getTimeTrack_ReqRpReview_UserID()
    {
        String           sValue = dbnbDB.getFieldData( "TimeTrack_ReqRpReview_UserID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setNextActionNotes(String newValue)
    {
                dbnbDB.setFieldData( "NextActionNotes", newValue.toString() );
    }

    public String getNextActionNotes()
    {
        String           sValue = dbnbDB.getFieldData( "NextActionNotes" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setVoidLeakReasonID(Integer newValue)
    {
                dbnbDB.setFieldData( "VoidLeakReasonID", newValue.toString() );
    }

    public Integer getVoidLeakReasonID()
    {
        String           sValue = dbnbDB.getFieldData( "VoidLeakReasonID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setExportPaymentToQB(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "ExportPaymentToQB", formatter.format( newValue ) );
    }

    public Date getExportPaymentToQB()
    {
        String           sValue = dbnbDB.getFieldData( "ExportPaymentToQB" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setSentTo_SP_NCM(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "SentTo_SP_NCM", formatter.format( newValue ) );
    }

    public Date getSentTo_SP_NCM()
    {
        String           sValue = dbnbDB.getFieldData( "SentTo_SP_NCM" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setSentTo_SP_NCM_CRID(Integer newValue)
    {
                dbnbDB.setFieldData( "SentTo_SP_NCM_CRID", newValue.toString() );
    }

    public Integer getSentTo_SP_NCM_CRID()
    {
        String           sValue = dbnbDB.getFieldData( "SentTo_SP_NCM_CRID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setSentTo_RP_NCM(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "SentTo_RP_NCM", formatter.format( newValue ) );
    }

    public Date getSentTo_RP_NCM()
    {
        String           sValue = dbnbDB.getFieldData( "SentTo_RP_NCM" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setSentTo_RP_NCM_CRID(Integer newValue)
    {
                dbnbDB.setFieldData( "SentTo_RP_NCM_CRID", newValue.toString() );
    }

    public Integer getSentTo_RP_NCM_CRID()
    {
        String           sValue = dbnbDB.getFieldData( "SentTo_RP_NCM_CRID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setSentTo_ReqRec_NCM(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "SentTo_ReqRec_NCM", formatter.format( newValue ) );
    }

    public Date getSentTo_ReqRec_NCM()
    {
        String           sValue = dbnbDB.getFieldData( "SentTo_ReqRec_NCM" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setSentTo_ReqRec_NCM_CRID(Integer newValue)
    {
                dbnbDB.setFieldData( "SentTo_ReqRec_NCM_CRID", newValue.toString() );
    }

    public Integer getSentTo_ReqRec_NCM_CRID()
    {
        String           sValue = dbnbDB.getFieldData( "SentTo_ReqRec_NCM_CRID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setisSTAT(Integer newValue)
    {
                dbnbDB.setFieldData( "isSTAT", newValue.toString() );
    }

    public Integer getisSTAT()
    {
        String           sValue = dbnbDB.getFieldData( "isSTAT" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setRequiresFilms(Integer newValue)
    {
                dbnbDB.setFieldData( "RequiresFilms", newValue.toString() );
    }

    public Integer getRequiresFilms()
    {
        String           sValue = dbnbDB.getFieldData( "RequiresFilms" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setHasBeenRescheduled(Integer newValue)
    {
                dbnbDB.setFieldData( "HasBeenRescheduled", newValue.toString() );
    }

    public Integer getHasBeenRescheduled()
    {
        String           sValue = dbnbDB.getFieldData( "HasBeenRescheduled" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setSentTo_SP_Adm2(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "SentTo_SP_Adm2", formatter.format( newValue ) );
    }

    public Date getSentTo_SP_Adm2()
    {
        String           sValue = dbnbDB.getFieldData( "SentTo_SP_Adm2" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setSentTo_SP_Adm2_CRID(Integer newValue)
    {
                dbnbDB.setFieldData( "SentTo_SP_Adm2_CRID", newValue.toString() );
    }

    public Integer getSentTo_SP_Adm2_CRID()
    {
        String           sValue = dbnbDB.getFieldData( "SentTo_SP_Adm2_CRID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setSentTo_RP_Adm2(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "SentTo_RP_Adm2", formatter.format( newValue ) );
    }

    public Date getSentTo_RP_Adm2()
    {
        String           sValue = dbnbDB.getFieldData( "SentTo_RP_Adm2" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setSentTo_RP_Adm2_CRID(Integer newValue)
    {
                dbnbDB.setFieldData( "SentTo_RP_Adm2_CRID", newValue.toString() );
    }

    public Integer getSentTo_RP_Adm2_CRID()
    {
        String           sValue = dbnbDB.getFieldData( "SentTo_RP_Adm2_CRID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setSentTo_ReqRec_Adm2(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "SentTo_ReqRec_Adm2", formatter.format( newValue ) );
    }

    public Date getSentTo_ReqRec_Adm2()
    {
        String           sValue = dbnbDB.getFieldData( "SentTo_ReqRec_Adm2" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setSentTo_ReqRec_Adm2_CRID(Integer newValue)
    {
                dbnbDB.setFieldData( "SentTo_ReqRec_Adm2_CRID", newValue.toString() );
    }

    public Integer getSentTo_ReqRec_Adm2_CRID()
    {
        String           sValue = dbnbDB.getFieldData( "SentTo_ReqRec_Adm2_CRID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setSentTo_SP_Adm3(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "SentTo_SP_Adm3", formatter.format( newValue ) );
    }

    public Date getSentTo_SP_Adm3()
    {
        String           sValue = dbnbDB.getFieldData( "SentTo_SP_Adm3" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setSentTo_SP_Adm3_CRID(Integer newValue)
    {
                dbnbDB.setFieldData( "SentTo_SP_Adm3_CRID", newValue.toString() );
    }

    public Integer getSentTo_SP_Adm3_CRID()
    {
        String           sValue = dbnbDB.getFieldData( "SentTo_SP_Adm3_CRID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setSentTo_RP_Adm3(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "SentTo_RP_Adm3", formatter.format( newValue ) );
    }

    public Date getSentTo_RP_Adm3()
    {
        String           sValue = dbnbDB.getFieldData( "SentTo_RP_Adm3" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setSentTo_RP_Adm3_CRID(Integer newValue)
    {
                dbnbDB.setFieldData( "SentTo_RP_Adm3_CRID", newValue.toString() );
    }

    public Integer getSentTo_RP_Adm3_CRID()
    {
        String           sValue = dbnbDB.getFieldData( "SentTo_RP_Adm3_CRID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setSentTo_ReqRec_Adm3(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "SentTo_ReqRec_Adm3", formatter.format( newValue ) );
    }

    public Date getSentTo_ReqRec_Adm3()
    {
        String           sValue = dbnbDB.getFieldData( "SentTo_ReqRec_Adm3" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setSentTo_ReqRec_Adm3_CRID(Integer newValue)
    {
                dbnbDB.setFieldData( "SentTo_ReqRec_Adm3_CRID", newValue.toString() );
    }

    public Integer getSentTo_ReqRec_Adm3_CRID()
    {
        String           sValue = dbnbDB.getFieldData( "SentTo_ReqRec_Adm3_CRID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setSentTo_SP_Adm4(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "SentTo_SP_Adm4", formatter.format( newValue ) );
    }

    public Date getSentTo_SP_Adm4()
    {
        String           sValue = dbnbDB.getFieldData( "SentTo_SP_Adm4" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setSentTo_SP_Adm4_CRID(Integer newValue)
    {
                dbnbDB.setFieldData( "SentTo_SP_Adm4_CRID", newValue.toString() );
    }

    public Integer getSentTo_SP_Adm4_CRID()
    {
        String           sValue = dbnbDB.getFieldData( "SentTo_SP_Adm4_CRID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setSentTo_RP_Adm4(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "SentTo_RP_Adm4", formatter.format( newValue ) );
    }

    public Date getSentTo_RP_Adm4()
    {
        String           sValue = dbnbDB.getFieldData( "SentTo_RP_Adm4" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setSentTo_RP_Adm4_CRID(Integer newValue)
    {
                dbnbDB.setFieldData( "SentTo_RP_Adm4_CRID", newValue.toString() );
    }

    public Integer getSentTo_RP_Adm4_CRID()
    {
        String           sValue = dbnbDB.getFieldData( "SentTo_RP_Adm4_CRID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setSentTo_ReqRec_Adm4(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "SentTo_ReqRec_Adm4", formatter.format( newValue ) );
    }

    public Date getSentTo_ReqRec_Adm4()
    {
        String           sValue = dbnbDB.getFieldData( "SentTo_ReqRec_Adm4" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setSentTo_ReqRec_Adm4_CRID(Integer newValue)
    {
                dbnbDB.setFieldData( "SentTo_ReqRec_Adm4_CRID", newValue.toString() );
    }

    public Integer getSentTo_ReqRec_Adm4_CRID()
    {
        String           sValue = dbnbDB.getFieldData( "SentTo_ReqRec_Adm4_CRID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setRequiresAgeInjury(Integer newValue)
    {
                dbnbDB.setFieldData( "RequiresAgeInjury", newValue.toString() );
    }

    public Integer getRequiresAgeInjury()
    {
        String           sValue = dbnbDB.getFieldData( "RequiresAgeInjury" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setisCourtesy(Integer newValue)
    {
                dbnbDB.setFieldData( "isCourtesy", newValue.toString() );
    }

    public Integer getisCourtesy()
    {
        String           sValue = dbnbDB.getFieldData( "isCourtesy" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setBillingHCFA_ToPayer_FileID(Integer newValue)
    {
                dbnbDB.setFieldData( "BillingHCFA_ToPayer_FileID", newValue.toString() );
    }

    public Integer getBillingHCFA_ToPayer_FileID()
    {
        String           sValue = dbnbDB.getFieldData( "BillingHCFA_ToPayer_FileID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setBillingHCFA_FromProvider_FileID(Integer newValue)
    {
                dbnbDB.setFieldData( "BillingHCFA_FromProvider_FileID", newValue.toString() );
    }

    public Integer getBillingHCFA_FromProvider_FileID()
    {
        String           sValue = dbnbDB.getFieldData( "BillingHCFA_FromProvider_FileID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setisRetro(Integer newValue)
    {
                dbnbDB.setFieldData( "isRetro", newValue.toString() );
    }

    public Integer getisRetro()
    {
        String           sValue = dbnbDB.getFieldData( "isRetro" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setExportAPToQB(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "ExportAPToQB", formatter.format( newValue ) );
    }

    public Date getExportAPToQB()
    {
        String           sValue = dbnbDB.getFieldData( "ExportAPToQB" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setExportARToQB(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "ExportARToQB", formatter.format( newValue ) );
    }

    public Date getExportARToQB()
    {
        String           sValue = dbnbDB.getFieldData( "ExportARToQB" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setAmountBilledByProvider(Double newValue)
    {
                java.text.DecimalFormat defDecFormat2 = new java.text.DecimalFormat(PLCUtils.String_defDecFormat2);
                dbnbDB.setFieldData( "AmountBilledByProvider", defDecFormat2.format(newValue) );
    }

    public Double getAmountBilledByProvider()
    {
        String           sValue = dbnbDB.getFieldData( "AmountBilledByProvider" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDoubleValue : sValue;
        return new Double( sValue  ); 
    }

    public void setSeeNetDev_Flagged(Integer newValue)
    {
                dbnbDB.setFieldData( "SeeNetDev_Flagged", newValue.toString() );
    }

    public Integer getSeeNetDev_Flagged()
    {
        String           sValue = dbnbDB.getFieldData( "SeeNetDev_Flagged" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setSeeNetDev_ReqSentToNetDev(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "SeeNetDev_ReqSentToNetDev", formatter.format( newValue ) );
    }

    public Date getSeeNetDev_ReqSentToNetDev()
    {
        String           sValue = dbnbDB.getFieldData( "SeeNetDev_ReqSentToNetDev" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setSeeNetDev_ReqSentToNetDev_UserID(Integer newValue)
    {
                dbnbDB.setFieldData( "SeeNetDev_ReqSentToNetDev_UserID", newValue.toString() );
    }

    public Integer getSeeNetDev_ReqSentToNetDev_UserID()
    {
        String           sValue = dbnbDB.getFieldData( "SeeNetDev_ReqSentToNetDev_UserID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setSeeNetDev_Waiting(Integer newValue)
    {
                dbnbDB.setFieldData( "SeeNetDev_Waiting", newValue.toString() );
    }

    public Integer getSeeNetDev_Waiting()
    {
        String           sValue = dbnbDB.getFieldData( "SeeNetDev_Waiting" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setPatientAvailability(String newValue)
    {
                dbnbDB.setFieldData( "PatientAvailability", newValue.toString() );
    }

    public String getPatientAvailability()
    {
        String           sValue = dbnbDB.getFieldData( "PatientAvailability" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setRequiresOnlineImage(Integer newValue)
    {
                dbnbDB.setFieldData( "RequiresOnlineImage", newValue.toString() );
    }

    public Integer getRequiresOnlineImage()
    {
        String           sValue = dbnbDB.getFieldData( "RequiresOnlineImage" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setPricing_Structure(Integer newValue)
    {
                dbnbDB.setFieldData( "Pricing_Structure", newValue.toString() );
    }

    public Integer getPricing_Structure()
    {
        String           sValue = dbnbDB.getFieldData( "Pricing_Structure" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setSeeNetDev_SelectedPracticeID(Integer newValue)
    {
                dbnbDB.setFieldData( "SeeNetDev_SelectedPracticeID", newValue.toString() );
    }

    public Integer getSeeNetDev_SelectedPracticeID()
    {
        String           sValue = dbnbDB.getFieldData( "SeeNetDev_SelectedPracticeID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setActionDate_OnlineImageRequest(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "ActionDate_OnlineImageRequest", formatter.format( newValue ) );
    }

    public Date getActionDate_OnlineImageRequest()
    {
        String           sValue = dbnbDB.getFieldData( "ActionDate_OnlineImageRequest" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setActionDate_ReportRequest(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "ActionDate_ReportRequest", formatter.format( newValue ) );
    }

    public Date getActionDate_ReportRequest()
    {
        String           sValue = dbnbDB.getFieldData( "ActionDate_ReportRequest" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setVoid_UserID(Integer newValue)
    {
                dbnbDB.setFieldData( "Void_UserID", newValue.toString() );
    }

    public Integer getVoid_UserID()
    {
        String           sValue = dbnbDB.getFieldData( "Void_UserID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setRequiresHandCarryCD(Integer newValue)
    {
                dbnbDB.setFieldData( "RequiresHandCarryCD", newValue.toString() );
    }

    public Integer getRequiresHandCarryCD()
    {
        String           sValue = dbnbDB.getFieldData( "RequiresHandCarryCD" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setReportFollowUpCounter(Integer newValue)
    {
                dbnbDB.setFieldData( "ReportFollowUpCounter", newValue.toString() );
    }

    public Integer getReportFollowUpCounter()
    {
        String           sValue = dbnbDB.getFieldData( "ReportFollowUpCounter" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setProblemCategory(String newValue)
    {
                dbnbDB.setFieldData( "ProblemCategory", newValue.toString() );
    }

    public String getProblemCategory()
    {
        String           sValue = dbnbDB.getFieldData( "ProblemCategory" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setProblemDescription(String newValue)
    {
                dbnbDB.setFieldData( "ProblemDescription", newValue.toString() );
    }

    public String getProblemDescription()
    {
        String           sValue = dbnbDB.getFieldData( "ProblemDescription" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setAgeCommentLock(Integer newValue)
    {
                dbnbDB.setFieldData( "AgeCommentLock", newValue.toString() );
    }

    public Integer getAgeCommentLock()
    {
        String           sValue = dbnbDB.getFieldData( "AgeCommentLock" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setSentTo_ReqRec_Pt(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "SentTo_ReqRec_Pt", formatter.format( newValue ) );
    }

    public Date getSentTo_ReqRec_Pt()
    {
        String           sValue = dbnbDB.getFieldData( "SentTo_ReqRec_Pt" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setSentToPt(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "SentToPt", formatter.format( newValue ) );
    }

    public Date getSentToPt()
    {
        String           sValue = dbnbDB.getFieldData( "SentToPt" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setSentTo_SP_Pt(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "SentTo_SP_Pt", formatter.format( newValue ) );
    }

    public Date getSentTo_SP_Pt()
    {
        String           sValue = dbnbDB.getFieldData( "SentTo_SP_Pt" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setNIDReferralSource(String newValue)
    {
                dbnbDB.setFieldData( "NIDReferralSource", newValue.toString() );
    }

    public String getNIDReferralSource()
    {
        String           sValue = dbnbDB.getFieldData( "NIDReferralSource" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setNIDReferralCode(String newValue)
    {
                dbnbDB.setFieldData( "NIDReferralCode", newValue.toString() );
    }

    public String getNIDReferralCode()
    {
        String           sValue = dbnbDB.getFieldData( "NIDReferralCode" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setCCTransactionID(Integer newValue)
    {
                dbnbDB.setFieldData( "CCTransactionID", newValue.toString() );
    }

    public Integer getCCTransactionID()
    {
        String           sValue = dbnbDB.getFieldData( "CCTransactionID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setisVIP(Integer newValue)
    {
                dbnbDB.setFieldData( "isVIP", newValue.toString() );
    }

    public Integer getisVIP()
    {
        String           sValue = dbnbDB.getFieldData( "isVIP" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setHCFAinvoice(String newValue)
    {
                dbnbDB.setFieldData( "HCFAinvoice", newValue.toString() );
    }

    public String getHCFAinvoice()
    {
        String           sValue = dbnbDB.getFieldData( "HCFAinvoice" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

}    // End of bltNIM3_Encounter class definition
