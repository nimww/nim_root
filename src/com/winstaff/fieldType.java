package com.winstaff;
import java.awt.*;


public class fieldType extends java.lang.Object
{

	public fieldType()
	{
	}
	public String translateFieldType(String myFieldType)
	{
	    String myVal=myFieldType;
	    if (myFieldType.equalsIgnoreCase("Date"))
	    {
	        myVal = "java.util.Date";
	    }
	    return myVal;
	}

	public void setDbName(String dbName)
	{
			this.dbName = dbName;
	}

	public String getDbName()
	{
		return this.dbName;
	}

	public void setJavaName(String javaName)
	{
			this.javaName = javaName;
	}

	public String getJavaName()
	{
		return this.javaName;
	}

	public void setFieldTypeJava(String fieldTypeJava)
	{
			if (fieldTypeJava.equalsIgnoreCase("String"))
			{
			    this.setDefaultValue("");
			}
			else if (fieldTypeJava.equalsIgnoreCase("Integer"))
			{
			    this.setDefaultValue("0");
			}
			else if (fieldTypeJava.equalsIgnoreCase("Double"))
			{
			    this.setDefaultValue("0");
			}
			else if (fieldTypeJava.equalsIgnoreCase("Boolean"))
			{
			    this.setDefaultValue("0");
			}
			else if (fieldTypeJava.equalsIgnoreCase("Date"))
			{
			    this.setDefaultValue("1800-01-01");
			}

			this.fieldTypeJava = fieldTypeJava;
	}

	public String getFieldTypeJava()
	{
		return this.fieldTypeJava;
	}


	public void setFieldSize(int fieldSize)
	{
			this.fieldSize = fieldSize;
	}

	public int getFieldSize()
	{
		return this.fieldSize;
	}
	
	public String getIncludeName()
	{
	    return this.includeName;
	}
	
	public void setIncludeName(String includeName)
	{
	    this.includeName = includeName;
	}
	
	public String getDefaultValue()
	{
	    return this.defaultValue;
	}
	
	public void setDefaultValue(String myval)
	{
	    this.defaultValue = myval;
	}

	public void setRequiredField(String myval)
	{
	    if (myval.equalsIgnoreCase("R"))
	    {
	        this.requiredField = true;
	    }
	}
	
	public boolean isRequiredField()
	{
	    return this.requiredField;
	}
	
	public void setRequiredField(boolean myval)
	{
	    this.requiredField = myval;
	}



	public void setEnglishName(String englishName)
	{
			this.englishName = englishName;
	}

	public String getEnglishName()
	{
		return this.englishName;
	}

	public void setExpiredTrack(boolean expiredTrack)
	{
			this.expiredTrack = expiredTrack;
	}

	public void setExpiredTrack(String myval)
	{
	    if (myval.equalsIgnoreCase("Y"))
	    {
	        this.expiredTrack = true;
	    }
	}



	public boolean isExpiredTrack()
	{
		return this.expiredTrack;
	}

	protected boolean expiredTrack = false;
	protected String englishName = "";//this is the english name used for UI display
	protected boolean requiredField = false;
	protected int fieldSize = 20;
	protected String defaultValue = "";
	protected String includeName = "";
	protected String fieldTypeJava = "";
	protected String dbName = "";
	protected String javaName = "";

}
