package com.winstaff;

/**
 * Created with IntelliJ IDEA.
 * User: Scott
 * Date: 9/29/13
 * Time: 6:22 PM
 * To change this template use File | Settings | File Templates.
 */
public class NID_PartnerPayoutObject {


    Integer payerID;
    String payerName;
    Double payerTotal_AsSelf = 0.0;
    Double payerTotal_AsParent = 0.0;
    Integer payerCount_AsSelf = 0;
    Integer payerCount_AsParent = 0;

    public NID_PartnerPayoutObject(Integer payerID, String payerName) {
        this.payerID = payerID;
        this.payerName = payerName;
    }

    public Double getPayerTotal(){
        return this.getPayerTotal_AsParent() + this.getPayerTotal_AsSelf();
    }

    public Integer getPayerCount(){
        return this.getPayerCount_AsParent() + this.getPayerCount_AsSelf();
    }

    public Integer getPayerID() {
        return payerID;
    }

    public void setPayerID(Integer payerID) {
        this.payerID = payerID;
    }

    public String getPayerName() {
        return payerName;
    }

    public void setPayerName(String payerName) {
        this.payerName = payerName;
    }

    public Double getPayerTotal_AsSelf() {
        return payerTotal_AsSelf;
    }

    public void addToPayerTotal_AsSelf(Double payerTotal_AsSelf) {
        this.payerTotal_AsSelf += payerTotal_AsSelf;
    }

    public Double getPayerTotal_AsParent() {
        return payerTotal_AsParent;
    }

    public void addToPayerTotal_AsParent(Double payerTotal_AsParent) {
        this.payerTotal_AsParent += payerTotal_AsParent;
    }

    public Integer getPayerCount_AsSelf() {
        return payerCount_AsSelf;
    }

    public void addToPayerCount_AsSelf(Integer payerCount_AsSelf) {
        this.payerCount_AsSelf += payerCount_AsSelf;
    }

    public Integer getPayerCount_AsParent() {
        return payerCount_AsParent;
    }

    public void addToPayerCount_AsParent(Integer payerCount_AsParent) {
        this.payerCount_AsParent += payerCount_AsParent;
    }
}
