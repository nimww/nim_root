package com.winstaff;


import com.winstaff.dbtGroupSecurityItemsLI;
import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public interface InttGroupSecurityItemsLI extends dbTableInterface
{

    public void setGroupSecurityItemsID(Integer newValue);
    public Integer getGroupSecurityItemsID();
    public void setUniqueCreateDate(Date newValue);
    public Date getUniqueCreateDate();
    public void setUniqueModifyDate(Date newValue);
    public Date getUniqueModifyDate();
    public void setUniqueModifyComments(String newValue);
    public String getUniqueModifyComments();
    public void setSecurityGroupID(Integer newValue);
    public Integer getSecurityGroupID();
    public void setGroupSecurityItem(String newValue);
    public String getGroupSecurityItem();
    public void setGroupSecurityDesc(String newValue);
    public String getGroupSecurityDesc();
}    // End of bltGroupSecurityItemsLI class definition
