package com.winstaff;

/**
 * Created by IntelliJ IDEA.
 * User: ste
 * Date: 8/23/11
 * Time: 2:07 PM
 * To change this template use File | Settings | File Templates.
 */

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;


public class NIM3_CoreObject extends Object{


    public String toJSON(){
        return toJSON(new Gson());
    }

    public String toJSON(Gson gson){
        return gson.toJson(this);
    }

    public String toJSON_Python(){
        return toJSON (new GsonBuilder().setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES).create());
    }




}
