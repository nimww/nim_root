package com.winstaff;
import java.awt.*;
import java.net.*;
import java.util.*;
import java.io.*;
import java.text.*;
import javax.mail.*;
import javax.mail.internet.*;
import javax.activation.*;


public class emailType extends java.lang.Object
{

	public emailType()
	{
	}

	public void setTo(String to)
	{
			this.to = to;
	}

	public String getTo()
	{
		return this.to;
	}

	public void setFrom(String from)
	{
			this.from = from;
	}

	public String getFrom()
	{
		return this.from;
	}

	public void setSubject(String subject)
	{
			this.subject = subject;
	}

	public String getSubject()
	{
		return this.subject;
	}

	public void setBody(String body)
	{
			this.body = body;
	}

	public String getBody()
	{
		return this.body;
	}

	public void setHost(String host)
	{
			this.host = host;
	}

	public String getHost()
	{
		return this.host;
	}

	public void setMessage(String message)
	{
			this.message = message;
	}

	public String getMessage()
	{
		return this.message;
	}

	public boolean isSendMail()
	{
	    boolean myValue = false; 
	    String myMessage = "";
	        boolean debug = false;
	    try
	    {
	        // create some properties and get the default Session
	        Properties props = new Properties();
	        props.put( "mail.smtp.host", this.getHost() );
	        if( debug )
	        {
	            props.put( "mail.debug", "true" );
	        }
	        Session ses = Session.getDefaultInstance( props, null );
	        ses.setDebug( debug );
	        try 
	        {
	            Message msg = new MimeMessage( ses );
	            msg.setFrom( new InternetAddress( this.getFrom() ) );
	            InternetAddress[] address = { new InternetAddress( this.getTo() ) };
	            msg.setRecipients( Message.RecipientType.TO, address );
	            msg.setSubject( this.getSubject() );
	            msg.setSentDate( new Date() );
	            msg.setText( this.getBody() );
	            Transport.send( msg );
	            myMessage = ("SMTP Service reached and mail sent.");		
	            myValue = true;
	        } 
	        catch( Exception e )
	        {
	            myMessage = ("failed: Error: " + e.toString() );
	            myValue = false;

		        try 
		        {
		            Message msg = new MimeMessage( ses );
		            props.put( "mail.smtp.host", "mail.abac.com" );
		            msg.setFrom( new InternetAddress( "sellis@winstaff.com" ) );
		            InternetAddress[] address = { new InternetAddress( "sellis@winstaff.com" ) };
		            msg.setRecipients( Message.RecipientType.TO, address );
		            msg.setSubject( "Critical level 2" );
		            msg.setSentDate( new Date() );
		            msg.setText( "email can't send" );
		            Transport.send( msg );
		            myMessage = ("SMTP Service reached and mail sent.");		
		            myValue = true;
		        } 
		        catch( Exception e545 )
		        {
		            myMessage += ("failed: Error: " + e545.toString() );
		            myValue = false;
		        }


	        }
	        this.setMessage(myMessage);
	     }
	     catch( Exception e )
	     {
	         myMessage = ("failed: Error: " + e.toString() );
	         myValue = false;
	     }
	     return myValue;
	}

	protected String body = "ALERTBody";
	protected String subject = "ALERT";
	protected String host = ConfigurationInformation.SMTPServer;
	protected String to = "to@domain.com";
	protected String from = ConfigurationInformation.ReturnEmail;
	protected String message = "Message";

}
