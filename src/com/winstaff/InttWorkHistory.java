package com.winstaff;


import com.winstaff.dbtWorkHistory;
import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public interface InttWorkHistory extends dbTableInterface
{

    public void setWorkHistoryID(Integer newValue);
    public Integer getWorkHistoryID();
    public void setUniqueCreateDate(Date newValue);
    public Date getUniqueCreateDate();
    public void setUniqueModifyDate(Date newValue);
    public Date getUniqueModifyDate();
    public void setUniqueModifyComments(String newValue);
    public String getUniqueModifyComments();
    public void setPhysicianID(Integer newValue);
    public Integer getPhysicianID();
    public void setWorkHistoryTypeID(Integer newValue);
    public Integer getWorkHistoryTypeID();
    public void setName(String newValue);
    public String getName();
    public void setWorkDescription(String newValue);
    public String getWorkDescription();
    public void setFromDate(Date newValue);
    public Date getFromDate();
    public void setToDate(Date newValue);
    public Date getToDate();
    public void setReasonForLeaving(String newValue);
    public String getReasonForLeaving();
    public void setAddress1(String newValue);
    public String getAddress1();
    public void setAddress2(String newValue);
    public String getAddress2();
    public void setCity(String newValue);
    public String getCity();
    public void setStateID(Integer newValue);
    public Integer getStateID();
    public void setProvince(String newValue);
    public String getProvince();
    public void setZIP(String newValue);
    public String getZIP();
    public void setCountryID(Integer newValue);
    public Integer getCountryID();
    public void setPhone(String newValue);
    public String getPhone();
    public void setFax(String newValue);
    public String getFax();
    public void setContactName(String newValue);
    public String getContactName();
    public void setContactEmail(String newValue);
    public String getContactEmail();
    public void setComments(String newValue);
    public String getComments();
}    // End of bltWorkHistory class definition
