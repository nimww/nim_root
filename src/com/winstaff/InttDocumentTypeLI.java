package com.winstaff;


import com.winstaff.dbtDocumentTypeLI;
import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public interface InttDocumentTypeLI extends dbTableInterface
{

    public void setDocumentTypeID(Integer newValue);
    public Integer getDocumentTypeID();
    public void setDocumentTypeLong(String newValue);
    public String getDocumentTypeLong();
}    // End of bltDocumentTypeLI class definition
