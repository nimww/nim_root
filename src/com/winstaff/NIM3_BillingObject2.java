package com.winstaff;

/**
 * Created by IntelliJ IDEA.
 * User: ste
 * Date: 3/11/11
 * Time: 3:19 PM
 * To change this template use File | Settings | File Templates.
 */
public class NIM3_BillingObject2
{
    Double Reference_Payer_BillAmount = 0.0;
    Double Reference_Payer_AllowAmount = 0.0;
    Double Reference_Provider_BillAmount = 0.0;
    Double Reference_Provider_AllowAmount = 0.0;
    Double Reference_FeeScheduleAmount = 0.0;
    Double Reference_UCAmount = 0.0;

    Double Actual_Revenue = 0.0;     //1
    Double Actual_ARPayment = 0.0;   //2
    Double Actual_COS = 0.0;          //3
    Double Actual_APPayment = 0.0;    //4
    Double Actual_RevenueAdjustment = 0.0; //6
    Double Actual_COSAdjustment = 0.0;   //7

    public static int REVENUE_TYPE = 1;     //1
    public static int ARPAYMENT_TYPE = 2;     //2
    public static int COS_TYPE = 3;     //3
    public static int APPAYMENT_TYPE = 4;     //4
    public static int REVENUEADJUSTMENT_TYPE = 6;     //6
    public static int COSADJUSTMENT_TYPE = 7;   //7

    public Double getNetActualRevenue()
    {
        return (Actual_Revenue + Actual_RevenueAdjustment);
    }

    public Double getNetActualCOS()
    {
        return (Actual_COS + Actual_COSAdjustment);
    }

    public Double getNetActualExpected()
    {
        return getNetActualRevenue() - getNetActualCOS();
    }

    public Double getNetReferenceExpected()
    {
        return (this.Reference_Payer_AllowAmount) - (this.Reference_Provider_AllowAmount);
    }

    public Double getNetReferenceExpected_AsPercent()
    {
        return (  (   this.Reference_Payer_AllowAmount - this.Reference_Provider_AllowAmount)  / this.Reference_Payer_AllowAmount );
    }

    public Double getReferenceCostSavings_FeeSchedule()
    {
        return this.Reference_FeeScheduleAmount - (this.Reference_Payer_AllowAmount);
    }

    public Double getReferenceCostSavings_ProviderBilled()
    {
        return this.Reference_Provider_BillAmount - (this.Reference_Payer_AllowAmount);
    }

    public Double getReferenceCostSavings_UC()
    {
        return this.Reference_UCAmount - (this.Reference_Payer_AllowAmount);
    }

    public Double getARActualRemaining()
    {
        return getNetActualRevenue() - (Actual_ARPayment) ;
    }

    public Double getAPActualRemaining()
    {
        return getNetActualCOS() - (Actual_APPayment) ;
    }


    public String getStatus(String lineBreak)
    {
        String myVal = "";
        if (getNetActualRevenue()==0 && getNetActualCOS()==0)
        {
            myVal+= lineBreak + "It looks like we have not yet properly billed. No Revenue / No COS!";
        }
        else if (getNetActualRevenue()==0)
        {
            myVal+= lineBreak + "Warning: No Revenue but incurred COS!";
        }
        else if (getNetActualCOS()==0)
        {
            myVal+= lineBreak + "Warning: Incurred Revenue but no COS!";
        }
        else
        {
            //What are we going to make?
            java.text.DecimalFormat myDF = new java.text.DecimalFormat(PLCUtils.String_defDecFormat1);
            java.text.DecimalFormat myDF2 = new java.text.DecimalFormat(PLCUtils.String_defDecFormat2);
            java.text.DecimalFormat defDecFormatCurrency1 = new java.text.DecimalFormat(PLCUtils.String_defDecFormatCurrency1);
            if (getNetActualExpected()==0)
            {
                myVal+= lineBreak + "We expect to break even on this case.";
            }
            else if (getNetActualExpected()<0)
            {
                myVal+= lineBreak + "We expect to lose ["+ defDecFormatCurrency1.format(-getNetActualExpected())+"] on this case.";
            }
            else if (getNetActualExpected()>0)
            {
                myVal+= lineBreak + "We expect to make ["+ defDecFormatCurrency1.format(getNetActualExpected())+"] on this case.";
            }
            //what have we received
            myVal+= lineBreak + "We have been paid [" + defDecFormatCurrency1.format(Actual_ARPayment) +"] out of [" + defDecFormatCurrency1.format(getNetActualRevenue()) +"] with ["+ defDecFormatCurrency1.format(getARActualRemaining()) +"] still owed to us.";
            //what have we paid
            myVal+= lineBreak + "We have paid out [" + defDecFormatCurrency1.format(Actual_APPayment) +"] out of [" + defDecFormatCurrency1.format(getNetActualCOS()) +"] with ["+ defDecFormatCurrency1.format(getAPActualRemaining()) +"] that we still owe to provider.";

        }
        return myVal;
    }

    public Double getNetActualRealized()
    {
        return (Actual_ARPayment) - (Actual_APPayment);
    }

    public Double getReference_Payer_BillAmount() {
        return Reference_Payer_BillAmount;
    }

    public void setReference_Payer_BillAmount(Double reference_Payer_BillAmount) {
        Reference_Payer_BillAmount = reference_Payer_BillAmount;
    }

    public Double getReference_Payer_AllowAmount() {
        return Reference_Payer_AllowAmount;
    }

    public void setReference_Payer_AllowAmount(Double reference_Payer_AllowAmount) {
        Reference_Payer_AllowAmount = reference_Payer_AllowAmount;
    }

    public Double getReference_Provider_BillAmount() {
        return Reference_Provider_BillAmount;
    }

    public void setReference_Provider_BillAmount(Double reference_Provider_BillAmount) {
        Reference_Provider_BillAmount = reference_Provider_BillAmount;
    }

    public Double getReference_Provider_AllowAmount() {
        return Reference_Provider_AllowAmount;
    }

    public void setReference_Provider_AllowAmount(Double reference_Provider_AllowAmount) {
        Reference_Provider_AllowAmount = reference_Provider_AllowAmount;
    }

    public Double getReference_FeeScheduleAmount() {
        return Reference_FeeScheduleAmount;
    }

    public void setReference_FeeScheduleAmount(Double reference_FeeScheduleAmount) {
        Reference_FeeScheduleAmount = reference_FeeScheduleAmount;
    }

    public Double getReference_UCAmount() {
        return Reference_UCAmount;
    }

    public void setReference_UCAmount(Double reference_UCAmount) {
        Reference_UCAmount = reference_UCAmount;
    }

    public Double getActual_Revenue() {
        return Actual_Revenue;
    }

    public void setActual_Revenue(Double actual_Revenue) {
        Actual_Revenue = actual_Revenue;
    }

    public Double getActual_ARPayment() {
        return Actual_ARPayment;
    }

    public void setActual_ARPayment(Double actual_ARPayment) {
        Actual_ARPayment = actual_ARPayment;
    }

    public Double getActual_COS() {
        return Actual_COS;
    }

    public void setActual_COS(Double actual_COS) {
        Actual_COS = actual_COS;
    }

    public Double getActual_APPayment() {
        return Actual_APPayment;
    }

    public void setActual_APPayment(Double actual_APPayment) {
        Actual_APPayment = actual_APPayment;
    }

    public Double getActual_RevenueAdjustment() {
        return Actual_RevenueAdjustment;
    }

    public void setActual_RevenueAdjustment(Double actual_RevenueAdjustment) {
        Actual_RevenueAdjustment = actual_RevenueAdjustment;
    }

    public Double getActual_COSAdjustment() {
        return Actual_COSAdjustment;
    }

    public void setActual_COSAdjustment(Double actual_COSAdjustment) {
        Actual_COSAdjustment = actual_COSAdjustment;
    }
}
