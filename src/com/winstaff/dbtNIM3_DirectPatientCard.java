

package com.winstaff;

import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public class dbtNIM3_DirectPatientCard extends Object implements InttNIM3_DirectPatientCard
{

        db_NewBase    dbnbDB;

    public dbtNIM3_DirectPatientCard()
    {
        dbnbDB = new db_NewBase( "tNIM3_DirectPatientCard", "DirectPatientCardID" );

    }    // End of default constructor

    public dbtNIM3_DirectPatientCard( Integer iNewID )
    {
        dbnbDB = new db_NewBase( "tNIM3_DirectPatientCard", "DirectPatientCardID" );
        setUniqueID( iNewID );
    }    // End of Constructor knowing an ID

    public void commitData() throws SQLException
    {
            //no longer trapping/ignoring errors 2011-03-17
            dbnbDB.commitData();
    }    // End of commitData()

    public void commitDataForced() throws SQLException
    {
        dbnbDB.commitDataForced();
    }    // End of commitData()

    public Integer getUniqueID()
    {
        return dbnbDB.getUniqueID();
    }    // End of getUniqueID()

    public void setUniqueID( Integer iNewID )
    {
        dbnbDB.setUniqueID( iNewID );
    }    // End of setUniqueID()

    public void setModifyComments( String sNew )
    {
        dbnbDB.setModifyComments( sNew );
    }    // End of setUniqueID()


    public void setDirectPatientCardID(Integer newValue)
    {
                dbnbDB.setFieldData( "DirectPatientCardID", newValue.toString() );
    }

    public Integer getDirectPatientCardID()
    {
        String           sValue = dbnbDB.getFieldData( "DirectPatientCardID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setField(String fieldN, Object fieldV)
    {DebugLogger.println("setField Method not implemented yet in classNIM3_DirectPatientCard!");}

    public String getFieldType(String fieldN)
    {return null;}


    public void setUniqueCreateDate(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "UniqueCreateDate", formatter.format( newValue ) );
    }

    public Date getUniqueCreateDate()
    {
        String           sValue = dbnbDB.getFieldData( "UniqueCreateDate" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setUniqueModifyDate(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "UniqueModifyDate", formatter.format( newValue ) );
    }

    public Date getUniqueModifyDate()
    {
        String           sValue = dbnbDB.getFieldData( "UniqueModifyDate" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setDirectReferralSourceID(Integer newValue)
    {
                dbnbDB.setFieldData( "DirectReferralSourceID", newValue.toString() );
    }

    public Integer getDirectReferralSourceID()
    {
        String           sValue = dbnbDB.getFieldData( "DirectReferralSourceID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setUniqueModifyComments(String newValue)
    {
                dbnbDB.setFieldData( "UniqueModifyComments", newValue.toString() );
    }

    public String getUniqueModifyComments()
    {
        String           sValue = dbnbDB.getFieldData( "UniqueModifyComments" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setPatientFirstName(String newValue)
    {
                dbnbDB.setFieldData( "PatientFirstName", newValue.toString() );
    }

    public String getPatientFirstName()
    {
        String           sValue = dbnbDB.getFieldData( "PatientFirstName" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setPatientLastName(String newValue)
    {
                dbnbDB.setFieldData( "PatientLastName", newValue.toString() );
    }

    public String getPatientLastName()
    {
        String           sValue = dbnbDB.getFieldData( "PatientLastName" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setSignUpDate(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "SignUpDate", formatter.format( newValue ) );
    }

    public Date getSignUpDate()
    {
        String           sValue = dbnbDB.getFieldData( "SignUpDate" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setSourceWebsite(String newValue)
    {
                dbnbDB.setFieldData( "SourceWebsite", newValue.toString() );
    }

    public String getSourceWebsite()
    {
        String           sValue = dbnbDB.getFieldData( "SourceWebsite" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setPatientMRN(String newValue)
    {
                dbnbDB.setFieldData( "PatientMRN", newValue.toString() );
    }

    public String getPatientMRN()
    {
        String           sValue = dbnbDB.getFieldData( "PatientMRN" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

}    // End of bltNIM3_DirectPatientCard class definition
