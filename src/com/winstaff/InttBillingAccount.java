

package com.winstaff;

import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public interface InttBillingAccount extends dbTableInterface
{

    public void setBillingID(Integer newValue);
    public Integer getBillingID();
    public void setUniqueCreateDate(Date newValue);
    public Date getUniqueCreateDate();
    public void setUniqueModifyDate(Date newValue);
    public Date getUniqueModifyDate();
    public void setUniqueModifyComments(String newValue);
    public String getUniqueModifyComments();
    public void setBillingType(String newValue);
    public String getBillingType();
    public void setStatus(Integer newValue);
    public Integer getStatus();
    public void setWSAccountNumber(String newValue);
    public String getWSAccountNumber();
    public void setContactFirstName(String newValue);
    public String getContactFirstName();
    public void setContactLastName(String newValue);
    public String getContactLastName();
    public void setContactEmail(String newValue);
    public String getContactEmail();
    public void setContactAddress1(String newValue);
    public String getContactAddress1();
    public void setContactAddress2(String newValue);
    public String getContactAddress2();
    public void setContactCity(String newValue);
    public String getContactCity();
    public void setContactStateID(Integer newValue);
    public Integer getContactStateID();
    public void setContactProvince(String newValue);
    public String getContactProvince();
    public void setContactZIP(String newValue);
    public String getContactZIP();
    public void setContactCountryID(Integer newValue);
    public Integer getContactCountryID();
    public void setContactPhone(String newValue);
    public String getContactPhone();
    public void setAccountFullName(String newValue);
    public String getAccountFullName();
    public void setAccountNumber1(String newValue);
    public String getAccountNumber1();
    public void setAccountNumber2(String newValue);
    public String getAccountNumber2();
    public void setAccountType(String newValue);
    public String getAccountType();
    public void setAccountExpMonth(String newValue);
    public String getAccountExpMonth();
    public void setAccountExpYear(String newValue);
    public String getAccountExpYear();
    public void setAccountChargeDate(Date newValue);
    public Date getAccountChargeDate();
    public void setAccountPostDate(Date newValue);
    public Date getAccountPostDate();
    public void setAlertEmail(String newValue);
    public String getAlertEmail();
    public void setAlertDays(Integer newValue);
    public Integer getAlertDays();
    public void setBillingComments(String newValue);
    public String getBillingComments();
    public void setDebitAmount(Double newValue);
    public Double getDebitAmount();
    public void setComments(String newValue);
    public String getComments();
    public void setPromoCode(String newValue);
    public String getPromoCode();
}    // End of bltBillingAccount class definition
