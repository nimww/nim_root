package com.winstaff;


import com.winstaff.dbtProfessionalLiability;
import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public interface InttProfessionalLiability extends dbTableInterface
{

    public void setProfessionalLiabilityID(Integer newValue);
    public Integer getProfessionalLiabilityID();
    public void setUniqueCreateDate(Date newValue);
    public Date getUniqueCreateDate();
    public void setUniqueModifyDate(Date newValue);
    public Date getUniqueModifyDate();
    public void setUniqueModifyComments(String newValue);
    public String getUniqueModifyComments();
    public void setPhysicianID(Integer newValue);
    public Integer getPhysicianID();
    public void setCoverageType(Integer newValue);
    public Integer getCoverageType();
    public void setInsuranceCarrier(String newValue);
    public String getInsuranceCarrier();
    public void setPolicyHolder(String newValue);
    public String getPolicyHolder();
    public void setAgentName(String newValue);
    public String getAgentName();
    public void setAddress1(String newValue);
    public String getAddress1();
    public void setAddress2(String newValue);
    public String getAddress2();
    public void setCity(String newValue);
    public String getCity();
    public void setStateID(Integer newValue);
    public Integer getStateID();
    public void setProvince(String newValue);
    public String getProvince();
    public void setZIP(String newValue);
    public String getZIP();
    public void setCountryID(Integer newValue);
    public Integer getCountryID();
    public void setPhone(String newValue);
    public String getPhone();
    public void setFax(String newValue);
    public String getFax();
    public void setContactName(String newValue);
    public String getContactName();
    public void setContactEmail(String newValue);
    public String getContactEmail();
    public void setPolicyNumber(String newValue);
    public String getPolicyNumber();
    public void setInsuredFromDate(Date newValue);
    public Date getInsuredFromDate();
    public void setInsuredToDate(Date newValue);
    public Date getInsuredToDate();
    public void setOriginalEffectiveDate(Date newValue);
    public Date getOriginalEffectiveDate();
    public void setTerminationDate(Date newValue);
    public Date getTerminationDate();
    public void setPerClaimAmount(Double newValue);
    public Double getPerClaimAmount();
    public void setAggregateAmount(Double newValue);
    public Double getAggregateAmount();
    public void setDescOfSurcharge(String newValue);
    public String getDescOfSurcharge();
    public void setDocuLinkID(Integer newValue);
    public Integer getDocuLinkID();
    public void setComments(String newValue);
    public String getComments();
}    // End of bltProfessionalLiability class definition
