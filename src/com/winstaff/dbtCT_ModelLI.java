

package com.winstaff;

import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public class dbtCT_ModelLI extends Object implements InttCT_ModelLI
{

        db_NewBase    dbnbDB;

    public dbtCT_ModelLI()
    {
        dbnbDB = new db_NewBase( "tCT_ModelLI", "CT_ModelID" );

    }    // End of default constructor

    public dbtCT_ModelLI( Integer iNewID )
    {
        dbnbDB = new db_NewBase( "tCT_ModelLI", "CT_ModelID" );
        setUniqueID( iNewID );
    }    // End of Constructor knowing an ID

    public void commitData() throws SQLException
    {
            //no longer trapping/ignoring errors 2011-03-17
            dbnbDB.commitData();
    }    // End of commitData()

    public void commitDataForced() throws SQLException
    {
        dbnbDB.commitDataForced();
    }    // End of commitData()

    public Integer getUniqueID()
    {
        return dbnbDB.getUniqueID();
    }    // End of getUniqueID()

    public void setUniqueID( Integer iNewID )
    {
        dbnbDB.setUniqueID( iNewID );
    }    // End of setUniqueID()

    public void setModifyComments( String sNew )
    {
        dbnbDB.setModifyComments( sNew );
    }    // End of setUniqueID()


    public void setCT_ModelID(Integer newValue)
    {
                dbnbDB.setFieldData( "CT_ModelID", newValue.toString() );
    }

    public Integer getCT_ModelID()
    {
        String           sValue = dbnbDB.getFieldData( "CT_ModelID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setField(String fieldN, Object fieldV)
    {DebugLogger.println("setField Method not implemented yet in class!");}

    public String getFieldType(String fieldN)
    {return null;}


    public void setDescriptionLong(String newValue)
    {
                dbnbDB.setFieldData( "DescriptionLong", newValue.toString() );
    }

    public String getDescriptionLong()
    {
        String           sValue = dbnbDB.getFieldData( "DescriptionLong" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setManufacturer(String newValue)
    {
                dbnbDB.setFieldData( "Manufacturer", newValue.toString() );
    }

    public String getManufacturer()
    {
        String           sValue = dbnbDB.getFieldData( "Manufacturer" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setModel(String newValue)
    {
                dbnbDB.setFieldData( "Model", newValue.toString() );
    }

    public String getModel()
    {
        String           sValue = dbnbDB.getFieldData( "Model" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setSlices(Integer newValue)
    {
                dbnbDB.setFieldData( "Slices", newValue.toString() );
    }

    public Integer getSlices()
    {
        String           sValue = dbnbDB.getFieldData( "Slices" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setImages_Transferred(Integer newValue)
    {
                dbnbDB.setFieldData( "Images_Transferred", newValue.toString() );
    }

    public Integer getImages_Transferred()
    {
        String           sValue = dbnbDB.getFieldData( "Images_Transferred" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setIn_Seconds(Integer newValue)
    {
                dbnbDB.setFieldData( "In_Seconds", newValue.toString() );
    }

    public Integer getIn_Seconds()
    {
        String           sValue = dbnbDB.getFieldData( "In_Seconds" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

}    // End of bltCT_ModelLI class definition
