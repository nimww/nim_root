/*
	A basic Java class stub for a Win32 Console Application.
 */
package com.winstaff;
import com.winstaff.*;

public class EmailTransTestSingle  extends Thread
{

	public EmailTransTestSingle () 
	{

	}
	
    static public void main(String args[]) 
    {
        EmailTransTestSingle testMe = new EmailTransTestSingle();
        if (true)
        {
            try
            {
//		        int mySleep = 300;
	            System.out.println(">>> Running Email Send " + new java.util.Date());
                testMe.runMe();
    	        //System.out.println(">>> sleeping for " + Math.round(mySleep/60) + " Minutes...");
//                testMe.sleep(mySleep * 1000);
            }
            catch (Exception e)
            {
                System.out.println("Error in Thread: "+ e );
            }
        }
    }	
	

    public static void runMe()
    {
        String batchGroup = "5";
            System.out.println("==============================================================================");
        java.text.SimpleDateFormat displayDateSDF1Full = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1Full);
        System.out.println("Starting Batch Send [Batch Group:"+batchGroup+"] at " + displayDateSDF1Full.format(new java.util.Date()));
        try
        {
            emailType_V3 myETSend = new emailType_V3();
            myETSend.setTo("scottellis@gmail.com");
            myETSend.setFrom("andy@nextimagemedical.com");
            myETSend.setSubject("Testing EmailTypeV3_DirectSend2");
            myETSend.setBody("<h1>test</h1>");
            myETSend.setBody("TEST\\n\\nTest");
            //myETSend.setBodyType(myET.getEmailBodyType());
            myETSend.setBodyType(emailType_V3.HTML_TYPE);
            //System.out.println("Body Type: " + myETSend.getBodyType());
            if (myETSend.isSendMail_Direct2())
            {
                System.out.println("Successful Send");
            }
            else
            {
                System.out.println("Failed Send");
            }
            System.out.println("Done Saving");
        }
        catch(Exception e)
        {
            System.out.println(e);
        }
        
    }

	//{{DECLARE_CONTROLS
	//}}
}

