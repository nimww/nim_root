package com.winstaff;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
/*
 * ConfigurationInformation.java
 *
 * Created on April 14, 2001, 7:41 PM
 * Created on 9/28/2011, 3:44 PM
 */
import java.util.Properties;


/**
 *
 * @author  Scott Ellis
 * @version 2.00
 */
public class ConfigurationInformation 
{
	public static Properties prop = new Properties(properties.getProperties());
	 
    public static  String          sDBType                 = "ps"; //[mySQL = mySQL] [SQLServ = MS] [Access=access]
    public static  String          sJDBCDriverName         = "org.postgresql.Driver";
    
	public static  String          sURL                    = prop.getProperty("sURL");
    public static  String          sStandby_URL            = "jdbc:postgresql://173.204.89.19:5432/v3_1";
    public static  String          sUser                   = prop.getProperty("sUser");
    public static  String          sPassword               = prop.getProperty("sPassword");

    public static  String          sTomcat = "tomcat7";
    public static  String          serverName = prop.getProperty("serverName");

    public static int NIM_DB_NID_PARENT_PAYER = 951;
    
    public static  boolean		   storeDocumentsInDatabase = false;
    
    public static  boolean         bRequiredByGroup	= true;
    public static  boolean         bExpiredByGroup	= true;
    public static  boolean         bSecurityByGroup	= true;


    public static  boolean         bRunAudit            = true;
    public static  boolean         bInDebugMode            = false;
    public static  boolean         bInDebugModeShowFldSets = false;
    public static  boolean         bCreateLogBasic = true;
    public static  boolean         bCreateLogFull = true;
    public static  String          sDisplayHidden		= "[hidden]";
    public static  String          sDateTimeFormat         = "yyyy-MM-dd HH:mm:ss";
    public static  String          sDefaultDateValue       = "1800-01-01 00:00:00 AM";
    public static final String          sDefaultIntegerValue    = "0";
    public static final String          sDefaultBooleanValue    = "0";
    public static final String          sDefaultStringValue     = "";
    public static final String          sDefaultDoubleValue     = "0.0";

    public static  String          sLogFolder   = "/var/lib/" + sTomcat + "/webapps/logs/";
    public static  String          sUploadFolderDirectory   = "/mnt/cloudstorage/nimdox/";
    public static  String          sDICOMFolderDirectory   = "/var/lib/" + sTomcat + "/webapps/DICOM/";
    public static  String          sLinkedPDFDirectory   = sUploadFolderDirectory;
    public static  String          sUnLinkedPDFDirectory   = sUploadFolderDirectory;
    public static  String          serverIP		= "127.0.0.1";
    public static  String          applicationFolder	= "winstaff";
    public static  String          applicationVersion	= "NIM3";
    public static  long            sessionTimeOutInMillis = 900000;

//    public static  String	SMTPServer = "smtp.abac.com";
//    public static  String	SMTPServer = "mail.pacbell.net";
//    public static  String	SMTPServer = "pro-file.winstaff.com";
    public static  String	SMTPServer = "127.0.0.1";  //old
    public static  String	ErrorFromAddress = "server@nextimagemedical.com";
    public static  String	ErrorToAddress = "po.le@nextimagemedical.com";
    public static  String	FaxFromAddress = "server@nextimagemedical.com";
    
// Previous assignments.
    public static  String	SMTPServerHost = "smtp01.peertopeer.net";
    public static  String	SMTPServerHostPort = "587";
    public static  String	SMTPServerUser = "nextimagemedical.com";
    public static  String	SMTPServerPass = "c5G=4b";
    
//    public static  String	SMTPServerHost = "mail.global.frontbridge.com";
//    public static  String	SMTPServerHostPort = "587";
//    public static  String	SMTPServerUser = "admin@nextimagemedical.com";
//    public static  String	SMTPServerPass = "drowssaP5*";
    
    public static  String	ReturnEmail = "po.le@nextimagemedical.com";

    /** Creates new ConfigurationInformation */
    public ConfigurationInformation () 
    {
    }   // End of default constructor


    private static class properties{
    	private static Properties prop = new Properties();
    	
    	private static Properties getProperties(){
    		try{
    		prop.load(new FileInputStream("/var/lib/tomcat7/webapps/config.properties"));
    		} catch(Exception e){}
    		return prop;
    	}
    }

}   // End of ConfigurationInformation class


