

package com.winstaff;

import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public class dbtPracticeMaster extends Object implements InttPracticeMaster
{

        db_NewBase    dbnbDB;

    public dbtPracticeMaster()
    {
        dbnbDB = new db_NewBase( "tPracticeMaster", "PracticeID" );

    }    // End of default constructor

    public dbtPracticeMaster( Integer iNewID )
    {
        dbnbDB = new db_NewBase( "tPracticeMaster", "PracticeID" );
        setUniqueID( iNewID );
    }    // End of Constructor knowing an ID

    public void commitData() throws SQLException
    {
            //no longer trapping/ignoring errors 2011-03-17
            dbnbDB.commitData();
    }    // End of commitData()

    public void commitDataForced() throws SQLException
    {
        dbnbDB.commitDataForced();
    }    // End of commitData()

    public Integer getUniqueID()
    {
        return dbnbDB.getUniqueID();
    }    // End of getUniqueID()

    public void setUniqueID( Integer iNewID )
    {
        dbnbDB.setUniqueID( iNewID );
    }    // End of setUniqueID()

    public void setModifyComments( String sNew )
    {
        dbnbDB.setModifyComments( sNew );
    }    // End of setUniqueID()


    public void setPracticeID(Integer newValue)
    {
                dbnbDB.setFieldData( "PracticeID", newValue.toString() );
    }

    public Integer getPracticeID()
    {
        String           sValue = dbnbDB.getFieldData( "PracticeID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setField(String fieldN, Object fieldV)
    {DebugLogger.println("setField Method not implemented yet in classPracticeMaster!");}

    public String getFieldType(String fieldN)
    {return null;}


    public void setUniqueCreateDate(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "UniqueCreateDate", formatter.format( newValue ) );
    }

    public Date getUniqueCreateDate()
    {
        String           sValue = dbnbDB.getFieldData( "UniqueCreateDate" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setUniqueModifyDate(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "UniqueModifyDate", formatter.format( newValue ) );
    }

    public Date getUniqueModifyDate()
    {
        String           sValue = dbnbDB.getFieldData( "UniqueModifyDate" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setUniqueModifyComments(String newValue)
    {
                dbnbDB.setFieldData( "UniqueModifyComments", newValue.toString() );
    }

    public String getUniqueModifyComments()
    {
        String           sValue = dbnbDB.getFieldData( "UniqueModifyComments" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setPracticeName(String newValue)
    {
                dbnbDB.setFieldData( "PracticeName", newValue.toString() );
    }

    public String getPracticeName()
    {
        String           sValue = dbnbDB.getFieldData( "PracticeName" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setDepartmentName(String newValue)
    {
                dbnbDB.setFieldData( "DepartmentName", newValue.toString() );
    }

    public String getDepartmentName()
    {
        String           sValue = dbnbDB.getFieldData( "DepartmentName" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setTypeOfPractice(Integer newValue)
    {
                dbnbDB.setFieldData( "TypeOfPractice", newValue.toString() );
    }

    public Integer getTypeOfPractice()
    {
        String           sValue = dbnbDB.getFieldData( "TypeOfPractice" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setOfficeFederalTaxID(String newValue)
    {
                dbnbDB.setFieldData( "OfficeFederalTaxID", newValue.toString() );
    }

    public String getOfficeFederalTaxID()
    {
        String           sValue = dbnbDB.getFieldData( "OfficeFederalTaxID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setOfficeTaxIDNameAffiliation(String newValue)
    {
                dbnbDB.setFieldData( "OfficeTaxIDNameAffiliation", newValue.toString() );
    }

    public String getOfficeTaxIDNameAffiliation()
    {
        String           sValue = dbnbDB.getFieldData( "OfficeTaxIDNameAffiliation" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setOfficeAddress1(String newValue)
    {
                dbnbDB.setFieldData( "OfficeAddress1", newValue.toString() );
    }

    public String getOfficeAddress1()
    {
        String           sValue = dbnbDB.getFieldData( "OfficeAddress1" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setOfficeAddress2(String newValue)
    {
                dbnbDB.setFieldData( "OfficeAddress2", newValue.toString() );
    }

    public String getOfficeAddress2()
    {
        String           sValue = dbnbDB.getFieldData( "OfficeAddress2" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setOfficeCity(String newValue)
    {
                dbnbDB.setFieldData( "OfficeCity", newValue.toString() );
    }

    public String getOfficeCity()
    {
        String           sValue = dbnbDB.getFieldData( "OfficeCity" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setOfficeStateID(Integer newValue)
    {
                dbnbDB.setFieldData( "OfficeStateID", newValue.toString() );
    }

    public Integer getOfficeStateID()
    {
        String           sValue = dbnbDB.getFieldData( "OfficeStateID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setOfficeProvince(String newValue)
    {
                dbnbDB.setFieldData( "OfficeProvince", newValue.toString() );
    }

    public String getOfficeProvince()
    {
        String           sValue = dbnbDB.getFieldData( "OfficeProvince" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setOfficeZIP(String newValue)
    {
                dbnbDB.setFieldData( "OfficeZIP", newValue.toString() );
    }

    public String getOfficeZIP()
    {
        String           sValue = dbnbDB.getFieldData( "OfficeZIP" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setOfficeCountryID(Integer newValue)
    {
                dbnbDB.setFieldData( "OfficeCountryID", newValue.toString() );
    }

    public Integer getOfficeCountryID()
    {
        String           sValue = dbnbDB.getFieldData( "OfficeCountryID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setOfficePhone(String newValue)
    {
                dbnbDB.setFieldData( "OfficePhone", newValue.toString() );
    }

    public String getOfficePhone()
    {
        String           sValue = dbnbDB.getFieldData( "OfficePhone" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setBackOfficePhoneNo(String newValue)
    {
                dbnbDB.setFieldData( "BackOfficePhoneNo", newValue.toString() );
    }

    public String getBackOfficePhoneNo()
    {
        String           sValue = dbnbDB.getFieldData( "BackOfficePhoneNo" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setOfficeFaxNo(String newValue)
    {
                dbnbDB.setFieldData( "OfficeFaxNo", newValue.toString() );
    }

    public String getOfficeFaxNo()
    {
        String           sValue = dbnbDB.getFieldData( "OfficeFaxNo" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setOfficeEmail(String newValue)
    {
                dbnbDB.setFieldData( "OfficeEmail", newValue.toString() );
    }

    public String getOfficeEmail()
    {
        String           sValue = dbnbDB.getFieldData( "OfficeEmail" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setOfficeManagerFirstName(String newValue)
    {
                dbnbDB.setFieldData( "OfficeManagerFirstName", newValue.toString() );
    }

    public String getOfficeManagerFirstName()
    {
        String           sValue = dbnbDB.getFieldData( "OfficeManagerFirstName" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setOfficeManagerLastName(String newValue)
    {
                dbnbDB.setFieldData( "OfficeManagerLastName", newValue.toString() );
    }

    public String getOfficeManagerLastName()
    {
        String           sValue = dbnbDB.getFieldData( "OfficeManagerLastName" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setOfficeManagerPhone(String newValue)
    {
                dbnbDB.setFieldData( "OfficeManagerPhone", newValue.toString() );
    }

    public String getOfficeManagerPhone()
    {
        String           sValue = dbnbDB.getFieldData( "OfficeManagerPhone" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setOfficeManagerEmail(String newValue)
    {
                dbnbDB.setFieldData( "OfficeManagerEmail", newValue.toString() );
    }

    public String getOfficeManagerEmail()
    {
        String           sValue = dbnbDB.getFieldData( "OfficeManagerEmail" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setAnsweringService(Integer newValue)
    {
                dbnbDB.setFieldData( "AnsweringService", newValue.toString() );
    }

    public Integer getAnsweringService()
    {
        String           sValue = dbnbDB.getFieldData( "AnsweringService" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setAnsweringServicePhone(String newValue)
    {
                dbnbDB.setFieldData( "AnsweringServicePhone", newValue.toString() );
    }

    public String getAnsweringServicePhone()
    {
        String           sValue = dbnbDB.getFieldData( "AnsweringServicePhone" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setCoverage247(Integer newValue)
    {
                dbnbDB.setFieldData( "Coverage247", newValue.toString() );
    }

    public Integer getCoverage247()
    {
        String           sValue = dbnbDB.getFieldData( "Coverage247" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setMinorityEnterprise(Integer newValue)
    {
                dbnbDB.setFieldData( "MinorityEnterprise", newValue.toString() );
    }

    public Integer getMinorityEnterprise()
    {
        String           sValue = dbnbDB.getFieldData( "MinorityEnterprise" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setLanguagesSpokenInOffice(String newValue)
    {
                dbnbDB.setFieldData( "LanguagesSpokenInOffice", newValue.toString() );
    }

    public String getLanguagesSpokenInOffice()
    {
        String           sValue = dbnbDB.getFieldData( "LanguagesSpokenInOffice" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setAcceptAllNewPatients(Integer newValue)
    {
                dbnbDB.setFieldData( "AcceptAllNewPatients", newValue.toString() );
    }

    public Integer getAcceptAllNewPatients()
    {
        String           sValue = dbnbDB.getFieldData( "AcceptAllNewPatients" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setAcceptExistingPayorChange(Integer newValue)
    {
                dbnbDB.setFieldData( "AcceptExistingPayorChange", newValue.toString() );
    }

    public Integer getAcceptExistingPayorChange()
    {
        String           sValue = dbnbDB.getFieldData( "AcceptExistingPayorChange" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setAcceptNewFromReferralOnly(Integer newValue)
    {
                dbnbDB.setFieldData( "AcceptNewFromReferralOnly", newValue.toString() );
    }

    public Integer getAcceptNewFromReferralOnly()
    {
        String           sValue = dbnbDB.getFieldData( "AcceptNewFromReferralOnly" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setAcceptNewMedicare(Integer newValue)
    {
                dbnbDB.setFieldData( "AcceptNewMedicare", newValue.toString() );
    }

    public Integer getAcceptNewMedicare()
    {
        String           sValue = dbnbDB.getFieldData( "AcceptNewMedicare" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setAcceptNewMedicaid(Integer newValue)
    {
                dbnbDB.setFieldData( "AcceptNewMedicaid", newValue.toString() );
    }

    public Integer getAcceptNewMedicaid()
    {
        String           sValue = dbnbDB.getFieldData( "AcceptNewMedicaid" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setPracticeLimitationAge(String newValue)
    {
                dbnbDB.setFieldData( "PracticeLimitationAge", newValue.toString() );
    }

    public String getPracticeLimitationAge()
    {
        String           sValue = dbnbDB.getFieldData( "PracticeLimitationAge" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setPracticeLimitationSex(String newValue)
    {
                dbnbDB.setFieldData( "PracticeLimitationSex", newValue.toString() );
    }

    public String getPracticeLimitationSex()
    {
        String           sValue = dbnbDB.getFieldData( "PracticeLimitationSex" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setPracticeLimitationOther(String newValue)
    {
                dbnbDB.setFieldData( "PracticeLimitationOther", newValue.toString() );
    }

    public String getPracticeLimitationOther()
    {
        String           sValue = dbnbDB.getFieldData( "PracticeLimitationOther" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setBillingPayableTo(String newValue)
    {
                dbnbDB.setFieldData( "BillingPayableTo", newValue.toString() );
    }

    public String getBillingPayableTo()
    {
        String           sValue = dbnbDB.getFieldData( "BillingPayableTo" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setBillingFirstName(String newValue)
    {
                dbnbDB.setFieldData( "BillingFirstName", newValue.toString() );
    }

    public String getBillingFirstName()
    {
        String           sValue = dbnbDB.getFieldData( "BillingFirstName" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setBillingLastName(String newValue)
    {
                dbnbDB.setFieldData( "BillingLastName", newValue.toString() );
    }

    public String getBillingLastName()
    {
        String           sValue = dbnbDB.getFieldData( "BillingLastName" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setBillingAddress1(String newValue)
    {
                dbnbDB.setFieldData( "BillingAddress1", newValue.toString() );
    }

    public String getBillingAddress1()
    {
        String           sValue = dbnbDB.getFieldData( "BillingAddress1" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setBillingAddress2(String newValue)
    {
                dbnbDB.setFieldData( "BillingAddress2", newValue.toString() );
    }

    public String getBillingAddress2()
    {
        String           sValue = dbnbDB.getFieldData( "BillingAddress2" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setBillingCity(String newValue)
    {
                dbnbDB.setFieldData( "BillingCity", newValue.toString() );
    }

    public String getBillingCity()
    {
        String           sValue = dbnbDB.getFieldData( "BillingCity" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setBillingStateID(Integer newValue)
    {
                dbnbDB.setFieldData( "BillingStateID", newValue.toString() );
    }

    public Integer getBillingStateID()
    {
        String           sValue = dbnbDB.getFieldData( "BillingStateID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setBillingProvince(String newValue)
    {
                dbnbDB.setFieldData( "BillingProvince", newValue.toString() );
    }

    public String getBillingProvince()
    {
        String           sValue = dbnbDB.getFieldData( "BillingProvince" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setBillingZIP(String newValue)
    {
                dbnbDB.setFieldData( "BillingZIP", newValue.toString() );
    }

    public String getBillingZIP()
    {
        String           sValue = dbnbDB.getFieldData( "BillingZIP" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setBillingCountryID(Integer newValue)
    {
                dbnbDB.setFieldData( "BillingCountryID", newValue.toString() );
    }

    public Integer getBillingCountryID()
    {
        String           sValue = dbnbDB.getFieldData( "BillingCountryID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setBillingPhone(String newValue)
    {
                dbnbDB.setFieldData( "BillingPhone", newValue.toString() );
    }

    public String getBillingPhone()
    {
        String           sValue = dbnbDB.getFieldData( "BillingPhone" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setBillingFax(String newValue)
    {
                dbnbDB.setFieldData( "BillingFax", newValue.toString() );
    }

    public String getBillingFax()
    {
        String           sValue = dbnbDB.getFieldData( "BillingFax" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setCredentialingContactFirstName(String newValue)
    {
                dbnbDB.setFieldData( "CredentialingContactFirstName", newValue.toString() );
    }

    public String getCredentialingContactFirstName()
    {
        String           sValue = dbnbDB.getFieldData( "CredentialingContactFirstName" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setCredentialingContactLastName(String newValue)
    {
                dbnbDB.setFieldData( "CredentialingContactLastName", newValue.toString() );
    }

    public String getCredentialingContactLastName()
    {
        String           sValue = dbnbDB.getFieldData( "CredentialingContactLastName" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setCredentialingContactAddress1(String newValue)
    {
                dbnbDB.setFieldData( "CredentialingContactAddress1", newValue.toString() );
    }

    public String getCredentialingContactAddress1()
    {
        String           sValue = dbnbDB.getFieldData( "CredentialingContactAddress1" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setCredentialingContactAddress2(String newValue)
    {
                dbnbDB.setFieldData( "CredentialingContactAddress2", newValue.toString() );
    }

    public String getCredentialingContactAddress2()
    {
        String           sValue = dbnbDB.getFieldData( "CredentialingContactAddress2" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setCredentialingContactCity(String newValue)
    {
                dbnbDB.setFieldData( "CredentialingContactCity", newValue.toString() );
    }

    public String getCredentialingContactCity()
    {
        String           sValue = dbnbDB.getFieldData( "CredentialingContactCity" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setCredentialingContactStateID(Integer newValue)
    {
                dbnbDB.setFieldData( "CredentialingContactStateID", newValue.toString() );
    }

    public Integer getCredentialingContactStateID()
    {
        String           sValue = dbnbDB.getFieldData( "CredentialingContactStateID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setCredentialingContactProvince(String newValue)
    {
                dbnbDB.setFieldData( "CredentialingContactProvince", newValue.toString() );
    }

    public String getCredentialingContactProvince()
    {
        String           sValue = dbnbDB.getFieldData( "CredentialingContactProvince" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setCredentialingContactZIP(String newValue)
    {
                dbnbDB.setFieldData( "CredentialingContactZIP", newValue.toString() );
    }

    public String getCredentialingContactZIP()
    {
        String           sValue = dbnbDB.getFieldData( "CredentialingContactZIP" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setCredentialingContactCountryID(Integer newValue)
    {
                dbnbDB.setFieldData( "CredentialingContactCountryID", newValue.toString() );
    }

    public Integer getCredentialingContactCountryID()
    {
        String           sValue = dbnbDB.getFieldData( "CredentialingContactCountryID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setCredentialingContactPhone(String newValue)
    {
                dbnbDB.setFieldData( "CredentialingContactPhone", newValue.toString() );
    }

    public String getCredentialingContactPhone()
    {
        String           sValue = dbnbDB.getFieldData( "CredentialingContactPhone" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setCredentialingContactFax(String newValue)
    {
                dbnbDB.setFieldData( "CredentialingContactFax", newValue.toString() );
    }

    public String getCredentialingContactFax()
    {
        String           sValue = dbnbDB.getFieldData( "CredentialingContactFax" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setCredentiallingContactEmail(String newValue)
    {
                dbnbDB.setFieldData( "CredentiallingContactEmail", newValue.toString() );
    }

    public String getCredentiallingContactEmail()
    {
        String           sValue = dbnbDB.getFieldData( "CredentiallingContactEmail" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setOfficeWorkHoursOpenMonday(String newValue)
    {
                dbnbDB.setFieldData( "OfficeWorkHoursOpenMonday", newValue.toString() );
    }

    public String getOfficeWorkHoursOpenMonday()
    {
        String           sValue = dbnbDB.getFieldData( "OfficeWorkHoursOpenMonday" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setOfficeWorkHoursOpenTuesday(String newValue)
    {
                dbnbDB.setFieldData( "OfficeWorkHoursOpenTuesday", newValue.toString() );
    }

    public String getOfficeWorkHoursOpenTuesday()
    {
        String           sValue = dbnbDB.getFieldData( "OfficeWorkHoursOpenTuesday" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setOfficeWorkHoursOpenWednesday(String newValue)
    {
                dbnbDB.setFieldData( "OfficeWorkHoursOpenWednesday", newValue.toString() );
    }

    public String getOfficeWorkHoursOpenWednesday()
    {
        String           sValue = dbnbDB.getFieldData( "OfficeWorkHoursOpenWednesday" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setOfficeWorkHoursOpenThursday(String newValue)
    {
                dbnbDB.setFieldData( "OfficeWorkHoursOpenThursday", newValue.toString() );
    }

    public String getOfficeWorkHoursOpenThursday()
    {
        String           sValue = dbnbDB.getFieldData( "OfficeWorkHoursOpenThursday" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setOfficeWorkHoursOpenFriday(String newValue)
    {
                dbnbDB.setFieldData( "OfficeWorkHoursOpenFriday", newValue.toString() );
    }

    public String getOfficeWorkHoursOpenFriday()
    {
        String           sValue = dbnbDB.getFieldData( "OfficeWorkHoursOpenFriday" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setOfficeWorkHoursOpenSaturday(String newValue)
    {
                dbnbDB.setFieldData( "OfficeWorkHoursOpenSaturday", newValue.toString() );
    }

    public String getOfficeWorkHoursOpenSaturday()
    {
        String           sValue = dbnbDB.getFieldData( "OfficeWorkHoursOpenSaturday" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setOfficeWorkHoursOpenSunday(String newValue)
    {
                dbnbDB.setFieldData( "OfficeWorkHoursOpenSunday", newValue.toString() );
    }

    public String getOfficeWorkHoursOpenSunday()
    {
        String           sValue = dbnbDB.getFieldData( "OfficeWorkHoursOpenSunday" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setOfficeWorkHoursCloseMonday(String newValue)
    {
                dbnbDB.setFieldData( "OfficeWorkHoursCloseMonday", newValue.toString() );
    }

    public String getOfficeWorkHoursCloseMonday()
    {
        String           sValue = dbnbDB.getFieldData( "OfficeWorkHoursCloseMonday" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setOfficeWorkHoursCloseTuesday(String newValue)
    {
                dbnbDB.setFieldData( "OfficeWorkHoursCloseTuesday", newValue.toString() );
    }

    public String getOfficeWorkHoursCloseTuesday()
    {
        String           sValue = dbnbDB.getFieldData( "OfficeWorkHoursCloseTuesday" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setOfficeWorkHoursCloseWednesday(String newValue)
    {
                dbnbDB.setFieldData( "OfficeWorkHoursCloseWednesday", newValue.toString() );
    }

    public String getOfficeWorkHoursCloseWednesday()
    {
        String           sValue = dbnbDB.getFieldData( "OfficeWorkHoursCloseWednesday" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setOfficeWorkHoursCloseThursday(String newValue)
    {
                dbnbDB.setFieldData( "OfficeWorkHoursCloseThursday", newValue.toString() );
    }

    public String getOfficeWorkHoursCloseThursday()
    {
        String           sValue = dbnbDB.getFieldData( "OfficeWorkHoursCloseThursday" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setOfficeWorkHoursCloseFriday(String newValue)
    {
                dbnbDB.setFieldData( "OfficeWorkHoursCloseFriday", newValue.toString() );
    }

    public String getOfficeWorkHoursCloseFriday()
    {
        String           sValue = dbnbDB.getFieldData( "OfficeWorkHoursCloseFriday" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setOfficeWorkHoursCloseSaturday(String newValue)
    {
                dbnbDB.setFieldData( "OfficeWorkHoursCloseSaturday", newValue.toString() );
    }

    public String getOfficeWorkHoursCloseSaturday()
    {
        String           sValue = dbnbDB.getFieldData( "OfficeWorkHoursCloseSaturday" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setOfficeWorkHoursCloseSunday(String newValue)
    {
                dbnbDB.setFieldData( "OfficeWorkHoursCloseSunday", newValue.toString() );
    }

    public String getOfficeWorkHoursCloseSunday()
    {
        String           sValue = dbnbDB.getFieldData( "OfficeWorkHoursCloseSunday" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setOfficeAfterHoursOpenMonday(String newValue)
    {
                dbnbDB.setFieldData( "OfficeAfterHoursOpenMonday", newValue.toString() );
    }

    public String getOfficeAfterHoursOpenMonday()
    {
        String           sValue = dbnbDB.getFieldData( "OfficeAfterHoursOpenMonday" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setOfficeAfterHoursOpenTuesday(String newValue)
    {
                dbnbDB.setFieldData( "OfficeAfterHoursOpenTuesday", newValue.toString() );
    }

    public String getOfficeAfterHoursOpenTuesday()
    {
        String           sValue = dbnbDB.getFieldData( "OfficeAfterHoursOpenTuesday" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setOfficeAfterHoursOpenWednesday(String newValue)
    {
                dbnbDB.setFieldData( "OfficeAfterHoursOpenWednesday", newValue.toString() );
    }

    public String getOfficeAfterHoursOpenWednesday()
    {
        String           sValue = dbnbDB.getFieldData( "OfficeAfterHoursOpenWednesday" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setOfficeAfterHoursOpenThursday(String newValue)
    {
                dbnbDB.setFieldData( "OfficeAfterHoursOpenThursday", newValue.toString() );
    }

    public String getOfficeAfterHoursOpenThursday()
    {
        String           sValue = dbnbDB.getFieldData( "OfficeAfterHoursOpenThursday" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setOfficeAfterHoursOpenFriday(String newValue)
    {
                dbnbDB.setFieldData( "OfficeAfterHoursOpenFriday", newValue.toString() );
    }

    public String getOfficeAfterHoursOpenFriday()
    {
        String           sValue = dbnbDB.getFieldData( "OfficeAfterHoursOpenFriday" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setOfficeAfterHoursOpenSaturday(String newValue)
    {
                dbnbDB.setFieldData( "OfficeAfterHoursOpenSaturday", newValue.toString() );
    }

    public String getOfficeAfterHoursOpenSaturday()
    {
        String           sValue = dbnbDB.getFieldData( "OfficeAfterHoursOpenSaturday" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setOfficeAfterHoursOpenSunday(String newValue)
    {
                dbnbDB.setFieldData( "OfficeAfterHoursOpenSunday", newValue.toString() );
    }

    public String getOfficeAfterHoursOpenSunday()
    {
        String           sValue = dbnbDB.getFieldData( "OfficeAfterHoursOpenSunday" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setOfficeAfterHoursCloseMonday(String newValue)
    {
                dbnbDB.setFieldData( "OfficeAfterHoursCloseMonday", newValue.toString() );
    }

    public String getOfficeAfterHoursCloseMonday()
    {
        String           sValue = dbnbDB.getFieldData( "OfficeAfterHoursCloseMonday" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setOfficeAfterHoursCloseTuesday(String newValue)
    {
                dbnbDB.setFieldData( "OfficeAfterHoursCloseTuesday", newValue.toString() );
    }

    public String getOfficeAfterHoursCloseTuesday()
    {
        String           sValue = dbnbDB.getFieldData( "OfficeAfterHoursCloseTuesday" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setOfficeAfterHoursCloseWednesday(String newValue)
    {
                dbnbDB.setFieldData( "OfficeAfterHoursCloseWednesday", newValue.toString() );
    }

    public String getOfficeAfterHoursCloseWednesday()
    {
        String           sValue = dbnbDB.getFieldData( "OfficeAfterHoursCloseWednesday" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setOfficeAfterHoursCloseThursday(String newValue)
    {
                dbnbDB.setFieldData( "OfficeAfterHoursCloseThursday", newValue.toString() );
    }

    public String getOfficeAfterHoursCloseThursday()
    {
        String           sValue = dbnbDB.getFieldData( "OfficeAfterHoursCloseThursday" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setOfficeAfterHoursCloseFriday(String newValue)
    {
                dbnbDB.setFieldData( "OfficeAfterHoursCloseFriday", newValue.toString() );
    }

    public String getOfficeAfterHoursCloseFriday()
    {
        String           sValue = dbnbDB.getFieldData( "OfficeAfterHoursCloseFriday" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setOfficeAfterHoursCloseSaturday(String newValue)
    {
                dbnbDB.setFieldData( "OfficeAfterHoursCloseSaturday", newValue.toString() );
    }

    public String getOfficeAfterHoursCloseSaturday()
    {
        String           sValue = dbnbDB.getFieldData( "OfficeAfterHoursCloseSaturday" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setOfficeAfterHoursCloseSunday(String newValue)
    {
                dbnbDB.setFieldData( "OfficeAfterHoursCloseSunday", newValue.toString() );
    }

    public String getOfficeAfterHoursCloseSunday()
    {
        String           sValue = dbnbDB.getFieldData( "OfficeAfterHoursCloseSunday" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setComments(String newValue)
    {
                dbnbDB.setFieldData( "Comments", newValue.toString() );
    }

    public String getComments()
    {
        String           sValue = dbnbDB.getFieldData( "Comments" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setAnesthesiaLocal(Integer newValue)
    {
                dbnbDB.setFieldData( "AnesthesiaLocal", newValue.toString() );
    }

    public Integer getAnesthesiaLocal()
    {
        String           sValue = dbnbDB.getFieldData( "AnesthesiaLocal" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setAnesthesiaRegional(Integer newValue)
    {
                dbnbDB.setFieldData( "AnesthesiaRegional", newValue.toString() );
    }

    public Integer getAnesthesiaRegional()
    {
        String           sValue = dbnbDB.getFieldData( "AnesthesiaRegional" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setAnesthesiaConscious(Integer newValue)
    {
                dbnbDB.setFieldData( "AnesthesiaConscious", newValue.toString() );
    }

    public Integer getAnesthesiaConscious()
    {
        String           sValue = dbnbDB.getFieldData( "AnesthesiaConscious" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setAnesthesiaGeneral(Integer newValue)
    {
                dbnbDB.setFieldData( "AnesthesiaGeneral", newValue.toString() );
    }

    public Integer getAnesthesiaGeneral()
    {
        String           sValue = dbnbDB.getFieldData( "AnesthesiaGeneral" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setMinorSurgery(Integer newValue)
    {
                dbnbDB.setFieldData( "MinorSurgery", newValue.toString() );
    }

    public Integer getMinorSurgery()
    {
        String           sValue = dbnbDB.getFieldData( "MinorSurgery" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setGynecology(Integer newValue)
    {
                dbnbDB.setFieldData( "Gynecology", newValue.toString() );
    }

    public Integer getGynecology()
    {
        String           sValue = dbnbDB.getFieldData( "Gynecology" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setXRayProcedures(Integer newValue)
    {
                dbnbDB.setFieldData( "XRayProcedures", newValue.toString() );
    }

    public Integer getXRayProcedures()
    {
        String           sValue = dbnbDB.getFieldData( "XRayProcedures" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setDrawBlood(Integer newValue)
    {
                dbnbDB.setFieldData( "DrawBlood", newValue.toString() );
    }

    public Integer getDrawBlood()
    {
        String           sValue = dbnbDB.getFieldData( "DrawBlood" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setBasicLab(Integer newValue)
    {
                dbnbDB.setFieldData( "BasicLab", newValue.toString() );
    }

    public Integer getBasicLab()
    {
        String           sValue = dbnbDB.getFieldData( "BasicLab" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setEKG(Integer newValue)
    {
                dbnbDB.setFieldData( "EKG", newValue.toString() );
    }

    public Integer getEKG()
    {
        String           sValue = dbnbDB.getFieldData( "EKG" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setMinorLacerations(Integer newValue)
    {
                dbnbDB.setFieldData( "MinorLacerations", newValue.toString() );
    }

    public Integer getMinorLacerations()
    {
        String           sValue = dbnbDB.getFieldData( "MinorLacerations" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setPulmonary(Integer newValue)
    {
                dbnbDB.setFieldData( "Pulmonary", newValue.toString() );
    }

    public Integer getPulmonary()
    {
        String           sValue = dbnbDB.getFieldData( "Pulmonary" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setAllergy(Integer newValue)
    {
                dbnbDB.setFieldData( "Allergy", newValue.toString() );
    }

    public Integer getAllergy()
    {
        String           sValue = dbnbDB.getFieldData( "Allergy" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setVisualScreen(Integer newValue)
    {
                dbnbDB.setFieldData( "VisualScreen", newValue.toString() );
    }

    public Integer getVisualScreen()
    {
        String           sValue = dbnbDB.getFieldData( "VisualScreen" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setAudiometry(Integer newValue)
    {
                dbnbDB.setFieldData( "Audiometry", newValue.toString() );
    }

    public Integer getAudiometry()
    {
        String           sValue = dbnbDB.getFieldData( "Audiometry" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setSigmoidoscopy(Integer newValue)
    {
                dbnbDB.setFieldData( "Sigmoidoscopy", newValue.toString() );
    }

    public Integer getSigmoidoscopy()
    {
        String           sValue = dbnbDB.getFieldData( "Sigmoidoscopy" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setImmunizations(Integer newValue)
    {
                dbnbDB.setFieldData( "Immunizations", newValue.toString() );
    }

    public Integer getImmunizations()
    {
        String           sValue = dbnbDB.getFieldData( "Immunizations" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setAsthma(Integer newValue)
    {
                dbnbDB.setFieldData( "Asthma", newValue.toString() );
    }

    public Integer getAsthma()
    {
        String           sValue = dbnbDB.getFieldData( "Asthma" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setIVTreatment(Integer newValue)
    {
                dbnbDB.setFieldData( "IVTreatment", newValue.toString() );
    }

    public Integer getIVTreatment()
    {
        String           sValue = dbnbDB.getFieldData( "IVTreatment" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setOsteopathic(Integer newValue)
    {
                dbnbDB.setFieldData( "Osteopathic", newValue.toString() );
    }

    public Integer getOsteopathic()
    {
        String           sValue = dbnbDB.getFieldData( "Osteopathic" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setHydration(Integer newValue)
    {
                dbnbDB.setFieldData( "Hydration", newValue.toString() );
    }

    public Integer getHydration()
    {
        String           sValue = dbnbDB.getFieldData( "Hydration" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setCardiacStress(Integer newValue)
    {
                dbnbDB.setFieldData( "CardiacStress", newValue.toString() );
    }

    public Integer getCardiacStress()
    {
        String           sValue = dbnbDB.getFieldData( "CardiacStress" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setPhysicalTherapy(Integer newValue)
    {
                dbnbDB.setFieldData( "PhysicalTherapy", newValue.toString() );
    }

    public Integer getPhysicalTherapy()
    {
        String           sValue = dbnbDB.getFieldData( "PhysicalTherapy" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setMaternalHealth(Integer newValue)
    {
                dbnbDB.setFieldData( "MaternalHealth", newValue.toString() );
    }

    public Integer getMaternalHealth()
    {
        String           sValue = dbnbDB.getFieldData( "MaternalHealth" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setCHDP(Integer newValue)
    {
                dbnbDB.setFieldData( "CHDP", newValue.toString() );
    }

    public Integer getCHDP()
    {
        String           sValue = dbnbDB.getFieldData( "CHDP" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setOfficeServicesComments(String newValue)
    {
                dbnbDB.setFieldData( "OfficeServicesComments", newValue.toString() );
    }

    public String getOfficeServicesComments()
    {
        String           sValue = dbnbDB.getFieldData( "OfficeServicesComments" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setLicenseDisplayed(Integer newValue)
    {
                dbnbDB.setFieldData( "LicenseDisplayed", newValue.toString() );
    }

    public Integer getLicenseDisplayed()
    {
        String           sValue = dbnbDB.getFieldData( "LicenseDisplayed" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setCPRPresent(Integer newValue)
    {
                dbnbDB.setFieldData( "CPRPresent", newValue.toString() );
    }

    public Integer getCPRPresent()
    {
        String           sValue = dbnbDB.getFieldData( "CPRPresent" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setAmbuBagAvailable(Integer newValue)
    {
                dbnbDB.setFieldData( "AmbuBagAvailable", newValue.toString() );
    }

    public Integer getAmbuBagAvailable()
    {
        String           sValue = dbnbDB.getFieldData( "AmbuBagAvailable" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setOxygenAvailable(Integer newValue)
    {
                dbnbDB.setFieldData( "OxygenAvailable", newValue.toString() );
    }

    public Integer getOxygenAvailable()
    {
        String           sValue = dbnbDB.getFieldData( "OxygenAvailable" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setSurgicalSuite(Integer newValue)
    {
                dbnbDB.setFieldData( "SurgicalSuite", newValue.toString() );
    }

    public Integer getSurgicalSuite()
    {
        String           sValue = dbnbDB.getFieldData( "SurgicalSuite" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setCertType(String newValue)
    {
                dbnbDB.setFieldData( "CertType", newValue.toString() );
    }

    public String getCertType()
    {
        String           sValue = dbnbDB.getFieldData( "CertType" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setLabOnSite(Integer newValue)
    {
                dbnbDB.setFieldData( "LabOnSite", newValue.toString() );
    }

    public Integer getLabOnSite()
    {
        String           sValue = dbnbDB.getFieldData( "LabOnSite" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setCLIANumber(String newValue)
    {
                dbnbDB.setFieldData( "CLIANumber", newValue.toString() );
    }

    public String getCLIANumber()
    {
        String           sValue = dbnbDB.getFieldData( "CLIANumber" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setCLIAWaiver(String newValue)
    {
                dbnbDB.setFieldData( "CLIAWaiver", newValue.toString() );
    }

    public String getCLIAWaiver()
    {
        String           sValue = dbnbDB.getFieldData( "CLIAWaiver" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setADAAccessibility(Integer newValue)
    {
                dbnbDB.setFieldData( "ADAAccessibility", newValue.toString() );
    }

    public Integer getADAAccessibility()
    {
        String           sValue = dbnbDB.getFieldData( "ADAAccessibility" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setHandicapAccessBuilding(Integer newValue)
    {
                dbnbDB.setFieldData( "HandicapAccessBuilding", newValue.toString() );
    }

    public Integer getHandicapAccessBuilding()
    {
        String           sValue = dbnbDB.getFieldData( "HandicapAccessBuilding" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setHandicapAccessRampRail(Integer newValue)
    {
                dbnbDB.setFieldData( "HandicapAccessRampRail", newValue.toString() );
    }

    public Integer getHandicapAccessRampRail()
    {
        String           sValue = dbnbDB.getFieldData( "HandicapAccessRampRail" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setHandicapAccessParking(Integer newValue)
    {
                dbnbDB.setFieldData( "HandicapAccessParking", newValue.toString() );
    }

    public Integer getHandicapAccessParking()
    {
        String           sValue = dbnbDB.getFieldData( "HandicapAccessParking" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setHandicapAccessWheel(Integer newValue)
    {
                dbnbDB.setFieldData( "HandicapAccessWheel", newValue.toString() );
    }

    public Integer getHandicapAccessWheel()
    {
        String           sValue = dbnbDB.getFieldData( "HandicapAccessWheel" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setHandicapAccessRail(Integer newValue)
    {
                dbnbDB.setFieldData( "HandicapAccessRail", newValue.toString() );
    }

    public Integer getHandicapAccessRail()
    {
        String           sValue = dbnbDB.getFieldData( "HandicapAccessRail" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setHandicapAccessElevator(Integer newValue)
    {
                dbnbDB.setFieldData( "HandicapAccessElevator", newValue.toString() );
    }

    public Integer getHandicapAccessElevator()
    {
        String           sValue = dbnbDB.getFieldData( "HandicapAccessElevator" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setHandicapAccessBraille(Integer newValue)
    {
                dbnbDB.setFieldData( "HandicapAccessBraille", newValue.toString() );
    }

    public Integer getHandicapAccessBraille()
    {
        String           sValue = dbnbDB.getFieldData( "HandicapAccessBraille" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setHandicapFountainPhone(Integer newValue)
    {
                dbnbDB.setFieldData( "HandicapFountainPhone", newValue.toString() );
    }

    public Integer getHandicapFountainPhone()
    {
        String           sValue = dbnbDB.getFieldData( "HandicapFountainPhone" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setNearPublicTransportation(Integer newValue)
    {
                dbnbDB.setFieldData( "NearPublicTransportation", newValue.toString() );
    }

    public Integer getNearPublicTransportation()
    {
        String           sValue = dbnbDB.getFieldData( "NearPublicTransportation" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setDisabledTTY(Integer newValue)
    {
                dbnbDB.setFieldData( "DisabledTTY", newValue.toString() );
    }

    public Integer getDisabledTTY()
    {
        String           sValue = dbnbDB.getFieldData( "DisabledTTY" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setDisabledASL(Integer newValue)
    {
                dbnbDB.setFieldData( "DisabledASL", newValue.toString() );
    }

    public Integer getDisabledASL()
    {
        String           sValue = dbnbDB.getFieldData( "DisabledASL" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setChildcareServices(Integer newValue)
    {
                dbnbDB.setFieldData( "ChildcareServices", newValue.toString() );
    }

    public Integer getChildcareServices()
    {
        String           sValue = dbnbDB.getFieldData( "ChildcareServices" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setSafetyFireExtinguisher(Integer newValue)
    {
                dbnbDB.setFieldData( "SafetyFireExtinguisher", newValue.toString() );
    }

    public Integer getSafetyFireExtinguisher()
    {
        String           sValue = dbnbDB.getFieldData( "SafetyFireExtinguisher" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setSafetyExtinguisherInspected(Integer newValue)
    {
                dbnbDB.setFieldData( "SafetyExtinguisherInspected", newValue.toString() );
    }

    public Integer getSafetyExtinguisherInspected()
    {
        String           sValue = dbnbDB.getFieldData( "SafetyExtinguisherInspected" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setSafetySmokeDetectors(Integer newValue)
    {
                dbnbDB.setFieldData( "SafetySmokeDetectors", newValue.toString() );
    }

    public Integer getSafetySmokeDetectors()
    {
        String           sValue = dbnbDB.getFieldData( "SafetySmokeDetectors" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setSafetyCorridorsClear(Integer newValue)
    {
                dbnbDB.setFieldData( "SafetyCorridorsClear", newValue.toString() );
    }

    public Integer getSafetyCorridorsClear()
    {
        String           sValue = dbnbDB.getFieldData( "SafetyCorridorsClear" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setInfectionControlHandwashing(Integer newValue)
    {
                dbnbDB.setFieldData( "InfectionControlHandwashing", newValue.toString() );
    }

    public Integer getInfectionControlHandwashing()
    {
        String           sValue = dbnbDB.getFieldData( "InfectionControlHandwashing" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setInfectionControlGloves(Integer newValue)
    {
                dbnbDB.setFieldData( "InfectionControlGloves", newValue.toString() );
    }

    public Integer getInfectionControlGloves()
    {
        String           sValue = dbnbDB.getFieldData( "InfectionControlGloves" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setInfectionControlBloodCleaned(Integer newValue)
    {
                dbnbDB.setFieldData( "InfectionControlBloodCleaned", newValue.toString() );
    }

    public Integer getInfectionControlBloodCleaned()
    {
        String           sValue = dbnbDB.getFieldData( "InfectionControlBloodCleaned" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setFacilityComments(String newValue)
    {
                dbnbDB.setFieldData( "FacilityComments", newValue.toString() );
    }

    public String getFacilityComments()
    {
        String           sValue = dbnbDB.getFieldData( "FacilityComments" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setMedicalRecordsIndividual(Integer newValue)
    {
                dbnbDB.setFieldData( "MedicalRecordsIndividual", newValue.toString() );
    }

    public Integer getMedicalRecordsIndividual()
    {
        String           sValue = dbnbDB.getFieldData( "MedicalRecordsIndividual" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setMedicalRecordsInaccessible(Integer newValue)
    {
                dbnbDB.setFieldData( "MedicalRecordsInaccessible", newValue.toString() );
    }

    public Integer getMedicalRecordsInaccessible()
    {
        String           sValue = dbnbDB.getFieldData( "MedicalRecordsInaccessible" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setMedicalRecordsUniform(Integer newValue)
    {
                dbnbDB.setFieldData( "MedicalRecordsUniform", newValue.toString() );
    }

    public Integer getMedicalRecordsUniform()
    {
        String           sValue = dbnbDB.getFieldData( "MedicalRecordsUniform" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setMedicalRecordsPatientsNamePerPage(Integer newValue)
    {
                dbnbDB.setFieldData( "MedicalRecordsPatientsNamePerPage", newValue.toString() );
    }

    public Integer getMedicalRecordsPatientsNamePerPage()
    {
        String           sValue = dbnbDB.getFieldData( "MedicalRecordsPatientsNamePerPage" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setMedicalRecordsSignatureAuthor(Integer newValue)
    {
                dbnbDB.setFieldData( "MedicalRecordsSignatureAuthor", newValue.toString() );
    }

    public Integer getMedicalRecordsSignatureAuthor()
    {
        String           sValue = dbnbDB.getFieldData( "MedicalRecordsSignatureAuthor" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setMedicalRecordsEntryDate(Integer newValue)
    {
                dbnbDB.setFieldData( "MedicalRecordsEntryDate", newValue.toString() );
    }

    public Integer getMedicalRecordsEntryDate()
    {
        String           sValue = dbnbDB.getFieldData( "MedicalRecordsEntryDate" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setMedicalRecordsHistory(Integer newValue)
    {
                dbnbDB.setFieldData( "MedicalRecordsHistory", newValue.toString() );
    }

    public Integer getMedicalRecordsHistory()
    {
        String           sValue = dbnbDB.getFieldData( "MedicalRecordsHistory" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setMedicalRecordsAdverse(Integer newValue)
    {
                dbnbDB.setFieldData( "MedicalRecordsAdverse", newValue.toString() );
    }

    public Integer getMedicalRecordsAdverse()
    {
        String           sValue = dbnbDB.getFieldData( "MedicalRecordsAdverse" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setMedicalRecordsComments(String newValue)
    {
                dbnbDB.setFieldData( "MedicalRecordsComments", newValue.toString() );
    }

    public String getMedicalRecordsComments()
    {
        String           sValue = dbnbDB.getFieldData( "MedicalRecordsComments" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setLastModifiedDate(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "LastModifiedDate", formatter.format( newValue ) );
    }

    public Date getLastModifiedDate()
    {
        String           sValue = dbnbDB.getFieldData( "LastModifiedDate" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setAllowPhysEdit(Integer newValue)
    {
                dbnbDB.setFieldData( "AllowPhysEdit", newValue.toString() );
    }

    public Integer getAllowPhysEdit()
    {
        String           sValue = dbnbDB.getFieldData( "AllowPhysEdit" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setDiagnosticUltraSound(Integer newValue)
    {
                dbnbDB.setFieldData( "DiagnosticUltraSound", newValue.toString() );
    }

    public Integer getDiagnosticUltraSound()
    {
        String           sValue = dbnbDB.getFieldData( "DiagnosticUltraSound" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setEndoscopy(Integer newValue)
    {
                dbnbDB.setFieldData( "Endoscopy", newValue.toString() );
    }

    public Integer getEndoscopy()
    {
        String           sValue = dbnbDB.getFieldData( "Endoscopy" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setAcceptReferralsComments(String newValue)
    {
                dbnbDB.setFieldData( "AcceptReferralsComments", newValue.toString() );
    }

    public String getAcceptReferralsComments()
    {
        String           sValue = dbnbDB.getFieldData( "AcceptReferralsComments" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setPatientBringComments(String newValue)
    {
                dbnbDB.setFieldData( "PatientBringComments", newValue.toString() );
    }

    public String getPatientBringComments()
    {
        String           sValue = dbnbDB.getFieldData( "PatientBringComments" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setPatientReferralQuestions(String newValue)
    {
                dbnbDB.setFieldData( "PatientReferralQuestions", newValue.toString() );
    }

    public String getPatientReferralQuestions()
    {
        String           sValue = dbnbDB.getFieldData( "PatientReferralQuestions" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setCLIAExpirationDate(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "CLIAExpirationDate", formatter.format( newValue ) );
    }

    public Date getCLIAExpirationDate()
    {
        String           sValue = dbnbDB.getFieldData( "CLIAExpirationDate" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setAuditStatus(String newValue)
    {
                dbnbDB.setFieldData( "AuditStatus", newValue.toString() );
    }

    public String getAuditStatus()
    {
        String           sValue = dbnbDB.getFieldData( "AuditStatus" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setAuditDate(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "AuditDate", formatter.format( newValue ) );
    }

    public Date getAuditDate()
    {
        String           sValue = dbnbDB.getFieldData( "AuditDate" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setAuditScore(Integer newValue)
    {
                dbnbDB.setFieldData( "AuditScore", newValue.toString() );
    }

    public Integer getAuditScore()
    {
        String           sValue = dbnbDB.getFieldData( "AuditScore" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setContractingStatusID(Integer newValue)
    {
                dbnbDB.setFieldData( "ContractingStatusID", newValue.toString() );
    }

    public Integer getContractingStatusID()
    {
        String           sValue = dbnbDB.getFieldData( "ContractingStatusID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setOwnerName(String newValue)
    {
                dbnbDB.setFieldData( "OwnerName", newValue.toString() );
    }

    public String getOwnerName()
    {
        String           sValue = dbnbDB.getFieldData( "OwnerName" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setPrice_Mod_MRI_W(Double newValue)
    {
                java.text.DecimalFormat defDecFormat2 = new java.text.DecimalFormat(PLCUtils.String_defDecFormat2);
                dbnbDB.setFieldData( "Price_Mod_MRI_W", defDecFormat2.format(newValue) );
    }

    public Double getPrice_Mod_MRI_W()
    {
        String           sValue = dbnbDB.getFieldData( "Price_Mod_MRI_W" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDoubleValue : sValue;
        return new Double( sValue  ); 
    }

    public void setPrice_Mod_MRI_WO(Double newValue)
    {
                java.text.DecimalFormat defDecFormat2 = new java.text.DecimalFormat(PLCUtils.String_defDecFormat2);
                dbnbDB.setFieldData( "Price_Mod_MRI_WO", defDecFormat2.format(newValue) );
    }

    public Double getPrice_Mod_MRI_WO()
    {
        String           sValue = dbnbDB.getFieldData( "Price_Mod_MRI_WO" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDoubleValue : sValue;
        return new Double( sValue  ); 
    }

    public void setPrice_Mod_MRI_WWO(Double newValue)
    {
                java.text.DecimalFormat defDecFormat2 = new java.text.DecimalFormat(PLCUtils.String_defDecFormat2);
                dbnbDB.setFieldData( "Price_Mod_MRI_WWO", defDecFormat2.format(newValue) );
    }

    public Double getPrice_Mod_MRI_WWO()
    {
        String           sValue = dbnbDB.getFieldData( "Price_Mod_MRI_WWO" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDoubleValue : sValue;
        return new Double( sValue  ); 
    }

    public void setPrice_Mod_CT_W(Double newValue)
    {
                java.text.DecimalFormat defDecFormat2 = new java.text.DecimalFormat(PLCUtils.String_defDecFormat2);
                dbnbDB.setFieldData( "Price_Mod_CT_W", defDecFormat2.format(newValue) );
    }

    public Double getPrice_Mod_CT_W()
    {
        String           sValue = dbnbDB.getFieldData( "Price_Mod_CT_W" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDoubleValue : sValue;
        return new Double( sValue  ); 
    }

    public void setPrice_Mod_CT_WO(Double newValue)
    {
                java.text.DecimalFormat defDecFormat2 = new java.text.DecimalFormat(PLCUtils.String_defDecFormat2);
                dbnbDB.setFieldData( "Price_Mod_CT_WO", defDecFormat2.format(newValue) );
    }

    public Double getPrice_Mod_CT_WO()
    {
        String           sValue = dbnbDB.getFieldData( "Price_Mod_CT_WO" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDoubleValue : sValue;
        return new Double( sValue  ); 
    }

    public void setPrice_Mod_CT_WWO(Double newValue)
    {
                java.text.DecimalFormat defDecFormat2 = new java.text.DecimalFormat(PLCUtils.String_defDecFormat2);
                dbnbDB.setFieldData( "Price_Mod_CT_WWO", defDecFormat2.format(newValue) );
    }

    public Double getPrice_Mod_CT_WWO()
    {
        String           sValue = dbnbDB.getFieldData( "Price_Mod_CT_WWO" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDoubleValue : sValue;
        return new Double( sValue  ); 
    }

    public void setWebURL(String newValue)
    {
                dbnbDB.setFieldData( "WebURL", newValue.toString() );
    }

    public String getWebURL()
    {
        String           sValue = dbnbDB.getFieldData( "WebURL" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setNPINumber(String newValue)
    {
                dbnbDB.setFieldData( "NPINumber", newValue.toString() );
    }

    public String getNPINumber()
    {
        String           sValue = dbnbDB.getFieldData( "NPINumber" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setStateLicenseNumber(String newValue)
    {
                dbnbDB.setFieldData( "StateLicenseNumber", newValue.toString() );
    }

    public String getStateLicenseNumber()
    {
        String           sValue = dbnbDB.getFieldData( "StateLicenseNumber" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setMedicareLicenseNumber(String newValue)
    {
                dbnbDB.setFieldData( "MedicareLicenseNumber", newValue.toString() );
    }

    public String getMedicareLicenseNumber()
    {
        String           sValue = dbnbDB.getFieldData( "MedicareLicenseNumber" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setWalkinMRICT(Integer newValue)
    {
                dbnbDB.setFieldData( "WalkinMRICT", newValue.toString() );
    }

    public Integer getWalkinMRICT()
    {
        String           sValue = dbnbDB.getFieldData( "WalkinMRICT" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setDiagnosticTechOnly(Integer newValue)
    {
                dbnbDB.setFieldData( "DiagnosticTechOnly", newValue.toString() );
    }

    public Integer getDiagnosticTechOnly()
    {
        String           sValue = dbnbDB.getFieldData( "DiagnosticTechOnly" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setDICOMForward(Integer newValue)
    {
                dbnbDB.setFieldData( "DICOMForward", newValue.toString() );
    }

    public Integer getDICOMForward()
    {
        String           sValue = dbnbDB.getFieldData( "DICOMForward" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setDiagnosticAfterHours(Integer newValue)
    {
                dbnbDB.setFieldData( "DiagnosticAfterHours", newValue.toString() );
    }

    public Integer getDiagnosticAfterHours()
    {
        String           sValue = dbnbDB.getFieldData( "DiagnosticAfterHours" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setMalPracticePolicyCarrier(String newValue)
    {
                dbnbDB.setFieldData( "MalPracticePolicyCarrier", newValue.toString() );
    }

    public String getMalPracticePolicyCarrier()
    {
        String           sValue = dbnbDB.getFieldData( "MalPracticePolicyCarrier" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setMalPracticePolicyNumber(String newValue)
    {
                dbnbDB.setFieldData( "MalPracticePolicyNumber", newValue.toString() );
    }

    public String getMalPracticePolicyNumber()
    {
        String           sValue = dbnbDB.getFieldData( "MalPracticePolicyNumber" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setMalPracticePolicyExpDate(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "MalPracticePolicyExpDate", formatter.format( newValue ) );
    }

    public Date getMalPracticePolicyExpDate()
    {
        String           sValue = dbnbDB.getFieldData( "MalPracticePolicyExpDate" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setBillingAddressName(String newValue)
    {
                dbnbDB.setFieldData( "BillingAddressName", newValue.toString() );
    }

    public String getBillingAddressName()
    {
        String           sValue = dbnbDB.getFieldData( "BillingAddressName" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setBillingEmail(String newValue)
    {
                dbnbDB.setFieldData( "BillingEmail", newValue.toString() );
    }

    public String getBillingEmail()
    {
        String           sValue = dbnbDB.getFieldData( "BillingEmail" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setCredentialingContactAddressName(String newValue)
    {
                dbnbDB.setFieldData( "CredentialingContactAddressName", newValue.toString() );
    }

    public String getCredentialingContactAddressName()
    {
        String           sValue = dbnbDB.getFieldData( "CredentialingContactAddressName" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setRate_CurbAppeal(Integer newValue)
    {
                dbnbDB.setFieldData( "Rate_CurbAppeal", newValue.toString() );
    }

    public Integer getRate_CurbAppeal()
    {
        String           sValue = dbnbDB.getFieldData( "Rate_CurbAppeal" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setRate_AccessSafety(Integer newValue)
    {
                dbnbDB.setFieldData( "Rate_AccessSafety", newValue.toString() );
    }

    public Integer getRate_AccessSafety()
    {
        String           sValue = dbnbDB.getFieldData( "Rate_AccessSafety" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setRate_Cleanliness(Integer newValue)
    {
                dbnbDB.setFieldData( "Rate_Cleanliness", newValue.toString() );
    }

    public Integer getRate_Cleanliness()
    {
        String           sValue = dbnbDB.getFieldData( "Rate_Cleanliness" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setRate_Appropriateness(Integer newValue)
    {
                dbnbDB.setFieldData( "Rate_Appropriateness", newValue.toString() );
    }

    public Integer getRate_Appropriateness()
    {
        String           sValue = dbnbDB.getFieldData( "Rate_Appropriateness" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setRate_StaffFriendly(Integer newValue)
    {
                dbnbDB.setFieldData( "Rate_StaffFriendly", newValue.toString() );
    }

    public Integer getRate_StaffFriendly()
    {
        String           sValue = dbnbDB.getFieldData( "Rate_StaffFriendly" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setRate_AttentiveTimely(Integer newValue)
    {
                dbnbDB.setFieldData( "Rate_AttentiveTimely", newValue.toString() );
    }

    public Integer getRate_AttentiveTimely()
    {
        String           sValue = dbnbDB.getFieldData( "Rate_AttentiveTimely" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setFeeScheduleRefID(Integer newValue)
    {
                dbnbDB.setFieldData( "FeeScheduleRefID", newValue.toString() );
    }

    public Integer getFeeScheduleRefID()
    {
        String           sValue = dbnbDB.getFieldData( "FeeScheduleRefID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setFeePercentage(Double newValue)
    {
                java.text.DecimalFormat defDecFormat2 = new java.text.DecimalFormat(PLCUtils.String_defDecFormat2);
                dbnbDB.setFieldData( "FeePercentage", defDecFormat2.format(newValue) );
    }

    public Double getFeePercentage()
    {
        String           sValue = dbnbDB.getFieldData( "FeePercentage" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDoubleValue : sValue;
        return new Double( sValue  ); 
    }

    public void setInitialContractDate(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "InitialContractDate", formatter.format( newValue ) );
    }

    public Date getInitialContractDate()
    {
        String           sValue = dbnbDB.getFieldData( "InitialContractDate" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setContractDate(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "ContractDate", formatter.format( newValue ) );
    }

    public Date getContractDate()
    {
        String           sValue = dbnbDB.getFieldData( "ContractDate" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setPACSSystemVendor(String newValue)
    {
                dbnbDB.setFieldData( "PACSSystemVendor", newValue.toString() );
    }

    public String getPACSSystemVendor()
    {
        String           sValue = dbnbDB.getFieldData( "PACSSystemVendor" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setRISSystemVendor(String newValue)
    {
                dbnbDB.setFieldData( "RISSystemVendor", newValue.toString() );
    }

    public String getRISSystemVendor()
    {
        String           sValue = dbnbDB.getFieldData( "RISSystemVendor" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setIsBlueStar(Integer newValue)
    {
                dbnbDB.setFieldData( "IsBlueStar", newValue.toString() );
    }

    public Integer getIsBlueStar()
    {
        String           sValue = dbnbDB.getFieldData( "IsBlueStar" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setInitialQuota(Integer newValue)
    {
                dbnbDB.setFieldData( "InitialQuota", newValue.toString() );
    }

    public Integer getInitialQuota()
    {
        String           sValue = dbnbDB.getFieldData( "InitialQuota" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setPaymentTermsNet(Integer newValue)
    {
                dbnbDB.setFieldData( "PaymentTermsNet", newValue.toString() );
    }

    public Integer getPaymentTermsNet()
    {
        String           sValue = dbnbDB.getFieldData( "PaymentTermsNet" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setPaymentTermsMethod(Integer newValue)
    {
                dbnbDB.setFieldData( "PaymentTermsMethod", newValue.toString() );
    }

    public Integer getPaymentTermsMethod()
    {
        String           sValue = dbnbDB.getFieldData( "PaymentTermsMethod" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setDiagnosticMDArthrogram(Integer newValue)
    {
                dbnbDB.setFieldData( "DiagnosticMDArthrogram", newValue.toString() );
    }

    public Integer getDiagnosticMDArthrogram()
    {
        String           sValue = dbnbDB.getFieldData( "DiagnosticMDArthrogram" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setOfficeMDArthrogramHoursOpenMonday(String newValue)
    {
                dbnbDB.setFieldData( "OfficeMDArthrogramHoursOpenMonday", newValue.toString() );
    }

    public String getOfficeMDArthrogramHoursOpenMonday()
    {
        String           sValue = dbnbDB.getFieldData( "OfficeMDArthrogramHoursOpenMonday" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setOfficeMDArthrogramHoursOpenTuesday(String newValue)
    {
                dbnbDB.setFieldData( "OfficeMDArthrogramHoursOpenTuesday", newValue.toString() );
    }

    public String getOfficeMDArthrogramHoursOpenTuesday()
    {
        String           sValue = dbnbDB.getFieldData( "OfficeMDArthrogramHoursOpenTuesday" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setOfficeMDArthrogramHoursOpenWednesday(String newValue)
    {
                dbnbDB.setFieldData( "OfficeMDArthrogramHoursOpenWednesday", newValue.toString() );
    }

    public String getOfficeMDArthrogramHoursOpenWednesday()
    {
        String           sValue = dbnbDB.getFieldData( "OfficeMDArthrogramHoursOpenWednesday" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setOfficeMDArthrogramHoursOpenThursday(String newValue)
    {
                dbnbDB.setFieldData( "OfficeMDArthrogramHoursOpenThursday", newValue.toString() );
    }

    public String getOfficeMDArthrogramHoursOpenThursday()
    {
        String           sValue = dbnbDB.getFieldData( "OfficeMDArthrogramHoursOpenThursday" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setOfficeMDArthrogramHoursOpenFriday(String newValue)
    {
                dbnbDB.setFieldData( "OfficeMDArthrogramHoursOpenFriday", newValue.toString() );
    }

    public String getOfficeMDArthrogramHoursOpenFriday()
    {
        String           sValue = dbnbDB.getFieldData( "OfficeMDArthrogramHoursOpenFriday" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setOfficeMDArthrogramHoursOpenSaturday(String newValue)
    {
                dbnbDB.setFieldData( "OfficeMDArthrogramHoursOpenSaturday", newValue.toString() );
    }

    public String getOfficeMDArthrogramHoursOpenSaturday()
    {
        String           sValue = dbnbDB.getFieldData( "OfficeMDArthrogramHoursOpenSaturday" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setOfficeMDArthrogramHoursOpenSunday(String newValue)
    {
                dbnbDB.setFieldData( "OfficeMDArthrogramHoursOpenSunday", newValue.toString() );
    }

    public String getOfficeMDArthrogramHoursOpenSunday()
    {
        String           sValue = dbnbDB.getFieldData( "OfficeMDArthrogramHoursOpenSunday" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setOfficeMDArthrogramHoursCloseMonday(String newValue)
    {
                dbnbDB.setFieldData( "OfficeMDArthrogramHoursCloseMonday", newValue.toString() );
    }

    public String getOfficeMDArthrogramHoursCloseMonday()
    {
        String           sValue = dbnbDB.getFieldData( "OfficeMDArthrogramHoursCloseMonday" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setOfficeMDArthrogramHoursCloseTuesday(String newValue)
    {
                dbnbDB.setFieldData( "OfficeMDArthrogramHoursCloseTuesday", newValue.toString() );
    }

    public String getOfficeMDArthrogramHoursCloseTuesday()
    {
        String           sValue = dbnbDB.getFieldData( "OfficeMDArthrogramHoursCloseTuesday" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setOfficeMDArthrogramHoursCloseWednesday(String newValue)
    {
                dbnbDB.setFieldData( "OfficeMDArthrogramHoursCloseWednesday", newValue.toString() );
    }

    public String getOfficeMDArthrogramHoursCloseWednesday()
    {
        String           sValue = dbnbDB.getFieldData( "OfficeMDArthrogramHoursCloseWednesday" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setOfficeMDArthrogramHoursCloseThursday(String newValue)
    {
                dbnbDB.setFieldData( "OfficeMDArthrogramHoursCloseThursday", newValue.toString() );
    }

    public String getOfficeMDArthrogramHoursCloseThursday()
    {
        String           sValue = dbnbDB.getFieldData( "OfficeMDArthrogramHoursCloseThursday" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setOfficeMDArthrogramHoursCloseFriday(String newValue)
    {
                dbnbDB.setFieldData( "OfficeMDArthrogramHoursCloseFriday", newValue.toString() );
    }

    public String getOfficeMDArthrogramHoursCloseFriday()
    {
        String           sValue = dbnbDB.getFieldData( "OfficeMDArthrogramHoursCloseFriday" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setOfficeMDArthrogramHoursCloseSaturday(String newValue)
    {
                dbnbDB.setFieldData( "OfficeMDArthrogramHoursCloseSaturday", newValue.toString() );
    }

    public String getOfficeMDArthrogramHoursCloseSaturday()
    {
        String           sValue = dbnbDB.getFieldData( "OfficeMDArthrogramHoursCloseSaturday" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setOfficeMDArthrogramHoursCloseSunday(String newValue)
    {
                dbnbDB.setFieldData( "OfficeMDArthrogramHoursCloseSunday", newValue.toString() );
    }

    public String getOfficeMDArthrogramHoursCloseSunday()
    {
        String           sValue = dbnbDB.getFieldData( "OfficeMDArthrogramHoursCloseSunday" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setDiagnosticMDMyelogram(Integer newValue)
    {
                dbnbDB.setFieldData( "DiagnosticMDMyelogram", newValue.toString() );
    }

    public Integer getDiagnosticMDMyelogram()
    {
        String           sValue = dbnbDB.getFieldData( "DiagnosticMDMyelogram" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setOfficeMDMyelogramHoursOpenMonday(String newValue)
    {
                dbnbDB.setFieldData( "OfficeMDMyelogramHoursOpenMonday", newValue.toString() );
    }

    public String getOfficeMDMyelogramHoursOpenMonday()
    {
        String           sValue = dbnbDB.getFieldData( "OfficeMDMyelogramHoursOpenMonday" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setOfficeMDMyelogramHoursOpenTuesday(String newValue)
    {
                dbnbDB.setFieldData( "OfficeMDMyelogramHoursOpenTuesday", newValue.toString() );
    }

    public String getOfficeMDMyelogramHoursOpenTuesday()
    {
        String           sValue = dbnbDB.getFieldData( "OfficeMDMyelogramHoursOpenTuesday" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setOfficeMDMyelogramHoursOpenWednesday(String newValue)
    {
                dbnbDB.setFieldData( "OfficeMDMyelogramHoursOpenWednesday", newValue.toString() );
    }

    public String getOfficeMDMyelogramHoursOpenWednesday()
    {
        String           sValue = dbnbDB.getFieldData( "OfficeMDMyelogramHoursOpenWednesday" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setOfficeMDMyelogramHoursOpenThursday(String newValue)
    {
                dbnbDB.setFieldData( "OfficeMDMyelogramHoursOpenThursday", newValue.toString() );
    }

    public String getOfficeMDMyelogramHoursOpenThursday()
    {
        String           sValue = dbnbDB.getFieldData( "OfficeMDMyelogramHoursOpenThursday" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setOfficeMDMyelogramHoursOpenFriday(String newValue)
    {
                dbnbDB.setFieldData( "OfficeMDMyelogramHoursOpenFriday", newValue.toString() );
    }

    public String getOfficeMDMyelogramHoursOpenFriday()
    {
        String           sValue = dbnbDB.getFieldData( "OfficeMDMyelogramHoursOpenFriday" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setOfficeMDMyelogramHoursOpenSaturday(String newValue)
    {
                dbnbDB.setFieldData( "OfficeMDMyelogramHoursOpenSaturday", newValue.toString() );
    }

    public String getOfficeMDMyelogramHoursOpenSaturday()
    {
        String           sValue = dbnbDB.getFieldData( "OfficeMDMyelogramHoursOpenSaturday" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setOfficeMDMyelogramHoursOpenSunday(String newValue)
    {
                dbnbDB.setFieldData( "OfficeMDMyelogramHoursOpenSunday", newValue.toString() );
    }

    public String getOfficeMDMyelogramHoursOpenSunday()
    {
        String           sValue = dbnbDB.getFieldData( "OfficeMDMyelogramHoursOpenSunday" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setOfficeMDMyelogramHoursCloseMonday(String newValue)
    {
                dbnbDB.setFieldData( "OfficeMDMyelogramHoursCloseMonday", newValue.toString() );
    }

    public String getOfficeMDMyelogramHoursCloseMonday()
    {
        String           sValue = dbnbDB.getFieldData( "OfficeMDMyelogramHoursCloseMonday" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setOfficeMDMyelogramHoursCloseTuesday(String newValue)
    {
                dbnbDB.setFieldData( "OfficeMDMyelogramHoursCloseTuesday", newValue.toString() );
    }

    public String getOfficeMDMyelogramHoursCloseTuesday()
    {
        String           sValue = dbnbDB.getFieldData( "OfficeMDMyelogramHoursCloseTuesday" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setOfficeMDMyelogramHoursCloseWednesday(String newValue)
    {
                dbnbDB.setFieldData( "OfficeMDMyelogramHoursCloseWednesday", newValue.toString() );
    }

    public String getOfficeMDMyelogramHoursCloseWednesday()
    {
        String           sValue = dbnbDB.getFieldData( "OfficeMDMyelogramHoursCloseWednesday" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setOfficeMDMyelogramHoursCloseThursday(String newValue)
    {
                dbnbDB.setFieldData( "OfficeMDMyelogramHoursCloseThursday", newValue.toString() );
    }

    public String getOfficeMDMyelogramHoursCloseThursday()
    {
        String           sValue = dbnbDB.getFieldData( "OfficeMDMyelogramHoursCloseThursday" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setOfficeMDMyelogramHoursCloseFriday(String newValue)
    {
                dbnbDB.setFieldData( "OfficeMDMyelogramHoursCloseFriday", newValue.toString() );
    }

    public String getOfficeMDMyelogramHoursCloseFriday()
    {
        String           sValue = dbnbDB.getFieldData( "OfficeMDMyelogramHoursCloseFriday" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setOfficeMDMyelogramHoursCloseSaturday(String newValue)
    {
                dbnbDB.setFieldData( "OfficeMDMyelogramHoursCloseSaturday", newValue.toString() );
    }

    public String getOfficeMDMyelogramHoursCloseSaturday()
    {
        String           sValue = dbnbDB.getFieldData( "OfficeMDMyelogramHoursCloseSaturday" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setOfficeMDMyelogramHoursCloseSunday(String newValue)
    {
                dbnbDB.setFieldData( "OfficeMDMyelogramHoursCloseSunday", newValue.toString() );
    }

    public String getOfficeMDMyelogramHoursCloseSunday()
    {
        String           sValue = dbnbDB.getFieldData( "OfficeMDMyelogramHoursCloseSunday" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setPetLinQID(String newValue)
    {
                dbnbDB.setFieldData( "PetLinQID", newValue.toString() );
    }

    public String getPetLinQID()
    {
        String           sValue = dbnbDB.getFieldData( "PetLinQID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setDoesAging(Integer newValue)
    {
                dbnbDB.setFieldData( "DoesAging", newValue.toString() );
    }

    public Integer getDoesAging()
    {
        String           sValue = dbnbDB.getFieldData( "DoesAging" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setSelectMRI_ID(Integer newValue)
    {
                dbnbDB.setFieldData( "SelectMRI_ID", newValue.toString() );
    }

    public Integer getSelectMRI_ID()
    {
        String           sValue = dbnbDB.getFieldData( "SelectMRI_ID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setSelectMRI_Notes(String newValue)
    {
                dbnbDB.setFieldData( "SelectMRI_Notes", newValue.toString() );
    }

    public String getSelectMRI_Notes()
    {
        String           sValue = dbnbDB.getFieldData( "SelectMRI_Notes" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setSelectMRI_QBID(String newValue)
    {
                dbnbDB.setFieldData( "SelectMRI_QBID", newValue.toString() );
    }

    public String getSelectMRI_QBID()
    {
        String           sValue = dbnbDB.getFieldData( "SelectMRI_QBID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setScheduling_Notes(String newValue)
    {
                dbnbDB.setFieldData( "Scheduling_Notes", newValue.toString() );
    }

    public String getScheduling_Notes()
    {
        String           sValue = dbnbDB.getFieldData( "Scheduling_Notes" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setBReader(Integer newValue)
    {
                dbnbDB.setFieldData( "BReader", newValue.toString() );
    }

    public Integer getBReader()
    {
        String           sValue = dbnbDB.getFieldData( "BReader" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setNextImageTeleRadMember(Integer newValue)
    {
                dbnbDB.setFieldData( "NextImageTeleRadMember", newValue.toString() );
    }

    public Integer getNextImageTeleRadMember()
    {
        String           sValue = dbnbDB.getFieldData( "NextImageTeleRadMember" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setContractGlobal(Integer newValue)
    {
                dbnbDB.setFieldData( "ContractGlobal", newValue.toString() );
    }

    public Integer getContractGlobal()
    {
        String           sValue = dbnbDB.getFieldData( "ContractGlobal" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setContractTechOnly(Integer newValue)
    {
                dbnbDB.setFieldData( "ContractTechOnly", newValue.toString() );
    }

    public Integer getContractTechOnly()
    {
        String           sValue = dbnbDB.getFieldData( "ContractTechOnly" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setContractProfessionalOnly(Integer newValue)
    {
                dbnbDB.setFieldData( "ContractProfessionalOnly", newValue.toString() );
    }

    public Integer getContractProfessionalOnly()
    {
        String           sValue = dbnbDB.getFieldData( "ContractProfessionalOnly" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setFeeScheduleOverRefID(Integer newValue)
    {
                dbnbDB.setFieldData( "FeeScheduleOverRefID", newValue.toString() );
    }

    public Integer getFeeScheduleOverRefID()
    {
        String           sValue = dbnbDB.getFieldData( "FeeScheduleOverRefID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setFeeOverPercentage(Double newValue)
    {
                java.text.DecimalFormat defDecFormat2 = new java.text.DecimalFormat(PLCUtils.String_defDecFormat2);
                dbnbDB.setFieldData( "FeeOverPercentage", defDecFormat2.format(newValue) );
    }

    public Double getFeeOverPercentage()
    {
        String           sValue = dbnbDB.getFieldData( "FeeOverPercentage" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDoubleValue : sValue;
        return new Double( sValue  ); 
    }

    public void setPrice_Mod_MRI_W_26(Double newValue)
    {
                java.text.DecimalFormat defDecFormat2 = new java.text.DecimalFormat(PLCUtils.String_defDecFormat2);
                dbnbDB.setFieldData( "Price_Mod_MRI_W_26", defDecFormat2.format(newValue) );
    }

    public Double getPrice_Mod_MRI_W_26()
    {
        String           sValue = dbnbDB.getFieldData( "Price_Mod_MRI_W_26" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDoubleValue : sValue;
        return new Double( sValue  ); 
    }

    public void setPrice_Mod_MRI_WO_26(Double newValue)
    {
                java.text.DecimalFormat defDecFormat2 = new java.text.DecimalFormat(PLCUtils.String_defDecFormat2);
                dbnbDB.setFieldData( "Price_Mod_MRI_WO_26", defDecFormat2.format(newValue) );
    }

    public Double getPrice_Mod_MRI_WO_26()
    {
        String           sValue = dbnbDB.getFieldData( "Price_Mod_MRI_WO_26" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDoubleValue : sValue;
        return new Double( sValue  ); 
    }

    public void setPrice_Mod_MRI_WWO_26(Double newValue)
    {
                java.text.DecimalFormat defDecFormat2 = new java.text.DecimalFormat(PLCUtils.String_defDecFormat2);
                dbnbDB.setFieldData( "Price_Mod_MRI_WWO_26", defDecFormat2.format(newValue) );
    }

    public Double getPrice_Mod_MRI_WWO_26()
    {
        String           sValue = dbnbDB.getFieldData( "Price_Mod_MRI_WWO_26" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDoubleValue : sValue;
        return new Double( sValue  ); 
    }

    public void setPrice_Mod_CT_W_26(Double newValue)
    {
                java.text.DecimalFormat defDecFormat2 = new java.text.DecimalFormat(PLCUtils.String_defDecFormat2);
                dbnbDB.setFieldData( "Price_Mod_CT_W_26", defDecFormat2.format(newValue) );
    }

    public Double getPrice_Mod_CT_W_26()
    {
        String           sValue = dbnbDB.getFieldData( "Price_Mod_CT_W_26" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDoubleValue : sValue;
        return new Double( sValue  ); 
    }

    public void setPrice_Mod_CT_WO_26(Double newValue)
    {
                java.text.DecimalFormat defDecFormat2 = new java.text.DecimalFormat(PLCUtils.String_defDecFormat2);
                dbnbDB.setFieldData( "Price_Mod_CT_WO_26", defDecFormat2.format(newValue) );
    }

    public Double getPrice_Mod_CT_WO_26()
    {
        String           sValue = dbnbDB.getFieldData( "Price_Mod_CT_WO_26" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDoubleValue : sValue;
        return new Double( sValue  ); 
    }

    public void setPrice_Mod_CT_WWO_26(Double newValue)
    {
                java.text.DecimalFormat defDecFormat2 = new java.text.DecimalFormat(PLCUtils.String_defDecFormat2);
                dbnbDB.setFieldData( "Price_Mod_CT_WWO_26", defDecFormat2.format(newValue) );
    }

    public Double getPrice_Mod_CT_WWO_26()
    {
        String           sValue = dbnbDB.getFieldData( "Price_Mod_CT_WWO_26" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDoubleValue : sValue;
        return new Double( sValue  ); 
    }

    public void setPrice_Mod_MRI_W_TC(Double newValue)
    {
                java.text.DecimalFormat defDecFormat2 = new java.text.DecimalFormat(PLCUtils.String_defDecFormat2);
                dbnbDB.setFieldData( "Price_Mod_MRI_W_TC", defDecFormat2.format(newValue) );
    }

    public Double getPrice_Mod_MRI_W_TC()
    {
        String           sValue = dbnbDB.getFieldData( "Price_Mod_MRI_W_TC" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDoubleValue : sValue;
        return new Double( sValue  ); 
    }

    public void setPrice_Mod_MRI_WO_TC(Double newValue)
    {
                java.text.DecimalFormat defDecFormat2 = new java.text.DecimalFormat(PLCUtils.String_defDecFormat2);
                dbnbDB.setFieldData( "Price_Mod_MRI_WO_TC", defDecFormat2.format(newValue) );
    }

    public Double getPrice_Mod_MRI_WO_TC()
    {
        String           sValue = dbnbDB.getFieldData( "Price_Mod_MRI_WO_TC" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDoubleValue : sValue;
        return new Double( sValue  ); 
    }

    public void setPrice_Mod_MRI_WWO_TC(Double newValue)
    {
                java.text.DecimalFormat defDecFormat2 = new java.text.DecimalFormat(PLCUtils.String_defDecFormat2);
                dbnbDB.setFieldData( "Price_Mod_MRI_WWO_TC", defDecFormat2.format(newValue) );
    }

    public Double getPrice_Mod_MRI_WWO_TC()
    {
        String           sValue = dbnbDB.getFieldData( "Price_Mod_MRI_WWO_TC" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDoubleValue : sValue;
        return new Double( sValue  ); 
    }

    public void setPrice_Mod_CT_W_TC(Double newValue)
    {
                java.text.DecimalFormat defDecFormat2 = new java.text.DecimalFormat(PLCUtils.String_defDecFormat2);
                dbnbDB.setFieldData( "Price_Mod_CT_W_TC", defDecFormat2.format(newValue) );
    }

    public Double getPrice_Mod_CT_W_TC()
    {
        String           sValue = dbnbDB.getFieldData( "Price_Mod_CT_W_TC" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDoubleValue : sValue;
        return new Double( sValue  ); 
    }

    public void setPrice_Mod_CT_WO_TC(Double newValue)
    {
                java.text.DecimalFormat defDecFormat2 = new java.text.DecimalFormat(PLCUtils.String_defDecFormat2);
                dbnbDB.setFieldData( "Price_Mod_CT_WO_TC", defDecFormat2.format(newValue) );
    }

    public Double getPrice_Mod_CT_WO_TC()
    {
        String           sValue = dbnbDB.getFieldData( "Price_Mod_CT_WO_TC" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDoubleValue : sValue;
        return new Double( sValue  ); 
    }

    public void setPrice_Mod_CT_WWO_TC(Double newValue)
    {
                java.text.DecimalFormat defDecFormat2 = new java.text.DecimalFormat(PLCUtils.String_defDecFormat2);
                dbnbDB.setFieldData( "Price_Mod_CT_WWO_TC", defDecFormat2.format(newValue) );
    }

    public Double getPrice_Mod_CT_WWO_TC()
    {
        String           sValue = dbnbDB.getFieldData( "Price_Mod_CT_WWO_TC" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDoubleValue : sValue;
        return new Double( sValue  ); 
    }

    public void setFaxReportReminderOverride(String newValue)
    {
                dbnbDB.setFieldData( "FaxReportReminderOverride", newValue.toString() );
    }

    public String getFaxReportReminderOverride()
    {
        String           sValue = dbnbDB.getFieldData( "FaxReportReminderOverride" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setGH_Price_Mod_MRI_W(Double newValue)
    {
                java.text.DecimalFormat defDecFormat2 = new java.text.DecimalFormat(PLCUtils.String_defDecFormat2);
                dbnbDB.setFieldData( "GH_Price_Mod_MRI_W", defDecFormat2.format(newValue) );
    }

    public Double getGH_Price_Mod_MRI_W()
    {
        String           sValue = dbnbDB.getFieldData( "GH_Price_Mod_MRI_W" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDoubleValue : sValue;
        return new Double( sValue  ); 
    }

    public void setGH_Price_Mod_MRI_WO(Double newValue)
    {
                java.text.DecimalFormat defDecFormat2 = new java.text.DecimalFormat(PLCUtils.String_defDecFormat2);
                dbnbDB.setFieldData( "GH_Price_Mod_MRI_WO", defDecFormat2.format(newValue) );
    }

    public Double getGH_Price_Mod_MRI_WO()
    {
        String           sValue = dbnbDB.getFieldData( "GH_Price_Mod_MRI_WO" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDoubleValue : sValue;
        return new Double( sValue  ); 
    }

    public void setGH_Price_Mod_MRI_WWO(Double newValue)
    {
                java.text.DecimalFormat defDecFormat2 = new java.text.DecimalFormat(PLCUtils.String_defDecFormat2);
                dbnbDB.setFieldData( "GH_Price_Mod_MRI_WWO", defDecFormat2.format(newValue) );
    }

    public Double getGH_Price_Mod_MRI_WWO()
    {
        String           sValue = dbnbDB.getFieldData( "GH_Price_Mod_MRI_WWO" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDoubleValue : sValue;
        return new Double( sValue  ); 
    }

    public void setGH_Price_Mod_CT_W(Double newValue)
    {
                java.text.DecimalFormat defDecFormat2 = new java.text.DecimalFormat(PLCUtils.String_defDecFormat2);
                dbnbDB.setFieldData( "GH_Price_Mod_CT_W", defDecFormat2.format(newValue) );
    }

    public Double getGH_Price_Mod_CT_W()
    {
        String           sValue = dbnbDB.getFieldData( "GH_Price_Mod_CT_W" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDoubleValue : sValue;
        return new Double( sValue  ); 
    }

    public void setGH_Price_Mod_CT_WO(Double newValue)
    {
                java.text.DecimalFormat defDecFormat2 = new java.text.DecimalFormat(PLCUtils.String_defDecFormat2);
                dbnbDB.setFieldData( "GH_Price_Mod_CT_WO", defDecFormat2.format(newValue) );
    }

    public Double getGH_Price_Mod_CT_WO()
    {
        String           sValue = dbnbDB.getFieldData( "GH_Price_Mod_CT_WO" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDoubleValue : sValue;
        return new Double( sValue  ); 
    }

    public void setGH_Price_Mod_CT_WWO(Double newValue)
    {
                java.text.DecimalFormat defDecFormat2 = new java.text.DecimalFormat(PLCUtils.String_defDecFormat2);
                dbnbDB.setFieldData( "GH_Price_Mod_CT_WWO", defDecFormat2.format(newValue) );
    }

    public Double getGH_Price_Mod_CT_WWO()
    {
        String           sValue = dbnbDB.getFieldData( "GH_Price_Mod_CT_WWO" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDoubleValue : sValue;
        return new Double( sValue  ); 
    }

    public void setGH_Price_Mod_MRI_W_26(Double newValue)
    {
                java.text.DecimalFormat defDecFormat2 = new java.text.DecimalFormat(PLCUtils.String_defDecFormat2);
                dbnbDB.setFieldData( "GH_Price_Mod_MRI_W_26", defDecFormat2.format(newValue) );
    }

    public Double getGH_Price_Mod_MRI_W_26()
    {
        String           sValue = dbnbDB.getFieldData( "GH_Price_Mod_MRI_W_26" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDoubleValue : sValue;
        return new Double( sValue  ); 
    }

    public void setGH_Price_Mod_MRI_WO_26(Double newValue)
    {
                java.text.DecimalFormat defDecFormat2 = new java.text.DecimalFormat(PLCUtils.String_defDecFormat2);
                dbnbDB.setFieldData( "GH_Price_Mod_MRI_WO_26", defDecFormat2.format(newValue) );
    }

    public Double getGH_Price_Mod_MRI_WO_26()
    {
        String           sValue = dbnbDB.getFieldData( "GH_Price_Mod_MRI_WO_26" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDoubleValue : sValue;
        return new Double( sValue  ); 
    }

    public void setGH_Price_Mod_MRI_WWO_26(Double newValue)
    {
                java.text.DecimalFormat defDecFormat2 = new java.text.DecimalFormat(PLCUtils.String_defDecFormat2);
                dbnbDB.setFieldData( "GH_Price_Mod_MRI_WWO_26", defDecFormat2.format(newValue) );
    }

    public Double getGH_Price_Mod_MRI_WWO_26()
    {
        String           sValue = dbnbDB.getFieldData( "GH_Price_Mod_MRI_WWO_26" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDoubleValue : sValue;
        return new Double( sValue  ); 
    }

    public void setGH_Price_Mod_CT_W_26(Double newValue)
    {
                java.text.DecimalFormat defDecFormat2 = new java.text.DecimalFormat(PLCUtils.String_defDecFormat2);
                dbnbDB.setFieldData( "GH_Price_Mod_CT_W_26", defDecFormat2.format(newValue) );
    }

    public Double getGH_Price_Mod_CT_W_26()
    {
        String           sValue = dbnbDB.getFieldData( "GH_Price_Mod_CT_W_26" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDoubleValue : sValue;
        return new Double( sValue  ); 
    }

    public void setGH_Price_Mod_CT_WO_26(Double newValue)
    {
                java.text.DecimalFormat defDecFormat2 = new java.text.DecimalFormat(PLCUtils.String_defDecFormat2);
                dbnbDB.setFieldData( "GH_Price_Mod_CT_WO_26", defDecFormat2.format(newValue) );
    }

    public Double getGH_Price_Mod_CT_WO_26()
    {
        String           sValue = dbnbDB.getFieldData( "GH_Price_Mod_CT_WO_26" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDoubleValue : sValue;
        return new Double( sValue  ); 
    }

    public void setGH_Price_Mod_CT_WWO_26(Double newValue)
    {
                java.text.DecimalFormat defDecFormat2 = new java.text.DecimalFormat(PLCUtils.String_defDecFormat2);
                dbnbDB.setFieldData( "GH_Price_Mod_CT_WWO_26", defDecFormat2.format(newValue) );
    }

    public Double getGH_Price_Mod_CT_WWO_26()
    {
        String           sValue = dbnbDB.getFieldData( "GH_Price_Mod_CT_WWO_26" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDoubleValue : sValue;
        return new Double( sValue  ); 
    }

    public void setGH_Price_Mod_MRI_W_TC(Double newValue)
    {
                java.text.DecimalFormat defDecFormat2 = new java.text.DecimalFormat(PLCUtils.String_defDecFormat2);
                dbnbDB.setFieldData( "GH_Price_Mod_MRI_W_TC", defDecFormat2.format(newValue) );
    }

    public Double getGH_Price_Mod_MRI_W_TC()
    {
        String           sValue = dbnbDB.getFieldData( "GH_Price_Mod_MRI_W_TC" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDoubleValue : sValue;
        return new Double( sValue  ); 
    }

    public void setGH_Price_Mod_MRI_WO_TC(Double newValue)
    {
                java.text.DecimalFormat defDecFormat2 = new java.text.DecimalFormat(PLCUtils.String_defDecFormat2);
                dbnbDB.setFieldData( "GH_Price_Mod_MRI_WO_TC", defDecFormat2.format(newValue) );
    }

    public Double getGH_Price_Mod_MRI_WO_TC()
    {
        String           sValue = dbnbDB.getFieldData( "GH_Price_Mod_MRI_WO_TC" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDoubleValue : sValue;
        return new Double( sValue  ); 
    }

    public void setGH_Price_Mod_MRI_WWO_TC(Double newValue)
    {
                java.text.DecimalFormat defDecFormat2 = new java.text.DecimalFormat(PLCUtils.String_defDecFormat2);
                dbnbDB.setFieldData( "GH_Price_Mod_MRI_WWO_TC", defDecFormat2.format(newValue) );
    }

    public Double getGH_Price_Mod_MRI_WWO_TC()
    {
        String           sValue = dbnbDB.getFieldData( "GH_Price_Mod_MRI_WWO_TC" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDoubleValue : sValue;
        return new Double( sValue  ); 
    }

    public void setGH_Price_Mod_CT_W_TC(Double newValue)
    {
                java.text.DecimalFormat defDecFormat2 = new java.text.DecimalFormat(PLCUtils.String_defDecFormat2);
                dbnbDB.setFieldData( "GH_Price_Mod_CT_W_TC", defDecFormat2.format(newValue) );
    }

    public Double getGH_Price_Mod_CT_W_TC()
    {
        String           sValue = dbnbDB.getFieldData( "GH_Price_Mod_CT_W_TC" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDoubleValue : sValue;
        return new Double( sValue  ); 
    }

    public void setGH_Price_Mod_CT_WO_TC(Double newValue)
    {
                java.text.DecimalFormat defDecFormat2 = new java.text.DecimalFormat(PLCUtils.String_defDecFormat2);
                dbnbDB.setFieldData( "GH_Price_Mod_CT_WO_TC", defDecFormat2.format(newValue) );
    }

    public Double getGH_Price_Mod_CT_WO_TC()
    {
        String           sValue = dbnbDB.getFieldData( "GH_Price_Mod_CT_WO_TC" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDoubleValue : sValue;
        return new Double( sValue  ); 
    }

    public void setGH_Price_Mod_CT_WWO_TC(Double newValue)
    {
                java.text.DecimalFormat defDecFormat2 = new java.text.DecimalFormat(PLCUtils.String_defDecFormat2);
                dbnbDB.setFieldData( "GH_Price_Mod_CT_WWO_TC", defDecFormat2.format(newValue) );
    }

    public Double getGH_Price_Mod_CT_WWO_TC()
    {
        String           sValue = dbnbDB.getFieldData( "GH_Price_Mod_CT_WWO_TC" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDoubleValue : sValue;
        return new Double( sValue  ); 
    }

    public void setGH_FeeScheduleRefID(Integer newValue)
    {
                dbnbDB.setFieldData( "GH_FeeScheduleRefID", newValue.toString() );
    }

    public Integer getGH_FeeScheduleRefID()
    {
        String           sValue = dbnbDB.getFieldData( "GH_FeeScheduleRefID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setGH_FeePercentage(Double newValue)
    {
                java.text.DecimalFormat defDecFormat2 = new java.text.DecimalFormat(PLCUtils.String_defDecFormat2);
                dbnbDB.setFieldData( "GH_FeePercentage", defDecFormat2.format(newValue) );
    }

    public Double getGH_FeePercentage()
    {
        String           sValue = dbnbDB.getFieldData( "GH_FeePercentage" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDoubleValue : sValue;
        return new Double( sValue  ); 
    }

    public void setGH_InitialQuota(Integer newValue)
    {
                dbnbDB.setFieldData( "GH_InitialQuota", newValue.toString() );
    }

    public Integer getGH_InitialQuota()
    {
        String           sValue = dbnbDB.getFieldData( "GH_InitialQuota" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setGH_PaymentTermsNet(Integer newValue)
    {
                dbnbDB.setFieldData( "GH_PaymentTermsNet", newValue.toString() );
    }

    public Integer getGH_PaymentTermsNet()
    {
        String           sValue = dbnbDB.getFieldData( "GH_PaymentTermsNet" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setGH_PaymentTermsMethod(Integer newValue)
    {
                dbnbDB.setFieldData( "GH_PaymentTermsMethod", newValue.toString() );
    }

    public Integer getGH_PaymentTermsMethod()
    {
        String           sValue = dbnbDB.getFieldData( "GH_PaymentTermsMethod" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setGH_DoesAging(Integer newValue)
    {
                dbnbDB.setFieldData( "GH_DoesAging", newValue.toString() );
    }

    public Integer getGH_DoesAging()
    {
        String           sValue = dbnbDB.getFieldData( "GH_DoesAging" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setGH_DiagnosticMDArthrogram(Integer newValue)
    {
                dbnbDB.setFieldData( "GH_DiagnosticMDArthrogram", newValue.toString() );
    }

    public Integer getGH_DiagnosticMDArthrogram()
    {
        String           sValue = dbnbDB.getFieldData( "GH_DiagnosticMDArthrogram" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setGH_DiagnosticMDMyelogram(Integer newValue)
    {
                dbnbDB.setFieldData( "GH_DiagnosticMDMyelogram", newValue.toString() );
    }

    public Integer getGH_DiagnosticMDMyelogram()
    {
        String           sValue = dbnbDB.getFieldData( "GH_DiagnosticMDMyelogram" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setGH_ClientTypeID(Integer newValue)
    {
                dbnbDB.setFieldData( "GH_ClientTypeID", newValue.toString() );
    }

    public Integer getGH_ClientTypeID()
    {
        String           sValue = dbnbDB.getFieldData( "GH_ClientTypeID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setGH_Terminated(Integer newValue)
    {
                dbnbDB.setFieldData( "GH_Terminated", newValue.toString() );
    }

    public Integer getGH_Terminated()
    {
        String           sValue = dbnbDB.getFieldData( "GH_Terminated" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setGH_TerminatedDate(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "GH_TerminatedDate", formatter.format( newValue ) );
    }

    public Date getGH_TerminatedDate()
    {
        String           sValue = dbnbDB.getFieldData( "GH_TerminatedDate" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setPrice_Mod_PETCT_WWO(Double newValue)
    {
                java.text.DecimalFormat defDecFormat2 = new java.text.DecimalFormat(PLCUtils.String_defDecFormat2);
                dbnbDB.setFieldData( "Price_Mod_PETCT_WWO", defDecFormat2.format(newValue) );
    }

    public Double getPrice_Mod_PETCT_WWO()
    {
        String           sValue = dbnbDB.getFieldData( "Price_Mod_PETCT_WWO" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDoubleValue : sValue;
        return new Double( sValue  ); 
    }

    public void setPrice_Mod_PETCT_WWO_TC(Double newValue)
    {
                java.text.DecimalFormat defDecFormat2 = new java.text.DecimalFormat(PLCUtils.String_defDecFormat2);
                dbnbDB.setFieldData( "Price_Mod_PETCT_WWO_TC", defDecFormat2.format(newValue) );
    }

    public Double getPrice_Mod_PETCT_WWO_TC()
    {
        String           sValue = dbnbDB.getFieldData( "Price_Mod_PETCT_WWO_TC" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDoubleValue : sValue;
        return new Double( sValue  ); 
    }

    public void setPrice_Mod_PETCT_WWO_26(Double newValue)
    {
                java.text.DecimalFormat defDecFormat2 = new java.text.DecimalFormat(PLCUtils.String_defDecFormat2);
                dbnbDB.setFieldData( "Price_Mod_PETCT_WWO_26", defDecFormat2.format(newValue) );
    }

    public Double getPrice_Mod_PETCT_WWO_26()
    {
        String           sValue = dbnbDB.getFieldData( "Price_Mod_PETCT_WWO_26" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDoubleValue : sValue;
        return new Double( sValue  ); 
    }

    public void setGH_Price_Mod_PETCT_WWO(Double newValue)
    {
                java.text.DecimalFormat defDecFormat2 = new java.text.DecimalFormat(PLCUtils.String_defDecFormat2);
                dbnbDB.setFieldData( "GH_Price_Mod_PETCT_WWO", defDecFormat2.format(newValue) );
    }

    public Double getGH_Price_Mod_PETCT_WWO()
    {
        String           sValue = dbnbDB.getFieldData( "GH_Price_Mod_PETCT_WWO" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDoubleValue : sValue;
        return new Double( sValue  ); 
    }

    public void setGH_Price_Mod_PETCT_WWO_TC(Double newValue)
    {
                java.text.DecimalFormat defDecFormat2 = new java.text.DecimalFormat(PLCUtils.String_defDecFormat2);
                dbnbDB.setFieldData( "GH_Price_Mod_PETCT_WWO_TC", defDecFormat2.format(newValue) );
    }

    public Double getGH_Price_Mod_PETCT_WWO_TC()
    {
        String           sValue = dbnbDB.getFieldData( "GH_Price_Mod_PETCT_WWO_TC" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDoubleValue : sValue;
        return new Double( sValue  ); 
    }

    public void setGH_Price_Mod_PETCT_WWO_26(Double newValue)
    {
                java.text.DecimalFormat defDecFormat2 = new java.text.DecimalFormat(PLCUtils.String_defDecFormat2);
                dbnbDB.setFieldData( "GH_Price_Mod_PETCT_WWO_26", defDecFormat2.format(newValue) );
    }

    public Double getGH_Price_Mod_PETCT_WWO_26()
    {
        String           sValue = dbnbDB.getFieldData( "GH_Price_Mod_PETCT_WWO_26" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDoubleValue : sValue;
        return new Double( sValue  ); 
    }

    public void setGH_FeeScheduleOverRefID(Integer newValue)
    {
                dbnbDB.setFieldData( "GH_FeeScheduleOverRefID", newValue.toString() );
    }

    public Integer getGH_FeeScheduleOverRefID()
    {
        String           sValue = dbnbDB.getFieldData( "GH_FeeScheduleOverRefID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setGH_FeeOverPercentage(Double newValue)
    {
                java.text.DecimalFormat defDecFormat2 = new java.text.DecimalFormat(PLCUtils.String_defDecFormat2);
                dbnbDB.setFieldData( "GH_FeeOverPercentage", defDecFormat2.format(newValue) );
    }

    public Double getGH_FeeOverPercentage()
    {
        String           sValue = dbnbDB.getFieldData( "GH_FeeOverPercentage" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDoubleValue : sValue;
        return new Double( sValue  ); 
    }

    public void setClientTypeID(Integer newValue)
    {
                dbnbDB.setFieldData( "ClientTypeID", newValue.toString() );
    }

    public Integer getClientTypeID()
    {
        String           sValue = dbnbDB.getFieldData( "ClientTypeID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setTerminated(Integer newValue)
    {
                dbnbDB.setFieldData( "Terminated", newValue.toString() );
    }

    public Integer getTerminated()
    {
        String           sValue = dbnbDB.getFieldData( "Terminated" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setTerminatedDate(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "TerminatedDate", formatter.format( newValue ) );
    }

    public Date getTerminatedDate()
    {
        String           sValue = dbnbDB.getFieldData( "TerminatedDate" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setHasCardiacMR(Integer newValue)
    {
                dbnbDB.setFieldData( "HasCardiacMR", newValue.toString() );
    }

    public Integer getHasCardiacMR()
    {
        String           sValue = dbnbDB.getFieldData( "HasCardiacMR" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setHasCardiacCT(Integer newValue)
    {
                dbnbDB.setFieldData( "HasCardiacCT", newValue.toString() );
    }

    public Integer getHasCardiacCT()
    {
        String           sValue = dbnbDB.getFieldData( "HasCardiacCT" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

}    // End of bltPracticeMaster class definition
