package com.winstaff;
import java.lang.Character;

public class replaceSubstring
{

    private String result;

    public replaceSubstring()
    {
    }


    public String getReplacedSubstring(String fullS, String old1, String new1)
    {
        int i = 1;
        String s = fullS;
        String s1 = "";
        if(old1.equals(""))
        {
            setResult(fullS);
        }
        while(i >= 0)
        {
            i = s.indexOf(old1);
            if(i >= 0)
            {
                s1 = s1 + s.substring(0, i) + new1;
                s = s.substring(i + old1.length());
            } else
            {
                s1 = s1 + s;
            }
        }
        setResult(s1);
        return getResult();
        //return s1 + "ste1";
    }

    public String getReplacedSubstringWildcard(String fullS, String old1, String new1, boolean ignoreCase)
    {
        String myNewString = "";
        int previousIndex=0;
        for (int i=0;i<fullS.length();i++)
        {
            boolean thinkIFoundIt = true;
            boolean foundIt = false;
            int subIndex = 0;
            while (thinkIFoundIt)
            {
                char compareFullS = fullS.charAt(i+subIndex);
                char compareOld1 = old1.charAt(subIndex);
                if (ignoreCase)
                {
                    compareFullS = Character.toLowerCase(compareFullS);
                    compareOld1 = Character.toLowerCase(compareOld1);
                }


                if ( (compareFullS == old1.charAt(subIndex))
                   ||(old1.charAt(subIndex) == this.getWildcard())
                   )
                {
                    thinkIFoundIt = true;
                    if (subIndex == old1.length()-1)
                    {
                        foundIt = true;
                        thinkIFoundIt = false;
                    }
                }
                else
                {
                    thinkIFoundIt = false;
                }

                subIndex++;

                if (i+subIndex>=fullS.length())
                {
                    thinkIFoundIt = false;
                }
            }
            if (foundIt)
            {
              myNewString += fullS.substring(previousIndex,i) + new1;
              previousIndex = i+old1.length();
            }
        }
        myNewString += fullS.substring(previousIndex,fullS.length());
        return myNewString;
    }

    public char getWildcard()
    {
      return wildcard;
    }

    public void setWildcard(char myChar)
    {
      wildcard = myChar;
    }

    protected char wildcard = '@';

    public String getResult()
    {
        return result;
    }


    private void setResult(String s)
    {
        String s1 = result;
        result = s;
    }
}
