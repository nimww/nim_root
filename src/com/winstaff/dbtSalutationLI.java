

package com.winstaff;

import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public class dbtSalutationLI extends Object implements InttSalutationLI
{

        db_NewBase    dbnbDB;

    public dbtSalutationLI()
    {
        dbnbDB = new db_NewBase( "tSalutationLI", "SalutationID" );

    }    // End of default constructor

    public dbtSalutationLI( Integer iNewID )
    {
        dbnbDB = new db_NewBase( "tSalutationLI", "SalutationID" );
        setUniqueID( iNewID );
    }    // End of Constructor knowing an ID

    public void commitData() throws SQLException
    {
            //no longer trapping/ignoring errors 2011-03-17
            dbnbDB.commitData();
    }    // End of commitData()

    public void commitDataForced() throws SQLException
    {
        dbnbDB.commitDataForced();
    }    // End of commitData()

    public Integer getUniqueID()
    {
        return dbnbDB.getUniqueID();
    }    // End of getUniqueID()

    public void setUniqueID( Integer iNewID )
    {
        dbnbDB.setUniqueID( iNewID );
    }    // End of setUniqueID()

    public void setModifyComments( String sNew )
    {
        dbnbDB.setModifyComments( sNew );
    }    // End of setUniqueID()


    public void setSalutationID(Integer newValue)
    {
                dbnbDB.setFieldData( "SalutationID", newValue.toString() );
    }

    public Integer getSalutationID()
    {
        String           sValue = dbnbDB.getFieldData( "SalutationID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setField(String fieldN, Object fieldV)
    {DebugLogger.println("setField Method not implemented yet in class!");}

    public String getFieldType(String fieldN)
    {return null;}


    public void setSalutationShort(String newValue)
    {
                dbnbDB.setFieldData( "SalutationShort", newValue.toString() );
    }

    public String getSalutationShort()
    {
        String           sValue = dbnbDB.getFieldData( "SalutationShort" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setSalutationLong(String newValue)
    {
                dbnbDB.setFieldData( "SalutationLong", newValue.toString() );
    }

    public String getSalutationLong()
    {
        String           sValue = dbnbDB.getFieldData( "SalutationLong" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

}    // End of bltSalutationLI class definition
