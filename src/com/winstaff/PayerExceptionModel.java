package com.winstaff;

import com.google.gson.Gson;
import com.winstaff.PayerExceptionObject.FeeType;

public class PayerExceptionModel{
	private String payerExceptionId;
	private String payerId; 
	private String practiceId; 
	private String cpts; 
	private String fs; 
	private String percentage; 
	private FeeType feeType;
	public PayerExceptionModel() {
	}
	public PayerExceptionModel(String payerId, String practiceId, String cpts, String fs, String percentage, FeeType feeType) {
		this.payerId = payerId;
		this.practiceId = practiceId;
		this.cpts = cpts;
		this.fs = fs;
		this.percentage = percentage;
		this.feeType = feeType;
	}
	public String getPayerExceptionId() {
		return payerExceptionId;
	}
	public void setPayerExceptionId(String payerExceptionId) {
		this.payerExceptionId = payerExceptionId;
	}
	public String getPayerId() {
		return payerId;
	}
	public void setPayerId(String payerId) {
		this.payerId = payerId;
	}
	public String getPracticeId() {
		return practiceId;
	}
	public void setPracticeId(String practiceId) {
		this.practiceId = practiceId;
	}
	public String getCpts() {
		return cpts;
	}
	public void setCpts(String cpts) {
		this.cpts = cpts;
	}
	public String getFs() {
		return fs;
	}
	public void setFs(String fs) {
		this.fs = fs;
	}
	public String getPercentage() {
		return percentage;
	}
	public void setPercentage(String percentage) {
		this.percentage = percentage;
	}
	public FeeType getFeeType() {
		return feeType;
	}
	public void setFeeType(FeeType feeType) {
		this.feeType = feeType;
	}
	@Override
	public String toString() {
		return new Gson().toJson(this);
	}
}