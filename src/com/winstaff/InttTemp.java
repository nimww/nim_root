

package com.winstaff;

import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public interface InttTemp extends dbTableInterface
{

    public void settTempID(Integer newValue);
    public Integer gettTempID();
    public void settext(String newValue);
    public String gettext();
}    // End of bltTemp class definition
