package com.winstaff;
import javax.security.auth.login.Configuration;
import java.io.DataOutputStream;
import java.io.FileOutputStream;
import java.util.Date;
import java.text.SimpleDateFormat;

public class DebugLogger
{
    static String              sFileName        = "";
    static DataOutputStream    out;
    static boolean             firstTime        = true;
    static int                 linesOutputed    = 0;
    static int                 maxLinesOutput   = 75000;

    static String              sFileName_ERROR        = "";
    static DataOutputStream    out_ERROR;
    static boolean             firstTime_ERROR        = true;
    static int                 linesOutputed_ERROR    = 0;
    static int                 maxLinesOutput_ERROR   = 75000;

    
  	/**
	 * <i>Default constructor.</i>
	 * 
	 */
    public DebugLogger()
    {
    }   // End of HistoryLogger()



    public static void println( String s )
    {
        if ( ConfigurationInformation.bInDebugMode||ConfigurationInformation.bInDebugModeShowFldSets )
        {
	        if ( (!s.equalsIgnoreCase(""))&&(!s.equalsIgnoreCase("     ")) )
	        {
	            System.out.println(s);   
	            //printLine(s);   
	        }
        }
        if ( ConfigurationInformation.bCreateLogBasic||ConfigurationInformation.bCreateLogFull )
        {
	        if ( (!s.equalsIgnoreCase(""))&&(!s.equalsIgnoreCase("     ")) )
	        {
	            printLine(s);   
	        }
        }
    }

    public static void println( Exception e )
    {
        println(e.toString());
    }

    public static void println( Integer e )
    {
        println(e.toString());
    }

    public static void println( int e )
    {
        println(e);
    }

    public static void println( long e )
    {
        println(e);
    }

    public static void sendFatalError(String sDebugString)
    {
        java.util.Date myD = new java.util.Date();
        DebugLogger.printError("FE:", "FATAL_ERROR:", "FATAL ERROR\n----------\n"+ sDebugString  +"\n-----------\n" + "[" + myD + "]" );
//        emailType_V3 myEM = new emailType_V3();
//        myEM.setTo(ConfigurationInformation.ErrorToAddress);
//        myEM.setFrom(ConfigurationInformation.ErrorFromAddress);
//        myEM.setSubject("FATAL ERRROR ["+ConfigurationInformation.serverName+"]");
//        myEM.setBody("FATAL ERROR\n----------\n"+ sDebugString  +"\n-----------\n" + "[" + myD + "]" );
//        myEM.isSendMail();
        //myEM.setTo("scottellis42@hotmail.com");
        //myEM.isSendMail_Direct();
    }



    public static void printLine( String s )
    {
        printLine("","",s);
        
    }
    
    public static void e (String t, String s)
    {
        printError("E:",t,s);
    }
    public static void i (String t, String s)
    {
        printLine("I:",t,s);
    }
    public static void d  (String t, String s)
    {
        printLine("D:",t,s);
    }


    public static void printError( String Level, String TAG, String s )
    {
        SimpleDateFormat    formatter1              = new SimpleDateFormat( "MM_dd_yyyy_hh_mm_ss" );
        SimpleDateFormat    formatter2              = new SimpleDateFormat( "yyyy-MM-dd_HH:mm:ss" );
        String              newDateFileNameString   = formatter1.format( new Date() );


        try
        {
            if ( firstTime_ERROR )
            {
                firstTime_ERROR = false;

                linesOutputed_ERROR = 0;

                sFileName_ERROR = ConfigurationInformation.sLogFolder + "ERROR_" + newDateFileNameString + ".txt";

                out_ERROR = new DataOutputStream( new FileOutputStream( sFileName_ERROR ) );
            }

            out_ERROR.writeBytes( "\n******Error Start******" );
            out_ERROR.writeBytes( "\n******"+formatter2.format(new java.util.Date())+"\n" );
            out_ERROR.writeBytes( Level + "> ["+TAG+"]:{\n" + s +"\n}" );
            out_ERROR.writeBytes( "\n" );
            out_ERROR.writeBytes( "\n******Error Stop******" );
            out_ERROR.flush();

            if ( linesOutputed_ERROR++ > maxLinesOutput_ERROR )
            {
                out_ERROR.close();
                firstTime_ERROR = true;
            }
        }
        catch ( Exception e )
        {
            System.out.println( "Logger error: " + e.toString() );
        }

    }   // End of println()

    
    public static void printLine( String Level, String TAG, String s )
    {
        SimpleDateFormat    formatter1              = new SimpleDateFormat( "MM_dd_yyyy_hh_mm_ss" );
        SimpleDateFormat    formatter2              = new SimpleDateFormat( "yyyy-MM-dd_HH:mm:ss" );
        String              newDateFileNameString   = formatter1.format( new Date() );
        
        
        try
        {
            if ( firstTime )
            {
                firstTime = false;
                
                linesOutputed = 0;                
            
                sFileName = ConfigurationInformation.sLogFolder + "DebugQR_" + newDateFileNameString + ".txt";
            
                out = new DataOutputStream( new FileOutputStream( sFileName ) );
            }

            out.writeBytes( Level + " " + formatter2.format(new java.util.Date()) + "> ["+TAG+"] {{" + s +"}}" );
            out.writeBytes( "\n" );
            out.flush();
            
            if ( linesOutputed++ > maxLinesOutput )
            {
                out.close();
                firstTime = true;
            }            
        }
        catch ( Exception e )
        {
            System.out.println( "Logger error: " + e.toString() );
        }
        
    }   // End of println()
    
}   // End of HistoryLogger()
