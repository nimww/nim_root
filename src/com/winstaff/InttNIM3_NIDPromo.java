

package com.winstaff;

import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public interface InttNIM3_NIDPromo extends dbTableInterface
{

    public void setNIDPromoID(Integer newValue);
    public Integer getNIDPromoID();
    public void setUniqueCreateDate(Date newValue);
    public Date getUniqueCreateDate();
    public void setUniqueModifyDate(Date newValue);
    public Date getUniqueModifyDate();
    public void setUniqueModifyComments(String newValue);
    public String getUniqueModifyComments();
    public void setPayerID(Integer newValue);
    public Integer getPayerID();
    public void setWebLink(String newValue);
    public String getWebLink();
    public void setComments(String newValue);
    public String getComments();
    public void setIsActive(Integer newValue);
    public Integer getIsActive();
    public void setDiscountHighAmount(Double newValue);
    public Double getDiscountHighAmount();
    public void setDiscountHighPercentage(Double newValue);
    public Double getDiscountHighPercentage();
    public void setDiscountLowAmount(Double newValue);
    public Double getDiscountLowAmount();
    public void setDiscountLowPercentage(Double newValue);
    public Double getDiscountLowPercentage();
}    // End of bltNIM3_NIDPromo class definition
