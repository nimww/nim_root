

package com.winstaff;

import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public class dbtCoverageTypeLI extends Object implements InttCoverageTypeLI
{

        db_NewBase    dbnbDB;

    public dbtCoverageTypeLI()
    {
        dbnbDB = new db_NewBase( "tCoverageTypeLI", "CoverageID" );

    }    // End of default constructor

    public dbtCoverageTypeLI( Integer iNewID )
    {
        dbnbDB = new db_NewBase( "tCoverageTypeLI", "CoverageID" );
        setUniqueID( iNewID );
    }    // End of Constructor knowing an ID

    public void commitData() throws SQLException
    {
            //no longer trapping/ignoring errors 2011-03-17
            dbnbDB.commitData();
    }    // End of commitData()

    public void commitDataForced() throws SQLException
    {
        dbnbDB.commitDataForced();
    }    // End of commitData()

    public Integer getUniqueID()
    {
        return dbnbDB.getUniqueID();
    }    // End of getUniqueID()

    public void setUniqueID( Integer iNewID )
    {
        dbnbDB.setUniqueID( iNewID );
    }    // End of setUniqueID()

    public void setModifyComments( String sNew )
    {
        dbnbDB.setModifyComments( sNew );
    }    // End of setUniqueID()


    public void setCoverageID(Integer newValue)
    {
                dbnbDB.setFieldData( "CoverageID", newValue.toString() );
    }

    public Integer getCoverageID()
    {
        String           sValue = dbnbDB.getFieldData( "CoverageID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setField(String fieldN, Object fieldV)
    {DebugLogger.println("setField Method not implemented yet in class!");}

    public String getFieldType(String fieldN)
    {return null;}


    public void setCoverageLong(String newValue)
    {
                dbnbDB.setFieldData( "CoverageLong", newValue.toString() );
    }

    public String getCoverageLong()
    {
        String           sValue = dbnbDB.getFieldData( "CoverageLong" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

}    // End of bltCoverageTypeLI class definition
