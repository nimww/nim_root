package com.winstaff;


import com.winstaff.dbtCountryLI;
import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public interface InttCountryLI extends dbTableInterface
{

    public void setCountryID(Integer newValue);
    public Integer getCountryID();
    public void setCountryCode(String newValue);
    public String getCountryCode();
    public void setCountryLong(String newValue);
    public String getCountryLong();
}    // End of bltCountryLI class definition
