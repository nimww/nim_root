package com.winstaff;

import java.util.List;
 
public class DistanceMatrix {
	List<String> destination_addresses;
	List<String> origin_addresses;
	List<Rows> rows;
	public List<String> getDestination_addresses() {
		return destination_addresses;
	}
	public void setDestination_addresses(List<String> destination_addresses) {
		this.destination_addresses = destination_addresses;
	}
	public List<String> getOrigin_addresses() {
		return origin_addresses;
	}
	public void setOrigin_addresses(List<String> origin_addresses) {
		this.origin_addresses = origin_addresses;
	}
	public List<Rows> getRows() {
		return rows;
	}
	public void setRows(List<Rows> rows) {
		this.rows = rows;
	}
}

class Rows{
	List<Elements> elements;
	public List<Elements> getElements() {
		return elements;
	}
	public void setElements(List<Elements> elements) {
		this.elements = elements;
	}
}

class Elements{
	ElementDetails distance;
	ElementDetails duration;
	String status;
	public ElementDetails getDistance() {
		return distance;
	}
	public void setDistance(ElementDetails distance) {
		this.distance = distance;
	}
	public ElementDetails getDuration() {
		return duration;
	}
	public void setDuration(ElementDetails duration) {
		this.duration = duration;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
}

class ElementDetails{
	String text;
	String value;
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
}

//{
//	   "destination_addresses" : [ "3390 Carmel Mountain Road, San Diego, CA 92121, USA" ],
//	   "origin_addresses" : [ "7415 Andasol Street, San Diego, CA 92126, USA" ],
//	   "rows" : [
//	      {
//	         "elements" : [
//	            {
//	               "distance" : {
//	                  "text" : "10.5 km",
//	                  "value" : 10454
//	               },
//	               "duration" : {
//	                  "text" : "12 mins",
//	                  "value" : 724
//	               },
//	               "status" : "OK"
//	            }
//	         ]
//	      }
//	   ],
//	   "status" : "OK"
//	}