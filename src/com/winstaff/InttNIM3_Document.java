

package com.winstaff;

import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public interface InttNIM3_Document extends dbTableInterface
{

    public void setDocumentID(Integer newValue);
    public Integer getDocumentID();
    public void setUniqueCreateDate(Date newValue);
    public Date getUniqueCreateDate();
    public void setUniqueModifyDate(Date newValue);
    public Date getUniqueModifyDate();
    public void setUniqueModifyComments(String newValue);
    public String getUniqueModifyComments();
    public void setCaseID(Integer newValue);
    public Integer getCaseID();
    public void setReferralID(Integer newValue);
    public Integer getReferralID();
    public void setEncounterID(Integer newValue);
    public Integer getEncounterID();
    public void setServiceID(Integer newValue);
    public Integer getServiceID();
    public void setUploadUserID(Integer newValue);
    public Integer getUploadUserID();
    public void setReferenceDate(Date newValue);
    public Date getReferenceDate();
    public void setDocType(Integer newValue);
    public Integer getDocType();
    public void setDocName(String newValue);
    public String getDocName();
    public void setFileName(String newValue);
    public String getFileName();
    public void setDCMFile(String newValue);
    public String getDCMFile();
    public void setReportSummary(String newValue);
    public String getReportSummary();
    public void setReportText(String newValue);
    public String getReportText();
    public void setReportDictationFile(String newValue);
    public String getReportDictationFile();
    public void setTreatingPhysician(String newValue);
    public String getTreatingPhysician();
    public void setDiagnosticPacs(String newValue);
    public String getDiagnosticPacs();
    public void setEMR(String newValue);
    public String getEMR();
    public void setReferringPhysician(String newValue);
    public String getReferringPhysician();
    public void setComments(String newValue);
    public String getComments();
}    // End of bltNIM3_Document class definition
