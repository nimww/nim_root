

package com.winstaff;

import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public class dbtGenderLI extends Object implements InttGenderLI
{

        db_NewBase    dbnbDB;

    public dbtGenderLI()
    {
        dbnbDB = new db_NewBase( "tGenderLI", "GenderID" );

    }    // End of default constructor

    public dbtGenderLI( Integer iNewID )
    {
        dbnbDB = new db_NewBase( "tGenderLI", "GenderID" );
        setUniqueID( iNewID );
    }    // End of Constructor knowing an ID

    public void commitData() throws SQLException
    {
            //no longer trapping/ignoring errors 2011-03-17
            dbnbDB.commitData();
    }    // End of commitData()

    public void commitDataForced() throws SQLException
    {
        dbnbDB.commitDataForced();
    }    // End of commitData()

    public Integer getUniqueID()
    {
        return dbnbDB.getUniqueID();
    }    // End of getUniqueID()

    public void setUniqueID( Integer iNewID )
    {
        dbnbDB.setUniqueID( iNewID );
    }    // End of setUniqueID()

    public void setModifyComments( String sNew )
    {
        dbnbDB.setModifyComments( sNew );
    }    // End of setUniqueID()


    public void setGenderID(Integer newValue)
    {
                dbnbDB.setFieldData( "GenderID", newValue.toString() );
    }

    public Integer getGenderID()
    {
        String           sValue = dbnbDB.getFieldData( "GenderID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setField(String fieldN, Object fieldV)
    {DebugLogger.println("setField Method not implemented yet in class!");}

    public String getFieldType(String fieldN)
    {return null;}


    public void setGenderShort(String newValue)
    {
                dbnbDB.setFieldData( "GenderShort", newValue.toString() );
    }

    public String getGenderShort()
    {
        String           sValue = dbnbDB.getFieldData( "GenderShort" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setGenderLong(String newValue)
    {
                dbnbDB.setFieldData( "GenderLong", newValue.toString() );
    }

    public String getGenderLong()
    {
        String           sValue = dbnbDB.getFieldData( "GenderLong" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

}    // End of bltGenderLI class definition
