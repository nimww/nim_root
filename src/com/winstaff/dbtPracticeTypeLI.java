

package com.winstaff;

import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public class dbtPracticeTypeLI extends Object implements InttPracticeTypeLI
{

        db_NewBase    dbnbDB;

    public dbtPracticeTypeLI()
    {
        dbnbDB = new db_NewBase( "tPracticeTypeLI", "PracticeTypeID" );

    }    // End of default constructor

    public dbtPracticeTypeLI( Integer iNewID )
    {
        dbnbDB = new db_NewBase( "tPracticeTypeLI", "PracticeTypeID" );
        setUniqueID( iNewID );
    }    // End of Constructor knowing an ID

    public void commitData() throws SQLException
    {
            //no longer trapping/ignoring errors 2011-03-17
            dbnbDB.commitData();
    }    // End of commitData()

    public void commitDataForced() throws SQLException
    {
        dbnbDB.commitDataForced();
    }    // End of commitData()

    public Integer getUniqueID()
    {
        return dbnbDB.getUniqueID();
    }    // End of getUniqueID()

    public void setUniqueID( Integer iNewID )
    {
        dbnbDB.setUniqueID( iNewID );
    }    // End of setUniqueID()

    public void setModifyComments( String sNew )
    {
        dbnbDB.setModifyComments( sNew );
    }    // End of setUniqueID()


    public void setPracticeTypeID(Integer newValue)
    {
                dbnbDB.setFieldData( "PracticeTypeID", newValue.toString() );
    }

    public Integer getPracticeTypeID()
    {
        String           sValue = dbnbDB.getFieldData( "PracticeTypeID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setField(String fieldN, Object fieldV)
    {DebugLogger.println("setField Method not implemented yet in class!");}

    public String getFieldType(String fieldN)
    {return null;}


    public void setPracticeTypeLong(String newValue)
    {
                dbnbDB.setFieldData( "PracticeTypeLong", newValue.toString() );
    }

    public String getPracticeTypeLong()
    {
        String           sValue = dbnbDB.getFieldData( "PracticeTypeLong" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

}    // End of bltPracticeTypeLI class definition
