package com.winstaff;
import org.joda.time.DateMidnight;
import org.joda.time.Hours;

import java.awt.*;
import java.text.DecimalFormat;
import java.util.Calendar;


public class PLCUtils
{

	public PLCUtils()
	{
	}

    public static String toProperCase(String name, boolean allWords) {
        if (allWords)    {
            return org.apache.commons.lang3.text.WordUtils.capitalizeFully(name);
        } else {
            return org.apache.commons.lang3.text.WordUtils.capitalize(name);
        }

  
    }

    public static String getDisplayDefaultDecimalFormat2(Double myD)
    {
        java.text.DecimalFormat myDF2 = new DecimalFormat(String_defDecFormat2);
        try
        {
            return myDF2.format(myD);
        }
        catch(Exception e)
        {
            DebugLogger.printLine("PLCUtils:getDisplayDefaultDecimalFormat2:[" + e+"]");
            return "0.00";
        }
    }

    public static String getDisplayDefaultDecimalFormatCurrency(Double myD)
    {
        java.text.DecimalFormat myDF2 = new DecimalFormat(String_defDecFormatCurrency2);
        try
        {
            return myDF2.format(myD);
        }
        catch(Exception e)
        {
            DebugLogger.printLine("PLCUtils:getDisplayDefaultDecimalFormatCurrency:[" + e+"]");
            return "0.00";
        }
    }

    public static String getDisplayDefaultDecimalFormatCurrency1(Double myD)
    {
        java.text.DecimalFormat myDF2 = new DecimalFormat(String_defDecFormatCurrency1);
        try
        {
            return myDF2.format(myD);
        }
        catch(Exception e)
        {
            DebugLogger.printLine("PLCUtils:getDisplayDefaultDecimalFormatCurrency1:[" + e+"]");
            return "0.00";
        }
    }

    public static String getDisplayDateWithTime(java.util.Date myDate)
    {
        return  getDisplayDate(myDate, true);
    }

    public static String getDisplayDateWithoutTime(java.util.Date myDate)
    {
        return  getDisplayDate(myDate, false);
    }

    public static java.util.Date getParseDBDFT(String myIn)
    {
        if (myIn==null||myIn.equalsIgnoreCase(""))
        {
            myIn ="1800-01-01";
        }
        try
        {
            java.text.SimpleDateFormat dbdft = new java.text.SimpleDateFormat(String_dbdft);
            return dbdft.parse(myIn);
        }
        catch (Exception e)
        {
            return null;
        }
    }

    public static String getDisplayDate(java.util.Date myDate, boolean isIncludeTime)
    {
        return getDisplayDate(myDate,isIncludeTime,PLCUtils.DATE_FULLDATE);
    }

    public static String getDisplayDate(java.util.Date myDate, boolean isIncludeTime, int iDatePart)
    {
        java.text.SimpleDateFormat dbdft = new java.text.SimpleDateFormat(String_dbdft);
        java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(String_dbdf);
        java.text.SimpleDateFormat displayDateSDF2Full = new java.text.SimpleDateFormat(String_displayDateSDF2Full);
        java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat(String_displayDateSDF1);
        java.text.SimpleDateFormat displayDate_Month = new java.text.SimpleDateFormat(String_displayDateMonth2);
        java.text.SimpleDateFormat displayDate_Day = new java.text.SimpleDateFormat(String_displayDateDay);
        java.text.SimpleDateFormat displayDate_Year = new java.text.SimpleDateFormat(String_displayDateYear);
        String myVal = "";
        String CurrentSelection = dbdft.format(myDate);
        if (CurrentSelection==null||CurrentSelection.equalsIgnoreCase(""))
        {
            CurrentSelection ="1800-01-01";
        }
        if (CurrentSelection.equalsIgnoreCase("1800-01-01")||CurrentSelection.equalsIgnoreCase("1800-01-01 00:00:00") )
        {
            myVal = ("");
        }
        else if (CurrentSelection.equalsIgnoreCase("1800-01-02")||CurrentSelection.equalsIgnoreCase("1800-01-02 00:00:00") )
        {
            myVal = ("Current");
        }
        else if (CurrentSelection.equalsIgnoreCase("1800-01-03")||CurrentSelection.equalsIgnoreCase("1800-01-03 00:00:00") )
        {
            myVal = ("NA");
        }
        else if (isIncludeTime)
        {
            try
            {
                if (iDatePart==PLCUtils.DATE_FULLDATE)
                {
                    myVal = (displayDateSDF2Full.format(dbdft.parse(CurrentSelection)));
                }
                else if (iDatePart==PLCUtils.DATE_MONTH)
                {
                    myVal = (displayDate_Month.format(myDate));
                }
                else if (iDatePart==PLCUtils.DATE_DAY)
                {
                    myVal = (displayDate_Day.format(myDate));
                }
                else if (iDatePart==PLCUtils.DATE_YEAR)
                {
                    myVal = (displayDate_Year.format(myDate));
                }
            }
            catch (Exception e)
            {
                try
                {
                    if (iDatePart==PLCUtils.DATE_FULLDATE)
                    {
                        myVal = (displayDateSDF2Full.format(dbdf.parse(CurrentSelection)));
                    }
                    else if (iDatePart==PLCUtils.DATE_MONTH)
                    {
                        myVal = (displayDate_Month.format(myDate));
                    }
                    else if (iDatePart==PLCUtils.DATE_DAY)
                    {
                        myVal = (displayDate_Day.format(myDate));
                    }
                    else if (iDatePart==PLCUtils.DATE_YEAR)
                    {
                        myVal = (displayDate_Year.format(myDate));
                    }
                }
                catch (Exception e2)
                {
                    myVal = "Error";
                }
            }
        }
        else
        {
            try
            {
                if (iDatePart==PLCUtils.DATE_FULLDATE)
                {
                    myVal = (displayDateSDF1.format(dbdf.parse(CurrentSelection)));
                }
                else if (iDatePart==PLCUtils.DATE_MONTH)
                {
                    myVal = (displayDate_Month.format(myDate));
                }
                else if (iDatePart==PLCUtils.DATE_DAY)
                {
                    myVal = (displayDate_Day.format(myDate));
                }
                else if (iDatePart==PLCUtils.DATE_YEAR)
                {
                    myVal = (displayDate_Year.format(myDate));
                }
            }
            catch (Exception e)
            {
                myVal = "Error";
            }
        }
        return myVal;
    }



    public static String StripDashes(String mySSNs)
	{
	    while (mySSNs.indexOf("-")>=0)
	    {
	        int myIndex = mySSNs.indexOf("-");
	        mySSNs = mySSNs.substring(0,myIndex) + mySSNs.substring(myIndex+1, mySSNs.length());
	    }
	    return mySSNs;
	}

    public static Double getDecimalDefault(String inS)
    {
        return getDecimal(inS,false)   ;
    }

    public static Double getDecimal(String inS, boolean allowAny)
    {
        Double myD = new Double(0.00);
        if (!inS.equalsIgnoreCase(""))
        {
            java.text.DecimalFormat myDF = new DecimalFormat(String_defDecFormat1);
            java.text.DecimalFormat myDF2 = new DecimalFormat(String_defDecFormat2);
            java.text.DecimalFormat defDecFormatCurrency1 = new DecimalFormat(String_defDecFormatCurrency1);
            if (!allowAny)
            {
                try
                {
                    myD = new Double(myDF.parse(inS,new java.text.ParsePosition(0)).toString());
                }
                catch (Exception e)
                {
                    try
                    {
                        myD = new Double(myDF2.parse(inS,new java.text.ParsePosition(0)).toString());
                    }
                    catch (Exception e2)
                    {
                        myD = new Double(0.00);
                        DebugLogger.println("PLCUtils:getDecimal:"+e2);
                    }
                }
            }
            else
            {
                try//standard
                {
                    myD = new Double(myDF.parse(inS,new java.text.ParsePosition(0)).toString());
                }
                catch (Exception e)
                {
                    try//currency format
                    {
                        myD = new Double(defDecFormatCurrency1.parse(inS,new java.text.ParsePosition(0)).toString());
                    }
                    catch (Exception e2)
                    {
                        myD = new Double(0.00);
                    }
                }
            }
        }

        return myD;
    }

    public static Integer getDaysTill(java.util.Date date)
    {
        Integer myVal = null;
        //org.joda.time.DateTime myJDAte = new org.joda.time.DateTime(date);
        org.joda.time.DateTime endDate = new  org.joda.time.DateTime(date);
        org.joda.time.DateTime startDate = new  org.joda.time.DateTime();
        org.joda.time.Days d =  org.joda.time.Days.daysBetween(startDate, endDate);
        myVal = d.getDays();
        return myVal;
    }


    public static String getDaysTill_Display(java.util.Date date, boolean isSimplify)
    {
        Integer days = getDaysTill(date);
        String myVal = "";
        if (days==null)
        {
            myVal = "Error (invalid days)";
        }
        else if (isSimplify)
        {
            if (days==0)
            {
                myVal = "Within the last day";
            }
            else if (days==1)
            {
                myVal = "Tomorrow";
            }
            else if (days>0&&days<14)
            {
                myVal ="In " + days + " days";
            }
            else if (days>14)
            {
                myVal ="In 2+ weeks";
            }
            else if (days<0&&days>-14)
            {
                myVal ="" + -days + " days ago";
            }
            else if (days<-14)
            {
                myVal ="2+ weeks ago";
            }
        }
        else
        {
            if (days>=0)
            {
                myVal ="In " + days + " days";
            }
            else
            {
                myVal ="" + -days + " days ago";
            }
        }
        return myVal;
    }

    public static Integer getHoursTill(java.util.Date date)
    {
        Integer myVal = null;
        //org.joda.time.DateTime myJDAte = new org.joda.time.DateTime(date);
        org.joda.time.DateTime endDate = new  org.joda.time.DateTime(date);
        org.joda.time.DateTime startDate = new  org.joda.time.DateTime();
        Hours h =  org.joda.time.Hours.hoursBetween(startDate, endDate);
        myVal = h.getHours();
        return myVal;
    }


    public static String getHoursTill_Display(java.util.Date date, boolean isSimplify)
    {
        Integer hours = getHoursTill(date);
        String myVal = "";
        if (hours==null)
        {
            myVal = "Error (invalid hours)";
        }
        else if (isSimplify)
        {
            if (hours==0)
            {
                myVal = "Within the hour";
            }
            else if (hours>0&&hours<72)
            {
                myVal ="In " + hours + " hours";
            }
            else if (hours>72)
            {
                myVal ="in 3+ days";
            }
            else if (hours<0&&hours>-72)
            {
                myVal ="" + -hours + " hours ago";
            }
            else if (hours<-72)
            {
                myVal ="3+ days ago";
            }
        }
        else
        {
            if (hours==0)
            {
                myVal = "Within the hour";
            }
            else if (hours>0)
            {
                myVal ="In " + hours + " hours";
            }
            else
            {
                myVal ="" + -hours + " hours ago";
            }
        }
        return myVal;
    }




    public static boolean isToday(java.util.Date date)
    {
        org.joda.time.DateTime jDate = new org.joda.time.DateTime(date);
        org.joda.time.DateTime now = new org.joda.time.DateTime();
        return (jDate.getYear() == now.getYear() &&
                jDate.getMonthOfYear () == now.getMonthOfYear() &&
                jDate.getDayOfMonth() == now.getDayOfMonth());
    }

    public static boolean isTomorrow(java.util.Date date)
    {
        org.joda.time.DateTime myJDAte = new org.joda.time.DateTime(date);
        return isToday(myJDAte.minusDays(1).toDate());
    }



    public static java.util.Date getYesterday()
    {
         return getDayModify(PLCUtils.getNowDate(false),-1);
    }


    public static java.util.Date getToday()
    {
         return (PLCUtils.getNowDate(false));
    }

    public static java.util.Date getDayModify(java.util.Date date, int iDays)
    {
         java.util.Calendar cal = java.util.Calendar.getInstance();
         cal.setTime(date);
         cal.add(java.util.Calendar.DATE, iDays);
         return cal.getTime();
    }

    public static java.util.Date getDateNewHour(java.util.Date theDate, int hour)
    {
        Calendar cal = java.util.Calendar.getInstance();
        cal.setTime(theDate);
        cal.set(Calendar.HOUR_OF_DAY, hour);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }



    public static java.util.Calendar getNow()
    {
        return PLCUtils.getNow(false);
    }

    public static java.util.Calendar getNow(boolean ZeroOutSeconds)
    {
        java.util.Calendar cal = java.util.Calendar.getInstance();
        cal.setTime(new java.util.Date());
        if (ZeroOutSeconds)
        {
            cal.set(java.util.Calendar.SECOND, 0);
            cal.set(java.util.Calendar.MILLISECOND, 0);
        }
        return cal;
    }

    public static Double DoubleRound_Dollars(Double myDouble)
    {
        double myVal = Math.round(myDouble*100)/100.00;
        return myVal;
    }

    public static java.util.Date getNowDate(boolean ZeroOutSeconds)
    {
        return PLCUtils.getNow(ZeroOutSeconds).getTime();
    }

    public static String getDBDFT_AsString(java.util.Date myD)
    {
        String myVal = "error";
        try
        {
            java.text.SimpleDateFormat dbdft = new java.text.SimpleDateFormat(String_dbdft);
            myVal = dbdft.format(myD);
        }
        catch (Exception e)
        {
            DebugLogger.printLine("PLCUtils:getDBDFT_AsString:[" + e+"]");
        }

        return myVal;

    }

    public static java.util.Date getSubDate(String inputDate)
    {
        java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat(String_displayDateSDF1);
        java.text.SimpleDateFormat displayDateSDF2Full = new java.text.SimpleDateFormat(String_displayDateSDF2Full);
        java.text.SimpleDateFormat displayDateSDF3Full = new java.text.SimpleDateFormat(String_displayDateSDF3Full);
       java.util.Date testObj = null;
       try
       {
           testObj = (displayDateSDF2Full.parse(inputDate)) ;
       }
       catch(Exception sdfull333)
       {
        try
        {
            testObj = (displayDateSDF3Full.parse(inputDate)) ;
        }
        catch(Exception sdfull4)
        {
            try
            {
                testObj = (displayDateSDF1.parse(inputDate)) ;
            }
            catch(Exception sdf1)
            {
                if (inputDate.length()>=3&&inputDate.equalsIgnoreCase("tam"))
                {
                    java.util.Calendar cal = PLCUtils.getNow(true);
                    cal.add(java.util.Calendar.DAY_OF_YEAR, 1); // <--
                    cal.set(java.util.Calendar.HOUR_OF_DAY, 7);
                    cal.set(java.util.Calendar.MINUTE, 0);
                    testObj = cal.getTime();
                }
                else if (inputDate.length()>=3&&inputDate.substring(0,2).equalsIgnoreCase("h+"))
                {
                    try
                    {
                        Integer myAddHours = new Integer(inputDate.substring(2,3));
                        java.util.Calendar cal = PLCUtils.getNow(true);
                        cal.add(java.util.Calendar.HOUR_OF_DAY, myAddHours); // <--
                        testObj = cal.getTime();
                    }
                    catch(Exception etplus)
                    {
                        //do nothing
                    }

                    //System.out.println(tomorrow);
                }
                else if (inputDate.length()>=2&&inputDate.substring(0,2).equalsIgnoreCase("h+"))
                {
                    java.util.Calendar cal = PLCUtils.getNow(true);
                    cal.add(java.util.Calendar.HOUR_OF_DAY, 1); // <--
                    testObj = cal.getTime();
                    //System.out.println(tomorrow);
                }
                else if (inputDate.length()>=3&&inputDate.substring(0,2).equalsIgnoreCase("h-"))
                {
                    try
                    {
                        Integer myAddHours = new Integer(inputDate.substring(2,3));
                        java.util.Calendar cal = PLCUtils.getNow(true);
                        cal.add(java.util.Calendar.HOUR_OF_DAY, -myAddHours); // <--
                        testObj = cal.getTime();
                    }
                    catch(Exception etplus)
                    {
                        //do nothing
                    }

                    //System.out.println(tomorrow);
                }
                else if (inputDate.length()>=2&&inputDate.substring(0,2).equalsIgnoreCase("h-"))
                {
                    java.util.Calendar cal = PLCUtils.getNow(true);
                    cal.add(java.util.Calendar.HOUR_OF_DAY, -1); // <--
                    testObj = cal.getTime();
                    //System.out.println(tomorrow);
                }
                else if (inputDate.length()>=3&&inputDate.substring(0,2).equalsIgnoreCase("t+"))
                {
                    try
                    {
                        Integer myAddDays = new Integer(inputDate.substring(2,3));
                        java.util.Calendar cal = PLCUtils.getNow(true);
                        cal.add(java.util.Calendar.DAY_OF_YEAR, myAddDays); // <--
                        testObj = cal.getTime();
                    }
                    catch(Exception etplus)
                    {
                        //do nothing
                    }

                    //System.out.println(tomorrow);
                }
                else if (inputDate.length()>=2&&inputDate.substring(0,2).equalsIgnoreCase("t+"))
                {
                    java.util.Calendar cal = PLCUtils.getNow(true);
                    cal.add(java.util.Calendar.DAY_OF_YEAR, 1); // <--
                    testObj = cal.getTime();
                    //System.out.println(tomorrow);
                }
                else if (inputDate.length()>=3&&inputDate.substring(0,2).equalsIgnoreCase("t-"))
                {
                    try
                    {
                        Integer myAddDays = new Integer(inputDate.substring(2,3));
                        java.util.Calendar cal = PLCUtils.getNow(true);
                        cal.add(java.util.Calendar.DAY_OF_YEAR, -myAddDays); // <--
                        testObj = cal.getTime();
                    }
                    catch(Exception etplus)
                    {
                        //do nothing
                    }

                    //System.out.println(tomorrow);
                }
                else if (inputDate.length()>=2&&inputDate.substring(0,2).equalsIgnoreCase("t-"))
                {
                    java.util.Calendar cal = PLCUtils.getNow(true);
                    cal.add(java.util.Calendar.DAY_OF_YEAR, -1); // <--
                    testObj = cal.getTime();
                    //System.out.println(tomorrow);
                }
                else if (inputDate.equalsIgnoreCase("today")||inputDate.equalsIgnoreCase("t"))
                {
                    java.util.Calendar cal = PLCUtils.getNow(true);
                    testObj = cal.getTime();
                }
                else if (inputDate.equalsIgnoreCase("current")||inputDate.equalsIgnoreCase("present"))
                {
                    try
                    {
                        testObj = displayDateSDF1.parse("01/02/1800") ;
                    }
                    catch (Exception e)
                    {
                        //do nothing
                    }
                }
                else if (inputDate.equalsIgnoreCase("na")||inputDate.equalsIgnoreCase("n/a"))
                {
                    try
                    {
                        testObj = displayDateSDF1.parse("01/03/1800") ;
                    }
                    catch (Exception e)
                    {
                        //do nothing
                    }
                }
                else
                {
                    try
                    {
                        testObj = displayDateSDF1.parse("01/01/1800") ;
                    }
                    catch (Exception e)
                    {
                        //do nothing
                    }
                }
            }
        }
       }
       try
       {
//	       if (testObj.before(displayDateSDF1.parse("01/01/1800"))  )
	       if (    testObj.before(displayDateSDF1.parse("01/01/1800")) || testObj.after(displayDateSDF1.parse("01/01/2500")) )
	       {
	             testObj = displayDateSDF1.parse("01/01/1800") ;
	       }
       }
       catch (Exception e) 
       {
                        //do nothing
       }
       return testObj;
    }

/*
   public static java.text.DecimalFormat defDecFormat1 = new java.text.DecimalFormat("###,###,###,###,###,##0.00##");
   public static java.text.DecimalFormat defDecFormat2 = new java.text.DecimalFormat("#################0.00##");
   public static java.text.DecimalFormat defDecFormatCurrency1 = new java.text.DecimalFormat("$###,###,###,###,##0.00##");
   public static java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat("MM/dd/yyyy");
   public static java.text.SimpleDateFormat displayDateSDF2 = new java.text.SimpleDateFormat("MM-dd-yyyy");
   public static java.text.SimpleDateFormat displayDateSDF3 = new java.text.SimpleDateFormat("MMddyyyy");
   public static java.text.SimpleDateFormat displayDateSDF4 = new java.text.SimpleDateFormat("MM/dd/yy");
   //public static java.text.SimpleDateFormat displayDateSDF5 = new java.text.SimpleDateFormat("MMddyyyy");
   public static java.text.SimpleDateFormat displayDateSDF1Full = new java.text.SimpleDateFormat("MM/dd/yyyy hh:mm:ss a");
   public static java.text.SimpleDateFormat displayDateSDF2Full = new java.text.SimpleDateFormat("MM/dd/yyyy hh:mm a");
   public static java.text.SimpleDateFormat displayDateSDF3Full = new java.text.SimpleDateFormat("MM/dd/yyyy HH:mm");
   public static java.text.SimpleDateFormat displayDateDayWeek = new java.text.SimpleDateFormat("EEEE");
   public static java.text.SimpleDateFormat displayDateDay = new java.text.SimpleDateFormat("dd");
   public static java.text.SimpleDateFormat displayDateMonth = new java.text.SimpleDateFormat("MMMM");
   public static java.text.SimpleDateFormat displayDateYear = new java.text.SimpleDateFormat("yyyy");
   public static java.text.SimpleDateFormat displayDateHour = new java.text.SimpleDateFormat("h a");
   public static java.text.SimpleDateFormat displayDateHourMin = new java.text.SimpleDateFormat("h:mm a");

   public static java.text.SimpleDateFormat dbdft = PLCUtils.getDBDFT(new Integer(1));
   public static java.text.SimpleDateFormat dbdf = PLCUtils.getDBDF();
   //public static java.text.SimpleDateFormat displayDateTimeSDF = displayDateSDF2Full;

*/


    public static String String_defDecFormat1 = "###,###,###,###,###,##0.00##";
    public static String String_defDecFormat2  = "#################0.00##";
    public static String String_defDecFormatCurrency1  = "$###,###,###,###,##0.00";
    public static String String_defDecFormatCurrency2  = "###,###,###,###,##0.00";
    public static String String_displayDateSDF1 = "MM/dd/yyyy";
    public static String String_displayDateSDF1Full = "MM/dd/yyyy hh:mm:ss a";
    public static String String_displayDateSDF2Full = "MM/dd/yyyy hh:mm a";
    public static String String_displayDateSDF3Full = "MM/dd/yyyy HH:mm";
    public static String String_displayDateDayWeek = "EEEE";
    public static String String_displayDateDay = "dd";
    public static String String_displayDateMonth = "MMMM";
    public static String String_displayDateMonth2 = "MM";
    public static String String_displayDateYear = "yyyy";
    public static String String_displayDateHour = "h a";
    public static String String_displayDateHourMin = "h:mm a";

    public static String String_dbdft = ConfigurationInformation.sDateTimeFormat;
    public static String String_dbdf = ConfigurationInformation.sDateTimeFormat;
    //public static String displayDateTimeSDF = displayDateSDF2Full;

    public static int DATE_FULLDATE = 1;
    public static int DATE_MONTH = 2;
    public static int DATE_YEAR = 3;
    public static int DATE_DAY = 4;



	//{{DECLARE_CONTROLS
	//}}
}
