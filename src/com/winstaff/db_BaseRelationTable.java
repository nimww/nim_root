package com.winstaff;
/*
    *
    *
    *
 * db_BaseLookupTable.java
 *
 * This base class used to build lookup table classes.
 *
 * Created on March 30, 2001, 1:38 PM
 */
import java.sql.*;
import java.util.Vector;
import java.util.Enumeration;

/**
 *
 * @author      Michael Million
 * @version     1.00
 */
public abstract class db_BaseRelationTable
{
    // Attribute information
    protected   Vector          vList             =       new Vector();
    
    // Identifier, sometimes unique, to retrieve records.
    //private Integer         iUniqueID;
    private     String          sTableName;
    protected   String          sLinkFieldName;
    
    protected   String          sUniqueIDFieldName;
    private     String          sRelationKeyFieldName;
    private     Integer         iRelationUniqueID;
    
    //
    private     String          sExtraWhere;
    private     String          sSortBy;

    // Class attribute stuff
    private String          sClassIdentifier;
    
    // Database stuff
    private Connection      cCon;

    /** Constructs new db_BaseLookupTable
     * @param sTableName String Name of table to set/get information from
     * @param sKeyFieldName String Comma delimited list of fields to set/get
    */
    public db_BaseRelationTable (String sTableName, String sLinkFieldName, String sUniqueIDFieldName, String sRelationKeyFieldName, Integer iRelationUniqueID )
    {
        loadDefaults( sTableName, sLinkFieldName, sUniqueIDFieldName, sRelationKeyFieldName, iRelationUniqueID, "", "" );
        
        // Initialize and load data
        init();
    }   // End of default constructor

    public db_BaseRelationTable (String sTableName, String sLinkFieldName, String sUniqueIDFieldName, String sRelationKeyFieldName, Integer iRelationUniqueID, String sExtraWhere, String sSortBy )
    {
        loadDefaults( sTableName, sLinkFieldName, sUniqueIDFieldName, sRelationKeyFieldName, iRelationUniqueID, sExtraWhere, sSortBy );
        
        // Initialize and load data
        init();
    }   // End of default constructor

    
    private void loadDefaults( String sTableName, String sLinkFieldName, String sUniqueIDFieldName, String sRelationKeyFieldName, Integer iRelationUniqueID, String sExtraWhere, String sSortBy )
    {
        this.sTableName             = sTableName;
        this.sLinkFieldName         = sLinkFieldName;
        this.sUniqueIDFieldName     = sUniqueIDFieldName;
        this.sRelationKeyFieldName  = sRelationKeyFieldName;
        this.iRelationUniqueID      = iRelationUniqueID;
        this.sExtraWhere            = sExtraWhere;
        this.sSortBy                = sSortBy;
    }   // End of loadDefaults()
    
    private void init()
    {
        String  sDebugString = "";
        
        this.sClassIdentifier =  this.getClass().getName();
        
        try
        {
            sDebugString = ".forName";
            Class.forName( ConfigurationInformation.sJDBCDriverName );
        }
        catch ( Exception e )
        {
            DebugLogger.println ( sClassIdentifier + ": in db_Base Constuctor Error: " + e.toString () + "\n" + sDebugString );
        }
        
        loadFromDatabase();
    }    
    
    /** Return enumeration of projects
    * @return Enumeration Enumeration of Projects
    */    
    public Enumeration elements()
    {
        return new VectorEnumeration( vList );
    }   // End of elements()
    
    /** Method to be defined for each child class.  It is used to initialize and invoke the SQL query.
    */    
    protected final void loadFromDatabase ()
    {
        String  sDebugString = "";
        String  sSQLString;
        
        try
        {
            sDebugString = ".getConnection";
            cCon = DriverManager.getConnection( ConfigurationInformation.sURL );

            sDebugString = ".createStatement";
            Statement   stmt = cCon.createStatement();
            
            sDebugString = ".executeQuery";
            sSQLString = "select " + sLinkFieldName + ", " + sUniqueIDFieldName + " from " + sTableName + " where (" + sRelationKeyFieldName + "=" + iRelationUniqueID + ")";
            
            if ( sExtraWhere.length() > 0 )
            {
                sSQLString += " and ( " + sExtraWhere + ")";
            }
            
            if ( sSortBy.length() > 0 )
            {
                sSQLString += " order by " + sSortBy;
            }
            

            if ( ConfigurationInformation.bInDebugMode )
            {
                DebugLogger.println( "Query: " + sSQLString );
            }
            
            ResultSet rs = stmt.executeQuery( sSQLString );
             
            // Via polymorphism, allow class to load member attributes.  Note this could be a list of records.
            transferData( rs );

            sDebugString = "resultset close";
            rs.close();
            
            sDebugString = "statement close";
            stmt.close();
            
            sDebugString = "connection close";
            cCon.close ();
        }
        catch( Exception e )
        {
            DebugLogger.println(  sClassIdentifier + ": " + e.toString() + "\n" + sDebugString );
        }
        finally
        {
    	    try
            {
    	        cCon.close();
            }
            catch (SQLException SQLEx)
            {
                DebugLogger.println(  sClassIdentifier + ": Finally " + SQLEx.toString() + "\n" + sDebugString );
    	    }
        }


    }   // End of loadFromDatabase()

    /*
    * Abstract method to pull information from the result set and load into class attributes.
    * @param sFieldList Comma delimited list of fields for which data has been retrieved.
    * @param rs Result set from which to pull the data from.
    * @throws SQLException Passes on SQLException
    */    
    protected abstract void transferData( ResultSet rs )  throws SQLException;
}   // End of db_BaseLookupTable class definition
