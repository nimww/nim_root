package com.winstaff;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;

public class runVmEncounterNoVoidBpPlusChecks{
	
	public static void main(String[]args) throws IOException, SQLException{
		String query = "SELECT * FROM \"vMQ4_Encounter_NoVoid_BP_Plus_Checks\"";
		searchDB2 ss = new searchDB2();
		java.sql.ResultSet rs = ss.executeStatement(query);
		getMonthlyNumbers(rs);
		ss.closeAll();
	}
	public static void getMonthlyNumbers(ResultSet rs) throws SQLException{
		try {
				PrintWriter writer = new PrintWriter(new FileWriter("export/vMQ4_Encounter_NoVoid_Plus_Checks.csv"));
				ResultSetMetaData meta = rs.getMetaData();
				int numOfColumns = meta.getColumnCount();
				//String myHeaders = "\"" + meta.getColumnName(1) + "\"";
				String myHeaders = meta.getColumnName(1);
				for(int i = 2; i < numOfColumns + 1; i++){
					myHeaders += "," +  meta.getColumnName(i);
				}
				writer.println(myHeaders);
				while(rs.next()){
					String row = rs.getString(1);
					for(int i = 2; i < numOfColumns + 1; i++){
						row += ",\"" + rs.getString(i) + "\"";
					}
					writer.println(row);
				}
				writer.close();
			}catch (IOException e) {
				e.printStackTrace();
			}
	}
}