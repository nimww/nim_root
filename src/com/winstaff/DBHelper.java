package com.winstaff;

/**
 * Created by IntelliJ IDEA.
 * User: ste
 * Date: 5/26/11
 * Time: 7:02 PM
 * To change this template use File | Settings | File Templates.
 */
public class DBHelper
{
   public static String getSQL_Strip(String myField)
   {
       return "upper(regexp_replace(" + myField + ", E'(\\#)|(\\$)|(\\:)|(\\;)|(\\,)|(\\-)|(\\_)|(\\\\s)', '','g'))";
   }

    public static String getLevenshtein_Stripped(String myField1,String myField2)
    {
        return "levenshtein(" + DBHelper.getSQL_Strip(myField1) + "," + DBHelper.getSQL_Strip(myField2) + ")";
    }

    public static String getTrigramSimilarity_Stripped(String myField1,String myField2)
    {
        return "(CASE WHEN similarity(" + DBHelper.getSQL_Strip(myField1) + "," + DBHelper.getSQL_Strip(myField2) + ") = 'NaN' THEN 0 ELSE similarity(" + DBHelper.getSQL_Strip(myField1) + "," + DBHelper.getSQL_Strip(myField2) + ") END) ";
    }

}
