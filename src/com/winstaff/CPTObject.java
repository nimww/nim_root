package com.winstaff;

/**
 * Created by IntelliJ IDEA.
 * User: ste
 * Date: 6/15/11
 * Time: 2:42 PM
 * To change this template use File | Settings | File Templates.
 */
public class CPTObject
{
    public CPTObject(String CPT_Code, String CPT_Modifier)
    {
        this.CPT_Code = CPT_Code;
        this.CPT_Modifier = CPT_Modifier;
    }

    public CPTObject(String CPT_Code, String CPT_Modifier, int CPT_Qty)
    {
        this.CPT_Code = CPT_Code;
        this.CPT_Modifier = CPT_Modifier;
        this.CPT_Qty = CPT_Qty;
    }

    public CPTObject(String CPT_Code, String CPT_Modifier, int CPT_Qty, String CPT_Description) {
        this.CPT_Code = CPT_Code;
        this.CPT_Modifier = CPT_Modifier;
        this.CPT_Qty = CPT_Qty;
        this.CPT_Description = CPT_Description;
    }

    private String CPT_Code = null;
    private String CPT_Modifier = null;
    private int CPT_Qty = 1;

    private String CPT_Description = "";

    public String getCPT_Code()
    {
        return CPT_Code;
    }

    public String getCPT_Modifier() {
        return CPT_Modifier;
    }

    public void setCPT_Modifier(String CPT_Modifier) {
        this.CPT_Modifier = CPT_Modifier;
    }

    public void setCPT_Code(String CPT_Code)
    {

        this.CPT_Code = CPT_Code;
    }

    public int getCPT_Qty() {
        return CPT_Qty;
    }

    public void setCPT_Qty(int CPT_Qty) {
        this.CPT_Qty = CPT_Qty;
    }

    public String getCPT_Description() {
        return CPT_Description;
    }

    public void setCPT_Description(String CPT_Description) {
        this.CPT_Description = CPT_Description;
    }
}