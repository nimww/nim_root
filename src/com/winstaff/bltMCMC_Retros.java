

package com.winstaff;

import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;
import java.util.Enumeration;

public class bltMCMC_Retros extends Object implements InttMCMC_Retros
{

    dbtMCMC_Retros    dbDB;

    public bltMCMC_Retros()
    {
        dbDB = new dbtMCMC_Retros();
        dbDB.setUniqueCreateDate(new java.util.Date());
        makeRequiredHash();
        makeExpiredHash();
        makeEnglishHash();

    }    // End of default constructor

    public bltMCMC_Retros( Integer iNewID )
    {        dbDB = new dbtMCMC_Retros( iNewID );
        makeRequiredHash();
        makeExpiredHash();
        makeEnglishHash();
        iSecurityCheck = new Integer(2);
    }    // End of Constructor knowing an ID


    public bltMCMC_Retros( Integer iNewID, Integer iGroupSecurityID )
    {
        this.GroupSecurityInit(iGroupSecurityID);
        dbDB = new dbtMCMC_Retros( iNewID );
        makeExpiredHash();
        makeEnglishHash();
        makeRequiredHash(iGroupSecurityID);
    }    // End of Constructor knowing an ID

    public bltMCMC_Retros( Integer iGroupSecurityID, boolean isTrue )
    {
        this.GroupSecurityInit(iGroupSecurityID);
        dbDB = new dbtMCMC_Retros(  );
        makeExpiredHash();
        makeEnglishHash();
        makeRequiredHash(iGroupSecurityID);
    }    // End of Security unknown ID

    public void GroupSecurityInit( Integer iGroupSecurityID )
    {
        bltSecurityGroupMaster mySGM = new bltSecurityGroupMaster(iGroupSecurityID);
        this.UserSecurityID = iGroupSecurityID;
        if (mySGM.getRunFieldSecurityID().intValue()==1)
        {
            this.runFieldSecurity = true;
            makeRequiredHash(iGroupSecurityID);
        }
        iSecurityCheck = SecurityCheck.CheckItem("nim3", iGroupSecurityID);
    }  

    public EventReferenceType getEventReference()
    {
        return EventUtils.getEventReference("tMCMC_Retros", this.getUniqueID());
    }  

    public String convertString2LogString(String normalString) 
    {
        String myVal = "";
        for (int i=0;i<normalString.length();i++)
        {
            if ( (normalString.charAt(i)!='\r')&&(normalString.charAt(i)!='\n')&&(normalString.charAt(i)!='\t') )
            {
                myVal+=normalString.charAt(i);
            }
            else
            {
                myVal+="<NLT>";
            }
        }
        return myVal;
    }
    public void runAudit()
    {
        String AuditString = new java.util.Date()+" \t " ;  
        AuditString += " tMCMC_Retros "; 
        AuditString += " MCMCID ="+this.getUniqueID(); 

        for (int i=0;i<this.AuditVector.size();i++)
        {
            AuditString+="\t"+convertString2LogString((String)this.AuditVector.elementAt(i));
        }
        AuditLogger.printLine(AuditString);
    }
    public void commitData() throws SQLException
    {
        runAudit();
        dbDB.setModifyComments(this.getUniqueModifyComments());
        dbDB.setUniqueModifyDate(new java.util.Date());
        dbDB.commitData();
        DataControlUtils.dataChanged("tMCMC_Retros",this.getUniqueID() ,this.AuditVector.size(), "MCMCID");
        this.AuditVector.removeAllElements();
    }    // End of commitData()

    public void commitDataForced() throws SQLException
    {
        dbDB.commitDataForced();
    }    // End of commitData()

    public Integer getUniqueID()
    {
        return dbDB.getUniqueID();
    }    // End of getUniqueID()

    public void setUniqueID( Integer iNewID )
    {
        dbDB.setUniqueID( iNewID );
    }    // End of setUniqueID()


    public void setMCMCID(Integer newValue)
    {
        dbDB.setMCMCID(newValue);
    }

    public Integer getMCMCID()
    {
        return dbDB.getMCMCID();
    }

    java.util.Vector AuditVector = new java.util.Vector();

    public void setUniqueCreateDate(Date newValue)
    {
        this.setUniqueCreateDate(newValue,this.UserSecurityID);


    }
    public Date getUniqueCreateDate()
    {
        return this.getUniqueCreateDate(this.UserSecurityID);
    }

    public void setUniqueCreateDate(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='UniqueCreateDate' AND TableName='tMCMC_Retros'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[UniqueCreateDate]=["+newValue+"]");
                   dbDB.setUniqueCreateDate(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[UniqueCreateDate]=["+newValue+"]");
           dbDB.setUniqueCreateDate(newValue);
         }
    }
    public Date getUniqueCreateDate(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='UniqueCreateDate' AND TableName='tMCMC_Retros'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getUniqueCreateDate();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getUniqueCreateDate();
         }
        return myVal;
    }

    public void setUniqueModifyDate(Date newValue)
    {
        this.setUniqueModifyDate(newValue,this.UserSecurityID);


    }
    public Date getUniqueModifyDate()
    {
        return this.getUniqueModifyDate(this.UserSecurityID);
    }

    public void setUniqueModifyDate(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='UniqueModifyDate' AND TableName='tMCMC_Retros'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[UniqueModifyDate]=["+newValue+"]");
                   dbDB.setUniqueModifyDate(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[UniqueModifyDate]=["+newValue+"]");
           dbDB.setUniqueModifyDate(newValue);
         }
    }
    public Date getUniqueModifyDate(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='UniqueModifyDate' AND TableName='tMCMC_Retros'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getUniqueModifyDate();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getUniqueModifyDate();
         }
        return myVal;
    }

    public void setUniqueModifyComments(String newValue)
    {
        this.setUniqueModifyComments(newValue,this.UserSecurityID);


    }
    public String getUniqueModifyComments()
    {
        return this.getUniqueModifyComments(this.UserSecurityID);
    }

    public void setUniqueModifyComments(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='UniqueModifyComments' AND TableName='tMCMC_Retros'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[UniqueModifyComments]=["+newValue+"]");
                   dbDB.setUniqueModifyComments(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[UniqueModifyComments]=["+newValue+"]");
           dbDB.setUniqueModifyComments(newValue);
         }
    }
    public String getUniqueModifyComments(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='UniqueModifyComments' AND TableName='tMCMC_Retros'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getUniqueModifyComments();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getUniqueModifyComments();
         }
        return myVal;
    }

    public void setMCMCBillID(Integer newValue)
    {
        this.setMCMCBillID(newValue,this.UserSecurityID);


    }
    public Integer getMCMCBillID()
    {
        return this.getMCMCBillID(this.UserSecurityID);
    }

    public void setMCMCBillID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='MCMCBillID' AND TableName='tMCMC_Retros'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[MCMCBillID]=["+newValue+"]");
                   dbDB.setMCMCBillID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[MCMCBillID]=["+newValue+"]");
           dbDB.setMCMCBillID(newValue);
         }
    }
    public Integer getMCMCBillID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='MCMCBillID' AND TableName='tMCMC_Retros'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getMCMCBillID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getMCMCBillID();
         }
        return myVal;
    }

    public void setNetworkBillID(Integer newValue)
    {
        this.setNetworkBillID(newValue,this.UserSecurityID);


    }
    public Integer getNetworkBillID()
    {
        return this.getNetworkBillID(this.UserSecurityID);
    }

    public void setNetworkBillID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='NetworkBillID' AND TableName='tMCMC_Retros'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[NetworkBillID]=["+newValue+"]");
                   dbDB.setNetworkBillID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[NetworkBillID]=["+newValue+"]");
           dbDB.setNetworkBillID(newValue);
         }
    }
    public Integer getNetworkBillID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='NetworkBillID' AND TableName='tMCMC_Retros'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getNetworkBillID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getNetworkBillID();
         }
        return myVal;
    }

    public void setMCMCLocation(String newValue)
    {
        this.setMCMCLocation(newValue,this.UserSecurityID);


    }
    public String getMCMCLocation()
    {
        return this.getMCMCLocation(this.UserSecurityID);
    }

    public void setMCMCLocation(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='MCMCLocation' AND TableName='tMCMC_Retros'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[MCMCLocation]=["+newValue+"]");
                   dbDB.setMCMCLocation(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[MCMCLocation]=["+newValue+"]");
           dbDB.setMCMCLocation(newValue);
         }
    }
    public String getMCMCLocation(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='MCMCLocation' AND TableName='tMCMC_Retros'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getMCMCLocation();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getMCMCLocation();
         }
        return myVal;
    }

    public void setRenderingProviderTaxID(String newValue)
    {
        this.setRenderingProviderTaxID(newValue,this.UserSecurityID);


    }
    public String getRenderingProviderTaxID()
    {
        return this.getRenderingProviderTaxID(this.UserSecurityID);
    }

    public void setRenderingProviderTaxID(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='RenderingProviderTaxID' AND TableName='tMCMC_Retros'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[RenderingProviderTaxID]=["+newValue+"]");
                   dbDB.setRenderingProviderTaxID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[RenderingProviderTaxID]=["+newValue+"]");
           dbDB.setRenderingProviderTaxID(newValue);
         }
    }
    public String getRenderingProviderTaxID(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='RenderingProviderTaxID' AND TableName='tMCMC_Retros'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getRenderingProviderTaxID();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getRenderingProviderTaxID();
         }
        return myVal;
    }

    public void setRenderingProviderName(String newValue)
    {
        this.setRenderingProviderName(newValue,this.UserSecurityID);


    }
    public String getRenderingProviderName()
    {
        return this.getRenderingProviderName(this.UserSecurityID);
    }

    public void setRenderingProviderName(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='RenderingProviderName' AND TableName='tMCMC_Retros'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[RenderingProviderName]=["+newValue+"]");
                   dbDB.setRenderingProviderName(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[RenderingProviderName]=["+newValue+"]");
           dbDB.setRenderingProviderName(newValue);
         }
    }
    public String getRenderingProviderName(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='RenderingProviderName' AND TableName='tMCMC_Retros'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getRenderingProviderName();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getRenderingProviderName();
         }
        return myVal;
    }

    public void setRenderingProviderAddress(String newValue)
    {
        this.setRenderingProviderAddress(newValue,this.UserSecurityID);


    }
    public String getRenderingProviderAddress()
    {
        return this.getRenderingProviderAddress(this.UserSecurityID);
    }

    public void setRenderingProviderAddress(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='RenderingProviderAddress' AND TableName='tMCMC_Retros'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[RenderingProviderAddress]=["+newValue+"]");
                   dbDB.setRenderingProviderAddress(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[RenderingProviderAddress]=["+newValue+"]");
           dbDB.setRenderingProviderAddress(newValue);
         }
    }
    public String getRenderingProviderAddress(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='RenderingProviderAddress' AND TableName='tMCMC_Retros'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getRenderingProviderAddress();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getRenderingProviderAddress();
         }
        return myVal;
    }

    public void setRenderingProviderCity(String newValue)
    {
        this.setRenderingProviderCity(newValue,this.UserSecurityID);


    }
    public String getRenderingProviderCity()
    {
        return this.getRenderingProviderCity(this.UserSecurityID);
    }

    public void setRenderingProviderCity(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='RenderingProviderCity' AND TableName='tMCMC_Retros'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[RenderingProviderCity]=["+newValue+"]");
                   dbDB.setRenderingProviderCity(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[RenderingProviderCity]=["+newValue+"]");
           dbDB.setRenderingProviderCity(newValue);
         }
    }
    public String getRenderingProviderCity(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='RenderingProviderCity' AND TableName='tMCMC_Retros'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getRenderingProviderCity();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getRenderingProviderCity();
         }
        return myVal;
    }

    public void setRenderingProviderState(String newValue)
    {
        this.setRenderingProviderState(newValue,this.UserSecurityID);


    }
    public String getRenderingProviderState()
    {
        return this.getRenderingProviderState(this.UserSecurityID);
    }

    public void setRenderingProviderState(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='RenderingProviderState' AND TableName='tMCMC_Retros'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[RenderingProviderState]=["+newValue+"]");
                   dbDB.setRenderingProviderState(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[RenderingProviderState]=["+newValue+"]");
           dbDB.setRenderingProviderState(newValue);
         }
    }
    public String getRenderingProviderState(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='RenderingProviderState' AND TableName='tMCMC_Retros'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getRenderingProviderState();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getRenderingProviderState();
         }
        return myVal;
    }

    public void setRenderingProviderZip(String newValue)
    {
        this.setRenderingProviderZip(newValue,this.UserSecurityID);


    }
    public String getRenderingProviderZip()
    {
        return this.getRenderingProviderZip(this.UserSecurityID);
    }

    public void setRenderingProviderZip(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='RenderingProviderZip' AND TableName='tMCMC_Retros'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[RenderingProviderZip]=["+newValue+"]");
                   dbDB.setRenderingProviderZip(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[RenderingProviderZip]=["+newValue+"]");
           dbDB.setRenderingProviderZip(newValue);
         }
    }
    public String getRenderingProviderZip(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='RenderingProviderZip' AND TableName='tMCMC_Retros'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getRenderingProviderZip();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getRenderingProviderZip();
         }
        return myVal;
    }

    public void setBillingProviderTaxID(String newValue)
    {
        this.setBillingProviderTaxID(newValue,this.UserSecurityID);


    }
    public String getBillingProviderTaxID()
    {
        return this.getBillingProviderTaxID(this.UserSecurityID);
    }

    public void setBillingProviderTaxID(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='BillingProviderTaxID' AND TableName='tMCMC_Retros'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[BillingProviderTaxID]=["+newValue+"]");
                   dbDB.setBillingProviderTaxID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[BillingProviderTaxID]=["+newValue+"]");
           dbDB.setBillingProviderTaxID(newValue);
         }
    }
    public String getBillingProviderTaxID(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='BillingProviderTaxID' AND TableName='tMCMC_Retros'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getBillingProviderTaxID();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getBillingProviderTaxID();
         }
        return myVal;
    }

    public void setBillingProviderName(String newValue)
    {
        this.setBillingProviderName(newValue,this.UserSecurityID);


    }
    public String getBillingProviderName()
    {
        return this.getBillingProviderName(this.UserSecurityID);
    }

    public void setBillingProviderName(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='BillingProviderName' AND TableName='tMCMC_Retros'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[BillingProviderName]=["+newValue+"]");
                   dbDB.setBillingProviderName(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[BillingProviderName]=["+newValue+"]");
           dbDB.setBillingProviderName(newValue);
         }
    }
    public String getBillingProviderName(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='BillingProviderName' AND TableName='tMCMC_Retros'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getBillingProviderName();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getBillingProviderName();
         }
        return myVal;
    }

    public void setBillingProviderAddress(String newValue)
    {
        this.setBillingProviderAddress(newValue,this.UserSecurityID);


    }
    public String getBillingProviderAddress()
    {
        return this.getBillingProviderAddress(this.UserSecurityID);
    }

    public void setBillingProviderAddress(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='BillingProviderAddress' AND TableName='tMCMC_Retros'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[BillingProviderAddress]=["+newValue+"]");
                   dbDB.setBillingProviderAddress(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[BillingProviderAddress]=["+newValue+"]");
           dbDB.setBillingProviderAddress(newValue);
         }
    }
    public String getBillingProviderAddress(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='BillingProviderAddress' AND TableName='tMCMC_Retros'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getBillingProviderAddress();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getBillingProviderAddress();
         }
        return myVal;
    }

    public void setBillingProviderCity(String newValue)
    {
        this.setBillingProviderCity(newValue,this.UserSecurityID);


    }
    public String getBillingProviderCity()
    {
        return this.getBillingProviderCity(this.UserSecurityID);
    }

    public void setBillingProviderCity(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='BillingProviderCity' AND TableName='tMCMC_Retros'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[BillingProviderCity]=["+newValue+"]");
                   dbDB.setBillingProviderCity(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[BillingProviderCity]=["+newValue+"]");
           dbDB.setBillingProviderCity(newValue);
         }
    }
    public String getBillingProviderCity(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='BillingProviderCity' AND TableName='tMCMC_Retros'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getBillingProviderCity();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getBillingProviderCity();
         }
        return myVal;
    }

    public void setBillingProviderState(String newValue)
    {
        this.setBillingProviderState(newValue,this.UserSecurityID);


    }
    public String getBillingProviderState()
    {
        return this.getBillingProviderState(this.UserSecurityID);
    }

    public void setBillingProviderState(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='BillingProviderState' AND TableName='tMCMC_Retros'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[BillingProviderState]=["+newValue+"]");
                   dbDB.setBillingProviderState(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[BillingProviderState]=["+newValue+"]");
           dbDB.setBillingProviderState(newValue);
         }
    }
    public String getBillingProviderState(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='BillingProviderState' AND TableName='tMCMC_Retros'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getBillingProviderState();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getBillingProviderState();
         }
        return myVal;
    }

    public void setBillingProviderZip(String newValue)
    {
        this.setBillingProviderZip(newValue,this.UserSecurityID);


    }
    public String getBillingProviderZip()
    {
        return this.getBillingProviderZip(this.UserSecurityID);
    }

    public void setBillingProviderZip(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='BillingProviderZip' AND TableName='tMCMC_Retros'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[BillingProviderZip]=["+newValue+"]");
                   dbDB.setBillingProviderZip(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[BillingProviderZip]=["+newValue+"]");
           dbDB.setBillingProviderZip(newValue);
         }
    }
    public String getBillingProviderZip(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='BillingProviderZip' AND TableName='tMCMC_Retros'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getBillingProviderZip();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getBillingProviderZip();
         }
        return myVal;
    }

    public void setClientName(String newValue)
    {
        this.setClientName(newValue,this.UserSecurityID);


    }
    public String getClientName()
    {
        return this.getClientName(this.UserSecurityID);
    }

    public void setClientName(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ClientName' AND TableName='tMCMC_Retros'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[ClientName]=["+newValue+"]");
                   dbDB.setClientName(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[ClientName]=["+newValue+"]");
           dbDB.setClientName(newValue);
         }
    }
    public String getClientName(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ClientName' AND TableName='tMCMC_Retros'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getClientName();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getClientName();
         }
        return myVal;
    }

    public void setClaimNumber(String newValue)
    {
        this.setClaimNumber(newValue,this.UserSecurityID);


    }
    public String getClaimNumber()
    {
        return this.getClaimNumber(this.UserSecurityID);
    }

    public void setClaimNumber(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ClaimNumber' AND TableName='tMCMC_Retros'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[ClaimNumber]=["+newValue+"]");
                   dbDB.setClaimNumber(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[ClaimNumber]=["+newValue+"]");
           dbDB.setClaimNumber(newValue);
         }
    }
    public String getClaimNumber(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ClaimNumber' AND TableName='tMCMC_Retros'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getClaimNumber();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getClaimNumber();
         }
        return myVal;
    }

    public void setPatientSSN(String newValue)
    {
        this.setPatientSSN(newValue,this.UserSecurityID);


    }
    public String getPatientSSN()
    {
        return this.getPatientSSN(this.UserSecurityID);
    }

    public void setPatientSSN(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientSSN' AND TableName='tMCMC_Retros'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[PatientSSN]=["+newValue+"]");
                   dbDB.setPatientSSN(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[PatientSSN]=["+newValue+"]");
           dbDB.setPatientSSN(newValue);
         }
    }
    public String getPatientSSN(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientSSN' AND TableName='tMCMC_Retros'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPatientSSN();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPatientSSN();
         }
        return myVal;
    }

    public void setPatientFirstName(String newValue)
    {
        this.setPatientFirstName(newValue,this.UserSecurityID);


    }
    public String getPatientFirstName()
    {
        return this.getPatientFirstName(this.UserSecurityID);
    }

    public void setPatientFirstName(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientFirstName' AND TableName='tMCMC_Retros'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[PatientFirstName]=["+newValue+"]");
                   dbDB.setPatientFirstName(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[PatientFirstName]=["+newValue+"]");
           dbDB.setPatientFirstName(newValue);
         }
    }
    public String getPatientFirstName(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientFirstName' AND TableName='tMCMC_Retros'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPatientFirstName();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPatientFirstName();
         }
        return myVal;
    }

    public void setPatientLastName(String newValue)
    {
        this.setPatientLastName(newValue,this.UserSecurityID);


    }
    public String getPatientLastName()
    {
        return this.getPatientLastName(this.UserSecurityID);
    }

    public void setPatientLastName(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientLastName' AND TableName='tMCMC_Retros'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[PatientLastName]=["+newValue+"]");
                   dbDB.setPatientLastName(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[PatientLastName]=["+newValue+"]");
           dbDB.setPatientLastName(newValue);
         }
    }
    public String getPatientLastName(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientLastName' AND TableName='tMCMC_Retros'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPatientLastName();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPatientLastName();
         }
        return myVal;
    }

    public void setPatientAddress1(String newValue)
    {
        this.setPatientAddress1(newValue,this.UserSecurityID);


    }
    public String getPatientAddress1()
    {
        return this.getPatientAddress1(this.UserSecurityID);
    }

    public void setPatientAddress1(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientAddress1' AND TableName='tMCMC_Retros'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[PatientAddress1]=["+newValue+"]");
                   dbDB.setPatientAddress1(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[PatientAddress1]=["+newValue+"]");
           dbDB.setPatientAddress1(newValue);
         }
    }
    public String getPatientAddress1(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientAddress1' AND TableName='tMCMC_Retros'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPatientAddress1();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPatientAddress1();
         }
        return myVal;
    }

    public void setPatientAddress2(String newValue)
    {
        this.setPatientAddress2(newValue,this.UserSecurityID);


    }
    public String getPatientAddress2()
    {
        return this.getPatientAddress2(this.UserSecurityID);
    }

    public void setPatientAddress2(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientAddress2' AND TableName='tMCMC_Retros'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[PatientAddress2]=["+newValue+"]");
                   dbDB.setPatientAddress2(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[PatientAddress2]=["+newValue+"]");
           dbDB.setPatientAddress2(newValue);
         }
    }
    public String getPatientAddress2(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientAddress2' AND TableName='tMCMC_Retros'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPatientAddress2();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPatientAddress2();
         }
        return myVal;
    }

    public void setPatientCity(String newValue)
    {
        this.setPatientCity(newValue,this.UserSecurityID);


    }
    public String getPatientCity()
    {
        return this.getPatientCity(this.UserSecurityID);
    }

    public void setPatientCity(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientCity' AND TableName='tMCMC_Retros'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[PatientCity]=["+newValue+"]");
                   dbDB.setPatientCity(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[PatientCity]=["+newValue+"]");
           dbDB.setPatientCity(newValue);
         }
    }
    public String getPatientCity(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientCity' AND TableName='tMCMC_Retros'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPatientCity();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPatientCity();
         }
        return myVal;
    }

    public void setPatientState(String newValue)
    {
        this.setPatientState(newValue,this.UserSecurityID);


    }
    public String getPatientState()
    {
        return this.getPatientState(this.UserSecurityID);
    }

    public void setPatientState(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientState' AND TableName='tMCMC_Retros'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[PatientState]=["+newValue+"]");
                   dbDB.setPatientState(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[PatientState]=["+newValue+"]");
           dbDB.setPatientState(newValue);
         }
    }
    public String getPatientState(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientState' AND TableName='tMCMC_Retros'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPatientState();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPatientState();
         }
        return myVal;
    }

    public void setPatientZip(String newValue)
    {
        this.setPatientZip(newValue,this.UserSecurityID);


    }
    public String getPatientZip()
    {
        return this.getPatientZip(this.UserSecurityID);
    }

    public void setPatientZip(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientZip' AND TableName='tMCMC_Retros'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[PatientZip]=["+newValue+"]");
                   dbDB.setPatientZip(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[PatientZip]=["+newValue+"]");
           dbDB.setPatientZip(newValue);
         }
    }
    public String getPatientZip(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientZip' AND TableName='tMCMC_Retros'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPatientZip();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPatientZip();
         }
        return myVal;
    }

    public void setEmployerName(String newValue)
    {
        this.setEmployerName(newValue,this.UserSecurityID);


    }
    public String getEmployerName()
    {
        return this.getEmployerName(this.UserSecurityID);
    }

    public void setEmployerName(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='EmployerName' AND TableName='tMCMC_Retros'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[EmployerName]=["+newValue+"]");
                   dbDB.setEmployerName(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[EmployerName]=["+newValue+"]");
           dbDB.setEmployerName(newValue);
         }
    }
    public String getEmployerName(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='EmployerName' AND TableName='tMCMC_Retros'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getEmployerName();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getEmployerName();
         }
        return myVal;
    }

    public void setEmployerAddress1(String newValue)
    {
        this.setEmployerAddress1(newValue,this.UserSecurityID);


    }
    public String getEmployerAddress1()
    {
        return this.getEmployerAddress1(this.UserSecurityID);
    }

    public void setEmployerAddress1(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='EmployerAddress1' AND TableName='tMCMC_Retros'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[EmployerAddress1]=["+newValue+"]");
                   dbDB.setEmployerAddress1(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[EmployerAddress1]=["+newValue+"]");
           dbDB.setEmployerAddress1(newValue);
         }
    }
    public String getEmployerAddress1(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='EmployerAddress1' AND TableName='tMCMC_Retros'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getEmployerAddress1();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getEmployerAddress1();
         }
        return myVal;
    }

    public void setEmployerAddress2(String newValue)
    {
        this.setEmployerAddress2(newValue,this.UserSecurityID);


    }
    public String getEmployerAddress2()
    {
        return this.getEmployerAddress2(this.UserSecurityID);
    }

    public void setEmployerAddress2(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='EmployerAddress2' AND TableName='tMCMC_Retros'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[EmployerAddress2]=["+newValue+"]");
                   dbDB.setEmployerAddress2(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[EmployerAddress2]=["+newValue+"]");
           dbDB.setEmployerAddress2(newValue);
         }
    }
    public String getEmployerAddress2(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='EmployerAddress2' AND TableName='tMCMC_Retros'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getEmployerAddress2();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getEmployerAddress2();
         }
        return myVal;
    }

    public void setEmployerCity(String newValue)
    {
        this.setEmployerCity(newValue,this.UserSecurityID);


    }
    public String getEmployerCity()
    {
        return this.getEmployerCity(this.UserSecurityID);
    }

    public void setEmployerCity(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='EmployerCity' AND TableName='tMCMC_Retros'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[EmployerCity]=["+newValue+"]");
                   dbDB.setEmployerCity(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[EmployerCity]=["+newValue+"]");
           dbDB.setEmployerCity(newValue);
         }
    }
    public String getEmployerCity(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='EmployerCity' AND TableName='tMCMC_Retros'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getEmployerCity();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getEmployerCity();
         }
        return myVal;
    }

    public void setEmployerState(String newValue)
    {
        this.setEmployerState(newValue,this.UserSecurityID);


    }
    public String getEmployerState()
    {
        return this.getEmployerState(this.UserSecurityID);
    }

    public void setEmployerState(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='EmployerState' AND TableName='tMCMC_Retros'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[EmployerState]=["+newValue+"]");
                   dbDB.setEmployerState(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[EmployerState]=["+newValue+"]");
           dbDB.setEmployerState(newValue);
         }
    }
    public String getEmployerState(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='EmployerState' AND TableName='tMCMC_Retros'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getEmployerState();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getEmployerState();
         }
        return myVal;
    }

    public void setEmployerZip(String newValue)
    {
        this.setEmployerZip(newValue,this.UserSecurityID);


    }
    public String getEmployerZip()
    {
        return this.getEmployerZip(this.UserSecurityID);
    }

    public void setEmployerZip(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='EmployerZip' AND TableName='tMCMC_Retros'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[EmployerZip]=["+newValue+"]");
                   dbDB.setEmployerZip(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[EmployerZip]=["+newValue+"]");
           dbDB.setEmployerZip(newValue);
         }
    }
    public String getEmployerZip(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='EmployerZip' AND TableName='tMCMC_Retros'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getEmployerZip();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getEmployerZip();
         }
        return myVal;
    }

    public void setDiagnosisCode1(String newValue)
    {
        this.setDiagnosisCode1(newValue,this.UserSecurityID);


    }
    public String getDiagnosisCode1()
    {
        return this.getDiagnosisCode1(this.UserSecurityID);
    }

    public void setDiagnosisCode1(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='DiagnosisCode1' AND TableName='tMCMC_Retros'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[DiagnosisCode1]=["+newValue+"]");
                   dbDB.setDiagnosisCode1(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[DiagnosisCode1]=["+newValue+"]");
           dbDB.setDiagnosisCode1(newValue);
         }
    }
    public String getDiagnosisCode1(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='DiagnosisCode1' AND TableName='tMCMC_Retros'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getDiagnosisCode1();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getDiagnosisCode1();
         }
        return myVal;
    }

    public void setDiagnosisCode2(String newValue)
    {
        this.setDiagnosisCode2(newValue,this.UserSecurityID);


    }
    public String getDiagnosisCode2()
    {
        return this.getDiagnosisCode2(this.UserSecurityID);
    }

    public void setDiagnosisCode2(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='DiagnosisCode2' AND TableName='tMCMC_Retros'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[DiagnosisCode2]=["+newValue+"]");
                   dbDB.setDiagnosisCode2(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[DiagnosisCode2]=["+newValue+"]");
           dbDB.setDiagnosisCode2(newValue);
         }
    }
    public String getDiagnosisCode2(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='DiagnosisCode2' AND TableName='tMCMC_Retros'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getDiagnosisCode2();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getDiagnosisCode2();
         }
        return myVal;
    }

    public void setDiagnosisCode3(String newValue)
    {
        this.setDiagnosisCode3(newValue,this.UserSecurityID);


    }
    public String getDiagnosisCode3()
    {
        return this.getDiagnosisCode3(this.UserSecurityID);
    }

    public void setDiagnosisCode3(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='DiagnosisCode3' AND TableName='tMCMC_Retros'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[DiagnosisCode3]=["+newValue+"]");
                   dbDB.setDiagnosisCode3(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[DiagnosisCode3]=["+newValue+"]");
           dbDB.setDiagnosisCode3(newValue);
         }
    }
    public String getDiagnosisCode3(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='DiagnosisCode3' AND TableName='tMCMC_Retros'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getDiagnosisCode3();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getDiagnosisCode3();
         }
        return myVal;
    }

    public void setDiagnosisCode4(String newValue)
    {
        this.setDiagnosisCode4(newValue,this.UserSecurityID);


    }
    public String getDiagnosisCode4()
    {
        return this.getDiagnosisCode4(this.UserSecurityID);
    }

    public void setDiagnosisCode4(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='DiagnosisCode4' AND TableName='tMCMC_Retros'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[DiagnosisCode4]=["+newValue+"]");
                   dbDB.setDiagnosisCode4(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[DiagnosisCode4]=["+newValue+"]");
           dbDB.setDiagnosisCode4(newValue);
         }
    }
    public String getDiagnosisCode4(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='DiagnosisCode4' AND TableName='tMCMC_Retros'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getDiagnosisCode4();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getDiagnosisCode4();
         }
        return myVal;
    }

    public void setDateOfBirth(String newValue)
    {
        this.setDateOfBirth(newValue,this.UserSecurityID);


    }
    public String getDateOfBirth()
    {
        return this.getDateOfBirth(this.UserSecurityID);
    }

    public void setDateOfBirth(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='DateOfBirth' AND TableName='tMCMC_Retros'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[DateOfBirth]=["+newValue+"]");
                   dbDB.setDateOfBirth(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[DateOfBirth]=["+newValue+"]");
           dbDB.setDateOfBirth(newValue);
         }
    }
    public String getDateOfBirth(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='DateOfBirth' AND TableName='tMCMC_Retros'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getDateOfBirth();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getDateOfBirth();
         }
        return myVal;
    }

    public void setInjuryDate(String newValue)
    {
        this.setInjuryDate(newValue,this.UserSecurityID);


    }
    public String getInjuryDate()
    {
        return this.getInjuryDate(this.UserSecurityID);
    }

    public void setInjuryDate(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='InjuryDate' AND TableName='tMCMC_Retros'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[InjuryDate]=["+newValue+"]");
                   dbDB.setInjuryDate(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[InjuryDate]=["+newValue+"]");
           dbDB.setInjuryDate(newValue);
         }
    }
    public String getInjuryDate(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='InjuryDate' AND TableName='tMCMC_Retros'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getInjuryDate();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getInjuryDate();
         }
        return myVal;
    }

    public void setReviewState(String newValue)
    {
        this.setReviewState(newValue,this.UserSecurityID);


    }
    public String getReviewState()
    {
        return this.getReviewState(this.UserSecurityID);
    }

    public void setReviewState(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ReviewState' AND TableName='tMCMC_Retros'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[ReviewState]=["+newValue+"]");
                   dbDB.setReviewState(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[ReviewState]=["+newValue+"]");
           dbDB.setReviewState(newValue);
         }
    }
    public String getReviewState(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ReviewState' AND TableName='tMCMC_Retros'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getReviewState();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getReviewState();
         }
        return myVal;
    }

    public void setReviewZip(String newValue)
    {
        this.setReviewZip(newValue,this.UserSecurityID);


    }
    public String getReviewZip()
    {
        return this.getReviewZip(this.UserSecurityID);
    }

    public void setReviewZip(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ReviewZip' AND TableName='tMCMC_Retros'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[ReviewZip]=["+newValue+"]");
                   dbDB.setReviewZip(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[ReviewZip]=["+newValue+"]");
           dbDB.setReviewZip(newValue);
         }
    }
    public String getReviewZip(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ReviewZip' AND TableName='tMCMC_Retros'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getReviewZip();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getReviewZip();
         }
        return myVal;
    }

    public void setLineID(Integer newValue)
    {
        this.setLineID(newValue,this.UserSecurityID);


    }
    public Integer getLineID()
    {
        return this.getLineID(this.UserSecurityID);
    }

    public void setLineID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='LineID' AND TableName='tMCMC_Retros'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[LineID]=["+newValue+"]");
                   dbDB.setLineID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[LineID]=["+newValue+"]");
           dbDB.setLineID(newValue);
         }
    }
    public Integer getLineID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='LineID' AND TableName='tMCMC_Retros'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getLineID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getLineID();
         }
        return myVal;
    }

    public void setFSType(String newValue)
    {
        this.setFSType(newValue,this.UserSecurityID);


    }
    public String getFSType()
    {
        return this.getFSType(this.UserSecurityID);
    }

    public void setFSType(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='FSType' AND TableName='tMCMC_Retros'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[FSType]=["+newValue+"]");
                   dbDB.setFSType(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[FSType]=["+newValue+"]");
           dbDB.setFSType(newValue);
         }
    }
    public String getFSType(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='FSType' AND TableName='tMCMC_Retros'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getFSType();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getFSType();
         }
        return myVal;
    }

    public void setDateOfService(String newValue)
    {
        this.setDateOfService(newValue,this.UserSecurityID);


    }
    public String getDateOfService()
    {
        return this.getDateOfService(this.UserSecurityID);
    }

    public void setDateOfService(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='DateOfService' AND TableName='tMCMC_Retros'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[DateOfService]=["+newValue+"]");
                   dbDB.setDateOfService(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[DateOfService]=["+newValue+"]");
           dbDB.setDateOfService(newValue);
         }
    }
    public String getDateOfService(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='DateOfService' AND TableName='tMCMC_Retros'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getDateOfService();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getDateOfService();
         }
        return myVal;
    }

    public void setCPTCode(String newValue)
    {
        this.setCPTCode(newValue,this.UserSecurityID);


    }
    public String getCPTCode()
    {
        return this.getCPTCode(this.UserSecurityID);
    }

    public void setCPTCode(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CPTCode' AND TableName='tMCMC_Retros'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[CPTCode]=["+newValue+"]");
                   dbDB.setCPTCode(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[CPTCode]=["+newValue+"]");
           dbDB.setCPTCode(newValue);
         }
    }
    public String getCPTCode(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CPTCode' AND TableName='tMCMC_Retros'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getCPTCode();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getCPTCode();
         }
        return myVal;
    }

    public void setModifier1(String newValue)
    {
        this.setModifier1(newValue,this.UserSecurityID);


    }
    public String getModifier1()
    {
        return this.getModifier1(this.UserSecurityID);
    }

    public void setModifier1(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Modifier1' AND TableName='tMCMC_Retros'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[Modifier1]=["+newValue+"]");
                   dbDB.setModifier1(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[Modifier1]=["+newValue+"]");
           dbDB.setModifier1(newValue);
         }
    }
    public String getModifier1(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Modifier1' AND TableName='tMCMC_Retros'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getModifier1();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getModifier1();
         }
        return myVal;
    }

    public void setModifier2(String newValue)
    {
        this.setModifier2(newValue,this.UserSecurityID);


    }
    public String getModifier2()
    {
        return this.getModifier2(this.UserSecurityID);
    }

    public void setModifier2(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Modifier2' AND TableName='tMCMC_Retros'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[Modifier2]=["+newValue+"]");
                   dbDB.setModifier2(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[Modifier2]=["+newValue+"]");
           dbDB.setModifier2(newValue);
         }
    }
    public String getModifier2(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Modifier2' AND TableName='tMCMC_Retros'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getModifier2();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getModifier2();
         }
        return myVal;
    }

    public void setUnits(Double newValue)
    {
        this.setUnits(newValue,this.UserSecurityID);


    }
    public Double getUnits()
    {
        return this.getUnits(this.UserSecurityID);
    }

    public void setUnits(Double newValue, Integer GroupRefID)
    {
        Double myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Units' AND TableName='tMCMC_Retros'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[Units]=["+newValue+"]");
                   dbDB.setUnits(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[Units]=["+newValue+"]");
           dbDB.setUnits(newValue);
         }
    }
    public Double getUnits(Integer GroupRefID)
    {
        Double myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Units' AND TableName='tMCMC_Retros'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getUnits();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getUnits();
         }
        return myVal;
    }

    public void setProviderCharge(Double newValue)
    {
        this.setProviderCharge(newValue,this.UserSecurityID);


    }
    public Double getProviderCharge()
    {
        return this.getProviderCharge(this.UserSecurityID);
    }

    public void setProviderCharge(Double newValue, Integer GroupRefID)
    {
        Double myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ProviderCharge' AND TableName='tMCMC_Retros'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[ProviderCharge]=["+newValue+"]");
                   dbDB.setProviderCharge(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[ProviderCharge]=["+newValue+"]");
           dbDB.setProviderCharge(newValue);
         }
    }
    public Double getProviderCharge(Integer GroupRefID)
    {
        Double myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ProviderCharge' AND TableName='tMCMC_Retros'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getProviderCharge();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getProviderCharge();
         }
        return myVal;
    }

    public void setStateAllowance(Double newValue)
    {
        this.setStateAllowance(newValue,this.UserSecurityID);


    }
    public Double getStateAllowance()
    {
        return this.getStateAllowance(this.UserSecurityID);
    }

    public void setStateAllowance(Double newValue, Integer GroupRefID)
    {
        Double myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='StateAllowance' AND TableName='tMCMC_Retros'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[StateAllowance]=["+newValue+"]");
                   dbDB.setStateAllowance(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[StateAllowance]=["+newValue+"]");
           dbDB.setStateAllowance(newValue);
         }
    }
    public Double getStateAllowance(Integer GroupRefID)
    {
        Double myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='StateAllowance' AND TableName='tMCMC_Retros'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getStateAllowance();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getStateAllowance();
         }
        return myVal;
    }

    public void setMCMCAllowance(Double newValue)
    {
        this.setMCMCAllowance(newValue,this.UserSecurityID);


    }
    public Double getMCMCAllowance()
    {
        return this.getMCMCAllowance(this.UserSecurityID);
    }

    public void setMCMCAllowance(Double newValue, Integer GroupRefID)
    {
        Double myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='MCMCAllowance' AND TableName='tMCMC_Retros'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[MCMCAllowance]=["+newValue+"]");
                   dbDB.setMCMCAllowance(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[MCMCAllowance]=["+newValue+"]");
           dbDB.setMCMCAllowance(newValue);
         }
    }
    public Double getMCMCAllowance(Integer GroupRefID)
    {
        Double myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='MCMCAllowance' AND TableName='tMCMC_Retros'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getMCMCAllowance();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getMCMCAllowance();
         }
        return myVal;
    }

    public void setNetworkAllowance(Double newValue)
    {
        this.setNetworkAllowance(newValue,this.UserSecurityID);


    }
    public Double getNetworkAllowance()
    {
        return this.getNetworkAllowance(this.UserSecurityID);
    }

    public void setNetworkAllowance(Double newValue, Integer GroupRefID)
    {
        Double myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='NetworkAllowance' AND TableName='tMCMC_Retros'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[NetworkAllowance]=["+newValue+"]");
                   dbDB.setNetworkAllowance(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[NetworkAllowance]=["+newValue+"]");
           dbDB.setNetworkAllowance(newValue);
         }
    }
    public Double getNetworkAllowance(Integer GroupRefID)
    {
        Double myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='NetworkAllowance' AND TableName='tMCMC_Retros'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getNetworkAllowance();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getNetworkAllowance();
         }
        return myVal;
    }

    public void setNetworkSavings(Double newValue)
    {
        this.setNetworkSavings(newValue,this.UserSecurityID);


    }
    public Double getNetworkSavings()
    {
        return this.getNetworkSavings(this.UserSecurityID);
    }

    public void setNetworkSavings(Double newValue, Integer GroupRefID)
    {
        Double myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='NetworkSavings' AND TableName='tMCMC_Retros'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[NetworkSavings]=["+newValue+"]");
                   dbDB.setNetworkSavings(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[NetworkSavings]=["+newValue+"]");
           dbDB.setNetworkSavings(newValue);
         }
    }
    public Double getNetworkSavings(Integer GroupRefID)
    {
        Double myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='NetworkSavings' AND TableName='tMCMC_Retros'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getNetworkSavings();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getNetworkSavings();
         }
        return myVal;
    }

    public void setReasonCode1(String newValue)
    {
        this.setReasonCode1(newValue,this.UserSecurityID);


    }
    public String getReasonCode1()
    {
        return this.getReasonCode1(this.UserSecurityID);
    }

    public void setReasonCode1(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ReasonCode1' AND TableName='tMCMC_Retros'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[ReasonCode1]=["+newValue+"]");
                   dbDB.setReasonCode1(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[ReasonCode1]=["+newValue+"]");
           dbDB.setReasonCode1(newValue);
         }
    }
    public String getReasonCode1(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ReasonCode1' AND TableName='tMCMC_Retros'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getReasonCode1();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getReasonCode1();
         }
        return myVal;
    }

    public void setReasonCode2(String newValue)
    {
        this.setReasonCode2(newValue,this.UserSecurityID);


    }
    public String getReasonCode2()
    {
        return this.getReasonCode2(this.UserSecurityID);
    }

    public void setReasonCode2(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ReasonCode2' AND TableName='tMCMC_Retros'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[ReasonCode2]=["+newValue+"]");
                   dbDB.setReasonCode2(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[ReasonCode2]=["+newValue+"]");
           dbDB.setReasonCode2(newValue);
         }
    }
    public String getReasonCode2(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ReasonCode2' AND TableName='tMCMC_Retros'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getReasonCode2();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getReasonCode2();
         }
        return myVal;
    }

    public void setReasonCode3(String newValue)
    {
        this.setReasonCode3(newValue,this.UserSecurityID);


    }
    public String getReasonCode3()
    {
        return this.getReasonCode3(this.UserSecurityID);
    }

    public void setReasonCode3(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ReasonCode3' AND TableName='tMCMC_Retros'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[ReasonCode3]=["+newValue+"]");
                   dbDB.setReasonCode3(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[ReasonCode3]=["+newValue+"]");
           dbDB.setReasonCode3(newValue);
         }
    }
    public String getReasonCode3(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ReasonCode3' AND TableName='tMCMC_Retros'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getReasonCode3();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getReasonCode3();
         }
        return myVal;
    }

    public void setReasonCode4(String newValue)
    {
        this.setReasonCode4(newValue,this.UserSecurityID);


    }
    public String getReasonCode4()
    {
        return this.getReasonCode4(this.UserSecurityID);
    }

    public void setReasonCode4(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ReasonCode4' AND TableName='tMCMC_Retros'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[ReasonCode4]=["+newValue+"]");
                   dbDB.setReasonCode4(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[ReasonCode4]=["+newValue+"]");
           dbDB.setReasonCode4(newValue);
         }
    }
    public String getReasonCode4(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ReasonCode4' AND TableName='tMCMC_Retros'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getReasonCode4();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getReasonCode4();
         }
        return myVal;
    }

    public void setRejectDescription(String newValue)
    {
        this.setRejectDescription(newValue,this.UserSecurityID);


    }
    public String getRejectDescription()
    {
        return this.getRejectDescription(this.UserSecurityID);
    }

    public void setRejectDescription(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='RejectDescription' AND TableName='tMCMC_Retros'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[RejectDescription]=["+newValue+"]");
                   dbDB.setRejectDescription(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[RejectDescription]=["+newValue+"]");
           dbDB.setRejectDescription(newValue);
         }
    }
    public String getRejectDescription(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='RejectDescription' AND TableName='tMCMC_Retros'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getRejectDescription();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getRejectDescription();
         }
        return myVal;
    }

    public void setStatusID(Integer newValue)
    {
        this.setStatusID(newValue,this.UserSecurityID);


    }
    public Integer getStatusID()
    {
        return this.getStatusID(this.UserSecurityID);
    }

    public void setStatusID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='StatusID' AND TableName='tMCMC_Retros'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[StatusID]=["+newValue+"]");
                   dbDB.setStatusID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[StatusID]=["+newValue+"]");
           dbDB.setStatusID(newValue);
         }
    }
    public Integer getStatusID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='StatusID' AND TableName='tMCMC_Retros'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getStatusID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getStatusID();
         }
        return myVal;
    }

    public void setRequiredFieldList(java.util.Hashtable requiredFieldList)
	 {

        this.requiredFieldList = requiredFieldList;
    }

    public java.util.Hashtable getRequiredFieldList()
    {
        return this.requiredFieldList;
    }
    public void setExpiredFieldList(java.util.Hashtable expiredFieldList)
    {
        this.expiredFieldList = expiredFieldList;
    }

    public void setEnglishFieldList(java.util.Hashtable englishFieldList)
    {
        this.englishFieldList = englishFieldList;
    }

    public java.util.Hashtable getExpiredFieldList()
    {
        return this.expiredFieldList;
    }
    public boolean isExpired(java.util.Date myDate, int daysVector)
    {
       return isExpired(myDate,new java.util.Date(),daysVector);
    }


    public boolean isRead(String fieldN, Integer GroupRefID)
    {
        boolean myVal = false;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='"+fieldN+"' AND TableName='tMCMC_Retros'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==1)
               {
                   myVal = true;
               }
            }
        }
        else
        {

           if (iSecurityCheck.intValue()>=1)
           {
                  myVal = true;
           }
        }
        return myVal;
    }


    public boolean isWrite(String fieldN, Integer GroupRefID)
    {
        boolean myVal = false;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='"+fieldN+"' AND TableName='tMCMC_Retros'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {
                   myVal = true;
               }
            }
        }
        else
        {

           if (iSecurityCheck.intValue()>=2)
           {
                  myVal = true;
           }
        }
        return myVal;
    }


    public boolean isExpired(java.util.Date myDate, java.util.Date refDate,int daysVector)
    {
        java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat("yyyy-MM-dd");
        boolean myVal = true;
        try
        {
            if (myDate.after(dbdf.parse("1800-01-10")))
            {
                if (myDate.after(dateChange(refDate,daysVector)))
                {
                    myVal=false;
                }
            }
            else
            {
                myVal = false;
            }
        }
        catch (Exception Datee)
        {
        }
        return myVal;
    }

    public java.util.Date dateChange(java.util.Date myD, int dayChange)
    {
        java.util.Calendar calendar = java.util.Calendar.getInstance();
        calendar.setTime(myD);
        calendar.add(java.util.Calendar.DATE, dayChange);
        java.util.Date resultAsDate = new java.util.Date(calendar.getTime().getTime());
        return resultAsDate;
    }

    public boolean isRequired(String fieldN)
    {
        return isRequired(fieldN, new Integer("1"));
    }

    public boolean isRequired(String fieldN, Integer GroupRefID)
    {
        boolean myVal = false;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='"+fieldN+"' AND TableName='tMCMC_Retros'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getRequiredID().intValue()==1)
               {
                   myVal = true;
               }
            }
        }
        else
        {
            if (this.getRequiredFieldList().containsKey(fieldN))
            {
                myVal = true;
            }
        }
        return myVal;
    }
    public boolean isExpiredCheck(String fieldN)
    {
        return isExpiredCheck(fieldN, new Integer ("1"));
    }

    public boolean isExpiredCheck(String fieldN, Integer GroupRefID)
    {
        boolean myVal = false;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='"+fieldN+"' AND TableName='tMCMC_Retros'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getTrackedID().intValue()==1)
               {
                   myVal = true;
               }
            }
        }
        else
        {
            if (this.getExpiredFieldList().containsKey(fieldN))
           {
                myVal = true;
            }
        }
        return myVal;
    }

    public void makeRequiredHash()
    {
       makeRequiredHash(new Integer(1));
    }

    public void makeRequiredHash(Integer GroupRefID)
    {

        java.util.Hashtable newHash = new java.util.Hashtable();
        if (runFieldSecurity)
        {
            java.util.Enumeration myKeys = englishFieldList.keys(); 
            while (myKeys.hasMoreElements()) 
            {
                 String myElement = (String) myKeys.nextElement();
                 bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='"+myElement+"' AND TableName='tMCMC_Retros'" ,"");
                 bltFieldSecurity        working_bltFieldSecurity;
                 ListElement         leCurrentElement;
                 Enumeration eList = myFSL.elements();
                 if (eList.hasMoreElements())
                 {
                     leCurrentElement    = (ListElement) eList.nextElement();
                     working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
                     if (working_bltFieldSecurity.getRequiredID().intValue()==1)
                     {
                         newHash.put(myElement,new Boolean(true));
                     }
                 }
            }
        }
        else
        {
        }
        this.setRequiredFieldList(newHash);
    }

    public void makeEnglishHash()
    {

        java.util.Hashtable newHash = new java.util.Hashtable();        newHash.put("UniqueCreateDate","UniqueCreateDate");
        newHash.put("UniqueModifyDate","UniqueModifyDate");
        newHash.put("UniqueModifyComments","UniqueModifyComments");
        newHash.put("MCMCBillID","MCMCBillID");
        newHash.put("NetworkBillID","NetworkBillID");
        newHash.put("MCMCLocation","MCMCLocation");
        newHash.put("RenderingProviderTaxID","RenderingProviderTaxID");
        newHash.put("RenderingProviderName","RenderingProviderName");
        newHash.put("RenderingProviderAddress","RenderingProviderAddress");
        newHash.put("RenderingProviderCity","RenderingProviderCity");
        newHash.put("RenderingProviderState","RenderingProviderState");
        newHash.put("RenderingProviderZip","RenderingProviderZip");
        newHash.put("BillingProviderTaxID","BillingProviderTaxID");
        newHash.put("BillingProviderName","BillingProviderName");
        newHash.put("BillingProviderAddress","BillingProviderAddress");
        newHash.put("BillingProviderCity","BillingProviderCity");
        newHash.put("BillingProviderState","BillingProviderState");
        newHash.put("BillingProviderZip","BillingProviderZip");
        newHash.put("ClientName","ClientName");
        newHash.put("ClaimNumber","ClaimNumber");
        newHash.put("PatientSSN","PatientSSN");
        newHash.put("PatientFirstName","PatientFirstName");
        newHash.put("PatientLastName","PatientLastName");
        newHash.put("PatientAddress1","PatientAddress1");
        newHash.put("PatientAddress2","PatientAddress2");
        newHash.put("PatientCity","PatientCity");
        newHash.put("PatientState","PatientState");
        newHash.put("PatientZip","PatientZip");
        newHash.put("EmployerName","EmployerName");
        newHash.put("EmployerAddress1","EmployerAddress1");
        newHash.put("EmployerAddress2","EmployerAddress2");
        newHash.put("EmployerCity","EmployerCity");
        newHash.put("EmployerState","EmployerState");
        newHash.put("EmployerZip","EmployerZip");
        newHash.put("DiagnosisCode1","DiagnosisCode1");
        newHash.put("DiagnosisCode2","DiagnosisCode2");
        newHash.put("DiagnosisCode3","DiagnosisCode3");
        newHash.put("DiagnosisCode4","DiagnosisCode4");
        newHash.put("DateOfBirth","DateOfBirth");
        newHash.put("InjuryDate","InjuryDate");
        newHash.put("ReviewState","ReviewState");
        newHash.put("ReviewZip","ReviewZip");
        newHash.put("LineID","LineID");
        newHash.put("FSType","FSType");
        newHash.put("DateOfService","DateOfService");
        newHash.put("CPTCode","CPTCode");
        newHash.put("Modifier1","Modifier1");
        newHash.put("Modifier2","Modifier2");
        newHash.put("Units","Units");
        newHash.put("ProviderCharge","ProviderCharge");
        newHash.put("StateAllowance","StateAllowance");
        newHash.put("MCMCAllowance","MCMCAllowance");
        newHash.put("NetworkAllowance","NetworkAllowance");
        newHash.put("NetworkSavings","NetworkSavings");
        newHash.put("ReasonCode1","ReasonCode1");
        newHash.put("ReasonCode2","ReasonCode2");
        newHash.put("ReasonCode3","ReasonCode3");
        newHash.put("ReasonCode4","ReasonCode4");
        newHash.put("RejectDescription","RejectDescription");
        newHash.put("StatusID","StatusID");

        this.setEnglishFieldList(newHash);
    }
    public void makeExpiredHash()
    {

        java.util.Hashtable newHash = new java.util.Hashtable();
        this.setExpiredFieldList(newHash);
    }

    public java.util.Hashtable englishFieldList = new java.util.Hashtable();
    public java.util.Hashtable requiredFieldList = new java.util.Hashtable();
    public java.util.Hashtable expiredFieldList = new java.util.Hashtable();

    public boolean isComplete()
    {
	     boolean myVal = true;
	     java.util.Enumeration myKeys = this.getRequiredFieldList().keys(); 
	     while (myKeys.hasMoreElements()) 
	     {
	         String myElement = (String) myKeys.nextElement();
	         if (!isComplete(myElement))
	         {
	             myVal = false;
	         }
	     }
	     return myVal;
    }

    public String getEnglish(String fieldN)
    {
        String myVal = null;
        if (this.englishFieldList.containsKey(fieldN))
        {
            myVal = (String)this.englishFieldList.get(fieldN);
        }
        return myVal;
    }
    public java.util.Vector isComplete(int daysVector)
    {
	     boolean myVal = false;
	     java.util.Enumeration myKeys = this.getExpiredFieldList().keys(); 
	     java.util.Vector myExpiredList = new java.util.Vector(); 
	     while (myKeys.hasMoreElements()) 
	     {
	         String myElement = (String) myKeys.nextElement();
	         if (isExpired(myElement,daysVector))
	         {
	             myVal = true;
	             myExpiredList.addElement(myElement);
	         }
	     }
	     return myExpiredList;
    }

    public void setField(String fieldN, Object fieldV)
    {
     boolean myVal = false;
     try
     {
        java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat("yyyy-MM-dd");

        if (fieldN.equals("MCMCID"))
        {
             this.setMCMCID((Integer)fieldV);
        }

        else if (fieldN.equals("UniqueCreateDate"))
        {
            this.setUniqueCreateDate((java.util.Date)fieldV);
        }

        else if (fieldN.equals("UniqueModifyDate"))
        {
            this.setUniqueModifyDate((java.util.Date)fieldV);
        }

        else if (fieldN.equals("UniqueModifyComments"))
        {
            this.setUniqueModifyComments((String)fieldV);
        }

        else if (fieldN.equals("MCMCBillID"))
        {
             this.setMCMCBillID((Integer)fieldV);
        }

        else if (fieldN.equals("NetworkBillID"))
        {
             this.setNetworkBillID((Integer)fieldV);
        }

        else if (fieldN.equals("MCMCLocation"))
        {
            this.setMCMCLocation((String)fieldV);
        }

        else if (fieldN.equals("RenderingProviderTaxID"))
        {
            this.setRenderingProviderTaxID((String)fieldV);
        }

        else if (fieldN.equals("RenderingProviderName"))
        {
            this.setRenderingProviderName((String)fieldV);
        }

        else if (fieldN.equals("RenderingProviderAddress"))
        {
            this.setRenderingProviderAddress((String)fieldV);
        }

        else if (fieldN.equals("RenderingProviderCity"))
        {
            this.setRenderingProviderCity((String)fieldV);
        }

        else if (fieldN.equals("RenderingProviderState"))
        {
            this.setRenderingProviderState((String)fieldV);
        }

        else if (fieldN.equals("RenderingProviderZip"))
        {
            this.setRenderingProviderZip((String)fieldV);
        }

        else if (fieldN.equals("BillingProviderTaxID"))
        {
            this.setBillingProviderTaxID((String)fieldV);
        }

        else if (fieldN.equals("BillingProviderName"))
        {
            this.setBillingProviderName((String)fieldV);
        }

        else if (fieldN.equals("BillingProviderAddress"))
        {
            this.setBillingProviderAddress((String)fieldV);
        }

        else if (fieldN.equals("BillingProviderCity"))
        {
            this.setBillingProviderCity((String)fieldV);
        }

        else if (fieldN.equals("BillingProviderState"))
        {
            this.setBillingProviderState((String)fieldV);
        }

        else if (fieldN.equals("BillingProviderZip"))
        {
            this.setBillingProviderZip((String)fieldV);
        }

        else if (fieldN.equals("ClientName"))
        {
            this.setClientName((String)fieldV);
        }

        else if (fieldN.equals("ClaimNumber"))
        {
            this.setClaimNumber((String)fieldV);
        }

        else if (fieldN.equals("PatientSSN"))
        {
            this.setPatientSSN((String)fieldV);
        }

        else if (fieldN.equals("PatientFirstName"))
        {
            this.setPatientFirstName((String)fieldV);
        }

        else if (fieldN.equals("PatientLastName"))
        {
            this.setPatientLastName((String)fieldV);
        }

        else if (fieldN.equals("PatientAddress1"))
        {
            this.setPatientAddress1((String)fieldV);
        }

        else if (fieldN.equals("PatientAddress2"))
        {
            this.setPatientAddress2((String)fieldV);
        }

        else if (fieldN.equals("PatientCity"))
        {
            this.setPatientCity((String)fieldV);
        }

        else if (fieldN.equals("PatientState"))
        {
            this.setPatientState((String)fieldV);
        }

        else if (fieldN.equals("PatientZip"))
        {
            this.setPatientZip((String)fieldV);
        }

        else if (fieldN.equals("EmployerName"))
        {
            this.setEmployerName((String)fieldV);
        }

        else if (fieldN.equals("EmployerAddress1"))
        {
            this.setEmployerAddress1((String)fieldV);
        }

        else if (fieldN.equals("EmployerAddress2"))
        {
            this.setEmployerAddress2((String)fieldV);
        }

        else if (fieldN.equals("EmployerCity"))
        {
            this.setEmployerCity((String)fieldV);
        }

        else if (fieldN.equals("EmployerState"))
        {
            this.setEmployerState((String)fieldV);
        }

        else if (fieldN.equals("EmployerZip"))
        {
            this.setEmployerZip((String)fieldV);
        }

        else if (fieldN.equals("DiagnosisCode1"))
        {
            this.setDiagnosisCode1((String)fieldV);
        }

        else if (fieldN.equals("DiagnosisCode2"))
        {
            this.setDiagnosisCode2((String)fieldV);
        }

        else if (fieldN.equals("DiagnosisCode3"))
        {
            this.setDiagnosisCode3((String)fieldV);
        }

        else if (fieldN.equals("DiagnosisCode4"))
        {
            this.setDiagnosisCode4((String)fieldV);
        }

        else if (fieldN.equals("DateOfBirth"))
        {
            this.setDateOfBirth((String)fieldV);
        }

        else if (fieldN.equals("InjuryDate"))
        {
            this.setInjuryDate((String)fieldV);
        }

        else if (fieldN.equals("ReviewState"))
        {
            this.setReviewState((String)fieldV);
        }

        else if (fieldN.equals("ReviewZip"))
        {
            this.setReviewZip((String)fieldV);
        }

        else if (fieldN.equals("LineID"))
        {
             this.setLineID((Integer)fieldV);
        }

        else if (fieldN.equals("FSType"))
        {
            this.setFSType((String)fieldV);
        }

        else if (fieldN.equals("DateOfService"))
        {
            this.setDateOfService((String)fieldV);
        }

        else if (fieldN.equals("CPTCode"))
        {
            this.setCPTCode((String)fieldV);
        }

        else if (fieldN.equals("Modifier1"))
        {
            this.setModifier1((String)fieldV);
        }

        else if (fieldN.equals("Modifier2"))
        {
            this.setModifier2((String)fieldV);
        }

        else if (fieldN.equals("Units"))
        {
            this.setUnits((Double)fieldV);
        }

        else if (fieldN.equals("ProviderCharge"))
        {
            this.setProviderCharge((Double)fieldV);
        }

        else if (fieldN.equals("StateAllowance"))
        {
            this.setStateAllowance((Double)fieldV);
        }

        else if (fieldN.equals("MCMCAllowance"))
        {
            this.setMCMCAllowance((Double)fieldV);
        }

        else if (fieldN.equals("NetworkAllowance"))
        {
            this.setNetworkAllowance((Double)fieldV);
        }

        else if (fieldN.equals("NetworkSavings"))
        {
            this.setNetworkSavings((Double)fieldV);
        }

        else if (fieldN.equals("ReasonCode1"))
        {
            this.setReasonCode1((String)fieldV);
        }

        else if (fieldN.equals("ReasonCode2"))
        {
            this.setReasonCode2((String)fieldV);
        }

        else if (fieldN.equals("ReasonCode3"))
        {
            this.setReasonCode3((String)fieldV);
        }

        else if (fieldN.equals("ReasonCode4"))
        {
            this.setReasonCode4((String)fieldV);
        }

        else if (fieldN.equals("RejectDescription"))
        {
            this.setRejectDescription((String)fieldV);
        }

        else if (fieldN.equals("StatusID"))
        {
             this.setStatusID((Integer)fieldV);
        }

	    }
	    catch(Exception e2)
	    {}
	    //return myVal;
	}

    public String getFieldType(String fieldN)
    {
     String  myVal = null;
     try
     {
        java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat("yyyy-MM-dd");

        if (fieldN.equals("MCMCID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("UniqueCreateDate"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("UniqueModifyDate"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("UniqueModifyComments"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("MCMCBillID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("NetworkBillID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("MCMCLocation"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("RenderingProviderTaxID"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("RenderingProviderName"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("RenderingProviderAddress"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("RenderingProviderCity"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("RenderingProviderState"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("RenderingProviderZip"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("BillingProviderTaxID"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("BillingProviderName"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("BillingProviderAddress"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("BillingProviderCity"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("BillingProviderState"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("BillingProviderZip"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("ClientName"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("ClaimNumber"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("PatientSSN"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("PatientFirstName"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("PatientLastName"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("PatientAddress1"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("PatientAddress2"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("PatientCity"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("PatientState"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("PatientZip"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("EmployerName"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("EmployerAddress1"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("EmployerAddress2"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("EmployerCity"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("EmployerState"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("EmployerZip"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("DiagnosisCode1"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("DiagnosisCode2"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("DiagnosisCode3"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("DiagnosisCode4"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("DateOfBirth"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("InjuryDate"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("ReviewState"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("ReviewZip"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("LineID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("FSType"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("DateOfService"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("CPTCode"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("Modifier1"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("Modifier2"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("Units"))
        {
            myVal = "Double";
        }

        else if (fieldN.equals("ProviderCharge"))
        {
            myVal = "Double";
        }

        else if (fieldN.equals("StateAllowance"))
        {
            myVal = "Double";
        }

        else if (fieldN.equals("MCMCAllowance"))
        {
            myVal = "Double";
        }

        else if (fieldN.equals("NetworkAllowance"))
        {
            myVal = "Double";
        }

        else if (fieldN.equals("NetworkSavings"))
        {
            myVal = "Double";
        }

        else if (fieldN.equals("ReasonCode1"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("ReasonCode2"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("ReasonCode3"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("ReasonCode4"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("RejectDescription"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("StatusID"))
        {
            myVal = "Integer";
        }

	    }
	    catch(Exception e2)
	    {}
	    return myVal;
	}

    public boolean isComplete(String fieldN)
    {
	     boolean myVal = false;
	     try
	     {
        java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat("yyyy-MM-dd");

	     if (fieldN.equals("MCMCID"))

	     {
                    if (this.getMCMCID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("UniqueCreateDate"))

	     {
                    if (this.getUniqueCreateDate().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("UniqueModifyDate"))

	     {
                    if (this.getUniqueModifyDate().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("UniqueModifyComments"))

	     {
                    if (this.getUniqueModifyComments().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("MCMCBillID"))

	     {
                    if (this.getMCMCBillID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("NetworkBillID"))

	     {
                    if (this.getNetworkBillID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("MCMCLocation"))

	     {
                    if (this.getMCMCLocation().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("RenderingProviderTaxID"))

	     {
                    if (this.getRenderingProviderTaxID().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("RenderingProviderName"))

	     {
                    if (this.getRenderingProviderName().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("RenderingProviderAddress"))

	     {
                    if (this.getRenderingProviderAddress().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("RenderingProviderCity"))

	     {
                    if (this.getRenderingProviderCity().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("RenderingProviderState"))

	     {
                    if (this.getRenderingProviderState().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("RenderingProviderZip"))

	     {
                    if (this.getRenderingProviderZip().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("BillingProviderTaxID"))

	     {
                    if (this.getBillingProviderTaxID().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("BillingProviderName"))

	     {
                    if (this.getBillingProviderName().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("BillingProviderAddress"))

	     {
                    if (this.getBillingProviderAddress().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("BillingProviderCity"))

	     {
                    if (this.getBillingProviderCity().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("BillingProviderState"))

	     {
                    if (this.getBillingProviderState().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("BillingProviderZip"))

	     {
                    if (this.getBillingProviderZip().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("ClientName"))

	     {
                    if (this.getClientName().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("ClaimNumber"))

	     {
                    if (this.getClaimNumber().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("PatientSSN"))

	     {
                    if (this.getPatientSSN().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("PatientFirstName"))

	     {
                    if (this.getPatientFirstName().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("PatientLastName"))

	     {
                    if (this.getPatientLastName().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("PatientAddress1"))

	     {
                    if (this.getPatientAddress1().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("PatientAddress2"))

	     {
                    if (this.getPatientAddress2().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("PatientCity"))

	     {
                    if (this.getPatientCity().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("PatientState"))

	     {
                    if (this.getPatientState().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("PatientZip"))

	     {
                    if (this.getPatientZip().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("EmployerName"))

	     {
                    if (this.getEmployerName().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("EmployerAddress1"))

	     {
                    if (this.getEmployerAddress1().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("EmployerAddress2"))

	     {
                    if (this.getEmployerAddress2().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("EmployerCity"))

	     {
                    if (this.getEmployerCity().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("EmployerState"))

	     {
                    if (this.getEmployerState().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("EmployerZip"))

	     {
                    if (this.getEmployerZip().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("DiagnosisCode1"))

	     {
                    if (this.getDiagnosisCode1().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("DiagnosisCode2"))

	     {
                    if (this.getDiagnosisCode2().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("DiagnosisCode3"))

	     {
                    if (this.getDiagnosisCode3().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("DiagnosisCode4"))

	     {
                    if (this.getDiagnosisCode4().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("DateOfBirth"))

	     {
                    if (this.getDateOfBirth().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("InjuryDate"))

	     {
                    if (this.getInjuryDate().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("ReviewState"))

	     {
                    if (this.getReviewState().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("ReviewZip"))

	     {
                    if (this.getReviewZip().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("LineID"))

	     {
                    if (this.getLineID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("FSType"))

	     {
                    if (this.getFSType().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("DateOfService"))

	     {
                    if (this.getDateOfService().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("CPTCode"))

	     {
                    if (this.getCPTCode().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("Modifier1"))

	     {
                    if (this.getModifier1().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("Modifier2"))

	     {
                    if (this.getModifier2().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("Units"))

	     {
                    if (this.getUnits().equals(new Double("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("ProviderCharge"))

	     {
                    if (this.getProviderCharge().equals(new Double("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("StateAllowance"))

	     {
                    if (this.getStateAllowance().equals(new Double("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("MCMCAllowance"))

	     {
                    if (this.getMCMCAllowance().equals(new Double("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("NetworkAllowance"))

	     {
                    if (this.getNetworkAllowance().equals(new Double("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("NetworkSavings"))

	     {
                    if (this.getNetworkSavings().equals(new Double("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("ReasonCode1"))

	     {
                    if (this.getReasonCode1().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("ReasonCode2"))

	     {
                    if (this.getReasonCode2().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("ReasonCode3"))

	     {
                    if (this.getReasonCode3().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("ReasonCode4"))

	     {
                    if (this.getReasonCode4().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("RejectDescription"))

	     {
                    if (this.getRejectDescription().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("StatusID"))

	     {
                    if (this.getStatusID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	    }
	    catch(Exception e2)
	    {}
	    return myVal;
	}

    public boolean isExpired(String fieldN,int daysVector)
    {
        return isExpired(fieldN,new java.util.Date(),daysVector);
    }

    public boolean isExpired(String fieldN,java.util.Date refDate,int daysVector)
    {
	     boolean myVal = false;
	     try
	     {
        java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat("yyyy-MM-dd");

        if (fieldN.equals("UniqueCreateDate"))

	     {
                    if (this.isExpired(this.getUniqueCreateDate(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

        if (fieldN.equals("UniqueModifyDate"))

	     {
                    if (this.isExpired(this.getUniqueModifyDate(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

	    }
	    catch(Exception e2)
	    {}
	    return myVal;
	}

    public java.util.Date getDateField(String fieldN)
    {
	     java.util.Date myVal = null;
	     try
	     {
        java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat("yyyy-MM-dd");

        if (fieldN.equals("UniqueCreateDate"))

	     {
             myVal = this.getUniqueCreateDate();
        }

        if (fieldN.equals("UniqueModifyDate"))

	     {
             myVal = this.getUniqueModifyDate();
        }

	    }
	    catch(Exception e2)
	    {}
	    return myVal;
	}
	public boolean runFieldSecurity = false;
	public Integer UserSecurityID = null;
   public Integer iSecurityCheck = null;
}    // End of bltMCMC_Retros class definition
