

package com.winstaff;

import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public class dbtCaseAccountStatusLI extends Object implements InttCaseAccountStatusLI
{

        db_NewBase    dbnbDB;

    public dbtCaseAccountStatusLI()
    {
        dbnbDB = new db_NewBase( "tCaseAccountStatusLI", "CaseStatusID" );

    }    // End of default constructor

    public dbtCaseAccountStatusLI( Integer iNewID )
    {
        dbnbDB = new db_NewBase( "tCaseAccountStatusLI", "CaseStatusID" );
        setUniqueID( iNewID );
    }    // End of Constructor knowing an ID

    public void commitData() throws SQLException
    {
            //no longer trapping/ignoring errors 2011-03-17
            dbnbDB.commitData();
    }    // End of commitData()

    public void commitDataForced() throws SQLException
    {
        dbnbDB.commitDataForced();
    }    // End of commitData()

    public Integer getUniqueID()
    {
        return dbnbDB.getUniqueID();
    }    // End of getUniqueID()

    public void setUniqueID( Integer iNewID )
    {
        dbnbDB.setUniqueID( iNewID );
    }    // End of setUniqueID()

    public void setModifyComments( String sNew )
    {
        dbnbDB.setModifyComments( sNew );
    }    // End of setUniqueID()


    public void setCaseStatusID(Integer newValue)
    {
                dbnbDB.setFieldData( "CaseStatusID", newValue.toString() );
    }

    public Integer getCaseStatusID()
    {
        String           sValue = dbnbDB.getFieldData( "CaseStatusID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setField(String fieldN, Object fieldV)
    {DebugLogger.println("setField Method not implemented yet in class!");}

    public String getFieldType(String fieldN)
    {return null;}


    public void setCaseStatusShort(String newValue)
    {
                dbnbDB.setFieldData( "CaseStatusShort", newValue.toString() );
    }

    public String getCaseStatusShort()
    {
        String           sValue = dbnbDB.getFieldData( "CaseStatusShort" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setCaseStatusLong(String newValue)
    {
                dbnbDB.setFieldData( "CaseStatusLong", newValue.toString() );
    }

    public String getCaseStatusLong()
    {
        String           sValue = dbnbDB.getFieldData( "CaseStatusLong" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

}    // End of bltCaseAccountStatusLI class definition
