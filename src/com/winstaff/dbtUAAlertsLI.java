

package com.winstaff;

import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public class dbtUAAlertsLI extends Object implements InttUAAlertsLI
{

        db_NewBase    dbnbDB;

    public dbtUAAlertsLI()
    {
        dbnbDB = new db_NewBase( "tUAAlertsLI", "UAAlertsID" );

    }    // End of default constructor

    public dbtUAAlertsLI( Integer iNewID )
    {
        dbnbDB = new db_NewBase( "tUAAlertsLI", "UAAlertsID" );
        setUniqueID( iNewID );
    }    // End of Constructor knowing an ID

    public void commitData() throws SQLException
    {
            //no longer trapping/ignoring errors 2011-03-17
            dbnbDB.commitData();
    }    // End of commitData()

    public void commitDataForced() throws SQLException
    {
        dbnbDB.commitDataForced();
    }    // End of commitData()

    public Integer getUniqueID()
    {
        return dbnbDB.getUniqueID();
    }    // End of getUniqueID()

    public void setUniqueID( Integer iNewID )
    {
        dbnbDB.setUniqueID( iNewID );
    }    // End of setUniqueID()

    public void setModifyComments( String sNew )
    {
        dbnbDB.setModifyComments( sNew );
    }    // End of setUniqueID()


    public void setUAAlertsID(Integer newValue)
    {
                dbnbDB.setFieldData( "UAAlertsID", newValue.toString() );
    }

    public Integer getUAAlertsID()
    {
        String           sValue = dbnbDB.getFieldData( "UAAlertsID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setField(String fieldN, Object fieldV)
    {DebugLogger.println("setField Method not implemented yet in class!");}

    public String getFieldType(String fieldN)
    {return null;}


    public void setUAAlertsLong(String newValue)
    {
                dbnbDB.setFieldData( "UAAlertsLong", newValue.toString() );
    }

    public String getUAAlertsLong()
    {
        String           sValue = dbnbDB.getFieldData( "UAAlertsLong" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

}    // End of bltUAAlertsLI class definition
