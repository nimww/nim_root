package com.winstaff;


import com.winstaff.dbtOtherCertification;
import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public interface InttOtherCertification extends dbTableInterface
{

    public void setOtherCertID(Integer newValue);
    public Integer getOtherCertID();
    public void setUniqueCreateDate(Date newValue);
    public Date getUniqueCreateDate();
    public void setUniqueModifyDate(Date newValue);
    public Date getUniqueModifyDate();
    public void setUniqueModifyComments(String newValue);
    public String getUniqueModifyComments();
    public void setPhysicianID(Integer newValue);
    public Integer getPhysicianID();
    public void setCertificationType(String newValue);
    public String getCertificationType();
    public void setCertificationNumber(String newValue);
    public String getCertificationNumber();
    public void setExpirationDate(Date newValue);
    public Date getExpirationDate();
    public void setName(String newValue);
    public String getName();
    public void setAddress1(String newValue);
    public String getAddress1();
    public void setAddress2(String newValue);
    public String getAddress2();
    public void setCity(String newValue);
    public String getCity();
    public void setStateID(Integer newValue);
    public Integer getStateID();
    public void setProvince(String newValue);
    public String getProvince();
    public void setZIP(String newValue);
    public String getZIP();
    public void setCountryID(Integer newValue);
    public Integer getCountryID();
    public void setPhone(String newValue);
    public String getPhone();
    public void setFax(String newValue);
    public String getFax();
    public void setContactName(String newValue);
    public String getContactName();
    public void setContactEmail(String newValue);
    public String getContactEmail();
    public void setDocuLinkID(Integer newValue);
    public Integer getDocuLinkID();
    public void setComments(String newValue);
    public String getComments();
}    // End of bltOtherCertification class definition
