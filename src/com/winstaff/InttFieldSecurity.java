package com.winstaff;


import com.winstaff.dbtFieldSecurity;
import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public interface InttFieldSecurity extends dbTableInterface
{

    public void setFieldSecurityID(Integer newValue);
    public Integer getFieldSecurityID();
    public void setUniqueCreateDate(Date newValue);
    public Date getUniqueCreateDate();
    public void setUniqueModifyDate(Date newValue);
    public Date getUniqueModifyDate();
    public void setUniqueModifyComments(String newValue);
    public String getUniqueModifyComments();
    public void setSecurityGroupID(Integer newValue);
    public Integer getSecurityGroupID();
    public void setTableName(String newValue);
    public String getTableName();
    public void setFieldName(String newValue);
    public String getFieldName();
    public void setRequiredID(Integer newValue);
    public Integer getRequiredID();
    public void setTrackedID(Integer newValue);
    public Integer getTrackedID();
    public void setAccessLevel(Integer newValue);
    public Integer getAccessLevel();
}    // End of bltFieldSecurity class definition
