

package com.winstaff;

import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public interface InttServiceStatusLI extends dbTableInterface
{

    public void setServiceStatusID(Integer newValue);
    public Integer getServiceStatusID();
    public void setServiceStatusShort(String newValue);
    public String getServiceStatusShort();
    public void setServiceStatusLong(String newValue);
    public String getServiceStatusLong();
}    // End of bltServiceStatusLI class definition
