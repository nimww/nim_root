package com.winstaff;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: btdev
 * Date: 3/17/13
 * Time: 7:42 PM
 * To change this template use File | Settings | File Templates.
 */
public class NIM3_CPTWizard{


    public static ArrayList<String> getWizardOptions(){
        ArrayList myOptions = new ArrayList<String>();
        myOptions.add("Please Select...");
        String mySQL = "select modality from tcptwizard  where allownid=1 group by modality order by modality";
        searchDB2 mySS = new searchDB2();
        java.sql.ResultSet myRS = null;
        try
        {
            db_PreparedStatementObject myDPSO = new db_PreparedStatementObject(mySQL);
//            myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,iPayerID.toString()));
            myRS = mySS.executePreparedStatement(myDPSO);
            while(myRS!=null&&myRS.next())
            {
                myOptions.add(myRS.getString(1));

            }
            mySS.closeAll();
        } catch(Exception e) {
            DebugLogger.printLine("NIM3_CPTWizard:getWizardOptions:error:" + e);
        } finally {
            mySS.closeAll();
        }
        return myOptions;
    }

    public static ArrayList<String> getWizardOptions(String modality){
        ArrayList myOptions = new ArrayList<String>();
        myOptions.add("Please Select...");
        String mySQL = "select bodypart from tcptwizard where modality=? and allownid=1 group by modality, bodypart order by bodypart ";
        searchDB2 mySS = new searchDB2();
        java.sql.ResultSet myRS = null;
        try
        {
            db_PreparedStatementObject myDPSO = new db_PreparedStatementObject(mySQL);
            myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,modality));
            myRS = mySS.executePreparedStatement(myDPSO);
            while(myRS!=null&&myRS.next())
            {
                myOptions.add(myRS.getString(1));

            }
            mySS.closeAll();
        } catch(Exception e) {
            DebugLogger.printLine("NIM3_CPTWizard:getWizardOptions:error:" + e);
        } finally {
            mySS.closeAll();
        }
        return myOptions;
    }

    public static ArrayList<String> getWizardOptions(String modality, String bodyPart){
        ArrayList myOptions = new ArrayList<String>();
        myOptions.add("Please Select...");
        String mySQL = "select special from tcptwizard where modality=? and bodyPart=? and allownid=1 group by modality, bodyPart, special order by special ";
        searchDB2 mySS = new searchDB2();
        java.sql.ResultSet myRS = null;
        try
        {
            db_PreparedStatementObject myDPSO = new db_PreparedStatementObject(mySQL);
            myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,modality));
            myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,bodyPart));
            myRS = mySS.executePreparedStatement(myDPSO);
            while(myRS!=null&&myRS.next())
            {
                myOptions.add(myRS.getString(1));

            }
            mySS.closeAll();
        } catch(Exception e) {
            DebugLogger.printLine("NIM3_CPTWizard:getWizardOptions:error:" + e);
        } finally {
            mySS.closeAll();
        }
        return myOptions;
    }


    public static ArrayList<String> getWizardOptions(String modality, String bodyPart, String special){
        ArrayList myOptions = new ArrayList<String>();
        myOptions.add("Please Select...");
        String mySQL = "select contrast from tcptwizard where modality=? and bodypart=? and special=? and allownid=1 group by modality, bodypart, special, contrast order by contrast ";
        searchDB2 mySS = new searchDB2();
        java.sql.ResultSet myRS = null;
        try
        {
            db_PreparedStatementObject myDPSO = new db_PreparedStatementObject(mySQL);
            myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,modality));
            myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,bodyPart));
            myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,special));
            myRS = mySS.executePreparedStatement(myDPSO);
            while(myRS!=null&&myRS.next())
            {
                myOptions.add(myRS.getString(1));

            }
            mySS.closeAll();
        } catch(Exception e) {
            DebugLogger.printLine("NIM3_CPTWizard:getWizardOptions:error:" + e);
        } finally {
            mySS.closeAll();
        }
        return myOptions;
    }

    public static ArrayList<String> getWizardOptions(String modality, String bodyPart, String special, String contrast){
        ArrayList myOptions = new ArrayList<String>();
        myOptions.add("Please Select...");
        String mySQL = "select orientation from tcptwizard where modality=? and bodypart=? and special=? and contrast=? and allownid=1 group by modality, bodypart, special, contrast, orientation order by orientation";
        searchDB2 mySS = new searchDB2();
        java.sql.ResultSet myRS = null;
        try
        {
            db_PreparedStatementObject myDPSO = new db_PreparedStatementObject(mySQL);
            myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,modality));
            myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,bodyPart));
            myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,special));
            myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,contrast));
            myRS = mySS.executePreparedStatement(myDPSO);
            while(myRS!=null&&myRS.next())
            {
                myOptions.add(myRS.getString(1));
            }
            mySS.closeAll();
        } catch(Exception e) {
            DebugLogger.printLine("NIM3_CPTWizard:getWizardOptions:error:" + e);
        } finally {
            mySS.closeAll();
        }
        if (myOptions.size()==1){
            myOptions.add("");
        }
        return myOptions;
    }

    public static bltCPTWizard getWizardOptions(String modality, String bodyPart, String special, String contrast, String orientation){
        Integer myWizard_ID = null;
        String mySQL = "select cptwizardid from tcptwizard where modality=? and bodypart=? and special=? and contrast=? and orientation=? and allownid=1";
        searchDB2 mySS = new searchDB2();
        java.sql.ResultSet myRS = null;
        try
        {
            db_PreparedStatementObject myDPSO = new db_PreparedStatementObject(mySQL);
            myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,modality));
            myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,bodyPart));
            myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,special));
            myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,contrast));
            myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,orientation));
            myRS = mySS.executePreparedStatement(myDPSO);
            if(myRS!=null&&myRS.next())
            {
                myWizard_ID = new Integer(myRS.getString(1));
            }
            mySS.closeAll();
        } catch(Exception e) {
            DebugLogger.printLine("NIM3_CPTWizard:getWizardOptions:error:" + e);
        } finally {
            mySS.closeAll();
        }
        if (myWizard_ID!=null){
            return new bltCPTWizard(myWizard_ID);
        } else {
            return null;
        }
    }

}