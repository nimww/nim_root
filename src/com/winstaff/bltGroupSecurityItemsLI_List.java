package com.winstaff;
/*
 * bltGroupSecurityItemsLI_List.java
 *
 * Created: Wed Apr 02 15:09:22 PST 2003
 */



import java.sql.*;
import com.winstaff.dbtGroupSecurityItemsLI;
import com.winstaff.ListElement;


/**
 *
 * @author  Scott Ellis via listClassGenCode
 * @version 2.5
 */
public class bltGroupSecurityItemsLI_List extends db_BaseRelationTable
{
    /** Default constructor
    */    
    public bltGroupSecurityItemsLI_List ( Integer iSecurityGroupID )
    {
        super( "tGroupSecurityItemsLI", "GroupSecurityItemsID", "GroupSecurityItemsID", "SecurityGroupID", iSecurityGroupID );
    }   // End of bltGroupSecurityItemsLI_List()
    

    public void setField(String fieldN, Object fieldV)
    {DebugLogger.println("setField Method not implemented yet in classbltGroupSecurityItemsLI_List!");}

    public String getFieldType(String fieldN)
    {return null;}

    public bltGroupSecurityItemsLI_List ( Integer iSecurityGroupID, String extraWhere, String OrderBy )
    {
        super( "tGroupSecurityItemsLI", "GroupSecurityItemsID", "GroupSecurityItemsID", "SecurityGroupID", iSecurityGroupID, extraWhere,OrderBy );
    }   // End of bltGroupSecurityItemsLI_List()
     /** Method pull information from the result set and load into class attributes.
    * @param sFieldList Comma delimited list of fields for which data has been retrieved.
    * @param rs Result set from which to pull the data from.
    * @throws SQLException Passes on SQLException
    */      
    protected void transferData( ResultSet rs ) throws SQLException
    {
        while ( rs.next()  )
        {
            Integer         iValue              = new Integer( rs.getString( sLinkFieldName ) );
            Integer         iLinkRecordID       = new Integer( rs.getString( sUniqueIDFieldName ) );
            Object          oReferenceObject;
            ListElement     leNewElement;
            
            oReferenceObject    = new bltGroupSecurityItemsLI( iValue );
            
            leNewElement        = new ListElement( iLinkRecordID, oReferenceObject );
            
            vList.add( leNewElement );

        }
    }   // End of transferData()
}   // End of bltGroupSecurityItemsLI_List class

