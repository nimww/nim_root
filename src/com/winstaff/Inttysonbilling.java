

package com.winstaff;

import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public interface Inttysonbilling extends dbTableInterface
{

    public void setTysonBillingID(Integer newValue);
    public Integer getTysonBillingID();
    public void setUniqueCreateDate(Date newValue);
    public Date getUniqueCreateDate();
    public void setUniqueModifyDate(Date newValue);
    public Date getUniqueModifyDate();
    public void setUniqueModifyComments(String newValue);
    public String getUniqueModifyComments();
    public void setActionID(Integer newValue);
    public Integer getActionID();
    public void setReferralID(Integer newValue);
    public Integer getReferralID();
    public void setUserApproveID(Integer newValue);
    public Integer getUserApproveID();
    public void setGeneratedBillName(String newValue);
    public String getGeneratedBillName();
}    // End of bltysonbilling class definition
