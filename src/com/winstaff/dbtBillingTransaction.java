

package com.winstaff;

import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public class dbtBillingTransaction extends Object implements InttBillingTransaction
{

        db_NewBase    dbnbDB;

    public dbtBillingTransaction()
    {
        dbnbDB = new db_NewBase( "tBillingTransaction", "BillingTransactionID" );

    }    // End of default constructor

    public dbtBillingTransaction( Integer iNewID )
    {
        dbnbDB = new db_NewBase( "tBillingTransaction", "BillingTransactionID" );
        setUniqueID( iNewID );
    }    // End of Constructor knowing an ID

    public void commitData() throws SQLException
    {
            //no longer trapping/ignoring errors 2011-03-17
            dbnbDB.commitData();
    }    // End of commitData()

    public void commitDataForced() throws SQLException
    {
        dbnbDB.commitDataForced();
    }    // End of commitData()

    public Integer getUniqueID()
    {
        return dbnbDB.getUniqueID();
    }    // End of getUniqueID()

    public void setUniqueID( Integer iNewID )
    {
        dbnbDB.setUniqueID( iNewID );
    }    // End of setUniqueID()

    public void setModifyComments( String sNew )
    {
        dbnbDB.setModifyComments( sNew );
    }    // End of setUniqueID()


    public void setBillingTransactionID(Integer newValue)
    {
                dbnbDB.setFieldData( "BillingTransactionID", newValue.toString() );
    }

    public Integer getBillingTransactionID()
    {
        String           sValue = dbnbDB.getFieldData( "BillingTransactionID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setField(String fieldN, Object fieldV)
    {DebugLogger.println("setField Method not implemented yet in classBillingTransaction!");}

    public String getFieldType(String fieldN)
    {return null;}


    public void setUniqueCreateDate(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "UniqueCreateDate", formatter.format( newValue ) );
    }

    public Date getUniqueCreateDate()
    {
        String           sValue = dbnbDB.getFieldData( "UniqueCreateDate" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setUniqueModifyDate(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "UniqueModifyDate", formatter.format( newValue ) );
    }

    public Date getUniqueModifyDate()
    {
        String           sValue = dbnbDB.getFieldData( "UniqueModifyDate" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setUniqueModifyComments(String newValue)
    {
                dbnbDB.setFieldData( "UniqueModifyComments", newValue.toString() );
    }

    public String getUniqueModifyComments()
    {
        String           sValue = dbnbDB.getFieldData( "UniqueModifyComments" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setBillingID(Integer newValue)
    {
                dbnbDB.setFieldData( "BillingID", newValue.toString() );
    }

    public Integer getBillingID()
    {
        String           sValue = dbnbDB.getFieldData( "BillingID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setStatus(Integer newValue)
    {
                dbnbDB.setFieldData( "Status", newValue.toString() );
    }

    public Integer getStatus()
    {
        String           sValue = dbnbDB.getFieldData( "Status" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setSettlementDate(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "SettlementDate", formatter.format( newValue ) );
    }

    public Date getSettlementDate()
    {
        String           sValue = dbnbDB.getFieldData( "SettlementDate" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setSettlementAmount(Double newValue)
    {
                java.text.DecimalFormat defDecFormat2 = new java.text.DecimalFormat(PLCUtils.String_defDecFormat2);
                dbnbDB.setFieldData( "SettlementAmount", defDecFormat2.format(newValue) );
    }

    public Double getSettlementAmount()
    {
        String           sValue = dbnbDB.getFieldData( "SettlementAmount" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDoubleValue : sValue;
        return new Double( sValue  ); 
    }

    public void setComments(String newValue)
    {
                dbnbDB.setFieldData( "Comments", newValue.toString() );
    }

    public String getComments()
    {
        String           sValue = dbnbDB.getFieldData( "Comments" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

}    // End of bltBillingTransaction class definition
