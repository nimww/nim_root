package com.winstaff;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class HistoricalVolume {
	private static String datevar1, datevar2;
	public HistoricalVolume(String datevar1, String datevar2){
		HistoricalVolume.datevar1 = datevar1;
		HistoricalVolume.datevar2 = datevar2;
	}
	public List<myHistoricalListModel> gethistoricalModel(){
		List<myHistoricalListModel> myHistoricalList = new ArrayList<myHistoricalListModel>();
		String query = "select \"CaseID\", \"PayerName\",\"Encounter_Status\",\"PatientFirstName\", \"PatientLastName\",\"PatientState\",to_char(\"Referral_ReceiveDate\",'mm/dd/yy') receive_date,to_char(\"Encounter_DateOfService\",'mm/dd/yy') date_of_service, to_char(\"Encounter_TimeTrack_ReqDelivered\",'mm/dd/yy'),\"Encounter_ReportFileID\",\"Encounter_TimeTrack_ReqRec\",\"Encounter_TimeTrack_ReqCreated\", \"Encounter_TimeTrack_ReqProc\",\"Encounter_TimeTrack_ReqSched\",\"Encounter_TimeTrack_ReqDelivered\",\"Encounter_TimeTrack_ReqApproved\",\"Encounter_TimeTrack_ReqInitialAppointment\",\"Encounter_TimeTrack_ReqRxReview\",\"Encounter_TimeTrack_ReqRpReview\",\"Appointment_StatusID\",\"Service_first_CPT\", \"Service_modality\" from \"vMQ4_Encounter_NoVoid_BP\"Where \"Referral_ReceiveDate\" BETWEEN '" + datevar1 + "'::DATE AND '" + datevar2 + "'::DATE ORDER BY \"CaseID\" DESC";
		searchDB2 conn =new searchDB2();
		ResultSet rs = conn.executeStatement(query);
		try{
			while(rs.next()){
				int j = 1;
				myHistoricalList.add(new myHistoricalListModel(rs.getInt(j++), rs.getString(j++), rs.getString(j++), rs.getString(j++), rs.getString(j++), rs.getString(j++), rs.getString(j++), rs.getString(j++), rs.getString(j++), rs.getString(j++), rs.getString(j++),rs.getString(j++),rs.getString(j++),rs.getString(j++),rs.getString(j++),rs.getString(j++),rs.getString(j++),rs.getString(j++),rs.getString(j++), rs.getInt(j++), rs.getString(j++), rs.getString(j++)));
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			conn.closeAll();
		}
		return myHistoricalList;
	}
	public static int casecount() throws SQLException{
		String mycount = "select distinct count(*) \"CaseID\" from \"vMQ4_Encounter_NoVoid_BP\" Where \"Referral_ReceiveDate\" BETWEEN '" + datevar1 + "'::DATE AND  '" + datevar2 + "' ::DATE";
		searchDB2 conn =new searchDB2();
		ResultSet rs = conn.executeStatement(mycount);
		int nor = 0;
			try{
				if(rs.next()){
					 nor = rs.getInt(1);
				}
			}catch(Exception e){
				
			}finally{
				conn.closeAll();
			}
			return nor;
	}
	public static int limitedcasecount() throws SQLException{
		String mycount = "select distinct count(*) \"CaseID\" from \"vMQ4_Encounter_NoVoid_BP\" Where \"Referral_ReceiveDate\" BETWEEN '" + datevar1 + "'::DATE AND  '" + datevar2 + "':: DATE and  \"Encounter_TimeTrack_ReqDelivered\" > \"Referral_ReceiveDate\"";
		searchDB2 conn =new searchDB2();
		ResultSet rs = conn.executeStatement(mycount);
		int lnor = 0;
			try{
				if(rs.next()){
					 lnor = rs.getInt(1);
				}
			}catch(Exception e){
				
			}finally{
				conn.closeAll();
			}
			return lnor;
	}
	public List<myHistoricalListModel> getLimitedhistoricalModel(){
		List<myHistoricalListModel> myHistoricalList = new ArrayList<myHistoricalListModel>();
		String query = "select \"CaseID\", \"PayerName\",\"Encounter_Status\",\"PatientFirstName\", \"PatientLastName\",\"PatientState\",to_char(\"Referral_ReceiveDate\",'mm/dd/yy') receive_date,to_char(\"Encounter_DateOfService\",'mm/dd/yy') date_of_service, to_char(\"Encounter_TimeTrack_ReqDelivered\",'mm/dd/yy'),\"Encounter_ReportFileID\",\"Encounter_TimeTrack_ReqRec\",\"Encounter_TimeTrack_ReqCreated\", \"Encounter_TimeTrack_ReqProc\",\"Encounter_TimeTrack_ReqSched\",\"Encounter_TimeTrack_ReqDelivered\",\"Encounter_TimeTrack_ReqApproved\",\"Encounter_TimeTrack_ReqInitialAppointment\",\"Encounter_TimeTrack_ReqRxReview\",\"Encounter_TimeTrack_ReqRpReview\",\"Appointment_StatusID\",\"Service_first_CPT\", \"Service_modality\" from \"vMQ4_Encounter_NoVoid_BP\"Where \"Referral_ReceiveDate\" BETWEEN '" + datevar1 + "'::DATE AND '" + datevar2 + "'::DATE and  \"Encounter_TimeTrack_ReqDelivered\" > \"Referral_ReceiveDate\" ORDER BY \"Encounter_TimeTrack_ReqRec\" ASC";
		searchDB2 conn =new searchDB2();
		ResultSet rs = conn.executeStatement(query);
		try{
			
			while(rs.next()){
				int j = 1;
				myHistoricalList.add(new myHistoricalListModel(rs.getInt(j++), rs.getString(j++), rs.getString(j++), rs.getString(j++), rs.getString(j++), rs.getString(j++), rs.getString(j++), rs.getString(j++), rs.getString(j++), rs.getString(j++), rs.getString(j++),rs.getString(j++),rs.getString(j++),rs.getString(j++),rs.getString(j++),rs.getString(j++),rs.getString(j++),rs.getString(j++),rs.getString(j++), rs.getInt(j++), rs.getString(j++), rs.getString(j++)));
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			conn.closeAll();
		}
		return myHistoricalList;
	}
	public class myHistoricalListModel{
		//historicalData
		private int caseid;
		private String payername;
		private String eidstatus;
		private String ptfirstname;
		private String ptlastname;
		private String state;
		private String recdate;
		private String dos;
		private String rpdeliverydate;
		private String rpfileid;
		private String recvdreq;
		private String reqcreated;
		private String reqproc;
		private String reqsched;
		private String reqdelivered;
		private String reqapproved;
		private String reqinitialappt;
		private String reqrxrev;
		private String reqrprev;
		private int apptstatusid;
		private String cpt;
		private String modality;
		public myHistoricalListModel(int caseid, String payername,
				String eidstatus, String ptfirstname, String ptlastname,
				String state, String recdate, String dos,
				String rpdeliverydate, String rpfileid, String recvdreq,
				String reqcreated, String reqproc, String reqsched,
				String reqdelivered, String reqapproved, String reqinitialappt,
				String reqrxrev, String reqrprev, int apptstatusid, String cpt,
				String modality) {
			super();
			this.caseid = caseid;
			this.payername = payername;
			this.eidstatus = eidstatus;
			this.ptfirstname = ptfirstname;
			this.ptlastname = ptlastname;
			this.state = state;
			this.recdate = recdate;
			this.dos = dos;
			this.rpdeliverydate = rpdeliverydate;
			this.rpfileid = rpfileid;
			this.recvdreq = recvdreq;
			this.reqcreated = reqcreated;
			this.reqproc = reqproc;
			this.reqsched = reqsched;
			this.reqdelivered = reqdelivered;
			this.reqapproved = reqapproved;
			this.reqinitialappt = reqinitialappt;
			this.reqrxrev = reqrxrev;
			this.reqrprev = reqrprev;
			this.apptstatusid = apptstatusid;
			this.cpt = cpt;
			this.modality = modality;
		}
		public int getCaseid() {
			return caseid;
		}
		public void setCaseid(int caseid) {
			this.caseid = caseid;
		}
		public String getPayername() {
			return payername;
		}
		public void setPayername(String payername) {
			this.payername = payername;
		}
		public String getEidstatus() {
			return eidstatus;
		}
		public void setEidstatus(String eidstatus) {
			this.eidstatus = eidstatus;
		}
		public String getPtfirstname() {
			return ptfirstname;
		}
		public void setPtfirstname(String ptfirstname) {
			this.ptfirstname = ptfirstname;
		}
		public String getPtlastname() {
			return ptlastname;
		}
		public void setPtlastname(String ptlastname) {
			this.ptlastname = ptlastname;
		}
		public String getState() {
			return state;
		}
		public void setState(String state) {
			this.state = state;
		}
		public String getRecdate() {
			return recdate;
		}
		public void setRecdate(String recdate) {
			this.recdate = recdate;
		}
		public String getDos() {
			return dos;
		}
		public void setDos(String dos) {
			this.dos = dos;
		}
		public String getRpdeliverydate() {
			return rpdeliverydate;
		}
		public void setRpdeliverydate(String rpdeliverydate) {
			this.rpdeliverydate = rpdeliverydate;
		}
		public String getRpfileid() {
			return rpfileid;
		}
		public void setRpfileid(String rpfileid) {
			this.rpfileid = rpfileid;
		}
		public String getRecvdreq() {
			return recvdreq;
		}
		public void setRecvdreq(String recvdreq) {
			this.recvdreq = recvdreq;
		}
		public String getReqcreated() {
			return reqcreated;
		}
		public void setReqcreated(String reqcreated) {
			this.reqcreated = reqcreated;
		}
		public String getReqproc() {
			return reqproc;
		}
		public void setReqproc(String reqproc) {
			this.reqproc = reqproc;
		}
		public String getReqsched() {
			return reqsched;
		}
		public void setReqsched(String reqsched) {
			this.reqsched = reqsched;
		}
		public String getReqdelivered() {
			return reqdelivered;
		}
		public void setReqdelivered(String reqdelivered) {
			this.reqdelivered = reqdelivered;
		}
		public String getReqapproved() {
			return reqapproved;
		}
		public void setReqapproved(String reqapproved) {
			this.reqapproved = reqapproved;
		}
		public String getReqinitialappt() {
			return reqinitialappt;
		}
		public void setReqinitialappt(String reqinitialappt) {
			this.reqinitialappt = reqinitialappt;
		}
		public String getReqrxrev() {
			return reqrxrev;
		}
		public void setReqrxrev(String reqrxrev) {
			this.reqrxrev = reqrxrev;
		}
		public String getReqrprev() {
			return reqrprev;
		}
		public void setReqrprev(String reqrprev) {
			this.reqrprev = reqrprev;
		}
		public int getApptstatusid() {
			return apptstatusid;
		}
		public void setApptstatusid(int apptstatusid) {
			this.apptstatusid = apptstatusid;
		}
		public String getCpt() {
			return cpt;
		}
		public void setCpt(String cpt) {
			this.cpt = cpt;
		}
		public String getModality() {
			return modality;
		}
		public void setModality(String modality) {
			this.modality = modality;
		}
	}
}
