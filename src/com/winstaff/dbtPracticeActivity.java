

package com.winstaff;

import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public class dbtPracticeActivity extends Object implements InttPracticeActivity
{

        db_NewBase    dbnbDB;

    public dbtPracticeActivity()
    {
        dbnbDB = new db_NewBase( "tPracticeActivity", "PracticeActivityID" );

    }    // End of default constructor

    public dbtPracticeActivity( Integer iNewID )
    {
        dbnbDB = new db_NewBase( "tPracticeActivity", "PracticeActivityID" );
        setUniqueID( iNewID );
    }    // End of Constructor knowing an ID

    public void commitData() throws SQLException
    {
            //no longer trapping/ignoring errors 2011-03-17
            dbnbDB.commitData();
    }    // End of commitData()

    public void commitDataForced() throws SQLException
    {
        dbnbDB.commitDataForced();
    }    // End of commitData()

    public Integer getUniqueID()
    {
        return dbnbDB.getUniqueID();
    }    // End of getUniqueID()

    public void setUniqueID( Integer iNewID )
    {
        dbnbDB.setUniqueID( iNewID );
    }    // End of setUniqueID()

    public void setModifyComments( String sNew )
    {
        dbnbDB.setModifyComments( sNew );
    }    // End of setUniqueID()


    public void setPracticeActivityID(Integer newValue)
    {
                dbnbDB.setFieldData( "PracticeActivityID", newValue.toString() );
    }

    public Integer getPracticeActivityID()
    {
        String           sValue = dbnbDB.getFieldData( "PracticeActivityID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setField(String fieldN, Object fieldV)
    {DebugLogger.println("setField Method not implemented yet in classPracticeActivity!");}

    public String getFieldType(String fieldN)
    {return null;}


    public void setUniqueCreateDate(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "UniqueCreateDate", formatter.format( newValue ) );
    }

    public Date getUniqueCreateDate()
    {
        String           sValue = dbnbDB.getFieldData( "UniqueCreateDate" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setUniqueModifyDate(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "UniqueModifyDate", formatter.format( newValue ) );
    }

    public Date getUniqueModifyDate()
    {
        String           sValue = dbnbDB.getFieldData( "UniqueModifyDate" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setUniqueModifyComments(String newValue)
    {
                dbnbDB.setFieldData( "UniqueModifyComments", newValue.toString() );
    }

    public String getUniqueModifyComments()
    {
        String           sValue = dbnbDB.getFieldData( "UniqueModifyComments" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setPracticeID(Integer newValue)
    {
                dbnbDB.setFieldData( "PracticeID", newValue.toString() );
    }

    public Integer getPracticeID()
    {
        String           sValue = dbnbDB.getFieldData( "PracticeID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setAdminID(Integer newValue)
    {
                dbnbDB.setFieldData( "AdminID", newValue.toString() );
    }

    public Integer getAdminID()
    {
        String           sValue = dbnbDB.getFieldData( "AdminID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setName(String newValue)
    {
                dbnbDB.setFieldData( "Name", newValue.toString() );
    }

    public String getName()
    {
        String           sValue = dbnbDB.getFieldData( "Name" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setActivityType(String newValue)
    {
                dbnbDB.setFieldData( "ActivityType", newValue.toString() );
    }

    public String getActivityType()
    {
        String           sValue = dbnbDB.getFieldData( "ActivityType" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setisCompleted(Integer newValue)
    {
                dbnbDB.setFieldData( "isCompleted", newValue.toString() );
    }

    public Integer getisCompleted()
    {
        String           sValue = dbnbDB.getFieldData( "isCompleted" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setStartDate(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "StartDate", formatter.format( newValue ) );
    }

    public Date getStartDate()
    {
        String           sValue = dbnbDB.getFieldData( "StartDate" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setEndDate(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "EndDate", formatter.format( newValue ) );
    }

    public Date getEndDate()
    {
        String           sValue = dbnbDB.getFieldData( "EndDate" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setRemindDate(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "RemindDate", formatter.format( newValue ) );
    }

    public Date getRemindDate()
    {
        String           sValue = dbnbDB.getFieldData( "RemindDate" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setDescription(String newValue)
    {
                dbnbDB.setFieldData( "Description", newValue.toString() );
    }

    public String getDescription()
    {
        String           sValue = dbnbDB.getFieldData( "Description" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setTaskFileReference(String newValue)
    {
                dbnbDB.setFieldData( "TaskFileReference", newValue.toString() );
    }

    public String getTaskFileReference()
    {
        String           sValue = dbnbDB.getFieldData( "TaskFileReference" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setTaskLogicReference(String newValue)
    {
                dbnbDB.setFieldData( "TaskLogicReference", newValue.toString() );
    }

    public String getTaskLogicReference()
    {
        String           sValue = dbnbDB.getFieldData( "TaskLogicReference" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setSummary(String newValue)
    {
                dbnbDB.setFieldData( "Summary", newValue.toString() );
    }

    public String getSummary()
    {
        String           sValue = dbnbDB.getFieldData( "Summary" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setComments(String newValue)
    {
                dbnbDB.setFieldData( "Comments", newValue.toString() );
    }

    public String getComments()
    {
        String           sValue = dbnbDB.getFieldData( "Comments" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setReferenceID(Integer newValue)
    {
                dbnbDB.setFieldData( "ReferenceID", newValue.toString() );
    }

    public Integer getReferenceID()
    {
        String           sValue = dbnbDB.getFieldData( "ReferenceID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setDocuLinkID(Integer newValue)
    {
                dbnbDB.setFieldData( "DocuLinkID", newValue.toString() );
    }

    public Integer getDocuLinkID()
    {
        String           sValue = dbnbDB.getFieldData( "DocuLinkID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setAssignedToID(Integer newValue)
    {
                dbnbDB.setFieldData( "AssignedToID", newValue.toString() );
    }

    public Integer getAssignedToID()
    {
        String           sValue = dbnbDB.getFieldData( "AssignedToID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

}    // End of bltPracticeActivity class definition
