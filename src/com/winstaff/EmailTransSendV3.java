/*
	A basic Java class stub for a Win32 Console Application.
 */
package com.winstaff;

import com.winstaff.*;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

public class EmailTransSendV3 extends Thread {

	public EmailTransSendV3() {

	}

	static public void main(String args[]) {
		EmailTransSendV3 testMe = new EmailTransSendV3();
		if (true) {
			try {
				// int mySleep = 300;
				System.out.println(">>> Running Email Send " + new java.util.Date());
				testMe.runMe();
				// System.out.println(">>> sleeping for " +
				// Math.round(mySleep/60) + " Minutes...");
				// testMe.sleep(mySleep * 1000);
			} catch (Exception e) {
				System.out.println("Error in Thread: " + e);
			}
		}
	}

	public static void runMe() {
		String batchGroup = "100";
		System.out.println("==============================================================================");
		java.text.SimpleDateFormat displayDateSDF1Full = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1Full);
		System.out.println("Starting Batch Send [Batch Group:" + batchGroup + "] at " + displayDateSDF1Full.format(new java.util.Date()));
		searchDB2 mySDBA = new searchDB2();
		try {
			String mySQLA = "select top " + batchGroup + " * from tEmailTransaction where ActionID=0";
			mySQLA = "select * from tEmailTransaction where ActionID=0 LIMIT " + batchGroup + "";
			java.sql.ResultSet myRSA = mySDBA.executeStatement(mySQLA);
			int totalCnt = 0;
			int successCnt = 0;
			while (myRSA.next()) {
				System.out.println("Sending");
				totalCnt++;
				bltEmailTransaction myET = new bltEmailTransaction(new Integer(myRSA.getString("emailtransactionid")));
				emailType_V3 myETSend = new emailType_V3();
				myETSend.setTo(myET.getEmailTo());
				myETSend.setFrom(myET.getEmailFrom());
				myETSend.setSubject(myET.getEmailSubject());
				myETSend.setBody(myET.getEmailBody());
				myETSend.setBodyType(myET.getEmailBodyType());
				myETSend.setVirtualAttachment(myET.getVirtualAttachment());
				if (!myET.getEmailCC1().equalsIgnoreCase("")) {
					myETSend.cc.add(myET.getEmailCC1());
				}
				if (!myET.getEmailCC2().equalsIgnoreCase("")) {
					myETSend.cc.add(myET.getEmailCC2());
				}
				if (!myET.getEmailCC3().equalsIgnoreCase("")) {
					myETSend.cc.add(myET.getEmailCC3());
				}
				if (!myET.getEmailCC4().equalsIgnoreCase("")) {
					myETSend.cc.add(myET.getEmailCC4());
				}
				if (!myET.getEmailCC5().equalsIgnoreCase("")) {
					myETSend.cc.add(myET.getEmailCC5());
				}
				// System.out.println("Body Type: " + myETSend.getBodyType());
				if (myETSend.isSendMail_Direct2()) {
					myET.setActionID(new Integer(1));
					myET.setUniqueModifyComments("NIM EmailTransSendV3");
					myET.setComments("Successful Send on [" + displayDateSDF1Full.format(new java.util.Date()) + "]");
					successCnt++;
				} else {
					myET.setActionID(new Integer(2));
					myET.setUniqueModifyComments("NIM EmailTransSendV3");
					myET.setComments("Failed on [" + displayDateSDF1Full.format(new java.util.Date()) + "] - [" + myETSend.getMessage() + "]");
					// Send Alert Email: TODO: What happens if this also fails -
					// should LOG as well.
					DebugLogger.printLine("EmailTransSendV3: runMe: Email Server Failed to Send an Email on [" + new Date() + "]");
					// send to IT
					emailType_V3 myETSend_error = new emailType_V3();
					myETSend_error.setTo("it.alert@nextimagemedical.com");
					myETSend_error.setSubject("EMAIL SERVER FAILURE (2): EmailTransactionID = " + myET.getEmailTransactionID());
					myETSend_error.setBody("Failed on [" + displayDateSDF1Full.format(new java.util.Date()) + "] - [" + myETSend.getMessage() + "]\n\nEmail To: " + myET.getEmailTo() + "\n\nEmail From: " + myET.getEmailFrom() + "\n\nSubject:[" + myET.getEmailSubject() + "]");
					myETSend_error.setBodyType(emailType_V3.PLAIN_TYPE);
					myETSend_error.isSendMail_Direct2();
					// send to sender
					myETSend_error = new emailType_V3();
					myETSend_error.setTo(myET.getEmailFrom());
					myETSend_error.setSubject("EMAIL SERVER FAILURE (2): EmailTransactionID = " + myET.getEmailTransactionID());
					myETSend_error.setBody("Failed on [" + displayDateSDF1Full.format(new java.util.Date()) + "] - [" + myETSend.getMessage() + "]\n\nEmail To: " + myET.getEmailTo() + "\n\nEmail From: " + myET.getEmailFrom() + "\n\nSubject:[" + myET.getEmailSubject() + "]");
					myETSend_error.setBodyType(emailType_V3.PLAIN_TYPE);
					// TODO: Not Enabled
					// myETSend_error.isSendMail_Direct2();
				}
				myET.commitData();
				System.out.println("Done Saving");
			}
			mySDBA.closeAll();
			System.out.println("==============================================================================");
			System.out.println("Total Attempted: " + totalCnt);
			System.out.println("Total Sent: " + successCnt);
			System.out.println("Total Failed: " + (totalCnt - successCnt));
			System.out.println("==============================================================================");
		} catch (Exception e) {
			System.out.println(e);
		} finally {
			mySDBA.closeAll();

		}

	}

	// {{DECLARE_CONTROLS
	// }}

	public void runFax() throws Exception {
		searchDB2 db = new searchDB2();
		String query = "select * from temailtransaction where actionid=33 order by emailtransactionid desc";
		ResultSet rs = db.executeStatement(query);
		
		while(rs.next()){
			try{
				if(sendFax(rs.getString("virtualattachment"), rs.getString("emailtransactionid"))>0){
					db.executeUpdate("update temailtransaction set actionid = 34 where emailtransactionid="+rs.getString("emailtransactionid"));
				} else{
					db.executeUpdate("update temailtransaction set actionid = 35 where emailtransactionid="+rs.getString("emailtransactionid"));
				}
			} catch(Exception e){
				db.executeUpdate("update temailtransaction set actionid = 35 where emailtransactionid="+rs.getString("emailtransactionid"));
			}
		}
	}
	public void getStatus(int jobid) throws Exception{
		DefaultHttpClient httpclient = new DefaultHttpClient();
		
		HttpPost httpost = new HttpPost("https://www.faxage.com/httpsfax.php");
		List<NameValuePair> nvps = new ArrayList<NameValuePair>();

		nvps.add(new BasicNameValuePair("username", "ple"));
		nvps.add(new BasicNameValuePair("company", "21586"));
		nvps.add(new BasicNameValuePair("password", "pL123654"));
		nvps.add(new BasicNameValuePair("operation", "status"));
		nvps.add(new BasicNameValuePair("jobid", Integer.toString(jobid)));
		
		httpost.setEntity(new UrlEncodedFormEntity(nvps, org.apache.http.protocol.HTTP.UTF_8));
		HttpResponse response2 = httpclient.execute(httpost);
		HttpEntity entity = response2.getEntity();
		
		String e = EntityUtils.toString(entity);
		String[] row = e.split("\n");

		searchDB2 db = new searchDB2();
		String query = "";
		String[] col = null;
		for(String s : row){
			col = s.split("\t");
			if(col[4]!="pending"){
				query = "insert into faxstatus values ("+col[0]+",'"+col[3]+"', '"+col[4]+"', '"+col[5]+"','"+col[6]+"'::timestamp, '"+col[7]+"'::timestamp)";
				db.executeUpdate(query);
				query = "update temailtranction set actionid=36 where emailtransactionid = (select emailtransactionid from etfaxagelink where jobid = "+col[0]+")";
				db.executeUpdate(query);
			}
		}
		db.closeAll();
		
		//System.out.println(e);
	}
	public int sendFax(String doc, String etid) throws Exception {

		DefaultHttpClient httpclient = new DefaultHttpClient();

		HttpPost httpost = new HttpPost("https://www.faxage.com/httpsfax.php");
		List<NameValuePair> nvps = new ArrayList<NameValuePair>();
		String[] faxurls = new String[2];
		String[] faxurltypes = new String[2];

		faxurls[0] = "http://www.nextimagemedical.com/requestAuth.php?etid="+etid;
		faxurls[1] = "http://173.204.89.20/requestDoc.php?doc="+doc;

		faxurltypes[0] = "HTML";
		faxurltypes[1] = "PDF";

		nvps.add(new BasicNameValuePair("username", "ple"));
		nvps.add(new BasicNameValuePair("company", "21586"));
		nvps.add(new BasicNameValuePair("password", "pL123654"));
		nvps.add(new BasicNameValuePair("operation", "sendfax"));
		nvps.add(new BasicNameValuePair("recipname", "NextImage Medical"));
		nvps.add(new BasicNameValuePair("faxno", "8588479135"));
		nvps.add(new BasicNameValuePair("faxurls[0]", faxurls[0]));
		nvps.add(new BasicNameValuePair("faxurltypes[0]", faxurltypes[0]));
		if(doc.length()>0){
			nvps.add(new BasicNameValuePair("faxurls[1]", faxurls[1]));
			nvps.add(new BasicNameValuePair("faxurltypes[1]", faxurltypes[1]));
		}

		httpost.setEntity(new UrlEncodedFormEntity(nvps, org.apache.http.protocol.HTTP.UTF_8));
		HttpResponse response2 = httpclient.execute(httpost);
		HttpEntity entity = response2.getEntity();

		String jobid = EntityUtils.toString(entity).replaceAll("JOBID: ", "");
		
		searchDB2 db = new searchDB2();
		String query = "insert into etfaxagelink values("+etid+", "+jobid+")";
		db.executeUpdate(query);
		
		
		
		db.closeAll();
		
		return new Integer(jobid);

	}

}
