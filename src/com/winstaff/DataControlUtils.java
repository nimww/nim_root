package com.winstaff;
/**
 *
 * @author  Scott Ellis
 * @version 1.00
 */
public class DataControlUtils
{
    public DataControlUtils () 
    {
//	theExcludeList.addElement(new Integer(1));
//	theExcludeList.addElement(new Integer(146));


    }   // End of default constructor


	public static String filterJavaScriptString(String myString)
	{
	    replaceSubstring myRS = new replaceSubstring();
	    return myRS.getReplacedSubstring(myString,"'","\\'");
	}



	public static String getOpenFlow(String myString)
	{
		String myForwardFile = "";
		if (myString.equalsIgnoreCase("tNIM3_CaseAccount_main_NIM3_CaseAccount_PayerID_form_authorize"))
		{
			myForwardFile = "tNIM3_Referral_main_NIM3_Referral_CaseID.jsp";
		}
		else if (myString.equalsIgnoreCase("tNIM3_Referral_main_NIM3_Referral_CaseID_form_authorize"))
		{
			myForwardFile = "tNIM3_Encounter_main_NIM3_Encounter_ReferralID.jsp";
		}
		else if (myString.equalsIgnoreCase("tNIM3_Encounter_main_NIM3_Encounter_ReferralID_form_authorize"))
		{
			myForwardFile = "tNIM3_Service_main_NIM3_Service_EncounterID.jsp";
		}
		else if (myString.equalsIgnoreCase("tNIM3_Service_main_NIM3_Service_EncounterID_form_authorize"))
		{
			myForwardFile = "tNIM3_Service_main_NIM3_Service_EncounterID.jsp";
		}
		return myForwardFile;
		
	}



	public static java.util.Vector getPrivilegingList(Integer iHCOID, Integer iPhysicianID)
	{
	    java.util.Vector myV = null;
		searchDB2 mySS = new searchDB2();
		String mySQL = "SELECT tPhysicianEventLU.EventID from tPhysicianEventLU INNER JOIN tEventMaster on tEventMaster.EventID = tPhysicianEventLU.EventID where tPhysicianEventLU.HCOID=" + iHCOID + " AND tPhysicianEventLU.PhysicianID = " + iPhysicianID + " and tEventMaster.EventType=10 order by tEventMaster.StartDate DESC";
		java.sql.ResultSet myRS =  mySS.executeStatement(mySQL);
		try
		{
		    if (myRS!=null&&myRS.next())
		    {
		        Integer iEventID = new Integer(myRS.getString("EventID"));
		        myV = DataControlUtils.getEventPrivCategoriesVector(iEventID);
		        //System.out.println("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n");
		        //for (int a1=0;a1<myV.size();a1++)
                //{
                    //bltPrivilegeCategoryTypeLI myE = (bltPrivilegeCategoryTypeLI) myV.elementAt(a1);
                    //System.out.println("^^^^^^^^^^" + myE.getUniqueID());
		        //}
		        //System.out.println("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n");
		    }
		    else
		    {
		        //System.out.println("******************************************No Resutls");
		    }
            mySS.closeAll();
		}
		catch(Exception ee)
		{
		    //System.out.println("******************************************Error: " + ee);
		}
		return myV;
	}
	
	public static String getFullNameTitle (String preFix, String fName, String mName, String lName, String Suffix, String Title)
	{
		String myFullName = fName;
		if (!preFix.equalsIgnoreCase(""))
		{
			myFullName= preFix + " " + fName;
		}
		if (!mName.equalsIgnoreCase(""))
		{
			myFullName+= " " + mName;
		}
		if (!lName.equalsIgnoreCase(""))
		{
			myFullName+= " " + lName;
		}
		if (!Suffix.equalsIgnoreCase(""))
		{
			myFullName+= " " + Suffix;
		}
		if (!Title.equalsIgnoreCase(""))
		{
			myFullName+= ", " + Title;
		}
		return myFullName;
	}

	public static String getCSZ (String sCity,Integer iState, String sProvince, String sZIP, Integer iCountry)
	{
//mod ste 2010-05-10		String myCSZ = sCity + ", " + formPop_Base.ProvinceState(iState, sProvince);
		String myCSZ = sCity + ", " + new bltStateLI(iState).getShortState();
		if (!sZIP.equalsIgnoreCase(""))
		{
			myCSZ+= " " + sZIP;
		}
		if (iCountry.intValue()!=0&&iCountry.intValue()!=212)
		{
			myCSZ+= " " + (new bltCountryLI(iCountry)).getCountryLong();
		}
		return myCSZ;
	}



	public static int dataChanged(String theType, Integer UniqueID, int FieldCount, String theParent)
	{
	    int myType = 0;
	    if (FieldCount>0)
	    {
	        myType = 1;
	        if (theParent.equalsIgnoreCase("PHYSICIANID"))
	        {
    	        boolean removeAttestation = false;
                if (theType.equalsIgnoreCase("tBoardCertification"))
	            {
	                removeAttestation = true;
	            }
                else if (theType.equalsIgnoreCase("tCoveringPhysicians"))
	            {
	                removeAttestation = true;
	            }
                else if (theType.equalsIgnoreCase("tAdditionalInformation"))
	            {
	                removeAttestation = true;
	            }
                else if (theType.equalsIgnoreCase("tContinuingEducation"))
	            {
	                removeAttestation = true;
	            }
                else if (theType.equalsIgnoreCase("tExperience"))
	            {
	                removeAttestation = true;
	            }
                else if (theType.equalsIgnoreCase("tFacilityAffiliation"))
	            {
	                removeAttestation = true;
	            }
                else if (theType.equalsIgnoreCase("tLicenseRegistration"))
	            {
	                removeAttestation = true;
	            }
                else if (theType.equalsIgnoreCase("tMalpractice"))
	            {
	                removeAttestation = true;
	            }
                else if (theType.equalsIgnoreCase("tManagedCarePlan"))
	            {
	                removeAttestation = true;
	            }
                else if (theType.equalsIgnoreCase("tOtherCertification"))
	            {
	                removeAttestation = true;
	            }
                else if (theType.equalsIgnoreCase("tPeerReference"))
	            {
	                removeAttestation = true;
	            }
                else if (theType.equalsIgnoreCase("tProfessionalEducation"))
	            {
	                removeAttestation = true;
	            }
                else if (theType.equalsIgnoreCase("tProfessionalLiability"))
	            {
	                removeAttestation = true;
	            }
                else if (theType.equalsIgnoreCase("tProfessionalMisconduct"))
	            {
	                removeAttestation = true;
	            }
                else if (theType.equalsIgnoreCase("tProfessionalSociety"))
	            {
	                removeAttestation = true;
	            }
                else if (theType.equalsIgnoreCase("tWorkHistory"))
	            {
	                removeAttestation = true;
	            }
                else if (theType.equalsIgnoreCase("tPhysicianMaster"))
	            {
	                removeAttestation = true;
	            }
	                bltPhysicianMaster pm = new bltPhysicianMaster(UniqueID);
                        
			if (pm.isComplete())
			{                     
			    pm.setIsViewable(new Integer(1));                     
			}                     
                        else
			{                     
			    pm.setIsViewable(new Integer(2));                     
			}                     

	            //kill attestation
    		  if (removeAttestation)
                  {
	                if (pm.getIsAttested().intValue()==1)
			{
		                pm.setDeAttestDate(new java.util.Date());
			}
	                pm.setIsAttested(new Integer(2));
	                pm.setIsAttestedPending(new Integer(2));
	                pm.setAttestationComments("application data was changed");
                  }
			pm.setLastModifiedDate(new java.util.Date());
	            try
	            {
	                pm.commitDataForced();
	            }
	            catch(Exception e43)
	            {
	                myType = -1;
	            }



	        }
	        else if (theParent.equalsIgnoreCase("PRACTICEID"))
	        {
	                try
			{
				bltPracticeMaster pm = new bltPracticeMaster(UniqueID);
				pm.setLastModifiedDate(new java.util.Date());
		                pm.commitDataForced();
			}
			catch (Exception eee)
			{
			}
                }
	    }
	    return (myType);
	}

    public static int HCOAuthLogic(Integer iPhysicianID, Integer iHCOID)
    {


	if (ConfigurationInformation.serverName.equalsIgnoreCase("https://pro-file2.winstaff.com"))
	{

		if (iHCOID.intValue()==8 || (iHCOID.intValue()>=13 && iHCOID.intValue()<=18) )
		{
			try
			{
			    ConfigurationMessages.bltAdminPhysicianLU_List_LU_AdminID bltAdminPhysicianLU_List_LU_AdminID        =    new ConfigurationMessages.bltAdminPhysicianLU_List_LU_AdminID(new Integer(146),"PhysicianID="+iPhysicianID,"");
			    ListElement         leCurrentElement;
			    java.util.Enumeration eList = bltAdminPhysicianLU_List_LU_AdminID.elements();
			    if (!eList.hasMoreElements())
			    {
				bltAdminPhysicianLU        working_bltAdminPhysicianLU = new bltAdminPhysicianLU();
				working_bltAdminPhysicianLU.setPhysicianID(iPhysicianID);
				working_bltAdminPhysicianLU.setAdminID(new Integer(146));
				working_bltAdminPhysicianLU.setUniqueModifyComments("HCO Auth Add");
				working_bltAdminPhysicianLU.setUniqueModifyDate(new java.util.Date());
				working_bltAdminPhysicianLU.setUniqueCreateDate(new java.util.Date());
				working_bltAdminPhysicianLU.commitData();
			    }
			}
			catch(Exception eee)
			{
				
			}
		}


	}
	return 1;

    }

 
    
    public static byte [] FileToBytes(String fileName)
    {
        byte[] buf = null;
        try
        {
            java.io.File f = new java.io.File(fileName);
            java.io.FileInputStream fis = new java.io.FileInputStream(f);
            buf = new byte[(int)f.length()];
            fis.read(buf);
            fis.close();             
            //System.out.println("Hello World");
        }
        catch (Exception e)
        {
            System.out.println("EEE"+e);
        }
        return buf;
    } 	

    public static String fixApostrophe(String inS)
    {
        String myVal="";
        java.util.StringTokenizer myST = new java.util.StringTokenizer(inS,"'");
        while (myST.hasMoreTokens())
        {
            String tempS = myST.nextToken();
                myVal+=tempS;
            if (myST.hasMoreTokens())
            {
                myVal+="''";
            }
        }
        return myVal;


    }

    public static String Text2OneLine(String inS)
    {
        String myVal="";
        java.util.StringTokenizer myST = new java.util.StringTokenizer(inS.replace('\n','|').replace('\r','|'),"|");
        while (myST.hasMoreTokens())
        {
            String tempS = myST.nextToken();
                myVal+=tempS;
            if (myST.hasMoreTokens())
            {
                myVal+="\\n";
            }
        }
        return myVal;


    }



	public static java.util.Vector theExcludeList = new java.util.Vector();	

	public static boolean isAdminUpgraded(Integer iPhysicianID)
	{
		theExcludeList.addElement(new Integer(1));
		theExcludeList.addElement(new Integer(6));
		theExcludeList.addElement(new Integer(7));
		theExcludeList.addElement(new Integer(146));
		return isAdminUpgraded(theExcludeList,iPhysicianID);
	}

	public static boolean isAdminUpgraded(java.util.Vector theExclude, Integer iPhysicianID)
	{

//opposite logic... returns true if NOT upgraded
		boolean myVal = false;
//removed 2010-05-10
/*
		String ExcludeList = "";
		for (int i5=0;i5<theExclude.size();i5++)
		{
			ExcludeList += " AND (tUserAccount.ReferenceID <> "+theExclude.elementAt(i5)+") ";
		}
		searchDB2 mySS = new searchDB2();
		java.sql.ResultSet myRS = null;;
		try
		{
			myRS = mySS.executeStatement("SELECT tUserAccount.CreditCardFullName, tUserAccount.LogonUserName, tUserAccount.UserID,tUserAccount.ContactFirstName,tUserAccount.ContactLastName,tUserAccount.ContactEmail FROM tUserAccount CROSS JOIN  tAdminPhysicianLU INNER JOIN tUserAccount tUserAccount_1 ON tUserAccount.ReferenceID = tAdminPhysicianLU.AdminID where (tUserAccount.AccountType = 'adminid') AND (tAdminPhysicianLU.PhysicianID = "+iPhysicianID+") " + ExcludeList + " GROUP BY tUserAccount.UserID, tUserAccount.LogonUserName, tUserAccount.ContactFirstName,tUserAccount.ContactLastName,tUserAccount.ContactEmail, tUserAccount.CreditCardFullName");
			int cnt=0;
		        while (myRS!=null&&myRS.next()&&!myVal)
			{
				if (myRS.getString("CreditCardFullName").equalsIgnoreCase("Basic"))
				{
					myVal = true;
				}
			}
			mySS.closeAll();
		}
		catch(Exception e)
		{
			DebugLogger.println("ResultsSet:"+e);
		}
*/		return myVal;
		
	}

	public static boolean onListInteger(java.util.Vector myList,Integer myInt)
	{
		boolean isFound = false;
		int i=0;
		while (i<myList.size()&&!isFound)
		{
			Integer myTest = (Integer) myList.elementAt(i);
			if (myTest.intValue()==myInt.intValue())
			{
				isFound = true;
			}
			i++;
		}
		return isFound;
	}


	public static boolean compareDisclosureLists(java.util.Hashtable myOldList, java.util.Hashtable myNewList)
	{
		boolean myVal = false;
//removed 2010-05-10
/*
		try
		{
  		  for (int i=1;i<250;i++)
		  {
			if (myNewList.containsKey(new Integer(i)))
			{
				bltAttestR myNew = (bltAttestR)myNewList.get(new Integer(i));
				if (myOldList.containsKey(new Integer(i)))
				{
					bltAttestR myOld = (bltAttestR)myOldList.get(new Integer(i));
					if (myOld.getAnswer().intValue()==myNew.getAnswer().intValue()&&myOld.getDetails().equalsIgnoreCase(myNew.getDetails()))
					{
						System.out.println("Found Match - NO Changes ->" + i);
					}
					else 
					{
						System.out.println("Found Match - HAS Changes ->" + i);
						myVal = true;
						//make changes here
						myOld.setAnswer(myNew.getAnswer());
						myOld.setDetails(myNew.getDetails());
						myOld.commitData();
					}
				}
				else 
				{
					System.out.println("No Match - HAS Changes ->" + i);
					myVal = true;
					myNew.commitData();
					//make changes here
				}
			}
			
		  }
		}
		catch (Exception e)
		{
		}
*/		  return myVal;
	}


	public static boolean comparePrivilegeLists(java.util.Hashtable myOldList, java.util.Hashtable myNewList)
	{
		boolean myVal = false;
//removed 2010-05-10
/*
		try
		{
  		  for (int i=1;i<500;i++)
		  {
			if (myNewList.containsKey(new Integer(i)))
			{
				bltPrivilegeR myNew = (bltPrivilegeR)myNewList.get(new Integer(i));
				if (myOldList.containsKey(new Integer(i)))
				{
					bltPrivilegeR myOld = (bltPrivilegeR)myOldList.get(new Integer(i));
					if (myOld.getRequestAnswer().intValue()==myNew.getRequestAnswer().intValue()&&myOld.getRequestDetails().equalsIgnoreCase(myNew.getRequestDetails()) )
					{
						System.out.println("Found Match - NO Changes ->" + i);
					}
					else 
					{
						System.out.println("Found Match - HAS Changes ->" + i);
						myVal = true;
						//make changes here
						myOld.setRequestAnswer(myNew.getRequestAnswer());
						myOld.setRequestDetails(myNew.getRequestDetails());
						myOld.setUniqueModifyComments("PhysAppReq");
						myOld.commitData();
					}
				}
				else 
				{
					System.out.println("No Match - HAS Changes ->" + i);
					myVal = true;
					myNew.commitData();
					//make changes here
				}
			}
			
		  }
		}
		catch (Exception e)
		{
		}
*/		  return myVal;
	}


	public static boolean comparePrivilegeListsHCO(java.util.Hashtable myOldList, java.util.Hashtable myNewList)
	{
		boolean myVal = false;
//removed 2010-05-10
/*
		try
		{
  		  for (int i=1;i<500;i++)
		  {
			if (myNewList.containsKey(new Integer(i)))
			{
				bltPrivilegeR myNew = (bltPrivilegeR)myNewList.get(new Integer(i));
				if (myOldList.containsKey(new Integer(i)))
				{
					bltPrivilegeR myOld = (bltPrivilegeR)myOldList.get(new Integer(i));
					if (myOld.getResponseAnswer().intValue()==myNew.getResponseAnswer().intValue()&&myOld.getResponseDetails().equalsIgnoreCase(myNew.getResponseDetails())   )
					{
						System.out.println("Found Match - NO Changes ->" + i);
					}
					else 
					{
						System.out.println("Found Match - HAS Changes ->" + i);
						myVal = true;
						//make changes here
						myOld.setResponseAnswer(myNew.getResponseAnswer());
						myOld.setResponseDetails(myNew.getResponseDetails());
						myOld.setUniqueModifyComments("HCOAppRes");
						myOld.commitData();
					}
				}
				else 
				{
					System.out.println("No Match - HAS Changes ->" + i);
					myVal = true;
					myNew.commitData();
					//make changes here
				}
			}
			
		  }
		}
		catch (Exception e)
		{
		}
*/		  return myVal;
	}



	public static java.util.Vector getDisclosureQuestionCategories()
	{
		java.util.Vector myList = new java.util.Vector();
//removed 2010-05-10
/*
		searchDB2 mySS = new searchDB2();
		java.sql.ResultSet myRS = null;;
		try
		{
			myRS = mySS.executeStatement("select * from tDisclosureCategoryTypeLI order by DisclosureCategoryID");
		        while (myRS!=null&&myRS.next())
			{
				if (true)
				{
					bltDisclosureCategoryTypeLI myDQLI = new bltDisclosureCategoryTypeLI();
					myDQLI.setDisclosureCategoryShort(myRS.getString("DisclosureCategoryShort"));
					myDQLI.setDisclosureCategoryLong(myRS.getString("DisclosureCategoryLong"));
					myDQLI.setDisclosureCategoryID(new Integer(myRS.getString("DisclosureCategoryID")));
					myList.addElement(myDQLI);
				}
			}
			mySS.closeAll();
		}
		catch(Exception e)
		{
			DebugLogger.println("ResultsSet:"+e);
		}
		mySS.closeAll();
*/
		return myList;
	}


	public static java.util.Vector getDisclosureQuestionCategoryList(Integer myCatID, java.util.Vector myList)
	{
		java.util.Vector myListS = new java.util.Vector();
//removed 2010-05-10
/*
		for (int i5=0;i5<myList.size();i5++)
		{
			bltDisclosureQuestionLI myDQLI = (bltDisclosureQuestionLI)myList.elementAt(i5);
			if (myDQLI.getDisclosureCategoryID().intValue()==myCatID.intValue())
			{
				myListS.addElement(myList.elementAt(i5));	
			}
		}
*/		
		return myListS;
	}



	public static java.util.Vector getDisclosureQuestions(Integer iPhysicianID)
	{
		java.util.Vector myList = new java.util.Vector();
//removed 2010-05-10
/*
		java.util.Vector myListDQLI = new java.util.Vector();
		searchDB2 mySS = new searchDB2();
		java.sql.ResultSet myRS = null;;
		try
		{
			myRS = mySS.executeStatement("select * from tDisclosureQuestionLI inner join tHCODisclosureQuestionLU on tHCODisclosureQuestionLU.QuestionID = tDisclosureQuestionLI.QuestionID inner join tHCOPhysicianLU on tHCOPhysicianLU.HCOID = tHCODisclosureQuestionLU.HCOID inner join tPhysicianMaster on tHCOPhysicianLU.PhysicianID = tPhysicianMaster.PhysicianID where tPhysicianMaster.PhysicianID="+iPhysicianID);
		        while (myRS!=null&&myRS.next())
			{
				Integer myTestInt = new Integer(myRS.getString("QuestionID"));
				if (!onListInteger(myList,myTestInt))
				{
					myList.addElement(myTestInt);
					bltDisclosureQuestionLI myDQLI = new bltDisclosureQuestionLI();
					myDQLI.setDisclosureCategoryID(new Integer(myRS.getString("DisclosureCategoryID")));
					myDQLI.setQuestionText(myRS.getString("QuestionText"));
					myDQLI.setQuestionID(myTestInt);
					myListDQLI.addElement(myDQLI);
				}
			}
			mySS.closeAll();
		}
		catch(Exception e)
		{
			DebugLogger.println("ResultsSet:"+e);
		}

		myRS = null;;
		try
		{
			myRS = mySS.executeStatement("select * from tDisclosureQuestionLI inner join tFormDisclosureQuestionLU on tFormDisclosureQuestionLU.QuestionID = tDisclosureQuestionLI.QuestionID inner join tPhysicianFormLU on tPhysicianFormLU.FormID = tFormDisclosureQuestionLU.FormID inner join tPhysicianMaster on tPhysicianFormLU.PhysicianID = tPhysicianMaster.PhysicianID where tPhysicianMaster.PhysicianID="+iPhysicianID);
		        while (myRS!=null&&myRS.next())
			{
				Integer myTestInt = new Integer(myRS.getString("QuestionID"));
				if (!onListInteger(myList,myTestInt))
				{
					myList.addElement(myTestInt);
					bltDisclosureQuestionLI myDQLI = new bltDisclosureQuestionLI();
					myDQLI.setDisclosureCategoryID(new Integer(myRS.getString("DisclosureCategoryID")));
					myDQLI.setQuestionText(myRS.getString("QuestionText"));
					myDQLI.setQuestionID(myTestInt);
					myListDQLI.addElement(myDQLI);
				}
			}
			mySS.closeAll();
		}
		catch(Exception e)
		{
			DebugLogger.println("ResultsSet:"+e);
		}

		myRS = null;;
		try
		{
			myRS = mySS.executeStatement("SELECT * FROM  tDisclosureQuestionLI INNER JOIN tAttestR ON tAttestR.QuestionID = tDisclosureQuestionLI.QuestionID WHERE (tAttestR.PhysicianID = "+iPhysicianID+")");
		        while (myRS!=null&&myRS.next())
			{
				Integer myTestInt = new Integer(myRS.getString("QuestionID"));
				if (!onListInteger(myList,myTestInt))
				{
					myList.addElement(myTestInt);
					bltDisclosureQuestionLI myDQLI = new bltDisclosureQuestionLI();
					myDQLI.setDisclosureCategoryID(new Integer(myRS.getString("DisclosureCategoryID")));
					myDQLI.setQuestionText(myRS.getString("QuestionText"));
					myDQLI.setQuestionID(myTestInt);
					myListDQLI.addElement(myDQLI);
				}
			}
			mySS.closeAll();
		}
		catch(Exception e)
		{
			DebugLogger.println("ResultsSet:"+e);
		}

		mySS.closeAll();

*/
//	return myListDQLI;
	return myList;
	}


	
	public static boolean needsUpgrade(Integer iPhysicianID)
	{
	    if (needsUpgradeInt(iPhysicianID)==1)
	    {
	        return true;
	    }
	    else
	    {
	        return false;
	    }
		
	}
	
	public static boolean hasPremHCO(java.util.Vector myList)
	{
	    boolean myVal = false;
//removed 2010-05-10
/*
	    int i=0;
	    while (i<myList.size()&&!myVal)
	    {
	        Integer myTest = (Integer) myList.elementAt(i);
		bltHCOMaster hm = new bltHCOMaster(myTest);
		if (hm.getHCOTypeID().intValue()==2)
		{
			myVal = true;
		}
		i++;
	    }
*/	    return myVal;
	}

	public static int needsUpgradeInt(Integer iPhysicianID)
	{
	    int myVal = 0;
//removed 2010-05-10
/*
	    bltPhysicianMaster pm = new bltPhysicianMaster(iPhysicianID);
            bltHCOPhysicianLU_List_LU_PhysicianID        bltHCOPhysicianLU_List_LU_PhysicianID        =    new    bltHCOPhysicianLU_List_LU_PhysicianID(iPhysicianID);
            bltHCOPhysicianLU        working_bltHCOPhysicianLU;
            ListElement         leCurrentElement;
            java.util.Enumeration eList = bltHCOPhysicianLU_List_LU_PhysicianID.elements();
            java.util.Vector myList = new java.util.Vector();
            while (eList.hasMoreElements())
            {
                leCurrentElement    = (ListElement) eList.nextElement();
                working_bltHCOPhysicianLU  = (bltHCOPhysicianLU) leCurrentElement.getObject();
                myList.addElement(working_bltHCOPhysicianLU.getHCOID());
            }
            if (hasPremHCO(myList))
            {
                myVal = 1;
            }
*/            return myVal;
	}


	public static java.util.Vector getPrivCategories(Integer HCOID)
	{
		java.util.Vector myV = new java.util.Vector();
//removed 2010-05-10
/*
		searchDB2 mySS = new searchDB2();
		java.sql.ResultSet myRS = null;
		try
		{
			myRS = mySS.executeStatement("SELECT  tPrivilegeQuestionLI.PrivilegeCategoryID FROM     tPrivilegeQuestionLI INNER JOIN tHCOPrivilegeQuestionLU ON tHCOPrivilegeQuestionLU.QuestionID = tPrivilegeQuestionLI.QuestionID WHERE  (tHCOPrivilegeQuestionLU.HCOID = "+HCOID+") GROUP BY tPrivilegeQuestionLI.PrivilegeCategoryID" );

		}
		catch(Exception e)
		{
			DebugLogger.println("ResultsSet:"+e);
		}
		try
		{
	 		while (myRS!=null&&myRS.next())
	        	{   
				bltPrivilegeCategoryTypeLI myBC = new bltPrivilegeCategoryTypeLI(new Integer(myRS.getString("PrivilegeCategoryID"))   );
				myV.addElement(myBC);
			}

		}
		catch(Exception eee)
		{
		}	
		mySS.closeAll();
*/		return myV;
	}
	    

	public static java.util.Hashtable getEventPrivCategories(Integer EventID)
	{
		java.util.Hashtable myV = new java.util.Hashtable();
//removed 2010-05-10
/*
		searchDB2 mySS = new searchDB2();
		java.sql.ResultSet myRS = null;
		try
		{
			myRS = mySS.executeStatement("SELECT QuestionID FROM tPrivilegeR where EventID=" + EventID );

		}
		catch(Exception e)
		{
			DebugLogger.println("ResultsSet:"+e);
		}
		try
		{
	 		while (myRS!=null&&myRS.next())
	        	{   
				bltPrivilegeQuestionLI myBC = new bltPrivilegeQuestionLI (new Integer(myRS.getString("QuestionID"))   );
				String myCatID = myBC.getPrivilegeCategoryID() + "";
				int cnt = 0;
				if (myV.containsKey(myCatID))
				{
					cnt = ((Integer)myV.get(myCatID)).intValue();
				}
				cnt++;
				myV.put(myCatID, new Integer(cnt));
			}

		}
		catch(Exception eee)
		{
		}	
		mySS.closeAll();
*/
return myV;
	}



	public static java.util.Vector getEventPrivCategoriesVector(Integer EventID)
	{
		java.util.Vector myV = new java.util.Vector();
//removed 2010-05-10
/*
		searchDB2 mySS = new searchDB2();
		java.sql.ResultSet myRS = null;
		try
		{
			myRS = mySS.executeStatement("SELECT tPrivilegeQuestionLI.PrivilegeCategoryID FROM tPrivilegeR INNER JOIN tPrivilegeQuestionLI ON tPrivilegeR.QuestionID = tPrivilegeQuestionLI.QuestionID WHERE  (tPrivilegeR.EventID = "+EventID+") GROUP BY tPrivilegeQuestionLI.PrivilegeCategoryID" );

		}
		catch(Exception e)
		{
			DebugLogger.println("ResultsSet:"+e);
		}
		try
		{
	 		while (myRS!=null&&myRS.next())
        	{   
				bltPrivilegeCategoryTypeLI myBC = new bltPrivilegeCategoryTypeLI(new Integer(myRS.getString("PrivilegeCategoryID"))   );
				myV.addElement(myBC);
			}

		}
		catch(Exception e)
		{
			DebugLogger.println("ResultsSet:"+e);
		}	
		mySS.closeAll();
*/		return myV;
	}




	public static java.util.Hashtable getPrivQuestionsByCategoryHT(Integer HCOID, Integer PrivCatID)
	{
		java.util.Hashtable myV = new java.util.Hashtable();
//removed 2010-05-10
/*
		searchDB2 mySS = new searchDB2();
		java.sql.ResultSet myRS = null;
		try
		{
			if (PrivCatID.intValue()==0)
			{
				myRS = mySS.executeStatement("SELECT  tPrivilegeQuestionLI.QuestionID FROM     tPrivilegeQuestionLI INNER JOIN tHCOPrivilegeQuestionLU ON tHCOPrivilegeQuestionLU.QuestionID = tPrivilegeQuestionLI.QuestionID WHERE  (tHCOPrivilegeQuestionLU.HCOID = "+HCOID+"" );
			}
			else
			{
				myRS = mySS.executeStatement("SELECT  tPrivilegeQuestionLI.QuestionID FROM     tPrivilegeQuestionLI INNER JOIN tHCOPrivilegeQuestionLU ON tHCOPrivilegeQuestionLU.QuestionID = tPrivilegeQuestionLI.QuestionID WHERE  (tHCOPrivilegeQuestionLU.HCOID = "+HCOID+" AND tPrivilegeQuestionLI.PrivilegeCategoryID="+PrivCatID+")" );
			}

		}
		catch(Exception e)
		{
			DebugLogger.println("ResultsSet:"+e);
		}
		try
		{
	 		while (myRS!=null&&myRS.next())
	        	{   
				bltPrivilegeQuestionLI myBC = new bltPrivilegeQuestionLI (new Integer(myRS.getString("QuestionID"))   );
				myV.put(myBC.getQuestionID(),myBC);
			}

		}
		catch(Exception eee)
		{
		}	
		mySS.closeAll();
*/		return myV;
	}

	public static java.util.Vector getPrivQuestionsByCategory(Integer HCOID, Integer PrivCatID)
	{
		java.util.Vector myV = new java.util.Vector();
//removed 2010-05-10
/*
		searchDB2 mySS = new searchDB2();
		java.sql.ResultSet myRS = null;
		try
		{
			myRS = mySS.executeStatement("SELECT  tPrivilegeQuestionLI.QuestionID FROM     tPrivilegeQuestionLI INNER JOIN tHCOPrivilegeQuestionLU ON tHCOPrivilegeQuestionLU.QuestionID = tPrivilegeQuestionLI.QuestionID WHERE  (tHCOPrivilegeQuestionLU.HCOID = "+HCOID+" AND tPrivilegeQuestionLI.PrivilegeCategoryID="+PrivCatID+")" );

		}
		catch(Exception e)
		{
			DebugLogger.println("ResultsSet:"+e);
		}
		try
		{
	 		while (myRS!=null&&myRS.next())
	        	{   
				bltPrivilegeQuestionLI myBC = new bltPrivilegeQuestionLI (new Integer(myRS.getString("QuestionID"))   );
				myV.addElement(myBC);
			}

		}
		catch(Exception eee)
		{
		}	
		mySS.closeAll();
*/		return myV;
	}

	    

	public static java.util.Vector getEventPrivQuestions(Integer EventID)
	{
		java.util.Vector myV = new java.util.Vector();
//removed 2010-05-10
/*
		searchDB2 mySS = new searchDB2();
		java.sql.ResultSet myRS = null;
		try
		{
			myRS = mySS.executeStatement("SELECT QuestionID FROM tPrivilegeR where EventID=" + EventID );

		}
		catch(Exception e)
		{
			DebugLogger.println("ResultsSet:"+e);
		}
		try
		{
	 		while (myRS!=null&&myRS.next())
	        	{   
				bltPrivilegeQuestionLI myBC = new bltPrivilegeQuestionLI (new Integer(myRS.getString("QuestionID"))   );
				myV.addElement(myBC);
			}

		}
		catch(Exception eee)
		{
		}	
		mySS.closeAll();
*/		return myV;
	}
	    
//removed 2010-05-10
/*
	public static Integer getCVO_Physician_ControlStatus(Integer CVOID, Integer PhysicianID)
	{
		Integer myV = new Integer(0);
		searchDB2 mySS = new searchDB2();
		java.sql.ResultSet myRS = null;
		try
		{
			myRS = mySS.executeStatement("SELECT  tHCOPhysicianLU.PhysicianID, MAX(DISTINCT tCVOHCOLU.ControlType) AS TopControlType FROM     tHCOPhysicianLU INNER JOIN  tCVOHCOLU ON tCVOHCOLU.HCOID = tHCOPhysicianLU.HCOID WHERE  (tCVOHCOLU.CVOID = "+CVOID+") AND (tHCOPhysicianLU.PhysicianID = "+PhysicianID+")  GROUP BY tHCOPhysicianLU.PhysicianID");

		}
		catch(Exception e)
		{
			DebugLogger.println("ResultsSet:"+e);
		}
		try
		{
	 		while (myRS!=null&&myRS.next())
	        	{   
				myV = new Integer(myRS.getString("TopControlType"));
			}
		}
		catch(Exception eee)
		{
		}	
		mySS.closeAll();
		return myV;
	}
	    
	public static CVOPhysician_ControlType getCVO_Physician_ControlType(Integer CVOID, Integer PhysicianID)
	{
		CVOPhysician_ControlType myV = new CVOPhysician_ControlType();
//removed 2010-05-10
/*
		searchDB2 mySS = new searchDB2();
		java.sql.ResultSet myRS = null;
		try
		{

			myRS = mySS.executeStatement("SELECT  tHCOPhysicianLU.HCOID, tHCOPhysicianLU.LookupID AS HCOLUID, tHCOPhysicianLU.PhysicianID, tCVOHCOLU.ControlType, tCVOHCOLU.LookupID, tCVOHCOLU.PriorityType FROM tHCOPhysicianLU INNER JOIN tCVOHCOLU ON tCVOHCOLU.HCOID = tHCOPhysicianLU.HCOID WHERE  (tCVOHCOLU.CVOID = "+CVOID+") AND (tHCOPhysicianLU.PhysicianID = "+PhysicianID+")  ORDER BY tCVOHCOLU.PriorityType DESC, tCVOHCOLU.ControlType DESC");

		}
		catch(Exception e)
		{
			DebugLogger.println("ResultsSet:"+e);
		}
		try
		{
	 		while (myRS!=null&&myRS.next())
	        	{   
				myV.setCVOHCOLUID(new Integer(myRS.getString("LookupID")));
				myV.setControlType(new Integer(myRS.getString("ControlType")));
				myV.setPriorityType(new Integer(myRS.getString("PriorityType")));
				myV.setHCOID(new Integer(myRS.getString("HCOID")));
				myV.setHCOLUID(new Integer(myRS.getString("HCOLUID")));
				myV.setPhysicianID(PhysicianID);
			}
		}
		catch(Exception eee)
		{
		}	
		mySS.closeAll();
		return myV;
	}
	    

	    
	public static boolean getCVOPracticeAccess(Integer iCVOID, Integer iPracticeID)
	{
		boolean myV = false;
//removed 2010-05-10
/*
		searchDB2 mySS = new searchDB2();
		java.sql.ResultSet myRS = null;
		try
		{

			myRS = mySS.executeStatement("SELECT  tPracticeMaster.PracticeID, tPracticeMaster.PracticeName, tPracticeMaster.OfficeCity, tPracticeMaster.OfficeStateID, tPracticeMaster.OfficeZIP FROM tPracticeMaster INNER JOIN tPhysicianPracticeLU ON tPhysicianPracticeLU.PracticeID = tPracticeMaster.PracticeID INNER JOIN tHCOPhysicianLU ON tPhysicianPracticeLU.PhysicianID = tHCOPhysicianLU.PhysicianID INNER JOIN  tCVOHCOLU ON tCVOHCOLU.HCOID = tHCOPhysicianLU.HCOID where tCVOHCOLU.CVOID=" + iCVOID + " and tPracticeMaster.PracticeID=" + iPracticeID + " GROUP BY tPracticeMaster.PracticeID, tPracticeMaster.PracticeName, tPracticeMaster.OfficeCity, tPracticeMaster.OfficeStateID, tPracticeMaster.OfficeZIP, tCVOHCOLU.CVOID");

		}
		catch(Exception e)
		{
			DebugLogger.println("ResultsSet:"+e);
		}
		try
		{
	 		if (myRS!=null&&myRS.next())
	        	{   
				myV = true;
			}
		}
		catch(Exception eee)
		{
		}	
		mySS.closeAll();
		return myV;
	}
	    
*/

	public static String ENC_KEY = "ewlkj23gjisrdfgSPMszqgb4ASD57thsf";

	public static java.text.SimpleDateFormat mySDF = new java.text.SimpleDateFormat("MM/dd/yyyy");
	public static int PARENT_PHYSICIANID = 1;
	public static int PARENT_PRACTICEID = 2;
	public static int PARENT_ADMINID = 3;
	public static int PARENT_EVENTEID = 4;
	public static int PARENT_COMPANYID = 5;

 }   // End of ConfigurationInformation class
