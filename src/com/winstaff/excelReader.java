package com.winstaff;
import java.awt.*;
import java.io.IOException;
import java.io.InputStream;

import java.util.Iterator;

import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.usermodel.HSSFRow;


public class excelReader extends java.lang.Object
{

	public excelReader()
	{
	}
	
	public boolean isMatchArray(String[] myArray, String[] myException, String fieldMatch, boolean isExactMatch)
	{
	    boolean myVal = false;
	    int iCNT = 0;
	    while (iCNT<myArray.length&&!myVal)
	    {
	        if (fieldMatch.toUpperCase().indexOf(myArray[iCNT].toUpperCase())>=0)
	        {
	            myVal=true;
	            for (int i=0;i<myException.length;i++)
	            {
	                if (fieldMatch.toUpperCase().indexOf(myException[i].toUpperCase())>=0)
	                {
	                    myVal=false;
	                }
	            }
	        }
	        iCNT++;
	    }
	    
	    return myVal;
	}
	
	public String  getOptionList(String selectName,String noneColor,String[] myArray, String[] myTest,String[] expectedFieldMatch)
	{
	    return this.getOptionList(selectName,noneColor,myArray,myTest, expectedFieldMatch, new String[0]);
	}
	

	
	public String  getOptionList(String selectName,String noneColor,String[] myArray,String[] myTest, String[] expectedFieldMatch, String[] exceptionFieldList)
	{
        String tempString="";
        boolean hasMatch = false;
	    for (int i=0;i<myArray.length;i++)
	    {
	        if (this.isMatchArray(expectedFieldMatch, exceptionFieldList, myArray[i],false))
	        {
                tempString += "<option selected value=\"" + i + "\">" + myArray[i] + " (row1: " + myTest[i] + ")</option>";
                hasMatch=true;
	        }
	        else
	        {
                tempString+= "<option  value=\"" + i + "\">" + myArray[i] + " (row1: " + myTest[i] + ")</option>";
	        }
	    }
	    String myVal = "<select name=\"" + selectName + "\" >";
	    if (!hasMatch)
	    {
	        myVal = "<select name=\"" + selectName + "\" style=\"background-color: #" + noneColor +"\"  >";
	    }
	    myVal += "<option selected value=\"none\">Ignore Field</option>";
        myVal+=tempString;
        myVal+="</select>";
	    return myVal;
	}
	
	
	public void displayFile (String fileName)
	{
        try 
        {
            //InputStream input =  excelReader.class.getResourceAsStream( fileName );
            java.io.InputStream input = new java.io.FileInputStream(fileName);
            POIFSFileSystem fs = new POIFSFileSystem( input );
            HSSFWorkbook wb = new HSSFWorkbook(fs);
            HSSFSheet sheet = wb.getSheetAt(0);
            
            // Iterate over each row in the sheet
            Iterator rows = sheet.rowIterator();
            while( rows.hasNext() ) 
            {          
                HSSFRow row = (HSSFRow) rows.next();
                System.out.println( "Row #" + row.getRowNum() );

                // Iterate over each cell in the row and print out the cell's content
                Iterator cells = row.cellIterator();
                while( cells.hasNext() ) 
                {
                    HSSFCell cell = (HSSFCell) cells.next();
                    //System.out.print( "," + cell.getCellNum() );
                    int cellCNT = 0;
                    cellCNT++;
                    if (cellCNT==1)
                    {
                        System.out.print(",");
                    }
                    switch ( cell.getCellType() ) 
                    {
                        case HSSFCell.CELL_TYPE_NUMERIC:
                            {
                                try
                                {
                                    System.out.print( cell.getDateCellValue() );
                                }
                                catch(Exception e)
                                {
                                    System.out.print( cell.getNumericCellValue() );
                                }
                            }
                            break;
                        case HSSFCell.CELL_TYPE_STRING:
                            System.out.print( cell.getStringCellValue() );
                            break;
                        default:
                            System.out.print( "unsuported sell type" );
                            break;
                    }
                }
                
                
            }
            
        } 
        catch ( IOException ex ) 
        {
            ex.printStackTrace();
        }
    }


	public java.util.Vector getFile (String fileName, boolean isTop2Only) throws Exception
	{
	    java.util.Vector myVal = new java.util.Vector();
        java.io.InputStream input = null;
        try 
        {
            //InputStream input =  excelReader.class.getResourceAsStream( fileName );
            input = new java.io.FileInputStream(fileName);
            POIFSFileSystem fs = new POIFSFileSystem( input );
            HSSFWorkbook wb = new HSSFWorkbook(fs);
            HSSFSheet sheet = wb.getSheetAt(0);
            
            // Iterate over each row in the sheet
            Iterator rows =   sheet.rowIterator();
            
            
            int rowCNT=0;
            String[] myHeader=null;
            int totalColumns = 0;
            if( rows.hasNext() ) 
            {
                HSSFRow row = (HSSFRow) rows.next();
                int cellCNT = 0;
                //get header ROW and Total Count
                java.util.Vector myHeaderV = new java.util.Vector();
                //Iterator cells = row.cellIterator();
                boolean isCellValid = true;
                while(isCellValid ) 
                {
                    HSSFCell cell = (HSSFCell) row.getCell((short)cellCNT);
                    if (cell!=null)
                    {
                        myHeaderV.add(cell.getStringCellValue());
                        cellCNT++;
                    }
                    else
                    {
                        isCellValid=false;
                    }
                }
                totalColumns = cellCNT;
                myHeader = new String[cellCNT];
                for (int i=0;i<totalColumns;i++)
                {
                    myHeader[i] = (String)myHeaderV.elementAt(i);
                }
                myVal.add(myHeader);
            }
            
            
            
            rowCNT=1;
            boolean isContinue = true;
            
            while( rows.hasNext()&& isContinue ) 
            {          
                if (rowCNT==1&&isTop2Only)
                {
                    isContinue=false;
                }
                rowCNT++;
                HSSFRow row = (HSSFRow) rows.next();
                //System.out.println( "Row #" + row.getRowNum() );

                // Iterate over each cell in the row and print out the cell's content
                //Iterator cells = row.cellIterator();
                int cellCNT = 0;
                int cellCNT_skipper = 0;
                String[] myCells = new String[totalColumns];
                while (cellCNT<totalColumns) 
                {
                    HSSFCell cell = (HSSFCell) row.getCell((short)cellCNT_skipper);
                    cellCNT_skipper++;
                    if (cell!=null)
                    {
                        java.text.SimpleDateFormat mySDF = new java.text.SimpleDateFormat("MM/dd/yyyy");
                        if( cell.getCellType() == HSSFCell.CELL_TYPE_NUMERIC && (myHeader[cellCNT].toUpperCase().indexOf("FROM")>=0||myHeader[cellCNT].toUpperCase().indexOf("TO")>=0||myHeader[cellCNT].toUpperCase().indexOf("DATE")>=0)   )
                        {
                            try
                            {
                                myCells[cellCNT] = mySDF.format( cell.getDateCellValue() );
                            }
                            catch(Exception e)
                            {
                                //myCells[cellCNT] = ( cell.getStringCellValue() );
/*                                String myTempNum = ""+( cell.getNumericCellValue());
                                if (myTempNum.lastIndexOf(".0")==myTempNum.length()-2)
                                {
                                    myTempNum = myTempNum.substring(0,myTempNum.length()-2);
                                }
*/
                                double myd = cell.getNumericCellValue();
                                String myTempNum = "" + (int)Math.round(myd);
                                myCells[cellCNT] = myTempNum;
                            
                            }
                        }
                        else if( cell.getCellType() == HSSFCell.CELL_TYPE_NUMERIC )
                        {
                            double myd = cell.getNumericCellValue();
                            String myTempNum = "" + (int)Math.round(myd);
                            myCells[cellCNT] = myTempNum;
                        }
                        else if( cell.getCellType() == HSSFCell.CELL_TYPE_BLANK)
                        {
                            myCells[cellCNT] = "";
                        }
                        else if( cell.getCellType() == HSSFCell.CELL_TYPE_STRING)
                        {
                            myCells[cellCNT] = ( cell.getStringCellValue() );
                        }
                        else
                        {
                            throw new java.io.IOException("Unsupported Field Type");
                        }
                        cellCNT++;
                    }
                    else if (cellCNT>0)
                    {
                        myCells[cellCNT] = "";
                        cellCNT++;
                    }
                   
                }
                myVal.add(myCells);
            }
            
        } 
        catch ( IOException ex ) 
        {
            ex.printStackTrace();
        }
        
        input.close();
        return myVal;
	}



}