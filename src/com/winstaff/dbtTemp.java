

package com.winstaff;

import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public class dbtTemp extends Object implements InttTemp
{

        db_NewBase    dbnbDB;

    public dbtTemp()
    {
        dbnbDB = new db_NewBase( "tTemp", "tTempID" );

    }    // End of default constructor

    public dbtTemp( Integer iNewID )
    {
        dbnbDB = new db_NewBase( "tTemp", "tTempID" );
        setUniqueID( iNewID );
    }    // End of Constructor knowing an ID

    public void commitData() throws SQLException
    {
            //no longer trapping/ignoring errors 2011-03-17
            dbnbDB.commitData();
    }    // End of commitData()

    public void commitDataForced() throws SQLException
    {
        dbnbDB.commitDataForced();
    }    // End of commitData()

    public Integer getUniqueID()
    {
        return dbnbDB.getUniqueID();
    }    // End of getUniqueID()

    public void setUniqueID( Integer iNewID )
    {
        dbnbDB.setUniqueID( iNewID );
    }    // End of setUniqueID()

    public void setModifyComments( String sNew )
    {
        dbnbDB.setModifyComments( sNew );
    }    // End of setUniqueID()


    public void settTempID(Integer newValue)
    {
                dbnbDB.setFieldData( "tTempID", newValue.toString() );
    }

    public Integer gettTempID()
    {
        String           sValue = dbnbDB.getFieldData( "tTempID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setField(String fieldN, Object fieldV)
    {DebugLogger.println("setField Method not implemented yet in class!");}

    public String getFieldType(String fieldN)
    {return null;}


    public void settext(String newValue)
    {
                dbnbDB.setFieldData( "text", newValue.toString() );
    }

    public String gettext()
    {
        String           sValue = dbnbDB.getFieldData( "text" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

}    // End of bltTemp class definition
