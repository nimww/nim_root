/*
	A basic Java class stub for a Win32 Console Application.
 */
package com.winstaff;
import com.winstaff.*;

public class SelectMRI_Import  extends Thread
{

	public SelectMRI_Import () 
	{

	}
	
    static public void main(String args[]) 
    {
        SelectMRI_Import testMe = new SelectMRI_Import();
        if (true)
        {
            try
            {
		       // int mySleep = 300;
	            System.out.println(">>> Running Email Send " + new java.util.Date());
                testMe.runMe();
    	        //System.out.println(">>> sleeping for " + Math.round(mySleep/60) + " Minutes...");
                //testMe.sleep(mySleep * 1000);
            }
            catch (Exception e)
            {
                System.out.println("Error in Thread: "+ e );
            }
        }
    }	
	
	
    public static java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat("MM/dd/yyyy");
    public static java.text.SimpleDateFormat displayDateSDF2 = new java.text.SimpleDateFormat("MM-dd-yyyy");
    public static java.text.SimpleDateFormat displayDateSDF3 = new java.text.SimpleDateFormat("MMddyyyy");
    public static java.text.SimpleDateFormat displayDateSDF4 = new java.text.SimpleDateFormat("MM/dd/yy");
    //public static java.text.SimpleDateFormat displayDateSDF5 = new java.text.SimpleDateFormat("MMddyyyy");
    public static java.text.SimpleDateFormat displayDateSDF1Full = new java.text.SimpleDateFormat("MM/dd/yyyy hh:mm:ss a");
    public static java.text.SimpleDateFormat displayDateSDF2Full = new java.text.SimpleDateFormat("MM/dd/yyyy hh:mm a");
    public static java.text.SimpleDateFormat displayDateSDF3Full = new java.text.SimpleDateFormat("MM/dd/yyyy HH:mm");

    public static void runMe()
    {
        String batchGroup = "4";
            System.out.println("==============================================================================");
        System.out.println("Starting Import of Clients ==> tNIM3_PayerMaster");
        try
        {
            searchDB2 mySDBA = new searchDB2();
            String mySQLA = "select * from \"SelectMRI_qPayerMaster_Clients\"";
            java.sql.ResultSet myRS = mySDBA.executeStatement(mySQLA);
            int totalCnt=0;
            int successCnt=0;
            while (myRS.next())
            {
                String tempPayerName = myRS.getString("PayerName"); if (tempPayerName==null) {  tempPayerName = "" ;}
                String tempQuickBooksID = myRS.getString("QuickBooksID"); if (tempQuickBooksID==null) {  tempQuickBooksID = "" ;}
                String tempBillingName = myRS.getString("BillingName"); if (tempBillingName==null) {  tempBillingName = "" ;}
                String tempBillingAddress1 = myRS.getString("BillingAddress1"); if (tempBillingAddress1==null) {  tempBillingAddress1 = "" ;}
                String tempBillingAddress2 = myRS.getString("BillingAddress2"); if (tempBillingAddress2==null) {  tempBillingAddress2 = "" ;}
                String tempBillingCity = myRS.getString("BillingCity"); if (tempBillingCity==null) {  tempBillingCity = "" ;}
                String tempBillingStateID = myRS.getString("BillingStateID"); if (tempBillingStateID==null) {  tempBillingStateID = "" ;}
                String tempBillingZIP = myRS.getString("BillingZIP"); if (tempBillingZIP==null) {  tempBillingZIP = "" ;}
                String tempUniqueModifyDate = myRS.getString("UniqueModifyDate"); if (tempUniqueModifyDate==null) {  tempUniqueModifyDate = "" ;}
                String tempUniqueCreateDate = myRS.getString("UniqueCreateDate"); if (tempUniqueCreateDate==null) {  tempUniqueCreateDate = "" ;}
                String tempBillingPhone = myRS.getString("BillingPhone"); if (tempBillingPhone==null) {  tempBillingPhone = "" ;}
                String tempBillingFax = myRS.getString("BillingFax"); if (tempBillingFax==null) {  tempBillingFax = "" ;}
                String tempSelectMRI_Notes = myRS.getString("SelectMRI_Notes"); if (tempSelectMRI_Notes==null) {  tempSelectMRI_Notes = "" ;}
                String tempClientID = myRS.getString("ClientID"); if (tempClientID==null) {  tempClientID = "" ;}

                totalCnt++;
                System.out.println("(" + totalCnt + ") -- Importing Client ["+tempPayerName+"]");
                bltNIM3_PayerMaster myPM = new bltNIM3_PayerMaster();
                myPM.setPayerName(tempPayerName);
                myPM.setSelectMRI_QBID(tempQuickBooksID);
                myPM.setBillingName(tempBillingName);
                myPM.setBillingAddress1(tempBillingAddress1);
                myPM.setBillingAddress2(tempBillingAddress2);
                myPM.setBillingCity(tempBillingCity);
                try
                {
                    myPM.setBillingStateID(new Integer(tempBillingStateID));
                }
                catch (Exception eeee)
                {
                    System.out.println("      ******** Failed on stateid = " + tempBillingStateID);
                }
                myPM.setBillingZIP(tempBillingZIP);
                try
                {
                    myPM.setUniqueCreateDate(displayDateSDF1.parse(tempUniqueCreateDate));
                }
                catch (Exception eeee)
                {
                    System.out.println("      ******** Failed on ucd = " + tempUniqueCreateDate);
                }
                try
                {
                    myPM.setUniqueModifyDate(displayDateSDF1.parse(tempUniqueModifyDate));
                }
                catch (Exception eeee)
                {
                    System.out.println("      ******** Failed on umd = " + tempUniqueModifyDate);
                }
                myPM.setBillingPhone(tempBillingPhone);
                myPM.setBillingFax(tempBillingFax);
                myPM.setSelectMRI_Notes(tempSelectMRI_Notes);
                myPM.setPayerTypeID(3);
                try
                {
                    myPM.setSelectMRI_ID(new Integer(tempClientID));
                }
                catch (Exception eeee)
                {
                    System.out.print("Failed on clientid = " + tempClientID);
                }
                if (totalCnt==1||true)
                {
                    myPM.commitData();
                    //import all branches
                    searchDB2 mySDBA2 = new searchDB2();
                    String mySQLA2 = "select * from \"SelectMRI_qPayerMaster_Branches\" where  \"SelectMRI_qPayerMaster_Branches\".\"ClientID\" = '" + myPM.getSelectMRI_ID() + "'";
                    java.sql.ResultSet myRS2 = mySDBA2.executeStatement(mySQLA2);
                    int total2Cnt=0;
                    while (myRS2.next())
                    {
                        String temp2ClientID = myRS2.getString("ClientID"); if (temp2ClientID==null) {  temp2ClientID = "" ;}
                        String temp2SelectMRI_ID = myRS2.getString("SelectMRI_ID"); if (temp2SelectMRI_ID==null) {  temp2SelectMRI_ID = "" ;}
                        String temp2PayerName = myRS2.getString("PayerName"); if (temp2PayerName==null) {  temp2PayerName = "" ;}
                        String temp2OfficePhone = myRS2.getString("OfficePhone"); if (temp2OfficePhone==null) {  temp2OfficePhone = "" ;}
                        String temp2OfficeFax = myRS2.getString("OfficeFax"); if (temp2OfficeFax==null) {  temp2OfficeFax = "" ;}
                        String temp2OfficeContact = myRS2.getString("OfficeContact"); if (temp2OfficeContact==null) {  temp2OfficeContact = "" ;}
                        String temp2SelectMRI_Notes = myRS2.getString("SelectMRI_Notes"); if (temp2SelectMRI_Notes==null) {  temp2SelectMRI_Notes = "" ;}
                        String temp2ContactName = myRS2.getString("ContactName"); if (temp2ContactName==null) {  temp2ContactName = "" ;}
                        String temp2OfficeAddress1 = myRS2.getString("OfficeAddress1"); if (temp2OfficeAddress1==null) {  temp2OfficeAddress1 = "" ;}
                        String temp2OfficeAddress2 = myRS2.getString("OfficeAddress2"); if (temp2OfficeAddress2==null) {  temp2OfficeAddress2 = "" ;}
                        String temp2OfficeCity = myRS2.getString("OfficeCity"); if (temp2OfficeCity==null) {  temp2OfficeCity = "" ;}
                        String temp2OfficeStateID = myRS2.getString("OfficeStateID"); if (temp2OfficeStateID==null) {  temp2OfficeStateID = "" ;}
                        String temp2OfficeState = myRS2.getString("OfficeState"); if (temp2OfficeState==null) {  temp2OfficeState = "" ;}
                        String temp2OfficeZIP = myRS2.getString("OfficeZIP"); if (temp2OfficeZIP==null) {  temp2OfficeZIP = "" ;}
                        String temp2SelectMRI_QBID = myRS2.getString("SelectMRI_QBID"); if (temp2SelectMRI_QBID==null) {  temp2SelectMRI_QBID = "" ;}

                        total2Cnt++;
                        System.out.println("   //(" + total2Cnt + ") -- Importing Branch ["+temp2PayerName+"]");
                        bltNIM3_PayerMaster myPM2 = new bltNIM3_PayerMaster();
                        myPM2.setPayerName(temp2PayerName);
                        myPM2.setSelectMRI_QBID(temp2SelectMRI_QBID);
                        myPM2.setContactName(temp2ContactName);
                        myPM2.setOfficeAddress1(temp2OfficeAddress1);
                        myPM2.setOfficeAddress2(temp2OfficeAddress2);
                        myPM2.setOfficeCity(temp2OfficeCity);
                        try
                        {
                            myPM2.setOfficeStateID(new Integer(temp2OfficeStateID));
                        }
                        catch (Exception eeee)
                        {
                            System.out.println("      ******** Failed on stateid = " + temp2OfficeStateID);
                        }
                        myPM2.setOfficeZIP(temp2OfficeZIP);
                        myPM2.setUniqueCreateDate(PLCUtils.getNowDate(true));
                        myPM2.setOfficePhone(temp2OfficePhone);
                        myPM2.setOfficeFax(temp2OfficeFax);
                        myPM2.setSelectMRI_Notes(temp2SelectMRI_Notes);
                        myPM2.setPayerTypeID(2);
                        try
                        {
                            myPM2.setSelectMRI_ID(new Integer(temp2SelectMRI_ID));
                        }
                        catch (Exception eeee)
                        {
                            System.out.println("      ******** Failed on branchid = " + temp2SelectMRI_ID);
                        }
                        myPM2.setParentPayerID(myPM.getUniqueID());//set to PM


                        if (true)
                        {
                            myPM2.commitData();
                            //import all Users
                            searchDB2 mySDBA3 = new searchDB2();
                            String mySQLA3 = "select * from \"SelectMRI_qUserUnion\" where  \"SelectMRI_qUserUnion\".\"BranchID\" = '" + myPM2.getSelectMRI_ID() + "'";
                            java.sql.ResultSet myRS3 = mySDBA3.executeStatement(mySQLA3);
                            int total3Cnt=0;
                            while (myRS3.next())
                            {
                                String temp3BranchID = myRS3.getString("BranchID"); if (temp3BranchID==null) {  temp3BranchID = "" ;}
                                String temp3SelectMRI_ID = myRS3.getString("SelectMRI_ID"); if (temp3SelectMRI_ID==null) {  temp3SelectMRI_ID = "" ;}
                                String temp3ContactLastName = myRS3.getString("ContactLastName"); if (temp3ContactLastName==null) {  temp3ContactLastName = "" ;}
                                String temp3ContactFirstName = myRS3.getString("ContactFirstName"); if (temp3ContactFirstName==null) {  temp3ContactFirstName = "" ;}
                                String temp3UniqueCreateDate = myRS3.getString("UniqueCreateDate"); if (temp3UniqueCreateDate==null) {  temp3UniqueCreateDate = "" ;}
                                String temp3UniqueModifyDate = myRS3.getString("UniqueModifyDate"); if (temp3UniqueModifyDate==null) {  temp3UniqueModifyDate = "" ;}
                                String temp3LogonUserName = myRS3.getString("LogonUserName"); if (temp3LogonUserName==null) {  temp3LogonUserName = "" ;}
                                String temp3ContactEmail = myRS3.getString("ContactEmail"); if (temp3ContactEmail==null) {  temp3ContactEmail = "" ;}
                                String temp3ContactPhone = myRS3.getString("ContactPhone"); if (temp3ContactPhone==null) {  temp3ContactPhone = "" ;}
                                String temp3ContactFax = myRS3.getString("ContactFax"); if (temp3ContactFax==null) {  temp3ContactFax = "" ;}
                                String temp3ContactAddress1 = myRS3.getString("ContactAddress1"); if (temp3ContactAddress1==null) {  temp3ContactAddress1 = "" ;}
                                String temp3ContactAddress2 = myRS3.getString("ContactAddress2"); if (temp3ContactAddress2==null) {  temp3ContactAddress2 = "" ;}
                                String temp3ContactCity = myRS3.getString("ContactCity"); if (temp3ContactCity==null) {  temp3ContactCity = "" ;}
                                String temp3ContactStateID = myRS3.getString("ContactStateID"); if (temp3ContactStateID==null) {  temp3ContactStateID = "" ;}
                                String temp3ContactZIP = myRS3.getString("ContactZIP"); if (temp3ContactZIP==null) {  temp3ContactZIP = "" ;}
                                String temp3MELicense = myRS3.getString("MELicense"); if (temp3MELicense==null) {  temp3MELicense = "" ;}
                                String temp3SelectMRI_Notes = myRS3.getString("SelectMRI_Notes"); if (temp3SelectMRI_Notes==null) {  temp3SelectMRI_Notes = "" ;}
                                String temp3SelectMRI_UserType = myRS3.getString("SelectMRI_UserType"); if (temp3SelectMRI_UserType==null) {  temp3SelectMRI_UserType = "" ;}

                                total3Cnt++;
                                System.out.println("   //(" + total3Cnt + ") -- Importing User  ["+temp3ContactLastName+"]");
                                bltUserAccount myUA2 = new bltUserAccount();
                                myUA2.setAccountType("AdjusterID");
                                try
                                {
                                    myUA2.setSelectMRI_ID(new Integer(temp3SelectMRI_ID));
                                }
                                catch (Exception eeee)
                                {
                                    System.out.println("      ******** Failed on s mri id = " + temp3SelectMRI_ID);
                                }
                                myUA2.setContactLastName(temp3ContactLastName);
                                myUA2.setContactFirstName(temp3ContactFirstName);
                                try
                                {
                                    myUA2.setUniqueCreateDate(displayDateSDF1.parse(temp3UniqueCreateDate));
                                }
                                catch (Exception eeee)
                                {
                                    System.out.println("      ******** Failed on ucd = " + temp3UniqueCreateDate);
                                }
                                try
                                {
                                    myUA2.setUniqueModifyDate(displayDateSDF1.parse(temp3UniqueModifyDate));
                                }
                                catch (Exception eeee)
                                {
                                    System.out.println("      ******** Failed on umd = " + temp3UniqueModifyDate);
                                }
                                myUA2.setLogonUserName(temp3LogonUserName);
                                myUA2.setLogonUserPassword("589890sdfjklsdfjifjsdf");
                                myUA2.setContactEmail(temp3ContactEmail);
                                myUA2.setContactPhone(temp3ContactPhone);
                                myUA2.setContactFax(temp3ContactFax);
                                myUA2.setContactAddress1(temp3ContactAddress1);
                                myUA2.setContactAddress2(temp3ContactAddress2);
                                myUA2.setContactCity(temp3ContactCity);
                                myUA2.setMELicense(temp3MELicense);
                                myUA2.setSelectMRI_UserType(temp3SelectMRI_UserType);
                                try
                                {
                                    myUA2.setContactStateID(new Integer(temp3ContactStateID));
                                }
                                catch (Exception eeee)
                                {
                                    System.out.println("      ******** Failed on stateid = " + temp3ContactStateID);
                                }
                                myUA2.setContactZIP(temp3ContactZIP);
                                myUA2.setSelectMRI_Notes(temp3SelectMRI_Notes);
                                myUA2.setPayerID(myPM2.getUniqueID());//set to pm2 new id
                                myUA2.setSecurityGroupID(new Integer(2));
                                myUA2.setGenericSecurityGroupID(new Integer(1));
                                myUA2.setStatus(new Integer(2));
                                myUA2.setAccessType(new Integer(2));
                                myUA2.setStartPage("../nim3/Payer_Home.jsp");
                                myUA2.setPLCID(new Integer(200));


                                if (true)
                                {
                                    myUA2.commitData();
                                }
                                System.out.println("   \\===============Done Saving User:" + temp3ContactLastName);
                            }
                            mySDBA3.closeAll();
                        }
                        System.out.println("   \\===============Done Saving Branch:" + temp2PayerName);
                    }
                    mySDBA2.closeAll();
                }
                System.out.println("=======Done Saving Client: " + tempPayerName );
            }
    	    mySDBA.closeAll();

            //import all Phys -50 Users
            searchDB2 mySDBA4 = new searchDB2();
            String mySQLA4 = "select * from \"SelectMRI_qUserUnion\" where  \"SelectMRI_qUserUnion\".\"BranchID\" = '-50'";
            java.sql.ResultSet myRS4 = mySDBA4.executeStatement(mySQLA4);
            int total4Cnt=0;
            Integer MasterPayerProvider = 124;
            
            while (myRS4.next())
            {
                String temp4BranchID = myRS4.getString("BranchID"); if (temp4BranchID==null) {  temp4BranchID = "" ;}
                String temp4SelectMRI_ID = myRS4.getString("SelectMRI_ID"); if (temp4SelectMRI_ID==null) {  temp4SelectMRI_ID = "" ;}
                String temp4ContactLastName = myRS4.getString("ContactLastName"); if (temp4ContactLastName==null) {  temp4ContactLastName = "" ;}
                String temp4ContactFirstName = myRS4.getString("ContactFirstName"); if (temp4ContactFirstName==null) {  temp4ContactFirstName = "" ;}
                String temp4UniqueCreateDate = myRS4.getString("UniqueCreateDate"); if (temp4UniqueCreateDate==null) {  temp4UniqueCreateDate = "" ;}
                String temp4UniqueModifyDate = myRS4.getString("UniqueModifyDate"); if (temp4UniqueModifyDate==null) {  temp4UniqueModifyDate = "" ;}
                String temp4LogonUserName = myRS4.getString("LogonUserName"); if (temp4LogonUserName==null) {  temp4LogonUserName = "" ;}
                String temp4ContactEmail = myRS4.getString("ContactEmail"); if (temp4ContactEmail==null) {  temp4ContactEmail = "" ;}
                String temp4ContactPhone = myRS4.getString("ContactPhone"); if (temp4ContactPhone==null) {  temp4ContactPhone = "" ;}
                String temp4ContactFax = myRS4.getString("ContactFax"); if (temp4ContactFax==null) {  temp4ContactFax = "" ;}
                String temp4ContactAddress1 = myRS4.getString("ContactAddress1"); if (temp4ContactAddress1==null) {  temp4ContactAddress1 = "" ;}
                String temp4ContactAddress2 = myRS4.getString("ContactAddress2"); if (temp4ContactAddress2==null) {  temp4ContactAddress2 = "" ;}
                String temp4ContactCity = myRS4.getString("ContactCity"); if (temp4ContactCity==null) {  temp4ContactCity = "" ;}
                String temp4ContactStateID = myRS4.getString("ContactStateID"); if (temp4ContactStateID==null) {  temp4ContactStateID = "" ;}
                String temp4ContactZIP = myRS4.getString("ContactZIP"); if (temp4ContactZIP==null) {  temp4ContactZIP = "" ;}
                String temp4MELicense = myRS4.getString("MELicense"); if (temp4MELicense==null) {  temp4MELicense = "" ;}
                String temp4SelectMRI_Notes = myRS4.getString("SelectMRI_Notes"); if (temp4SelectMRI_Notes==null) {  temp4SelectMRI_Notes = "" ;}
                String temp4SelectMRI_UserType = myRS4.getString("SelectMRI_UserType"); if (temp4SelectMRI_UserType==null) {  temp4SelectMRI_UserType = "" ;}

                total4Cnt++;
                System.out.println("   //(" + total4Cnt + ") -- Importing User  ["+temp4ContactLastName+"]");
                bltUserAccount myUA3 = new bltUserAccount();
                myUA3.setAccountType("PhysicianID");
                try
                {
                    myUA3.setSelectMRI_ID(new Integer(temp4SelectMRI_ID));
                }
                catch (Exception eeee)
                {
                    System.out.println("      ******** Failed on s mri id = " + temp4SelectMRI_ID);
                }
                myUA3.setContactLastName(temp4ContactLastName);
                myUA3.setContactFirstName(temp4ContactFirstName);
                try
                {
                    myUA3.setUniqueCreateDate(displayDateSDF1.parse(temp4UniqueCreateDate));
                }
                catch (Exception eeee)
                {
                    System.out.println("      ******** Failed on ucd = " + temp4UniqueCreateDate);
                }
                try
                {
                    myUA3.setUniqueModifyDate(displayDateSDF1.parse(temp4UniqueModifyDate));
                }
                catch (Exception eeee)
                {
                    System.out.println("      ******** Failed on umd = " + temp4UniqueModifyDate);
                }
                myUA3.setLogonUserName(temp4LogonUserName);
                myUA3.setLogonUserPassword("589890sdfjklsdfjifjsdf");
                myUA3.setContactEmail(temp4ContactEmail);
                myUA3.setContactPhone(temp4ContactPhone);
                myUA3.setContactFax(temp4ContactFax);
                myUA3.setContactAddress1(temp4ContactAddress1);
                myUA3.setContactAddress2(temp4ContactAddress2);
                myUA3.setContactCity(temp4ContactCity);
                myUA3.setMELicense(temp4MELicense);
                myUA3.setSelectMRI_UserType(temp4SelectMRI_UserType);
                try
                {
                    myUA3.setContactStateID(new Integer(temp4ContactStateID));
                }
                catch (Exception eeee)
                {
                    System.out.println("      ******** Failed on stateid = " + temp4ContactStateID);
                }
                myUA3.setContactZIP(temp4ContactZIP);
                myUA3.setSelectMRI_Notes(temp4SelectMRI_Notes);
                myUA3.setPayerID(MasterPayerProvider);//set to pm2 new id
                myUA3.setSecurityGroupID(new Integer(2));
                myUA3.setGenericSecurityGroupID(new Integer(1));
                myUA3.setStatus(new Integer(2));
                myUA3.setAccessType(new Integer(2));
                myUA3.setStartPage("../nim3/Payer_Home.jsp");
                myUA3.setPLCID(new Integer(200));


                if (true)
                {
                    myUA3.commitData();
                }
                System.out.println("   \\===============Done Saving User:" + temp4ContactLastName);
            }
            mySDBA4.closeAll();




        }
        catch(Exception e)
        {
            DebugLogger.println("Critical Failure in SelectMRI Import: " + e);
            System.out.println("Critical Failure in SelectMRI Import: " + e);
        }
        
    }

	//{{DECLARE_CONTROLS
	//}}
}

