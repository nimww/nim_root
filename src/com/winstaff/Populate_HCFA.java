package com.winstaff;

import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Random;
import java.util.Set;

import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.AcroFields;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;
import com.itextpdf.xmp.impl.Utils;

public class Populate_HCFA {
	
	public static void main(String[]args) throws Exception{
		Populate_HCFA ph = new Populate_HCFA(); 
		ph.runJob();
		
	}
	private void runJob() throws Exception{
		HCFA_Model hm = getHCFA_Query("SP23034TX52JX");
		PdfReader reader = null;
		String filePath = "/Users/giovannihernandez/Desktop/";
		//String inName = "fillable1500_response.pdf";
		String inName = "1500_Form.pdf";
		Random ran = new Random();
		int top = 12;
		char data = ' ';
		String dat = "";

		for (int i=0; i<=top; i++) {
		  data = (char)(ran.nextInt(25)+97);
		  dat = data + dat;
		}
		String name = "outfile_"+ dat +".pdf";
		try {
			reader = new PdfReader(filePath+inName);
			
			PdfStamper stamper = new PdfStamper(reader, new FileOutputStream(filePath + name));
			AcroFields pdfForm = stamper.getAcroFields();
			
			Class cls = Class.forName(HCFA_Model.class.getName());
			for(Field f : HCFA_Model.class.getFields()){
				System.out.println(f.getName()+" "+(String) cls.getDeclaredMethod("get"+captialize(f.getName())).invoke(hm, null));
				if((String) cls.getDeclaredMethod("get"+captialize(f.getName())).invoke(hm, null)!=null){
					pdfForm.setField(f.getName(), (String) cls.getDeclaredMethod("get"+captialize(f.getName())).invoke(hm, null));
				}
			}
			AcroFields fields = reader.getAcroFields();
			Set<String> fldNames = fields.getFields().keySet();
			System.out.println("BEGINING\n\n\n");
			for(String fieldName:fldNames){
				System.out.println(fieldName+":"+fields.getField(fieldName));
			}
			System.out.println("\n\n\nEND");
			System.out.println("Pt Name:\t"+hm.getPatientfullname());
			stamper.setFormFlattening(true);
			stamper.close();
	        reader.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
	}
	private String captialize(String s){
		return s.substring(0,1).toUpperCase()+s.substring(1, s.length());
	}
	
	
	@SuppressWarnings("unchecked")
	public HCFA_Model getHCFA_Query(String sp) throws SQLException, Exception, SecurityException, IllegalAccessException, InvocationTargetException, NoSuchMethodException{
		HCFA_Model hm = new HCFA_Model();
		searchDB2 ss = new searchDB2();
		String query = "select en.scanpass, ca.patientfirstname, ca.patientlastname, ca.patientaddress1, ca.patientaddress2, ca.patientcity, pst.shortstate as shortstate, ca.patientzip, ca.patienthomephone, ca.patientcellphone, ca.patientdob, ca.dateofinjury , sr.cptbodypart,\n" +
				"		ca.patientssn, pm.payername, md_id.contactfirstname, md_id.contactlastname, md_id.usernpi, sr.dcpt1, sr.dcpt2, sr.dcpt3, sr.dcpt4, ap.appointmenttime, sr.cpt, sr.cptmodifier, prm.price_mod_ct_w,\n" +
				"	    prm.price_mod_ct_wo, prm.price_mod_ct_wwo, prm.price_mod_mri_w, prm.price_mod_mri_wo, prm.price_mod_mri_wwo, prm.officefederaltaxid, prm.practicename, prm.officeaddress1, prm.officeaddress2, prm.officecity,\n" +
				"		st.shortstate as ofc_state, prm.officezip, prm.billingaddress1, prm.billingaddress2, prm.billingcity, bill_st.shortstate as billing_state, prm.billingzip, prm.npinumber, sr.billamount, sr.allowamount,\n" +
				"		sr.receivedamount, sr.paidoutamount, ca.patientgender\n" +
				"from tnim3_encounter as en\n" +
				"inner join tnim3_referral as rf\n" +
				"on rf.referralid = en.referralid\n" +
				"inner join tnim3_caseaccount as ca \n" +
				"on ca.caseid = rf.caseid\n" +
				"inner join tnim3_payermaster as pm\n" +
				"on pm.payerid = ca.payerid	\n" +
				"inner join tnim3_service as sr\n" +
				"on sr.encounterid = en.encounterid\n" +
				"inner join tnim3_appointment as ap\n" +
				"on ap.appointmentid = en.appointmentid\n" +
				"inner join tpracticemaster as prm\n" +
				"on prm.practiceid = ap.providerid\n" +
				"inner join tuseraccount as md_id\n" +
				"on md_id.userid = rf.referringphysicianid\n" +
				"inner join tstateli as st\n" +
				"on st.stateid = prm.officestateid\n" +
				"inner join tstateli as bill_st\n" +
				"on bill_st.stateid = prm.officestateid\n" +
				"inner join tstateli pst\n" +
				"on pst.stateid = ca.patientstateid\n" +
				"where scanpass = '"+sp+"'";
		ResultSet rs = ss.executeStatement(query);
		//'"+sp+"'"
		
		//ResultSetMetaData rsmd = rs.getMetaData();
		//int colcount = rsmd.getColumnCount();
		@SuppressWarnings("rawtypes")
		Class[] string = new Class[1];
		string[0] = String.class;
		int count = 1;
		@SuppressWarnings("rawtypes")
		Class cls = Class.forName(HCFA_Model.class.getName());
		try{
			while(rs.next()){
				cls.getDeclaredMethod("setFirstgroupid",string).invoke(hm, rs.getString("scanpass"));
				cls.getDeclaredMethod("setRisingaddress",string).invoke(hm, "Rising Medical Solutions, Inc 325 North LaSalle St, Suit 600 Chicago, IL 60654");
				cls.getDeclaredMethod("setMedicareradio",string).invoke(hm, "other");
				cls.getDeclaredMethod("setPatientfullname",string).invoke(hm, rs.getString("patientfirstname")+" "+rs.getString("patientlastname"));
				cls.getDeclaredMethod("setPatientaddress",string).invoke(hm, rs.getString("patientaddress1")+" "+rs.getString("patientaddress2"));
				cls.getDeclaredMethod("setPatientcity",string).invoke(hm, rs.getString("patientcity"));
				cls.getDeclaredMethod("setPatientstate",string).invoke(hm, rs.getString("shortstate"));
				cls.getDeclaredMethod("setPatientzip",string).invoke(hm, rs.getString("patientzip"));
				cls.getDeclaredMethod("setPatientphone",string).invoke(hm, rs.getString("patienthomephone"));
				cls.getDeclaredMethod("setPatientdobdd3",string).invoke(hm, rs.getString("patientdob").substring(8, 10));
				cls.getDeclaredMethod("setPatientdobmm",string).invoke(hm, rs.getString("patientdob").substring(5, 7));
				cls.getDeclaredMethod("setPatientdobyy3",string).invoke(hm, rs.getString("patientdob").substring(2, 4));
				cls.getDeclaredMethod("setPatientrelationship",string).invoke(hm, "self");
				cls.getDeclaredMethod("setPatientemploymentstatus",string).invoke(hm, "employed");
				cls.getDeclaredMethod("setPatientsignature",string).invoke(hm, "Signiture on File");
				cls.getDeclaredMethod("setPatientsignaturedate",string).invoke(hm, rs.getString("dateofinjury").substring(0, 10));
				//cls.getDeclaredMethod("setPatientconditionemployment",string).invoke(hm, "Yes");
				cls.getDeclaredMethod("setPatientconditionauto",string).invoke(hm, "no");
				cls.getDeclaredMethod("setPatientconditionother",string).invoke(hm, "no");
				cls.getDeclaredMethod("setPatientrelationshipstatus",string).invoke(hm, "other");
				cls.getDeclaredMethod("setPlacestate",string).invoke(hm, rs.getString("shortstate"));
				//cls.getDeclaredMethod("setInsuredidnumber",string).invoke(hm, rs.getString("patientssn"));
				cls.getDeclaredMethod("setInsuredfullname",string).invoke(hm, rs.getString("patientfirstname")+" "+rs.getString("patientlastname"));
				cls.getDeclaredMethod("setInsuredaddress",string).invoke(hm, rs.getString("patientaddress1")+rs.getString("patientaddress2"));
				cls.getDeclaredMethod("setInsuredcity",string).invoke(hm, rs.getString("patientcity"));
				cls.getDeclaredMethod("setInsuredstate",string).invoke(hm, rs.getString("shortstate"));
				cls.getDeclaredMethod("setInsuredzip",string).invoke(hm, rs.getString("patientzip"));
				cls.getDeclaredMethod("setInsuredphone",string).invoke(hm, rs.getString("patienthomephone"));
				cls.getDeclaredMethod("setInsuranceplanname",string).invoke(hm, "NextImage Medical");
				cls.getDeclaredMethod("setInsuredemployername",string).invoke(hm, "N/A");
				cls.getDeclaredMethod("setInsurancepolicynum",string).invoke(hm, rs.getString("scanpass"));
				cls.getDeclaredMethod("setDobinsuredyy",string).invoke(hm, rs.getString("patientdob").substring(2, 4));
				cls.getDeclaredMethod("setDobinsureddd",string).invoke(hm, rs.getString("patientdob").substring(5, 7));
				cls.getDeclaredMethod("setInsureddobmm",string).invoke(hm, rs.getString("patientdob").substring(8, 10));
				cls.getDeclaredMethod("setInsuredsex",string).invoke(hm, rs.getString("patientgender"));
				cls.getDeclaredMethod("setInsuredinsuranceplanname",string).invoke(hm, "NextImage Medical");
				cls.getDeclaredMethod("setEmployerschoolname",string).invoke(hm, "N/A");
				cls.getDeclaredMethod("setAuthorizedsignature",string).invoke(hm, "Signature on File");
				cls.getDeclaredMethod("setPhysicianfullname",string).invoke(hm, rs.getString("contactlastname")+rs.getString("contactfirstname"));
				cls.getDeclaredMethod("setPhysicianidnum",string).invoke(hm, rs.getString("usernpi"));
				cls.getDeclaredMethod("setIcd1",string).invoke(hm, rs.getString("dcpt1"));
				cls.getDeclaredMethod("setIcd2",string).invoke(hm, rs.getString("dcpt2"));
				cls.getDeclaredMethod("setIcd3",string).invoke(hm, rs.getString("dcpt3"));
				cls.getDeclaredMethod("setIcd4",string).invoke(hm, rs.getString("dcpt4"));
				cls.getDeclaredMethod("setOutsidelab",string).invoke(hm, "no");
				cls.getDeclaredMethod("setOtherhealthbenefitplan",string).invoke(hm, "no");
				cls.getDeclaredMethod("setDateofinjurydd",string).invoke(hm, rs.getString("dateofinjury").substring(8, 10));
				cls.getDeclaredMethod("setDateofinjurymm",string).invoke(hm, rs.getString("dateofinjury").substring(5, 7));
				cls.getDeclaredMethod("setDateofinjuryyy",string).invoke(hm, rs.getString("dateofinjury").substring(2, 4));
				cls.getDeclaredMethod("setDatesofservicefromdd"+count,string).invoke(hm, rs.getString("appointmenttime").substring(8, 10));
				cls.getDeclaredMethod("setDatesofservicefrommm"+count,string).invoke(hm, rs.getString("appointmenttime").substring(5, 7));
				cls.getDeclaredMethod("setDatesofservicefromyy"+count,string).invoke(hm, rs.getString("appointmenttime").substring(2, 4));
				cls.getDeclaredMethod("setDatesofservicetodd"+count,string).invoke(hm, rs.getString("appointmenttime").substring(8, 10));
				cls.getDeclaredMethod("setDatesofservicetomm"+count,string).invoke(hm, rs.getString("appointmenttime").substring(5, 7));
				cls.getDeclaredMethod("setDatesofservicetoyy"+count,string).invoke(hm, rs.getString("appointmenttime").substring(2, 4));
				cls.getDeclaredMethod("setCpt"+count,string).invoke(hm, rs.getString("cpt"));
				cls.getDeclaredMethod("setTypeofservice"+count,string).invoke(hm, rs.getString("cptbodypart").substring(0, 2));
				cls.getDeclaredMethod("setModifier"+count,string).invoke(hm, rs.getString("cptmodifier"));
				cls.getDeclaredMethod("setDianosiscode1",string).invoke(hm, rs.getString("dcpt1"));
				cls.getDeclaredMethod("setFederaltaxid",string).invoke(hm, rs.getString("officefederaltaxid"));
				cls.getDeclaredMethod("setServicerenderedaddress",string).invoke(hm, rs.getString("officeaddress1") + " " + rs.getString("officeaddress2"));
				cls.getDeclaredMethod("setServicerenderedaddress2",string).invoke(hm, rs.getString("officecity") + " " + rs.getString("ofc_state") + " " + rs.getString("officezip"));
				cls.getDeclaredMethod("setSignature31",string).invoke(hm, "Signiture on File");
				cls.getDeclaredMethod("setDate31",string).invoke(hm, rs.getString("appointmenttime").substring(0, 10));
				cls.getDeclaredMethod("setPhysiciansupplieraddress",string).invoke(hm, rs.getString("officeaddress1") + " " + rs.getString("officeaddress2"));
				cls.getDeclaredMethod("setPhysiciansupplieraddress2",string).invoke(hm, rs.getString("officecity") + " " + rs.getString("ofc_state") + " " + rs.getString("officezip"));
				cls.getDeclaredMethod("setAcceptassignment",string).invoke(hm, "Yes");
				cls.getDeclaredMethod("setCharges1",string).invoke(hm, rs.getString("billamount"));
				cls.getDeclaredMethod("setIcnpi",string).invoke(hm, rs.getString("usernpi"));
				cls.getDeclaredMethod("setNimnpi",string).invoke(hm, rs.getString("npinumber"));
				
				count++;
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			ss.closeAll();
		}
		return (HCFA_Model) cls.getDeclaredMethod("getSelf").invoke(hm, null);
	}
	
}

