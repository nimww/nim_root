

package com.winstaff;

import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;
import java.util.Enumeration;

public class bltBoardCertification extends Object implements InttBoardCertification
{

    dbtBoardCertification    dbDB;

    public bltBoardCertification()
    {
        dbDB = new dbtBoardCertification();
        dbDB.setUniqueCreateDate(new java.util.Date());
        makeRequiredHash();
        makeExpiredHash();
        makeEnglishHash();

    }    // End of default constructor

    public bltBoardCertification( Integer iNewID )
    {        dbDB = new dbtBoardCertification( iNewID );
        makeRequiredHash();
        makeExpiredHash();
        makeEnglishHash();
    }    // End of Constructor knowing an ID


    public bltBoardCertification( Integer iNewID, Integer iGroupSecurityID )
    {
        this.GroupSecurityInit(iGroupSecurityID);
        dbDB = new dbtBoardCertification( iNewID );
        makeExpiredHash();
        makeEnglishHash();
        makeRequiredHash(iGroupSecurityID);
    }    // End of Constructor knowing an ID

    public bltBoardCertification( Integer iGroupSecurityID, boolean isTrue )
    {
        this.GroupSecurityInit(iGroupSecurityID);
        dbDB = new dbtBoardCertification(  );
        makeExpiredHash();
        makeEnglishHash();
        makeRequiredHash(iGroupSecurityID);
    }    // End of Security unknown ID

    public void GroupSecurityInit( Integer iGroupSecurityID )
    {
        bltSecurityGroupMaster mySGM = new bltSecurityGroupMaster(iGroupSecurityID);
        this.UserSecurityID = iGroupSecurityID;
        if (mySGM.getRunFieldSecurityID().intValue()==1)
        {
            this.runFieldSecurity = true;
            makeRequiredHash(iGroupSecurityID);
        }
    }  

    public EventReferenceType getEventReference()
    {
        return EventUtils.getEventReference("tBoardCertification", this.getUniqueID());
    }  

    public String convertString2LogString(String normalString) 
    {
        String myVal = "";
        for (int i=0;i<normalString.length();i++)
        {
            if ( (normalString.charAt(i)!='\r')&&(normalString.charAt(i)!='\n')&&(normalString.charAt(i)!='\t') )
            {
                myVal+=normalString.charAt(i);
            }
            else
            {
                myVal+="<NLT>";
            }
        }
        return myVal;
    }
    public void runAudit()
    {
        String AuditString = new java.util.Date()+" \t " ;  
        AuditString += " tBoardCertification "; 
        AuditString += " BoardCertificationID ="+this.getUniqueID(); 
        AuditString += " PhysicianID ="+this.getPhysicianID(); 
        for (int i=0;i<this.AuditVector.size();i++)
        {
            AuditString+="\t"+convertString2LogString((String)this.AuditVector.elementAt(i));
        }
        AuditLogger.printLine(AuditString);
    }
    public void commitData() throws SQLException
    {
        runAudit();
        dbDB.setModifyComments(this.getUniqueModifyComments());
        dbDB.setUniqueModifyDate(new java.util.Date());
        dbDB.commitData();
        DataControlUtils.dataChanged("tBoardCertification",this.getPhysicianID() ,this.AuditVector.size(), "PhysicianID");
        this.AuditVector.removeAllElements();
    }    // End of commitData()

    public void commitDataForced() throws SQLException
    {
        dbDB.commitDataForced();
    }    // End of commitData()

    public Integer getUniqueID()
    {
        return dbDB.getUniqueID();
    }    // End of getUniqueID()

    public void setUniqueID( Integer iNewID )
    {
        dbDB.setUniqueID( iNewID );
    }    // End of setUniqueID()


    public void setBoardCertificationID(Integer newValue)
    {
        dbDB.setBoardCertificationID(newValue);
    }

    public Integer getBoardCertificationID()
    {
        return dbDB.getBoardCertificationID();
    }

    java.util.Vector AuditVector = new java.util.Vector();

    public void setUniqueCreateDate(Date newValue)
    {        this.setUniqueCreateDate(newValue,this.UserSecurityID);

    }
    public Date getUniqueCreateDate()
    {
        return this.getUniqueCreateDate(this.UserSecurityID);
    }

    public void setUniqueCreateDate(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='UniqueCreateDate' AND TableName='tBoardCertification'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {
                   this.AuditVector.addElement("[UniqueCreateDate]=["+newValue+"]");
                   this.AuditVector.addElement("[UniqueCreateDate]=["+newValue+"]");
                   dbDB.setUniqueCreateDate(newValue);
               }
            }
        }
        else
        {
           this.AuditVector.addElement("[UniqueCreateDate]=["+newValue+"]");
           this.AuditVector.addElement("[UniqueCreateDate]=["+newValue+"]");
           dbDB.setUniqueCreateDate(newValue);
         }
    }
    public Date getUniqueCreateDate(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='UniqueCreateDate' AND TableName='tBoardCertification'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getUniqueCreateDate();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getUniqueCreateDate();
         }
        return myVal;
    }

    public void setUniqueModifyDate(Date newValue)
    {        this.setUniqueModifyDate(newValue,this.UserSecurityID);

    }
    public Date getUniqueModifyDate()
    {
        return this.getUniqueModifyDate(this.UserSecurityID);
    }

    public void setUniqueModifyDate(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='UniqueModifyDate' AND TableName='tBoardCertification'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {
                   this.AuditVector.addElement("[UniqueModifyDate]=["+newValue+"]");
                   this.AuditVector.addElement("[UniqueModifyDate]=["+newValue+"]");
                   dbDB.setUniqueModifyDate(newValue);
               }
            }
        }
        else
        {
           this.AuditVector.addElement("[UniqueModifyDate]=["+newValue+"]");
           this.AuditVector.addElement("[UniqueModifyDate]=["+newValue+"]");
           dbDB.setUniqueModifyDate(newValue);
         }
    }
    public Date getUniqueModifyDate(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='UniqueModifyDate' AND TableName='tBoardCertification'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getUniqueModifyDate();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getUniqueModifyDate();
         }
        return myVal;
    }

    public void setUniqueModifyComments(String newValue)
    {        this.setUniqueModifyComments(newValue,this.UserSecurityID);

    }
    public String getUniqueModifyComments()
    {
        return this.getUniqueModifyComments(this.UserSecurityID);
    }

    public void setUniqueModifyComments(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='UniqueModifyComments' AND TableName='tBoardCertification'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {
                   this.AuditVector.addElement("[UniqueModifyComments]=["+newValue+"]");
                   this.AuditVector.addElement("[UniqueModifyComments]=["+newValue+"]");
                   dbDB.setUniqueModifyComments(newValue);
               }
            }
        }
        else
        {
           this.AuditVector.addElement("[UniqueModifyComments]=["+newValue+"]");
           this.AuditVector.addElement("[UniqueModifyComments]=["+newValue+"]");
           dbDB.setUniqueModifyComments(newValue);
         }
    }
    public String getUniqueModifyComments(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='UniqueModifyComments' AND TableName='tBoardCertification'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getUniqueModifyComments();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getUniqueModifyComments();
         }
        return myVal;
    }

    public void setPhysicianID(Integer newValue)
    {        this.setPhysicianID(newValue,this.UserSecurityID);

    }
    public Integer getPhysicianID()
    {
        return this.getPhysicianID(this.UserSecurityID);
    }

    public void setPhysicianID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PhysicianID' AND TableName='tBoardCertification'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {
                   this.AuditVector.addElement("[PhysicianID]=["+newValue+"]");
                   this.AuditVector.addElement("[PhysicianID]=["+newValue+"]");
                   dbDB.setPhysicianID(newValue);
               }
            }
        }
        else
        {
           this.AuditVector.addElement("[PhysicianID]=["+newValue+"]");
           this.AuditVector.addElement("[PhysicianID]=["+newValue+"]");
           dbDB.setPhysicianID(newValue);
         }
    }
    public Integer getPhysicianID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PhysicianID' AND TableName='tBoardCertification'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPhysicianID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPhysicianID();
         }
        return myVal;
    }

    public void setSpecialty(String newValue)
    {        this.setSpecialty(newValue,this.UserSecurityID);

    }
    public String getSpecialty()
    {
        return this.getSpecialty(this.UserSecurityID);
    }

    public void setSpecialty(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Specialty' AND TableName='tBoardCertification'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {
                   this.AuditVector.addElement("[Specialty]=["+newValue+"]");
                   this.AuditVector.addElement("[Specialty]=["+newValue+"]");
                   dbDB.setSpecialty(newValue);
               }
            }
        }
        else
        {
           this.AuditVector.addElement("[Specialty]=["+newValue+"]");
           this.AuditVector.addElement("[Specialty]=["+newValue+"]");
           dbDB.setSpecialty(newValue);
         }
    }
    public String getSpecialty(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Specialty' AND TableName='tBoardCertification'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getSpecialty();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getSpecialty();
         }
        return myVal;
    }

    public void setSubSpecialty(String newValue)
    {        this.setSubSpecialty(newValue,this.UserSecurityID);

    }
    public String getSubSpecialty()
    {
        return this.getSubSpecialty(this.UserSecurityID);
    }

    public void setSubSpecialty(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SubSpecialty' AND TableName='tBoardCertification'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {
                   this.AuditVector.addElement("[SubSpecialty]=["+newValue+"]");
                   this.AuditVector.addElement("[SubSpecialty]=["+newValue+"]");
                   dbDB.setSubSpecialty(newValue);
               }
            }
        }
        else
        {
           this.AuditVector.addElement("[SubSpecialty]=["+newValue+"]");
           this.AuditVector.addElement("[SubSpecialty]=["+newValue+"]");
           dbDB.setSubSpecialty(newValue);
         }
    }
    public String getSubSpecialty(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SubSpecialty' AND TableName='tBoardCertification'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getSubSpecialty();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getSubSpecialty();
         }
        return myVal;
    }

    public void setIsListed(Integer newValue)
    {        this.setIsListed(newValue,this.UserSecurityID);

    }
    public Integer getIsListed()
    {
        return this.getIsListed(this.UserSecurityID);
    }

    public void setIsListed(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='IsListed' AND TableName='tBoardCertification'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {
                   this.AuditVector.addElement("[IsListed]=["+newValue+"]");
                   this.AuditVector.addElement("[IsListed]=["+newValue+"]");
                   dbDB.setIsListed(newValue);
               }
            }
        }
        else
        {
           this.AuditVector.addElement("[IsListed]=["+newValue+"]");
           this.AuditVector.addElement("[IsListed]=["+newValue+"]");
           dbDB.setIsListed(newValue);
         }
    }
    public Integer getIsListed(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='IsListed' AND TableName='tBoardCertification'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getIsListed();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getIsListed();
         }
        return myVal;
    }

    public void setIsPrimary(Integer newValue)
    {        this.setIsPrimary(newValue,this.UserSecurityID);

    }
    public Integer getIsPrimary()
    {
        return this.getIsPrimary(this.UserSecurityID);
    }

    public void setIsPrimary(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='IsPrimary' AND TableName='tBoardCertification'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {
                   this.AuditVector.addElement("[IsPrimary]=["+newValue+"]");
                   this.AuditVector.addElement("[IsPrimary]=["+newValue+"]");
                   dbDB.setIsPrimary(newValue);
               }
            }
        }
        else
        {
           this.AuditVector.addElement("[IsPrimary]=["+newValue+"]");
           this.AuditVector.addElement("[IsPrimary]=["+newValue+"]");
           dbDB.setIsPrimary(newValue);
         }
    }
    public Integer getIsPrimary(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='IsPrimary' AND TableName='tBoardCertification'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getIsPrimary();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getIsPrimary();
         }
        return myVal;
    }

    public void setSpecialtyBoardCertified(Integer newValue)
    {        this.setSpecialtyBoardCertified(newValue,this.UserSecurityID);

    }
    public Integer getSpecialtyBoardCertified()
    {
        return this.getSpecialtyBoardCertified(this.UserSecurityID);
    }

    public void setSpecialtyBoardCertified(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SpecialtyBoardCertified' AND TableName='tBoardCertification'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {
                   this.AuditVector.addElement("[SpecialtyBoardCertified]=["+newValue+"]");
                   this.AuditVector.addElement("[SpecialtyBoardCertified]=["+newValue+"]");
                   dbDB.setSpecialtyBoardCertified(newValue);
               }
            }
        }
        else
        {
           this.AuditVector.addElement("[SpecialtyBoardCertified]=["+newValue+"]");
           this.AuditVector.addElement("[SpecialtyBoardCertified]=["+newValue+"]");
           dbDB.setSpecialtyBoardCertified(newValue);
         }
    }
    public Integer getSpecialtyBoardCertified(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SpecialtyBoardCertified' AND TableName='tBoardCertification'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getSpecialtyBoardCertified();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getSpecialtyBoardCertified();
         }
        return myVal;
    }

    public void setBoardName(String newValue)
    {        this.setBoardName(newValue,this.UserSecurityID);

    }
    public String getBoardName()
    {
        return this.getBoardName(this.UserSecurityID);
    }

    public void setBoardName(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='BoardName' AND TableName='tBoardCertification'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {
                   this.AuditVector.addElement("[BoardName]=["+newValue+"]");
                   this.AuditVector.addElement("[BoardName]=["+newValue+"]");
                   dbDB.setBoardName(newValue);
               }
            }
        }
        else
        {
           this.AuditVector.addElement("[BoardName]=["+newValue+"]");
           this.AuditVector.addElement("[BoardName]=["+newValue+"]");
           dbDB.setBoardName(newValue);
         }
    }
    public String getBoardName(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='BoardName' AND TableName='tBoardCertification'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getBoardName();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getBoardName();
         }
        return myVal;
    }

    public void setContactName(String newValue)
    {        this.setContactName(newValue,this.UserSecurityID);

    }
    public String getContactName()
    {
        return this.getContactName(this.UserSecurityID);
    }

    public void setContactName(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ContactName' AND TableName='tBoardCertification'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {
                   this.AuditVector.addElement("[ContactName]=["+newValue+"]");
                   this.AuditVector.addElement("[ContactName]=["+newValue+"]");
                   dbDB.setContactName(newValue);
               }
            }
        }
        else
        {
           this.AuditVector.addElement("[ContactName]=["+newValue+"]");
           this.AuditVector.addElement("[ContactName]=["+newValue+"]");
           dbDB.setContactName(newValue);
         }
    }
    public String getContactName(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ContactName' AND TableName='tBoardCertification'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getContactName();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getContactName();
         }
        return myVal;
    }

    public void setContactEmail(String newValue)
    {        this.setContactEmail(newValue,this.UserSecurityID);

    }
    public String getContactEmail()
    {
        return this.getContactEmail(this.UserSecurityID);
    }

    public void setContactEmail(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ContactEmail' AND TableName='tBoardCertification'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {
                   this.AuditVector.addElement("[ContactEmail]=["+newValue+"]");
                   this.AuditVector.addElement("[ContactEmail]=["+newValue+"]");
                   dbDB.setContactEmail(newValue);
               }
            }
        }
        else
        {
           this.AuditVector.addElement("[ContactEmail]=["+newValue+"]");
           this.AuditVector.addElement("[ContactEmail]=["+newValue+"]");
           dbDB.setContactEmail(newValue);
         }
    }
    public String getContactEmail(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ContactEmail' AND TableName='tBoardCertification'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getContactEmail();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getContactEmail();
         }
        return myVal;
    }

    public void setAddress1(String newValue)
    {        this.setAddress1(newValue,this.UserSecurityID);

    }
    public String getAddress1()
    {
        return this.getAddress1(this.UserSecurityID);
    }

    public void setAddress1(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Address1' AND TableName='tBoardCertification'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {
                   this.AuditVector.addElement("[Address1]=["+newValue+"]");
                   this.AuditVector.addElement("[Address1]=["+newValue+"]");
                   dbDB.setAddress1(newValue);
               }
            }
        }
        else
        {
           this.AuditVector.addElement("[Address1]=["+newValue+"]");
           this.AuditVector.addElement("[Address1]=["+newValue+"]");
           dbDB.setAddress1(newValue);
         }
    }
    public String getAddress1(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Address1' AND TableName='tBoardCertification'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getAddress1();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getAddress1();
         }
        return myVal;
    }

    public void setAddress2(String newValue)
    {        this.setAddress2(newValue,this.UserSecurityID);

    }
    public String getAddress2()
    {
        return this.getAddress2(this.UserSecurityID);
    }

    public void setAddress2(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Address2' AND TableName='tBoardCertification'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {
                   this.AuditVector.addElement("[Address2]=["+newValue+"]");
                   this.AuditVector.addElement("[Address2]=["+newValue+"]");
                   dbDB.setAddress2(newValue);
               }
            }
        }
        else
        {
           this.AuditVector.addElement("[Address2]=["+newValue+"]");
           this.AuditVector.addElement("[Address2]=["+newValue+"]");
           dbDB.setAddress2(newValue);
         }
    }
    public String getAddress2(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Address2' AND TableName='tBoardCertification'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getAddress2();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getAddress2();
         }
        return myVal;
    }

    public void setCity(String newValue)
    {        this.setCity(newValue,this.UserSecurityID);

    }
    public String getCity()
    {
        return this.getCity(this.UserSecurityID);
    }

    public void setCity(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='City' AND TableName='tBoardCertification'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {
                   this.AuditVector.addElement("[City]=["+newValue+"]");
                   this.AuditVector.addElement("[City]=["+newValue+"]");
                   dbDB.setCity(newValue);
               }
            }
        }
        else
        {
           this.AuditVector.addElement("[City]=["+newValue+"]");
           this.AuditVector.addElement("[City]=["+newValue+"]");
           dbDB.setCity(newValue);
         }
    }
    public String getCity(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='City' AND TableName='tBoardCertification'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getCity();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getCity();
         }
        return myVal;
    }

    public void setStateID(Integer newValue)
    {        this.setStateID(newValue,this.UserSecurityID);

    }
    public Integer getStateID()
    {
        return this.getStateID(this.UserSecurityID);
    }

    public void setStateID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='StateID' AND TableName='tBoardCertification'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {
                   this.AuditVector.addElement("[StateID]=["+newValue+"]");
                   this.AuditVector.addElement("[StateID]=["+newValue+"]");
                   dbDB.setStateID(newValue);
               }
            }
        }
        else
        {
           this.AuditVector.addElement("[StateID]=["+newValue+"]");
           this.AuditVector.addElement("[StateID]=["+newValue+"]");
           dbDB.setStateID(newValue);
         }
    }
    public Integer getStateID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='StateID' AND TableName='tBoardCertification'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getStateID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getStateID();
         }
        return myVal;
    }

    public void setProvince(String newValue)
    {        this.setProvince(newValue,this.UserSecurityID);

    }
    public String getProvince()
    {
        return this.getProvince(this.UserSecurityID);
    }

    public void setProvince(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Province' AND TableName='tBoardCertification'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {
                   this.AuditVector.addElement("[Province]=["+newValue+"]");
                   this.AuditVector.addElement("[Province]=["+newValue+"]");
                   dbDB.setProvince(newValue);
               }
            }
        }
        else
        {
           this.AuditVector.addElement("[Province]=["+newValue+"]");
           this.AuditVector.addElement("[Province]=["+newValue+"]");
           dbDB.setProvince(newValue);
         }
    }
    public String getProvince(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Province' AND TableName='tBoardCertification'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getProvince();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getProvince();
         }
        return myVal;
    }

    public void setZIP(String newValue)
    {        this.setZIP(newValue,this.UserSecurityID);

    }
    public String getZIP()
    {
        return this.getZIP(this.UserSecurityID);
    }

    public void setZIP(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ZIP' AND TableName='tBoardCertification'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {
                   this.AuditVector.addElement("[ZIP]=["+newValue+"]");
                   this.AuditVector.addElement("[ZIP]=["+newValue+"]");
                   dbDB.setZIP(newValue);
               }
            }
        }
        else
        {
           this.AuditVector.addElement("[ZIP]=["+newValue+"]");
           this.AuditVector.addElement("[ZIP]=["+newValue+"]");
           dbDB.setZIP(newValue);
         }
    }
    public String getZIP(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ZIP' AND TableName='tBoardCertification'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getZIP();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getZIP();
         }
        return myVal;
    }

    public void setCountryID(Integer newValue)
    {        this.setCountryID(newValue,this.UserSecurityID);

    }
    public Integer getCountryID()
    {
        return this.getCountryID(this.UserSecurityID);
    }

    public void setCountryID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CountryID' AND TableName='tBoardCertification'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {
                   this.AuditVector.addElement("[CountryID]=["+newValue+"]");
                   this.AuditVector.addElement("[CountryID]=["+newValue+"]");
                   dbDB.setCountryID(newValue);
               }
            }
        }
        else
        {
           this.AuditVector.addElement("[CountryID]=["+newValue+"]");
           this.AuditVector.addElement("[CountryID]=["+newValue+"]");
           dbDB.setCountryID(newValue);
         }
    }
    public Integer getCountryID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CountryID' AND TableName='tBoardCertification'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getCountryID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getCountryID();
         }
        return myVal;
    }

    public void setPhone(String newValue)
    {        this.setPhone(newValue,this.UserSecurityID);

    }
    public String getPhone()
    {
        return this.getPhone(this.UserSecurityID);
    }

    public void setPhone(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Phone' AND TableName='tBoardCertification'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {
                   this.AuditVector.addElement("[Phone]=["+newValue+"]");
                   this.AuditVector.addElement("[Phone]=["+newValue+"]");
                   dbDB.setPhone(newValue);
               }
            }
        }
        else
        {
           this.AuditVector.addElement("[Phone]=["+newValue+"]");
           this.AuditVector.addElement("[Phone]=["+newValue+"]");
           dbDB.setPhone(newValue);
         }
    }
    public String getPhone(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Phone' AND TableName='tBoardCertification'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPhone();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPhone();
         }
        return myVal;
    }

    public void setFax(String newValue)
    {        this.setFax(newValue,this.UserSecurityID);

    }
    public String getFax()
    {
        return this.getFax(this.UserSecurityID);
    }

    public void setFax(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Fax' AND TableName='tBoardCertification'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {
                   this.AuditVector.addElement("[Fax]=["+newValue+"]");
                   this.AuditVector.addElement("[Fax]=["+newValue+"]");
                   dbDB.setFax(newValue);
               }
            }
        }
        else
        {
           this.AuditVector.addElement("[Fax]=["+newValue+"]");
           this.AuditVector.addElement("[Fax]=["+newValue+"]");
           dbDB.setFax(newValue);
         }
    }
    public String getFax(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Fax' AND TableName='tBoardCertification'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getFax();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getFax();
         }
        return myVal;
    }

    public void setBoardDateInitialCertified(Date newValue)
    {        this.setBoardDateInitialCertified(newValue,this.UserSecurityID);

    }
    public Date getBoardDateInitialCertified()
    {
        return this.getBoardDateInitialCertified(this.UserSecurityID);
    }

    public void setBoardDateInitialCertified(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='BoardDateInitialCertified' AND TableName='tBoardCertification'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {
                   this.AuditVector.addElement("[BoardDateInitialCertified]=["+newValue+"]");
                   this.AuditVector.addElement("[BoardDateInitialCertified]=["+newValue+"]");
                   dbDB.setBoardDateInitialCertified(newValue);
               }
            }
        }
        else
        {
           this.AuditVector.addElement("[BoardDateInitialCertified]=["+newValue+"]");
           this.AuditVector.addElement("[BoardDateInitialCertified]=["+newValue+"]");
           dbDB.setBoardDateInitialCertified(newValue);
         }
    }
    public Date getBoardDateInitialCertified(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='BoardDateInitialCertified' AND TableName='tBoardCertification'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getBoardDateInitialCertified();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getBoardDateInitialCertified();
         }
        return myVal;
    }

    public void setBoardDateRecertified(Date newValue)
    {        this.setBoardDateRecertified(newValue,this.UserSecurityID);

    }
    public Date getBoardDateRecertified()
    {
        return this.getBoardDateRecertified(this.UserSecurityID);
    }

    public void setBoardDateRecertified(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='BoardDateRecertified' AND TableName='tBoardCertification'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {
                   this.AuditVector.addElement("[BoardDateRecertified]=["+newValue+"]");
                   this.AuditVector.addElement("[BoardDateRecertified]=["+newValue+"]");
                   dbDB.setBoardDateRecertified(newValue);
               }
            }
        }
        else
        {
           this.AuditVector.addElement("[BoardDateRecertified]=["+newValue+"]");
           this.AuditVector.addElement("[BoardDateRecertified]=["+newValue+"]");
           dbDB.setBoardDateRecertified(newValue);
         }
    }
    public Date getBoardDateRecertified(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='BoardDateRecertified' AND TableName='tBoardCertification'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getBoardDateRecertified();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getBoardDateRecertified();
         }
        return myVal;
    }

    public void setExpirationDate(Date newValue)
    {        this.setExpirationDate(newValue,this.UserSecurityID);

    }
    public Date getExpirationDate()
    {
        return this.getExpirationDate(this.UserSecurityID);
    }

    public void setExpirationDate(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ExpirationDate' AND TableName='tBoardCertification'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {
                   this.AuditVector.addElement("[ExpirationDate]=["+newValue+"]");
                   this.AuditVector.addElement("[ExpirationDate]=["+newValue+"]");
                   dbDB.setExpirationDate(newValue);
               }
            }
        }
        else
        {
           this.AuditVector.addElement("[ExpirationDate]=["+newValue+"]");
           this.AuditVector.addElement("[ExpirationDate]=["+newValue+"]");
           dbDB.setExpirationDate(newValue);
         }
    }
    public Date getExpirationDate(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ExpirationDate' AND TableName='tBoardCertification'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getExpirationDate();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getExpirationDate();
         }
        return myVal;
    }

    public void setDocumentNumber(String newValue)
    {        this.setDocumentNumber(newValue,this.UserSecurityID);

    }
    public String getDocumentNumber()
    {
        return this.getDocumentNumber(this.UserSecurityID);
    }

    public void setDocumentNumber(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='DocumentNumber' AND TableName='tBoardCertification'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {
                   this.AuditVector.addElement("[DocumentNumber]=["+newValue+"]");
                   this.AuditVector.addElement("[DocumentNumber]=["+newValue+"]");
                   dbDB.setDocumentNumber(newValue);
               }
            }
        }
        else
        {
           this.AuditVector.addElement("[DocumentNumber]=["+newValue+"]");
           this.AuditVector.addElement("[DocumentNumber]=["+newValue+"]");
           dbDB.setDocumentNumber(newValue);
         }
    }
    public String getDocumentNumber(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='DocumentNumber' AND TableName='tBoardCertification'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getDocumentNumber();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getDocumentNumber();
         }
        return myVal;
    }

    public void setIfNotCert(String newValue)
    {        this.setIfNotCert(newValue,this.UserSecurityID);

    }
    public String getIfNotCert()
    {
        return this.getIfNotCert(this.UserSecurityID);
    }

    public void setIfNotCert(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='IfNotCert' AND TableName='tBoardCertification'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {
                   this.AuditVector.addElement("[IfNotCert]=["+newValue+"]");
                   this.AuditVector.addElement("[IfNotCert]=["+newValue+"]");
                   dbDB.setIfNotCert(newValue);
               }
            }
        }
        else
        {
           this.AuditVector.addElement("[IfNotCert]=["+newValue+"]");
           this.AuditVector.addElement("[IfNotCert]=["+newValue+"]");
           dbDB.setIfNotCert(newValue);
         }
    }
    public String getIfNotCert(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='IfNotCert' AND TableName='tBoardCertification'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getIfNotCert();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getIfNotCert();
         }
        return myVal;
    }

    public void setCertEligible(Integer newValue)
    {        this.setCertEligible(newValue,this.UserSecurityID);

    }
    public Integer getCertEligible()
    {
        return this.getCertEligible(this.UserSecurityID);
    }

    public void setCertEligible(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CertEligible' AND TableName='tBoardCertification'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {
                   this.AuditVector.addElement("[CertEligible]=["+newValue+"]");
                   this.AuditVector.addElement("[CertEligible]=["+newValue+"]");
                   dbDB.setCertEligible(newValue);
               }
            }
        }
        else
        {
           this.AuditVector.addElement("[CertEligible]=["+newValue+"]");
           this.AuditVector.addElement("[CertEligible]=["+newValue+"]");
           dbDB.setCertEligible(newValue);
         }
    }
    public Integer getCertEligible(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CertEligible' AND TableName='tBoardCertification'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getCertEligible();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getCertEligible();
         }
        return myVal;
    }

    public void setEligibleStartDate(Date newValue)
    {        this.setEligibleStartDate(newValue,this.UserSecurityID);

    }
    public Date getEligibleStartDate()
    {
        return this.getEligibleStartDate(this.UserSecurityID);
    }

    public void setEligibleStartDate(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='EligibleStartDate' AND TableName='tBoardCertification'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {
                   this.AuditVector.addElement("[EligibleStartDate]=["+newValue+"]");
                   this.AuditVector.addElement("[EligibleStartDate]=["+newValue+"]");
                   dbDB.setEligibleStartDate(newValue);
               }
            }
        }
        else
        {
           this.AuditVector.addElement("[EligibleStartDate]=["+newValue+"]");
           this.AuditVector.addElement("[EligibleStartDate]=["+newValue+"]");
           dbDB.setEligibleStartDate(newValue);
         }
    }
    public Date getEligibleStartDate(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='EligibleStartDate' AND TableName='tBoardCertification'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getEligibleStartDate();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getEligibleStartDate();
         }
        return myVal;
    }

    public void setEligibleExpirationDate(Date newValue)
    {        this.setEligibleExpirationDate(newValue,this.UserSecurityID);

    }
    public Date getEligibleExpirationDate()
    {
        return this.getEligibleExpirationDate(this.UserSecurityID);
    }

    public void setEligibleExpirationDate(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='EligibleExpirationDate' AND TableName='tBoardCertification'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {
                   this.AuditVector.addElement("[EligibleExpirationDate]=["+newValue+"]");
                   this.AuditVector.addElement("[EligibleExpirationDate]=["+newValue+"]");
                   dbDB.setEligibleExpirationDate(newValue);
               }
            }
        }
        else
        {
           this.AuditVector.addElement("[EligibleExpirationDate]=["+newValue+"]");
           this.AuditVector.addElement("[EligibleExpirationDate]=["+newValue+"]");
           dbDB.setEligibleExpirationDate(newValue);
         }
    }
    public Date getEligibleExpirationDate(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='EligibleExpirationDate' AND TableName='tBoardCertification'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getEligibleExpirationDate();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getEligibleExpirationDate();
         }
        return myVal;
    }

    public void setCertPlanningToTake(Integer newValue)
    {        this.setCertPlanningToTake(newValue,this.UserSecurityID);

    }
    public Integer getCertPlanningToTake()
    {
        return this.getCertPlanningToTake(this.UserSecurityID);
    }

    public void setCertPlanningToTake(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CertPlanningToTake' AND TableName='tBoardCertification'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {
                   this.AuditVector.addElement("[CertPlanningToTake]=["+newValue+"]");
                   this.AuditVector.addElement("[CertPlanningToTake]=["+newValue+"]");
                   dbDB.setCertPlanningToTake(newValue);
               }
            }
        }
        else
        {
           this.AuditVector.addElement("[CertPlanningToTake]=["+newValue+"]");
           this.AuditVector.addElement("[CertPlanningToTake]=["+newValue+"]");
           dbDB.setCertPlanningToTake(newValue);
         }
    }
    public Integer getCertPlanningToTake(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CertPlanningToTake' AND TableName='tBoardCertification'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getCertPlanningToTake();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getCertPlanningToTake();
         }
        return myVal;
    }

    public void setCertPlanDate(Date newValue)
    {        this.setCertPlanDate(newValue,this.UserSecurityID);

    }
    public Date getCertPlanDate()
    {
        return this.getCertPlanDate(this.UserSecurityID);
    }

    public void setCertPlanDate(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CertPlanDate' AND TableName='tBoardCertification'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {
                   this.AuditVector.addElement("[CertPlanDate]=["+newValue+"]");
                   this.AuditVector.addElement("[CertPlanDate]=["+newValue+"]");
                   dbDB.setCertPlanDate(newValue);
               }
            }
        }
        else
        {
           this.AuditVector.addElement("[CertPlanDate]=["+newValue+"]");
           this.AuditVector.addElement("[CertPlanDate]=["+newValue+"]");
           dbDB.setCertPlanDate(newValue);
         }
    }
    public Date getCertPlanDate(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CertPlanDate' AND TableName='tBoardCertification'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getCertPlanDate();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getCertPlanDate();
         }
        return myVal;
    }

    public void setDocuLinkID(Integer newValue)
    {        this.setDocuLinkID(newValue,this.UserSecurityID);

    }
    public Integer getDocuLinkID()
    {
        return this.getDocuLinkID(this.UserSecurityID);
    }

    public void setDocuLinkID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='DocuLinkID' AND TableName='tBoardCertification'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {
                   this.AuditVector.addElement("[DocuLinkID]=["+newValue+"]");
                   this.AuditVector.addElement("[DocuLinkID]=["+newValue+"]");
                   dbDB.setDocuLinkID(newValue);
               }
            }
        }
        else
        {
           this.AuditVector.addElement("[DocuLinkID]=["+newValue+"]");
           this.AuditVector.addElement("[DocuLinkID]=["+newValue+"]");
           dbDB.setDocuLinkID(newValue);
         }
    }
    public Integer getDocuLinkID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='DocuLinkID' AND TableName='tBoardCertification'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getDocuLinkID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getDocuLinkID();
         }
        return myVal;
    }

    public void setComments(String newValue)
    {        this.setComments(newValue,this.UserSecurityID);

    }
    public String getComments()
    {
        return this.getComments(this.UserSecurityID);
    }

    public void setComments(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Comments' AND TableName='tBoardCertification'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {
                   this.AuditVector.addElement("[Comments]=["+newValue+"]");
                   this.AuditVector.addElement("[Comments]=["+newValue+"]");
                   dbDB.setComments(newValue);
               }
            }
        }
        else
        {
           this.AuditVector.addElement("[Comments]=["+newValue+"]");
           this.AuditVector.addElement("[Comments]=["+newValue+"]");
           dbDB.setComments(newValue);
         }
    }
    public String getComments(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Comments' AND TableName='tBoardCertification'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getComments();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getComments();
         }
        return myVal;
    }

    public void setRequiredFieldList(java.util.Hashtable requiredFieldList)
	 {

        this.requiredFieldList = requiredFieldList;
    }

    public java.util.Hashtable getRequiredFieldList()
    {
        return this.requiredFieldList;
    }
    public void setExpiredFieldList(java.util.Hashtable expiredFieldList)
    {
        this.expiredFieldList = expiredFieldList;
    }

    public void setEnglishFieldList(java.util.Hashtable englishFieldList)
    {
        this.englishFieldList = englishFieldList;
    }

    public java.util.Hashtable getExpiredFieldList()
    {
        return this.expiredFieldList;
    }
    public boolean isExpired(java.util.Date myDate, int daysVector)
    {
       return isExpired(myDate,new java.util.Date(),daysVector);
    }


    public boolean isRead(String fieldN, Integer GroupRefID)
    {
        boolean myVal = false;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='"+fieldN+"' AND TableName='tBoardCertification'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==1)
               {
                   myVal = true;
               }
            }
        }
        else
        {
           Integer iSecurityCheck = SecurityCheck.CheckItem("PractitionerSection4", GroupRefID);
           if (iSecurityCheck.intValue()>=1)
           {
                  myVal = true;
           }
        }
        return myVal;
    }


    public boolean isWrite(String fieldN, Integer GroupRefID)
    {
        boolean myVal = false;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='"+fieldN+"' AND TableName='tBoardCertification'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {
                   myVal = true;
               }
            }
        }
        else
        {
           Integer iSecurityCheck = SecurityCheck.CheckItem("PractitionerSection4", GroupRefID);
           if (iSecurityCheck.intValue()>=2)
           {
                  myVal = true;
           }
        }
        return myVal;
    }


    public boolean isExpired(java.util.Date myDate, java.util.Date refDate,int daysVector)
    {
        java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat("yyyy-MM-dd");
        boolean myVal = true;
        try
        {
            if (myDate.after(dbdf.parse("1800-01-10")))
            {
                if (myDate.after(dateChange(refDate,daysVector)))
                {
                    myVal=false;
                }
            }
            else
            {
                myVal = false;
            }
        }
        catch (Exception Datee)
        {
        }
        return myVal;
    }

    public java.util.Date dateChange(java.util.Date myD, int dayChange)
    {
        java.util.Calendar calendar = java.util.Calendar.getInstance();
        calendar.setTime(myD);
        calendar.add(java.util.Calendar.DATE, dayChange);
        java.util.Date resultAsDate = new java.util.Date(calendar.getTime().getTime());
        return resultAsDate;
    }

    public boolean isRequired(String fieldN)
    {
        return isRequired(fieldN, new Integer("1"));
    }

    public boolean isRequired(String fieldN, Integer GroupRefID)
    {
        boolean myVal = false;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='"+fieldN+"' AND TableName='tBoardCertification'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getRequiredID().intValue()==1)
               {
                   myVal = true;
               }
            }
        }
        else
        {
            if (this.getRequiredFieldList().containsKey(fieldN))
            {
                myVal = true;
            }
        }
        return myVal;
    }
    public boolean isExpiredCheck(String fieldN)
    {
        return isExpiredCheck(fieldN, new Integer ("1"));
    }

    public boolean isExpiredCheck(String fieldN, Integer GroupRefID)
    {
        boolean myVal = false;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='"+fieldN+"' AND TableName='tBoardCertification'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getTrackedID().intValue()==1)
               {
                   myVal = true;
               }
            }
        }
        else
        {
            if (this.getExpiredFieldList().containsKey(fieldN))
           {
                myVal = true;
            }
        }
        return myVal;
    }

    public void makeRequiredHash()
    {
       makeRequiredHash(new Integer(1));
    }

    public void makeRequiredHash(Integer GroupRefID)
    {

        java.util.Hashtable newHash = new java.util.Hashtable();
        if (runFieldSecurity)
        {
            java.util.Enumeration myKeys = englishFieldList.keys(); 
            while (myKeys.hasMoreElements()) 
            {
                 String myElement = (String) myKeys.nextElement();
                 bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='"+myElement+"' AND TableName='tBoardCertification'" ,"");
                 bltFieldSecurity        working_bltFieldSecurity;
                 ListElement         leCurrentElement;
                 Enumeration eList = myFSL.elements();
                 if (eList.hasMoreElements())
                 {
                     leCurrentElement    = (ListElement) eList.nextElement();
                     working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
                     if (working_bltFieldSecurity.getRequiredID().intValue()==1)
                     {
                         newHash.put(myElement,new Boolean(true));
                     }
                 }
            }
        }
        else
        {
            newHash.put("PhysicianID",new Boolean(true));
            newHash.put("Specialty",new Boolean(true));
//            newHash.put("IsListed",new Boolean(true));
            newHash.put("IsPrimary",new Boolean(true));
        }
        this.setRequiredFieldList(newHash);
    }

    public void makeEnglishHash()
    {

        java.util.Hashtable newHash = new java.util.Hashtable();        newHash.put("UniqueCreateDate","Item Create Date");
        newHash.put("UniqueModifyDate","Item Modify Date");
        newHash.put("UniqueModifyComments","Item Modification Comments");
        newHash.put("PhysicianID","PhysicianID");
        newHash.put("Specialty","Specialty");
        newHash.put("SubSpecialty","Sub-specialty related to above specialty");
        newHash.put("IsListed","Do you wish to be listed in Directories under this specialty?");
        newHash.put("IsPrimary","Is this your primary specialty?");
        newHash.put("SpecialtyBoardCertified","Specialty Status");
        newHash.put("BoardName","Board Name");
        newHash.put("ContactName","Contact Name");
        newHash.put("ContactEmail","Contact E-mail");
        newHash.put("Address1","Address");
        newHash.put("Address2","Address 2");
        newHash.put("City","Institution City");
        newHash.put("StateID","State");
        newHash.put("Province","Province, District, State");
        newHash.put("ZIP","ZIP");
        newHash.put("CountryID","Country");
        newHash.put("Phone","Phone (XXX-XXX-XXXX)");
        newHash.put("Fax","Fax (XXX-XXX-XXXX)");
        newHash.put("BoardDateInitialCertified","Initial Certification Date");
        newHash.put("BoardDateRecertified","Date of Recertification");
        newHash.put("ExpirationDate","Expiration Date");
        newHash.put("DocumentNumber","Certificate/Document Number");
        newHash.put("IfNotCert","If you are not certified, please explain here");
        newHash.put("CertEligible","If you are not certified, are you eligible to take this test?");
        newHash.put("EligibleStartDate","If YES, please enter the date you were eligible");
        newHash.put("EligibleExpirationDate","If YES, please enter the date your eligiblity expires");
        newHash.put("CertPlanningToTake","Are you planning on taking this test?");
        newHash.put("CertPlanDate","If so, when are you planning on taking your certification test?");
        newHash.put("DocuLinkID","DocuLink");
        newHash.put("Comments","Extra Comments");

        this.setEnglishFieldList(newHash);
    }
    public void makeExpiredHash()
    {

        java.util.Hashtable newHash = new java.util.Hashtable();
//        newHash.put("ExpirationDate",new Boolean(true));
        this.setExpiredFieldList(newHash);
    }

    public java.util.Hashtable englishFieldList = new java.util.Hashtable();
    public java.util.Hashtable requiredFieldList = new java.util.Hashtable();
    public java.util.Hashtable expiredFieldList = new java.util.Hashtable();

    public boolean isComplete()
    {
	     boolean myVal = true;
	     java.util.Enumeration myKeys = this.getRequiredFieldList().keys(); 
	     while (myKeys.hasMoreElements()) 
	     {
	         String myElement = (String) myKeys.nextElement();
	         if (!isComplete(myElement))
	         {
	             myVal = false;
	         }
	     }
	     return myVal;
    }

    public String getEnglish(String fieldN)
    {
        String myVal = null;
        if (this.englishFieldList.containsKey(fieldN))
        {
            myVal = (String)this.englishFieldList.get(fieldN);
        }
        return myVal;
    }
    public java.util.Vector isComplete(int daysVector)
    {
	     boolean myVal = false;
	     java.util.Enumeration myKeys = this.getExpiredFieldList().keys(); 
	     java.util.Vector myExpiredList = new java.util.Vector(); 
	     while (myKeys.hasMoreElements()) 
	     {
	         String myElement = (String) myKeys.nextElement();
	         if (isExpired(myElement,daysVector))
	         {
	             myVal = true;
	             myExpiredList.addElement(myElement);
	         }
	     }
	     return myExpiredList;
    }

    public void setField(String fieldN, Object fieldV)
    {
     boolean myVal = false;
     try
     {
        java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat("yyyy-MM-dd");

        if (fieldN.equals("BoardCertificationID"))
        {
             this.setBoardCertificationID((Integer)fieldV);
        }

        else if (fieldN.equals("UniqueCreateDate"))
        {
            this.setUniqueCreateDate((java.util.Date)fieldV);
        }

        else if (fieldN.equals("UniqueModifyDate"))
        {
            this.setUniqueModifyDate((java.util.Date)fieldV);
        }

        else if (fieldN.equals("UniqueModifyComments"))
        {
            this.setUniqueModifyComments((String)fieldV);
        }

        else if (fieldN.equals("PhysicianID"))
        {
             this.setPhysicianID((Integer)fieldV);
        }

        else if (fieldN.equals("Specialty"))
        {
            this.setSpecialty((String)fieldV);
        }

        else if (fieldN.equals("SubSpecialty"))
        {
            this.setSubSpecialty((String)fieldV);
        }

        else if (fieldN.equals("IsListed"))
        {
             this.setIsListed((Integer)fieldV);
        }

        else if (fieldN.equals("IsPrimary"))
        {
             this.setIsPrimary((Integer)fieldV);
        }

        else if (fieldN.equals("SpecialtyBoardCertified"))
        {
             this.setSpecialtyBoardCertified((Integer)fieldV);
        }

        else if (fieldN.equals("BoardName"))
        {
            this.setBoardName((String)fieldV);
        }

        else if (fieldN.equals("ContactName"))
        {
            this.setContactName((String)fieldV);
        }

        else if (fieldN.equals("ContactEmail"))
        {
            this.setContactEmail((String)fieldV);
        }

        else if (fieldN.equals("Address1"))
        {
            this.setAddress1((String)fieldV);
        }

        else if (fieldN.equals("Address2"))
        {
            this.setAddress2((String)fieldV);
        }

        else if (fieldN.equals("City"))
        {
            this.setCity((String)fieldV);
        }

        else if (fieldN.equals("StateID"))
        {
             this.setStateID((Integer)fieldV);
        }

        else if (fieldN.equals("Province"))
        {
            this.setProvince((String)fieldV);
        }

        else if (fieldN.equals("ZIP"))
        {
            this.setZIP((String)fieldV);
        }

        else if (fieldN.equals("CountryID"))
        {
             this.setCountryID((Integer)fieldV);
        }

        else if (fieldN.equals("Phone"))
        {
            this.setPhone((String)fieldV);
        }

        else if (fieldN.equals("Fax"))
        {
            this.setFax((String)fieldV);
        }

        else if (fieldN.equals("BoardDateInitialCertified"))
        {
            this.setBoardDateInitialCertified((java.util.Date)fieldV);
        }

        else if (fieldN.equals("BoardDateRecertified"))
        {
            this.setBoardDateRecertified((java.util.Date)fieldV);
        }

        else if (fieldN.equals("ExpirationDate"))
        {
            this.setExpirationDate((java.util.Date)fieldV);
        }

        else if (fieldN.equals("DocumentNumber"))
        {
            this.setDocumentNumber((String)fieldV);
        }

        else if (fieldN.equals("IfNotCert"))
        {
            this.setIfNotCert((String)fieldV);
        }

        else if (fieldN.equals("CertEligible"))
        {
             this.setCertEligible((Integer)fieldV);
        }

        else if (fieldN.equals("EligibleStartDate"))
        {
            this.setEligibleStartDate((java.util.Date)fieldV);
        }

        else if (fieldN.equals("EligibleExpirationDate"))
        {
            this.setEligibleExpirationDate((java.util.Date)fieldV);
        }

        else if (fieldN.equals("CertPlanningToTake"))
        {
             this.setCertPlanningToTake((Integer)fieldV);
        }

        else if (fieldN.equals("CertPlanDate"))
        {
            this.setCertPlanDate((java.util.Date)fieldV);
        }

        else if (fieldN.equals("DocuLinkID"))
        {
             this.setDocuLinkID((Integer)fieldV);
        }

        else if (fieldN.equals("Comments"))
        {
            this.setComments((String)fieldV);
        }

	    }
	    catch(Exception e2)
	    {}
	    //return myVal;
	}

    public String getFieldType(String fieldN)
    {
     String  myVal = null;
     try
     {
        java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat("yyyy-MM-dd");

        if (fieldN.equals("BoardCertificationID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("UniqueCreateDate"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("UniqueModifyDate"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("UniqueModifyComments"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("PhysicianID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("Specialty"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("SubSpecialty"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("IsListed"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("IsPrimary"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("SpecialtyBoardCertified"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("BoardName"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("ContactName"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("ContactEmail"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("Address1"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("Address2"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("City"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("StateID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("Province"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("ZIP"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("CountryID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("Phone"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("Fax"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("BoardDateInitialCertified"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("BoardDateRecertified"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("ExpirationDate"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("DocumentNumber"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("IfNotCert"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("CertEligible"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("EligibleStartDate"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("EligibleExpirationDate"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("CertPlanningToTake"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("CertPlanDate"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("DocuLinkID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("Comments"))
        {
            myVal = "String";
        }

	    }
	    catch(Exception e2)
	    {}
	    return myVal;
	}

    public boolean isComplete(String fieldN)
    {
	     boolean myVal = false;
	     try
	     {
        java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat("yyyy-MM-dd");

	     if (fieldN.equals("BoardCertificationID"))

	     {
                    if (this.getBoardCertificationID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("UniqueCreateDate"))

	     {
                    if (this.getUniqueCreateDate().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("UniqueModifyDate"))

	     {
                    if (this.getUniqueModifyDate().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("UniqueModifyComments"))

	     {
                    if (this.getUniqueModifyComments().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("PhysicianID"))

	     {
                    if (this.getPhysicianID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("Specialty"))

	     {
                    if (this.getSpecialty().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("SubSpecialty"))

	     {
                    if (this.getSubSpecialty().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("IsListed"))

	     {
                    if (this.getIsListed().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("IsPrimary"))

	     {
                    if (this.getIsPrimary().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("SpecialtyBoardCertified"))

	     {
                    if (this.getSpecialtyBoardCertified().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("BoardName"))

	     {
                    if (this.getBoardName().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("ContactName"))

	     {
                    if (this.getContactName().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("ContactEmail"))

	     {
                    if (this.getContactEmail().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("Address1"))

	     {
                    if (this.getAddress1().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("Address2"))

	     {
                    if (this.getAddress2().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("City"))

	     {
                    if (this.getCity().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("StateID"))

	     {
                    if (this.getStateID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("Province"))

	     {
                    if (this.getProvince().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("ZIP"))

	     {
                    if (this.getZIP().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("CountryID"))

	     {
                    if (this.getCountryID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("Phone"))

	     {
                    if (this.getPhone().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("Fax"))

	     {
                    if (this.getFax().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("BoardDateInitialCertified"))

	     {
                    if (this.getBoardDateInitialCertified().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("BoardDateRecertified"))

	     {
                    if (this.getBoardDateRecertified().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("ExpirationDate"))

	     {
                    if (this.getExpirationDate().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("DocumentNumber"))

	     {
                    if (this.getDocumentNumber().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("IfNotCert"))

	     {
                    if (this.getIfNotCert().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("CertEligible"))

	     {
                    if (this.getCertEligible().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("EligibleStartDate"))

	     {
                    if (this.getEligibleStartDate().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("EligibleExpirationDate"))

	     {
                    if (this.getEligibleExpirationDate().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("CertPlanningToTake"))

	     {
                    if (this.getCertPlanningToTake().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("CertPlanDate"))

	     {
                    if (this.getCertPlanDate().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("DocuLinkID"))

	     {
                    if (this.getDocuLinkID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("Comments"))

	     {
                    if (this.getComments().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	    }
	    catch(Exception e2)
	    {}
	    return myVal;
	}

    public boolean isExpired(String fieldN,int daysVector)
    {
        return isExpired(fieldN,new java.util.Date(),daysVector);
    }

    public boolean isExpired(String fieldN,java.util.Date refDate,int daysVector)
    {
	     boolean myVal = false;
	     try
	     {
        java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat("yyyy-MM-dd");

        if (fieldN.equals("UniqueCreateDate"))

	     {
                    if (this.isExpired(this.getUniqueCreateDate(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

        if (fieldN.equals("UniqueModifyDate"))

	     {
                    if (this.isExpired(this.getUniqueModifyDate(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

        if (fieldN.equals("BoardDateInitialCertified"))

	     {
                    if (this.isExpired(this.getBoardDateInitialCertified(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

        if (fieldN.equals("BoardDateRecertified"))

	     {
                    if (this.isExpired(this.getBoardDateRecertified(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

        if (fieldN.equals("ExpirationDate"))

	     {
                    if (this.isExpired(this.getExpirationDate(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

        if (fieldN.equals("EligibleStartDate"))

	     {
                    if (this.isExpired(this.getEligibleStartDate(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

        if (fieldN.equals("EligibleExpirationDate"))

	     {
                    if (this.isExpired(this.getEligibleExpirationDate(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

        if (fieldN.equals("CertPlanDate"))

	     {
                    if (this.isExpired(this.getCertPlanDate(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

	    }
	    catch(Exception e2)
	    {}
	    return myVal;
	}

    public java.util.Date getDateField(String fieldN)
    {
	     java.util.Date myVal = null;
	     try
	     {
        java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat("yyyy-MM-dd");

        if (fieldN.equals("UniqueCreateDate"))

	     {
             myVal = this.getUniqueCreateDate();
        }

        if (fieldN.equals("UniqueModifyDate"))

	     {
             myVal = this.getUniqueModifyDate();
        }

        if (fieldN.equals("BoardDateInitialCertified"))

	     {
             myVal = this.getBoardDateInitialCertified();
        }

        if (fieldN.equals("BoardDateRecertified"))

	     {
             myVal = this.getBoardDateRecertified();
        }

        if (fieldN.equals("ExpirationDate"))

	     {
             myVal = this.getExpirationDate();
        }

        if (fieldN.equals("EligibleStartDate"))

	     {
             myVal = this.getEligibleStartDate();
        }

        if (fieldN.equals("EligibleExpirationDate"))

	     {
             myVal = this.getEligibleExpirationDate();
        }

        if (fieldN.equals("CertPlanDate"))

	     {
             myVal = this.getCertPlanDate();
        }

	    }
	    catch(Exception e2)
	    {}
	    return myVal;
	}
	public boolean runFieldSecurity = false;
	public Integer UserSecurityID = null;

}    // End of bltBoardCertification class definition
