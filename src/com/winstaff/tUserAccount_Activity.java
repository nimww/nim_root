package com.winstaff;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class tUserAccount_Activity{
	public static void main(String[]args){
		
	}
	public static List<UserAccountActivity> getUserAccountActivityList(){
		List<UserAccountActivity> uaal = new ArrayList<UserAccountActivity>();
		//String query = "SELECT DISTINCT ON(ua.userid) userid, ua.uniquecreatedate, refer.referredbycontactid, en.scanpass, ua.contactfirstname, ua.contactlastname, contactphone, contactfax, ua.contactemail, CASE WHEN sentto_reqrec_adj = '1800-01-01 00:00:00' THEN 'no' ELSE 'yes' END AS adjuster_ref, sentto_reqrec_adj, CASE WHEN sentto_reqrec_ncm = '1800-01-01 00:00:00' THEN 'no' ELSE 'yes' END AS ncm_ref, sentto_reqrec_ncm, CASE WHEN sentto_reqrec_adm = '1800-01-01 00:00:00' THEN 'no' ELSE 'yes' END AS adm_ref, sentto_reqrec_adm, CASE WHEN sentto_reqrec_refdr = '1800-01-01 00:00:00' THEN 'no' ELSE 'yes' END AS md_ref, sentto_reqrec_refdr FROM tuseraccount ua INNER JOIN tnim3_referral refer ON refer.referredbycontactid = ua.userid INNER JOIN tnim3_encounter en ON en.referralid = refer.referralid WHERE en.sentto_reqrec_adj >= '1800-01-01 00:00:00' OR en.sentto_reqrec_ncm >= '1800-01-01 00:00:00' OR en.sentto_reqrec_adm >= '1800-01-01 00:00:00' ORDER BY ua.userid DESC";
		String query = "SELECT DISTINCT ON(ua.userid) userid, ua.uniquecreatedate, refer.referredbycontactid, en.scanpass, ua.contactfirstname, ua.contactlastname, ua.contactphone, ua.contactfax, ua.contactemail, CASE WHEN sentto_reqrec_adj = '1800-01-01 00:00:00' THEN 'no' ELSE 'yes' END AS adjuster_ref, sentto_reqrec_adj, CASE WHEN sentto_reqrec_ncm = '1800-01-01 00:00:00' THEN 'no' ELSE 'yes' END AS ncm_ref, sentto_reqrec_ncm, CASE WHEN sentto_reqrec_adm = '1800-01-01 00:00:00' THEN 'no' ELSE 'yes' END AS adm_ref, sentto_reqrec_adm , CASE WHEN sentto_reqrec_refdr = '1800-01-01 00:00:00' THEN 'no' ELSE 'yes' END AS md_ref, sentto_reqrec_refdr FROM tuseraccount ua INNER JOIN tnim3_referral refer ON refer.referredbycontactid = ua.userid INNER JOIN tnim3_encounter en ON en.referralid = refer.referralid WHERE ua.accounttype != 'PhysicianID' and ( en.sentto_reqrec_adj >= '1800-01-01 00:00:00' OR en.sentto_reqrec_ncm >= '1800-01-01 00:00:00' OR en.sentto_reqrec_adm >= '1800-01-01 00:00:00' ) ORDER BY ua.userid DESC LIMIT 1000";
		searchDB2 ss = new searchDB2();
		ResultSet rs = ss.executeStatement(query);
		try{
			while(rs.next()){
				int i = 1;
				uaal.add(new UserAccountActivity(rs.getInt(i++), rs.getString(i++), rs.getInt(i++), rs.getString(i++), rs.getString(i++), rs.getString(i++), rs.getString(i++), rs.getString(i++), rs.getString(i++), rs.getString(i++), rs.getString(i++), rs.getString(i++), rs.getString(i++), rs.getString(i++), rs.getString(i++), rs.getString(i++), rs.getString(i++)));
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			ss.closeAll();
		}
		return uaal;
	}
	public static class UserAccountActivity{
		int uid;
		String uniCreateDate;
		int refbyconId;
		String sp;
		String first;
		String last;
		String phone;
		String fax;
		String email;
		String sentto_reqrec_adj_yesNo;
		String sentto_reqrec_adj;
		String sentto_reqrec_ncm_yesNo;
		String sentto_reqrec_ncm;
		String sentto_reqrec_adm_yesNo;
		String sentto_reqrec_adm;
		String sentto_reqrec_refdr_yesNo;
		String sentto_reqrec_refdr;
		public UserAccountActivity(int uid, String uniCreateDate,
				int refbyconId, String sp, String first, String last,
				String phone, String fax, String email,
				String sentto_reqrec_adj_yesNo, String sentto_reqrec_adj,
				String sentto_reqrec_ncm_yesNo, String sentto_reqrec_ncm,
				String sentto_reqrec_adm_yesNo, String sentto_reqrec_adm,
				String sentto_reqrec_refdr_yesNo, String sentto_reqrec_refdr) {
			super();
			this.uid = uid;
			this.uniCreateDate = uniCreateDate;
			this.refbyconId = refbyconId;
			this.sp = sp;
			this.first = first;
			this.last = last;
			this.phone = phone;
			this.fax = fax;
			this.email = email;
			this.sentto_reqrec_adj_yesNo = sentto_reqrec_adj_yesNo;
			this.sentto_reqrec_adj = sentto_reqrec_adj;
			this.sentto_reqrec_ncm_yesNo = sentto_reqrec_ncm_yesNo;
			this.sentto_reqrec_ncm = sentto_reqrec_ncm;
			this.sentto_reqrec_adm_yesNo = sentto_reqrec_adm_yesNo;
			this.sentto_reqrec_adm = sentto_reqrec_adm;
			this.sentto_reqrec_refdr_yesNo = sentto_reqrec_refdr_yesNo;
			this.sentto_reqrec_refdr = sentto_reqrec_refdr;
		}
		public int getUid() {
			return uid;
		}
		public void setUid(int uid) {
			this.uid = uid;
		}
		public String getUniCreateDate() {
			return uniCreateDate;
		}
		public void setUniCreateDate(String uniCreateDate) {
			this.uniCreateDate = uniCreateDate;
		}
		public int getRefbyconId() {
			return refbyconId;
		}
		public void setRefbyconId(int refbyconId) {
			this.refbyconId = refbyconId;
		}
		public String getSp() {
			return sp;
		}
		public void setSp(String sp) {
			this.sp = sp;
		}
		public String getFirst() {
			return first;
		}
		public void setFirst(String first) {
			this.first = first;
		}
		public String getLast() {
			return last;
		}
		public void setLast(String last) {
			this.last = last;
		}
		public String getPhone() {
			return phone;
		}
		public void setPhone(String phone) {
			this.phone = phone;
		}
		public String getFax() {
			return fax;
		}
		public void setFax(String fax) {
			this.fax = fax;
		}
		public String getEmail() {
			return email;
		}
		public void setEmail(String email) {
			this.email = email;
		}
		public String getSentto_reqrec_adj_yesNo() {
			return sentto_reqrec_adj_yesNo;
		}
		public void setSentto_reqrec_adj_yesNo(String sentto_reqrec_adj_yesNo) {
			this.sentto_reqrec_adj_yesNo = sentto_reqrec_adj_yesNo;
		}
		public String getSentto_reqrec_adj() {
			return sentto_reqrec_adj;
		}
		public void setSentto_reqrec_adj(String sentto_reqrec_adj) {
			this.sentto_reqrec_adj = sentto_reqrec_adj;
		}
		public String getSentto_reqrec_ncm_yesNo() {
			return sentto_reqrec_ncm_yesNo;
		}
		public void setSentto_reqrec_ncm_yesNo(String sentto_reqrec_ncm_yesNo) {
			this.sentto_reqrec_ncm_yesNo = sentto_reqrec_ncm_yesNo;
		}
		public String getSentto_reqrec_ncm() {
			return sentto_reqrec_ncm;
		}
		public void setSentto_reqrec_ncm(String sentto_reqrec_ncm) {
			this.sentto_reqrec_ncm = sentto_reqrec_ncm;
		}
		public String getSentto_reqrec_adm_yesNo() {
			return sentto_reqrec_adm_yesNo;
		}
		public void setSentto_reqrec_adm_yesNo(String sentto_reqrec_adm_yesNo) {
			this.sentto_reqrec_adm_yesNo = sentto_reqrec_adm_yesNo;
		}
		public String getSentto_reqrec_adm() {
			return sentto_reqrec_adm;
		}
		public void setSentto_reqrec_adm(String sentto_reqrec_adm) {
			this.sentto_reqrec_adm = sentto_reqrec_adm;
		}
		public String getSentto_reqrec_refdr_yesNo() {
			return sentto_reqrec_refdr_yesNo;
		}
		public void setSentto_reqrec_refdr_yesNo(String sentto_reqrec_refdr_yesNo) {
			this.sentto_reqrec_refdr_yesNo = sentto_reqrec_refdr_yesNo;
		}
		public String getSentto_reqrec_refdr() {
			return sentto_reqrec_refdr;
		}
		public void setSentto_reqrec_refdr(String sentto_reqrec_refdr) {
			this.sentto_reqrec_refdr = sentto_reqrec_refdr;
		}
	}
}