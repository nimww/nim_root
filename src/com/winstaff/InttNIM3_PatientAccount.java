

package com.winstaff;

import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public interface InttNIM3_PatientAccount extends dbTableInterface
{

    public void setPatientID(Integer newValue);
    public Integer getPatientID();
    public void setUniqueCreateDate(Date newValue);
    public Date getUniqueCreateDate();
    public void setUniqueModifyDate(Date newValue);
    public Date getUniqueModifyDate();
    public void setUniqueModifyComments(String newValue);
    public String getUniqueModifyComments();
    public void setPayerID(Integer newValue);
    public Integer getPayerID();
    public void setPayerAccountIDNumber(String newValue);
    public String getPayerAccountIDNumber();
    public void setPatientFirstName(String newValue);
    public String getPatientFirstName();
    public void setPatientLastName(String newValue);
    public String getPatientLastName();
    public void setPatientAccountNumber(String newValue);
    public String getPatientAccountNumber();
    public void setPatientExpirationDate(Date newValue);
    public Date getPatientExpirationDate();
    public void setPatientIsActive(Integer newValue);
    public Integer getPatientIsActive();
    public void setPatientDOB(Date newValue);
    public Date getPatientDOB();
    public void setPatientSSN(String newValue);
    public String getPatientSSN();
    public void setPatientGender(String newValue);
    public String getPatientGender();
    public void setPatientAddress1(String newValue);
    public String getPatientAddress1();
    public void setPatientAddress2(String newValue);
    public String getPatientAddress2();
    public void setPatientCity(String newValue);
    public String getPatientCity();
    public void setPatientStateID(Integer newValue);
    public Integer getPatientStateID();
    public void setPatientZIP(String newValue);
    public String getPatientZIP();
    public void setPatientHomePhone(String newValue);
    public String getPatientHomePhone();
    public void setPatientWorkPhone(String newValue);
    public String getPatientWorkPhone();
    public void setPatientCellPhone(String newValue);
    public String getPatientCellPhone();
    public void setPatientCCAccountFullName(String newValue);
    public String getPatientCCAccountFullName();
    public void setPatientCCAccountNumber1(String newValue);
    public String getPatientCCAccountNumber1();
    public void setPatientCCAccountNumber2(String newValue);
    public String getPatientCCAccountNumber2();
    public void setPatientCCAccountType(String newValue);
    public String getPatientCCAccountType();
    public void setPatientCCAccountExpMonth(String newValue);
    public String getPatientCCAccountExpMonth();
    public void setPatientCCAccountExpYear(String newValue);
    public String getPatientCCAccountExpYear();
    public void setEmployerID(Integer newValue);
    public Integer getEmployerID();
    public void setJobID(Integer newValue);
    public Integer getJobID();
    public void setJobNotes(String newValue);
    public String getJobNotes();
    public void setSupervisorName(String newValue);
    public String getSupervisorName();
    public void setSupervisorPhone(String newValue);
    public String getSupervisorPhone();
    public void setSupervisorFax(String newValue);
    public String getSupervisorFax();
    public void setComments(String newValue);
    public String getComments();
    public void setAuditNotes(String newValue);
    public String getAuditNotes();
}    // End of bltNIM3_PatientAccount class definition
