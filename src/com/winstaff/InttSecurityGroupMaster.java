package com.winstaff;


import com.winstaff.dbtSecurityGroupMaster;
import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public interface InttSecurityGroupMaster extends dbTableInterface
{

    public void setSecurityGroupID(Integer newValue);
    public Integer getSecurityGroupID();
    public void setUniqueCreateDate(Date newValue);
    public Date getUniqueCreateDate();
    public void setUniqueModifyDate(Date newValue);
    public Date getUniqueModifyDate();
    public void setUniqueModifyComments(String newValue);
    public String getUniqueModifyComments();
    public void setGroupName(String newValue);
    public String getGroupName();
    public void setRunFieldSecurityID(Integer newValue);
    public Integer getRunFieldSecurityID();
    public void setShowAuditID(Integer newValue);
    public Integer getShowAuditID();
}    // End of bltSecurityGroupMaster class definition
