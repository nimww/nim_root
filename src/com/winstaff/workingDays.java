package com.winstaff;

import java.util.Date;

/**
 * Created by IntelliJ IDEA.
 * User: ste
 * Date: 7/27/11
 * Time: 12:47 PM
 * To change this template use File | Settings | File Templates.
 */
public class workingDays {

    public static int getTotalDays (Date myStart, Date myEnd)
    {
        org.joda.time.DateTime startDate = new  org.joda.time.DateTime(myStart);
        org.joda.time.DateTime endDate = new  org.joda.time.DateTime(myEnd);
        org.joda.time.Days d = org.joda.time.Days.daysBetween(startDate,endDate);
        return d.getDays();
    }

    public static int getWorkingDays (Date myStart, Date myEnd)
    {
        org.joda.time.DateTime tempDate = new  org.joda.time.DateTime(myStart);
        org.joda.time.DateTime endDate = new  org.joda.time.DateTime(myEnd);
        int countDays = 0;
        while (tempDate.isBefore(endDate))
        {
            if (tempDate.getDayOfWeek()!=6&&tempDate.getDayOfWeek()!=7)
            {
                countDays++;
            }
            tempDate = tempDate.plusDays(1);
        }
        return countDays;
    }

    public static int getWorkingHours (Date myStart, Date myEnd)
    {
        return getWorkingHours(myStart, myEnd, 5,18,false);
    }


    public static int getWorkingHours (Date myStart, Date myEnd, int hourStart, int hourStop, boolean includeWeekends)
    {
        org.joda.time.DateTime tempDate = new  org.joda.time.DateTime(myStart);
        org.joda.time.DateTime endDate = new  org.joda.time.DateTime(myEnd);
        int countHours = 0;
        while (tempDate.isBefore(endDate))
        {
            boolean isWeekday = tempDate.getDayOfWeek()!=6&&tempDate.getDayOfWeek()!=7;
            if ( (includeWeekends) || (!includeWeekends&&isWeekday) )
            {
                if (tempDate.getHourOfDay()>hourStart&&tempDate.getHourOfDay()<hourStop)
                {
                    countHours++;
                }
            }
            tempDate = tempDate.plusHours(1);
        }
        return countHours;
    }

    public static int getMinutes (Date myStart, Date myEnd)
    {
        org.joda.time.DateTime tempDate = new  org.joda.time.DateTime(myStart);
        org.joda.time.DateTime endDate = new  org.joda.time.DateTime(myEnd);
        org.joda.time.Minutes M = org.joda.time.Minutes.minutesBetween(tempDate,endDate);
        return  M.getMinutes();
    }




    public static Date getDate_AddMinutes( Date start, int minutes){
        org.joda.time.DateTime tempDate = new  org.joda.time.DateTime(start);
        return tempDate.plusMinutes(minutes).toDate();

    }



    public static String getWorkingHours_displayEasy (Date myStart, Date myEnd, int hourStart, int hourStop, boolean includeWeekends)
    {
        int i5 = getWorkingHours(myStart, myEnd, hourStart,hourStop,includeWeekends);
        int m5 = getMinutes(myStart,myEnd);
        if (m5<1){
            return "instant";
        } else if (i5<2){
            return "about " + m5 + " minutes";
        } else if (i5<2){
            return "about 1 hour";
        } else {
            return "about " + i5 + " hours";
        }

    }






}
