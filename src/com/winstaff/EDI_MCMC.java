package com.winstaff;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;

import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.net.ftp.FTPReply;
import org.apache.commons.net.ftp.FTPSClient;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;

public class EDI_MCMC {
	private String TOMCAT = "tomcat7";
	private final String localDir = "/var/lib/" + TOMCAT + "/webapps/ROOT/WEB-INF/classes/MCMC";
	private String remoteDir = "/incoming/fromMCMC";
	private String host = "gw3.mcmcllc.com";
	private String user = "nextimage";
	private String password = "WbU6SwYvtjGijBRgcFGACHZUeIdQDLZy";
	private HashMap<Integer, MCMCPrice> hm = new HashMap<Integer, MCMCPrice>();

	public static void main(String args[]) throws SQLException, IOException {
		EDI_MCMC mcmc = new EDI_MCMC();
		mcmc.runCheck();
		mcmc.processFiles();
		mcmc.runNumbers();
		mcmc.generateResponseFile();
		//upload response files
	}
	
	private void runNumbers() throws SQLException {
		List<Integer> rejected = new ArrayList<Integer>();
		List<Integer> approved = new ArrayList<Integer>();
		List<Integer> ota = new ArrayList<Integer>();
		fillMCMCPrice();
		
		
		//Deny all TC/26 encounters
		String query = "select mcmcid from tmcmc_retros where (lower(modifier1) in ('26','tc') or lower(modifier2) in ('26','tc')) and statusid = 0";
		searchDB2 conn = new searchDB2();
		ResultSet rs = conn.executeStatement(query);
		System.out.println("Deny TC/26 Claims:");
		while (rs.next()) {
			bltMCMC_Retros mcmc = new bltMCMC_Retros(rs.getInt("mcmcid"));
			System.out.println("\tMCMC ID: "+rs.getInt("mcmcid")+"\tDeny");
			mcmc.setStatusID(3);

			mcmc.commitData();

			rejected.add(rs.getInt("mcmcid"));
		}
		
		//approve/deny tc/26
		
		
		
		
		
		//Approve encounters by matching TIN and zip
		//query = "select DISTINCT on (mcmcid) mcmc.mcmcid, pm.practiceid, ms.stateid, cpt.\"CPTGroup\" \"mod\", mcmc.mcmcallowance from tmcmc_retros mcmc join tpracticemaster pm on replace(pm.officefederaltaxid,'-', '') = mcmc.renderingprovidertaxid join tstateli ms on lower(renderingproviderstate) = lower(ms.shortstate) join \"CPTGroup\" cpt on mcmc.cptcode = cast(cpt.\"CPT\" as varchar) where lower(modifier1) not in ('26','tc') and substr(pm.officezip, 1, 5) = substr(renderingproviderzip, 1, 5) and statusid = 0 and pm.contractingstatusid in (2,8)";
		query = "select DISTINCT on (mcmcid) mcmc.mcmcid, pm.practiceid, ms.stateid, cpt.\"CPTGroup\" \"mod\", mcmc.mcmcallowance from tmcmc_retros mcmc join tpracticemaster pm on replace(pm.officefederaltaxid,'-', '') = mcmc.renderingprovidertaxid join tstateli ms on lower(renderingproviderstate) = lower(ms.shortstate) join \"CPTGroup\" cpt on mcmc.cptcode = cast(cpt.\"CPT\" as varchar) where lower(modifier1) not in ('26','tc') and statusid = 0 and pm.contractingstatusid in (2,8)";
		rs = conn.executeStatement(query);
		System.out.println("Claims With TIN & ZIP match:");
		while (rs.next()) {
			bltMCMC_Retros mcmc = new bltMCMC_Retros(rs.getInt("mcmcid"));
			if (rs.getDouble("mcmcallowance") > getDMCMCPrice(rs.getInt("stateid"), rs.getString("mod"))) {
				System.out.println("\tMCMC ID: "+rs.getInt("mcmcid")+"\tApprove\tMCMC Allowance: "+rs.getDouble("mcmcallowance") +"\tNIM Price: "+getDMCMCPrice(rs.getInt("stateid"), rs.getString("mod")));
				mcmc.setNetworkAllowance(getDMCMCPrice(rs.getInt("stateid"), rs.getString("mod")));
				mcmc.setNetworkSavings(rs.getDouble("mcmcallowance")-getDMCMCPrice(rs.getInt("stateid"), rs.getString("mod")));
				mcmc.setStatusID(1);
				approved.add(mcmc.getMCMCID());
			} else {
				System.out.println("\tMCMC ID: "+rs.getInt("mcmcid")+"\tDeny\tMCMC Allowance: "+rs.getDouble("mcmcallowance") +"\tNIM Price: "+getDMCMCPrice(rs.getInt("stateid"), rs.getString("mod")));
				mcmc.setStatusID(3);
				rejected.add(mcmc.getMCMCID());
			}

			mcmc.commitData();
		}
		//Handle retro-ota by sending them to NetDev (The left overs)
		query = "select DISTINCT on (mcmcid) mcmc.mcmcid, pm.practiceid, ms.stateid, cpt.\"CPTGroup\" \"mod\", mcmc.mcmcallowance from tmcmc_retros mcmc join tpracticemaster pm on replace(pm.officefederaltaxid,'-', '') = mcmc.renderingprovidertaxid join tstateli ms on lower(renderingproviderstate) = lower(ms.shortstate) join \"CPTGroup\" cpt on mcmc.cptcode = cast(cpt.\"CPT\" as varchar) where lower(modifier1) not in ('26','tc') and statusid = 0";
		System.out.println("Send OTA Claims to NetDev:");
		rs = conn.executeStatement(query);
		while (rs.next()) {
			System.out.println("\tMCMC ID: "+rs.getInt("mcmcid")+"\tOTA");
			bltMCMC_Retros mcmc = new bltMCMC_Retros(rs.getInt("mcmcid"));
			mcmc.setStatusID(5);
			mcmc.commitData();
			ota.add(rs.getInt("mcmcid"));
		}
		conn.closeAll();
		//generateResponseFile(rejected, approved);
	}

	private void createRetroCase(int mcmcID){
		bltNIM3_CaseAccount ca = new bltNIM3_CaseAccount();
		bltNIM3_Referral rf = new bltNIM3_Referral();
		bltNIM3_Encounter en = new bltNIM3_Encounter();
		bltNIM3_Appointment ap = new bltNIM3_Appointment();
		bltNIM3_Service sr = new bltNIM3_Service();
		bltNIM3_CommTrack ct = new bltNIM3_CommTrack();
		
		bltMCMC_Retros mcmc = new bltMCMC_Retros(mcmcID);
		SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd");
		String commtracks = "";
		ca.setPatientFirstName(mcmc.getPatientFirstName());
        ca.setPatientLastName(mcmc.getPatientLastName());
        ca.setPatientAddress1(mcmc.getPatientAddress1());
        ca.setPatientCity(mcmc.getPatientCity());
        ca.setPatientZIP(mcmc.getPatientZip());
        ca.setPayerID(1375);
        ca.setPatientGender("unknown");
        try {
			ca.setPatientDOB(df.parse(mcmc.getDateOfBirth()));
		} catch (ParseException e1) {
		}
        try {
        	
        	searchDB2 conn = new searchDB2();
        	ResultSet rs = conn.executeStatement("select stateid from tstateli where shortstate = '"+mcmc.getPatientState()+"'");
			rs.next();
        	ca.setPatientStateID(rs.getInt("stateid"));
        	conn.closeAll();
		} catch (SQLException e1) {
		}
        ca.setCaseStatusID(new Integer(1));
        ca.setUniqueCreateDate(PLCUtils.getNowDate(false));
        ca.setUniqueModifyDate(PLCUtils.getNowDate(false));
        ca.setUniqueModifyComments("MCMC EDI Process");
        ca.setPatientSSN("Not Given");
        ca.setCaseClaimNumber(mcmc.getClaimNumber());
        ca.setAdjusterID(33411);//MCMC adjuster place holder account
        ca.setPayerID(1599);
        try {
        	 ca.setDateOfInjury(df.parse(mcmc.getInjuryDate()));
		} catch (ParseException e1) {
		}
       
        try {
			ca.commitData();
		} catch (SQLException e) {
			e.printStackTrace();
		}
        ca.setCaseCode("CP" +  ca.getUniqueID() + com.winstaff.password.RandomString.generateString(2).toUpperCase() + Math.round(Math.random()*100)+com.winstaff.password.RandomString.generateString(2).toUpperCase());
        ca.setAssignedToID(23611);//jess

        try {
			ca.commitData();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
        rf.setCaseID(ca.getUniqueID());
        rf.setUniqueCreateDate(PLCUtils.getNowDate(false));
        rf.setUniqueModifyDate(PLCUtils.getNowDate(false));
        rf.setUniqueModifyComments("MCMC EDI Process");
        
        rf.setReferralTypeID(3);
        rf.setReferralDate(PLCUtils.getNowDate(true));
        rf.setReceiveDate(PLCUtils.getNowDate(true));

        rf.setReferralStatusID(1);

        rf.setAttendingPhysicianID(33411);
        rf.setReferringPhysicianID(33411);
        rf.setReferralMethod("Online (Sec)");
        
        rf.setReferredByContactID(33411);

        rf.setRxFileID(NIMUtils.EncounterBypassRXID);
        rf.setOrderFileID(NIMUtils.EncounterBypassRXID);
        rf.setComments("THIS IS AN MCMC RETRO CASE SEE VASANTHI.");
        try {
			rf.commitData();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
        en.setReferralID(rf.getUniqueID());
        en.setUniqueCreateDate(PLCUtils.getNowDate(false));
        en.setUniqueModifyDate(PLCUtils.getNowDate(false));
        en.setAttendingPhysicianID(52);
        en.setUniqueModifyComments("MCMC EDI Process");
        en.setEncounterStatusID(1);
        en.setRequiresHandCarryCD(1);

        //fill in report
        en.setReportFileID(NIMUtils.EncounterBypassRXID);
        en.setSentToAdj(PLCUtils.getSubDate("na"));
        en.setSentToRefDr(PLCUtils.getSubDate("na"));
        en.setTimeTrack_ReqRpReview(PLCUtils.getSubDate("na"));
        en.setTimeTrack_ReqRpReview_UserID(33411);

        
        en.setTimeTrack_ReqApproved(PLCUtils.getSubDate("na"));
        en.setTimeTrack_ReqApproved_UserID(33411);

        en.setTimeTrack_ReqCreated(PLCUtils.getSubDate("na"));
        en.setTimeTrack_ReqCreated_UserID(33411);

        en.setTimeTrack_ReqDelivered(PLCUtils.getSubDate("na"));
        en.setTimeTrack_ReqDelivered_UserID(33411);

        en.setTimeTrack_ReqInitialAppointment(PLCUtils.getSubDate("na"));
        en.setTimeTrack_ReqInitialAppointment_UserID(33411);

        en.setTimeTrack_ReqProc(PLCUtils.getSubDate("na"));
        en.setTimeTrack_ReqProc_UserID(33411);

        en.setTimeTrack_ReqRec(PLCUtils.getSubDate("na"));
        en.setTimeTrack_ReqRec_UserID(33411);

        en.setTimeTrack_ReqRxReview(PLCUtils.getSubDate("na"));
        en.setTimeTrack_ReqRxReview_UserID(33411);

        en.setTimeTrack_ReqSched(PLCUtils.getSubDate("na"));
        en.setTimeTrack_ReqSched_UserID(33411);

        en.setSentTo_ReqRec_Adj(PLCUtils.getSubDate("na"));
        en.setSentTo_DataProc_Adj(PLCUtils.getSubDate("na"));
        en.setSentTo_DataProc_RefDr(PLCUtils.getSubDate("na"));
        en.setSentTo_ReqRec_RefDr(PLCUtils.getSubDate("na"));
        en.setSentTo_SP_RefDr(PLCUtils.getSubDate("na"));
        en.setSentTo_SP_Adj(PLCUtils.getSubDate("na"));
        en.getisRetro(1);
        
        
        //first let's convert the wizard into CPT Group Object 2
        String modal ="";
		try {
			searchDB2 conn = new searchDB2();
			ResultSet rs = conn.executeStatement("select * from \"CPTGroup\" where \"CPT\" = "+mcmc.getCPTCode());
			rs.next();
			
			modal = rs.getString("CPTGroup");
			conn.closeAll();
		} catch (SQLException e1) {
			e1.printStackTrace();
		}

//        value=4>Diag: MRI</option>
//        value=5>Diag: CT</option>
//        value=6>Diag: EMG</option>
//        value=7>Diag: NCV</option>
//        value=8>Diag: PET</option>
//        value=9>Diag: Other</option>


        if (modal.equals("mr_wo")||modal.equals("mr_w")||modal.equals("mr_wwo")){
            en.setEncounterTypeID(4);
        } else if (modal.equals("ct_wo")||modal.equals("ct_w")||modal.equals("ct_wwo")){
            en.setEncounterTypeID(5);
        } else {
            en.setEncounterTypeID(9);
        }

        try {
        	searchDB2 conn = new searchDB2();
        	ResultSet rs = conn.executeStatement("select DISTINCT on (mcmcid) mcmc.mcmcid, pm.practiceid from tmcmc_retros mcmc join tpracticemaster pm on replace(pm.officefederaltaxid,'-', '') = mcmc.renderingprovidertaxid where mcmcid = "+mcmc.getMCMCID());
        	//ResultSet rs = conn.executeStatement("select DISTINCT on (mcmcid) mcmc.mcmcid, pm.practiceid from tmcmc_retros mcmc join tpracticemaster pm on replace(pm.officefederaltaxid,'-', '') = mcmc.renderingprovidertaxid where substr(pm.officezip, 1, 5) = substr(renderingproviderzip, 1, 5) and mcmcid = "+mcmc.getMCMCID());
			rs.next();
        	en.setSeeNetDev_SelectedPracticeID(rs.getInt("mcmcid"));
        	conn.closeAll();
		} catch (SQLException e) {
			e.printStackTrace();
		}
        try {
			en.commitData();
		} catch (SQLException e) {
			e.printStackTrace();
		}
        en.setScanPass("SP" +  en.getUniqueID() + com.winstaff.password.RandomString.generateString(2).toUpperCase() + Math.round(Math.random()*100)+com.winstaff.password.RandomString.generateString(2).toUpperCase());
        try {
			en.commitData();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
        try {
        	searchDB2 conn = new searchDB2();
        	ResultSet rs = conn.executeStatement("select DISTINCT on (mcmcid) mcmc.mcmcid, pm.practiceid from tmcmc_retros mcmc join tpracticemaster pm on replace(pm.officefederaltaxid,'-', '') = mcmc.renderingprovidertaxid and mcmcid = "+mcmc.getMCMCID());
			rs.next();
        	ap.setProviderID(rs.getInt("practiceid"));
        	conn.closeAll();
		} catch (SQLException e) {
			e.printStackTrace();
		}
        
        try{
        	searchDB2 conn = new searchDB2();
        	ResultSet rs = conn.executeStatement("SELECT cptcode, modifier1 FROM tmcmc_retros where mcmcid = " + mcmc.getMCMCID());
        	rs.next();
        	sr.setCPT(rs.getString("cptcode"));
        	sr.setCPTBodyPart(rs.getString("modifier1"));
        	sr.setCPTQty(1);
        }catch(Exception e){}
        
        
        ap.setiStatus(1);
        ap.setAppointmentTime(PLCUtils.getNowDate(false));
        ap.setAppointmentConfirmation("NID_ON_"+com.winstaff.password.RandomString.generateString(2).toUpperCase() + Math.round(Math.random()*100)+com.winstaff.password.RandomString.generateString(2).toUpperCase());
        ap.setInitialEncounterID(en.getUniqueID());
        try {
			ap.commitData();
		} catch (SQLException e) {
			e.printStackTrace();
		}

        //Now let's save the appointment into the Encounter
        en.setAppointmentID(ap.getUniqueID());
        //set date of service to tomorrow
        en.setDateOfService(PLCUtils.getSubDate("t+1"));
        en.setNextActionDate(PLCUtils.getSubDate("tam"));
        en.setNextActionNotes("Ensure Retro is good");
        try {
			en.commitData();
		} catch (SQLException e) {
			e.printStackTrace();
		}
        try {
        ct.setUniqueCreateDate(PLCUtils.getNowDate(false));
		ct.setEncounterID(en.getEncounterID());
		ct.setMessageText("AutoGen MCMC Retro Case");
		ct.setCommStart(PLCUtils.getNowDate(false));
		ct.setCommEnd(PLCUtils.getNowDate(false));
		ct.setMessageName("From MCMC" + " " + "AutoGen Case");
		ct.setMessageCompany("AutoGen MCMC");
		ct.setCaseID(ca.getUniqueID());
		ct.setEncounterID(en.getUniqueID());
		ct.setReferralID(rf.getReferralID());
		ct.setCommTypeID(2);
			ct.commitData();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	private void generateResponseFile() throws SQLException, IOException{
		searchDB2 conn = new searchDB2();
		
		String toMCMC = "";
		boolean isHeader = true;
		boolean isMCMC = false;
		String query = "select * from tmcmc_retros where statusid in (1, 3)";
		System.out.println(query);
		ResultSet rs = conn.executeStatement(query);
		while (rs.next()) {
			isMCMC = true;
			if(isHeader){
				toMCMC+=rs.getMetaData().getColumnName(5);
				for (int x = 6; x < rs.getMetaData().getColumnCount();x++){
					toMCMC +="|"+rs.getMetaData().getColumnName(x);
				}
				toMCMC +="\n";
				isHeader = false;
				System.out.println("header");
			}
			
			bltMCMC_Retros mcmc = new bltMCMC_Retros(rs.getInt("mcmcid"));
			
			if(mcmc.getStatusID()==1){
				createRetroCase(mcmc.getMCMCID());
				mcmc.setStatusID(2);
			} else {
				mcmc.setStatusID(4);
			}
			
			mcmc.commitData();
			
			toMCMC +=rs.getString(5);
			System.out.println("body");
			for (int x = 6; x < rs.getMetaData().getColumnCount();x++){
				toMCMC +="|"+(x == 54 ? (rs.getInt(x)==0?"":rs.getString(x)):rs.getString(x));
			}
			toMCMC +="\n";
		}
		if(isMCMC){
			DataOutputStream out = new DataOutputStream( new FileOutputStream( "/MCMC/NIM_MCMC_S4"+(new SimpleDateFormat("yyyyMMdd").format(new Date())+".txt") ) );
			out.writeBytes(toMCMC);
			out.close();
		}
		
		isHeader = true;
		String toNetDev = "";
		boolean isnetDev = false;
		query = "select * from tmcmc_retros where statusid in (5)";
		rs = conn.executeStatement(query);
		
		while (rs.next()) {
			isnetDev = true;
			if(isHeader){
				toNetDev+=rs.getMetaData().getColumnName(5);
				for (int x = 6; x < rs.getMetaData().getColumnCount();x++){
					toNetDev +="|"+rs.getMetaData().getColumnName(x);
				}
				toNetDev +="\n";
				isHeader = false;
			}
			
			bltMCMC_Retros mcmc = new bltMCMC_Retros(rs.getInt("mcmcid"));
			mcmc.setStatusID(6);
			mcmc.commitData();
			toNetDev +=rs.getString(5);
			
			for (int x = 6; x < rs.getMetaData().getColumnCount();x++){
				toNetDev +="|"+(x == 54 ? (rs.getInt(x)==0?"":rs.getString(x)):rs.getString(x));
			}
			toNetDev +="\n";
			
		}
		
		if(isnetDev){
			email( "retros@nextimagemedical.com",  "vasanthi.murulidhar@nextimagemedical.com",  "MCMC OTAs",  toNetDev);
		}
		
		//System.out.println(toMCMC);
		conn.closeAll();
	}
	
	private double getDMCMCPrice(int stateid, String mod) {
		double dReturn = 0;

		if (mod.equals("mr_wo")) {
			dReturn = hm.get(stateid).getMrwo();
		} else if (mod.equals("mr_w")) {
			dReturn = hm.get(stateid).getMrw();
		} else if (mod.equals("mr_wwo")) {
			dReturn = hm.get(stateid).getMrwwo();
		} else if (mod.equals("ct_wo")) {
			dReturn = hm.get(stateid).getCtwo();
		} else if (mod.equals("ct_w")) {
			dReturn = hm.get(stateid).getCtw();
		} else if (mod.equals("ct_wwo")) {
			dReturn = hm.get(stateid).getCtwwo();
		}

		return dReturn;
	}

	private void fillMCMCPrice() {
		//NOV 1, 2013
		hm.put(30, new MCMCPrice("Alabama",525,630,750,270,320,375));
		hm.put(8, new MCMCPrice("Arizona",610,710,810,360,400,440));
		hm.put(21, new MCMCPrice("Arkansas",500,595,725,300,350,425));
		hm.put(1, new MCMCPrice("California",480,560,650,290,340,400));
		hm.put(10, new MCMCPrice("Colorado",595,720,800,310,360,410));
		hm.put(45, new MCMCPrice("Connecticut",750,875,950,440,495,565));
		hm.put(47, new MCMCPrice("Delaware",620,725,875,375,450,525));
		hm.put(51, new MCMCPrice("District of Columbia",520,575,700,290,360,420));
		hm.put(33, new MCMCPrice("Florida",435,525,650,245,320,375));
		hm.put(32, new MCMCPrice("Georgia",495,575,775,290,340,390));
		hm.put(50, new MCMCPrice("Hawaii",775,845,925,395,485,575));
		hm.put(5, new MCMCPrice("Idaho",595,695,795,475,525,550));
		hm.put(24, new MCMCPrice("Illinois",575,650,750,350,375,475));
		hm.put(27, new MCMCPrice("Indiana",690,730,845,475,495,525));
		hm.put(19, new MCMCPrice("Iowa",750,890,990,400,465,600));
		hm.put(15, new MCMCPrice("Kansas",700,750,850,300,375,475));
		hm.put(28, new MCMCPrice("Kentucky",570,725,825,350,400,500));
		hm.put(48, new MCMCPrice("Maryland",515,590,840,350,400,500));
		hm.put(43, new MCMCPrice("Massachusetts",500,575,895,195,225,295));
		hm.put(26, new MCMCPrice("Michigan",585,735,895,315,375,425));
		hm.put(18, new MCMCPrice("Minnesota",750,900,1050,450,550,625));
		hm.put(25, new MCMCPrice("Mississippi",750,840,1060,375,425,475));
		hm.put(20, new MCMCPrice("Missouri",740,840,895,395,445,495));
		hm.put(14, new MCMCPrice("Nebraska",695,775,875,400,475,550));
		hm.put(6, new MCMCPrice("Nevada",550,660,760,330,365,435));
		hm.put(46, new MCMCPrice("New Jersey",485,650,850,325,400,450));
		hm.put(11, new MCMCPrice("New Mexico",750,850,875,350,400,450));
		hm.put(37, new MCMCPrice("New York",540,620,875,295,345,395));
		hm.put(41, new MCMCPrice("North Carolina",750,850,950,350,450,510));
		hm.put(31, new MCMCPrice("Ohio",575,640,850,250,300,390));
		hm.put(16, new MCMCPrice("Oklahoma",750,825,925,350,400,450));
		hm.put(38, new MCMCPrice("Pennsylvania",465,630,800,265,335,435));
		hm.put(44, new MCMCPrice("Rhode Island",590,725,750,325,350,450));
		hm.put(42, new MCMCPrice("South Carolina",495,595,695,285,405,430));
		hm.put(13, new MCMCPrice("South Dakota",920,1020,1320,760,1100,1210));
		hm.put(29, new MCMCPrice("Tennessee",595,670,800,395,465,525));
		hm.put(17, new MCMCPrice("Texas",595,780,950,395,440,495));
		hm.put(7, new MCMCPrice("Utah",560,700,800,280,360,450));
		hm.put(36, new MCMCPrice("Vermont",600,700,825,395,465,495));
		hm.put(2, new MCMCPrice("Washington",670,730,895,410,495,525));

	}

	private void email(String emailTo, String emailFrom, String theSubject, String theBody) throws SQLException {
		bltEmailTransaction email = new bltEmailTransaction();
		email.setEmailTo(emailTo);
		email.setEmailFrom(emailFrom);
		email.setEmailSubject(theSubject);
		email.setEmailBody(theBody);
		email.setEmailBodyType(emailType_V3.PLAIN_TYPE);
		email.setTransactionDate(new java.util.Date());
		email.commitData();
	}

	private void processFiles() throws IOException {
		String newFolder = new SimpleDateFormat("yyyyMMdd").format(new Date());
		System.out.println("mkdir: " + localDir + "/" + newFolder);

		File file = new File(localDir);

		for (File s : file.listFiles()) {
			if (s.isFile()) {
				new File(localDir + "/" + newFolder).mkdir();
			}
		}

		for (File s : file.listFiles()) {
			if (s.isFile()) {
				System.out.println("Load to DB: " + localDir + "/" + newFolder + "/" + s.getName());
			}
			if (s.isFile() && loadToDB(s)) {
				System.out.println("\tOK - Move File: " + localDir + "/" + newFolder + "/" + s.getName());
				s.renameTo(new File(localDir + "/" + newFolder + "/" + s.getName()));
			}
		}
	}

	private Integer intProcessing(String s) {
		int rInt = 0;
		if (!s.isEmpty()) {
			rInt = new Integer(s);
		}
		return rInt;
	}

	private Double doubleProcessing(String s) {
		Double rDouble = (double) 0;
		if (!s.isEmpty()) {
			rDouble = new Double(s);
		}
		return rDouble;
	}

	private boolean loadToDB(File file) throws IOException {
		boolean isCommit = false;
		BufferedReader br = new BufferedReader(new FileReader(file));
		String line = null;

		while ((line = br.readLine()) != null) {
			try {
				String[] s = line.split("\\|");

				bltMCMC_Retros mcmc = new bltMCMC_Retros();

				mcmc.setMCMCBillID(intProcessing(s[0]));
				mcmc.setNetworkBillID(intProcessing(s[1]));
				mcmc.setMCMCLocation(s[2]);
				mcmc.setRenderingProviderTaxID(s[3]);
				mcmc.setRenderingProviderName(s[4]);
				mcmc.setRenderingProviderAddress(s[5]);
				mcmc.setRenderingProviderCity(s[6]);
				mcmc.setRenderingProviderState(s[7]);
				mcmc.setRenderingProviderZip(s[8]);
				mcmc.setBillingProviderTaxID(s[9]);
				mcmc.setBillingProviderName(s[10]);
				mcmc.setBillingProviderAddress(s[11]);
				mcmc.setBillingProviderCity(s[12]);
				mcmc.setBillingProviderState(s[13]);
				mcmc.setBillingProviderZip(s[14]);
				mcmc.setClientName(s[15]);
				mcmc.setClaimNumber(s[16]);
				mcmc.setPatientSSN(s[17]);
				mcmc.setPatientFirstName(s[18]);
				mcmc.setPatientLastName(s[19]);
				mcmc.setPatientAddress1(s[20]);
				mcmc.setPatientAddress2(s[21]);
				mcmc.setPatientCity(s[22]);
				mcmc.setPatientState(s[23]);
				mcmc.setPatientZip(s[24]);
				mcmc.setEmployerName(s[25]);
				mcmc.setEmployerAddress1(s[26]);
				mcmc.setEmployerAddress2(s[27]);
				mcmc.setEmployerCity(s[28]);
				mcmc.setEmployerState(s[29]);
				mcmc.setEmployerZip(s[30]);
				mcmc.setDiagnosisCode1(s[31]);
				mcmc.setDiagnosisCode2(s[32]);
				mcmc.setDiagnosisCode3(s[33]);
				mcmc.setDiagnosisCode4(s[34]);
				mcmc.setDateOfBirth(s[35]);
				mcmc.setInjuryDate(s[36]);
				mcmc.setReviewState(s[37]);
				mcmc.setReviewZip(s[38]);
				mcmc.setLineID(intProcessing(s[39]));
				mcmc.setFSType(s[40]);
				mcmc.setDateOfService(s[41]);
				mcmc.setCPTCode(s[42]);
				mcmc.setModifier1(s[43]);
				mcmc.setModifier2(s[44]);
				mcmc.setUnits(doubleProcessing(s[45]));
				mcmc.setProviderCharge(doubleProcessing(s[46]));
				mcmc.setStateAllowance(doubleProcessing(s[47]));
				mcmc.setMCMCAllowance(doubleProcessing(s[48]));
				mcmc.setNetworkAllowance(doubleProcessing(s[49]));
				mcmc.setNetworkSavings(doubleProcessing(s[50]));
				mcmc.setReasonCode1(s[51]);
				// mcmc.setReasonCode2(s[52]);
				// mcmc.setReasonCode3(s[53]);
				// mcmc.setReasonCode4(s[54]);
				// mcmc.setRejectDescription(s[55]);

				mcmc.commitData();
				isCommit = true;
			} catch (Exception eFull) {
				System.out.println("\tError loadToDB(" + file + "): " + eFull.toString());
			}
		}

		br.close();
		return isCommit;
	}
	
	private void runCheck() throws SQLException {
		java.text.SimpleDateFormat displayDateSDF1Full = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1Full);
		System.out.println("Running MCMC check " + displayDateSDF1Full.format(PLCUtils.getNowDate(true)));

		int listSize = 0;
		JSch jsch = new JSch();
		Session session = null;
		try {
			session = jsch.getSession(user, host, 22);
			session.setConfig("StrictHostKeyChecking", "no");
			session.setPassword(password);
			session.connect();

			Channel channel = session.openChannel("sftp");
			channel.connect();
			ChannelSftp sftpChannel = (ChannelSftp) channel;
			sftpChannel.cd(remoteDir);
			Vector<ChannelSftp.LsEntry> list = sftpChannel.ls("*.txt");
			listSize = list.size();
			String emailBody = "New files have been added to the Database\n";

			if (list.size() > 0) {
				for (ChannelSftp.LsEntry entry : list) {
					System.out.println("Getting: " + entry.getFilename());
					emailBody += entry.getFilename() + "\n";
					sftpChannel.get("*.txt", localDir);
					System.out.println("Deleting: " + entry.getFilename());
					sftpChannel.rm(entry.getFilename());
				}

				System.out.println("Sending Email Alerts");

				String emailFrom = "support@nextimagemedical.com";
				String emailSubject = "MCMC sFTP dump";

				email("giovanni.hernandez@nextimagemedical.com", emailFrom, emailSubject, emailBody);
				email("vasanthi.murulidhar@nextimagemedical.com", emailFrom, emailSubject, emailBody);
				// email("bob@nextimagemedical.com",emailFrom,emailSubject,emailBody);

			}

			sftpChannel.exit();
			session.disconnect();

		} catch (JSchException e) {
			e.printStackTrace(); // To change body of catch statement use File |
									// Settings | File Templates.
		} catch (SftpException e) {
			e.printStackTrace();
		}
		System.out.println("Done Checking MCMC sFTP");
		// System.exit(listSize);
	}

	public class MCMCPrice {
		private String state;
		private double mrwo;
		private double mrw;
		private double mrwwo;
		private double ctwo;
		private double ctw;
		private double ctwwo;

		public MCMCPrice(String state, double mrwo, double mrw, double mrwwo, double ctwo, double ctw, double ctwwo) {
			this.state = state;
			this.mrwo = mrwo;
			this.mrw = mrw;
			this.mrwwo = mrwwo;
			this.ctwo = ctwo;
			this.ctw = ctw;
			this.ctwwo = ctwwo;
		}

		public String getState() {
			return state;
		}

		public void setState(String state) {
			this.state = state;
		}

		public double getMrwo() {
			return mrwo;
		}

		public void setMrwo(double mrwo) {
			this.mrwo = mrwo;
		}

		public double getMrw() {
			return mrw;
		}

		public void setMrw(double mrw) {
			this.mrw = mrw;
		}

		public double getMrwwo() {
			return mrwwo;
		}

		public void setMrwwo(double mrwwo) {
			this.mrwwo = mrwwo;
		}

		public double getCtwo() {
			return ctwo;
		}

		public void setCtwo(double ctwo) {
			this.ctwo = ctwo;
		}

		public double getCtw() {
			return ctw;
		}

		public void setCtw(double ctw) {
			this.ctw = ctw;
		}

		public double getCtwwo() {
			return ctwwo;
		}

		public void setCtwwo(double ctwwo) {
			this.ctwwo = ctwwo;
		}
	}
}
