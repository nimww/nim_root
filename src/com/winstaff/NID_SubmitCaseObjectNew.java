package com.winstaff;

import java.sql.ResultSet;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: Scott
 * Date: 5/31/13
 * Time: 7:51 PM
 * To change this template use File | Settings | File Templates.
 */
public class NID_SubmitCaseObjectNew {

    public static Integer NID_ORDERS_USER = 52;
    public static Integer NID_PAYER = 123;
    public static Integer NID_REF_SOURCE = 1283;
    public static Integer NID_ASSIGNED = 35141; 

    public static String TAG = "NID_SubmitCaseObject";

    private Integer payerID;
    private String patientFirstName;
    private String patientLastName;
    private String patientAddress1;
    private String patientCity;
    private String patientZIP;
    private Integer patientStateID;
    private String patientHomePhone;
    private String patientEmail;
    private String patientHeight;
    private String patientGender;
    private Date patientDOB;

    private String WebLinkCode;


    private String patientAvailability1;
    private String patientAvailability2;
    private String patientAvailability3;

    private Integer selectedPracticeID;
    private Integer wizardID;

    private Integer patientWeight;
    private Integer isClaustrophobic;
    private Integer isPregnant;
    private Integer isMetal;
    private Integer isHasConditions;

    private String patientHasMetalInBodyDesc;
    private String patientHasKidneyLiverHypertensionDiabeticConditionsDesc;

    private String diagnosis;
    private String bodyPartDisplay;

    private String referringDoctorFirstName;
    private String referringDoctorLastName;
    private String referringDoctorPhone;
    private String referringDoctorFax;

    private String referralSource;
    private String referralCode;

    private String remoteAddr;
    private String remoteHost;

    private Integer submittedEncounterID = null;
    private Integer submittedCaseID = null;
    private Integer submittedReferralID = null;

    public String getWebLinkCode() {
        if (WebLinkCode==null){
            DebugLogger.e(TAG,"WebLinkCode is null");
            return "";
        }
        return WebLinkCode;
    }

    public void setWebLinkCode(String webLinkCode) {
        WebLinkCode = webLinkCode;
    }

    public Integer getSubmittedEncounterID() {
        return submittedEncounterID;
    }

    public void setSubmittedEncounterID(Integer submittedEncounterID) {
        this.submittedEncounterID = submittedEncounterID;
    }

    public Integer getSubmittedCaseID() {
        return submittedCaseID;
    }

    public void setSubmittedCaseID(Integer submittedCaseID) {
        this.submittedCaseID = submittedCaseID;
    }

    public Integer getSubmittedReferralID() {
        return submittedReferralID;
    }

    public void setSubmittedReferralID(Integer submittedReferralID) {
        this.submittedReferralID = submittedReferralID;
    }

    public NID_SubmitCaseObjectNew() {
    }

    public String getRemoteAddr() {
        return remoteAddr;
    }

    public void setRemoteAddr(String remoteAddr) {
        this.remoteAddr = remoteAddr;
    }

    public String getRemoteHost() {
        return remoteHost;
    }

    public void setRemoteHost(String remoteHost) {
        this.remoteHost = remoteHost;
    }

    public Integer getPayerID() {
        if (payerID==null){
            DebugLogger.e(TAG,"payerid is null");
            return 0;
        }
        return payerID;
    }

    public void setPayerID(Integer payerID) {
        this.payerID = payerID;
    }

    public String getPatientFirstName() {
        if (patientFirstName==null){
            DebugLogger.e(TAG,"patientFirstName is null");
            return "";
        }
        return patientFirstName;
    }

    public void setPatientFirstName(String patientFirstName) {
        this.patientFirstName = patientFirstName;
    }

    public String getPatientLastName() {
        if (patientLastName==null){
            DebugLogger.e(TAG,"patientLastName is null");
            return "";
        }
        return patientLastName;
    }

    public void setPatientLastName(String patientLastName) {
        this.patientLastName = patientLastName;
    }

    public String getPatientAddress1() {
        if (patientAddress1==null){
            DebugLogger.e(TAG,"patientAddress1 is null");
            return "";
        }
        return patientAddress1;
    }

    public void setPatientAddress1(String patientAddress1) {
        this.patientAddress1 = patientAddress1;
    }

    public String getPatientCity() {
        if (patientCity==null){
            DebugLogger.e(TAG,"patientCity is null");
            return "";
        }
        return patientCity;
    }

    public void setPatientCity(String patientCity) {
        this.patientCity = patientCity;
    }

    public String getPatientZIP() {
        if (patientZIP==null){
            DebugLogger.e(TAG,"patientZIP is null");
            return "";
        }
        return patientZIP;
    }

    public void setPatientZIP(String patientZIP) {
        this.patientZIP = patientZIP;
    }

    public Integer getPatientStateID() {
        if (patientStateID==null){
            DebugLogger.e(TAG,"patientStateID is null");
            return 0;
        }
        return patientStateID;
    }

    public void setPatientStateID(Integer patientStateID) {
        this.patientStateID = patientStateID;
    }

    public String getPatientHomePhone() {
        if (patientHomePhone==null){
            DebugLogger.e(TAG,"patientHomePhone is null");
            return "";
        }
        return patientHomePhone;
    }

    public void setPatientHomePhone(String patientHomePhone) {
        this.patientHomePhone = patientHomePhone;
    }

    public String getPatientEmail() {
        if (patientEmail==null){
            DebugLogger.e(TAG,"patientEmail is null");
            return "";
        }
        return patientEmail;
    }

    public void setPatientEmail(String patientEmail) {
        this.patientEmail = patientEmail;
    }

    public String getPatientHeight() {
        if (patientHeight==null){
            DebugLogger.e(TAG,"patientHeight is null");
            return "";
        }
        return patientHeight;
    }

    public void setPatientHeight(String patientHeight) {
        this.patientHeight = patientHeight;
    }

    public String getPatientGender() {
        if (patientGender==null){
            DebugLogger.e(TAG,"patientGender is null");
            return "";
        }
        return patientGender;
    }

    public void setPatientGender(String patientGender) {
        this.patientGender = patientGender;
    }

    public Date getPatientDOB() {
        if (patientDOB==null){
            DebugLogger.e(TAG,"patientDOB is null");
            return PLCUtils.getSubDate("");
        }
        return patientDOB;
    }

    public void setPatientDOB(Date patientDOB) {
        this.patientDOB = patientDOB;
    }

    public Integer getSelectedPracticeID() {
        if (selectedPracticeID==null){
            DebugLogger.e(TAG,"selectedPracticeID is null");
            return 0;
        }
        return selectedPracticeID;
    }

    public void setSelectedPracticeID(Integer selectedPracticeID) {
        this.selectedPracticeID = selectedPracticeID;
    }

    public Integer getWizardID() {
        if (wizardID==null){
            DebugLogger.e(TAG,"wizardID is null");
            return 0;
        }
        return wizardID;
    }

    public void setWizardID(Integer wizardID) {
        this.wizardID = wizardID;
    }

    public Integer getPatientWeight() {
        if (patientWeight==null){
            DebugLogger.e(TAG,"patientWeight is null");
            return 0;
        }
        return patientWeight;
    }

    public void setPatientWeight(Integer patientWeight) {
        this.patientWeight = patientWeight;
    }

    public Integer getClaustrophobic() {
        if (isClaustrophobic==null){
            DebugLogger.e(TAG,"isClaustrophobic is null");
            return 0;
        }
        return isClaustrophobic;
    }

    public void setClaustrophobic(Integer claustrophobic) {
        isClaustrophobic = claustrophobic;
    }

    public Integer getPregnant() {
        if (isPregnant==null){
            DebugLogger.e(TAG,"isPregnant is null");
            return 0;
        }
        return isPregnant;
    }

    public void setPregnant(Integer pregnant) {
        isPregnant = pregnant;
    }

    public Integer getMetal() {
        if (isMetal==null){
            DebugLogger.e(TAG,"isMetal is null");
            return 0;
        }
        return isMetal;
    }

    public void setMetal(Integer metal) {
        isMetal = metal;
    }

    public Integer getHasConditions() {
        if (isHasConditions==null){
            DebugLogger.e(TAG,"isHasConditions is null");
            return 0;
        }
        return isHasConditions;
    }

    public void setHasConditions(Integer hasConditions) {
        isHasConditions = hasConditions;
    }

    public String getPatientHasMetalInBodyDesc() {
        if (patientHasMetalInBodyDesc==null){
            DebugLogger.e(TAG,"patientHasMetalInBodyDesc is null");
            return "";
        }
        return patientHasMetalInBodyDesc;
    }

    public void setPatientHasMetalInBodyDesc(String patientHasMetalInBodyDesc) {
        this.patientHasMetalInBodyDesc = patientHasMetalInBodyDesc;
    }

    public String getPatientHasKidneyLiverHypertensionDiabeticConditionsDesc() {
        if (patientHasKidneyLiverHypertensionDiabeticConditionsDesc==null){
            DebugLogger.e(TAG,"patientHasKidneyLiverHypertensionDiabeticConditionsDesc is null");
            return "";
        }
        return patientHasKidneyLiverHypertensionDiabeticConditionsDesc;
    }

    public void setPatientHasKidneyLiverHypertensionDiabeticConditionsDesc(String patientHasKidneyLiverHypertensionDiabeticConditionsDesc) {
        this.patientHasKidneyLiverHypertensionDiabeticConditionsDesc = patientHasKidneyLiverHypertensionDiabeticConditionsDesc;
    }

    public String getDiagnosis() {
        if (diagnosis==null){
            DebugLogger.e(TAG,"diagnosis is null");
            return "";
        }
        return diagnosis;
    }

    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
    }

    public String getBodyPartDisplay() {
        if (bodyPartDisplay==null){
            DebugLogger.e(TAG,"bodyPartDisplay is null");
            return "";
        }
        return bodyPartDisplay;
    }

    public void setBodyPartDisplay(String bodyPartDisplay) {
        this.bodyPartDisplay = bodyPartDisplay;
    }

    public String getReferringDoctorFirstName() {
        if (referringDoctorFirstName==null){
            DebugLogger.e(TAG,"referringDoctorFirstName is null");
            return "";
        }
        return referringDoctorFirstName;
    }

    public void setReferringDoctorFirstName(String referringDoctorFirstName) {
        this.referringDoctorFirstName = referringDoctorFirstName;
    }

    public String getReferringDoctorLastName() {
        if (referringDoctorLastName==null){
            DebugLogger.e(TAG,"referringDoctorLastName is null");
            return "";
        }
        return referringDoctorLastName;
    }

    public void setReferringDoctorLastName(String referringDoctorLastName) {
        this.referringDoctorLastName = referringDoctorLastName;
    }

    public String getReferringDoctorPhone() {
        if (referringDoctorPhone==null){
            DebugLogger.e(TAG,"referringDoctorPhone is null");
            return "";
        }
        return referringDoctorPhone;
    }

    public void setReferringDoctorPhone(String referringDoctorPhone) {
        this.referringDoctorPhone = referringDoctorPhone;
    }

    public String getReferringDoctorFax() {
        if (referringDoctorFax==null){
            DebugLogger.e(TAG,"referringDoctorFax is null");
            return "";
        }
        return referringDoctorFax;
    }

    public void setReferringDoctorFax(String referringDoctorFax) {
        this.referringDoctorFax = referringDoctorFax;
    }

    public String getPatientAvailability1() {
        if (patientAvailability1==null){
            DebugLogger.e(TAG,"patientAvailability1 is null");
            return "";
        }
        return patientAvailability1;
    }

    public void setPatientAvailability1(String patientAvailability1) {
        this.patientAvailability1 = patientAvailability1;
    }

    public String getPatientAvailability2() {
        if (patientAvailability2==null){
            DebugLogger.e(TAG,"patientAvailability2 is null");
            return "";
        }
        return patientAvailability2;
    }

    public void setPatientAvailability2(String patientAvailability2) {
        this.patientAvailability2 = patientAvailability2;
    }

    public String getPatientAvailability3() {
        if (patientAvailability3==null){
            DebugLogger.e(TAG,"patientAvailability3 is null");
            return "";
        }
        return patientAvailability3;
    }

    public void setPatientAvailability3(String patientAvailability3) {
        this.patientAvailability3 = patientAvailability3;
    }

    public String getReferralSource() {
        if (referralSource==null){
            DebugLogger.e(TAG,"referralSource is null");
            return "";
        }
        return referralSource;
    }

    public void setReferralSource(String referralSource) {
        this.referralSource = referralSource;
    }

    public String getReferralCode() {
        if (referralCode==null){
            DebugLogger.e(TAG,"referralCode is null");
            return "";
        }
        return referralCode;
    }

    public void setReferralCode(String referralCode) {
        this.referralCode = referralCode;
    }

    public Boolean submitCase(){
        /*
        Create a case based on the objects data
         */
        try {
            bltNIM3_CaseAccount NIM3_CaseAccount = new bltNIM3_CaseAccount();
            bltNIM3_Referral NIM3_Referral = new bltNIM3_Referral();
            bltNIM3_Encounter NIM3_Encounter = new bltNIM3_Encounter();
            bltNIM3_Appointment NIM3_Appointment = new bltNIM3_Appointment();
            bltNIM3_CommTrack NIM3_CommTrack = new bltNIM3_CommTrack();
            bltCPTWizard wiz = new bltCPTWizard(this.getWizardID());
            NIM3_ReferralSourceObject myRSO = translateWebCodesToReferralSource(this.getReferralSource(),this.getReferralCode(), this.getWebLinkCode());
            Integer caseReferID = myRSO.getReferralSource();
            NIM3_CaseAccount.setPayerID(myRSO.getPayerID());

            String ip = this.getRemoteAddr();
            String host = this.getRemoteHost();
            String reqEmail = this.getPatientEmail();

            PracticeMaster_SearchResultsObject pm_sro = new PracticeMaster_SearchResultsObject(this.getSelectedPracticeID());
            String commtrackMessageText = "";


            bltNIM3_PayerMaster payerMaster = new bltNIM3_PayerMaster(myRSO.getPayerID());
            bltNIM3_NIDPromo nidPromo = null;
            if (myRSO.getNidPromo()!=null){
                nidPromo = new bltNIM3_NIDPromo(myRSO.getNidPromo());
            }


            Double theCost = NIMUtils.getCPTGroupPayPriceforPracticeMaster(pm_sro.practiceMaster,NIMUtils.CPTWizardToCPTGroupObject(wiz), PLCUtils.getToday(), NIMUtils.PAYMENT_TYPE_GROUPHEALTH);
            pm_sro.setPrice(theCost);
            pm_sro.setCustomerPrice(NIDUtils.getNIDFinalPrice(theCost, pm_sro.getPracticeMaster(), payerMaster, nidPromo ));

            try{
                NIM3_CaseAccount.setPatientFirstName(this.getPatientFirstName());
                NIM3_CaseAccount.setPatientLastName(this.getPatientLastName());
                NIM3_CaseAccount.setPatientAddress1(this.getPatientAddress1());
                NIM3_CaseAccount.setPatientCity(this.getPatientCity());
                NIM3_CaseAccount.setPatientZIP(this.getPatientZIP());
                NIM3_CaseAccount.setPatientHomePhone(this.getPatientHomePhone());
                NIM3_CaseAccount.setPatientEmail(this.getPatientEmail());
                NIM3_CaseAccount.setPatientHeight(this.getPatientHeight());
                NIM3_CaseAccount.setPatientWeight(this.getPatientWeight());
                NIM3_CaseAccount.setPatientGender(this.getPatientGender());
                NIM3_CaseAccount.setPatientDOB(this.getPatientDOB());
                NIM3_CaseAccount.setPatientStateID(this.getPatientStateID());
                NIM3_CaseAccount.setPatientIsClaus(this.getClaustrophobic());
                NIM3_CaseAccount.setPatientIsPregnant(this.getPregnant());

                NIM3_CaseAccount.setPatientHasMetalInBody(this.getMetal());
                //NIM3_CaseAccount.setPatientHasMetalInBodyDesc(this.patientHasMetalInBodyDesc);

                NIM3_CaseAccount.setPatientHasKidneyLiverHypertensionDiabeticConditions(this.getHasConditions());
                //NIM3_CaseAccount.setPatientHasKidneyLiverHypertensionDiabeticConditionsDesc(this.getPatientHasKidneyLiverHypertensionDiabeticConditionsDesc());

                NIM3_CaseAccount.setCaseStatusID(new Integer(1));
                NIM3_CaseAccount.setUniqueCreateDate(PLCUtils.getNowDate(false));
                NIM3_CaseAccount.setUniqueModifyDate(PLCUtils.getNowDate(false));
                NIM3_CaseAccount.setUniqueModifyComments("Created by NID Online Form V2");
                NIM3_CaseAccount.setPatientSSN("Not Given");
                NIM3_CaseAccount.setCaseClaimNumber("NID-"+this.getPatientLastName().toUpperCase()+"-"+Math.round(Math.random()*100)+com.winstaff.password.RandomString.generateString(2).toUpperCase()+Math.round(Math.random()*100)+com.winstaff.password.RandomString.generateString(2).toUpperCase());
                NIM3_CaseAccount.setAdjusterID(caseReferID);

                NIM3_CaseAccount.commitData();
                NIM3_CaseAccount.setCaseCode("CP" +  NIM3_CaseAccount.getUniqueID() + com.winstaff.password.RandomString.generateString(2).toUpperCase() + Math.round(Math.random()*100)+com.winstaff.password.RandomString.generateString(2).toUpperCase());
                NIM3_CaseAccount.setAssignedToID(NID_ASSIGNED);

                NIM3_CaseAccount.commitData();
            } catch (Exception eCase){
                DebugLogger.e("submitcase:NIM3_CaseAccount", ""+eCase);
            }
            DebugLogger.e("po:caseid:",""+NIM3_CaseAccount.getUniqueID());
            this.setSubmittedCaseID(NIM3_CaseAccount.getUniqueID());

            //autosets Referral

            NIM3_Referral.setCaseID(NIM3_CaseAccount.getUniqueID());
            NIM3_Referral.setUniqueCreateDate(PLCUtils.getNowDate(false));
            NIM3_Referral.setUniqueModifyDate(PLCUtils.getNowDate(false));
            NIM3_Referral.setUniqueModifyComments("Created by Online Form [WLC="+this.getWebLinkCode()+"]");

            NIM3_Referral.setReferralWebLink(this.getWebLinkCode());
            //NIM3_Referral.setComments(parameterPassString);
            NIM3_Referral.setReferralTypeID(3);
            NIM3_Referral.setReferralDate(PLCUtils.getNowDate(true));
            NIM3_Referral.setReceiveDate(PLCUtils.getNowDate(true));

            NIM3_Referral.setReferralStatusID(1);

            NIM3_Referral.setAttendingPhysicianID(NID_ORDERS_USER);
            NIM3_Referral.setReferringPhysicianID(NID_ORDERS_USER);
            NIM3_Referral.setReferralMethod("Online (Pub)");

            NIM3_Referral.setReferredByContactID(caseReferID);

            if (myRSO.getNidPromo()!=null){
                NIM3_Referral.setReferralNIDPromoID(myRSO.getNidPromo());
            }

            NIM3_Referral.setRxFileID(NIMUtils.EncounterBypassRXID);
            NIM3_Referral.setOrderFileID(NIMUtils.EncounterBypassRXID);

            NIM3_Referral.setReferenceFax(this.getReferringDoctorFax());
            NIM3_Referral.setReferencePhone(this.getReferringDoctorPhone());
            NIM3_Referral.setComments(
                    this.getReferringDoctorFirstName() + " " + this.getReferringDoctorLastName()
                    + "\nFax: " + this.getReferringDoctorFax()
                    + "\nPhone: " + this.getReferringDoctorPhone()
            );
            DebugLogger.e("po:rid:",""+NIM3_Referral.getUniqueID());
            NIM3_Referral.commitData();

            this.setSubmittedReferralID(NIM3_Referral.getUniqueID());

            //autosets Encounter
            NIM3_Encounter.setReferralID(NIM3_Referral.getUniqueID());
            NIM3_Encounter.setUniqueCreateDate(PLCUtils.getNowDate(false));
            NIM3_Encounter.setUniqueModifyDate(PLCUtils.getNowDate(false));
            NIM3_Encounter.setAttendingPhysicianID(NID_ORDERS_USER);
            NIM3_Encounter.setUniqueModifyComments("Created by Online Form");
            NIM3_Encounter.setEncounterStatusID(1);
            //NIM3_Encounter.setPatientAvailability("[1] " + this.getPatientAvailability1() + "<br>[2] " + this.getPatientAvailability2() + "<br>[3] " + this.getPatientAvailability3());
            NIM3_Encounter.setRequiresHandCarryCD(1);
            NIM3_Encounter.setNIDReferralSource(this.getReferralSource());
            NIM3_Encounter.setNIDReferralCode(this.getReferralCode());

            //fill in report
            NIM3_Encounter.setReportFileID(NIMUtils.EncounterBypassRXID);
            NIM3_Encounter.setSentToAdj(PLCUtils.getSubDate("na"));
            NIM3_Encounter.setSentToRefDr(PLCUtils.getSubDate("na"));
            NIM3_Encounter.setTimeTrack_ReqRpReview(PLCUtils.getSubDate("na"));
            NIM3_Encounter.setTimeTrack_ReqRpReview_UserID(NID_ORDERS_USER);


            NIM3_Encounter.setTimeTrack_ReqApproved(PLCUtils.getSubDate("na"));
            NIM3_Encounter.setTimeTrack_ReqApproved_UserID(NID_ORDERS_USER);

            NIM3_Encounter.setTimeTrack_ReqCreated(PLCUtils.getSubDate("na"));
            NIM3_Encounter.setTimeTrack_ReqCreated_UserID(NID_ORDERS_USER);

            NIM3_Encounter.setTimeTrack_ReqDelivered(PLCUtils.getSubDate("na"));
            NIM3_Encounter.setTimeTrack_ReqDelivered_UserID(NID_ORDERS_USER);

            NIM3_Encounter.setTimeTrack_ReqInitialAppointment(PLCUtils.getSubDate("na"));
            NIM3_Encounter.setTimeTrack_ReqInitialAppointment_UserID(NID_ORDERS_USER);

            NIM3_Encounter.setTimeTrack_ReqProc(PLCUtils.getSubDate("na"));
            NIM3_Encounter.setTimeTrack_ReqProc_UserID(NID_ORDERS_USER);

            NIM3_Encounter.setTimeTrack_ReqRec(PLCUtils.getSubDate("na"));
            NIM3_Encounter.setTimeTrack_ReqRec_UserID(NID_ORDERS_USER);

            NIM3_Encounter.setTimeTrack_ReqRxReview(PLCUtils.getSubDate("na"));
            NIM3_Encounter.setTimeTrack_ReqRxReview_UserID(NID_ORDERS_USER);

            NIM3_Encounter.setTimeTrack_ReqSched(PLCUtils.getSubDate("na"));
            NIM3_Encounter.setTimeTrack_ReqSched_UserID(NID_ORDERS_USER);

            NIM3_Encounter.setSentTo_ReqRec_Adj(PLCUtils.getSubDate("na"));
            NIM3_Encounter.setSentTo_DataProc_Adj(PLCUtils.getSubDate("na"));
            NIM3_Encounter.setSentTo_DataProc_RefDr(PLCUtils.getSubDate("na"));
            NIM3_Encounter.setSentTo_ReqRec_RefDr(PLCUtils.getSubDate("na"));
            NIM3_Encounter.setSentTo_SP_RefDr(PLCUtils.getSubDate("na"));
            NIM3_Encounter.setSentTo_SP_Adj(PLCUtils.getSubDate("na"));

            //first let's convert the wizard into CPT Group Object 2
            CPTGroupObject2 myCPTGO2 = NIMUtils.CPTWizardToCPTGroupObject(wiz);


//            value=4>Diag: MRI</option>
//            value=5>Diag: CT</option>
//            value=6>Diag: EMG</option>
//            value=7>Diag: NCV</option>
//            value=8>Diag: PET</option>
//            value=9>Diag: Other</option>


            if (myCPTGO2.isHasMRI()){
                NIM3_Encounter.setEncounterTypeID(4);
            } else if (myCPTGO2.isHasCT()){
                NIM3_Encounter.setEncounterTypeID(5);
            } else if (myCPTGO2.isHasPET()){
                NIM3_Encounter.setEncounterTypeID(8);
            } else {
                NIM3_Encounter.setEncounterTypeID(9);
            }



            NIM3_Encounter.setSeeNetDev_SelectedPracticeID( pm_sro.getPracticeMaster().getUniqueID());
            NIM3_Encounter.commitData();
            NIM3_Encounter.setScanPass("SP" +  NIM3_Encounter.getUniqueID() + com.winstaff.password.RandomString.generateString(2).toUpperCase() + Math.round(Math.random()*100)+com.winstaff.password.RandomString.generateString(2).toUpperCase());
            NIM3_Encounter.commitData();

            DebugLogger.e("po:eid:",""+NIM3_Encounter.getUniqueID());
            this.setSubmittedEncounterID(NIM3_Encounter.getUniqueID());

            //now let's create the appt
            NIM3_Appointment.setProviderID(pm_sro.getPracticeMaster().getUniqueID());
            NIM3_Appointment.setiStatus(1);
            NIM3_Appointment.setAppointmentTime(PLCUtils.getNowDate(false));
            NIM3_Appointment.setAppointmentConfirmation("NID_ON_"+com.winstaff.password.RandomString.generateString(2).toUpperCase() + Math.round(Math.random()*100)+com.winstaff.password.RandomString.generateString(2).toUpperCase());
            NIM3_Appointment.setInitialEncounterID(NIM3_Encounter.getUniqueID());
            NIM3_Appointment.commitData();

            //Now let's save the appointment into the Encounter
            NIM3_Encounter.setAppointmentID(NIM3_Appointment.getUniqueID());
            //set date of service to tomorrow
            NIM3_Encounter.setDateOfService(PLCUtils.getSubDate("t+1"));
            NIM3_Encounter.setNextActionDate(PLCUtils.getSubDate("tam"));
            NIM3_Encounter.setNextActionNotes("Ensure that the patient has paid!");
            NIM3_Encounter.commitData();

            //autoset Service
            //create all services:
            for (CPTObject CPTO: myCPTGO2.getCPTList()){
                bltNIM3_Service NIM3_Service = new bltNIM3_Service();
                NIM3_Service.setEncounterID(NIM3_Encounter.getUniqueID());
                //we have to recalc the billing to get the service level item price
                Double tempPrice = NIMUtils.getCPTPayPriceforPracticeMaster(pm_sro.getPracticeMaster(), CPTO, PLCUtils.getToday(), NIMUtils.PAYMENT_TYPE_GROUPHEALTH);
                NIM3_Service.setPaidOutAmount(tempPrice);
                NIM3_Service.setBillAmount(NIDUtils.getNIDFinalPrice(tempPrice, pm_sro.getPracticeMaster(), payerMaster, nidPromo ));
                NIM3_Service.setAllowAmount(NIDUtils.getNIDFinalPrice(tempPrice, pm_sro.getPracticeMaster(), payerMaster, nidPromo ));
                NIM3_Service.setCPT(CPTO.getCPT_Code());
                NIM3_Service.setCPTQty(CPTO.getCPT_Qty());
                NIM3_Service.setCPTModifier(CPTO.getCPT_Modifier());
                NIM3_Service.setCPTWizardID(wiz.getUniqueID());
                NIM3_Service.setServiceStatusID(1);
                NIM3_Service.setCPTBodyPart(CPTO.getCPT_Description());
                //NIM3_Service.setCPTText("[" + this.getDiagnosis() + "]");
                NIM3_Service.setServiceTypeID(3);
                NIM3_Service.commitData();
            }

            //autosets Commtrack
            NIM3_CommTrack.setUniqueCreateDate(PLCUtils.getNowDate(false));
            NIM3_CommTrack.setMessageName(NIM3_CaseAccount.getPatientFirstName() + " " + NIM3_CaseAccount.getPatientLastName());
            NIM3_CommTrack.setMessageCompany ("NID Patient [Search Reservation]");
            NIM3_CommTrack.setMessagePhone(NIM3_CaseAccount.getPatientCellPhone());
            NIM3_CommTrack.setUniqueModifyDate(PLCUtils.getNowDate(false));
            NIM3_CommTrack.setUniqueModifyComments("Created by NID Search");
            NIM3_CommTrack.setCommEnd(PLCUtils.getNowDate(false));
            NIM3_CommTrack.setCommStart(PLCUtils.getNowDate(false));
            NIM3_CommTrack.setCaseID(NIM3_CaseAccount.getUniqueID());
            NIM3_CommTrack.setEncounterID(NIM3_Encounter.getUniqueID());
            NIM3_CommTrack.setReferralID(NIM3_Referral.getUniqueID());
            NIM3_CommTrack.setExtUserID(1);
            NIM3_CommTrack.setCommTypeID(2);//1=message 2=sched     0 = bogus


            commtrackMessageText += pm_sro.getPracticeMaster().getPracticeName()+"<br>";
            commtrackMessageText += "$" + Math.round(pm_sro.getCustomerPrice()) + "<br>";
            commtrackMessageText += "Payment not collected<br>";
            //commtrackMessageText += "Procedure code: "+myProc+"<br>";
            commtrackMessageText += "Body Part: "+this.getBodyPartDisplay()+ "<br>";
            //commtrackMessageText += "Diagnosis: "+this.getDiagnosis()+ "<br><br>";
            commtrackMessageText += "Referring Physician's Information:<br>"+
                    "<strong>Name:</strong> "+this.getReferringDoctorFirstName()+" "+this.getReferringDoctorLastName()+"<br>"+
                    "<strong>Phone:</strong> "+this.getReferringDoctorPhone() +"<br>"+
                    "<strong>Fax:</strong> "+this.getReferringDoctorFax();


            NIM3_CommTrack.setMessageText(commtrackMessageText);
            NIM3_CommTrack.setComments("[SEARCH] Schedule Submit [CL:<strong>" + NIM3_CaseAccount.getCaseClaimNumber() + "</strong>]\n[IP: " + ip + "]\n[Host: " + host + "]");
            NIM3_CommTrack.commitData();


            //Send Notification to Orders
            emailType_V3 myETSend = new emailType_V3();
            String theBody = "NextImageGrid CommTrack Notification:\n\n";
            theBody += "From: " + NIM3_CommTrack.getMessageName() + " @ " +NIM3_CommTrack.getMessageCompany() ;
            theBody += "\nSubmitted by: NID Search Reservation ";
            theBody += "\nTrackingID: " + NIM3_CommTrack.getUniqueID();
            theBody += "\nRe/Patient: " + NIM3_CaseAccount.getPatientLastName();
            theBody += "\nRe/Claim: " + NIM3_CaseAccount.getCaseClaimNumber();
            theBody += "\nScanPass: " + NIM3_Encounter.getScanPass();
            theBody += "\nDate: " + PLCUtils.getNowDate(false);

            myETSend.setBody(theBody);
            myETSend.setBodyType(myETSend.PLAIN_TYPE);

            myETSend.setFrom("support@nextimagemedical.com");
            myETSend.setSubject("NextImageGrid - CommTrack Submitted by NID Search [Public] ["+NIM3_Encounter.getScanPass()+"] ");
            myETSend.setTo("orders@nextimagemedical.com");
//            myETSend.setTo("scott@nextimagemedical.com");
            myETSend.isSendMail();
            
            
            
   
            
            
//            
//
//            theBody = "Thank you for choosing NextImage Direct for your imaging needs.\n\n";
//            theBody += "\nScanPass: " + NIM3_Encounter.getScanPass();
//            theBody += "\nDate: " + PLCUtils.getNowDate(false);
//            theBody += "\n\nWe are in the process of notifying the imaging center of your reservation. You should receive a call from them within 2 business day to setup your specific appointment. To expedite your order, please fax your prescription (or have your doctor's office fax it) to: " + pm_sro.getPracticeMaster().getOfficeFaxNo() + "" +
//                    "\n\nIf you have any questions, please feel free to call us directly at: 888-693-8867" +
//                    "\n\nThank you for choosing NextImage for your radiology needs!";
//            
//            
//            
//            myETSend.setBody(theBody);
//            myETSend.setBodyType(myETSend.PLAIN_TYPE);
//
//            myETSend.setFrom("support@nextimagemedical.com");
//            myETSend.setSubject("Thank you for your Diagnostic Imaging Reservation!  ["+NIM3_Encounter.getScanPass()+"] ");
//            myETSend.setTo("order@nextimagemedical.com");
//            myETSend.setTo(reqEmail);
//            myETSend.setTo("gio78allday@gmail.com");
//            if (NIMUtils.isValidEmail(reqEmail)){
//                myETSend.isSendMail();
//            }
//
//            theBody = "NextImage Medical has referred a patient to your facility for the procedure requested below. Please call patient to schedule the appointment.\n" +
//                    "\n" +
//                    "ScanPass#:\t"+NIM3_Encounter.getScanPass()+"\n" +
//                    "Patient:\t"+NIM3_CaseAccount.getPatientLastName()+", "+NIM3_CaseAccount.getPatientFirstName()+"\tGender: "+NIM3_CaseAccount.getPatientGender()+"\n" +
//                    "Address:\t"+NIM3_CaseAccount.getPatientAddress1()+"  / "+NIM3_CaseAccount.getPatientCity()+", "+new bltStateLI(NIM3_CaseAccount.getPatientStateID()).getShortState()+" "+NIM3_CaseAccount.getPatientZIP()+"\n" +
//                    "DOB:\t"+PLCUtils.getDisplayDate(NIM3_CaseAccount.getPatientDOB(), false)+"\t\n" +
//                    "Phone 1:\t"+NIM3_CaseAccount.getPatientHomePhone()+"\n" +
//                    "Height:\t"+NIM3_CaseAccount.getPatientHeight()+"\n" +
//                    "Weight:\t"+NIM3_CaseAccount.getPatientWeight()+"\n" +
//                    "\n" +
//                    "Referring Physician:\t"+this.getReferringDoctorFirstName()+" "+ this.getReferringDoctorLastName()+"\n" +
//                    "Phone: "+this.getReferringDoctorPhone()+"\tFax: "+this.getReferringDoctorFax()+"\n" +
//                    "\n" +
//                    "Authorized services:\n";
//
//            for (CPTObject CPTO: myCPTGO2.getCPTList()){
//                theBody += CPTO.getCPT_Code() + " - " + CPTO.getCPT_Description() + "\n";
//                theBody += "Diag/Rule-Out: " + this.getDiagnosis() + "\n";
//            }
//            theBody += "\nImportant Notes:" +
//                    "\n" +
//                    "\n* DO NOT TO BILL THE PATIENT!!!   Send bill NextImage Medical and we will pay you promptly *" +
//                    "\n*****" +
//                    "\n" +
//                    "\n* Call patient to schedule appointment *" +
//                    "\n*****" +
//                    "\n" +
//                    "\n* Hand Carry CD: Yes *" +
//                    "\n*****" +
//                    "\n" +
//                    "\n* To obtain Rx for this procedure please contact referring doctor at "+this.getReferringDoctorPhone()+"" +
//                    "\n" +
//                    "\nAppointment Time:\tPlease call patient to schedule appointment" +
//                    "\n" +
//                    "\nSEND CLAIMS TO:" +
//                    "\nNextImage Medical" +
//                    "\nAttn: Medical Claims" +
//                    "\n3390 Carmel Mountain Rd. Suite 150" +
//                    "\nSan Diego, CA  92121" +
//                    "\n" +
//                    "\nIf you have any questions, please call us. Thank You," +
//                    "\n" +
//                    "\nElizabeth Ayala" +
//                    "\n888-693-8867" +
//                    "\nFax: 888-371-3302" +
//                    "\n" +
//                    "\n_____________________________________________________________________________________" +
//                    "\n1. To ensure prompt payment, please fax all medical reports to (fax number of scheduler) within 48 hours of services being rendered." +
//                    "\n2. Discrepancies between the scheduled service and physician's script must be clarified before service is rendered.";
//
//            myETSend.setBody(theBody);
//            myETSend.setBodyType(myETSend.PLAIN_TYPE);
//
//            myETSend.setFrom("eayala@nextimagemedical.com");
//            myETSend.setSubject("NextImage has referred a patient to you: ["+NIM3_Encounter.getScanPass()+"] ");
//            myETSend.setTo(NIMUtils.fax2EmailFax(pm_sro.getPracticeMaster().getOfficeFaxNo()));
//            //myETSend.setTo("scott@nextimagemedical.com");    
//            if (NIMUtils.isValidEmail(reqEmail)){
//                myETSend.isSendMail();
//            }
//            NIM3_Encounter.commitData();


            
            
            
            
            
            
            
            
            
            
            

        } catch (Exception eee){
            DebugLogger.e("NID_SubmitCaseObject:submitCase", "Overall Failure: ["+eee+"]");
            return false;
        }
        return true;
    }

    public static boolean fakeCaseUpdate(NIM3_EncounterObject2 myEO2){

        bltCCardTransaction ccTrans = new bltCCardTransaction();
        ccTrans.setUniqueCreateDate(PLCUtils.getNowDate(false));
        ccTrans.setUniqueModifyDate(PLCUtils.getNowDate(false));
        ccTrans.setUniqueModifyComments("FAKE Sage Payment via NID");
        ccTrans.setTransactionDate(PLCUtils.getNowDate(false));
        ccTrans.setActionID(1);
        ccTrans.setCName("FAKE");
        ccTrans.setCAddress("FAKE");
        ccTrans.setCCity("FAKE");
        ccTrans.setCState("FK");
        ccTrans.setCZip("FAKE");
        ccTrans.setCCNumber(1234);
        ccTrans.setCEXP("FAKE");
        ccTrans.setOrderNumber("FAKE");
        ccTrans.setAuthNumber("FAKE");
        ccTrans.setAmountCharged("1000.00");
        ccTrans.setEncounterID(myEO2.getNIM3_Encounter().getUniqueID());
        try {
            ccTrans.commitData();
        } catch (Exception ee){
            DebugLogger.e("Fake Test", "Error ["+ee+"]") ;
        }

        PracticeMaster_SearchResultsObject pm_sro = new PracticeMaster_SearchResultsObject(myEO2.getAppointment_PracticeMaster().getUniqueID());

        pm_sro.setPrice(1000.00);
        String res = "123456789012345678901234567890123456789012345678901234567890555";
        return updateCaseWithPayment(myEO2, "", res, pm_sro,ccTrans );

    }

    public static boolean updateCaseWithPayment(NIM3_EncounterObject2 myEO2, String paymentNotes, String res, PracticeMaster_SearchResultsObject pm_sro, bltCCardTransaction ccTrans){

        //called when charge is successful

        try {
            myEO2.getNIM3_Encounter().setSentTo_Bill_Pay(PLCUtils.getNowDate(false));

            //update the first service with payment info
            bltNIM3_Service        working_bltNIM3_Service;
            ListElement         leCurrentElement_Service;
            java.util.Enumeration eList_Service = myEO2.getNIM3_Service_List().elements();

            if (eList_Service.hasMoreElements()) {
                leCurrentElement_Service    = (ListElement) eList_Service.nextElement();
                working_bltNIM3_Service  = (bltNIM3_Service) leCurrentElement_Service.getObject();
                working_bltNIM3_Service.setReceivedCheck1Number(res.substring(57,res.length()-2));

                working_bltNIM3_Service.setReceivedCheck1Amount(new Double(Math.round(pm_sro.getCustomerPrice())));

                working_bltNIM3_Service.setReceivedCheck1Date(PLCUtils.getNowDate(false));
                working_bltNIM3_Service.commitData();
            }
    //        bltNIM3_Service NIM3_Service = new bltNIM3_Service();
    //        NIM3_Service.setEncounterID(new Integer(request.getParameter("eID")));
    //        NIM3_Service.setBillAmount(new Double(Math.round(pm_sro.getCustomerPrice())));
    //        NIM3_Service.setAllowAmount(new Double(Math.round(pm_sro.getCustomerPrice())));
    //        NIM3_Service.setReceivedCheck1Number(res.substring(57,res.length()-2));
    //        NIM3_Service.setReceivedCheck1Amount(new Double(Math.round(pm_sro.getCustomerPrice())));
    //        NIM3_Service.setReceivedCheck1Date(PLCUtils.getNowDate(false));
    //        NIM3_Service.commitData();

            bltNIM3_CommTrack NIM3_CommTrack = new bltNIM3_CommTrack();
            NIM3_CommTrack.setUniqueCreateDate(PLCUtils.getNowDate(false));
            NIM3_CommTrack.setMessageName("Sage Payment via NID");
            NIM3_CommTrack.setMessageCompany ("Sage Payment via NID");
            NIM3_CommTrack.setUniqueModifyDate(PLCUtils.getNowDate(false));
            NIM3_CommTrack.setUniqueModifyComments("Created by Sage Payment via NID");
            NIM3_CommTrack.setCommEnd(PLCUtils.getNowDate(false));
            NIM3_CommTrack.setCommStart(PLCUtils.getNowDate(false));
            NIM3_CommTrack.setAlertStatusCode(1);
            NIM3_CommTrack.setCaseID(myEO2.getNIM3_CaseAccount().getUniqueID());
            NIM3_CommTrack.setEncounterID(myEO2.getNIM3_Encounter().getUniqueID());
            NIM3_CommTrack.setReferralID(myEO2.getNIM3_Referral().getUniqueID());

            NIM3_CommTrack.setExtUserID(new Integer(0));
            NIM3_CommTrack.setCommTypeID(new Integer(2));//1=message 2=sched     0 = bogus

            String commtrackMessageText = "CC: "+res.substring(8,40)+"<br>";

            //commtrackMessageText += "Charge Amount: $"+Math.round(pm_sro.getCustomerPrice());
            NIM3_CommTrack.setMessageText(commtrackMessageText);
            NIM3_CommTrack.commitData();

            myEO2.getNIM3_Encounter().setCCTransactionID(ccTrans.getUniqueID());
            //set to billing in progress
            myEO2.getNIM3_Encounter().setEncounterStatusID(9);
            myEO2.getNIM3_Encounter().commitData();

            //update billing
            NIMUtils.ReCalcServiceBilling(myEO2.getNIM3_Encounter().getUniqueID());
            NIMUtils.ReCalcServiceBilling2(myEO2.getNIM3_Encounter().getUniqueID());
            myEO2.synchronizeBilling_One_to_Two();


            //Send Notification to Patient
            String result_temp = NIMUtils.sendAlertEmail(myEO2, "email_rc_pt", "", false, "Payment Received - Alerts Sent");

            //Send Notification to Ref Dr
            result_temp = NIMUtils.sendAlertEmail(myEO2, "fax_rc_refdr", "", false, "Payment Received - Alerts Sent");

            //Send Notification to Ref Dr
            result_temp = NIMUtils.sendAlertEmail(myEO2, "fax_sp_ic", "", false, "Payment Received - Alerts Sent");

            return true;
        } catch (Exception eeee){
            DebugLogger.e("NID_SubmitCaseObject:updateCaseWithPayment","Global Error: ["+eeee+"]");
            return false;
        }
    }

    public static NIM3_ReferralSourceObject translateWebCodesToReferralSource(String source, String code, String weblink){
        /*
        TODO: note this method should eventually be turned into an automatic look up in the DB

        PPS and NHLA are not in the DB
         */


        searchDB2 mySS = new searchDB2();
        ResultSet myRS = null;
        Boolean goodCode = true;
        Boolean goodSource = true;
        Boolean goodWebLink = true;

        if (code==null || code.equalsIgnoreCase("")){
            code = "-invalid";
            goodCode = false;
        } else {
            code = code.trim();
        }
        if (source==null || source.equalsIgnoreCase("")){
            source = "-invalid";
            goodSource = false;
        } else {
            source = source.trim();
        }
        if (weblink==null || weblink.equalsIgnoreCase("")){
            weblink = "-invalid";
            goodWebLink = false;
        } else {
            weblink = weblink.trim();
        }

        //We test Source and Code first since they will override a weblink
        if (goodSource || goodCode ){
            try {
                String mySQL = "select payerid, nid_referralsource from tnim3_payermaster where upper(nid_webcode) = upper(?)  OR upper(nid_webcode) = upper(?)  ";
                //SQL Prepared Statements Update 2011
                db_PreparedStatementObject myDPSO = new db_PreparedStatementObject(mySQL);
                myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,source));
                myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,code));
                myRS = mySS.executePreparedStatement(myDPSO);
                if(myRS!=null&&myRS.next())
                {
                    Integer temp_rs = myRS.getInt("nid_referralsource");
                    Integer temp_pid = myRS.getInt("payerid");
                    if (temp_rs!=null && temp_rs>0){
                        //found it in the database so let's return it.
                        return new NIM3_ReferralSourceObject(temp_pid, temp_rs);
                    }
                }
                mySS.closeAll();
            } catch (Exception eee3eeee) {
                DebugLogger.e ("NID_SubmitCaseObject:translateReferralSourceToPayerID","DB fail[" + eee3eeee.toString()+"]");
            } finally {
                mySS.closeAll();
            }
        }

        //now test promo code table
        if ( goodSource || goodCode ){
            try {
                String mySQL = "select tnim3_payermaster.payerid, nid_referralsource, NIDPromoID from tnim3_payermaster INNER JOIN tnim3_nidpromo on tnim3_nidpromo.payerid = tnim3_payermaster.payerid where upper(weblink) = upper(?) OR upper(weblink) = upper(?)  ";
                //SQL Prepared Statements Update 2011
                db_PreparedStatementObject myDPSO = new db_PreparedStatementObject(mySQL);
                myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,source));
                myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,code));
                myRS = mySS.executePreparedStatement(myDPSO);
                if(myRS!=null&&myRS.next())
                {
                    Integer temp_rs = myRS.getInt("nid_referralsource");
                    Integer temp_pid = myRS.getInt("payerid");
                    Integer temp_prid = myRS.getInt("NIDPromoID");
                    if (temp_rs!=null && temp_rs>0){
                        //found it in the database so let's return it.
                        return new NIM3_ReferralSourceObject(temp_pid, temp_rs, temp_prid);
                    }
                }
                mySS.closeAll();
            } catch (Exception eee3eeee) {
                DebugLogger.e ("NID_SubmitCaseObject:translateReferralSourceToPayerID","DB fail promo[" + eee3eeee.toString()+"]");
            } finally {
                mySS.closeAll();
            }
        }


        //check weblink only
        if (goodWebLink  ){
            try {
                String mySQL = "select payerid, nid_referralsource from tnim3_payermaster where upper(nid_webcode) = upper(?) ";
                //SQL Prepared Statements Update 2011
                db_PreparedStatementObject myDPSO = new db_PreparedStatementObject(mySQL);
                myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,weblink));
                myRS = mySS.executePreparedStatement(myDPSO);
                if(myRS!=null&&myRS.next())
                {
                    Integer temp_rs = myRS.getInt("nid_referralsource");
                    Integer temp_pid = myRS.getInt("payerid");
                    if (temp_rs!=null && temp_rs>0){
                        //found it in the database so let's return it.
                        return new NIM3_ReferralSourceObject(temp_pid, temp_rs);
                    }
                }
                mySS.closeAll();
            } catch (Exception eee3eeee) {
                DebugLogger.e ("NID_SubmitCaseObject:translateReferralSourceToPayerID","DB fail[" + eee3eeee.toString()+"]");
            } finally {
                mySS.closeAll();
            }
        }

        //now test promo code table   for WebLink
        if (goodWebLink  ){
            try {
                String mySQL = "select tnim3_payermaster.payerid, nid_referralsource, NIDPromoID from tnim3_payermaster INNER JOIN tnim3_nidpromo on tnim3_nidpromo.payerid = tnim3_payermaster.payerid where upper(weblink) = upper(?)  ";
                //SQL Prepared Statements Update 2011
                db_PreparedStatementObject myDPSO = new db_PreparedStatementObject(mySQL);
                myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,weblink));
                myRS = mySS.executePreparedStatement(myDPSO);
                if(myRS!=null&&myRS.next())
                {
                    Integer temp_rs = myRS.getInt("nid_referralsource");
                    Integer temp_pid = myRS.getInt("payerid");
                    Integer temp_prid = myRS.getInt("NIDPromoID");
                    if (temp_rs!=null && temp_rs>0){
                        //found it in the database so let's return it.
                        return new NIM3_ReferralSourceObject(temp_pid, temp_rs, temp_prid);
                    }
                }
                mySS.closeAll();
            } catch (Exception eee3eeee) {
                DebugLogger.e ("NID_SubmitCaseObject:translateReferralSourceToPayerID","DB fail promo[" + eee3eeee.toString()+"]");
            } finally {
                mySS.closeAll();
            }
        }

//        //leave the hardcoded once in as a backup just in case.  Once all data entry is tested, we can remove.
//        if (goodCode){
//
//            if (code.equalsIgnoreCase("nb")){
//                return 22239;
//            } else if (code.equalsIgnoreCase("dl")){
//                return 21065;
//            } else if (code.equalsIgnoreCase("coverdell")){
//                return 30236;
//            } else if (code.equalsIgnoreCase("needymeds")){
//                return 28453;
//            } else if (code.equalsIgnoreCase("pps") || code.equalsIgnoreCase("Premier Payor Solutions")    ){
//                return 30237;
//            } else if (code.equalsIgnoreCase("nop")){
//                return 28763;
//            }
//        }
//        if (goodSource){
//            if (source.equalsIgnoreCase("nb")){
//                return 22239;
//            } else if (source.equalsIgnoreCase("dl")){
//                return 21065;
//            } else if (source.equalsIgnoreCase("coverdell")){
//                return 30236;
//            } else if (source.equalsIgnoreCase("needymeds")){
//                return 28453;
//            } else if (source.equalsIgnoreCase("pps") || source.equalsIgnoreCase("Premier Payor Solutions")    ){
//                return 30237;
//            } else if (source.equalsIgnoreCase("nop")){
//                return 28763;
//            }
//        }
        //none found, return default
        return new NIM3_ReferralSourceObject(123, NID_REF_SOURCE);

    }
}
