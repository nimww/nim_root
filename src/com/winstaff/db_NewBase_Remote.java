package com.winstaff;
/**
 * @author  Michael Million
 * @version 1.00
 *
 * db_Base.java
 *
 * This class is used to load a record where only one occurance exists in a table.  This will be the case where the record has a unique ID.
 *
 * Created on March 10, 2001, 1:38 PM
 */

// SQL Stuff
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;

// Iterator like stuff
import java.util.Iterator;
import java.util.Enumeration;
import java.text.StringCharacterIterator;

// Collections
import java.util.Hashtable;
import java.util.Vector;

// Date parsing stuff
import java.util.Date;
import java.text.SimpleDateFormat;
import java.lang.Integer;
import java.text.ParseException;
import java.sql.Types;


// My Stuff

/**
 *
 * @author      Scott Ellis, Michael Million
 * @version     2.00
 */
public class  db_NewBase_Remote extends Object implements dbTableInterface
{
    // Used to signify if no client has been initialized, as in the case of a new client.
    public  static final    Integer NO_UNIQUE_ID            = new Integer( -1 );
    
    // Identifier, sometimes unique, to retrieve records.
    private Integer         iUniqueID                       = new Integer( NO_UNIQUE_ID.toString() );
    private String          sTableName;
    private String          sKeyFieldName;
//    private boolean         bReadingInData;

    // Used to commit data
    private Hashtable       htCurrentData;                  // Contains name/value pairs for data read in
    private Vector          vModifiedData;
    
    // Class attribute stuff
    private String          sClassIdentifier;
    
    // Database stuff
    private Connection      cCon;

    /** Configures new db_Base
     * @param sTableName String Name of table to set/get information from
     * @param sKeyFieldName String Comma delimited list of fields to set/get
    */
    public db_NewBase_Remote ( String sTableName, String sKeyFieldName )
    {
        String  sDebugString = "";
        
        htCurrentData           = new Hashtable();
        vModifiedData           = new Vector();
//        bReadingInData          = false;
        
        this.sClassIdentifier =  this.getClass().getName();
        this.sTableName = sTableName;
        this.sKeyFieldName = sKeyFieldName;
        
        //DebugLogger.println( sClassIdentifier );
       
        try
        {
            sDebugString = ".forName";
            Class.forName( ConfigurationInformation_Remote.sJDBCDriverName );
        }
        catch ( Exception e )
        {
            DebugLogger.println ( sClassIdentifier + ": in db_Base Constuctor Error: " + e.toString () + "\n" + sDebugString );
        }
    }   // End of default constructor
   
  
    protected final void loadData ()
    {
        String  sDebugString = "";
        String  sSQLString;
        int     numberOfColumns;
        
        try
        {
            sDebugString = ".getConnection";
            cCon = DriverManager.getConnection( ConfigurationInformation_Remote.sURL );
            
            sDebugString = ".createStatement";
            Statement   stmt = cCon.createStatement();
            
            sDebugString = ".executeQuery";
            sSQLString = "select * from " + sTableName + " where " + sKeyFieldName + "=" + iUniqueID.toString();
            
//            if ( ConfigurationInformation.bInDebugMode )
            {
                DebugLogger.println( "Query: " + sSQLString );
            }
            
            sDebugString = "first execute";
            ResultSet           rs = stmt.executeQuery( sSQLString );
            
            sDebugString = "get meta data";
            ResultSetMetaData   rsmd = rs.getMetaData();
            numberOfColumns = rsmd.getColumnCount();
		System.out.println("---------Load Data");//ste 2008
		System.out.println("   # of Cols:" + numberOfColumns);//ste 2008
            sDebugString = "Clear current aata";
            // Lose old data
            clearCurrentData();
            
            // Read in data from result set
//            bReadingInData = true;
            
            sDebugString = "loop through meta data";
            rs.next();

            int              iColumnType;
            Date             dDate;
            Date             dDefaultDate;
            SimpleDateFormat sdDefaultFormatter = new SimpleDateFormat ( ConfigurationInformation_Remote.sDateTimeFormat );
            try
            {
                  dDefaultDate = sdDefaultFormatter.parse( ConfigurationInformation_Remote.sDefaultDateValue );
            }
            catch ( java.text.ParseException e )
            {
                  DebugLogger.println( e.toString() );
                  throw e;
            }            
            for ( int i = 0; i < numberOfColumns; i++ )
            {
                String          sName;
                String          sValue="";
                String          sdDate="";

                sName = rsmd.getColumnName ( i + 1 );
                iColumnType = rsmd.getColumnType( i + 1 );
                
                //DebugLogger.println( iColumnType );
                //DebugLogger.println( java.sql.Types.DATE );
                //DebugLogger.println( java.sql.Types.TIME );
                //DebugLogger.println( java.sql.Types.TIMESTAMP );


		System.out.println("   ColType: " + iColumnType);//ste 2008


                switch ( iColumnType )
                {
                    case java.sql.Types.DATE:
                    case java.sql.Types.TIME:
                    case java.sql.Types.TIMESTAMP:
                        SimpleDateFormat formatter = new SimpleDateFormat ( ConfigurationInformation_Remote.sDateTimeFormat );

			if (sName.equalsIgnoreCase("appointmenttime")||sName.equalsIgnoreCase("lastimportdate")||sName.equalsIgnoreCase("uniquemodifydate")||sName.equalsIgnoreCase("uniquecreatedate")||sName.equalsIgnoreCase("transactiondate")||sName.equalsIgnoreCase("callstart")||sName.equalsIgnoreCase("callend"))
		        {
	                        sdDate = dateUtils.fixDate(rs.getTimestamp( i + 1 ));
	                        sValue =  ( sdDate == null ) ? ConfigurationInformation_Remote.sDefaultDateValue : sdDate ;
		        }
		        else
			{
	                        dDate = rs.getDate( i + 1 );
	                        sValue = formatter.format( ( dDate == null ) ? dDefaultDate : dDate );
			}
			try
			{
	                        sValue = formatter.format(formatter.parse(sValue));
			}
			catch (Exception eeeee)
			{
				System.out.println("**************\n\n"+sdDate+"\n"+eeeee+"\n\n\n\n*****************");
			}
                        break;

                    default:
                        sValue = rs.getString( i + 1 );                        
                     break;
                }   // End of switch


                if ( sValue != null )
                {
                    NameValuePair   nvp = new NameValuePair( sName.toUpperCase(), sValue, false );
                    htCurrentData.put( sName.toUpperCase(), nvp );
	 	    System.out.println("   SName#" + sName + " | " + " SValue#" + sValue);//ste 2008
                }
                else
                {
                    //DebugLogger.println( sName + ": null" );
			System.out.println("   SValueNUL?????#" + sValue);//ste 2008

                }
                
                if ( ConfigurationInformation_Remote.bInDebugModeShowFldSets )
                {
                    DebugLogger.println( "[ " + sName + ", " + sValue + "]" );
                }
            }
            
//            bReadingInData = false;

            sDebugString = "resultset close";
            rs.close();
            
            sDebugString = "statement close";
            stmt.close();
            
            sDebugString = "connection close";
            cCon.close ();
            System.out.println(  "   Close Load Data-----------------------" );
        }
        catch( Exception e )
        {
            System.out.println(  "BIG ERROR" + sClassIdentifier + ": " + e.toString() + "\n" + sDebugString );
            DebugLogger.println(  sClassIdentifier + ": " + e.toString() + "\n" + sDebugString );
        }
    }   // End of loadFromDatabase()

    /** Set Unique Identifier for given information.  Note: this identifier could be used to retrieve more than one record.
    * @param iUniqueID Unique identifier
    */    
    public void setUniqueID( Integer iparamUniqueID )
    {
        long        lParameterValue     = iparamUniqueID.longValue();
        long        lCurrentValue       = iUniqueID.longValue ();
        long        lNoUniqueID         = NO_UNIQUE_ID.longValue();
        
        // If no change to Unique ID, don't bother with operation.
        //if ( ( lParameterValue != lCurrentValue ) &&  ( lCurrentValue != lNoUniqueID ) )
        if ( ConfigurationInformation_Remote.bInDebugModeShowFldSets )
        {
            DebugLogger.println( lCurrentValue );
        }
        
            try
            {
                if ( ConfigurationInformation_Remote.bInDebugModeShowFldSets )
                {
                    DebugLogger.println( "Commiting data for " );
                    DebugLogger.println( lCurrentValue );
                }
                
                //Changing Unique ID, I need to commit current client data to database.
                //this seems to be called upon any LOAD, thus causing commitData to run for any initializations of existing records
		if (false)
		{
			commitData();
		}

            }
            catch ( SQLException e )
            {
                DebugLogger.println( sClassIdentifier + ": " + e.toString() );
            }

            iUniqueID = iparamUniqueID;
            
            loadData();
            
        if ( ConfigurationInformation_Remote.bInDebugModeShowFldSets )
        {
            DebugLogger.println( "SetUniqueID:Leaving" );
        }
    }   // End of setUniqueID 
    
    /** Get current Unique ID
    * @return Integer Unique ID
    */    
    public Integer getUniqueID()
    {
        return iUniqueID;
    }   // End of getUniqueID
  
    
    public String getFieldData( String sFieldName )
    {
        NameValuePair   nvp;
        if ( ( nvp = (NameValuePair) htCurrentData.get( sFieldName.toUpperCase() ) ) == null )
        {
            return null;
        }
        else
        {
            return nvp.getValue();
        }
    }
    
    public void setFieldData( String sFieldName, String sValue )
    {
        NameValuePair   nvp;
	if (sValue.length()>=3998&&!sFieldName.equalsIgnoreCase("emailbody"))
	{
		sValue = sValue.substring(0,3998);
	}
	
        
        if ( ( nvp = (NameValuePair) htCurrentData.get( sFieldName.toUpperCase() ) ) == null )
        {
            nvp = new NameValuePair( sFieldName.toUpperCase(), sValue );
        }
        else
        {
            nvp.setValue( sValue );
        }

        nvp.setIsModified( true );
        
        htCurrentData.put( sFieldName.toUpperCase(), nvp );

    }
    
    
    protected void dataUpdate( String sFieldName, String sValue )
    {
        if ( sValue.indexOf ('\'' ) != -1 )
        {
            // We need to escape sequence ' with ''
            String      sNewValue = "";
           
            StringCharacterIterator sciInterator = new StringCharacterIterator( sValue );

            for(char c = sciInterator.first(); c != StringCharacterIterator.DONE; c = sciInterator.next() )
            {
                if ( c == '\'' )
                {
                    sNewValue += '\'';
                }
                
                sNewValue += c;
            }
            
            buildModifiedString( sFieldName.toUpperCase(), "'" + sNewValue + "'" );
        }
        else if ( sValue.equalsIgnoreCase ("false") || sValue.equalsIgnoreCase ("true") )
        {
            //se add/modify 12-18-2001
	    if (ConfigurationInformation_Remote.sDBType.equalsIgnoreCase("access"))
	    {
                buildModifiedString( sFieldName.toUpperCase(), sValue );
            }
            else
            {
//	    else if (ConfigurationInformation_Remote.sDBType.equalsIgnoreCase("mySQL"))
                String sValueMod = "0";
                if (sValue.equalsIgnoreCase("true"))
                {
                    sValueMod = "1";
                }
                buildModifiedString( sFieldName.toUpperCase(), sValueMod );
            }
        }
        else
        {        
            buildModifiedString( sFieldName.toUpperCase(), "'" + sValue + "'" );
        }
    }
    
    private void buildModifiedString( String sFieldName, String sValue )
    {
//        if (!bReadingInData )
        {
            NameValuePair       nvpNewOne = new NameValuePair( sFieldName.toUpperCase(), sValue );
            
            vModifiedData.add ( nvpNewOne );

//            if ( ConfigurationInformation.bInDebugMode )
            {
                DebugLogger.println( "buildModifiedString: " + nvpNewOne.toString() );
            }
        }
    }
    
    private void clearCurrentData()
    {
        htCurrentData.clear ();
    }
    
    private void clearModifiedData()
    {
        NameValuePair   nvp;
        
        
        vModifiedData.clear();

        for (Enumeration e = htCurrentData.elements() ; e.hasMoreElements() ;)
        {
            nvp = (NameValuePair) e.nextElement();
            nvp.setIsModified( false );
        }
    }   // End of clearModifiedData()

    //SE - made for basic audit
    private String buildAuditString()
    {
        Iterator        iIterator       = vModifiedData.iterator ();
        String          sAuditString      = "";
        NameValuePair   nvpNameValue;
        
        while ( iIterator.hasNext() )
        {
            nvpNameValue = (NameValuePair) iIterator.next();

            if ( sAuditString.length () != 0 )
            {
                sAuditString += ", ";
            }
            
            sAuditString += "[" + nvpNameValue.getName().toUpperCase()  + "]=[" + nvpNameValue.getValue()  + "]";
        }
        
        return sAuditString;
    }   // End of buildUpdateSQLString()
    
    private String buildUpdateSQLString()
    {
        Iterator        iIterator       = vModifiedData.iterator ();
        String          sSQLString      = "";
        NameValuePair   nvpNameValue;
        
        while ( iIterator.hasNext() )
        {
            nvpNameValue = (NameValuePair) iIterator.next();
            
            if ( sSQLString.length () != 0 )
            {
                sSQLString += ", ";
            }
            
            sSQLString += nvpNameValue.getName().toUpperCase() + "=" + nvpNameValue.getValue ();
        }
        
        return sSQLString;
    }   // End of buildUpdateSQLString()
    
    private String buildInsertFieldSQLString()
    {
        Iterator        iIterator       = vModifiedData.iterator ();
        String          sSQLString      = "";
        NameValuePair   nvpNameValue;
        
        while ( iIterator.hasNext() )
        {
            nvpNameValue = (NameValuePair) iIterator.next();
            
            if ( sSQLString.length () != 0 )
            {
                sSQLString += ", ";
            }
            
            sSQLString += nvpNameValue.getName().toUpperCase();
        }
        
        return "(" + sSQLString + ")";
    }   // End of buildInsertFieldSQLString()
    
    
   private String buildInsertValueSQLString()
    {
        Iterator        iIterator       = vModifiedData.iterator ();
        String          sSQLString      = "";
        NameValuePair   nvpNameValue;
        
        while ( iIterator.hasNext() )
        {
            nvpNameValue = (NameValuePair) iIterator.next();
            
            if ( sSQLString.length () != 0 )
            {
                sSQLString += ", ";
            }
            
            sSQLString += nvpNameValue.getValue();
        }
        
        return "(" + sSQLString + ")";
    }   // End of buildInsertValueSQLString()
    
   private java.util.Vector buildAuditValueSQLVector(String refID)
    {
        SimpleDateFormat sdDefaultFormatter = new SimpleDateFormat ( ConfigurationInformation_Remote.sDateTimeFormat );
        Iterator        iIterator       = vModifiedData.iterator ();
        NameValuePair   nvpNameValue;
	java.util.Vector valueVector = new java.util.Vector();
        if (sTableName.equalsIgnoreCase("tEmailTransaction")||sTableName.equalsIgnoreCase("tAudit"))
        {
	}
	else
	{

         while ( iIterator.hasNext() )
         {
            nvpNameValue = (NameValuePair) iIterator.next();
                String oldValue = nvpNameValue.getValue();
                String newValue = "";
                if (oldValue.length()>=3998)
                {
                    newValue = oldValue.substring(0,3998) + "'";
                }
                else
                {
                    newValue = oldValue;
                }
            if (sTableName.equalsIgnoreCase("tEmailTransaction")||sTableName.equalsIgnoreCase("tAudit"))
	    {
	    }
		else
	    {
		    String sSQLString = "insert into tAudit (auditDate, tableName, fieldName, refID, ValueChange, Modifier, Comments) values ('" + sdDefaultFormatter.format(new java.util.Date()) + "','" + sTableName + "','" + nvpNameValue.getName() + "','"  + iUniqueID.toString()  + "'," + newValue + ",'" + this.ModifyComments + "','" + "Audit Version 1.0 for V3.0a " + "')";
		    valueVector.addElement(sSQLString);
	    }
         }
        }//1 level audit check
        
        return valueVector;
    }   // End of buildInsertValueSQLString()
    
    private void determineMondifiedData()
    {
        //String          sKey;
        //Object          oData;
        NameValuePair   nvp;


        for (Enumeration e = htCurrentData.elements() ; e.hasMoreElements() ;)
        {
            nvp = (NameValuePair) e.nextElement();
            
            if ( nvp.getIsModified() )
            {
                dataUpdate( nvp.getName().toUpperCase(), nvp.getValue() );

                DebugLogger.println( nvp.toString() );
            }
        }
    }
    
    /** Commit current data to database.  This information will be assocated with the current Unique ID.
    * @throws SQLException Passes on exception
    */    
    public void commitData() throws SQLException
    {
        String  sDebugString = "";
        String  sSQLString = null;
        String  sAuditString = "";
        int     iRowsAffected = 0;
        try
        {
	    DebugLogger.println("Running CommitData Version 3 9/10/2002");
            sDebugString = ".getConnection";
            //se add
            //DebugLogger.println("sUser:"+ConfigurationInformation.sUser);
            //se add
            //DebugLogger.println("sURL:"+ConfigurationInformation.sURL);
            //se mod 12-16  - this shoudl work for any DB, however orignally it did not have the U/P parameters
            cCon = DriverManager.getConnection( ConfigurationInformation_Remote.sURL,ConfigurationInformation_Remote.sUser,ConfigurationInformation_Remote.sPassword);
            

            sDebugString = ".createStatement";
            Statement   stmt = cCon.createStatement();

            determineMondifiedData();

            // Is an update needed?
            if ( vModifiedData.size() > 0 )
            {
                if ( iUniqueID.compareTo( NO_UNIQUE_ID ) == 0 )
                {
                    sDebugString = ".executeUpdate for insert";
                    sSQLString = "insert into " + sTableName + " " + buildInsertFieldSQLString() + " values " + buildInsertValueSQLString();

                }
                else
                {
                    sDebugString = ".executeUpdate";
                    sSQLString = "update " + sTableName + " set " + buildUpdateSQLString() + " where " + sKeyFieldName.toUpperCase() + "=" + iUniqueID.toString ();;
		    //SE - Basic Audit Trail
                    //sAuditString = (new java.util.Date()) + "\t" + sKeyFieldName + "\t" + iUniqueID.toString() + "\t" + sTableName + "\t" + buildAuditString()  ;
                }

//                if ( ConfigurationInformation.bInDebugMode )
                {
                    DebugLogger.println( "SQL string: " + sSQLString );
                    //DebugLogger.println( "AUDIT string: " + sAuditString );
                    //AuditLogger.printLine( "AUDIT string: " + sAuditString );
                }

		try
		{
	                iRowsAffected = stmt.executeUpdate( sSQLString );
		}
		catch(Exception e55)
		{
			sendFatalError("[data collection error]",e55.toString(),sDebugString);
			throw new java.sql.SQLException ("[data collection error] " + e55);
		}

                

//                if ( ConfigurationInformation.bInDebugMode )
                {
                    DebugLogger.println( "Rows affected: " );
                    DebugLogger.println( iRowsAffected );
                }
            
            // Get unique ID when performing an insert
            if ( iUniqueID.compareTo( NO_UNIQUE_ID ) == 0 )            
            {
                sDebugString = ".executeQuery to obtain new unique key";
		//se add/modify for support for MS & SQL
	        if (ConfigurationInformation_Remote.sDBType.equalsIgnoreCase("ms"))
	        {
                    sSQLString = "SELECT @@identity from " + sTableName;
                }
	        else if (ConfigurationInformation_Remote.sDBType.equalsIgnoreCase("ps"))
                {
                    sSQLString = "SELECT currval('" + sTableName + "_" + sKeyFieldName + "_seq')";
                }
	        else if (ConfigurationInformation_Remote.sDBType.equalsIgnoreCase("mySQL"))
                {
                    sSQLString = "SELECT last_insert_id() from " + sTableName;
                }

                DebugLogger.println("sSQLString: " + sSQLString);
                
                ResultSet rs = stmt.executeQuery( sSQLString );
                rs.next ();
                iUniqueID = new Integer( rs.getInt( 1 ) );

//                if ( ConfigurationInformation.bInDebugMode )
                {
                    DebugLogger.println( "new record uid:=" );
                    DebugLogger.println( iUniqueID );
	            //se add
                    //DebugLogger.println("iUniqueID: " + iUniqueID);
                }
            }
            if (ConfigurationInformation_Remote.bRunAudit)
	    {
	            //build Audit List
		    java.util.Vector AuditList = buildAuditValueSQLVector(iUniqueID.toString());
	            //run AuditSQL
	            for (int i=0; i<AuditList.size();i++)
		    {
	                String TempSQLString = (String) AuditList.elementAt(i);
	                DebugLogger.println("TempSQLString: " + TempSQLString);
			int iRowsAffected2;
			try
			{
		                iRowsAffected2 = stmt.executeUpdate( TempSQLString );
			}
			catch(Exception e55)
			{
				sendFatalError("[data audit error]\n["+TempSQLString +"]",e55.toString(),sDebugString);
				throw new java.sql.SQLException ("[data audit error] " + e55);
			}
	                DebugLogger.println("-->" + iRowsAffected2);
		    }
	    }


//modified close
            }



            
            sDebugString = "statement close";
            stmt.close();

            sDebugString = "connection close";
            cCon.close ();

            // Remove information about changed data
            clearModifiedData();
         }
        catch( Exception e )
        {
	        DebugLogger.println(  sClassIdentifier + ": " + e.toString() + "\n" + sDebugString );
		sendFatalError("[uncaught commitData error]",e.toString(),sDebugString);
		throw new java.sql.SQLException ("[data collection error] " + e);
        }
 
    }   // End of Commit()


public void sendFatalError(String sType,String FE, String sDebugString)
{
	java.util.Date myD = new java.util.Date();
	DebugLogger.println(  "[" + myD + "]" );
	emailType myEM = new emailType();
	myEM.setTo("sellis@winstaff.com");
	myEM.setFrom("sellis@winstaff.com");
	myEM.setSubject("FATAL ERRROR ["+ConfigurationInformation_Remote.serverName+"]");
	myEM.setBody("FATAL ERROR\n----------\n"+sType+"\n----------\n" + sClassIdentifier + ": " + FE + "\n" + sDebugString  +"\n-----------\n" + "[" + myD + "]" );
	myEM.isSendMail();
	myEM.setTo("scottellis42@hotmail.com");
	myEM.isSendMail();
}



//new commit method needed for V2... sometimes an item is made with NO Data changes.
//created by Scott Ellis 2-18-2002
    public void commitDataForced() throws SQLException
    {
        DebugLogger.println("Running FORCEDCommitData Version 3 9/10/2002");
        String  sDebugString = "";
        String  sSQLString;
        int     iRowsAffected = 0;

        try
        {
            sDebugString = ".getConnection";
            //se add
            //DebugLogger.println("sUser:"+ConfigurationInformation.sUser);
            //se add
            //DebugLogger.println("sURL:"+ConfigurationInformation.sURL);
            //se mod 12-16  - this shoudl work for any DB, however orignally it did not have the U/P parameters
            cCon = DriverManager.getConnection( ConfigurationInformation_Remote.sURL,ConfigurationInformation_Remote.sUser,ConfigurationInformation_Remote.sPassword);
            

            sDebugString = ".createStatement";
            Statement   stmt = cCon.createStatement();

            determineMondifiedData();

            // Is an update needed?
            //this was modified to always be true	
            if ( true )
            {
                if ( iUniqueID.compareTo( NO_UNIQUE_ID ) == 0 )
                {
                    sDebugString = ".executeUpdate for insert";
                    sSQLString = "insert into " + sTableName + " " + buildInsertFieldSQLString() + " values " + buildInsertValueSQLString();
                }
                else
                {
                    sDebugString = ".executeUpdate";
                    sSQLString = "update " + sTableName + " set " + buildUpdateSQLString() + " where " + sKeyFieldName.toUpperCase() + "=" + iUniqueID.toString ();;
                }

//                if ( ConfigurationInformation.bInDebugMode )
                {
                    DebugLogger.println( "SQL string: " + sSQLString );
                }

                iRowsAffected = stmt.executeUpdate( sSQLString );
                

//                if ( ConfigurationInformation.bInDebugMode )
                {
                    DebugLogger.println( "Rows affected: " );
                    DebugLogger.println( iRowsAffected );
                }
            
            // Get unique ID when performing an insert
            if ( iUniqueID.compareTo( NO_UNIQUE_ID ) == 0 )            
            {
                sDebugString = ".executeQuery to obtain new unique key";
		//se add/modify for support for MS & SQL
	        if (ConfigurationInformation_Remote.sDBType.equalsIgnoreCase("ms"))
	        {
                    sSQLString = "SELECT @@identity from " + sTableName;
                }
	        else if (ConfigurationInformation_Remote.sDBType.equalsIgnoreCase("ps"))
                {
                    sSQLString = "SELECT currval('" + sTableName + "_" + sKeyFieldName + "_seq')";
                }
	        else if (ConfigurationInformation_Remote.sDBType.equalsIgnoreCase("mySQL"))
                {
                    sSQLString = "SELECT last_insert_id() from " + sTableName;
                }

                DebugLogger.println("sSQLString: " + sSQLString);
                
                ResultSet rs = stmt.executeQuery( sSQLString );
                rs.next ();
                iUniqueID = new Integer( rs.getInt( 1 ) );
                //se add
                DebugLogger.println("iUniqueID: " + iUniqueID);

//                if ( ConfigurationInformation.bInDebugMode )
                {
                    DebugLogger.println( "new record uid:=" );
                    DebugLogger.println( iUniqueID );
                }
            }
            }
            
            sDebugString = "statement close";
            stmt.close();

            sDebugString = "connection close";
            cCon.close ();

            // Remove information about changed data
            clearModifiedData();
         }
        catch( Exception e )
        {
            DebugLogger.println(  sClassIdentifier + ": " + e.toString() + "\n" + sDebugString );
        }
 
    }   // End of Commit()



	public void setModifyComments(String aaa)
	{
		this.ModifyComments = aaa;

	}
	public String ModifyComments = "[no-data]";



    public void setField(String fieldN, Object fieldV)
    {DebugLogger.println("setField Method not implemented yet in classAdditionalInformation!");}

    public String getFieldType(String fieldN)
    {return null;}




}   // End of db_Base class definitio
