

package com.winstaff;

import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public class dbtSupportTypeLI extends Object implements InttSupportTypeLI
{

        db_NewBase    dbnbDB;

    public dbtSupportTypeLI()
    {
        dbnbDB = new db_NewBase( "tSupportTypeLI", "SupportTypeID" );

    }    // End of default constructor

    public dbtSupportTypeLI( Integer iNewID )
    {
        dbnbDB = new db_NewBase( "tSupportTypeLI", "SupportTypeID" );
        setUniqueID( iNewID );
    }    // End of Constructor knowing an ID

    public void commitData() throws SQLException
    {
            //no longer trapping/ignoring errors 2011-03-17
            dbnbDB.commitData();
    }    // End of commitData()

    public void commitDataForced() throws SQLException
    {
        dbnbDB.commitDataForced();
    }    // End of commitData()

    public Integer getUniqueID()
    {
        return dbnbDB.getUniqueID();
    }    // End of getUniqueID()

    public void setUniqueID( Integer iNewID )
    {
        dbnbDB.setUniqueID( iNewID );
    }    // End of setUniqueID()

    public void setModifyComments( String sNew )
    {
        dbnbDB.setModifyComments( sNew );
    }    // End of setUniqueID()


    public void setSupportTypeID(Integer newValue)
    {
                dbnbDB.setFieldData( "SupportTypeID", newValue.toString() );
    }

    public Integer getSupportTypeID()
    {
        String           sValue = dbnbDB.getFieldData( "SupportTypeID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setField(String fieldN, Object fieldV)
    {DebugLogger.println("setField Method not implemented yet in class!");}

    public String getFieldType(String fieldN)
    {return null;}


    public void setSupportTypeShort(String newValue)
    {
                dbnbDB.setFieldData( "SupportTypeShort", newValue.toString() );
    }

    public String getSupportTypeShort()
    {
        String           sValue = dbnbDB.getFieldData( "SupportTypeShort" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setSupportTypeLong(String newValue)
    {
                dbnbDB.setFieldData( "SupportTypeLong", newValue.toString() );
    }

    public String getSupportTypeLong()
    {
        String           sValue = dbnbDB.getFieldData( "SupportTypeLong" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

}    // End of bltSupportTypeLI class definition
