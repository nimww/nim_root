package com.winstaff;


import com.winstaff.dbtGenderLI;
import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public interface InttGenderLI extends dbTableInterface
{

    public void setGenderID(Integer newValue);
    public Integer getGenderID();
    public void setGenderShort(String newValue);
    public String getGenderShort();
    public void setGenderLong(String newValue);
    public String getGenderLong();
}    // End of bltGenderLI class definition
