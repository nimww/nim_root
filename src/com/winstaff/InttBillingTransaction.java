

package com.winstaff;

import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public interface InttBillingTransaction extends dbTableInterface
{

    public void setBillingTransactionID(Integer newValue);
    public Integer getBillingTransactionID();
    public void setUniqueCreateDate(Date newValue);
    public Date getUniqueCreateDate();
    public void setUniqueModifyDate(Date newValue);
    public Date getUniqueModifyDate();
    public void setUniqueModifyComments(String newValue);
    public String getUniqueModifyComments();
    public void setBillingID(Integer newValue);
    public Integer getBillingID();
    public void setStatus(Integer newValue);
    public Integer getStatus();
    public void setSettlementDate(Date newValue);
    public Date getSettlementDate();
    public void setSettlementAmount(Double newValue);
    public Double getSettlementAmount();
    public void setComments(String newValue);
    public String getComments();
}    // End of bltBillingTransaction class definition
