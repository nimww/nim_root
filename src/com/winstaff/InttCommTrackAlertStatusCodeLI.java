

package com.winstaff;

import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public interface InttCommTrackAlertStatusCodeLI extends dbTableInterface
{

    public void setCommTrackAlertStatusCodeID(Integer newValue);
    public Integer getCommTrackAlertStatusCodeID();
    public void setStatusShort(String newValue);
    public String getStatusShort();
    public void setStatusLong(String newValue);
    public String getStatusLong();
}    // End of bltCommTrackAlertStatusCodeLI class definition
