package com.winstaff;
/*
 * bltProfessionalLiability_List.java
 *
 * Created: Wed Apr 02 15:09:05 PST 2003
 */



import java.sql.*;
import com.winstaff.dbtProfessionalLiability;
import com.winstaff.ListElement;


/**
 *
 * @author  Scott Ellis via listClassGenCode
 * @version 2.5
 */
public class bltProfessionalLiability_List extends db_BaseRelationTable
{
    /** Default constructor
    */    
    public bltProfessionalLiability_List ( Integer iPhysicianID )
    {
        super( "tProfessionalLiability", "ProfessionalLiabilityID", "ProfessionalLiabilityID", "PhysicianID", iPhysicianID );
    }   // End of bltProfessionalLiability_List()
    

    public void setField(String fieldN, Object fieldV)
    {DebugLogger.println("setField Method not implemented yet in classbltProfessionalLiability_List!");}

    public String getFieldType(String fieldN)
    {return null;}

    public bltProfessionalLiability_List ( Integer iPhysicianID, String extraWhere, String OrderBy )
    {
        super( "tProfessionalLiability", "ProfessionalLiabilityID", "ProfessionalLiabilityID", "PhysicianID", iPhysicianID, extraWhere,OrderBy );
    }   // End of bltProfessionalLiability_List()
     /** Method pull information from the result set and load into class attributes.
    * @param sFieldList Comma delimited list of fields for which data has been retrieved.
    * @param rs Result set from which to pull the data from.
    * @throws SQLException Passes on SQLException
    */      
    protected void transferData( ResultSet rs ) throws SQLException
    {
        while ( rs.next()  )
        {
            Integer         iValue              = new Integer( rs.getString( sLinkFieldName ) );
            Integer         iLinkRecordID       = new Integer( rs.getString( sUniqueIDFieldName ) );
            Object          oReferenceObject;
            ListElement     leNewElement;
            
            oReferenceObject    = new bltProfessionalLiability( iValue );
            
            leNewElement        = new ListElement( iLinkRecordID, oReferenceObject );
            
            vList.add( leNewElement );

        }
    }   // End of transferData()
}   // End of bltProfessionalLiability_List class

