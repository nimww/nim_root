package com.winstaff;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;

import com.google.gson.Gson;

public class PayerExceptionObject {
	private Gson gson = new Gson();

	public PayerExceptionObject() {
	}

	public PayerExceptionObject(String dataString) {
		insertPayerException(dataString);
	}

	public Double checkExceptionPricing(bltNIM3_PayerMaster pm, bltPracticeMaster prm, String cpt) {
		Double exceptionPrice = 0d;

		searchDB2 conn = new searchDB2();
		String query = "select * from payerExceptionlist where payerid=" + pm.getPayerID() + " and practiceid=" + prm.getPracticeID() + " and cpts~'" + cpt + "'";
		ResultSet rs = conn.executeStatement(query);
		try {
			while (rs.next()) {
				exceptionPrice = rs.getDouble("fsid");
				System.out.println("EX P: "+exceptionPrice);
				System.out.println(query);
				break;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		conn.closeAll();
		return exceptionPrice;
	}

	public Double checkExceptionPricing(bltNIM3_PayerMaster pm, bltPracticeMaster prm, CPTGroupObject2 myCGO2_in) {
		Double exceptionPrice = 0d;

		searchDB2 conn = new searchDB2();
		for (CPTObject cPTObject : myCGO2_in.getCPTList()) {
			String query = "select * from payerExceptionlist where payerid=" + pm.getPayerID() + " and practiceid=" + prm.getPracticeID() + " and cpts~'" + cPTObject.getCPT_Code() + "'";
			ResultSet rs = conn.executeStatement(query);
			try {
				while (rs.next()) {
					exceptionPrice += rs.getDouble("fsid");
					break;
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		conn.closeAll();
		return exceptionPrice;
	}

	public String editPayerException(String dataString) {
		PayerExceptionModel payerExceptionModel = new Gson().fromJson(dataString, PayerExceptionModel.class);

		searchDB2 conn = new searchDB2();
		String query = "UPDATE payerExceptionlist SET payerid=" + payerExceptionModel.getPayerId() + ",practiceid=" + payerExceptionModel.getPracticeId() + ",cpts='" + payerExceptionModel.getCpts() + "',fsid='" + payerExceptionModel.getFs() + "',fsidpercentage='" + payerExceptionModel.getPercentage() + "',feetype='" + payerExceptionModel.getFeeType() + "' where payerexceptionlistid =" + payerExceptionModel.getPayerExceptionId() + ";";
		conn.executeUpdate(query);
		conn.closeAll();
		return getExceptionList();
	}

	public String insertPayerException(String dataString) {
		PayerExceptionModel payerExceptionModel = new Gson().fromJson(dataString, PayerExceptionModel.class);

		searchDB2 conn = new searchDB2();
		String query = "INSERT INTO payerExceptionlist (payerid,practiceid,cpts,fsid,fsidpercentage,feetype) VALUES ('" + payerExceptionModel.getPayerId() + "','" + payerExceptionModel.getPracticeId() + "','" + payerExceptionModel.getCpts().replace(" ", "") + "','" + payerExceptionModel.getFs() + "','" + payerExceptionModel.getPercentage() + "','" + payerExceptionModel.getFeeType() + "');";
		conn.executeUpdate(query);
		conn.closeAll();
		return getExceptionList();
	}

	public String getExceptionList() {
		LinkedHashMap<String, PayerExceptionModel> payerExceptionModelList = new LinkedHashMap<String, PayerExceptionModel>();

		searchDB2 conn = new searchDB2();
		String query = "select * from payerexceptionlist order by payerexceptionlistid desc";
		ResultSet rs = conn.executeStatement(query);

		try {
			while (rs.next()) {
				payerExceptionModelList.put(rs.getString("payerexceptionlistid"), new PayerExceptionModel(rs.getString("payerid"), rs.getString("practiceid"), rs.getString("cpts"), rs.getString("fsid"), rs.getString("fsidpercentage"), FeeType.valueOf(rs.getString("feetype"))));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		conn.closeAll();
		return gson.toJson(payerExceptionModelList);
	}

	public String getFeeType() {
		return gson.toJson(FeeType.values());
	}

	public String getModals() {
		LinkedHashMap<String, List<String>> modalObject = new LinkedHashMap<String, List<String>>();

		searchDB2 conn = new searchDB2();
		String query = "select * from \"CPTGroup\"";
		ResultSet rs = conn.executeStatement(query);

		try {
			while (rs.next()) {
				if (modalObject.get(rs.getString(1).trim()) == null) {
					List<String> modalList = new ArrayList<String>();
					modalList.add(rs.getString(2).trim());
					modalObject.put(rs.getString(1).trim(), modalList);
				} else {
					modalObject.get(rs.getString(1).trim()).add(rs.getString(2).trim());
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		conn.closeAll();
		return gson.toJson(modalObject);
	}

	enum FeeType {
		direct, fsid
	}

	public static void main(String[] args) {
		// PayerExceptionModel PayerExceptionModel = new PayerExceptionModel();
		// PayerExceptionModel.setPracticeId("123");
		// PayerExceptionModel.setCpts("72148");
		// PayerExceptionModel.setFeeType(FeeType.direct);
		// PayerExceptionModel.setFs("900");
		// PayerExceptionModel.setPayerId("148");
		// PayerExceptionModel.setPracticeId("111");
		// PayerExceptionModel.setPercentage("1");
		// LinkedHashMap<String,PayerExceptionModel> payerExceptionModelList =
		// new LinkedHashMap<String,PayerExceptionModel>();
		//
		// payerExceptionModelList.put("123", PayerExceptionModel);
		//
		// System.out.println(new Gson().toJson(payerExceptionModelList));

		// System.out.println(new Gson().toJson(FeeType.values()));

		List<Integer> ctps = Arrays.asList(70540, 71550, 72141, 72146, 72148, 72195, 73218, 73221, 73718, 73721, 74181, 70336, 70544, 70547, 70551, 70557, 71555);

		System.out.println(ctps.toString().replaceAll("\\[", "").replaceAll("\\]", ""));

	}
}
