

package com.winstaff;

import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;
import java.util.Enumeration;

public class bltLicenseRegistration extends Object implements InttLicenseRegistration
{

    dbtLicenseRegistration    dbDB;

    public bltLicenseRegistration()
    {
        dbDB = new dbtLicenseRegistration();
        dbDB.setUniqueCreateDate(new java.util.Date());
        makeRequiredHash();
        makeExpiredHash();
        makeEnglishHash();

    }    // End of default constructor

    public bltLicenseRegistration( Integer iNewID )
    {        dbDB = new dbtLicenseRegistration( iNewID );
        makeRequiredHash();
        makeExpiredHash();
        makeEnglishHash();
    }    // End of Constructor knowing an ID


    public bltLicenseRegistration( Integer iNewID, Integer iGroupSecurityID )
    {
        this.GroupSecurityInit(iGroupSecurityID);
        dbDB = new dbtLicenseRegistration( iNewID );
        makeExpiredHash();
        makeEnglishHash();
        makeRequiredHash(iGroupSecurityID);
    }    // End of Constructor knowing an ID

    public bltLicenseRegistration( Integer iGroupSecurityID, boolean isTrue )
    {
        this.GroupSecurityInit(iGroupSecurityID);
        dbDB = new dbtLicenseRegistration(  );
        makeExpiredHash();
        makeEnglishHash();
        makeRequiredHash(iGroupSecurityID);
    }    // End of Security unknown ID

    public void GroupSecurityInit( Integer iGroupSecurityID )
    {
        bltSecurityGroupMaster mySGM = new bltSecurityGroupMaster(iGroupSecurityID);
        this.UserSecurityID = iGroupSecurityID;
        if (mySGM.getRunFieldSecurityID().intValue()==1)
        {
            this.runFieldSecurity = true;
            makeRequiredHash(iGroupSecurityID);
        }
    }  

    public EventReferenceType getEventReference()
    {
        return EventUtils.getEventReference("tLicenseRegistration", this.getUniqueID());
    }  

    public String convertString2LogString(String normalString) 
    {
        String myVal = "";
        for (int i=0;i<normalString.length();i++)
        {
            if ( (normalString.charAt(i)!='\r')&&(normalString.charAt(i)!='\n')&&(normalString.charAt(i)!='\t') )
            {
                myVal+=normalString.charAt(i);
            }
            else
            {
                myVal+="<NLT>";
            }
        }
        return myVal;
    }
    public void runAudit()
    {
        String AuditString = new java.util.Date()+" \t " ;  
        AuditString += " tLicenseRegistration "; 
        AuditString += " LicenseRegistrationID ="+this.getUniqueID(); 
        AuditString += " PhysicianID ="+this.getPhysicianID(); 
        for (int i=0;i<this.AuditVector.size();i++)
        {
            AuditString+="\t"+convertString2LogString((String)this.AuditVector.elementAt(i));
        }
        AuditLogger.printLine(AuditString);
    }
    public void commitData() throws SQLException
    {
        runAudit();
        dbDB.setModifyComments(this.getUniqueModifyComments());
        dbDB.setUniqueModifyDate(new java.util.Date());
        dbDB.commitData();
        DataControlUtils.dataChanged("tLicenseRegistration",this.getPhysicianID() ,this.AuditVector.size(), "PhysicianID");
        this.AuditVector.removeAllElements();
    }    // End of commitData()

    public void commitDataForced() throws SQLException
    {
        dbDB.commitDataForced();
    }    // End of commitData()

    public Integer getUniqueID()
    {
        return dbDB.getUniqueID();
    }    // End of getUniqueID()

    public void setUniqueID( Integer iNewID )
    {
        dbDB.setUniqueID( iNewID );
    }    // End of setUniqueID()


    public void setLicenseRegistrationID(Integer newValue)
    {
        dbDB.setLicenseRegistrationID(newValue);
    }

    public Integer getLicenseRegistrationID()
    {
        return dbDB.getLicenseRegistrationID();
    }

    java.util.Vector AuditVector = new java.util.Vector();

    public void setUniqueCreateDate(Date newValue)
    {        this.setUniqueCreateDate(newValue,this.UserSecurityID);

    }
    public Date getUniqueCreateDate()
    {
        return this.getUniqueCreateDate(this.UserSecurityID);
    }

    public void setUniqueCreateDate(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='UniqueCreateDate' AND TableName='tLicenseRegistration'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {
                   this.AuditVector.addElement("[UniqueCreateDate]=["+newValue+"]");
                   this.AuditVector.addElement("[UniqueCreateDate]=["+newValue+"]");
                   dbDB.setUniqueCreateDate(newValue);
               }
            }
        }
        else
        {
           this.AuditVector.addElement("[UniqueCreateDate]=["+newValue+"]");
           this.AuditVector.addElement("[UniqueCreateDate]=["+newValue+"]");
           dbDB.setUniqueCreateDate(newValue);
         }
    }
    public Date getUniqueCreateDate(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='UniqueCreateDate' AND TableName='tLicenseRegistration'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getUniqueCreateDate();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getUniqueCreateDate();
         }
        return myVal;
    }

    public void setUniqueModifyDate(Date newValue)
    {        this.setUniqueModifyDate(newValue,this.UserSecurityID);

    }
    public Date getUniqueModifyDate()
    {
        return this.getUniqueModifyDate(this.UserSecurityID);
    }

    public void setUniqueModifyDate(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='UniqueModifyDate' AND TableName='tLicenseRegistration'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {
                   this.AuditVector.addElement("[UniqueModifyDate]=["+newValue+"]");
                   this.AuditVector.addElement("[UniqueModifyDate]=["+newValue+"]");
                   dbDB.setUniqueModifyDate(newValue);
               }
            }
        }
        else
        {
           this.AuditVector.addElement("[UniqueModifyDate]=["+newValue+"]");
           this.AuditVector.addElement("[UniqueModifyDate]=["+newValue+"]");
           dbDB.setUniqueModifyDate(newValue);
         }
    }
    public Date getUniqueModifyDate(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='UniqueModifyDate' AND TableName='tLicenseRegistration'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getUniqueModifyDate();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getUniqueModifyDate();
         }
        return myVal;
    }

    public void setUniqueModifyComments(String newValue)
    {        this.setUniqueModifyComments(newValue,this.UserSecurityID);

    }
    public String getUniqueModifyComments()
    {
        return this.getUniqueModifyComments(this.UserSecurityID);
    }

    public void setUniqueModifyComments(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='UniqueModifyComments' AND TableName='tLicenseRegistration'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {
                   this.AuditVector.addElement("[UniqueModifyComments]=["+newValue+"]");
                   this.AuditVector.addElement("[UniqueModifyComments]=["+newValue+"]");
                   dbDB.setUniqueModifyComments(newValue);
               }
            }
        }
        else
        {
           this.AuditVector.addElement("[UniqueModifyComments]=["+newValue+"]");
           this.AuditVector.addElement("[UniqueModifyComments]=["+newValue+"]");
           dbDB.setUniqueModifyComments(newValue);
         }
    }
    public String getUniqueModifyComments(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='UniqueModifyComments' AND TableName='tLicenseRegistration'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getUniqueModifyComments();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getUniqueModifyComments();
         }
        return myVal;
    }

    public void setPhysicianID(Integer newValue)
    {        this.setPhysicianID(newValue,this.UserSecurityID);

    }
    public Integer getPhysicianID()
    {
        return this.getPhysicianID(this.UserSecurityID);
    }

    public void setPhysicianID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PhysicianID' AND TableName='tLicenseRegistration'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {
                   this.AuditVector.addElement("[PhysicianID]=["+newValue+"]");
                   this.AuditVector.addElement("[PhysicianID]=["+newValue+"]");
                   dbDB.setPhysicianID(newValue);
               }
            }
        }
        else
        {
           this.AuditVector.addElement("[PhysicianID]=["+newValue+"]");
           this.AuditVector.addElement("[PhysicianID]=["+newValue+"]");
           dbDB.setPhysicianID(newValue);
         }
    }
    public Integer getPhysicianID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PhysicianID' AND TableName='tLicenseRegistration'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPhysicianID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPhysicianID();
         }
        return myVal;
    }

    public void setIsCurrent(Integer newValue)
    {        this.setIsCurrent(newValue,this.UserSecurityID);

    }
    public Integer getIsCurrent()
    {
        return this.getIsCurrent(this.UserSecurityID);
    }

    public void setIsCurrent(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='IsCurrent' AND TableName='tLicenseRegistration'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {
                   this.AuditVector.addElement("[IsCurrent]=["+newValue+"]");
                   this.AuditVector.addElement("[IsCurrent]=["+newValue+"]");
                   dbDB.setIsCurrent(newValue);
               }
            }
        }
        else
        {
           this.AuditVector.addElement("[IsCurrent]=["+newValue+"]");
           this.AuditVector.addElement("[IsCurrent]=["+newValue+"]");
           dbDB.setIsCurrent(newValue);
         }
    }
    public Integer getIsCurrent(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='IsCurrent' AND TableName='tLicenseRegistration'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getIsCurrent();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getIsCurrent();
         }
        return myVal;
    }

    public void setLicenseType(Integer newValue)
    {        this.setLicenseType(newValue,this.UserSecurityID);

    }
    public Integer getLicenseType()
    {
        return this.getLicenseType(this.UserSecurityID);
    }

    public void setLicenseType(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='LicenseType' AND TableName='tLicenseRegistration'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {
                   this.AuditVector.addElement("[LicenseType]=["+newValue+"]");
                   this.AuditVector.addElement("[LicenseType]=["+newValue+"]");
                   dbDB.setLicenseType(newValue);
               }
            }
        }
        else
        {
           this.AuditVector.addElement("[LicenseType]=["+newValue+"]");
           this.AuditVector.addElement("[LicenseType]=["+newValue+"]");
           dbDB.setLicenseType(newValue);
         }
    }
    public Integer getLicenseType(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='LicenseType' AND TableName='tLicenseRegistration'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getLicenseType();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getLicenseType();
         }
        return myVal;
    }

    public void setSubType(String newValue)
    {        this.setSubType(newValue,this.UserSecurityID);

    }
    public String getSubType()
    {
        return this.getSubType(this.UserSecurityID);
    }

    public void setSubType(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SubType' AND TableName='tLicenseRegistration'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {
                   this.AuditVector.addElement("[SubType]=["+newValue+"]");
                   this.AuditVector.addElement("[SubType]=["+newValue+"]");
                   dbDB.setSubType(newValue);
               }
            }
        }
        else
        {
           this.AuditVector.addElement("[SubType]=["+newValue+"]");
           this.AuditVector.addElement("[SubType]=["+newValue+"]");
           dbDB.setSubType(newValue);
         }
    }
    public String getSubType(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='SubType' AND TableName='tLicenseRegistration'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getSubType();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getSubType();
         }
        return myVal;
    }

    public void setNPDBFOL(Integer newValue)
    {        this.setNPDBFOL(newValue,this.UserSecurityID);

    }
    public Integer getNPDBFOL()
    {
        return this.getNPDBFOL(this.UserSecurityID);
    }

    public void setNPDBFOL(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='NPDBFOL' AND TableName='tLicenseRegistration'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {
                   this.AuditVector.addElement("[NPDBFOL]=["+newValue+"]");
                   this.AuditVector.addElement("[NPDBFOL]=["+newValue+"]");
                   dbDB.setNPDBFOL(newValue);
               }
            }
        }
        else
        {
           this.AuditVector.addElement("[NPDBFOL]=["+newValue+"]");
           this.AuditVector.addElement("[NPDBFOL]=["+newValue+"]");
           dbDB.setNPDBFOL(newValue);
         }
    }
    public Integer getNPDBFOL(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='NPDBFOL' AND TableName='tLicenseRegistration'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getNPDBFOL();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getNPDBFOL();
         }
        return myVal;
    }

    public void setStateID(Integer newValue)
    {        this.setStateID(newValue,this.UserSecurityID);

    }
    public Integer getStateID()
    {
        return this.getStateID(this.UserSecurityID);
    }

    public void setStateID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='StateID' AND TableName='tLicenseRegistration'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {
                   this.AuditVector.addElement("[StateID]=["+newValue+"]");
                   this.AuditVector.addElement("[StateID]=["+newValue+"]");
                   dbDB.setStateID(newValue);
               }
            }
        }
        else
        {
           this.AuditVector.addElement("[StateID]=["+newValue+"]");
           this.AuditVector.addElement("[StateID]=["+newValue+"]");
           dbDB.setStateID(newValue);
         }
    }
    public Integer getStateID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='StateID' AND TableName='tLicenseRegistration'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getStateID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getStateID();
         }
        return myVal;
    }

    public void setIsPractice(Integer newValue)
    {        this.setIsPractice(newValue,this.UserSecurityID);

    }
    public Integer getIsPractice()
    {
        return this.getIsPractice(this.UserSecurityID);
    }

    public void setIsPractice(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='IsPractice' AND TableName='tLicenseRegistration'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {
                   this.AuditVector.addElement("[IsPractice]=["+newValue+"]");
                   this.AuditVector.addElement("[IsPractice]=["+newValue+"]");
                   dbDB.setIsPractice(newValue);
               }
            }
        }
        else
        {
           this.AuditVector.addElement("[IsPractice]=["+newValue+"]");
           this.AuditVector.addElement("[IsPractice]=["+newValue+"]");
           dbDB.setIsPractice(newValue);
         }
    }
    public Integer getIsPractice(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='IsPractice' AND TableName='tLicenseRegistration'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getIsPractice();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getIsPractice();
         }
        return myVal;
    }

    public void setLicenseDocumentNumber(String newValue)
    {        this.setLicenseDocumentNumber(newValue,this.UserSecurityID);

    }
    public String getLicenseDocumentNumber()
    {
        return this.getLicenseDocumentNumber(this.UserSecurityID);
    }

    public void setLicenseDocumentNumber(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='LicenseDocumentNumber' AND TableName='tLicenseRegistration'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {
                   this.AuditVector.addElement("[LicenseDocumentNumber]=["+newValue+"]");
                   this.AuditVector.addElement("[LicenseDocumentNumber]=["+newValue+"]");
                   dbDB.setLicenseDocumentNumber(newValue);
               }
            }
        }
        else
        {
           this.AuditVector.addElement("[LicenseDocumentNumber]=["+newValue+"]");
           this.AuditVector.addElement("[LicenseDocumentNumber]=["+newValue+"]");
           dbDB.setLicenseDocumentNumber(newValue);
         }
    }
    public String getLicenseDocumentNumber(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='LicenseDocumentNumber' AND TableName='tLicenseRegistration'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getLicenseDocumentNumber();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getLicenseDocumentNumber();
         }
        return myVal;
    }

    public void setIssueDate(Date newValue)
    {        this.setIssueDate(newValue,this.UserSecurityID);

    }
    public Date getIssueDate()
    {
        return this.getIssueDate(this.UserSecurityID);
    }

    public void setIssueDate(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='IssueDate' AND TableName='tLicenseRegistration'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {
                   this.AuditVector.addElement("[IssueDate]=["+newValue+"]");
                   this.AuditVector.addElement("[IssueDate]=["+newValue+"]");
                   dbDB.setIssueDate(newValue);
               }
            }
        }
        else
        {
           this.AuditVector.addElement("[IssueDate]=["+newValue+"]");
           this.AuditVector.addElement("[IssueDate]=["+newValue+"]");
           dbDB.setIssueDate(newValue);
         }
    }
    public Date getIssueDate(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='IssueDate' AND TableName='tLicenseRegistration'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getIssueDate();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getIssueDate();
         }
        return myVal;
    }

    public void setExpirationDate(Date newValue)
    {        this.setExpirationDate(newValue,this.UserSecurityID);

    }
    public Date getExpirationDate()
    {
        return this.getExpirationDate(this.UserSecurityID);
    }

    public void setExpirationDate(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ExpirationDate' AND TableName='tLicenseRegistration'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {
                   this.AuditVector.addElement("[ExpirationDate]=["+newValue+"]");
                   this.AuditVector.addElement("[ExpirationDate]=["+newValue+"]");
                   dbDB.setExpirationDate(newValue);
               }
            }
        }
        else
        {
           this.AuditVector.addElement("[ExpirationDate]=["+newValue+"]");
           this.AuditVector.addElement("[ExpirationDate]=["+newValue+"]");
           dbDB.setExpirationDate(newValue);
         }
    }
    public Date getExpirationDate(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ExpirationDate' AND TableName='tLicenseRegistration'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getExpirationDate();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getExpirationDate();
         }
        return myVal;
    }

    public void setName(String newValue)
    {        this.setName(newValue,this.UserSecurityID);

    }
    public String getName()
    {
        return this.getName(this.UserSecurityID);
    }

    public void setName(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Name' AND TableName='tLicenseRegistration'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {
                   this.AuditVector.addElement("[Name]=["+newValue+"]");
                   this.AuditVector.addElement("[Name]=["+newValue+"]");
                   dbDB.setName(newValue);
               }
            }
        }
        else
        {
           this.AuditVector.addElement("[Name]=["+newValue+"]");
           this.AuditVector.addElement("[Name]=["+newValue+"]");
           dbDB.setName(newValue);
         }
    }
    public String getName(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Name' AND TableName='tLicenseRegistration'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getName();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getName();
         }
        return myVal;
    }

    public void setAddress1(String newValue)
    {        this.setAddress1(newValue,this.UserSecurityID);

    }
    public String getAddress1()
    {
        return this.getAddress1(this.UserSecurityID);
    }

    public void setAddress1(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Address1' AND TableName='tLicenseRegistration'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {
                   this.AuditVector.addElement("[Address1]=["+newValue+"]");
                   this.AuditVector.addElement("[Address1]=["+newValue+"]");
                   dbDB.setAddress1(newValue);
               }
            }
        }
        else
        {
           this.AuditVector.addElement("[Address1]=["+newValue+"]");
           this.AuditVector.addElement("[Address1]=["+newValue+"]");
           dbDB.setAddress1(newValue);
         }
    }
    public String getAddress1(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Address1' AND TableName='tLicenseRegistration'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getAddress1();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getAddress1();
         }
        return myVal;
    }

    public void setAddress2(String newValue)
    {        this.setAddress2(newValue,this.UserSecurityID);

    }
    public String getAddress2()
    {
        return this.getAddress2(this.UserSecurityID);
    }

    public void setAddress2(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Address2' AND TableName='tLicenseRegistration'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {
                   this.AuditVector.addElement("[Address2]=["+newValue+"]");
                   this.AuditVector.addElement("[Address2]=["+newValue+"]");
                   dbDB.setAddress2(newValue);
               }
            }
        }
        else
        {
           this.AuditVector.addElement("[Address2]=["+newValue+"]");
           this.AuditVector.addElement("[Address2]=["+newValue+"]");
           dbDB.setAddress2(newValue);
         }
    }
    public String getAddress2(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Address2' AND TableName='tLicenseRegistration'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getAddress2();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getAddress2();
         }
        return myVal;
    }

    public void setCity(String newValue)
    {        this.setCity(newValue,this.UserSecurityID);

    }
    public String getCity()
    {
        return this.getCity(this.UserSecurityID);
    }

    public void setCity(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='City' AND TableName='tLicenseRegistration'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {
                   this.AuditVector.addElement("[City]=["+newValue+"]");
                   this.AuditVector.addElement("[City]=["+newValue+"]");
                   dbDB.setCity(newValue);
               }
            }
        }
        else
        {
           this.AuditVector.addElement("[City]=["+newValue+"]");
           this.AuditVector.addElement("[City]=["+newValue+"]");
           dbDB.setCity(newValue);
         }
    }
    public String getCity(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='City' AND TableName='tLicenseRegistration'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getCity();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getCity();
         }
        return myVal;
    }

    public void setOrgStateID(Integer newValue)
    {        this.setOrgStateID(newValue,this.UserSecurityID);

    }
    public Integer getOrgStateID()
    {
        return this.getOrgStateID(this.UserSecurityID);
    }

    public void setOrgStateID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='OrgStateID' AND TableName='tLicenseRegistration'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {
                   this.AuditVector.addElement("[OrgStateID]=["+newValue+"]");
                   this.AuditVector.addElement("[OrgStateID]=["+newValue+"]");
                   dbDB.setOrgStateID(newValue);
               }
            }
        }
        else
        {
           this.AuditVector.addElement("[OrgStateID]=["+newValue+"]");
           this.AuditVector.addElement("[OrgStateID]=["+newValue+"]");
           dbDB.setOrgStateID(newValue);
         }
    }
    public Integer getOrgStateID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='OrgStateID' AND TableName='tLicenseRegistration'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getOrgStateID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getOrgStateID();
         }
        return myVal;
    }

    public void setProvince(String newValue)
    {        this.setProvince(newValue,this.UserSecurityID);

    }
    public String getProvince()
    {
        return this.getProvince(this.UserSecurityID);
    }

    public void setProvince(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Province' AND TableName='tLicenseRegistration'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {
                   this.AuditVector.addElement("[Province]=["+newValue+"]");
                   this.AuditVector.addElement("[Province]=["+newValue+"]");
                   dbDB.setProvince(newValue);
               }
            }
        }
        else
        {
           this.AuditVector.addElement("[Province]=["+newValue+"]");
           this.AuditVector.addElement("[Province]=["+newValue+"]");
           dbDB.setProvince(newValue);
         }
    }
    public String getProvince(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Province' AND TableName='tLicenseRegistration'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getProvince();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getProvince();
         }
        return myVal;
    }

    public void setZIP(String newValue)
    {        this.setZIP(newValue,this.UserSecurityID);

    }
    public String getZIP()
    {
        return this.getZIP(this.UserSecurityID);
    }

    public void setZIP(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ZIP' AND TableName='tLicenseRegistration'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {
                   this.AuditVector.addElement("[ZIP]=["+newValue+"]");
                   this.AuditVector.addElement("[ZIP]=["+newValue+"]");
                   dbDB.setZIP(newValue);
               }
            }
        }
        else
        {
           this.AuditVector.addElement("[ZIP]=["+newValue+"]");
           this.AuditVector.addElement("[ZIP]=["+newValue+"]");
           dbDB.setZIP(newValue);
         }
    }
    public String getZIP(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ZIP' AND TableName='tLicenseRegistration'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getZIP();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getZIP();
         }
        return myVal;
    }

    public void setCountryID(Integer newValue)
    {        this.setCountryID(newValue,this.UserSecurityID);

    }
    public Integer getCountryID()
    {
        return this.getCountryID(this.UserSecurityID);
    }

    public void setCountryID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CountryID' AND TableName='tLicenseRegistration'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {
                   this.AuditVector.addElement("[CountryID]=["+newValue+"]");
                   this.AuditVector.addElement("[CountryID]=["+newValue+"]");
                   dbDB.setCountryID(newValue);
               }
            }
        }
        else
        {
           this.AuditVector.addElement("[CountryID]=["+newValue+"]");
           this.AuditVector.addElement("[CountryID]=["+newValue+"]");
           dbDB.setCountryID(newValue);
         }
    }
    public Integer getCountryID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CountryID' AND TableName='tLicenseRegistration'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getCountryID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getCountryID();
         }
        return myVal;
    }

    public void setPhone(String newValue)
    {        this.setPhone(newValue,this.UserSecurityID);

    }
    public String getPhone()
    {
        return this.getPhone(this.UserSecurityID);
    }

    public void setPhone(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Phone' AND TableName='tLicenseRegistration'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {
                   this.AuditVector.addElement("[Phone]=["+newValue+"]");
                   this.AuditVector.addElement("[Phone]=["+newValue+"]");
                   dbDB.setPhone(newValue);
               }
            }
        }
        else
        {
           this.AuditVector.addElement("[Phone]=["+newValue+"]");
           this.AuditVector.addElement("[Phone]=["+newValue+"]");
           dbDB.setPhone(newValue);
         }
    }
    public String getPhone(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Phone' AND TableName='tLicenseRegistration'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPhone();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPhone();
         }
        return myVal;
    }

    public void setFax(String newValue)
    {        this.setFax(newValue,this.UserSecurityID);

    }
    public String getFax()
    {
        return this.getFax(this.UserSecurityID);
    }

    public void setFax(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Fax' AND TableName='tLicenseRegistration'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {
                   this.AuditVector.addElement("[Fax]=["+newValue+"]");
                   this.AuditVector.addElement("[Fax]=["+newValue+"]");
                   dbDB.setFax(newValue);
               }
            }
        }
        else
        {
           this.AuditVector.addElement("[Fax]=["+newValue+"]");
           this.AuditVector.addElement("[Fax]=["+newValue+"]");
           dbDB.setFax(newValue);
         }
    }
    public String getFax(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Fax' AND TableName='tLicenseRegistration'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getFax();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getFax();
         }
        return myVal;
    }

    public void setContactName(String newValue)
    {        this.setContactName(newValue,this.UserSecurityID);

    }
    public String getContactName()
    {
        return this.getContactName(this.UserSecurityID);
    }

    public void setContactName(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ContactName' AND TableName='tLicenseRegistration'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {
                   this.AuditVector.addElement("[ContactName]=["+newValue+"]");
                   this.AuditVector.addElement("[ContactName]=["+newValue+"]");
                   dbDB.setContactName(newValue);
               }
            }
        }
        else
        {
           this.AuditVector.addElement("[ContactName]=["+newValue+"]");
           this.AuditVector.addElement("[ContactName]=["+newValue+"]");
           dbDB.setContactName(newValue);
         }
    }
    public String getContactName(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ContactName' AND TableName='tLicenseRegistration'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getContactName();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getContactName();
         }
        return myVal;
    }

    public void setContactEmail(String newValue)
    {        this.setContactEmail(newValue,this.UserSecurityID);

    }
    public String getContactEmail()
    {
        return this.getContactEmail(this.UserSecurityID);
    }

    public void setContactEmail(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ContactEmail' AND TableName='tLicenseRegistration'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {
                   this.AuditVector.addElement("[ContactEmail]=["+newValue+"]");
                   this.AuditVector.addElement("[ContactEmail]=["+newValue+"]");
                   dbDB.setContactEmail(newValue);
               }
            }
        }
        else
        {
           this.AuditVector.addElement("[ContactEmail]=["+newValue+"]");
           this.AuditVector.addElement("[ContactEmail]=["+newValue+"]");
           dbDB.setContactEmail(newValue);
         }
    }
    public String getContactEmail(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ContactEmail' AND TableName='tLicenseRegistration'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getContactEmail();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getContactEmail();
         }
        return myVal;
    }

    public void setDocuLinkID(Integer newValue)
    {        this.setDocuLinkID(newValue,this.UserSecurityID);

    }
    public Integer getDocuLinkID()
    {
        return this.getDocuLinkID(this.UserSecurityID);
    }

    public void setDocuLinkID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='DocuLinkID' AND TableName='tLicenseRegistration'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {
                   this.AuditVector.addElement("[DocuLinkID]=["+newValue+"]");
                   this.AuditVector.addElement("[DocuLinkID]=["+newValue+"]");
                   dbDB.setDocuLinkID(newValue);
               }
            }
        }
        else
        {
           this.AuditVector.addElement("[DocuLinkID]=["+newValue+"]");
           this.AuditVector.addElement("[DocuLinkID]=["+newValue+"]");
           dbDB.setDocuLinkID(newValue);
         }
    }
    public Integer getDocuLinkID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='DocuLinkID' AND TableName='tLicenseRegistration'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getDocuLinkID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getDocuLinkID();
         }
        return myVal;
    }

    public void setComments(String newValue)
    {        this.setComments(newValue,this.UserSecurityID);

    }
    public String getComments()
    {
        return this.getComments(this.UserSecurityID);
    }

    public void setComments(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Comments' AND TableName='tLicenseRegistration'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {
                   this.AuditVector.addElement("[Comments]=["+newValue+"]");
                   this.AuditVector.addElement("[Comments]=["+newValue+"]");
                   dbDB.setComments(newValue);
               }
            }
        }
        else
        {
           this.AuditVector.addElement("[Comments]=["+newValue+"]");
           this.AuditVector.addElement("[Comments]=["+newValue+"]");
           dbDB.setComments(newValue);
         }
    }
    public String getComments(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Comments' AND TableName='tLicenseRegistration'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getComments();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getComments();
         }
        return myVal;
    }

    public void setRequiredFieldList(java.util.Hashtable requiredFieldList)
	 {

        this.requiredFieldList = requiredFieldList;
    }

    public java.util.Hashtable getRequiredFieldList()
    {
        return this.requiredFieldList;
    }
    public void setExpiredFieldList(java.util.Hashtable expiredFieldList)
    {
        this.expiredFieldList = expiredFieldList;
    }

    public void setEnglishFieldList(java.util.Hashtable englishFieldList)
    {
        this.englishFieldList = englishFieldList;
    }

    public java.util.Hashtable getExpiredFieldList()
    {
        return this.expiredFieldList;
    }
    public boolean isExpired(java.util.Date myDate, int daysVector)
    {
       return isExpired(myDate,new java.util.Date(),daysVector);
    }


    public boolean isRead(String fieldN, Integer GroupRefID)
    {
        boolean myVal = false;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='"+fieldN+"' AND TableName='tLicenseRegistration'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==1)
               {
                   myVal = true;
               }
            }
        }
        else
        {
           Integer iSecurityCheck = SecurityCheck.CheckItem("PractitionerSection3", GroupRefID);
           if (iSecurityCheck.intValue()>=1)
           {
                  myVal = true;
           }
        }
        return myVal;
    }


    public boolean isWrite(String fieldN, Integer GroupRefID)
    {
        boolean myVal = false;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='"+fieldN+"' AND TableName='tLicenseRegistration'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {
                   myVal = true;
               }
            }
        }
        else
        {
           Integer iSecurityCheck = SecurityCheck.CheckItem("PractitionerSection3", GroupRefID);
           if (iSecurityCheck.intValue()>=2)
           {
                  myVal = true;
           }
        }
        return myVal;
    }


    public boolean isExpired(java.util.Date myDate, java.util.Date refDate,int daysVector)
    {
        java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat("yyyy-MM-dd");
        boolean myVal = true;
        try
        {
            if (myDate.after(dbdf.parse("1800-01-10")))
            {
                if (myDate.after(dateChange(refDate,daysVector)))
                {
                    myVal=false;
                }
            }
            else
            {
                myVal = false;
            }
        }
        catch (Exception Datee)
        {
        }
        return myVal;
    }

    public java.util.Date dateChange(java.util.Date myD, int dayChange)
    {
        java.util.Calendar calendar = java.util.Calendar.getInstance();
        calendar.setTime(myD);
        calendar.add(java.util.Calendar.DATE, dayChange);
        java.util.Date resultAsDate = new java.util.Date(calendar.getTime().getTime());
        return resultAsDate;
    }

    public boolean isRequired(String fieldN)
    {
        return isRequired(fieldN, new Integer("1"));
    }

    public boolean isRequired(String fieldN, Integer GroupRefID)
    {
        boolean myVal = false;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='"+fieldN+"' AND TableName='tLicenseRegistration'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getRequiredID().intValue()==1)
               {
                   myVal = true;
               }
            }
        }
        else
        {
            if (this.getRequiredFieldList().containsKey(fieldN))
            {
                myVal = true;
            }
        }
        return myVal;
    }
    public boolean isExpiredCheck(String fieldN)
    {
        return isExpiredCheck(fieldN, new Integer ("1"));
    }

    public boolean isExpiredCheck(String fieldN, Integer GroupRefID)
    {
        boolean myVal = false;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='"+fieldN+"' AND TableName='tLicenseRegistration'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getTrackedID().intValue()==1)
               {
                   myVal = true;
               }
            }
        }
        else
        {
            if (this.getExpiredFieldList().containsKey(fieldN))
           {
                myVal = true;
            }
        }
        return myVal;
    }

    public void makeRequiredHash()
    {
       makeRequiredHash(new Integer(1));
    }

    public void makeRequiredHash(Integer GroupRefID)
    {

        java.util.Hashtable newHash = new java.util.Hashtable();
        if (runFieldSecurity)
        {
            java.util.Enumeration myKeys = englishFieldList.keys(); 
            while (myKeys.hasMoreElements()) 
            {
                 String myElement = (String) myKeys.nextElement();
                 bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='"+myElement+"' AND TableName='tLicenseRegistration'" ,"");
                 bltFieldSecurity        working_bltFieldSecurity;
                 ListElement         leCurrentElement;
                 Enumeration eList = myFSL.elements();
                 if (eList.hasMoreElements())
                 {
                     leCurrentElement    = (ListElement) eList.nextElement();
                     working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
                     if (working_bltFieldSecurity.getRequiredID().intValue()==1)
                     {
                         newHash.put(myElement,new Boolean(true));
                     }
                 }
            }
        }
        else
        {
            newHash.put("IsCurrent",new Boolean(true));
            newHash.put("LicenseType",new Boolean(true));
            newHash.put("SubType",new Boolean(true));
            newHash.put("NPDBFOL",new Boolean(true));
            newHash.put("StateID",new Boolean(true));
            newHash.put("IsPractice",new Boolean(true));
            newHash.put("LicenseDocumentNumber",new Boolean(true));
            newHash.put("IssueDate",new Boolean(true));
            newHash.put("ExpirationDate",new Boolean(true));
        }
        this.setRequiredFieldList(newHash);
    }

    public void makeEnglishHash()
    {

        java.util.Hashtable newHash = new java.util.Hashtable();        newHash.put("UniqueCreateDate","Item Create Date");
        newHash.put("UniqueModifyDate","Item Modify Date");
        newHash.put("UniqueModifyComments","Item Modification Comments");
        newHash.put("PhysicianID","PhysicianID");
        newHash.put("IsCurrent","Is this a current license?");
        newHash.put("LicenseType","Type");
        newHash.put("SubType","Sub-Type (or description)");
        newHash.put("NPDBFOL","Field of Licensure");
        newHash.put("StateID","State");
        newHash.put("IsPractice","Do you currently practice in this state?");
        newHash.put("LicenseDocumentNumber","Document Number");
        newHash.put("IssueDate","Issue Date");
        newHash.put("ExpirationDate","Expiration Date");
        newHash.put("Name","Licensing Organization");
        newHash.put("Address1","Address");
        newHash.put("Address2","Address 2");
        newHash.put("City","Institution City");
        newHash.put("OrgStateID","State");
        newHash.put("Province","Non-US Province, District, State");
        newHash.put("ZIP","ZIP");
        newHash.put("CountryID","Country");
        newHash.put("Phone","Phone (XXX-XXX-XXXX)");
        newHash.put("Fax","Fax (XXX-XXX-XXXX)");
        newHash.put("ContactName","Contact Name");
        newHash.put("ContactEmail","Contact E-mail");
        newHash.put("DocuLinkID","DocuLink");
        newHash.put("Comments","Extra Comments");

        this.setEnglishFieldList(newHash);
    }
    public void makeExpiredHash()
    {

        java.util.Hashtable newHash = new java.util.Hashtable();
        newHash.put("ExpirationDate",new Boolean(true));
        this.setExpiredFieldList(newHash);
    }

    public java.util.Hashtable englishFieldList = new java.util.Hashtable();
    public java.util.Hashtable requiredFieldList = new java.util.Hashtable();
    public java.util.Hashtable expiredFieldList = new java.util.Hashtable();

    public boolean isComplete()
    {
	     boolean myVal = true;
	     java.util.Enumeration myKeys = this.getRequiredFieldList().keys(); 
	     while (myKeys.hasMoreElements()) 
	     {
	         String myElement = (String) myKeys.nextElement();
	         if (!isComplete(myElement))
	         {
	             myVal = false;
	         }
	     }
	     return myVal;
    }

    public String getEnglish(String fieldN)
    {
        String myVal = null;
        if (this.englishFieldList.containsKey(fieldN))
        {
            myVal = (String)this.englishFieldList.get(fieldN);
        }
        return myVal;
    }
    public java.util.Vector isComplete(int daysVector)
    {
	     boolean myVal = false;
	     java.util.Enumeration myKeys = this.getExpiredFieldList().keys(); 
	     java.util.Vector myExpiredList = new java.util.Vector(); 
	     while (myKeys.hasMoreElements()) 
	     {
	         String myElement = (String) myKeys.nextElement();
	         if (isExpired(myElement,daysVector))
	         {
	             myVal = true;
	             myExpiredList.addElement(myElement);
	         }
	     }
	     return myExpiredList;
    }

    public void setField(String fieldN, Object fieldV)
    {
     boolean myVal = false;
     try
     {
        java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat("yyyy-MM-dd");

        if (fieldN.equals("LicenseRegistrationID"))
        {
             this.setLicenseRegistrationID((Integer)fieldV);
        }

        else if (fieldN.equals("UniqueCreateDate"))
        {
            this.setUniqueCreateDate((java.util.Date)fieldV);
        }

        else if (fieldN.equals("UniqueModifyDate"))
        {
            this.setUniqueModifyDate((java.util.Date)fieldV);
        }

        else if (fieldN.equals("UniqueModifyComments"))
        {
            this.setUniqueModifyComments((String)fieldV);
        }

        else if (fieldN.equals("PhysicianID"))
        {
             this.setPhysicianID((Integer)fieldV);
        }

        else if (fieldN.equals("IsCurrent"))
        {
             this.setIsCurrent((Integer)fieldV);
        }

        else if (fieldN.equals("LicenseType"))
        {
             this.setLicenseType((Integer)fieldV);
        }

        else if (fieldN.equals("SubType"))
        {
            this.setSubType((String)fieldV);
        }

        else if (fieldN.equals("NPDBFOL"))
        {
             this.setNPDBFOL((Integer)fieldV);
        }

        else if (fieldN.equals("StateID"))
        {
             this.setStateID((Integer)fieldV);
        }

        else if (fieldN.equals("IsPractice"))
        {
             this.setIsPractice((Integer)fieldV);
        }

        else if (fieldN.equals("LicenseDocumentNumber"))
        {
            this.setLicenseDocumentNumber((String)fieldV);
        }

        else if (fieldN.equals("IssueDate"))
        {
            this.setIssueDate((java.util.Date)fieldV);
        }

        else if (fieldN.equals("ExpirationDate"))
        {
            this.setExpirationDate((java.util.Date)fieldV);
        }

        else if (fieldN.equals("Name"))
        {
            this.setName((String)fieldV);
        }

        else if (fieldN.equals("Address1"))
        {
            this.setAddress1((String)fieldV);
        }

        else if (fieldN.equals("Address2"))
        {
            this.setAddress2((String)fieldV);
        }

        else if (fieldN.equals("City"))
        {
            this.setCity((String)fieldV);
        }

        else if (fieldN.equals("OrgStateID"))
        {
             this.setOrgStateID((Integer)fieldV);
        }

        else if (fieldN.equals("Province"))
        {
            this.setProvince((String)fieldV);
        }

        else if (fieldN.equals("ZIP"))
        {
            this.setZIP((String)fieldV);
        }

        else if (fieldN.equals("CountryID"))
        {
             this.setCountryID((Integer)fieldV);
        }

        else if (fieldN.equals("Phone"))
        {
            this.setPhone((String)fieldV);
        }

        else if (fieldN.equals("Fax"))
        {
            this.setFax((String)fieldV);
        }

        else if (fieldN.equals("ContactName"))
        {
            this.setContactName((String)fieldV);
        }

        else if (fieldN.equals("ContactEmail"))
        {
            this.setContactEmail((String)fieldV);
        }

        else if (fieldN.equals("DocuLinkID"))
        {
             this.setDocuLinkID((Integer)fieldV);
        }

        else if (fieldN.equals("Comments"))
        {
            this.setComments((String)fieldV);
        }

	    }
	    catch(Exception e2)
	    {}
	    //return myVal;
	}

    public String getFieldType(String fieldN)
    {
     String  myVal = null;
     try
     {
        java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat("yyyy-MM-dd");

        if (fieldN.equals("LicenseRegistrationID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("UniqueCreateDate"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("UniqueModifyDate"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("UniqueModifyComments"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("PhysicianID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("IsCurrent"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("LicenseType"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("SubType"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("NPDBFOL"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("StateID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("IsPractice"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("LicenseDocumentNumber"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("IssueDate"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("ExpirationDate"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("Name"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("Address1"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("Address2"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("City"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("OrgStateID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("Province"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("ZIP"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("CountryID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("Phone"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("Fax"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("ContactName"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("ContactEmail"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("DocuLinkID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("Comments"))
        {
            myVal = "String";
        }

	    }
	    catch(Exception e2)
	    {}
	    return myVal;
	}

    public boolean isComplete(String fieldN)
    {
	     boolean myVal = false;
	     try
	     {
        java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat("yyyy-MM-dd");

	     if (fieldN.equals("LicenseRegistrationID"))

	     {
                    if (this.getLicenseRegistrationID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("UniqueCreateDate"))

	     {
                    if (this.getUniqueCreateDate().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("UniqueModifyDate"))

	     {
                    if (this.getUniqueModifyDate().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("UniqueModifyComments"))

	     {
                    if (this.getUniqueModifyComments().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("PhysicianID"))

	     {
                    if (this.getPhysicianID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("IsCurrent"))

	     {
                    if (this.getIsCurrent().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("LicenseType"))

	     {
                    if (this.getLicenseType().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("SubType"))

	     {
                    if (this.getSubType().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("NPDBFOL"))

	     {
                    if (this.getNPDBFOL().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("StateID"))

	     {
                    if (this.getStateID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("IsPractice"))

	     {
                    if (this.getIsPractice().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("LicenseDocumentNumber"))

	     {
                    if (this.getLicenseDocumentNumber().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("IssueDate"))

	     {
                    if (this.getIssueDate().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("ExpirationDate"))

	     {
                    if (this.getExpirationDate().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("Name"))

	     {
                    if (this.getName().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("Address1"))

	     {
                    if (this.getAddress1().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("Address2"))

	     {
                    if (this.getAddress2().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("City"))

	     {
                    if (this.getCity().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("OrgStateID"))

	     {
                    if (this.getOrgStateID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("Province"))

	     {
                    if (this.getProvince().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("ZIP"))

	     {
                    if (this.getZIP().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("CountryID"))

	     {
                    if (this.getCountryID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("Phone"))

	     {
                    if (this.getPhone().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("Fax"))

	     {
                    if (this.getFax().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("ContactName"))

	     {
                    if (this.getContactName().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("ContactEmail"))

	     {
                    if (this.getContactEmail().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("DocuLinkID"))

	     {
                    if (this.getDocuLinkID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("Comments"))

	     {
                    if (this.getComments().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	    }
	    catch(Exception e2)
	    {}
	    return myVal;
	}

    public boolean isExpired(String fieldN,int daysVector)
    {
        return isExpired(fieldN,new java.util.Date(),daysVector);
    }

    public boolean isExpired(String fieldN,java.util.Date refDate,int daysVector)
    {
	     boolean myVal = false;
	     try
	     {
        java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat("yyyy-MM-dd");

        if (fieldN.equals("UniqueCreateDate"))

	     {
                    if (this.isExpired(this.getUniqueCreateDate(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

        if (fieldN.equals("UniqueModifyDate"))

	     {
                    if (this.isExpired(this.getUniqueModifyDate(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

        if (fieldN.equals("IssueDate"))

	     {
                    if (this.isExpired(this.getIssueDate(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

        if (fieldN.equals("ExpirationDate"))

	     {
                    if (this.isExpired(this.getExpirationDate(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

	    }
	    catch(Exception e2)
	    {}
	    return myVal;
	}

    public java.util.Date getDateField(String fieldN)
    {
	     java.util.Date myVal = null;
	     try
	     {
        java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat("yyyy-MM-dd");

        if (fieldN.equals("UniqueCreateDate"))

	     {
             myVal = this.getUniqueCreateDate();
        }

        if (fieldN.equals("UniqueModifyDate"))

	     {
             myVal = this.getUniqueModifyDate();
        }

        if (fieldN.equals("IssueDate"))

	     {
             myVal = this.getIssueDate();
        }

        if (fieldN.equals("ExpirationDate"))

	     {
             myVal = this.getExpirationDate();
        }

	    }
	    catch(Exception e2)
	    {}
	    return myVal;
	}
	public boolean runFieldSecurity = false;
	public Integer UserSecurityID = null;

}    // End of bltLicenseRegistration class definition
