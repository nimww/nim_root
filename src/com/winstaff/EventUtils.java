package com.winstaff;
import java.util.Enumeration;
/**
 *
 * @author  Scott Ellis
 * @version 1.00
 */
public class EventUtils
{
    public EventUtils () 
    {
    }   // End of default constructor

    public java.util.Hashtable CompletedStatus = new java.util.Hashtable();
    public java.util.Hashtable ItemAmounts = new java.util.Hashtable();
    public java.util.Hashtable ItemAmountsTotal = new java.util.Hashtable();
    public boolean isCalc = false;
    public Integer iEventMasterID = null;
    public Integer iHCOID = null;
    public Integer iPhysicianID = null;
    




	public static String getMagicURL(Integer iEventChildID)
	{
	    String myVal = "no url found";
	    bltEventChild myEC = new bltEventChild(iEventChildID);
        if (myEC.getCompanyName().indexOf("University of California")!=-1)
        {
            myVal="http://www.ucsd.edu";
        }
	    
	    return myVal;
	}
	















    public void setEventMasterID(Integer iEM)
    {
        this.iEventMasterID = iEM;
    }
    
    public void setHCOID(Integer iEM)
    {
        this.iHCOID = iEM;
    }
    
    public void setPhysicianID(Integer iEM)
    {
        this.iPhysicianID = iEM;
    }

    public static boolean isStandardCredentialingStep(String inS)
    {
        boolean myVal = false;
        if (inS.equalsIgnoreCase("tPhysicianMaster")||inS.equalsIgnoreCase("tAttestR_form")||inS.equalsIgnoreCase("tCoveringPhysicians")||inS.equalsIgnoreCase("tLicenseRegistration")||inS.equalsIgnoreCase("tBoardCertification")||inS.equalsIgnoreCase("tOtherCertification")||inS.equalsIgnoreCase("tProfessionalEducation")||inS.equalsIgnoreCase("tExperience")||inS.equalsIgnoreCase("tWorkHistory")||inS.equalsIgnoreCase("tProfessionalLiability")||inS.equalsIgnoreCase("tFacilityAffiliation")||inS.equalsIgnoreCase("tPeerReference")||inS.equalsIgnoreCase("tProfessionalSociety")||inS.equalsIgnoreCase("tMalpractice")||inS.equalsIgnoreCase("tProfessionalMisconduct")||inS.equalsIgnoreCase("tContinuingEducation")||inS.equalsIgnoreCase("tManagedCarePlan")||inS.equalsIgnoreCase("tAdditionalInformation"))
        {
            myVal = true;
        }
        return myVal;
    }
    
    public  String findNewData(Integer iPhysicianID)
    {
        return this.findNewData(iPhysicianID, new Integer(1));
    }
    
    public static String getButtonCode(String sLabel, String sOnClick)
    {
        String myVal="";
        myVal = "<input  style=\"font-family: Arial; font-size: 10px; background-color: #C0C0C0\"    type=\"Button\" onClick=\""+sOnClick+"\" name=\""+sLabel+"\" value=\""+sLabel+"\">";
        return myVal;
    }
    
    public static String getProcessTaskFileReference(Integer iHCOID, Integer iEventChildID, String TaskLogicReference, String EventType)
    {
        String myVal = "";
		//removed STE 2010-05-10
		/*
        //check for logic 100 url = magic lookup
        if (TaskLogicReference.equalsIgnoreCase("100"))
        {
	        myVal = EventUtils.getMagicURL(iEventChildID);
        }
        else
        {
            bltActivityFileReference_List        bltActivityFileReference_List        =    new    bltActivityFileReference_List(iHCOID,"(EventType='"+EventType+"' OR EventType='All') AND TaskLogicReference='"+TaskLogicReference+"'","");

        //declaration of Enumeration
            bltActivityFileReference        working_bltActivityFileReference;
            ListElement         leCurrentElement;
            Enumeration eList = bltActivityFileReference_List.elements();
            if (eList.hasMoreElements())
            {
                leCurrentElement    = (ListElement) eList.nextElement();
                working_bltActivityFileReference  = (bltActivityFileReference) leCurrentElement.getObject();
                //working_bltActivityFileReference.GroupSecurityInit(UserSecurityGroupID);
   		        myVal = (working_bltActivityFileReference.getFileReference());	
            }
            else 
            {
	            bltActivityFileReference_List        =    new    bltActivityFileReference_List(new Integer(0),"(EventType='"+EventType+"' OR EventType='All') AND TaskLogicReference='"+TaskLogicReference+"'","");
    	        eList = bltActivityFileReference_List.elements();
	            if (eList.hasMoreElements())
	            {
	                leCurrentElement    = (ListElement) eList.nextElement();
	                working_bltActivityFileReference  = (bltActivityFileReference) leCurrentElement.getObject();
	                //working_bltActivityFileReference.GroupSecurityInit(UserSecurityGroupID);
    		        myVal = (working_bltActivityFileReference.getFileReference());	
	            }
	            else 
	            {
	                myVal = ("");

	            }

            }

        }
		*/
        return myVal;
    }





    public static void processActivity(Integer iActivityID,Integer iEventChildID,Integer iEventID,Integer iHCOID, String sKeyword)
    {
        String myVal = "";
		//removed STE 2010-05-10
		/*
		
	    bltActivity        prevA = new bltActivity(iActivityID);
	if (prevA.getActivityType().equalsIgnoreCase("response")&&prevA.getCompleted().intValue()==2)
	{
//if repsonse is no.. make a new activity based on info passed into response.  For now it just makes a phone call.
	    bltActivity        working_bltActivity = new bltActivity();
	    if (prevA.getReferenceID().intValue()!=0)
	    {
		    bltActivity        thirdA = new bltActivity(prevA.getReferenceID());
		    working_bltActivity.setEventChildID(iEventChildID);
		    working_bltActivity.setUniqueCreateDate(new java.util.Date());
		    working_bltActivity.setUniqueModifyDate(new java.util.Date());
		    working_bltActivity.setStartDate(new java.util.Date());
		    working_bltActivity.setCompleted(new Integer(0));

		    if (thirdA.getActivityType().equalsIgnoreCase("word merge"))
		    {
//inster HCO Specific Limits here  for now use generic
			    int letterLimit = 2;
			    int faxLimit = 2;
			    if (thirdA.getTaskLogicReference().equalsIgnoreCase("1"))
			    {
				    working_bltActivity.setActivityType("word merge");
				    working_bltActivity.setTaskLogicReference("2");
//it would be nice to have ActivityType and LogicRef be able to look up and generate the name.
				    working_bltActivity.setName("Send Letter 2");
			    }
			    else if (thirdA.getTaskLogicReference().equalsIgnoreCase("2"))
			    {
				    working_bltActivity.setActivityType("word merge");
				    working_bltActivity.setTaskLogicReference("3");
//it would be nice to have ActivityType and LogicRef be able to look up and generate the name.
				    working_bltActivity.setName("Send Letter 3");
			    }
			    else if (thirdA.getTaskLogicReference().equalsIgnoreCase("3"))
			    {
				    working_bltActivity.setActivityType("phone");
				    working_bltActivity.setTaskLogicReference("1");
				    working_bltActivity.setName("Phone Call Follow up to Send Letter 3");
			    }
			    else if (thirdA.getTaskLogicReference().equalsIgnoreCase("20"))
			    {
				    working_bltActivity.setActivityType("word merge");
				    working_bltActivity.setTaskLogicReference("21");
//it would be nice to have ActivityType and LogicRef be able to look up and generate the name.
				    working_bltActivity.setName("Send Letter 2");
			    }
			    else if (thirdA.getTaskLogicReference().equalsIgnoreCase("21"))
			    {
				    working_bltActivity.setActivityType("word merge");
				    working_bltActivity.setTaskLogicReference("22");
//it would be nice to have ActivityType and LogicRef be able to look up and generate the name.
				    working_bltActivity.setName("Send Letter 3");
			    }
			    else if (thirdA.getTaskLogicReference().equalsIgnoreCase("22"))
			    {
				    working_bltActivity.setActivityType("phone");
				    working_bltActivity.setTaskLogicReference("1");
				    working_bltActivity.setName("Phone Call Follow up to Send Letter 3");
			    }
			    else if (thirdA.getTaskLogicReference().equalsIgnoreCase("10"))
			    {
				    working_bltActivity.setActivityType("word merge");
				    working_bltActivity.setTaskLogicReference("11");
				    working_bltActivity.setName("Send Fax 2");
			    }
			    else if (thirdA.getTaskLogicReference().equalsIgnoreCase("11"))
			    {
				    working_bltActivity.setActivityType("word merge");
				    working_bltActivity.setTaskLogicReference("12");
				    working_bltActivity.setName("Send Fax 2");
			    }
			    else if (thirdA.getTaskLogicReference().equalsIgnoreCase("12"))
			    {
				    working_bltActivity.setActivityType("phone");
				    working_bltActivity.setTaskLogicReference("1");
				    working_bltActivity.setName("Phone Call Follow up to Send Fax 2");
			    }
	            }
		    else
		    {
			    working_bltActivity.setActivityType(thirdA.getActivityType());
			    working_bltActivity.setTaskLogicReference(thirdA.getTaskLogicReference());
			    working_bltActivity.setName("Follow up to: " + thirdA.getName());
	            }

	    }
	    else
	    {
		    working_bltActivity.setEventChildID(iEventChildID);
		    working_bltActivity.setUniqueCreateDate(new java.util.Date());
		    working_bltActivity.setUniqueModifyDate(new java.util.Date());
		    working_bltActivity.setStartDate(new java.util.Date());
		    working_bltActivity.setCompleted(new Integer(0));
		    working_bltActivity.setActivityType("misc");
		    working_bltActivity.setTaskLogicReference("1");
		    working_bltActivity.setName("Follow up to: " + prevA.getName() );
        }

	    working_bltActivity.setReferenceID(iActivityID);
	    try
	    {
            bltEventChild myEC = new bltEventChild(iEventChildID);
            working_bltActivity.setTaskFileReference(getProcessTaskFileReference(iHCOID,iEventChildID,working_bltActivity.getTaskLogicReference(), myEC.getEventType() ));
		    working_bltActivity.commitData();

	    }
	    catch(Exception eeee)
	    {
	    }


	}
	else if (!prevA.getActivityType().equalsIgnoreCase("response"))
	{

	    if (sKeyword.equalsIgnoreCase("add"))
	    {
		    bltActivity        working_bltActivity = new bltActivity();
		    working_bltActivity.setEventChildID(iEventChildID);
		    working_bltActivity.setUniqueCreateDate(new java.util.Date());
		    working_bltActivity.setUniqueModifyDate(new java.util.Date());
		    working_bltActivity.setStartDate(new java.util.Date());
		    working_bltActivity.setActivityType("response");
		    working_bltActivity.setCompleted(new Integer(0));
		    working_bltActivity.setTaskLogicReference("1");
		    working_bltActivity.setName("Response to:" + prevA.getName());
		    working_bltActivity.setReferenceID(iActivityID);
		    try
		    {
			    working_bltActivity.commitData();
		    }
		    catch(Exception eeee)
		    {
		    }
	    }
	    else if (prevA.getCompleted().intValue()==2)
	    {
		    bltActivity        working_bltActivity = new bltActivity();
		    working_bltActivity.setEventChildID(iEventChildID);
		    working_bltActivity.setUniqueCreateDate(new java.util.Date());
		    working_bltActivity.setUniqueModifyDate(new java.util.Date());
		    working_bltActivity.setStartDate(new java.util.Date());
		    working_bltActivity.setCompleted(new Integer(0));
		    working_bltActivity.setReferenceID(iActivityID);
		    working_bltActivity.setActivityType(prevA.getActivityType());
		    working_bltActivity.setTaskLogicReference(prevA.getTaskLogicReference());
		    working_bltActivity.setName("Follow up to: " + prevA.getName());
		    try
		    {
			    working_bltActivity.commitData();
		    }
		    catch(Exception eeee)
		    {
		    }
	    }
	    else if (prevA.getActivityType().equalsIgnoreCase("word merge")||prevA.getActivityType().equalsIgnoreCase("email")||prevA.getActivityType().equalsIgnoreCase("export"))
	    {
		    bltActivity        working_bltActivity = new bltActivity();
		    working_bltActivity.setEventChildID(iEventChildID);
		    working_bltActivity.setUniqueCreateDate(new java.util.Date());
		    working_bltActivity.setUniqueModifyDate(new java.util.Date());
		    working_bltActivity.setStartDate(new java.util.Date());
		    working_bltActivity.setActivityType("response");
		    working_bltActivity.setCompleted(new Integer(0));
		    working_bltActivity.setTaskLogicReference("1");
		    working_bltActivity.setName("Response to: " + prevA.getName());
		    working_bltActivity.setReferenceID(iActivityID);
		    try
		    {
			    working_bltActivity.commitData();
		    }
		    catch(Exception eeee)
		    {
		    }
	    }
	}

*/
		
    }

















    public static String getActivityActionButtons(String ActivityType, String LogicReference)
    {
        String myVal = "<b>Actions:</b><br>";
		//removed STE 2010-05-10
		/*
        myVal +=getButtonCode("Save & Refresh","javascript:document.forms[0].submit()") + "<br>";
        if (ActivityType.equalsIgnoreCase("Word Merge"))
        {
            myVal +=getButtonCode("Run Merge","MM_openBrWindow('tActivity_Merge.jsp','RUNMERGE','status=yes,scrollbars=yes,resizable=yes,width=400,height=300')") + "<br>";
            myVal +=getButtonCode("Close & Exit","MM_goToURL('this','tActivity_form_close.jsp');return document.MM_returnValue") + "<br>";
        }
        else if (ActivityType.equalsIgnoreCase("Response"))
        {
            myVal +=getButtonCode("Response Received","MM_goToURL('this','tActivity_form_close.jsp?sResponse=rec');return document.MM_returnValue") + "<br>";
            myVal +=getButtonCode("No Response, Resend","MM_goToURL('this','tActivity_form_close.jsp?sResponse=norec');return document.MM_returnValue") + "<br>";
        }
        else if (ActivityType.equalsIgnoreCase("Email"))
        {
            myVal +=getButtonCode("Send Email","") + "<br>";
            myVal +=getButtonCode("Close & Exit","MM_goToURL('this','tActivity_form_close.jsp');return document.MM_returnValue") + "<br>";
        }
        else if (ActivityType.equalsIgnoreCase("Misc"))
        {
            myVal +=getButtonCode("Close & Add Response","MM_goToURL('this','tActivity_form_close.jsp?sResponse=add');return document.MM_returnValue") + "<br>";
            myVal +=getButtonCode("Close & Exit","MM_goToURL('this','tActivity_form_close.jsp');return document.MM_returnValue") + "<br>";
        }
        else if (ActivityType.equalsIgnoreCase("phone"))
        {
            myVal +=getButtonCode("Completed Call","MM_goToURL('this','tActivity_form_close.jsp?sResponse=callyes');return document.MM_returnValue") + "<br>";
            myVal +=getButtonCode("Did Not Complete Call","MM_goToURL('this','tActivity_form_close.jsp?sResponse=callno');return document.MM_returnValue") + "<br>";
            myVal +=getButtonCode("Close & Add Response","MM_goToURL('this','tActivity_form_close.jsp?sResponse=add');return document.MM_returnValue") + "<br>";
            myVal +=getButtonCode("Close & Exit","MM_goToURL('this','tActivity_form_close.jsp');return document.MM_returnValue") + "<br>";
        }
        else if (ActivityType.equalsIgnoreCase("URL"))
        {
            myVal +=getButtonCode("Open URL","MM_openBrWindow('tActivity_URL.jsp','OpenURL','scrollbars=yes,resizable=yes,width=400,height=300')") + "<br>";
            myVal +=getButtonCode("Close & Add Response","MM_goToURL('this','tActivity_form_close.jsp?sResponse=add');return document.MM_returnValue") + "<br>";
            myVal +=getButtonCode("Close & Exit","MM_goToURL('this','tActivity_form_close.jsp');return document.MM_returnValue") + "<br>";
        }
        else if (ActivityType.equalsIgnoreCase("Export"))
        {
            myVal +=getButtonCode("Run Export","MM_openBrWindow('tActivity_Export.jsp','RUNEXPORT','scrollbars=yes,resizable=yes,width=400,height=300')") + "<br>";
            myVal +=getButtonCode("Close & Exit","MM_goToURL('this','tActivity_form_close.jsp');return document.MM_returnValue") + "<br>";
        }
        else if (ActivityType.equalsIgnoreCase("Warning"))
        {
            myVal +=getButtonCode("Close Warning","MM_goToURL('this','tActivity_form_close.jsp');return document.MM_returnValue") + "<br>";
            myVal +=getButtonCode("Keep Warning Open","MM_goToURL('this','tEventChild_form.jsp');return document.MM_returnValue") + "<br>";
        }
        else if (ActivityType.equalsIgnoreCase("Scanned Image"))
        {
            myVal +=getButtonCode("Print Document","") + "<br>";
            myVal +=getButtonCode("Close & Exit","MM_goToURL('this','tActivity_form_close.jsp');return document.MM_returnValue") + "<br>";
        }
        myVal +="<br><hr><br>"+getButtonCode("Exit (don't close)","MM_goToURL('this','tEventChild_form.jsp');return document.MM_returnValue") + "<br>";
//        myVal +="<br><hr><br>"+getButtonCode("Add Warning","") + "<br>";
*/
        return myVal;
        
        
    }

    public  String findNewData(Integer iPhysicianID, Integer EventReqID)
    {
        String myVal = "";
        java.util.Vector myV = getEventRequirements(EventReqID.intValue());
        for (int i5=0;i5<myV.size();i5++)
        {
            String myMes = this.findNewData(iPhysicianID,(String)myV.elementAt(i5));
            if (myMes.length()>5)
            {
                //myVal +="\n" + myMes;
                myVal += myMes;
            }
            
        }
        return myVal;
    }
    
    public bltEventChild mergeChildEventWithEventReference(EventReferenceType myEVT) 
    {
        bltEventChild myEVC = new bltEventChild();
	return updateChildEventWithEventReference(myEVC, myEVT);
/*
        myEVC.setName(myEVT.getName());
        myEVC.setAddress1(myEVT.getContactReference().getAddress1());
        myEVC.setAddress2(myEVT.getContactReference().getAddress2());
        myEVC.setCity(myEVT.getContactReference().getCity());
        myEVC.setStateID(myEVT.getContactReference().getStateID());
        myEVC.setProvince(myEVT.getContactReference().getProvince());
        myEVC.setZIP(myEVT.getContactReference().getZIP());
        myEVC.setCountryID(myEVT.getContactReference().getCountryID());
        myEVC.setSummary(myEVT.getSummaryNotes());
        myEVC.setItemRefID(myEVT.getRefID());
        myEVC.setItemRefType(myEVT.getRefIDType());
	myEVC.setCompanyName(myEVT.getContactReference().getCompanyName());
        myEVC.setContactName(myEVT.getContactReference().getName());
        myEVC.setEmail(myEVT.getContactReference().getEmail());
        myEVC.setPhone(myEVT.getContactReference().getPhone());
        myEVC.setFax(myEVT.getContactReference().getFax());
        myEVC.setStartDate(new java.util.Date());
        myEVC.setUniqueCreateDate(new java.util.Date());
        
        myEVC.setMergeGeneric1(myEVT.getMergeGeneric1());
        myEVC.setMergeGeneric2(myEVT.getMergeGeneric2());
        myEVC.setMergeGeneric3(myEVT.getMergeGeneric3());
        myEVC.setMergeGeneric4(myEVT.getMergeGeneric4());
        myEVC.setMergeGeneric5(myEVT.getMergeGeneric5());

	myEVC.setEventType(myEVT.getRefIDType());
        return myEVC;
*/
    }
    
    public bltEventChild updateChildEventWithEventReference(bltEventChild myEVC, EventReferenceType myEVT) 
    {
        myEVC.setName(myEVT.getName());
        myEVC.setAddress1(myEVT.getContactReference().getAddress1());
        myEVC.setAddress2(myEVT.getContactReference().getAddress2());
        myEVC.setCity(myEVT.getContactReference().getCity());
        myEVC.setStateID(myEVT.getContactReference().getStateID());
        myEVC.setProvince(myEVT.getContactReference().getProvince());
        myEVC.setZIP(myEVT.getContactReference().getZIP());
        myEVC.setCountryID(myEVT.getContactReference().getCountryID());
        myEVC.setSummary(myEVT.getSummaryNotes());
        myEVC.setItemRefID(myEVT.getRefID());
        myEVC.setItemRefType(myEVT.getRefIDType());
	myEVC.setCompanyName(myEVT.getContactReference().getCompanyName());
        myEVC.setContactName(myEVT.getContactReference().getName());
        myEVC.setEmail(myEVT.getContactReference().getEmail());
        myEVC.setPhone(myEVT.getContactReference().getPhone());
        myEVC.setFax(myEVT.getContactReference().getFax());
        myEVC.setStartDate(new java.util.Date());
        myEVC.setUniqueCreateDate(new java.util.Date());

        myEVC.setMergeGeneric1(myEVT.getMergeGeneric1());
        myEVC.setMergeGeneric2(myEVT.getMergeGeneric2());
        myEVC.setMergeGeneric3(myEVT.getMergeGeneric3());
        myEVC.setMergeGeneric4(myEVT.getMergeGeneric4());
        myEVC.setMergeGeneric5(myEVT.getMergeGeneric5());
        
	myEVC.setEventType(myEVT.getRefIDType());
        return myEVC;
    }
    
    public String findNewData(Integer iPhysicianID, String sItemRefType)
    {
        String myVal = "";
		//removed STE 2010-05-10
		/*
        String myVal="";
        //this method finds and ADDS elements that do not currently have an event
        java.util.Hashtable myCurrentChildEvents = new java.util.Hashtable();
        bltEventChild_List        bltEventChild_List        =    new    bltEventChild_List(iEventMasterID,"itemRefType='"+sItemRefType+"'","");
        bltEventChild        working_bltEventChild;
        ListElement         leCurrentElement;
        Enumeration eList = bltEventChild_List.elements();
        while (eList.hasMoreElements())
        {
            leCurrentElement    = (ListElement) eList.nextElement();
            working_bltEventChild  = (bltEventChild) leCurrentElement.getObject();
            myCurrentChildEvents.put(working_bltEventChild.getItemRefID(),new Boolean(true));
        }
        Integer iPhysicianID_Local = this.iPhysicianID;
        if (sItemRefType.equalsIgnoreCase("tPhysicianMaster")&&myCurrentChildEvents.isEmpty() )
        {
//***
            EventReferenceType myEVT = this.getEventReference(sItemRefType,this.iPhysicianID);
            bltEventChild myChild = this.mergeChildEventWithEventReference(myEVT);
            myChild.setEventID(this.iEventMasterID);
            try
            {
                myChild.commitData();
                myVal +="\nNew "+ConfigurationMessages.getDataCategory(sItemRefType)+ "Event Record Added";
            }
            catch(Exception e430)
            {
                DebugLogger.println("Error in Event Find All RefType:" + e430);
            }
        }
        if (sItemRefType.equalsIgnoreCase("tAttestR_form")&&myCurrentChildEvents.isEmpty() )
        {
            EventReferenceType myEVT = this.getEventReference(sItemRefType,this.iPhysicianID);
            bltEventChild myChild = this.mergeChildEventWithEventReference(myEVT);
            myChild.setEventID(this.iEventMasterID);
            try
            {
                myChild.commitData();
                myVal +="\nNew Disclosure/Attestation Event Record Added";
            }
            catch(Exception e430)
            {
                DebugLogger.println("Error in Event Find All RefType:" + e430);
            }
        }

        if (sItemRefType.equalsIgnoreCase("tCoveringPhysicians") )
        {
            bltCoveringPhysicians_List        bltCoveringPhysicians_List        =    new    bltCoveringPhysicians_List(iPhysicianID_Local);
            //declaration of Enumeration
            bltCoveringPhysicians        working_bltCoveringPhysicians;
            ListElement         leCurrentElement2;
            Enumeration eList2 = bltCoveringPhysicians_List.elements();
            while (eList2.hasMoreElements())
            {
                leCurrentElement2    = (ListElement) eList2.nextElement();
                working_bltCoveringPhysicians  = (bltCoveringPhysicians) leCurrentElement2.getObject();
                EventReferenceType myEVT = this.getEventReference(sItemRefType,working_bltCoveringPhysicians.getUniqueID());
                bltEventChild myChild = this.mergeChildEventWithEventReference(myEVT);
                myChild.setEventID(this.iEventMasterID);
                try
                {
                    if (!myCurrentChildEvents.containsKey(myChild.getItemRefID())) {myChild.commitData();myVal +="\nNew "+ConfigurationMessages.getDataCategory(sItemRefType)+ " Event Record Added";}
                    
                }
                catch(Exception e430)
                {
                    DebugLogger.println("Error in Event Find All RefType:" + e430);
                }
            }
        }


        if (sItemRefType.equalsIgnoreCase("tLicenseRegistration") )
        {
            bltLicenseRegistration_List        bltLicenseRegistration_List        =    new    bltLicenseRegistration_List(iPhysicianID_Local);
            //declaration of Enumeration
            bltLicenseRegistration        working_bltLicenseRegistration;
            ListElement         leCurrentElement2;
            Enumeration eList2 = bltLicenseRegistration_List.elements();
            while (eList2.hasMoreElements())
            {
                leCurrentElement2    = (ListElement) eList2.nextElement();
                working_bltLicenseRegistration  = (bltLicenseRegistration) leCurrentElement2.getObject();
                EventReferenceType myEVT = this.getEventReference(sItemRefType,working_bltLicenseRegistration.getUniqueID());
                bltEventChild myChild = this.mergeChildEventWithEventReference(myEVT);
                myChild.setEventID(this.iEventMasterID);
                try
                {
                    if (!myCurrentChildEvents.containsKey(myChild.getItemRefID())) {myChild.commitData();myVal +="\nNew "+ConfigurationMessages.getDataCategory(sItemRefType)+ " Event Record Added";}
                    
                }
                catch(Exception e430)
                {
                    DebugLogger.println("Error in Event Find All RefType:" + e430);
                }
            }
        }

        if (sItemRefType.equalsIgnoreCase("tBoardCertification") )
        {
            bltBoardCertification_List        bltBoardCertification_List        =    new    bltBoardCertification_List(iPhysicianID_Local);
            //declaration of Enumeration
            bltBoardCertification        working_bltBoardCertification;
            ListElement         leCurrentElement2;
            Enumeration eList2 = bltBoardCertification_List.elements();
            while (eList2.hasMoreElements())
            {
                leCurrentElement2    = (ListElement) eList2.nextElement();
                working_bltBoardCertification  = (bltBoardCertification) leCurrentElement2.getObject();
                EventReferenceType myEVT = this.getEventReference(sItemRefType,working_bltBoardCertification.getUniqueID());
                bltEventChild myChild = this.mergeChildEventWithEventReference(myEVT);
                myChild.setEventID(this.iEventMasterID);
                try
                {
                    if (!myCurrentChildEvents.containsKey(myChild.getItemRefID())) {myChild.commitData();myVal +="\nNew "+ConfigurationMessages.getDataCategory(sItemRefType)+ " Event Record Added";}
                    
                }
                catch(Exception e430)
                {
                    DebugLogger.println("Error in Event Find All RefType:" + e430);
                }
            }
        }

        if (sItemRefType.equalsIgnoreCase("tOtherCertification") )
        {
            bltOtherCertification_List        bltOtherCertification_List        =    new    bltOtherCertification_List(iPhysicianID_Local);
            //declaration of Enumeration
            bltOtherCertification        working_bltOtherCertification;
            ListElement         leCurrentElement2;
            Enumeration eList2 = bltOtherCertification_List.elements();
            while (eList2.hasMoreElements())
            {
                leCurrentElement2    = (ListElement) eList2.nextElement();
                working_bltOtherCertification  = (bltOtherCertification) leCurrentElement2.getObject();
                EventReferenceType myEVT = this.getEventReference(sItemRefType,working_bltOtherCertification.getUniqueID());
                bltEventChild myChild = this.mergeChildEventWithEventReference(myEVT);
                myChild.setEventID(this.iEventMasterID);
                try
                {
                    if (!myCurrentChildEvents.containsKey(myChild.getItemRefID())) {myChild.commitData();myVal +="\nNew "+ConfigurationMessages.getDataCategory(sItemRefType)+ " Event Record Added";}
                    
                }
                catch(Exception e430)
                {
                    DebugLogger.println("Error in Event Find All RefType:" + e430);
                }
            }
        }

        if (sItemRefType.equalsIgnoreCase("tProfessionalEducation") )
        {
            bltProfessionalEducation_List        bltProfessionalEducation_List        =    new    bltProfessionalEducation_List(iPhysicianID_Local);
            //declaration of Enumeration
            bltProfessionalEducation        working_bltProfessionalEducation;
            ListElement         leCurrentElement2;
            Enumeration eList2 = bltProfessionalEducation_List.elements();
            while (eList2.hasMoreElements())
            {
                leCurrentElement2    = (ListElement) eList2.nextElement();
                working_bltProfessionalEducation  = (bltProfessionalEducation) leCurrentElement2.getObject();
                EventReferenceType myEVT = this.getEventReference(sItemRefType,working_bltProfessionalEducation.getUniqueID());
                bltEventChild myChild = this.mergeChildEventWithEventReference(myEVT);
                myChild.setEventID(this.iEventMasterID);
                try
                {
                    if (!myCurrentChildEvents.containsKey(myChild.getItemRefID())) {myChild.commitData();myVal +="\nNew "+ConfigurationMessages.getDataCategory(sItemRefType)+ " Event Record Added";}
                    
                }
                catch(Exception e430)
                {
                    DebugLogger.println("Error in Event Find All RefType:" + e430);
                }
            }
        }

        if (sItemRefType.equalsIgnoreCase("tExperience") )
        {
            bltExperience_List        bltExperience_List        =    new    bltExperience_List(iPhysicianID_Local);
            //declaration of Enumeration
            bltExperience        working_bltExperience;
            ListElement         leCurrentElement2;
            Enumeration eList2 = bltExperience_List.elements();
            while (eList2.hasMoreElements())
            {
                leCurrentElement2    = (ListElement) eList2.nextElement();
                working_bltExperience  = (bltExperience) leCurrentElement2.getObject();
                EventReferenceType myEVT = this.getEventReference(sItemRefType,working_bltExperience.getUniqueID());
                bltEventChild myChild = this.mergeChildEventWithEventReference(myEVT);
                myChild.setEventID(this.iEventMasterID);
                try
                {
                    if (!myCurrentChildEvents.containsKey(myChild.getItemRefID())) {myChild.commitData();myVal +="\nNew "+ConfigurationMessages.getDataCategory(sItemRefType)+ " Event Record Added";}
                    
                }
                catch(Exception e430)
                {
                    DebugLogger.println("Error in Event Find All RefType:" + e430);
                }
            }
        }

        if (sItemRefType.equalsIgnoreCase("tWorkHistory") )
        {
            bltWorkHistory_List        bltWorkHistory_List        =    new    bltWorkHistory_List(iPhysicianID_Local);
            //declaration of Enumeration
            bltWorkHistory        working_bltWorkHistory;
            ListElement         leCurrentElement2;
            Enumeration eList2 = bltWorkHistory_List.elements();
            while (eList2.hasMoreElements())
            {
                leCurrentElement2    = (ListElement) eList2.nextElement();
                working_bltWorkHistory  = (bltWorkHistory) leCurrentElement2.getObject();
                EventReferenceType myEVT = this.getEventReference(sItemRefType,working_bltWorkHistory.getUniqueID());
                bltEventChild myChild = this.mergeChildEventWithEventReference(myEVT);
                myChild.setEventID(this.iEventMasterID);
                try
                {
                    if (!myCurrentChildEvents.containsKey(myChild.getItemRefID())) {myChild.commitData();myVal +="\nNew "+ConfigurationMessages.getDataCategory(sItemRefType)+ " Event Record Added";}
                    
                }
                catch(Exception e430)
                {
                    DebugLogger.println("Error in Event Find All RefType:" + e430);
                }
            }
        }

        if (sItemRefType.equalsIgnoreCase("tProfessionalLiability") )
        {
            bltProfessionalLiability_List        bltProfessionalLiability_List        =    new    bltProfessionalLiability_List(iPhysicianID_Local);
            //declaration of Enumeration
            bltProfessionalLiability        working_bltProfessionalLiability;
            ListElement         leCurrentElement2;
            Enumeration eList2 = bltProfessionalLiability_List.elements();
            while (eList2.hasMoreElements())
            {
                leCurrentElement2    = (ListElement) eList2.nextElement();
                working_bltProfessionalLiability  = (bltProfessionalLiability) leCurrentElement2.getObject();
                EventReferenceType myEVT = this.getEventReference(sItemRefType,working_bltProfessionalLiability.getUniqueID());
                bltEventChild myChild = this.mergeChildEventWithEventReference(myEVT);
                myChild.setEventID(this.iEventMasterID);
                try
                {
                    if (!myCurrentChildEvents.containsKey(myChild.getItemRefID())) {myChild.commitData();myVal +="\nNew "+ConfigurationMessages.getDataCategory(sItemRefType)+ " Event Record Added";}
                    
                }
                catch(Exception e430)
                {
                    DebugLogger.println("Error in Event Find All RefType:" + e430);
                }
            }
        }

        if (sItemRefType.equalsIgnoreCase("tFacilityAffiliation") )
        {
            bltFacilityAffiliation_List        bltFacilityAffiliation_List        =    new    bltFacilityAffiliation_List(iPhysicianID_Local);
            //declaration of Enumeration
            bltFacilityAffiliation        working_bltFacilityAffiliation;
            ListElement         leCurrentElement2;
            Enumeration eList2 = bltFacilityAffiliation_List.elements();
            while (eList2.hasMoreElements())
            {
                leCurrentElement2    = (ListElement) eList2.nextElement();
                working_bltFacilityAffiliation  = (bltFacilityAffiliation) leCurrentElement2.getObject();
                EventReferenceType myEVT = this.getEventReference(sItemRefType,working_bltFacilityAffiliation.getUniqueID());
                bltEventChild myChild = this.mergeChildEventWithEventReference(myEVT);
                myChild.setEventID(this.iEventMasterID);
                try
                {
                    if (!myCurrentChildEvents.containsKey(myChild.getItemRefID())) {myChild.commitData();myVal +="\nNew "+ConfigurationMessages.getDataCategory(sItemRefType)+ " Event Record Added";}
                    
                }
                catch(Exception e430)
                {
                    DebugLogger.println("Error in Event Find All RefType:" + e430);
                }
            }
        }

        if (sItemRefType.equalsIgnoreCase("tPeerReference") )
        {
            bltPeerReference_List        bltPeerReference_List        =    new    bltPeerReference_List(iPhysicianID_Local);
            //declaration of Enumeration
            bltPeerReference        working_bltPeerReference;
            ListElement         leCurrentElement2;
            Enumeration eList2 = bltPeerReference_List.elements();
            while (eList2.hasMoreElements())
            {
                leCurrentElement2    = (ListElement) eList2.nextElement();
                working_bltPeerReference  = (bltPeerReference) leCurrentElement2.getObject();
                EventReferenceType myEVT = this.getEventReference(sItemRefType,working_bltPeerReference.getUniqueID());
                bltEventChild myChild = this.mergeChildEventWithEventReference(myEVT);
                myChild.setEventID(this.iEventMasterID);
                try
                {
                    if (!myCurrentChildEvents.containsKey(myChild.getItemRefID())) {myChild.commitData();myVal +="\nNew "+ConfigurationMessages.getDataCategory(sItemRefType)+ " Event Record Added";}
                    
                }
                catch(Exception e430)
                {
                    DebugLogger.println("Error in Event Find All RefType:" + e430);
                }
            }
        }

        if (sItemRefType.equalsIgnoreCase("tProfessionalSociety") )
        {
            bltProfessionalSociety_List        bltProfessionalSociety_List        =    new    bltProfessionalSociety_List(iPhysicianID_Local);
            //declaration of Enumeration
            bltProfessionalSociety        working_bltProfessionalSociety;
            ListElement         leCurrentElement2;
            Enumeration eList2 = bltProfessionalSociety_List.elements();
            while (eList2.hasMoreElements())
            {
                leCurrentElement2    = (ListElement) eList2.nextElement();
                working_bltProfessionalSociety  = (bltProfessionalSociety) leCurrentElement2.getObject();
                EventReferenceType myEVT = this.getEventReference(sItemRefType,working_bltProfessionalSociety.getUniqueID());
                bltEventChild myChild = this.mergeChildEventWithEventReference(myEVT);
                myChild.setEventID(this.iEventMasterID);
                try
                {
                    if (!myCurrentChildEvents.containsKey(myChild.getItemRefID())) {myChild.commitData();myVal +="\nNew "+ConfigurationMessages.getDataCategory(sItemRefType)+ " Event Record Added";}
                    
                }
                catch(Exception e430)
                {
                    DebugLogger.println("Error in Event Find All RefType:" + e430);
                }
            }
        }

        if (sItemRefType.equalsIgnoreCase("tMalpractice") )
        {
            bltMalpractice_List        bltMalpractice_List        =    new    bltMalpractice_List(iPhysicianID_Local);
            //declaration of Enumeration
            bltMalpractice        working_bltMalpractice;
            ListElement         leCurrentElement2;
            Enumeration eList2 = bltMalpractice_List.elements();
            while (eList2.hasMoreElements())
            {
                leCurrentElement2    = (ListElement) eList2.nextElement();
                working_bltMalpractice  = (bltMalpractice) leCurrentElement2.getObject();
                EventReferenceType myEVT = this.getEventReference(sItemRefType,working_bltMalpractice.getUniqueID());
                bltEventChild myChild = this.mergeChildEventWithEventReference(myEVT);
                myChild.setEventID(this.iEventMasterID);
                try
                {
                    if (!myCurrentChildEvents.containsKey(myChild.getItemRefID())) {myChild.commitData();myVal +="\nNew "+ConfigurationMessages.getDataCategory(sItemRefType)+ " Event Record Added";}
                    
                }
                catch(Exception e430)
                {
                    DebugLogger.println("Error in Event Find All RefType:" + e430);
                }
            }
        }

        if (sItemRefType.equalsIgnoreCase("tProfessionalMisconduct") )
        {
            bltProfessionalMisconduct_List        bltProfessionalMisconduct_List        =    new    bltProfessionalMisconduct_List(iPhysicianID_Local);
            //declaration of Enumeration
            bltProfessionalMisconduct        working_bltProfessionalMisconduct;
            ListElement         leCurrentElement2;
            Enumeration eList2 = bltProfessionalMisconduct_List.elements();
            while (eList2.hasMoreElements())
            {
                leCurrentElement2    = (ListElement) eList2.nextElement();
                working_bltProfessionalMisconduct  = (bltProfessionalMisconduct) leCurrentElement2.getObject();
                EventReferenceType myEVT = this.getEventReference(sItemRefType,working_bltProfessionalMisconduct.getUniqueID());
                bltEventChild myChild = this.mergeChildEventWithEventReference(myEVT);
                myChild.setEventID(this.iEventMasterID);
                try
                {
                    if (!myCurrentChildEvents.containsKey(myChild.getItemRefID())) {myChild.commitData();myVal +="\nNew "+ConfigurationMessages.getDataCategory(sItemRefType)+ " Event Record Added";}
                    
                }
                catch(Exception e430)
                {
                    DebugLogger.println("Error in Event Find All RefType:" + e430);
                }
            }
        }

        if (sItemRefType.equalsIgnoreCase("tContinuingEducation") )
        {
            bltContinuingEducation_List        bltContinuingEducation_List        =    new    bltContinuingEducation_List(iPhysicianID_Local);
            //declaration of Enumeration
            bltContinuingEducation        working_bltContinuingEducation;
            ListElement         leCurrentElement2;
            Enumeration eList2 = bltContinuingEducation_List.elements();
            while (eList2.hasMoreElements())
            {
                leCurrentElement2    = (ListElement) eList2.nextElement();
                working_bltContinuingEducation  = (bltContinuingEducation) leCurrentElement2.getObject();
                EventReferenceType myEVT = this.getEventReference(sItemRefType,working_bltContinuingEducation.getUniqueID());
                bltEventChild myChild = this.mergeChildEventWithEventReference(myEVT);
                myChild.setEventID(this.iEventMasterID);
                try
                {
                    if (!myCurrentChildEvents.containsKey(myChild.getItemRefID())) {myChild.commitData();myVal +="\nNew "+ConfigurationMessages.getDataCategory(sItemRefType)+ " Event Record Added";}
                    
                }
                catch(Exception e430)
                {
                    DebugLogger.println("Error in Event Find All RefType:" + e430);
                }
            }
        }

        if (sItemRefType.equalsIgnoreCase("tManagedCarePlan") )
        {
            bltManagedCarePlan_List        bltManagedCarePlan_List        =    new    bltManagedCarePlan_List(iPhysicianID_Local);
            //declaration of Enumeration
            bltManagedCarePlan        working_bltManagedCarePlan;
            ListElement         leCurrentElement2;
            Enumeration eList2 = bltManagedCarePlan_List.elements();
            while (eList2.hasMoreElements())
            {
                leCurrentElement2    = (ListElement) eList2.nextElement();
                working_bltManagedCarePlan  = (bltManagedCarePlan) leCurrentElement2.getObject();
                EventReferenceType myEVT = this.getEventReference(sItemRefType,working_bltManagedCarePlan.getUniqueID());
                bltEventChild myChild = this.mergeChildEventWithEventReference(myEVT);
                myChild.setEventID(this.iEventMasterID);
                try
                {
                    if (!myCurrentChildEvents.containsKey(myChild.getItemRefID())) {myChild.commitData();myVal +="\nNew "+ConfigurationMessages.getDataCategory(sItemRefType)+ " Event Record Added";}
                    
                }
                catch(Exception e430)
                {
                    DebugLogger.println("Error in Event Find All RefType:" + e430);
                }
            }
        }

        if (sItemRefType.equalsIgnoreCase("tAdditionalInformation") )
        {
            bltAdditionalInformation_List        bltAdditionalInformation_List        =    new    bltAdditionalInformation_List(iPhysicianID_Local);
            //declaration of Enumeration
            bltAdditionalInformation        working_bltAdditionalInformation;
            ListElement         leCurrentElement2;
            Enumeration eList2 = bltAdditionalInformation_List.elements();
            while (eList2.hasMoreElements())
            {
                leCurrentElement2    = (ListElement) eList2.nextElement();
                working_bltAdditionalInformation  = (bltAdditionalInformation) leCurrentElement2.getObject();
                EventReferenceType myEVT = this.getEventReference(sItemRefType,working_bltAdditionalInformation.getUniqueID());
                bltEventChild myChild = this.mergeChildEventWithEventReference(myEVT);
                myChild.setEventID(this.iEventMasterID);
                try
                {
                    if (!myCurrentChildEvents.containsKey(myChild.getItemRefID())) {myChild.commitData();myVal +="\nNew "+ConfigurationMessages.getDataCategory(sItemRefType)+ " Event Record Added";}
                    
                }
                catch(Exception e430)
                {
                    DebugLogger.println("Error in Event Find All RefType:" + e430);
                }
            }
        }
        
        return myVal;
    }
    
    public boolean isNewReferenceInformation(bltEventChild myEC)
    {
        boolean myVal = false;
        EventReferenceType myERT = this.getEventReference(myEC.getItemRefType(), myEC.getItemRefID());
        if (!myEC.getSummary().equalsIgnoreCase(myERT.getSummaryNotes()))
        {
            myVal = true;
        }
        else if (!myEC.getContactName().equalsIgnoreCase(myERT.getContactReference().getName()))
        {
            myVal = true;
        }
        else if (!myEC.getAddress1().equalsIgnoreCase(myERT.getContactReference().getAddress1() ))
        {
            myVal = true;
        }
        else if (!myEC.getAddress2().equalsIgnoreCase(myERT.getContactReference().getAddress2() ))
        {
            myVal = true;
        }
        else if (!myEC.getCity().equalsIgnoreCase(myERT.getContactReference().getCity() ))
        {
            myVal = true;
        }
        else if (myEC.getStateID().intValue()!=myERT.getContactReference().getStateID().intValue())
        {
            myVal = true;
        }
        else if (!myEC.getZIP().equalsIgnoreCase(myERT.getContactReference().getZIP() ))
        {
            myVal = true;
        }
        else if (!myEC.getProvince().equalsIgnoreCase(myERT.getContactReference().getProvince() ))
        {
            myVal = true;
        }
        else if (myEC.getCountryID().intValue()!=myERT.getContactReference().getCountryID().intValue())
        {
            myVal = true;
        }
        else if (!myEC.getEmail().equalsIgnoreCase(myERT.getContactReference().getEmail() ))
        {
            myVal = true;
        }
        else if (!myEC.getFax().equalsIgnoreCase(myERT.getContactReference().getFax() ))
        {
            myVal = true;
        }
        else if (!myEC.getPhone().equalsIgnoreCase(myERT.getContactReference().getPhone() ))
        {
            myVal = true;
        }
*/
return myVal;
    }

    
    public void runCalc()
    {
        this.resetCompletedStatusAllTrue();
        //declaration of Enumeration
        bltEventChild_List        bltEventChild_List        =    new    bltEventChild_List(iEventMasterID);
        bltEventChild        working_bltEventChild;
        ListElement         leCurrentElement;
        Enumeration eList = bltEventChild_List.elements();
        while (eList.hasMoreElements())
        {
            leCurrentElement    = (ListElement) eList.nextElement();
            working_bltEventChild  = (bltEventChild) leCurrentElement.getObject();
            if (!working_bltEventChild.isComplete())
            {
                this.CompletedStatus.put(working_bltEventChild.getItemRefType(), new Boolean(false));
                Integer newCount = new Integer( ((Integer)this.ItemAmounts.get(working_bltEventChild.getItemRefType())).intValue()+1 );
                this.ItemAmounts.put(working_bltEventChild.getItemRefType(),newCount);
            }
            Integer newCount2 = new Integer( ((Integer)this.ItemAmountsTotal.get(working_bltEventChild.getItemRefType())).intValue()+1 );
            this.ItemAmountsTotal.put(working_bltEventChild.getItemRefType(),newCount2);
        }
        
        this.isCalc = true;
    }


    public  java.util.Hashtable getCompletedStatus()
    {
        if (!isCalc)
        {
            this.runCalc();
        }
        return this.CompletedStatus;
    }


    public  java.util.Hashtable getItemAmounts()
    {
        if (!isCalc)
        {
            this.runCalc();
        }
        return this.ItemAmounts;
    }

    public  java.util.Hashtable getItemAmountsTotal()
    {
        if (!isCalc)
        {
            this.runCalc();
        }
        return this.ItemAmountsTotal;
    }

    public Integer getItemAmount(String itemRefType)
    {
        Integer myVal = null;
        if (isCalc)
        {
            myVal = (Integer)this.ItemAmounts.get(itemRefType);
        }
        else
        {
            myVal = new Integer(0);
            //declaration of Enumeration
            bltEventChild_List        bltEventChild_List        =    new    bltEventChild_List(iEventMasterID,"itemRefType='"+itemRefType+"'","");
            bltEventChild        working_bltEventChild;
            ListElement         leCurrentElement;
            Enumeration eList = bltEventChild_List.elements();
            int cnt=0;
            while (eList.hasMoreElements())
            {
                leCurrentElement    = (ListElement) eList.nextElement();
                working_bltEventChild  = (bltEventChild) leCurrentElement.getObject();
                cnt++;
            }
            myVal = new Integer(cnt);
        }
        return myVal;
    }


    public Integer getItemAmountTotal(String itemRefType)
    {
        Integer myVal = null;
        if (isCalc)
        {
            myVal = (Integer)this.ItemAmountsTotal.get(itemRefType);
        }
        else
        {
            myVal = new Integer(0);
            //declaration of Enumeration
            bltEventChild_List        bltEventChild_List        =    new    bltEventChild_List(iEventMasterID,"itemRefType='"+itemRefType+"'","");
            bltEventChild        working_bltEventChild;
            ListElement         leCurrentElement;
            Enumeration eList = bltEventChild_List.elements();
            int cnt=0;
            while (eList.hasMoreElements())
            {
                leCurrentElement    = (ListElement) eList.nextElement();
                working_bltEventChild  = (bltEventChild) leCurrentElement.getObject();
                cnt++;
            }
            myVal = new Integer(cnt);
        }
        return myVal;
    }



    public Boolean getCompletedStatus(String itemRefType)
    {
        Boolean myVal = null;
        if (isCalc)
        {
            myVal = (Boolean)this.CompletedStatus.get(itemRefType);
        }
        else
        {
            myVal = new Boolean(true);
            //declaration of Enumeration
            bltEventChild_List        bltEventChild_List        =    new    bltEventChild_List(iEventMasterID,"itemRefType='"+itemRefType+"'","");
            bltEventChild        working_bltEventChild;
            ListElement         leCurrentElement;
            Enumeration eList = bltEventChild_List.elements();
            while (eList.hasMoreElements())
            {
                leCurrentElement    = (ListElement) eList.nextElement();
                working_bltEventChild  = (bltEventChild) leCurrentElement.getObject();
                if (!working_bltEventChild.isComplete())
                {
                    myVal = new Boolean(false);
                }
            }
        }
        return myVal;
    }

    public Boolean getOverallStatus()
    {
        Boolean myVal = new Boolean (true);
        if (this.isCalc)
        {
            java.util.Vector myV = getEventRequirements(1);
            for (int i5=0;i5<myV.size();i5++)
            {
                if (    !((Boolean)this.CompletedStatus.get((String)myV.elementAt(i5))).booleanValue()   )
                {
                    myVal = new Boolean(false);
                }
            }
        }
        else
        {
            myVal = new Boolean(false);
        }
        return myVal;
    }


    public void resetCompletedStatusAllTrue()
    {
        this.isCalc = false;
        this.CompletedStatus = new java.util.Hashtable();
        java.util.Vector myV = getEventRequirements(1);
        for (int i5=0;i5<myV.size();i5++)
        {
            this.CompletedStatus.put((String)myV.elementAt(i5), new Boolean(true));
            this.ItemAmounts.put((String)myV.elementAt(i5), new Integer(0));
            this.ItemAmountsTotal.put((String)myV.elementAt(i5), new Integer(0));
        }
    }
    
    public static java.util.Vector getEventRequirements(int EventReqID)
    {
        java.util.Vector myVal = new java.util.Vector();
        if (EventReqID==1)
        {
            myVal.addElement("tPhysicianMaster");
            //myVal.addElement("tCoveringPhysicians");
//            myVal.addElement("tAttestR_form");
            myVal.addElement("tLicenseRegistration");
            myVal.addElement("tBoardCertification");
            myVal.addElement("tOtherCertification");
            myVal.addElement("tProfessionalEducation");
            myVal.addElement("tExperience");
            myVal.addElement("tWorkHistory");
            myVal.addElement("tProfessionalLiability");
            myVal.addElement("tFacilityAffiliation");
            myVal.addElement("tPeerReference");
            myVal.addElement("tProfessionalSociety");
            myVal.addElement("tMalpractice");
            myVal.addElement("tProfessionalMisconduct");
            myVal.addElement("tContinuingEducation");
            myVal.addElement("tAttestR_form");
            //myVal.addElement("tManagedCarePlan");
            //myVal.addElement("tAdditionalInformation");
            myVal.addElement("Misc");
        }
        else if (EventReqID==2)
        {
            myVal.addElement("tPhysicianMaster");
            //myVal.addElement("tCoveringPhysicians");
            myVal.addElement("tLicenseRegistration");
            myVal.addElement("tBoardCertification");
            myVal.addElement("tOtherCertification");
            //myVal.addElement("tProfessionalEducation");
            //myVal.addElement("tExperience");
            //myVal.addElement("tWorkHistory");
            myVal.addElement("tProfessionalLiability");
            myVal.addElement("tFacilityAffiliation");
            //myVal.addElement("tPeerReference");
            //myVal.addElement("tProfessionalSociety");
            myVal.addElement("tMalpractice");
            myVal.addElement("tProfessionalMisconduct");
            myVal.addElement("tContinuingEducation");
            myVal.addElement("tAttestR_form");
            //myVal.addElement("tManagedCarePlan");
            //myVal.addElement("tAdditionalInformation");
            myVal.addElement("Misc");
        }
        return myVal;
    }




	public static int getType(String theType)
	{
	    int myType = -1;
        if (theType.equalsIgnoreCase("tBoardCertification"))
	    {
	        myType = TYPE_tBoardCertification;
	    }
        else if (theType.equalsIgnoreCase("tAttestR_form"))
	    {
	        myType = TYPE_tAttestR_form;
	    }
        else if (theType.equalsIgnoreCase("tPhysicianMaster"))
	    {
	        myType = TYPE_tPhysicianMaster;
	    }
        else if (theType.equalsIgnoreCase("tCoveringPhysicians"))
	    {
	        myType = TYPE_tCoveringPhysicians;
	    }
        else if (theType.equalsIgnoreCase("tAdditionalInformation"))
	    {
	        myType = TYPE_tAdditionalInformation;
	    }
        else if (theType.equalsIgnoreCase("tContinuingEducation"))
	    {
	        myType = TYPE_tContinuingEducation;
	    }
        else if (theType.equalsIgnoreCase("tExperience"))
	    {
	        myType = TYPE_tExperience;
	    }
        else if (theType.equalsIgnoreCase("tFacilityAffiliation"))
	    {
	        myType = TYPE_tFacilityAffiliation;
	    }
        else if (theType.equalsIgnoreCase("tLicenseRegistration"))
	    {
	        myType = TYPE_tLicenseRegistration;
	    }
        else if (theType.equalsIgnoreCase("tMalpractice"))
	    {
	        myType = TYPE_tMalpractice;
	    }
        else if (theType.equalsIgnoreCase("tManagedCarePlan"))
	    {
	        myType = TYPE_tManagedCarePlan;
	    }
        else if (theType.equalsIgnoreCase("tOtherCertification"))
	    {
	        myType = TYPE_tOtherCertification;
	    }
        else if (theType.equalsIgnoreCase("tPeerReference"))
	    {
	        myType = TYPE_tPeerReference;
	    }
        else if (theType.equalsIgnoreCase("tProfessionalEducation"))
	    {
	        myType = TYPE_tProfessionalEducation;
	    }
        else if (theType.equalsIgnoreCase("tProfessionalLiability"))
	    {
	        myType = TYPE_tProfessionalLiability;
	    }
        else if (theType.equalsIgnoreCase("tProfessionalMisconduct"))
	    {
	        myType = TYPE_tProfessionalMisconduct;
	    }
        else if (theType.equalsIgnoreCase("tProfessionalSociety"))
	    {
	        myType = TYPE_tProfessionalSociety;
	    }
        else if (theType.equalsIgnoreCase("tWorkHistory"))
	    {
	        myType = TYPE_tWorkHistory;
	    }
	    return (myType);
	}
    

        
        
        
        
        

    
    
    public static EventReferenceType getEventReference(String typeCode, Integer UniqueID)
    {
        EventReferenceType myVal = new EventReferenceType();
		//removed STE 2010-05-10
		/*
        myVal.setRefIDType(typeCode);
	    myVal.setRefID(UniqueID);
        ContactReferenceType myCon = new ContactReferenceType();
	    int theType = getType(typeCode);
	    if (theType == TYPE_tAdditionalInformation)
	    {
		    bltAdditionalInformation myClass = new bltAdditionalInformation(UniqueID);
		    myVal.setLastModifiedDate(myClass.getUniqueModifyDate());
		    myVal.setLastModifiedUser(myClass.getUniqueModifyComments());
		    myVal.setName(ConfigurationMessages.getDataCategory( typeCode));
		    
		    myVal.setContactReference(myCon);
		    
		    myVal.setSummaryNotes(myClass.getSubject()+"\n"+myClass.getNoteContent());
	    }
	    else if (theType == TYPE_tAttestR_form)
	    {
 	        String mySummary = "";
    		bltAttestR_List        bltAttestR_List        =    new    bltAttestR_List(UniqueID);
		bltPhysicianMaster myClass = new bltPhysicianMaster(UniqueID);
	        bltAttestR        working_bltAttestR;
                ListElement         leCurrentElement;
                Enumeration eList = bltAttestR_List.elements();
		int qCNT = 0;
	        if (eList.hasMoreElements())
                {
		     mySummary += "Disclosure Question Summary:\n";
		     mySummary += "----------------------------\n";
                     while (eList.hasMoreElements())
		     {
                        qCNT++;
			leCurrentElement    = (ListElement) eList.nextElement();
			working_bltAttestR  = (bltAttestR) leCurrentElement.getObject();
			mySummary += "Q." + working_bltAttestR.getQuestionID() + " - [" + formPop_Base.integer2string( working_bltAttestR.getAnswer() ,"tYesNoLILong") + "]\n";
		     }
		}
		else
		{
		     mySummary += "No Disclosure Questions Answered\n";
		     mySummary += "--------------------------------\n";
		}
		    mySummary += "-------------------\n";
 	            mySummary += "Attestation Summary\n";
		    mySummary += "-------------------\n";
		    mySummary += "Attested: "+ formPop_Base.integer2string( myClass.getIsAttested() ,"tYesNoLILong")+"\n";
		    mySummary += "Attest Date: "+ formPop_Base.date2string(myClass.getAttestDate())+"\n";

		    myVal.setLastModifiedDate(myClass.getUniqueModifyDate());
		    myVal.setLastModifiedUser(myClass.getUniqueModifyComments());
		    myVal.setName("Disclosure/Attestation for: "+myClass.getFirstName() + " " + myClass.getMiddleName() + " " + myClass.getLastName() + " " + myClass.getSuffix());

		    myCon.setName(myClass.getFirstName() + " " + myClass.getMiddleName() + " " + myClass.getLastName() + " " + myClass.getSuffix());
		    myCon.setAddress1(myClass.getHomeAddress1());
		    myCon.setAddress2(myClass.getHomeAddress2());
		    myCon.setCity(myClass.getHomeCity());
		    myCon.setStateID(myClass.getHomeStateID());
		    myCon.setProvince(myClass.getHomeProvince());
		    myCon.setZIP(myClass.getHomeZIP());
		    myCon.setCountryID(myClass.getHomeCountryID());
		    myCon.setEmail(myClass.getHomeEmail());
		    myCon.setFax(myClass.getHomeFax());
		    myCon.setPhone(myClass.getHomePhone());


		    myVal.setMergeGeneric1(formPop_Base.integer2string( myClass.getIsAttested() ,"tYesNoLILong"));
	            myVal.setMergeGeneric2(formPop_Base.date2string(myClass.getAttestDate()));
	            myVal.setMergeGeneric3(qCNT + " questions");

		    
		    myVal.setContactReference(myCon);
		    
		    myVal.setSummaryNotes(mySummary);
	    }
	    else if (theType == TYPE_tBoardCertification)
	    {
		    bltBoardCertification myClass = new bltBoardCertification(UniqueID);
		    myVal.setLastModifiedDate(myClass.getUniqueModifyDate());
		    myVal.setLastModifiedUser(myClass.getUniqueModifyComments());
		    myVal.setName(myClass.getSpecialty() + " #" +  myClass.getDocumentNumber() + " [Exp:" + formPop_Base.date2string(myClass.getExpirationDate()) + "]");
		    
		    myVal.setMergeGeneric1(myClass.getSpecialty());
		    myVal.setMergeGeneric2(myClass.getSubSpecialty());
		    myVal.setMergeGeneric3( (new bltSpecialtyStatusLI(myClass.getSpecialtyBoardCertified())).getLongStatus()  );
		    myVal.setMergeGeneric4(myClass.getDocumentNumber());
		    
          	  myCon.setCompanyName(myClass.getBoardName());   
		    myCon.setName(myClass.getContactName());
		    myCon.setAddress1(myClass.getAddress1());
		    myCon.setAddress2(myClass.getAddress2());
		    myCon.setCity(myClass.getCity());
		    myCon.setStateID(myClass.getStateID());
		    myCon.setProvince(myClass.getProvince());
		    myCon.setZIP(myClass.getZIP());
		    myCon.setCountryID(myClass.getCountryID());
		    myCon.setEmail(myClass.getContactEmail());
		    myCon.setFax(myClass.getFax());
		    myCon.setPhone(myClass.getPhone());
		    
		    myVal.setContactReference(myCon);
		    
		    String mySummary = "";
		    mySummary += "Board Name: " +  myClass.getBoardName() + "\n";
		    mySummary += "Document Number: " + myClass.getDocumentNumber() + "\n";
		    mySummary += "Status: " + (new bltSpecialtyStatusLI(myClass.getSpecialtyBoardCertified())).getLongStatus() + "\n";
		    mySummary += "Expiraton Date: " + formPop_Base.date2string(myClass.getExpirationDate()) + "\n";
		    mySummary += (myClass.getBoardName()) + "\n";
		    mySummary += (myClass.getAddress1()) + "\n";
		    mySummary += (myClass.getAddress2()) + "\n";
		    mySummary += (myClass.getCity()) + "\n";
		    mySummary += formPop_Base.ProvinceState(myClass.getStateID(),myClass.getProvince()) + "\n";
		    
		    mySummary += (myClass.getZIP()) + "\n";
		    mySummary += (myClass.getCountryID()) + "\n";
		    mySummary += (myClass.getContactEmail()) + "\n";
		    mySummary += (myClass.getFax()) + "\n";
		    mySummary += (myClass.getPhone()) + "\n";

		    myVal.setSummaryNotes(mySummary);
	    }
	    else if (theType == TYPE_tPhysicianMaster)
	    {
		    bltPhysicianMaster myClass = new bltPhysicianMaster(UniqueID);
		    myVal.setLastModifiedDate(myClass.getUniqueModifyDate());
		    myVal.setLastModifiedUser(myClass.getUniqueModifyComments());
		    myVal.setName(ConfigurationMessages.getDataCategory(typeCode));
		    
		    myCon.setName(myClass.getFirstName() + " " + myClass.getMiddleName() + " " + myClass.getLastName() + " " + myClass.getSuffix());
		    myCon.setAddress1(myClass.getHomeAddress1());
		    myCon.setAddress2(myClass.getHomeAddress2());
		    myCon.setCity(myClass.getHomeCity());
		    myCon.setStateID(myClass.getHomeStateID());
		    myCon.setProvince(myClass.getHomeProvince());
		    myCon.setZIP(myClass.getHomeZIP());
		    myCon.setCountryID(myClass.getHomeCountryID());
		    myCon.setEmail(myClass.getHomeEmail());
		    myCon.setFax(myClass.getHomeFax());
		    myCon.setPhone(myClass.getHomePhone());
		    
		    myVal.setContactReference(myCon);
		    
		    String mySummary = "";
		    mySummary += myClass.getFirstName() + " " + myClass.getMiddleName() + " " + myClass.getLastName() + " " + myClass.getSuffix() + "\n";
		    mySummary += "Other Names:" +  "\n";
		    mySummary += myClass.getOtherName1() + "(From: " + formPop_Base.date2string(myClass.getOtherName1Start()) + " To:"  + formPop_Base.date2string(myClass.getOtherName1End()) + ")" +  "\n";
		    mySummary += myClass.getOtherName2() + "(From: " + formPop_Base.date2string(myClass.getOtherName2Start()) + " To:"  + formPop_Base.date2string(myClass.getOtherName2End()) + ")" +  "\n";
		    mySummary += "Born on " + formPop_Base.date2string(myClass.getDateOfBirth()) + " in " +  myClass.getPlaceOfBirth() + "\n";
		    mySummary += "Souse: " + myClass.getSpouse() + "\n";
		    mySummary += "Home Address:" +  "\n";
		    mySummary += (myClass.getHomeAddress1()) + "\n";
		    mySummary += (myClass.getHomeAddress2()) + "\n";
		    mySummary += (myClass.getHomeCity()) + "\n";
		    mySummary += formPop_Base.ProvinceState(myClass.getHomeStateID(),myClass.getHomeProvince()) + "\n";
		    mySummary += (myClass.getHomeZIP()) + "\n";
		    mySummary += (myClass.getHomeCountryID()) + "\n";
		    mySummary += (myClass.getHomeEmail()) + "\n";
		    mySummary += (myClass.getHomeFax()) + "\n";
		    mySummary += (myClass.getHomePhone()) + "\n";
		    mySummary += "Other Information:" +  "\n";
            mySummary += "US Citizen: " + formPop_Base.integer2string(myClass.getCitizenshipYN(),"tYesNoLILong")  + "\n";
            mySummary += "Citizenship: " + myClass.getCitizenship() + "\n";
            mySummary += "Visa Number: " + myClass.getVisaNumber() + "\n"; 
            mySummary += "Visa Status: " + myClass.getVisaStatus() + "\n"; 
            mySummary += "Eligible to work in the US: " + formPop_Base.integer2string(myClass.getEligibleToWorkInUS(),"tYesNoLILong")  + "\n";
            mySummary += "Social Security Number: " + myClass.getSSN() + "\n";
            mySummary += "Gender: " + formPop_Base.integer2string( myClass.getGender() ,"tGenderLILong")  + "\n";
            mySummary += "Active in the Military: " + formPop_Base.integer2string( myClass.getMilitaryActive() ,"tYesNoLILong")  + "\n";
            mySummary += "Branch: " + myClass.getMilitaryBranch() + "\n";
            mySummary += "Military Reserves: " + formPop_Base.integer2string( myClass.getMilitaryReserve() ,"tYesNoLILong")  + "\n";
            mySummary += "Hospital Admiting Privileges: " + formPop_Base.integer2string( myClass.getHospitalAdmitingPrivileges() ,"tYesNoLILong")  + "\n";
            mySummary += "Admiting Arrangements: " + myClass.getHospitalAdmitingPrivilegesNo() + "\n";
            mySummary += "Physician Category: " + formPop_Base.integer2string( myClass.getPhysicianCategoryID() ,"tYesNoLILong")  + "\n";
            mySummary += "Affiliated with any other IPAs or Medical Groups: " + formPop_Base.integer2string(myClass.getIPAMedicalAffiliation() ,"tYesNoLILong")  + "\n";
            mySummary += "IPAs: " + myClass.getAffiliationDesc1() + "\n";
            mySummary += "Spoken Languages: " + myClass.getPhysicianLanguages() + "\n";
            mySummary += "ECFMG Number: " + myClass.getECFMGNo() + "\n";
            mySummary += "ECFMG Issue Date: " +  formPop_Base.date2string(myClass.getECFMGDateIssued()) + "\n"; 
            mySummary += "ECFMG Expiration Date: " +  formPop_Base.date2string(myClass.getECFMGDateExpires()) + "\n"; 
            mySummary += "Medicare UPIN: " + myClass.getMedicareNo() + "\n";
            mySummary += "NPI Number: " + myClass.getMediCaidNo() + "\n"; 
            mySummary += "Participating Medicare Provider: " + formPop_Base.integer2string( myClass.getMedicareParticipation(),"tYesNoLILong")  + "\n";
            mySummary += "Medicare Provider Number(s): " + myClass.getMedicareNo() + "\n"; 
            mySummary += "Participating Medicaid Provider: " + formPop_Base.integer2string( myClass.getMediCaidParticipation()  ,"tYesNoLILong") + "\n";
            mySummary += "Medicaid Provider Number(s): " + myClass.getMediCaidNo() + "\n"; 
            mySummary += "Supplemental ID #1: " + myClass.getSupplementalIDNumber1() + "\n";
            mySummary += "Supplemental ID #2: " + myClass.getSupplementalIDNumber2() + "\n";

		    myVal.setSummaryNotes(mySummary);
	    }
	    else if (theType == TYPE_tCoveringPhysicians)
	    {
		    bltCoveringPhysicians myClass = new bltCoveringPhysicians(UniqueID);
		    myVal.setLastModifiedDate(myClass.getUniqueModifyDate());
		    myVal.setLastModifiedUser(myClass.getUniqueModifyComments());
		    myVal.setName(myClass.getFirstName() + " " + myClass.getLastName());
		    
		    myCon.setName(myClass.getFirstName() + " " + myClass.getLastName());
		    myCon.setAddress1(myClass.getAddress1());
		    myCon.setAddress2(myClass.getAddress2());
		    myCon.setCity(myClass.getCity());
		    myCon.setStateID(myClass.getStateID());
		    myCon.setProvince(myClass.getProvince());
		    myCon.setZIP(myClass.getZIP());
		    myCon.setCountryID(myClass.getCountryID());
		    myCon.setEmail(myClass.getContactEmail());
		    myCon.setFax(myClass.getFax());
		    myCon.setPhone(myClass.getPhone());
		    
		    myVal.setContactReference(myCon);
		    
		    String mySummary = "";
		    mySummary += myClass.getFirstName() + " " + myClass.getLastName() + "\n";
		    mySummary += "Specialty: " + myClass.getSpecialty() + "\n";
		    mySummary += "License Number: " + myClass.getLicenseNumber() + "\n";
		    mySummary += "Address: " + "\n";
		    mySummary += (myClass.getAddress1()) + "\n";
		    mySummary += (myClass.getAddress2()) + "\n";
		    mySummary += (myClass.getCity()) + "\n";
		    mySummary += formPop_Base.ProvinceState(myClass.getStateID(),myClass.getProvince()) + "\n";
		    
		    mySummary += (myClass.getZIP()) + "\n";
		    mySummary += (myClass.getCountryID()) + "\n";
		    mySummary += (myClass.getContactEmail()) + "\n";
		    mySummary += (myClass.getFax()) + "\n";
		    mySummary += (myClass.getPhone()) + "\n";

		    myVal.setSummaryNotes(mySummary);
	    }
	    else if (theType == TYPE_tContinuingEducation)
	    {
		    bltContinuingEducation myClass = new bltContinuingEducation(UniqueID);
		    myVal.setLastModifiedDate(myClass.getUniqueModifyDate());
		    myVal.setLastModifiedUser(myClass.getUniqueModifyComments());
		    myVal.setName(myClass.getCourseName() + " [Credits: " + myClass.getCredits() + "]" );

            myVal.setMergeGeneric1(myClass.getCourseName());
            myVal.setMergeGeneric2(formPop_Base.date2string(myClass.getStartDate()));
            myVal.setMergeGeneric3(formPop_Base.date2string(myClass.getEndDate()));
            myVal.setMergeGeneric4(myClass.getCredits());
            
		    myCon.setCompanyName(myClass.getSchoolName());
		    myCon.setName(myClass.getContactName());
		    myCon.setAddress1(myClass.getSchoolAddress1());
		    myCon.setAddress2(myClass.getSchoolAddress2());
		    myCon.setCity(myClass.getSchoolCity());
		    myCon.setStateID(myClass.getSchoolStateID());
		    myCon.setProvince(myClass.getSchoolProvince());
		    myCon.setZIP(myClass.getSchoolZIP());
		    myCon.setCountryID(myClass.getCountryID());
		    myCon.setEmail(myClass.getContactEmail());
		    myCon.setFax(myClass.getSchoolFax());
		    myCon.setPhone(myClass.getSchoolPhone());
		    
		    myVal.setContactReference(myCon);
		    
		    String mySummary = "";
		    mySummary += "Course Name: " + myClass.getCourseName() + "\n";
		    mySummary += "Credits: " + myClass.getCredits() + "\n";
		    mySummary += "Sponsor Name: " + myClass.getSponsorName() + "\n";
		    mySummary += "Course Dates -- From: " + formPop_Base.date2string(myClass.getStartDate()) + " To: " +  formPop_Base.date2string(myClass.getEndDate()) + "\n";
		    mySummary += "School Information:  " + "\n";
		    mySummary += (myClass.getSchoolName()) + "\n";
		    mySummary += (myClass.getSchoolAddress1()) + "\n";
		    mySummary += (myClass.getSchoolAddress2()) + "\n";
		    mySummary += (myClass.getSchoolCity()) + "\n";
		    mySummary += formPop_Base.ProvinceState(myClass.getSchoolStateID(),myClass.getSchoolProvince()) + "\n";
		    mySummary += (myClass.getSchoolZIP()) + "\n";
		    mySummary += (myClass.getCountryID()) + "\n";
		    mySummary += (myClass.getContactEmail()) + "\n";
		    mySummary += (myClass.getSchoolFax()) + "\n";
		    mySummary += (myClass.getSchoolPhone()) + "\n";

		    myVal.setSummaryNotes(mySummary);
	    }
	    else if (theType == TYPE_tExperience)
	    {
		    bltExperience myClass = new bltExperience(UniqueID);
		    myVal.setLastModifiedDate(myClass.getUniqueModifyDate());
		    myVal.setLastModifiedUser(myClass.getUniqueModifyComments());
		    myVal.setName("(" + (new bltExperienceTypeLI(myClass.getTypeOfExperience())).getExperienceLong()+ ") " + myClass.getSpecialty() + " [" + formPop_Base.date2string(myClass.getDateTo()) + "]");
		    
		    
		    myVal.setMergeGeneric1( (new bltExperienceTypeLI(myClass.getTypeOfExperience())).getExperienceLong()  );
		    myVal.setMergeGeneric2(myClass.getSpecialty());
		    myVal.setMergeGeneric3(formPop_Base.date2string(myClass.getDateFrom()));
		    myVal.setMergeGeneric4(formPop_Base.date2string(myClass.getDateTo()));
		    myVal.setMergeGeneric5(myClass.getContactName());

		    
		    myCon.setCompanyName(myClass.getName());
		    myCon.setName(myClass.getContactName());
		    myCon.setAddress1(myClass.getAddress1());
		    myCon.setAddress2(myClass.getAddress2());
		    myCon.setCity(myClass.getCity());
		    myCon.setStateID(myClass.getStateID());
		    myCon.setProvince(myClass.getProvince());
		    myCon.setZIP(myClass.getZIP());
		    myCon.setCountryID(myClass.getCountryID());
		    myCon.setEmail(myClass.getContactEmail());
		    myCon.setFax(myClass.getFax());
		    myCon.setPhone(myClass.getPhone());
		    
		    myVal.setContactReference(myCon);
		    
		    String mySummary = "";
		    mySummary += "Facility:" +  myClass.getName() + "\n";
		    mySummary += "Experience Type: " + formPop_Base.integer2string( myClass.getTypeOfExperience() ,"tExperienceTypeLILong")  + "\n";
		    mySummary += "Specialty" + myClass.getSpecialty() + "\n";
		    mySummary += myClass.getOther() + "\n";
		    mySummary += "Dates -- From: " + formPop_Base.date2string(myClass.getDateFrom()) + " To: " + formPop_Base.date2string(myClass.getDateTo()) + "\n";
		    mySummary += "Address: " + "\n";
		    mySummary += (myClass.getName()) + "\n";
		    mySummary += (myClass.getAddress1()) + "\n";
		    mySummary += (myClass.getAddress2()) + "\n";
		    mySummary += (myClass.getCity()) + "\n";
		    mySummary += formPop_Base.ProvinceState(myClass.getStateID(),myClass.getProvince()) + "\n";
		    
		    mySummary += (myClass.getZIP()) + "\n";
		    mySummary += (myClass.getCountryID()) + "\n";
		    mySummary += (myClass.getContactEmail()) + "\n";
		    mySummary += (myClass.getFax()) + "\n";
		    mySummary += (myClass.getPhone()) + "\n";

		    myVal.setSummaryNotes(mySummary);
	    }
	    else if (theType == TYPE_tFacilityAffiliation)
	    {
		    bltFacilityAffiliation myClass = new bltFacilityAffiliation(UniqueID);
		    myVal.setLastModifiedDate(myClass.getUniqueModifyDate());
		    myVal.setLastModifiedUser(myClass.getUniqueModifyComments());
		    myVal.setName(myClass.getFacilityName() + " ("+myClass.getAppointmentLevel()+")");
		    
		    myVal.setMergeGeneric1(myClass.getAppointmentLevel());
		    myVal.setMergeGeneric2(formPop_Base.date2string(myClass.getStartDate()));
		    myVal.setMergeGeneric3(formPop_Base.date2string(myClass.getEndDate()));

            myCon.setCompanyName(myClass.getFacilityName());
		    myCon.setName(myClass.getContactName());
		    myCon.setAddress1(myClass.getFacilityAddress1());
		    myCon.setAddress2(myClass.getFacilityAddress2());
		    myCon.setCity(myClass.getFacilityCity());
		    myCon.setStateID(myClass.getFacilityStateID());
		    myCon.setProvince(myClass.getFacilityProvince());
		    myCon.setZIP(myClass.getFacilityZIP());
		    myCon.setCountryID(myClass.getFacilityCountryID());
		    myCon.setEmail(myClass.getContactEmail());
		    myCon.setFax(myClass.getFacilityFax());
		    myCon.setPhone(myClass.getFacilityPhone());
		    
		    myVal.setContactReference(myCon);
		    
		    String mySummary = "";
		    mySummary += "Facility: " +  myClass.getFacilityName() + "\n";
		    mySummary += "Admission Arrangements: " +  myClass.getAdmissionArrangements() + "\n";
		    mySummary += "Admission Priviledges: " +  myClass.getAdmissionPriviledges() + "\n";
		    mySummary += "Appointment Level: " +  myClass.getAppointmentLevel() + "\n";
		    mySummary += "Inpatient Care: " +  myClass.getInpatientCare() + "\n";
		    mySummary += "Primary Facility: " +  formPop_Base.integer2string( myClass.getIsPrimary() ,"tYesNoLILong")  + "\n";
		    mySummary += "Percent Admissions: " +  myClass.getPercentAdmissions() + "\n";
		    mySummary += "Reason for Leaving: " +  myClass.getReasonForLeaving() + "\n";
		    mySummary += "Temporary Priviledges: " +  myClass.getTempPriviledges() + "\n";
		    mySummary += "Unrestriceted Admission: " +  myClass.getUnrestrictedAdmission() + "\n";
		    mySummary += "Affiliation Dates -- From: " + formPop_Base.date2string(myClass.getStartDate()) + " To:" + formPop_Base.date2string(myClass.getEndDate()) + "\n";
		    mySummary += "Pending Date: " +  formPop_Base.date2string(myClass.getPendingDate()) + "\n";
		    mySummary += "Address: " + "\n";
		    mySummary += (myClass.getFacilityName()) + "\n";
		    mySummary += (myClass.getFacilityAddress1()) + "\n";
		    mySummary += (myClass.getFacilityAddress2()) + "\n";
		    mySummary += (myClass.getFacilityCity()) + "\n";
		    mySummary += formPop_Base.ProvinceState(myClass.getFacilityStateID(),myClass.getFacilityProvince()) + "\n";
		    mySummary += (myClass.getFacilityZIP()) + "\n";
		    mySummary += (myClass.getFacilityCountryID()) + "\n";
		    mySummary += (myClass.getContactEmail()) + "\n";
		    mySummary += (myClass.getFacilityFax()) + "\n";
		    mySummary += (myClass.getFacilityPhone()) + "\n";

		    myVal.setSummaryNotes(mySummary);
	    }
	    else if (theType == TYPE_tLicenseRegistration)
	    {
		    bltLicenseRegistration myClass = new bltLicenseRegistration(UniqueID);
		    myVal.setLastModifiedDate(myClass.getUniqueModifyDate());
		    myVal.setLastModifiedUser(myClass.getUniqueModifyComments());
		    myVal.setName("("+(new bltLicenseTypeLI(myClass.getLicenseType())).getLicenseTypeShort() + ") #" + myClass.getLicenseDocumentNumber() + " [Exp:" + formPop_Base.date2string(myClass.getExpirationDate()) + "]");

		    myVal.setMergeGeneric1(  (new bltLicenseTypeLI(myClass.getLicenseType())).getLicenseTypeLong()  );
		    myVal.setMergeGeneric2(myClass.getLicenseDocumentNumber());
		    myVal.setMergeGeneric3(formPop_Base.date2string(myClass.getExpirationDate()));
		    myVal.setMergeGeneric4( (new bltNPDBFOLTypeLI(myClass.getNPDBFOL())).getNPDBFOLShort() );
            
            
            myCon.setCompanyName(myClass.getName());
		    myCon.setName(myClass.getContactName());
		    myCon.setAddress1(myClass.getAddress1());
		    myCon.setAddress2(myClass.getAddress2());
		    myCon.setCity(myClass.getCity());
		    myCon.setStateID(myClass.getStateID());
		    myCon.setProvince(myClass.getProvince());
		    myCon.setZIP(myClass.getZIP());
		    myCon.setCountryID(myClass.getCountryID());
		    myCon.setEmail(myClass.getContactEmail());
		    myCon.setFax(myClass.getFax());
		    myCon.setPhone(myClass.getPhone());
		    
		    myVal.setContactReference(myCon);
		    
		    String mySummary = "";
		    mySummary += "License Type: " +  formPop_Base.integer2string( myClass.getLicenseType() ,"tLicenseTypeLILong")  + "\n";
		    mySummary += "License Sub-Type: " +  myClass.getSubType() + "\n";
		    mySummary += "License NDPB FOL: " +  (new bltNPDBFOLTypeLI(myClass.getNPDBFOL())).getNPDBFOLLong() + " ("+(new bltNPDBFOLTypeLI(myClass.getNPDBFOL())).getNPDBFOLShort() + ")\n";
		    mySummary += "License Org: " +  myClass.getName() + "\n";
		    mySummary += "Is Current: " +  formPop_Base.integer2string( myClass.getIsCurrent() ,"tYesNoLILong")  + "\n";
		    mySummary += "Practice in this State: " +  formPop_Base.integer2string( myClass.getIsPractice() ,"tYesNoLILong")  + "\n";
		    mySummary += "Document Number: " +  myClass.getLicenseDocumentNumber() + "\n";
		    mySummary += "License State: " +  formPop_Base.integer2string(myClass.getStateID(),"tStateLILong") + "\n";
		    mySummary += "Issue Date: " +  formPop_Base.date2string(myClass.getIssueDate()) + "\n";
		    mySummary += "Expiration Date: " +  formPop_Base.date2string(myClass.getExpirationDate()) + "\n";
		    mySummary += "Address: " + "\n";
		    mySummary += (myClass.getName()) + "\n";
		    mySummary += (myClass.getAddress1()) + "\n";
		    mySummary += (myClass.getAddress2()) + "\n";
		    mySummary += (myClass.getCity()) + "\n";
		    mySummary += formPop_Base.ProvinceState(myClass.getStateID(),myClass.getProvince()) + "\n";
		    
		    mySummary += (myClass.getZIP()) + "\n";
		    mySummary += (myClass.getCountryID()) + "\n";
		    mySummary += (myClass.getContactEmail()) + "\n";
		    mySummary += (myClass.getFax()) + "\n";
		    mySummary += (myClass.getPhone()) + "\n";

		    myVal.setSummaryNotes(mySummary);
	    }
	    else if (theType == TYPE_tMalpractice)
	    {
		    bltMalpractice myClass = new bltMalpractice(UniqueID);
		    myVal.setLastModifiedDate(myClass.getUniqueModifyDate());
		    myVal.setLastModifiedUser(myClass.getUniqueModifyComments());
		    myVal.setName(formPop_Base.date2string(myClass.getSuitDate()) + " ("+myClass.getSuitCity()+")");

		    myVal.setMergeGeneric1(formPop_Base.date2string(myClass.getSuitDate()));
		    myVal.setMergeGeneric2(myClass.getAllegation());
		    myVal.setMergeGeneric3(myClass.getSuitCity());
		    
		    
            myCon.setCompanyName(myClass.getInsuranceCompany());
		    myCon.setName(myClass.getContactFirstName() + " " + myClass.getContactLastName());
		    myCon.setAddress1(myClass.getAddress1());
		    myCon.setAddress2(myClass.getAddress2());
		    myCon.setCity(myClass.getCity());
		    myCon.setStateID(myClass.getStateID());
		    myCon.setProvince(myClass.getProvince());
		    myCon.setZIP(myClass.getZIP());
		    myCon.setCountryID(myClass.getCountryID());
		    myCon.setEmail(myClass.getContactEmail());
		    myCon.setFax(myClass.getFax());
		    myCon.setPhone(myClass.getPhone());
		    
		    myVal.setContactReference(myCon);
		    
		    String mySummary = "";
		    mySummary += "Suit Date: " +  formPop_Base.date2string(myClass.getSuitDate()) + "\n";
		    mySummary += "Suit City: " +  myClass.getSuitCity() + "\n";
		    mySummary += "Suite State: " +  myClass.getSuitState() + "\n";
		    mySummary += "Incident Date: " +  formPop_Base.date2string(myClass.getInicidentDate()) + "\n";
		    mySummary += "Incident Location: " +  myClass.getInicidentLocation() + "\n";
		    mySummary += "Allegation: " +  myClass.getAllegation() + "\n";
		    mySummary += "Patient Age: " +  myClass.getPatientAge() + "\n";
		    mySummary += "Patient Gender: " +  myClass.getPatientGender() + "\n";
		    mySummary += "Relationship to Patient: " +  myClass.getPatientRelationship() + "\n";
		    mySummary += "Attorney Name: " +  myClass.getAttorneyNameFull() + "\n";
		    mySummary += "Attorney Phone: " +  myClass.getAttorneyPhone() + "\n";
		    mySummary += "Was Insured: " +  formPop_Base.integer2string( myClass.getWasInsured() ,"tYesNoLILong")  + "\n";
		    mySummary += "Insurance Claim Number: " +  myClass.getInsuranceClaimNumber() + "\n";
		    mySummary += "Contact Name: " +  myClass.getContactFirstName() +" " +  myClass.getContactLastName() + "\n";
		    mySummary += "Address: " + "\n";
		    mySummary += myClass.getInsuranceCompany() + "\n";
		    mySummary += (myClass.getAddress1()) + "\n";
		    mySummary += (myClass.getAddress2()) + "\n";
		    mySummary += (myClass.getCity()) + "\n";
		    mySummary += formPop_Base.ProvinceState(myClass.getStateID(),myClass.getProvince()) + "\n";
		    
		    mySummary += (myClass.getZIP()) + "\n";
		    mySummary += (myClass.getCountryID()) + "\n";
		    mySummary += (myClass.getContactEmail()) + "\n";
		    mySummary += (myClass.getFax()) + "\n";
		    mySummary += (myClass.getPhone()) + "\n";

		    myVal.setSummaryNotes(mySummary);
	    }
	    else if (theType == TYPE_tManagedCarePlan)
	    {
		    bltManagedCarePlan myClass = new bltManagedCarePlan(UniqueID);
		    myVal.setLastModifiedDate(myClass.getUniqueModifyDate());
		    myVal.setLastModifiedUser(myClass.getUniqueModifyComments());
		    myVal.setName(myClass.getName() + " #" + myClass.getPlanNumber() );

            myCon.setCompanyName(myClass.getName());
		    myCon.setName(myClass.getContactName());
		    myCon.setAddress1(myClass.getAddress1());
		    myCon.setAddress2(myClass.getAddress2());
		    myCon.setCity(myClass.getCity());
		    myCon.setStateID(myClass.getStateID());
		    myCon.setProvince(myClass.getProvince());
		    myCon.setZIP(myClass.getZIP());
		    myCon.setCountryID(myClass.getCountryID());
		    myCon.setEmail(myClass.getContactEmail());
		    myCon.setFax(myClass.getFax());
		    myCon.setPhone(myClass.getPhone());
		    
		    myVal.setContactReference(myCon);
		    
		    String mySummary = "";
		    mySummary += "Plan Name: " + (myClass.getName()) + "\n";
		    mySummary += "Plan Number: "+ (myClass.getPlanNumber()) + "\n";
		    mySummary += "Reason for Leaving: " + (myClass.getReasonForLeaving()) + "\n";
		    mySummary += "Status: " + (myClass.getStatus()) + "\n";
		    mySummary += "Start Date: " + formPop_Base.date2string(myClass.getStartDate()) + "\n";
		    mySummary += "End Date: " + formPop_Base.date2string(myClass.getEndDate()) + "\n";
		    mySummary += "Pending Date: " + formPop_Base.date2string(myClass.getPendingDate()) + "\n";
		    mySummary += "Contact Name: " +  myClass.getContactName() + "\n";
		    mySummary += "Address: " + "\n";
		    mySummary += (myClass.getName()) + "\n";
		    mySummary += (myClass.getAddress1()) + "\n";
		    mySummary += (myClass.getAddress2()) + "\n";
		    mySummary += (myClass.getCity()) + "\n";
		    mySummary += formPop_Base.ProvinceState(myClass.getStateID(),myClass.getProvince()) + "\n";
		    
		    mySummary += (myClass.getZIP()) + "\n";
		    mySummary += (myClass.getCountryID()) + "\n";
		    mySummary += (myClass.getContactEmail()) + "\n";
		    mySummary += (myClass.getFax()) + "\n";
		    mySummary += (myClass.getPhone()) + "\n";

		    myVal.setSummaryNotes(mySummary);
	    }
	    else if (theType == TYPE_tOtherCertification)
	    {
		    bltOtherCertification myClass = new bltOtherCertification(UniqueID);
		    myVal.setLastModifiedDate(myClass.getUniqueModifyDate());
		    myVal.setLastModifiedUser(myClass.getUniqueModifyComments());
		    myVal.setName("("+myClass.getCertificationType() + ") #" + myClass.getCertificationNumber() + " [Exp:"+ formPop_Base.date2string(myClass.getExpirationDate()) +"]");
		    

		    myVal.setMergeGeneric1(myClass.getCertificationType() );
		    myVal.setMergeGeneric2(myClass.getCertificationNumber());
		    myVal.setMergeGeneric3(formPop_Base.date2string(myClass.getExpirationDate()));

            myCon.setCompanyName(myClass.getName());
		    myCon.setName(myClass.getContactName());
		    myCon.setAddress1(myClass.getAddress1());
		    myCon.setAddress2(myClass.getAddress2());
		    myCon.setCity(myClass.getCity());
		    myCon.setStateID(myClass.getStateID());
		    myCon.setProvince(myClass.getProvince());
		    myCon.setZIP(myClass.getZIP());
		    myCon.setCountryID(myClass.getCountryID());
		    myCon.setEmail(myClass.getContactEmail());
		    myCon.setFax(myClass.getFax());
		    myCon.setPhone(myClass.getPhone());
		    
		    myVal.setContactReference(myCon);
		    
		    String mySummary = "";
		    mySummary += "Certification Type: " + (myClass.getCertificationType()) + "\n";
		    mySummary += "Certification Number: " + (myClass.getCertificationNumber()) + "\n";
		    mySummary += "Certification Org: " + (myClass.getName()) + "\n";
		    mySummary += "Expiration Date: " + formPop_Base.date2string(myClass.getExpirationDate()) + "\n";
		    mySummary += "Address: " + "\n";
		    mySummary += (myClass.getName()) + "\n";
		    mySummary += (myClass.getAddress1()) + "\n";
		    mySummary += (myClass.getAddress2()) + "\n";
		    mySummary += (myClass.getCity()) + "\n";
		    mySummary += formPop_Base.ProvinceState(myClass.getStateID(),myClass.getProvince()) + "\n";
		    
		    mySummary += (myClass.getZIP()) + "\n";
		    mySummary += (myClass.getCountryID()) + "\n";
		    mySummary += (myClass.getContactEmail()) + "\n";
		    mySummary += (myClass.getFax()) + "\n";
		    mySummary += (myClass.getPhone()) + "\n";

		    myVal.setSummaryNotes(mySummary);
	    }
	    else if (theType == TYPE_tPeerReference)
	    {
		    bltPeerReference myClass = new bltPeerReference(UniqueID);
		    myVal.setLastModifiedDate(myClass.getUniqueModifyDate());
		    myVal.setLastModifiedUser(myClass.getUniqueModifyComments());
		    myVal.setName(myClass.getFirstName() + " " + myClass.getLastName());

		    String myFullName = myClass.getFirstName() + " " + myClass.getLastName();
		    if (!myClass.getTitle().equalsIgnoreCase(""))
		    {
			myFullName = myFullName + ", " +  myClass.getTitle();
		    }
		    else if (myClass.getSalutation().intValue()>0)
		    {
			myFullName = new bltSalutationLI(myClass.getSalutation()).getSalutationShort() + " " +   myFullName;
		    }
		    myCon.setName(myFullName);
		    myCon.setAddress1(myClass.getAddress1());
		    myCon.setAddress2(myClass.getAddress2());
		    myCon.setCity(myClass.getCity());
		    myCon.setStateID(myClass.getStateID());
		    myCon.setProvince(myClass.getProvince());
		    myCon.setZIP(myClass.getZIP());
		    myCon.setCountryID(myClass.getCountryID());
		    myCon.setEmail(myClass.getContactEmail());
		    myCon.setFax(myClass.getFax());
		    myCon.setPhone(myClass.getPhone());
		    
		    myVal.setContactReference(myCon);
		    
		    String mySummary = "";
		    mySummary += "Peer Reference Name: " + (myClass.getFirstName()) + " " + (myClass.getLastName()) + "\n";
		    mySummary += "Years Associated: " + (myClass.getYearsAssociated()) + "\n";
		    mySummary += "Address: " + "\n";
		    mySummary += (myClass.getAddress1()) + "\n";
		    mySummary += (myClass.getAddress2()) + "\n";
		    mySummary += (myClass.getCity()) + "\n";
		    mySummary += formPop_Base.ProvinceState(myClass.getStateID(),myClass.getProvince()) + "\n";
		    
		    mySummary += (myClass.getZIP()) + "\n";
		    mySummary += (myClass.getCountryID()) + "\n";
		    mySummary += (myClass.getContactEmail()) + "\n";
		    mySummary += (myClass.getFax()) + "\n";
		    mySummary += (myClass.getPhone()) + "\n";

		    myVal.setSummaryNotes(mySummary);
	    }
	    else if (theType == TYPE_tProfessionalEducation)
	    {
		    bltProfessionalEducation myClass = new bltProfessionalEducation(UniqueID);
		    myVal.setLastModifiedDate(myClass.getUniqueModifyDate());
		    myVal.setLastModifiedUser(myClass.getUniqueModifyComments());
		    myVal.setName("(" + (new bltDegreeTypeLI(myClass.getDegreeID())).getDegreeLong() + ") " + myClass.getFocus() + " [" + formPop_Base.date2string(myClass.getStartDate()) + " to " + formPop_Base.date2string(myClass.getDateOfGraduation()) + "]");

		    

//		    myVal.setMergeGeneric1(  (new bltDegreeTypeLI(myClass.getDegreeID())).getDegreeLong()  );
		    myVal.setMergeGeneric1(  myClass.getOther() );
		    myVal.setMergeGeneric2(formPop_Base.date2string(myClass.getStartDate()));
		    myVal.setMergeGeneric3(formPop_Base.date2string(myClass.getDateOfGraduation()));
		    myVal.setMergeGeneric4(myClass.getFocus());
		    myVal.setMergeGeneric5(formPop_Base.date2string(myClass.getEndDate()));

            myCon.setCompanyName(myClass.getSchoolName());
		    myCon.setName(myClass.getContactName());
		    myCon.setAddress1(myClass.getSchoolAddress1());
		    myCon.setAddress2(myClass.getSchoolAddress2());
		    myCon.setCity(myClass.getSchoolCity());
		    myCon.setStateID(myClass.getSchoolStateID());
		    myCon.setProvince(myClass.getSchoolProvince());
		    myCon.setZIP(myClass.getSchoolZIP());
		    myCon.setCountryID(myClass.getSchoolCountryID());
		    myCon.setEmail(myClass.getContactEmail());
		    myCon.setFax(myClass.getSchoolFax());
		    myCon.setPhone(myClass.getSchoolPhone());
		    
		    myVal.setContactReference(myCon);
		    
		    String mySummary = "";
		    mySummary += "School: " + myClass.getSchoolName() + "\n";
		    mySummary += "Degree Type: " + formPop_Base.integer2string(myClass.getDegreeID(),"tDegreetypeLILong") + "\n";
		    mySummary += "Focus:: " + myClass.getFocus() + "\n";
		    mySummary += ": " + myClass.getOther() + "\n";
		    mySummary += "Start Date: " + formPop_Base.date2string(myClass.getStartDate()) + "\n";
		    mySummary += "Graduation Date: " + formPop_Base.date2string(myClass.getDateOfGraduation()) + "\n";
		    mySummary += "Or End Date: " + formPop_Base.date2string(myClass.getEndDate()) + "\n";
		    mySummary += "Address: " + "\n";
		    mySummary += (myClass.getSchoolName()) + "\n";
		    mySummary += (myClass.getSchoolAddress1()) + "\n";
		    mySummary += (myClass.getSchoolAddress2()) + "\n";
		    mySummary += (myClass.getSchoolCity()) + "\n";
		    mySummary += formPop_Base.ProvinceState(myClass.getSchoolStateID(),myClass.getSchoolProvince()) + "\n";
		    mySummary += (myClass.getSchoolZIP()) + "\n";
		    mySummary += (myClass.getSchoolCountryID()) + "\n";
		    mySummary += (myClass.getContactEmail()) + "\n";
		    mySummary += (myClass.getSchoolFax()) + "\n";
		    mySummary += (myClass.getSchoolPhone()) + "\n";

		    myVal.setSummaryNotes(mySummary);
	    }
	    else if (theType == TYPE_tProfessionalLiability)
	    {
		    bltProfessionalLiability myClass = new bltProfessionalLiability(UniqueID);
		    myVal.setLastModifiedDate(myClass.getUniqueModifyDate());
		    myVal.setLastModifiedUser(myClass.getUniqueModifyComments());
		    myVal.setName(myClass.getInsuranceCarrier() + " #" + myClass.getPolicyNumber());
		    

		    myVal.setMergeGeneric1(myClass.getInsuranceCarrier());
		    myVal.setMergeGeneric2(myClass.getPolicyHolder());
		    myVal.setMergeGeneric3(myClass.getPolicyNumber());
		    try
		    {
		        if (myClass.getTerminationDate().after(PLCUtils.getDisplayDateSDF1().parse("01/01/1850")))
		        {
        		    myVal.setMergeGeneric4(formPop_Base.date2string(myClass.getOriginalEffectiveDate()));
		            myVal.setMergeGeneric5(formPop_Base.date2string(myClass.getTerminationDate()));
    		    }
	    	    else     
		        {
        		    myVal.setMergeGeneric4(formPop_Base.date2string(myClass.getInsuredFromDate()));
		            myVal.setMergeGeneric5(formPop_Base.date2string(myClass.getInsuredToDate()));
    		    }
    		}
    		catch(Exception e55)
    		{
        		    myVal.setMergeGeneric4(formPop_Base.date2string(myClass.getInsuredFromDate()));
		            myVal.setMergeGeneric5(formPop_Base.date2string(myClass.getInsuredToDate()));
    		}


		    
            myCon.setCompanyName(myClass.getInsuranceCarrier());
		    myCon.setName(myClass.getContactName());
		    myCon.setAddress1(myClass.getAddress1());
		    myCon.setAddress2(myClass.getAddress2());
		    myCon.setCity(myClass.getCity());
		    myCon.setStateID(myClass.getStateID());
		    myCon.setProvince(myClass.getProvince());
		    myCon.setZIP(myClass.getZIP());
		    myCon.setCountryID(myClass.getCountryID());
		    myCon.setEmail(myClass.getContactEmail());
		    myCon.setFax(myClass.getFax());
		    myCon.setPhone(myClass.getPhone());
		    
		    myVal.setContactReference(myCon);
		    
		    String mySummary = "";
		    mySummary += "Insurance Carrier: " + (myClass.getInsuranceCarrier()) + "\n";
		    mySummary += "Coverage Type: " + (myClass.getCoverageType()) + "\n";
		    mySummary += "Policy Number: " + (myClass.getPolicyNumber()) + "\n";
		    mySummary += "Policy Holder: " + (myClass.getPolicyHolder()) + "\n";
		    mySummary += "Agent Name: " + myClass.getAgentName() + "\n";
		    mySummary += "Aggregate Amount: " + (myClass.getAggregateAmount()) + "\n";
		    mySummary += "Per Claim Amount: " + (myClass.getPerClaimAmount()) + "\n";
		    mySummary += "Desc. of Surcharge: " + (myClass.getDescOfSurcharge()) + "\n";
		    mySummary += "If Current -- From: " + formPop_Base.date2string(myClass.getInsuredFromDate()) + " To: " + formPop_Base.date2string(myClass.getInsuredToDate()) + "\n";
		    mySummary += "Original Effective Date: " + formPop_Base.date2string(myClass.getOriginalEffectiveDate()) + "\n";
		    mySummary += "Terminiation Date: " + formPop_Base.date2string(myClass.getTerminationDate()) + "\n";
		    mySummary += "Address: " + "\n";
		    mySummary += (myClass.getAddress1()) + "\n";
		    mySummary += (myClass.getAddress2()) + "\n";
		    mySummary += (myClass.getCity()) + "\n";
		    mySummary += formPop_Base.ProvinceState(myClass.getStateID(),myClass.getProvince()) + "\n";
		    
		    mySummary += (myClass.getZIP()) + "\n";
		    mySummary += (myClass.getCountryID()) + "\n";
		    mySummary += (myClass.getContactEmail()) + "\n";
		    mySummary += (myClass.getFax()) + "\n";
		    mySummary += (myClass.getPhone()) + "\n";

		    myVal.setSummaryNotes(mySummary);
	    }
	    else if (theType == TYPE_tProfessionalMisconduct)
	    {
		    bltProfessionalMisconduct myClass = new bltProfessionalMisconduct(UniqueID);
		    myVal.setLastModifiedDate(myClass.getUniqueModifyDate());
		    myVal.setLastModifiedUser(myClass.getUniqueModifyComments());
		    myVal.setName(formPop_Base.date2string(myClass.getSuitDate()) + " ("+myClass.getInicidentLocation()+")");

		    myVal.setMergeGeneric1(formPop_Base.date2string(myClass.getSuitDate()));
		    myVal.setMergeGeneric2(myClass.getAllegation());
		    myVal.setMergeGeneric3(myClass.getInicidentLocation());

            myCon.setCompanyName(myClass.getReviewBoardName());
		    myCon.setName(myClass.getContactName());
		    myCon.setAddress1(myClass.getAddress1());
		    myCon.setAddress2(myClass.getAddress2());
		    myCon.setCity(myClass.getCity());
		    myCon.setStateID(myClass.getStateID());
		    myCon.setProvince(myClass.getProvince());
		    myCon.setZIP(myClass.getZIP());
		    myCon.setCountryID(myClass.getCountryID());
		    myCon.setEmail(myClass.getContactEmail());
		    myCon.setFax(myClass.getFax());
		    myCon.setPhone(myClass.getPhone());
		    
		    myVal.setContactReference(myCon);
		    
		    String mySummary = "";
		    mySummary += "Suite Date: " + formPop_Base.date2string(myClass.getSuitDate()) + "\n";
		    mySummary += "Review Board Name: " + (myClass.getReviewBoardName()) + "\n";
		    mySummary += "Allegation: " + (myClass.getAllegation()) + "\n";
		    mySummary += "Incident Date: " + formPop_Base.date2string(myClass.getInicidentDate()) + "\n";
		    mySummary += "Incident Location: " + (myClass.getInicidentLocation()) + "\n";
		    mySummary += "Realtionship to Patient: " + (myClass.getPatientRelationship()) + "\n";
		    mySummary += "Findings: " + (myClass.getFindings()) + "\n";
		    mySummary += "Address: " + "\n";
		    mySummary += (myClass.getAddress1()) + "\n";
		    mySummary += (myClass.getAddress2()) + "\n";
		    mySummary += (myClass.getCity()) + "\n";
		    mySummary += formPop_Base.ProvinceState(myClass.getStateID(),myClass.getProvince()) + "\n";
		    
		    mySummary += (myClass.getZIP()) + "\n";
		    mySummary += (myClass.getCountryID()) + "\n";
		    mySummary += (myClass.getContactEmail()) + "\n";
		    mySummary += (myClass.getFax()) + "\n";
		    mySummary += (myClass.getPhone()) + "\n";

		    myVal.setSummaryNotes(mySummary);
	    }
	    else if (theType == TYPE_tProfessionalSociety)
	    {
		    bltProfessionalSociety myClass = new bltProfessionalSociety(UniqueID);
		    myVal.setLastModifiedDate(myClass.getUniqueModifyDate());
		    myVal.setLastModifiedUser(myClass.getUniqueModifyComments());
		    myVal.setName(myClass.getName() + " (" +myClass.getMembershipStatus()+")"  );
		    
		    myVal.setMergeGeneric1(myClass.getMembershipStatus());
		    myVal.setMergeGeneric2(formPop_Base.date2string(myClass.getFromDate()));
		    myVal.setMergeGeneric3(formPop_Base.date2string(myClass.getToDate()));
            
            myCon.setCompanyName(myClass.getName());
		    myCon.setName(myClass.getContactName());
		    myCon.setAddress1(myClass.getAddress1());
		    myCon.setAddress2(myClass.getAddress2());
		    myCon.setCity(myClass.getCity());
		    myCon.setStateID(myClass.getStateID());
		    myCon.setProvince(myClass.getProvince());
		    myCon.setZIP(myClass.getZIP());
		    myCon.setCountryID(myClass.getCountryID());
		    myCon.setEmail(myClass.getContactEmail());
		    myCon.setFax(myClass.getFax());
		    myCon.setPhone(myClass.getPhone());
		    
		    myVal.setContactReference(myCon);
		    
		    String mySummary = "";
		    mySummary += "Society Org. Name: " + myClass.getName() + "\n";
		    mySummary += "Membership Status: " + myClass.getMembershipStatus() + "\n";
		    mySummary += "Membership Dates -- From: " + formPop_Base.date2string(myClass.getFromDate()) + " To: " + formPop_Base.date2string(myClass.getToDate()) + "\n";
		    mySummary += "Address: " + "\n";
		    mySummary += (myClass.getName()) + "\n";
		    mySummary += (myClass.getAddress1()) + "\n";
		    mySummary += (myClass.getAddress2()) + "\n";
		    mySummary += (myClass.getCity()) + "\n";
		    mySummary += formPop_Base.ProvinceState(myClass.getStateID(),myClass.getProvince()) + "\n";
		    
		    mySummary += (myClass.getZIP()) + "\n";
		    mySummary += (myClass.getCountryID()) + "\n";
		    mySummary += (myClass.getContactEmail()) + "\n";
		    mySummary += (myClass.getFax()) + "\n";
		    mySummary += (myClass.getPhone()) + "\n";

		    myVal.setSummaryNotes(mySummary);
	    }
	    else if (theType == TYPE_tWorkHistory)
	    {
		    bltWorkHistory myClass = new bltWorkHistory(UniqueID);
		    myVal.setLastModifiedDate(myClass.getUniqueModifyDate());
		    myVal.setLastModifiedUser(myClass.getUniqueModifyComments());
		    myVal.setName("("+(new bltWorkHistoryTypeLI(myClass.getWorkHistoryTypeID())).getWorkHistoryTypeLong() +")"+myClass.getName());
		    
		    myVal.setMergeGeneric1(  (new bltWorkHistoryTypeLI(myClass.getWorkHistoryTypeID())).getWorkHistoryTypeLong()  );
		    myVal.setMergeGeneric2(myClass.getWorkDescription());
		    myVal.setMergeGeneric3(formPop_Base.date2string(myClass.getFromDate()));
		    myVal.setMergeGeneric4(formPop_Base.date2string(myClass.getToDate()));
		    myVal.setMergeGeneric5(myClass.getReasonForLeaving());

            myCon.setCompanyName(myClass.getName());
		    myCon.setName(myClass.getContactName());
		    myCon.setAddress1(myClass.getAddress1());
		    myCon.setAddress2(myClass.getAddress2());
		    myCon.setCity(myClass.getCity());
		    myCon.setStateID(myClass.getStateID());
		    myCon.setProvince(myClass.getProvince());
		    myCon.setZIP(myClass.getZIP());
		    myCon.setCountryID(myClass.getCountryID());
		    myCon.setEmail(myClass.getContactEmail());
		    myCon.setFax(myClass.getFax());
		    myCon.setPhone(myClass.getPhone());
		    
		    myVal.setContactReference(myCon);
		    
		    String mySummary = "";
		    mySummary += "Facility Name: " + myClass.getName() + "\n";
		    mySummary += "Work Type: " + formPop_Base.integer2string(myClass.getWorkHistoryTypeID(),"tWorkHistoryTypeLILong") + "\n";
		    mySummary += "Description of Work: " + myClass.getWorkDescription() + "\n";
		    mySummary += "Reason for Leaving: " + myClass.getReasonForLeaving() + "\n";
		    mySummary += "Work Dates -- From: " + formPop_Base.date2string(myClass.getFromDate()) + " To:" + formPop_Base.date2string(myClass.getToDate()) + "\n";
		    mySummary += "Address: " + "\n";
		    mySummary += (myClass.getName()) + "\n";
		    mySummary += (myClass.getAddress1()) + "\n";
		    mySummary += (myClass.getAddress2()) + "\n";
		    mySummary += (myClass.getCity()) + "\n";
		    mySummary += formPop_Base.ProvinceState(myClass.getStateID(),myClass.getProvince()) + "\n";
		    
		    mySummary += (myClass.getZIP()) + "\n";
		    mySummary += (myClass.getCountryID()) + "\n";
		    mySummary += (myClass.getContactEmail()) + "\n";
		    mySummary += (myClass.getFax()) + "\n";
		    mySummary += (myClass.getPhone()) + "\n";

		    myVal.setSummaryNotes(mySummary);
	    }
*/
return myVal;
    }

    public static int  TYPE_tAdditionalInformation = 0;
    public static int  TYPE_tPhysicianMaster = 15;
    public static int  TYPE_tCoveringPhysicians = 16;
    public static int  TYPE_tAttestR_form = 17;
    public static int  TYPE_tBoardCertification = 1;
    public static int  TYPE_tContinuingEducation = 2;
    public static int  TYPE_tExperience = 3;
    public static int  TYPE_tFacilityAffiliation = 4;
    public static int  TYPE_tLicenseRegistration = 5;
    public static int  TYPE_tMalpractice = 6;
    public static int  TYPE_tManagedCarePlan = 7;
    public static int  TYPE_tOtherCertification = 8;
    public static int  TYPE_tPeerReference = 9;
    public static int  TYPE_tProfessionalEducation = 10;
    public static int  TYPE_tProfessionalLiability = 11;
    public static int  TYPE_tProfessionalMisconduct = 12;
    public static int  TYPE_tProfessionalSociety = 13;
    public static int  TYPE_tWorkHistory = 14;
	public static java.text.SimpleDateFormat mySDF = new java.text.SimpleDateFormat("MM/dd/yyyy");

 }   // End of ConfigurationInformation class
