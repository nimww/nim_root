

package com.winstaff;

import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public class dbtCCardTransaction extends Object implements InttCCardTransaction
{

        db_NewBase    dbnbDB;

    public dbtCCardTransaction()
    {
        dbnbDB = new db_NewBase( "tCCardTransaction", "CCardTransactionID" );

    }    // End of default constructor

    public dbtCCardTransaction( Integer iNewID )
    {
        dbnbDB = new db_NewBase( "tCCardTransaction", "CCardTransactionID" );
        setUniqueID( iNewID );
    }    // End of Constructor knowing an ID

    public void commitData() throws SQLException
    {
            //no longer trapping/ignoring errors 2011-03-17
            dbnbDB.commitData();
    }    // End of commitData()

    public void commitDataForced() throws SQLException
    {
        dbnbDB.commitDataForced();
    }    // End of commitData()

    public Integer getUniqueID()
    {
        return dbnbDB.getUniqueID();
    }    // End of getUniqueID()

    public void setUniqueID( Integer iNewID )
    {
        dbnbDB.setUniqueID( iNewID );
    }    // End of setUniqueID()

    public void setModifyComments( String sNew )
    {
        dbnbDB.setModifyComments( sNew );
    }    // End of setUniqueID()


    public void setCCardTransactionID(Integer newValue)
    {
                dbnbDB.setFieldData( "CCardTransactionID", newValue.toString() );
    }

    public Integer getCCardTransactionID()
    {
        String           sValue = dbnbDB.getFieldData( "CCardTransactionID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setField(String fieldN, Object fieldV)
    {DebugLogger.println("setField Method not implemented yet in classCCardTransaction!");}

    public String getFieldType(String fieldN)
    {return null;}


    public void setUniqueCreateDate(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "UniqueCreateDate", formatter.format( newValue ) );
    }

    public Date getUniqueCreateDate()
    {
        String           sValue = dbnbDB.getFieldData( "UniqueCreateDate" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setUniqueModifyDate(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "UniqueModifyDate", formatter.format( newValue ) );
    }

    public Date getUniqueModifyDate()
    {
        String           sValue = dbnbDB.getFieldData( "UniqueModifyDate" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setUniqueModifyComments(String newValue)
    {
                dbnbDB.setFieldData( "UniqueModifyComments", newValue.toString() );
    }

    public String getUniqueModifyComments()
    {
        String           sValue = dbnbDB.getFieldData( "UniqueModifyComments" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setTransactionDate(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "TransactionDate", formatter.format( newValue ) );
    }

    public Date getTransactionDate()
    {
        String           sValue = dbnbDB.getFieldData( "TransactionDate" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setActionID(Integer newValue)
    {
                dbnbDB.setFieldData( "ActionID", newValue.toString() );
    }

    public Integer getActionID()
    {
        String           sValue = dbnbDB.getFieldData( "ActionID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setCName(String newValue)
    {
                dbnbDB.setFieldData( "CName", newValue.toString() );
    }

    public String getCName()
    {
        String           sValue = dbnbDB.getFieldData( "CName" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setCAddress(String newValue)
    {
                dbnbDB.setFieldData( "CAddress", newValue.toString() );
    }

    public String getCAddress()
    {
        String           sValue = dbnbDB.getFieldData( "CAddress" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setCCity(String newValue)
    {
                dbnbDB.setFieldData( "CCity", newValue.toString() );
    }

    public String getCCity()
    {
        String           sValue = dbnbDB.getFieldData( "CCity" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setCState(String newValue)
    {
                dbnbDB.setFieldData( "CState", newValue.toString() );
    }

    public String getCState()
    {
        String           sValue = dbnbDB.getFieldData( "CState" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setCZip(String newValue)
    {
                dbnbDB.setFieldData( "CZip", newValue.toString() );
    }

    public String getCZip()
    {
        String           sValue = dbnbDB.getFieldData( "CZip" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setCCNumber(Integer newValue)
    {
                dbnbDB.setFieldData( "CCNumber", newValue.toString() );
    }

    public Integer getCCNumber()
    {
        String           sValue = dbnbDB.getFieldData( "CCNumber" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setCEXP(String newValue)
    {
                dbnbDB.setFieldData( "CEXP", newValue.toString() );
    }

    public String getCEXP()
    {
        String           sValue = dbnbDB.getFieldData( "CEXP" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setOrderNumber(String newValue)
    {
                dbnbDB.setFieldData( "OrderNumber", newValue.toString() );
    }

    public String getOrderNumber()
    {
        String           sValue = dbnbDB.getFieldData( "OrderNumber" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setAuthNumber(String newValue)
    {
                dbnbDB.setFieldData( "AuthNumber", newValue.toString() );
    }

    public String getAuthNumber()
    {
        String           sValue = dbnbDB.getFieldData( "AuthNumber" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setAmountCharged(String newValue)
    {
                dbnbDB.setFieldData( "AmountCharged", newValue.toString() );
    }

    public String getAmountCharged()
    {
        String           sValue = dbnbDB.getFieldData( "AmountCharged" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setEncounterID(Integer newValue)
    {
                dbnbDB.setFieldData( "EncounterID", newValue.toString() );
    }

    public Integer getEncounterID()
    {
        String           sValue = dbnbDB.getFieldData( "EncounterID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

}    // End of bltCCardTransaction class definition
