

package com.winstaff;

import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public interface InttNIM3_Modality extends dbTableInterface
{

    public void setModalityID(Integer newValue);
    public Integer getModalityID();
    public void setUniqueCreateDate(Date newValue);
    public Date getUniqueCreateDate();
    public void setUniqueModifyDate(Date newValue);
    public Date getUniqueModifyDate();
    public void setUniqueModifyComments(String newValue);
    public String getUniqueModifyComments();
    public void setPracticeID(Integer newValue);
    public Integer getPracticeID();
    public void setModalityTypeID(Integer newValue);
    public Integer getModalityTypeID();
    public void setModalityModelID(Integer newValue);
    public Integer getModalityModelID();
    public void setSoftwareVersion(String newValue);
    public String getSoftwareVersion();
    public void setLastServiceDate(Date newValue);
    public Date getLastServiceDate();
    public void setCoilWrist(Integer newValue);
    public Integer getCoilWrist();
    public void setCoilElbow(Integer newValue);
    public Integer getCoilElbow();
    public void setCoilKnee(Integer newValue);
    public Integer getCoilKnee();
    public void setCoilAnkle(Integer newValue);
    public Integer getCoilAnkle();
    public void setCoilShoulder(Integer newValue);
    public Integer getCoilShoulder();
    public void setCoilSpine(Integer newValue);
    public Integer getCoilSpine();
    public void setIsACR(Integer newValue);
    public Integer getIsACR();
    public void setACRExpiration(Date newValue);
    public Date getACRExpiration();
    public void setIsACRVerified(Integer newValue);
    public Integer getIsACRVerified();
    public void setACRVerifiedDate(Date newValue);
    public Date getACRVerifiedDate();
    public void setComments(String newValue);
    public String getComments();
}    // End of bltNIM3_Modality class definition
