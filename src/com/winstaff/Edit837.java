package com.winstaff;


import com.berryworks.edireader.AnsiReader;
import com.berryworks.edireader.EDIReader;
import com.berryworks.edireader.adapter.FilteredEDIReader;
import com.berryworks.edireader.adapter.HipaaAdapter;
import com.berryworks.edireader.edit837.DomEditor;
import com.berryworks.edireader.edit837.NextImageMedicalDomEditor;
import com.berryworks.edireader.passthrough.EditorHandler;
import com.berryworks.edireader.saxeditor.rules.BCBSRules;
import com.berryworks.edireader.tokenizer.EDITokenizer;
import com.berryworks.edireader.util.CommandLine;
import com.berryworks.edireader.util.PassThroughParameters;
import com.berryworks.ediwriter.EDIWriter;
import com.berryworks.ediwriter.EDIWriterFactory;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMResult;
import javax.xml.transform.sax.SAXSource;
import java.io.*;

public class Edit837 {
    protected final Reader inputReader;
    protected final OutputStream editedOutput;
    protected final String ackFile;
    protected final PassThroughParameters parameters;

    protected int interchangeCount;

    public Edit837(String input, String output, String ackFile, String parameterFileName, boolean debug) {
        if (debug) {
            EDIReader.setDebug(true);
        }

        if (output == null) {
            editedOutput = System.out;
        } else {
            try {
                editedOutput = new BufferedOutputStream(new FileOutputStream(output));
            } catch (IOException e) {
                System.err.println(e.getMessage());
                throw new RuntimeException(e.getMessage());
            }
        }

        try {
            inputReader = new FileReader(input);
        } catch (IOException e) {
            System.err.println(e.getMessage());
            throw new RuntimeException(e.getMessage());
        }

        this.ackFile = ackFile;

        parameters = PassThroughParameters.instance();
        if (parameterFileName != null) {
            try {
                parameters.load(new File(parameterFileName));
            } catch (IOException e) {
                throw new RuntimeException("Unable to load parameter file " + parameterFileName);
            }
        }
    }


    public void run() {

        EditorHandler handler = new EditorHandler(new BCBSRules());
        handler.setDebug(false);
        char[] leftOver = null;
        Writer outputWriter = new PrintWriter(editedOutput);
        StringWriter ackWriter = new StringWriter();

        try {
            Transformer transformer = TransformerFactory.newInstance().newTransformer();
            while (true) {
                EDITokenizer tokenizer = new EDITokenizer(inputReader, leftOver);
                tokenizer.scanTerminatorSuffix();
                if (tokenizer.isEndOfData()) {
                    // end of input
                    break;
                }

                handler.setTokenizer(tokenizer);
                AnsiReader ediReader = new AnsiReader();
                ediReader.setAcknowledgment(ackWriter);

                if (ackFile != null) {
                    ediReader.setAcknowledgment(ackWriter);
                }

                EDIReader hipaaParser = new FilteredEDIReader(ediReader, new HipaaAdapter());
                hipaaParser.setTokenizer(tokenizer);
                hipaaParser.setContentHandler(handler);

                SAXSource saxSource = new SAXSource(hipaaParser, new InputSource(inputReader));
                DOMResult domResult = new DOMResult();
                transformer.transform(saxSource, domResult);

                Document document = (Document) domResult.getNode();
                DomEditor domEditor = new NextImageMedicalDomEditor(parameters);
                domEditor.edit(document, 0);

                File templateFile = new File("template837");
                Reader ediTemplate = templateFile.exists() ?
                        new FileReader(templateFile) :
                        new StringReader(DomEditor.getDefaultTemplate());

                generate(document, ediTemplate, outputWriter);

                leftOver = tokenizer.getBuffered();
            }

            if (ackFile != null) {
                Writer writer = new FileWriter(ackFile);
                writer.write(ackWriter.toString());
                writer.close();
            }

        } catch (IOException e) {
            System.err.println(e.getMessage());
            throw new RuntimeException(e.getMessage());
        } catch (SAXException e) {
            System.err.println("\nEDI input not well-formed:\n" + e.toString());
            throw new RuntimeException(e.getMessage());
        } catch (TransformerException e) {
            System.err.println(e.getMessage());
            throw new RuntimeException(e.getMessage());
        } finally {
//            close();
        }

    }

    private void close() {
        if (inputReader != null) try {
            inputReader.close();
            editedOutput.close();
        } catch (IOException ignore) {
        }
    }


    public static void main(String args[]) {
        boolean debug = false;

        CommandLine commandLine = new CommandLine(args);
        String inputFileName = commandLine.getPosition(0);
        if (inputFileName == null) badArgs();
        String outputFileName = commandLine.getPosition(1);
        if (outputFileName == null) badArgs();
        String ackFileName = commandLine.getOption("a");
        String parameterFileName = commandLine.getOption("p");
        String debugOption = commandLine.getOption("d");

        String lineSeparator = System.getProperty("line.separator");
        System.out.println(lineSeparator + lineSeparator);
        System.out.println(lineSeparator);
        System.out.println("Editing input EDI file: " + inputFileName);
        System.out.println("Generating output file: " + outputFileName);
        if (ackFileName != null) {
            System.out.println("Generating acknowledgement file: " + ackFileName);
        }
        if (parameterFileName != null) {
            System.out.println("Use parameter file: " + parameterFileName);
        }
        System.out.println(lineSeparator);

        Edit837 editor = new Edit837(inputFileName, outputFileName, ackFileName, parameterFileName, debug);
        editor.run();
        System.out.println(lineSeparator);
        System.out.println("Editing complete");
        System.out.println(lineSeparator);
    }

    public void generate(Document document, Reader ediTemplate, Writer ediOutput)
            throws SAXException, IOException {
        InputSource template = new InputSource(ediTemplate);
        EDIWriter ediWriter = EDIWriterFactory.createEDIWriter(template);
        ediWriter.setDocument(document);
        ediWriter.write(ediOutput);
    }

    /**
     * Print a summary of the proper command line usage.
     */
    private static void badArgs() {
        System.out.println("Usage: PassThrough inputFile outputFile [-a ackFile]  [-p parameterFile] [-d]");
        System.out.println("where:  inputFile         file containing EDI input");
        System.out.println("        outputFile        file for edited EDI output");
        System.out.println("        -a ackFile        (optional) file for 997 acknowledgement");
        System.out.println("        -p parameterFile  (optional) file containing program parameters");
        System.out.println("        -d                (optional) debug mode: true or false (default)");
        throw new RuntimeException("Missing or invalid command line arguments");
    }

    public static String trimLeading(char c, String value) {

        int countOfLeadingCharsToTrim = 0;
        for (int i = 0; i < value.length(); i++) {
            if (c == value.charAt(i)) {
                countOfLeadingCharsToTrim++;
            } else {
                break;
            }
        }

        if (countOfLeadingCharsToTrim == value.length() && countOfLeadingCharsToTrim > 0) {
            // Don't trim the last char
            countOfLeadingCharsToTrim--;
        }

        return (countOfLeadingCharsToTrim > 0) ? value.substring(countOfLeadingCharsToTrim) : value;

    }
}
