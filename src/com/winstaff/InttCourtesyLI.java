

package com.winstaff;

import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public interface InttCourtesyLI extends dbTableInterface
{

    public void setCourtesyID(Integer newValue);
    public Integer getCourtesyID();
    public void setCourtesyShort(String newValue);
    public String getCourtesyShort();
    public void setCourtesyLong(String newValue);
    public String getCourtesyLong();
}    // End of bltCourtesyLI class definition
