

package com.winstaff;

import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public interface InttEmailTransaction extends dbTableInterface
{

    public void setEmailTransactionID(Integer newValue);
    public Integer getEmailTransactionID();
    public void setUniqueCreateDate(Date newValue);
    public Date getUniqueCreateDate();
    public void setUniqueModifyDate(Date newValue);
    public Date getUniqueModifyDate();
    public void setUniqueModifyComments(String newValue);
    public String getUniqueModifyComments();
    public void setTransactionDate(Date newValue);
    public Date getTransactionDate();
    public void setActionID(Integer newValue);
    public Integer getActionID();
    public void setEmailTo(String newValue);
    public String getEmailTo();
    public void setEmailFrom(String newValue);
    public String getEmailFrom();
    public void setEmailBody(String newValue);
    public String getEmailBody();
    public void setEmailBodyType(String newValue);
    public String getEmailBodyType();
    public void setEmailSubject(String newValue);
    public String getEmailSubject();
    public void setEmailImportance(Integer newValue);
    public Integer getEmailImportance();
    public void setComments(String newValue);
    public String getComments();
    public void setVirtualAttachment(String newValue);
    public String getVirtualAttachment();
    public void setEmailCC1(String newValue);
    public String getEmailCC1();
    public void setEmailCC2(String newValue);
    public String getEmailCC2();
    public void setEmailCC3(String newValue);
    public String getEmailCC3();
    public void setEmailCC4(String newValue);
    public String getEmailCC4();
    public void setEmailCC5(String newValue);
    public String getEmailCC5();
}    // End of bltEmailTransaction class definition
