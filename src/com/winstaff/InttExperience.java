package com.winstaff;


import com.winstaff.dbtExperience;
import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public interface InttExperience extends dbTableInterface
{

    public void setExperienceID(Integer newValue);
    public Integer getExperienceID();
    public void setUniqueCreateDate(Date newValue);
    public Date getUniqueCreateDate();
    public void setUniqueModifyDate(Date newValue);
    public Date getUniqueModifyDate();
    public void setUniqueModifyComments(String newValue);
    public String getUniqueModifyComments();
    public void setPhysicianID(Integer newValue);
    public Integer getPhysicianID();
    public void setTypeOfExperience(Integer newValue);
    public Integer getTypeOfExperience();
    public void setOther(String newValue);
    public String getOther();
    public void setSpecialty(String newValue);
    public String getSpecialty();
    public void setDateFrom(Date newValue);
    public Date getDateFrom();
    public void setDateTo(Date newValue);
    public Date getDateTo();
    public void setNoComplete(String newValue);
    public String getNoComplete();
    public void setName(String newValue);
    public String getName();
    public void setAddress1(String newValue);
    public String getAddress1();
    public void setAddress2(String newValue);
    public String getAddress2();
    public void setCity(String newValue);
    public String getCity();
    public void setStateID(Integer newValue);
    public Integer getStateID();
    public void setProvince(String newValue);
    public String getProvince();
    public void setZIP(String newValue);
    public String getZIP();
    public void setCountryID(Integer newValue);
    public Integer getCountryID();
    public void setPhone(String newValue);
    public String getPhone();
    public void setFax(String newValue);
    public String getFax();
    public void setContactName(String newValue);
    public String getContactName();
    public void setContactEmail(String newValue);
    public String getContactEmail();
    public void setComments(String newValue);
    public String getComments();
}    // End of bltExperience class definition
