

package com.winstaff;

import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public interface InttPHDBHospitalLI extends dbTableInterface
{

    public void setHospitalID(Integer newValue);
    public Integer getHospitalID();
    public void setUniqueCreateDate(Date newValue);
    public Date getUniqueCreateDate();
    public void setUniqueModifyDate(Date newValue);
    public Date getUniqueModifyDate();
    public void setUniqueModifyComments(String newValue);
    public String getUniqueModifyComments();
    public void setStatus(Integer newValue);
    public Integer getStatus();
    public void setName(String newValue);
    public String getName();
    public void setAddress1(String newValue);
    public String getAddress1();
    public void setAddress2(String newValue);
    public String getAddress2();
    public void setCity(String newValue);
    public String getCity();
    public void setState(String newValue);
    public String getState();
    public void setZIP(String newValue);
    public String getZIP();
    public void setPhone(String newValue);
    public String getPhone();
    public void setFax(String newValue);
    public String getFax();
    public void setComments(String newValue);
    public String getComments();
    public void setContactName(String newValue);
    public String getContactName();
    public void setEmail(String newValue);
    public String getEmail();
}    // End of bltPHDBHospitalLI class definition
