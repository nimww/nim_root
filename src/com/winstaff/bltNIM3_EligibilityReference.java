

package com.winstaff;

import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;
import java.util.Enumeration;

public class bltNIM3_EligibilityReference extends Object implements InttNIM3_EligibilityReference
{

    dbtNIM3_EligibilityReference    dbDB;

    public bltNIM3_EligibilityReference()
    {
        dbDB = new dbtNIM3_EligibilityReference();
        dbDB.setUniqueCreateDate(new java.util.Date());
        makeRequiredHash();
        makeExpiredHash();
        makeEnglishHash();

    }    // End of default constructor

    public bltNIM3_EligibilityReference( Integer iNewID )
    {        dbDB = new dbtNIM3_EligibilityReference( iNewID );
        makeRequiredHash();
        makeExpiredHash();
        makeEnglishHash();
        iSecurityCheck = new Integer(2);
    }    // End of Constructor knowing an ID


    public bltNIM3_EligibilityReference( Integer iNewID, Integer iGroupSecurityID )
    {
        this.GroupSecurityInit(iGroupSecurityID);
        dbDB = new dbtNIM3_EligibilityReference( iNewID );
        makeExpiredHash();
        makeEnglishHash();
        makeRequiredHash(iGroupSecurityID);
    }    // End of Constructor knowing an ID

    public bltNIM3_EligibilityReference( Integer iGroupSecurityID, boolean isTrue )
    {
        this.GroupSecurityInit(iGroupSecurityID);
        dbDB = new dbtNIM3_EligibilityReference(  );
        makeExpiredHash();
        makeEnglishHash();
        makeRequiredHash(iGroupSecurityID);
    }    // End of Security unknown ID

    public void GroupSecurityInit( Integer iGroupSecurityID )
    {
        bltSecurityGroupMaster mySGM = new bltSecurityGroupMaster(iGroupSecurityID);
        this.UserSecurityID = iGroupSecurityID;
        if (mySGM.getRunFieldSecurityID().intValue()==1)
        {
            this.runFieldSecurity = true;
            makeRequiredHash(iGroupSecurityID);
        }
        iSecurityCheck = SecurityCheck.CheckItem("nim1", iGroupSecurityID);
    }  

    public EventReferenceType getEventReference()
    {
        return EventUtils.getEventReference("tNIM3_EligibilityReference", this.getUniqueID());
    }  

    public String convertString2LogString(String normalString) 
    {
        String myVal = "";
        for (int i=0;i<normalString.length();i++)
        {
            if ( (normalString.charAt(i)!='\r')&&(normalString.charAt(i)!='\n')&&(normalString.charAt(i)!='\t') )
            {
                myVal+=normalString.charAt(i);
            }
            else
            {
                myVal+="<NLT>";
            }
        }
        return myVal;
    }
    public void runAudit()
    {
        String AuditString = new java.util.Date()+" \t " ;  
        AuditString += " tNIM3_EligibilityReference "; 
        AuditString += " EligibilityReferenceID ="+this.getUniqueID(); 
        AuditString += " PayerID ="+this.getPayerID(); 
        for (int i=0;i<this.AuditVector.size();i++)
        {
            AuditString+="\t"+convertString2LogString((String)this.AuditVector.elementAt(i));
        }
        AuditLogger.printLine(AuditString);
    }

    public void commitData() throws SQLException
    {
        runAudit();
        dbDB.setModifyComments(this.getUniqueModifyComments());
        dbDB.setUniqueModifyDate(new java.util.Date());
        dbDB.commitData();
        DataControlUtils.dataChanged("tNIM3_EligibilityReference",this.getPayerID() ,this.AuditVector.size(), "PayerID");
        this.AuditVector.removeAllElements();
    }    // End of commitData()

    public void commitDataDisableAudit() throws SQLException
    {
        runAudit();
        dbDB.setModifyComments(this.getUniqueModifyComments());
        dbDB.setUniqueModifyDate(new java.util.Date());
        dbDB.commitDataDisableAudit();
        DataControlUtils.dataChanged("tNIM3_EligibilityReference",this.getPayerID() ,this.AuditVector.size(), "PayerID");
        this.AuditVector.removeAllElements();
    }    // End of commitData()

    public void commitDataForced() throws SQLException
    {
        dbDB.commitDataForced();
    }    // End of commitData()

    public Integer getUniqueID()
    {
        return dbDB.getUniqueID();
    }    // End of getUniqueID()

    public void setUniqueID( Integer iNewID )
    {
        dbDB.setUniqueID( iNewID );
    }    // End of setUniqueID()


    public void setEligibilityReferenceID(Integer newValue)
    {
        dbDB.setEligibilityReferenceID(newValue);
    }

    public Integer getEligibilityReferenceID()
    {
        return dbDB.getEligibilityReferenceID();
    }

    java.util.Vector AuditVector = new java.util.Vector();

    public void setUniqueCreateDate(Date newValue)
    {
        this.setUniqueCreateDate(newValue,this.UserSecurityID);


    }
    public Date getUniqueCreateDate()
    {
        return this.getUniqueCreateDate(this.UserSecurityID);
    }

    public void setUniqueCreateDate(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='UniqueCreateDate' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[UniqueCreateDate]=["+newValue+"]");
                   dbDB.setUniqueCreateDate(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[UniqueCreateDate]=["+newValue+"]");
           dbDB.setUniqueCreateDate(newValue);
         }
    }
    public Date getUniqueCreateDate(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='UniqueCreateDate' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getUniqueCreateDate();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getUniqueCreateDate();
         }
        return myVal;
    }

    public void setUniqueModifyDate(Date newValue)
    {
        this.setUniqueModifyDate(newValue,this.UserSecurityID);


    }
    public Date getUniqueModifyDate()
    {
        return this.getUniqueModifyDate(this.UserSecurityID);
    }

    public void setUniqueModifyDate(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='UniqueModifyDate' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[UniqueModifyDate]=["+newValue+"]");
                   dbDB.setUniqueModifyDate(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[UniqueModifyDate]=["+newValue+"]");
           dbDB.setUniqueModifyDate(newValue);
         }
    }
    public Date getUniqueModifyDate(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='UniqueModifyDate' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getUniqueModifyDate();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getUniqueModifyDate();
         }
        return myVal;
    }

    public void setUniqueModifyComments(String newValue)
    {
        this.setUniqueModifyComments(newValue,this.UserSecurityID);


    }
    public String getUniqueModifyComments()
    {
        return this.getUniqueModifyComments(this.UserSecurityID);
    }

    public void setUniqueModifyComments(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='UniqueModifyComments' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[UniqueModifyComments]=["+newValue+"]");
                   dbDB.setUniqueModifyComments(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[UniqueModifyComments]=["+newValue+"]");
           dbDB.setUniqueModifyComments(newValue);
         }
    }
    public String getUniqueModifyComments(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='UniqueModifyComments' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getUniqueModifyComments();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getUniqueModifyComments();
         }
        return myVal;
    }

    public void setPayerID(Integer newValue)
    {
        this.setPayerID(newValue,this.UserSecurityID);


    }
    public Integer getPayerID()
    {
        return this.getPayerID(this.UserSecurityID);
    }

    public void setPayerID(Integer newValue, Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PayerID' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[PayerID]=["+newValue+"]");
                   dbDB.setPayerID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[PayerID]=["+newValue+"]");
           dbDB.setPayerID(newValue);
         }
    }
    public Integer getPayerID(Integer GroupRefID)
    {
        Integer myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PayerID' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPayerID();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPayerID();
         }
        return myVal;
    }

    public void setCaseClaimNumber(String newValue)
    {
        this.setCaseClaimNumber(newValue,this.UserSecurityID);


    }
    public String getCaseClaimNumber()
    {
        return this.getCaseClaimNumber(this.UserSecurityID);
    }

    public void setCaseClaimNumber(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CaseClaimNumber' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[CaseClaimNumber]=["+newValue+"]");
                   dbDB.setCaseClaimNumber(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[CaseClaimNumber]=["+newValue+"]");
           dbDB.setCaseClaimNumber(newValue);
         }
    }
    public String getCaseClaimNumber(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CaseClaimNumber' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getCaseClaimNumber();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getCaseClaimNumber();
         }
        return myVal;
    }

    public void setClientPayerID(String newValue)
    {
        this.setClientPayerID(newValue,this.UserSecurityID);


    }
    public String getClientPayerID()
    {
        return this.getClientPayerID(this.UserSecurityID);
    }

    public void setClientPayerID(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ClientPayerID' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[ClientPayerID]=["+newValue+"]");
                   dbDB.setClientPayerID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[ClientPayerID]=["+newValue+"]");
           dbDB.setClientPayerID(newValue);
         }
    }
    public String getClientPayerID(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ClientPayerID' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getClientPayerID();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getClientPayerID();
         }
        return myVal;
    }

    public void setPayerIndicator(String newValue)
    {
        this.setPayerIndicator(newValue,this.UserSecurityID);


    }
    public String getPayerIndicator()
    {
        return this.getPayerIndicator(this.UserSecurityID);
    }

    public void setPayerIndicator(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PayerIndicator' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[PayerIndicator]=["+newValue+"]");
                   dbDB.setPayerIndicator(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[PayerIndicator]=["+newValue+"]");
           dbDB.setPayerIndicator(newValue);
         }
    }
    public String getPayerIndicator(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PayerIndicator' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPayerIndicator();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPayerIndicator();
         }
        return myVal;
    }

    public void setEmployerName(String newValue)
    {
        this.setEmployerName(newValue,this.UserSecurityID);


    }
    public String getEmployerName()
    {
        return this.getEmployerName(this.UserSecurityID);
    }

    public void setEmployerName(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='EmployerName' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[EmployerName]=["+newValue+"]");
                   dbDB.setEmployerName(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[EmployerName]=["+newValue+"]");
           dbDB.setEmployerName(newValue);
         }
    }
    public String getEmployerName(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='EmployerName' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getEmployerName();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getEmployerName();
         }
        return myVal;
    }

    public void setEmployerPhone(String newValue)
    {
        this.setEmployerPhone(newValue,this.UserSecurityID);


    }
    public String getEmployerPhone()
    {
        return this.getEmployerPhone(this.UserSecurityID);
    }

    public void setEmployerPhone(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='EmployerPhone' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[EmployerPhone]=["+newValue+"]");
                   dbDB.setEmployerPhone(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[EmployerPhone]=["+newValue+"]");
           dbDB.setEmployerPhone(newValue);
         }
    }
    public String getEmployerPhone(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='EmployerPhone' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getEmployerPhone();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getEmployerPhone();
         }
        return myVal;
    }

    public void setEmployerAddress1(String newValue)
    {
        this.setEmployerAddress1(newValue,this.UserSecurityID);


    }
    public String getEmployerAddress1()
    {
        return this.getEmployerAddress1(this.UserSecurityID);
    }

    public void setEmployerAddress1(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='EmployerAddress1' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[EmployerAddress1]=["+newValue+"]");
                   dbDB.setEmployerAddress1(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[EmployerAddress1]=["+newValue+"]");
           dbDB.setEmployerAddress1(newValue);
         }
    }
    public String getEmployerAddress1(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='EmployerAddress1' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getEmployerAddress1();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getEmployerAddress1();
         }
        return myVal;
    }

    public void setEmployerAddress2(String newValue)
    {
        this.setEmployerAddress2(newValue,this.UserSecurityID);


    }
    public String getEmployerAddress2()
    {
        return this.getEmployerAddress2(this.UserSecurityID);
    }

    public void setEmployerAddress2(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='EmployerAddress2' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[EmployerAddress2]=["+newValue+"]");
                   dbDB.setEmployerAddress2(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[EmployerAddress2]=["+newValue+"]");
           dbDB.setEmployerAddress2(newValue);
         }
    }
    public String getEmployerAddress2(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='EmployerAddress2' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getEmployerAddress2();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getEmployerAddress2();
         }
        return myVal;
    }

    public void setEmployerCity(String newValue)
    {
        this.setEmployerCity(newValue,this.UserSecurityID);


    }
    public String getEmployerCity()
    {
        return this.getEmployerCity(this.UserSecurityID);
    }

    public void setEmployerCity(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='EmployerCity' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[EmployerCity]=["+newValue+"]");
                   dbDB.setEmployerCity(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[EmployerCity]=["+newValue+"]");
           dbDB.setEmployerCity(newValue);
         }
    }
    public String getEmployerCity(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='EmployerCity' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getEmployerCity();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getEmployerCity();
         }
        return myVal;
    }

    public void setEmployerCounty(String newValue)
    {
        this.setEmployerCounty(newValue,this.UserSecurityID);


    }
    public String getEmployerCounty()
    {
        return this.getEmployerCounty(this.UserSecurityID);
    }

    public void setEmployerCounty(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='EmployerCounty' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[EmployerCounty]=["+newValue+"]");
                   dbDB.setEmployerCounty(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[EmployerCounty]=["+newValue+"]");
           dbDB.setEmployerCounty(newValue);
         }
    }
    public String getEmployerCounty(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='EmployerCounty' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getEmployerCounty();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getEmployerCounty();
         }
        return myVal;
    }

    public void setEmployerState(String newValue)
    {
        this.setEmployerState(newValue,this.UserSecurityID);


    }
    public String getEmployerState()
    {
        return this.getEmployerState(this.UserSecurityID);
    }

    public void setEmployerState(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='EmployerState' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[EmployerState]=["+newValue+"]");
                   dbDB.setEmployerState(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[EmployerState]=["+newValue+"]");
           dbDB.setEmployerState(newValue);
         }
    }
    public String getEmployerState(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='EmployerState' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getEmployerState();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getEmployerState();
         }
        return myVal;
    }

    public void setEmployerZip(String newValue)
    {
        this.setEmployerZip(newValue,this.UserSecurityID);


    }
    public String getEmployerZip()
    {
        return this.getEmployerZip(this.UserSecurityID);
    }

    public void setEmployerZip(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='EmployerZip' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[EmployerZip]=["+newValue+"]");
                   dbDB.setEmployerZip(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[EmployerZip]=["+newValue+"]");
           dbDB.setEmployerZip(newValue);
         }
    }
    public String getEmployerZip(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='EmployerZip' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getEmployerZip();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getEmployerZip();
         }
        return myVal;
    }

    public void setDateCaseOpened(Date newValue)
    {
        this.setDateCaseOpened(newValue,this.UserSecurityID);


    }
    public Date getDateCaseOpened()
    {
        return this.getDateCaseOpened(this.UserSecurityID);
    }

    public void setDateCaseOpened(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='DateCaseOpened' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[DateCaseOpened]=["+newValue+"]");
                   dbDB.setDateCaseOpened(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[DateCaseOpened]=["+newValue+"]");
           dbDB.setDateCaseOpened(newValue);
         }
    }
    public Date getDateCaseOpened(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='DateCaseOpened' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getDateCaseOpened();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getDateCaseOpened();
         }
        return myVal;
    }

    public void setDateOfInjury(Date newValue)
    {
        this.setDateOfInjury(newValue,this.UserSecurityID);


    }
    public Date getDateOfInjury()
    {
        return this.getDateOfInjury(this.UserSecurityID);
    }

    public void setDateOfInjury(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='DateOfInjury' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[DateOfInjury]=["+newValue+"]");
                   dbDB.setDateOfInjury(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[DateOfInjury]=["+newValue+"]");
           dbDB.setDateOfInjury(newValue);
         }
    }
    public Date getDateOfInjury(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='DateOfInjury' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getDateOfInjury();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getDateOfInjury();
         }
        return myVal;
    }

    public void setAttorneyFirstName(String newValue)
    {
        this.setAttorneyFirstName(newValue,this.UserSecurityID);


    }
    public String getAttorneyFirstName()
    {
        return this.getAttorneyFirstName(this.UserSecurityID);
    }

    public void setAttorneyFirstName(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AttorneyFirstName' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[AttorneyFirstName]=["+newValue+"]");
                   dbDB.setAttorneyFirstName(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[AttorneyFirstName]=["+newValue+"]");
           dbDB.setAttorneyFirstName(newValue);
         }
    }
    public String getAttorneyFirstName(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AttorneyFirstName' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getAttorneyFirstName();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getAttorneyFirstName();
         }
        return myVal;
    }

    public void setAttorneyLastName(String newValue)
    {
        this.setAttorneyLastName(newValue,this.UserSecurityID);


    }
    public String getAttorneyLastName()
    {
        return this.getAttorneyLastName(this.UserSecurityID);
    }

    public void setAttorneyLastName(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AttorneyLastName' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[AttorneyLastName]=["+newValue+"]");
                   dbDB.setAttorneyLastName(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[AttorneyLastName]=["+newValue+"]");
           dbDB.setAttorneyLastName(newValue);
         }
    }
    public String getAttorneyLastName(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AttorneyLastName' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getAttorneyLastName();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getAttorneyLastName();
         }
        return myVal;
    }

    public void setAttorneyWorkPhone(String newValue)
    {
        this.setAttorneyWorkPhone(newValue,this.UserSecurityID);


    }
    public String getAttorneyWorkPhone()
    {
        return this.getAttorneyWorkPhone(this.UserSecurityID);
    }

    public void setAttorneyWorkPhone(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AttorneyWorkPhone' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[AttorneyWorkPhone]=["+newValue+"]");
                   dbDB.setAttorneyWorkPhone(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[AttorneyWorkPhone]=["+newValue+"]");
           dbDB.setAttorneyWorkPhone(newValue);
         }
    }
    public String getAttorneyWorkPhone(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AttorneyWorkPhone' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getAttorneyWorkPhone();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getAttorneyWorkPhone();
         }
        return myVal;
    }

    public void setAttorneyFax(String newValue)
    {
        this.setAttorneyFax(newValue,this.UserSecurityID);


    }
    public String getAttorneyFax()
    {
        return this.getAttorneyFax(this.UserSecurityID);
    }

    public void setAttorneyFax(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AttorneyFax' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[AttorneyFax]=["+newValue+"]");
                   dbDB.setAttorneyFax(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[AttorneyFax]=["+newValue+"]");
           dbDB.setAttorneyFax(newValue);
         }
    }
    public String getAttorneyFax(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AttorneyFax' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getAttorneyFax();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getAttorneyFax();
         }
        return myVal;
    }

    public void setPatientAccountNumber(String newValue)
    {
        this.setPatientAccountNumber(newValue,this.UserSecurityID);


    }
    public String getPatientAccountNumber()
    {
        return this.getPatientAccountNumber(this.UserSecurityID);
    }

    public void setPatientAccountNumber(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientAccountNumber' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[PatientAccountNumber]=["+newValue+"]");
                   dbDB.setPatientAccountNumber(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[PatientAccountNumber]=["+newValue+"]");
           dbDB.setPatientAccountNumber(newValue);
         }
    }
    public String getPatientAccountNumber(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientAccountNumber' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPatientAccountNumber();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPatientAccountNumber();
         }
        return myVal;
    }

    public void setPatientFirstName(String newValue)
    {
        this.setPatientFirstName(newValue,this.UserSecurityID);


    }
    public String getPatientFirstName()
    {
        return this.getPatientFirstName(this.UserSecurityID);
    }

    public void setPatientFirstName(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientFirstName' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[PatientFirstName]=["+newValue+"]");
                   dbDB.setPatientFirstName(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[PatientFirstName]=["+newValue+"]");
           dbDB.setPatientFirstName(newValue);
         }
    }
    public String getPatientFirstName(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientFirstName' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPatientFirstName();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPatientFirstName();
         }
        return myVal;
    }

    public void setPatientLastName(String newValue)
    {
        this.setPatientLastName(newValue,this.UserSecurityID);


    }
    public String getPatientLastName()
    {
        return this.getPatientLastName(this.UserSecurityID);
    }

    public void setPatientLastName(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientLastName' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[PatientLastName]=["+newValue+"]");
                   dbDB.setPatientLastName(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[PatientLastName]=["+newValue+"]");
           dbDB.setPatientLastName(newValue);
         }
    }
    public String getPatientLastName(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientLastName' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPatientLastName();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPatientLastName();
         }
        return myVal;
    }

    public void setPatientDOB(Date newValue)
    {
        this.setPatientDOB(newValue,this.UserSecurityID);


    }
    public Date getPatientDOB()
    {
        return this.getPatientDOB(this.UserSecurityID);
    }

    public void setPatientDOB(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientDOB' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[PatientDOB]=["+newValue+"]");
                   dbDB.setPatientDOB(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[PatientDOB]=["+newValue+"]");
           dbDB.setPatientDOB(newValue);
         }
    }
    public Date getPatientDOB(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientDOB' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPatientDOB();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPatientDOB();
         }
        return myVal;
    }

    public void setPatientSSN(String newValue)
    {
        this.setPatientSSN(newValue,this.UserSecurityID);


    }
    public String getPatientSSN()
    {
        return this.getPatientSSN(this.UserSecurityID);
    }

    public void setPatientSSN(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientSSN' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[PatientSSN]=["+newValue+"]");
                   dbDB.setPatientSSN(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[PatientSSN]=["+newValue+"]");
           dbDB.setPatientSSN(newValue);
         }
    }
    public String getPatientSSN(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientSSN' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPatientSSN();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPatientSSN();
         }
        return myVal;
    }

    public void setPatientGender(String newValue)
    {
        this.setPatientGender(newValue,this.UserSecurityID);


    }
    public String getPatientGender()
    {
        return this.getPatientGender(this.UserSecurityID);
    }

    public void setPatientGender(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientGender' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[PatientGender]=["+newValue+"]");
                   dbDB.setPatientGender(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[PatientGender]=["+newValue+"]");
           dbDB.setPatientGender(newValue);
         }
    }
    public String getPatientGender(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientGender' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPatientGender();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPatientGender();
         }
        return myVal;
    }

    public void setPatientAddress1(String newValue)
    {
        this.setPatientAddress1(newValue,this.UserSecurityID);


    }
    public String getPatientAddress1()
    {
        return this.getPatientAddress1(this.UserSecurityID);
    }

    public void setPatientAddress1(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientAddress1' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[PatientAddress1]=["+newValue+"]");
                   dbDB.setPatientAddress1(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[PatientAddress1]=["+newValue+"]");
           dbDB.setPatientAddress1(newValue);
         }
    }
    public String getPatientAddress1(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientAddress1' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPatientAddress1();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPatientAddress1();
         }
        return myVal;
    }

    public void setPatientAddress2(String newValue)
    {
        this.setPatientAddress2(newValue,this.UserSecurityID);


    }
    public String getPatientAddress2()
    {
        return this.getPatientAddress2(this.UserSecurityID);
    }

    public void setPatientAddress2(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientAddress2' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[PatientAddress2]=["+newValue+"]");
                   dbDB.setPatientAddress2(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[PatientAddress2]=["+newValue+"]");
           dbDB.setPatientAddress2(newValue);
         }
    }
    public String getPatientAddress2(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientAddress2' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPatientAddress2();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPatientAddress2();
         }
        return myVal;
    }

    public void setPatientCity(String newValue)
    {
        this.setPatientCity(newValue,this.UserSecurityID);


    }
    public String getPatientCity()
    {
        return this.getPatientCity(this.UserSecurityID);
    }

    public void setPatientCity(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientCity' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[PatientCity]=["+newValue+"]");
                   dbDB.setPatientCity(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[PatientCity]=["+newValue+"]");
           dbDB.setPatientCity(newValue);
         }
    }
    public String getPatientCity(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientCity' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPatientCity();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPatientCity();
         }
        return myVal;
    }

    public void setPatientState(String newValue)
    {
        this.setPatientState(newValue,this.UserSecurityID);


    }
    public String getPatientState()
    {
        return this.getPatientState(this.UserSecurityID);
    }

    public void setPatientState(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientState' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[PatientState]=["+newValue+"]");
                   dbDB.setPatientState(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[PatientState]=["+newValue+"]");
           dbDB.setPatientState(newValue);
         }
    }
    public String getPatientState(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientState' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPatientState();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPatientState();
         }
        return myVal;
    }

    public void setPatientZip(String newValue)
    {
        this.setPatientZip(newValue,this.UserSecurityID);


    }
    public String getPatientZip()
    {
        return this.getPatientZip(this.UserSecurityID);
    }

    public void setPatientZip(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientZip' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[PatientZip]=["+newValue+"]");
                   dbDB.setPatientZip(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[PatientZip]=["+newValue+"]");
           dbDB.setPatientZip(newValue);
         }
    }
    public String getPatientZip(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientZip' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPatientZip();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPatientZip();
         }
        return myVal;
    }

    public void setPatientHomePhone(String newValue)
    {
        this.setPatientHomePhone(newValue,this.UserSecurityID);


    }
    public String getPatientHomePhone()
    {
        return this.getPatientHomePhone(this.UserSecurityID);
    }

    public void setPatientHomePhone(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientHomePhone' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[PatientHomePhone]=["+newValue+"]");
                   dbDB.setPatientHomePhone(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[PatientHomePhone]=["+newValue+"]");
           dbDB.setPatientHomePhone(newValue);
         }
    }
    public String getPatientHomePhone(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientHomePhone' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPatientHomePhone();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPatientHomePhone();
         }
        return myVal;
    }

    public void setPatientWorkPhone(String newValue)
    {
        this.setPatientWorkPhone(newValue,this.UserSecurityID);


    }
    public String getPatientWorkPhone()
    {
        return this.getPatientWorkPhone(this.UserSecurityID);
    }

    public void setPatientWorkPhone(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientWorkPhone' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[PatientWorkPhone]=["+newValue+"]");
                   dbDB.setPatientWorkPhone(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[PatientWorkPhone]=["+newValue+"]");
           dbDB.setPatientWorkPhone(newValue);
         }
    }
    public String getPatientWorkPhone(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientWorkPhone' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPatientWorkPhone();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPatientWorkPhone();
         }
        return myVal;
    }

    public void setPatientCellPhone(String newValue)
    {
        this.setPatientCellPhone(newValue,this.UserSecurityID);


    }
    public String getPatientCellPhone()
    {
        return this.getPatientCellPhone(this.UserSecurityID);
    }

    public void setPatientCellPhone(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientCellPhone' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[PatientCellPhone]=["+newValue+"]");
                   dbDB.setPatientCellPhone(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[PatientCellPhone]=["+newValue+"]");
           dbDB.setPatientCellPhone(newValue);
         }
    }
    public String getPatientCellPhone(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientCellPhone' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPatientCellPhone();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPatientCellPhone();
         }
        return myVal;
    }

    public void setPatientEmail(String newValue)
    {
        this.setPatientEmail(newValue,this.UserSecurityID);


    }
    public String getPatientEmail()
    {
        return this.getPatientEmail(this.UserSecurityID);
    }

    public void setPatientEmail(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientEmail' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[PatientEmail]=["+newValue+"]");
                   dbDB.setPatientEmail(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[PatientEmail]=["+newValue+"]");
           dbDB.setPatientEmail(newValue);
         }
    }
    public String getPatientEmail(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PatientEmail' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPatientEmail();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPatientEmail();
         }
        return myVal;
    }

    public void setCorrusClaimId(String newValue)
    {
        this.setCorrusClaimId(newValue,this.UserSecurityID);


    }
    public String getCorrusClaimId()
    {
        return this.getCorrusClaimId(this.UserSecurityID);
    }

    public void setCorrusClaimId(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CorrusClaimId' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[CorrusClaimId]=["+newValue+"]");
                   dbDB.setCorrusClaimId(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[CorrusClaimId]=["+newValue+"]");
           dbDB.setCorrusClaimId(newValue);
         }
    }
    public String getCorrusClaimId(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CorrusClaimId' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getCorrusClaimId();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getCorrusClaimId();
         }
        return myVal;
    }

    public void setClaimdate(Date newValue)
    {
        this.setClaimdate(newValue,this.UserSecurityID);


    }
    public Date getClaimdate()
    {
        return this.getClaimdate(this.UserSecurityID);
    }

    public void setClaimdate(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Claimdate' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[Claimdate]=["+newValue+"]");
                   dbDB.setClaimdate(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[Claimdate]=["+newValue+"]");
           dbDB.setClaimdate(newValue);
         }
    }
    public Date getClaimdate(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Claimdate' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getClaimdate();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getClaimdate();
         }
        return myVal;
    }

    public void setClientClaimNum(String newValue)
    {
        this.setClientClaimNum(newValue,this.UserSecurityID);


    }
    public String getClientClaimNum()
    {
        return this.getClientClaimNum(this.UserSecurityID);
    }

    public void setClientClaimNum(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ClientClaimNum' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[ClientClaimNum]=["+newValue+"]");
                   dbDB.setClientClaimNum(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[ClientClaimNum]=["+newValue+"]");
           dbDB.setClientClaimNum(newValue);
         }
    }
    public String getClientClaimNum(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ClientClaimNum' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getClientClaimNum();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getClientClaimNum();
         }
        return myVal;
    }

    public void setPrimaryDiagCode(String newValue)
    {
        this.setPrimaryDiagCode(newValue,this.UserSecurityID);


    }
    public String getPrimaryDiagCode()
    {
        return this.getPrimaryDiagCode(this.UserSecurityID);
    }

    public void setPrimaryDiagCode(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PrimaryDiagCode' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[PrimaryDiagCode]=["+newValue+"]");
                   dbDB.setPrimaryDiagCode(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[PrimaryDiagCode]=["+newValue+"]");
           dbDB.setPrimaryDiagCode(newValue);
         }
    }
    public String getPrimaryDiagCode(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='PrimaryDiagCode' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getPrimaryDiagCode();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getPrimaryDiagCode();
         }
        return myVal;
    }

    public void setDiagCode2(String newValue)
    {
        this.setDiagCode2(newValue,this.UserSecurityID);


    }
    public String getDiagCode2()
    {
        return this.getDiagCode2(this.UserSecurityID);
    }

    public void setDiagCode2(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='DiagCode2' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[DiagCode2]=["+newValue+"]");
                   dbDB.setDiagCode2(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[DiagCode2]=["+newValue+"]");
           dbDB.setDiagCode2(newValue);
         }
    }
    public String getDiagCode2(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='DiagCode2' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getDiagCode2();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getDiagCode2();
         }
        return myVal;
    }

    public void setDiagCode3(String newValue)
    {
        this.setDiagCode3(newValue,this.UserSecurityID);


    }
    public String getDiagCode3()
    {
        return this.getDiagCode3(this.UserSecurityID);
    }

    public void setDiagCode3(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='DiagCode3' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[DiagCode3]=["+newValue+"]");
                   dbDB.setDiagCode3(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[DiagCode3]=["+newValue+"]");
           dbDB.setDiagCode3(newValue);
         }
    }
    public String getDiagCode3(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='DiagCode3' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getDiagCode3();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getDiagCode3();
         }
        return myVal;
    }

    public void setClaimstatus(String newValue)
    {
        this.setClaimstatus(newValue,this.UserSecurityID);


    }
    public String getClaimstatus()
    {
        return this.getClaimstatus(this.UserSecurityID);
    }

    public void setClaimstatus(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Claimstatus' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[Claimstatus]=["+newValue+"]");
                   dbDB.setClaimstatus(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[Claimstatus]=["+newValue+"]");
           dbDB.setClaimstatus(newValue);
         }
    }
    public String getClaimstatus(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Claimstatus' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getClaimstatus();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getClaimstatus();
         }
        return myVal;
    }

    public void setCaseManagerFirstName(String newValue)
    {
        this.setCaseManagerFirstName(newValue,this.UserSecurityID);


    }
    public String getCaseManagerFirstName()
    {
        return this.getCaseManagerFirstName(this.UserSecurityID);
    }

    public void setCaseManagerFirstName(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CaseManagerFirstName' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[CaseManagerFirstName]=["+newValue+"]");
                   dbDB.setCaseManagerFirstName(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[CaseManagerFirstName]=["+newValue+"]");
           dbDB.setCaseManagerFirstName(newValue);
         }
    }
    public String getCaseManagerFirstName(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CaseManagerFirstName' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getCaseManagerFirstName();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getCaseManagerFirstName();
         }
        return myVal;
    }

    public void setCaseManagerLastName(String newValue)
    {
        this.setCaseManagerLastName(newValue,this.UserSecurityID);


    }
    public String getCaseManagerLastName()
    {
        return this.getCaseManagerLastName(this.UserSecurityID);
    }

    public void setCaseManagerLastName(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CaseManagerLastName' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[CaseManagerLastName]=["+newValue+"]");
                   dbDB.setCaseManagerLastName(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[CaseManagerLastName]=["+newValue+"]");
           dbDB.setCaseManagerLastName(newValue);
         }
    }
    public String getCaseManagerLastName(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='CaseManagerLastName' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getCaseManagerLastName();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getCaseManagerLastName();
         }
        return myVal;
    }

    public void setAccountReference(String newValue)
    {
        this.setAccountReference(newValue,this.UserSecurityID);


    }
    public String getAccountReference()
    {
        return this.getAccountReference(this.UserSecurityID);
    }

    public void setAccountReference(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AccountReference' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[AccountReference]=["+newValue+"]");
                   dbDB.setAccountReference(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[AccountReference]=["+newValue+"]");
           dbDB.setAccountReference(newValue);
         }
    }
    public String getAccountReference(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AccountReference' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getAccountReference();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getAccountReference();
         }
        return myVal;
    }

    public void setControverted(String newValue)
    {
        this.setControverted(newValue,this.UserSecurityID);


    }
    public String getControverted()
    {
        return this.getControverted(this.UserSecurityID);
    }

    public void setControverted(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Controverted' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[Controverted]=["+newValue+"]");
                   dbDB.setControverted(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[Controverted]=["+newValue+"]");
           dbDB.setControverted(newValue);
         }
    }
    public String getControverted(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Controverted' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getControverted();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getControverted();
         }
        return myVal;
    }

    public void setMMI(Date newValue)
    {
        this.setMMI(newValue,this.UserSecurityID);


    }
    public Date getMMI()
    {
        return this.getMMI(this.UserSecurityID);
    }

    public void setMMI(Date newValue, Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='MMI' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[MMI]=["+newValue+"]");
                   dbDB.setMMI(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[MMI]=["+newValue+"]");
           dbDB.setMMI(newValue);
         }
    }
    public Date getMMI(Integer GroupRefID)
    {
        Date myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='MMI' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getMMI();
               }
               else
               {
                   myVal = null;
               }
            }
        }
        else
        {
                   myVal = dbDB.getMMI();
         }
        return myVal;
    }

    public void setDatabaseID(String newValue)
    {
        this.setDatabaseID(newValue,this.UserSecurityID);


    }
    public String getDatabaseID()
    {
        return this.getDatabaseID(this.UserSecurityID);
    }

    public void setDatabaseID(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='DatabaseID' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[DatabaseID]=["+newValue+"]");
                   dbDB.setDatabaseID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[DatabaseID]=["+newValue+"]");
           dbDB.setDatabaseID(newValue);
         }
    }
    public String getDatabaseID(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='DatabaseID' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getDatabaseID();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getDatabaseID();
         }
        return myVal;
    }

    public void setJurisdicationState(String newValue)
    {
        this.setJurisdicationState(newValue,this.UserSecurityID);


    }
    public String getJurisdicationState()
    {
        return this.getJurisdicationState(this.UserSecurityID);
    }

    public void setJurisdicationState(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='JurisdicationState' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[JurisdicationState]=["+newValue+"]");
                   dbDB.setJurisdicationState(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[JurisdicationState]=["+newValue+"]");
           dbDB.setJurisdicationState(newValue);
         }
    }
    public String getJurisdicationState(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='JurisdicationState' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getJurisdicationState();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getJurisdicationState();
         }
        return myVal;
    }

    public void setAdjusterReferenceID(String newValue)
    {
        this.setAdjusterReferenceID(newValue,this.UserSecurityID);


    }
    public String getAdjusterReferenceID()
    {
        return this.getAdjusterReferenceID(this.UserSecurityID);
    }

    public void setAdjusterReferenceID(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AdjusterReferenceID' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[AdjusterReferenceID]=["+newValue+"]");
                   dbDB.setAdjusterReferenceID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[AdjusterReferenceID]=["+newValue+"]");
           dbDB.setAdjusterReferenceID(newValue);
         }
    }
    public String getAdjusterReferenceID(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AdjusterReferenceID' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getAdjusterReferenceID();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getAdjusterReferenceID();
         }
        return myVal;
    }

    public void setAdjusterFirstName(String newValue)
    {
        this.setAdjusterFirstName(newValue,this.UserSecurityID);


    }
    public String getAdjusterFirstName()
    {
        return this.getAdjusterFirstName(this.UserSecurityID);
    }

    public void setAdjusterFirstName(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AdjusterFirstName' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[AdjusterFirstName]=["+newValue+"]");
                   dbDB.setAdjusterFirstName(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[AdjusterFirstName]=["+newValue+"]");
           dbDB.setAdjusterFirstName(newValue);
         }
    }
    public String getAdjusterFirstName(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AdjusterFirstName' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getAdjusterFirstName();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getAdjusterFirstName();
         }
        return myVal;
    }

    public void setAdjusterLastName(String newValue)
    {
        this.setAdjusterLastName(newValue,this.UserSecurityID);


    }
    public String getAdjusterLastName()
    {
        return this.getAdjusterLastName(this.UserSecurityID);
    }

    public void setAdjusterLastName(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AdjusterLastName' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[AdjusterLastName]=["+newValue+"]");
                   dbDB.setAdjusterLastName(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[AdjusterLastName]=["+newValue+"]");
           dbDB.setAdjusterLastName(newValue);
         }
    }
    public String getAdjusterLastName(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AdjusterLastName' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getAdjusterLastName();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getAdjusterLastName();
         }
        return myVal;
    }

    public void setAdjusterPhoneNumber(String newValue)
    {
        this.setAdjusterPhoneNumber(newValue,this.UserSecurityID);


    }
    public String getAdjusterPhoneNumber()
    {
        return this.getAdjusterPhoneNumber(this.UserSecurityID);
    }

    public void setAdjusterPhoneNumber(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AdjusterPhoneNumber' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[AdjusterPhoneNumber]=["+newValue+"]");
                   dbDB.setAdjusterPhoneNumber(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[AdjusterPhoneNumber]=["+newValue+"]");
           dbDB.setAdjusterPhoneNumber(newValue);
         }
    }
    public String getAdjusterPhoneNumber(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AdjusterPhoneNumber' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getAdjusterPhoneNumber();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getAdjusterPhoneNumber();
         }
        return myVal;
    }

    public void setAdjusterFaxNumber(String newValue)
    {
        this.setAdjusterFaxNumber(newValue,this.UserSecurityID);


    }
    public String getAdjusterFaxNumber()
    {
        return this.getAdjusterFaxNumber(this.UserSecurityID);
    }

    public void setAdjusterFaxNumber(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AdjusterFaxNumber' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[AdjusterFaxNumber]=["+newValue+"]");
                   dbDB.setAdjusterFaxNumber(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[AdjusterFaxNumber]=["+newValue+"]");
           dbDB.setAdjusterFaxNumber(newValue);
         }
    }
    public String getAdjusterFaxNumber(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AdjusterFaxNumber' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getAdjusterFaxNumber();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getAdjusterFaxNumber();
         }
        return myVal;
    }

    public void setAdjusterEmail(String newValue)
    {
        this.setAdjusterEmail(newValue,this.UserSecurityID);


    }
    public String getAdjusterEmail()
    {
        return this.getAdjusterEmail(this.UserSecurityID);
    }

    public void setAdjusterEmail(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AdjusterEmail' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[AdjusterEmail]=["+newValue+"]");
                   dbDB.setAdjusterEmail(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[AdjusterEmail]=["+newValue+"]");
           dbDB.setAdjusterEmail(newValue);
         }
    }
    public String getAdjusterEmail(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AdjusterEmail' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getAdjusterEmail();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getAdjusterEmail();
         }
        return myVal;
    }

    public void setAdjusterCompany(String newValue)
    {
        this.setAdjusterCompany(newValue,this.UserSecurityID);


    }
    public String getAdjusterCompany()
    {
        return this.getAdjusterCompany(this.UserSecurityID);
    }

    public void setAdjusterCompany(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AdjusterCompany' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[AdjusterCompany]=["+newValue+"]");
                   dbDB.setAdjusterCompany(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[AdjusterCompany]=["+newValue+"]");
           dbDB.setAdjusterCompany(newValue);
         }
    }
    public String getAdjusterCompany(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AdjusterCompany' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getAdjusterCompany();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getAdjusterCompany();
         }
        return myVal;
    }

    public void setAdjusterAddress1(String newValue)
    {
        this.setAdjusterAddress1(newValue,this.UserSecurityID);


    }
    public String getAdjusterAddress1()
    {
        return this.getAdjusterAddress1(this.UserSecurityID);
    }

    public void setAdjusterAddress1(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AdjusterAddress1' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[AdjusterAddress1]=["+newValue+"]");
                   dbDB.setAdjusterAddress1(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[AdjusterAddress1]=["+newValue+"]");
           dbDB.setAdjusterAddress1(newValue);
         }
    }
    public String getAdjusterAddress1(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AdjusterAddress1' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getAdjusterAddress1();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getAdjusterAddress1();
         }
        return myVal;
    }

    public void setAdjusterAddress2(String newValue)
    {
        this.setAdjusterAddress2(newValue,this.UserSecurityID);


    }
    public String getAdjusterAddress2()
    {
        return this.getAdjusterAddress2(this.UserSecurityID);
    }

    public void setAdjusterAddress2(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AdjusterAddress2' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[AdjusterAddress2]=["+newValue+"]");
                   dbDB.setAdjusterAddress2(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[AdjusterAddress2]=["+newValue+"]");
           dbDB.setAdjusterAddress2(newValue);
         }
    }
    public String getAdjusterAddress2(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AdjusterAddress2' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getAdjusterAddress2();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getAdjusterAddress2();
         }
        return myVal;
    }

    public void setAdjusterCity(String newValue)
    {
        this.setAdjusterCity(newValue,this.UserSecurityID);


    }
    public String getAdjusterCity()
    {
        return this.getAdjusterCity(this.UserSecurityID);
    }

    public void setAdjusterCity(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AdjusterCity' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[AdjusterCity]=["+newValue+"]");
                   dbDB.setAdjusterCity(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[AdjusterCity]=["+newValue+"]");
           dbDB.setAdjusterCity(newValue);
         }
    }
    public String getAdjusterCity(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AdjusterCity' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getAdjusterCity();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getAdjusterCity();
         }
        return myVal;
    }

    public void setAdjusterState(String newValue)
    {
        this.setAdjusterState(newValue,this.UserSecurityID);


    }
    public String getAdjusterState()
    {
        return this.getAdjusterState(this.UserSecurityID);
    }

    public void setAdjusterState(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AdjusterState' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[AdjusterState]=["+newValue+"]");
                   dbDB.setAdjusterState(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[AdjusterState]=["+newValue+"]");
           dbDB.setAdjusterState(newValue);
         }
    }
    public String getAdjusterState(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AdjusterState' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getAdjusterState();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getAdjusterState();
         }
        return myVal;
    }

    public void setAdjusterZip(String newValue)
    {
        this.setAdjusterZip(newValue,this.UserSecurityID);


    }
    public String getAdjusterZip()
    {
        return this.getAdjusterZip(this.UserSecurityID);
    }

    public void setAdjusterZip(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AdjusterZip' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[AdjusterZip]=["+newValue+"]");
                   dbDB.setAdjusterZip(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[AdjusterZip]=["+newValue+"]");
           dbDB.setAdjusterZip(newValue);
         }
    }
    public String getAdjusterZip(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='AdjusterZip' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getAdjusterZip();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getAdjusterZip();
         }
        return myVal;
    }

    public void setReferringDoctorReferenceID(String newValue)
    {
        this.setReferringDoctorReferenceID(newValue,this.UserSecurityID);


    }
    public String getReferringDoctorReferenceID()
    {
        return this.getReferringDoctorReferenceID(this.UserSecurityID);
    }

    public void setReferringDoctorReferenceID(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ReferringDoctorReferenceID' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[ReferringDoctorReferenceID]=["+newValue+"]");
                   dbDB.setReferringDoctorReferenceID(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[ReferringDoctorReferenceID]=["+newValue+"]");
           dbDB.setReferringDoctorReferenceID(newValue);
         }
    }
    public String getReferringDoctorReferenceID(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ReferringDoctorReferenceID' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getReferringDoctorReferenceID();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getReferringDoctorReferenceID();
         }
        return myVal;
    }

    public void setReferringDoctorNPI(String newValue)
    {
        this.setReferringDoctorNPI(newValue,this.UserSecurityID);


    }
    public String getReferringDoctorNPI()
    {
        return this.getReferringDoctorNPI(this.UserSecurityID);
    }

    public void setReferringDoctorNPI(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ReferringDoctorNPI' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[ReferringDoctorNPI]=["+newValue+"]");
                   dbDB.setReferringDoctorNPI(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[ReferringDoctorNPI]=["+newValue+"]");
           dbDB.setReferringDoctorNPI(newValue);
         }
    }
    public String getReferringDoctorNPI(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ReferringDoctorNPI' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getReferringDoctorNPI();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getReferringDoctorNPI();
         }
        return myVal;
    }

    public void setReferringDoctorMedicalLicense(String newValue)
    {
        this.setReferringDoctorMedicalLicense(newValue,this.UserSecurityID);


    }
    public String getReferringDoctorMedicalLicense()
    {
        return this.getReferringDoctorMedicalLicense(this.UserSecurityID);
    }

    public void setReferringDoctorMedicalLicense(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ReferringDoctorMedicalLicense' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[ReferringDoctorMedicalLicense]=["+newValue+"]");
                   dbDB.setReferringDoctorMedicalLicense(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[ReferringDoctorMedicalLicense]=["+newValue+"]");
           dbDB.setReferringDoctorMedicalLicense(newValue);
         }
    }
    public String getReferringDoctorMedicalLicense(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ReferringDoctorMedicalLicense' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getReferringDoctorMedicalLicense();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getReferringDoctorMedicalLicense();
         }
        return myVal;
    }

    public void setReferringDoctorFirstName(String newValue)
    {
        this.setReferringDoctorFirstName(newValue,this.UserSecurityID);


    }
    public String getReferringDoctorFirstName()
    {
        return this.getReferringDoctorFirstName(this.UserSecurityID);
    }

    public void setReferringDoctorFirstName(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ReferringDoctorFirstName' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[ReferringDoctorFirstName]=["+newValue+"]");
                   dbDB.setReferringDoctorFirstName(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[ReferringDoctorFirstName]=["+newValue+"]");
           dbDB.setReferringDoctorFirstName(newValue);
         }
    }
    public String getReferringDoctorFirstName(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ReferringDoctorFirstName' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getReferringDoctorFirstName();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getReferringDoctorFirstName();
         }
        return myVal;
    }

    public void setReferringDoctorLastName(String newValue)
    {
        this.setReferringDoctorLastName(newValue,this.UserSecurityID);


    }
    public String getReferringDoctorLastName()
    {
        return this.getReferringDoctorLastName(this.UserSecurityID);
    }

    public void setReferringDoctorLastName(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ReferringDoctorLastName' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[ReferringDoctorLastName]=["+newValue+"]");
                   dbDB.setReferringDoctorLastName(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[ReferringDoctorLastName]=["+newValue+"]");
           dbDB.setReferringDoctorLastName(newValue);
         }
    }
    public String getReferringDoctorLastName(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ReferringDoctorLastName' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getReferringDoctorLastName();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getReferringDoctorLastName();
         }
        return myVal;
    }

    public void setReferringDoctorPhoneNumber(String newValue)
    {
        this.setReferringDoctorPhoneNumber(newValue,this.UserSecurityID);


    }
    public String getReferringDoctorPhoneNumber()
    {
        return this.getReferringDoctorPhoneNumber(this.UserSecurityID);
    }

    public void setReferringDoctorPhoneNumber(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ReferringDoctorPhoneNumber' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[ReferringDoctorPhoneNumber]=["+newValue+"]");
                   dbDB.setReferringDoctorPhoneNumber(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[ReferringDoctorPhoneNumber]=["+newValue+"]");
           dbDB.setReferringDoctorPhoneNumber(newValue);
         }
    }
    public String getReferringDoctorPhoneNumber(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ReferringDoctorPhoneNumber' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getReferringDoctorPhoneNumber();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getReferringDoctorPhoneNumber();
         }
        return myVal;
    }

    public void setReferringDoctorFaxNumber(String newValue)
    {
        this.setReferringDoctorFaxNumber(newValue,this.UserSecurityID);


    }
    public String getReferringDoctorFaxNumber()
    {
        return this.getReferringDoctorFaxNumber(this.UserSecurityID);
    }

    public void setReferringDoctorFaxNumber(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ReferringDoctorFaxNumber' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[ReferringDoctorFaxNumber]=["+newValue+"]");
                   dbDB.setReferringDoctorFaxNumber(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[ReferringDoctorFaxNumber]=["+newValue+"]");
           dbDB.setReferringDoctorFaxNumber(newValue);
         }
    }
    public String getReferringDoctorFaxNumber(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ReferringDoctorFaxNumber' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getReferringDoctorFaxNumber();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getReferringDoctorFaxNumber();
         }
        return myVal;
    }

    public void setReferringDoctorEmail(String newValue)
    {
        this.setReferringDoctorEmail(newValue,this.UserSecurityID);


    }
    public String getReferringDoctorEmail()
    {
        return this.getReferringDoctorEmail(this.UserSecurityID);
    }

    public void setReferringDoctorEmail(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ReferringDoctorEmail' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[ReferringDoctorEmail]=["+newValue+"]");
                   dbDB.setReferringDoctorEmail(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[ReferringDoctorEmail]=["+newValue+"]");
           dbDB.setReferringDoctorEmail(newValue);
         }
    }
    public String getReferringDoctorEmail(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ReferringDoctorEmail' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getReferringDoctorEmail();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getReferringDoctorEmail();
         }
        return myVal;
    }

    public void setReferringDoctorCompany(String newValue)
    {
        this.setReferringDoctorCompany(newValue,this.UserSecurityID);


    }
    public String getReferringDoctorCompany()
    {
        return this.getReferringDoctorCompany(this.UserSecurityID);
    }

    public void setReferringDoctorCompany(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ReferringDoctorCompany' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[ReferringDoctorCompany]=["+newValue+"]");
                   dbDB.setReferringDoctorCompany(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[ReferringDoctorCompany]=["+newValue+"]");
           dbDB.setReferringDoctorCompany(newValue);
         }
    }
    public String getReferringDoctorCompany(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ReferringDoctorCompany' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getReferringDoctorCompany();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getReferringDoctorCompany();
         }
        return myVal;
    }

    public void setReferringDoctorAddress1(String newValue)
    {
        this.setReferringDoctorAddress1(newValue,this.UserSecurityID);


    }
    public String getReferringDoctorAddress1()
    {
        return this.getReferringDoctorAddress1(this.UserSecurityID);
    }

    public void setReferringDoctorAddress1(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ReferringDoctorAddress1' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[ReferringDoctorAddress1]=["+newValue+"]");
                   dbDB.setReferringDoctorAddress1(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[ReferringDoctorAddress1]=["+newValue+"]");
           dbDB.setReferringDoctorAddress1(newValue);
         }
    }
    public String getReferringDoctorAddress1(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ReferringDoctorAddress1' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getReferringDoctorAddress1();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getReferringDoctorAddress1();
         }
        return myVal;
    }

    public void setReferringDoctorAddress2(String newValue)
    {
        this.setReferringDoctorAddress2(newValue,this.UserSecurityID);


    }
    public String getReferringDoctorAddress2()
    {
        return this.getReferringDoctorAddress2(this.UserSecurityID);
    }

    public void setReferringDoctorAddress2(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ReferringDoctorAddress2' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[ReferringDoctorAddress2]=["+newValue+"]");
                   dbDB.setReferringDoctorAddress2(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[ReferringDoctorAddress2]=["+newValue+"]");
           dbDB.setReferringDoctorAddress2(newValue);
         }
    }
    public String getReferringDoctorAddress2(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ReferringDoctorAddress2' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getReferringDoctorAddress2();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getReferringDoctorAddress2();
         }
        return myVal;
    }

    public void setReferringDoctorCity(String newValue)
    {
        this.setReferringDoctorCity(newValue,this.UserSecurityID);


    }
    public String getReferringDoctorCity()
    {
        return this.getReferringDoctorCity(this.UserSecurityID);
    }

    public void setReferringDoctorCity(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ReferringDoctorCity' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[ReferringDoctorCity]=["+newValue+"]");
                   dbDB.setReferringDoctorCity(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[ReferringDoctorCity]=["+newValue+"]");
           dbDB.setReferringDoctorCity(newValue);
         }
    }
    public String getReferringDoctorCity(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ReferringDoctorCity' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getReferringDoctorCity();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getReferringDoctorCity();
         }
        return myVal;
    }

    public void setReferringDoctorState(String newValue)
    {
        this.setReferringDoctorState(newValue,this.UserSecurityID);


    }
    public String getReferringDoctorState()
    {
        return this.getReferringDoctorState(this.UserSecurityID);
    }

    public void setReferringDoctorState(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ReferringDoctorState' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[ReferringDoctorState]=["+newValue+"]");
                   dbDB.setReferringDoctorState(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[ReferringDoctorState]=["+newValue+"]");
           dbDB.setReferringDoctorState(newValue);
         }
    }
    public String getReferringDoctorState(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ReferringDoctorState' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getReferringDoctorState();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getReferringDoctorState();
         }
        return myVal;
    }

    public void setReferringDoctorZip(String newValue)
    {
        this.setReferringDoctorZip(newValue,this.UserSecurityID);


    }
    public String getReferringDoctorZip()
    {
        return this.getReferringDoctorZip(this.UserSecurityID);
    }

    public void setReferringDoctorZip(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ReferringDoctorZip' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[ReferringDoctorZip]=["+newValue+"]");
                   dbDB.setReferringDoctorZip(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[ReferringDoctorZip]=["+newValue+"]");
           dbDB.setReferringDoctorZip(newValue);
         }
    }
    public String getReferringDoctorZip(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='ReferringDoctorZip' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getReferringDoctorZip();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getReferringDoctorZip();
         }
        return myVal;
    }

    public void setNotes(String newValue)
    {
        this.setNotes(newValue,this.UserSecurityID);


    }
    public String getNotes()
    {
        return this.getNotes(this.UserSecurityID);
    }

    public void setNotes(String newValue, Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Notes' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {

                   this.AuditVector.addElement("[Notes]=["+newValue+"]");
                   dbDB.setNotes(newValue);
               }
            }
        }
        else
        {

           this.AuditVector.addElement("[Notes]=["+newValue+"]");
           dbDB.setNotes(newValue);
         }
    }
    public String getNotes(Integer GroupRefID)
    {
        String myVal = null;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='Notes' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()>=1)
               {
                   myVal = dbDB.getNotes();
               }
               else
               {
                   myVal = ConfigurationInformation.sDisplayHidden;
               }
            }
        }
        else
        {
                   myVal = dbDB.getNotes();
         }
        return myVal;
    }

    public void setRequiredFieldList(java.util.Hashtable requiredFieldList)
	 {

        this.requiredFieldList = requiredFieldList;
    }

    public java.util.Hashtable getRequiredFieldList()
    {
        return this.requiredFieldList;
    }
    public void setExpiredFieldList(java.util.Hashtable expiredFieldList)
    {
        this.expiredFieldList = expiredFieldList;
    }

    public void setEnglishFieldList(java.util.Hashtable englishFieldList)
    {
        this.englishFieldList = englishFieldList;
    }

    public java.util.Hashtable getExpiredFieldList()
    {
        return this.expiredFieldList;
    }
    public boolean isExpired(java.util.Date myDate, int daysVector)
    {
       return isExpired(myDate,new java.util.Date(),daysVector);
    }


    public boolean isRead(String fieldN, Integer GroupRefID)
    {
        boolean myVal = false;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='"+fieldN+"' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==1)
               {
                   myVal = true;
               }
            }
        }
        else
        {

           if (iSecurityCheck.intValue()>=1)
           {
                  myVal = true;
           }
        }
        return myVal;
    }


    public boolean isWrite(String fieldN, Integer GroupRefID)
    {
        boolean myVal = false;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='"+fieldN+"' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getAccessLevel().intValue()==2)
               {
                   myVal = true;
               }
            }
        }
        else
        {

           if (iSecurityCheck.intValue()>=2)
           {
                  myVal = true;
           }
        }
        return myVal;
    }


    public boolean isExpired(java.util.Date myDate, java.util.Date refDate,int daysVector)
    {
        java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat("yyyy-MM-dd");
        boolean myVal = true;
        try
        {
            if (myDate.after(dbdf.parse("1800-01-10")))
            {
                if (myDate.after(dateChange(refDate,daysVector)))
                {
                    myVal=false;
                }
            }
            else
            {
                myVal = false;
            }
        }
        catch (Exception Datee)
        {
        }
        return myVal;
    }

    public java.util.Date dateChange(java.util.Date myD, int dayChange)
    {
        java.util.Calendar calendar = java.util.Calendar.getInstance();
        calendar.setTime(myD);
        calendar.add(java.util.Calendar.DATE, dayChange);
        java.util.Date resultAsDate = new java.util.Date(calendar.getTime().getTime());
        return resultAsDate;
    }

    public boolean isRequired(String fieldN)
    {
        return isRequired(fieldN, new Integer("1"));
    }

    public boolean isRequired(String fieldN, Integer GroupRefID)
    {
        boolean myVal = false;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='"+fieldN+"' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getRequiredID().intValue()==1)
               {
                   myVal = true;
               }
            }
        }
        else
        {
            if (this.getRequiredFieldList().containsKey(fieldN))
            {
                myVal = true;
            }
        }
        return myVal;
    }
    public boolean isExpiredCheck(String fieldN)
    {
        return isExpiredCheck(fieldN, new Integer ("1"));
    }

    public boolean isExpiredCheck(String fieldN, Integer GroupRefID)
    {
        boolean myVal = false;
        if (this.runFieldSecurity)
        {
            bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='"+fieldN+"' AND TableName='tNIM3_EligibilityReference'" ,"");
            bltFieldSecurity        working_bltFieldSecurity;
            ListElement         leCurrentElement;
            Enumeration eList = myFSL.elements();
            if (eList.hasMoreElements())
            {
               leCurrentElement    = (ListElement) eList.nextElement();
               working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
               if (working_bltFieldSecurity.getTrackedID().intValue()==1)
               {
                   myVal = true;
               }
            }
        }
        else
        {
            if (this.getExpiredFieldList().containsKey(fieldN))
           {
                myVal = true;
            }
        }
        return myVal;
    }

    public void makeRequiredHash()
    {
       makeRequiredHash(new Integer(1));
    }

    public void makeRequiredHash(Integer GroupRefID)
    {

        java.util.Hashtable newHash = new java.util.Hashtable();
        if (runFieldSecurity)
        {
            java.util.Enumeration myKeys = englishFieldList.keys(); 
            while (myKeys.hasMoreElements()) 
            {
                 String myElement = (String) myKeys.nextElement();
                 bltFieldSecurity_List myFSL = new bltFieldSecurity_List(GroupRefID,"FieldName='"+myElement+"' AND TableName='tNIM3_EligibilityReference'" ,"");
                 bltFieldSecurity        working_bltFieldSecurity;
                 ListElement         leCurrentElement;
                 Enumeration eList = myFSL.elements();
                 if (eList.hasMoreElements())
                 {
                     leCurrentElement    = (ListElement) eList.nextElement();
                     working_bltFieldSecurity  = (bltFieldSecurity) leCurrentElement.getObject();
                     if (working_bltFieldSecurity.getRequiredID().intValue()==1)
                     {
                         newHash.put(myElement,new Boolean(true));
                     }
                 }
            }
        }
        else
        {
            newHash.put("PayerID",new Boolean(true));
            newHash.put("CaseClaimNumber",new Boolean(true));
            newHash.put("EmployerName",new Boolean(true));
            newHash.put("DateCaseOpened",new Boolean(true));
            newHash.put("DateOfInjury",new Boolean(true));
            newHash.put("AttorneyFirstName",new Boolean(true));
            newHash.put("AttorneyLastName",new Boolean(true));
            newHash.put("AttorneyWorkPhone",new Boolean(true));
            newHash.put("AttorneyFax",new Boolean(true));
            newHash.put("PatientAccountNumber",new Boolean(true));
            newHash.put("PatientGender",new Boolean(true));
            newHash.put("Claimdate",new Boolean(true));
            newHash.put("MMI",new Boolean(true));
        }
        this.setRequiredFieldList(newHash);
    }

    public void makeEnglishHash()
    {

        java.util.Hashtable newHash = new java.util.Hashtable();        newHash.put("UniqueCreateDate","Item Create Date");
        newHash.put("UniqueModifyDate","Item Modify Date");
        newHash.put("UniqueModifyComments","Item Modification Comments");
        newHash.put("PayerID","PayerID");
        newHash.put("CaseClaimNumber","CaseClaimNumber");
        newHash.put("ClientPayerID","ClientPayerID");
        newHash.put("PayerIndicator","PayerIndicator");
        newHash.put("EmployerName","EmployerName");
        newHash.put("EmployerPhone","EmployerPhone");
        newHash.put("EmployerAddress1","EmployerAddress1");
        newHash.put("EmployerAddress2","EmployerAddress2");
        newHash.put("EmployerCity","EmployerCity");
        newHash.put("EmployerCounty","EmployerCounty");
        newHash.put("EmployerState","EmployerState");
        newHash.put("EmployerZip","EmployerZip");
        newHash.put("DateCaseOpened","DateCaseOpened");
        newHash.put("DateOfInjury","DateOfInjury");
        newHash.put("AttorneyFirstName","AttorneyFirstName");
        newHash.put("AttorneyLastName","AttorneyLastName");
        newHash.put("AttorneyWorkPhone","AttorneyWorkPhone");
        newHash.put("AttorneyFax","AttorneyFax");
        newHash.put("PatientAccountNumber","PatientAccountNumber");
        newHash.put("PatientFirstName","PatientFirstName");
        newHash.put("PatientLastName","PatientLastName");
        newHash.put("PatientDOB","PatientDOB");
        newHash.put("PatientSSN","PatientSSN");
        newHash.put("PatientGender","PatientGender");
        newHash.put("PatientAddress1","PatientAddress1");
        newHash.put("PatientAddress2","PatientAddress2");
        newHash.put("PatientCity","PatientCity");
        newHash.put("PatientState","PatientState");
        newHash.put("PatientZip","PatientZip");
        newHash.put("PatientHomePhone","PatientHomePhone");
        newHash.put("PatientWorkPhone","PatientWorkPhone");
        newHash.put("PatientCellPhone","PatientCellPhone");
        newHash.put("PatientEmail","PatientEmail");
        newHash.put("CorrusClaimId","CorrusClaimId");
        newHash.put("Claimdate","Claimdate");
        newHash.put("ClientClaimNum","ClientClaimNum");
        newHash.put("PrimaryDiagCode","PrimaryDiagCode");
        newHash.put("DiagCode2","DiagCode2");
        newHash.put("DiagCode3","DiagCode3");
        newHash.put("Claimstatus","Claimstatus");
        newHash.put("CaseManagerFirstName","CaseManagerFirstName");
        newHash.put("CaseManagerLastName","CaseManagerLastName");
        newHash.put("AccountReference","AccountReference");
        newHash.put("Controverted","Controverted");
        newHash.put("MMI","MMI");
        newHash.put("DatabaseID","DatabaseID");
        newHash.put("JurisdicationState","JurisdicationState");
        newHash.put("AdjusterReferenceID","AdjusterReferenceID");
        newHash.put("AdjusterFirstName","AdjusterFirstName");
        newHash.put("AdjusterLastName","AdjusterLastName");
        newHash.put("AdjusterPhoneNumber","AdjusterPhoneNumber");
        newHash.put("AdjusterFaxNumber","AdjusterFaxNumber");
        newHash.put("AdjusterEmail","AdjusterEmail");
        newHash.put("AdjusterCompany","AdjusterCompany");
        newHash.put("AdjusterAddress1","AdjusterAddress1");
        newHash.put("AdjusterAddress2","AdjusterAddress2");
        newHash.put("AdjusterCity","AdjusterCity");
        newHash.put("AdjusterState","AdjusterState");
        newHash.put("AdjusterZip","AdjusterZip");
        newHash.put("ReferringDoctorReferenceID","ReferringDoctorReferenceID");
        newHash.put("ReferringDoctorNPI","ReferringDoctorNPI");
        newHash.put("ReferringDoctorMedicalLicense","ReferringDoctorMedicalLicense");
        newHash.put("ReferringDoctorFirstName","ReferringDoctorFirstName");
        newHash.put("ReferringDoctorLastName","ReferringDoctorLastName");
        newHash.put("ReferringDoctorPhoneNumber","ReferringDoctorPhoneNumber");
        newHash.put("ReferringDoctorFaxNumber","ReferringDoctorFaxNumber");
        newHash.put("ReferringDoctorEmail","ReferringDoctorEmail");
        newHash.put("ReferringDoctorCompany","ReferringDoctorCompany");
        newHash.put("ReferringDoctorAddress1","ReferringDoctorAddress1");
        newHash.put("ReferringDoctorAddress2","ReferringDoctorAddress2");
        newHash.put("ReferringDoctorCity","ReferringDoctorCity");
        newHash.put("ReferringDoctorState","ReferringDoctorState");
        newHash.put("ReferringDoctorZip","ReferringDoctorZip");
        newHash.put("Notes","Notes");

        this.setEnglishFieldList(newHash);
    }
    public void makeExpiredHash()
    {

        java.util.Hashtable newHash = new java.util.Hashtable();
        this.setExpiredFieldList(newHash);
    }

    public java.util.Hashtable englishFieldList = new java.util.Hashtable();
    public java.util.Hashtable requiredFieldList = new java.util.Hashtable();
    public java.util.Hashtable expiredFieldList = new java.util.Hashtable();

    public boolean isComplete()
    {
	     boolean myVal = true;
	     java.util.Enumeration myKeys = this.getRequiredFieldList().keys(); 
	     while (myKeys.hasMoreElements()) 
	     {
	         String myElement = (String) myKeys.nextElement();
	         if (!isComplete(myElement))
	         {
	             myVal = false;
	         }
	     }
	     return myVal;
    }

    public String getEnglish(String fieldN)
    {
        String myVal = null;
        if (this.englishFieldList.containsKey(fieldN))
        {
            myVal = (String)this.englishFieldList.get(fieldN);
        }
        return myVal;
    }
    public java.util.Vector isComplete(int daysVector)
    {
	     boolean myVal = false;
	     java.util.Enumeration myKeys = this.getExpiredFieldList().keys(); 
	     java.util.Vector myExpiredList = new java.util.Vector(); 
	     while (myKeys.hasMoreElements()) 
	     {
	         String myElement = (String) myKeys.nextElement();
	         if (isExpired(myElement,daysVector))
	         {
	             myVal = true;
	             myExpiredList.addElement(myElement);
	         }
	     }
	     return myExpiredList;
    }

    public void setField(String fieldN, Object fieldV)
    {
     boolean myVal = false;
     try
     {
        java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat("yyyy-MM-dd");

        if (fieldN.equals("EligibilityReferenceID"))
        {
             this.setEligibilityReferenceID((Integer)fieldV);
        }

        else if (fieldN.equals("UniqueCreateDate"))
        {
            this.setUniqueCreateDate((java.util.Date)fieldV);
        }

        else if (fieldN.equals("UniqueModifyDate"))
        {
            this.setUniqueModifyDate((java.util.Date)fieldV);
        }

        else if (fieldN.equals("UniqueModifyComments"))
        {
            this.setUniqueModifyComments((String)fieldV);
        }

        else if (fieldN.equals("PayerID"))
        {
             this.setPayerID((Integer)fieldV);
        }

        else if (fieldN.equals("CaseClaimNumber"))
        {
            this.setCaseClaimNumber((String)fieldV);
        }

        else if (fieldN.equals("ClientPayerID"))
        {
            this.setClientPayerID((String)fieldV);
        }

        else if (fieldN.equals("PayerIndicator"))
        {
            this.setPayerIndicator((String)fieldV);
        }

        else if (fieldN.equals("EmployerName"))
        {
            this.setEmployerName((String)fieldV);
        }

        else if (fieldN.equals("EmployerPhone"))
        {
            this.setEmployerPhone((String)fieldV);
        }

        else if (fieldN.equals("EmployerAddress1"))
        {
            this.setEmployerAddress1((String)fieldV);
        }

        else if (fieldN.equals("EmployerAddress2"))
        {
            this.setEmployerAddress2((String)fieldV);
        }

        else if (fieldN.equals("EmployerCity"))
        {
            this.setEmployerCity((String)fieldV);
        }

        else if (fieldN.equals("EmployerCounty"))
        {
            this.setEmployerCounty((String)fieldV);
        }

        else if (fieldN.equals("EmployerState"))
        {
            this.setEmployerState((String)fieldV);
        }

        else if (fieldN.equals("EmployerZip"))
        {
            this.setEmployerZip((String)fieldV);
        }

        else if (fieldN.equals("DateCaseOpened"))
        {
            this.setDateCaseOpened((java.util.Date)fieldV);
        }

        else if (fieldN.equals("DateOfInjury"))
        {
            this.setDateOfInjury((java.util.Date)fieldV);
        }

        else if (fieldN.equals("AttorneyFirstName"))
        {
            this.setAttorneyFirstName((String)fieldV);
        }

        else if (fieldN.equals("AttorneyLastName"))
        {
            this.setAttorneyLastName((String)fieldV);
        }

        else if (fieldN.equals("AttorneyWorkPhone"))
        {
            this.setAttorneyWorkPhone((String)fieldV);
        }

        else if (fieldN.equals("AttorneyFax"))
        {
            this.setAttorneyFax((String)fieldV);
        }

        else if (fieldN.equals("PatientAccountNumber"))
        {
            this.setPatientAccountNumber((String)fieldV);
        }

        else if (fieldN.equals("PatientFirstName"))
        {
            this.setPatientFirstName((String)fieldV);
        }

        else if (fieldN.equals("PatientLastName"))
        {
            this.setPatientLastName((String)fieldV);
        }

        else if (fieldN.equals("PatientDOB"))
        {
            this.setPatientDOB((java.util.Date)fieldV);
        }

        else if (fieldN.equals("PatientSSN"))
        {
            this.setPatientSSN((String)fieldV);
        }

        else if (fieldN.equals("PatientGender"))
        {
            this.setPatientGender((String)fieldV);
        }

        else if (fieldN.equals("PatientAddress1"))
        {
            this.setPatientAddress1((String)fieldV);
        }

        else if (fieldN.equals("PatientAddress2"))
        {
            this.setPatientAddress2((String)fieldV);
        }

        else if (fieldN.equals("PatientCity"))
        {
            this.setPatientCity((String)fieldV);
        }

        else if (fieldN.equals("PatientState"))
        {
            this.setPatientState((String)fieldV);
        }

        else if (fieldN.equals("PatientZip"))
        {
            this.setPatientZip((String)fieldV);
        }

        else if (fieldN.equals("PatientHomePhone"))
        {
            this.setPatientHomePhone((String)fieldV);
        }

        else if (fieldN.equals("PatientWorkPhone"))
        {
            this.setPatientWorkPhone((String)fieldV);
        }

        else if (fieldN.equals("PatientCellPhone"))
        {
            this.setPatientCellPhone((String)fieldV);
        }

        else if (fieldN.equals("PatientEmail"))
        {
            this.setPatientEmail((String)fieldV);
        }

        else if (fieldN.equals("CorrusClaimId"))
        {
            this.setCorrusClaimId((String)fieldV);
        }

        else if (fieldN.equals("Claimdate"))
        {
            this.setClaimdate((java.util.Date)fieldV);
        }

        else if (fieldN.equals("ClientClaimNum"))
        {
            this.setClientClaimNum((String)fieldV);
        }

        else if (fieldN.equals("PrimaryDiagCode"))
        {
            this.setPrimaryDiagCode((String)fieldV);
        }

        else if (fieldN.equals("DiagCode2"))
        {
            this.setDiagCode2((String)fieldV);
        }

        else if (fieldN.equals("DiagCode3"))
        {
            this.setDiagCode3((String)fieldV);
        }

        else if (fieldN.equals("Claimstatus"))
        {
            this.setClaimstatus((String)fieldV);
        }

        else if (fieldN.equals("CaseManagerFirstName"))
        {
            this.setCaseManagerFirstName((String)fieldV);
        }

        else if (fieldN.equals("CaseManagerLastName"))
        {
            this.setCaseManagerLastName((String)fieldV);
        }

        else if (fieldN.equals("AccountReference"))
        {
            this.setAccountReference((String)fieldV);
        }

        else if (fieldN.equals("Controverted"))
        {
            this.setControverted((String)fieldV);
        }

        else if (fieldN.equals("MMI"))
        {
            this.setMMI((java.util.Date)fieldV);
        }

        else if (fieldN.equals("DatabaseID"))
        {
            this.setDatabaseID((String)fieldV);
        }

        else if (fieldN.equals("JurisdicationState"))
        {
            this.setJurisdicationState((String)fieldV);
        }

        else if (fieldN.equals("AdjusterReferenceID"))
        {
            this.setAdjusterReferenceID((String)fieldV);
        }

        else if (fieldN.equals("AdjusterFirstName"))
        {
            this.setAdjusterFirstName((String)fieldV);
        }

        else if (fieldN.equals("AdjusterLastName"))
        {
            this.setAdjusterLastName((String)fieldV);
        }

        else if (fieldN.equals("AdjusterPhoneNumber"))
        {
            this.setAdjusterPhoneNumber((String)fieldV);
        }

        else if (fieldN.equals("AdjusterFaxNumber"))
        {
            this.setAdjusterFaxNumber((String)fieldV);
        }

        else if (fieldN.equals("AdjusterEmail"))
        {
            this.setAdjusterEmail((String)fieldV);
        }

        else if (fieldN.equals("AdjusterCompany"))
        {
            this.setAdjusterCompany((String)fieldV);
        }

        else if (fieldN.equals("AdjusterAddress1"))
        {
            this.setAdjusterAddress1((String)fieldV);
        }

        else if (fieldN.equals("AdjusterAddress2"))
        {
            this.setAdjusterAddress2((String)fieldV);
        }

        else if (fieldN.equals("AdjusterCity"))
        {
            this.setAdjusterCity((String)fieldV);
        }

        else if (fieldN.equals("AdjusterState"))
        {
            this.setAdjusterState((String)fieldV);
        }

        else if (fieldN.equals("AdjusterZip"))
        {
            this.setAdjusterZip((String)fieldV);
        }

        else if (fieldN.equals("ReferringDoctorReferenceID"))
        {
            this.setReferringDoctorReferenceID((String)fieldV);
        }

        else if (fieldN.equals("ReferringDoctorNPI"))
        {
            this.setReferringDoctorNPI((String)fieldV);
        }

        else if (fieldN.equals("ReferringDoctorMedicalLicense"))
        {
            this.setReferringDoctorMedicalLicense((String)fieldV);
        }

        else if (fieldN.equals("ReferringDoctorFirstName"))
        {
            this.setReferringDoctorFirstName((String)fieldV);
        }

        else if (fieldN.equals("ReferringDoctorLastName"))
        {
            this.setReferringDoctorLastName((String)fieldV);
        }

        else if (fieldN.equals("ReferringDoctorPhoneNumber"))
        {
            this.setReferringDoctorPhoneNumber((String)fieldV);
        }

        else if (fieldN.equals("ReferringDoctorFaxNumber"))
        {
            this.setReferringDoctorFaxNumber((String)fieldV);
        }

        else if (fieldN.equals("ReferringDoctorEmail"))
        {
            this.setReferringDoctorEmail((String)fieldV);
        }

        else if (fieldN.equals("ReferringDoctorCompany"))
        {
            this.setReferringDoctorCompany((String)fieldV);
        }

        else if (fieldN.equals("ReferringDoctorAddress1"))
        {
            this.setReferringDoctorAddress1((String)fieldV);
        }

        else if (fieldN.equals("ReferringDoctorAddress2"))
        {
            this.setReferringDoctorAddress2((String)fieldV);
        }

        else if (fieldN.equals("ReferringDoctorCity"))
        {
            this.setReferringDoctorCity((String)fieldV);
        }

        else if (fieldN.equals("ReferringDoctorState"))
        {
            this.setReferringDoctorState((String)fieldV);
        }

        else if (fieldN.equals("ReferringDoctorZip"))
        {
            this.setReferringDoctorZip((String)fieldV);
        }

        else if (fieldN.equals("Notes"))
        {
            this.setNotes((String)fieldV);
        }

	    }
	    catch(Exception e2)
	    {}
	    //return myVal;
	}

    public String getFieldType(String fieldN)
    {
     String  myVal = null;
     try
     {
        java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat("yyyy-MM-dd");

        if (fieldN.equals("EligibilityReferenceID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("UniqueCreateDate"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("UniqueModifyDate"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("UniqueModifyComments"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("PayerID"))
        {
            myVal = "Integer";
        }

        else if (fieldN.equals("CaseClaimNumber"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("ClientPayerID"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("PayerIndicator"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("EmployerName"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("EmployerPhone"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("EmployerAddress1"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("EmployerAddress2"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("EmployerCity"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("EmployerCounty"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("EmployerState"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("EmployerZip"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("DateCaseOpened"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("DateOfInjury"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("AttorneyFirstName"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("AttorneyLastName"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("AttorneyWorkPhone"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("AttorneyFax"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("PatientAccountNumber"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("PatientFirstName"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("PatientLastName"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("PatientDOB"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("PatientSSN"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("PatientGender"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("PatientAddress1"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("PatientAddress2"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("PatientCity"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("PatientState"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("PatientZip"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("PatientHomePhone"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("PatientWorkPhone"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("PatientCellPhone"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("PatientEmail"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("CorrusClaimId"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("Claimdate"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("ClientClaimNum"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("PrimaryDiagCode"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("DiagCode2"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("DiagCode3"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("Claimstatus"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("CaseManagerFirstName"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("CaseManagerLastName"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("AccountReference"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("Controverted"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("MMI"))
        {
            myVal = "Date";
        }

        else if (fieldN.equals("DatabaseID"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("JurisdicationState"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("AdjusterReferenceID"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("AdjusterFirstName"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("AdjusterLastName"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("AdjusterPhoneNumber"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("AdjusterFaxNumber"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("AdjusterEmail"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("AdjusterCompany"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("AdjusterAddress1"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("AdjusterAddress2"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("AdjusterCity"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("AdjusterState"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("AdjusterZip"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("ReferringDoctorReferenceID"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("ReferringDoctorNPI"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("ReferringDoctorMedicalLicense"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("ReferringDoctorFirstName"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("ReferringDoctorLastName"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("ReferringDoctorPhoneNumber"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("ReferringDoctorFaxNumber"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("ReferringDoctorEmail"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("ReferringDoctorCompany"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("ReferringDoctorAddress1"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("ReferringDoctorAddress2"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("ReferringDoctorCity"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("ReferringDoctorState"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("ReferringDoctorZip"))
        {
            myVal = "String";
        }

        else if (fieldN.equals("Notes"))
        {
            myVal = "String";
        }

	    }
	    catch(Exception e2)
	    {}
	    return myVal;
	}

    public boolean isComplete(String fieldN)
    {
	     boolean myVal = false;
	     try
	     {
        java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat("yyyy-MM-dd");

	     if (fieldN.equals("EligibilityReferenceID"))

	     {
                    if (this.getEligibilityReferenceID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("UniqueCreateDate"))

	     {
                    if (this.getUniqueCreateDate().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("UniqueModifyDate"))

	     {
                    if (this.getUniqueModifyDate().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("UniqueModifyComments"))

	     {
                    if (this.getUniqueModifyComments().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("PayerID"))

	     {
                    if (this.getPayerID().equals(new Integer("0")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("CaseClaimNumber"))

	     {
                    if (this.getCaseClaimNumber().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("ClientPayerID"))

	     {
                    if (this.getClientPayerID().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("PayerIndicator"))

	     {
                    if (this.getPayerIndicator().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("EmployerName"))

	     {
                    if (this.getEmployerName().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("EmployerPhone"))

	     {
                    if (this.getEmployerPhone().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("EmployerAddress1"))

	     {
                    if (this.getEmployerAddress1().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("EmployerAddress2"))

	     {
                    if (this.getEmployerAddress2().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("EmployerCity"))

	     {
                    if (this.getEmployerCity().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("EmployerCounty"))

	     {
                    if (this.getEmployerCounty().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("EmployerState"))

	     {
                    if (this.getEmployerState().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("EmployerZip"))

	     {
                    if (this.getEmployerZip().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("DateCaseOpened"))

	     {
                    if (this.getDateCaseOpened().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("DateOfInjury"))

	     {
                    if (this.getDateOfInjury().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("AttorneyFirstName"))

	     {
                    if (this.getAttorneyFirstName().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("AttorneyLastName"))

	     {
                    if (this.getAttorneyLastName().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("AttorneyWorkPhone"))

	     {
                    if (this.getAttorneyWorkPhone().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("AttorneyFax"))

	     {
                    if (this.getAttorneyFax().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("PatientAccountNumber"))

	     {
                    if (this.getPatientAccountNumber().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("PatientFirstName"))

	     {
                    if (this.getPatientFirstName().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("PatientLastName"))

	     {
                    if (this.getPatientLastName().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("PatientDOB"))

	     {
                    if (this.getPatientDOB().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("PatientSSN"))

	     {
                    if (this.getPatientSSN().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("PatientGender"))

	     {
                    if (this.getPatientGender().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("PatientAddress1"))

	     {
                    if (this.getPatientAddress1().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("PatientAddress2"))

	     {
                    if (this.getPatientAddress2().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("PatientCity"))

	     {
                    if (this.getPatientCity().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("PatientState"))

	     {
                    if (this.getPatientState().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("PatientZip"))

	     {
                    if (this.getPatientZip().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("PatientHomePhone"))

	     {
                    if (this.getPatientHomePhone().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("PatientWorkPhone"))

	     {
                    if (this.getPatientWorkPhone().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("PatientCellPhone"))

	     {
                    if (this.getPatientCellPhone().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("PatientEmail"))

	     {
                    if (this.getPatientEmail().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("CorrusClaimId"))

	     {
                    if (this.getCorrusClaimId().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("Claimdate"))

	     {
                    if (this.getClaimdate().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("ClientClaimNum"))

	     {
                    if (this.getClientClaimNum().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("PrimaryDiagCode"))

	     {
                    if (this.getPrimaryDiagCode().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("DiagCode2"))

	     {
                    if (this.getDiagCode2().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("DiagCode3"))

	     {
                    if (this.getDiagCode3().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("Claimstatus"))

	     {
                    if (this.getClaimstatus().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("CaseManagerFirstName"))

	     {
                    if (this.getCaseManagerFirstName().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("CaseManagerLastName"))

	     {
                    if (this.getCaseManagerLastName().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("AccountReference"))

	     {
                    if (this.getAccountReference().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("Controverted"))

	     {
                    if (this.getControverted().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("MMI"))

	     {
                    if (this.getMMI().equals(dbdf.parse("1800-01-01")))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("DatabaseID"))

	     {
                    if (this.getDatabaseID().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("JurisdicationState"))

	     {
                    if (this.getJurisdicationState().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("AdjusterReferenceID"))

	     {
                    if (this.getAdjusterReferenceID().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("AdjusterFirstName"))

	     {
                    if (this.getAdjusterFirstName().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("AdjusterLastName"))

	     {
                    if (this.getAdjusterLastName().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("AdjusterPhoneNumber"))

	     {
                    if (this.getAdjusterPhoneNumber().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("AdjusterFaxNumber"))

	     {
                    if (this.getAdjusterFaxNumber().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("AdjusterEmail"))

	     {
                    if (this.getAdjusterEmail().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("AdjusterCompany"))

	     {
                    if (this.getAdjusterCompany().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("AdjusterAddress1"))

	     {
                    if (this.getAdjusterAddress1().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("AdjusterAddress2"))

	     {
                    if (this.getAdjusterAddress2().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("AdjusterCity"))

	     {
                    if (this.getAdjusterCity().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("AdjusterState"))

	     {
                    if (this.getAdjusterState().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("AdjusterZip"))

	     {
                    if (this.getAdjusterZip().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("ReferringDoctorReferenceID"))

	     {
                    if (this.getReferringDoctorReferenceID().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("ReferringDoctorNPI"))

	     {
                    if (this.getReferringDoctorNPI().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("ReferringDoctorMedicalLicense"))

	     {
                    if (this.getReferringDoctorMedicalLicense().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("ReferringDoctorFirstName"))

	     {
                    if (this.getReferringDoctorFirstName().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("ReferringDoctorLastName"))

	     {
                    if (this.getReferringDoctorLastName().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("ReferringDoctorPhoneNumber"))

	     {
                    if (this.getReferringDoctorPhoneNumber().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("ReferringDoctorFaxNumber"))

	     {
                    if (this.getReferringDoctorFaxNumber().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("ReferringDoctorEmail"))

	     {
                    if (this.getReferringDoctorEmail().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("ReferringDoctorCompany"))

	     {
                    if (this.getReferringDoctorCompany().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("ReferringDoctorAddress1"))

	     {
                    if (this.getReferringDoctorAddress1().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("ReferringDoctorAddress2"))

	     {
                    if (this.getReferringDoctorAddress2().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("ReferringDoctorCity"))

	     {
                    if (this.getReferringDoctorCity().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("ReferringDoctorState"))

	     {
                    if (this.getReferringDoctorState().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("ReferringDoctorZip"))

	     {
                    if (this.getReferringDoctorZip().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	     else if (fieldN.equals("Notes"))

	     {
                    if (this.getNotes().equals(new String("")  ))
                    {
                        myVal = false;
                    }
                    else
                    {
                        myVal = true;
                    }
            }

	    }
	    catch(Exception e2)
	    {}
	    return myVal;
	}

    public boolean isExpired(String fieldN,int daysVector)
    {
        return isExpired(fieldN,new java.util.Date(),daysVector);
    }

    public boolean isExpired(String fieldN,java.util.Date refDate,int daysVector)
    {
	     boolean myVal = false;
	     try
	     {
        java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat("yyyy-MM-dd");

        if (fieldN.equals("UniqueCreateDate"))

	     {
                    if (this.isExpired(this.getUniqueCreateDate(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

        if (fieldN.equals("UniqueModifyDate"))

	     {
                    if (this.isExpired(this.getUniqueModifyDate(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

        if (fieldN.equals("DateCaseOpened"))

	     {
                    if (this.isExpired(this.getDateCaseOpened(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

        if (fieldN.equals("DateOfInjury"))

	     {
                    if (this.isExpired(this.getDateOfInjury(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

        if (fieldN.equals("PatientDOB"))

	     {
                    if (this.isExpired(this.getPatientDOB(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

        if (fieldN.equals("Claimdate"))

	     {
                    if (this.isExpired(this.getClaimdate(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

        if (fieldN.equals("MMI"))

	     {
                    if (this.isExpired(this.getMMI(),refDate,daysVector))
                    {
                        myVal = true;
                    }
                    else
                    {
                        myVal = false;
                    }
        }

	    }
	    catch(Exception e2)
	    {}
	    return myVal;
	}

    public java.util.Date getDateField(String fieldN)
    {
	     java.util.Date myVal = null;
	     try
	     {
        java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat("yyyy-MM-dd");

        if (fieldN.equals("UniqueCreateDate"))

	     {
             myVal = this.getUniqueCreateDate();
        }

        if (fieldN.equals("UniqueModifyDate"))

	     {
             myVal = this.getUniqueModifyDate();
        }

        if (fieldN.equals("DateCaseOpened"))

	     {
             myVal = this.getDateCaseOpened();
        }

        if (fieldN.equals("DateOfInjury"))

	     {
             myVal = this.getDateOfInjury();
        }

        if (fieldN.equals("PatientDOB"))

	     {
             myVal = this.getPatientDOB();
        }

        if (fieldN.equals("Claimdate"))

	     {
             myVal = this.getClaimdate();
        }

        if (fieldN.equals("MMI"))

	     {
             myVal = this.getMMI();
        }

	    }
	    catch(Exception e2)
	    {}
	    return myVal;
	}
	public boolean runFieldSecurity = false;
	public Integer UserSecurityID = null;
   public Integer iSecurityCheck = null;
}    // End of bltNIM3_EligibilityReference class definition
