package com.winstaff;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;

/**
 * Created by IntelliJ IDEA.
 * User: ste
 * Date: 9/16/11
 * Time: 4:28 PM
 * To change this template use File | Settings | File Templates.
 */
public class EDI_HCPSegmentObject {
        /*
            00 Zero Pricing (Not Covered Under Contract)
            hc
            02 Priced at the Standard Fee Schedule
            03 Priced at a Contractual Percentage
            04 Bundled Pricing
            05 Peer Review Pricing
            07 Flat Rate Pricing
            08 Combination Pricing
            09 Maternity Pricing
            10 Other Pricing
            11 Lower of Cost
            12 Ratio of Cost
            13 Cost Reimbursed
            14 Adjustment Pricing
         */


    private String repricingCode = "00";
    private BigDecimal currentAllow = null;
    private BigDecimal currentCharge = null;
    private BigDecimal repricedAmount = null;

    public String getRepricingCode() {
        return repricingCode;
    }

    public void setRepricingCode(String repricingCode) {
        this.repricingCode = repricingCode;
    }

    public BigDecimal getCurrentAllow() {
        return currentAllow;
    }

    public void setCurrentAllow(BigDecimal currentAllow) {
        this.currentAllow = currentAllow;
    }

    public void addToCurrentAllow(BigDecimal currentAllow) {
        if (this.currentAllow==null){
            this.currentAllow = new BigDecimal("0.0");
        }
        this.currentAllow = this.currentAllow.add(currentAllow);
    }



    public BigDecimal getCurrentCharge() {
        return currentCharge;
    }

    public void setCurrentCharge(BigDecimal currentCharge) {
        this.currentCharge = currentCharge;
    }

    public void addToCurrentCharge(BigDecimal currentCharge) {
        if (this.currentCharge==null){
            this.currentCharge = new BigDecimal("0.0");
        }
        this.currentCharge=this.currentCharge.add(currentCharge);
    }

    public BigDecimal getRepricedAmount() {
        return repricedAmount;
    }

    public void setRepricedAmount(BigDecimal repricedAmount) {
        this.repricedAmount = repricedAmount;
    }

    public void addToRepricedAmount(BigDecimal repricedAmount) {
        if (this.repricedAmount==null){
            this.repricedAmount = new BigDecimal("0.0");
        }
        this.repricedAmount=this.repricedAmount.add(repricedAmount);
    }


    public BigDecimal getRepricedSavingsAmount(){
        if (this.getCurrentAllow()!=null&&this.getRepricedAmount()!=null){
            return this.getCurrentAllow().subtract(this.getRepricedAmount());
        } else {
            return null;
        }

    }

}
