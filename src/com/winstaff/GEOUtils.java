package com.winstaff;
import java.net.*;
import java.io.*;

public class GEOUtils 
{
	private final static String ENCODING = "UTF-8";
    //private final static String KEY = "xyz";
    private final static String KEY = "ABQIAAAAM7hvG1DJhf2TtXeNloLnlBT_8jAO_dNHWn0-7Sy73XDsGscKTRQ7jzNfm4K0e1tBVkjqL0sYYTOq0w";

	
	public static class Location 
	{
		public String lon, lat;
		
		private Location (String lat, String lon) 
		{
			this.lon = lon;
			this.lat = lat;
		}
		
		public Location () 
		{
		}
		
		public String toString () 
		{
			return "Lat: "+lat+", Lon: "+lon; 
		}
	}
		
	public static Location getLocation(String address) throws IOException{
		GoogleMaps gm = MapsAPI.getGeoCode(address);
		
		return new Location(String.valueOf(MapsAPI.getLat(gm)),String.valueOf(MapsAPI.getLng(gm)));
	}
	
	public static Location getLocationOLD (String address) throws IOException 
	{
		BufferedReader in = new BufferedReader (new InputStreamReader (new URL ("http://maps.google.com/maps/geo?q="+URLEncoder.encode (address, ENCODING)+"&output=csv&key="+KEY).openStream ()));
		String line;
		Location location = null;
		int statusCode = -1;
		while ((line = in.readLine ()) != null) 
		{
			// Format: 200,6,42.730070,-73.690570
			statusCode = Integer.parseInt (line.substring (0, 3));
			if (statusCode == 200)
			location = new Location (
			line.substring ("200,6,".length (), line.indexOf (',', "200,6,".length ())),
			line.substring (line.indexOf (',', "200,6,".length ())+1, line.length ()));
		}
		if (location == null) 
		{
			switch (statusCode) 
			{
				case 400: throw new IOException ("Bad Request");
				case 500: throw new IOException ("Unknown error from Google Encoder");
				case 601: throw new IOException ("Missing query");
				case 602: return null;
				case 603: throw new IOException ("Legal problem");
				case 604: throw new IOException ("No route");
				case 610: throw new IOException ("Bad key");
				case 620: throw new IOException ("Too many queries");
			}
		}
		return location;
	}
		
	
	public static boolean cacheLocation(String address, String sLat, String sLon)
	{
		try
		{
			java.sql.Connection cCon = java.sql.DriverManager.getConnection( ConfigurationInformation.sURL,ConfigurationInformation.sUser,ConfigurationInformation.sPassword);
			java.sql.PreparedStatement stmt2 = null;
			String sSQLString = "insert into \"public\".\"GEOCODES\" (address, lat, lon) values (?,?,?)";
			stmt2 = cCon.prepareStatement(sSQLString);
			stmt2.setString(1, address);
			stmt2.setFloat(2, new Float(sLat));
			stmt2.setFloat(3, new Float(sLon));
			stmt2.executeUpdate();
			cCon.close();
		}
		catch(Exception eeee)
		{
            DebugLogger.println ( "GEOUtils: in db_Base Constuctor Error: " + eeee.toString () );
		}
		
		//write to database
		return false;
	}
}