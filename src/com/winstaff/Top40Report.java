package com.winstaff;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class Top40Report {
	public final static int t4 = 4;
	public final static int t3 = 3;
	public final static int t2 = 2;
	public final static int t1 = 1;
	public final static int t0 = 0;
	public final static int p = 5;
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}
	public static int getTopNumber(List<Top40Model> top40model, int checkTop, int type){
		int total = 0;
		
		if (type==t4){
			for(int x = 0; x < checkTop - 1; x++){
				total += top40model.get(x).getFourmonthago();
			}
		} else if (type==t3){
			for(int x = 0; x < checkTop - 1; x++){
				total += top40model.get(x).getThreemonthago();
			}
		} else if (type==t2){
			for(int x = 0; x < checkTop - 1; x++){
				total += top40model.get(x).getTwomonthago();
			}
		} else if (type==t1){
			for(int x = 0; x < checkTop - 1; x++){
				total += top40model.get(x).getLastmonth();
			}
		} else if (type==t0){
			for(int x = 0; x < checkTop - 1; x++){
				total += top40model.get(x).getThismonth();
			}
		} else if (type==p){
			for(int x = 0; x < checkTop - 1; x++){
				total += top40model.get(x).getProjected();
			}
		} 
		
		return total;
	}
	public static List<Top40Model> getTop40(){
		List<Top40Model> top40model = new ArrayList<Top40Model>();
		
		String query = "select * from \"tTop40_java\"";
		searchDB2 conn = new searchDB2();
		ResultSet rs = conn.executeStatement(query);
		
		try {
			while (rs.next()){
				int i = 1;
				top40model.add(new Top40Model(rs.getString(i++),rs.getInt(i++),rs.getInt(i++),rs.getInt(i++),rs.getInt(i++),rs.getInt(i++),rs.getInt(i++),rs.getInt(i++),rs.getString(i++),rs.getString(i++)));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally{
			conn.closeAll();
		}
		return top40model;
	}
	
	public static class Top40Model{
		private String payername;
		private int payerid;
		private int fourmonthago;
		private int threemonthago;
		private int twomonthago;
		private int lastmonth;
		private int thismonth;
		private int projected;
		private String change;
		private String trailing;
		public Top40Model(String payername, int payerid, int fourmonthago, int threemonthago, int twomonthago, int lastmonth, int thismonth, int projected, String change, String trailing) {
			this.payername = payername;
			this.payerid = payerid;
			this.fourmonthago = fourmonthago;
			this.threemonthago = threemonthago;
			this.twomonthago = twomonthago;
			this.lastmonth = lastmonth;
			this.thismonth = thismonth;
			this.projected = projected;
			this.change = change;
			this.trailing = trailing;
		}
		public String getPayername() {
			return payername;
		}
		public void setPayername(String payername) {
			this.payername = payername;
		}
		public int getPayerid() {
			return payerid;
		}
		public void setPayerid(int payerid) {
			this.payerid = payerid;
		}
		public int getFourmonthago() {
			return fourmonthago;
		}
		public void setFourmonthago(int fourmonthago) {
			this.fourmonthago = fourmonthago;
		}
		public int getThreemonthago() {
			return threemonthago;
		}
		public void setThreemonthago(int threemonthago) {
			this.threemonthago = threemonthago;
		}
		public int getTwomonthago() {
			return twomonthago;
		}
		public void setTwomonthago(int twomonthago) {
			this.twomonthago = twomonthago;
		}
		public int getLastmonth() {
			return lastmonth;
		}
		public void setLastmonth(int lastmonth) {
			this.lastmonth = lastmonth;
		}
		public int getThismonth() {
			return thismonth;
		}
		public void setThismonth(int thismonth) {
			this.thismonth = thismonth;
		}
		public int getProjected() {
			return projected;
		}
		public void setProjected(int projected) {
			this.projected = projected;
		}
		public String getChange() {
			return change;
		}
		public void setChange(String change) {
			this.change = change;
		}
		public String getTrailing() {
			return trailing;
		}
		public void setTrailing(String trailing) {
			this.trailing = trailing;
		}
		
	}
	
}
