package com.winstaff;


import com.winstaff.dbtEventChild;
import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public interface InttEventChild extends dbTableInterface
{

    public void setEventChildID(Integer newValue);
    public Integer getEventChildID();
    public void setUniqueCreateDate(Date newValue);
    public Date getUniqueCreateDate();
    public void setUniqueModifyDate(Date newValue);
    public Date getUniqueModifyDate();
    public void setUniqueModifyComments(String newValue);
    public String getUniqueModifyComments();
    public void setEventID(Integer newValue);
    public Integer getEventID();
    public void setName(String newValue);
    public String getName();
    public void setEventType(String newValue);
    public String getEventType();
    public void setCompleted(Integer newValue);
    public Integer getCompleted();
    public void setStartDate(Date newValue);
    public Date getStartDate();
    public void setEndDate(Date newValue);
    public Date getEndDate();
    public void setRemindDate(Date newValue);
    public Date getRemindDate();
    public void setSummary(String newValue);
    public String getSummary();
    public void setItemRefID(Integer newValue);
    public Integer getItemRefID();
    public void setItemRefType(String newValue);
    public String getItemRefType();
    public void setCompanyName(String newValue);
    public String getCompanyName();
    public void setContactName(String newValue);
    public String getContactName();
    public void setEmail(String newValue);
    public String getEmail();
    public void setAddress1(String newValue);
    public String getAddress1();
    public void setAddress2(String newValue);
    public String getAddress2();
    public void setCity(String newValue);
    public String getCity();
    public void setStateID(Integer newValue);
    public Integer getStateID();
    public void setProvince(String newValue);
    public String getProvince();
    public void setZIP(String newValue);
    public String getZIP();
    public void setCountryID(Integer newValue);
    public Integer getCountryID();
    public void setPhone(String newValue);
    public String getPhone();
    public void setFax(String newValue);
    public String getFax();
    public void setMergeGeneric1(String newValue);
    public String getMergeGeneric1();
    public void setMergeGeneric2(String newValue);
    public String getMergeGeneric2();
    public void setMergeGeneric3(String newValue);
    public String getMergeGeneric3();
    public void setMergeGeneric4(String newValue);
    public String getMergeGeneric4();
    public void setMergeGeneric5(String newValue);
    public String getMergeGeneric5();
    public void setComments(String newValue);
    public String getComments();
}    // End of bltEventChild class definition
