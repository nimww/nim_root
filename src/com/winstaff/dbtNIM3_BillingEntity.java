

package com.winstaff;

import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public class dbtNIM3_BillingEntity extends Object implements InttNIM3_BillingEntity
{

        db_NewBase    dbnbDB;

    public dbtNIM3_BillingEntity()
    {
        dbnbDB = new db_NewBase( "tNIM3_BillingEntity", "BillingEntityID" );

    }    // End of default constructor

    public dbtNIM3_BillingEntity( Integer iNewID )
    {
        dbnbDB = new db_NewBase( "tNIM3_BillingEntity", "BillingEntityID" );
        setUniqueID( iNewID );
    }    // End of Constructor knowing an ID

    public void commitData() throws SQLException
    {
            //no longer trapping/ignoring errors 2011-03-17
            dbnbDB.commitData();
    }    // End of commitData()

    public void commitDataForced() throws SQLException
    {
        dbnbDB.commitDataForced();
    }    // End of commitData()

    public Integer getUniqueID()
    {
        return dbnbDB.getUniqueID();
    }    // End of getUniqueID()

    public void setUniqueID( Integer iNewID )
    {
        dbnbDB.setUniqueID( iNewID );
    }    // End of setUniqueID()

    public void setModifyComments( String sNew )
    {
        dbnbDB.setModifyComments( sNew );
    }    // End of setUniqueID()


    public void setBillingEntityID(Integer newValue)
    {
                dbnbDB.setFieldData( "BillingEntityID", newValue.toString() );
    }

    public Integer getBillingEntityID()
    {
        String           sValue = dbnbDB.getFieldData( "BillingEntityID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setField(String fieldN, Object fieldV)
    {DebugLogger.println("setField Method not implemented yet in classNIM3_BillingEntity!");}

    public String getFieldType(String fieldN)
    {return null;}


    public void setUniqueCreateDate(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "UniqueCreateDate", formatter.format( newValue ) );
    }

    public Date getUniqueCreateDate()
    {
        String           sValue = dbnbDB.getFieldData( "UniqueCreateDate" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setUniqueModifyDate(Date newValue)
    {
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
                dbnbDB.setFieldData( "UniqueModifyDate", formatter.format( newValue ) );
    }

    public Date getUniqueModifyDate()
    {
        String           sValue = dbnbDB.getFieldData( "UniqueModifyDate" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDateValue : sValue;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ( ConfigurationInformation.sDateTimeFormat );
        Date             dRetVal   = new Date();
        try
        {
              dRetVal = formatter.parse( sValue );
        }
        catch ( java.text.ParseException e )
        {
              DebugLogger.println( e.toString() );
        }
        return ( dRetVal ); 
    }

    public void setUniqueModifyComments(String newValue)
    {
                dbnbDB.setFieldData( "UniqueModifyComments", newValue.toString() );
    }

    public String getUniqueModifyComments()
    {
        String           sValue = dbnbDB.getFieldData( "UniqueModifyComments" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setEntityName(String newValue)
    {
                dbnbDB.setFieldData( "EntityName", newValue.toString() );
    }

    public String getEntityName()
    {
        String           sValue = dbnbDB.getFieldData( "EntityName" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

    public void setFeeScheduleRefID(Integer newValue)
    {
                dbnbDB.setFieldData( "FeeScheduleRefID", newValue.toString() );
    }

    public Integer getFeeScheduleRefID()
    {
        String           sValue = dbnbDB.getFieldData( "FeeScheduleRefID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setFeePercentage(Double newValue)
    {
                java.text.DecimalFormat defDecFormat2 = new java.text.DecimalFormat(PLCUtils.String_defDecFormat2);
                dbnbDB.setFieldData( "FeePercentage", defDecFormat2.format(newValue) );
    }

    public Double getFeePercentage()
    {
        String           sValue = dbnbDB.getFieldData( "FeePercentage" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDoubleValue : sValue;
        return new Double( sValue  ); 
    }

    public void setOverrideTypeID(Integer newValue)
    {
                dbnbDB.setFieldData( "OverrideTypeID", newValue.toString() );
    }

    public Integer getOverrideTypeID()
    {
        String           sValue = dbnbDB.getFieldData( "OverrideTypeID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setOverrideScheduleRefID(Integer newValue)
    {
                dbnbDB.setFieldData( "OverrideScheduleRefID", newValue.toString() );
    }

    public Integer getOverrideScheduleRefID()
    {
        String           sValue = dbnbDB.getFieldData( "OverrideScheduleRefID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setOverridePercentage(Double newValue)
    {
                java.text.DecimalFormat defDecFormat2 = new java.text.DecimalFormat(PLCUtils.String_defDecFormat2);
                dbnbDB.setFieldData( "OverridePercentage", defDecFormat2.format(newValue) );
    }

    public Double getOverridePercentage()
    {
        String           sValue = dbnbDB.getFieldData( "OverridePercentage" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultDoubleValue : sValue;
        return new Double( sValue  ); 
    }

}    // End of bltNIM3_BillingEntity class definition
