

package com.winstaff;

import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public class dbtDocumentTypeLI extends Object implements InttDocumentTypeLI
{

        db_NewBase    dbnbDB;

    public dbtDocumentTypeLI()
    {
        dbnbDB = new db_NewBase( "tDocumentTypeLI", "DocumentTypeID" );

    }    // End of default constructor

    public dbtDocumentTypeLI( Integer iNewID )
    {
        dbnbDB = new db_NewBase( "tDocumentTypeLI", "DocumentTypeID" );
        setUniqueID( iNewID );
    }    // End of Constructor knowing an ID

    public void commitData() throws SQLException
    {
            //no longer trapping/ignoring errors 2011-03-17
            dbnbDB.commitData();
    }    // End of commitData()

    public void commitDataForced() throws SQLException
    {
        dbnbDB.commitDataForced();
    }    // End of commitData()

    public Integer getUniqueID()
    {
        return dbnbDB.getUniqueID();
    }    // End of getUniqueID()

    public void setUniqueID( Integer iNewID )
    {
        dbnbDB.setUniqueID( iNewID );
    }    // End of setUniqueID()

    public void setModifyComments( String sNew )
    {
        dbnbDB.setModifyComments( sNew );
    }    // End of setUniqueID()


    public void setDocumentTypeID(Integer newValue)
    {
                dbnbDB.setFieldData( "DocumentTypeID", newValue.toString() );
    }

    public Integer getDocumentTypeID()
    {
        String           sValue = dbnbDB.getFieldData( "DocumentTypeID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setField(String fieldN, Object fieldV)
    {DebugLogger.println("setField Method not implemented yet in class!");}

    public String getFieldType(String fieldN)
    {return null;}


    public void setDocumentTypeLong(String newValue)
    {
                dbnbDB.setFieldData( "DocumentTypeLong", newValue.toString() );
    }

    public String getDocumentTypeLong()
    {
        String           sValue = dbnbDB.getFieldData( "DocumentTypeLong" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

}    // End of bltDocumentTypeLI class definition
