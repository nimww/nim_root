
package com.winstaff;
/*
 * bltAdminPracticeLU_List_AdminMaster_PracticeID.java
 *
 * Created: Tue Nov 03 16:01:08 PST 2009
 */



import java.sql.*;




/**
 *
 * @author  Scott Ellis via listClassGenCode
 * @version 2.5
 */
public class bltAdminPracticeLU_List_AdminMaster_PracticeID extends db_BaseRelationTable
{
    /** Default constructor
    */    
    public bltAdminPracticeLU_List_AdminMaster_PracticeID ( Integer iPracticeID )
    {
        super( "tAdminPracticeLU", "AdminID", "LookupID", "PracticeID", iPracticeID );
    }   // End of bltAdminPracticeLU_List_AdminMaster_PracticeID()
    

    public void setField(String fieldN, Object fieldV)
    {DebugLogger.println("setField Method not implemented yet in classbltAdminPracticeLU_List_AdminMaster_PracticeID!");}

    public String getFieldType(String fieldN)
    {return null;}

    public bltAdminPracticeLU_List_AdminMaster_PracticeID ( Integer iPracticeID, String extraWhere, String OrderBy )
    {
        super( "tAdminPracticeLU", "AdminID", "LookupID", "PracticeID", iPracticeID, extraWhere,OrderBy );
    }   // End of bltAdminPracticeLU_List_AdminMaster_PracticeID()
     /** Method pull information from the result set and load into class attributes.
    * @param sFieldList Comma delimited list of fields for which data has been retrieved.
    * @param rs Result set from which to pull the data from.
    * @throws SQLException Passes on SQLException
    */      
    protected void transferData( ResultSet rs ) throws SQLException
    {
        while ( rs.next()  )
        {
            Integer         iValue              = new Integer( rs.getString( sLinkFieldName ) );
            Integer         iLinkRecordID       = new Integer( rs.getString( sUniqueIDFieldName ) );
            Object          oReferenceObject;
            ListElement     leNewElement;
            
            oReferenceObject    = new bltAdminMaster( iValue );
            
            leNewElement        = new ListElement( iLinkRecordID, oReferenceObject );
            
            vList.add( leNewElement );

        }
    }   // End of transferData()
}   // End of bltAdminPracticeLU_List_AdminMaster_PracticeID class

