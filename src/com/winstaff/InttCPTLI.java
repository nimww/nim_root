

package com.winstaff;

import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public interface InttCPTLI extends dbTableInterface
{

    public void setLookupID(Integer newValue);
    public Integer getLookupID();
    public void setUniqueCreateDate(Date newValue);
    public Date getUniqueCreateDate();
    public void setUniqueModifyDate(Date newValue);
    public Date getUniqueModifyDate();
    public void setUniqueModifyComments(String newValue);
    public String getUniqueModifyComments();
    public void setCPT(String newValue);
    public String getCPT();
    public void setMOD(String newValue);
    public String getMOD();
    public void setDescription(String newValue);
    public String getDescription();
    public void setCPTCommon1(String newValue);
    public String getCPTCommon1();
}    // End of bltCPTLI class definition
