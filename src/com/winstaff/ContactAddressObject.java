package com.winstaff;

public class ContactAddressObject extends Object 
{

    public ContactAddressObject()
    {
    }    // End of default constructor

    public ContactAddressObject(String address1, String address2, String city, String comments, String company, String firstName, String lastName, String state, String zip) {
        Address1 = address1;
        Address2 = address2;
        City = city;
        Comments = comments;
        Company = company;
        FirstName = firstName;
        LastName = lastName;
        State = state;
        Zip = zip;
    }

    public ContactAddressObject(String address1, String city, String state, String zip) {
        Address1 = address1;
        City = city;
        State = state;
        Zip = zip;
        Address2 = "";
        Comments = "";
        Company = "";
        FirstName = "";
        LastName = "";
    }

    public String getAddress1() {
        return Address1;
    }

    public void setAddress1(String address1) {
        Address1 = address1;
    }

    public String getAddress2() {
        return Address2;
    }

    public void setAddress2(String address2) {
        Address2 = address2;
    }

    public String getCity() {
        return City;
    }

    public void setCity(String city) {
        City = city;
    }

    public String getCompany() {
        return Company;
    }

    public void setCompany(String company) {
        Company = company;
    }

    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String firstName) {
        FirstName = firstName;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String lastName) {
        LastName = lastName;
    }

    public String getState() {
        return State;
    }

    public void setState(String state) {
        State = state;
    }

    public String getZip() {
        return Zip;
    }

    public void setZip(String zip) {
        Zip = zip;
    }

    private String FirstName = null;
    private String LastName = null;
    private String Company = null;
    private String Address1 = null;
    private String Address2 = null;
    private String City = null;
    private String State = null;
    private String Zip = null;

    public String getComments() {
        return Comments;
    }

    public void setComments(String comments) {
        Comments = comments;
    }

    private String Comments = null;



}    // End of ContactAddressObject class definition
