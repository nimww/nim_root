

package com.winstaff;

import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public class dbtUpdateItemStatusLI extends Object implements InttUpdateItemStatusLI
{

        db_NewBase    dbnbDB;

    public dbtUpdateItemStatusLI()
    {
        dbnbDB = new db_NewBase( "tUpdateItemStatusLI", "UpdateItemStatusID" );

    }    // End of default constructor

    public dbtUpdateItemStatusLI( Integer iNewID )
    {
        dbnbDB = new db_NewBase( "tUpdateItemStatusLI", "UpdateItemStatusID" );
        setUniqueID( iNewID );
    }    // End of Constructor knowing an ID

    public void commitData() throws SQLException
    {
            //no longer trapping/ignoring errors 2011-03-17
            dbnbDB.commitData();
    }    // End of commitData()

    public void commitDataForced() throws SQLException
    {
        dbnbDB.commitDataForced();
    }    // End of commitData()

    public Integer getUniqueID()
    {
        return dbnbDB.getUniqueID();
    }    // End of getUniqueID()

    public void setUniqueID( Integer iNewID )
    {
        dbnbDB.setUniqueID( iNewID );
    }    // End of setUniqueID()

    public void setModifyComments( String sNew )
    {
        dbnbDB.setModifyComments( sNew );
    }    // End of setUniqueID()


    public void setUpdateItemStatusID(Integer newValue)
    {
                dbnbDB.setFieldData( "UpdateItemStatusID", newValue.toString() );
    }

    public Integer getUpdateItemStatusID()
    {
        String           sValue = dbnbDB.getFieldData( "UpdateItemStatusID" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultIntegerValue : sValue;
        return new Integer( sValue  ); 
    }

    public void setField(String fieldN, Object fieldV)
    {DebugLogger.println("setField Method not implemented yet in class!");}

    public String getFieldType(String fieldN)
    {return null;}


    public void setUpdateItemStatusText(String newValue)
    {
                dbnbDB.setFieldData( "UpdateItemStatusText", newValue.toString() );
    }

    public String getUpdateItemStatusText()
    {
        String           sValue = dbnbDB.getFieldData( "UpdateItemStatusText" );
        sValue = (sValue == null) ? ConfigurationInformation.sDefaultStringValue : sValue;
        return new String( sValue  ); 
    }

}    // End of bltUpdateItemStatusLI class definition
