package com.winstaff;

import java.util.ArrayList;

/**
 * Created by IntelliJ IDEA.
 * User: ste
 * Date: 2/25/12
 * Time: 10:47 AM
 * To change this template use File | Settings | File Templates.
 */
public class NIM3_BooleanAnalysisResultsObject {

    private Boolean result = false;
    private ArrayList<String> arrayMessage = new ArrayList<String>();

    public NIM3_BooleanAnalysisResultsObject() {
   }

    public ArrayList<String> getArrayMessage() {
        return arrayMessage;
    }

    public void setArrayMessage(ArrayList arrayMessage) {
        this.arrayMessage = arrayMessage;
    }

    public void addArrayMessage(String newMessage) {
        this.arrayMessage.add(newMessage);
    }

    public Boolean getResult() {
        return result;
    }

    public void setResult(Boolean result) {
        this.result = result;
    }

    public String getMessage(String delim) {
        String myVal = "";
        int cnt=0;
        for (String tempMessage : this.getArrayMessage()){
            if (cnt>0){
                myVal += delim;
            }
            myVal += tempMessage ;
            cnt++;
        }
        return myVal;
    }

}
