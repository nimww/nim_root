package com.winstaff;

import java.security.PrivateKey;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.util.Enumeration;

import com.winstaff.NIMUtils;

/**
 * Created by IntelliJ IDEA.
 * User: ste
 * Date: May 18, 2010
 * Time: 3:04:41 PM
 * To change this template use File | Settings | File Templates.
 */
public class NIM3_EncounterObject
{

    public NIM3_EncounterObject(Integer iEncounterID)
    {
         this(iEncounterID, "[no-data]");
    }


    public bltNIM3_CaseAccount getNIM3_CaseAccount() {
        return NIM3_CaseAccount;
    }

    public void setNIM3_CaseAccount(bltNIM3_CaseAccount NIM3_CaseAccount) {
        this.NIM3_CaseAccount = NIM3_CaseAccount;
    }

    public bltNIM3_Referral getNIM3_Referral() {
        return NIM3_Referral;
    }

    public void setNIM3_Referral(bltNIM3_Referral NIM3_Referral) {
        this.NIM3_Referral = NIM3_Referral;
    }

    public bltNIM3_Encounter getNIM3_Encounter() {
        return NIM3_Encounter;
    }

    public void setNIM3_Encounter(bltNIM3_Encounter NIM3_Encounter) {
        this.NIM3_Encounter = NIM3_Encounter;
    }

    public bltNIM3_Appointment getNIM3_Appointment() {
        return NIM3_Appointment;
    }

    public void setNIM3_Appointment(bltNIM3_Appointment NIM3_Appointment) {
        this.NIM3_Appointment = NIM3_Appointment;
    }

    public bltNIM3_PayerMaster getNIM3_PayerMaster() {
        return NIM3_PayerMaster;
    }

    public void setNIM3_PayerMaster(bltNIM3_PayerMaster NIM3_PayerMaster) {
        this.NIM3_PayerMaster = NIM3_PayerMaster;
    }

    public bltNIM3_PayerMaster getNIM3_ParentPayerMaster() {
        return NIM3_ParentPayerMaster;
    }

    public void setNIM3_ParentPayerMaster(bltNIM3_PayerMaster NIM3_ParentPayerMaster) {
        this.NIM3_ParentPayerMaster = NIM3_ParentPayerMaster;
    }

    public bltNIM3_Service_List getNIM3_Service_List() {
        return NIM3_Service_List;
    }

    public void setNIM3_Service_List(bltNIM3_Service_List NIM3_Service_List) {
        this.NIM3_Service_List = NIM3_Service_List;
    }

    public bltUserAccount getCase_AssignedTo() {
        return Case_AssignedTo;
    }

    public void setCase_AssignedTo(bltUserAccount case_AssignedTo) {
        Case_AssignedTo = case_AssignedTo;
    }

    public bltNIM3_PayerMaster getCase_AssignedTo_Payer() {
        return Case_AssignedTo_Payer;
    }

    public void setCase_AssignedTo_Payer(bltNIM3_PayerMaster case_AssignedTo_Payer) {
        Case_AssignedTo_Payer = case_AssignedTo_Payer;
    }

    public bltPracticeMaster getAppointment_PracticeMaster() {
        return Appointment_PracticeMaster;
    }

    public void setAppointment_PracticeMaster(bltPracticeMaster appointment_PracticeMaster) {
        Appointment_PracticeMaster = appointment_PracticeMaster;
    }

    public bltUserAccount getCase_Adjuster() {
        return Case_Adjuster;
    }

    public void setCase_Adjuster(bltUserAccount case_Adjuster) {
        Case_Adjuster = case_Adjuster;
    }

    public bltUserAccount getCase_Admin() {
        return Case_Admin;
    }

    public void setCase_Admin(bltUserAccount case_Admin) {
        Case_Admin = case_Admin;
    }

    public bltUserAccount getCase_BillTo() {
        return Case_BillTo;
    }

    public void setCase_BillTo(bltUserAccount case_BillTo) {
        Case_BillTo = case_BillTo;
    }

    public bltUserAccount getCase_Admin2() {
        return Case_Admin2;
    }

    public void setCase_Admin2(bltUserAccount case_Admin2) {
        Case_Admin2 = case_Admin2;
    }

    public bltUserAccount getCase_Admin3() {
        return Case_Admin3;
    }

    public void setCase_Admin3(bltUserAccount case_Admin3) {
        Case_Admin3 = case_Admin3;
    }

    public bltUserAccount getCase_Admin4() {
        return Case_Admin4;
    }

    public void setCase_Admin4(bltUserAccount case_Admin4) {
        Case_Admin4 = case_Admin4;
    }

    public bltUserAccount getCase_NurseCaseManager() {
        return Case_NurseCaseManager;
    }

    public void setCase_NurseCaseManager(bltUserAccount case_NurseCaseManager) {
        Case_NurseCaseManager = case_NurseCaseManager;
    }

    public bltUserAccount getReferral_ReferringDoctor() {
        return Referral_ReferringDoctor;
    }

    public void setReferral_ReferringDoctor(bltUserAccount referral_ReferringDoctor) {
        Referral_ReferringDoctor = referral_ReferringDoctor;
    }

    public NIM3_EncounterObject(Integer iEncounterID, String UMC)
    {
        NIM3_Encounter = new bltNIM3_Encounter(iEncounterID);
        NIM3_Encounter.setUniqueModifyComments(UMC);
        if (NIM3_Encounter.getAppointmentID()>0)
        {
            NIM3_Appointment = new bltNIM3_Appointment(NIM3_Encounter.getAppointmentID());
            NIM3_Appointment.setUniqueModifyComments(UMC);
            Appointment_PracticeMaster = new bltPracticeMaster(NIM3_Appointment.getProviderID());
        }
        NIM3_Referral = new bltNIM3_Referral (NIM3_Encounter.getReferralID());
        NIM3_Referral.setUniqueModifyComments(UMC);
        NIM3_CaseAccount = new bltNIM3_CaseAccount (NIM3_Referral.getCaseID());
        NIM3_CaseAccount.setUniqueModifyComments(UMC);
        NIM3_Service_List = new bltNIM3_Service_List(iEncounterID,"","ServiceID");
        this.Case_Adjuster = new bltUserAccount(NIM3_CaseAccount.getAdjusterID());
        this.Case_Adjuster.setUniqueModifyComments(UMC);
        this.Case_Admin= new bltUserAccount(NIM3_CaseAccount.getCaseAdministratorID());
        this.Case_Admin.setUniqueModifyComments(UMC);

        this.NIM3_PayerMaster = new bltNIM3_PayerMaster(NIM3_CaseAccount.getPayerID());
        this.NIM3_PayerMaster.setUniqueModifyComments(UMC);

        this.NIM3_ParentPayerMaster = new bltNIM3_PayerMaster(NIM3_PayerMaster.getParentPayerID());
        this.NIM3_ParentPayerMaster.setUniqueModifyComments(UMC);

        this.Case_BillTo= new bltUserAccount(NIM3_CaseAccount.getBillToContactID());
        this.Case_BillTo.setUniqueModifyComments(UMC);

        this.Case_Admin2= new bltUserAccount(NIM3_CaseAccount.getCaseAdministrator2ID());
        this.Case_Admin2.setUniqueModifyComments(UMC);
        this.Case_Admin3= new bltUserAccount(NIM3_CaseAccount.getCaseAdministrator3ID());
        this.Case_Admin3.setUniqueModifyComments(UMC);
        this.Case_Admin4= new bltUserAccount(NIM3_CaseAccount.getCaseAdministrator4ID());
        this.Case_Admin4.setUniqueModifyComments(UMC);
        this.Case_NurseCaseManager = new bltUserAccount(NIM3_CaseAccount.getNurseCaseManagerID());
        this.Case_NurseCaseManager.setUniqueModifyComments(UMC);
        this.Referral_ReferringDoctor = new bltUserAccount(NIM3_Referral.getReferringPhysicianID());
        this.Referral_ReferringDoctor .setUniqueModifyComments(UMC);
        this.Case_AssignedTo = new bltUserAccount (NIM3_CaseAccount.getAssignedToID());
        this.Case_AssignedTo.setUniqueModifyComments(UMC);
        this.Case_AssignedTo_Payer = new bltNIM3_PayerMaster (Case_AssignedTo.getPayerID());
        this.Case_AssignedTo_Payer.setUniqueModifyComments(UMC);

        //pulling logic into central method
        isAlertSkip_Adj = NIMUtils.isAlertSkip(Case_Adjuster,NIMUtils.CASE_USER_ADJUSTER, 1);
        isAlertSkip_Adm = NIMUtils.isAlertSkip(Case_Admin, NIMUtils.CASE_USER_ADMIN, 1);
        isAlertSkip_Adm2 = NIMUtils.isAlertSkip(Case_Admin2, NIMUtils.CASE_USER_ADMIN2, 1);
        isAlertSkip_Adm3 = NIMUtils.isAlertSkip(Case_Admin3, NIMUtils.CASE_USER_ADMIN3, 1);
        isAlertSkip_Adm4 = NIMUtils.isAlertSkip(Case_Admin4, NIMUtils.CASE_USER_ADMIN4, 1);
        isAlertSkip_NCM = NIMUtils.isAlertSkip(Case_NurseCaseManager, NIMUtils.CASE_USER_NURSE_CASE_MANAGER, 1);
        isAlertSkip_RefDr = NIMUtils.isAlertSkip(Referral_ReferringDoctor, NIMUtils.CASE_USER_REFERRING_DOCTOR, 1);

        //pulling logic into central method
        isAlert2Skip_Adj = NIMUtils.isAlertSkip(Case_Adjuster,NIMUtils.CASE_USER_ADJUSTER, 2);
        isAlert2Skip_Adm = NIMUtils.isAlertSkip(Case_Admin, NIMUtils.CASE_USER_ADMIN, 2);
        isAlert2Skip_Adm2 = NIMUtils.isAlertSkip(Case_Admin2, NIMUtils.CASE_USER_ADMIN2, 2);
        isAlert2Skip_Adm3 = NIMUtils.isAlertSkip(Case_Admin3, NIMUtils.CASE_USER_ADMIN3, 2);
        isAlert2Skip_Adm4 = NIMUtils.isAlertSkip(Case_Admin4, NIMUtils.CASE_USER_ADMIN4, 2);
        isAlert2Skip_NCM = NIMUtils.isAlertSkip(Case_NurseCaseManager, NIMUtils.CASE_USER_NURSE_CASE_MANAGER, 2);
        isAlert2Skip_RefDr = NIMUtils.isAlertSkip(Referral_ReferringDoctor, NIMUtils.CASE_USER_REFERRING_DOCTOR, 2);

        //need to repeat logic
        isReportSkip_Adj = NIMUtils.isAlertSkip(Case_Adjuster,NIMUtils.CASE_USER_ADJUSTER, 3);
        isReportSkip_Adm = NIMUtils.isAlertSkip(Case_Admin, NIMUtils.CASE_USER_ADMIN, 3);
        isReportSkip_Adm2 = NIMUtils.isAlertSkip(Case_Admin2, NIMUtils.CASE_USER_ADMIN2, 3);
        isReportSkip_Adm3 = NIMUtils.isAlertSkip(Case_Admin3, NIMUtils.CASE_USER_ADMIN3, 3);
        isReportSkip_Adm4 = NIMUtils.isAlertSkip(Case_Admin4, NIMUtils.CASE_USER_ADMIN4, 3);
        isReportSkip_NCM = NIMUtils.isAlertSkip(Case_NurseCaseManager, NIMUtils.CASE_USER_NURSE_CASE_MANAGER, 3);
        isReportSkip_RefDr = NIMUtils.isAlertSkip(Referral_ReferringDoctor, NIMUtils.CASE_USER_REFERRING_DOCTOR, 3);
        isReportSkip_Adj = false;
        isReportSkip_RefDr = false;

        //skips alerts because case is not approved.
        if (NIM3_Referral.getReferralStatusID()!=1)
        {
            isAlertSkip_Adj = true;
            isAlertSkip_Adm = true;
            isAlertSkip_Adm2 = true;
            isAlertSkip_Adm3 = true;
            isAlertSkip_Adm4 = true;
            isAlertSkip_NCM = true;

            isAlert2Skip_Adj = true;
            isAlert2Skip_Adm = true;
            isAlert2Skip_Adm2 = true;
            isAlert2Skip_Adm3 = true;
            isAlert2Skip_Adm4 = true;
            isAlert2Skip_NCM = true;
        }


    }

    public String getScanPass()
    {
        return NIM3_Encounter.getScanPass();
    }

    public boolean isEscalatedSTAT()
    {
        if (NIM3_Encounter.getisSTAT()==1)
        {
            return true;
        }
        else
        {
            return false;
        }

    }

    public String getFeeScheduleRateTablePayerMRI(String sDel)
    {
//need to update this to use the actual appointment State/ZIP

        if (this.NIM3_Encounter.getDateOfService().after(NIMUtils.getBeforeTime()))
        {
            return getFeeScheduleRateTablePayerMRI(this.NIM3_CaseAccount.getPatientZIP(),this.NIM3_Encounter.getDateOfService(),sDel);
        }
        else
        {
            return getFeeScheduleRateTablePayerMRI(this.NIM3_CaseAccount.getPatientZIP(),PLCUtils.getToday(),sDel);
        }
    }


    public String getFeeScheduleRateTablePayerMRI(String sZIP,java.util.Date EffDate, String sDel)
    {
        java.text.DecimalFormat DFCurrency = new DecimalFormat(PLCUtils.String_defDecFormatCurrency1);
        return
                "mr_wo = " + DFCurrency.format(NIMUtils.getPayerAllowAmount(this.NIM3_PayerMaster.getPayerID(),sZIP,"mr_wo","",EffDate)) + sDel +
                        "mr_w = " + DFCurrency.format(NIMUtils.getPayerAllowAmount(this.NIM3_PayerMaster.getPayerID(),sZIP,"mr_w","",EffDate)) + sDel +
                        "mr_wwo = " + DFCurrency.format(NIMUtils.getPayerAllowAmount(this.NIM3_PayerMaster.getPayerID(),sZIP,"mr_wwo","",EffDate)) + sDel +
                        "ct_wo = " + DFCurrency.format(NIMUtils.getPayerAllowAmount(this.NIM3_PayerMaster.getPayerID(),sZIP,"ct_wo","",EffDate)) + sDel +
                        "ct_w = " + DFCurrency.format(NIMUtils.getPayerAllowAmount(this.NIM3_PayerMaster.getPayerID(),sZIP,"ct_w","",EffDate)) + sDel +
                        "ct_wwo = " + DFCurrency.format(NIMUtils.getPayerAllowAmount(this.NIM3_PayerMaster.getPayerID(),sZIP,"ct_wwo","",EffDate)) + sDel;
    }


    public String getPayerFullName()
    {
        if (this.NIM3_PayerMaster!=null&&this.NIM3_ParentPayerMaster!=null)
        {
            return this.NIM3_PayerMaster.getPayerName() + " (" + this.NIM3_ParentPayerMaster.getPayerName() + ")";
        }
        else if (this.NIM3_PayerMaster!=null)
        {
            return this.NIM3_PayerMaster.getPayerName() + " (No Parent)";
        }
        else
        {
           return "Invalid Payer";
        }
    }



    public String getCaseDashboard_AlertCode_HTML()
    {
        /*
        <%if (myPM.getPayerName().indexOf("Tyson")>=0||myPM.getParentPayerID()==138){%><script language="javascript">alert("Note: This is a Tyson case.  Please do not call/email the Patient directly for any reason.\n\nIf you have any questions, please see Andy Lock")</script><%}else if (myPM.getPayerID()==214||myPM.getPayerID()==216||myPM.getPayerID()==523){%><script language="javascript">alert("Note: This is a Chartis/AIG case.  Please See NetDev for Scheduling to ensure that we can obtain a CD of the Scan.\n\nIf you have any questions, please see Andy Lock")</script><%}else if (myPM.getPayerID()==297){%><script language="javascript">alert("Note: This is a PMA case. Please send all reports as TIFF Documents.\n\nALSO: if auth is needed\nDO NOT CONTACT THE ADJUSTER\nCall the customer service line at: 800-476-2669\n\nIf you have any questions, please see Elizabeth Vidal or Andy Lock")</script><%}%>
         */
        String myVal = "";
        if (this.NIM3_CaseAccount.getPayerID()==50)
        {
            myVal = "<script language=\"javascript\">alert(\"WARNING\\n\\nThis case is linked to NO PAYER, please fix this ASAP!\");</script>";
        }
        if (this.Case_Adjuster.getPayerID()==50)
        {
            myVal = "<script language=\"javascript\">alert(\"WARNING\\n\\nThe Adjuster for this Case is linked to NO PAYER, please fix this ASAP or notify a manager!\");</script>";
        }


        if (this.NIM3_PayerMaster.getImportantNotes_Alert().equalsIgnoreCase("")&&this.NIM3_ParentPayerMaster.getImportantNotes_Alert().equalsIgnoreCase(""))
        {
            myVal += "<!-- No Payer Popup Alerts -->";
        }
        else
        {
            String tempVal = "";
            if (!this.NIM3_PayerMaster.getImportantNotes_Alert().equalsIgnoreCase(""))
            {
                tempVal += "Payer ["+this.NIM3_PayerMaster.getPayerName()+"]: "+ DataControlUtils.Text2OneLine(this.NIM3_PayerMaster.getImportantNotes_Alert());
            }
            if (!this.NIM3_ParentPayerMaster.getImportantNotes_Alert().equalsIgnoreCase(""))
            {
                if (!tempVal.equalsIgnoreCase(""))
                {
                    tempVal+="\\n\\n";
                }
                tempVal += "Parent Payer ["+this.NIM3_ParentPayerMaster.getPayerName()+"]: "+ DataControlUtils.Text2OneLine(this.NIM3_ParentPayerMaster.getImportantNotes_Alert());
            }
            myVal += "<script language=\"javascript\">alert(\""+tempVal+"\");</script>";
        }

        String tempUserVal = "";
        if (!this.Case_Adjuster.getImportantNotes_Alert().equalsIgnoreCase(""))
        {
            if (!tempUserVal.equalsIgnoreCase(""))
            {
                tempUserVal+="\\n\\n";
            }
            tempUserVal += "Adjuster ["+this.Case_Adjuster.getContactFirstName()+" "+this.Case_Adjuster.getContactLastName()+"]: "+ DataControlUtils.Text2OneLine(this.Case_Adjuster.getImportantNotes_Alert());
        }
        if (!this.Case_NurseCaseManager.getImportantNotes_Alert().equalsIgnoreCase(""))
        {
            if (!tempUserVal.equalsIgnoreCase(""))
            {
                tempUserVal+="\\n\\n";
            }
            tempUserVal += "Nurse Case Mgr ["+this.Case_NurseCaseManager.getContactFirstName()+" "+this.Case_NurseCaseManager.getContactLastName()+"]: "+ DataControlUtils.Text2OneLine(this.Case_NurseCaseManager.getImportantNotes_Alert());
        }
        if (!this.Referral_ReferringDoctor.getImportantNotes_Alert().equalsIgnoreCase(""))
        {
            if (!tempUserVal.equalsIgnoreCase(""))
            {
                tempUserVal+="\\n\\n";
            }
            tempUserVal += "Ref Dr ["+this.Referral_ReferringDoctor.getContactFirstName()+" "+this.Referral_ReferringDoctor.getContactLastName()+"]: "+ DataControlUtils.Text2OneLine(this.Referral_ReferringDoctor.getImportantNotes_Alert());
        }
        if (!this.Case_Admin.getImportantNotes_Alert().equalsIgnoreCase(""))
        {
            if (!tempUserVal.equalsIgnoreCase(""))
            {
                tempUserVal+="\\n\\n";
            }
            tempUserVal += "Case Adm1 ["+this.Case_Admin.getContactFirstName()+" "+this.Case_Admin.getContactLastName()+"]: "+ DataControlUtils.Text2OneLine(this.Case_Admin.getImportantNotes_Alert());
        }
        if (!this.Case_Admin2.getImportantNotes_Alert().equalsIgnoreCase(""))
        {
            if (!tempUserVal.equalsIgnoreCase(""))
            {
                tempUserVal+="\\n\\n";
            }
            tempUserVal += "Case Adm2 ["+this.Case_Admin2.getContactFirstName()+" "+this.Case_Admin2.getContactLastName()+"]: "+ DataControlUtils.Text2OneLine(this.Case_Admin2.getImportantNotes_Alert());
        }
        if (!this.Case_Admin3.getImportantNotes_Alert().equalsIgnoreCase(""))
        {
            if (!tempUserVal.equalsIgnoreCase(""))
            {
                tempUserVal+="\\n\\n";
            }
            tempUserVal += "Case Adm3 ["+this.Case_Admin3.getContactFirstName()+" "+this.Case_Admin3.getContactLastName()+"]: "+ DataControlUtils.Text2OneLine(this.Case_Admin3.getImportantNotes_Alert());
        }
        if (!this.Case_Admin4.getImportantNotes_Alert().equalsIgnoreCase(""))
        {
            if (!tempUserVal.equalsIgnoreCase(""))
            {
                tempUserVal+="\\n\\n";
            }
            tempUserVal += "Case Adm4 ["+this.Case_Admin4.getContactFirstName()+" "+this.Case_Admin4.getContactLastName()+"]: "+ DataControlUtils.Text2OneLine(this.Case_Admin4.getImportantNotes_Alert());
        }
        if (!tempUserVal.equalsIgnoreCase(""))
        {
            myVal += "<script language=\"javascript\">alert(\""+tempUserVal+"\");</script>";
        }

        return myVal;
    }

    public String getImportantNotes_All(bltUserAccount theUser, String lineBreak)
    {
        String myVal = "";
        if (!theUser.getImportantNotes().equalsIgnoreCase(""))
        {
            myVal += lineBreak + theUser.getImportantNotes();
        }
        if (!theUser.getImportantNotes_Alert().equalsIgnoreCase(""))
        {
            myVal += lineBreak  + theUser.getImportantNotes_Alert();
        }
        if (!theUser.getEmailAlertNotes_Alert().equalsIgnoreCase(""))
        {
            myVal += lineBreak  + theUser.getEmailAlertNotes_Alert();
        }
        return myVal;

    }

    public String getEmailImportantNotes_Payer(String lineBreak)
    {
        String myVal = "";
        if (!NIM3_ParentPayerMaster.getEmailAlertNotes_Alert().equalsIgnoreCase(""))
        {
            if (!myVal.equalsIgnoreCase(""))
            {
                myVal+=lineBreak;
            }
            myVal +=  NIM3_ParentPayerMaster.getEmailAlertNotes_Alert();
        }
        if (!NIM3_PayerMaster.getEmailAlertNotes_Alert().equalsIgnoreCase(""))
        {
            if (!myVal.equalsIgnoreCase(""))
            {
                myVal+=lineBreak;
            }
            myVal +=  NIM3_PayerMaster.getEmailAlertNotes_Alert();
        }
        return myVal;

    }


    public String getCaseDashboard_CombinedNotes(String lineBreak)
    {
        String myVal = "";
        if (!this.NIM3_PayerMaster.getImportantNotes().equalsIgnoreCase(""))
        {
            if (!myVal.equalsIgnoreCase(""))
            {
                myVal+=lineBreak;
            }
            myVal +=  "Case Notes: " + this.NIM3_CaseAccount.getComments();
        }
        if (!this.NIM3_PayerMaster.getImportantNotes().equalsIgnoreCase(""))
        {
            if (!myVal.equalsIgnoreCase(""))
            {
                myVal+=lineBreak;
            }
            myVal +=  "Payer["+this.NIM3_PayerMaster.getPayerName()+"]: " + this.NIM3_PayerMaster.getImportantNotes();
        }
        if (!this.NIM3_ParentPayerMaster.getImportantNotes().equalsIgnoreCase(""))
        {
            if (!myVal.equalsIgnoreCase(""))
            {
                myVal+=lineBreak;
            }
            myVal +=  "Parent["+this.NIM3_ParentPayerMaster.getPayerName()+"]: " + this.NIM3_ParentPayerMaster.getImportantNotes();
        }
        if (!this.Case_Adjuster.getImportantNotes().equalsIgnoreCase(""))
        {
            if (!myVal.equalsIgnoreCase(""))
            {
                myVal+=lineBreak;
            }
            myVal +=  "Adj["+this.Case_Adjuster.getContactFirstName()+" "+this.Case_Adjuster.getContactLastName()+"]: " + this.Case_Adjuster.getImportantNotes();
        }
        if (!this.Case_NurseCaseManager.getImportantNotes().equalsIgnoreCase(""))
        {
            if (!myVal.equalsIgnoreCase(""))
            {
                myVal+=lineBreak;
            }
            myVal +=  "NCM["+this.Case_NurseCaseManager.getContactFirstName()+" "+this.Case_NurseCaseManager.getContactLastName()+"]: " + this.Case_NurseCaseManager.getImportantNotes();
        }
        if (!this.Referral_ReferringDoctor.getImportantNotes().equalsIgnoreCase(""))
        {
            if (!myVal.equalsIgnoreCase(""))
            {
                myVal+=lineBreak;
            }
            myVal +=  "Ref Dr["+this.Referral_ReferringDoctor.getContactFirstName()+" "+this.Referral_ReferringDoctor.getContactLastName()+"]: " + this.Referral_ReferringDoctor.getImportantNotes();
        }
        if (!this.Case_Admin.getImportantNotes().equalsIgnoreCase(""))
        {
            if (!myVal.equalsIgnoreCase(""))
            {
                myVal+=lineBreak;
            }
            myVal +=  "Adm1["+this.Case_Admin.getContactFirstName()+" "+this.Case_Admin.getContactLastName()+"]: " + this.Case_Admin.getImportantNotes();
        }
        if (!this.Case_Admin2.getImportantNotes().equalsIgnoreCase(""))
        {
            if (!myVal.equalsIgnoreCase(""))
            {
                myVal+=lineBreak;
            }
            myVal +=  "Adm2["+this.Case_Admin2.getContactFirstName()+" "+this.Case_Admin2.getContactLastName()+"]: " + this.Case_Admin2.getImportantNotes();
        }
        if (!this.Case_Admin3.getImportantNotes().equalsIgnoreCase(""))
        {
            if (!myVal.equalsIgnoreCase(""))
            {
                myVal+=lineBreak;
            }
            myVal +=  "Adm3["+this.Case_Admin3.getContactFirstName()+" "+this.Case_Admin3.getContactLastName()+"]: " + this.Case_Admin3.getImportantNotes();
        }
        if (!this.Case_Admin4.getImportantNotes().equalsIgnoreCase(""))
        {
            if (!myVal.equalsIgnoreCase(""))
            {
                myVal+=lineBreak;
            }
            myVal +=  "Adm4["+this.Case_Admin4.getContactFirstName()+" "+this.Case_Admin4.getContactLastName()+"]: " + this.Case_Admin4.getImportantNotes();
        }
        return myVal;
    }


    public String getCourtesyStatus_Display()
    {
        String myVal ="None-Error";
        if (NIM3_Encounter.getisCourtesy()==0) {myVal = "NA";}
        else if (NIM3_Encounter.getisCourtesy()==1) {myVal = "Yes";}
        else if (NIM3_Encounter.getisCourtesy()==2) {myVal = "No";}
        else if (NIM3_Encounter.getisCourtesy()==3) {myVal = "Decline";}
        else if (NIM3_Encounter.getisCourtesy()==4) {myVal = "Pending";}
        return myVal;
    }


    public String getEncounterType_Display()
    {
        String myVal ="None-Error";
        if (NIM3_Encounter.getEncounterTypeID()==0 ) {myVal = "None";}
        else if (NIM3_Encounter.getEncounterTypeID()==1 ) {myVal = "Other: Single-Visit";}
        else if (NIM3_Encounter.getEncounterTypeID()==2 ) {myVal = "Other: Multiple-Visit";}
        else if (NIM3_Encounter.getEncounterTypeID()==3 ) {myVal = "Other";}
        else if (NIM3_Encounter.getEncounterTypeID()==4 ) {myVal = "Diag: MRI";}
        else if (NIM3_Encounter.getEncounterTypeID()==5 ) {myVal = "Diag: CT";}
        else if (NIM3_Encounter.getEncounterTypeID()==6 ) {myVal = "Diag: EMG";}
        else if (NIM3_Encounter.getEncounterTypeID()==7) {myVal = "Diag: NCV";}
        else if (NIM3_Encounter.getEncounterTypeID()==8 ) {myVal = "Diag: PET";}
        else if (NIM3_Encounter.getEncounterTypeID()==9 ) {myVal = "Diag: Other";}
        else if (NIM3_Encounter.getEncounterTypeID()==1 ) {myVal = "Lab";}
        return myVal;
    }


    public String getEncounterStory(String sDel)
    {
        String myVal = sDel;
        bltUserAccount myUA_TTReqCre = new bltUserAccount(NIM3_Encounter.getTimeTrack_ReqCreated_UserID());
        myVal += "This referral was received on " + PLCUtils.getDisplayDateWithTime(NIM3_Referral.getReceiveDate())
                + " and initial creation was completed on " +PLCUtils.getDisplayDateWithTime(NIM3_Encounter.getTimeTrack_ReqCreated())
                + " by " + myUA_TTReqCre.getContactFirstName() + " " +  myUA_TTReqCre.getContactLastName() + "." + sDel ;
        myVal += "Receipt confirmations look like they were sent out as follows:" + sDel;
        if (NIM3_Encounter.getSentTo_ReqRec_Adj().after(NIMUtils.getBeforeTime()))
        {
            myVal += " To the Adjuster (" + Case_Adjuster.getContactFirstName() + " " + Case_Adjuster.getContactLastName() + ") on "
                    + PLCUtils.getDisplayDateWithTime(NIM3_Encounter.getSentTo_ReqRec_Adj()) + ";" + sDel;
        }
        if (NIM3_Encounter.getSentTo_ReqRec_NCM().after(NIMUtils.getBeforeTime()))
        {
            myVal += " To the Nurse Case Manager (" +  Case_NurseCaseManager.getContactFirstName() + " " + Case_NurseCaseManager.getContactLastName() + ") on "
                    + PLCUtils.getDisplayDateWithTime(NIM3_Encounter.getSentTo_ReqRec_NCM()) + ";" + sDel;
        }
        if (NIM3_Encounter.getSentTo_ReqRec_Adm().after(NIMUtils.getBeforeTime()))
        {
            myVal += " To the Case Admin 1 (" +  Case_Admin.getContactFirstName() + " " + Case_Admin.getContactLastName() + ") on "
                    + PLCUtils.getDisplayDateWithTime(NIM3_Encounter.getSentTo_ReqRec_Adm()) + ";" + sDel;
        }
        if (NIM3_Encounter.getSentTo_ReqRec_Adm2().after(NIMUtils.getBeforeTime()))
        {
            myVal += " To the Case Admin 2 (" +  Case_Admin2.getContactFirstName() + " " + Case_Admin2.getContactLastName() + ") on "
                    + PLCUtils.getDisplayDateWithTime(NIM3_Encounter.getSentTo_ReqRec_Adm2()) + ";" + sDel;
        }
        if (NIM3_Encounter.getSentTo_ReqRec_Adm3().after(NIMUtils.getBeforeTime()))
        {
            myVal += " To the Case Admin 3 (" +  Case_Admin3.getContactFirstName() + " " + Case_Admin3.getContactLastName() + ") on "
                    + PLCUtils.getDisplayDateWithTime(NIM3_Encounter.getSentTo_ReqRec_Adm3()) + ";" + sDel;
        }
        if (NIM3_Encounter.getSentTo_ReqRec_Adm4().after(NIMUtils.getBeforeTime()))
        {
            myVal += " To the Case Admin 4 (" +  Case_Admin4.getContactFirstName() + " " + Case_Admin4.getContactLastName() + ") on "
                    + PLCUtils.getDisplayDateWithTime(NIM3_Encounter.getSentTo_ReqRec_Adm4()) + ";" + sDel;
        }
        if (NIM3_Encounter.getTimeTrack_ReqProc().after(NIMUtils.getBeforeTime()))
        {
            bltUserAccount myUA_TTReqPro = new bltUserAccount(NIM3_Encounter.getTimeTrack_ReqProc_UserID());
            myVal += "This referral was processed on " +PLCUtils.getDisplayDateWithTime(NIM3_Encounter.getTimeTrack_ReqProc())
                    + " by " + myUA_TTReqPro.getContactFirstName() + " " +  myUA_TTReqPro.getContactLastName() + "." + sDel ;
        }
        if (NIM3_Encounter.getTimeTrack_ReqSched().after(NIMUtils.getBeforeTime()))
        {
            bltUserAccount myUA_TTReqSch = new bltUserAccount(NIM3_Encounter.getTimeTrack_ReqSched_UserID());
            myVal += "Scheduling was completed on " +PLCUtils.getDisplayDateWithTime(NIM3_Encounter.getTimeTrack_ReqSched())
                    + " by " + myUA_TTReqSch.getContactFirstName() + " " +  myUA_TTReqSch.getContactLastName()
                    + " with an initial appointment of  " +PLCUtils.getDisplayDateWithTime(NIM3_Encounter.getTimeTrack_ReqInitialAppointment()) + sDel ;
        }
        if (NIM3_Encounter.getSeeNetDev_Flagged()==1)
        {
            myVal += "Note: the NetDev department was involved in scheduling this case and had the following CommTracks:" + sDel;
            try
            {
                searchDB2 mySS_ct = new searchDB2();
                java.sql.ResultSet myRS_ct = null;
                String mySQL_ct = "SELECT commtrackid from tnim3_commtrack where encounterid=" + NIM3_Encounter.getEncounterID() +" AND CommTypeID=4";
                myRS_ct = mySS_ct.executeStatement(mySQL_ct);
                int i2cnt_ct=0;
                while(myRS_ct!=null&&myRS_ct.next())
                {
                    bltNIM3_CommTrack myCT = new bltNIM3_CommTrack (new  Integer(myRS_ct.getString("commtrackid")));
                    if (myCT.getCommEnd().after(NIMUtils.getBeforeTime()))
                    {
                        myVal += " Request sent on " + PLCUtils.getDisplayDateWithTime(myCT.getCommStart()) + " with a duration of " + Math.round((myCT.getCommEnd().getTime()-myCT.getCommStart().getTime())/1000/60/60) + " hours";
                    }
                    else
                    {
                        myVal += " Request sent on " + PLCUtils.getDisplayDateWithTime(myCT.getCommStart()) + " and still open (" + PLCUtils.getHoursTill_Display(myCT.getCommStart(), false)+ " hours so far)";
                    }
                }
                mySS_ct.closeAll();
            }
            catch (Exception eee)
            {
                DebugLogger.printLine("NIM3_EncounterObject:getEncounterStory:ParseCommTracks_NetDev:["+ eee +"]");
            }
        }

        if (NIM3_Encounter.getisCourtesy()==1)
        {
            myVal += "This case was designated to be completed as a Courtesy Schedule";
        }
        else if (NIM3_Encounter.getisCourtesy()==4)
        {
            myVal += "This case is awaiting approval for a Courtesy Schedule.";
        }
        else if (NIM3_Encounter.getisCourtesy()==3)
        {
            myVal += "This case has been declined as a Courtesy Schedule.";
        }

        //report
        if (NIM3_Encounter.getTimeTrack_ReqDelivered().after(NIMUtils.getBeforeTime()))
        {
            bltUserAccount myUA_TTReqDel = new bltUserAccount(NIM3_Encounter.getTimeTrack_ReqDelivered_UserID());
            myVal += "Report was delivered on " +PLCUtils.getDisplayDateWithTime(NIM3_Encounter.getTimeTrack_ReqDelivered())
                    + " (an estimated " + Math.round((NIM3_Encounter.getTimeTrack_ReqDelivered().getTime()-NIM3_Appointment.getAppointmentTime().getTime())/1000/60/60) + " hours after appointment)"
                    + " by " + myUA_TTReqDel.getContactFirstName() + " " +  myUA_TTReqDel.getContactLastName() + sDel;
        }
        myVal +="Billing:" + sDel;
        if (NIM3_Encounter.getSentTo_Bill_Pay().before(NIMUtils.getBeforeTime()))
        {
            myVal += "We have not yet sent any bills to the Payer." + sDel;
        }
        else
        {
            myVal += "We have sent a bill to the Payer: " + PLCUtils.getDaysTill_Display(NIM3_Encounter.getSentTo_Bill_Pay(),false) + "." + sDel;

        }
        if (NIM3_Encounter.getRec_Bill_Pro().before(NIMUtils.getBeforeTime()))
        {
            myVal +="We have not yet received a bill (HFCA) from Provider." + sDel;
        }
        else
        {
            myVal +="We have received a bill (HFCA) from Provider:" + PLCUtils.getDaysTill_Display(NIM3_Encounter.getRec_Bill_Pro(),false) + ".";
        }
        NIM3_BillingObject2 myBO2 = getBillingTotals2();
        myVal += myBO2.getStatus(sDel);


        return myVal;
    }


    public boolean isEncounterProcessed()
    {
        boolean myVal = false;
        if (!NIM3_Encounter.getTimeTrack_ReqProc().before(NIMUtils.getBeforeTime()) && NIM3_Encounter.getTimeTrack_ReqProc_UserID()>0)
        {
            myVal = true;
        }
        else
        {
        }
        return myVal;
    }

    public boolean ProcessEncounter(bltUserAccount CurrentUserAccount)
    {
        boolean myVal = false;
        try
        {
            if ( (NIM3_Encounter.getTimeTrack_ReqProc_UserID()==0||NIM3_Encounter.getTimeTrack_ReqProc().before(NIMUtils.getBeforeTime())) &&NIMUtils.isEncounterReadyToSchedule(this,false,false))
            {
                NIM3_Encounter.setTimeTrack_ReqProc(PLCUtils.getNowDate(true));
                NIM3_Encounter.setTimeTrack_ReqProc_UserID(CurrentUserAccount.getUserID());
                NIM3_Encounter.setUniqueModifyComments("SystemGenerated:EncounterProcessed:UserID:"+CurrentUserAccount.getUserID());
                NIM3_Encounter.commitData();
                myVal = true;
                //insert commTrack for processed.
                bltNIM3_CommTrack myCT = new bltNIM3_CommTrack();
                myCT.setCommStart(PLCUtils.getNowDate(false));
                myCT.setCommEnd(PLCUtils.getNowDate(false));
                myCT.setAlertStatusCode(new Integer(1));
                myCT.setReferralID(NIM3_Referral.getReferralID());
                myCT.setEncounterID(NIM3_Encounter.getEncounterID());
		        myCT.setMessageText("Encounter Processed on: " + PLCUtils.getNowDate(false)  );
                myCT.setMessageName(CurrentUserAccount.getContactLastName() + " " + CurrentUserAccount.getContactFirstName());
                myCT.setUniqueModifyComments("System Generated:Encounter Processed: UserID ["+ CurrentUserAccount.getUserID()+"]=" + CurrentUserAccount.getLogonUserName());
                myCT.commitData();
            }
        }
        catch(Exception PEerror)
        {
            DebugLogger.printLine("NIM3_EncounterObject:ProcessEncounter:" + PEerror);
        }

        return myVal;
    }

    public NIM3_AddressObject getWhoShouldIBillTo()
    {
        return NIMUtils.getWhoShouldIBillTo(this.NIM3_CaseAccount.getPayerID(),this.NIM3_CaseAccount.getBillToContactID());
    }

    public NIM3_AddressObject getPatientAddressObject()
    {
        NIM3_AddressObject myAO = new NIM3_AddressObject();
        myAO.setAddress1(this.NIM3_CaseAccount.getPatientAddress1());
        myAO.setAddress2(this.NIM3_CaseAccount.getPatientAddress2());
        myAO.setCity(this.NIM3_CaseAccount.getPatientCity());
        myAO.setStateID(this.NIM3_CaseAccount.getPatientStateID());
        myAO.setZIP(this.NIM3_CaseAccount.getPatientZIP());
        return myAO;
    }

    public NIM3_AddressObject getAppointmentPracticeAddressObject()
    {
        NIM3_AddressObject myAO = new NIM3_AddressObject();
        if (NIM3_Appointment!=null&&Appointment_PracticeMaster!=null)
        {
            myAO.setAddress1(Appointment_PracticeMaster.getOfficeAddress1());
            myAO.setAddress2(Appointment_PracticeMaster.getOfficeAddress2());
            myAO.setCity(Appointment_PracticeMaster.getOfficeCity());
            myAO.setStateID(Appointment_PracticeMaster.getOfficeStateID());
            myAO.setZIP(Appointment_PracticeMaster.getOfficeZIP());
        }
        return myAO;
    }



    public java.util.Vector getPatientPreScreen()
    {
        return getPatientPreScreen(this.NIM3_CaseAccount);
    }


   public boolean isPreScreenComplete()
    {
        boolean myVal = true;
        if (this.NIM3_CaseAccount.getPatientHasImplants()==0)
        {
            myVal =false;
        }
        if (this.NIM3_CaseAccount.getPatientHasMetalInBody()==0)
        {
            myVal =false;
        }
        if (this.NIM3_CaseAccount.getPatientHasAllergies()==0)
        {
            myVal =false;
        }
        if (this.NIM3_CaseAccount.getPatientHasRecentSurgery()==0)
        {
            myVal =false;
        }
        if (this.NIM3_CaseAccount.getPatientHasPreviousMRIs()==0)
        {
            myVal =false;
        }
        if (this.NIM3_CaseAccount.getPatientHasKidneyLiverHypertensionDiabeticConditions()==0)
        {
            myVal =false;
        }
        if (this.NIM3_CaseAccount.getPatientIsPregnant()==0)
        {
            myVal =false;
        }
        if (this.NIM3_CaseAccount.getPatientIsClaus()==0)
        {
            myVal =false;
        }
        return myVal;
    }

    public static java.util.Vector getPatientPreScreen(bltNIM3_CaseAccount myCA)
        {
        java.util.Vector<String> myVal = new java.util.Vector<String>();
        if (myCA.getPatientHasImplants()==1)
        {
            myVal.add("Has Implants: YES   [" + myCA.getPatientHasImplantsDesc() + "]");
        }
        if (myCA.getPatientHasMetalInBody()==1)
        {
            myVal.add("Has Metal in Body: YES   [" + myCA.getPatientHasMetalInBodyDesc() + "]");
        }
        if (myCA.getPatientHasAllergies()==1)
        {
            myVal.add("Has Allergies: YES   [" + myCA.getPatientHasAllergiesDesc() + "]");
        }
        if (myCA.getPatientHasRecentSurgery()==1)
        {
            myVal.add("Has Recent Surgery: YES   [" + myCA.getPatientHasRecentSurgeryDesc() + "]");
        }
        if (myCA.getPatientHasPreviousMRIs()==1)
        {
            myVal.add("Has Previous MRI: YES   [" + myCA.getPatientHasPreviousMRIsDesc() + "]");
        }
        if (myCA.getPatientHasKidneyLiverHypertensionDiabeticConditions()==1)
        {
            myVal.add("Has Kidney/Liver/Hypertension/Diabetic Condition: YES   [" + myCA.getPatientHasKidneyLiverHypertensionDiabeticConditionsDesc() + "]");
        }
        if (myCA.getPatientIsPregnant()==1)
        {
            myVal.add("Is Pregnant: YES   [" + myCA.getPatientIsPregnantDesc() + "]");
        }
        if (myCA.getPatientIsClaus()==1)
        {
            myVal.add("Severely Claustrophobic: YES ");
        }

        return myVal;

    }

    public boolean isRxReviewed()
    {
        boolean myVal = false;
        if (!NIM3_Encounter.getTimeTrack_ReqRxReview().before(NIMUtils.getBeforeTime())&&NIM3_Encounter.getTimeTrack_ReqRxReview_UserID()>0)
        {
            myVal = true;
        }
        return myVal;

    }

    public String getPatientPhoneDisplay()
    {
        String myVal = "";
        if (!NIM3_CaseAccount.getPatientCellPhone().equalsIgnoreCase(""))
        {
            myVal +="Cell: " + NIM3_CaseAccount.getPatientCellPhone();
        }
        else if (!NIM3_CaseAccount.getPatientHomePhone().equalsIgnoreCase(""))
        {
            myVal +="Home: " + NIM3_CaseAccount.getPatientHomePhone();
        }
        else if (!NIM3_CaseAccount.getPatientWorkPhone().equalsIgnoreCase(""))
        {
            myVal +="Work: " + NIM3_CaseAccount.getPatientWorkPhone();
        }

        return myVal;


    }


    public NIM3_EncounterObject()
    {
        NIM3_Encounter = new bltNIM3_Encounter();
        NIM3_Referral = new bltNIM3_Referral ();
        NIM3_CaseAccount = new bltNIM3_CaseAccount ();
        //NIM3_Service_List = new bltNIM3_Service_List(iEncounterID);
    }

    private boolean commitDataCRE(Integer iUserID, String sModifyComments)
    {
        try
        {
            NIM3_Encounter.setUniqueModifyComments("U[" + iUserID + "][" + sModifyComments+"]");
            NIM3_Referral.setUniqueModifyComments("U[" + iUserID + "][" + sModifyComments+"]");
            NIM3_CaseAccount.setUniqueModifyComments("U[" + iUserID + "][" + sModifyComments+"]");

            NIM3_Encounter.commitData();
            NIM3_Referral.commitData();
            NIM3_CaseAccount.commitData();
            return true;
        }
        catch (Exception CommitDataEE)
        {
            DebugLogger.printLine("NIM3_EncounterObject:commitDataCRE:"+CommitDataEE.toString());
            return false;
        }
    }



    private boolean commitDataHOLD(Integer iUserID, String sModifyComments)
    {
        try
        {
            NIM3_Encounter.setUniqueModifyComments("U[" + iUserID + "][" + sModifyComments+"]");
            NIM3_Referral.setUniqueModifyComments("U[" + iUserID + "][" + sModifyComments+"]");
            NIM3_CaseAccount.setUniqueModifyComments("U[" + iUserID + "][" + sModifyComments+"]");

            NIM3_Encounter.commitData();
            NIM3_Referral.commitData();
            NIM3_CaseAccount.commitData();
            NIM3_Service_List = new bltNIM3_Service_List(NIM3_Encounter.getEncounterID());
            bltNIM3_Service        working_bltNIM3_Service;
            ListElement         leCurrentElement;
            java.util.Enumeration eList = NIM3_Service_List.elements();
            while (eList.hasMoreElements())
            {
                leCurrentElement    = (ListElement) eList.nextElement();
                working_bltNIM3_Service  = (bltNIM3_Service) leCurrentElement.getObject();
                working_bltNIM3_Service.commitData();
                working_bltNIM3_Service.setUniqueModifyComments("U[" + iUserID + "][" + sModifyComments+"]");
            }
            return true;
        }
        catch (Exception CommitDataEE)
        {
            DebugLogger.printLine("NIM3_EncounterObject:commitDataHOLD:"+CommitDataEE.toString());
            return false;
        }
    }


    public boolean isScanPassAlertsComplete(int AlertsType)
    {
        boolean myVal = false;
        if (AlertsType==1 &&
           (this.isAlertSkip_RefDr||this.NIM3_Encounter.getSentTo_ReqRec_RefDr().after(NIMUtils.getBeforeTime()))
         &&(this.isAlertSkip_Adj||this.NIM3_Encounter.getSentTo_ReqRec_Adj().after(NIMUtils.getBeforeTime()))
        &&(this.isAlertSkip_Adm||this.NIM3_Encounter.getSentTo_ReqRec_Adm().after(NIMUtils.getBeforeTime()))
        &&(this.isAlertSkip_Adm2||this.NIM3_Encounter.getSentTo_ReqRec_Adm2().after(NIMUtils.getBeforeTime()))
        &&(this.isAlertSkip_Adm3||this.NIM3_Encounter.getSentTo_ReqRec_Adm3().after(NIMUtils.getBeforeTime()))
        &&(this.isAlertSkip_Adm4||this.NIM3_Encounter.getSentTo_ReqRec_Adm4().after(NIMUtils.getBeforeTime()))
         &&(this.isAlertSkip_NCM||this.NIM3_Encounter.getSentTo_ReqRec_NCM().after(NIMUtils.getBeforeTime()))
                )
        {
            myVal=true;
        }
        else if (AlertsType==2 &&
           (this.isAlert2Skip_RefDr||this.NIM3_Encounter.getSentTo_SP_RefDr().after(NIMUtils.getBeforeTime()))
         &&(this.isAlert2Skip_Adj||this.NIM3_Encounter.getSentTo_SP_Adj().after(NIMUtils.getBeforeTime()))
        &&(this.isAlert2Skip_Adm||this.NIM3_Encounter.getSentTo_SP_Adm().after(NIMUtils.getBeforeTime()))
        &&(this.isAlert2Skip_Adm2||this.NIM3_Encounter.getSentTo_SP_Adm2().after(NIMUtils.getBeforeTime()))
        &&(this.isAlert2Skip_Adm3||this.NIM3_Encounter.getSentTo_SP_Adm3().after(NIMUtils.getBeforeTime()))
        &&(this.isAlert2Skip_Adm4||this.NIM3_Encounter.getSentTo_SP_Adm4().after(NIMUtils.getBeforeTime()))
         &&(this.isAlert2Skip_NCM||this.NIM3_Encounter.getSentTo_SP_NCM().after(NIMUtils.getBeforeTime()))
                )
        {
            myVal=true;
        }
        else if (AlertsType==3 &&
           (this.isReportSkip_RefDr||this.NIM3_Encounter.getSentToRefDr().after(NIMUtils.getBeforeTime()))
         &&(this.isReportSkip_Adj||this.NIM3_Encounter.getSentToAdj().after(NIMUtils.getBeforeTime()))
        &&(this.isReportSkip_Adm||this.NIM3_Encounter.getSentToAdm().after(NIMUtils.getBeforeTime()))
        &&(this.isReportSkip_Adm2||this.NIM3_Encounter.getSentTo_RP_Adm2().after(NIMUtils.getBeforeTime()))
        &&(this.isReportSkip_Adm3||this.NIM3_Encounter.getSentTo_RP_Adm3().after(NIMUtils.getBeforeTime()))
        &&(this.isReportSkip_Adm4||this.NIM3_Encounter.getSentTo_RP_Adm4().after(NIMUtils.getBeforeTime()))
         &&(this.isReportSkip_NCM||this.NIM3_Encounter.getSentTo_RP_NCM().after(NIMUtils.getBeforeTime()))
                )
        {
            myVal=true;
        }
        return myVal;
    }


    public bltNIM3_CaseAccount NIM3_CaseAccount = null;
    public bltNIM3_Referral NIM3_Referral= null;
    public bltNIM3_Encounter NIM3_Encounter = null;
    public bltNIM3_Appointment NIM3_Appointment = null;
    public bltNIM3_PayerMaster NIM3_PayerMaster = null;
    public bltNIM3_PayerMaster NIM3_ParentPayerMaster = null;
    public bltNIM3_Service_List        NIM3_Service_List        =    null;

    public bltUserAccount Case_AssignedTo = null;
    public bltNIM3_PayerMaster Case_AssignedTo_Payer = null;

    public bltPracticeMaster Appointment_PracticeMaster = null;

    public bltUserAccount Case_Adjuster = null;
    public bltUserAccount Case_Admin = null;
    public bltUserAccount Case_BillTo = null;
    public bltUserAccount Case_Admin2 = null;
    public bltUserAccount Case_Admin3 = null;
    public bltUserAccount Case_Admin4 = null;
    public bltUserAccount Case_NurseCaseManager = null;
    public bltUserAccount Referral_ReferringDoctor = null;

    //pulling logic into central method
    public boolean isAlertSkip_Adj = false;
    public boolean isAlertSkip_Adm = false;
    public boolean isAlertSkip_Adm2 = false;
    public boolean isAlertSkip_Adm3 = false;
    public boolean isAlertSkip_Adm4 = false;
    public boolean isAlertSkip_NCM = false;
    public boolean isAlertSkip_RefDr = false;

    public boolean isAlert2Skip_Adj = false;
    public boolean isAlert2Skip_Adm = false;
    public boolean isAlert2Skip_Adm2 = false;
    public boolean isAlert2Skip_Adm3 = false;
    public boolean isAlert2Skip_Adm4 = false;
    public boolean isAlert2Skip_NCM = false;
    public boolean isAlert2Skip_RefDr = false;

    //need to repeat logic
    public boolean isReportSkip_Adj = false;
    public boolean isReportSkip_Adm = false;
    public boolean isReportSkip_Adm2 = false;
    public boolean isReportSkip_Adm3 = false;
    public boolean isReportSkip_Adm4 = false;
    public boolean isReportSkip_NCM = false;
    public boolean isReportSkip_RefDr = false;


    public NIM3_BillingObject2 getBillingTotals2()
    {
        NIM3_BillingObject2 myBO2 = new NIM3_BillingObject2();
        bltNIM3_Service        working_bltNIM3_Service;
        ListElement         leCurrentElement_Service;
        java.util.Enumeration eList_Service = NIM3_Service_List.elements();
        while (eList_Service.hasMoreElements())
        {
            leCurrentElement_Service    = (ListElement) eList_Service.nextElement();
            working_bltNIM3_Service  = (bltNIM3_Service) leCurrentElement_Service.getObject();
            if (working_bltNIM3_Service.getServiceStatusID()==1)
            {
                myBO2.Reference_FeeScheduleAmount += working_bltNIM3_Service.getReference_FeeScheduleAmount();
                myBO2.Reference_Payer_AllowAmount += working_bltNIM3_Service.getReference_PayerContractAmount();
                myBO2.Reference_Payer_BillAmount += working_bltNIM3_Service.getReference_PayerBillAmount();
                myBO2.Reference_Provider_AllowAmount += working_bltNIM3_Service.getReference_ProviderContractAmount();
                myBO2.Reference_Provider_BillAmount += working_bltNIM3_Service.getReference_ProviderBilledAmount();
                myBO2.Reference_UCAmount += working_bltNIM3_Service.getReference_UCAmount();

                //get actual by parsing through service billing transaction level items
                bltNIM3_ServiceBillingTransaction_List        bltNIM3_ServiceBillingTransaction_List        =    new    bltNIM3_ServiceBillingTransaction_List(working_bltNIM3_Service.getServiceID());
                //declaration of Enumeration
                bltNIM3_ServiceBillingTransaction        working_bltNIM3_ServiceBillingTransaction;
                ListElement         leCurrentElement_ServiceBillingTransaction;
                Enumeration eList_ServiceBillingTransaction = bltNIM3_ServiceBillingTransaction_List.elements();
                if (eList_ServiceBillingTransaction.hasMoreElements())
                {
                     while (eList_ServiceBillingTransaction.hasMoreElements())
                     {
                         leCurrentElement_ServiceBillingTransaction    = (ListElement) eList_ServiceBillingTransaction.nextElement();
                         working_bltNIM3_ServiceBillingTransaction  = (bltNIM3_ServiceBillingTransaction) leCurrentElement_ServiceBillingTransaction.getObject();
                         if (working_bltNIM3_ServiceBillingTransaction.getServiceBillingTransactionTypeID()==NIM3_BillingObject2.REVENUE_TYPE)
                         {
                             myBO2.Actual_Revenue += working_bltNIM3_ServiceBillingTransaction.getTransactionAmount();
                         }
                         else if (working_bltNIM3_ServiceBillingTransaction.getServiceBillingTransactionTypeID()==NIM3_BillingObject2.REVENUEADJUSTMENT_TYPE)
                         {
                             myBO2.Actual_RevenueAdjustment += working_bltNIM3_ServiceBillingTransaction.getTransactionAmount();
                         }
                         else if (working_bltNIM3_ServiceBillingTransaction.getServiceBillingTransactionTypeID()==NIM3_BillingObject2.ARPAYMENT_TYPE)
                         {
                             myBO2.Actual_ARPayment += working_bltNIM3_ServiceBillingTransaction.getTransactionAmount();
                         }
                         else if (working_bltNIM3_ServiceBillingTransaction.getServiceBillingTransactionTypeID()==NIM3_BillingObject2.COS_TYPE)
                         {
                             myBO2.Actual_COS += working_bltNIM3_ServiceBillingTransaction.getTransactionAmount();
                         }
                         else if (working_bltNIM3_ServiceBillingTransaction.getServiceBillingTransactionTypeID()==NIM3_BillingObject2.COSADJUSTMENT_TYPE)
                         {
                             myBO2.Actual_COSAdjustment += working_bltNIM3_ServiceBillingTransaction.getTransactionAmount();
                         }
                         else if (working_bltNIM3_ServiceBillingTransaction.getServiceBillingTransactionTypeID()==NIM3_BillingObject2.APPAYMENT_TYPE)
                         {
                             myBO2.Actual_APPayment += working_bltNIM3_ServiceBillingTransaction.getTransactionAmount();
                         }
                     }
                }
            }
        }
        return myBO2;

    }




    public NIM3_BillingObject getBillingTotals()
    {
        NIM3_BillingObject myBO = new NIM3_BillingObject();
        Double myNetExpected = 0.0;
        Double myNetActual = 0.0;
        bltNIM3_Service        working_bltNIM3_Service;
        ListElement         leCurrentElement_Service;
        java.util.Enumeration eList_Service = NIM3_Service_List.elements();
        while (eList_Service.hasMoreElements())
        {
            leCurrentElement_Service    = (ListElement) eList_Service.nextElement();
            working_bltNIM3_Service  = (bltNIM3_Service) leCurrentElement_Service.getObject();
            if (working_bltNIM3_Service.getServiceStatusID()==1)
            {
                myBO.Payer_ActualAmount += working_bltNIM3_Service.getReceivedAmount();
                myBO.Payer_AllowAmount += working_bltNIM3_Service.getAllowAmount();
                myBO.Payer_AllowAmount_Adjustment += working_bltNIM3_Service.getAllowAmountAdjustment();
                myBO.Provider_AllowAmount += working_bltNIM3_Service.getPaidOutAmount();

            }
        }
        myBO.Provider_ActualAmount = NIM3_Encounter.getPaidToProviderCheck1Amount() + NIM3_Encounter.getPaidToProviderCheck2Amount() + NIM3_Encounter.getPaidToProviderCheck3Amount();
        return myBO;

    }

    public  String getServicePrintOut(boolean isHTML)
    {
        String theBody = "";
        bltNIM3_Service        working_bltNIM3_Service;
        ListElement         leCurrentElement_Service;
        java.util.Enumeration eList_Service = NIM3_Service_List.elements();
        int cnt55=0;
        while (eList_Service.hasMoreElements())
        {
            cnt55++;
            leCurrentElement_Service    = (ListElement) eList_Service.nextElement();
            working_bltNIM3_Service  = (bltNIM3_Service) leCurrentElement_Service.getObject();
            if ( working_bltNIM3_Service.getServiceStatusID()==1)
            {
                theBody += "\n" ;
                if ( !working_bltNIM3_Service.getCPT().equalsIgnoreCase(""))
                {
                    if (isHTML)
                    {
                        theBody += "" + working_bltNIM3_Service.getCPT() + "[" + NIMUtils.getCPTText(working_bltNIM3_Service.getCPT()) + "]";
                    }
                    else
                    {
                        theBody += "\t" + working_bltNIM3_Service.getCPT() + "[" + NIMUtils.getCPTText(working_bltNIM3_Service.getCPT()) + "]";
                    }
                }
                if ( !working_bltNIM3_Service.getCPTBodyPart().equalsIgnoreCase(""))
                {
                    if (isHTML)
                    {
                        theBody += "<br>&nbsp;&nbsp;&nbsp;Body Part:" + working_bltNIM3_Service.getCPTBodyPart();
                    }
                    else
                    {
                        theBody += "\tBP:" + working_bltNIM3_Service.getCPTBodyPart();
                    }
                }
                if ( !working_bltNIM3_Service.getCPTText().equalsIgnoreCase(""))
                {
                    if (isHTML)
                    {
                        theBody += "<br>&nbsp;&nbsp;&nbsp;Diag/Rule-Out: " + working_bltNIM3_Service.getCPTText();
                    }
                    else
                    {
                        theBody += "\tDiag/Rule-Out:\t" + working_bltNIM3_Service.getCPTText();
                    }
                }
            }

        }
        return theBody;

    }
}




