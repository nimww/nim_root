package com.winstaff;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import sun.management.snmp.jvminstr.JvmThreadInstanceEntryImpl;

import java.sql.SQLException;
import java.util.Date;


/**
 * Created by IntelliJ IDEA.
 * User: ste
 * Date: 8/24/11
 * Time: 3:48 PM
 * To change this template use File | Settings | File Templates.
 */
public class NIM3_DirectPatientCard_Wrapper extends NIM3_CoreObject
{
    transient bltNIM3_DirectPatientCard localDirectPatientCard;

    private Integer DirectReferralSourceID;
    private String	UniqueModifyComments;
    private String	PatientFirstName;
    private String	PatientLastName;
    private java.util.Date	SignUpDate;
    private String	SourceWebsite;
    private String	PatientMRN;

    public NIM3_DirectPatientCard_Wrapper(Integer iPatientCardID)
    {
        this.localDirectPatientCard = new bltNIM3_DirectPatientCard(iPatientCardID);
    }

    public static NIM3_DirectPatientCard_Wrapper cloneFromJSON(String inJSON, Gson gson){
        return gson.fromJson(inJSON, NIM3_DirectPatientCard_Wrapper.class);
    }

    public static NIM3_DirectPatientCard_Wrapper cloneFromJSON(String inJSON){
        return cloneFromJSON(inJSON,new Gson());
    }

    public static NIM3_DirectPatientCard_Wrapper cloneFromJSON_Python(String inJSON){
        return cloneFromJSON(inJSON,new GsonBuilder().setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES).create());
    }

    public Integer getDirectReferralSourceID() {
        return DirectReferralSourceID;
    }

    public String getUniqueModifyComments() {
        return UniqueModifyComments;
    }

    public String getPatientFirstName() {
        return PatientFirstName;
    }

    public String getPatientLastName() {
        return PatientLastName;
    }

    public Date getSignUpDate() {
        return SignUpDate;
    }

    public String getSourceWebsite() {
        return SourceWebsite;
    }

    public String getPatientMRN() {
        return PatientMRN;
    }

    public boolean commitData(){
        try{
            this.localDirectPatientCard = new bltNIM3_DirectPatientCard();
            this.localDirectPatientCard.setPatientFirstName(this.getPatientFirstName());
            this.localDirectPatientCard.setPatientLastName(this.getPatientLastName());
            this.localDirectPatientCard.setDirectReferralSourceID(this.getDirectReferralSourceID());
            this.localDirectPatientCard.setSourceWebsite(this.getSourceWebsite());
            this.localDirectPatientCard.setSignUpDate(PLCUtils.getToday());
            this.localDirectPatientCard.commitData();
            this.localDirectPatientCard.setPatientMRN(this.localDirectPatientCard.getUniqueID() + "M" + Math.round(Math.random()*1000000000));
            this.localDirectPatientCard.commitData();
            //set local variables for return JSON
            this.PatientMRN = this.localDirectPatientCard.getPatientMRN();
            this.SignUpDate = this.localDirectPatientCard.getSignUpDate();
            return true;
        }catch (SQLException se){
            DebugLogger.printLine("NIM3_DirectPatientCard_Wrapper:commitData:["+se+"]");
            return false;
        }
    }

}
