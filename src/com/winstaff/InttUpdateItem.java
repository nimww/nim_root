

package com.winstaff;

import java.sql.SQLException;
import java.util.Date;
import java.lang.Integer;

public interface InttUpdateItem extends dbTableInterface
{

    public void setUpdateItemID(Integer newValue);
    public Integer getUpdateItemID();
    public void setUniqueCreateDate(Date newValue);
    public Date getUniqueCreateDate();
    public void setUniqueModifyDate(Date newValue);
    public Date getUniqueModifyDate();
    public void setUniqueModifyComments(String newValue);
    public String getUniqueModifyComments();
    public void setSubmissionDate(Date newValue);
    public Date getSubmissionDate();
    public void setReleaseVersion(String newValue);
    public String getReleaseVersion();
    public void setCategoryID(Integer newValue);
    public Integer getCategoryID();
    public void setPriorityID(Integer newValue);
    public Integer getPriorityID();
    public void setStatusID(Integer newValue);
    public Integer getStatusID();
    public void setItemTitle(String newValue);
    public String getItemTitle();
    public void setItemDescription(String newValue);
    public String getItemDescription();
    public void setDeveloperComments(String newValue);
    public String getDeveloperComments();
    public void setTesterComments(String newValue);
    public String getTesterComments();
    public void setDateTestedOnDevelopment(Date newValue);
    public Date getDateTestedOnDevelopment();
    public void setDateTestedOnQA(Date newValue);
    public Date getDateTestedOnQA();
    public void setDateTestedOnProduction(Date newValue);
    public Date getDateTestedOnProduction();
    public void setRequesterInitials(String newValue);
    public String getRequesterInitials();
    public void setRemoteIP(String newValue);
    public String getRemoteIP();
    public void setHowToTestComments(String newValue);
    public String getHowToTestComments();
    public void setComments(String newValue);
    public String getComments();
    public void setPLCSecID(Integer newValue);
    public Integer getPLCSecID();
    public void setTicketNumber(String newValue);
    public String getTicketNumber();
    public void setAssignedToID(Integer newValue);
    public Integer getAssignedToID();
    public void setAssignedToRoleID(Integer newValue);
    public Integer getAssignedToRoleID();
}    // End of bltUpdateItem class definition
