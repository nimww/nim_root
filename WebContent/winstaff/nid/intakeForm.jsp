<%@page contentType="text/html" language="java" import="com.winstaff.*,org.apache.http.*,java.io.*" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>NIM3 Intake Form</title>
<style>
body {
	font-family: Helvetica, Verdana, Geneva, sans-serif;
	font-size:12px;
}
#referral-intake {
	background: lightblue;
	border-bottom: 2px solid black;
}
#referral-intake span {
	font-weight: bold;
}
input[type=text], textarea {
	width: 200px;
	padding: 2px;
	border-radius: 3px;
	padding: 3px;
	border: 1px solid;
}
.overflow {
	overflow: hidden;
}
.block {
	width: 33%;
	float: left;
	text-align: right;
}
#block-2 {
	float: left;
	text-align: right;
}
#scan-type div {
	text-align: left;
}
.clear {
	clear: both;
}
.full-width {
	width: 100%;
}
.left{
	float:left;
}
#follow-up-md{
	margin-left:70px;	
}
.special{
	margin:10px 45px 15px 0;
}
textarea{
	width:100%;
	height:100px;
	resize:none;
}
.hidden{
	display:none;	
}
</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/jquery-ui.min.js"></script>
<%
	searchDB2 conn = new searchDB2();
	
	String query = "";
	
	java.sql.ResultSet rs = null;
	boolean first = true;
%>

<script type="text/javascript">
<%
String[] mod = {"US", "CT", "MR"}; 
String[] special = {"Normal","Arthrogram"};
String[] contrast = {"WO","W","WO_W"};
String[] ori = {"Left","Right","None"};
%>

<%
for (String key : mod){%>
	var <%=key.toLowerCase()%> = {

	<%query = "select cptwizardid, bp1 from tcptwizard where orientation not in ('Left','Right') and special = 'Normal' and modality='"+key+"'";
	rs = conn.executeStatement(query);
	first = true;
	
	while (rs.next()){
		if(first){%>
		 	"<%=rs.getString("cptwizardid")%>":"<%=rs.getString("bp1")%>"
		<% first=false;
		} else {%>
			,"<%=rs.getString("cptwizardid")%>":"<%=rs.getString("bp1")%>"
		<%}
	}%>
	}
	function get<%=key%>(){
	$('#select').find('option').remove();
	
	$.each(<%=key.toLowerCase()%>, function(key, value) {   
		 $('#select')
			.append($("<option></option>")
			.attr("value",key)
			.text(value)); 
	});
};
<%}%>

<%
for (String key : mod){
	
	for(String side : ori){
	for(String con : contrast){
	for(String spe : special){%>
				var <%=key%><%=side%><%=con%><%=spe%> = {
			
				<%query = "select cptwizardid, bp1 from tcptwizard where orientation = '"+side+"' and special = '"+spe+"' and modality='"+key+"' and contrast='"+con+"'";
				rs = conn.executeStatement(query);
				first = true;
				
				while (rs.next()){
					if(first){%>
						"<%=rs.getString("cptwizardid")%>":"<%=rs.getString("bp1")%>"
					<% first=false;
					} else {%>
						,"<%=rs.getString("cptwizardid")%>":"<%=rs.getString("bp1")%>"
					<%}
				}%>
				}
				
				function get<%=key%><%=side%><%=con%><%=spe%>(){
					$('#select').find('option').remove();
					
					$.each(<%=key%><%=side%><%=con%><%=spe%>, function(key, value) {   
						 $('#select')
							.append($("<option></option>")
							.attr("value",key)
							.text(value)); 
					});
				};
	
	<%}}}
}
String[] divSide = {"None","Left","Right"} ;
for (String div : divSide){%>
	function get<%=div%>(){
		$('.mod').hide();
		$('#select').find('option').remove().end().append($("<option></option>").text("Please Select Options"));;
		$('#mod<%=div%>').show();
	}
<%}%>
 
$(document).ready(function(e) {
  function genR(input){
	var fn = window[input];
	fn();
}
   var input = "getMRRightWO_WNormal";
	genR(input);
});


</script>


</head>

<body>
<form method="post" action="intake_sub.php">
  <div style="width:850px">
  <div id="referral-intake">
    <h2>Referral Intake</h2>
  </div>
  <br>
  <div id="referral-source">
    <h3 style="display:inline;">Referral Source:</h3>
    <input id="referralsourceADJ" type="radio" name="referralSource" value="adj">
    <label for="referralsource">ADJ</label>
    <input id="referralsourceNCM" type="radio" name="referralSource" value="ncm">
    <label for="referralsource">NCM</label>
    <input id="referralsourceMD" type="radio" name="referralSource" value="md">
    <label for="referralsource">MD</label>
    <input id="referralsourceRC" type="radio" name="referralSource" value="rc">
    <label for="referralsource">RC</label>
    <input id="referralsourceUR" type="radio" name="referralSource" value="ur">
    <label for="referralsource">UR</label>
    <input id="referralsourceATTY" type="radio" name="referralSource" value="atty">
    <label for="referralsource">ATTY</label>
  </div>
  <div>
    <h3>Adjuster</h3>
  </div>
  <div class="overflow">
    <div class="block">
      <label for="adjName">Adj Name</label>
      <input id="adjName" type="text" name="adjName">
      <br>
      <label for="adjEmail">Email</label>
      <input id="adjEmail" type="text" name="adjEmail">
      <br>
    </div>
    <div class="block">
      <label for="insurance">Insurance</label>
      <input id="insurance" type="text" name="insurance">
      <br>
      <label for="claim">Claim#</label>
      <input id="claim" type="text" name="claimNumber">
      <br>
    </div>
    <div class="block">
      <label for="adjPhone">Phone</label>
      <input id="adjPhone" type="text" name="adjPhone">
      <br>
      <label for="adjFax">Fax</label>
      <input id="adjFax" type="text" name="adjFax">
      <br>
    </div>
  </div>
  <div class="overflow">
    <h3>NCM</h3>
  </div>
  <div class="overflow">
    <div class="block">
      <label for="ncmName">NCM Name</label>
      <input id="ncmName" type="text" name="ncmName">
      <br>
      <label for="ncmEmail">Email</label>
      <input id="ncmEmail" type="text" name="ncmEmail">
      <br>
    </div>
    <div class="block">
      <label for="ncmPhone">Phone</label>
      <input id="ncmPhone" type="text" name="ncmPhone">
      <br>
      <label for="ncmFax">Fax</label>
      <input id="ncmFax" type="text" name="ncmFax">
      <br>
    </div>
  </div>
  <div class="overflow">
    <h3>Patient/Claimant Info</h3>
  </div>
  <div class="overflow">
    <div class="block">
      <label for="ptName">PT Name</label>
      <input id="ptName" type="text" name="ptName">
      <br>
      <label for="ptDOB">DOB</label>
      <input id="ptDOB" type="text" name="ptDOB">
      <br>
      <label for="ptDOI">DOI</label>
      <input id="ptDOI" type="text" name="ptDOI">
      <br>
      <div style="float:left;width:100%;text-align:left;margin-left:26px;">
          <label for="ptGender">Gender</label>
          <input id="ptGenderM" type="radio" name="ptGender" value="m">
          <label for="ptGenderM">M</label>
          <input id="ptGenderF" type="radio" name="ptGender" value="f">
          <label for="ptGenderF">F</label>
      </div>
      <br>
      <label for="ptEmployer">Employer</label>
      <input id="ptEmployer" type="text" name="ptEmployer">
      <br>
    </div>
    <div class="block">
      <label for="ptAddress">Address</label>
      <input id="ptAddress" type="text" name="ptAddress">
      <br>
      <label for="ptCity">City</label>
      <input id="ptCity" type="text" name="ptCity">
      <br>
      <label for="ptState">State</label>
      <input id="ptState" type="text" name="ptState">
      <br>
      <label for="ptZip">Zip</label>
      <input id="ptZip" type="text" name="ptZip">
      <br>
    </div>
    <div class="block">
      <label for="ptPhone">Phone</label>
      <input id="ptPhone" type="text" name="ptPhone">
      <br>
      <label for="ptCell">Mobile</label>
      <input id="ptCell" type="text" name="ptCell">
      <br>
      <label for="ptWork">Work</label>
      <input id="ptWork" type="text" name="ptWork">
      <br>
      <label for="ptEmail">Email</label>
      <input id="ptEmail" type="text" name="ptEmail">
      <br>
    </div>
  </div>
  <div class="overflow">
    <h3>Referring/Treating Physician and Test Information</h3>
  </div>
  <div class="overflow clear full-width" id="ref-md">
        <div class="block">
          <label for="mdName">MD Name</label>
          <input id="mdName" type="text" name="mdName">
          <br>
          <label for="mdPhone">Phone</label>
          <input id="mdPhone" type="text" name="mdPhone">
          <br>
          <label for="mdFax">Fax</label>
          <input id="mdFax" type="text" name="mdFax">
          <br>
        </div>
        <div class="block">
          <label for="followupAppt"class="left" id="follow-up-md">Follow-up MD Appointment</label>
          <input id="followupAppt" type="text" name="followupAppt">
          <br>
        </div>
    </div>
    <h3>Procedure Type</h3>
    <div class="full-width overflow" id="scan-type">
      <div id="block-2" style="margin:0 7px">
        <label for="">Orientation</label><br>
        <input type="radio" name="side" id="radio" onClick="getNone()" checked>
        <label for="radio">None</label><br>
        <input type="radio" name="side" id="radio" onClick="getLeft()">
        <label for="radio">Left</label><br>
        <input type="radio" name="side" id="radio" onClick="getRight()">
        <label for="radio">Right</label><br>
      </div>
      <div class="block" style="margin-left:15px;width:9%;">
		
        <div class="mod" id="modNone">
            <label for="procedureType">Scan Type</label><br>
            <input id="procedureTypeMRI" type="radio" name="procedureType" value="mri" onClick="getMR()">
            <label for="procedureType">MR</label><br>
            <input id="procedureTypeCT" type="radio" name="procedureType" value="ct" onClick="getCT()">
            <label for="procedureType">CT</label><br>
    <!--    <input id="procedureTypeEMG" type="radio" name="procedureType" value="emg">
            <label for="procedureType">EMG</label><br>-->
            <input id="procedureTypeUS" type="radio" name="procedureType" value="us" onClick="getUS()">
            <label for="procedureType">US</label><br>
		</div>
        <div class="mod hidden" id="modLeft">
        	<label for="procedureType">Scan Type</label><br>
            <input type="radio" name="procedureType" id="procedureTypeMRLeft" onClick="getMRLeft()">
            <label for="procedureType">MR</label><br>
            <input type="radio" name="procedureType" id="procedureTypeCTLeft" onClick="getCTLeft()">
            <label for="procedureType">CT</label><br>
            <input type="radio" name="procedureType" id="procedureTypeUSLeft" onClick="getUSLeft()">
            <label for="procedureType">US</label><br>
        </div>
        <div class="mod hidden" id="modRight">
        	<label for="procedureType">Scan Type</label><br>
            <input type="radio" name="procedureType" id="procedureTypeMRRight" onClick="getMRRight()">
            <label for="procedureType">MR</label><br>
            <input type="radio" name="procedureType" id="procedureTypeCTRight" onClick="getCTRight()">
            <label for="procedureType">CT</label><br>
            <input type="radio" name="procedureType" id="procedureTypeUSRight" onClick="getUSRight()">
            <label for="procedureType">US</label><br>
            
        </div>
     
      </div>
      <div class="block" style="margin-left:15px;width:44.5%;">
        <label for="">Body Part</label>
        <br>
        <select name="bp" id="select" style="width:90%;">
        	<option>Please Select Options</option>
        </select>
      </div>
      <div class="block">
        <label for="ruleOut">Rule Out</label>
        <br>
        <textarea id="ruleOut" name="ruleOut"></textarea>
      </div>
    </div>
    <div class="left special">
      <label for="">Films</label>
      <input id="filmsYes" type="radio" name="films" value="true"> 
      <label for="filmsYes">Yes</label> 
      <input id="filmsNo" type="radio" name="films" value="false"> 
      <label for="filmsNo">No</label>
      <br>
    </div>
   <div class="left special">
      <label for="">CD</label>
      <input id="cdYes" type="radio" name="cd" value="true">
      <label for="cdYes">Yes</label>
      <input id="cdNo" type="radio" name="cd" value="false">
      <label for="cdNo">No</label>
      <br>
    </div>
    <div class="left special">
      <label for="age">Age Comment</label>
      <input id="age" type="checkbox" name="age" value="true">
      <br>
    </div>
    <div class="full-width overflow" style="padding:0 15px 0 0">
      <label for="notes">Notes or Speical Requests</label>
      <br>
      <textarea name="notes" id="notes"></textarea>
    </div>
    <div class="overflow">
    <h3>Upload Files</h3
    ><div>
    Upload any orders / auth related to referral.
    <br><br>
    	<input id="upload" type="file" name="upload">
    </div>
  </div>
  <input type="submit">
</form>
</body>
</html>
