<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.*,org.apache.commons.fileupload.*" %>
<%/*
    filename: tPracticeActivity_main_PracticeActivity_CaseID.jsp
    Created on May/14/2008
    Type: 1-n main class file
    Created by: Scott Ellis
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>

<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_PracticeID_nim.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>

    <table cellpadding=0 cellspacing=0 border=0 width=<%=MasterTableWidth%> >
    <tr><td width=10>&nbsp;</td><td>
      <%//ConfigurationMessages.getHTML("INTERVIEWTopControl","tPracticeActivity")%>
      
      
      
  <%
//initial declaration of list class and parentID
    Integer        iPracticeActivityID        =    null;
    java.text.SimpleDateFormat displayDateTimeSDF = new java.text.SimpleDateFormat("MM/dd/yyyy hh:mm a");
    boolean accessValid = false;
   Integer iSecurityCheck = SecurityCheck.CheckItem("nim1", UserSecurityGroupID);
	Integer iAuthID = null;
   if (iSecurityCheck.intValue()!=0)
   {
    if (pageControllerHash.containsKey("iPracticeActivityID")) 
    {
	    iPracticeActivityID = (Integer)pageControllerHash.get("iPracticeActivityID");
        accessValid = true;
    }
  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat dbdft = new java.text.SimpleDateFormat(PLCUtils.String_dbdft);
      java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);
	
	bltPracticeActivity myPA = new bltPracticeActivity(iPracticeActivityID);
String myFileSS = "";


   try
	    {
//		  out.println("Test1: " + iHCOID);

			// Create a new file upload handler
			DiskFileUpload upload = new DiskFileUpload();
			
			// Set upload parameters
			upload.setSizeThreshold(1900000);
			upload.setSizeMax(1900000);
//			upload.setRepositoryPath("c:\\web\\webapps\\netdevdox\\");
			String myRPath = "/var/lib/tomcat5/webapps/nimdox/";
//			upload.setRepositoryPath("c:\\web\\webapps\\nimdox\\");
			upload.setRepositoryPath(myRPath);			
			
			// Parse the request
			java.util.List /* FileItem */ items = upload.parseRequest(request);
			java.util.Iterator iter = items.iterator();
 	        java.text.SimpleDateFormat fileDF = new java.text.SimpleDateFormat("yyyy-MM-dd" );
			String sFileDF = "nd_" + com.winstaff.password.RandomString.generateString(6).toLowerCase() + "_"+ fileDF.format(new java.util.Date()) + "_actid" + myPA.getPracticeActivityID();
			while (iter.hasNext()) 
			{
			    FileItem item = (FileItem) iter.next();
				String oFileName = item.getName();
				oFileName = oFileName.substring(oFileName.lastIndexOf("\\")+1,oFileName.length());
				oFileName = oFileName.replace(' ' , '_');				
//				myFileSS= sFileDF + "_" + oFileName;
				myFileSS= sFileDF + "_" + oFileName;
				String nFileName = myRPath + myFileSS;
				java.io.File uploadedFile =new java.io.File(nFileName);
				item.write(uploadedFile);


				if (false)
				{
					emailType myETSend = new emailType();
					myETSend.setTo("scott@nextimagemedical.com");
					myETSend.setFrom("scott@nextimagemedical.com");
					myETSend.setSubject("Report Uploaded Complete");
					String theBody = "Done";
					myETSend.setBody(theBody);
					myETSend.isSendMail();
	
	
					//create update Item
				}

			}

	}
	catch (Exception eeee)
	{
//		out.println("******" + eeee + "*****");
	}
	bltDocumentManagement myDoc = new bltDocumentManagement();
	myDoc.setDocumentFileName(myFileSS);
	myDoc.setComments("ND");
	myDoc.setDocumentName(myPA.getName());
	myDoc.setDateReceived(new java.util.Date());
	myDoc.commitData();
	myPA.setDocuLinkID(myDoc.getUniqueID());
	myPA.commitData();











  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>
      
    </td></tr>
    <tr>
      <td>&nbsp;</td>
      <td><h1>  Uploaded</h1></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    </table>
<br>

<%

String nextPage=null;
        if (pageControllerHash.containsKey("sParentReturnPage"))
        {
            nextPage = (String)pageControllerHash.get("sParentReturnPage");
        }
		else if (pageControllerHash.containsKey("sLocalChildReturnPage"))
        {
            nextPage = (String)pageControllerHash.get("sLocalChildReturnPage");
        }

if (nextPage!=null)
{
	    %><script language=Javascript>
alert("Document Uploaded");
	      document.location="<%=nextPage%>";
	      </script><%
    //response.sendRedirect(nextPage+"?EDIT=edit");
}

%>

<script language="javascript">
alert("Document Uploaded");
//document.location='tPracticeActivity_main_PracticeActivity_CaseID.jsp';
</script>

<%String bnIncludeFN = "ui_"+thePLCID+"\\bot-nav_CaseID.jsp";
%>
