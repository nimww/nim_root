<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.bltDocumentManagement,java.util.Vector,java.util.Enumeration,com.winstaff.ListElement, com.winstaff.PLCUtils, com.winstaff.SecurityCheck, com.winstaff.ConfigurationMessages,com.winstaff.bltOtherCertification,com.winstaff.DocumentManagerUtils" %>
<%/*
    filename: out\jsp\tOtherCertification_form_sub.jsp
    Created on Sep/11/2003
    Created by: Scott Ellis
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>


<%

//initial declaration of list class and parentID
    Integer        iOtherCertID        =    null;
    boolean accessValid = false;

   Integer iSecurityCheck = SecurityCheck.CheckItem("PractitionerSection5", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {

    if (pageControllerHash.containsKey("iOtherCertID")) 
    {
        iOtherCertID        =    (Integer)pageControllerHash.get("iOtherCertID");
        if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {
            accessValid = true;
        }
    }
  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);

//initial declaration of list class and parentID

    bltOtherCertification        OtherCertification        =    null;

    if ( request.getParameter( "EDIT" ).equalsIgnoreCase("edit") )
    {
        OtherCertification        =    new    bltOtherCertification(iOtherCertID,UserSecurityGroupID);
    }
    else if ( request.getParameter( "EDIT" ).equalsIgnoreCase("new") )
    {
        OtherCertification        =    new    bltOtherCertification(UserSecurityGroupID,true);
    }

String testChangeID = "0";

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("UniqueCreateDate"))) ;
    if ( !OtherCertification.getUniqueCreateDate().equals(testObj)  )
    {
         OtherCertification.setUniqueCreateDate( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("OtherCertification UniqueCreateDate not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("UniqueModifyDate"))) ;
    if ( !OtherCertification.getUniqueModifyDate().equals(testObj)  )
    {
         OtherCertification.setUniqueModifyDate( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("OtherCertification UniqueModifyDate not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("UniqueModifyComments")) ;
    if ( !OtherCertification.getUniqueModifyComments().equals(testObj)  )
    {
         OtherCertification.setUniqueModifyComments( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("OtherCertification UniqueModifyComments not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("PhysicianID")) ;
    if ( !OtherCertification.getPhysicianID().equals(testObj)  )
    {
         OtherCertification.setPhysicianID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("OtherCertification PhysicianID not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("CertificationType")) ;
    if ( !OtherCertification.getCertificationType().equals(testObj)  )
    {
         OtherCertification.setCertificationType( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("OtherCertification CertificationType not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("CertificationNumber")) ;
    if ( !OtherCertification.getCertificationNumber().equals(testObj)  )
    {
         OtherCertification.setCertificationNumber( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("OtherCertification CertificationNumber not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("ExpirationDate"))) ;
    if ( !OtherCertification.getExpirationDate().equals(testObj)  )
    {
         OtherCertification.setExpirationDate( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("OtherCertification ExpirationDate not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("Name")) ;
    if ( !OtherCertification.getName().equals(testObj)  )
    {
         OtherCertification.setName( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("OtherCertification Name not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("Address1")) ;
    if ( !OtherCertification.getAddress1().equals(testObj)  )
    {
         OtherCertification.setAddress1( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("OtherCertification Address1 not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("Address2")) ;
    if ( !OtherCertification.getAddress2().equals(testObj)  )
    {
         OtherCertification.setAddress2( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("OtherCertification Address2 not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("City")) ;
    if ( !OtherCertification.getCity().equals(testObj)  )
    {
         OtherCertification.setCity( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("OtherCertification City not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("StateID")) ;
    if ( !OtherCertification.getStateID().equals(testObj)  )
    {
         OtherCertification.setStateID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("OtherCertification StateID not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("Province")) ;
    if ( !OtherCertification.getProvince().equals(testObj)  )
    {
         OtherCertification.setProvince( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("OtherCertification Province not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("ZIP")) ;
    if ( !OtherCertification.getZIP().equals(testObj)  )
    {
         OtherCertification.setZIP( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("OtherCertification ZIP not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("CountryID")) ;
    if ( !OtherCertification.getCountryID().equals(testObj)  )
    {
         OtherCertification.setCountryID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("OtherCertification CountryID not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("Phone")) ;
    if ( !OtherCertification.getPhone().equals(testObj)  )
    {
         OtherCertification.setPhone( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("OtherCertification Phone not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("Fax")) ;
    if ( !OtherCertification.getFax().equals(testObj)  )
    {
         OtherCertification.setFax( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("OtherCertification Fax not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("ContactName")) ;
    if ( !OtherCertification.getContactName().equals(testObj)  )
    {
         OtherCertification.setContactName( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("OtherCertification ContactName not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("ContactEmail")) ;
    if ( !OtherCertification.getContactEmail().equals(testObj)  )
    {
         OtherCertification.setContactEmail( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("OtherCertification ContactEmail not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("DocuLinkID")) ;
    if ( !OtherCertification.getDocuLinkID().equals(testObj)  )
    {
         OtherCertification.setDocuLinkID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("OtherCertification DocuLinkID not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("Comments")) ;
    if ( !OtherCertification.getComments().equals(testObj)  )
    {
         OtherCertification.setComments( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("OtherCertification Comments not set. this is ok-not an error");
}
boolean bINT = false;
boolean bNextStep = false;
String sRefreshDoc = "refreshMe=true&";
              if (request.getParameter("INTNext")!=null&&request.getParameter("INTNext").equalsIgnoreCase("yes") ) 
	            {
	                bINT = true;
	                sRefreshDoc = "";
	            }
              else if (request.getParameter("INTNext")!=null&&request.getParameter("INTNext").equalsIgnoreCase("next") ) 
	            {
	                bINT = true;
	                sINTNext = ConfigurationMessages.getInterviewLinkRaw("tOtherCertification","next");
	                sRefreshDoc = "";
	            }

// If an edit, update information; if new, append information
boolean errorRouteMe = false;
if (testChangeID.equalsIgnoreCase("1"))
{

	OtherCertification.setUniqueModifyDate(new java.util.Date());
	String UserLogonDescription = "n/a";
	if (pageControllerHash.containsKey("UserLogonDescription"))
	{
	    UserLogonDescription = (String)pageControllerHash.get("UserLogonDescription");
	}
    //if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
    {
	    OtherCertification.setUniqueModifyComments(UserLogonDescription);
	    try
	    {
	        OtherCertification.commitData();
	    }
	    catch (Exception e55)
	    {
	        errorRouteMe = true;
	    }	//Doculink Addition
	//Doculink Does Exist, prompt to Modify
	Integer iPhysicianID = OtherCertification.getPhysicianID();
     if (!errorRouteMe&&OtherCertification.getDocuLinkID().intValue()>0)
     {
            bltDocumentManagement myDoc = new bltDocumentManagement(OtherCertification.getDocuLinkID());
            if (myDoc.getDocumentFileName().equalsIgnoreCase("")) 
            {
                DocumentManagerUtils myDUtils = new DocumentManagerUtils();
                String myDType = "tOtherCertification";
                myDUtils.setDocumentType(myDType, OtherCertification.getUniqueID());
                myDoc.setDocumentName(myDUtils.getDocumentName());
                myDoc.setDateOfExpiration(myDUtils.getDateOfExpiration());
                myDoc.setDocumentTypeID(myDUtils.getDocumentTypeID());
                myDoc.setPhysicianID(iPhysicianID);
                myDoc.setArchived(new Integer(2));
                myDoc.commitData();
            }
            else
            {
                //archive then create new
                myDoc.setArchived(new Integer("1"));
                myDoc.commitData();
                myDoc = new bltDocumentManagement();
                DocumentManagerUtils myDUtils = new DocumentManagerUtils();
                String myDType = "tOtherCertification";
                myDUtils.setDocumentType(myDType, OtherCertification.getUniqueID());
                myDoc.setDocumentName(myDUtils.getDocumentName());
                myDoc.setDateOfExpiration(myDUtils.getDateOfExpiration());
                myDoc.setDocumentTypeID(myDUtils.getDocumentTypeID());
                myDoc.setPhysicianID(iPhysicianID);
                myDoc.setArchived(new Integer(2));
                myDoc.commitData();
                OtherCertification.setDocuLinkID(myDoc.getUniqueID());
                OtherCertification.commitData();
            }
     }
     else if (!errorRouteMe)
     {
            bltDocumentManagement myDoc = new bltDocumentManagement();
            DocumentManagerUtils myDUtils = new DocumentManagerUtils();
            String myDType = "tOtherCertification";
            myDUtils.setDocumentType(myDType, OtherCertification.getUniqueID());
            myDoc.setDocumentName(myDUtils.getDocumentName());
            myDoc.setDateOfExpiration(myDUtils.getDateOfExpiration());
            myDoc.setDocumentTypeID(myDUtils.getDocumentTypeID());
                myDoc.setPhysicianID(iPhysicianID);
            myDoc.setArchived(new Integer(2));
            myDoc.commitData();
            OtherCertification.setDocuLinkID(myDoc.getUniqueID());
            OtherCertification.commitData();
     }

    }
}
String routePageReference = request.getParameter("routePageReference");
String nextPage=null;
if (routePageReference!=null)
{
        if (pageControllerHash.containsKey(routePageReference))
        {
            nextPage = (String)pageControllerHash.get(routePageReference);
        }
        else if (pageControllerHash.containsKey("sLocalChildReturnPage"))
        {
            nextPage = (String)pageControllerHash.get("sLocalChildReturnPage");
        }
        else if (pageControllerHash.containsKey("sParentReturnPage"))
        {
            nextPage = (String)pageControllerHash.get("sParentReturnPage");
        }
}
              if (bINT ) 
	            {
	                nextPage = sINTNext;
	                if (nextPage==null||nextPage.equalsIgnoreCase("")||nextPage.equalsIgnoreCase("#"))
	                {
	                    nextPage = (String)pageControllerHash.get("sLocalChildReturnPage");
	                }
	            }
if (errorRouteMe)
{
	out.println(ConfigurationMessages.getMessage("FatalError")); 
}
else if (nextPage!=null)
{
	    %><script language=Javascript>
	      document.location="<%=nextPage%>";
	      </script><%
    //response.sendRedirect(nextPage+"?EDIT=edit");
}
        %>


  <%
  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
  %>
