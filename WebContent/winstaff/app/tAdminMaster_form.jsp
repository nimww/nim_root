<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.*" %>
<%/*

    filename: out\jsp\tAdminMaster_form.jsp
    Created on Mar/21/2003
    Created by: Scott Ellis
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>

<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_AdminID_nim.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>

<!--<body onUnload="if (document.getElementById('isSaveMe').value=='1'&&confirm('You have modified data\n\nDo you want to stay on this page?\n\nClick Cancel to leave page...')){return false;">-->

    <table cellpadding=0 cellspacing=0 border=0 width=100% >
    <tr><td width=10>&nbsp;</td><td>
    <%//ConfigurationMessages.getHTML("INTERVIEWTopControl","tAdminMaster")%>



<%
//initial declaration of list class and parentID
    Integer        iAdminID        =    null;
    boolean accessValid = false;
    // required for Type2
    String sKeyMasterReference = null;

   Integer iSecurityCheck = SecurityCheck.CheckItem("company1", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {
    if (pageControllerHash.containsKey("iAdminID")) 
    {
        iAdminID        =    (Integer)pageControllerHash.get("iAdminID");
        accessValid = true;
	}
//	else if (CurrentUserAccount.getAccountType().equalsIgnoreCase("GenAdminID"))
	{
//        iAdminID        =    new Integer(request.getParameter("EDITID"));
//        accessValid = true;
	}
  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);      java.text.SimpleDateFormat displayDateTimeSDF = new java.text.SimpleDateFormat("MM/dd/yyyy hh:mm a");

    pageControllerHash.put("sLocalChildReturnPage","tAdminMaster_form.jsp");
    pageControllerHash.put("iAdminID",iAdminID);
    session.setAttribute("pageControllerHash",pageControllerHash);

//initial declaration of list class and parentID

    bltAdminMaster        AdminMaster        =    null;

    if ( request.getParameter( "EDIT" ).equalsIgnoreCase("edit") )
    {
        AdminMaster        =    new    bltAdminMaster(iAdminID,UserSecurityGroupID);
    }
    else if ( request.getParameter( "EDIT" ).equalsIgnoreCase("new") )
    {
        AdminMaster        =    new    bltAdminMaster(UserSecurityGroupID, true);
    }

//fields
        %>
        <%@ include file="tAdminMaster_form_instructions.jsp" %>
        <form action="tAdminMaster_form_sub.jsp" name="tAdminMaster_form1" id="tAdminMaster_form1" method="POST">
      <input name="EDITID" value="<%=iAdminID%>" type="hidden">
        <input onChange="document.getElementById('isSaveMe').value='1';" type="hidden" name="isSaveMe" id="isSaveMe" value="0" />
<%
    if ( request.getParameter( "EDIT" ) != null )
    {
    %>
        <input onChange="document.getElementById('isSaveMe').value='1';" type="hidden" name="EDIT" value = "<%=request.getParameter( "EDIT" )%>" >  
    <%
    }
%>
          <%  String theClass ="tdBase";%>
        <table width=100% border=1 bordercolor=#333333 cellpadding=0 cellspacing=0 class=tableBase>
         <tr>
           <td class=tableColor>
            <table width=100% border="0" cellpadding=2 cellspacing=0>

                        <tr><td valign=top><p class=<%=theClass%> ><b>Assigned To:&nbsp;</b></p></td><td valign=top><p><select class="tdBaseAltYellow" onchange="document.getElementById('isSaveMe').value='1';"   name="AssignedToID" ><jsp:include page="../generic/tAssignTo_genAdminID.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=AdminMaster.getAssignedToID()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=AssignedToID&amp;sTableName=tAdminMaster&amp;sRefID=<%=AdminMaster.getAdminID()%>&amp;sFieldNameDisp=<%=AdminMaster.getEnglish("AssignedToID")%>&amp;sTableNameDisp=tAdminMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td>
                          <td valign=top align="right"><input class="tdBaseAltGreen" onClick="document.getElementById('isSaveMe').value='0';" type=Submit value="Continue" name=Submit>&nbsp;</td>
                </tr>


            <%
            if ( (AdminMaster.isRequired("Name",UserSecurityGroupID))&&(!AdminMaster.isComplete("Name")) )
            {
                theClass = "requiredField";
            }
            else if ((AdminMaster.isExpired("Name",expiredDays))&&(AdminMaster.isExpiredCheck("Name",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((AdminMaster.isWrite("Name",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Organization Name&nbsp;</b></p></td><td colspan="2" valign=top><p><input class="tdHeader" onChange="document.getElementById('isSaveMe').value='1';" maxlength="100" type=text size="40" name="Name" value="<%=AdminMaster.getName()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Name&amp;sTableName=tAdminMaster&amp;sRefID=<%=AdminMaster.getAdminID()%>&amp;sFieldNameDisp=<%=AdminMaster.getEnglish("Name")%>&amp;sTableNameDisp=tAdminMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((AdminMaster.isRead("Name",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Organization Name&nbsp;</b></p></td><td colspan="2" valign=top><p><%=AdminMaster.getName()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Name&amp;sTableName=tAdminMaster&amp;sRefID=<%=AdminMaster.getAdminID()%>&amp;sFieldNameDisp=<%=AdminMaster.getEnglish("Name")%>&amp;sTableNameDisp=tAdminMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>




            <%
            if ( (AdminMaster.isRequired("Phone",UserSecurityGroupID))&&(!AdminMaster.isComplete("Phone")) )
            {
                theClass = "requiredField";
            }
            else if ((AdminMaster.isExpired("Phone",expiredDays))&&(AdminMaster.isExpiredCheck("Phone",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((AdminMaster.isWrite("Phone",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Phone</b></p></td><td colspan="2" valign=top><p><input class="tdHeader" onChange="document.getElementById('isSaveMe').value='1';" maxlength="50" type=text size="40" name="Phone" value="<%=AdminMaster.getPhone()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Phone&amp;sTableName=tAdminMaster&amp;sRefID=<%=AdminMaster.getAdminID()%>&amp;sFieldNameDisp=<%=AdminMaster.getEnglish("Phone")%>&amp;sTableNameDisp=tAdminMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((AdminMaster.isRead("Phone",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Organization Phone (XXX-XXX-XXXX)&nbsp;</b></p></td><td colspan="2" valign=top><p><%=AdminMaster.getPhone()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Phone&amp;sTableName=tAdminMaster&amp;sRefID=<%=AdminMaster.getAdminID()%>&amp;sFieldNameDisp=<%=AdminMaster.getEnglish("Phone")%>&amp;sTableNameDisp=tAdminMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>




            <%
            if ( (AdminMaster.isRequired("fax",UserSecurityGroupID))&&(!AdminMaster.isComplete("fax")) )
            {
                theClass = "requiredField";
            }
            else if ((AdminMaster.isExpired("fax",expiredDays))&&(AdminMaster.isExpiredCheck("fax",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((AdminMaster.isWrite("fax",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Fax&nbsp;</b></p></td><td colspan="2" valign=top><p><input maxlength="100" type=text size="80" name="fax" value="<%=AdminMaster.getfax()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=fax&amp;sTableName=tAdminMaster&amp;sRefID=<%=AdminMaster.getAdminID()%>&amp;sFieldNameDisp=<%=AdminMaster.getEnglish("fax")%>&amp;sTableNameDisp=tAdminMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((AdminMaster.isRead("fax",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>fax&nbsp;</b></p></td><td colspan="2" valign=top><p><%=AdminMaster.getfax()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=fax&amp;sTableName=tAdminMaster&amp;sRefID=<%=AdminMaster.getAdminID()%>&amp;sFieldNameDisp=<%=AdminMaster.getEnglish("fax")%>&amp;sTableNameDisp=tAdminMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (AdminMaster.isRequired("Comments",UserSecurityGroupID))&&(!AdminMaster.isComplete("Comments")) )
            {
                theClass = "requiredField";
            }
            else if ((AdminMaster.isExpired("Comments",expiredDays))&&(AdminMaster.isExpiredCheck("Comments",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((AdminMaster.isWrite("Comments",UserSecurityGroupID)))
            {
                        %>
                       <tr><td valign=top><p class=<%=theClass%> ><b>Notes:&nbsp;</b>
                       
<br />
<input name="none1" class="tdBaseAltYellow" type="button" id="none1" onClick="document.getElementById('isSaveMe').value='1'; document.getElementById('Comments').value += '\n[' + Date() + ' - <%=CurrentUserAccount.getLogonUserName()%>]\n';" value="Add Date/Time Stamp">                       
                       
                       </p></td><td colspan="2" valign=top><p><textarea onKeyDown="textAreaStop(this,7000)" rows="10" name="Comments" cols="60"><%=AdminMaster.getComments()%></textarea>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Comments&amp;sTableName=tAdminMaster&amp;sRefID=<%=AdminMaster.getAdminID()%>&amp;sFieldNameDisp=<%=AdminMaster.getEnglish("Comments")%>&amp;sTableNameDisp=tAdminMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((AdminMaster.isRead("Comments",UserSecurityGroupID)))
            {
                        %>
                       <tr><td valign=top> <p class=<%=theClass%> ><b><%=AdminMaster.getEnglish("Comments")%>&nbsp;</b></p></td><td colspan="2" valign=top><p><%=AdminMaster.getComments()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Comments&amp;sTableName=tAdminMaster&amp;sRefID=<%=AdminMaster.getAdminID()%>&amp;sFieldNameDisp=<%=AdminMaster.getEnglish("Comments")%>&amp;sTableNameDisp=tAdminMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>




            <%
            if ( (AdminMaster.isRequired("Address1",UserSecurityGroupID))&&(!AdminMaster.isComplete("Address1")) )
            {
                theClass = "requiredField";
            }
            else if ((AdminMaster.isExpired("Address1",expiredDays))&&(AdminMaster.isExpiredCheck("Address1",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((AdminMaster.isWrite("Address1",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Address&nbsp;</b></p></td><td colspan="2" valign=top><p><input onChange="document.getElementById('isSaveMe').value='1';" maxlength="100" type=text size="80" name="Address1" value="<%=AdminMaster.getAddress1()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Address1&amp;sTableName=tAdminMaster&amp;sRefID=<%=AdminMaster.getAdminID()%>&amp;sFieldNameDisp=<%=AdminMaster.getEnglish("Address1")%>&amp;sTableNameDisp=tAdminMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((AdminMaster.isRead("Address1",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Organization Address&nbsp;</b></p></td><td colspan="2" valign=top><p><%=AdminMaster.getAddress1()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Address1&amp;sTableName=tAdminMaster&amp;sRefID=<%=AdminMaster.getAdminID()%>&amp;sFieldNameDisp=<%=AdminMaster.getEnglish("Address1")%>&amp;sTableNameDisp=tAdminMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (AdminMaster.isRequired("Address2",UserSecurityGroupID))&&(!AdminMaster.isComplete("Address2")) )
            {
                theClass = "requiredField";
            }
            else if ((AdminMaster.isExpired("Address2",expiredDays))&&(AdminMaster.isExpiredCheck("Address2",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((AdminMaster.isWrite("Address2",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Address 2&nbsp;</b></p></td><td colspan="2" valign=top><p><input onChange="document.getElementById('isSaveMe').value='1';" maxlength="100" type=text size="80" name="Address2" value="<%=AdminMaster.getAddress2()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Address2&amp;sTableName=tAdminMaster&amp;sRefID=<%=AdminMaster.getAdminID()%>&amp;sFieldNameDisp=<%=AdminMaster.getEnglish("Address2")%>&amp;sTableNameDisp=tAdminMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((AdminMaster.isRead("Address2",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Organization Address 2&nbsp;</b></p></td><td colspan="2" valign=top><p><%=AdminMaster.getAddress2()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Address2&amp;sTableName=tAdminMaster&amp;sRefID=<%=AdminMaster.getAdminID()%>&amp;sFieldNameDisp=<%=AdminMaster.getEnglish("Address2")%>&amp;sTableNameDisp=tAdminMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (AdminMaster.isRequired("City",UserSecurityGroupID))&&(!AdminMaster.isComplete("City")) )
            {
                theClass = "requiredField";
            }
            else if ((AdminMaster.isExpired("City",expiredDays))&&(AdminMaster.isExpiredCheck("City",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((AdminMaster.isWrite("City",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>City&nbsp;</b></p></td><td colspan="2" valign=top><p><input onChange="document.getElementById('isSaveMe').value='1';" maxlength="100" type=text size="80" name="City" value="<%=AdminMaster.getCity()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=City&amp;sTableName=tAdminMaster&amp;sRefID=<%=AdminMaster.getAdminID()%>&amp;sFieldNameDisp=<%=AdminMaster.getEnglish("City")%>&amp;sTableNameDisp=tAdminMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((AdminMaster.isRead("City",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>City&nbsp;</b></p></td><td colspan="2" valign=top><p><%=AdminMaster.getCity()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=City&amp;sTableName=tAdminMaster&amp;sRefID=<%=AdminMaster.getAdminID()%>&amp;sFieldNameDisp=<%=AdminMaster.getEnglish("City")%>&amp;sTableNameDisp=tAdminMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (AdminMaster.isRequired("StateID",UserSecurityGroupID))&&(!AdminMaster.isComplete("StateID")) )
            {
                theClass = "requiredField";
            }
            else if ((AdminMaster.isExpired("StateID",expiredDays))&&(AdminMaster.isExpiredCheck("StateID",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((AdminMaster.isWrite("StateID",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>State&nbsp;</b></p></td><td colspan="2" valign=top><p><select  onchange="document.getElementById('isSaveMe').value='1';"   name="StateID" ><jsp:include page="../generic/tStateLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=AdminMaster.getStateID()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=StateID&amp;sTableName=tAdminMaster&amp;sRefID=<%=AdminMaster.getAdminID()%>&amp;sFieldNameDisp=<%=AdminMaster.getEnglish("StateID")%>&amp;sTableNameDisp=tAdminMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((AdminMaster.isRead("StateID",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>State&nbsp;</b></p></td><td colspan="2" valign=top><p><jsp:include page="../generic/tStateLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=AdminMaster.getStateID()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=StateID&amp;sTableName=tAdminMaster&amp;sRefID=<%=AdminMaster.getAdminID()%>&amp;sFieldNameDisp=<%=AdminMaster.getEnglish("StateID")%>&amp;sTableNameDisp=tAdminMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (AdminMaster.isRequired("ZIP",UserSecurityGroupID))&&(!AdminMaster.isComplete("ZIP")) )
            {
                theClass = "requiredField";
            }
            else if ((AdminMaster.isExpired("ZIP",expiredDays))&&(AdminMaster.isExpiredCheck("ZIP",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((AdminMaster.isWrite("ZIP",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>ZIP&nbsp;</b></p></td><td colspan="2" valign=top><p><input onChange="document.getElementById('isSaveMe').value='1';" maxlength="50" type=text size="80" name="ZIP" value="<%=AdminMaster.getZIP()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ZIP&amp;sTableName=tAdminMaster&amp;sRefID=<%=AdminMaster.getAdminID()%>&amp;sFieldNameDisp=<%=AdminMaster.getEnglish("ZIP")%>&amp;sTableNameDisp=tAdminMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((AdminMaster.isRead("ZIP",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>ZIP&nbsp;</b></p></td><td colspan="2" valign=top><p><%=AdminMaster.getZIP()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ZIP&amp;sTableName=tAdminMaster&amp;sRefID=<%=AdminMaster.getAdminID()%>&amp;sFieldNameDisp=<%=AdminMaster.getEnglish("ZIP")%>&amp;sTableNameDisp=tAdminMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (AdminMaster.isRequired("AlertEmail",UserSecurityGroupID))&&(!AdminMaster.isComplete("AlertEmail")) )
            {
                theClass = "requiredField";
            }
            else if ((AdminMaster.isExpired("AlertEmail",expiredDays))&&(AdminMaster.isExpiredCheck("AlertEmail",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((AdminMaster.isWrite("AlertEmail",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Alert Email&nbsp;</b></p></td><td colspan="2" valign=top><p><input onChange="document.getElementById('isSaveMe').value='1';" maxlength="50" type=text size="80" name="AlertEmail" value="<%=AdminMaster.getAlertEmail()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=AlertEmail&amp;sTableName=tAdminMaster&amp;sRefID=<%=AdminMaster.getAdminID()%>&amp;sFieldNameDisp=<%=AdminMaster.getEnglish("AlertEmail")%>&amp;sTableNameDisp=tAdminMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((AdminMaster.isRead("AlertEmail",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Alert Email&nbsp;</b></p></td><td colspan="2" valign=top><p><%=AdminMaster.getAlertEmail()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=AlertEmail&amp;sTableName=tAdminMaster&amp;sRefID=<%=AdminMaster.getAdminID()%>&amp;sFieldNameDisp=<%=AdminMaster.getEnglish("AlertEmail")%>&amp;sTableNameDisp=tAdminMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (AdminMaster.isRequired("AlertDays",UserSecurityGroupID))&&(!AdminMaster.isComplete("AlertDays")) )
            {
                theClass = "requiredField";
            }
            else if ((AdminMaster.isExpired("AlertDays",expiredDays))&&(AdminMaster.isExpiredCheck("AlertDays",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((AdminMaster.isWrite("AlertDays",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Alert Days&nbsp;</b></p></td><td colspan="2" valign=top><p><input onChange="document.getElementById('isSaveMe').value='1';" maxlength="20" type=text size="80" name="AlertDays" value="<%=AdminMaster.getAlertDays()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=AlertDays&amp;sTableName=tAdminMaster&amp;sRefID=<%=AdminMaster.getAdminID()%>&amp;sFieldNameDisp=<%=AdminMaster.getEnglish("AlertDays")%>&amp;sTableNameDisp=tAdminMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((AdminMaster.isRead("AlertDays",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Alert Days&nbsp;</b></p></td><td colspan="2" valign=top><p><%=AdminMaster.getAlertDays()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=AlertDays&amp;sTableName=tAdminMaster&amp;sRefID=<%=AdminMaster.getAdminID()%>&amp;sFieldNameDisp=<%=AdminMaster.getEnglish("AlertDays")%>&amp;sTableNameDisp=tAdminMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (AdminMaster.isRequired("ContactFirstName",UserSecurityGroupID))&&(!AdminMaster.isComplete("ContactFirstName")) )
            {
                theClass = "requiredField";
            }
            else if ((AdminMaster.isExpired("ContactFirstName",expiredDays))&&(AdminMaster.isExpiredCheck("ContactFirstName",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((AdminMaster.isWrite("ContactFirstName",UserSecurityGroupID)))
            {
                        %>
                     <tr class="tdHeaderAlt">
                       <td valign=top colspan="3"><hr></td>
                     </tr>
                     <tr class="tdHeaderAlt">
                       <td valign=top>Contact Information</td>
                       <td colspan="2" valign=top>&nbsp;</td>
                     </tr>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Contact First Name&nbsp;</b></p></td><td colspan="2" valign=top><p><input class="tdHeader" onChange="document.getElementById('isSaveMe').value='1';" maxlength="50" type=text size="40" name="ContactFirstName" value="<%=AdminMaster.getContactFirstName()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ContactFirstName&amp;sTableName=tAdminMaster&amp;sRefID=<%=AdminMaster.getAdminID()%>&amp;sFieldNameDisp=<%=AdminMaster.getEnglish("ContactFirstName")%>&amp;sTableNameDisp=tAdminMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((AdminMaster.isRead("ContactFirstName",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Contact First Name&nbsp;</b></p></td><td colspan="2" valign=top><p><%=AdminMaster.getContactFirstName()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ContactFirstName&amp;sTableName=tAdminMaster&amp;sRefID=<%=AdminMaster.getAdminID()%>&amp;sFieldNameDisp=<%=AdminMaster.getEnglish("ContactFirstName")%>&amp;sTableNameDisp=tAdminMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (AdminMaster.isRequired("ContactLastName",UserSecurityGroupID))&&(!AdminMaster.isComplete("ContactLastName")) )
            {
                theClass = "requiredField";
            }
            else if ((AdminMaster.isExpired("ContactLastName",expiredDays))&&(AdminMaster.isExpiredCheck("ContactLastName",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((AdminMaster.isWrite("ContactLastName",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Contact Last Name&nbsp;</b></p></td><td colspan="2" valign=top><p><input class="tdHeader" onChange="document.getElementById('isSaveMe').value='1';" maxlength="50" type=text size="40" name="ContactLastName" value="<%=AdminMaster.getContactLastName()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ContactLastName&amp;sTableName=tAdminMaster&amp;sRefID=<%=AdminMaster.getAdminID()%>&amp;sFieldNameDisp=<%=AdminMaster.getEnglish("ContactLastName")%>&amp;sTableNameDisp=tAdminMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((AdminMaster.isRead("ContactLastName",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Contact Last Name&nbsp;</b></p></td><td colspan="2" valign=top><p><%=AdminMaster.getContactLastName()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ContactLastName&amp;sTableName=tAdminMaster&amp;sRefID=<%=AdminMaster.getAdminID()%>&amp;sFieldNameDisp=<%=AdminMaster.getEnglish("ContactLastName")%>&amp;sTableNameDisp=tAdminMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (AdminMaster.isRequired("ContactPhone",UserSecurityGroupID))&&(!AdminMaster.isComplete("ContactPhone")) )
            {
                theClass = "requiredField";
            }
            else if ((AdminMaster.isExpired("ContactPhone",expiredDays))&&(AdminMaster.isExpiredCheck("ContactPhone",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((AdminMaster.isWrite("ContactPhone",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Contact Phone</b></p></td><td colspan="2" valign=top><p><input onChange="document.getElementById('isSaveMe').value='1';" maxlength="50" type=text size="80" name="ContactPhone" value="<%=AdminMaster.getContactPhone()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ContactPhone&amp;sTableName=tAdminMaster&amp;sRefID=<%=AdminMaster.getAdminID()%>&amp;sFieldNameDisp=<%=AdminMaster.getEnglish("ContactPhone")%>&amp;sTableNameDisp=tAdminMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((AdminMaster.isRead("ContactPhone",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Contact Phone (XXX-XXX-XXXX)&nbsp;</b></p></td><td colspan="2" valign=top><p><%=AdminMaster.getContactPhone()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ContactPhone&amp;sTableName=tAdminMaster&amp;sRefID=<%=AdminMaster.getAdminID()%>&amp;sFieldNameDisp=<%=AdminMaster.getEnglish("ContactPhone")%>&amp;sTableNameDisp=tAdminMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (AdminMaster.isRequired("ContactFax",UserSecurityGroupID))&&(!AdminMaster.isComplete("ContactFax")) )
            {
                theClass = "requiredField";
            }
            else if ((AdminMaster.isExpired("ContactFax",expiredDays))&&(AdminMaster.isExpiredCheck("ContactFax",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((AdminMaster.isWrite("ContactFax",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Contact Fax&nbsp;</b></p></td><td colspan="2" valign=top><p><input maxlength="50" type=text size="80" name="ContactFax" value="<%=AdminMaster.getContactFax()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ContactFax&amp;sTableName=tAdminMaster&amp;sRefID=<%=AdminMaster.getAdminID()%>&amp;sFieldNameDisp=<%=AdminMaster.getEnglish("ContactFax")%>&amp;sTableNameDisp=tAdminMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((AdminMaster.isRead("ContactFax",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>ContactFax&nbsp;</b></p></td><td colspan="2" valign=top><p><%=AdminMaster.getContactFax()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ContactFax&amp;sTableName=tAdminMaster&amp;sRefID=<%=AdminMaster.getAdminID()%>&amp;sFieldNameDisp=<%=AdminMaster.getEnglish("ContactFax")%>&amp;sTableNameDisp=tAdminMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>





            <%
            if ( (AdminMaster.isRequired("ContactEmail",UserSecurityGroupID))&&(!AdminMaster.isComplete("ContactEmail")) )
            {
                theClass = "requiredField";
            }
            else if ((AdminMaster.isExpired("ContactEmail",expiredDays))&&(AdminMaster.isExpiredCheck("ContactEmail",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((AdminMaster.isWrite("ContactEmail",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Contact Email&nbsp;</b></p></td><td colspan="2" valign=top><p><input onChange="document.getElementById('isSaveMe').value='1';" maxlength="75" type=text size="80" name="ContactEmail" value="<%=AdminMaster.getContactEmail()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ContactEmail&amp;sTableName=tAdminMaster&amp;sRefID=<%=AdminMaster.getAdminID()%>&amp;sFieldNameDisp=<%=AdminMaster.getEnglish("ContactEmail")%>&amp;sTableNameDisp=tAdminMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((AdminMaster.isRead("ContactEmail",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Contact Email&nbsp;</b></p></td><td colspan="2" valign=top><p><%=AdminMaster.getContactEmail()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ContactEmail&amp;sTableName=tAdminMaster&amp;sRefID=<%=AdminMaster.getAdminID()%>&amp;sFieldNameDisp=<%=AdminMaster.getEnglish("ContactEmail")%>&amp;sTableNameDisp=tAdminMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (AdminMaster.isRequired("ContactAddress1",UserSecurityGroupID))&&(!AdminMaster.isComplete("ContactAddress1")) )
            {
                theClass = "requiredField";
            }
            else if ((AdminMaster.isExpired("ContactAddress1",expiredDays))&&(AdminMaster.isExpiredCheck("ContactAddress1",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((AdminMaster.isWrite("ContactAddress1",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Contact Address&nbsp;</b></p></td><td colspan="2" valign=top><p><input onChange="document.getElementById('isSaveMe').value='1';" maxlength="50" type=text size="80" name="ContactAddress1" value="<%=AdminMaster.getContactAddress1()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ContactAddress1&amp;sTableName=tAdminMaster&amp;sRefID=<%=AdminMaster.getAdminID()%>&amp;sFieldNameDisp=<%=AdminMaster.getEnglish("ContactAddress1")%>&amp;sTableNameDisp=tAdminMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((AdminMaster.isRead("ContactAddress1",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Contact Address&nbsp;</b></p></td><td colspan="2" valign=top><p><%=AdminMaster.getContactAddress1()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ContactAddress1&amp;sTableName=tAdminMaster&amp;sRefID=<%=AdminMaster.getAdminID()%>&amp;sFieldNameDisp=<%=AdminMaster.getEnglish("ContactAddress1")%>&amp;sTableNameDisp=tAdminMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (AdminMaster.isRequired("ContactAddress2",UserSecurityGroupID))&&(!AdminMaster.isComplete("ContactAddress2")) )
            {
                theClass = "requiredField";
            }
            else if ((AdminMaster.isExpired("ContactAddress2",expiredDays))&&(AdminMaster.isExpiredCheck("ContactAddress2",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((AdminMaster.isWrite("ContactAddress2",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Contact Address 2&nbsp;</b></p></td><td colspan="2" valign=top><p><input onChange="document.getElementById('isSaveMe').value='1';" maxlength="20" type=text size="80" name="ContactAddress2" value="<%=AdminMaster.getContactAddress2()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ContactAddress2&amp;sTableName=tAdminMaster&amp;sRefID=<%=AdminMaster.getAdminID()%>&amp;sFieldNameDisp=<%=AdminMaster.getEnglish("ContactAddress2")%>&amp;sTableNameDisp=tAdminMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((AdminMaster.isRead("ContactAddress2",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Contact Address 2&nbsp;</b></p></td><td colspan="2" valign=top><p><%=AdminMaster.getContactAddress2()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ContactAddress2&amp;sTableName=tAdminMaster&amp;sRefID=<%=AdminMaster.getAdminID()%>&amp;sFieldNameDisp=<%=AdminMaster.getEnglish("ContactAddress2")%>&amp;sTableNameDisp=tAdminMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (AdminMaster.isRequired("ContactCity",UserSecurityGroupID))&&(!AdminMaster.isComplete("ContactCity")) )
            {
                theClass = "requiredField";
            }
            else if ((AdminMaster.isExpired("ContactCity",expiredDays))&&(AdminMaster.isExpiredCheck("ContactCity",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((AdminMaster.isWrite("ContactCity",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Contact City&nbsp;</b></p></td><td colspan="2" valign=top><p><input onChange="document.getElementById('isSaveMe').value='1';" maxlength="30" type=text size="80" name="ContactCity" value="<%=AdminMaster.getContactCity()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ContactCity&amp;sTableName=tAdminMaster&amp;sRefID=<%=AdminMaster.getAdminID()%>&amp;sFieldNameDisp=<%=AdminMaster.getEnglish("ContactCity")%>&amp;sTableNameDisp=tAdminMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((AdminMaster.isRead("ContactCity",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Contact City&nbsp;</b></p></td><td colspan="2" valign=top><p><%=AdminMaster.getContactCity()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ContactCity&amp;sTableName=tAdminMaster&amp;sRefID=<%=AdminMaster.getAdminID()%>&amp;sFieldNameDisp=<%=AdminMaster.getEnglish("ContactCity")%>&amp;sTableNameDisp=tAdminMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (AdminMaster.isRequired("ContactStateID",UserSecurityGroupID))&&(!AdminMaster.isComplete("ContactStateID")) )
            {
                theClass = "requiredField";
            }
            else if ((AdminMaster.isExpired("ContactStateID",expiredDays))&&(AdminMaster.isExpiredCheck("ContactStateID",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((AdminMaster.isWrite("ContactStateID",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Contact State&nbsp;</b></p></td><td colspan="2" valign=top><p><select  onchange="document.getElementById('isSaveMe').value='1';"   name="ContactStateID" ><jsp:include page="../generic/tStateLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=AdminMaster.getContactStateID()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ContactStateID&amp;sTableName=tAdminMaster&amp;sRefID=<%=AdminMaster.getAdminID()%>&amp;sFieldNameDisp=<%=AdminMaster.getEnglish("ContactStateID")%>&amp;sTableNameDisp=tAdminMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((AdminMaster.isRead("ContactStateID",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Contact State&nbsp;</b></p></td><td colspan="2" valign=top><p><jsp:include page="../generic/tStateLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=AdminMaster.getContactStateID()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ContactStateID&amp;sTableName=tAdminMaster&amp;sRefID=<%=AdminMaster.getAdminID()%>&amp;sFieldNameDisp=<%=AdminMaster.getEnglish("ContactStateID")%>&amp;sTableNameDisp=tAdminMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (AdminMaster.isRequired("ContactZIP",UserSecurityGroupID))&&(!AdminMaster.isComplete("ContactZIP")) )
            {
                theClass = "requiredField";
            }
            else if ((AdminMaster.isExpired("ContactZIP",expiredDays))&&(AdminMaster.isExpiredCheck("ContactZIP",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((AdminMaster.isWrite("ContactZIP",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Contact ZIP&nbsp;</b></p></td><td colspan="2" valign=top><p><input onChange="document.getElementById('isSaveMe').value='1';" maxlength="50" type=text size="80" name="ContactZIP" value="<%=AdminMaster.getContactZIP()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ContactZIP&amp;sTableName=tAdminMaster&amp;sRefID=<%=AdminMaster.getAdminID()%>&amp;sFieldNameDisp=<%=AdminMaster.getEnglish("ContactZIP")%>&amp;sTableNameDisp=tAdminMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((AdminMaster.isRead("ContactZIP",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Contact ZIP&nbsp;</b></p></td><td colspan="2" valign=top><p><%=AdminMaster.getContactZIP()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ContactZIP&amp;sTableName=tAdminMaster&amp;sRefID=<%=AdminMaster.getAdminID()%>&amp;sFieldNameDisp=<%=AdminMaster.getEnglish("ContactZIP")%>&amp;sTableNameDisp=tAdminMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>






            <tr><td>&nbsp;</td><td colspan="2">&nbsp;</td></tr>
            </table>
        <%if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {%>
            <input onChange="document.getElementById('isSaveMe').value='1';" type=hidden name=routePageReference value="sParentReturnPage">
             <%
              if (request.getParameter("INTNext")!=null&&request.getParameter("INTNext").equalsIgnoreCase("yes") ) 
              {
              %>
                  <table width=75% border=1 bordercolor=333333 align=left cellspacing=0 cellpadding=0 class=wizardTable>
                  <tr class=requiredField><td>
                  <input onChange="document.getElementById('isSaveMe').value='1';"  <%=HTMLFormStyleButton%> type="radio" value="next" name="INTNext" checked>&nbsp;&nbsp;<%=ConfigurationMessages.getInterviewMessage("INTERVIEWNoMore","tAdminMaster")%>
                  <br>
                  <input onChange="document.getElementById('isSaveMe').value='1';"  <%=HTMLFormStyleButton%> type="radio" value="yes" name="INTNext">&nbsp;&nbsp;<%=ConfigurationMessages.getInterviewMessage("INTERVIEWAddMore","tAdminMaster")%>
                  </td></tr></table><br><br><br>
              <%
              }
              %>
            <p><input class="tdBaseAltGreen" onClick="document.getElementById('isSaveMe').value='0';" type=Submit value="Continue" name=Submit></p>
        <%}%>
        </td></tr></table>
        </form>
        <%
  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>

    </td></tr></table>


