<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.bltDocumentManagement,java.util.Vector,java.util.Enumeration,com.winstaff.ListElement, com.winstaff.PLCUtils, com.winstaff.SecurityCheck, com.winstaff.ConfigurationMessages,com.winstaff.bltBoardCertification,com.winstaff.bltBoardCertification_List,com.winstaff.DocumentManagerUtils" %>
<%/*
    filename: tBoardCertification_main_BoardCertification_PhysicianID.jsp
    Created on Nov/12/2003
    Type: 1-n main class file
    Created by: Scott Ellis
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>
    <%
    Integer iExpressMode = new Integer(1);
    if (pageControllerHash.containsKey("iExpressMode")) 
    {
        iExpressMode =    (Integer)pageControllerHash.get("iExpressMode");
    }
    %>

    <table cellpadding=0 cellspacing=0 border=0 width=800 >
<%
//initial declaration of list class and parentID
    Integer        iPhysicianID        =    null;
    boolean accessValid = false;
   Integer iSecurityCheck = SecurityCheck.CheckItem("PractitionerSection4", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {
    if (pageControllerHash.containsKey("iPhysicianID")) 
    {
        iPhysicianID        =    (Integer)pageControllerHash.get("iPhysicianID");
        accessValid = true;
    }

  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);
      pageControllerHash.put("sParentReturnPage","tBoardCertification_main_BoardCertification_PhysicianID_Expand.jsp");
    pageControllerHash.remove("iBoardCertificationID");
    session.setAttribute("pageControllerHash",pageControllerHash);

    bltBoardCertification_List        bltBoardCertification_List        =    new    bltBoardCertification_List(iPhysicianID);

//declaration of Enumeration
    bltBoardCertification        BoardCertification;
    ListElement         leCurrentElement;
    Enumeration eList = bltBoardCertification_List.elements();
    %>
    <%
    int iExpress=0;
boolean errorRouteMeTotal = false;
boolean isAllComplete = true;
    while (eList.hasMoreElements()||iExpress<=ConfigurationMessages.getExpressItemCount("tBoardCertification"))
    {
       iExpress++;

      boolean isNewRecord= false;
      if (eList.hasMoreElements())
      {
        leCurrentElement    = (ListElement) eList.nextElement();
        BoardCertification  = (bltBoardCertification) leCurrentElement.getObject();
      }
      else
      {
        BoardCertification  = new bltBoardCertification();
        BoardCertification.setPhysicianID(iPhysicianID);
        isNewRecord= true;
      }
        BoardCertification.GroupSecurityInit(UserSecurityGroupID);
        String theClass = "tdHeader";
        %>

        <%  {

String testChangeID = "0";

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("UniqueCreateDate_"+iExpress))) ;
    if ( !BoardCertification.getUniqueCreateDate().equals(testObj)  )
    {
         BoardCertification.setUniqueCreateDate( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("BoardCertification UniqueCreateDate not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("UniqueModifyDate_"+iExpress))) ;
    if ( !BoardCertification.getUniqueModifyDate().equals(testObj)  )
    {
         BoardCertification.setUniqueModifyDate( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("BoardCertification UniqueModifyDate not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("UniqueModifyComments_"+iExpress)) ;
    if ( !BoardCertification.getUniqueModifyComments().equals(testObj)  )
    {
         BoardCertification.setUniqueModifyComments( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("BoardCertification UniqueModifyComments not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("PhysicianID_"+iExpress)) ;
    if ( !BoardCertification.getPhysicianID().equals(testObj)  )
    {
         BoardCertification.setPhysicianID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("BoardCertification PhysicianID not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("Specialty_"+iExpress)) ;
    if ( !BoardCertification.getSpecialty().equals(testObj)  )
    {
         BoardCertification.setSpecialty( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("BoardCertification Specialty not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("SubSpecialty_"+iExpress)) ;
    if ( !BoardCertification.getSubSpecialty().equals(testObj)  )
    {
         BoardCertification.setSubSpecialty( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("BoardCertification SubSpecialty not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("IsListed_"+iExpress)) ;
    if ( !BoardCertification.getIsListed().equals(testObj)  )
    {
         BoardCertification.setIsListed( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("BoardCertification IsListed not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("IsPrimary_"+iExpress)) ;
    if ( !BoardCertification.getIsPrimary().equals(testObj)  )
    {
         BoardCertification.setIsPrimary( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("BoardCertification IsPrimary not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("SpecialtyBoardCertified_"+iExpress)) ;
    if ( !BoardCertification.getSpecialtyBoardCertified().equals(testObj)  )
    {
         BoardCertification.setSpecialtyBoardCertified( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("BoardCertification SpecialtyBoardCertified not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("BoardName_"+iExpress)) ;
    if ( !BoardCertification.getBoardName().equals(testObj)  )
    {
         BoardCertification.setBoardName( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("BoardCertification BoardName not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("ContactName_"+iExpress)) ;
    if ( !BoardCertification.getContactName().equals(testObj)  )
    {
         BoardCertification.setContactName( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("BoardCertification ContactName not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("ContactEmail_"+iExpress)) ;
    if ( !BoardCertification.getContactEmail().equals(testObj)  )
    {
         BoardCertification.setContactEmail( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("BoardCertification ContactEmail not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("Address1_"+iExpress)) ;
    if ( !BoardCertification.getAddress1().equals(testObj)  )
    {
         BoardCertification.setAddress1( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("BoardCertification Address1 not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("Address2_"+iExpress)) ;
    if ( !BoardCertification.getAddress2().equals(testObj)  )
    {
         BoardCertification.setAddress2( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("BoardCertification Address2 not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("City_"+iExpress)) ;
    if ( !BoardCertification.getCity().equals(testObj)  )
    {
         BoardCertification.setCity( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("BoardCertification City not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("StateID_"+iExpress)) ;
    if ( !BoardCertification.getStateID().equals(testObj)  )
    {
         BoardCertification.setStateID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("BoardCertification StateID not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("Province_"+iExpress)) ;
    if ( !BoardCertification.getProvince().equals(testObj)  )
    {
         BoardCertification.setProvince( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("BoardCertification Province not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("ZIP_"+iExpress)) ;
    if ( !BoardCertification.getZIP().equals(testObj)  )
    {
         BoardCertification.setZIP( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("BoardCertification ZIP not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("CountryID_"+iExpress)) ;
    if ( !BoardCertification.getCountryID().equals(testObj)  )
    {
         BoardCertification.setCountryID( testObj,UserSecurityGroupID   );
         //testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("BoardCertification CountryID not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("Phone_"+iExpress)) ;
    if ( !BoardCertification.getPhone().equals(testObj)  )
    {
         BoardCertification.setPhone( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("BoardCertification Phone not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("Fax_"+iExpress)) ;
    if ( !BoardCertification.getFax().equals(testObj)  )
    {
         BoardCertification.setFax( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("BoardCertification Fax not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("BoardDateInitialCertified_"+iExpress))) ;
    if ( !BoardCertification.getBoardDateInitialCertified().equals(testObj)  )
    {
         BoardCertification.setBoardDateInitialCertified( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("BoardCertification BoardDateInitialCertified not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("BoardDateRecertified_"+iExpress))) ;
    if ( !BoardCertification.getBoardDateRecertified().equals(testObj)  )
    {
         BoardCertification.setBoardDateRecertified( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("BoardCertification BoardDateRecertified not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("ExpirationDate_"+iExpress))) ;
    if ( !BoardCertification.getExpirationDate().equals(testObj)  )
    {
         BoardCertification.setExpirationDate( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("BoardCertification ExpirationDate not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("DocumentNumber_"+iExpress)) ;
    if ( !BoardCertification.getDocumentNumber().equals(testObj)  )
    {
         BoardCertification.setDocumentNumber( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("BoardCertification DocumentNumber not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("IfNotCert_"+iExpress)) ;
    if ( !BoardCertification.getIfNotCert().equals(testObj)  )
    {
         BoardCertification.setIfNotCert( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("BoardCertification IfNotCert not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("CertEligible_"+iExpress)) ;
    if ( !BoardCertification.getCertEligible().equals(testObj)  )
    {
         BoardCertification.setCertEligible( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("BoardCertification CertEligible not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("EligibleStartDate_"+iExpress))) ;
    if ( !BoardCertification.getEligibleStartDate().equals(testObj)  )
    {
         BoardCertification.setEligibleStartDate( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("BoardCertification EligibleStartDate not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("EligibleExpirationDate_"+iExpress))) ;
    if ( !BoardCertification.getEligibleExpirationDate().equals(testObj)  )
    {
         BoardCertification.setEligibleExpirationDate( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("BoardCertification EligibleExpirationDate not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("CertPlanningToTake_"+iExpress)) ;
    if ( !BoardCertification.getCertPlanningToTake().equals(testObj)  )
    {
         BoardCertification.setCertPlanningToTake( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("BoardCertification CertPlanningToTake not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("CertPlanDate_"+iExpress))) ;
    if ( !BoardCertification.getCertPlanDate().equals(testObj)  )
    {
         BoardCertification.setCertPlanDate( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("BoardCertification CertPlanDate not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("DocuLinkID_"+iExpress)) ;
    if ( !BoardCertification.getDocuLinkID().equals(testObj)  )
    {
         BoardCertification.setDocuLinkID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("BoardCertification DocuLinkID not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("Comments_"+iExpress)) ;
    if ( !BoardCertification.getComments().equals(testObj)  )
    {
         BoardCertification.setComments( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("BoardCertification Comments not set. this is ok-not an error");
}
boolean bINT = false;
boolean bNextStep = false;
String sRefreshDoc = "refreshMe=true&";
              if (false&&request.getParameter("INTNext")!=null&&request.getParameter("INTNext").equalsIgnoreCase("yes") ) 
	            {
	                bINT = true;
	                sRefreshDoc = "";
	            }
              else if (request.getParameter("INTNext")!=null&&request.getParameter("INTNext").equalsIgnoreCase("next") ) 
	            {
	                bINT = true;
	                sINTNext = ConfigurationMessages.getInterviewLinkRaw("tBoardCertification","next");
	                sRefreshDoc = "";
	            }

// If an edit, update information; if new, append information
boolean errorRouteMe = false;
if (testChangeID.equalsIgnoreCase("1"))
{

	BoardCertification.setUniqueModifyDate(new java.util.Date());
	String UserLogonDescription = "n/a";
	if (pageControllerHash.containsKey("UserLogonDescription"))
	{
	    UserLogonDescription = (String)pageControllerHash.get("UserLogonDescription");
	}
    if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
    {
	    BoardCertification.setUniqueModifyComments("EX:"+UserLogonDescription);
	    try
	    {
	        BoardCertification.commitData();
	        if (!BoardCertification.isComplete())
	        {
	            isAllComplete = false;
	        }
	    }
	    catch (Exception e55)
	    {
	        errorRouteMe = true;
	        errorRouteMeTotal = true;
	    }	//Doculink Addition
	//Doculink Does Exist, prompt to Modify

     if (!errorRouteMe&&BoardCertification.getDocuLinkID().intValue()>0)
     {
            bltDocumentManagement myDoc = new bltDocumentManagement(BoardCertification.getDocuLinkID());
            if (myDoc.getDocumentFileName().equalsIgnoreCase("")) 
            {
                DocumentManagerUtils myDUtils = new DocumentManagerUtils();
                String myDType = "tBoardCertification";
                myDUtils.setDocumentType(myDType, BoardCertification.getUniqueID());
                myDoc.setDocumentName(myDUtils.getDocumentName());
                myDoc.setDateOfExpiration(myDUtils.getDateOfExpiration());
                myDoc.setDocumentTypeID(myDUtils.getDocumentTypeID());
                myDoc.setPhysicianID(iPhysicianID);
                myDoc.setArchived(new Integer(2));
                myDoc.commitData();
            }
            else
            {
                //archive then create new
                myDoc.setArchived(new Integer("1"));
                myDoc.commitData();
                myDoc = new bltDocumentManagement();
                DocumentManagerUtils myDUtils = new DocumentManagerUtils();
                String myDType = "tBoardCertification";
                myDUtils.setDocumentType(myDType, BoardCertification.getUniqueID());
                myDoc.setDocumentName(myDUtils.getDocumentName());
                myDoc.setDateOfExpiration(myDUtils.getDateOfExpiration());
                myDoc.setDocumentTypeID(myDUtils.getDocumentTypeID());
                myDoc.setPhysicianID(iPhysicianID);
                myDoc.setArchived(new Integer(2));
                myDoc.commitData();
                BoardCertification.setDocuLinkID(myDoc.getUniqueID());
                BoardCertification.commitData();
            }
     }
     else if (!errorRouteMe)
     {
            bltDocumentManagement myDoc = new bltDocumentManagement();
            DocumentManagerUtils myDUtils = new DocumentManagerUtils();
            String myDType = "tBoardCertification";
            myDUtils.setDocumentType(myDType, BoardCertification.getUniqueID());
            myDoc.setDocumentName(myDUtils.getDocumentName());
            myDoc.setDateOfExpiration(myDUtils.getDateOfExpiration());
            myDoc.setDocumentTypeID(myDUtils.getDocumentTypeID());
                myDoc.setPhysicianID(iPhysicianID);
            myDoc.setArchived(new Integer(2));
            myDoc.commitData();
            BoardCertification.setDocuLinkID(myDoc.getUniqueID());
            BoardCertification.commitData();
     }

    }
}       }
    }//while

if (errorRouteMeTotal)
{
	out.println(ConfigurationMessages.getMessage("FatalError")); 
}
else 
{
String nextPage = request.getParameter("nextPage");
if (nextPage==null||nextPage.equalsIgnoreCase(""))
{
	nextPage = ConfigurationMessages.getExpressLinkRaw("tBoardCertification","next",iExpressMode);
}
if (!isAllComplete)
{
	%>
	<script language=Javascript>
	      if ( confirm("<%=ConfigurationMessages.getMessage("ConfirmReqFieldsReturn")%>") )
	      {
	          document.location="<%=ConfigurationMessages.getDataCategoryLink("tBoardCertification","Express")%>";
	      }
	      else 
	      {
	          document.location="<%=nextPage%>";
	      }
	</script>
  <%
}
else
{
	%>
	<script language=Javascript>
	      document.location="<%=nextPage%>";
	</script>
  <%
}
}
       
  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>

    </td></tr></table>


<%String bnIncludeFN = "ui_"+thePLCID+"\\bot-nav_PhysicianID.jsp";
%>
<jsp:include page="<%=bnIncludeFN%>" flush="true" ><jsp:param name="plcID" value="<%=thePLCID%>"/></jsp:include>
