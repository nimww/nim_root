<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.bltDocumentManagement,java.util.Vector,java.util.Enumeration,com.winstaff.ListElement, com.winstaff.PLCUtils, com.winstaff.SecurityCheck, com.winstaff.ConfigurationMessages,com.winstaff.bltAdminPracticeLU,com.winstaff.bltAdminPracticeLU_List_LU_AdminID" %>
<%/*
    filename: tAdminPracticeLU_main_LU_AdminID.jsp
    Created on Mar/21/2003
    Type: 1-n main class file
    Created by: Scott Ellis
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>

<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_AdminID.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>

    <table cellpadding=0 cellspacing=0 border=0 width=<%=MasterTableWidth%> >
    <tr><td width=10>&nbsp;</td><td>
    <%=ConfigurationMessages.getHTML("INTERVIEWTopControl","tAdminPracticeLU")%>



<%
//initial declaration of list class and parentID
    Integer        iAdminID        =    null;
    boolean accessValid = false;
   Integer iSecurityCheck = SecurityCheck.CheckItem("Practice1", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {
    if (pageControllerHash.containsKey("iAdminID")) 
    {
        iAdminID        =    (Integer)pageControllerHash.get("iAdminID");
        accessValid = true;
    }
  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);
      pageControllerHash.put("sParentReturnPage","tAdminPracticeLU_main_LU_AdminID.jsp");
    pageControllerHash.remove("iLookupID");
    pageControllerHash.put("sINTNext","tAdminPracticeLU_main_LU_AdminID_form_create.jsp?EDIT=new&KM=p&INTNext=yes");
    session.setAttribute("pageControllerHash",pageControllerHash);

    bltAdminPracticeLU_List_LU_AdminID        bltAdminPracticeLU_List_LU_AdminID        =    new    bltAdminPracticeLU_List_LU_AdminID(iAdminID);

//declaration of Enumeration
    bltAdminPracticeLU        working_bltAdminPracticeLU;
    ListElement         leCurrentElement;
    java.util.Enumeration eList = bltAdminPracticeLU_List_LU_AdminID.elements();
    %>
        <%@ include file="tAdminPracticeLU_main_LU_AdminID_instructions.jsp" %>

        <%if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {%>
        <a class=linkBase href = "tAdminPracticeLU_main_LU_AdminID_form_create.jsp?EDIT=new&KM=p&INTNext=yes"><img border=0 src="ui_<%=thePLCID%>/icons/create_AdminID.gif"></a>
        <%}%>
         <table border="1" bordercolor="CCCCCC" cellpadding="3" class=tdBase cellspacing="0" width="100%">
    <%
    int altCnt = 0;
    if (eList.hasMoreElements())
    {
     while (eList.hasMoreElements())
     {

        altCnt++;
        String theClass = "tdBase";
        if (altCnt%2!=0)
        {
            theClass = "tdBaseAlt";
        }
        leCurrentElement    = (ListElement) eList.nextElement();
        working_bltAdminPracticeLU  = (bltAdminPracticeLU) leCurrentElement.getObject();
        working_bltAdminPracticeLU.GroupSecurityInit(UserSecurityGroupID);
        if (!working_bltAdminPracticeLU.isComplete())
        {
            theClass = "incompleteItem";
        %>
                <tr class=incompleteItem><td><b>Not Complete</b><br>
        <%
        }
        else
        {
        %>
        <tr class=<%=theClass%> ><td> 
        <%
        }
        %>

              <b>Item ID:&nbsp;</b><%=working_bltAdminPracticeLU.getLookupID()%></td>
<%String theClassF = "textBase";%>

<%theClassF = "textBase";%>
<%if ((working_bltAdminPracticeLU.isExpired("PracticeID",expiredDays))&&(working_bltAdminPracticeLU.isExpiredCheck("PracticeID"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltAdminPracticeLU.isRequired("PracticeID"))&&(!working_bltAdminPracticeLU.isComplete("PracticeID")) ){theClassF = "requiredFieldMain";}%>
            <td><p class=<%=theClassF%> ><b>PracticeID:&nbsp;</b><%=working_bltAdminPracticeLU.getPracticeID()%></p></td>

<%theClassF = "textBase";%>
<%if ((working_bltAdminPracticeLU.isExpired("Comments",expiredDays))&&(working_bltAdminPracticeLU.isExpiredCheck("Comments"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltAdminPracticeLU.isRequired("Comments"))&&(!working_bltAdminPracticeLU.isComplete("Comments")) ){theClassF = "requiredFieldMain";}%>
            <td><p class=<%=theClassF%> ><b>Extra Comments:&nbsp;</b><%=working_bltAdminPracticeLU.getComments()%></p></td>

            <td > 
        <a class=linkBase href = "tAdminPracticeLU_main_LU_AdminID_form_authorize.jsp?EDIT=edit&EDITID=<%=working_bltAdminPracticeLU.getLookupID()%>&KM=p"><img border=0 src="ui_<%=thePLCID%>/icons/edit_AdminID.gif"></a>

        <%if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {%>
        <a class=linkBase  onClick="return confirmDelete()"  href = "tAdminPracticeLU_main_LU_AdminID_form_authorize.jsp?EDIT=del&EDITID=<%=working_bltAdminPracticeLU.getLookupID()%>&KM=p"><img border=0 src="ui_<%=thePLCID%>/icons/delete_AdminID.gif"></a>
        <% }%>
                  </td></tr>
        <%
    }//end while
       }//end of if
       else 
       {
           %>
           <tr><td><b>Please click the "create" to add <%=ConfigurationMessages.getDataCategory("tAdminPracticeLU")%> information or click 'Continue' to go to the next section.</b>
           <script language=javascript>
           if (confirm("<%=ConfigurationMessages.getInterviewMessage("INTERVIEWNoElements","tAdminPracticeLU")%>"))
           {
               document.location="tAdminPracticeLU_main_LU_AdminID_form_create.jsp?EDIT=new&KM=p&INTNext=yes"; 
           }
           else
           {

           }
           </script>
           </td></tr>
           <%
       }
    %>

    </table>
    <%
  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>

    </td></tr></table><br>


<%String bnIncludeFN = "ui_"+thePLCID+"\\bot-nav_AdminID.jsp";
%>
<jsp:include page="<%=bnIncludeFN%>" flush="true" ><jsp:param name="plcID" value="<%=thePLCID%>"/></jsp:include>
