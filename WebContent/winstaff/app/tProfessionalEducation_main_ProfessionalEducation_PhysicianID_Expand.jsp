<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.bltDocumentManagement,java.util.Vector,java.util.Enumeration,com.winstaff.ListElement, com.winstaff.PLCUtils, com.winstaff.SecurityCheck, com.winstaff.ConfigurationMessages,com.winstaff.bltProfessionalEducation,com.winstaff.bltProfessionalEducation_List" %>
<%/*
    filename: tProfessionalEducation_main_ProfessionalEducation_PhysicianID.jsp
    Created on Mar/21/2003
    Type: 1-n main class file
    Created by: Scott Ellis
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>

<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_PhysicianID.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>

    <table cellpadding=0 cellspacing=0 border=0 width=<%=MasterTableWidth%> >
    <tr><td width=10>&nbsp;</td><td>
    <%=ConfigurationMessages.getHTML("INTERVIEWTopControl","tProfessionalEducation")%>

    <br><a href="tProfessionalEducation_main_ProfessionalEducation_PhysicianID.jsp">Compact</a>

<%
//initial declaration of list class and parentID
    Integer        iPhysicianID        =    null;
    boolean accessValid = false;
   Integer iSecurityCheck = SecurityCheck.CheckItem("PractitionerSection6", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {
    if (pageControllerHash.containsKey("iPhysicianID")) 
    {
        iPhysicianID        =    (Integer)pageControllerHash.get("iPhysicianID");
        accessValid = true;
    }
  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);
      pageControllerHash.put("sParentReturnPage","tProfessionalEducation_main_ProfessionalEducation_PhysicianID_Expand.jsp");
    pageControllerHash.remove("iProfessionalEducationID");
    session.setAttribute("pageControllerHash",pageControllerHash);

    bltProfessionalEducation_List        bltProfessionalEducation_List        =    new    bltProfessionalEducation_List(iPhysicianID);

//declaration of Enumeration
    bltProfessionalEducation        working_bltProfessionalEducation;
    ListElement         leCurrentElement;
    Enumeration eList = bltProfessionalEducation_List.elements();
    %>
        <%@ include file="tProfessionalEducation_main_ProfessionalEducation_PhysicianID_instructions.jsp" %>

        <%if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {%>
        <a class=linkBase href = "tProfessionalEducation_main_ProfessionalEducation_PhysicianID_form_create.jsp?EDIT=new&KM=p"><img border=0 src="ui_<%=thePLCID%>/icons/create_PhysicianID.gif"></a>
        <%}%>
    <%
    while (eList.hasMoreElements())
    {
         %>
         <table border="0" bordercolor="333333" cellpadding="3" class=tdHeaderAlt cellspacing="0" width="100%">
            <tr>
              <td width="50%">
                   <table border="0" bordercolor="333333" cellpadding="3" class=tdHeaderAlt cellspacing="0" width="100%">
                     <tr> 
                       <td>
         <%

        leCurrentElement    = (ListElement) eList.nextElement();
        working_bltProfessionalEducation  = (bltProfessionalEducation) leCurrentElement.getObject();
        working_bltProfessionalEducation.GroupSecurityInit(UserSecurityGroupID);
        String theClass = "tdHeader";
        if (!working_bltProfessionalEducation.isComplete())
        {
            theClass = "incompleteItem";
        %>
                <span class=incompleteItem><b>Not Complete</b></span><br>
        <%
        }
        %>
               <span class=<%=theClass%> ><b>ID:&nbsp;</b><%=working_bltProfessionalEducation.getProfessionalEducationID()%> </span>
                  </td></tr>
                  <tr><td><b>Item Create Date:&nbsp;</b><jsp:include page="../generic/DateTypeConvert_Main.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(working_bltProfessionalEducation.getUniqueCreateDate())%>" /></jsp:include></td></tr>
                  <tr><td><b>Item Modify Date:&nbsp;</b><jsp:include page="../generic/DateTypeConvert_Main.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(working_bltProfessionalEducation.getUniqueModifyDate())%>" /></jsp:include></td></tr>
                  <tr><td><b>Item Modify Comments:&nbsp;</b><%=working_bltProfessionalEducation.getUniqueModifyComments()%></td></tr>
                  </td></tr></table>
            </td><td width="50%" bgColor=#ffffff> 
        <a class=linkBase href = "tProfessionalEducation_main_ProfessionalEducation_PhysicianID_form_authorize.jsp?EDIT=edit&EDITID=<%=working_bltProfessionalEducation.getProfessionalEducationID()%>&KM=p"><img border=0 src="ui_<%=thePLCID%>/icons/edit_PhysicianID.gif"></a>


        <%if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {%>
        <br><a class=linkBase  onClick="return confirmDelete()"  href = "tProfessionalEducation_main_ProfessionalEducation_PhysicianID_form_authorize.jsp?EDIT=del&EDITID=<%=working_bltProfessionalEducation.getProfessionalEducationID()%>&KM=p"><img border=0 src="ui_<%=thePLCID%>/icons/delete_PhysicianID.gif"></a>

        <%}%>
                  </td></tr>
                 </table>
                 <table width="100%" border="1" cellspacing="0" bordercolor="#333333">
                  <tr>
                   <td>
                     <table width="100%" border="0" cellspacing="0">
                     <tr><td>
<%String theClassF = "textBase";%>

<%theClassF = "textBase";%>
<%if ((working_bltProfessionalEducation.isExpired("DegreeID",expiredDays))&&(working_bltProfessionalEducation.isExpiredCheck("DegreeID"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltProfessionalEducation.isRequired("DegreeID"))&&(!working_bltProfessionalEducation.isComplete("DegreeID")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Degree Type:&nbsp;</b><jsp:include page="../generic/tDegreeTypeLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltProfessionalEducation.getDegreeID()%>" /></jsp:include></p>

<%theClassF = "textBase";%>
<%if ((working_bltProfessionalEducation.isExpired("Other",expiredDays))&&(working_bltProfessionalEducation.isExpiredCheck("Other"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltProfessionalEducation.isRequired("Other"))&&(!working_bltProfessionalEducation.isComplete("Other")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>If Other, list here:&nbsp;</b><%=working_bltProfessionalEducation.getOther()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltProfessionalEducation.isExpired("StartDate",expiredDays))&&(working_bltProfessionalEducation.isExpiredCheck("StartDate"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltProfessionalEducation.isRequired("StartDate"))&&(!working_bltProfessionalEducation.isComplete("StartDate")) ){theClassF = "requiredFieldMain";}%>

                        <p class=<%=theClassF%> ><b>Start Date:&nbsp;</b><jsp:include page="../generic/DateTypeConvert_Main.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(working_bltProfessionalEducation.getStartDate())%>" /></jsp:include></p>


<%theClassF = "textBase";%>
<%if ((working_bltProfessionalEducation.isExpired("DateOfGraduation",expiredDays))&&(working_bltProfessionalEducation.isExpiredCheck("DateOfGraduation"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltProfessionalEducation.isRequired("DateOfGraduation"))&&(!working_bltProfessionalEducation.isComplete("DateOfGraduation")) ){theClassF = "requiredFieldMain";}%>

                        <p class=<%=theClassF%> ><b>Graduation Date:&nbsp;</b><jsp:include page="../generic/DateTypeConvert_Main.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(working_bltProfessionalEducation.getDateOfGraduation())%>" /></jsp:include></p>


<%theClassF = "textBase";%>
<%if ((working_bltProfessionalEducation.isExpired("EndDate",expiredDays))&&(working_bltProfessionalEducation.isExpiredCheck("EndDate"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltProfessionalEducation.isRequired("EndDate"))&&(!working_bltProfessionalEducation.isComplete("EndDate")) ){theClassF = "requiredFieldMain";}%>

                        <p class=<%=theClassF%> ><b>End Date (if not graduated):&nbsp;</b><jsp:include page="../generic/DateTypeConvert_Main.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(working_bltProfessionalEducation.getEndDate())%>" /></jsp:include></p>


<%theClassF = "textBase";%>
<%if ((working_bltProfessionalEducation.isExpired("Focus",expiredDays))&&(working_bltProfessionalEducation.isExpiredCheck("Focus"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltProfessionalEducation.isRequired("Focus"))&&(!working_bltProfessionalEducation.isComplete("Focus")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Major/Focus:&nbsp;</b><%=working_bltProfessionalEducation.getFocus()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltProfessionalEducation.isExpired("SchoolName",expiredDays))&&(working_bltProfessionalEducation.isExpiredCheck("SchoolName"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltProfessionalEducation.isRequired("SchoolName"))&&(!working_bltProfessionalEducation.isComplete("SchoolName")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>School Name:&nbsp;</b><%=working_bltProfessionalEducation.getSchoolName()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltProfessionalEducation.isExpired("SchoolAddress1",expiredDays))&&(working_bltProfessionalEducation.isExpiredCheck("SchoolAddress1"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltProfessionalEducation.isRequired("SchoolAddress1"))&&(!working_bltProfessionalEducation.isComplete("SchoolAddress1")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Address:&nbsp;</b><%=working_bltProfessionalEducation.getSchoolAddress1()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltProfessionalEducation.isExpired("SchoolAddress2",expiredDays))&&(working_bltProfessionalEducation.isExpiredCheck("SchoolAddress2"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltProfessionalEducation.isRequired("SchoolAddress2"))&&(!working_bltProfessionalEducation.isComplete("SchoolAddress2")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Address 2:&nbsp;</b><%=working_bltProfessionalEducation.getSchoolAddress2()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltProfessionalEducation.isExpired("SchoolCity",expiredDays))&&(working_bltProfessionalEducation.isExpiredCheck("SchoolCity"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltProfessionalEducation.isRequired("SchoolCity"))&&(!working_bltProfessionalEducation.isComplete("SchoolCity")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>City:&nbsp;</b><%=working_bltProfessionalEducation.getSchoolCity()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltProfessionalEducation.isExpired("SchoolStateID",expiredDays))&&(working_bltProfessionalEducation.isExpiredCheck("SchoolStateID"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltProfessionalEducation.isRequired("SchoolStateID"))&&(!working_bltProfessionalEducation.isComplete("SchoolStateID")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>State:&nbsp;</b><jsp:include page="../generic/tStateLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltProfessionalEducation.getSchoolStateID()%>" /></jsp:include></p>

<%theClassF = "textBase";%>
<%if ((working_bltProfessionalEducation.isExpired("SchoolProvince",expiredDays))&&(working_bltProfessionalEducation.isExpiredCheck("SchoolProvince"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltProfessionalEducation.isRequired("SchoolProvince"))&&(!working_bltProfessionalEducation.isComplete("SchoolProvince")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Province, District, State:&nbsp;</b><%=working_bltProfessionalEducation.getSchoolProvince()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltProfessionalEducation.isExpired("SchoolZIP",expiredDays))&&(working_bltProfessionalEducation.isExpiredCheck("SchoolZIP"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltProfessionalEducation.isRequired("SchoolZIP"))&&(!working_bltProfessionalEducation.isComplete("SchoolZIP")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>ZIP:&nbsp;</b><%=working_bltProfessionalEducation.getSchoolZIP()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltProfessionalEducation.isExpired("SchoolCountryID",expiredDays))&&(working_bltProfessionalEducation.isExpiredCheck("SchoolCountryID"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltProfessionalEducation.isRequired("SchoolCountryID"))&&(!working_bltProfessionalEducation.isComplete("SchoolCountryID")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Country:&nbsp;</b><jsp:include page="../generic/tCountryLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltProfessionalEducation.getSchoolCountryID()%>" /></jsp:include></p>

<%theClassF = "textBase";%>
<%if ((working_bltProfessionalEducation.isExpired("SchoolPhone",expiredDays))&&(working_bltProfessionalEducation.isExpiredCheck("SchoolPhone"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltProfessionalEducation.isRequired("SchoolPhone"))&&(!working_bltProfessionalEducation.isComplete("SchoolPhone")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>School Phone (XXX-XXX-XXXX):&nbsp;</b><%=working_bltProfessionalEducation.getSchoolPhone()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltProfessionalEducation.isExpired("SchoolFax",expiredDays))&&(working_bltProfessionalEducation.isExpiredCheck("SchoolFax"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltProfessionalEducation.isRequired("SchoolFax"))&&(!working_bltProfessionalEducation.isComplete("SchoolFax")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Fax (XXX-XXX-XXXX):&nbsp;</b><%=working_bltProfessionalEducation.getSchoolFax()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltProfessionalEducation.isExpired("ContactName",expiredDays))&&(working_bltProfessionalEducation.isExpiredCheck("ContactName"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltProfessionalEducation.isRequired("ContactName"))&&(!working_bltProfessionalEducation.isComplete("ContactName")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Contact Name:&nbsp;</b><%=working_bltProfessionalEducation.getContactName()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltProfessionalEducation.isExpired("ContactEmail",expiredDays))&&(working_bltProfessionalEducation.isExpiredCheck("ContactEmail"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltProfessionalEducation.isRequired("ContactEmail"))&&(!working_bltProfessionalEducation.isComplete("ContactEmail")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Contact E-mail:&nbsp;</b><%=working_bltProfessionalEducation.getContactEmail()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltProfessionalEducation.isExpired("Comments",expiredDays))&&(working_bltProfessionalEducation.isExpiredCheck("Comments"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltProfessionalEducation.isRequired("Comments"))&&(!working_bltProfessionalEducation.isComplete("Comments")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Extra Comments:&nbsp;</b><%=working_bltProfessionalEducation.getComments()%></p>

        </td></tr></table></table><br>        <%
    }
    %>
    </table>
    <%
  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>

    </td></tr></table>


<%String bnIncludeFN = "ui_"+thePLCID+"\\bot-nav_PhysicianID.jsp";
%>
<jsp:include page="<%=bnIncludeFN%>" flush="true" ><jsp:param name="plcID" value="<%=thePLCID%>"/></jsp:include>
