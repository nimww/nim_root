<%@page contentType="text/html" language="java" import="com.winstaff.bltStateLI, com.winstaff.*" %>

<%@ include file="../../generic/CheckLogin.jsp" %>

 
<%
    Integer        iPracticeID        =    null;
    Integer        iAdminID        =    null;
    boolean accessValid = false;
	bltPracticeMaster pm = null;
	bltAdminMaster am = null;
    if (pageControllerHash.containsKey("iPracticeID")) 
    {
        iPracticeID        =    (Integer)pageControllerHash.get("iPracticeID");
        accessValid = true;
		pm = new bltPracticeMaster (iPracticeID);
    }
    if (pageControllerHash.containsKey("iAdminID")) 
    {
        iAdminID        =    (Integer)pageControllerHash.get("iAdminID");
        accessValid = true;
		am = new bltAdminMaster (iAdminID);
    }

%>
<link rel="stylesheet" href="ui_<%=thePLCID%>\style_AdminID.css" type="text/css">
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor=#003333>
  <tr>
    <td>
      <table width="100%" border="1" cellspacing="0" cellpadding="0" bordercolor="#999999">
<%
if (am!=null&&am.getUniqueID()!=0)
{
%>
        <tr>
          <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="1" bordercolor="#CCCCCC" bgcolor="#FFFFFF">
              <tr>
                <td class=tdHeaderAlt nowrap>Account:</td>
                <td class=tdHeader nowrap width="99%">&nbsp;<%=am.getName()%></td>
                <td class=tdHeader nowrap>AccountID:&nbsp;<%=am.getAdminID()%></td>
                <td class=tdHeader nowrap>&nbsp;</td>
                <td class=tdHeader nowrap>&nbsp;</td>
                <td class=tdHeader nowrap>&nbsp;</td>
                <td class=tdHeader nowrap><a href="pro-file-manager_Status.jsp">Account Home</a></td>
              </tr>
            </table>
          </td>
        </tr>
<%
}
if (pm!=null)
{
%>
        <tr>
          <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="1" bordercolor="#CCCCCC" bgcolor="#FFFFFF">
              <tr>
                <td class=tdHeaderAlt nowrap>Practice:</td>
                <td class=tdHeader nowrap width="99%">&nbsp;<%=pm.getPracticeName()%> (<%=pm.getOfficeCity()%>, <%=new bltStateLI(pm.getOfficeStateID()).getLongState()%>) [PracticeID:&nbsp;<%=pm.getPracticeID()%>]</td>
                <td class=tdHeader nowrap>
                
<%
if (am!=null&&am.getUniqueID()!=0)
{
%>
                
                <a href="pro-file-manager_Status.jsp">Account Home</a>
                
<%
}else 
{
	%>
    &nbsp;
    <%
}%>
                
                
                </td>
                <td class=tdHeader nowrap>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;</td>
                <td class=tdHeader nowrap><a href="practice_Status.jsp">Practice Home</a></td>
              </tr>
            </table>
          </td>
        </tr>
<%
}%>
      </table>
    </td>
  </tr>
	</table>
<br>
