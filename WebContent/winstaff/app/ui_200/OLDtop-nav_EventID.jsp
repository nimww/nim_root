

<%@page contentType="text/html" language="java" import="com.winstaff.bltEventMaster,com.winstaff.stepVerify_PhysicianID,com.winstaff.bltStateLI, com.winstaff.bltPhysicianMaster" %>

<%@ include file="../../generic/CheckLogin.jsp" %>
 
<%
    Integer        iPhysicianID        =    null;
    Integer        iEventID        =    null;
    boolean accessValid = false;
    if (pageControllerHash.containsKey("iPhysicianID")&&pageControllerHash.containsKey("iEventID")) 
    {
        iPhysicianID        =    (Integer)pageControllerHash.get("iPhysicianID");
        iEventID        =    (Integer)pageControllerHash.get("iEventID");
        accessValid = true;
    }
	bltPhysicianMaster pm = new bltPhysicianMaster(iPhysicianID);
	bltEventMaster em = new bltEventMaster(iEventID);

    String plcID = request.getParameter("plcID");
    if ((plcID==null)||(plcID.equalsIgnoreCase("null")))
    {
        plcID="";
    }
    else
    {
        plcID="_"+plcID;
    }
%>
<link rel="stylesheet" href="ui<%=plcID%>\style_EventID.css" type="text/css">
<script language="JavaScript1.2" src="ui_1/fw_menu.js"></script>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<table width=700 border="0" cellspacing="0" cellpadding="0"  >
  <tr> 
    <td bgcolor="#663300"><img src="ui_1/images/top-nav_EventID.gif" ></td>
  </tr>
  <tr>
    <td>

      <table width="100%" border="1" cellspacing="0" cellpadding="3">
        <tr class=tdBaseAlt> 
          <td><%=pm.getLastName()%>, <%=pm.getFirstName()%> [<%=iPhysicianID%>]</td>
          <td><%=em.getName()%></td>
          <td>&nbsp;</td>
        </tr>
      </table>
    </td>
  </tr>
</table>
