

<%@page contentType="text/html" language="java" import="com.winstaff.bltEventMaster,com.winstaff.stepVerify_PhysicianID,com.winstaff.bltStateLI, com.winstaff.bltPhysicianMaster" %>

<%@ include file="../../generic/CheckLogin.jsp" %>
 
<%
    Integer        iPhysicianID        =    null;
    Integer        iEventID        =    null;
    boolean accessValid = false;
    if (pageControllerHash.containsKey("iPhysicianID")&&pageControllerHash.containsKey("iEventID")) 
    {
        iPhysicianID        =    (Integer)pageControllerHash.get("iPhysicianID");
        iEventID        =    (Integer)pageControllerHash.get("iEventID");
        accessValid = true;
    }
	bltPhysicianMaster pm = new bltPhysicianMaster(iPhysicianID);
	bltEventMaster em = new bltEventMaster(iEventID);

    String plcID = request.getParameter("plcID");
    if ((plcID==null)||(plcID.equalsIgnoreCase("null")))
    {
        plcID="";
    }
    else
    {
        plcID="_"+plcID;
    }
String tnIncludeFN = "/winstaff/app/ui_"+thePLCID+"/top-nav_HCOID.jsp?plcID="+plcID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<link rel="stylesheet" href="ui<%=plcID%>\style_HCOID.css" type="text/css">
<table border="0" cellpadding="0" cellspacing="0" width="100%" >
  <tr> 
    <td> 
      <table width="700" border="1" cellspacing="0" cellpadding="3">
        <tr class=tdBaseAlt> 
          <td colspan=2> <a href="HCO-query_view.jsp">Practitioner View</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;<a href="EventMaster_HCOID.jsp">Credentialing Home Page</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp; <a href="tEventMaster_Status_HCOID.jsp">Credentialing 
            Track Status</a>
          </td>
          <td>&nbsp;</td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<br>