 
<%
    String plcID = request.getParameter("plcID");
    if ((plcID==null)||(plcID.equalsIgnoreCase("null")))
    {
        plcID="";
    }
    else
    {
        plcID="_"+plcID;
    }
%>
<link rel="stylesheet" href="ui<%=plcID%>\style_PhysicianID.css" type="text/css">
<script language="JavaScript">
<!--
function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_preloadImages() { //v3.0
 var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
   var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
   if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function fwLoadMenus() {
  if (window.fw_menu_0) return;
  window.fw_menu_0 = new Menu("root",100,17,"Arial, Helvetica, sans-serif",10,"#000000","#993333","#ffffff","#cccccc");
  fw_menu_0.addMenuItem("Pro-File Status","location='pro-file_Status.jsp'");
  fw_menu_0.addMenuItem("Populate Forms","location='populateForms.jsp'");
  fw_menu_0.addMenuItem("Attestation","location='populateForms.jsp'");
  fw_menu_0.addMenuItem("Return","location='tAdminPhysicianLU_main_PhysicianMaster_AdminID.jsp'");
  fw_menu_0.addMenuItem("Exit","location='../admin1.jsp'");
   fw_menu_0.fontWeight="bold";
   fw_menu_0.hideOnMouseOut=true;
  window.fw_menu_1 = new Menu("root",159,17,"Arial, Helvetica, sans-serif",10,"#000000","#993333","#ffffff","#cccccc");
  fw_menu_1.addMenuItem("01.General Information","location='tPhysicianMaster_form.jsp?EDIT=edit'");

  fw_menu_1.addMenuItem("02.Practice Information","location='tPhysicianPracticeLU_main_PracticeMaster_PhysicianID.jsp'");
//  fw_menu_1.addMenuItem("02.Practice Information","location='tPhysicianPracticeLU_main_LU_PhysicianID.jsp'");
  fw_menu_1.addMenuItem("03.Licensure/Registration","location='tLicenseRegistration_main_LicenseRegistration_PhysicianID.jsp'");
  fw_menu_1.addMenuItem("04.Professional Liability","location='tProfessionalLiability_main_ProfessionalLiability_PhysicianID.jsp'");
  fw_menu_1.addMenuItem("05.Professional Education","location='tProfessionalEducation_main_ProfessionalEducation_PhysicianID.jsp'");
  fw_menu_1.addMenuItem("06.Professional Experience","location='tExperience_main_Experience_PhysicianID.jsp'");
  fw_menu_1.addMenuItem("07.Specialty/Certifications","location='tBoardCertification_main_BoardCertification_PhysicianID.jsp'");
  fw_menu_1.addMenuItem("08.Other Certification","location='tOtherCertification_main_OtherCertification_PhysicianID.jsp'");
  fw_menu_1.addMenuItem("09.Facility Affiliations","location='tFacilityAffiliation_main_FacilityAffiliation_PhysicianID.jsp'");
  fw_menu_1.addMenuItem("10.Peer References","location='tPeerReference_main_PeerReference_PhysicianID.jsp'");
  fw_menu_1.addMenuItem("11.Work History","location='tWorkHistory_main_WorkHistory_PhysicianID.jsp'");
  fw_menu_1.addMenuItem("12.Professional Affiliation","location='tProfessionalSociety_main_ProfessionalSociety_PhysicianID.jsp'");
  fw_menu_1.addMenuItem("13.Disclosure Questions","location='tAttestR_main_AttestR_PhysicianID.jsp'");
  fw_menu_1.addMenuItem("14.Additional Notes","location='tAdditionalInformation_main_AdditionalInformation_PhysicianID.jsp'");
  fw_menu_1.addMenuItem("15.Supplemental Data","location='SupplementalData_main_PhysicianID.jsp'");
   fw_menu_1.fontWeight="bold";
   fw_menu_1.hideOnMouseOut=true;
  window.fw_menu_2 = new Menu("root",108,17,"Arial, Helvetica, sans-serif",10,"#000000","#993333","#ffffff","#cccccc");
  fw_menu_2.addMenuItem("View/Edit Events","location='tPhysicianEventLU_main_EventMaster_PhysicianID.jsp'");
  fw_menu_2.addMenuItem("Event Reports","location='#'");
   fw_menu_2.fontWeight="bold";
   fw_menu_2.hideOnMouseOut=true;
  window.fw_menu_3 = new Menu("root",161,17,"Arial, Helvetica, sans-serif",10,"#000000","#993333","#ffffff","#cccccc");
  fw_menu_3.addMenuItem("Frequently Asked Questions");
  fw_menu_3.addMenuItem("Contact Us");
  fw_menu_3.addMenuItem("About");
   fw_menu_3.fontWeight="bold";
   fw_menu_3.hideOnMouseOut=true;

  fw_menu_3.writeMenus();
} 

function MM_findObj(n, d) { //v4.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById) x=document.getElementById(n); return x;
}
//-->
</script>
<script language="JavaScript1.2" src="ui_1/fw_menu.js"></script>
<body onLoad="MM_preloadImages('ui_1/ui_1/ui_1/ui_1/images/top-nav_r2_c2_f2.gif','ui_1/ui_1/ui_1/ui_1/images/top-nav_r2_c4_f2.gif','ui_1/ui_1/ui_1/ui_1/images/top-nav_r2_c6_f2.gif','ui_1/ui_1/ui_1/ui_1/images/top-nav_r2_c8_f2.gif')" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor=#003366>
  <tr> 
    <td> 
      <script language="JavaScript1.2">fwLoadMenus();</script>
      <table border="0" cellpadding="0" cellspacing="0" width="700">
        <!-- fwtable fwsrc="top-nav2_PhysicianID.png" fwbase="top-nav_PhysicianID.jpg" fwstyle="Dreamweaver" fwdocid = "742308039" fwnested="0" -->
        <tr> 
          <td><img src="ui_1/images/spacer.gif" width="133" height="1" border="0"></td>
          <td><img src="ui_1/images/spacer.gif" width="92" height="1" border="0"></td>
          <td><img src="ui_1/images/spacer.gif" width="5" height="1" border="0"></td>
          <td><img src="ui_1/images/spacer.gif" width="102" height="1" border="0"></td>
          <td><img src="ui_1/images/spacer.gif" width="5" height="1" border="0"></td>
          <td><img src="ui_1/images/spacer.gif" width="102" height="1" border="0"></td>
          <td><img src="ui_1/images/spacer.gif" width="5" height="1" border="0"></td>
          <td><img src="ui_1/images/spacer.gif" width="59" height="1" border="0"></td>
          <td><img src="ui_1/images/spacer.gif" width="5" height="1" border="0"></td>
          <td><img src="ui_1/images/spacer.gif" width="77" height="1" border="0"></td>
          <td><img src="ui_1/images/spacer.gif" width="115" height="1" border="0"></td>
          <td><img src="ui_1/images/spacer.gif" width="1" height="1" border="0"></td>
        </tr>
        <tr> 
          <td colspan="11"><img name="topnav_PhysicianID_r1_c1" src="ui_1/images/top-nav_PhysicianID_r1_c1.jpg" width="700" height="60" border="0"></td>
          <td><img src="ui_1/images/spacer.gif" width="1" height="60" border="0"></td>
        </tr>
        <tr> 
          <td rowspan="2"><img name="topnav_PhysicianID_r2_c1" src="ui_1/images/top-nav_PhysicianID_r2_c1.jpg" width="133" height="35" border="0"></td>
          <td><a href="#" onMouseOut="FW_startTimeout();"  onMouseOver="window.FW_showMenu(window.fw_menu_0,134,85);" ><img name="topnav_PhysicianID_r2_c2" src="ui_1/images/top-nav_PhysicianID_r2_c2.jpg" width="92" height="25" border="0"></a></td>
          <td rowspan="2"><img name="topnav_PhysicianID_r2_c3" src="ui_1/images/top-nav_PhysicianID_r2_c3.jpg" width="5" height="35" border="0"></td>
          <td><a href="#" onMouseOut="FW_startTimeout();"  onMouseOver="window.FW_showMenu(window.fw_menu_1,232,85);" ><img name="topnav_PhysicianID_r2_c4" src="ui_1/images/top-nav_PhysicianID_r2_c4.jpg" width="102" height="25" border="0"></a></td>
          <td rowspan="2"><img name="topnav_PhysicianID_r2_c5" src="ui_1/images/top-nav_PhysicianID_r2_c5.jpg" width="5" height="35" border="0"></td>
          <td><a href="#" onMouseOut="FW_startTimeout();"  onMouseOver="window.FW_showMenu(window.fw_menu_2,338,85);" ><img name="topnav_PhysicianID_r2_c6" src="ui_1/images/top-nav_PhysicianID_r2_c6.jpg" width="102" height="25" border="0"></a></td>
          <td rowspan="2"><img name="topnav_PhysicianID_r2_c7" src="ui_1/images/top-nav_PhysicianID_r2_c7.jpg" width="5" height="35" border="0"></td>
          <td><a href="#" onMouseOut="FW_startTimeout();"  onMouseOver="window.FW_showMenu(window.fw_menu_3,445,85);" ><img name="topnav_PhysicianID_r2_c8" src="ui_1/images/top-nav_PhysicianID_r2_c8.jpg" width="59" height="25" border="0"></a></td>
          <td rowspan="2"><img name="topnav_PhysicianID_r2_c9" src="ui_1/images/top-nav_PhysicianID_r2_c9.jpg" width="5" height="35" border="0"></td>
          <td><img name="topnav_PhysicianID_r2_c10" src="ui_1/images/top-nav_PhysicianID_r2_c10.jpg" width="77" height="25" border="0"></td>
          <td rowspan="2"><img name="topnav_PhysicianID_r2_c11" src="ui_1/images/top-nav_PhysicianID_r2_c11.jpg" width="115" height="35" border="0"></td>
          <td><img src="ui_1/images/spacer.gif" width="1" height="25" border="0"></td>
        </tr>
        <tr> 
          <td><img name="topnav_PhysicianID_r3_c2" src="ui_1/images/top-nav_PhysicianID_r3_c2.jpg" width="92" height="10" border="0"></td>
          <td><img name="topnav_PhysicianID_r3_c4" src="ui_1/images/top-nav_PhysicianID_r3_c4.jpg" width="102" height="10" border="0"></td>
          <td><img name="topnav_PhysicianID_r3_c6" src="ui_1/images/top-nav_PhysicianID_r3_c6.jpg" width="102" height="10" border="0"></td>
          <td><img name="topnav_PhysicianID_r3_c8" src="ui_1/images/top-nav_PhysicianID_r3_c8.jpg" width="59" height="10" border="0"></td>
          <td><img name="topnav_PhysicianID_r3_c10" src="ui_1/images/top-nav_PhysicianID_r3_c10.jpg" width="77" height="10" border="0"></td>
          <td><img src="ui_1/images/spacer.gif" width="1" height="10" border="0"></td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<!--
<table width="100%" border="0" cellspacing="0" cellpadding="0" class=tableColor>
  <tr class=tdBase> 
    <td>
      PhysicianID top-nav [top-nav_PhysicianID.jsp] Include
      
      <form name=form1>
      <select name="linker" onChange="javascript:document.location=document.form1.linker.value;">
      <option value="null">Select Link</option>
      <option value="tAdminPhysicianLU_main_LU_PhysicianID.jsp">Open AdminPhysicianLU via PhysicianID</option>
      <option value="tAdminPhysicianLU_main_AdminMaster_PhysicianID.jsp">Open AdminMaster via PhysicianID</option>
      <option value="tAttestR_main_AttestR_PhysicianID.jsp">Open AttestR via PhysicianID</option>
      <option value="tBoardCertification_main_BoardCertification_PhysicianID.jsp">Open BoardCertification via PhysicianID</option>
      <option value="tDocumentManagement_main_DocumentManagement_PhysicianID.jsp">Open DocumentManagement via PhysicianID</option>
      <option value="tExperience_main_Experience_PhysicianID.jsp">Open Experience via PhysicianID</option>
      <option value="tFacilityAffiliation_main_FacilityAffiliation_PhysicianID.jsp">Open FacilityAffiliation via PhysicianID</option>
      <option value="tOtherCertification_main_OtherCertification_PhysicianID.jsp">Open OtherCertification via PhysicianID</option>
      <option value="tPeerReference_main_PeerReference_PhysicianID.jsp">Open PeerReference via PhysicianID</option>
      <option value="tProfessionalEducation_main_ProfessionalEducation_PhysicianID.jsp">Open ProfessionalEducation via PhysicianID</option>
      </select>
      </form>
      
    </td>
  </tr>
</table>
-->
