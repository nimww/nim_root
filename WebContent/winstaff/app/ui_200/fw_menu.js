//custom

function confirmDelete()
{
	return confirm("Are you sure you want to delete this item?");
}

/**
 * fw_menu 24OCT2000 Version 4.0
 * John Ahlquist, October 2000
 * Copyright (c) 2000 Macromedia, Inc.
 *
 * based on menu.js
 * by gary smith, July 1997
 * Copyright (c) 1997-1999 Netscape Communications Corp.
 *
 * Netscape grants you a royalty free license to use or modify this
 * software provided that this copyright notice appears on all copies.
 * This software is provided "AS IS," without a warranty of any kind.
 */
function Menu(label, mw, mh, fnt, fs, fclr, fhclr, bg, bgh) {
	this.version = "990702 [Menu; menu.js]";
	this.type = "Menu";
	this.menuWidth = mw;
	this.menuItemHeight = mh;
	this.fontSize = fs||12;
	this.fontWeight = "plain";
	this.fontFamily = fnt||"arial,helvetica,verdana,sans-serif";
	this.fontColor = fclr||"#000000";
	this.fontColorHilite = fhclr||"#ffffff";
	this.bgColor = "#555555";
	this.menuBorder = 1;
	this.menuItemBorder = 1;
	this.menuItemBgColor = bg||"#cccccc";
	this.menuLiteBgColor = "#ffffff";
	this.menuBorderBgColor = "#777777";
	this.menuHiliteBgColor = bgh||"#000084";
	this.menuContainerBgColor = "#cccccc";
	this.childMenuIcon = "arrows.gif";
	this.items = new Array();
	this.actions = new Array();
	this.childMenus = new Array();

	this.hideOnMouseOut = true;

	this.addMenuItem = addMenuItem;
	this.addMenuSeparator = addMenuSeparator;
	this.writeMenus = writeMenus;
	this.FW_showMenu = FW_showMenu;
	this.onMenuItemOver = onMenuItemOver;
	this.onMenuItemAction = onMenuItemAction;
	this.hideMenu = hideMenu;
	this.hideChildMenu = hideChildMenu;

	if (!window.menus) window.menus = new Array();
	this.label = label || "menuLabel" + window.menus.length;
	window.menus[this.label] = this;
	window.menus[window.menus.length] = this;
	if (!window.activeMenus) window.activeMenus = new Array();
}

function addMenuItem(label, action) {
	this.items[this.items.length] = label;
	this.actions[this.actions.length] = action;
}

function addMenuSeparator() {
	this.items[this.items.length] = "separator";
	this.actions[this.actions.length] = "";
	this.menuItemBorder = 0;
}

// For NS6. 
function FIND(item) {
	if (document.all) return(document.all[item]);
	if (document.getElementById) return(document.getElementById(item));
	return(false);
}

function writeMenus(container) {
	if (window.triedToWriteMenus) return;

	if (!container && document.layers) {
		window.delayWriteMenus = this.writeMenus;
		var timer = setTimeout('delayWriteMenus()', 250);
		container = new Layer(100);
		clearTimeout(timer);
	} else if (document.all || document.hasChildNodes) {
		document.writeln('<SPAN ID="menuContainer"></SPAN>');
		container = FIND("menuContainer");
	}

	window.fwHideMenuTimer = null;
	if (!container) return;	
	window.triedToWriteMenus = true; 
	container.isContainer = true;
	container.menus = new Array();
	for (var i=0; i<window.menus.length; i++) 
		container.menus[i] = window.menus[i];
	window.menus.length = 0;
	var countMenus = 0;
	var countItems = 0;
	var top = 0;
	var content = '';
	var lrs = false;
	var theStat = "";
	var tsc = 0;
	if (document.layers) lrs = true;
	for (var i=0; i<container.menus.length; i++, countMenus++) {
		var menu = container.menus[i];
		if (menu.bgImageUp) {
			menu.menuBorder = 0;
			menu.menuItemBorder = 0;
		}
		if (lrs) {
			var menuLayer = new Layer(100, container);
			var lite = new Layer(100, menuLayer);
			lite.top = menu.menuBorder;
			lite.left = menu.menuBorder;
			var body = new Layer(100, lite);
			body.top = menu.menuBorder;
			body.left = menu.menuBorder;
		} else {
			content += ''+
			'<DIV ID="menuLayer'+ countMenus +'" STYLE="position:absolute;z-index:1;left:10;top:'+ (i * 100) +';visibility:hidden;">\n'+
			'  <DIV ID="menuLite'+ countMenus +'" STYLE="position:absolute;z-index:1;left:'+ menu.menuBorder +';top:'+ menu.menuBorder +';visibility:hide;" onMouseOut="mouseoutMenu();">\n'+
			'	 <DIV ID="menuFg'+ countMenus +'" STYLE="position:absolute;left:'+ menu.menuBorder +';top:'+ menu.menuBorder +';visibility:hide;">\n'+
			'';
		}
		var x=i;
		for (var i=0; i<menu.items.length; i++) {
			var item = menu.items[i];
			var childMenu = false;
			var defaultHeight = menu.fontSize+6;
			var defaultIndent = menu.fontSize;
			if (item.label) {
				item = item.label;
				childMenu = true;
			}
			menu.menuItemHeight = menu.menuItemHeight || defaultHeight;
			menu.menuItemIndent = menu.menuItemIndent || defaultIndent;
			var itemProps = 'font-family:' + menu.fontFamily +';font-weight:' + menu.fontWeight + ';fontSize:' + menu.fontSize + ';';
			if (menu.fontStyle) itemProps += 'font-style:' + menu.fontStyle + ';';
			if (document.all) 
				itemProps += 'font-size:' + menu.fontSize + ';" onMouseOver="onMenuItemOver(null,this);" onClick="onMenuItemAction(null,this);';
			else if (!document.layers) {
				itemProps += 'font-size:' + menu.fontSize + 'px;'; // zilla wants 12px.
			}
			var l;
			if (lrs) {
				l = new Layer(800,body);
			}
			var dTag	= '<DIV ID="menuItem'+ countItems +'" STYLE="position:absolute;left:0;top:'+ (i * menu.menuItemHeight) +';'+ itemProps +'">';
			var dClose = '</DIV>'
			if (menu.bgImageUp) {
				menu.menuBorder = 0;
				menu.menuItemBorder = 0;
				dTag	= '<DIV ID="menuItem'+ countItems +'" STYLE="background:url('+menu.bgImageUp+');position:absolute;left:0;top:'+ (i * menu.menuItemHeight) +';'+ itemProps +'">';
				if (document.layers) {
					dTag = '<LAYER BACKGROUND="'+menu.bgImageUp+'" ID="menuItem'+ countItems +'" TOP="'+ (i * menu.menuItemHeight) +'" style="' + itemProps +'">';
					dClose = '</LAYER>';
				}
			}
			var textProps = 'position:absolute;left:' + menu.menuItemIndent + ';top:1;';
			if (lrs) {
				textProps +=itemProps;
				dTag = "";
				dClose = "";
			}

			var dText	= '<DIV ID="menuItemText'+ countItems +'" STYLE="' + textProps + 'color:'+ menu.fontColor +';">'+ item +'&nbsp</DIV>\n<DIV ID="menuItemHilite'+ countItems +'" STYLE="' + textProps + 'top:1;color:'+ menu.fontColorHilite +';visibility:hidden;">'+ item +'&nbsp</DIV>';
			if (item == "separator") {
				content += ( dTag + '<DIV ID="menuSeparator'+ countItems +'" STYLE="position:absolute;left:1;top:2;"></DIV>\n<DIV ID="menuSeparatorLite'+ countItems +'" STYLE="position:absolute;left:1;top:2;"></DIV>\n' + dClose);
			} else if (childMenu) {
				content += ( dTag + dText + '<DIV ID="childMenu'+ countItems +'" STYLE="position:absolute;left:0;top:3;"><IMG SRC="'+ menu.childMenuIcon +'"></DIV>\n' + dClose);
			} else {
				content += ( dTag + dText + dClose);
			}
			if (lrs) {
				l.document.open("text/html");
				l.document.writeln(content);
				l.document.close();	
				content = '';
				theStat += "-";
				tsc++;
				if (tsc > 50) {
					tsc = 0;
					theStat = "";
				}
				status = theStat;
			}
			countItems++;  
		}
		if (lrs) {
			// focus layer
			var focusItem = new Layer(100, body);
			focusItem.visiblity="hidden";
			focusItem.document.open("text/html");
			focusItem.document.writeln("&nbsp;");
			focusItem.document.close();	
		} else {
		  content += '	  <DIV ID="focusItem'+ countMenus +'" STYLE="position:absolute;left:0;top:0;visibility:hide;" onClick="onMenuItemAction(null,this);">&nbsp;</DIV>\n';
		  content += '   </DIV>\n  </DIV>\n</DIV>\n';
		}
		i=x;
	}
	if (document.layers) {		
		container.clip.width = window.innerWidth;
		container.clip.height = window.innerHeight;
		container.onmouseout = mouseoutMenu;
		container.menuContainerBgColor = this.menuContainerBgColor;
		for (var i=0; i<container.document.layers.length; i++) {
			proto = container.menus[i];
			var menu = container.document.layers[i];
			container.menus[i].menuLayer = menu;
			container.menus[i].menuLayer.Menu = container.menus[i];
			container.menus[i].menuLayer.Menu.container = container;
			var body = menu.document.layers[0].document.layers[0];
			body.clip.width = proto.menuWidth || body.clip.width;
			body.clip.height = proto.menuHeight || body.clip.height;
			for (var n=0; n<body.document.layers.length-1; n++) {
				var l = body.document.layers[n];
				l.Menu = container.menus[i];
				l.menuHiliteBgColor = proto.menuHiliteBgColor;
				l.document.bgColor = proto.menuItemBgColor;
				l.saveColor = proto.menuItemBgColor;
				l.onmouseover = proto.onMenuItemOver;
				l.onclick = proto.onMenuItemAction;
				l.action = container.menus[i].actions[n];
				l.focusItem = body.document.layers[body.document.layers.length-1];
				l.clip.width = proto.menuWidth || body.clip.width + proto.menuItemIndent;
				l.clip.height = proto.menuItemHeight || l.clip.height;
				if (n>0) l.top = body.document.layers[n-1].top + body.document.layers[n-1].clip.height + proto.menuItemBorder;
				l.hilite = l.document.layers[1];
				if (proto.bgImageUp) l.background.src = proto.bgImageUp;
				l.document.layers[1].isHilite = true;
				if (l.document.layers[0].id.indexOf("menuSeparator") != -1) {
					l.hilite = null;
					l.clip.height -= l.clip.height / 2;
					l.document.layers[0].document.bgColor = proto.bgColor;
					l.document.layers[0].clip.width = l.clip.width -2;
					l.document.layers[0].clip.height = 1;
					l.document.layers[1].document.bgColor = proto.menuLiteBgColor;
					l.document.layers[1].clip.width = l.clip.width -2;
					l.document.layers[1].clip.height = 1;
					l.document.layers[1].top = l.document.layers[0].top + 1;
				} else if (l.document.layers.length > 2) {
					l.childMenu = container.menus[i].items[n].menuLayer;
					l.document.layers[2].left = l.clip.width -13;
					l.document.layers[2].top = (l.clip.height / 2) -4;
					l.document.layers[2].clip.left += 3;
					l.Menu.childMenus[l.Menu.childMenus.length] = l.childMenu;
				}
			}
			body.document.bgColor = proto.bgColor;
			body.clip.width  = l.clip.width +proto.menuBorder;
			body.clip.height = l.top + l.clip.height +proto.menuBorder;
			var focusItem = body.document.layers[n];
			focusItem.clip.width = body.clip.width;
			focusItem.Menu = l.Menu;
			focusItem.top = -30;
            focusItem.captureEvents(Event.MOUSEDOWN);
            focusItem.onmousedown = onMenuItemDown;
			menu.document.bgColor = proto.menuBorderBgColor;
			var lite = menu.document.layers[0];
			lite.document.bgColor = proto.menuLiteBgColor;
			lite.clip.width = body.clip.width +1;
			lite.clip.height = body.clip.height +1;
			menu.clip.width = body.clip.width + (proto.menuBorder * 3) ;
			menu.clip.height = body.clip.height + (proto.menuBorder * 3);
		}
	} else {
		if ((!document.all) && (container.hasChildNodes)) {
			container.innerHTML=content;
		} else {
			container.document.open("text/html");
			container.document.writeln(content);
			container.document.close();	
		}
		if (!FIND("menuLayer0")) return;
		var menuCount = 0;
		for (var x=0; x<container.menus.length; x++) {
			var menuLayer = FIND("menuLayer" + x);
			container.menus[x].menuLayer = "menuLayer" + x;
			menuLayer.Menu = container.menus[x];
			menuLayer.Menu.container = "menuLayer" + x;
			menuLayer.style.zIndex = 1;
		    var s = menuLayer.style;
			s.top = s.pixelTop = -300;
			s.left = s.pixelLeft = -300;

			var menu = container.menus[x];
			menu.menuItemWidth = menu.menuWidth || menu.menuIEWidth || 140;
			menuLayer.style.backgroundColor = menu.menuBorderBgColor;
			var top = 0;
			for (var i=0; i<container.menus[x].items.length; i++) {
				var l = FIND("menuItem" + menuCount);
				l.Menu = container.menus[x];
				if (l.addEventListener) { // ns6
					l.style.width = menu.menuItemWidth;	
					l.style.height = menu.menuItemHeight;
					l.style.top = top;
					l.addEventListener("mouseover", onMenuItemOver, false);
					l.addEventListener("click", onMenuItemAction, false);
					l.addEventListener("mouseout", mouseoutMenu, false);
				} else { //ie
					l.style.pixelWidth = menu.menuItemWidth;	
					l.style.pixelHeight = menu.menuItemHeight;
					l.style.pixelTop = top;
				}
				top = top + menu.menuItemHeight+menu.menuItemBorder;
				l.style.fontSize = menu.fontSize;
				l.style.backgroundColor = menu.menuItemBgColor;
				l.style.visibility = "inherit";
				l.saveColor = menu.menuItemBgColor;
				l.menuHiliteBgColor = menu.menuHiliteBgColor;
				l.action = container.menus[x].actions[i];
				l.hilite = FIND("menuItemHilite" + menuCount);
				l.focusItem = FIND("focusItem" + x);
				l.focusItem.style.pixelTop = l.focusItem.style.top = -30;
				var childItem = FIND("childMenu" + menuCount);
				if (childItem) {
					l.childMenu = container.menus[x].items[i].menuLayer;
					childItem.style.pixelLeft = childItem.style.left = menu.menuItemWidth -11;
					childItem.style.pixelTop = childItem.style.top =(menu.menuItemHeight /2) -4;
					//childItem.style.pixelWidth = 30 || 7;
					//childItem.style.clip = "rect(0 7 7 3)";
					l.Menu.childMenus[l.Menu.childMenus.length] = l.childMenu;
				}
				var sep = FIND("menuSeparator" + menuCount);
				if (sep) {
					sep.style.clip = "rect(0 " + (menu.menuItemWidth - 3) + " 1 0)";
					sep.style.width = sep.style.pixelWidth = menu.menuItemWidth;	
					sep.style.backgroundColor = menu.bgColor;
					sep = FIND("menuSeparatorLite" + menuCount);
					sep.style.clip = "rect(1 " + (menu.menuItemWidth - 3) + " 2 0)";
					sep.style.width = sep.style.pixelWidth = menu.menuItemWidth;	
					sep.style.backgroundColor = menu.menuLiteBgColor;
					l.style.height = l.style.pixelHeight = menu.menuItemHeight/2;
					l.isSeparator = true
					top -= (menu.menuItemHeight - l.style.pixelHeight)
				} else {
					l.style.cursor = "hand"
				}
				menuCount++;
			}
			menu.menuHeight = top-1;
			var lite = FIND("menuLite" + x);
			var s = lite.style;
			s.height = s.pixelHeight = menu.menuHeight +(menu.menuBorder * 2);
			s.width = s.pixelWidth = menu.menuItemWidth + (menu.menuBorder * 2);
			s.backgroundColor = menu.menuLiteBgColor;

			var body = FIND("menuFg" + x);
			s = body.style;
			s.height = s.pixelHeight = menu.menuHeight + menu.menuBorder;
			s.width = s.pixelWidth = menu.menuItemWidth + menu.menuBorder;
			s.backgroundColor = menu.bgColor;

			s = menuLayer.style;
			s.width = s.pixelWidth  = menu.menuItemWidth + (menu.menuBorder * 4);
			s.height = s.pixelHeight  = menu.menuHeight+(menu.menuBorder*4);
		}
	}
	if (document.captureEvents) {	
		document.captureEvents(Event.MOUSEUP);
	}
	if (document.addEventListener) {	
		document.addEventListener("mouseup", onMenuItemOver, false);
	}
	if (document.layers && window.innerWidth) {
		window.onresize = NS4resize;
		window.NS4sIW = window.innerWidth;
		window.NS4sIH = window.innerHeight;
	}
	document.onmouseup = mouseupMenu;
	window.fwWroteMenu = true;
	status = "";
}

function NS4resize() {
	if (NS4sIW < window.innerWidth || 
		NS4sIW > window.innerWidth || 
		NS4sIH > window.innerHeight || 
		NS4sIH < window.innerHeight ) 
	{
		window.location.reload();
	}
}

function onMenuItemOver(e, l) {
	FW_clearTimeout();
	l = l || this;
	a = window.ActiveMenuItem;
	if (document.layers) {
		if (a) {
			a.document.bgColor = a.saveColor;
			if (a.hilite) a.hilite.visibility = "hidden";
			if (a.Menu.bgImageOver) {
				a.background.src = a.Menu.bgImageUp;
			}
			a.focusItem.top = -100;
			a.clicked = false;
		}
		if (l.hilite) {
			l.document.bgColor = l.menuHiliteBgColor;
			l.zIndex = 1;
			l.hilite.visibility = "inherit";
			l.hilite.zIndex = 2;
			l.document.layers[1].zIndex = 1;
			l.focusItem.zIndex = this.zIndex +2;
		}
		if (l.Menu.bgImageOver) {
			l.background.src = l.Menu.bgImageOver;
		}
		l.focusItem.top = this.top;
		l.Menu.hideChildMenu(l);
	} else if (l.style && l.Menu) {
		if (a) {
			a.style.backgroundColor = a.saveColor;
			if (a.hilite) a.hilite.style.visibility = "hidden";
			if (a.Menu.bgImageUp) {
				a.style.background = "url(" + a.Menu.bgImageUp +")";;
			}
		} 
		if (l.isSeparator) return;
		l.style.backgroundColor = l.menuHiliteBgColor;
		l.zIndex = 1;  // magic IE 4.5 mac happy doohicky.	jba
		if (l.Menu.bgImageOver) {
			l.style.background = "url(" + l.Menu.bgImageOver +")";
		}
		if (l.hilite) {
			l.style.backgroundColor = l.menuHiliteBgColor;
			l.hilite.style.visibility = "inherit";
		}
		l.focusItem.style.top = l.focusItem.style.pixelTop = l.style.pixelTop;
		l.focusItem.style.zIndex = l.zIndex +1;
		l.Menu.hideChildMenu(l);
	} else {
		return; // not a menu - magic IE 4.5 mac happy doohicky.  jba
	}
	window.ActiveMenuItem = l;
}

function onMenuItemAction(e, l) {
	l = window.ActiveMenuItem;
	if (!l) return;
	hideActiveMenus();
	if (l.action) {
		eval("" + l.action);
	}
	window.ActiveMenuItem = 0;
}

function FW_clearTimeout()
{
	if (fwHideMenuTimer) clearTimeout(fwHideMenuTimer);
	fwHideMenuTimer = null;
	fwDHFlag = false;
}
function FW_startTimeout()
{
	fwStart = new Date();
	fwDHFlag = true;
	fwHideMenuTimer = setTimeout("fwDoHide()", 1000);
}

function fwDoHide()
{
	if (!fwDHFlag) return;
	var elapsed = new Date() - fwStart;
	if (elapsed < 1000) {
		fwHideMenuTimer = setTimeout("fwDoHide()", 1100-elapsed);
		return;
	}
	fwDHFlag = false;
	hideActiveMenus();
	window.ActiveMenuItem = 0;
}

function FW_showMenu(menu, x, y, child) {
	if (!window.fwWroteMenu) return;
	FW_clearTimeout();
	if (document.layers) {
		if (menu) {
			var l = menu.menuLayer || menu;
			l.left = 1;
			l.top = 1;
			hideActiveMenus();
			if (this.visibility) l = this;
			window.ActiveMenu = l;
		} else {
			var l = child;
		}
		if (!l) return;
		for (var i=0; i<l.layers.length; i++) { 			   
			if (!l.layers[i].isHilite) 
				l.layers[i].visibility = "inherit";
			if (l.layers[i].document.layers.length > 0) 
				FW_showMenu(null, "relative", "relative", l.layers[i]);
		}
		if (l.parentLayer) {
			if (x != "relative") 
				l.parentLayer.left = x || window.pageX || 0;
			if (l.parentLayer.left + l.clip.width > window.innerWidth) 
				l.parentLayer.left -= (l.parentLayer.left + l.clip.width - window.innerWidth);
			if (y != "relative") 
				l.parentLayer.top = y || window.pageY || 0;
			if (l.parentLayer.isContainer) {
				l.Menu.xOffset = window.pageXOffset;
				l.Menu.yOffset = window.pageYOffset;
				l.parentLayer.clip.width = window.ActiveMenu.clip.width +2;
				l.parentLayer.clip.height = window.ActiveMenu.clip.height +2;
				if (l.parentLayer.menuContainerBgColor) l.parentLayer.document.bgColor = l.parentLayer.menuContainerBgColor;
			}
		}
		l.visibility = "inherit";
		if (l.Menu) l.Menu.container.visibility = "inherit";
	} else if (FIND("menuItem0")) {
		var l = menu.menuLayer || menu;	
		hideActiveMenus();
		if (typeof(l) == "string") {
			l = FIND(l);
		}
		window.ActiveMenu = l;
		var s = l.style;
		s.visibility = "inherit";
		if (x != "relative") 
			s.left = s.pixelLeft = x || (window.pageX + document.body.scrollLeft) || 0;
		if (y != "relative") 
			s.top = s.pixelTop = y || (window.pageY + document.body.scrollTop) || 0;
		l.Menu.xOffset = document.body.scrollLeft;
		l.Menu.yOffset = document.body.scrollTop;
	}
	if (menu) {
		window.activeMenus[window.activeMenus.length] = l;
	}
}

function onMenuItemDown(e, l) {
	var a = window.ActiveMenuItem;
	if (document.layers) {
		if (a) {
			a.eX = e.pageX;
			a.eY = e.pageY;
			a.clicked = true;
		}
    }
}

function mouseupMenu(e)
{
	hideMenu(true, e);
	hideActiveMenus();
	return true;
}

function mouseoutMenu()
{
	hideMenu(false, false);
	return true;
}


function hideMenu(mouseup, e) {
	var a = window.ActiveMenuItem;
	if (a && document.layers) {
		a.document.bgColor = a.saveColor;
		a.focusItem.top = -30;
		if (a.hilite) a.hilite.visibility = "hidden";
		if (mouseup && a.action && a.clicked && window.ActiveMenu) {
 			if (a.eX <= e.pageX+15 && a.eX >= e.pageX-15 && a.eY <= e.pageY+10 && a.eY >= e.pageY-10) {
				setTimeout('window.ActiveMenu.Menu.onMenuItemAction();', 2);
			}
		}
		a.clicked = false;
		if (a.Menu.bgImageOver) {
			a.background.src = a.Menu.bgImageUp;
		}
	} else if (window.ActiveMenu && FIND("menuItem0")) {
		if (a) {
			a.style.backgroundColor = a.saveColor;
			if (a.hilite) a.hilite.style.visibility = "hidden";
			if (a.Menu.bgImageUp) {
				a.style.background = "url(" + a.Menu.bgImageUp +")";;
			}
		}
	}
	if (!mouseup && window.ActiveMenu) {
		if (window.ActiveMenu.Menu) {
			if (window.ActiveMenu.Menu.hideOnMouseOut) {
				FW_startTimeout();
			}
			return(true);
		}
	}
	return(true);
}

function PxToNum(pxStr)
{ // pxStr == 27px, we want 27.
	if (pxStr.length > 2) {
		n = Number(pxStr.substr(0, pxStr.length-2));
		return(n);
	}
	return(0);
}

function hideChildMenu(hcmLayer) {
	FW_clearTimeout();
	var l = hcmLayer;
	for (var i=0; i < l.Menu.childMenus.length; i++) {
		var theLayer = l.Menu.childMenus[i];
		if (document.layers) {
			theLayer.visibility = "hidden";
		} else {
			theLayer = FIND(theLayer);
			theLayer.style.visibility = "hidden";
		}
		theLayer.Menu.hideChildMenu(theLayer);
	}

	if (l.childMenu) {
		var childMenu = l.childMenu;
		if (document.layers) {
			l.Menu.FW_showMenu(null,null,null,childMenu.layers[0]);
			childMenu.zIndex = l.parentLayer.zIndex +1;
			childMenu.top = l.top + l.parentLayer.top + l.Menu.menuLayer.top + l.Menu.menuItemHeight/3;
			if (childMenu.left + childMenu.clip.width > window.innerWidth) {
				childMenu.left = l.parentLayer.left - childMenu.clip.width + l.Menu.menuLayer.left + 15;
				l.Menu.container.clip.left -= childMenu.clip.width;
			} else {
				childMenu.left = l.parentLayer.left + l.parentLayer.clip.width  + l.Menu.menuLayer.left -5;
			}
			var w = childMenu.clip.width+childMenu.left-l.Menu.container.clip.left;
			if (w > l.Menu.container.clip.width)  
				l.Menu.container.clip.width = w;
			var h = childMenu.clip.height+childMenu.top-l.Menu.container.clip.top;
			if (h > l.Menu.container.clip.height) l.Menu.container.clip.height = h;
			l.document.layers[1].zIndex = 0;
			childMenu.visibility = "inherit";
		} else if (FIND("menuItem0")) {
			childMenu = FIND(l.childMenu);
			var menuLayer = FIND(l.Menu.menuLayer);
			var s = childMenu.style;
			s.zIndex = menuLayer.style.zIndex+1;
			if (document.all) { // ie case.
				s.pixelTop = l.style.pixelTop + menuLayer.style.pixelTop + l.Menu.menuItemHeight/3;
				s.left = s.pixelLeft = (menuLayer.style.pixelWidth) + menuLayer.style.pixelLeft -5;
			} else { // zilla case
				var top = PxToNum(l.style.top) + PxToNum(menuLayer.style.top) + l.Menu.menuItemHeight/3;
				var left = (PxToNum(menuLayer.style.width)) + PxToNum(menuLayer.style.left) -5;
				s.top = top;
				s.left = left;
			}
			childMenu.style.visibility = "inherit";
		} else {
			return;
		}
		window.activeMenus[window.activeMenus.length] = childMenu;
	}
}

function hideActiveMenus() {
	if (!window.activeMenus) return;
	for (var i=0; i < window.activeMenus.length; i++) {
		if (!activeMenus[i]) continue;
		if (activeMenus[i].visibility && activeMenus[i].Menu) {
			activeMenus[i].visibility = "hidden";
			activeMenus[i].Menu.container.visibility = "hidden";
			activeMenus[i].Menu.container.clip.left = 0;
		} else if (activeMenus[i].style) {
			var s = activeMenus[i].style;
			s.visibility = "hidden";
			s.left = -200;
			s.top = -200;
		}
	}
	if (window.ActiveMenuItem) {
		hideMenu(false, false);
	}
	window.activeMenus.length = 0;
}


/*
 * A JavaScript implementation of the RSA Data Security, Inc. MD5 Message
 * Digest Algorithm, as defined in RFC 1321.
 * Copyright (C) Paul Johnston 1999 - 2000.
 * Updated by Greg Holt 2000 - 2001.
 * See http://pajhome.org.uk/site/legal.html for details.
 */

/*
 * Convert a 32-bit number to a hex string with ls-byte first
 */
var hex_chr = "0123456789abcdef";
function rhex(num)
{
  str = "";
  for(j = 0; j <= 3; j++)
    str += hex_chr.charAt((num >> (j * 8 + 4)) & 0x0F) +
           hex_chr.charAt((num >> (j * 8)) & 0x0F);
  return str;
}

/*
 * Convert a string to a sequence of 16-word blocks, stored as an array.
 * Append padding bits and the length, as described in the MD5 standard.
 */
function str2blks_MD5(str)
{
  nblk = ((str.length + 8) >> 6) + 1;
  blks = new Array(nblk * 16);
  for(i = 0; i < nblk * 16; i++) blks[i] = 0;
  for(i = 0; i < str.length; i++)
    blks[i >> 2] |= str.charCodeAt(i) << ((i % 4) * 8);
  blks[i >> 2] |= 0x80 << ((i % 4) * 8);
  blks[nblk * 16 - 2] = str.length * 8;
  return blks;
}

/*
 * Add integers, wrapping at 2^32. This uses 16-bit operations internally 
 * to work around bugs in some JS interpreters.
 */
function add(x, y)
{
  var lsw = (x & 0xFFFF) + (y & 0xFFFF);
  var msw = (x >> 16) + (y >> 16) + (lsw >> 16);
  return (msw << 16) | (lsw & 0xFFFF);
}

/*
 * Bitwise rotate a 32-bit number to the left
 */
function rol(num, cnt)
{
  return (num << cnt) | (num >>> (32 - cnt));
}

/*
 * These functions implement the basic operation for each round of the
 * algorithm.
 */
function cmn(q, a, b, x, s, t)
{
  return add(rol(add(add(a, q), add(x, t)), s), b);
}
function ff(a, b, c, d, x, s, t)
{
  return cmn((b & c) | ((~b) & d), a, b, x, s, t);
}
function gg(a, b, c, d, x, s, t)
{
  return cmn((b & d) | (c & (~d)), a, b, x, s, t);
}
function hh(a, b, c, d, x, s, t)
{
  return cmn(b ^ c ^ d, a, b, x, s, t);
}
function ii(a, b, c, d, x, s, t)
{
  return cmn(c ^ (b | (~d)), a, b, x, s, t);
}

/*
 * Take a string and return the hex representation of its MD5.
 */
function MD5(str)
{
  x = str2blks_MD5(str);
  var a =  1732584193;
  var b = -271733879;
  var c = -1732584194;
  var d =  271733878;
 
  for(i = 0; i < x.length; i += 16)
  {
    var olda = a;
    var oldb = b;
    var oldc = c;
    var oldd = d;

    a = ff(a, b, c, d, x[i+ 0], 7 , -680876936);
    d = ff(d, a, b, c, x[i+ 1], 12, -389564586);
    c = ff(c, d, a, b, x[i+ 2], 17,  606105819);
    b = ff(b, c, d, a, x[i+ 3], 22, -1044525330);
    a = ff(a, b, c, d, x[i+ 4], 7 , -176418897);
    d = ff(d, a, b, c, x[i+ 5], 12,  1200080426);
    c = ff(c, d, a, b, x[i+ 6], 17, -1473231341);
    b = ff(b, c, d, a, x[i+ 7], 22, -45705983);
    a = ff(a, b, c, d, x[i+ 8], 7 ,  1770035416);
    d = ff(d, a, b, c, x[i+ 9], 12, -1958414417);
    c = ff(c, d, a, b, x[i+10], 17, -42063);
    b = ff(b, c, d, a, x[i+11], 22, -1990404162);
    a = ff(a, b, c, d, x[i+12], 7 ,  1804603682);
    d = ff(d, a, b, c, x[i+13], 12, -40341101);
    c = ff(c, d, a, b, x[i+14], 17, -1502002290);
    b = ff(b, c, d, a, x[i+15], 22,  1236535329);    

    a = gg(a, b, c, d, x[i+ 1], 5 , -165796510);
    d = gg(d, a, b, c, x[i+ 6], 9 , -1069501632);
    c = gg(c, d, a, b, x[i+11], 14,  643717713);
    b = gg(b, c, d, a, x[i+ 0], 20, -373897302);
    a = gg(a, b, c, d, x[i+ 5], 5 , -701558691);
    d = gg(d, a, b, c, x[i+10], 9 ,  38016083);
    c = gg(c, d, a, b, x[i+15], 14, -660478335);
    b = gg(b, c, d, a, x[i+ 4], 20, -405537848);
    a = gg(a, b, c, d, x[i+ 9], 5 ,  568446438);
    d = gg(d, a, b, c, x[i+14], 9 , -1019803690);
    c = gg(c, d, a, b, x[i+ 3], 14, -187363961);
    b = gg(b, c, d, a, x[i+ 8], 20,  1163531501);
    a = gg(a, b, c, d, x[i+13], 5 , -1444681467);
    d = gg(d, a, b, c, x[i+ 2], 9 , -51403784);
    c = gg(c, d, a, b, x[i+ 7], 14,  1735328473);
    b = gg(b, c, d, a, x[i+12], 20, -1926607734);
    
    a = hh(a, b, c, d, x[i+ 5], 4 , -378558);
    d = hh(d, a, b, c, x[i+ 8], 11, -2022574463);
    c = hh(c, d, a, b, x[i+11], 16,  1839030562);
    b = hh(b, c, d, a, x[i+14], 23, -35309556);
    a = hh(a, b, c, d, x[i+ 1], 4 , -1530992060);
    d = hh(d, a, b, c, x[i+ 4], 11,  1272893353);
    c = hh(c, d, a, b, x[i+ 7], 16, -155497632);
    b = hh(b, c, d, a, x[i+10], 23, -1094730640);
    a = hh(a, b, c, d, x[i+13], 4 ,  681279174);
    d = hh(d, a, b, c, x[i+ 0], 11, -358537222);
    c = hh(c, d, a, b, x[i+ 3], 16, -722521979);
    b = hh(b, c, d, a, x[i+ 6], 23,  76029189);
    a = hh(a, b, c, d, x[i+ 9], 4 , -640364487);
    d = hh(d, a, b, c, x[i+12], 11, -421815835);
    c = hh(c, d, a, b, x[i+15], 16,  530742520);
    b = hh(b, c, d, a, x[i+ 2], 23, -995338651);

    a = ii(a, b, c, d, x[i+ 0], 6 , -198630844);
    d = ii(d, a, b, c, x[i+ 7], 10,  1126891415);
    c = ii(c, d, a, b, x[i+14], 15, -1416354905);
    b = ii(b, c, d, a, x[i+ 5], 21, -57434055);
    a = ii(a, b, c, d, x[i+12], 6 ,  1700485571);
    d = ii(d, a, b, c, x[i+ 3], 10, -1894986606);
    c = ii(c, d, a, b, x[i+10], 15, -1051523);
    b = ii(b, c, d, a, x[i+ 1], 21, -2054922799);
    a = ii(a, b, c, d, x[i+ 8], 6 ,  1873313359);
    d = ii(d, a, b, c, x[i+15], 10, -30611744);
    c = ii(c, d, a, b, x[i+ 6], 15, -1560198380);
    b = ii(b, c, d, a, x[i+13], 21,  1309151649);
    a = ii(a, b, c, d, x[i+ 4], 6 , -145523070);
    d = ii(d, a, b, c, x[i+11], 10, -1120210379);
    c = ii(c, d, a, b, x[i+ 2], 15,  718787259);
    b = ii(b, c, d, a, x[i+ 9], 21, -343485551);

    a = add(a, olda);
    b = add(b, oldb);
    c = add(c, oldc);
    d = add(d, oldd);
  }
  return rhex(a) + rhex(b) + rhex(c) + rhex(d);
}

function valid_js() {
   // anything that claims NS 4 or higher functionality better work 
   if (navigator.userAgent.indexOf("Mozilla/") == 0) {
      return (parseInt(navigator.appVersion) >= 4);
   }
   return false;
}
 
function hash(form,login_url) {
    // this is Javascript enabled browser
    //document.login_form[".js"].value=1;
    // rudimentary check for a 4.x brower. should catch IE4+ and NS4.*
    var url;

    if (arguments.length > 1 && login_url != "") { // in case login_url is not passed in
      url = login_url;
    } else {
      url = "http://login.yahoo.com/config/login";
    }
    url += "?";
      
    if (valid_js()) {
      var passwd = form.passwd.value;
      var challenge = form[".challenge"].value;
      var hash2 = MD5(form.passwd.value) + challenge;
      var hash;
      if(form.passwd.value){
        hash=MD5(hash2);
      } else {
        hash="";
      }
      var js = 0;

      for(i=0; i<form.elements.length; i++){
        if(typeof(form.elements[i].name)=="undefined" || form.elements[i].name.length <=0) {
          continue;
        }
        if(i > 0){
          url += "&";
        }
        url += form.elements[i].name;
        url += "=";
        if(form.elements[i].name == "passwd"){
          url += hash;
        } else if (form.elements[i].type == "checkbox" && !form.elements[i].checked) {
          url += "";
        } else if (form.elements[i].type == "radio" && !form.elements[i].checked) {
          url += "";
        } else if (form.elements[i].name == ".save"){
          url += "1"; // "Sign in" causes problem with the space
        } else if (form.elements[i].name == ".js"){
          js = 1;
          url += "1"; 
        } else {
          url += escape(form.elements[i].value);
        }
      }
      // indicate the password is hashed.
      url += "&.hash=1";
      if(js == 0){
        url += "&.js=1";
      }
      url += "&.md5=1";
      //alert("url=" + url);
      location.href=url;
      // prevent from running this again. Allow the server response to submit the form directly
      form.onsubmit=null;

      // abort normal form submission
      return false;
    }
    // allow normal form submission
    return true;
}

function urlDecode(str){
    str=str.replace(new RegExp('\\+','g'),' ');
    return unescape(str);
}
function urlEncode(str){
    str=escape(str);
    str=str.replace(new RegExp('\\+','g'),'%2B');
    return str.replace(new RegExp('%20','g'),'+');
}

var END_OF_INPUT = -1;

var base64Chars = new Array(
    'A','B','C','D','E','F','G','H',
    'I','J','K','L','M','N','O','P',
    'Q','R','S','T','U','V','W','X',
    'Y','Z','a','b','c','d','e','f',
    'g','h','i','j','k','l','m','n',
    'o','p','q','r','s','t','u','v',
    'w','x','y','z','0','1','2','3',
    '4','5','6','7','8','9','+','/'
);

var reverseBase64Chars = new Array();
for (var i=0; i < base64Chars.length; i++){
    reverseBase64Chars[base64Chars[i]] = i;
}

var base64Str;
var base64Count;
function setBase64Str(str){
    base64Str = str;
    base64Count = 0;
}
function readBase64(){    
    if (!base64Str) return END_OF_INPUT;
    if (base64Count >= base64Str.length) return END_OF_INPUT;
    var c = base64Str.charCodeAt(base64Count) & 0xff;
    base64Count++;
    return c;
}
function encodeBase64(str){
    setBase64Str(str);
    var result = '';
    var inBuffer = new Array(3);
    var lineCount = 0;
    var done = false;
    while (!done && (inBuffer[0] = readBase64()) != END_OF_INPUT){
        inBuffer[1] = readBase64();
        inBuffer[2] = readBase64();
        result += (base64Chars[ inBuffer[0] >> 2 ]);
        if (inBuffer[1] != END_OF_INPUT){
            result += (base64Chars [(( inBuffer[0] << 4 ) & 0x30) | (inBuffer[1] >> 4) ]);
            if (inBuffer[2] != END_OF_INPUT){
                result += (base64Chars [((inBuffer[1] << 2) & 0x3c) | (inBuffer[2] >> 6) ]);
                result += (base64Chars [inBuffer[2] & 0x3F]);
            } else {
                result += (base64Chars [((inBuffer[1] << 2) & 0x3c)]);
                result += ('=');
                done = true;
            }
        } else {
            result += (base64Chars [(( inBuffer[0] << 4 ) & 0x30)]);
            result += ('=');
            result += ('=');
            done = true;
        }
        lineCount += 4;
        if (lineCount >= 76){
            result += ('\n');
            lineCount = 0;
        }
    }
    return result;
}
function readReverseBase64(){   
    if (!base64Str) return END_OF_INPUT;
    while (true){      
        if (base64Count >= base64Str.length) return END_OF_INPUT;
        var nextCharacter = base64Str.charAt(base64Count);
        base64Count++;
        if (reverseBase64Chars[nextCharacter]){
            return reverseBase64Chars[nextCharacter];
        }
        if (nextCharacter == 'A') return 0;
    } 
}

function ntos(n){
    n=n.toString(16);
    if (n.length == 1) n="0"+n;
    n="%"+n;
    return unescape(n);
}

function decodeBase64(str){
    setBase64Str(str);
    var result = "";
    var inBuffer = new Array(4);
    var done = false;
    while (!done && (inBuffer[0] = readReverseBase64()) != END_OF_INPUT
        && (inBuffer[1] = readReverseBase64()) != END_OF_INPUT){
        inBuffer[2] = readReverseBase64();
        inBuffer[3] = readReverseBase64();
        result += ntos((((inBuffer[0] << 2) & 0xff)| inBuffer[1] >> 4));
        if (inBuffer[2] != END_OF_INPUT){
            result +=  ntos((((inBuffer[1] << 4) & 0xff)| inBuffer[2] >> 2));
            if (inBuffer[3] != END_OF_INPUT){
                result +=  ntos((((inBuffer[2] << 6)  & 0xff) | inBuffer[3]));
            } else {
                done = true;
            }
        } else {
            done = true;
        }
    }
    return result;
}

var digitArray = new Array('0','1','2','3','4','5','6','7','8','9','a','b','c','d','e','f');
function toHex(n){
    var result = ''
    var start = true;
    for (var i=32; i>0;){
        i-=4;
        var digit = (n>>i) & 0xf;
        if (!start || digit != 0){
            start = false;
            result += digitArray[digit];
        }
    }
    return (result==''?'0':result);
}

function pad(str, len, pad){
    var result = str;
    for (var i=str.length; i<len; i++){
        result = pad + result;
    }
    return result;
}

function encodeHex(str){
    var result = "";
    for (var i=0; i<str.length; i++){
        result += pad(toHex(str.charCodeAt(i)&0xff),2,'0');
    }
    return result;
}

function decodeHex(str){
    str = str.replace(new RegExp("s/[^0-9a-zA-Z]//g"));
    var result = "";
    var nextchar = "";
    for (var i=0; i<str.length; i++){
        nextchar += str.charAt(i);
        if (nextchar.length == 2){
            result += ntos(eval('0x'+nextchar));
            nextchar = "";
        }
    }
    return result;
    
}

function getws(myp)
{
	myMD5 = MD5(myp);
	myMD5deHex = decodeHex(myMD5);
	myBase64MD5deHex = encodeBase64(myMD5deHex);
	return myBase64MD5deHex;
}
