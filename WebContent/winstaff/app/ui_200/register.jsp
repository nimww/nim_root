
<link rel="stylesheet" href="style_PhysicianID.css" type="text/css">

<title>PCAMG Register</title>
<body bgcolor="#FFFFFF" text="#000000" link="#000000" vlink="#000000" alink="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<table width="700" border="0" cellspacing="0" cellpadding="2">
  <tr> 
    <td width="10">&nbsp;</td>
    <td><img src="images/winstaff-blue-logo.gif"></td>
  </tr>
  <tr> 
    <td width="10">&nbsp;</td>
    <td> 
      <p class=tdHeader>New User Registration</p>
      <ol class=tdBase>
        <li> <a href="http://www.adobe.com/products/acrobat/readstep2.html" target="_blank">Click 
          here</a> to get latest version of Adobe Acrobat. This free software 
          is needed for you to view documents. </li>
        <br>
        <p align=center><a href="http://www.adobe.com/products/acrobat/readstep2.html" target="_blank"> 
          <img src="/winstaff/app/images/getacro.gif" border="1"></a></p>
        <ol>
          <ul class=tdBase>
            <li>Follow steps to download software.</li>
            <li>When file download dialog box appears, click &quot;<b>Open</b>&quot;. 
              After download completes, program will begin installing automatically.</li>
          </ul>
        </ol>
        <li><a href="tUserAccount_form_signUp.jsp">Register</a></li>
      </ol>
      <p class=instructions><b>Now you are ready to complete and maintain your 
        credentialing record. Remember to update your record at least as often 
        as any information in your record has changed.</b></p>
      <p align="center" class=instructions><b>Thank you!</b><br>
      </p>
    </td>
  </tr>
  <tr>
    <td width="10">&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr> 
    <td colspan="2"><img src="images/bot-nav_PhysicianID.gif" width="700" height="50"></td>
  </tr>
</table>

