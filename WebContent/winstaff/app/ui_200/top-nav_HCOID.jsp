<%@page contentType="text/html" language="java" import="com.winstaff.*" %>

<%@ include file="../../generic/CheckLogin.jsp" %>
 
<%
    Integer        iPhysicianID        =    null;
    Integer        iHCOLUID        =    null;
    boolean accessValid = false;
    if (pageControllerHash.containsKey("iPhysicianID")&&pageControllerHash.containsKey("iHCOLUID")) 
    {
        iPhysicianID        =    (Integer)pageControllerHash.get("iPhysicianID");
        iHCOLUID        =    (Integer)pageControllerHash.get("iHCOLUID");
        accessValid = true;
    }

 
    String plcID = request.getParameter("plcID");
    if ((plcID==null)||(plcID.equalsIgnoreCase("null")))
    {
        plcID="";
    }
    else
    {
        plcID="_"+plcID;
    }
%>
<link rel="stylesheet" href="ui<%=plcID%>\style_HCOID.css" type="text/css">
<script language="JavaScript">
<!--
function fwLoadMenus() {
  if (window.fw_menu_0) return;
  window.fw_menu_0 = new Menu("root",79,17,"Arial, Helvetica, sans-serif",10,"#000000","#993333","#ffffff","#cccccc");
  fw_menu_0.addMenuItem("HCO Status","location='pro-file_HCO-Status.jsp'");
  fw_menu_0.addMenuItem("Options","location='HCO_options.jsp'");
  fw_menu_0.addMenuItem("Sign Off","location='signOff.jsp'");
   fw_menu_0.fontWeight="bold";
   fw_menu_0.hideOnMouseOut=true;
  window.fw_menu_1 = new Menu("root",200,17,"Arial, Helvetica, sans-serif",10,"#000000","#993333","#ffffff","#cccccc");
  fw_menu_1.addMenuItem("Search Authorized Practitioners","location='HCO-query_HCOID.jsp'");
  fw_menu_1.addMenuItem("Search All Practitioners","location='HCO-query.jsp'");
  fw_menu_1.addMenuItem("Reports","location='reports_HCOID.jsp'");
   fw_menu_1.fontWeight="bold";
   fw_menu_1.hideOnMouseOut=true;
  window.fw_menu_2 = new Menu("root",161,17,"Arial, Helvetica, sans-serif",10,"#000000","#993333","#ffffff","#cccccc");
  fw_menu_2.addMenuItem("General Help","window.open('help_general.jsp', '_blank','scrollbars=yes,status=yes, width=630,height=500,resizable=yes');");
  fw_menu_2.addMenuItem("Frequently Asked Questions","window.open('help_faqs.jsp', '_blank','scrollbars=yes,status=yes, width=630,height=500,resizable=yes');");
  fw_menu_2.addMenuItem("Contact Us","window.open('ui_1/contact_us.jsp', '_blank','scrollbars=yes,status=yes, width=400,height=630,resizable=yes');");
  fw_menu_2.addMenuItem("About","window.open('ui_1/about.jsp', '_blank','scrollbars=yes,status=yes, width=630,height=500,resizable=yes');");
   fw_menu_2.fontWeight="bold";
   fw_menu_2.hideOnMouseOut=true;

  fw_menu_2.writeMenus();
} // fwLoadMenus()

//-->
</script>
<script language="JavaScript1.2" src="ui_1/fw_menu.js"></script>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor=#663333>
  <tr> 
    <td> 
      <script language="JavaScript1.2">fwLoadMenus();</script>
      <table border="0" cellpadding="0" cellspacing="0" width="700">
        <!-- fwtable fwsrc="top-nav2_HCOID.png" fwbase="top-nav_HCOID.jpg" fwstyle="Dreamweaver" fwdocid = "742308039" fwnested="0" -->
        <tr> 
          <td><img src="ui_1/images/spacer.gif" width="134" height="1" border="0"></td>
          <td><img src="ui_1/images/spacer.gif" width="92" height="1" border="0"></td>
          <td><img src="ui_1/images/spacer.gif" width="6" height="1" border="0"></td>
          <td><img src="ui_1/images/spacer.gif" width="102" height="1" border="0"></td>
          <td><img src="ui_1/images/spacer.gif" width="5" height="1" border="0"></td>
          <td><img src="ui_1/images/spacer.gif" width="59" height="1" border="0"></td>
          <td><img src="ui_1/images/spacer.gif" width="7" height="1" border="0"></td>
          <td><img src="ui_1/images/spacer.gif" width="77" height="1" border="0"></td>
          <td><img src="ui_1/images/spacer.gif" width="218" height="1" border="0"></td>
          <td><img src="ui_1/images/spacer.gif" width="1" height="1" border="0"></td>
        </tr>
        <tr> 
          <td colspan="9"><img name="topnav_HCOID_r1_c1" src="ui_1/images/top-nav_HCOID_r1_c1.jpg" width="700" height="66" border="0"></td>
          <td><img src="ui_1/images/spacer.gif" width="1" height="66" border="0"></td>
        </tr>
        <tr> 
          <td rowspan="2"><img name="topnav_HCOID_r2_c1" src="ui_1/images/top-nav_HCOID_r2_c1.jpg" width="134" height="34" border="0"></td>
          <td><a href="#" onMouseOut="FW_startTimeout();"  onMouseOver="window.FW_showMenu(window.fw_menu_0,135,91);" ><img name="topnav_HCOID_r2_c2" src="ui_1/images/top-nav_HCOID_r2_c2.jpg" width="92" height="25" border="0"></a></td>
          <td rowspan="2"><img name="topnav_HCOID_r2_c3" src="ui_1/images/top-nav_HCOID_r2_c3.jpg" width="6" height="34" border="0"></td>
          <td><a href="tAdminPhysicianLU_main_PhysicianMaster_AdminID.jsp" onMouseOut="FW_startTimeout();"  onMouseOver="window.FW_showMenu(window.fw_menu_1,232,91);" ><img name="topnav_HCOID_r2_c4" src="ui_1/images/top-nav_HCOID_r2_c4.jpg" width="102" height="25" border="0"></a></td>
          <td rowspan="2"><img name="topnav_HCOID_r2_c5" src="ui_1/images/top-nav_HCOID_r2_c5.jpg" width="5" height="34" border="0"></td>
          <td><a href="#" onMouseOut="FW_startTimeout();"  onMouseOver="window.FW_showMenu(window.fw_menu_2,340,91);" ><img name="topnav_HCOID_r2_c6" src="ui_1/images/top-nav_HCOID_r2_c6.jpg" width="59" height="25" border="0"></a></td>
          <td rowspan="2"><img name="topnav_HCOID_r2_c7" src="ui_1/images/top-nav_HCOID_r2_c7.jpg" width="7" height="34" border="0"></td>
          <td><a href="signOff.jsp"><img name="topnav_HCOID_r2_c8" src="ui_1/images/top-nav_HCOID_r2_c8.jpg" width="77" height="25" border="0"></a></td>
          <td rowspan="2"><img name="topnav_HCOID_r2_c9" src="ui_1/images/top-nav_HCOID_r2_c9.jpg" width="218" height="34" border="0"></td>
          <td><img src="ui_1/images/spacer.gif" width="1" height="25" border="0"></td>
        </tr>
        <tr> 
          <td><img name="topnav_HCOID_r3_c2" src="ui_1/images/top-nav_HCOID_r3_c2.jpg" width="92" height="9" border="0"></td>
          <td><img name="topnav_HCOID_r3_c4" src="ui_1/images/top-nav_HCOID_r3_c4.jpg" width="102" height="9" border="0"></td>
          <td><img name="topnav_HCOID_r3_c6" src="ui_1/images/top-nav_HCOID_r3_c6.jpg" width="59" height="9" border="0"></td>
          <td><img name="topnav_HCOID_r3_c8" src="ui_1/images/top-nav_HCOID_r3_c8.jpg" width="77" height="9" border="0"></td>
          <td><img src="ui_1/images/spacer.gif" width="1" height="9" border="0"></td>
        </tr>
      </table>
    </td>
  </tr>
<%
if (accessValid)
{
bltPhysicianMaster pm = new bltPhysicianMaster(iPhysicianID);
bltHCOPhysicianLU ph = new bltHCOPhysicianLU(iHCOLUID);
%>
  <tr> 
    <td> 
      <table width="700" border="1" cellspacing="0" cellpadding="3">
        <tr class=tdBaseAlt> 
          <td width=99%>Current Practitioner: <b><%=pm.getLastName()%>, <%=pm.getFirstName()%> <%=pm.getMiddleName()%></b></td> <td nowrap><b>Dept: <%=ph.getContDepartment()%></b></td>
          <td nowrap><b><%=iPhysicianID%></b></td>
        </tr>
      </table>
    </td>
  </tr>
<%
}
%>
</table>
