<%@page contentType="text/html" language="java" import="com.winstaff.*" %>

<%@ include file="../../generic/CheckLogin.jsp" %>

<%
//initial declaration of list class and parentID
    Integer        iPhysicianID        =    null;
    Integer        iHCOLUID        =    null;
    boolean accessValid = false;
    if (pageControllerHash.containsKey("iPhysicianID")&&pageControllerHash.containsKey("iHCOLUID")) 
    {
        iPhysicianID        =    (Integer)pageControllerHash.get("iPhysicianID");
        iHCOLUID        =    (Integer)pageControllerHash.get("iHCOLUID");
		accessValid=true;
    }
  //page security
  if (accessValid)
  {
  bltPhysicianMaster pm = new bltPhysicianMaster(iPhysicianID);
bltHCOPhysicianLU ph = new bltHCOPhysicianLU(iHCOLUID);
%>
<html>
<head>
<title>side-nav</title>
<meta http-equiv="Content-Type" content="text/html;">
<!-- Fireworks 4.0  Dreamweaver 4.0 target.  Created Mon Oct 07 16:23:07 GMT-0700 (Pacific Daylight Time) 2002-->
<link rel="stylesheet" href="style_SideNav.css" type="text/css">
<script language="JavaScript">
<!--
function MM_popupMsg(msg) { //v1.0
  alert(msg);
}
//-->
</script>
</head>
<base target="hcoMain">
<body bgcolor="#999999" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" text="#000000" link="#000000" vlink="#000000" alink="#000000">
<table border="2" cellspacing="0" cellpadding="0" bgcolor="#999999" bordercolor="#666666" width="700">
  <tr> 
    <td rowspan="2" width="150" height="105"> <img src="images/Phys_Tools1.jpg" width="150" height="105"></td>
    <td> 
      <table border="0" cellspacing="0" cellpadding="5" bgcolor="#999999" width="100%">
        <tr bgcolor="#999999" align="center" valign="bottom"> 
          <td nowrap> 
            <div align="center"><a href="../pro-file_Status.jsp"><img src="sid-nav_Images/home_icon1.gif" border="0" width="47" height="39"><br>
              Practitioner Home </a></div>
          </td>
          <td nowrap> 
            <div align="center"><a href="../FinishedCheckList_PhysicianID.jsp" ><img src="sid-nav_Images/maint_icon1.gif" border="0"><br>
              Final Checklist</a></div>
          </td>
          <td nowrap> 
            <div align="center"><a href="../populateForms.jsp"><img src="sid-nav_Images/forms_icon1.gif" border="0" ><br>
              Populate Forms</a></div>
          </td>
          <td nowrap> 
            <div align="center"><a href="../tDocumentManagement_query_PhysicianID.jsp?DocumentID=&DocumentTypeID=0&DocumentName=&DateOfExpiration=&Archived=2&orderBy=DocumentID&startID=0&maxResults=50&Submit2=Submit"><img src="sid-nav_Images/docu_icon1.gif" border="0" height="45"><br>
              Documents</a> </div>
          </td>
<%
if (false)
{
%>
          <td nowrap> 
            <div align="center"><a href="../reports_PhysicianID.jsp"><img src="sid-nav_Images/forms_icon1.gif" border="0" ><br>
              Reports</a></div>
          </td>
<%
}
%>
        </tr>
      </table>
    </td>
  </tr>
  <tr> 
    <td> 
      <table  border="1" cellspacing="0" cellpadding="3" bgcolor="#CCCCCC" width="100%">
        <tr class=tdBase> 
          <td width=99%>Current Practitioner: <b><%=pm.getLastName()%>, <%=pm.getFirstName()%> 
            <%=pm.getMiddleName()%></b></td>
          <td nowrap><b>Dept: <%=ph.getContDepartment()%></b></td>
          <td nowrap><b><%=iPhysicianID%></b></td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</body>
</html>
<%
}
%>
