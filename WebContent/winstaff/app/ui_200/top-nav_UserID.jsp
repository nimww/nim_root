


<%@ include file="../../generic/CheckLogin.jsp" %>
<html>
<head>
<title>top-nav_CompanyID.jpg</title>
<meta http-equiv="Content-Type" content="text/html;">
<!-- Fireworks 4.0  Dreamweaver 4.0 target.  Created Tue Mar 19 16:50:31 GMT-0800 (Pacific Standard Time) 2002-->
<script language="JavaScript">
<!--
function fwLoadMenus() {
  if (window.fw_menu_0) return;
  window.fw_menu_0 = new Menu("root",60,17,"Arial, Helvetica, sans-serif",10,"#000000","#993333","#ffffff","#cccccc");
  fw_menu_0.addMenuItem("Sample","location='#'");
   fw_menu_0.fontWeight="bold";
   fw_menu_0.hideOnMouseOut=true;
  window.fw_menu_1 = new Menu("root",138,17,"Arial, Helvetica, sans-serif",10,"#000000","#993333","#ffffff","#cccccc");
  fw_menu_1.addMenuItem("User Accounts","location='tCompanyUserAccountLU_main2_UserAccount_CompanyID.jsp'");
  fw_menu_1.addMenuItem("Admin Accounts","location='tCompanyAdminLU_main2_AdminMaster_CompanyID.jsp'");
  fw_menu_1.addMenuItem("Physician-Admin Control","location='tPhysicianMaster_via_CompanyID.jsp'");
  fw_menu_1.addMenuItem("Practice-Admin Control","location='tPracticeMaster_via_CompanyID.jsp'");
   fw_menu_1.fontWeight="bold";
   fw_menu_1.hideOnMouseOut=true;
  window.fw_menu_2 = new Menu("root",161,17,"Arial, Helvetica, sans-serif",10,"#000000","#993333","#ffffff","#cccccc");
  fw_menu_2.addMenuItem("General Help","window.open('help_general.jsp', '_blank','scrollbars=yes,status=yes, width=630,height=500,resizable=yes');");
  fw_menu_2.addMenuItem("Frequently Asked Questions","window.open('help_faqs.jsp', '_blank','scrollbars=yes,status=yes, width=630,height=500,resizable=yes');");
  fw_menu_2.addMenuItem("Contact Us","window.open('ui_1/contact_us.jsp', '_blank','scrollbars=yes,status=yes, width=400,height=630,resizable=yes');");
  fw_menu_2.addMenuItem("About","window.open('ui_1/about.jsp', '_blank','scrollbars=yes,status=yes, width=630,height=500,resizable=yes');");
   fw_menu_2.fontWeight="bold";
   fw_menu_2.hideOnMouseOut=true;

  fw_menu_2.writeMenus();
} // fwLoadMenus()

//-->
</script>
<script language="JavaScript1.2" src="ui_1/fw_menu.js"></script>
<link rel="stylesheet" href="ui_1/style_CompanyID.css" type="text/css">
</head>
<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" bgcolor="#ffffff">
<p> 
  <script language="JavaScript1.2">fwLoadMenus();</script>
</p>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td bgcolor="7c9bb2"> 
      <table border="0" cellpadding="0" cellspacing="0" width="700">
        <!-- fwtable fwsrc="top-nav2_CompanyID.png" fwbase="top-nav_CompanyID.jpg" fwstyle="Dreamweaver" fwdocid = "742308039" fwnested="0" -->
        <tr> 
          <td><img src="ui_1/images/spacer.gif" width="133" height="1" border="0"></td>
          <td><img src="ui_1/images/spacer.gif" width="92" height="1" border="0"></td>
          <td><img src="ui_1/images/spacer.gif" width="5" height="1" border="0"></td>
          <td><img src="ui_1/images/spacer.gif" width="118" height="1" border="0"></td>
          <td><img src="ui_1/images/spacer.gif" width="8" height="1" border="0"></td>
          <td><img src="ui_1/images/spacer.gif" width="59" height="1" border="0"></td>
          <td><img src="ui_1/images/spacer.gif" width="5" height="1" border="0"></td>
          <td><img src="ui_1/images/spacer.gif" width="77" height="1" border="0"></td>
          <td><img src="ui_1/images/spacer.gif" width="203" height="1" border="0"></td>
          <td><img src="ui_1/images/spacer.gif" width="1" height="1" border="0"></td>
        </tr>
        <tr> 
          <td colspan="9"><img name="topnav_CompanyID_r1_c1" src="ui_1/images/top-nav_CompanyID_r1_c1.jpg" width="700" height="60" border="0"></td>
          <td><img src="ui_1/images/spacer.gif" width="1" height="60" border="0"></td>
        </tr>
        <tr> 
          <td rowspan="2"><img name="topnav_CompanyID_r2_c1" src="ui_1/images/top-nav_CompanyID_r2_c1.jpg" width="133" height="35" border="0"></td>
          <td><a href="#" onMouseOut="FW_startTimeout();"  onMouseOver="window.FW_showMenu(window.fw_menu_0,134,85);" ><img name="topnav_CompanyID_r2_c2" src="ui_1/images/top-nav_CompanyID_r2_c2.jpg" width="92" height="25" border="0"></a></td>
          <td rowspan="2"><img name="topnav_CompanyID_r2_c3" src="ui_1/images/top-nav_CompanyID_r2_c3.jpg" width="5" height="35" border="0"></td>
          <td><a href="#" onMouseOut="FW_startTimeout();"  onMouseOver="window.FW_showMenu(window.fw_menu_1,231,85);" ><img name="topnav_CompanyID_r2_c4" src="ui_1/images/top-nav_CompanyID_r2_c4.jpg" width="118" height="25" border="0"></a></td>
          <td rowspan="2"><img name="topnav_CompanyID_r2_c5" src="ui_1/images/top-nav_CompanyID_r2_c5.jpg" width="8" height="35" border="0"></td>
          <td><a href="#" onMouseOut="FW_startTimeout();"  onMouseOver="window.FW_showMenu(window.fw_menu_2,357,85);" ><img name="topnav_CompanyID_r2_c6" src="ui_1/images/top-nav_CompanyID_r2_c6.jpg" width="59" height="25" border="0"></a></td>
          <td rowspan="2"><img name="topnav_CompanyID_r2_c7" src="ui_1/images/top-nav_CompanyID_r2_c7.jpg" width="5" height="35" border="0"></td>
          <td><a href="signOff.jsp"><img name="topnav_CompanyID_r2_c8" src="ui_1/images/top-nav_CompanyID_r2_c8.jpg" width="77" height="25" border="0"></a></td>
          <td rowspan="2"><img name="topnav_CompanyID_r2_c9" src="ui_1/images/top-nav_CompanyID_r2_c9.jpg" width="203" height="35" border="0"></td>
          <td><img src="ui_1/images/spacer.gif" width="1" height="25" border="0"></td>
        </tr>
        <tr> 
          <td><img name="topnav_CompanyID_r3_c2" src="ui_1/images/top-nav_CompanyID_r3_c2.jpg" width="92" height="10" border="0"></td>
          <td><img name="topnav_CompanyID_r3_c4" src="ui_1/images/top-nav_CompanyID_r3_c4.jpg" width="118" height="10" border="0"></td>
          <td><img name="topnav_CompanyID_r3_c6" src="ui_1/images/top-nav_CompanyID_r3_c6.jpg" width="59" height="10" border="0"></td>
          <td><img name="topnav_CompanyID_r3_c8" src="ui_1/images/top-nav_CompanyID_r3_c8.jpg" width="77" height="10" border="0"></td>
          <td><img src="ui_1/images/spacer.gif" width="1" height="10" border="0"></td>
        </tr>
      </table>
    </td>
    <td>&nbsp;</td>
  </tr>
</table>
</body>
</html>
