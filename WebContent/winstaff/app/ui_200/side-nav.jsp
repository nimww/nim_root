<%@page contentType="text/html" language="java" import="com.winstaff.*" %>

<%@ include file="../../generic/CheckLogin.jsp" %>

<html>
<head>
<title>side-nav</title>
<meta http-equiv="Content-Type" content="text/html;">
<!-- Fireworks 4.0  Dreamweaver 4.0 target.  Created Mon Oct 07 16:23:07 GMT-0700 (Pacific Daylight Time) 2002-->
<link rel="stylesheet" href="style_SideNav.css" type="text/css">
<script language="JavaScript">
<!--
function MM_popupMsg(msg) { //v1.0
  alert(msg);
}
//-->
</script>
</head>
<base target="wsMain">
<%
if (CurrentUserAccount.getAccountType().equalsIgnoreCase("HCOID")&&!CurrentUserAccount.getStartPage().equalsIgnoreCase("../phdb/tHCOPhysicianStanding_query.jsp"))
{
%>
<body bgcolor="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" text="#000000" link="#000000" vlink="#000000" alink="#000000">
<%
}
else if (CurrentUserAccount.getStartPage().equalsIgnoreCase("../phdb/tHCOPhysicianStanding_query.jsp"))
{
%>
<body bgcolor="#A49E86" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" text="#000000" link="#000000" vlink="#000000" alink="#000000">
<%
}
else
{
%>
<body bgcolor="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" text="#FFFFFF" link="#FFFFFF" vlink="#FFFFFF" alink="#FFFFFF">
<%
}
%>
<table border="0" cellspacing="0" cellpadding="2" bordercolor="#666666" align="center" >
  <tr> <td>
      <table border="0" cellspacing="0" cellpadding="4">
        <%
if (CurrentUserAccount.getAccountType().equalsIgnoreCase("GenAdminID"))
{
%>
        <tr>
          <td class=tdBaseAlt>Home:</td>
        </tr>
        <tr> 
          <td> 
            <div align="center"><a href="../AdminPracticeAll_home.jsp">Home</a></div>
          </td>
        </tr>
        <tr> 
          <td> 
            <div align="center"><a href="/winstaff/nim3/negotiationTable.jsp">N. Table</a></div>
          </td>
        </tr>
        <tr>
        <tr> 
          <td> 
            <div align="center"><a href="../AdminPracticeAll_home-seend.jsp">See NetDev</a></div>
          </td>
        </tr>
        <tr> 
          <td> 
            <div align="center"><a href="../AdminPracticeAll_home-act.jsp">Activities</a></div>
          </td>
        </tr>
        <tr> 
          <td> 
            <div align="center"><a href="../AdminPracticeAll_home-acc.jsp">Accounts</a></div>
          </td>
        </tr>
        <tr>
          <td class=tdBaseAlt>Adv. Search:</td>
        </tr>
        <tr> 
          <td> 
            <div align="center"><a href="../AdminPracticeAll_query.jsp"><img src="sid-nav_Images/new_icon_singlescreen.png" width="50" height="49" border="0"><br>
            No Frame</a></div>
          </td>
        </tr>
        <tr> 
          <td> 
            <div align="center"><a href="../AdminPracticeAll_frameT3.jsp"><img src="sid-nav_Images/new_icon_mutliscreen3.png" border="0"><br>
              3 Frame</a></div>
          </td>
        </tr>
        <tr>
          <td><hr></td>
        </tr>
        <tr>
          <td class=tdBaseAlt>Maintenance:</td>
        </tr>
        <tr> 
          <td> 
            <div align="center"><a href="../Practice_query.jsp">Unlinked<br>
            Practices</a></div>
          </td>
        </tr>
        <tr> 
          <td> 
            <div align="center"><a href="../Admin_query.jsp">Unlinked<br>
Accounts</a></div>
          </td>
        </tr>
        <tr>
          <td >&nbsp;</td>
        </tr>
        <tr> 
          <td> 
            <div align="center"><a href="../Admin_GenAdminID_create.jsp">Create Account</a></div>
          </td>
        </tr>
        <tr>
          <td >&nbsp;</td>
        </tr>
        <%
}
if (CurrentUserAccount.getAccountType().equalsIgnoreCase("CompanyID"))
{
%>
        <tr> 
          <td> 
            <div align="center"><a href="../pro-file-company_Status.jsp" ><img src="sid-nav_Images/maint_icon1.gif" width="46" height="47" border="0"><br>
              Maintenance</a></div>
          </td>
        </tr>
        <%
}
if (CurrentUserAccount.getAccountType().equalsIgnoreCase("AdminID"))
{
%>
        <tr> 
          <td> 
            <div align="center"><a href="../pro-file-manager_Status.jsp"><img src="sid-nav_Images/home_icon1.gif" border="0" width="47" height="39"><br>
              Admin Home </a></div>
          </td>
        </tr>
        <%
}
if (CurrentUserAccount.getAccountType().equalsIgnoreCase("SalesmanID"))
{
%>
        <tr> 
          <td> 
            <div align="center"><a href="../../sales/tSalesAccount_query.jsp"><img src="sid-nav_Images/practices_icon1.gif" border="0" width="64" height="47"><br>
            Search </a></div>
          </td>
        </tr>
        <%
}
if (CurrentUserAccount.getAccountType().equalsIgnoreCase("CVOID"))
{
%>
        <tr> 
          <td> 
            <div align="center"><a href="../pro-file_CVO-Status.jsp"><img src="sid-nav_Images/home_icon1.gif" border="0" width="47" height="39"><br>
              CVO Home </a></div>
          </td>
        </tr>
        <tr> 
          <td> 
            <div align="center"><a href="../CVO-query.jsp"><img src="sid-nav_Images/practitioners_icon2-search.gif" border="0" width="48" height="41"><br>
              Search Practitioners</a></div>
          </td>
        </tr>
        <%
	if (false)
	{
	%>
        <tr> 
          <td> 
            <div align="center"><a href="../tCVOHCOLU_main_HCOMaster_CVOID.jsp"><img src="sid-nav_Images/practitioners_icon2-search.gif" border="0" width="48" height="41"><br>
              View Groups</a></div>
          </td>
        </tr>
        <%
	}
	%>
        <tr> 
          <td> 
            <div align="center"><a href="../CVO-Practice-query.jsp"><img src="sid-nav_Images/building_icon1b.gif" border="0" width="42" height="41"><br>
              Search Practices</a></div>
          </td>
        </tr>
        <%
if (  (CurrentUserAccount.getAccessType().intValue()==2||CurrentUserAccount.getAccessType().intValue()==4)  )
{
%>
        <tr> 
          <td> 
            <div align="center"><a href="../CVOPhysician_Create.jsp"><img src="sid-nav_Images/masteradd_icon1.gif" border="0"><br>
              Create Practitioner</a></div>
          </td>
        </tr>
        <%
}
%>
        <tr> 
          <td> 
            <hr noshade>
          </td>
        </tr>
        <tr> 
          <td> 
            <div align="center"><a href="../tCVOPhysician_authorize.jsp?EDIT=open"><img src="sid-nav_Images/practitioners_icon1.gif" border="0"><br>
              Practitioner View</a></div>
          </td>
        </tr>
        <%
if (  (CurrentUserAccount.getAccessType().intValue()==2||CurrentUserAccount.getAccessType().intValue()==4)  )
{
%>
        <tr> 
          <td> 
            <div align="center"><a href="../tCVOPhysician_authorize.jsp?EDIT=edit"><img src="sid-nav_Images/practitioners_icon1.gif" border="0"><br>
              Practitioner Edit</a></div>
          </td>
        </tr>
        <%
}
%>
        <%
}
if (CurrentUserAccount.getAccountType().equalsIgnoreCase("HCOID")&&!CurrentUserAccount.getStartPage().equalsIgnoreCase("../phdb/tHCOPhysicianStanding_query.jsp"))
{
%>
        <tr> 
          <td> 
            <div align="center"><a href="../pro-file_HCO-Status.jsp"><img src="sid-nav_Images/home_icon1.gif" border="0" width="47" height="39"><br>
              HCO Home </a></div>
          </td>
        </tr>
        <tr> 
          <td> 
            <div align="center"><a href="../HCO-query.jsp"><img src="sid-nav_Images/practitioners_icon2-search.gif" border="0" width="48" height="41"><br>
              Search/Query All</a></div>
          </td>
        </tr>
        <tr> 
          <td> 
            <div align="center"><a href="../HCO-query_HCOID.jsp"><img src="sid-nav_Images/practitioners_icon2-search.gif" border="0" width="48" height="41"><br>
              Search/Query Authorized Only</a></div>
          </td>
        </tr>
        <tr> 
          <td> 
            <div align="center"><a href="../HCO-query_view.jsp"><img src="sid-nav_Images/practitioners_icon1.gif" border="0"><br>
              Practitioner View</a></div>
          </td>
        </tr>
        <tr> 
          <td> 
            <div align="center"><a href="../reports_HCOID.jsp"><img src="sid-nav_Images/forms_icon1.gif" border="0" ><br>
              Reports</a></div>
          </td>
        </tr>
        <%
}
if (CurrentUserAccount.getStartPage().equalsIgnoreCase("../phdb/tHCOPhysicianStanding_query.jsp"))
{
%>
        <tr> 
          <td> 
            <div align="center"><a href="../../phdb/tHCOPhysicianStanding_query.jsp"><img src="sid-nav_Images/practitioners_icon2-search.gif" border="0" width="48" height="41"><br>
              Search PHDB</a></div>
          </td>
        </tr>
        <tr> 
          <td> 
            <div align="center"><a href="../../phdb/tUserAccount_form.jsp" ><img src="sid-nav_Images/maint_icon1.gif" width="46" height="47" border="0"><br>
              Account info </a></div>
          </td>
        </tr>
        <tr> 
          <td> 
            <div align="center"><a href="../../phdb/help.jsp" ><img src="sid-nav_Images/help_icon1.gif"  border="0"><br>
              Help & Support </a></div>
          </td>
        </tr>
        <tr> 
          <td> 
            <hr noshade>
          </td>
        </tr>
        <tr> 
          <td> 
            <div align="center"> <a href="/winstaff/phdb/signIn.jsp" target="_top"><img src="sid-nav_Images/signOff1.gif" width="38" height="43" border="0"><br>
              Sign Off</a> </div>
          </td>
        </tr>
        <%
}

if (CurrentUserAccount.getAccountType().equalsIgnoreCase("AdminID"))
{
%>
        <tr> 
          <td> 
            <div align="center"><a href="../AdminPhysician_query.jsp?maxResults=50&startID=0&orderBy=LastName&Submit=Submit&PhysicianID=&LastName=&FirstName=&SSN=&IsAttested=0&DOBm=0&AttestDate=&LastModifiedDate="><img src="sid-nav_Images/practitioners_icon1.gif" border="0"><br>
              Practitioners </a></div>
          </td>
        </tr>
        <%
}
if (CurrentUserAccount.getAccountType().equalsIgnoreCase("AdminID"))
{
%>
        <tr> 
          <td> 
            <div align="center"><a href="../tAdminPhysician_MasterAdd.jsp"><img src="sid-nav_Images/masteradd_icon1.gif" border="0"><br>
              Find & Add a Practitioner</a></div>
          </td>
        </tr>
        <%
}
if (CurrentUserAccount.getAccountType().equalsIgnoreCase("AdminID"))
{
%>
        <tr> 
          <td> 
            <div align="center"><a href="../AdminPractice_query.jsp?PracticeID=&PracticeName=&OfficeCity=&OfficeStateID=0&OfficeZip=&orderBy=PracticeID&startID=0&maxResults=50&Submit2=Submit"><img src="sid-nav_Images/practices_icon1.gif" border="0"><br>
              Practices</a></div>
          </td>
        </tr>
        <%
}
if (CurrentUserAccount.getAccountType().equalsIgnoreCase("AdminID"))
{
%>
        <tr> 
          <td> 
            <div align="center"><a href="../QR_Admin_main.jsp"><img src="sid-nav_Images/forms_icon1.gif" border="0"><br>
              Queries &amp; Reports</a></div>
          </td>
        </tr>
        <%
}
if (CurrentUserAccount.getAccountType().equalsIgnoreCase("PhysicianID"))
{
%>
        <tr> 
          <td> 
            <div align="center"><a href="../tDocumentManagement_PhysicianID.jsp"><img src="sid-nav_Images/docu_icon1.gif" border="0"><br>
              Documents</a> </div>
          </td>
        </tr>
        <%
}
if (false)
{
%>
        <tr> 
          <td> 
            <div align="center"> <a href="../tResource_frame.jsp"><img src="sid-nav_Images/resources_icon1.gif" width="64" height="47" border="0"><br>
              Resources</a> </div>
          </td>
        </tr>
        <%
}
if (!CurrentUserAccount.getStartPage().equalsIgnoreCase("../phdb/tHCOPhysicianStanding_query.jsp"))
{
%>
        <tr> 
          <td> 
            <hr noshade>
          </td>
        </tr>
        <tr> 
          <td> 
            <div align="center"> <a href="signOff.jsp" target="_top">            Sign Off</a> </div>
          </td>
        </tr>
        <%
}
if (false)
{
%>
        <tr> 
          <td> 
            <div align="center"><a href="#" target="_self" onClick="MM_popupMsg('Coming Soon')">Credentialling</a></div>
          </td>
        </tr>
      </table>
    </td>
  </tr>
<%
}
%>


</table>
</body>
</html>
