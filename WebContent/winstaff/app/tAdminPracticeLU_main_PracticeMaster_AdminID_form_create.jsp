<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.bltDocumentManagement,java.util.Vector,java.util.Enumeration,com.winstaff.ListElement, com.winstaff.*, com.winstaff.SecurityCheck, com.winstaff.ConfigurationMessages,com.winstaff.bltPracticeMaster,com.winstaff.bltAdminPracticeLU,com.winstaff.bltAdminPracticeLU_List_LU_AdminID" %>
<%/*
    filename: out\jsp\tAdminPracticeLU_main_PracticeMaster_AdminID_form_create.jsp
    Created on Mar/21/2003
    Type: n-n main class file
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>


<%
//initial declaration of list class and parentID
    Integer        iAdminID        =    null;
    boolean accessValid = false;
   Integer iSecurityCheck = SecurityCheck.CheckItem("Practice1", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {

    {
        iAdminID        =    (Integer)pageControllerHash.get("iAdminID");
        if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {
            accessValid = true;
        }
    }
  //page security
  if (accessValid)
  {
        Integer requestID = null;
        if (request.getParameter("EDITID")!=null)
        {
            requestID = new Integer(request.getParameter("EDITID"));
            out.println(requestID);
        }

//declaration of Enumeration
    bltPracticeMaster        working_bltPracticeMaster = new bltPracticeMaster();
    working_bltPracticeMaster.setUniqueCreateDate(new java.util.Date());
    working_bltPracticeMaster.setUniqueModifyDate(new java.util.Date());
    working_bltPracticeMaster.commitDataForced();
    
	
	
	if (pageControllerHash.containsKey("UserLogonDescription"))
    {
        String UserLogonDescription = (String)pageControllerHash.get("UserLogonDescription");
        working_bltPracticeMaster.setUniqueModifyComments(""+UserLogonDescription);
    }

    bltAdminPracticeLU        working_bltAdminPracticeLU = new bltAdminPracticeLU();
    working_bltAdminPracticeLU.setAdminID(iAdminID);
    working_bltAdminPracticeLU.setRelationshipTypeID(new Integer(2));
    working_bltAdminPracticeLU.setPracticeID(working_bltPracticeMaster.getUniqueID());
    if (pageControllerHash.containsKey("UserLogonDescription"))
    {
        String UserLogonDescription = (String)pageControllerHash.get("UserLogonDescription");
        working_bltAdminPracticeLU.setUniqueModifyComments(""+UserLogonDescription);
    }

    working_bltAdminPracticeLU.commitData();
	bltUserAccount UA = new bltUserAccount(CurrentUserAccount.getUserID());
	bltNIM3_NetDevCommTrack NIM3_NetDevCommTrack = new bltNIM3_NetDevCommTrack();		
	NIM3_NetDevCommTrack.setUniqueCreateDate(PLCUtils.getNowDate(false),UserSecurityGroupID);
	NIM3_NetDevCommTrack.setUniqueModifyDate(PLCUtils.getNowDate(false),UserSecurityGroupID);
	NIM3_NetDevCommTrack.setUniqueModifyComments(UA.getContactFirstName()+" "+UA.getContactLastName(),UserSecurityGroupID);
	NIM3_NetDevCommTrack.setAccountID(working_bltPracticeMaster.getUniqueID(),UserSecurityGroupID);
	NIM3_NetDevCommTrack.setCommTypeID(0,UserSecurityGroupID);
	NIM3_NetDevCommTrack.setCommStart(PLCUtils.getNowDate(false),UserSecurityGroupID);
	NIM3_NetDevCommTrack.setCommEnd(PLCUtils.getNowDate(false),UserSecurityGroupID);
	NIM3_NetDevCommTrack.setMessageText("Created Practice",UserSecurityGroupID);
	NIM3_NetDevCommTrack.setMessageName(UA.getContactFirstName()+" "+UA.getContactLastName(),UserSecurityGroupID);
	
	NIM3_NetDevCommTrack.commitData();
    {
        pageControllerHash.put("iPracticeID",working_bltPracticeMaster.getUniqueID());
        pageControllerHash.put("sKeyMasterReference","p");
        session.setAttribute("pageControllerHash",pageControllerHash);
        //Parameter Pass Code here
String parameterPassString ="";
java.util.Enumeration myParameterPassList = request.getParameterNames();
while (myParameterPassList.hasMoreElements())
{
	String myName = (String)myParameterPassList.nextElement();
	String myS = (String) request.getParameter(myName);
	parameterPassString+="&"+myName + "=" + myS;
}
        String targetRedirect = "tPracticeMaster_form.jsp?nullParam=null"+parameterPassString    ;

        {
            targetRedirect = "tPracticeMaster_form.jsp?EDITID="+working_bltAdminPracticeLU.getUniqueID()+"&routePageReference=sParentReturnPage&EDIT=edit";
        }
        response.sendRedirect(targetRedirect);
    }

  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>

