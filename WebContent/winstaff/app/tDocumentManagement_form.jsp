
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="java.util.Vector,java.util.Enumeration,com.winstaff.ListElement, com.winstaff.PLCUtils,com.winstaff.bltDocumentManagement" %>
<%/*

    filename: out\jsp\tDocumentManagement_form.jsp
    Created on Apr/23/2002
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>

<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_PhysicianID.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>
<script language="JavaScript">
<!--
function MM_findObj(n, d) { //v4.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById) x=document.getElementById(n); return x;
}

function MM_validateForm() { //v4.0
  var i,p,q,nm,test,num,min,max,errors='',args=MM_validateForm.arguments;
  for (i=0; i<(args.length-2); i+=3) { test=args[i+2]; val=MM_findObj(args[i]);
    if (val) { nm=val.name; if ((val=val.value)!="") {
      if (test.indexOf('isEmail')!=-1) { p=val.indexOf('@');
        if (p<1 || p==(val.length-1)) errors+='- '+nm+' must contain an e-mail address.\n';
      } else if (test!='R') {
        if (isNaN(val)) errors+='- '+nm+' must contain a number.\n';
        if (test.indexOf('inRange') != -1) { p=test.indexOf(':');
          min=test.substring(8,p); max=test.substring(p+1);
          if (val<min || max<val) errors+='- '+nm+' must contain a number between '+min+' and '+max+'.\n';
    } } } else if (test.charAt(0) == 'R') errors += '- '+nm+' is required.\n'; }
  } if (errors) alert('The following error(s) occurred:\n'+errors);
  document.MM_returnValue = (errors == '');
}
//-->
</script>


    <table cellpadding=0 cellspacing=0 border=0 width=700>
    <tr><td width=10>&nbsp;</td><td>
    <span class=title>Document Management</span><br>


<%
//initial declaration of list class and parentID
    Integer        iDocumentID        =    null;
    boolean accessValid = false;
    // required for Type2
    String sKeyMasterReference = null;
    if (pageControllerHash.containsKey("iDocumentID")) 
    {
        iDocumentID        =    (Integer)pageControllerHash.get("iDocumentID");
        accessValid = true;    }
  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);      java.text.SimpleDateFormat displayDateTimeSDF = new java.text.SimpleDateFormat("MM/dd/yyyy hh:mm a");

    pageControllerHash.put("sLocalChildReturnPage","tDocumentManagement_query_PhysicianID.jsp?DocumentID=&DocumentTypeID=0&DocumentName=&DateOfExpiration=&Archived=2&orderBy=DocumentID&startID=0&maxResults=50&Submit2=Submit");
    pageControllerHash.put("sParentReturnPage","tDocumentManagement_query_PhysicianID.jsp?DocumentID=&DocumentTypeID=0&DocumentName=&DateOfExpiration=&Archived=2&orderBy=DocumentID&startID=0&maxResults=50&Submit2=Submit");
//initial declaration of list class and parentID

    bltDocumentManagement        DocumentManagement        =    null;

    if ( request.getParameter( "EDIT" ).equalsIgnoreCase("edit") )
    {
        DocumentManagement        =    new    bltDocumentManagement(iDocumentID);
    }
    else if ( request.getParameter( "EDIT" ).equalsIgnoreCase("new") )
    {
        DocumentManagement        =    new    bltDocumentManagement();
    }

//fields
        %>
        <%@ include file="tDocumentManagement_form_instructions.jsp" %>
        <form method=POST action="tDocumentManagement_form_sub.jsp" name="tDocumentManagement_form1">
<input type=hidden name="Archived" value=2>
<%
    if ( request.getParameter( "EDIT" ) != null )
    {
    %>
        <input type="hidden" name="EDIT" value = "<%=request.getParameter( "EDIT" )%>" >  
    <%
    }
%>
        <table border=0 cellpadding=0 cellspacing=0 class=tableBase>
        <tr><td>
        <table border=1 bordercolor=#333333 cellpadding=2 cellspacing=0 class=tableBase>
         <tr><td class=tableColor>
          <table border=0 bordercolor=#333333 cellpadding=2 cellspacing=0 class=tableBase>
           <tr class=tdBase><td><b>Item ID:&nbsp;</b><%=DocumentManagement.getDocumentID()%></td></tr>
           <tr class=tdBase><td><b>This Item was Created on:</b> <%=displayDateSDF.format(DocumentManagement.getUniqueCreateDate())%></td></tr>
           <tr class=tdBase><td><b>This Item was Last Modified on:</b> <%=displayDateSDF.format(DocumentManagement.getUniqueModifyDate())%></td></tr>
           <tr class=tdBase><td><b>Modification Comments:</b> <%=(DocumentManagement.getUniqueModifyComments())%></td></tr>
          </table>
         </td></tr>
        </table>
        </td></tr>
          <%  String theClass ="tdBase";%>

            <tr class=tdBase><td>&nbsp;</td></tr>
            <%
            if ( (DocumentManagement.isRequired("DocumentTypeID"))&&(!DocumentManagement.isComplete("DocumentTypeID")) )
            {
                theClass = "requiredField";
            }
            else if ((DocumentManagement.isExpired("DocumentTypeID",expiredDays))&&(DocumentManagement.isExpiredCheck("DocumentTypeID")))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <tr class=<%=theClass%> ><td><b>Document Type:&nbsp;</b><select   name="DocumentTypeID" ><jsp:include page="../generic/tDocumentTypeLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=DocumentManagement.getDocumentTypeID()%>" /></jsp:include></select></td></tr>
                <tr class=<%=theClass%> ><td>&nbsp;</td></tr>

            <%
            if ( (DocumentManagement.isRequired("DocumentName"))&&(!DocumentManagement.isComplete("DocumentName")) )
            {
                theClass = "requiredField";
            }
            else if ((DocumentManagement.isExpired("DocumentName",expiredDays))&&(DocumentManagement.isExpiredCheck("DocumentName")))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <tr class=<%=theClass%> ><td><b>Document Name:&nbsp;</b><input   type=text size="50" name="DocumentName" value="<%=DocumentManagement.getDocumentName()%>"></td></tr>
                <tr class=<%=theClass%> ><td>&nbsp;</td></tr>
            <%
            if ( (DocumentManagement.isRequired("DateOfExpiration"))&&(!DocumentManagement.isComplete("DateOfExpiration")) )
            {
                theClass = "requiredField";
            }
            else if ((DocumentManagement.isExpired("DateOfExpiration",expiredDays))&&(DocumentManagement.isExpiredCheck("DateOfExpiration")))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>

                        <tr class=<%=theClass%> ><td><b>Expiration Date</b>(mm/dd/yyyy):&nbsp;<input type=text size="20" name="DateOfExpiration" value='<jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(DocumentManagement.getDateOfExpiration())%>" /></jsp:include>' ></td></tr>
                        <tr class=<%=theClass%> ><td>&nbsp;</td></tr>


            <%
            if ( (DocumentManagement.isRequired("Comments"))&&(!DocumentManagement.isComplete("Comments")) )
            {
                theClass = "requiredField";
            }
            else if ((DocumentManagement.isExpired("Comments",expiredDays))&&(DocumentManagement.isExpiredCheck("Comments")))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <tr class=<%=theClass%> ><td><b>Extra Comments:&nbsp;</b><br><textarea rows="2" name="Comments" cols="40"><%=DocumentManagement.getComments()%></textarea></td></tr>
                <tr class=<%=theClass%> ><td>&nbsp;</td></tr>


        <%if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {%>
            <tr><td>&nbsp;</td></tr>
            <tr><td><select    name=routePageReference>
            <option value="sParentReturnPage">Save & Return</option>
            </select></td></tr>
            <tr><td>&nbsp;</td></tr>
            <tr><td>
              <input <%=HTMLFormStyleButton%> type=Submit value=Submit name=Submit onClick="MM_validateForm('DocumentName','','R','DateOfExpiration','','R');return document.MM_returnValue">
            </td></tr>
        <%}%>
        </table>
        </form>
        <%
  }
  else
  {
   response.sendRedirect("/errorhandler.jsp");
  }

%>

    </td></tr></table>



<%String bnIncludeFN = "ui_"+thePLCID+"\\bot-nav_PhysicianID.jsp";
%>
<jsp:include page="<%=bnIncludeFN%>" flush="true" ><jsp:param name="plcID" value="<%=thePLCID%>"/></jsp:include>
