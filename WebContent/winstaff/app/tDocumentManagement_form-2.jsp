<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.bltDocumentManagement,java.util.Vector,java.util.Enumeration,com.winstaff.ListElement, com.winstaff.PLCUtils, com.winstaff.SecurityCheck, com.winstaff.ConfigurationMessages,com.winstaff.bltDocumentManagement" %>
<%/*

    filename: out\jsp\tDocumentManagement_form.jsp
    Created on Mar/21/2003
    Created by: Scott Ellis
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>

<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_PhysicianID.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>


    <table cellpadding=0 cellspacing=0 border=0 width=<%=MasterTableWidth%> >
    <tr><td width=10>&nbsp;</td><td>
    <%=ConfigurationMessages.getHTML("INTERVIEWTopControl_form","tDocumentManagement")%>



<%
//initial declaration of list class and parentID
    Integer        iDocumentID        =    null;
    boolean accessValid = false;
    // required for Type2
    String sKeyMasterReference = null;

   Integer iSecurityCheck = SecurityCheck.CheckItem("DocumentManagement1", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {
    if (pageControllerHash.containsKey("iDocumentID")) 
    {
        iDocumentID        =    (Integer)pageControllerHash.get("iDocumentID");
        accessValid = true;    }
  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);      java.text.SimpleDateFormat displayDateTimeSDF = new java.text.SimpleDateFormat("MM/dd/yyyy hh:mm a");

    pageControllerHash.put("sLocalChildReturnPage","tDocumentManagement_form.jsp");
//initial declaration of list class and parentID

    bltDocumentManagement        DocumentManagement        =    null;

    if ( request.getParameter( "EDIT" ).equalsIgnoreCase("edit") )
    {
        DocumentManagement        =    new    bltDocumentManagement(iDocumentID,UserSecurityGroupID);
    }
    else if ( request.getParameter( "EDIT" ).equalsIgnoreCase("new") )
    {
        DocumentManagement        =    new    bltDocumentManagement(UserSecurityGroupID, true);
    }

//fields
        %>
        <%@ include file="tDocumentManagement_form_instructions.jsp" %>
        <form action="tDocumentManagement_form_sub.jsp" name="tDocumentManagement_form1" method="POST">
<%
    if ( request.getParameter( "EDIT" ) != null )
    {
    %>
        <input type="hidden" name="EDIT" value = "<%=request.getParameter( "EDIT" )%>" >  
    <%
    }
%>
          <%  String theClass ="tdBase";%>
        <table width=100% border=1 bordercolor=#333333 cellpadding=3 cellspacing=0 class=tableBase>
         <tr><td class=tableColor>


            <table cellpadding=0 cellspacing=0 width=100%>
            <%
            if ( (DocumentManagement.isRequired("DocumentTypeID",UserSecurityGroupID))&&(!DocumentManagement.isComplete("DocumentTypeID")) )
            {
                theClass = "requiredField";
            }
            else if ((DocumentManagement.isExpired("DocumentTypeID",expiredDays))&&(DocumentManagement.isExpiredCheck("DocumentTypeID",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((DocumentManagement.isWrite("DocumentTypeID",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Document Type&nbsp;</b></p></td><td valign=top><p><select   name="DocumentTypeID" ><jsp:include page="../generic/tDocumentTypeLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=DocumentManagement.getDocumentTypeID()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=DocumentTypeID&amp;sTableName=tDocumentManagement&amp;sRefID=<%=DocumentManagement.getDocumentID()%>&amp;sFieldNameDisp=<%=DocumentManagement.getEnglish("DocumentTypeID")%>&amp;sTableNameDisp=tDocumentManagement','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((DocumentManagement.isRead("DocumentTypeID",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Document Type&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tDocumentTypeLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=DocumentManagement.getDocumentTypeID()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=DocumentTypeID&amp;sTableName=tDocumentManagement&amp;sRefID=<%=DocumentManagement.getDocumentID()%>&amp;sFieldNameDisp=<%=DocumentManagement.getEnglish("DocumentTypeID")%>&amp;sTableNameDisp=tDocumentManagement','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (DocumentManagement.isRequired("DocumentName",UserSecurityGroupID))&&(!DocumentManagement.isComplete("DocumentName")) )
            {
                theClass = "requiredField";
            }
            else if ((DocumentManagement.isExpired("DocumentName",expiredDays))&&(DocumentManagement.isExpiredCheck("DocumentName",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((DocumentManagement.isWrite("DocumentName",UserSecurityGroupID)))
            {
                        %>
                       <tr><td valign=top><p class=<%=theClass%> ><b><%=DocumentManagement.getEnglish("DocumentName")%>&nbsp;</b></p></td><td valign=top><p><textarea onKeyDown="textAreaStop(this,200)" rows="2" name="DocumentName" cols="40" maxlength=200><%=DocumentManagement.getDocumentName()%></textarea>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=DocumentName&amp;sTableName=tDocumentManagement&amp;sRefID=<%=DocumentManagement.getDocumentID()%>&amp;sFieldNameDisp=<%=DocumentManagement.getEnglish("DocumentName")%>&amp;sTableNameDisp=tDocumentManagement','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((DocumentManagement.isRead("DocumentName",UserSecurityGroupID)))
            {
                        %>
                       <tr><td valign=top> <p class=<%=theClass%> ><b><%=DocumentManagement.getEnglish("DocumentName")%>&nbsp;</b></p></td><td valign=top><p><%=DocumentManagement.getDocumentName()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=DocumentName&amp;sTableName=tDocumentManagement&amp;sRefID=<%=DocumentManagement.getDocumentID()%>&amp;sFieldNameDisp=<%=DocumentManagement.getEnglish("DocumentName")%>&amp;sTableNameDisp=tDocumentManagement','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (DocumentManagement.isRequired("DocumentFileName",UserSecurityGroupID))&&(!DocumentManagement.isComplete("DocumentFileName")) )
            {
                theClass = "requiredField";
            }
            else if ((DocumentManagement.isExpired("DocumentFileName",expiredDays))&&(DocumentManagement.isExpiredCheck("DocumentFileName",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((DocumentManagement.isWrite("DocumentFileName",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>File Name&nbsp;</b></p></td><td valign=top><p><input maxlength="100" type=text size="80" name="DocumentFileName" value="<%=DocumentManagement.getDocumentFileName()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=DocumentFileName&amp;sTableName=tDocumentManagement&amp;sRefID=<%=DocumentManagement.getDocumentID()%>&amp;sFieldNameDisp=<%=DocumentManagement.getEnglish("DocumentFileName")%>&amp;sTableNameDisp=tDocumentManagement','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((DocumentManagement.isRead("DocumentFileName",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>File Name&nbsp;</b></p></td><td valign=top><p><%=DocumentManagement.getDocumentFileName()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=DocumentFileName&amp;sTableName=tDocumentManagement&amp;sRefID=<%=DocumentManagement.getDocumentID()%>&amp;sFieldNameDisp=<%=DocumentManagement.getEnglish("DocumentFileName")%>&amp;sTableNameDisp=tDocumentManagement','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (DocumentManagement.isRequired("DateReceived",UserSecurityGroupID))&&(!DocumentManagement.isComplete("DateReceived")) )
            {
                theClass = "requiredField";
            }
            else if ((DocumentManagement.isExpired("DateReceived",expiredDays))&&(DocumentManagement.isExpiredCheck("DateReceived",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>

            <%
            if ((DocumentManagement.isWrite("DateReceived",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Date Received&nbsp;(mm/dd/yyyy):&nbsp;</b></p></td><td valign=top><p><input maxlength=10  type=text size="80" name="DateReceived" value='<jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(DocumentManagement.getDateReceived())%>" /></jsp:include>' >&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=DateReceived&amp;sTableName=tDocumentManagement&amp;sRefID=<%=DocumentManagement.getDocumentID()%>&amp;sFieldNameDisp=<%=DocumentManagement.getEnglish("DateReceived")%>&amp;sTableNameDisp=tDocumentManagement','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((DocumentManagement.isRead("DateReceived",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Date Received&nbsp;(mm/dd/yyyy):&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(DocumentManagement.getDateReceived())%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=DateReceived&amp;sTableName=tDocumentManagement&amp;sRefID=<%=DocumentManagement.getDocumentID()%>&amp;sFieldNameDisp=<%=DocumentManagement.getEnglish("DateReceived")%>&amp;sTableNameDisp=tDocumentManagement','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (DocumentManagement.isRequired("DateOfExpiration",UserSecurityGroupID))&&(!DocumentManagement.isComplete("DateOfExpiration")) )
            {
                theClass = "requiredField";
            }
            else if ((DocumentManagement.isExpired("DateOfExpiration",expiredDays))&&(DocumentManagement.isExpiredCheck("DateOfExpiration",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>

            <%
            if ((DocumentManagement.isWrite("DateOfExpiration",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Expiration Date&nbsp;(mm/dd/yyyy):&nbsp;</b></p></td><td valign=top><p><input maxlength=10  type=text size="80" name="DateOfExpiration" value='<jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(DocumentManagement.getDateOfExpiration())%>" /></jsp:include>' >&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=DateOfExpiration&amp;sTableName=tDocumentManagement&amp;sRefID=<%=DocumentManagement.getDocumentID()%>&amp;sFieldNameDisp=<%=DocumentManagement.getEnglish("DateOfExpiration")%>&amp;sTableNameDisp=tDocumentManagement','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((DocumentManagement.isRead("DateOfExpiration",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Expiration Date&nbsp;(mm/dd/yyyy):&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(DocumentManagement.getDateOfExpiration())%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=DateOfExpiration&amp;sTableName=tDocumentManagement&amp;sRefID=<%=DocumentManagement.getDocumentID()%>&amp;sFieldNameDisp=<%=DocumentManagement.getEnglish("DateOfExpiration")%>&amp;sTableNameDisp=tDocumentManagement','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (DocumentManagement.isRequired("DateAlertSent",UserSecurityGroupID))&&(!DocumentManagement.isComplete("DateAlertSent")) )
            {
                theClass = "requiredField";
            }
            else if ((DocumentManagement.isExpired("DateAlertSent",expiredDays))&&(DocumentManagement.isExpiredCheck("DateAlertSent",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>

            <%
            if ((DocumentManagement.isWrite("DateAlertSent",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Date Alert Sent&nbsp;(mm/dd/yyyy):&nbsp;</b></p></td><td valign=top><p><input maxlength=10  type=text size="80" name="DateAlertSent" value='<jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(DocumentManagement.getDateAlertSent())%>" /></jsp:include>' >&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=DateAlertSent&amp;sTableName=tDocumentManagement&amp;sRefID=<%=DocumentManagement.getDocumentID()%>&amp;sFieldNameDisp=<%=DocumentManagement.getEnglish("DateAlertSent")%>&amp;sTableNameDisp=tDocumentManagement','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((DocumentManagement.isRead("DateAlertSent",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Date Alert Sent&nbsp;(mm/dd/yyyy):&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(DocumentManagement.getDateAlertSent())%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=DateAlertSent&amp;sTableName=tDocumentManagement&amp;sRefID=<%=DocumentManagement.getDocumentID()%>&amp;sFieldNameDisp=<%=DocumentManagement.getEnglish("DateAlertSent")%>&amp;sTableNameDisp=tDocumentManagement','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (DocumentManagement.isRequired("Archived",UserSecurityGroupID))&&(!DocumentManagement.isComplete("Archived")) )
            {
                theClass = "requiredField";
            }
            else if ((DocumentManagement.isExpired("Archived",expiredDays))&&(DocumentManagement.isExpiredCheck("Archived",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((DocumentManagement.isWrite("Archived",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Archive&nbsp;</b></p></td><td valign=top><p><select   name="Archived" ><jsp:include page="../generic/tYesNoLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=DocumentManagement.getArchived()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Archived&amp;sTableName=tDocumentManagement&amp;sRefID=<%=DocumentManagement.getDocumentID()%>&amp;sFieldNameDisp=<%=DocumentManagement.getEnglish("Archived")%>&amp;sTableNameDisp=tDocumentManagement','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((DocumentManagement.isRead("Archived",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Archive&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=DocumentManagement.getArchived()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Archived&amp;sTableName=tDocumentManagement&amp;sRefID=<%=DocumentManagement.getDocumentID()%>&amp;sFieldNameDisp=<%=DocumentManagement.getEnglish("Archived")%>&amp;sTableNameDisp=tDocumentManagement','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (DocumentManagement.isRequired("Comments",UserSecurityGroupID))&&(!DocumentManagement.isComplete("Comments")) )
            {
                theClass = "requiredField";
            }
            else if ((DocumentManagement.isExpired("Comments",expiredDays))&&(DocumentManagement.isExpiredCheck("Comments",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((DocumentManagement.isWrite("Comments",UserSecurityGroupID)))
            {
                        %>
                       <tr><td valign=top><p class=<%=theClass%> ><b><%=DocumentManagement.getEnglish("Comments")%>&nbsp;</b></p></td><td valign=top><p><textarea onKeyDown="textAreaStop(this,200)" rows="2" name="Comments" cols="40" maxlength=200><%=DocumentManagement.getComments()%></textarea>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Comments&amp;sTableName=tDocumentManagement&amp;sRefID=<%=DocumentManagement.getDocumentID()%>&amp;sFieldNameDisp=<%=DocumentManagement.getEnglish("Comments")%>&amp;sTableNameDisp=tDocumentManagement','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((DocumentManagement.isRead("Comments",UserSecurityGroupID)))
            {
                        %>
                       <tr><td valign=top> <p class=<%=theClass%> ><b><%=DocumentManagement.getEnglish("Comments")%>&nbsp;</b></p></td><td valign=top><p><%=DocumentManagement.getComments()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Comments&amp;sTableName=tDocumentManagement&amp;sRefID=<%=DocumentManagement.getDocumentID()%>&amp;sFieldNameDisp=<%=DocumentManagement.getEnglish("Comments")%>&amp;sTableNameDisp=tDocumentManagement','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>




            <tr><td width=40%>&nbsp;</td><td width=60%>&nbsp;</td></tr>
            </table>
        <%if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {%>
            <input type=hidden name=routePageReference value="sParentReturnPage">
             <%
              if (request.getParameter("INTNext")!=null&&request.getParameter("INTNext").equalsIgnoreCase("yes") ) 
              {
              %>
                  <table width=75% border=1 bordercolor=333333 align=left cellspacing=0 cellpadding=0 class=wizardTable>
                  <tr class=requiredField><td>
                  <input  <%=HTMLFormStyleButton%> type="radio" value="next" name="INTNext" checked>&nbsp;&nbsp;<%=ConfigurationMessages.getInterviewMessage("INTERVIEWNoMore","tDocumentManagement")%>
                  <br>
                  <input  <%=HTMLFormStyleButton%> type="radio" value="yes" name="INTNext">&nbsp;&nbsp;<%=ConfigurationMessages.getInterviewMessage("INTERVIEWAddMore","tDocumentManagement")%>
                  </td></tr></table><br><br><br>
              <%
              }
              %>
            <p><input <%=HTMLFormStyleButton%> type=Submit value="Continue" name=Submit></p>
        <%}%>
        </td></tr></table>
        </form>
        <%
  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>

    </td></tr></table>



<%String bnIncludeFN = "ui_"+thePLCID+"\\bot-nav_PhysicianID.jsp";
%>
<jsp:include page="<%=bnIncludeFN%>" flush="true" ><jsp:param name="plcID" value="<%=thePLCID%>"/></jsp:include>
