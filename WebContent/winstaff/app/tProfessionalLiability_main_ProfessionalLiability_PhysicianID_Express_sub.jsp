<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.bltDocumentManagement,java.util.Vector,java.util.Enumeration,com.winstaff.ListElement, com.winstaff.PLCUtils, com.winstaff.SecurityCheck, com.winstaff.ConfigurationMessages,com.winstaff.bltProfessionalLiability,com.winstaff.bltProfessionalLiability_List,com.winstaff.DocumentManagerUtils" %>
<%/*
    filename: tProfessionalLiability_main_ProfessionalLiability_PhysicianID.jsp
    Created on Nov/12/2003
    Type: 1-n main class file
    Created by: Scott Ellis
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>
    <%
    Integer iExpressMode = new Integer(1);
    if (pageControllerHash.containsKey("iExpressMode")) 
    {
        iExpressMode =    (Integer)pageControllerHash.get("iExpressMode");
    }
    %>

    <table cellpadding=0 cellspacing=0 border=0 width=800 >
<%
//initial declaration of list class and parentID
    Integer        iPhysicianID        =    null;
    boolean accessValid = false;
   Integer iSecurityCheck = SecurityCheck.CheckItem("PractitionerSection9", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {
    if (pageControllerHash.containsKey("iPhysicianID")) 
    {
        iPhysicianID        =    (Integer)pageControllerHash.get("iPhysicianID");
        accessValid = true;
    }

  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);
      pageControllerHash.put("sParentReturnPage","tProfessionalLiability_main_ProfessionalLiability_PhysicianID_Expand.jsp");
    pageControllerHash.remove("iProfessionalLiabilityID");
    session.setAttribute("pageControllerHash",pageControllerHash);

    bltProfessionalLiability_List        bltProfessionalLiability_List        =    new    bltProfessionalLiability_List(iPhysicianID);

//declaration of Enumeration
    bltProfessionalLiability        ProfessionalLiability;
    ListElement         leCurrentElement;
    Enumeration eList = bltProfessionalLiability_List.elements();
    %>
    <%
    int iExpress=0;
boolean errorRouteMeTotal = false;
boolean isAllComplete = true;
    while (eList.hasMoreElements()||iExpress<=ConfigurationMessages.getExpressItemCount("tProfessionalLiability"))
    {
       iExpress++;

      boolean isNewRecord= false;
      if (eList.hasMoreElements())
      {
        leCurrentElement    = (ListElement) eList.nextElement();
        ProfessionalLiability  = (bltProfessionalLiability) leCurrentElement.getObject();
      }
      else
      {
        ProfessionalLiability  = new bltProfessionalLiability();
        ProfessionalLiability.setPhysicianID(iPhysicianID);
        isNewRecord= true;
      }
        ProfessionalLiability.GroupSecurityInit(UserSecurityGroupID);
        String theClass = "tdHeader";
        %>

        <%  {

String testChangeID = "0";

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("UniqueCreateDate_"+iExpress))) ;
    if ( !ProfessionalLiability.getUniqueCreateDate().equals(testObj)  )
    {
         ProfessionalLiability.setUniqueCreateDate( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("ProfessionalLiability UniqueCreateDate not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("UniqueModifyDate_"+iExpress))) ;
    if ( !ProfessionalLiability.getUniqueModifyDate().equals(testObj)  )
    {
         ProfessionalLiability.setUniqueModifyDate( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("ProfessionalLiability UniqueModifyDate not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("UniqueModifyComments_"+iExpress)) ;
    if ( !ProfessionalLiability.getUniqueModifyComments().equals(testObj)  )
    {
         ProfessionalLiability.setUniqueModifyComments( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("ProfessionalLiability UniqueModifyComments not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("PhysicianID_"+iExpress)) ;
    if ( !ProfessionalLiability.getPhysicianID().equals(testObj)  )
    {
         ProfessionalLiability.setPhysicianID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("ProfessionalLiability PhysicianID not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("CoverageType_"+iExpress)) ;
    if ( !ProfessionalLiability.getCoverageType().equals(testObj)  )
    {
         ProfessionalLiability.setCoverageType( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("ProfessionalLiability CoverageType not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("InsuranceCarrier_"+iExpress)) ;
    if ( !ProfessionalLiability.getInsuranceCarrier().equals(testObj)  )
    {
         ProfessionalLiability.setInsuranceCarrier( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("ProfessionalLiability InsuranceCarrier not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("PolicyHolder_"+iExpress)) ;
    if ( !ProfessionalLiability.getPolicyHolder().equals(testObj)  )
    {
         ProfessionalLiability.setPolicyHolder( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("ProfessionalLiability PolicyHolder not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("AgentName_"+iExpress)) ;
    if ( !ProfessionalLiability.getAgentName().equals(testObj)  )
    {
         ProfessionalLiability.setAgentName( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("ProfessionalLiability AgentName not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("Address1_"+iExpress)) ;
    if ( !ProfessionalLiability.getAddress1().equals(testObj)  )
    {
         ProfessionalLiability.setAddress1( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("ProfessionalLiability Address1 not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("Address2_"+iExpress)) ;
    if ( !ProfessionalLiability.getAddress2().equals(testObj)  )
    {
         ProfessionalLiability.setAddress2( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("ProfessionalLiability Address2 not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("City_"+iExpress)) ;
    if ( !ProfessionalLiability.getCity().equals(testObj)  )
    {
         ProfessionalLiability.setCity( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("ProfessionalLiability City not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("StateID_"+iExpress)) ;
    if ( !ProfessionalLiability.getStateID().equals(testObj)  )
    {
         ProfessionalLiability.setStateID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("ProfessionalLiability StateID not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("Province_"+iExpress)) ;
    if ( !ProfessionalLiability.getProvince().equals(testObj)  )
    {
         ProfessionalLiability.setProvince( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("ProfessionalLiability Province not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("ZIP_"+iExpress)) ;
    if ( !ProfessionalLiability.getZIP().equals(testObj)  )
    {
         ProfessionalLiability.setZIP( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("ProfessionalLiability ZIP not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("CountryID_"+iExpress)) ;
    if ( !ProfessionalLiability.getCountryID().equals(testObj)  )
    {
         ProfessionalLiability.setCountryID( testObj,UserSecurityGroupID   );
         //testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("ProfessionalLiability CountryID not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("Phone_"+iExpress)) ;
    if ( !ProfessionalLiability.getPhone().equals(testObj)  )
    {
         ProfessionalLiability.setPhone( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("ProfessionalLiability Phone not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("Fax_"+iExpress)) ;
    if ( !ProfessionalLiability.getFax().equals(testObj)  )
    {
         ProfessionalLiability.setFax( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("ProfessionalLiability Fax not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("ContactName_"+iExpress)) ;
    if ( !ProfessionalLiability.getContactName().equals(testObj)  )
    {
         ProfessionalLiability.setContactName( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("ProfessionalLiability ContactName not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("ContactEmail_"+iExpress)) ;
    if ( !ProfessionalLiability.getContactEmail().equals(testObj)  )
    {
         ProfessionalLiability.setContactEmail( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("ProfessionalLiability ContactEmail not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("PolicyNumber_"+iExpress)) ;
    if ( !ProfessionalLiability.getPolicyNumber().equals(testObj)  )
    {
         ProfessionalLiability.setPolicyNumber( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("ProfessionalLiability PolicyNumber not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("InsuredFromDate_"+iExpress))) ;
    if ( !ProfessionalLiability.getInsuredFromDate().equals(testObj)  )
    {
         ProfessionalLiability.setInsuredFromDate( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("ProfessionalLiability InsuredFromDate not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("InsuredToDate_"+iExpress))) ;
    if ( !ProfessionalLiability.getInsuredToDate().equals(testObj)  )
    {
         ProfessionalLiability.setInsuredToDate( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("ProfessionalLiability InsuredToDate not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("OriginalEffectiveDate_"+iExpress))) ;
    if ( !ProfessionalLiability.getOriginalEffectiveDate().equals(testObj)  )
    {
         ProfessionalLiability.setOriginalEffectiveDate( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("ProfessionalLiability OriginalEffectiveDate not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("TerminationDate_"+iExpress))) ;
    if ( !ProfessionalLiability.getTerminationDate().equals(testObj)  )
    {
         ProfessionalLiability.setTerminationDate( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("ProfessionalLiability TerminationDate not set. this is ok-not an error");
}

try
{
    Double testObj = null ;
    testObj = (PLCUtils.getDecimalDefault(request.getParameter("PerClaimAmount_"+iExpress))) ;
    if ( !ProfessionalLiability.getPerClaimAmount().equals(testObj)  )
    {
         ProfessionalLiability.setPerClaimAmount( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("ProfessionalLiability PerClaimAmount not set. this is ok-not an error");
}

try
{
    Double testObj = null ;
    testObj = (PLCUtils.getDecimalDefault(request.getParameter("AggregateAmount_"+iExpress))) ;
    if ( !ProfessionalLiability.getAggregateAmount().equals(testObj)  )
    {
         ProfessionalLiability.setAggregateAmount( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("ProfessionalLiability AggregateAmount not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("DescOfSurcharge_"+iExpress)) ;
    if ( !ProfessionalLiability.getDescOfSurcharge().equals(testObj)  )
    {
         ProfessionalLiability.setDescOfSurcharge( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("ProfessionalLiability DescOfSurcharge not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("DocuLinkID_"+iExpress)) ;
    if ( !ProfessionalLiability.getDocuLinkID().equals(testObj)  )
    {
         ProfessionalLiability.setDocuLinkID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("ProfessionalLiability DocuLinkID not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("Comments_"+iExpress)) ;
    if ( !ProfessionalLiability.getComments().equals(testObj)  )
    {
         ProfessionalLiability.setComments( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("ProfessionalLiability Comments not set. this is ok-not an error");
}
boolean bINT = false;
boolean bNextStep = false;
String sRefreshDoc = "refreshMe=true&";
              if (false&&request.getParameter("INTNext")!=null&&request.getParameter("INTNext").equalsIgnoreCase("yes") ) 
	            {
	                bINT = true;
	                sRefreshDoc = "";
	            }
              else if (request.getParameter("INTNext")!=null&&request.getParameter("INTNext").equalsIgnoreCase("next") ) 
	            {
	                bINT = true;
	                sINTNext = ConfigurationMessages.getInterviewLinkRaw("tProfessionalLiability","next");
	                sRefreshDoc = "";
	            }

// If an edit, update information; if new, append information
boolean errorRouteMe = false;
if (testChangeID.equalsIgnoreCase("1"))
{

	ProfessionalLiability.setUniqueModifyDate(new java.util.Date());
	String UserLogonDescription = "n/a";
	if (pageControllerHash.containsKey("UserLogonDescription"))
	{
	    UserLogonDescription = (String)pageControllerHash.get("UserLogonDescription");
	}
    if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
    {
	    ProfessionalLiability.setUniqueModifyComments("EX:"+UserLogonDescription);
	    try
	    {
	        ProfessionalLiability.commitData();
	        if (!ProfessionalLiability.isComplete())
	        {
	            isAllComplete = false;
	        }
	    }
	    catch (Exception e55)
	    {
	        errorRouteMe = true;
	        errorRouteMeTotal = true;
	    }	//Doculink Addition
	//Doculink Does Exist, prompt to Modify

     if (!errorRouteMe&&ProfessionalLiability.getDocuLinkID().intValue()>0)
     {
            bltDocumentManagement myDoc = new bltDocumentManagement(ProfessionalLiability.getDocuLinkID());
            if (myDoc.getDocumentFileName().equalsIgnoreCase("")) 
            {
                DocumentManagerUtils myDUtils = new DocumentManagerUtils();
                String myDType = "tProfessionalLiability";
                myDUtils.setDocumentType(myDType, ProfessionalLiability.getUniqueID());
                myDoc.setDocumentName(myDUtils.getDocumentName());
                myDoc.setDateOfExpiration(myDUtils.getDateOfExpiration());
                myDoc.setDocumentTypeID(myDUtils.getDocumentTypeID());
                myDoc.setPhysicianID(iPhysicianID);
                myDoc.setArchived(new Integer(2));
                myDoc.commitData();
            }
            else
            {
                //archive then create new
                myDoc.setArchived(new Integer("1"));
                myDoc.commitData();
                myDoc = new bltDocumentManagement();
                DocumentManagerUtils myDUtils = new DocumentManagerUtils();
                String myDType = "tProfessionalLiability";
                myDUtils.setDocumentType(myDType, ProfessionalLiability.getUniqueID());
                myDoc.setDocumentName(myDUtils.getDocumentName());
                myDoc.setDateOfExpiration(myDUtils.getDateOfExpiration());
                myDoc.setDocumentTypeID(myDUtils.getDocumentTypeID());
                myDoc.setPhysicianID(iPhysicianID);
                myDoc.setArchived(new Integer(2));
                myDoc.commitData();
                ProfessionalLiability.setDocuLinkID(myDoc.getUniqueID());
                ProfessionalLiability.commitData();
            }
     }
     else if (!errorRouteMe)
     {
            bltDocumentManagement myDoc = new bltDocumentManagement();
            DocumentManagerUtils myDUtils = new DocumentManagerUtils();
            String myDType = "tProfessionalLiability";
            myDUtils.setDocumentType(myDType, ProfessionalLiability.getUniqueID());
            myDoc.setDocumentName(myDUtils.getDocumentName());
            myDoc.setDateOfExpiration(myDUtils.getDateOfExpiration());
            myDoc.setDocumentTypeID(myDUtils.getDocumentTypeID());
                myDoc.setPhysicianID(iPhysicianID);
            myDoc.setArchived(new Integer(2));
            myDoc.commitData();
            ProfessionalLiability.setDocuLinkID(myDoc.getUniqueID());
            ProfessionalLiability.commitData();
     }

    }
}       }
    }//while

if (errorRouteMeTotal)
{
	out.println(ConfigurationMessages.getMessage("FatalError")); 
}
else 
{
String nextPage = request.getParameter("nextPage");
if (nextPage==null||nextPage.equalsIgnoreCase(""))
{
	nextPage = ConfigurationMessages.getExpressLinkRaw("tProfessionalLiability","next",iExpressMode);
}
if (!isAllComplete)
{
	%>
	<script language=Javascript>
	      if ( confirm("<%=ConfigurationMessages.getMessage("ConfirmReqFieldsReturn")%>") )
	      {
	          document.location="<%=ConfigurationMessages.getDataCategoryLink("tProfessionalLiability","Express")%>";
	      }
	      else 
	      {
	          document.location="<%=nextPage%>";
	      }
	</script>
  <%
}
else
{
	%>
	<script language=Javascript>
	      document.location="<%=nextPage%>";
	</script>
  <%
}
}
       
  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>

    </td></tr></table>


<%String bnIncludeFN = "ui_"+thePLCID+"\\bot-nav_PhysicianID.jsp";
%>
<jsp:include page="<%=bnIncludeFN%>" flush="true" ><jsp:param name="plcID" value="<%=thePLCID%>"/></jsp:include>
