<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.ConfigurationInformation,com.winstaff.bltDocumentManagement,java.util.Vector,java.util.Enumeration,com.winstaff.ListElement, com.winstaff.PLCUtils, com.winstaff.SecurityCheck, com.winstaff.ConfigurationMessages,com.winstaff.bltOtherCertification,com.winstaff.bltOtherCertification_List" %>
<%/*
    filename: tOtherCertification_main_OtherCertification_PhysicianID.jsp
    Created on Mar/21/2003
    Type: 1-n main class file
    Created by: Scott Ellis
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>

<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_PhysicianID.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>

    <table cellpadding=0 cellspacing=0 border=0 width=<%=MasterTableWidth%> >
    <tr><td width=10>&nbsp;</td><td>
    <%=ConfigurationMessages.getHTML("INTERVIEWTopControl","tOtherCertification")%>



<%
//initial declaration of list class and parentID
    Integer        iPhysicianID        =    null;
    boolean accessValid = false;
   Integer iSecurityCheck = SecurityCheck.CheckItem("PractitionerSection5", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {
    if (pageControllerHash.containsKey("iPhysicianID")) 
    {
        iPhysicianID        =    (Integer)pageControllerHash.get("iPhysicianID");
        accessValid = true;
    }
  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);
      pageControllerHash.put("sParentReturnPage","tOtherCertification_main_OtherCertification_PhysicianID.jsp");
    pageControllerHash.remove("iOtherCertID");
    pageControllerHash.put("sINTNext","tOtherCertification_main_OtherCertification_PhysicianID_form_create.jsp?EDIT=new&KM=p&INTNext=yes");
    session.setAttribute("pageControllerHash",pageControllerHash);

    bltOtherCertification_List        bltOtherCertification_List        =    new    bltOtherCertification_List(iPhysicianID);

//declaration of Enumeration
    bltOtherCertification        working_bltOtherCertification;
    ListElement         leCurrentElement;
    Enumeration eList = bltOtherCertification_List.elements();
    %>
        <%@ include file="tOtherCertification_main_OtherCertification_PhysicianID_instructions.jsp" %>

        <%if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {%>
        <a class=linkBase href = "tOtherCertification_main_OtherCertification_PhysicianID_form_create.jsp?EDIT=new&KM=p&INTNext=yes"><img border=0 src="ui_<%=thePLCID%>/icons/create_PhysicianID.gif"></a>
        <%}%>
         <table border="1" bordercolor="CCCCCC" cellpadding="3" class=tdBase cellspacing="0" width="100%">
    <%
    int altCnt = 0;
    if (eList.hasMoreElements())
    {
     while (eList.hasMoreElements())
     {

        altCnt++;
        String theClass = "tdBase";
        if (altCnt%2!=0)
        {
            theClass = "tdBaseAlt";
        }
        leCurrentElement    = (ListElement) eList.nextElement();
        working_bltOtherCertification  = (bltOtherCertification) leCurrentElement.getObject();
        working_bltOtherCertification.GroupSecurityInit(UserSecurityGroupID);
        if (!working_bltOtherCertification.isComplete())
        {
            theClass = "incompleteItem";
        %>
                <tr class=incompleteItem><td><b>Not Complete</b><br>
        <%
        }
        else
        {
        %>
        <tr class=<%=theClass%> ><td> 
        <%
        }
        %>

              <b>Item ID:&nbsp;</b><%=working_bltOtherCertification.getOtherCertID()%></td>
<%String theClassF = "textBase";
		String sStatus="";
		if ((working_bltOtherCertification.isExpired("ExpirationDate",expiredDays)))
		{
			sStatus = "<b>Status: </b><span class=requiredField>Expired</span>";
		}
		else if ((working_bltOtherCertification.getExpirationDate().after(displayDateSDF1.parse("1/1/2000"))))
		{
			sStatus = "<b>Status: <font color=#006600>Current</font></b>";
		}
		else 
		{
			sStatus = "&nbsp;";
		}

%>

<%theClassF = "textBase";%>
<%if ((working_bltOtherCertification.isExpired("CertificationType",expiredDays))&&(working_bltOtherCertification.isExpiredCheck("CertificationType"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltOtherCertification.isRequired("CertificationType"))&&(!working_bltOtherCertification.isComplete("CertificationType")) ){theClassF = "requiredFieldMain";}%>
            <td><p class=<%=theClassF%> ><b>Type:&nbsp;</b><%=working_bltOtherCertification.getCertificationType()%>
              <br>
              <b>Issued by:&nbsp;</b><%=working_bltOtherCertification.getName()%>
			</p></td>

<%theClassF = "textBase";%>
            <td><p class=<%=theClassF%> ><b>Certificate #:&nbsp;</b><%=working_bltOtherCertification.getCertificationNumber()%>
			<br><%=sStatus%></p></td>

            <td > 
        <a class=linkBase href = "tOtherCertification_main_OtherCertification_PhysicianID_form_authorize.jsp?EDIT=edit&EDITID=<%=working_bltOtherCertification.getOtherCertID()%>&KM=p"><img border=0 src="ui_<%=thePLCID%>/icons/edit_PhysicianID.gif"></a>

        <%if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {%>
        <a class=linkBase  onClick="return confirmDelete()"  href = "tOtherCertification_main_OtherCertification_PhysicianID_form_authorize.jsp?EDIT=del&EDITID=<%=working_bltOtherCertification.getOtherCertID()%>&KM=p"><img border=0 src="ui_<%=thePLCID%>/icons/delete_PhysicianID.gif"></a>
        <br>
        <% 
            if (working_bltOtherCertification.getDocuLinkID().intValue()>0)
            {
              bltDocumentManagement myDoc = new bltDocumentManagement(working_bltOtherCertification.getDocuLinkID());
              if (!myDoc.getDocumentFileName().equalsIgnoreCase(""))
              {
              %>
                      <a target=_blank href="tOtherCertification_main_OtherCertification_PhysicianID_form_authorize.jsp?EDIT=print&EDITID=<%=working_bltOtherCertification.getOtherCertID()%>&KM=p">View Document</a>

              <%
              }
              else
              {
              %>

                      <a class=linkBase target=_blank href = "#" onClick="window.open('tDocumentManagement_main_DocumentManagement_PhysicianID_form_authorize.jsp?EDIT=faxCover&amp;EDITID=<%=working_bltOtherCertification.getDocuLinkID()%>','DocPDF','toolbar=yes,location=yes,status=yes,menubar=yes,scrollbars=yes,resizable=yes,width=500,height=500');return false;" >Print Cover Sheet</a>

              <%
              }
            }
            else
            {
             if (working_bltOtherCertification.isComplete())
             {
               %>
                      <a target=_blank class=linkBase href = "tOtherCertification_ModifyDocument.jsp?EDITID=<%=working_bltOtherCertification.getOtherCertID()%>&EDIT=CREATE&dType=tOtherCertification">Create Document</a>
               <%
             }
             else
             {
               %>
                      <span class=tdBase>You must complete this item before you can attach a document</span>
               <%
             }
            }
            %>
        <% }%>
                  </td></tr>
        <%
    }//end while
       }//end of if
       else 
       {
           %>
           <tr><td><b>Please click the "create" to add <%=ConfigurationMessages.getDataCategory("tOtherCertification")%> information or click 'Continue' to go to the next section.</b>
           <script language=javascript>
           if (confirm("<%=ConfigurationMessages.getInterviewMessage("INTERVIEWNoElements","tOtherCertification")%>"))
           {
               document.location="tOtherCertification_main_OtherCertification_PhysicianID_form_create.jsp?EDIT=new&KM=p&INTNext=yes"; 
           }
           else
           {

           }
           </script>
           </td></tr>
           <%
       }
    %>

    </table>
    <%
  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>

    </td></tr></table><br>


<%String bnIncludeFN = "ui_"+thePLCID+"\\bot-nav_PhysicianID.jsp";
%>
<jsp:include page="<%=bnIncludeFN%>" flush="true" ><jsp:param name="plcID" value="<%=thePLCID%>"/></jsp:include>
