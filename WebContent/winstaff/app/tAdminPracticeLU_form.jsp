<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.bltDocumentManagement,java.util.Vector,java.util.Enumeration,com.winstaff.ListElement, com.winstaff.PLCUtils, com.winstaff.*, com.winstaff.SecurityCheck, com.winstaff.ConfigurationMessages, com.winstaff.bltAdminPracticeLU" %>
<%/*

    filename: out\jsp\tAdminPracticeLU_form.jsp
    Created on Nov/03/2009
    Created by: Scott Ellis
    Version: 4.0
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>

<link href="ui_1/style.css" rel="stylesheet" type="text/css" />

<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_AdminID_nim.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>


    <table cellpadding=0 cellspacing=0 border=0 width=<%=MasterTableWidth%> >
    <tr><td width=10>&nbsp;</td><td>
    <%//ConfigurationMessages.getHTML("INTERVIEWTopControl_form","tAdminPracticeLU")%>



<%
//initial declaration of list class and parentID
    Integer        iLookupID        =    null;
    Integer        iEDITID        =    null;
    if ( request.getParameter( "EDITID" ) != null )
    {
    	iEDITID        =    new Integer(request.getParameter ("EDITID"));
    }
    else
    {
    	iEDITID        =    new Integer(0);
    }
    boolean accessValid = false;
    // required for Type2
    String sKeyMasterReference = null;

   Integer iSecurityCheck = SecurityCheck.CheckItem("Practice1", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {
    if (pageControllerHash.containsKey("iLookupID")) 
    {
        iLookupID        =    (Integer)pageControllerHash.get("iLookupID");
        accessValid = true;
        if (iLookupID.intValue() != iEDITID.intValue())
        {
        	accessValid = false;
        }

        if (pageControllerHash.containsKey("sKeyMasterReference"))
        {
            sKeyMasterReference = (String)pageControllerHash.get("sKeyMasterReference");
        }
        if (sKeyMasterReference!=null)
        {
            if (sKeyMasterReference.equalsIgnoreCase("p"))
            {
                if (!pageControllerHash.containsKey("iAdminID")) 
                {
                    accessValid=false;
                }
            }
            else if (sKeyMasterReference.equalsIgnoreCase("s"))
            {
                if (!pageControllerHash.containsKey("iPracticeID")) 
                {
                    accessValid=false;
                }
            }
            else
            {
                accessValid=false;
            }
        }
        else 
        {
            accessValid=false;
        }
    }
  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);      java.text.SimpleDateFormat displayDateTimeSDF = new java.text.SimpleDateFormat("MM/dd/yyyy hh:mm a");

    pageControllerHash.put("sLocalChildReturnPage","tAdminPracticeLU_form.jsp?EDIT=edit&EDITID=" + iEDITID);
//initial declaration of list class and parentID

    bltAdminPracticeLU        AdminPracticeLU        =    null;

    if ( request.getParameter( "EDIT" ).equalsIgnoreCase("edit") )
    {
        AdminPracticeLU        =    new    bltAdminPracticeLU(iLookupID,UserSecurityGroupID);
    }
    else if ( request.getParameter( "EDIT" ).equalsIgnoreCase("new") )
    {
        AdminPracticeLU        =    new    bltAdminPracticeLU(UserSecurityGroupID, true);
    }

//fields
        %>
        <%@ include file="tAdminPracticeLU_form_instructions.jsp" %>
        <form action="tAdminPracticeLU_form_sub.jsp" name="tAdminPracticeLU_form1" method="POST">
<%
    if ( request.getParameter( "EDIT" ) != null )
    {
    %>
        <input type="hidden" name="EDIT" value = "<%=request.getParameter( "EDIT" )%>" >  
    <%
    }
    %>
        <input type="hidden" name="EDITID" value = "<%=iEDITID%>" >  

          <%  String theClass ="tdBase";%>
        <table width=100% border=1 bordercolor=#333333 cellpadding=3 cellspacing=0 class=tableBase>
         <tr><td class=tableColor>
            <%
            if (false&&request.getParameter( "EDIT" ).equalsIgnoreCase("edit"))
            {
            %>
            <p class=tdBase><b>AdminID</b> <%=AdminPracticeLU.getAdminID()%></p>
            <p class=tdBaseAlt><b>PracticeID</b> <%=AdminPracticeLU.getPracticeID()%></p>
            <%
            }
            else if (sKeyMasterReference.equalsIgnoreCase("p"))
            {
            %>
            <p class=tdBase><b>AdminID</b> <%=AdminPracticeLU.getAdminID()%></p>
            <p class=tdBaseAlt><b>PracticeID</b><input   type=text name="PracticeID" value="<%=AdminPracticeLU.getPracticeID()%>"></p>
            <%
            }
            else if (sKeyMasterReference.equalsIgnoreCase("s"))
            {
            %>
            <p class=tdBase><b>PracticeID</b> <%=AdminPracticeLU.getPracticeID()%></p>
            <p class=tdBaseAlt><b>AdminID</b><input   type=text name="AdminID" value="<%=AdminPracticeLU.getAdminID()%>"></p>
            <%
            }
            %>
            <table cellpadding=0 cellspacing=0 width=100%>
            <%
            if ( (AdminPracticeLU.isRequired("Comments",UserSecurityGroupID))&&(!AdminPracticeLU.isComplete("Comments")) )
            {
                theClass = "requiredField";
            }
            else if ((AdminPracticeLU.isExpired("Comments",expiredDays))&&(AdminPracticeLU.isExpiredCheck("Comments",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((AdminPracticeLU.isWrite("Comments",UserSecurityGroupID)))
            {
                        %>
                       <tr><td valign=top><p class=<%=theClass%> ><b><%=AdminPracticeLU.getEnglish("Comments")%>&nbsp;</b></p></td><td valign=top><p><textarea onKeyDown="textAreaStop(this,200)" rows="2" name="Comments" cols="40" maxlength=200><%=AdminPracticeLU.getComments()%></textarea>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Comments&amp;sTableName=tAdminPracticeLU&amp;sRefID=<%=AdminPracticeLU.getLookupID()%>&amp;sFieldNameDisp=<%=AdminPracticeLU.getEnglish("Comments")%>&amp;sTableNameDisp=tAdminPracticeLU','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((AdminPracticeLU.isRead("Comments",UserSecurityGroupID)))
            {
                        %>
                       <tr><td valign=top> <p class=<%=theClass%> ><b><%=AdminPracticeLU.getEnglish("Comments")%>&nbsp;</b></p></td><td valign=top><p><%=AdminPracticeLU.getComments()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Comments&amp;sTableName=tAdminPracticeLU&amp;sRefID=<%=AdminPracticeLU.getLookupID()%>&amp;sFieldNameDisp=<%=AdminPracticeLU.getEnglish("Comments")%>&amp;sTableNameDisp=tAdminPracticeLU','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (AdminPracticeLU.isRequired("RelationshipTypeID",UserSecurityGroupID))&&(!AdminPracticeLU.isComplete("RelationshipTypeID")) )
            {
                theClass = "requiredField";
            }
            else if ((AdminPracticeLU.isExpired("RelationshipTypeID",expiredDays))&&(AdminPracticeLU.isExpiredCheck("RelationshipTypeID",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((AdminPracticeLU.isWrite("RelationshipTypeID",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>RelationshipTypeID&nbsp;</b></p></td><td valign=top><p><select   name="RelationshipTypeID" ><jsp:include page="../generic/tAdminPracticeRelationshipTypeLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=AdminPracticeLU.getRelationshipTypeID()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=RelationshipTypeID&amp;sTableName=tAdminPracticeLU&amp;sRefID=<%=AdminPracticeLU.getLookupID()%>&amp;sFieldNameDisp=<%=AdminPracticeLU.getEnglish("RelationshipTypeID")%>&amp;sTableNameDisp=tAdminPracticeLU','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((AdminPracticeLU.isRead("RelationshipTypeID",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>RelationshipTypeID&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tAdminPracticeRelationshipTypeLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=AdminPracticeLU.getRelationshipTypeID()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=RelationshipTypeID&amp;sTableName=tAdminPracticeLU&amp;sRefID=<%=AdminPracticeLU.getLookupID()%>&amp;sFieldNameDisp=<%=AdminPracticeLU.getEnglish("RelationshipTypeID")%>&amp;sTableNameDisp=tAdminPracticeLU','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <tr><td width=40%>&nbsp;</td><td width=60%>&nbsp;</td></tr>
            </table>
        <%if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {%>
            <input type=hidden name=routePageReference value="sParentReturnPage">
             <%
              if (request.getParameter("INTNext")!=null&&request.getParameter("INTNext").equalsIgnoreCase("yes") ) 
              {
              %>
                  <table width=75% border=1 bordercolor=333333 align=left cellspacing=0 cellpadding=0 class=wizardTable>
                  <tr class=requiredField><td>
                  <input  <%=HTMLFormStyleButton%> type="radio" value="next" name="INTNext" checked>&nbsp;&nbsp;<%=ConfigurationMessages.getInterviewMessage("INTERVIEWNoMore","tAdminPracticeLU")%>
                  <br>
                  <input  <%=HTMLFormStyleButton%> type="radio" value="yes" name="INTNext">&nbsp;&nbsp;<%=ConfigurationMessages.getInterviewMessage("INTERVIEWAddMore","tAdminPracticeLU")%>
                  </td></tr></table><br><br><br>
              <%
              }
              %>
            <p><input <%=HTMLFormStyleButton%> onClick = "this.disabled=true;submit();" type=Submit value="Continue" name=Submit></p>
        <%}%>
        </td></tr></table>
        </form>
        <%
  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>

    </td></tr></table>


