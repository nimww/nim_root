<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.PLCUtils,com.winstaff.bltPracticeMaster,com.winstaff.bltPhysicianMaster,com.winstaff.searchDB2,java.util.Vector,java.util.Enumeration,com.winstaff.ListElement,com.winstaff.bltAdminMaster,com.winstaff.bltCompanyAdminLU,com.winstaff.bltCompanyAdminLU_List_LU_CompanyID" %>
<%/*
    filename: out\jsp\tCompanyAdminLU_main_AdminMaster_CompanyID.jsp
    JSP AutoGen on Mar/26/2002
    Type: n-n main class file
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>

<%
//String thePLCID = (String)pageControllerHash.get("plcID");
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_PU.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>
<table cellpadding=0 cellspacing=0 border=0 width=500>	
  <tr> 
    <td width=10>&nbsp;</td>
    <td> <span class=title>Admin LookUP</span>
    <br>
      <%
//initial declaration of list class and parentID
    Integer        iCompanyID        =    null;
    boolean accessValid = false;
    if (pageControllerHash.containsKey("iCompanyID")) 
    {
        iCompanyID        =    (Integer)pageControllerHash.get("iCompanyID");
        accessValid = true;
    }
  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);
      java.text.SimpleDateFormat displayDateLongSDF = new java.text.SimpleDateFormat("MMMM dd yyyy");
    bltCompanyAdminLU_List_LU_CompanyID        bltCompanyAdminLU_List_LU_CompanyID        =    new    bltCompanyAdminLU_List_LU_CompanyID(iCompanyID);
int whichOne = 0;

if (request.getParameter("which")!=null)
{
    whichOne = Integer.parseInt(request.getParameter("which"));
}
%>
    <form name=fload>
    <select style="font-family: Arial; font-size: 10px" name=sload onChange="javascript:document.location=document.fload.sload.value;">
    <option value=Physician_and_Practice_via_CompanyID.jsp?which=0>Admin</option>
    </select><a href=# onClick="javascript:window.close()">close</a>
    </form>

<%

if (whichOne==0)
{
//if Admin
//declaration of Enumeration
    bltCompanyAdminLU        working_bltCompanyAdminLU;
    ListElement         leCurrentElement;
    java.util.Enumeration eList = bltCompanyAdminLU_List_LU_CompanyID.elements();

        String theClass = "tdHeader";
        %>
      <p class=<%=theClass%>> 
        <%String theClassF = "textBase";%>
      </p>
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr  class=tdHeader> 
          <td>ID</td>
          <td><b>Create Date</b></td>
          <td><b>Name</b></td>
          <td><b>City</b></td>
          <td><b>State</b></td>
          <td> 
            <p class=<%=theClassF%> ><b>ZIP</b></p>
          </td>
          <td>&nbsp;</td>
        </tr>
<%
    while (eList.hasMoreElements())
    {
        leCurrentElement    = (ListElement) eList.nextElement();
        working_bltCompanyAdminLU  = (bltCompanyAdminLU) leCurrentElement.getObject();
        bltAdminMaster working_bltAdminMaster  = new bltAdminMaster(working_bltCompanyAdminLU.getAdminID());
%>
        <tr> 
          <td><%=working_bltCompanyAdminLU.getAdminID()%></td>
          <td><%=displayDateSDF1.format(working_bltAdminMaster.getUniqueCreateDate())%></td>
          <td><%=working_bltAdminMaster.getName()%></td>
          <td><%=working_bltAdminMaster.getCity()%></td>
          <td> 
            <jsp:include page="../generic/tStateLILong_translate.jsp" flush="true" > 
            <jsp:param name="CurrentSelection" value="<%=working_bltAdminMaster.getStateID()%>" />
            </jsp:include>
          </td>
          <td> <%=working_bltAdminMaster.getZIP()%></td>
          <td> 
            <%if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
	        {%>
    	        <a class=linkBase href = "#" onClick="javascript:opener.tAdminLU_form1.AdminID.value=<%=working_bltCompanyAdminLU.getAdminID()%>;window.close();"><img border=0 src="ui_<%=thePLCID%>/icons/add_CompanyID.gif"></a> 
            <%}%>
     <%}%>
          </td>
        </tr>
      </table>
      <p> 
        <%
}


  }
  else
  {
   response.sendRedirect("/errorhandler.jsp");
  }
%>
      </p>
      </td>
  </tr>
</table>
<%String bnIncludeFN = "ui_"+thePLCID+"\\bot-nav_PU.jsp";
%>
<jsp:include page="<%=bnIncludeFN%>" flush="true" > 
<jsp:param name="plcID" value="<%=thePLCID%>"/>
</jsp:include>
