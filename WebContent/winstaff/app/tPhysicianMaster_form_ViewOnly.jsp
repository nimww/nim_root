<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.bltDocumentManagement,java.util.Vector,java.util.Enumeration,com.winstaff.ListElement, com.winstaff.PLCUtils, com.winstaff.SecurityCheck, com.winstaff.ConfigurationMessages,com.winstaff.bltPhysicianMaster" %>
<%/*

    filename: out\jsp\tPhysicianMaster_form.jsp
    Created on Mar/11/2003
    Created by: Scott Ellis
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>

<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_Clear.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>
<table cellpadding=0 cellspacing=0 border=0 width=<%=MasterTableWidth%> >
  <tr> 
    <td width=10>&nbsp;</td>
    <td>
      <%
//initial declaration of list class and parentID
    Integer        iPhysicianID        =    null;
    boolean accessValid = false;
    // required for Type2
    String sKeyMasterReference = null;

   Integer iSecurityCheck = SecurityCheck.CheckItem("PractitionerSection1", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {
    if (pageControllerHash.containsKey("iPhysicianID")) 
    {
        iPhysicianID        =    (Integer)pageControllerHash.get("iPhysicianID");
        accessValid = true;    }
  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);      java.text.SimpleDateFormat displayDateTimeSDF = new java.text.SimpleDateFormat("MM/dd/yyyy hh:mm a");

    pageControllerHash.put("sLocalChildReturnPage","tCoveringPhysicians_main_CoveringPhysicians_PhysicianID.jsp");
    pageControllerHash.put("sParentReturnPage","tCoveringPhysicians_main_CoveringPhysicians_PhysicianID.jsp");
    session.setAttribute("pageControllerHash",pageControllerHash);

//initial declaration of list class and parentID

    bltPhysicianMaster        PhysicianMaster        =    null;

if (request.getParameter("Edit")==null)
{
        PhysicianMaster        =    new    bltPhysicianMaster(iPhysicianID,UserSecurityGroupID);
}
else
{
    if ( request.getParameter( "EDIT" ).equalsIgnoreCase("edit") )
    {
        PhysicianMaster        =    new    bltPhysicianMaster(iPhysicianID,UserSecurityGroupID);
    }
    else if ( request.getParameter( "EDIT" ).equalsIgnoreCase("new") )
    {
        PhysicianMaster        =    new    bltPhysicianMaster(UserSecurityGroupID, true);
    }
}

//fields
        %>
      <%@ include file="tPhysicianMaster_form_instructions.jsp" %>
        <%
    if ( request.getParameter( "EDIT" ) != null )
    {
    %>
        <input type="hidden" name="EDIT" value = "<%=request.getParameter( "EDIT" )%>" >
        <%
    }
%>
        <%  String theClass ="tdBase";%>
        <table width=100% border=1 bordercolor=#333333 cellpadding=3 cellspacing=0 class=tableBase>
          <tr> 
            <td class=tableColor> 
              <table cellpadding=1 cellspacing=0 width=100%>
                <tr> 
                  <td class=title colspan=2>Personal Information:</td>
                </tr>
                <tr> 
                  <td class=title colspan=2>&nbsp; </td>
                </tr>
                <%
            if ( (PhysicianMaster.isRequired("Salutation",UserSecurityGroupID))&&(!PhysicianMaster.isComplete("Salutation")) )
            {
                theClass = "requiredField";
            }
            else if ((PhysicianMaster.isExpired("Salutation",expiredDays))&&(PhysicianMaster.isExpiredCheck("Salutation",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if (false&&(PhysicianMaster.isWrite("Salutation",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Salutation&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p> 
                      <select   name="Salutation" >
                        <jsp:include page="../generic/tSalutationLIShort.jsp" flush="true" > 
                        <jsp:param name="CurrentSelection" value="<%=PhysicianMaster.getSalutation()%>" />
                        </jsp:include>
                      </select>
                      &nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Salutation&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("Salutation")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((PhysicianMaster.isRead("Salutation",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Salutation&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p> 
                      <jsp:include page="../generic/tSalutationLIShort_translate.jsp" flush="true" > 
                      <jsp:param name="CurrentSelection" value="<%=PhysicianMaster.getSalutation()%>" />
                      </jsp:include>
                      &nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Salutation&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("Salutation")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (PhysicianMaster.isRequired("Title",UserSecurityGroupID))&&(!PhysicianMaster.isComplete("Title")) )
            {
                theClass = "requiredField";
            }
            else if ((PhysicianMaster.isExpired("Title",expiredDays))&&(PhysicianMaster.isExpiredCheck("Title",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if (false&&(PhysicianMaster.isWrite("Title",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Title/Degree&nbsp;&nbsp;(M.D. or 
                      R.N., etc.) </b></p>
                  </td>
                  <td valign=top> 
                    <p> 
                      <input maxlength="50" type=text size="80" name="Title" value="<%=PhysicianMaster.getTitle()%>">
                      &nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Title&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("Title")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((PhysicianMaster.isRead("Title",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Title/Degree&nbsp;&nbsp;(M.D. or 
                      R.N., etc.) </b></p>
                  </td>
                  <td valign=top> 
                    <p><%=PhysicianMaster.getTitle()%>&nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Title&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("Title")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (PhysicianMaster.isRequired("FirstName",UserSecurityGroupID))&&(!PhysicianMaster.isComplete("FirstName")) )
            {
                theClass = "requiredField";
            }
            else if ((PhysicianMaster.isExpired("FirstName",expiredDays))&&(PhysicianMaster.isExpiredCheck("FirstName",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if (false&&(PhysicianMaster.isWrite("FirstName",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>First Name&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p> 
                      <input maxlength="50" type=text size="80" name="FirstName" value="<%=PhysicianMaster.getFirstName()%>">
                      &nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=FirstName&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("FirstName")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((PhysicianMaster.isRead("FirstName",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>First Name&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p><%=PhysicianMaster.getFirstName()%>&nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=FirstName&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("FirstName")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (PhysicianMaster.isRequired("MiddleName",UserSecurityGroupID))&&(!PhysicianMaster.isComplete("MiddleName")) )
            {
                theClass = "requiredField";
            }
            else if ((PhysicianMaster.isExpired("MiddleName",expiredDays))&&(PhysicianMaster.isExpiredCheck("MiddleName",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if (false&&(PhysicianMaster.isWrite("MiddleName",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Middle Name&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p> 
                      <input maxlength="50" type=text size="80" name="MiddleName" value="<%=PhysicianMaster.getMiddleName()%>">
                      &nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=MiddleName&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("MiddleName")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((PhysicianMaster.isRead("MiddleName",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Middle Name&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p><%=PhysicianMaster.getMiddleName()%>&nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=MiddleName&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("MiddleName")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (PhysicianMaster.isRequired("LastName",UserSecurityGroupID))&&(!PhysicianMaster.isComplete("LastName")) )
            {
                theClass = "requiredField";
            }
            else if ((PhysicianMaster.isExpired("LastName",expiredDays))&&(PhysicianMaster.isExpiredCheck("LastName",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if (false&&(PhysicianMaster.isWrite("LastName",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Last Name&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p> 
                      <input maxlength="50" type=text size="80" name="LastName" value="<%=PhysicianMaster.getLastName()%>">
                      &nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=LastName&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("LastName")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((PhysicianMaster.isRead("LastName",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Last Name&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p><%=PhysicianMaster.getLastName()%>&nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=LastName&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("LastName")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (PhysicianMaster.isRequired("Suffix",UserSecurityGroupID))&&(!PhysicianMaster.isComplete("Suffix")) )
            {
                theClass = "requiredField";
            }
            else if ((PhysicianMaster.isExpired("Suffix",expiredDays))&&(PhysicianMaster.isExpiredCheck("Suffix",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if (false&&(PhysicianMaster.isWrite("Suffix",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Suffix&nbsp;(Jr., III, etc.)</b></p>
                  </td>
                  <td valign=top> 
                    <p> 
                      <input maxlength="50" type=text size="80" name="Suffix" value="<%=PhysicianMaster.getSuffix()%>">
                      &nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Suffix&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("Suffix")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((PhysicianMaster.isRead("Suffix",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Suffix&nbsp;(Jr., III, etc.)</b></p>
                  </td>
                  <td valign=top> 
                    <p><%=PhysicianMaster.getSuffix()%>&nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Suffix&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("Suffix")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (PhysicianMaster.isRequired("Gender",UserSecurityGroupID))&&(!PhysicianMaster.isComplete("Gender")) )
            {
                theClass = "requiredField";
            }
            else if ((PhysicianMaster.isExpired("Gender",expiredDays))&&(PhysicianMaster.isExpiredCheck("Gender",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if (false&&(PhysicianMaster.isWrite("Gender",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Gender&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p> 
                      <select   name="Gender" >
                        <jsp:include page="../generic/tGenderLILong.jsp" flush="true" > 
                        <jsp:param name="CurrentSelection" value="<%=PhysicianMaster.getGender()%>" />
                        </jsp:include>
                      </select>
                      &nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Gender&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("Gender")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((PhysicianMaster.isRead("Gender",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Gender&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p> 
                      <jsp:include page="../generic/tGenderLILong_translate.jsp" flush="true" > 
                      <jsp:param name="CurrentSelection" value="<%=PhysicianMaster.getGender()%>" />
                      </jsp:include>
                      &nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Gender&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("Gender")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (PhysicianMaster.isRequired("SSN",UserSecurityGroupID))&&(!PhysicianMaster.isComplete("SSN")) )
            {
                theClass = "requiredField";
            }
            else if ((PhysicianMaster.isExpired("SSN",expiredDays))&&(PhysicianMaster.isExpiredCheck("SSN",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if (false&&(PhysicianMaster.isWrite("SSN",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Social Security Number&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p> 
                      <input maxlength="50" type=text size="80" name="SSN" value="<%=PhysicianMaster.getSSN()%>">
                      &nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SSN&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("SSN")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((PhysicianMaster.isRead("SSN",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Social Security Number&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p><%=PhysicianMaster.getSSN()%>&nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SSN&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("SSN")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (PhysicianMaster.isRequired("DateOfBirth",UserSecurityGroupID))&&(!PhysicianMaster.isComplete("DateOfBirth")) )
            {
                theClass = "requiredField";
            }
            else if ((PhysicianMaster.isExpired("DateOfBirth",expiredDays))&&(PhysicianMaster.isExpiredCheck("DateOfBirth",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if (false&&(PhysicianMaster.isWrite("DateOfBirth",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Date of Birth&nbsp;(mm/dd/yyyy):&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p> 
                      <input maxlength=20  type=text size="80" name="DateOfBirth" value='<jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(PhysicianMaster.getDateOfBirth())%>" /></jsp:include>' >
                      &nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=DateOfBirth&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("DateOfBirth")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((PhysicianMaster.isRead("DateOfBirth",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Date of Birth&nbsp;(mm/dd/yyyy):&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p> 
                      <jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" > 
                      <jsp:param name="CurrentSelection" value="<%=dbdf.format(PhysicianMaster.getDateOfBirth())%>" />
                      </jsp:include>
                      &nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=DateOfBirth&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("DateOfBirth")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (PhysicianMaster.isRequired("PlaceOfBirth",UserSecurityGroupID))&&(!PhysicianMaster.isComplete("PlaceOfBirth")) )
            {
                theClass = "requiredField";
            }
            else if ((PhysicianMaster.isExpired("PlaceOfBirth",expiredDays))&&(PhysicianMaster.isExpiredCheck("PlaceOfBirth",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if (false&&(PhysicianMaster.isWrite("PlaceOfBirth",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Place of Birth&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p> 
                      <input maxlength="100" type=text size="80" name="PlaceOfBirth" value="<%=PhysicianMaster.getPlaceOfBirth()%>">
                      &nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PlaceOfBirth&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("PlaceOfBirth")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((PhysicianMaster.isRead("PlaceOfBirth",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Place of Birth&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p><%=PhysicianMaster.getPlaceOfBirth()%>&nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PlaceOfBirth&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("PlaceOfBirth")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (PhysicianMaster.isRequired("HomeEmail",UserSecurityGroupID))&&(!PhysicianMaster.isComplete("HomeEmail")) )
            {
                theClass = "requiredField";
            }
            else if ((PhysicianMaster.isExpired("HomeEmail",expiredDays))&&(PhysicianMaster.isExpiredCheck("HomeEmail",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if (false&&(PhysicianMaster.isWrite("HomeEmail",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Email Address&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p> 
                      <input maxlength="75" type=text size="80" name="HomeEmail" value="<%=PhysicianMaster.getHomeEmail()%>">
                      &nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=HomeEmail&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("HomeEmail")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((PhysicianMaster.isRead("HomeEmail",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Email Address&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p><%=PhysicianMaster.getHomeEmail()%>&nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=HomeEmail&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("HomeEmail")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <tr> 
                  <td class=title colspan=2> 
                    <hr noshade>
                  </td>
                </tr>
                <tr> 
                  <td class=title colspan=2>Home Address:</td>
                </tr>
                <tr> 
                  <td class=title colspan=2>&nbsp; </td>
                </tr>
                <%
            if ( (PhysicianMaster.isRequired("HomeAddress1",UserSecurityGroupID))&&(!PhysicianMaster.isComplete("HomeAddress1")) )
            {
                theClass = "requiredField";
            }
            else if ((PhysicianMaster.isExpired("HomeAddress1",expiredDays))&&(PhysicianMaster.isExpiredCheck("HomeAddress1",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if (false&&(PhysicianMaster.isWrite("HomeAddress1",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Address&nbsp;1</b></p>
                  </td>
                  <td valign=top> 
                    <p> 
                      <input maxlength="50" type=text size="80" name="HomeAddress1" value="<%=PhysicianMaster.getHomeAddress1()%>">
                      &nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=HomeAddress1&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("HomeAddress1")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((PhysicianMaster.isRead("HomeAddress1",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Address&nbsp;1</b></p>
                  </td>
                  <td valign=top> 
                    <p><%=PhysicianMaster.getHomeAddress1()%>&nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=HomeAddress1&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("HomeAddress1")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (PhysicianMaster.isRequired("HomeAddress2",UserSecurityGroupID))&&(!PhysicianMaster.isComplete("HomeAddress2")) )
            {
                theClass = "requiredField";
            }
            else if ((PhysicianMaster.isExpired("HomeAddress2",expiredDays))&&(PhysicianMaster.isExpiredCheck("HomeAddress2",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if (false&&(PhysicianMaster.isWrite("HomeAddress2",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Address 2&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p> 
                      <input maxlength="20" type=text size="80" name="HomeAddress2" value="<%=PhysicianMaster.getHomeAddress2()%>">
                      &nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=HomeAddress2&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("HomeAddress2")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((PhysicianMaster.isRead("HomeAddress2",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Address 2&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p><%=PhysicianMaster.getHomeAddress2()%>&nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=HomeAddress2&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("HomeAddress2")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (PhysicianMaster.isRequired("HomeCity",UserSecurityGroupID))&&(!PhysicianMaster.isComplete("HomeCity")) )
            {
                theClass = "requiredField";
            }
            else if ((PhysicianMaster.isExpired("HomeCity",expiredDays))&&(PhysicianMaster.isExpiredCheck("HomeCity",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if (false&&(PhysicianMaster.isWrite("HomeCity",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>City/Town</b></p>
                  </td>
                  <td valign=top> 
                    <p> 
                      <input maxlength="30" type=text size="80" name="HomeCity" value="<%=PhysicianMaster.getHomeCity()%>">
                      &nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=HomeCity&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("HomeCity")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((PhysicianMaster.isRead("HomeCity",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>City/Town</b></p>
                  </td>
                  <td valign=top> 
                    <p><%=PhysicianMaster.getHomeCity()%>&nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=HomeCity&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("HomeCity")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (PhysicianMaster.isRequired("HomeStateID",UserSecurityGroupID))&&(!PhysicianMaster.isComplete("HomeStateID")) )
            {
                theClass = "requiredField";
            }
            else if ((PhysicianMaster.isExpired("HomeStateID",expiredDays))&&(PhysicianMaster.isExpiredCheck("HomeStateID",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if (false&&(PhysicianMaster.isWrite("HomeStateID",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>State&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p> 
                      <select   name="HomeStateID" >
                        <jsp:include page="../generic/tStateLILong.jsp" flush="true" > 
                        <jsp:param name="CurrentSelection" value="<%=PhysicianMaster.getHomeStateID()%>" />
                        </jsp:include>
                      </select>
                      &nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=HomeStateID&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("HomeStateID")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((PhysicianMaster.isRead("HomeStateID",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>State&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p> 
                      <jsp:include page="../generic/tStateLILong_translate.jsp" flush="true" > 
                      <jsp:param name="CurrentSelection" value="<%=PhysicianMaster.getHomeStateID()%>" />
                      </jsp:include>
                      &nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=HomeStateID&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("HomeStateID")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (PhysicianMaster.isRequired("HomeProvince",UserSecurityGroupID))&&(!PhysicianMaster.isComplete("HomeProvince")) )
            {
                theClass = "requiredField";
            }
            else if ((PhysicianMaster.isExpired("HomeProvince",expiredDays))&&(PhysicianMaster.isExpiredCheck("HomeProvince",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if (false&&(PhysicianMaster.isWrite("HomeProvince",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Province, District, State&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p> 
                      <input maxlength="100" type=text size="80" name="HomeProvince" value="<%=PhysicianMaster.getHomeProvince()%>">
                      &nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=HomeProvince&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("HomeProvince")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((PhysicianMaster.isRead("HomeProvince",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Province, District, State&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p><%=PhysicianMaster.getHomeProvince()%>&nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=HomeProvince&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("HomeProvince")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (PhysicianMaster.isRequired("HomeZIP",UserSecurityGroupID))&&(!PhysicianMaster.isComplete("HomeZIP")) )
            {
                theClass = "requiredField";
            }
            else if ((PhysicianMaster.isExpired("HomeZIP",expiredDays))&&(PhysicianMaster.isExpiredCheck("HomeZIP",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if (false&&(PhysicianMaster.isWrite("HomeZIP",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>ZIP&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p> 
                      <input maxlength="50" type=text size="80" name="HomeZIP" value="<%=PhysicianMaster.getHomeZIP()%>">
                      &nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=HomeZIP&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("HomeZIP")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((PhysicianMaster.isRead("HomeZIP",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>ZIP&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p><%=PhysicianMaster.getHomeZIP()%>&nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=HomeZIP&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("HomeZIP")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (PhysicianMaster.isRequired("HomeCountryID",UserSecurityGroupID))&&(!PhysicianMaster.isComplete("HomeCountryID")) )
            {
                theClass = "requiredField";
            }
            else if ((PhysicianMaster.isExpired("HomeCountryID",expiredDays))&&(PhysicianMaster.isExpiredCheck("HomeCountryID",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if (false&&(PhysicianMaster.isWrite("HomeCountryID",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Country&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p> 
                      <select   name="HomeCountryID" >
                        <jsp:include page="../generic/tCountryLILong.jsp" flush="true" > 
                        <jsp:param name="CurrentSelection" value="<%=PhysicianMaster.getHomeCountryID()%>" />
                        </jsp:include>
                      </select>
                      &nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=HomeCountryID&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("HomeCountryID")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((PhysicianMaster.isRead("HomeCountryID",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Country&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p> 
                      <jsp:include page="../generic/tCountryLILong_translate.jsp" flush="true" > 
                      <jsp:param name="CurrentSelection" value="<%=PhysicianMaster.getHomeCountryID()%>" />
                      </jsp:include>
                      &nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=HomeCountryID&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("HomeCountryID")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (PhysicianMaster.isRequired("HomePhone",UserSecurityGroupID))&&(!PhysicianMaster.isComplete("HomePhone")) )
            {
                theClass = "requiredField";
            }
            else if ((PhysicianMaster.isExpired("HomePhone",expiredDays))&&(PhysicianMaster.isExpiredCheck("HomePhone",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if (false&&(PhysicianMaster.isWrite("HomePhone",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Home Phone Number (XXX-XXX-XXXX)&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p> 
                      <input maxlength="50" type=text size="80" name="HomePhone" value="<%=PhysicianMaster.getHomePhone()%>">
                      &nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=HomePhone&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("HomePhone")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((PhysicianMaster.isRead("HomePhone",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Home Phone Number (XXX-XXX-XXXX)&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p><%=PhysicianMaster.getHomePhone()%>&nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=HomePhone&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("HomePhone")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (PhysicianMaster.isRequired("HomeFax",UserSecurityGroupID))&&(!PhysicianMaster.isComplete("HomeFax")) )
            {
                theClass = "requiredField";
            }
            else if ((PhysicianMaster.isExpired("HomeFax",expiredDays))&&(PhysicianMaster.isExpiredCheck("HomeFax",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if (false&&(PhysicianMaster.isWrite("HomeFax",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Home Fax (XXX-XXX-XXXX)&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p> 
                      <input maxlength="50" type=text size="80" name="HomeFax" value="<%=PhysicianMaster.getHomeFax()%>">
                      &nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=HomeFax&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("HomeFax")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((PhysicianMaster.isRead("HomeFax",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Home Fax (XXX-XXX-XXXX)&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p><%=PhysicianMaster.getHomeFax()%>&nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=HomeFax&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("HomeFax")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (PhysicianMaster.isRequired("HomeMobile",UserSecurityGroupID))&&(!PhysicianMaster.isComplete("HomeMobile")) )
            {
                theClass = "requiredField";
            }
            else if ((PhysicianMaster.isExpired("HomeMobile",expiredDays))&&(PhysicianMaster.isExpiredCheck("HomeMobile",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if (false&&(PhysicianMaster.isWrite("HomeMobile",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Mobile Phone Number (XXX-XXX-XXXX)&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p> 
                      <input maxlength="50" type=text size="80" name="HomeMobile" value="<%=PhysicianMaster.getHomeMobile()%>">
                      &nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=HomeMobile&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("HomeMobile")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((PhysicianMaster.isRead("HomeMobile",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Mobile Phone Number (XXX-XXX-XXXX)&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p><%=PhysicianMaster.getHomeMobile()%>&nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=HomeMobile&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("HomeMobile")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (PhysicianMaster.isRequired("HomePager",UserSecurityGroupID))&&(!PhysicianMaster.isComplete("HomePager")) )
            {
                theClass = "requiredField";
            }
            else if ((PhysicianMaster.isExpired("HomePager",expiredDays))&&(PhysicianMaster.isExpiredCheck("HomePager",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if (false&&(PhysicianMaster.isWrite("HomePager",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Pager Number (XXX-XXX-XXXX)&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p> 
                      <input maxlength="50" type=text size="80" name="HomePager" value="<%=PhysicianMaster.getHomePager()%>">
                      &nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=HomePager&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("HomePager")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((PhysicianMaster.isRead("HomePager",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Pager Number (XXX-XXX-XXXX)&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    
                  <p><%=PhysicianMaster.getHomePager()%>&nbsp; 
                    <%if (isShowAudit){%>
                    <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=HomePager&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("HomePager")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                    <%}%>
                  </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (PhysicianMaster.isRequired("AlertEmail",UserSecurityGroupID))&&(!PhysicianMaster.isComplete("AlertEmail")) )
            {
                theClass = "requiredField";
            }
            else if ((PhysicianMaster.isExpired("AlertEmail",expiredDays))&&(PhysicianMaster.isExpiredCheck("AlertEmail",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if (false&&(PhysicianMaster.isWrite("AlertEmail",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Alert Email&nbsp;(used to notify 
                      you of pending expirations)</b></p>
                  </td>
                  <td valign=top> 
                    <p> 
                      <input maxlength="50" type=text size="80" name="AlertEmail" value="<%=PhysicianMaster.getAlertEmail()%>">
                      &nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=AlertEmail&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("AlertEmail")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((PhysicianMaster.isRead("AlertEmail",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Alert Email&nbsp;(used to notify 
                      you of pending expirations)</b></p>
                  </td>
                  <td valign=top> 
                    <p><%=PhysicianMaster.getAlertEmail()%>&nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=AlertEmail&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("AlertEmail")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (PhysicianMaster.isRequired("AlertDays",UserSecurityGroupID))&&(!PhysicianMaster.isComplete("AlertDays")) )
            {
                theClass = "requiredField";
            }
            else if ((PhysicianMaster.isExpired("AlertDays",expiredDays))&&(PhysicianMaster.isExpiredCheck("AlertDays",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if (false&&(PhysicianMaster.isWrite("AlertDays",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Alert Days&nbsp;(# of days prior 
                      to expiration for alert email)</b></p>
                  </td>
                  <td valign=top> 
                    <p> 
                      <input maxlength="20" type=text size="80" name="AlertDays" value="<%=PhysicianMaster.getAlertDays()%>">
                      &nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=AlertDays&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("AlertDays")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((PhysicianMaster.isRead("AlertDays",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Alert Days&nbsp;(# of days prior 
                      to expiration for alert email)</b></p>
                  </td>
                  <td valign=top> 
                    <p><%=PhysicianMaster.getAlertDays()%>&nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=AlertDays&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("AlertDays")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <tr> 
                  <td class=title colspan=2> 
                    <hr noshade>
                  </td>
                </tr>
                <tr> 
                  <td class=title colspan=2>Citizenship:</td>
                </tr>
                <tr> 
                  <td class=title colspan=2>&nbsp; </td>
                </tr>
                <%
            if ( (PhysicianMaster.isRequired("CitizenshipYN",UserSecurityGroupID))&&(!PhysicianMaster.isComplete("CitizenshipYN")) )
            {
                theClass = "requiredField";
            }
            else if ((PhysicianMaster.isExpired("CitizenshipYN",expiredDays))&&(PhysicianMaster.isExpiredCheck("CitizenshipYN",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if (false&&(PhysicianMaster.isWrite("CitizenshipYN",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Are you a US Citizen?&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p> 
                      <select   name="CitizenshipYN" >
                        <jsp:include page="../generic/tYesNoLILong.jsp" flush="true" > 
                        <jsp:param name="CurrentSelection" value="<%=PhysicianMaster.getCitizenshipYN()%>" />
                        </jsp:include>
                      </select>
                      &nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CitizenshipYN&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("CitizenshipYN")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((PhysicianMaster.isRead("CitizenshipYN",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Are you a US Citizen?&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p> 
                      <jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" > 
                      <jsp:param name="CurrentSelection" value="<%=PhysicianMaster.getCitizenshipYN()%>" />
                      </jsp:include>
                      &nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CitizenshipYN&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("CitizenshipYN")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (PhysicianMaster.isRequired("Citizenship",UserSecurityGroupID))&&(!PhysicianMaster.isComplete("Citizenship")) )
            {
                theClass = "requiredField";
            }
            else if ((PhysicianMaster.isExpired("Citizenship",expiredDays))&&(PhysicianMaster.isExpiredCheck("Citizenship",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <tr> 
                  <td colspan=2> 
                    <table border=2 bordercolor="#333333" cellpadding="3" cellspacing="0">
                      <tr class=tdHeader> 
                        <td colspan=2 bgcolor="#999999"> If not US citizen, please 
                          fill out the following:</td>
                      </tr>
                      <tr> 
                        <td bgcolor=#999999> 
                          <table border=0 width="100%" cellpadding="0" cellspacing="0">
                            <%
            if (false&&(PhysicianMaster.isWrite("Citizenship",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p class=<%=theClass%> ><b>List your citizenship:&nbsp;</b></p>
                              </td>
                              <td valign=top> 
                                <p> 
                                  <input maxlength="100" type=text size="60" name="Citizenship" value="<%=PhysicianMaster.getCitizenship()%>">
                                  &nbsp; 
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Citizenship&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("Citizenship")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else if ((PhysicianMaster.isRead("Citizenship",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p class=<%=theClass%> ><b>List your citizenship:&nbsp;</b></p>
                              </td>
                              <td valign=top> 
                                <p><%=PhysicianMaster.getCitizenship()%>&nbsp; 
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Citizenship&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("Citizenship")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else
            {
                        %>
                            <%
            }
            %>
                            <%
            if ( (PhysicianMaster.isRequired("CurrentVisaTemporary",UserSecurityGroupID))&&(!PhysicianMaster.isComplete("CurrentVisaTemporary")) )
            {
                theClass = "requiredField";
            }
            else if ((PhysicianMaster.isExpired("CurrentVisaTemporary",expiredDays))&&(PhysicianMaster.isExpiredCheck("CurrentVisaTemporary",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                            <%
            if (false&&(PhysicianMaster.isWrite("CurrentVisaTemporary",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p class=<%=theClass%> ><b>Are you currently in 
                                  the US on a Temporary Visa (i.e., J-1, H-1, 
                                  F-1)?&nbsp;</b></p>
                              </td>
                              <td valign=top> 
                                <p> 
                                  <select   name="CurrentVisaTemporary" >
                                    <jsp:include page="../generic/tYesNoLILong.jsp" flush="true" > 
                                    <jsp:param name="CurrentSelection" value="<%=PhysicianMaster.getCurrentVisaTemporary()%>" />
                                    </jsp:include>
                                  </select>
                                  &nbsp; 
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CurrentVisaTemporary&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("CurrentVisaTemporary")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else if ((PhysicianMaster.isRead("CurrentVisaTemporary",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p class=<%=theClass%> ><b>Are you currently in 
                                  the US on a Temporary Visa (i.e., J-1, H-1, 
                                  F-1)?&nbsp;</b></p>
                              </td>
                              <td valign=top> 
                                <p> 
                                  <jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" > 
                                  <jsp:param name="CurrentSelection" value="<%=PhysicianMaster.getCurrentVisaTemporary()%>" />
                                  </jsp:include>
                                  &nbsp; 
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CurrentVisaTemporary&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("CurrentVisaTemporary")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else
            {
                        %>
                            <%
            }
            %>
                            <%
            if ( (PhysicianMaster.isRequired("VisaNumber",UserSecurityGroupID))&&(!PhysicianMaster.isComplete("VisaNumber")) )
            {
                theClass = "requiredField";
            }
            else if ((PhysicianMaster.isExpired("VisaNumber",expiredDays))&&(PhysicianMaster.isExpiredCheck("VisaNumber",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                            <%
            if (false&&(PhysicianMaster.isWrite("VisaNumber",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p class=<%=theClass%> ><b>Visa Document Number&nbsp;</b></p>
                              </td>
                              <td valign=top> 
                                <p> 
                                  <input maxlength="100" type=text size="60" name="VisaNumber" value="<%=PhysicianMaster.getVisaNumber()%>">
                                  &nbsp; 
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=VisaNumber&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("VisaNumber")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else if ((PhysicianMaster.isRead("VisaNumber",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p class=<%=theClass%> ><b>Visa Document Number&nbsp;</b></p>
                              </td>
                              <td valign=top> 
                                <p><%=PhysicianMaster.getVisaNumber()%>&nbsp; 
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=VisaNumber&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("VisaNumber")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else
            {
                        %>
                            <%
            }
            %>
                            <%
            if ( (PhysicianMaster.isRequired("VisaStatus",UserSecurityGroupID))&&(!PhysicianMaster.isComplete("VisaStatus")) )
            {
                theClass = "requiredField";
            }
            else if ((PhysicianMaster.isExpired("VisaStatus",expiredDays))&&(PhysicianMaster.isExpiredCheck("VisaStatus",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                            <%
            if (false&&(PhysicianMaster.isWrite("VisaStatus",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p class=<%=theClass%> ><b>Visa Status&nbsp;</b></p>
                              </td>
                              <td valign=top> 
                                <p>
                                  <input maxlength="100" type=text size="60" name="VisaStatus" value="<%=PhysicianMaster.getVisaStatus()%>">

 &nbsp; 
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=VisaStatus&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("VisaStatus")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else if ((PhysicianMaster.isRead("VisaStatus",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p class=<%=theClass%> ><b>Visa Status&nbsp;</b></p>
                              </td>
                              <td valign=top> 
                                <p><%=PhysicianMaster.getVisaStatus()%>&nbsp; 
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=VisaStatus&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("VisaStatus")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else
            {
                        %>
                            <%
            }
            %>
                            <%
            if ( (PhysicianMaster.isRequired("VisaSponsor",UserSecurityGroupID))&&(!PhysicianMaster.isComplete("VisaSponsor")) )
            {
                theClass = "requiredField";
            }
            else if ((PhysicianMaster.isExpired("VisaSponsor",expiredDays))&&(PhysicianMaster.isExpiredCheck("VisaSponsor",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                            <%
            if (false&&(PhysicianMaster.isWrite("VisaSponsor",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p class=<%=theClass%> ><b>Visa Sponsor&nbsp;</b></p>
                              </td>
                              <td valign=top> 
                                <p> 
                                  <input maxlength="100" type=text size="60" name="VisaSponsor" value="<%=PhysicianMaster.getVisaSponsor()%>">
                                  &nbsp; 
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=VisaSponsor&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("VisaSponsor")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else if ((PhysicianMaster.isRead("VisaSponsor",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p class=<%=theClass%> ><b>Visa Sponsor&nbsp;</b></p>
                              </td>
                              <td valign=top> 
                                <p><%=PhysicianMaster.getVisaSponsor()%>&nbsp; 
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=VisaSponsor&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("VisaSponsor")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else
            {
                        %>
                            <%
            }
            %>
                            <%
            if ( (PhysicianMaster.isRequired("VisaExpiration",UserSecurityGroupID))&&(!PhysicianMaster.isComplete("VisaExpiration")) )
            {
                theClass = "requiredField";
            }
            else if ((PhysicianMaster.isExpired("VisaExpiration",expiredDays))&&(PhysicianMaster.isExpiredCheck("VisaExpiration",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                            <%
            if (false&&(PhysicianMaster.isWrite("VisaExpiration",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p class=<%=theClass%> ><b>Visa Expiration&nbsp;(mm/dd/yyyy):&nbsp;</b></p>
                              </td>
                              <td valign=top> 
                                <p> 
                                  <input maxlength=10  type=text size="60" name="VisaExpiration" value='<jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(PhysicianMaster.getVisaExpiration())%>" /></jsp:include>' >
                                  &nbsp; 
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=VisaExpiration&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("VisaExpiration")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else if ((PhysicianMaster.isRead("VisaExpiration",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p class=<%=theClass%> ><b>Visa Expiration&nbsp;(mm/dd/yyyy):&nbsp;</b></p>
                              </td>
                              <td valign=top> 
                                <p> 
                                  <jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" > 
                                  <jsp:param name="CurrentSelection" value="<%=dbdf.format(PhysicianMaster.getVisaExpiration())%>" />
                                  </jsp:include>
                                  &nbsp; 
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=VisaExpiration&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("VisaExpiration")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else
            {
                        %>
                            <%
            }
            %>
                            <%
            if ( (PhysicianMaster.isRequired("CurrentVisaExtended",UserSecurityGroupID))&&(!PhysicianMaster.isComplete("CurrentVisaExtended")) )
            {
                theClass = "requiredField";
            }
            else if ((PhysicianMaster.isExpired("CurrentVisaExtended",expiredDays))&&(PhysicianMaster.isExpiredCheck("CurrentVisaExtended",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                            <%
            if (false&&(PhysicianMaster.isWrite("CurrentVisaExtended",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p class=<%=theClass%> ><b>Is your visa being 
                                  extended to cover period of appointment?&nbsp;</b></p>
                              </td>
                              <td valign=top> 
                                <p> 
                                  <select   name="CurrentVisaExtended" >
                                    <jsp:include page="../generic/tYesNoLILong.jsp" flush="true" > 
                                    <jsp:param name="CurrentSelection" value="<%=PhysicianMaster.getCurrentVisaExtended()%>" />
                                    </jsp:include>
                                  </select>
                                  &nbsp; 
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CurrentVisaExtended&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("CurrentVisaExtended")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else if ((PhysicianMaster.isRead("CurrentVisaExtended",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p class=<%=theClass%> ><b>Is your visa being 
                                  extended to cover period of appointment?&nbsp;</b></p>
                              </td>
                              <td valign=top> 
                                <p> 
                                  <jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" > 
                                  <jsp:param name="CurrentSelection" value="<%=PhysicianMaster.getCurrentVisaExtended()%>" />
                                  </jsp:include>
                                  &nbsp; 
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CurrentVisaExtended&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("CurrentVisaExtended")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else
            {
                        %>
                            <%
            }
            %>
                            <%
            if ( (PhysicianMaster.isRequired("CountryOfIssueID",UserSecurityGroupID))&&(!PhysicianMaster.isComplete("CountryOfIssueID")) )
            {
                theClass = "requiredField";
            }
            else if ((PhysicianMaster.isExpired("CountryOfIssueID",expiredDays))&&(PhysicianMaster.isExpiredCheck("CountryOfIssueID",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                            <%
            if (false&&(PhysicianMaster.isWrite("CountryOfIssueID",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p class=<%=theClass%> ><b>Country of Issue&nbsp;</b></p>
                              </td>
                              <td valign=top> 
                                <p> 
                                  <select   name="CountryOfIssueID" >
                                    <jsp:include page="../generic/tCountryLILong.jsp" flush="true" > 
                                    <jsp:param name="CurrentSelection" value="<%=PhysicianMaster.getCountryOfIssueID()%>" />
                                    </jsp:include>
                                  </select>
                                  &nbsp; 
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CountryOfIssueID&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("CountryOfIssueID")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else if ((PhysicianMaster.isRead("CountryOfIssueID",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p class=<%=theClass%> ><b>Country of Issue&nbsp;</b></p>
                              </td>
                              <td valign=top> 
                                <p> 
                                  <jsp:include page="../generic/tCountryLILong_translate.jsp" flush="true" > 
                                  <jsp:param name="CurrentSelection" value="<%=PhysicianMaster.getCountryOfIssueID()%>" />
                                  </jsp:include>
                                  &nbsp; 
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CountryOfIssueID&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("CountryOfIssueID")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else
            {
                        %>
                            <%
            }
            %>
                            <%
            if ( (PhysicianMaster.isRequired("HaveGreencard",UserSecurityGroupID))&&(!PhysicianMaster.isComplete("HaveGreencard")) )
            {
                theClass = "requiredField";
            }
            else if ((PhysicianMaster.isExpired("HaveGreencard",expiredDays))&&(PhysicianMaster.isExpiredCheck("HaveGreencard",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                            <%
            if (false&&(PhysicianMaster.isWrite("HaveGreencard",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p class=<%=theClass%> ><b>Do you hold permanent 
                                  immigrant status in the United States?&nbsp;</b></p>
                              </td>
                              <td valign=top> 
                                <p> 
                                  <select   name="HaveGreencard" >
                                    <jsp:include page="../generic/tYesNoLILong.jsp" flush="true" > 
                                    <jsp:param name="CurrentSelection" value="<%=PhysicianMaster.getHaveGreencard()%>" />
                                    </jsp:include>
                                  </select>
                                  &nbsp; 
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=HaveGreencard&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("HaveGreencard")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else if ((PhysicianMaster.isRead("HaveGreencard",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p class=<%=theClass%> ><b>Do you hold permanent 
                                  immigrant status in the United States?&nbsp;</b></p>
                              </td>
                              <td valign=top> 
                                <p> 
                                  <jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" > 
                                  <jsp:param name="CurrentSelection" value="<%=PhysicianMaster.getHaveGreencard()%>" />
                                  </jsp:include>
                                  &nbsp; 
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=HaveGreencard&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("HaveGreencard")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else
            {
                        %>
                            <%
            }
            %>
                            <%
            if ( (PhysicianMaster.isRequired("VisaTemporary5Years",UserSecurityGroupID))&&(!PhysicianMaster.isComplete("VisaTemporary5Years")) )
            {
                theClass = "requiredField";
            }
            else if ((PhysicianMaster.isExpired("VisaTemporary5Years",expiredDays))&&(PhysicianMaster.isExpiredCheck("VisaTemporary5Years",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                            <%
            if ( (PhysicianMaster.isRequired("EligibleToWorkInUS",UserSecurityGroupID))&&(!PhysicianMaster.isComplete("EligibleToWorkInUS")) )
            {
                theClass = "requiredField";
            }
            else if ((PhysicianMaster.isExpired("EligibleToWorkInUS",expiredDays))&&(PhysicianMaster.isExpiredCheck("EligibleToWorkInUS",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                            <%
            if (false&&(PhysicianMaster.isWrite("EligibleToWorkInUS",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p class=<%=theClass%> ><b>Are you eligible to 
                                  work in the US?&nbsp;</b></p>
                              </td>
                              <td valign=top> 
                                <p> 
                                  <select   name="EligibleToWorkInUS" >
                                    <jsp:include page="../generic/tYesNoLILong.jsp" flush="true" > 
                                    <jsp:param name="CurrentSelection" value="<%=PhysicianMaster.getEligibleToWorkInUS()%>" />
                                    </jsp:include>
                                  </select>
                                  &nbsp; 
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=EligibleToWorkInUS&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("EligibleToWorkInUS")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else if ((PhysicianMaster.isRead("EligibleToWorkInUS",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                
                              <p class=<%=theClass%> ><b>Are you eligible to work 
                                in the US?&nbsp; </b></p>
                              </td>
                              <td valign=top> 
                                <p> 
                                  <jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" > 
                                  <jsp:param name="CurrentSelection" value="<%=PhysicianMaster.getEligibleToWorkInUS()%>" />
                                  </jsp:include>
                                  &nbsp; 
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=EligibleToWorkInUS&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("EligibleToWorkInUS")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else
            {
                        %>
                            <%
            }
            %>
                            <%
            if ( (PhysicianMaster.isRequired("VisaTemporary5Years",UserSecurityGroupID))&&(!PhysicianMaster.isComplete("VisaTemporary5Years")) )
            {
                theClass = "requiredField";
            }
            else if ((PhysicianMaster.isExpired("VisaTemporary5Years",expiredDays))&&(PhysicianMaster.isExpiredCheck("VisaTemporary5Years",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                            <%
            if (false&&(PhysicianMaster.isWrite("VisaTemporary5Years",UserSecurityGroupID)))
            {
                        %>
                            <tr>
                              <td valign=top> 
                                <p class=<%=theClass%> ><b>If not currently in 
                                  the US, have you been in the US on a temporary 
                                  visa within the past 5 years?<br>
                                  <br>
                                  If yes, please fill out the following: &nbsp;&nbsp;</b></p>
                              </td>
                              <td valign=top>
                                <p>
                                  <select   name="VisaTemporary5Years" >
                                    <jsp:include page="../generic/tYesNoLILong.jsp" flush="true" >
                                    <jsp:param name="CurrentSelection" value="<%=PhysicianMaster.getVisaTemporary5Years()%>" />
                                    </jsp:include>
                                  </select>
                                  &nbsp;
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=VisaTemporary5Years&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("VisaTemporary5Years")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else if ((PhysicianMaster.isRead("VisaTemporary5Years",UserSecurityGroupID)))
            {
                        %>
                            <tr>
                              <td valign=top> 
                                <p class=<%=theClass%> ><b>If not currently in 
                                  the US, have you been in the US on a temporary 
                                  visa within the past 5 years?<br>
                                  <br>
                                  If yes, please fill out the following: &nbsp;&nbsp;</b></p>
                              </td>
                              <td valign=top>
                                <p>
                                  <jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" >
                                  <jsp:param name="CurrentSelection" value="<%=PhysicianMaster.getVisaTemporary5Years()%>" />
                                  </jsp:include>
                                  &nbsp;
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=VisaTemporary5Years&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("VisaTemporary5Years")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else
            {
                        %>
                            <%
            }
            %>
                            <tr>
                              <td colspan=2> 
                                <table border=0 bgcolor="#FFFFFF" cellpadding="2" cellspacing="0" width="75%">
                                  <tr class=tdHeader> 
                                    <td width="25">&nbsp;</td>
                                    <td>Past Visa 1</td>
                                  </tr>
                                  <%
            if ( (PhysicianMaster.isRequired("PastVisa1DateFrom",UserSecurityGroupID))&&(!PhysicianMaster.isComplete("PastVisa1DateFrom")) )
            {
                theClass = "requiredField";
            }
            else if ((PhysicianMaster.isExpired("PastVisa1DateFrom",expiredDays))&&(PhysicianMaster.isExpiredCheck("PastVisa1DateFrom",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                                  <%
            if (false&&(PhysicianMaster.isWrite("PastVisa1DateFrom",UserSecurityGroupID)))
            {
                        %>
                                  <tr> 
                                    <td valign=top width="25">&nbsp;</td>
                                    <td valign=top> 
                                      <p class=<%=theClass%> ><b>From&nbsp;(mm/dd/yyyy):&nbsp;</b></p>
                                    </td>
                                    <td valign=top> 
                                      <p> 
                                        <input maxlength=10  type=text size="50" name="PastVisa1DateFrom" value='<jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(PhysicianMaster.getPastVisa1DateFrom())%>" /></jsp:include>' >
                                        &nbsp; 
                                        <%if (isShowAudit){%>
                                        <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PastVisa1DateFrom&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("PastVisa1DateFrom")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                        <%}%>
                                      </p>
                                    </td>
                                  </tr>
                                  <%
            }
            else if ((PhysicianMaster.isRead("PastVisa1DateFrom",UserSecurityGroupID)))
            {
                        %>
                                  <tr> 
                                    <td valign=top width="25">&nbsp;</td>
                                    <td valign=top> 
                                      <p class=<%=theClass%> ><b>From&nbsp;(mm/dd/yyyy):&nbsp;</b></p>
                                    </td>
                                    <td valign=top> 
                                      <p> 
                                        <jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" > 
                                        <jsp:param name="CurrentSelection" value="<%=dbdf.format(PhysicianMaster.getPastVisa1DateFrom())%>" />
                                        </jsp:include>
                                        &nbsp; 
                                        <%if (isShowAudit){%>
                                        <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PastVisa1DateFrom&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("PastVisa1DateFrom")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                        <%}%>
                                      </p>
                                    </td>
                                  </tr>
                                  <%
            }
            else
            {
                        %>
                                  <%
            }
            %>
                                  <%
            if ( (PhysicianMaster.isRequired("PastVisa1DateTo",UserSecurityGroupID))&&(!PhysicianMaster.isComplete("PastVisa1DateTo")) )
            {
                theClass = "requiredField";
            }
            else if ((PhysicianMaster.isExpired("PastVisa1DateTo",expiredDays))&&(PhysicianMaster.isExpiredCheck("PastVisa1DateTo",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                                  <%
            if (false&&(PhysicianMaster.isWrite("PastVisa1DateTo",UserSecurityGroupID)))
            {
                        %>
                                  <tr> 
                                    <td valign=top width="25">&nbsp;</td>
                                    <td valign=top> 
                                      <p class=<%=theClass%> ><b>To&nbsp;(mm/dd/yyyy):&nbsp;</b></p>
                                    </td>
                                    <td valign=top> 
                                      <p> 
                                        <input maxlength=10  type=text size="50" name="PastVisa1DateTo" value='<jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(PhysicianMaster.getPastVisa1DateTo())%>" /></jsp:include>' >
                                        &nbsp; 
                                        <%if (isShowAudit){%>
                                        <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PastVisa1DateTo&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("PastVisa1DateTo")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                        <%}%>
                                      </p>
                                    </td>
                                  </tr>
                                  <%
            }
            else if ((PhysicianMaster.isRead("PastVisa1DateTo",UserSecurityGroupID)))
            {
                        %>
                                  <tr> 
                                    <td valign=top width="25">&nbsp;</td>
                                    <td valign=top> 
                                      <p class=<%=theClass%> ><b>To&nbsp;(mm/dd/yyyy):&nbsp;</b></p>
                                    </td>
                                    <td valign=top> 
                                      <p> 
                                        <jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" > 
                                        <jsp:param name="CurrentSelection" value="<%=dbdf.format(PhysicianMaster.getPastVisa1DateTo())%>" />
                                        </jsp:include>
                                        &nbsp; 
                                        <%if (isShowAudit){%>
                                        <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PastVisa1DateTo&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("PastVisa1DateTo")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                        <%}%>
                                      </p>
                                    </td>
                                  </tr>
                                  <%
            }
            else
            {
                        %>
                                  <%
            }
            %>
                                  <%
            if ( (PhysicianMaster.isRequired("PastVisa1Type",UserSecurityGroupID))&&(!PhysicianMaster.isComplete("PastVisa1Type")) )
            {
                theClass = "requiredField";
            }
            else if ((PhysicianMaster.isExpired("PastVisa1Type",expiredDays))&&(PhysicianMaster.isExpiredCheck("PastVisa1Type",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                                  <%
            if (false&&(PhysicianMaster.isWrite("PastVisa1Type",UserSecurityGroupID)))
            {
                        %>
                                  <tr> 
                                    <td valign=top width="25">&nbsp;</td>
                                    <td valign=top> 
                                      <p class=<%=theClass%> ><b>Type&nbsp;</b></p>
                                    </td>
                                    <td valign=top> 
                                      <p> 
                                        <input maxlength="100" type=text size="50" name="PastVisa1Type" value="<%=PhysicianMaster.getPastVisa1Type()%>">
                                        &nbsp; 
									  
                                        <%if (isShowAudit){%>
                                        <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PastVisa1Type&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("PastVisa1Type")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                        <%}%>
                                      </p>
                                    </td>
                                  </tr>
                                  <%
            }
            else if ((PhysicianMaster.isRead("PastVisa1Type",UserSecurityGroupID)))
            {
                        %>
                                  <tr> 
                                    <td valign=top width="25">&nbsp;</td>
                                    <td valign=top> 
                                      <p class=<%=theClass%> ><b>Type&nbsp;</b></p>
                                    </td>
                                    <td valign=top> 
                                      <p><%=PhysicianMaster.getPastVisa1Type()%>&nbsp; 
                                        <%if (isShowAudit){%>
                                        <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PastVisa1Type&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("PastVisa1Type")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                        <%}%>
                                      </p>
                                    </td>
                                  </tr>
                                  <%
            }
            else
            {
                        %>
                                  <%
            }
            %>
                                  <%
            if ( (PhysicianMaster.isRequired("PastVisa1Sponsor",UserSecurityGroupID))&&(!PhysicianMaster.isComplete("PastVisa1Sponsor")) )
            {
                theClass = "requiredField";
            }
            else if ((PhysicianMaster.isExpired("PastVisa1Sponsor",expiredDays))&&(PhysicianMaster.isExpiredCheck("PastVisa1Sponsor",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                                  <%
            if (false&&(PhysicianMaster.isWrite("PastVisa1Sponsor",UserSecurityGroupID)))
            {
                        %>
                                  <tr> 
                                    <td valign=top width="25">&nbsp;</td>
                                    <td valign=top> 
                                      <p class=<%=theClass%> ><b>Sponsor&nbsp;</b></p>
                                    </td>
                                    <td valign=top> 
                                      <p> 
                                        <input maxlength="100" type=text size="50" name="PastVisa1Sponsor" value="<%=PhysicianMaster.getPastVisa1Sponsor()%>">
                                        &nbsp; 
                                        <%if (isShowAudit){%>
                                        <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PastVisa1Sponsor&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("PastVisa1Sponsor")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                        <%}%>
                                      </p>
                                    </td>
                                  </tr>
                                  <%
            }
            else if ((PhysicianMaster.isRead("PastVisa1Sponsor",UserSecurityGroupID)))
            {
                        %>
                                  <tr> 
                                    <td valign=top width="25">&nbsp;</td>
                                    <td valign=top> 
                                      <p class=<%=theClass%> ><b>Sponsor&nbsp;</b></p>
                                    </td>
                                    <td valign=top> 
                                      <p><%=PhysicianMaster.getPastVisa1Sponsor()%>&nbsp; 
                                        <%if (isShowAudit){%>
                                        <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PastVisa1Sponsor&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("PastVisa1Sponsor")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                        <%}%>
                                      </p>
                                    </td>
                                  </tr>
                                  <%
            }
            else
            {
                        %>
                                  <%
            }
            %>
                                </table>
                              </td>
                            </tr>
                            <tr>
                              <td colspan=2> 
                                <table border=0 bgcolor="#CCCCCC" cellpadding="2" cellspacing="0" width="75%">
                                  <tr class=tdHeader> 
                                    <td width="25">&nbsp;</td>
                                    <td>Past Visa 2</td>
                                  </tr>
                                  <%
            if ( (PhysicianMaster.isRequired("PastVisa2DateFrom",UserSecurityGroupID))&&(!PhysicianMaster.isComplete("PastVisa2DateFrom")) )
            {
                theClass = "requiredField";
            }
            else if ((PhysicianMaster.isExpired("PastVisa2DateFrom",expiredDays))&&(PhysicianMaster.isExpiredCheck("PastVisa2DateFrom",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                                  <%
            if (false&&(PhysicianMaster.isWrite("PastVisa2DateFrom",UserSecurityGroupID)))
            {
                        %>
                                  <tr> 
                                    <td valign=top width="25">&nbsp;</td>
                                    <td valign=top> 
                                      <p class=<%=theClass%> ><b> From&nbsp;(mm/dd/yyyy):&nbsp;</b></p>
                                    </td>
                                    <td valign=top> 
                                      <p> 
                                        <input maxlength=10  type=text size="50" name="PastVisa2DateFrom" value='<jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(PhysicianMaster.getPastVisa2DateFrom())%>" /></jsp:include>' >
                                        &nbsp; 
                                        <%if (isShowAudit){%>
                                        <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PastVisa2DateFrom&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("PastVisa2DateFrom")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                        <%}%>
                                      </p>
                                    </td>
                                  </tr>
                                  <%
            }
            else if ((PhysicianMaster.isRead("PastVisa2DateFrom",UserSecurityGroupID)))
            {
                        %>
                                  <tr> 
                                    <td valign=top width="25">&nbsp;</td>
                                    <td valign=top> 
                                      <p class=<%=theClass%> ><b>From&nbsp;(mm/dd/yyyy):&nbsp;</b></p>
                                    </td>
                                    <td valign=top> 
                                      <p> 
                                        <jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" > 
                                        <jsp:param name="CurrentSelection" value="<%=dbdf.format(PhysicianMaster.getPastVisa2DateFrom())%>" />
                                        </jsp:include>
                                        &nbsp; 
                                        <%if (isShowAudit){%>
                                        <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PastVisa2DateFrom&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("PastVisa2DateFrom")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                        <%}%>
                                      </p>
                                    </td>
                                  </tr>
                                  <%
            }
            else
            {
                        %>
                                  <%
            }
            %>
                                  <%
            if ( (PhysicianMaster.isRequired("PastVisa2DateTo",UserSecurityGroupID))&&(!PhysicianMaster.isComplete("PastVisa2DateTo")) )
            {
                theClass = "requiredField";
            }
            else if ((PhysicianMaster.isExpired("PastVisa2DateTo",expiredDays))&&(PhysicianMaster.isExpiredCheck("PastVisa2DateTo",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                                  <%
            if (false&&(PhysicianMaster.isWrite("PastVisa2DateTo",UserSecurityGroupID)))
            {
                        %>
                                  <tr> 
                                    <td valign=top width="25">&nbsp;</td>
                                    <td valign=top> 
                                      <p class=<%=theClass%> ><b>To&nbsp;(mm/dd/yyyy):&nbsp;</b></p>
                                    </td>
                                    <td valign=top> 
                                      <p> 
                                        <input maxlength=10  type=text size="50" name="PastVisa2DateTo" value='<jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(PhysicianMaster.getPastVisa2DateTo())%>" /></jsp:include>' >
                                        &nbsp; 
                                        <%if (isShowAudit){%>
                                        <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PastVisa2DateTo&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("PastVisa2DateTo")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                        <%}%>
                                      </p>
                                    </td>
                                  </tr>
                                  <%
            }
            else if ((PhysicianMaster.isRead("PastVisa2DateTo",UserSecurityGroupID)))
            {
                        %>
                                  <tr> 
                                    <td valign=top width="25">&nbsp;</td>
                                    <td valign=top> 
                                      <p class=<%=theClass%> ><b>To&nbsp;(mm/dd/yyyy):&nbsp;</b></p>
                                    </td>
                                    <td valign=top> 
                                      <p> 
                                        <jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" > 
                                        <jsp:param name="CurrentSelection" value="<%=dbdf.format(PhysicianMaster.getPastVisa2DateTo())%>" />
                                        </jsp:include>
                                        &nbsp; 
                                        <%if (isShowAudit){%>
                                        <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PastVisa2DateTo&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("PastVisa2DateTo")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                        <%}%>
                                      </p>
                                    </td>
                                  </tr>
                                  <%
            }
            else
            {
                        %>
                                  <%
            }
            %>
                                  <%
            if ( (PhysicianMaster.isRequired("PastVisa2Type",UserSecurityGroupID))&&(!PhysicianMaster.isComplete("PastVisa2Type")) )
            {
                theClass = "requiredField";
            }
            else if ((PhysicianMaster.isExpired("PastVisa2Type",expiredDays))&&(PhysicianMaster.isExpiredCheck("PastVisa2Type",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                                  <%
            if (false&&(PhysicianMaster.isWrite("PastVisa2Type",UserSecurityGroupID)))
            {
                        %>
                                  <tr> 
                                    <td valign=top width="25">&nbsp;</td>
                                    <td valign=top> 
                                      <p class=<%=theClass%> ><b>Type&nbsp;</b></p>
                                    </td>
                                    <td valign=top> 
                                      <p> 
                                        <input maxlength="100" type=text size="50" name="PastVisa2Type" value="<%=PhysicianMaster.getPastVisa2Type()%>">
                                        &nbsp; 
                                        <%if (isShowAudit){%>
                                        <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PastVisa2Type&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("PastVisa2Type")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                        <%}%>
                                      </p>
                                    </td>
                                  </tr>
                                  <%
            }
            else if ((PhysicianMaster.isRead("PastVisa2Type",UserSecurityGroupID)))
            {
                        %>
                                  <tr> 
                                    <td valign=top width="25">&nbsp;</td>
                                    <td valign=top> 
                                      <p class=<%=theClass%> ><b>Type&nbsp;</b></p>
                                    </td>
                                    <td valign=top> 
                                      <p><%=PhysicianMaster.getPastVisa2Type()%>&nbsp; 
                                        <%if (isShowAudit){%>
                                        <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PastVisa2Type&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("PastVisa2Type")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                        <%}%>
                                      </p>
                                    </td>
                                  </tr>
                                  <%
            }
            else
            {
                        %>
                                  <%
            }
            %>
                                  <%
            if ( (PhysicianMaster.isRequired("PastVisa2Sponsor",UserSecurityGroupID))&&(!PhysicianMaster.isComplete("PastVisa2Sponsor")) )
            {
                theClass = "requiredField";
            }
            else if ((PhysicianMaster.isExpired("PastVisa2Sponsor",expiredDays))&&(PhysicianMaster.isExpiredCheck("PastVisa2Sponsor",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                                  <%
            if (false&&(PhysicianMaster.isWrite("PastVisa2Sponsor",UserSecurityGroupID)))
            {
                        %>
                                  <tr> 
                                    <td valign=top width="25">&nbsp;</td>
                                    <td valign=top> 
                                      <p class=<%=theClass%> ><b>Sponsor&nbsp;</b></p>
                                    </td>
                                    <td valign=top> 
                                      <p> 
                                        <input maxlength="100" type=text size="50" name="PastVisa2Sponsor" value="<%=PhysicianMaster.getPastVisa2Sponsor()%>">
                                        &nbsp; 
                                        <%if (isShowAudit){%>
                                        <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PastVisa2Sponsor&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("PastVisa2Sponsor")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                        <%}%>
                                      </p>
                                    </td>
                                  </tr>
                                  <%
            }
            else if ((PhysicianMaster.isRead("PastVisa2Sponsor",UserSecurityGroupID)))
            {
                        %>
                                  <tr> 
                                    <td valign=top width="25">&nbsp;</td>
                                    <td valign=top> 
                                      <p class=<%=theClass%> ><b>Sponsor&nbsp;</b></p>
                                    </td>
                                    <td valign=top> 
                                      <p><%=PhysicianMaster.getPastVisa2Sponsor()%>&nbsp; 
                                        <%if (isShowAudit){%>
                                        <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PastVisa2Sponsor&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("PastVisa2Sponsor")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                        <%}%>
                                      </p>
                                    </td>
                                  </tr>
                                  <%
            }
            else
            {
                        %>
                                  <%
            }
            %>
                                </table>
                             <br></td>
                            </tr>
                          </table>
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>



                <tr> 
                  <td class=title colspan=2> 
                    <hr noshade>
                  </td>
                </tr>
                <tr> 
                  <td class=title colspan=2>Medical Education/License Testing:</td>
                </tr>
                <tr> 
                  <td class=title colspan=2>&nbsp; </td>
                </tr>
                <tr> 
                  <td class=instructions colspan=2>Please enter the dates you passed each step of the USMLE.  If you did not pass certain steps, please leave the date blank.</td>
                </tr>


            <%
            if ( (PhysicianMaster.isRequired("USMLEDatePassedStep1",UserSecurityGroupID))&&(!PhysicianMaster.isComplete("USMLEDatePassedStep1")) )
            {
                theClass = "requiredField";
            }
            else if ((PhysicianMaster.isExpired("USMLEDatePassedStep1",expiredDays))&&(PhysicianMaster.isExpiredCheck("USMLEDatePassedStep1",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>

            <%
            if (false&&(PhysicianMaster.isWrite("USMLEDatePassedStep1",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top>
                    <p class=<%=theClass%> ><b>USMLE Step 1&nbsp;(mm/dd/yyyy):&nbsp;</b></p>
                  </td><td valign=top><p><input maxlength=10  type=text size="80" name="USMLEDatePassedStep1" value='<jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(PhysicianMaster.getUSMLEDatePassedStep1())%>" /></jsp:include>' >&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=USMLEDatePassedStep1&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("USMLEDatePassedStep1")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((PhysicianMaster.isRead("USMLEDatePassedStep1",UserSecurityGroupID)))
            {
                        %>
                        <tr>
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>USMLE Step 1&nbsp;(mm/dd/yyyy):&nbsp;</b></p>
                  </td><td valign=top><p><jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(PhysicianMaster.getUSMLEDatePassedStep1())%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=USMLEDatePassedStep1&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("USMLEDatePassedStep1")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (PhysicianMaster.isRequired("USMLEDatePassedStep2",UserSecurityGroupID))&&(!PhysicianMaster.isComplete("USMLEDatePassedStep2")) )
            {
                theClass = "requiredField";
            }
            else if ((PhysicianMaster.isExpired("USMLEDatePassedStep2",expiredDays))&&(PhysicianMaster.isExpiredCheck("USMLEDatePassedStep2",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>

            <%
            if (false&&(PhysicianMaster.isWrite("USMLEDatePassedStep2",UserSecurityGroupID)))
            {
                        %>
                        <tr>
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>USMLE Step 2&nbsp;(mm/dd/yyyy):&nbsp;</b></p>
                  </td><td valign=top><p><input maxlength=10  type=text size="80" name="USMLEDatePassedStep2" value='<jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(PhysicianMaster.getUSMLEDatePassedStep2())%>" /></jsp:include>' >&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=USMLEDatePassedStep2&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("USMLEDatePassedStep2")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((PhysicianMaster.isRead("USMLEDatePassedStep2",UserSecurityGroupID)))
            {
                        %>
                        <tr>
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>USMLE Step 2&nbsp;(mm/dd/yyyy):&nbsp;</b></p>
                  </td><td valign=top><p><jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(PhysicianMaster.getUSMLEDatePassedStep2())%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=USMLEDatePassedStep2&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("USMLEDatePassedStep2")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (PhysicianMaster.isRequired("USMLEDatePassedStep3",UserSecurityGroupID))&&(!PhysicianMaster.isComplete("USMLEDatePassedStep3")) )
            {
                theClass = "requiredField";
            }
            else if ((PhysicianMaster.isExpired("USMLEDatePassedStep3",expiredDays))&&(PhysicianMaster.isExpiredCheck("USMLEDatePassedStep3",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>

            <%
            if (false&&(PhysicianMaster.isWrite("USMLEDatePassedStep3",UserSecurityGroupID)))
            {
                        %>
                        <tr>
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>USMLE Step 3&nbsp;(mm/dd/yyyy):&nbsp;</b></p>
                  </td><td valign=top><p><input maxlength=10  type=text size="80" name="USMLEDatePassedStep3" value='<jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(PhysicianMaster.getUSMLEDatePassedStep3())%>" /></jsp:include>' >&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=USMLEDatePassedStep3&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("USMLEDatePassedStep3")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((PhysicianMaster.isRead("USMLEDatePassedStep3",UserSecurityGroupID)))
            {
                        %>
                        <tr>
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>USMLE Step 3&nbsp;(mm/dd/yyyy):&nbsp;</b></p>
                  </td><td valign=top><p><jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(PhysicianMaster.getUSMLEDatePassedStep3())%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=USMLEDatePassedStep3&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("USMLEDatePassedStep3")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>





                <tr> 
                  <td colspan=2> 
                    <table border=2 bordercolor="#333333" cellpadding="3" cellspacing="0" width="100%">
                      <tr class=tdHeader> 
                        <td colspan=2 bgcolor="#999999"> If you are a Foreign 
                          Medical Graduate, please fill out the following (if 
                          applicable):</td>
                      </tr>
                      <tr> 
                        <td bgcolor=#999999> 
                          <table border=0 width="100%" cellpadding="0" cellspacing="0">


                <%
            if ( (PhysicianMaster.isRequired("ECFMGNo",UserSecurityGroupID))&&(!PhysicianMaster.isComplete("ECFMGNo")) )
            {
                theClass = "requiredField";
            }
            else if ((PhysicianMaster.isExpired("ECFMGNo",expiredDays))&&(PhysicianMaster.isExpiredCheck("ECFMGNo",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if (false&&(PhysicianMaster.isWrite("ECFMGNo",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>ECFMG Number (if applicable)&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p> 
                      <input maxlength="100" type=text size="80" name="ECFMGNo" value="<%=PhysicianMaster.getECFMGNo()%>">
                      &nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ECFMGNo&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("ECFMGNo")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((PhysicianMaster.isRead("ECFMGNo",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>ECFMG Number (if applicable)&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p><%=PhysicianMaster.getECFMGNo()%>&nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ECFMGNo&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("ECFMGNo")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (PhysicianMaster.isRequired("ECFMGDateIssued",UserSecurityGroupID))&&(!PhysicianMaster.isComplete("ECFMGDateIssued")) )
            {
                theClass = "requiredField";
            }
            else if ((PhysicianMaster.isExpired("ECFMGDateIssued",expiredDays))&&(PhysicianMaster.isExpiredCheck("ECFMGDateIssued",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if (false&&(PhysicianMaster.isWrite("ECFMGDateIssued",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>ECFMG Issue Date&nbsp;(mm/dd/yyyy):&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p> 
                      <input maxlength=20  type=text size="80" name="ECFMGDateIssued" value='<jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(PhysicianMaster.getECFMGDateIssued())%>" /></jsp:include>' >
                      &nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ECFMGDateIssued&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("ECFMGDateIssued")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((PhysicianMaster.isRead("ECFMGDateIssued",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>ECFMG Issue Date&nbsp;(mm/dd/yyyy):&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p> 
                      <jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" > 
                      <jsp:param name="CurrentSelection" value="<%=dbdf.format(PhysicianMaster.getECFMGDateIssued())%>" />
                      </jsp:include>
                      &nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ECFMGDateIssued&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("ECFMGDateIssued")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (PhysicianMaster.isRequired("ECFMGDateExpires",UserSecurityGroupID))&&(!PhysicianMaster.isComplete("ECFMGDateExpires")) )
            {
                theClass = "requiredField";
            }
            else if ((PhysicianMaster.isExpired("ECFMGDateExpires",expiredDays))&&(PhysicianMaster.isExpiredCheck("ECFMGDateExpires",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if (false&&(PhysicianMaster.isWrite("ECFMGDateExpires",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                              <td valign=top> 
                                <p class=<%=theClass%> ><b>ECFMG Expiration Date 
                                  (if applicable)&nbsp;&nbsp;(mm/dd/yyyy):&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p> 
                      <input maxlength=20  type=text size="80" name="ECFMGDateExpires" value='<jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(PhysicianMaster.getECFMGDateExpires())%>" /></jsp:include>' >
                      &nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ECFMGDateExpires&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("ECFMGDateExpires")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((PhysicianMaster.isRead("ECFMGDateExpires",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                              <td valign=top> 
                                <p class=<%=theClass%> ><b>ECFMG Expiration Date&nbsp; 
                                  (if applicable)&nbsp;(mm/dd/yyyy):&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p> 
                      <jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" > 
                      <jsp:param name="CurrentSelection" value="<%=dbdf.format(PhysicianMaster.getECFMGDateExpires())%>" />
                      </jsp:include>
                      &nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ECFMGDateExpires&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("ECFMGDateExpires")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>


            <%
            if ( (PhysicianMaster.isRequired("HaveFLEX",UserSecurityGroupID))&&(!PhysicianMaster.isComplete("HaveFLEX")) )
            {
                theClass = "requiredField";
            }
            else if ((PhysicianMaster.isExpired("HaveFLEX",expiredDays))&&(PhysicianMaster.isExpiredCheck("HaveFLEX",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if (false&&(PhysicianMaster.isWrite("HaveFLEX",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>FLEX?&nbsp;</b></p></td><td valign=top><p><select   name="HaveFLEX" ><jsp:include page="../generic/tYesNoLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PhysicianMaster.getHaveFLEX()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=HaveFLEX&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("HaveFLEX")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((PhysicianMaster.isRead("HaveFLEX",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>FLEX?&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PhysicianMaster.getHaveFLEX()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=HaveFLEX&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("HaveFLEX")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (PhysicianMaster.isRequired("FLEXDatePassed",UserSecurityGroupID))&&(!PhysicianMaster.isComplete("FLEXDatePassed")) )
            {
                theClass = "requiredField";
            }
            else if ((PhysicianMaster.isExpired("FLEXDatePassed",expiredDays))&&(PhysicianMaster.isExpiredCheck("FLEXDatePassed",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>

            <%
            if (false&&(PhysicianMaster.isWrite("FLEXDatePassed",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>FLEX Date Passed&nbsp;(mm/dd/yyyy):&nbsp;</b></p></td><td valign=top><p><input maxlength=10  type=text size="80" name="FLEXDatePassed" value='<jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(PhysicianMaster.getFLEXDatePassed())%>" /></jsp:include>' >&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=FLEXDatePassed&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("FLEXDatePassed")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((PhysicianMaster.isRead("FLEXDatePassed",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>FLEX Date Passed&nbsp;(mm/dd/yyyy):&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(PhysicianMaster.getFLEXDatePassed())%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=FLEXDatePassed&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("FLEXDatePassed")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



                          </table>
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>


                <tr> 
                  <td class=title colspan=2> 
                    <hr noshade>
                  </td>
                </tr>
                <tr> 
                  <td class=title colspan=2>Military Status:</td>
                </tr>
                <tr> 
                  <td class=title colspan=2>&nbsp; </td>
                </tr>
                <%
            if ( (PhysicianMaster.isRequired("MilitaryActive",UserSecurityGroupID))&&(!PhysicianMaster.isComplete("MilitaryActive")) )
            {
                theClass = "requiredField";
            }
            else if ((PhysicianMaster.isExpired("MilitaryActive",expiredDays))&&(PhysicianMaster.isExpiredCheck("MilitaryActive",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if (false&&(PhysicianMaster.isWrite("MilitaryActive",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Are you currently active in the 
                      Military?&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p> 
                      <select   name="MilitaryActive" >
                        <jsp:include page="../generic/tYesNoLILong.jsp" flush="true" > 
                        <jsp:param name="CurrentSelection" value="<%=PhysicianMaster.getMilitaryActive()%>" />
                        </jsp:include>
                      </select>
                      &nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=MilitaryActive&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("MilitaryActive")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((PhysicianMaster.isRead("MilitaryActive",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Are you currently active in the 
                      Military?&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p> 
                      <jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" > 
                      <jsp:param name="CurrentSelection" value="<%=PhysicianMaster.getMilitaryActive()%>" />
                      </jsp:include>
                      &nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=MilitaryActive&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("MilitaryActive")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (PhysicianMaster.isRequired("MilitaryBranch",UserSecurityGroupID))&&(!PhysicianMaster.isComplete("MilitaryBranch")) )
            {
                theClass = "requiredField";
            }
            else if ((PhysicianMaster.isExpired("MilitaryBranch",expiredDays))&&(PhysicianMaster.isExpiredCheck("MilitaryBranch",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if (false&&(PhysicianMaster.isWrite("MilitaryBranch",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>If yes, which branch?&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p> 
                      <input maxlength="40" type=text size="80" name="MilitaryBranch" value="<%=PhysicianMaster.getMilitaryBranch()%>">
                      &nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=MilitaryBranch&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("MilitaryBranch")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((PhysicianMaster.isRead("MilitaryBranch",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>If yes, which branch?&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p><%=PhysicianMaster.getMilitaryBranch()%>&nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=MilitaryBranch&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("MilitaryBranch")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (PhysicianMaster.isRequired("MilitaryRank",UserSecurityGroupID))&&(!PhysicianMaster.isComplete("MilitaryRank")) )
            {
                theClass = "requiredField";
            }
            else if ((PhysicianMaster.isExpired("MilitaryRank",expiredDays))&&(PhysicianMaster.isExpiredCheck("MilitaryRank",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if (false&&(PhysicianMaster.isWrite("MilitaryRank",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Military Rank&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <input maxlength="100" type=text size="80" name="MilitaryRank" value="<%=PhysicianMaster.getMilitaryRank()%>">
                      &nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=MilitaryRank&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("MilitaryRank")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((PhysicianMaster.isRead("MilitaryRank",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Military Rank&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p><%=PhysicianMaster.getMilitaryRank()%>&nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=MilitaryRank&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("MilitaryRank")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (PhysicianMaster.isRequired("MilitaryDutyStatus",UserSecurityGroupID))&&(!PhysicianMaster.isComplete("MilitaryDutyStatus")) )
            {
                theClass = "requiredField";
            }
            else if ((PhysicianMaster.isExpired("MilitaryDutyStatus",expiredDays))&&(PhysicianMaster.isExpiredCheck("MilitaryDutyStatus",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if (false&&(PhysicianMaster.isWrite("MilitaryDutyStatus",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Military Duty Status&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <input maxlength="100" type=text size="80" name="MilitaryDutyStatus" value="<%=PhysicianMaster.getMilitaryDutyStatus()%>">
                      &nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=MilitaryDutyStatus&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("MilitaryDutyStatus")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((PhysicianMaster.isRead("MilitaryDutyStatus",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Military Duty Status&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p><%=PhysicianMaster.getMilitaryDutyStatus()%>&nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=MilitaryDutyStatus&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("MilitaryDutyStatus")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (PhysicianMaster.isRequired("MilitaryCurrentAssignment",UserSecurityGroupID))&&(!PhysicianMaster.isComplete("MilitaryCurrentAssignment")) )
            {
                theClass = "requiredField";
            }
            else if ((PhysicianMaster.isExpired("MilitaryCurrentAssignment",expiredDays))&&(PhysicianMaster.isExpiredCheck("MilitaryCurrentAssignment",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if (false&&(PhysicianMaster.isWrite("MilitaryCurrentAssignment",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b><%=PhysicianMaster.getEnglish("MilitaryCurrentAssignment")%>&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <textarea onKeyDown="textAreaStop(this,200)" rows="2" name="MilitaryCurrentAssignment" cols="40" maxlength=200><%=PhysicianMaster.getMilitaryCurrentAssignment()%></textarea>
                      &nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=MilitaryCurrentAssignment&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("MilitaryCurrentAssignment")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((PhysicianMaster.isRead("MilitaryCurrentAssignment",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top> 
                    <p class=<%=theClass%> ><b><%=PhysicianMaster.getEnglish("MilitaryCurrentAssignment")%>&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p><%=PhysicianMaster.getMilitaryCurrentAssignment()%>&nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=MilitaryCurrentAssignment&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("MilitaryCurrentAssignment")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (PhysicianMaster.isRequired("MilitaryReserve",UserSecurityGroupID))&&(!PhysicianMaster.isComplete("MilitaryReserve")) )
            {
                theClass = "requiredField";
            }
            else if ((PhysicianMaster.isExpired("MilitaryReserve",expiredDays))&&(PhysicianMaster.isExpiredCheck("MilitaryReserve",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if (false&&(PhysicianMaster.isWrite("MilitaryReserve",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Are you currently in the Military 
                      Reserves?&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p> 
                      <select   name="MilitaryReserve" >
                        <jsp:include page="../generic/tYesNoLILong.jsp" flush="true" > 
                        <jsp:param name="CurrentSelection" value="<%=PhysicianMaster.getMilitaryReserve()%>" />
                        </jsp:include>
                      </select>
                      &nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=MilitaryReserve&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("MilitaryReserve")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((PhysicianMaster.isRead("MilitaryReserve",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Are you currently in the Military 
                      Reserves?&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p> 
                      <jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" > 
                      <jsp:param name="CurrentSelection" value="<%=PhysicianMaster.getMilitaryReserve()%>" />
                      </jsp:include>
                      &nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=MilitaryReserve&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("MilitaryReserve")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <tr> 
                  <td class=title colspan=2> 
                    <hr noshade>
                  </td>
                </tr>
                <tr> 
                  <td class=title colspan=2>Additional Information:</td>
                </tr>
                <tr> 
                  <td class=title colspan=2>&nbsp; </td>
                </tr>
                <%
            if ( (PhysicianMaster.isRequired("OtherName1",UserSecurityGroupID))&&(!PhysicianMaster.isComplete("OtherName1")) )
            {
                theClass = "requiredField";
            }
            else if ((PhysicianMaster.isExpired("OtherName1",expiredDays))&&(PhysicianMaster.isExpiredCheck("OtherName1",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if (false&&(PhysicianMaster.isWrite("OtherName1",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Other Name Used #1&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p> 
                      <input maxlength="100" type=text size="80" name="OtherName1" value="<%=PhysicianMaster.getOtherName1()%>">
                      &nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OtherName1&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("OtherName1")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((PhysicianMaster.isRead("OtherName1",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Other Name Used #1&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p><%=PhysicianMaster.getOtherName1()%>&nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OtherName1&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("OtherName1")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (PhysicianMaster.isRequired("OtherName1Start",UserSecurityGroupID))&&(!PhysicianMaster.isComplete("OtherName1Start")) )
            {
                theClass = "requiredField";
            }
            else if ((PhysicianMaster.isExpired("OtherName1Start",expiredDays))&&(PhysicianMaster.isExpiredCheck("OtherName1Start",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if (false&&(PhysicianMaster.isWrite("OtherName1Start",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Other name Used #1 From Date&nbsp;(mm/dd/yyyy):&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p> 
                      <input maxlength=20  type=text size="80" name="OtherName1Start" value='<jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(PhysicianMaster.getOtherName1Start())%>" /></jsp:include>' >
                      &nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OtherName1Start&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("OtherName1Start")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((PhysicianMaster.isRead("OtherName1Start",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Other name Used #1 From Date&nbsp;(mm/dd/yyyy):&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p> 
                      <jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" > 
                      <jsp:param name="CurrentSelection" value="<%=dbdf.format(PhysicianMaster.getOtherName1Start())%>" />
                      </jsp:include>
                      &nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OtherName1Start&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("OtherName1Start")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (PhysicianMaster.isRequired("OtherName1End",UserSecurityGroupID))&&(!PhysicianMaster.isComplete("OtherName1End")) )
            {
                theClass = "requiredField";
            }
            else if ((PhysicianMaster.isExpired("OtherName1End",expiredDays))&&(PhysicianMaster.isExpiredCheck("OtherName1End",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if (false&&(PhysicianMaster.isWrite("OtherName1End",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Other name Used #1 To Date&nbsp;(mm/dd/yyyy):&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p> 
                      <input maxlength=20  type=text size="80" name="OtherName1End" value='<jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(PhysicianMaster.getOtherName1End())%>" /></jsp:include>' >
                      &nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OtherName1End&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("OtherName1End")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((PhysicianMaster.isRead("OtherName1End",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Other name Used #1 To Date&nbsp;(mm/dd/yyyy):&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p> 
                      <jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" > 
                      <jsp:param name="CurrentSelection" value="<%=dbdf.format(PhysicianMaster.getOtherName1End())%>" />
                      </jsp:include>
                      &nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OtherName1End&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("OtherName1End")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (PhysicianMaster.isRequired("OtherName2",UserSecurityGroupID))&&(!PhysicianMaster.isComplete("OtherName2")) )
            {
                theClass = "requiredField";
            }
            else if ((PhysicianMaster.isExpired("OtherName2",expiredDays))&&(PhysicianMaster.isExpiredCheck("OtherName2",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if (false&&(PhysicianMaster.isWrite("OtherName2",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Other Name Used #2&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p> 
                      <input maxlength="100" type=text size="80" name="OtherName2" value="<%=PhysicianMaster.getOtherName2()%>">
                      &nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OtherName2&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("OtherName2")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((PhysicianMaster.isRead("OtherName2",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Other Name Used #2&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p><%=PhysicianMaster.getOtherName2()%>&nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OtherName2&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("OtherName2")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (PhysicianMaster.isRequired("OtherName2Start",UserSecurityGroupID))&&(!PhysicianMaster.isComplete("OtherName2Start")) )
            {
                theClass = "requiredField";
            }
            else if ((PhysicianMaster.isExpired("OtherName2Start",expiredDays))&&(PhysicianMaster.isExpiredCheck("OtherName2Start",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if (false&&(PhysicianMaster.isWrite("OtherName2Start",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Other name Used #2 From Date&nbsp;(mm/dd/yyyy):&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p> 
                      <input maxlength=20  type=text size="80" name="OtherName2Start" value='<jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(PhysicianMaster.getOtherName2Start())%>" /></jsp:include>' >
                      &nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OtherName2Start&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("OtherName2Start")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((PhysicianMaster.isRead("OtherName2Start",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Other name Used #2 From Date&nbsp;(mm/dd/yyyy):&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p> 
                      <jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" > 
                      <jsp:param name="CurrentSelection" value="<%=dbdf.format(PhysicianMaster.getOtherName2Start())%>" />
                      </jsp:include>
                      &nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OtherName2Start&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("OtherName2Start")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (PhysicianMaster.isRequired("OtherName2End",UserSecurityGroupID))&&(!PhysicianMaster.isComplete("OtherName2End")) )
            {
                theClass = "requiredField";
            }
            else if ((PhysicianMaster.isExpired("OtherName2End",expiredDays))&&(PhysicianMaster.isExpiredCheck("OtherName2End",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if (false&&(PhysicianMaster.isWrite("OtherName2End",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Other name Used #2 To Date&nbsp;(mm/dd/yyyy):&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p> 
                      <input maxlength=20  type=text size="80" name="OtherName2End" value='<jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(PhysicianMaster.getOtherName2End())%>" /></jsp:include>' >
                      &nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OtherName2End&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("OtherName2End")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((PhysicianMaster.isRead("OtherName2End",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Other name Used #2 To Date&nbsp;(mm/dd/yyyy):&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p> 
                      <jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" > 
                      <jsp:param name="CurrentSelection" value="<%=dbdf.format(PhysicianMaster.getOtherName2End())%>" />
                      </jsp:include>
                      &nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OtherName2End&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("OtherName2End")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (PhysicianMaster.isRequired("Spouse",UserSecurityGroupID))&&(!PhysicianMaster.isComplete("Spouse")) )
            {
                theClass = "requiredField";
            }
            else if ((PhysicianMaster.isExpired("Spouse",expiredDays))&&(PhysicianMaster.isExpiredCheck("Spouse",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if (false&&(PhysicianMaster.isWrite("Spouse",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Spouse Full Name (First Middle 
                      Last)&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p> 
                      <input maxlength="100" type=text size="80" name="Spouse" value="<%=PhysicianMaster.getSpouse()%>">
                      &nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Spouse&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("Spouse")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((PhysicianMaster.isRead("Spouse",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Spouse Full Name (First Middle 
                      Last)&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p><%=PhysicianMaster.getSpouse()%>&nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Spouse&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("Spouse")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (PhysicianMaster.isRequired("HospitalAdmitingPrivileges",UserSecurityGroupID))&&(!PhysicianMaster.isComplete("HospitalAdmitingPrivileges")) )
            {
                theClass = "requiredField";
            }
            else if ((PhysicianMaster.isExpired("HospitalAdmitingPrivileges",expiredDays))&&(PhysicianMaster.isExpiredCheck("HospitalAdmitingPrivileges",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if (false&&(PhysicianMaster.isWrite("HospitalAdmitingPrivileges",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Do you have hospital admitting 
                      privileges?&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p> 
                      <select   name="HospitalAdmitingPrivileges" >
                        <jsp:include page="../generic/tYesNoLILong.jsp" flush="true" > 
                        <jsp:param name="CurrentSelection" value="<%=PhysicianMaster.getHospitalAdmitingPrivileges()%>" />
                        </jsp:include>
                      </select>
                      &nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=HospitalAdmitingPrivileges&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("HospitalAdmitingPrivileges")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((PhysicianMaster.isRead("HospitalAdmitingPrivileges",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Do you have hospital admitting 
                      privileges?&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p> 
                      <jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" > 
                      <jsp:param name="CurrentSelection" value="<%=PhysicianMaster.getHospitalAdmitingPrivileges()%>" />
                      </jsp:include>
                      &nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=HospitalAdmitingPrivileges&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("HospitalAdmitingPrivileges")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (PhysicianMaster.isRequired("HospitalAdmitingPrivilegesNo",UserSecurityGroupID))&&(!PhysicianMaster.isComplete("HospitalAdmitingPrivilegesNo")) )
            {
                theClass = "requiredField";
            }
            else if ((PhysicianMaster.isExpired("HospitalAdmitingPrivilegesNo",expiredDays))&&(PhysicianMaster.isExpiredCheck("HospitalAdmitingPrivilegesNo",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if (false&&(PhysicianMaster.isWrite("HospitalAdmitingPrivilegesNo",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>If no, what admitting arrangements 
                      do you have?&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p> 
                      <textarea onKeyDown="textAreaStop(this,400)" rows="2" name="HospitalAdmitingPrivilegesNo" cols="40" maxlength=400><%=PhysicianMaster.getHospitalAdmitingPrivilegesNo()%></textarea>
                      &nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=HospitalAdmitingPrivilegesNo&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("HospitalAdmitingPrivilegesNo")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((PhysicianMaster.isRead("HospitalAdmitingPrivilegesNo",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b><%=PhysicianMaster.getEnglish("HospitalAdmitingPrivilegesNo")%>&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p><%=PhysicianMaster.getHospitalAdmitingPrivilegesNo()%>&nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=HospitalAdmitingPrivilegesNo&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("HospitalAdmitingPrivilegesNo")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (PhysicianMaster.isRequired("PhysicianCategoryID",UserSecurityGroupID))&&(!PhysicianMaster.isComplete("PhysicianCategoryID")) )
            {
                theClass = "requiredField";
            }
            else if ((PhysicianMaster.isExpired("PhysicianCategoryID",expiredDays))&&(PhysicianMaster.isExpiredCheck("PhysicianCategoryID",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if (false&&(PhysicianMaster.isWrite("PhysicianCategoryID",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Practitioner Category&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p> 
                      <select   name="PhysicianCategoryID" >
                        <jsp:include page="../generic/tPhysicianCategoryLILong.jsp" flush="true" > 
                        <jsp:param name="CurrentSelection" value="<%=PhysicianMaster.getPhysicianCategoryID()%>" />
                        </jsp:include>
                      </select>
                      &nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PhysicianCategoryID&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("PhysicianCategoryID")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((PhysicianMaster.isRead("PhysicianCategoryID",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Practitioner Category&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p> 
                      <jsp:include page="../generic/tPhysicianCategoryLILong_translate.jsp" flush="true" > 
                      <jsp:param name="CurrentSelection" value="<%=PhysicianMaster.getPhysicianCategoryID()%>" />
                      </jsp:include>
                      &nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PhysicianCategoryID&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("PhysicianCategoryID")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (PhysicianMaster.isRequired("IPAMedicalAffiliation",UserSecurityGroupID))&&(!PhysicianMaster.isComplete("IPAMedicalAffiliation")) )
            {
                theClass = "requiredField";
            }
            else if ((PhysicianMaster.isExpired("IPAMedicalAffiliation",expiredDays))&&(PhysicianMaster.isExpiredCheck("IPAMedicalAffiliation",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if (false&&(PhysicianMaster.isWrite("IPAMedicalAffiliation",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Are you currently affiliated with 
                      any other IPAs or Medical Groups?&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p> 
                      <select   name="IPAMedicalAffiliation" >
                        <jsp:include page="../generic/tYesNoLILong.jsp" flush="true" > 
                        <jsp:param name="CurrentSelection" value="<%=PhysicianMaster.getIPAMedicalAffiliation()%>" />
                        </jsp:include>
                      </select>
                      &nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=IPAMedicalAffiliation&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("IPAMedicalAffiliation")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((PhysicianMaster.isRead("IPAMedicalAffiliation",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Are you currently affiliated with 
                      any other IPAs or Medical Groups?&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p> 
                      <jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" > 
                      <jsp:param name="CurrentSelection" value="<%=PhysicianMaster.getIPAMedicalAffiliation()%>" />
                      </jsp:include>
                      &nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=IPAMedicalAffiliation&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("IPAMedicalAffiliation")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (PhysicianMaster.isRequired("AffiliationDesc1",UserSecurityGroupID))&&(!PhysicianMaster.isComplete("AffiliationDesc1")) )
            {
                theClass = "requiredField";
            }
            else if ((PhysicianMaster.isExpired("AffiliationDesc1",expiredDays))&&(PhysicianMaster.isExpiredCheck("AffiliationDesc1",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if (false&&(PhysicianMaster.isWrite("AffiliationDesc1",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b><%=PhysicianMaster.getEnglish("AffiliationDesc1")%>&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p> 
                      <textarea onKeyDown="textAreaStop(this,200)" rows="2" name="AffiliationDesc1" cols="40" maxlength=200><%=PhysicianMaster.getAffiliationDesc1()%></textarea>
                      &nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=AffiliationDesc1&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("AffiliationDesc1")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((PhysicianMaster.isRead("AffiliationDesc1",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b><%=PhysicianMaster.getEnglish("AffiliationDesc1")%>&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p><%=PhysicianMaster.getAffiliationDesc1()%>&nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=AffiliationDesc1&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("AffiliationDesc1")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (PhysicianMaster.isRequired("PhysicianLanguages",UserSecurityGroupID))&&(!PhysicianMaster.isComplete("PhysicianLanguages")) )
            {
                theClass = "requiredField";
            }
            else if ((PhysicianMaster.isExpired("PhysicianLanguages",expiredDays))&&(PhysicianMaster.isExpiredCheck("PhysicianLanguages",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if (false&&(PhysicianMaster.isWrite("PhysicianLanguages",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Please list any languages that 
                      you speak (other than English)&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p> 
                      <input maxlength="100" type=text size="80" name="PhysicianLanguages" value="<%=PhysicianMaster.getPhysicianLanguages()%>">
                      &nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PhysicianLanguages&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("PhysicianLanguages")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((PhysicianMaster.isRead("PhysicianLanguages",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Please list any languages that 
                      you speak (other than English)&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p><%=PhysicianMaster.getPhysicianLanguages()%>&nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PhysicianLanguages&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("PhysicianLanguages")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (PhysicianMaster.isRequired("MedicareUPIN",UserSecurityGroupID))&&(!PhysicianMaster.isComplete("MedicareUPIN")) )
            {
                theClass = "requiredField";
            }
            else if ((PhysicianMaster.isExpired("MedicareUPIN",expiredDays))&&(PhysicianMaster.isExpiredCheck("MedicareUPIN",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if (false&&(PhysicianMaster.isWrite("MedicareUPIN",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Medicare UPIN&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p> 
                      <input maxlength="100" type=text size="80" name="MedicareUPIN" value="<%=PhysicianMaster.getMedicareUPIN()%>">
                      &nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=MedicareUPIN&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("MedicareUPIN")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((PhysicianMaster.isRead("MedicareUPIN",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Medicare UPIN&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p><%=PhysicianMaster.getMedicareUPIN()%>&nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=MedicareUPIN&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("MedicareUPIN")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (PhysicianMaster.isRequired("UniqueNPI",UserSecurityGroupID))&&(!PhysicianMaster.isComplete("UniqueNPI")) )
            {
                theClass = "requiredField";
            }
            else if ((PhysicianMaster.isExpired("UniqueNPI",expiredDays))&&(PhysicianMaster.isExpiredCheck("UniqueNPI",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if (false&&(PhysicianMaster.isWrite("UniqueNPI",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Unique National Provider Number 
                      (NPI) (if applicable)&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p> 
                      <input maxlength="100" type=text size="80" name="UniqueNPI" value="<%=PhysicianMaster.getUniqueNPI()%>">
                      &nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=UniqueNPI&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("UniqueNPI")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((PhysicianMaster.isRead("UniqueNPI",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Unique National Provider Number 
                      (NPI) (if applicable)&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p><%=PhysicianMaster.getUniqueNPI()%>&nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=UniqueNPI&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("UniqueNPI")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (PhysicianMaster.isRequired("MedicareParticipation",UserSecurityGroupID))&&(!PhysicianMaster.isComplete("MedicareParticipation")) )
            {
                theClass = "requiredField";
            }
            else if ((PhysicianMaster.isExpired("MedicareParticipation",expiredDays))&&(PhysicianMaster.isExpiredCheck("MedicareParticipation",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if (false&&(PhysicianMaster.isWrite("MedicareParticipation",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Are you a Participating Medicare 
                      Provider?&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p> 
                      <select   name="MedicareParticipation" >
                        <jsp:include page="../generic/tYesNoLILong.jsp" flush="true" > 
                        <jsp:param name="CurrentSelection" value="<%=PhysicianMaster.getMedicareParticipation()%>" />
                        </jsp:include>
                      </select>
                      &nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=MedicareParticipation&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("MedicareParticipation")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((PhysicianMaster.isRead("MedicareParticipation",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Are you a Participating Medicare 
                      Provider?&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p> 
                      <jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" > 
                      <jsp:param name="CurrentSelection" value="<%=PhysicianMaster.getMedicareParticipation()%>" />
                      </jsp:include>
                      &nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=MedicareParticipation&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("MedicareParticipation")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (PhysicianMaster.isRequired("MedicareNo",UserSecurityGroupID))&&(!PhysicianMaster.isComplete("MedicareNo")) )
            {
                theClass = "requiredField";
            }
            else if ((PhysicianMaster.isExpired("MedicareNo",expiredDays))&&(PhysicianMaster.isExpiredCheck("MedicareNo",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if (false&&(PhysicianMaster.isWrite("MedicareNo",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Medicare Provider Number(s)&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p> 
                      <input maxlength="100" type=text size="80" name="MedicareNo" value="<%=PhysicianMaster.getMedicareNo()%>">
                      &nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=MedicareNo&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("MedicareNo")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((PhysicianMaster.isRead("MedicareNo",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Medicare Provider Number(s)&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p><%=PhysicianMaster.getMedicareNo()%>&nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=MedicareNo&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("MedicareNo")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (PhysicianMaster.isRequired("MediCaidParticipation",UserSecurityGroupID))&&(!PhysicianMaster.isComplete("MediCaidParticipation")) )
            {
                theClass = "requiredField";
            }
            else if ((PhysicianMaster.isExpired("MediCaidParticipation",expiredDays))&&(PhysicianMaster.isExpiredCheck("MediCaidParticipation",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if (false&&(PhysicianMaster.isWrite("MediCaidParticipation",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Are you a Participating Medicaid 
                      Provider?&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p> 
                      <select   name="MediCaidParticipation" >
                        <jsp:include page="../generic/tYesNoLILong.jsp" flush="true" > 
                        <jsp:param name="CurrentSelection" value="<%=PhysicianMaster.getMediCaidParticipation()%>" />
                        </jsp:include>
                      </select>
                      &nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=MediCaidParticipation&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("MediCaidParticipation")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((PhysicianMaster.isRead("MediCaidParticipation",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Are you a Participating Medicaid 
                      Provider?&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p> 
                      <jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" > 
                      <jsp:param name="CurrentSelection" value="<%=PhysicianMaster.getMediCaidParticipation()%>" />
                      </jsp:include>
                      &nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=MediCaidParticipation&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("MediCaidParticipation")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (PhysicianMaster.isRequired("MediCaidNo",UserSecurityGroupID))&&(!PhysicianMaster.isComplete("MediCaidNo")) )
            {
                theClass = "requiredField";
            }
            else if ((PhysicianMaster.isExpired("MediCaidNo",expiredDays))&&(PhysicianMaster.isExpiredCheck("MediCaidNo",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if (false&&(PhysicianMaster.isWrite("MediCaidNo",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Medicaid Provider Number(s)&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p> 
                      <input maxlength="100" type=text size="80" name="MediCaidNo" value="<%=PhysicianMaster.getMediCaidNo()%>">
                      &nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=MediCaidNo&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("MediCaidNo")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((PhysicianMaster.isRead("MediCaidNo",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Medicaid Provider Number(s)&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p><%=PhysicianMaster.getMediCaidNo()%>&nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=MediCaidNo&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("MediCaidNo")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (PhysicianMaster.isRequired("SupplementalIDNumber1",UserSecurityGroupID))&&(!PhysicianMaster.isComplete("SupplementalIDNumber1")) )
            {
                theClass = "requiredField";
            }
            else if ((PhysicianMaster.isExpired("SupplementalIDNumber1",expiredDays))&&(PhysicianMaster.isExpiredCheck("SupplementalIDNumber1",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if (false&&(PhysicianMaster.isWrite("SupplementalIDNumber1",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Supplemental ID #1&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p> 
                      <input maxlength="100" type=text size="80" name="SupplementalIDNumber1" value="<%=PhysicianMaster.getSupplementalIDNumber1()%>">
                      &nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SupplementalIDNumber1&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("SupplementalIDNumber1")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((PhysicianMaster.isRead("SupplementalIDNumber1",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Supplemental ID #1&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p><%=PhysicianMaster.getSupplementalIDNumber1()%>&nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SupplementalIDNumber1&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("SupplementalIDNumber1")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (PhysicianMaster.isRequired("SupplementalIDNumber2",UserSecurityGroupID))&&(!PhysicianMaster.isComplete("SupplementalIDNumber2")) )
            {
                theClass = "requiredField";
            }
            else if ((PhysicianMaster.isExpired("SupplementalIDNumber2",expiredDays))&&(PhysicianMaster.isExpiredCheck("SupplementalIDNumber2",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if (false&&(PhysicianMaster.isWrite("SupplementalIDNumber2",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Supplemental ID #2&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p> 
                      <input maxlength="100" type=text size="80" name="SupplementalIDNumber2" value="<%=PhysicianMaster.getSupplementalIDNumber2()%>">
                      &nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SupplementalIDNumber2&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("SupplementalIDNumber2")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((PhysicianMaster.isRead("SupplementalIDNumber2",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Supplemental ID #2&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p><%=PhysicianMaster.getSupplementalIDNumber2()%>&nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SupplementalIDNumber2&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("SupplementalIDNumber2")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (PhysicianMaster.isRequired("Comments",UserSecurityGroupID))&&(!PhysicianMaster.isComplete("Comments")) )
            {
                theClass = "requiredField";
            }
            else if ((PhysicianMaster.isExpired("Comments",expiredDays))&&(PhysicianMaster.isExpiredCheck("Comments",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if (false&&(PhysicianMaster.isWrite("Comments",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b><%=PhysicianMaster.getEnglish("Comments")%>&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p> 
                      <textarea onKeyDown="textAreaStop(this,200)" rows="2" name="Comments" cols="40" maxlength=200><%=PhysicianMaster.getComments()%></textarea>
                      &nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Comments&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("Comments")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((PhysicianMaster.isRead("Comments",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b><%=PhysicianMaster.getEnglish("Comments")%>&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p><%=PhysicianMaster.getComments()%>&nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Comments&amp;sTableName=tPhysicianMaster&amp;sRefID=<%=PhysicianMaster.getPhysicianID()%>&amp;sFieldNameDisp=<%=PhysicianMaster.getEnglish("Comments")%>&amp;sTableNameDisp=tPhysicianMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <tr> 
                  <td width=40%>&nbsp;</td>
                  <td width=60%>&nbsp;</td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      <%
  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>
    </td>
  </tr>
</table>
