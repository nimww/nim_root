<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.bltDocumentManagement,java.util.Vector,java.util.Enumeration,com.winstaff.ListElement, com.winstaff.PLCUtils, com.winstaff.SecurityCheck, com.winstaff.ConfigurationMessages,com.winstaff.bltWorkHistory,com.winstaff.bltWorkHistory_List,com.winstaff.DocumentManagerUtils" %>
<%/*
    filename: tWorkHistory_main_WorkHistory_PhysicianID.jsp
    Created on Nov/12/2003
    Type: 1-n main class file
    Created by: Scott Ellis
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>
    <%
    Integer iExpressMode = new Integer(1);
    if (pageControllerHash.containsKey("iExpressMode")) 
    {
        iExpressMode =    (Integer)pageControllerHash.get("iExpressMode");
    }
    %>

    <table cellpadding=0 cellspacing=0 border=0 width=800 >
<%
//initial declaration of list class and parentID
    Integer        iPhysicianID        =    null;
    boolean accessValid = false;
   Integer iSecurityCheck = SecurityCheck.CheckItem("PractitionerSection8", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {
    if (pageControllerHash.containsKey("iPhysicianID")) 
    {
        iPhysicianID        =    (Integer)pageControllerHash.get("iPhysicianID");
        accessValid = true;
    }

  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);
      pageControllerHash.put("sParentReturnPage","tWorkHistory_main_WorkHistory_PhysicianID_Expand.jsp");
    pageControllerHash.remove("iWorkHistoryID");
    session.setAttribute("pageControllerHash",pageControllerHash);

    bltWorkHistory_List        bltWorkHistory_List        =    new    bltWorkHistory_List(iPhysicianID);

//declaration of Enumeration
    bltWorkHistory        WorkHistory;
    ListElement         leCurrentElement;
    Enumeration eList = bltWorkHistory_List.elements();
    %>
    <%
    int iExpress=0;
boolean errorRouteMeTotal = false;
boolean isAllComplete = true;
    while (eList.hasMoreElements()||iExpress<=ConfigurationMessages.getExpressItemCount("tWorkHistory"))
    {
       iExpress++;

      boolean isNewRecord= false;
      if (eList.hasMoreElements())
      {
        leCurrentElement    = (ListElement) eList.nextElement();
        WorkHistory  = (bltWorkHistory) leCurrentElement.getObject();
      }
      else
      {
        WorkHistory  = new bltWorkHistory();
        WorkHistory.setPhysicianID(iPhysicianID);
        isNewRecord= true;
      }
        WorkHistory.GroupSecurityInit(UserSecurityGroupID);
        String theClass = "tdHeader";
        %>

        <%  {

String testChangeID = "0";

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("UniqueCreateDate_"+iExpress))) ;
    if ( !WorkHistory.getUniqueCreateDate().equals(testObj)  )
    {
         WorkHistory.setUniqueCreateDate( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("WorkHistory UniqueCreateDate not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("UniqueModifyDate_"+iExpress))) ;
    if ( !WorkHistory.getUniqueModifyDate().equals(testObj)  )
    {
         WorkHistory.setUniqueModifyDate( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("WorkHistory UniqueModifyDate not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("UniqueModifyComments_"+iExpress)) ;
    if ( !WorkHistory.getUniqueModifyComments().equals(testObj)  )
    {
         WorkHistory.setUniqueModifyComments( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("WorkHistory UniqueModifyComments not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("PhysicianID_"+iExpress)) ;
    if ( !WorkHistory.getPhysicianID().equals(testObj)  )
    {
         WorkHistory.setPhysicianID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("WorkHistory PhysicianID not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("WorkHistoryTypeID_"+iExpress)) ;
    if ( !WorkHistory.getWorkHistoryTypeID().equals(testObj)  )
    {
         WorkHistory.setWorkHistoryTypeID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("WorkHistory WorkHistoryTypeID not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("Name_"+iExpress)) ;
    if ( !WorkHistory.getName().equals(testObj)  )
    {
         WorkHistory.setName( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("WorkHistory Name not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("WorkDescription_"+iExpress)) ;
    if ( !WorkHistory.getWorkDescription().equals(testObj)  )
    {
         WorkHistory.setWorkDescription( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("WorkHistory WorkDescription not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("FromDate_"+iExpress))) ;
    if ( !WorkHistory.getFromDate().equals(testObj)  )
    {
         WorkHistory.setFromDate( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("WorkHistory FromDate not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("ToDate_"+iExpress))) ;
    if ( !WorkHistory.getToDate().equals(testObj)  )
    {
         WorkHistory.setToDate( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("WorkHistory ToDate not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("ReasonForLeaving_"+iExpress)) ;
    if ( !WorkHistory.getReasonForLeaving().equals(testObj)  )
    {
         WorkHistory.setReasonForLeaving( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("WorkHistory ReasonForLeaving not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("Address1_"+iExpress)) ;
    if ( !WorkHistory.getAddress1().equals(testObj)  )
    {
         WorkHistory.setAddress1( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("WorkHistory Address1 not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("Address2_"+iExpress)) ;
    if ( !WorkHistory.getAddress2().equals(testObj)  )
    {
         WorkHistory.setAddress2( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("WorkHistory Address2 not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("City_"+iExpress)) ;
    if ( !WorkHistory.getCity().equals(testObj)  )
    {
         WorkHistory.setCity( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("WorkHistory City not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("StateID_"+iExpress)) ;
    if ( !WorkHistory.getStateID().equals(testObj)  )
    {
         WorkHistory.setStateID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("WorkHistory StateID not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("Province_"+iExpress)) ;
    if ( !WorkHistory.getProvince().equals(testObj)  )
    {
         WorkHistory.setProvince( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("WorkHistory Province not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("ZIP_"+iExpress)) ;
    if ( !WorkHistory.getZIP().equals(testObj)  )
    {
         WorkHistory.setZIP( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("WorkHistory ZIP not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("CountryID_"+iExpress)) ;
    if ( !WorkHistory.getCountryID().equals(testObj)  )
    {
         WorkHistory.setCountryID( testObj,UserSecurityGroupID   );
         //testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("WorkHistory CountryID not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("Phone_"+iExpress)) ;
    if ( !WorkHistory.getPhone().equals(testObj)  )
    {
         WorkHistory.setPhone( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("WorkHistory Phone not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("Fax_"+iExpress)) ;
    if ( !WorkHistory.getFax().equals(testObj)  )
    {
         WorkHistory.setFax( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("WorkHistory Fax not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("ContactName_"+iExpress)) ;
    if ( !WorkHistory.getContactName().equals(testObj)  )
    {
         WorkHistory.setContactName( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("WorkHistory ContactName not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("ContactEmail_"+iExpress)) ;
    if ( !WorkHistory.getContactEmail().equals(testObj)  )
    {
         WorkHistory.setContactEmail( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("WorkHistory ContactEmail not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("Comments_"+iExpress)) ;
    if ( !WorkHistory.getComments().equals(testObj)  )
    {
         WorkHistory.setComments( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("WorkHistory Comments not set. this is ok-not an error");
}
boolean bINT = false;
boolean bNextStep = false;
String sRefreshDoc = "refreshMe=true&";
              if (false&&request.getParameter("INTNext")!=null&&request.getParameter("INTNext").equalsIgnoreCase("yes") ) 
	            {
	                bINT = true;
	                sRefreshDoc = "";
	            }
              else if (request.getParameter("INTNext")!=null&&request.getParameter("INTNext").equalsIgnoreCase("next") ) 
	            {
	                bINT = true;
	                sINTNext = ConfigurationMessages.getInterviewLinkRaw("tWorkHistory","next");
	                sRefreshDoc = "";
	            }

// If an edit, update information; if new, append information
boolean errorRouteMe = false;
if (testChangeID.equalsIgnoreCase("1"))
{

	WorkHistory.setUniqueModifyDate(new java.util.Date());
	String UserLogonDescription = "n/a";
	if (pageControllerHash.containsKey("UserLogonDescription"))
	{
	    UserLogonDescription = (String)pageControllerHash.get("UserLogonDescription");
	}
    if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
    {
	    WorkHistory.setUniqueModifyComments("EX:"+UserLogonDescription);
	    try
	    {
	        WorkHistory.commitData();
	        if (!WorkHistory.isComplete())
	        {
	            isAllComplete = false;
	        }
	    }
	    catch (Exception e55)
	    {
	        errorRouteMe = true;
	        errorRouteMeTotal = true;
	    }
    }
}       }
    }//while

if (errorRouteMeTotal)
{
	out.println(ConfigurationMessages.getMessage("FatalError")); 
}
else 
{
String nextPage = request.getParameter("nextPage");
if (nextPage==null||nextPage.equalsIgnoreCase(""))
{
	nextPage = ConfigurationMessages.getExpressLinkRaw("tWorkHistory","next",iExpressMode);
}
if (!isAllComplete)
{
	%>
	<script language=Javascript>
	      if ( confirm("<%=ConfigurationMessages.getMessage("ConfirmReqFieldsReturn")%>") )
	      {
	          document.location="<%=ConfigurationMessages.getDataCategoryLink("tWorkHistory","Express")%>";
	      }
	      else 
	      {
	          document.location="<%=nextPage%>";
	      }
	</script>
  <%
}
else
{
	%>
	<script language=Javascript>
	      document.location="<%=nextPage%>";
	</script>
  <%
}
}
       
  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>

    </td></tr></table>


<%String bnIncludeFN = "ui_"+thePLCID+"\\bot-nav_PhysicianID.jsp";
%>
<jsp:include page="<%=bnIncludeFN%>" flush="true" ><jsp:param name="plcID" value="<%=thePLCID%>"/></jsp:include>
