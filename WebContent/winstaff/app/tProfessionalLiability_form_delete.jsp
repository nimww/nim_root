<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.bltDocumentManagement,java.util.Vector,java.util.Enumeration,com.winstaff.ListElement, com.winstaff.PLCUtils, com.winstaff.SecurityCheck, com.winstaff.ConfigurationMessages,com.winstaff.bltProfessionalLiability,com.winstaff.DocumentManagerUtils" %>
<%/*
    filename: out\jsp\tProfessionalLiability_form_delete.jsp
    Created on Apr/10/2003
    Type: _form Delete
    Created by: Scott Ellis
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>


<%

//initial declaration of list class and parentID
    Integer        iProfessionalLiabilityID        =    null;
    boolean accessValid = false;

   Integer iSecurityCheck = SecurityCheck.CheckItem("PractitionerSection9", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {

    if (pageControllerHash.containsKey("iProfessionalLiabilityID")) 
    {
        iProfessionalLiabilityID        =    (Integer)pageControllerHash.get("iProfessionalLiabilityID");
        if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {
            accessValid = true;
        }
    }
  //page security
  if (accessValid)
  {

//initial declaration of list class and parentID

    bltProfessionalLiability        ProfessionalLiability        =    null;

    if ( request.getParameter( "EDIT" ).equalsIgnoreCase("del") )
    {
        ProfessionalLiability        =    new    bltProfessionalLiability(iProfessionalLiabilityID);
        ProfessionalLiability.setPhysicianID(new Integer("0"));
        //if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {
            ProfessionalLiability.commitData();
            DocumentManagerUtils.ArchiveDocument("tProfessionalLiability", ProfessionalLiability.getDocuLinkID() , 1);
            out.println("Successful delete");
        }
    }
String routePageReference = request.getParameter("routePageReference");
String nextPage=null;
if (routePageReference!=null)
{
        if (pageControllerHash.containsKey(routePageReference))
        {
            nextPage = (String)pageControllerHash.get(routePageReference);
        }
        else if (pageControllerHash.containsKey("sLocalChildReturnPage"))
        {
            nextPage = (String)pageControllerHash.get("sLocalChildReturnPage");
        }
}
if (nextPage!=null)
{
    response.sendRedirect(nextPage+"?EDIT=edit");
}
        %>
Successful Delete

  <%
  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
  %>


