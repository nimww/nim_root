

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.ConfigurationMessages,com.winstaff.bltHCOPhysicianLU_List_LU_PhysicianID,com.winstaff.bltHCOPhysicianLU, com.winstaff.bltPhysicianUserAccountLU_List_LU_PhysicianID, com.winstaff.bltPhysicianUserAccountLU, com.winstaff.bltDocumentManagement_List,com.winstaff.bltDocumentManagement,com.winstaff.PLCUtils,java.util.Vector,java.util.Enumeration,com.winstaff.ListElement,com.winstaff.bltPhysicianMaster,com.winstaff.stepVerify_PhysicianID" %>
<%/*
    filename: out\jsp\tPhysicianMaster_form.jsp
    JSP AutoGen on Mar/02/2002
*/%>
<%@ include file="../generic/CheckLogin.jsp" %>
<%
//String thePLCID = (String)pageControllerHash.get("plcID");
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_PhysicianID.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>
<%
//initial declaration of list class and parentID
    Integer        iPhysicianID        =    null;
    boolean accessValid = false;
    // required for Type2
    String sKeyMasterReference = null;
    if (pageControllerHash.containsKey("iPhysicianID")) 
    {
        iPhysicianID        =    (Integer)pageControllerHash.get("iPhysicianID");
        accessValid = true;    
   }
  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);
      java.text.SimpleDateFormat displayDateTimeSDF = new java.text.SimpleDateFormat("MM/dd/yyyy hh:mm a");

    pageControllerHash.put("sLocalChildReturnPage","pro-file_Status.jsp");
    pageControllerHash.put("sParentReturnPage","pro-file_Status.jsp");
    session.setAttribute("pageControllerHash",pageControllerHash);

stepVerify_PhysicianID mySV = new stepVerify_PhysicianID();
mySV.setPhysicianID(iPhysicianID);
mySV.setSecurityID(UserSecurityGroupID);
bltPhysicianMaster        PhysicianMaster        =   new  bltPhysicianMaster (iPhysicianID);

//verify data
boolean ApplicationDataCore = false;
if (mySV.isAllCore())
{
	ApplicationDataCore = true;
}
boolean ApplicationDataExtra = false;
if (mySV.isAllExtra())
{
	ApplicationDataExtra = true;
}
boolean ApplicationDataAll = (ApplicationDataExtra&&ApplicationDataCore);
//verify documents are complete:


boolean SupportingDocuments = true;
{
    bltDocumentManagement_List        bltDocumentManagement_List        =    new    bltDocumentManagement_List(iPhysicianID,"Archived=2","");
	//declaration of Enumeration
    bltDocumentManagement        working_bltDocumentManagement;
    ListElement         leCurrentElement;
    java.util.Enumeration eList = bltDocumentManagement_List.elements();
    while (eList.hasMoreElements()&&SupportingDocuments)
    {
        leCurrentElement    = (ListElement) eList.nextElement();
        working_bltDocumentManagement  = (bltDocumentManagement) leCurrentElement.getObject();
		if (!working_bltDocumentManagement.isComplete())
		{
			SupportingDocuments = false;
		}
	}
}
	//check Attestation Account Status
	boolean AttestationAccount = false;
	boolean AttestationAccountCreated = false;
{
    bltPhysicianUserAccountLU_List_LU_PhysicianID        bltPhysicianUserAccountLU_List_LU_PhysicianID        =    new    bltPhysicianUserAccountLU_List_LU_PhysicianID(iPhysicianID);
	//declaration of Enumeration
    bltPhysicianUserAccountLU        working_bltPhysicianUserAccountLU;
    ListElement         leCurrentElement;
    java.util.Enumeration eList = bltPhysicianUserAccountLU_List_LU_PhysicianID.elements();
    if (eList.hasMoreElements())
    {        
 	     AttestationAccountCreated = true;
         leCurrentElement    = (ListElement) eList.nextElement();
         working_bltPhysicianUserAccountLU  = (bltPhysicianUserAccountLU) leCurrentElement.getObject();
         bltUserAccount working_bltUserAccount  = new bltUserAccount(working_bltPhysicianUserAccountLU.getUserID());
		 if (working_bltUserAccount.getStatus().intValue()==2)
		 {
	 	     AttestationAccount = true;
		 }
    }
}

//check attest status
boolean Attestation = false;
if (PhysicianMaster.getIsAttested().intValue()==1)
{
	Attestation = true;
}


//check HCO Auth Status
boolean HCOAuth = false;
{
    bltHCOPhysicianLU_List_LU_PhysicianID        bltHCOPhysicianLU_List_LU_PhysicianID        =    new    bltHCOPhysicianLU_List_LU_PhysicianID(iPhysicianID);

//declaration of Enumeration
    bltHCOPhysicianLU        working_bltHCOPhysicianLU;
    ListElement         leCurrentElement;
    java.util.Enumeration eList = bltHCOPhysicianLU_List_LU_PhysicianID.elements();
    if (eList.hasMoreElements())
    {
         HCOAuth = true;
    }

}





%>
<table width="700" border="0" cellspacing="0" cellpadding="0">
  <tr> 
    <td width=10>&nbsp;</td>
    <td> 
      <table border="0" cellpadding="0" cellspacing="0" width="690">
        <!-- fwtable fwsrc="advancedTop2.png" fwbase="advanced-nav.gif" fwstyle="Dreamweaver" fwdocid = "742308039" fwnested="0" -->
        <tr> 
          <td><img src="express/nav-images/spacer.gif" width="468" height="1" border="0" name="undefined_2"></td>
          <td><img src="express/nav-images/spacer.gif" width="211" height="1" border="0" name="undefined_2"></td>
          <td><img src="express/nav-images/spacer.gif" width="11" height="1" border="0" name="undefined_2"></td>
          <td><img src="express/nav-images/spacer.gif" width="1" height="1" border="0" name="undefined_2"></td>
        </tr>
        <tr> 
          <td colspan="3"><img name="advancednav_r1_c1" src="express/nav-images/advanced-nav_r1_c1.gif" width="690" height="11" border="0"></td>
          <td><img src="express/nav-images/spacer.gif" width="1" height="11" border="0" name="undefined_2"></td>
        </tr>
        <tr> 
          <td rowspan="2"><img name="advancednav_r2_c1" src="express/nav-images/advanced-nav_r2_c1.gif" width="468" height="39" border="0"></td>
          <td bgcolor="#ffffff" valign="top"><a href="tPhysicianMaster_form.jsp?EDIT=edit"><img src="images/continue.gif" width="136" height="23" border="0"></a></td>
          <td rowspan="2"><img name="advancednav_r2_c3" src="express/nav-images/advanced-nav_r2_c3.gif" width="11" height="39" border="0"></td>
          <td><img src="express/nav-images/spacer.gif" width="1" height="29" border="0" name="undefined_2"></td>
        </tr>
        <tr> 
          <td><img name="advancednav_r3_c2" src="express/nav-images/advanced-nav_r3_c2.gif" width="211" height="10" border="0"></td>
          <td><img src="express/nav-images/spacer.gif" width="1" height="10" border="0" name="undefined_2"></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr> 
    <td width=10>&nbsp;</td>
    <td> 
      <table width="100%%" border="1" cellspacing="0" cellpadding="0" bordercolor="#333333">
        <tr> 
          <td> 
            <table width="100%%" border="0" cellspacing="0" cellpadding="5">
              <tr> 
                <td align="center">&nbsp;</td>
              </tr>
              <tr class=tdHeaderAlt> 
                <td align="center"> 
                  <div align="left">Data Collection Process:</div>
                </td>
              </tr>
              <tr> 
                <td align="center"> 
                  <table border="0" cellpadding="0" cellspacing="0" width="650">
                    <!-- fwtable fwsrc="dataprocess1.png" fwbase="dataprocess-nav.gif" fwstyle="Dreamweaver" fwdocid = "742308039" fwnested="0" -->
                    <tr> 
                      <td><img src="images/dataprocess/spacer.gif" width="6" height="1" border="0" name="undefined_3"></td>
                      <td><img src="images/dataprocess/spacer.gif" width="70" height="1" border="0" name="undefined_3"></td>
                      <td><img src="images/dataprocess/spacer.gif" width="8" height="1" border="0" name="undefined_3"></td>
                      <td><img src="images/dataprocess/spacer.gif" width="1" height="1" border="0" name="undefined_3"></td>
                      <td><img src="images/dataprocess/spacer.gif" width="139" height="1" border="0" name="undefined_3"></td>
                      <td><img src="images/dataprocess/spacer.gif" width="15" height="1" border="0" name="undefined_3"></td>
                      <td><img src="images/dataprocess/spacer.gif" width="77" height="1" border="0" name="undefined_3"></td>
                      <td><img src="images/dataprocess/spacer.gif" width="6" height="1" border="0" name="undefined_3"></td>
                      <td><img src="images/dataprocess/spacer.gif" width="121" height="1" border="0" name="undefined_3"></td>
                      <td><img src="images/dataprocess/spacer.gif" width="2" height="1" border="0" name="undefined_3"></td>
                      <td><img src="images/dataprocess/spacer.gif" width="121" height="1" border="0" name="undefined_3"></td>
                      <td><img src="images/dataprocess/spacer.gif" width="6" height="1" border="0" name="undefined_3"></td>
                      <td><img src="images/dataprocess/spacer.gif" width="62" height="1" border="0" name="undefined_3"></td>
                      <td><img src="images/dataprocess/spacer.gif" width="8" height="1" border="0" name="undefined_3"></td>
                      <td><img src="images/dataprocess/spacer.gif" width="8" height="1" border="0" name="undefined_3"></td>
                      <td><img src="images/dataprocess/spacer.gif" width="1" height="1" border="0" name="undefined_3"></td>
                    </tr>
                    <tr> 
                      <td colspan="15"><img name="dataprocessnav_r1_c1_2" src="images/dataprocess/dataprocess-nav_r1_c1.gif" width="650" height="13" border="0"></td>
                      <td><img src="images/dataprocess/spacer.gif" width="1" height="13" border="0" name="undefined_3"></td>
                    </tr>
                    <tr> 
                      <td rowspan="2" colspan="3"><img name="dataprocessnav_r2_c1_2" src="images/dataprocess/dataprocess-nav_r2_c1.gif" width="84" height="49" border="0"></td>
                      <%
if (ApplicationDataCore)
{
%>
                      <td colspan="2"><a href="pro-file_Status_Express.jsp"><img name="dataprocessnav_r2_c4_2" src="images/dataprocess/dataprocess-nav_r2_c4_f3.gif" width="140" height="43" border="0"></a></td>
                      <%
}
else
{
%>
                      <td colspan="2"><a href="pro-file_Status_Express.jsp"><img name="dataprocessnav_r2_c4_2" src="images/dataprocess/dataprocess-nav_r2_c4_f4.gif" width="140" height="43" border="0"></a></td>
                      <%
}
%>
                      <td rowspan="6"><img name="dataprocessnav_r2_c6_2" src="images/dataprocess/dataprocess-nav_r2_c6.gif" width="15" height="187" border="0"></td>
                      <td rowspan="2" colspan="8"><a href="pro-file_Status_Express.jsp"><img name="dataprocessnav_r2_c7" src="images/dataprocess/dataprocess-nav_r2_c7_f2.gif" width="403" height="49" border="0"></a></td>
                      <td rowspan="6"><img name="dataprocessnav_r2_c15" src="images/dataprocess/dataprocess-nav_r2_c15.gif" width="8" height="187" border="0"></td>
                      <td><img src="images/dataprocess/spacer.gif" width="1" height="43" border="0" name="undefined_3"></td>
                    </tr>
                    <tr> 
                      <td rowspan="3" colspan="2"><img name="dataprocessnav_r3_c4_2" src="images/dataprocess/dataprocess-nav_r3_c4.gif" width="140" height="83" border="0"></td>
                      <td><img src="images/dataprocess/spacer.gif" width="1" height="6" border="0" name="undefined_3"></td>
                    </tr>
                    <tr> 
                      <td rowspan="4"><img name="dataprocessnav_r4_c1_2" src="images/dataprocess/dataprocess-nav_r4_c1.gif" width="6" height="138" border="0"></td>
                      <td><a href="pro-file_Status_Express.jsp"><img name="dataprocessnav_r4_c2_2" src="images/dataprocess/dataprocess-nav_r4_c2_f4.gif" width="70" height="72" border="0"></a></td>
                      <td rowspan="4"><img name="dataprocessnav_r4_c3_2" src="images/dataprocess/dataprocess-nav_r4_c3.gif" width="8" height="138" border="0"></td>
                      <%
if (ApplicationDataExtra)
{
%>
                      <td><a href="ExtraInformation_PhysicianID.jsp"><img name="dataprocessnav_r4_c7_2" src="images/dataprocess/dataprocess-nav_r4_c7_f3.gif" width="77" height="72" border="0"></a></td>
                      <%
}
else
{
%>
                      <td><a href="ExtraInformation_PhysicianID.jsp"><img name="dataprocessnav_r4_c7_2" src="images/dataprocess/dataprocess-nav_r4_c7.gif" width="77" height="72" border="0"></a></td>
                      <%
}
%>
                      <td><img name="dataprocessnav_r4_c8_2" src="images/dataprocess/dataprocess-nav_r4_c8.gif" width="6" height="72" border="0"></td>
                      <%
if (PhysicianMaster.getIsAttested().intValue()==1)
{
%>
                      <td><a href="tPhysicianUserAccountLU_main2_UserAccount_PhysicianID.jsp"><img name="dataprocessnav_r4_c9_2" src="images/dataprocess/dataprocess-nav_r4_c9_f3.gif" width="121" height="72" border="0"></a></td>
                      <%
}
else
{
%>
                      <td><a href="tPhysicianUserAccountLU_main2_UserAccount_PhysicianID.jsp"><img name="dataprocessnav_r4_c9_2" src="images/dataprocess/dataprocess-nav_r4_c9.gif" width="121" height="72" border="0"></a></td>
                      <%
}
%>
                      <td><img name="dataprocessnav_r4_c10_2" src="images/dataprocess/dataprocess-nav_r4_c10.gif" width="2" height="72" border="0"></td>
                      <%
if (SupportingDocuments)
{
%>
                      <td><a href="tDocumentManagement_PhysicianID.jsp"><img name="dataprocessnav_r4_c11_2" src="images/dataprocess/dataprocess-nav_r4_c11_f3.gif" width="121" height="72" border="0"></a></td>
                      <%
}
else
{
%>
                      <td><a href="tDocumentManagement_PhysicianID.jsp"><img name="dataprocessnav_r4_c11_2" src="images/dataprocess/dataprocess-nav_r4_c11.gif" width="121" height="72" border="0"></a></td>
                      <%
}
%>
                      <td><img name="dataprocessnav_r4_c12_2" src="images/dataprocess/dataprocess-nav_r4_c12.gif" width="6" height="72" border="0"></td>
                      <td colspan="2"><a href="FinishedCheckList_PhysicianID.jsp"><img name="dataprocessnav_r4_c13_2" src="images/dataprocess/dataprocess-nav_r4_c13.gif" width="70" height="72" border="0"></a></td>
                      <td><img src="images/dataprocess/spacer.gif" width="1" height="72" border="0" name="undefined_3"></td>
                    </tr>
                    <tr> 
                      <td rowspan="3"><img name="dataprocessnav_r5_c2_2" src="images/dataprocess/dataprocess-nav_r5_c2.gif" width="70" height="66" border="0"></td>
                      <td rowspan="2" colspan="7"><img name="dataprocessnav_r5_c7_2" src="images/dataprocess/dataprocess-nav_r5_c7.gif" width="395" height="48" border="0"></td>
                      <td rowspan="3"><img name="dataprocessnav_r5_c14" src="images/dataprocess/dataprocess-nav_r5_c14.gif" width="8" height="66" border="0"></td>
                      <td><img src="images/dataprocess/spacer.gif" width="1" height="5" border="0" name="undefined_3"></td>
                    </tr>
                    <tr> 
                      <td rowspan="2"><img name="dataprocessnav_r6_c4_2" src="images/dataprocess/dataprocess-nav_r6_c4.gif" width="1" height="61" border="0"></td>
                      <%
if (ApplicationDataCore)
{
%>
                      <td><a href="pro-file_Status_Advanced.jsp"><img name="dataprocessnav_r6_c5_2" src="images/dataprocess/dataprocess-nav_r6_c5_f2.gif" width="139" height="43" border="0"></a></td>
                      <%
}
else
{
%>
                      <td><a href="pro-file_Status_Advanced.jsp"><img name="dataprocessnav_r6_c5_2" src="images/dataprocess/dataprocess-nav_r6_c5_f2.gif" width="139" height="43" border="0"></a></td>
                      <%
}
%>
                      <td><img src="images/dataprocess/spacer.gif" width="1" height="43" border="0" name="undefined_3"></td>
                    </tr>
                    <tr> 
                      <td><img name="dataprocessnav_r7_c5_2" src="images/dataprocess/dataprocess-nav_r7_c5.gif" width="139" height="18" border="0"></td>
                      <td colspan="7"><img name="dataprocessnav_r7_c7" src="images/dataprocess/dataprocess-nav_r7_c7.gif" width="395" height="18" border="0"></td>
                      <td><img src="images/dataprocess/spacer.gif" width="1" height="18" border="0" name="undefined_3"></td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr class=tdBase> 
                <td align="center"> 
                  <div align="left"> 
                    <p class=title>Tips:</p>
                  </div>
                  <ul>
                    <li> 
                      <div align="left"><b>Advanced Mode</b> will guide you through 
                        the collection of your Core Practitioner Data using a 
                        more advanced and flexible interface.<br>
                        <br>
                      </div>
                    </li>
                    <li> 
                      <div align="left">For Advanced Mode, click the Continue 
                        Icon <img src="images/continue.gif" width="136" height="23" align="absmiddle"> 
                        at the top of each section page navigate through the various 
                        sections. In each section:<br>
                        click the <img border=0 src="ui_<%=thePLCID%>/icons/edit_PhysicianID.gif"> 
                        Icon to edit a specific record (such as a DEA License 
                        Record).<br>
                        click the <img border=0 src="ui_<%=thePLCID%>/icons/delete_PhysicianID.gif"> 
                        icon in order to delete a specific record.<br>
                        click the <img border=0 src="ui_<%=thePLCID%>/icons/create_PhysicianID.gif"> 
                        icon in order to add a new record.<br>
                        <br>
                      </div>
                    </li>
                    <li> 
                      <div align="left"> To jump to a specific section please 
                        choose from the list below under &quot;<b><span class="textBase">Core 
                        Data Collection Status</span></b>.&quot;</div>
                    </li>
                  </ul>
                </td>
              </tr>
              <tr> 
                <td align="center">&nbsp;</td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr> 
    <td width=10>&nbsp;</td>
    <td></td>
  </tr>
  <tr> 
    <td width=10>&nbsp;</td>
    <td> 
      <table width="100%" border="1" cellspacing="0" cellpadding="0" bordercolor="#333333">
        <tr> 
          <td height="523"> 
            <table width="100%" border="1" cellspacing="0" cellpadding="2" bordercolor="#999999">
              <tr class=tdHeaderAlt> 
                <td colspan="2"><b><span class="textBase">Core Data Collection 
                  Status:</span></b> </td>
                <td>&nbsp;</td>
              </tr>
              <tr class=tdBase> 
                <td>Section 1</td>
                <td><a href="tPhysicianMaster_form.jsp?EDIT=edit">Identifying 
                  Information</a></td>
                <td> 
                  <jsp:include page="../generic/TFCompleted_translate.jsp" flush="true" > 
                  <jsp:param name="CurrentSelection" value="<%=mySV.isStep1ValidInt()%>" />
                  </jsp:include>
                </td>
              </tr>
              <tr class=tdBaseAlt> 
                <td>Section 2</td>
                <td><a href="tCoveringPhysicians_main_CoveringPhysicians_PhysicianID.jsp">Covering 
                  Physicians</a></td>
                <td> 
                  <jsp:include page="../generic/TFCompleted_translate.jsp" flush="true" > 
                  <jsp:param name="CurrentSelection" value="<%=mySV.isStep2ValidInt()%>" />
                  </jsp:include>
                </td>
              </tr>
              <tr class=tdBase> 
                <td>Section 3</td>
                <td><a href="tLicenseRegistration_main_LicenseRegistration_PhysicianID.jsp">Licensure/Registration 
                  </a></td>
                <td> 
                  <jsp:include page="../generic/TFCompleted_translate.jsp" flush="true" > 
                  <jsp:param name="CurrentSelection" value="<%=mySV.isStep3ValidInt()%>" />
                  </jsp:include>
                </td>
              </tr>
              <tr class=tdBaseAlt> 
                <td>Section 4</td>
                <td><a href="tBoardCertification_main_BoardCertification_PhysicianID.jsp">Specialty/Certifications</a></td>
                <td> 
                  <jsp:include page="../generic/TFCompleted_translate.jsp" flush="true" > 
                  <jsp:param name="CurrentSelection" value="<%=mySV.isStep4ValidInt()%>" />
                  </jsp:include>
                </td>
              </tr>
              <tr class=tdBase> 
                <td>Section 5</td>
                <td><a href="tOtherCertification_main_OtherCertification_PhysicianID.jsp">Other 
                  Certification</a></td>
                <td> 
                  <jsp:include page="../generic/TFCompleted_translate.jsp" flush="true" > 
                  <jsp:param name="CurrentSelection" value="<%=mySV.isStep5ValidInt()%>" />
                  </jsp:include>
                </td>
              </tr>
              <tr class=tdBaseAlt> 
                <td>Section 6</td>
                <td><a href="tProfessionalEducation_main_ProfessionalEducation_PhysicianID.jsp">Education</a></td>
                <td> 
                  <jsp:include page="../generic/TFCompleted_translate.jsp" flush="true" > 
                  <jsp:param name="CurrentSelection" value="<%=mySV.isStep6ValidInt()%>" />
                  </jsp:include>
                </td>
              </tr>
              <tr class=tdBase> 
                <td>Section 7</td>
                <td><a href="tExperience_main_Experience_PhysicianID.jsp">Training</a></td>
                <td> 
                  <jsp:include page="../generic/TFCompleted_translate.jsp" flush="true" > 
                  <jsp:param name="CurrentSelection" value="<%=mySV.isStep7ValidInt()%>" />
                  </jsp:include>
                </td>
              </tr>
              <tr class=tdBaseAlt> 
                <td>Section 8</td>
                <td><a href="tWorkHistory_main_WorkHistory_PhysicianID.jsp">Work 
                  History</a></td>
                <td> 
                  <jsp:include page="../generic/TFCompleted_translate.jsp" flush="true" > 
                  <jsp:param name="CurrentSelection" value="<%=mySV.isStep8ValidInt()%>" />
                  </jsp:include>
                </td>
              </tr>
              <tr class=tdBase> 
                <td>Section 9</td>
                <td><a href="tProfessionalLiability_main_ProfessionalLiability_PhysicianID.jsp">Professional 
                  Liability Coverage</a></td>
                <td> 
                  <jsp:include page="../generic/TFCompleted_translate.jsp" flush="true" > 
                  <jsp:param name="CurrentSelection" value="<%=mySV.isStep9ValidInt()%>" />
                  </jsp:include>
                </td>
              </tr>
              <tr class=tdBaseAlt> 
                <td>Section 10</td>
                <td><a href="tFacilityAffiliation_main_FacilityAffiliation_PhysicianID.jsp">Hospital/Facility 
                  Affiliations</a></td>
                <td> 
                  <jsp:include page="../generic/TFCompleted_translate.jsp" flush="true" > 
                  <jsp:param name="CurrentSelection" value="<%=mySV.isStep10ValidInt()%>" />
                  </jsp:include>
                </td>
              </tr>
              <tr class=tdBase> 
                <td>Section 11</td>
                <td><a href="tPeerReference_main_PeerReference_PhysicianID.jsp">Peer 
                  References</a></td>
                <td> 
                  <jsp:include page="../generic/TFCompleted_translate.jsp" flush="true" > 
                  <jsp:param name="CurrentSelection" value="<%=mySV.isStep11ValidInt()%>" />
                  </jsp:include>
                </td>
              </tr>
              <tr class=tdBaseAlt> 
                <td>Section 12</td>
                <td><a href="tProfessionalSociety_main_ProfessionalSociety_PhysicianID.jsp">Professional 
                  Associations</a></td>
                <td> 
                  <jsp:include page="../generic/TFCompleted_translate.jsp" flush="true" > 
                  <jsp:param name="CurrentSelection" value="<%=mySV.isStep12ValidInt()%>" />
                  </jsp:include>
                </td>
              </tr>
              <tr class=tdBase> 
                <td>Section 13</td>
                <td><a href="tAttestR_form2.jsp">Disclosure Questions</a></td>
                <td> 
                  <jsp:include page="../generic/TFCompleted_translate.jsp" flush="true" > 
                  <jsp:param name="CurrentSelection" value="<%=mySV.isStep13ValidInt()%>" />
                  </jsp:include>
                </td>
              </tr>
              <tr class=tdBaseAlt> 
                <td>Section 14</td>
                <td><a href="tMalpractice_main_Malpractice_PhysicianID.jsp">Malpractice 
                  Claims History</a></td>
                <td> 
                  <jsp:include page="../generic/TFCompleted_translate.jsp" flush="true" > 
                  <jsp:param name="CurrentSelection" value="<%=mySV.isStep14ValidInt()%>" />
                  </jsp:include>
                </td>
              </tr>
              <tr class=tdBase> 
                <td>Section 15</td>
                <td><a href="tProfessionalMisconduct_main_ProfessionalMisconduct_PhysicianID.jsp">Professional 
                  Misconduct</a></td>
                <td> 
                  <jsp:include page="../generic/TFCompleted_translate.jsp" flush="true" > 
                  <jsp:param name="CurrentSelection" value="<%=mySV.isStep15ValidInt()%>" />
                  </jsp:include>
                </td>
              </tr>
              <tr class=tdBaseAlt> 
                <td>Section 16</td>
                <td><a href="tContinuingEducation_main_ContinuingEducation_PhysicianID.jsp">Continuing 
                  Education</a></td>
                <td> 
                  <jsp:include page="../generic/TFCompleted_translate.jsp" flush="true" > 
                  <jsp:param name="CurrentSelection" value="<%=mySV.isStep16ValidInt()%>" />
                  </jsp:include>
                </td>
              </tr>
              <tr class=tdBase> 
                <td>Section 17</td>
                <td><a href="tManagedCarePlan_main_ManagedCarePlan_PhysicianID.jsp">Managed 
                  Care Plans</a></td>
                <td> 
                  <jsp:include page="../generic/TFCompleted_translate.jsp" flush="true" > 
                  <jsp:param name="CurrentSelection" value="<%=mySV.isStep17ValidInt()%>" />
                  </jsp:include>
                </td>
              </tr>
              <tr class=tdBase> 
                <td colspan="3">&nbsp;</td>
              </tr>
              <tr class=tdHeaderAlt> 
                <td colspan="3">Extra Info:</td>
              </tr>
              <tr class=tdBase> 
                <td>Section 18</td>
                <td><a href="SupplementalData_main_PhysicianID.jsp">Supplemental 
                  Data</a></td>
                <td>* May be required by some HCOs</td>
              </tr>
              <tr class=tdBaseAlt> 
                <td>Section 19</td>
                <td><a href="Physician_HCOAuth.jsp">HCO Authorization </a></td>
                <td>&nbsp;</td>
              </tr>
              <tr class=tdBase> 
                <td>Section 20</td>
                <td><a href="tPhysicianPracticeLU_PhysicianID.jsp">Practice Information</a></td>
                <td> 
                  <jsp:include page="../generic/TFCompleted_translate.jsp" flush="true" > 
                  <jsp:param name="CurrentSelection" value="<%=mySV.isStep20ValidInt()%>" />
                  </jsp:include>
                </td>
              </tr>
              <tr class=tdBase> 
                <td colspan="3">&nbsp;</td>
              </tr>
              <tr class=tdHeaderAlt> 
                <td colspan="3">Sign Your Application:</td>
              </tr>
              <tr class=tdBase> 
                <td>Section 21</td>
                <td><a href="tPhysicianUserAccountLU_main2_UserAccount_PhysicianID.jsp">Attestation 
                  Accounts</a></td>
                <td> 
                  <jsp:include page="../generic/TFCompleted_translate.jsp" flush="true" > 
                  <jsp:param name="CurrentSelection" value="<%=AttestationAccount%>" />
                  </jsp:include>
                </td>
              </tr>
              <tr class=tdBaseAlt> 
                <td>Section 22</td>
                <td><a href="pro-file_Attestation.jsp">Attestation</a></td>
                <td> 
                  <jsp:include page="../generic/TFCompleted_translate.jsp" flush="true" > 
                  <jsp:param name="CurrentSelection" value="<%=Attestation%>" />
                  </jsp:include>
                </td>
              </tr>
              <tr class=tdBase> 
                <td colspan="3">&nbsp;</td>
              </tr>
              <tr class=tdHeaderAlt> 
                <td colspan="3">Attach Your Documents:</td>
              </tr>
              <tr class=tdBase> 
                <td>Section 23</td>
                <td><a href="tDocumentManagement_PhysicianID.jsp">Attached Documents</a></td>
                <td> 
                  <jsp:include page="../generic/TFCompleted_translate.jsp" flush="true" > 
                  <jsp:param name="CurrentSelection" value="<%=SupportingDocuments%>" />
                  </jsp:include>
                </td>
              </tr>
              <tr class=tdBase> 
                <td colspan="3">&nbsp;</td>
              </tr>
              <tr class=tdHeaderAlt> 
                <td colspan="3">Review:</td>
              </tr>
              <tr class=tdBase> 
                <td>Section 24</td>
                <td><a href="FinishedCheckList_PhysicianID.jsp">Application Checklist</a></td>
                <td><a href="FinishedCheckList_PhysicianID.jsp">Click</a> for 
                  more information.</td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr class=instructions> 
    <td width=10>&nbsp;</td>
    <td>* Items in this section may be required to completely populate certain 
      forms. Your designated Healthcare Organization(s) may also require that 
      some/all of this section be completed.</td>
  </tr>
  <tr> 
    <td width=10>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr> 
    <td width=10>&nbsp;</td>
    <td class=tdHeader> 
      <table width="100%" border="1" cellspacing="0" cellpadding="0" bordercolor="#333333">
        <tr> 
          <td> 
            <table width="100%" border="1" cellspacing="0" cellpadding="3" bordercolor="#999999">
              <tr class=tdHeaderAlt> 
                <td nowrap>Action</td>
                <td width="90%">Description</td>
              </tr>
              <tr class=tdBase> 
                <td nowrap><a href="Physician_HCOAuth.jsp">HCO Authorization</a> 
                </td>
                <td width="90%">Authorize participating HCOs to have electronic 
                  access to your credentialing information.</td>
              </tr>
              <tr class=tdBaseAlt> 
                <td nowrap><a href="populateForms.jsp">Populate Forms</a></td>
                <td width="90%">Print HCO application forms with your data pre-populated.</td>
              </tr>
              <tr class=tdBase> 
                <td nowrap><a href="reports_PhysicianID.jsp">Run Reports</a> </td>
                <td width="90%">View and Print helpful reports</td>
              </tr>
              <tr class=tdBaseAlt> 
                <td nowrap><a href="tDocumentManagement_PhysicianID.jsp">Document 
                  Management</a> </td>
                <td width="90%">On-line storage of copies of your credentialing 
                  documents</td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr> 
    <td width=10>&nbsp;</td>
    <td class=tdHeader>&nbsp;</td>
  </tr>
</table>
<%

  }
  else
  {
   response.sendRedirect("/errorhandler.jsp");
  }

%>
<%String bnIncludeFN = "ui_"+thePLCID+"\\bot-nav_PhysicianID.jsp";
%>
<jsp:include page="<%=bnIncludeFN%>" flush="true" > 
<jsp:param name="plcID" value="<%=thePLCID%>"/>
</jsp:include>
