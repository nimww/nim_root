<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.bltDocumentManagement,java.util.Vector,java.util.Enumeration,com.winstaff.ListElement, com.winstaff.PLCUtils, com.winstaff.SecurityCheck, com.winstaff.ConfigurationMessages,com.winstaff.bltDocumentManagement,com.winstaff.DocumentManagerUtils" %>
<%/*
    filename: out\jsp\tDocumentManagement_form_sub.jsp
    Created on Mar/21/2003
    Created by: Scott Ellis
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>


<%

//initial declaration of list class and parentID
    Integer        iDocumentID        =    null;
    boolean accessValid = false;

   Integer iSecurityCheck = SecurityCheck.CheckItem("DocumentManagement1", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {

    if (pageControllerHash.containsKey("iDocumentID")) 
    {
        iDocumentID        =    (Integer)pageControllerHash.get("iDocumentID");
        if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {
            accessValid = true;
        }
    }
  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);

//initial declaration of list class and parentID

    bltDocumentManagement        DocumentManagement        =    null;

    if ( request.getParameter( "EDIT" ).equalsIgnoreCase("edit") )
    {
        DocumentManagement        =    new    bltDocumentManagement(iDocumentID,UserSecurityGroupID);
    }
    else if ( request.getParameter( "EDIT" ).equalsIgnoreCase("new") )
    {
        DocumentManagement        =    new    bltDocumentManagement(UserSecurityGroupID,true);
    }

String testChangeID = "0";

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("UniqueCreateDate"))) ;
    if ( !DocumentManagement.getUniqueCreateDate().equals(testObj)  )
    {
         DocumentManagement.setUniqueCreateDate( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("DocumentManagement UniqueCreateDate not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("UniqueModifyDate"))) ;
    if ( !DocumentManagement.getUniqueModifyDate().equals(testObj)  )
    {
         DocumentManagement.setUniqueModifyDate( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("DocumentManagement UniqueModifyDate not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("UniqueModifyComments")) ;
    if ( !DocumentManagement.getUniqueModifyComments().equals(testObj)  )
    {
         DocumentManagement.setUniqueModifyComments( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("DocumentManagement UniqueModifyComments not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("PhysicianID")) ;
    if ( !DocumentManagement.getPhysicianID().equals(testObj)  )
    {
         DocumentManagement.setPhysicianID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("DocumentManagement PhysicianID not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("DocumentTypeID")) ;
    if ( !DocumentManagement.getDocumentTypeID().equals(testObj)  )
    {
         DocumentManagement.setDocumentTypeID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("DocumentManagement DocumentTypeID not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("DocumentName")) ;
    if ( !DocumentManagement.getDocumentName().equals(testObj)  )
    {
         DocumentManagement.setDocumentName( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("DocumentManagement DocumentName not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("DocumentFileName")) ;
    if ( !DocumentManagement.getDocumentFileName().equals(testObj)  )
    {
         DocumentManagement.setDocumentFileName( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("DocumentManagement DocumentFileName not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("DateReceived"))) ;
    if ( !DocumentManagement.getDateReceived().equals(testObj)  )
    {
         DocumentManagement.setDateReceived( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("DocumentManagement DateReceived not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("DateOfExpiration"))) ;
    if ( !DocumentManagement.getDateOfExpiration().equals(testObj)  )
    {
         DocumentManagement.setDateOfExpiration( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("DocumentManagement DateOfExpiration not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("DateAlertSent"))) ;
    if ( !DocumentManagement.getDateAlertSent().equals(testObj)  )
    {
         DocumentManagement.setDateAlertSent( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("DocumentManagement DateAlertSent not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("Archived")) ;
    if ( !DocumentManagement.getArchived().equals(testObj)  )
    {
         DocumentManagement.setArchived( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("DocumentManagement Archived not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("Comments")) ;
    if ( !DocumentManagement.getComments().equals(testObj)  )
    {
         DocumentManagement.setComments( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("DocumentManagement Comments not set. this is ok-not an error");
}
boolean bINT = false;
boolean bNextStep = false;
String sRefreshDoc = "refreshMe=true&";
              if (request.getParameter("INTNext")!=null&&request.getParameter("INTNext").equalsIgnoreCase("yes") ) 
	            {
	                bINT = true;
	                sRefreshDoc = "";
	            }
              else if (request.getParameter("INTNext")!=null&&request.getParameter("INTNext").equalsIgnoreCase("next") ) 
	            {
	                bINT = true;
	                sINTNext = ConfigurationMessages.getInterviewLinkRaw("tDocumentManagement","next");
	                sRefreshDoc = "";
	            }

// If an edit, update information; if new, append information
if (testChangeID.equalsIgnoreCase("1"))
{

	DocumentManagement.setUniqueModifyDate(new java.util.Date());
	String UserLogonDescription = "n/a";
	if (pageControllerHash.containsKey("UserLogonDescription"))
	{
	    UserLogonDescription = (String)pageControllerHash.get("UserLogonDescription");
	}
    //if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
    {
	    DocumentManagement.setUniqueModifyComments(UserLogonDescription);
	    DocumentManagement.commitData();
    }
}
String routePageReference = request.getParameter("routePageReference");
String nextPage=null;
if (routePageReference!=null)
{
        if (pageControllerHash.containsKey(routePageReference))
        {
            nextPage = (String)pageControllerHash.get(routePageReference);
        }
        else if (pageControllerHash.containsKey("sLocalChildReturnPage"))
        {
            nextPage = (String)pageControllerHash.get("sLocalChildReturnPage");
        }
        else if (pageControllerHash.containsKey("sParentReturnPage"))
        {
            nextPage = (String)pageControllerHash.get("sParentReturnPage");
        }
}
              if (bINT ) 
	            {
	                nextPage = sINTNext;
	                if (nextPage==null||nextPage.equalsIgnoreCase("")||nextPage.equalsIgnoreCase("#"))
	                {
	                    nextPage = (String)pageControllerHash.get("sLocalChildReturnPage");
	                }
	            }
if (nextPage!=null)
{
	    %><script language=Javascript>
	      document.location="<%=nextPage%>";
	      </script><%
    //response.sendRedirect(nextPage+"?EDIT=edit");
}
        %>
Successful save

  <%
  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
  %>
