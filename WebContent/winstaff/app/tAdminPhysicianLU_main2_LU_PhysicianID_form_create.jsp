<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.PLCUtils,java.util.Vector,java.util.Enumeration,com.winstaff.ListElement,com.winstaff.bltAdminPhysicianLU,com.winstaff.bltAdminPhysicianLU_List_LU_PhysicianID" %>
<%/*
    filename: out\jsp\tAdminPhysicianLU_main2_LU_PhysicianID_form_create.jsp
    JSP AutoGen on Mar/26/2002
    Type: 1-n main class file
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>


<%
//initial declaration of list class and parentID
    Integer        iPhysicianID        =    null;
    boolean accessValid = false;

    {
        iPhysicianID        =    (Integer)pageControllerHash.get("iPhysicianID");
        if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {
            accessValid = true;
        }
    }
  //page security
  if (accessValid)
  {
        Integer requestID = null;
        if (request.getParameter("EDITID")!=null)
        {
            requestID = new Integer(request.getParameter("EDITID"));
        }

//declaration of Enumeration
    bltAdminPhysicianLU        working_bltAdminPhysicianLU = new bltAdminPhysicianLU();
    working_bltAdminPhysicianLU.setPhysicianID(iPhysicianID);
    working_bltAdminPhysicianLU.setUniqueCreateDate(new java.util.Date());
    working_bltAdminPhysicianLU.setUniqueModifyDate(new java.util.Date());
            working_bltAdminPhysicianLU.commitData();
        {
            {
                pageControllerHash.put("iLookupID",working_bltAdminPhysicianLU.getUniqueID());
                pageControllerHash.put("sKeyMasterReference",request.getParameter("KM"));
                session.setAttribute("pageControllerHash",pageControllerHash);
                //Parameter Pass Code here
String parameterPassString ="";
java.util.Enumeration myParameterPassList = request.getParameterNames();
while (myParameterPassList.hasMoreElements())
{
	String myName = (String)myParameterPassList.nextElement();
	String myS = (String) request.getParameter(myName);
	parameterPassString+="&"+myName + "=" + myS;
}
                String targetRedirect = "tAdminPhysicianLU_form2.jsp?nullParam=null"+parameterPassString    ;

                {
                    targetRedirect = "tAdminPhysicianLU_form2.jsp?EDITID="+working_bltAdminPhysicianLU.getUniqueID()+"&routePageReference=sParentReturnPage&EDIT=edit";
                }
                response.sendRedirect(targetRedirect);
            }
        }
  }
  else
  {
   response.sendRedirect("/errorhandler.jsp");
  }
%>





