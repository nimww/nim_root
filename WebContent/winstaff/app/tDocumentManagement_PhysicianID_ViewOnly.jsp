<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.*,java.util.Vector,java.util.Enumeration,com.winstaff.ListElement, com.winstaff.PLCUtils, com.winstaff.SecurityCheck, com.winstaff.ConfigurationMessages, com.winstaff.bltWorkHistory, com.winstaff.bltWorkHistory_List" %>
<%/*
    filename: tWorkHistory_main_WorkHistory_PhysicianID.jsp
    Created on May/26/2004
    Type: 1-n main class file
    Created by: Scott Ellis
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>
    <%
    Integer iExpressMode = new Integer(1);
    String MasterTableWidthVO = "100%";
    if (pageControllerHash.containsKey("iExpressMode")) 
    {
        iExpressMode =    (Integer)pageControllerHash.get("iExpressMode");
    }
    if (pageControllerHash.containsKey("MasterTableWidthVO")) 
    {
        MasterTableWidthVO = (String)pageControllerHash.get("MasterTableWidthVO");
    }
    %>

<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_Clear.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>

    <table cellpadding=0 cellspacing=0 border=0 width=<%=MasterTableWidthVO%> >
    <tr><td width=10>&nbsp;</td><td>
<p class=title>Attached Documents - View Mode</p>




<%
//initial declaration of list class and parentID
    Integer        iPhysicianID        =    null;
    boolean accessValid = false;
   Integer iSecurityCheck = SecurityCheck.CheckItem("PractitionerSection8", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {
    if (pageControllerHash.containsKey("iPhysicianID") ) 
    {
        iPhysicianID        =    (Integer)pageControllerHash.get("iPhysicianID");
        accessValid = true;
    }  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);
      pageControllerHash.put("sParentReturnPage","tWorkHistory_main_WorkHistory_PhysicianID_Expand.jsp");
    session.setAttribute("pageControllerHash",pageControllerHash);

%>
            <table width="100%%" border="1" cellspacing="0" cellpadding="0" bordercolor="#000066">
              <tr> 
                <td> 
                  <table width=100% border="1"  bordercolor="#999999"  cellpaddng=0 cellspacing=0 cellpadding="3">
                    <tr class=tdHeaderAlt> 
                      <td width=10%>Action</td>
                      <td width=40%>Document Name</td>
                      <td width=30%>Document Type</td>
                      <td width=20%>Expiration Date</td>
                    </tr>
                    <%
    bltDocumentManagement_List        bltDocumentManagement_List        =    new    bltDocumentManagement_List(iPhysicianID);

    bltDocumentManagement        working_bltDocumentManagement;
    ListElement         leCurrentElement5;
    java.util.Enumeration eList5 = bltDocumentManagement_List.elements();
    int cnt2=0;
    while (eList5.hasMoreElements())
    {
      cnt2++;
	String myClass = "tdBaseAlt";
      if (cnt2%2!=0)
	{
		myClass = "tdBase";
	}

leCurrentElement5    = (ListElement) eList5.nextElement();
        working_bltDocumentManagement  = (bltDocumentManagement) leCurrentElement5.getObject();
			    pageControllerHash.put("sFileName",ConfigurationInformation.sLinkedPDFDirectory + "\\" + working_bltDocumentManagement.getDocumentFileName());
			    pageControllerHash.put("sDownloadName",working_bltDocumentManagement.getDocumentFileName());
			    pageControllerHash.put("bDownload",new Boolean(false));
			    session.setAttribute("pageControllerHash",pageControllerHash);

	%>
                    <tr class=<%=myClass%>> 
                      <td width=10%> 
                        <%if (working_bltDocumentManagement.isComplete("DocumentFileName")){out.println("<a target=_blank href=\"tDocumentManagement_main_DocumentManagement_PhysicianID_form_authorize.jsp?EDIT=print&amp;EDITID="+working_bltDocumentManagement.getDocumentID()+"\" >View</a>");}else{out.println("Not Avail.");}%>
                      </td>
                      <td width=40%><%=working_bltDocumentManagement.getDocumentName()%></td>
                      <td width=30%><%=new bltDocumentTypeLI(working_bltDocumentManagement.getDocumentTypeID()).getDocumentTypeLong()%></td>
                      <td width=20% nowrap><%=PLCUtils.getDisplayDate(working_bltDocumentManagement.getDateOfExpiration(),false)%></td>
                    </tr>
                    <%

    }
%>
                  </table>


<%



  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>

    </td></tr></table>


