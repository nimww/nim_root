<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.bltDocumentManagement,java.util.Vector,java.util.Enumeration,com.winstaff.ListElement, com.winstaff.PLCUtils, com.winstaff.SecurityCheck, com.winstaff.ConfigurationMessages,com.winstaff.bltPeerReference,com.winstaff.bltPeerReference_List" %>
<%/*
    filename: out\jsp\tPeerReference_main_PeerReference_PhysicianID_form_create.jsp
    Created on Mar/21/2003
    Type: 1-n main class file
    Created by: Scott Ellis
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>


<%
//initial declaration of list class and parentID
    Integer        iPhysicianID        =    null;
    boolean accessValid = false;
   Integer iSecurityCheck = SecurityCheck.CheckItem("PractitionerSection11", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {

    {
        iPhysicianID        =    (Integer)pageControllerHash.get("iPhysicianID");
        if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {
            accessValid = true;
        }
    }
  //page security
  if (accessValid)
  {
        Integer requestID = null;
        if (request.getParameter("EDITID")!=null)
        {
            requestID = new Integer(request.getParameter("EDITID"));
        }

//declaration of Enumeration
    bltPeerReference        working_bltPeerReference = new bltPeerReference();
    working_bltPeerReference.setPhysicianID(iPhysicianID);
    working_bltPeerReference.setUniqueCreateDate(new java.util.Date());
    working_bltPeerReference.setUniqueModifyDate(new java.util.Date());
    if (pageControllerHash.containsKey("UserLogonDescription"))
    {
        String UserLogonDescription = (String)pageControllerHash.get("UserLogonDescription");
        working_bltPeerReference.setUniqueModifyComments(""+UserLogonDescription);
    }

            working_bltPeerReference.commitData();
        {
            {
                pageControllerHash.put("iReferenceID",working_bltPeerReference.getUniqueID());
                pageControllerHash.put("sKeyMasterReference",request.getParameter("KM"));
                session.setAttribute("pageControllerHash",pageControllerHash);
                //Parameter Pass Code here
String parameterPassString ="";
java.util.Enumeration myParameterPassList = request.getParameterNames();
while (myParameterPassList.hasMoreElements())
{
	String myName = (String)myParameterPassList.nextElement();
	String myS = (String) request.getParameter(myName);
	parameterPassString+="&"+myName + "=" + myS;
}
                String targetRedirect = "tPeerReference_form.jsp?nullParam=null"+parameterPassString    ;

                {
                    targetRedirect = "tPeerReference_form.jsp?EDITID="+working_bltPeerReference.getUniqueID()+"&routePageReference=sParentReturnPage&EDIT=edit"+parameterPassString    ;
                }
                response.sendRedirect(targetRedirect);
            }
        }

  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>





