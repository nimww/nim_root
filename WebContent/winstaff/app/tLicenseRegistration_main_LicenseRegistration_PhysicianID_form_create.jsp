<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.bltDocumentManagement,java.util.Vector,java.util.Enumeration,com.winstaff.ListElement, com.winstaff.PLCUtils, com.winstaff.SecurityCheck, com.winstaff.ConfigurationMessages,com.winstaff.bltLicenseRegistration,com.winstaff.bltLicenseRegistration_List" %>
<%/*
    filename: out\jsp\tLicenseRegistration_main_LicenseRegistration_PhysicianID_form_create.jsp
    Created on Mar/21/2003
    Type: 1-n main class file
    Created by: Scott Ellis
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>


<%
//initial declaration of list class and parentID
    Integer        iPhysicianID        =    null;
    boolean accessValid = false;
   Integer iSecurityCheck = SecurityCheck.CheckItem("PractitionerSection3", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {

    {
        iPhysicianID        =    (Integer)pageControllerHash.get("iPhysicianID");
        if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {
            accessValid = true;
        }
    }
  //page security
  if (accessValid)
  {
        Integer requestID = null;
        if (request.getParameter("EDITID")!=null)
        {
            requestID = new Integer(request.getParameter("EDITID"));
        }

//declaration of Enumeration
    bltLicenseRegistration        working_bltLicenseRegistration = new bltLicenseRegistration();
    working_bltLicenseRegistration.setPhysicianID(iPhysicianID);
    working_bltLicenseRegistration.setUniqueCreateDate(new java.util.Date());
    working_bltLicenseRegistration.setUniqueModifyDate(new java.util.Date());
    if (pageControllerHash.containsKey("UserLogonDescription"))
    {
        String UserLogonDescription = (String)pageControllerHash.get("UserLogonDescription");
        working_bltLicenseRegistration.setUniqueModifyComments(""+UserLogonDescription);
    }

            working_bltLicenseRegistration.commitData();
        {
            {
                pageControllerHash.put("iLicenseRegistrationID",working_bltLicenseRegistration.getUniqueID());
                pageControllerHash.put("sKeyMasterReference",request.getParameter("KM"));
                session.setAttribute("pageControllerHash",pageControllerHash);
                //Parameter Pass Code here
String parameterPassString ="";
java.util.Enumeration myParameterPassList = request.getParameterNames();
while (myParameterPassList.hasMoreElements())
{
	String myName = (String)myParameterPassList.nextElement();
	String myS = (String) request.getParameter(myName);
	parameterPassString+="&"+myName + "=" + myS;
}
                String targetRedirect = "tLicenseRegistration_form.jsp?nullParam=null"+parameterPassString    ;

                {
                    targetRedirect = "tLicenseRegistration_form.jsp?EDITID="+working_bltLicenseRegistration.getUniqueID()+"&routePageReference=sParentReturnPage&EDIT=edit"+parameterPassString    ;
                }
                response.sendRedirect(targetRedirect);
            }
        }

  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>





