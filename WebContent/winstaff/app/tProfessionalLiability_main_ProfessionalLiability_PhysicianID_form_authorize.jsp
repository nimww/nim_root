<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.ConfigurationInformation,com.winstaff.bltDocumentManagement,java.util.Vector,java.util.Enumeration,com.winstaff.ListElement, com.winstaff.PLCUtils, com.winstaff.SecurityCheck, com.winstaff.ConfigurationMessages,com.winstaff.bltProfessionalLiability,com.winstaff.bltProfessionalLiability_List" %>
<%/*
    filename: out\jsp\tProfessionalLiability_main_ProfessionalLiability_PhysicianID_form_authorize.jsp
    Created on Mar/21/2003
    Type: 1-n main class file
    Created by: Scott Ellis
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>


<%
//initial declaration of list class and parentID
    Integer        iPhysicianID        =    null;
    boolean accessValid = false;

   Integer iSecurityCheck = SecurityCheck.CheckItem("PractitionerSection9", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {

    if (pageControllerHash.containsKey("iPhysicianID")) 
    {
        iPhysicianID        =    (Integer)pageControllerHash.get("iPhysicianID");
        accessValid = true;
    }
  //page security
  if (accessValid)
  {
        Integer requestID = null;
        if (request.getParameter("EDITID")!=null)
        {
            requestID = new Integer(request.getParameter("EDITID"));
            out.println(requestID);
        }
    bltProfessionalLiability_List        bltProfessionalLiability_List        =    new    bltProfessionalLiability_List(iPhysicianID,"ProfessionalLiabilityID="+requestID,"");

//declaration of Enumeration
    bltProfessionalLiability        working_bltProfessionalLiability;
    ListElement         leCurrentElement;
    Enumeration eList = bltProfessionalLiability_List.elements();
    %>
    <%
    if (eList.hasMoreElements())
    {
        leCurrentElement    = (ListElement) eList.nextElement();
        working_bltProfessionalLiability  = (bltProfessionalLiability) leCurrentElement.getObject();
        pageControllerHash.put("iProfessionalLiabilityID",working_bltProfessionalLiability.getProfessionalLiabilityID());
        pageControllerHash.put("sKeyMasterReference",request.getParameter("KM"));
        session.setAttribute("pageControllerHash",pageControllerHash);
        //Parameter Pass Code here
String parameterPassString ="";
java.util.Enumeration myParameterPassList = request.getParameterNames();
while (myParameterPassList.hasMoreElements())
{
	String myName = (String)myParameterPassList.nextElement();
	String myS = (String) request.getParameter(myName);
	parameterPassString+="&"+myName + "=" + myS;
}
        String targetRedirect = "tProfessionalLiability_form.jsp?nullParam=null"+parameterPassString    ;
        if (request.getParameter("EDIT").equalsIgnoreCase("del"))
        {
            targetRedirect = "tProfessionalLiability_form_delete.jsp?routePageReference=sParentReturnPage"+parameterPassString    ;
        }
        if (request.getParameter("EDIT").equalsIgnoreCase("print"))
        {
	  bltDocumentManagement myDoc = new bltDocumentManagement(working_bltProfessionalLiability.getDocuLinkID());
			    pageControllerHash.put("sFileName",ConfigurationInformation.sLinkedPDFDirectory + "\\" + myDoc.getDocumentFileName());
			    pageControllerHash.put("sDownloadName",myDoc.getDocumentFileName());
			    pageControllerHash.put("bDownload",new Boolean(false));
			    session.setAttribute("pageControllerHash",pageControllerHash);
            targetRedirect = "fileRetrieve.jsp"   ;
        }
        response.sendRedirect(targetRedirect);
    }
    else
    {
   out.println("invalid where query");
    }

  }
  else
  {
   out.println("illegal");
  }
}
else
{
out.println("Your Security Level does not permit you to View this.");
}
%>




