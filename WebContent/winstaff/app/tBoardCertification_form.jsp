<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.ConfigurationInformation,com.winstaff.bltDocumentManagement,java.util.Vector,java.util.Enumeration,com.winstaff.ListElement, com.winstaff.PLCUtils, com.winstaff.SecurityCheck, com.winstaff.ConfigurationMessages,com.winstaff.bltBoardCertification" %>
<%/*

    filename: out\jsp\tBoardCertification_form.jsp
    Created on Mar/21/2003
    Created by: Scott Ellis
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>

<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_PhysicianID.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>


    <table cellpadding=0 cellspacing=0 border=0 width=<%=MasterTableWidth%> >
    <tr><td width=10>&nbsp;</td><td>
    <%=ConfigurationMessages.getHTML("INTERVIEWTopControl_form","tBoardCertification")%>



<%
//initial declaration of list class and parentID
    Integer        iBoardCertificationID        =    null;
    boolean accessValid = false;
    // required for Type2
    String sKeyMasterReference = null;

   Integer iSecurityCheck = SecurityCheck.CheckItem("PractitionerSection4", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {
    if (pageControllerHash.containsKey("iBoardCertificationID")) 
    {
        iBoardCertificationID        =    (Integer)pageControllerHash.get("iBoardCertificationID");
        accessValid = true;    }
  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);      java.text.SimpleDateFormat displayDateTimeSDF = new java.text.SimpleDateFormat("MM/dd/yyyy hh:mm a");

    pageControllerHash.put("sLocalChildReturnPage","tBoardCertification_form.jsp");
//initial declaration of list class and parentID

    bltBoardCertification        BoardCertification        =    null;

    if ( request.getParameter( "EDIT" ).equalsIgnoreCase("edit") )
    {
        BoardCertification        =    new    bltBoardCertification(iBoardCertificationID,UserSecurityGroupID);
    }
    else if ( request.getParameter( "EDIT" ).equalsIgnoreCase("new") )
    {
        BoardCertification        =    new    bltBoardCertification(UserSecurityGroupID, true);
    }

//fields
        %>
        <%@ include file="tBoardCertification_form_instructions.jsp" %>
        <form action="tBoardCertification_form_sub.jsp" name="tBoardCertification_form1" method="POST">
<%
    if ( request.getParameter( "EDIT" ) != null )
    {
    %>
        <input type="hidden" name="EDIT" value = "<%=request.getParameter( "EDIT" )%>" >  
    <%
    }
%>
          <%  String theClass ="tdBase";%>
        <table width=100% border=1 bordercolor=#333333 cellpadding=3 cellspacing=0 class=tableBase>
         <tr><td class=tableColor>


            <table cellpadding=0 cellspacing=0 width=100%>
            <%
            if ( (BoardCertification.isRequired("Specialty",UserSecurityGroupID))&&(!BoardCertification.isComplete("Specialty")) )
            {
                theClass = "requiredField";
            }
            else if ((BoardCertification.isExpired("Specialty",expiredDays))&&(BoardCertification.isExpiredCheck("Specialty",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((BoardCertification.isWrite("Specialty",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Specialty&nbsp;</b></p></td><td valign=top><p><input maxlength="100" type=text size="80" name="Specialty" value="<%=BoardCertification.getSpecialty()%>">&nbsp;<a href="#" onClick="window.open('LI_tSpecialtyLI_Search.jsp?OFieldName=Specialty','BNPop','status=yes,scrollbars=yes,resizable=yes,width=400,height=300'); return false;"><img align=middle border=0 src=images/icon_lu.gif></a>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Specialty&amp;sTableName=tBoardCertification&amp;sRefID=<%=BoardCertification.getBoardCertificationID()%>&amp;sFieldNameDisp=<%=BoardCertification.getEnglish("Specialty")%>&amp;sTableNameDisp=tBoardCertification','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>                      <%
            }
            else if ((BoardCertification.isRead("Specialty",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Specialty&nbsp;</b></p></td><td valign=top><p><%=BoardCertification.getSpecialty()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Specialty&amp;sTableName=tBoardCertification&amp;sRefID=<%=BoardCertification.getBoardCertificationID()%>&amp;sFieldNameDisp=<%=BoardCertification.getEnglish("Specialty")%>&amp;sTableNameDisp=tBoardCertification','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (BoardCertification.isRequired("SubSpecialty",UserSecurityGroupID))&&(!BoardCertification.isComplete("SubSpecialty")) )
            {
                theClass = "requiredField";
            }
            else if ((BoardCertification.isExpired("SubSpecialty",expiredDays))&&(BoardCertification.isExpiredCheck("SubSpecialty",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((BoardCertification.isWrite("SubSpecialty",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Sub-specialty related to above specialty&nbsp;</b></p></td><td valign=top><p><input maxlength="100" type=text size="80" name="SubSpecialty" value="<%=BoardCertification.getSubSpecialty()%>">&nbsp;<a href="#" onClick="window.open('LI_tSpecialtyLI_Search.jsp?OFieldName=SubSpecialty','BNPop','status=yes,scrollbars=yes,resizable=yes,width=400,height=300'); return false;"><img align=middle border=0 src=images/icon_lu.gif></a>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SubSpecialty&amp;sTableName=tBoardCertification&amp;sRefID=<%=BoardCertification.getBoardCertificationID()%>&amp;sFieldNameDisp=<%=BoardCertification.getEnglish("SubSpecialty")%>&amp;sTableNameDisp=tBoardCertification','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((BoardCertification.isRead("SubSpecialty",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Sub-specialty related to above specialty&nbsp;</b></p></td><td valign=top><p><%=BoardCertification.getSubSpecialty()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SubSpecialty&amp;sTableName=tBoardCertification&amp;sRefID=<%=BoardCertification.getBoardCertificationID()%>&amp;sFieldNameDisp=<%=BoardCertification.getEnglish("SubSpecialty")%>&amp;sTableNameDisp=tBoardCertification','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (BoardCertification.isRequired("IsListed",UserSecurityGroupID))&&(!BoardCertification.isComplete("IsListed")) )
            {
                theClass = "requiredField";
            }
            else if ((BoardCertification.isExpired("IsListed",expiredDays))&&(BoardCertification.isExpiredCheck("IsListed",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((BoardCertification.isWrite("IsListed",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Do you wish to be listed in Directories under this specialty?&nbsp;</b></p></td><td valign=top><p><select   name="IsListed" ><jsp:include page="../generic/tYesNoLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=BoardCertification.getIsListed()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=IsListed&amp;sTableName=tBoardCertification&amp;sRefID=<%=BoardCertification.getBoardCertificationID()%>&amp;sFieldNameDisp=<%=BoardCertification.getEnglish("IsListed")%>&amp;sTableNameDisp=tBoardCertification','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((BoardCertification.isRead("IsListed",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Do you wish to be listed in Directories under this specialty?&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=BoardCertification.getIsListed()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=IsListed&amp;sTableName=tBoardCertification&amp;sRefID=<%=BoardCertification.getBoardCertificationID()%>&amp;sFieldNameDisp=<%=BoardCertification.getEnglish("IsListed")%>&amp;sTableNameDisp=tBoardCertification','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (BoardCertification.isRequired("IsPrimary",UserSecurityGroupID))&&(!BoardCertification.isComplete("IsPrimary")) )
            {
                theClass = "requiredField";
            }
            else if ((BoardCertification.isExpired("IsPrimary",expiredDays))&&(BoardCertification.isExpiredCheck("IsPrimary",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((BoardCertification.isWrite("IsPrimary",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Is this your primary specialty?&nbsp;</b></p></td><td valign=top><p><select   name="IsPrimary" ><jsp:include page="../generic/tYesNoLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=BoardCertification.getIsPrimary()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=IsPrimary&amp;sTableName=tBoardCertification&amp;sRefID=<%=BoardCertification.getBoardCertificationID()%>&amp;sFieldNameDisp=<%=BoardCertification.getEnglish("IsPrimary")%>&amp;sTableNameDisp=tBoardCertification','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((BoardCertification.isRead("IsPrimary",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Is this your primary specialty?&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=BoardCertification.getIsPrimary()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=IsPrimary&amp;sTableName=tBoardCertification&amp;sRefID=<%=BoardCertification.getBoardCertificationID()%>&amp;sFieldNameDisp=<%=BoardCertification.getEnglish("IsPrimary")%>&amp;sTableNameDisp=tBoardCertification','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (BoardCertification.isRequired("SpecialtyBoardCertified",UserSecurityGroupID))&&(!BoardCertification.isComplete("SpecialtyBoardCertified")) )
            {
                theClass = "requiredField";
            }
            else if ((BoardCertification.isExpired("SpecialtyBoardCertified",expiredDays))&&(BoardCertification.isExpiredCheck("SpecialtyBoardCertified",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((BoardCertification.isWrite("SpecialtyBoardCertified",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Specialty Status&nbsp;</b></p></td><td valign=top><p><select   name="SpecialtyBoardCertified" ><jsp:include page="../generic/tSpecialtyStatusLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=BoardCertification.getSpecialtyBoardCertified()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SpecialtyBoardCertified&amp;sTableName=tBoardCertification&amp;sRefID=<%=BoardCertification.getBoardCertificationID()%>&amp;sFieldNameDisp=<%=BoardCertification.getEnglish("SpecialtyBoardCertified")%>&amp;sTableNameDisp=tBoardCertification','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((BoardCertification.isRead("SpecialtyBoardCertified",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Specialty Status&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tSpecialtyStatusLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=BoardCertification.getSpecialtyBoardCertified()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SpecialtyBoardCertified&amp;sTableName=tBoardCertification&amp;sRefID=<%=BoardCertification.getBoardCertificationID()%>&amp;sFieldNameDisp=<%=BoardCertification.getEnglish("SpecialtyBoardCertified")%>&amp;sTableNameDisp=tBoardCertification','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (BoardCertification.isRequired("BoardName",UserSecurityGroupID))&&(!BoardCertification.isComplete("BoardName")) )
            {
                theClass = "requiredField";
            }
            else if ((BoardCertification.isExpired("BoardName",expiredDays))&&(BoardCertification.isExpiredCheck("BoardName",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((BoardCertification.isWrite("BoardName",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Board Name&nbsp;</b></p></td><td valign=top><p><input maxlength="100" type=text size="80" name="BoardName" value="<%=BoardCertification.getBoardName()%>">&nbsp;<a href="#" onClick="window.open('LI_tBoardNameLI_Search.jsp?OFieldName=BoardName','BNPop','status=yes,scrollbars=yes,resizable=yes,width=400,height=300'); return false;"><img align=middle border=0 src=images/icon_lu.gif></a>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=BoardName&amp;sTableName=tBoardCertification&amp;sRefID=<%=BoardCertification.getBoardCertificationID()%>&amp;sFieldNameDisp=<%=BoardCertification.getEnglish("BoardName")%>&amp;sTableNameDisp=tBoardCertification','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((BoardCertification.isRead("BoardName",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Board Name&nbsp;</b></p></td><td valign=top><p><%=BoardCertification.getBoardName()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=BoardName&amp;sTableName=tBoardCertification&amp;sRefID=<%=BoardCertification.getBoardCertificationID()%>&amp;sFieldNameDisp=<%=BoardCertification.getEnglish("BoardName")%>&amp;sTableNameDisp=tBoardCertification','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (BoardCertification.isRequired("ContactName",UserSecurityGroupID))&&(!BoardCertification.isComplete("ContactName")) )
            {
                theClass = "requiredField";
            }
            else if ((BoardCertification.isExpired("ContactName",expiredDays))&&(BoardCertification.isExpiredCheck("ContactName",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((BoardCertification.isWrite("ContactName",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Contact Name&nbsp;</b></p></td><td valign=top><p><input maxlength="50" type=text size="80" name="ContactName" value="<%=BoardCertification.getContactName()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ContactName&amp;sTableName=tBoardCertification&amp;sRefID=<%=BoardCertification.getBoardCertificationID()%>&amp;sFieldNameDisp=<%=BoardCertification.getEnglish("ContactName")%>&amp;sTableNameDisp=tBoardCertification','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((BoardCertification.isRead("ContactName",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Contact Name&nbsp;</b></p></td><td valign=top><p><%=BoardCertification.getContactName()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ContactName&amp;sTableName=tBoardCertification&amp;sRefID=<%=BoardCertification.getBoardCertificationID()%>&amp;sFieldNameDisp=<%=BoardCertification.getEnglish("ContactName")%>&amp;sTableNameDisp=tBoardCertification','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (BoardCertification.isRequired("ContactEmail",UserSecurityGroupID))&&(!BoardCertification.isComplete("ContactEmail")) )
            {
                theClass = "requiredField";
            }
            else if ((BoardCertification.isExpired("ContactEmail",expiredDays))&&(BoardCertification.isExpiredCheck("ContactEmail",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((BoardCertification.isWrite("ContactEmail",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Contact E-mail&nbsp;</b></p></td><td valign=top><p><input maxlength="75" type=text size="80" name="ContactEmail" value="<%=BoardCertification.getContactEmail()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ContactEmail&amp;sTableName=tBoardCertification&amp;sRefID=<%=BoardCertification.getBoardCertificationID()%>&amp;sFieldNameDisp=<%=BoardCertification.getEnglish("ContactEmail")%>&amp;sTableNameDisp=tBoardCertification','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((BoardCertification.isRead("ContactEmail",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Contact E-mail&nbsp;</b></p></td><td valign=top><p><%=BoardCertification.getContactEmail()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ContactEmail&amp;sTableName=tBoardCertification&amp;sRefID=<%=BoardCertification.getBoardCertificationID()%>&amp;sFieldNameDisp=<%=BoardCertification.getEnglish("ContactEmail")%>&amp;sTableNameDisp=tBoardCertification','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (BoardCertification.isRequired("Address1",UserSecurityGroupID))&&(!BoardCertification.isComplete("Address1")) )
            {
                theClass = "requiredField";
            }
            else if ((BoardCertification.isExpired("Address1",expiredDays))&&(BoardCertification.isExpiredCheck("Address1",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((BoardCertification.isWrite("Address1",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Address&nbsp;</b></p></td><td valign=top><p><input maxlength="50" type=text size="80" name="Address1" value="<%=BoardCertification.getAddress1()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Address1&amp;sTableName=tBoardCertification&amp;sRefID=<%=BoardCertification.getBoardCertificationID()%>&amp;sFieldNameDisp=<%=BoardCertification.getEnglish("Address1")%>&amp;sTableNameDisp=tBoardCertification','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((BoardCertification.isRead("Address1",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Address&nbsp;</b></p></td><td valign=top><p><%=BoardCertification.getAddress1()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Address1&amp;sTableName=tBoardCertification&amp;sRefID=<%=BoardCertification.getBoardCertificationID()%>&amp;sFieldNameDisp=<%=BoardCertification.getEnglish("Address1")%>&amp;sTableNameDisp=tBoardCertification','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (BoardCertification.isRequired("Address2",UserSecurityGroupID))&&(!BoardCertification.isComplete("Address2")) )
            {
                theClass = "requiredField";
            }
            else if ((BoardCertification.isExpired("Address2",expiredDays))&&(BoardCertification.isExpiredCheck("Address2",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((BoardCertification.isWrite("Address2",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Address 2&nbsp;</b></p></td><td valign=top><p><input maxlength="20" type=text size="80" name="Address2" value="<%=BoardCertification.getAddress2()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Address2&amp;sTableName=tBoardCertification&amp;sRefID=<%=BoardCertification.getBoardCertificationID()%>&amp;sFieldNameDisp=<%=BoardCertification.getEnglish("Address2")%>&amp;sTableNameDisp=tBoardCertification','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((BoardCertification.isRead("Address2",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Address 2&nbsp;</b></p></td><td valign=top><p><%=BoardCertification.getAddress2()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Address2&amp;sTableName=tBoardCertification&amp;sRefID=<%=BoardCertification.getBoardCertificationID()%>&amp;sFieldNameDisp=<%=BoardCertification.getEnglish("Address2")%>&amp;sTableNameDisp=tBoardCertification','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (BoardCertification.isRequired("City",UserSecurityGroupID))&&(!BoardCertification.isComplete("City")) )
            {
                theClass = "requiredField";
            }
            else if ((BoardCertification.isExpired("City",expiredDays))&&(BoardCertification.isExpiredCheck("City",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((BoardCertification.isWrite("City",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Institution City&nbsp;</b></p></td><td valign=top><p><input maxlength="30" type=text size="80" name="City" value="<%=BoardCertification.getCity()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=City&amp;sTableName=tBoardCertification&amp;sRefID=<%=BoardCertification.getBoardCertificationID()%>&amp;sFieldNameDisp=<%=BoardCertification.getEnglish("City")%>&amp;sTableNameDisp=tBoardCertification','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((BoardCertification.isRead("City",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Institution City&nbsp;</b></p></td><td valign=top><p><%=BoardCertification.getCity()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=City&amp;sTableName=tBoardCertification&amp;sRefID=<%=BoardCertification.getBoardCertificationID()%>&amp;sFieldNameDisp=<%=BoardCertification.getEnglish("City")%>&amp;sTableNameDisp=tBoardCertification','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (BoardCertification.isRequired("StateID",UserSecurityGroupID))&&(!BoardCertification.isComplete("StateID")) )
            {
                theClass = "requiredField";
            }
            else if ((BoardCertification.isExpired("StateID",expiredDays))&&(BoardCertification.isExpiredCheck("StateID",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((BoardCertification.isWrite("StateID",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>State&nbsp;</b></p></td><td valign=top><p><select   name="StateID" ><jsp:include page="../generic/tStateLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=BoardCertification.getStateID()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=StateID&amp;sTableName=tBoardCertification&amp;sRefID=<%=BoardCertification.getBoardCertificationID()%>&amp;sFieldNameDisp=<%=BoardCertification.getEnglish("StateID")%>&amp;sTableNameDisp=tBoardCertification','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((BoardCertification.isRead("StateID",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>State&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tStateLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=BoardCertification.getStateID()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=StateID&amp;sTableName=tBoardCertification&amp;sRefID=<%=BoardCertification.getBoardCertificationID()%>&amp;sFieldNameDisp=<%=BoardCertification.getEnglish("StateID")%>&amp;sTableNameDisp=tBoardCertification','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (BoardCertification.isRequired("Province",UserSecurityGroupID))&&(!BoardCertification.isComplete("Province")) )
            {
                theClass = "requiredField";
            }
            else if ((BoardCertification.isExpired("Province",expiredDays))&&(BoardCertification.isExpiredCheck("Province",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((BoardCertification.isWrite("Province",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Province, District, State&nbsp;</b></p></td><td valign=top><p><input maxlength="100" type=text size="80" name="Province" value="<%=BoardCertification.getProvince()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Province&amp;sTableName=tBoardCertification&amp;sRefID=<%=BoardCertification.getBoardCertificationID()%>&amp;sFieldNameDisp=<%=BoardCertification.getEnglish("Province")%>&amp;sTableNameDisp=tBoardCertification','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((BoardCertification.isRead("Province",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Province, District, State&nbsp;</b></p></td><td valign=top><p><%=BoardCertification.getProvince()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Province&amp;sTableName=tBoardCertification&amp;sRefID=<%=BoardCertification.getBoardCertificationID()%>&amp;sFieldNameDisp=<%=BoardCertification.getEnglish("Province")%>&amp;sTableNameDisp=tBoardCertification','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (BoardCertification.isRequired("ZIP",UserSecurityGroupID))&&(!BoardCertification.isComplete("ZIP")) )
            {
                theClass = "requiredField";
            }
            else if ((BoardCertification.isExpired("ZIP",expiredDays))&&(BoardCertification.isExpiredCheck("ZIP",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((BoardCertification.isWrite("ZIP",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>ZIP&nbsp;</b></p></td><td valign=top><p><input maxlength="50" type=text size="80" name="ZIP" value="<%=BoardCertification.getZIP()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ZIP&amp;sTableName=tBoardCertification&amp;sRefID=<%=BoardCertification.getBoardCertificationID()%>&amp;sFieldNameDisp=<%=BoardCertification.getEnglish("ZIP")%>&amp;sTableNameDisp=tBoardCertification','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((BoardCertification.isRead("ZIP",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>ZIP&nbsp;</b></p></td><td valign=top><p><%=BoardCertification.getZIP()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ZIP&amp;sTableName=tBoardCertification&amp;sRefID=<%=BoardCertification.getBoardCertificationID()%>&amp;sFieldNameDisp=<%=BoardCertification.getEnglish("ZIP")%>&amp;sTableNameDisp=tBoardCertification','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (BoardCertification.isRequired("CountryID",UserSecurityGroupID))&&(!BoardCertification.isComplete("CountryID")) )
            {
                theClass = "requiredField";
            }
            else if ((BoardCertification.isExpired("CountryID",expiredDays))&&(BoardCertification.isExpiredCheck("CountryID",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((BoardCertification.isWrite("CountryID",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Country&nbsp;</b></p></td><td valign=top><p><select   name="CountryID" ><jsp:include page="../generic/tCountryLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=BoardCertification.getCountryID()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CountryID&amp;sTableName=tBoardCertification&amp;sRefID=<%=BoardCertification.getBoardCertificationID()%>&amp;sFieldNameDisp=<%=BoardCertification.getEnglish("CountryID")%>&amp;sTableNameDisp=tBoardCertification','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((BoardCertification.isRead("CountryID",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Country&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tCountryLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=BoardCertification.getCountryID()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CountryID&amp;sTableName=tBoardCertification&amp;sRefID=<%=BoardCertification.getBoardCertificationID()%>&amp;sFieldNameDisp=<%=BoardCertification.getEnglish("CountryID")%>&amp;sTableNameDisp=tBoardCertification','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (BoardCertification.isRequired("Phone",UserSecurityGroupID))&&(!BoardCertification.isComplete("Phone")) )
            {
                theClass = "requiredField";
            }
            else if ((BoardCertification.isExpired("Phone",expiredDays))&&(BoardCertification.isExpiredCheck("Phone",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((BoardCertification.isWrite("Phone",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Phone (XXX-XXX-XXXX)&nbsp;</b></p></td><td valign=top><p><input maxlength="50" type=text size="80" name="Phone" value="<%=BoardCertification.getPhone()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Phone&amp;sTableName=tBoardCertification&amp;sRefID=<%=BoardCertification.getBoardCertificationID()%>&amp;sFieldNameDisp=<%=BoardCertification.getEnglish("Phone")%>&amp;sTableNameDisp=tBoardCertification','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((BoardCertification.isRead("Phone",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Phone (XXX-XXX-XXXX)&nbsp;</b></p></td><td valign=top><p><%=BoardCertification.getPhone()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Phone&amp;sTableName=tBoardCertification&amp;sRefID=<%=BoardCertification.getBoardCertificationID()%>&amp;sFieldNameDisp=<%=BoardCertification.getEnglish("Phone")%>&amp;sTableNameDisp=tBoardCertification','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (BoardCertification.isRequired("Fax",UserSecurityGroupID))&&(!BoardCertification.isComplete("Fax")) )
            {
                theClass = "requiredField";
            }
            else if ((BoardCertification.isExpired("Fax",expiredDays))&&(BoardCertification.isExpiredCheck("Fax",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((BoardCertification.isWrite("Fax",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Fax (XXX-XXX-XXXX)&nbsp;</b></p></td><td valign=top><p><input maxlength="50" type=text size="80" name="Fax" value="<%=BoardCertification.getFax()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Fax&amp;sTableName=tBoardCertification&amp;sRefID=<%=BoardCertification.getBoardCertificationID()%>&amp;sFieldNameDisp=<%=BoardCertification.getEnglish("Fax")%>&amp;sTableNameDisp=tBoardCertification','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((BoardCertification.isRead("Fax",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Fax (XXX-XXX-XXXX)&nbsp;</b></p></td><td valign=top><p><%=BoardCertification.getFax()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Fax&amp;sTableName=tBoardCertification&amp;sRefID=<%=BoardCertification.getBoardCertificationID()%>&amp;sFieldNameDisp=<%=BoardCertification.getEnglish("Fax")%>&amp;sTableNameDisp=tBoardCertification','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (BoardCertification.isRequired("BoardDateInitialCertified",UserSecurityGroupID))&&(!BoardCertification.isComplete("BoardDateInitialCertified")) )
            {
                theClass = "requiredField";
            }
            else if ((BoardCertification.isExpired("BoardDateInitialCertified",expiredDays))&&(BoardCertification.isExpiredCheck("BoardDateInitialCertified",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>

            <%
            if ((BoardCertification.isWrite("BoardDateInitialCertified",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Initial Certification Date&nbsp;(mm/dd/yyyy):&nbsp;</b></p></td><td valign=top><p><input maxlength=20 type=text size="80" name="BoardDateInitialCertified" value='<jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(BoardCertification.getBoardDateInitialCertified())%>" /></jsp:include>' >&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=BoardDateInitialCertified&amp;sTableName=tBoardCertification&amp;sRefID=<%=BoardCertification.getBoardCertificationID()%>&amp;sFieldNameDisp=<%=BoardCertification.getEnglish("BoardDateInitialCertified")%>&amp;sTableNameDisp=tBoardCertification','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((BoardCertification.isRead("BoardDateInitialCertified",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Initial Certification Date&nbsp;(mm/dd/yyyy):&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(BoardCertification.getBoardDateInitialCertified())%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=BoardDateInitialCertified&amp;sTableName=tBoardCertification&amp;sRefID=<%=BoardCertification.getBoardCertificationID()%>&amp;sFieldNameDisp=<%=BoardCertification.getEnglish("BoardDateInitialCertified")%>&amp;sTableNameDisp=tBoardCertification','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (BoardCertification.isRequired("BoardDateRecertified",UserSecurityGroupID))&&(!BoardCertification.isComplete("BoardDateRecertified")) )
            {
                theClass = "requiredField";
            }
            else if ((BoardCertification.isExpired("BoardDateRecertified",expiredDays))&&(BoardCertification.isExpiredCheck("BoardDateRecertified",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>

            <%
            if ((BoardCertification.isWrite("BoardDateRecertified",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Date of Recertification&nbsp;(mm/dd/yyyy):&nbsp;</b></p></td><td valign=top><p><input maxlength=20 type=text size="80" name="BoardDateRecertified" value='<jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(BoardCertification.getBoardDateRecertified())%>" /></jsp:include>' >&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=BoardDateRecertified&amp;sTableName=tBoardCertification&amp;sRefID=<%=BoardCertification.getBoardCertificationID()%>&amp;sFieldNameDisp=<%=BoardCertification.getEnglish("BoardDateRecertified")%>&amp;sTableNameDisp=tBoardCertification','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((BoardCertification.isRead("BoardDateRecertified",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Date of Recertification&nbsp;(mm/dd/yyyy):&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(BoardCertification.getBoardDateRecertified())%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=BoardDateRecertified&amp;sTableName=tBoardCertification&amp;sRefID=<%=BoardCertification.getBoardCertificationID()%>&amp;sFieldNameDisp=<%=BoardCertification.getEnglish("BoardDateRecertified")%>&amp;sTableNameDisp=tBoardCertification','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (BoardCertification.isRequired("ExpirationDate",UserSecurityGroupID))&&(!BoardCertification.isComplete("ExpirationDate")) )
            {
                theClass = "requiredField";
            }
            else if ((BoardCertification.isExpired("ExpirationDate",expiredDays))&&(BoardCertification.isExpiredCheck("ExpirationDate",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>

            <%
            if ((BoardCertification.isWrite("ExpirationDate",UserSecurityGroupID)))
            {
                        %>
<tr><td colspan=2>&nbsp;</td></tr>
<tr>
                  <td colspan=2 class=instructions> If your certification does 
                    not expire, please enter &quot;n/a&quot;</td>
                </tr>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Expiration Date&nbsp;(mm/dd/yyyy):&nbsp;</b></p></td><td valign=top><p><input maxlength=20 type=text size="80" name="ExpirationDate" value='<jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(BoardCertification.getExpirationDate())%>" /></jsp:include>' >&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ExpirationDate&amp;sTableName=tBoardCertification&amp;sRefID=<%=BoardCertification.getBoardCertificationID()%>&amp;sFieldNameDisp=<%=BoardCertification.getEnglish("ExpirationDate")%>&amp;sTableNameDisp=tBoardCertification','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((BoardCertification.isRead("ExpirationDate",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Expiration Date&nbsp;(mm/dd/yyyy):&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(BoardCertification.getExpirationDate())%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ExpirationDate&amp;sTableName=tBoardCertification&amp;sRefID=<%=BoardCertification.getBoardCertificationID()%>&amp;sFieldNameDisp=<%=BoardCertification.getEnglish("ExpirationDate")%>&amp;sTableNameDisp=tBoardCertification','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (BoardCertification.isRequired("DocumentNumber",UserSecurityGroupID))&&(!BoardCertification.isComplete("DocumentNumber")) )
            {
                theClass = "requiredField";
            }
            else if ((BoardCertification.isExpired("DocumentNumber",expiredDays))&&(BoardCertification.isExpiredCheck("DocumentNumber",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((BoardCertification.isWrite("DocumentNumber",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Certificate/Document Number&nbsp;</b></p></td><td valign=top><p><input maxlength="100" type=text size="80" name="DocumentNumber" value="<%=BoardCertification.getDocumentNumber()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=DocumentNumber&amp;sTableName=tBoardCertification&amp;sRefID=<%=BoardCertification.getBoardCertificationID()%>&amp;sFieldNameDisp=<%=BoardCertification.getEnglish("DocumentNumber")%>&amp;sTableNameDisp=tBoardCertification','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((BoardCertification.isRead("DocumentNumber",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Certificate/Document Number&nbsp;</b></p></td><td valign=top><p><%=BoardCertification.getDocumentNumber()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=DocumentNumber&amp;sTableName=tBoardCertification&amp;sRefID=<%=BoardCertification.getBoardCertificationID()%>&amp;sFieldNameDisp=<%=BoardCertification.getEnglish("DocumentNumber")%>&amp;sTableNameDisp=tBoardCertification','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (BoardCertification.isRequired("IfNotCert",UserSecurityGroupID))&&(!BoardCertification.isComplete("IfNotCert")) )
            {
                theClass = "requiredField";
            }
            else if ((BoardCertification.isExpired("IfNotCert",expiredDays))&&(BoardCertification.isExpiredCheck("IfNotCert",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((BoardCertification.isWrite("IfNotCert",UserSecurityGroupID)))
            {
                        %>
                       <tr><td valign=top><p class=<%=theClass%> ><b><%=BoardCertification.getEnglish("IfNotCert")%>&nbsp;</b></p></td><td valign=top><p><textarea onKeyDown="textAreaStop(this,200)" rows="2" name="IfNotCert" cols="40" maxlength=200><%=BoardCertification.getIfNotCert()%></textarea>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=IfNotCert&amp;sTableName=tBoardCertification&amp;sRefID=<%=BoardCertification.getBoardCertificationID()%>&amp;sFieldNameDisp=<%=BoardCertification.getEnglish("IfNotCert")%>&amp;sTableNameDisp=tBoardCertification','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((BoardCertification.isRead("IfNotCert",UserSecurityGroupID)))
            {
                        %>
                       <tr><td valign=top> <p class=<%=theClass%> ><b><%=BoardCertification.getEnglish("IfNotCert")%>&nbsp;</b></p></td><td valign=top><p><%=BoardCertification.getIfNotCert()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=IfNotCert&amp;sTableName=tBoardCertification&amp;sRefID=<%=BoardCertification.getBoardCertificationID()%>&amp;sFieldNameDisp=<%=BoardCertification.getEnglish("IfNotCert")%>&amp;sTableNameDisp=tBoardCertification','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (BoardCertification.isRequired("CertEligible",UserSecurityGroupID))&&(!BoardCertification.isComplete("CertEligible")) )
            {
                theClass = "requiredField";
            }
            else if ((BoardCertification.isExpired("CertEligible",expiredDays))&&(BoardCertification.isExpiredCheck("CertEligible",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((BoardCertification.isWrite("CertEligible",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>If you are not certified, are you eligible to take this test?&nbsp;</b></p></td><td valign=top><p><select   name="CertEligible" ><jsp:include page="../generic/tYesNoLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=BoardCertification.getCertEligible()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CertEligible&amp;sTableName=tBoardCertification&amp;sRefID=<%=BoardCertification.getBoardCertificationID()%>&amp;sFieldNameDisp=<%=BoardCertification.getEnglish("CertEligible")%>&amp;sTableNameDisp=tBoardCertification','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((BoardCertification.isRead("CertEligible",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>If you are not certified, are you eligible to take this test?&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=BoardCertification.getCertEligible()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CertEligible&amp;sTableName=tBoardCertification&amp;sRefID=<%=BoardCertification.getBoardCertificationID()%>&amp;sFieldNameDisp=<%=BoardCertification.getEnglish("CertEligible")%>&amp;sTableNameDisp=tBoardCertification','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (BoardCertification.isRequired("EligibleStartDate",UserSecurityGroupID))&&(!BoardCertification.isComplete("EligibleStartDate")) )
            {
                theClass = "requiredField";
            }
            else if ((BoardCertification.isExpired("EligibleStartDate",expiredDays))&&(BoardCertification.isExpiredCheck("EligibleStartDate",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>

            <%
            if ((BoardCertification.isWrite("EligibleStartDate",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>If YES, please enter the date you were eligible&nbsp;(mm/dd/yyyy):&nbsp;</b></p></td><td valign=top><p><input maxlength=20 type=text size="80" name="EligibleStartDate" value='<jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(BoardCertification.getEligibleStartDate())%>" /></jsp:include>' >&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=EligibleStartDate&amp;sTableName=tBoardCertification&amp;sRefID=<%=BoardCertification.getBoardCertificationID()%>&amp;sFieldNameDisp=<%=BoardCertification.getEnglish("EligibleStartDate")%>&amp;sTableNameDisp=tBoardCertification','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((BoardCertification.isRead("EligibleStartDate",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>If YES, please enter the date you were eligible&nbsp;(mm/dd/yyyy):&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(BoardCertification.getEligibleStartDate())%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=EligibleStartDate&amp;sTableName=tBoardCertification&amp;sRefID=<%=BoardCertification.getBoardCertificationID()%>&amp;sFieldNameDisp=<%=BoardCertification.getEnglish("EligibleStartDate")%>&amp;sTableNameDisp=tBoardCertification','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (BoardCertification.isRequired("EligibleExpirationDate",UserSecurityGroupID))&&(!BoardCertification.isComplete("EligibleExpirationDate")) )
            {
                theClass = "requiredField";
            }
            else if ((BoardCertification.isExpired("EligibleExpirationDate",expiredDays))&&(BoardCertification.isExpiredCheck("EligibleExpirationDate",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>

            <%
            if ((BoardCertification.isWrite("EligibleExpirationDate",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top>
                    <p class=<%=theClass%> ><b>If YES, please enter the date your 
                      eligibility expires&nbsp;(mm/dd/yyyy):&nbsp;</b></p>
                  </td><td valign=top><p><input maxlength=20 type=text size="80" name="EligibleExpirationDate" value='<jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(BoardCertification.getEligibleExpirationDate())%>" /></jsp:include>' >&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=EligibleExpirationDate&amp;sTableName=tBoardCertification&amp;sRefID=<%=BoardCertification.getBoardCertificationID()%>&amp;sFieldNameDisp=<%=BoardCertification.getEnglish("EligibleExpirationDate")%>&amp;sTableNameDisp=tBoardCertification','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((BoardCertification.isRead("EligibleExpirationDate",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top>
                    <p class=<%=theClass%> ><b>If YES, please enter the date your 
                      eligibility expires&nbsp;(mm/dd/yyyy):&nbsp;</b></p>
                  </td><td valign=top><p><jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(BoardCertification.getEligibleExpirationDate())%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=EligibleExpirationDate&amp;sTableName=tBoardCertification&amp;sRefID=<%=BoardCertification.getBoardCertificationID()%>&amp;sFieldNameDisp=<%=BoardCertification.getEnglish("EligibleExpirationDate")%>&amp;sTableNameDisp=tBoardCertification','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (BoardCertification.isRequired("CertPlanningToTake",UserSecurityGroupID))&&(!BoardCertification.isComplete("CertPlanningToTake")) )
            {
                theClass = "requiredField";
            }
            else if ((BoardCertification.isExpired("CertPlanningToTake",expiredDays))&&(BoardCertification.isExpiredCheck("CertPlanningToTake",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((BoardCertification.isWrite("CertPlanningToTake",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Are you planning on taking this test?&nbsp;</b></p></td><td valign=top><p><select   name="CertPlanningToTake" ><jsp:include page="../generic/tYesNoLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=BoardCertification.getCertPlanningToTake()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CertPlanningToTake&amp;sTableName=tBoardCertification&amp;sRefID=<%=BoardCertification.getBoardCertificationID()%>&amp;sFieldNameDisp=<%=BoardCertification.getEnglish("CertPlanningToTake")%>&amp;sTableNameDisp=tBoardCertification','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((BoardCertification.isRead("CertPlanningToTake",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Are you planning on taking this test?&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=BoardCertification.getCertPlanningToTake()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CertPlanningToTake&amp;sTableName=tBoardCertification&amp;sRefID=<%=BoardCertification.getBoardCertificationID()%>&amp;sFieldNameDisp=<%=BoardCertification.getEnglish("CertPlanningToTake")%>&amp;sTableNameDisp=tBoardCertification','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (BoardCertification.isRequired("CertPlanDate",UserSecurityGroupID))&&(!BoardCertification.isComplete("CertPlanDate")) )
            {
                theClass = "requiredField";
            }
            else if ((BoardCertification.isExpired("CertPlanDate",expiredDays))&&(BoardCertification.isExpiredCheck("CertPlanDate",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>

            <%
            if ((BoardCertification.isWrite("CertPlanDate",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>If so, when are you planning on taking your certification test?&nbsp;(mm/dd/yyyy):&nbsp;</b></p></td><td valign=top><p><input maxlength=20 type=text size="80" name="CertPlanDate" value='<jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(BoardCertification.getCertPlanDate())%>" /></jsp:include>' >&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CertPlanDate&amp;sTableName=tBoardCertification&amp;sRefID=<%=BoardCertification.getBoardCertificationID()%>&amp;sFieldNameDisp=<%=BoardCertification.getEnglish("CertPlanDate")%>&amp;sTableNameDisp=tBoardCertification','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((BoardCertification.isRead("CertPlanDate",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>If so, when are you planning on taking your certification test?&nbsp;(mm/dd/yyyy):&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(BoardCertification.getCertPlanDate())%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CertPlanDate&amp;sTableName=tBoardCertification&amp;sRefID=<%=BoardCertification.getBoardCertificationID()%>&amp;sFieldNameDisp=<%=BoardCertification.getEnglish("CertPlanDate")%>&amp;sTableNameDisp=tBoardCertification','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (BoardCertification.isRequired("DocuLinkID",UserSecurityGroupID))&&(!BoardCertification.isComplete("DocuLinkID")) )
            {
                theClass = "requiredField";
            }
            else if ((BoardCertification.isExpired("DocuLinkID",expiredDays))&&(BoardCertification.isExpiredCheck("DocuLinkID",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((BoardCertification.isWrite("DocuLinkID",UserSecurityGroupID)))
            {
                        if (BoardCertification.getDocuLinkID().intValue()>0)
                        {
                            bltDocumentManagement myDoc = new bltDocumentManagement(BoardCertification.getDocuLinkID());
			    
                            if (!myDoc.getDocumentFileName().equalsIgnoreCase(""))
                            {
				    pageControllerHash.put("sFileName",ConfigurationInformation.sLinkedPDFDirectory + "\\" + myDoc.getDocumentFileName());
				    pageControllerHash.put("sDownloadName",myDoc.getDocumentFileName());
				    pageControllerHash.put("bDownload",new Boolean(false));
				    session.setAttribute("pageControllerHash",pageControllerHash);
                            %>
                            <tr><td valign=top><p class=<%=theClass%> ><b>Attached Document:&nbsp;</p></td><td valign=top><p><a target=_blank href="fileRetrieve.jsp">view</a></b></p></td></tr>
                            <%
                           }
                            else
                            {
                            %>

                            <%
                            }
                        }
            }
            else if ((BoardCertification.isRead("DocuLinkID",UserSecurityGroupID)))
            {
                        bltDocumentManagement myDoc = new bltDocumentManagement(BoardCertification.getDocuLinkID());
                            if (!myDoc.getDocumentFileName().equalsIgnoreCase(""))
                            {
				    pageControllerHash.put("sFileName",ConfigurationInformation.sLinkedPDFDirectory + "\\" + myDoc.getDocumentFileName());
				    pageControllerHash.put("sDownloadName",myDoc.getDocumentFileName());
				    pageControllerHash.put("bDownload",new Boolean(false));
				    session.setAttribute("pageControllerHash",pageControllerHash);
                            %>
                            <tr><td valign=top><p class=<%=theClass%> ><b>Attached Document:&nbsp;</p></td><td valign=top><p><a target=_blank href="fileRetrieve.jsp">view</a></b></p></td></tr>
                            <%
                           }
                        else
                        {
                        %>

                        <%
                        }
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (BoardCertification.isRequired("Comments",UserSecurityGroupID))&&(!BoardCertification.isComplete("Comments")) )
            {
                theClass = "requiredField";
            }
            else if ((BoardCertification.isExpired("Comments",expiredDays))&&(BoardCertification.isExpiredCheck("Comments",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((BoardCertification.isWrite("Comments",UserSecurityGroupID)))
            {
                        %>
                       <tr><td valign=top><p class=<%=theClass%> ><b><%=BoardCertification.getEnglish("Comments")%>&nbsp;</b></p></td><td valign=top><p><textarea onKeyDown="textAreaStop(this,200)" rows="2" name="Comments" cols="40" maxlength=200><%=BoardCertification.getComments()%></textarea>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Comments&amp;sTableName=tBoardCertification&amp;sRefID=<%=BoardCertification.getBoardCertificationID()%>&amp;sFieldNameDisp=<%=BoardCertification.getEnglish("Comments")%>&amp;sTableNameDisp=tBoardCertification','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((BoardCertification.isRead("Comments",UserSecurityGroupID)))
            {
                        %>
                       <tr><td valign=top> <p class=<%=theClass%> ><b><%=BoardCertification.getEnglish("Comments")%>&nbsp;</b></p></td><td valign=top><p><%=BoardCertification.getComments()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Comments&amp;sTableName=tBoardCertification&amp;sRefID=<%=BoardCertification.getBoardCertificationID()%>&amp;sFieldNameDisp=<%=BoardCertification.getEnglish("Comments")%>&amp;sTableNameDisp=tBoardCertification','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>




            <tr><td width=40%>&nbsp;</td><td width=60%>&nbsp;</td></tr>
            </table>
        <%if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {%>
            <input type=hidden name=routePageReference value="sParentReturnPage">
             <%
              if (request.getParameter("INTNext")!=null&&request.getParameter("INTNext").equalsIgnoreCase("yes") ) 
              {
              %>
                  <table width=75% border=1 bordercolor=333333 align=left cellspacing=0 cellpadding=0 class=wizardTable>
                  <tr class=requiredField><td>
                  <input  <%=HTMLFormStyleButton%> type="radio" value="next" name="INTNext" checked>&nbsp;&nbsp;<%=ConfigurationMessages.getInterviewMessage("INTERVIEWNoMore","tBoardCertification")%>
                  <br>
                  <input  <%=HTMLFormStyleButton%> type="radio" value="yes" name="INTNext">&nbsp;&nbsp;<%=ConfigurationMessages.getInterviewMessage("INTERVIEWAddMore","tBoardCertification")%>
                  </td></tr></table><br><br><br>
              <%
              }
              %>
            <p><input <%=HTMLFormStyleButton%> type=Submit value="Continue" name=Submit></p>
        <%}%>
        </td></tr></table>
        </form>
        <%
  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>

    </td></tr></table>



<%String bnIncludeFN = "ui_"+thePLCID+"\\bot-nav_PhysicianID.jsp";
%>
<jsp:include page="<%=bnIncludeFN%>" flush="true" ><jsp:param name="plcID" value="<%=thePLCID%>"/></jsp:include>
