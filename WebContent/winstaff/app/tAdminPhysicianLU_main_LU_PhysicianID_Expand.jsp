<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.bltDocumentManagement,java.util.Vector,java.util.Enumeration,com.winstaff.ListElement, com.winstaff.PLCUtils, com.winstaff.SecurityCheck, com.winstaff.ConfigurationMessages,com.winstaff.bltAdminPhysicianLU,com.winstaff.bltAdminPhysicianLU_List_LU_PhysicianID" %>
<%/*
    filename: tAdminPhysicianLU_main_LU_PhysicianID.jsp
    Created on Mar/21/2003
    Type: 1-n main class file
    Created by: Scott Ellis
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>

<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_PhysicianID.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>

    <table cellpadding=0 cellspacing=0 border=0 width=<%=MasterTableWidth%> >
    <tr><td width=10>&nbsp;</td><td>
    <%=ConfigurationMessages.getHTML("INTERVIEWTopControl","tAdminPhysicianLU")%>

    <br><a href="tAdminPhysicianLU_main_LU_PhysicianID.jsp">Compact</a>

<%
//initial declaration of list class and parentID
    Integer        iPhysicianID        =    null;
    boolean accessValid = false;
   Integer iSecurityCheck = SecurityCheck.CheckItem("Admin1", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {
    if (pageControllerHash.containsKey("iPhysicianID")) 
    {
        iPhysicianID        =    (Integer)pageControllerHash.get("iPhysicianID");
        accessValid = true;
    }
  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);
      pageControllerHash.put("sParentReturnPage","tAdminPhysicianLU_main_LU_PhysicianID_Expand.jsp");
    pageControllerHash.remove("iLookupID");
    session.setAttribute("pageControllerHash",pageControllerHash);

    bltAdminPhysicianLU_List_LU_PhysicianID        bltAdminPhysicianLU_List_LU_PhysicianID        =    new    bltAdminPhysicianLU_List_LU_PhysicianID(iPhysicianID);

//declaration of Enumeration
    bltAdminPhysicianLU        working_bltAdminPhysicianLU;
    ListElement         leCurrentElement;
    java.util.Enumeration eList = bltAdminPhysicianLU_List_LU_PhysicianID.elements();
    %>
        <%@ include file="tAdminPhysicianLU_main_LU_PhysicianID_instructions.jsp" %>

        <%if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {%>
        <a class=linkBase href = "tAdminPhysicianLU_main_LU_PhysicianID_form_create.jsp?EDIT=new&KM=s"><img border=0 src="ui_<%=thePLCID%>/icons/create_PhysicianID.gif"></a>
        <%}%>
    <%
    while (eList.hasMoreElements())
    {
         %>
         <table border="0" bordercolor="333333" cellpadding="3" class=tdHeaderAlt cellspacing="0" width="100%">
            <tr>
              <td width="50%">
                   <table border="0" bordercolor="333333" cellpadding="3" class=tdHeaderAlt cellspacing="0" width="100%">
                     <tr> 
                       <td>
         <%

        leCurrentElement    = (ListElement) eList.nextElement();
        working_bltAdminPhysicianLU  = (bltAdminPhysicianLU) leCurrentElement.getObject();
        working_bltAdminPhysicianLU.GroupSecurityInit(UserSecurityGroupID);
        String theClass = "tdHeader";
        if (!working_bltAdminPhysicianLU.isComplete())
        {
            theClass = "incompleteItem";
        %>
                <span class=incompleteItem><b>Not Complete</b></span><br>
        <%
        }
        %>
               <span class=<%=theClass%> ><b>ID:&nbsp;</b><%=working_bltAdminPhysicianLU.getLookupID()%> </span>
                  </td></tr>
                  <tr><td><b>Item Create Date:&nbsp;</b><jsp:include page="../generic/DateTypeConvert_Main.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(working_bltAdminPhysicianLU.getUniqueCreateDate())%>" /></jsp:include></td></tr>
                  <tr><td><b>Item Modify Date:&nbsp;</b><jsp:include page="../generic/DateTypeConvert_Main.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(working_bltAdminPhysicianLU.getUniqueModifyDate())%>" /></jsp:include></td></tr>
                  <tr><td><b>Item Modify Comments:&nbsp;</b><%=working_bltAdminPhysicianLU.getUniqueModifyComments()%></td></tr>
                  </td></tr></table>
            </td><td width="50%" bgColor=#ffffff> 
        <a class=linkBase href = "tAdminPhysicianLU_main_LU_PhysicianID_form_authorize.jsp?EDIT=edit&EDITID=<%=working_bltAdminPhysicianLU.getLookupID()%>&KM=s"><img border=0 src="ui_<%=thePLCID%>/icons/edit_PhysicianID.gif"></a>


        <%if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {%>
        <br><a class=linkBase  onClick="return confirmDelete()"  href = "tAdminPhysicianLU_main_LU_PhysicianID_form_authorize.jsp?EDIT=del&EDITID=<%=working_bltAdminPhysicianLU.getLookupID()%>&KM=p"><img border=0 src="ui_<%=thePLCID%>/icons/delete_PhysicianID.gif"></a>

        <%}%>
                  </td></tr>
                 </table>
                 <table width="100%" border="1" cellspacing="0" bordercolor="#333333">
                  <tr>
                   <td>
                     <table width="100%" border="0" cellspacing="0">
                     <tr><td>
<%String theClassF = "textBase";%>

<%theClassF = "textBase";%>
<%if ((working_bltAdminPhysicianLU.isExpired("PhysicianID",expiredDays))&&(working_bltAdminPhysicianLU.isExpiredCheck("PhysicianID"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltAdminPhysicianLU.isRequired("PhysicianID"))&&(!working_bltAdminPhysicianLU.isComplete("PhysicianID")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>PhysicianID:&nbsp;</b><%=working_bltAdminPhysicianLU.getPhysicianID()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltAdminPhysicianLU.isExpired("Comments",expiredDays))&&(working_bltAdminPhysicianLU.isExpiredCheck("Comments"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltAdminPhysicianLU.isRequired("Comments"))&&(!working_bltAdminPhysicianLU.isComplete("Comments")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Extra Comments:&nbsp;</b><%=working_bltAdminPhysicianLU.getComments()%></p>

        </td></tr></table></table><br>        <%
    }
    %>
    </table>
    <%
  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>

    </td></tr></table>


<%String bnIncludeFN = "ui_"+thePLCID+"\\bot-nav_PhysicianID.jsp";
%>
<jsp:include page="<%=bnIncludeFN%>" flush="true" ><jsp:param name="plcID" value="<%=thePLCID%>"/></jsp:include>
