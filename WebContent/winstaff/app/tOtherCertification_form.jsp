<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.ConfigurationInformation,com.winstaff.bltDocumentManagement,java.util.Vector,java.util.Enumeration,com.winstaff.ListElement, com.winstaff.PLCUtils, com.winstaff.SecurityCheck, com.winstaff.ConfigurationMessages,com.winstaff.bltOtherCertification" %>
<%/*

    filename: out\jsp\tOtherCertification_form.jsp
    Created on Mar/21/2003
    Created by: Scott Ellis
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>

<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_PhysicianID.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>


    <table cellpadding=0 cellspacing=0 border=0 width=<%=MasterTableWidth%> >
    <tr><td width=10>&nbsp;</td><td>
    <%=ConfigurationMessages.getHTML("INTERVIEWTopControl_form","tOtherCertification")%>



<%
//initial declaration of list class and parentID
    Integer        iOtherCertID        =    null;
    boolean accessValid = false;
    // required for Type2
    String sKeyMasterReference = null;

   Integer iSecurityCheck = SecurityCheck.CheckItem("PractitionerSection5", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {
    if (pageControllerHash.containsKey("iOtherCertID")) 
    {
        iOtherCertID        =    (Integer)pageControllerHash.get("iOtherCertID");
        accessValid = true;    }
  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);      java.text.SimpleDateFormat displayDateTimeSDF = new java.text.SimpleDateFormat("MM/dd/yyyy hh:mm a");

    pageControllerHash.put("sLocalChildReturnPage","tOtherCertification_form.jsp");
//initial declaration of list class and parentID

    bltOtherCertification        OtherCertification        =    null;

    if ( request.getParameter( "EDIT" ).equalsIgnoreCase("edit") )
    {
        OtherCertification        =    new    bltOtherCertification(iOtherCertID,UserSecurityGroupID);
    }
    else if ( request.getParameter( "EDIT" ).equalsIgnoreCase("new") )
    {
        OtherCertification        =    new    bltOtherCertification(UserSecurityGroupID, true);
    }

//fields
        %>
        <%@ include file="tOtherCertification_form_instructions.jsp" %>
        <form action="tOtherCertification_form_sub.jsp" name="tOtherCertification_form1" method="POST">
<%
    if ( request.getParameter( "EDIT" ) != null )
    {
    %>
        <input type="hidden" name="EDIT" value = "<%=request.getParameter( "EDIT" )%>" >  
    <%
    }
%>
          <%  String theClass ="tdBase";%>
        <table width=100% border=1 bordercolor=#333333 cellpadding=3 cellspacing=0 class=tableBase>
         <tr><td class=tableColor>


            <table cellpadding=0 cellspacing=0 width=100%>
            <%
            if ( (OtherCertification.isRequired("CertificationType",UserSecurityGroupID))&&(!OtherCertification.isComplete("CertificationType")) )
            {
                theClass = "requiredField";
            }
            else if ((OtherCertification.isExpired("CertificationType",expiredDays))&&(OtherCertification.isExpiredCheck("CertificationType",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((OtherCertification.isWrite("CertificationType",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Type&nbsp;</b></p></td><td valign=top><p><input maxlength="100" type=text size="80" name="CertificationType" value="<%=OtherCertification.getCertificationType()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CertificationType&amp;sTableName=tOtherCertification&amp;sRefID=<%=OtherCertification.getOtherCertID()%>&amp;sFieldNameDisp=<%=OtherCertification.getEnglish("CertificationType")%>&amp;sTableNameDisp=tOtherCertification','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((OtherCertification.isRead("CertificationType",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Type&nbsp;</b></p></td><td valign=top><p><%=OtherCertification.getCertificationType()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CertificationType&amp;sTableName=tOtherCertification&amp;sRefID=<%=OtherCertification.getOtherCertID()%>&amp;sFieldNameDisp=<%=OtherCertification.getEnglish("CertificationType")%>&amp;sTableNameDisp=tOtherCertification','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (OtherCertification.isRequired("CertificationNumber",UserSecurityGroupID))&&(!OtherCertification.isComplete("CertificationNumber")) )
            {
                theClass = "requiredField";
            }
            else if ((OtherCertification.isExpired("CertificationNumber",expiredDays))&&(OtherCertification.isExpiredCheck("CertificationNumber",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((OtherCertification.isWrite("CertificationNumber",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Certificate/Document Number&nbsp;</b></p></td><td valign=top><p><input maxlength="100" type=text size="80" name="CertificationNumber" value="<%=OtherCertification.getCertificationNumber()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CertificationNumber&amp;sTableName=tOtherCertification&amp;sRefID=<%=OtherCertification.getOtherCertID()%>&amp;sFieldNameDisp=<%=OtherCertification.getEnglish("CertificationNumber")%>&amp;sTableNameDisp=tOtherCertification','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((OtherCertification.isRead("CertificationNumber",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Certificate/Document Number&nbsp;</b></p></td><td valign=top><p><%=OtherCertification.getCertificationNumber()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CertificationNumber&amp;sTableName=tOtherCertification&amp;sRefID=<%=OtherCertification.getOtherCertID()%>&amp;sFieldNameDisp=<%=OtherCertification.getEnglish("CertificationNumber")%>&amp;sTableNameDisp=tOtherCertification','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (OtherCertification.isRequired("ExpirationDate",UserSecurityGroupID))&&(!OtherCertification.isComplete("ExpirationDate")) )
            {
                theClass = "requiredField";
            }
            else if ((OtherCertification.isExpired("ExpirationDate",expiredDays))&&(OtherCertification.isExpiredCheck("ExpirationDate",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>

            <%
            if ((OtherCertification.isWrite("ExpirationDate",UserSecurityGroupID)))
            {
                        %>
<tr><td colspan=2>&nbsp;</td></tr>
<tr>
                  <td colspan=2 class=instructions>If this certification does not expire please enter "Current" or "n/a"</td>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Expiration Date&nbsp;(mm/dd/yyyy):&nbsp;</b></p></td><td valign=top><p><input maxlength=20  type=text size="80" name="ExpirationDate" value='<jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(OtherCertification.getExpirationDate())%>" /></jsp:include>' >&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ExpirationDate&amp;sTableName=tOtherCertification&amp;sRefID=<%=OtherCertification.getOtherCertID()%>&amp;sFieldNameDisp=<%=OtherCertification.getEnglish("ExpirationDate")%>&amp;sTableNameDisp=tOtherCertification','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((OtherCertification.isRead("ExpirationDate",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Expiration Date&nbsp;(mm/dd/yyyy):&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(OtherCertification.getExpirationDate())%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ExpirationDate&amp;sTableName=tOtherCertification&amp;sRefID=<%=OtherCertification.getOtherCertID()%>&amp;sFieldNameDisp=<%=OtherCertification.getEnglish("ExpirationDate")%>&amp;sTableNameDisp=tOtherCertification','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (OtherCertification.isRequired("Name",UserSecurityGroupID))&&(!OtherCertification.isComplete("Name")) )
            {
                theClass = "requiredField";
            }
            else if ((OtherCertification.isExpired("Name",expiredDays))&&(OtherCertification.isExpiredCheck("Name",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((OtherCertification.isWrite("Name",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Issuing Institution&nbsp;</b></p></td><td valign=top><p><input maxlength="100" type=text size="80" name="Name" value="<%=OtherCertification.getName()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Name&amp;sTableName=tOtherCertification&amp;sRefID=<%=OtherCertification.getOtherCertID()%>&amp;sFieldNameDisp=<%=OtherCertification.getEnglish("Name")%>&amp;sTableNameDisp=tOtherCertification','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((OtherCertification.isRead("Name",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Issuing Institution&nbsp;</b></p></td><td valign=top><p><%=OtherCertification.getName()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Name&amp;sTableName=tOtherCertification&amp;sRefID=<%=OtherCertification.getOtherCertID()%>&amp;sFieldNameDisp=<%=OtherCertification.getEnglish("Name")%>&amp;sTableNameDisp=tOtherCertification','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (OtherCertification.isRequired("Address1",UserSecurityGroupID))&&(!OtherCertification.isComplete("Address1")) )
            {
                theClass = "requiredField";
            }
            else if ((OtherCertification.isExpired("Address1",expiredDays))&&(OtherCertification.isExpiredCheck("Address1",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((OtherCertification.isWrite("Address1",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Address&nbsp;</b></p></td><td valign=top><p><input maxlength="50" type=text size="80" name="Address1" value="<%=OtherCertification.getAddress1()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Address1&amp;sTableName=tOtherCertification&amp;sRefID=<%=OtherCertification.getOtherCertID()%>&amp;sFieldNameDisp=<%=OtherCertification.getEnglish("Address1")%>&amp;sTableNameDisp=tOtherCertification','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((OtherCertification.isRead("Address1",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Address&nbsp;</b></p></td><td valign=top><p><%=OtherCertification.getAddress1()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Address1&amp;sTableName=tOtherCertification&amp;sRefID=<%=OtherCertification.getOtherCertID()%>&amp;sFieldNameDisp=<%=OtherCertification.getEnglish("Address1")%>&amp;sTableNameDisp=tOtherCertification','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (OtherCertification.isRequired("Address2",UserSecurityGroupID))&&(!OtherCertification.isComplete("Address2")) )
            {
                theClass = "requiredField";
            }
            else if ((OtherCertification.isExpired("Address2",expiredDays))&&(OtherCertification.isExpiredCheck("Address2",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((OtherCertification.isWrite("Address2",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Address 2&nbsp;</b></p></td><td valign=top><p><input maxlength="20" type=text size="80" name="Address2" value="<%=OtherCertification.getAddress2()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Address2&amp;sTableName=tOtherCertification&amp;sRefID=<%=OtherCertification.getOtherCertID()%>&amp;sFieldNameDisp=<%=OtherCertification.getEnglish("Address2")%>&amp;sTableNameDisp=tOtherCertification','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((OtherCertification.isRead("Address2",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Address 2&nbsp;</b></p></td><td valign=top><p><%=OtherCertification.getAddress2()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Address2&amp;sTableName=tOtherCertification&amp;sRefID=<%=OtherCertification.getOtherCertID()%>&amp;sFieldNameDisp=<%=OtherCertification.getEnglish("Address2")%>&amp;sTableNameDisp=tOtherCertification','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (OtherCertification.isRequired("City",UserSecurityGroupID))&&(!OtherCertification.isComplete("City")) )
            {
                theClass = "requiredField";
            }
            else if ((OtherCertification.isExpired("City",expiredDays))&&(OtherCertification.isExpiredCheck("City",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((OtherCertification.isWrite("City",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Institution City&nbsp;</b></p></td><td valign=top><p><input maxlength="30" type=text size="80" name="City" value="<%=OtherCertification.getCity()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=City&amp;sTableName=tOtherCertification&amp;sRefID=<%=OtherCertification.getOtherCertID()%>&amp;sFieldNameDisp=<%=OtherCertification.getEnglish("City")%>&amp;sTableNameDisp=tOtherCertification','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((OtherCertification.isRead("City",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Institution City&nbsp;</b></p></td><td valign=top><p><%=OtherCertification.getCity()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=City&amp;sTableName=tOtherCertification&amp;sRefID=<%=OtherCertification.getOtherCertID()%>&amp;sFieldNameDisp=<%=OtherCertification.getEnglish("City")%>&amp;sTableNameDisp=tOtherCertification','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (OtherCertification.isRequired("StateID",UserSecurityGroupID))&&(!OtherCertification.isComplete("StateID")) )
            {
                theClass = "requiredField";
            }
            else if ((OtherCertification.isExpired("StateID",expiredDays))&&(OtherCertification.isExpiredCheck("StateID",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((OtherCertification.isWrite("StateID",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>State&nbsp;</b></p></td><td valign=top><p><select   name="StateID" ><jsp:include page="../generic/tStateLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=OtherCertification.getStateID()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=StateID&amp;sTableName=tOtherCertification&amp;sRefID=<%=OtherCertification.getOtherCertID()%>&amp;sFieldNameDisp=<%=OtherCertification.getEnglish("StateID")%>&amp;sTableNameDisp=tOtherCertification','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((OtherCertification.isRead("StateID",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>State&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tStateLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=OtherCertification.getStateID()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=StateID&amp;sTableName=tOtherCertification&amp;sRefID=<%=OtherCertification.getOtherCertID()%>&amp;sFieldNameDisp=<%=OtherCertification.getEnglish("StateID")%>&amp;sTableNameDisp=tOtherCertification','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (OtherCertification.isRequired("Province",UserSecurityGroupID))&&(!OtherCertification.isComplete("Province")) )
            {
                theClass = "requiredField";
            }
            else if ((OtherCertification.isExpired("Province",expiredDays))&&(OtherCertification.isExpiredCheck("Province",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((OtherCertification.isWrite("Province",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Province, District, State&nbsp;</b></p></td><td valign=top><p><input maxlength="100" type=text size="80" name="Province" value="<%=OtherCertification.getProvince()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Province&amp;sTableName=tOtherCertification&amp;sRefID=<%=OtherCertification.getOtherCertID()%>&amp;sFieldNameDisp=<%=OtherCertification.getEnglish("Province")%>&amp;sTableNameDisp=tOtherCertification','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((OtherCertification.isRead("Province",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Province, District, State&nbsp;</b></p></td><td valign=top><p><%=OtherCertification.getProvince()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Province&amp;sTableName=tOtherCertification&amp;sRefID=<%=OtherCertification.getOtherCertID()%>&amp;sFieldNameDisp=<%=OtherCertification.getEnglish("Province")%>&amp;sTableNameDisp=tOtherCertification','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (OtherCertification.isRequired("ZIP",UserSecurityGroupID))&&(!OtherCertification.isComplete("ZIP")) )
            {
                theClass = "requiredField";
            }
            else if ((OtherCertification.isExpired("ZIP",expiredDays))&&(OtherCertification.isExpiredCheck("ZIP",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((OtherCertification.isWrite("ZIP",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>ZIP&nbsp;</b></p></td><td valign=top><p><input maxlength="50" type=text size="80" name="ZIP" value="<%=OtherCertification.getZIP()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ZIP&amp;sTableName=tOtherCertification&amp;sRefID=<%=OtherCertification.getOtherCertID()%>&amp;sFieldNameDisp=<%=OtherCertification.getEnglish("ZIP")%>&amp;sTableNameDisp=tOtherCertification','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((OtherCertification.isRead("ZIP",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>ZIP&nbsp;</b></p></td><td valign=top><p><%=OtherCertification.getZIP()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ZIP&amp;sTableName=tOtherCertification&amp;sRefID=<%=OtherCertification.getOtherCertID()%>&amp;sFieldNameDisp=<%=OtherCertification.getEnglish("ZIP")%>&amp;sTableNameDisp=tOtherCertification','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (OtherCertification.isRequired("CountryID",UserSecurityGroupID))&&(!OtherCertification.isComplete("CountryID")) )
            {
                theClass = "requiredField";
            }
            else if ((OtherCertification.isExpired("CountryID",expiredDays))&&(OtherCertification.isExpiredCheck("CountryID",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((OtherCertification.isWrite("CountryID",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Country&nbsp;</b></p></td><td valign=top><p><select   name="CountryID" ><jsp:include page="../generic/tCountryLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=OtherCertification.getCountryID()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CountryID&amp;sTableName=tOtherCertification&amp;sRefID=<%=OtherCertification.getOtherCertID()%>&amp;sFieldNameDisp=<%=OtherCertification.getEnglish("CountryID")%>&amp;sTableNameDisp=tOtherCertification','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((OtherCertification.isRead("CountryID",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Country&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tCountryLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=OtherCertification.getCountryID()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CountryID&amp;sTableName=tOtherCertification&amp;sRefID=<%=OtherCertification.getOtherCertID()%>&amp;sFieldNameDisp=<%=OtherCertification.getEnglish("CountryID")%>&amp;sTableNameDisp=tOtherCertification','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (OtherCertification.isRequired("Phone",UserSecurityGroupID))&&(!OtherCertification.isComplete("Phone")) )
            {
                theClass = "requiredField";
            }
            else if ((OtherCertification.isExpired("Phone",expiredDays))&&(OtherCertification.isExpiredCheck("Phone",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((OtherCertification.isWrite("Phone",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Phone (XXX-XXX-XXXX)&nbsp;</b></p></td><td valign=top><p><input maxlength="50" type=text size="80" name="Phone" value="<%=OtherCertification.getPhone()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Phone&amp;sTableName=tOtherCertification&amp;sRefID=<%=OtherCertification.getOtherCertID()%>&amp;sFieldNameDisp=<%=OtherCertification.getEnglish("Phone")%>&amp;sTableNameDisp=tOtherCertification','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((OtherCertification.isRead("Phone",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Phone (XXX-XXX-XXXX)&nbsp;</b></p></td><td valign=top><p><%=OtherCertification.getPhone()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Phone&amp;sTableName=tOtherCertification&amp;sRefID=<%=OtherCertification.getOtherCertID()%>&amp;sFieldNameDisp=<%=OtherCertification.getEnglish("Phone")%>&amp;sTableNameDisp=tOtherCertification','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (OtherCertification.isRequired("Fax",UserSecurityGroupID))&&(!OtherCertification.isComplete("Fax")) )
            {
                theClass = "requiredField";
            }
            else if ((OtherCertification.isExpired("Fax",expiredDays))&&(OtherCertification.isExpiredCheck("Fax",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((OtherCertification.isWrite("Fax",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Fax (XXX-XXX-XXXX)&nbsp;</b></p></td><td valign=top><p><input maxlength="50" type=text size="80" name="Fax" value="<%=OtherCertification.getFax()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Fax&amp;sTableName=tOtherCertification&amp;sRefID=<%=OtherCertification.getOtherCertID()%>&amp;sFieldNameDisp=<%=OtherCertification.getEnglish("Fax")%>&amp;sTableNameDisp=tOtherCertification','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((OtherCertification.isRead("Fax",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Fax (XXX-XXX-XXXX)&nbsp;</b></p></td><td valign=top><p><%=OtherCertification.getFax()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Fax&amp;sTableName=tOtherCertification&amp;sRefID=<%=OtherCertification.getOtherCertID()%>&amp;sFieldNameDisp=<%=OtherCertification.getEnglish("Fax")%>&amp;sTableNameDisp=tOtherCertification','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (OtherCertification.isRequired("ContactName",UserSecurityGroupID))&&(!OtherCertification.isComplete("ContactName")) )
            {
                theClass = "requiredField";
            }
            else if ((OtherCertification.isExpired("ContactName",expiredDays))&&(OtherCertification.isExpiredCheck("ContactName",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((OtherCertification.isWrite("ContactName",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Contact Name&nbsp;</b></p></td><td valign=top><p><input maxlength="50" type=text size="80" name="ContactName" value="<%=OtherCertification.getContactName()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ContactName&amp;sTableName=tOtherCertification&amp;sRefID=<%=OtherCertification.getOtherCertID()%>&amp;sFieldNameDisp=<%=OtherCertification.getEnglish("ContactName")%>&amp;sTableNameDisp=tOtherCertification','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((OtherCertification.isRead("ContactName",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Contact Name&nbsp;</b></p></td><td valign=top><p><%=OtherCertification.getContactName()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ContactName&amp;sTableName=tOtherCertification&amp;sRefID=<%=OtherCertification.getOtherCertID()%>&amp;sFieldNameDisp=<%=OtherCertification.getEnglish("ContactName")%>&amp;sTableNameDisp=tOtherCertification','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (OtherCertification.isRequired("ContactEmail",UserSecurityGroupID))&&(!OtherCertification.isComplete("ContactEmail")) )
            {
                theClass = "requiredField";
            }
            else if ((OtherCertification.isExpired("ContactEmail",expiredDays))&&(OtherCertification.isExpiredCheck("ContactEmail",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((OtherCertification.isWrite("ContactEmail",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Contact E-mail&nbsp;</b></p></td><td valign=top><p><input maxlength="75" type=text size="80" name="ContactEmail" value="<%=OtherCertification.getContactEmail()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ContactEmail&amp;sTableName=tOtherCertification&amp;sRefID=<%=OtherCertification.getOtherCertID()%>&amp;sFieldNameDisp=<%=OtherCertification.getEnglish("ContactEmail")%>&amp;sTableNameDisp=tOtherCertification','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((OtherCertification.isRead("ContactEmail",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Contact E-mail&nbsp;</b></p></td><td valign=top><p><%=OtherCertification.getContactEmail()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ContactEmail&amp;sTableName=tOtherCertification&amp;sRefID=<%=OtherCertification.getOtherCertID()%>&amp;sFieldNameDisp=<%=OtherCertification.getEnglish("ContactEmail")%>&amp;sTableNameDisp=tOtherCertification','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (OtherCertification.isRequired("DocuLinkID",UserSecurityGroupID))&&(!OtherCertification.isComplete("DocuLinkID")) )
            {
                theClass = "requiredField";
            }
            else if ((OtherCertification.isExpired("DocuLinkID",expiredDays))&&(OtherCertification.isExpiredCheck("DocuLinkID",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((OtherCertification.isWrite("DocuLinkID",UserSecurityGroupID)))
            {
                        if (OtherCertification.getDocuLinkID().intValue()>0)
                        {
                            bltDocumentManagement myDoc = new bltDocumentManagement(OtherCertification.getDocuLinkID());
                            if (!myDoc.getDocumentFileName().equalsIgnoreCase(""))
                            {
				    pageControllerHash.put("sFileName",ConfigurationInformation.sLinkedPDFDirectory + "\\" + myDoc.getDocumentFileName());
				    pageControllerHash.put("sDownloadName",myDoc.getDocumentFileName());
				    pageControllerHash.put("bDownload",new Boolean(false));
				    session.setAttribute("pageControllerHash",pageControllerHash);
                            %>
                            <tr><td valign=top><p class=<%=theClass%> ><b>Attached Document:&nbsp;</p></td><td valign=top><p><a target=_blank href="fileRetrieve.jsp">view</a></b></p></td></tr>
                            <%
                           }
                            else
                            {
                            %>

                            <%
                            }
                        }
            }
            else if ((OtherCertification.isRead("DocuLinkID",UserSecurityGroupID)))
            {
                        bltDocumentManagement myDoc = new bltDocumentManagement(OtherCertification.getDocuLinkID());
                            if (!myDoc.getDocumentFileName().equalsIgnoreCase(""))
                            {
				    pageControllerHash.put("sFileName",ConfigurationInformation.sLinkedPDFDirectory + "\\" + myDoc.getDocumentFileName());
				    pageControllerHash.put("sDownloadName",myDoc.getDocumentFileName());
				    pageControllerHash.put("bDownload",new Boolean(false));
				    session.setAttribute("pageControllerHash",pageControllerHash);
                            %>
                            <tr><td valign=top><p class=<%=theClass%> ><b>Attached Document:&nbsp;</p></td><td valign=top><p><a target=_blank href="fileRetrieve.jsp">view</a></b></p></td></tr>
                            <%
                           }
                        else
                        {
                        %>

                        <%
                        }
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (OtherCertification.isRequired("Comments",UserSecurityGroupID))&&(!OtherCertification.isComplete("Comments")) )
            {
                theClass = "requiredField";
            }
            else if ((OtherCertification.isExpired("Comments",expiredDays))&&(OtherCertification.isExpiredCheck("Comments",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((OtherCertification.isWrite("Comments",UserSecurityGroupID)))
            {
                        %>
                       <tr><td valign=top><p class=<%=theClass%> ><b><%=OtherCertification.getEnglish("Comments")%>&nbsp;</b></p></td><td valign=top><p><textarea onKeyDown="textAreaStop(this,200)" rows="2" name="Comments" cols="40" maxlength=200><%=OtherCertification.getComments()%></textarea>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Comments&amp;sTableName=tOtherCertification&amp;sRefID=<%=OtherCertification.getOtherCertID()%>&amp;sFieldNameDisp=<%=OtherCertification.getEnglish("Comments")%>&amp;sTableNameDisp=tOtherCertification','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((OtherCertification.isRead("Comments",UserSecurityGroupID)))
            {
                        %>
                       <tr><td valign=top> <p class=<%=theClass%> ><b><%=OtherCertification.getEnglish("Comments")%>&nbsp;</b></p></td><td valign=top><p><%=OtherCertification.getComments()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Comments&amp;sTableName=tOtherCertification&amp;sRefID=<%=OtherCertification.getOtherCertID()%>&amp;sFieldNameDisp=<%=OtherCertification.getEnglish("Comments")%>&amp;sTableNameDisp=tOtherCertification','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>




            <tr><td width=40%>&nbsp;</td><td width=60%>&nbsp;</td></tr>
            </table>
        <%if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {%>
            <input type=hidden name=routePageReference value="sParentReturnPage">
             <%
              if (request.getParameter("INTNext")!=null&&request.getParameter("INTNext").equalsIgnoreCase("yes") ) 
              {
              %>
                  <table width=75% border=1 bordercolor=333333 align=left cellspacing=0 cellpadding=0 class=wizardTable>
                  <tr class=requiredField><td>
                  <input  <%=HTMLFormStyleButton%> type="radio" value="next" name="INTNext" checked>&nbsp;&nbsp;<%=ConfigurationMessages.getInterviewMessage("INTERVIEWNoMore","tOtherCertification")%>
                  <br>
                  <input  <%=HTMLFormStyleButton%> type="radio" value="yes" name="INTNext">&nbsp;&nbsp;<%=ConfigurationMessages.getInterviewMessage("INTERVIEWAddMore","tOtherCertification")%>
                  </td></tr></table><br><br><br>
              <%
              }
              %>
            <p><input <%=HTMLFormStyleButton%> type=Submit value="Continue" name=Submit></p>
        <%}%>
        </td></tr></table>
        </form>
        <%
  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>

    </td></tr></table>



<%String bnIncludeFN = "ui_"+thePLCID+"\\bot-nav_PhysicianID.jsp";
%>
<jsp:include page="<%=bnIncludeFN%>" flush="true" ><jsp:param name="plcID" value="<%=thePLCID%>"/></jsp:include>
