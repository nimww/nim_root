<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.bltDocumentManagement,java.util.Vector,java.util.Enumeration,com.winstaff.ListElement, com.winstaff.PLCUtils, com.winstaff.SecurityCheck, com.winstaff.ConfigurationMessages,com.winstaff.bltDocumentManagement,com.winstaff.bltDocumentManagement_List" %>
<%/*
    filename: tDocumentManagement_main_DocumentManagement_PhysicianID.jsp
    Created on Oct/22/2003
    Type: 1-n main class file
    Created by: Scott Ellis
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>
    <%
    Integer iExpressMode = new Integer(1);
    if (pageControllerHash.containsKey("iExpressMode")) 
    {
        iExpressMode =    (Integer)pageControllerHash.get("iExpressMode");
    }
    %>

<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_PhysicianID.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>

    <table cellpadding=0 cellspacing=0 border=0 width=<%=MasterTableWidth%> >
    <tr><td width=10>&nbsp;</td><td>
    <%=ConfigurationMessages.getHTML("EXPRESSTopControl","tDocumentManagement",iExpressMode)%>



<%
//initial declaration of list class and parentID
    Integer        iPhysicianID        =    null;
    boolean accessValid = false;
   Integer iSecurityCheck = SecurityCheck.CheckItem("DocumentManagement1", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {
    if (pageControllerHash.containsKey("iPhysicianID") ) 
    {
        iPhysicianID        =    (Integer)pageControllerHash.get("iPhysicianID");
        accessValid = true;
    }  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);
      pageControllerHash.put("sParentReturnPage","tDocumentManagement_main_DocumentManagement_PhysicianID_Expand.jsp");
    pageControllerHash.remove("iDocumentID");
    session.setAttribute("pageControllerHash",pageControllerHash);

    bltDocumentManagement_List        bltDocumentManagement_List        =    new    bltDocumentManagement_List(iPhysicianID);

//declaration of Enumeration
    bltDocumentManagement        DocumentManagement;
    ListElement         leCurrentElement;
    java.util.Enumeration eList = bltDocumentManagement_List.elements();
    %>
        <%@ include file="tDocumentManagement_main_DocumentManagement_PhysicianID_instructions.jsp" %>

        <form action="tDocumentManagement_main_DocumentManagement_PhysicianID_Express_sub.jsp" name="tDocumentManagement_main_DocumentManagement_PhysicianID1" method="POST">
    <%
    int iExpress=0;
    while (eList.hasMoreElements()||iExpress<=ConfigurationMessages.getExpressItemCount("tDocumentManagement"))
    {
       iExpress++;
         %>
         <table border="0" bordercolor="333333" cellpadding="0" class=tdHeaderAlt cellspacing="0" width="100%">
            <tr>
              <td width="50%">
                   <table border="0" bordercolor="333333" cellpadding="0" class=tdHeaderAlt cellspacing="0" width="100%">
                   <tr> 
                   	<td rowspan="2"><img src=express/left-corner.gif></td>
                   	<td width=100%><img width=100% height=2 src=express/small-line.gif></td>
                   	<td rowspan="2" align=right><img src=express/right-corner.gif></td>
                   </tr>
                     <tr> 
                       <td>
         <%

      boolean isNewRecord= false;
      if (eList.hasMoreElements())
      {
        leCurrentElement    = (ListElement) eList.nextElement();
        DocumentManagement  = (bltDocumentManagement) leCurrentElement.getObject();
      }
      else
      {
        DocumentManagement  = new bltDocumentManagement();
        isNewRecord= true;
      }
        DocumentManagement.GroupSecurityInit(UserSecurityGroupID);
        String theClass = "tdHeader";
        %>
               <span class=<%=theClass%> ><b><%=ConfigurationMessages.getDataCategory("tDocumentManagement")%> #<%=iExpress%></span>
                  </td></tr></table>
            </td></tr>
                 </table>
                 <table width="100%" border="1" cellspacing="0" bordercolor="#333333">
                  <tr>
                   <td>
                     <table width="100%" border="0" cellspacing="0">
                     <tr><td>
<%String theClassF = "textBase";%>
<%
if (isNewRecord)
{%>
<input type=hidden name=recordItemStatus_<%=iExpress%>="new">
<%}
else
{%>
<input type=hidden name=recordItemStatus_<%=iExpress%>="edit">
<%}

  {

        %>

          <%  theClass ="tdBase";%>
        <table width=100% border=1 bordercolor=#333333 cellpadding=3 cellspacing=0 class=tableBase>
         <tr><td class=tableColor>


            <table cellpadding=0 cellspacing=0 width=100%>
            <%
            if ( (DocumentManagement.isRequired("DocumentTypeID",UserSecurityGroupID))&&(!DocumentManagement.isComplete("DocumentTypeID")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((DocumentManagement.isExpired("DocumentTypeID",expiredDays))&&(DocumentManagement.isExpiredCheck("DocumentTypeID",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                     <input type=hidden name="DocumentTypeID_<%=iExpress%>" value="<%=DocumentManagement.getDocumentTypeID()%>">

            <%
            if ( (DocumentManagement.isRequired("DocumentName",UserSecurityGroupID))&&(!DocumentManagement.isComplete("DocumentName")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((DocumentManagement.isExpired("DocumentName",expiredDays))&&(DocumentManagement.isExpiredCheck("DocumentName",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                     <input type=hidden name="DocumentName_<%=iExpress%>" value="<%=DocumentManagement.getDocumentName()%>">

            <%
            if ( (DocumentManagement.isRequired("DocumentFileName",UserSecurityGroupID))&&(!DocumentManagement.isComplete("DocumentFileName")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((DocumentManagement.isExpired("DocumentFileName",expiredDays))&&(DocumentManagement.isExpiredCheck("DocumentFileName",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                     <input type=hidden name="DocumentFileName_<%=iExpress%>" value="<%=DocumentManagement.getDocumentFileName()%>">

            <%
            if ( (DocumentManagement.isRequired("DateReceived",UserSecurityGroupID))&&(!DocumentManagement.isComplete("DateReceived")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((DocumentManagement.isExpired("DateReceived",expiredDays))&&(DocumentManagement.isExpiredCheck("DateReceived",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                     <input type=hidden name="DateReceived_<%=iExpress%>" value="<%=DocumentManagement.getDateReceived()%>">

            <%
            if ( (DocumentManagement.isRequired("DateOfExpiration",UserSecurityGroupID))&&(!DocumentManagement.isComplete("DateOfExpiration")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((DocumentManagement.isExpired("DateOfExpiration",expiredDays))&&(DocumentManagement.isExpiredCheck("DateOfExpiration",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                     <input type=hidden name="DateOfExpiration_<%=iExpress%>" value="<%=DocumentManagement.getDateOfExpiration()%>">

            <%
            if ( (DocumentManagement.isRequired("DateAlertSent",UserSecurityGroupID))&&(!DocumentManagement.isComplete("DateAlertSent")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((DocumentManagement.isExpired("DateAlertSent",expiredDays))&&(DocumentManagement.isExpiredCheck("DateAlertSent",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                     <input type=hidden name="DateAlertSent_<%=iExpress%>" value="<%=DocumentManagement.getDateAlertSent()%>">

            <%
            if ( (DocumentManagement.isRequired("Archived",UserSecurityGroupID))&&(!DocumentManagement.isComplete("Archived")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((DocumentManagement.isExpired("Archived",expiredDays))&&(DocumentManagement.isExpiredCheck("Archived",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                     <input type=hidden name="Archived_<%=iExpress%>" value="<%=DocumentManagement.getArchived()%>">

            <%
            if ( (DocumentManagement.isRequired("Comments",UserSecurityGroupID))&&(!DocumentManagement.isComplete("Comments")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((DocumentManagement.isExpired("Comments",expiredDays))&&(DocumentManagement.isExpiredCheck("Comments",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                     <input type=hidden name="Comments_<%=iExpress%>" value="<%=DocumentManagement.getComments()%>">


            <tr><td width=40%>&nbsp;</td><td width=60%>&nbsp;</td></tr>

            <input type=hidden name=routePageReference value="sParentReturnPage">

        </td></tr></table>
        </td></tr></table>
        <%
  }%>

        </td></tr></table></table><br>        <%
    }
    %>
        <input <%=HTMLFormStyleButton%> onClick="document.forms[0].nextPage.value='<%=ConfigurationMessages.getExpressLinkRaw("tDocumentManagement","next",iExpressMode)%>'" type=Submit value="Continue" name=Submit>
        <input type=hidden name=nextPage value="">
        </form>

    </table>
    <%
  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>

    </td></tr></table>


<%String bnIncludeFN = "ui_"+thePLCID+"\\bot-nav_PhysicianID.jsp";
%>
<jsp:include page="<%=bnIncludeFN%>" flush="true" ><jsp:param name="plcID" value="<%=thePLCID%>"/></jsp:include>
