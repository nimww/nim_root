
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.*" %>
<%/*
    filename: tAdminPracticeLU_main_LU_AdminID.jsp
    Created on Apr/23/2002
    Type: 1-n main class file
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>

<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_Clear.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>

    <table cellpadding=0 cellspacing=0 border=0 width=100%>
    <tr><td width=10>&nbsp;</td>
    <td>
<p><br />    <span class=title>Accounts</span></p>


    <%
//initial declaration of list class and parentID
    Integer        iGenAdminID        =    null;
	String		savedSQL = null;
	String		savedURL = null;
    boolean accessValid = false;
    if (pageControllerHash.containsKey("iGenAdminID")) 
    {
        iGenAdminID        =    (Integer)pageControllerHash.get("iGenAdminID");
        accessValid = true;
    }
    if (pageControllerHash.containsKey("SQL_AdminPracticeAll_Query-acc")) 
    {
        savedSQL        =    (String)pageControllerHash.get("SQL_AdminPracticeAll_Query-acc");
//        accessValid = true;
    }
    if (pageControllerHash.containsKey("URL_AdminPracticeAll_Query-acc")) 
    {
        savedURL        =    (String)pageControllerHash.get("URL_AdminPracticeAll_Query-acc");
//        accessValid = true;
    }
  //page security
  if (accessValid)
  {



		java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat("yyyy-MM-dd");
      java.text.SimpleDateFormat dbdft = new java.text.SimpleDateFormat(PLCUtils.String_dbdft);
      java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);
//      pageControllerHash.put("sParentReturnPage","AdminPracticeAll_home-acc.jsp");

%>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr> 
    <td> 
<%
try
{


String myownername2 = "";
String myOfficeAddress1 = "";
String myOfficeCity = "";
String myOfficeStateID = "0";
String myOfficeZip = "";
String orderBy = " name " ;
String iAssignedToID = "-1" ;
int startID = 0;
int maxResults = 200;

boolean firstDisplay = false;

try
{
	maxResults = Integer.parseInt(request.getParameter("maxResults"));
	startID = Integer.parseInt(request.getParameter("startID"));
	myownername2 = request.getParameter("ownername2");
	myOfficeAddress1 = request.getParameter("OfficeAddress1");
	myOfficeCity = request.getParameter("OfficeCity");
	myOfficeStateID = request.getParameter("OfficeStateID");
	myOfficeZip = request.getParameter("OfficeZip");
	iAssignedToID = request.getParameter("AssignedToID");
	orderBy = request.getParameter("orderBy");
}
catch (Exception e)
{
	//out.print("--------------"  + e);
	maxResults = 200;
	startID = 0;
	firstDisplay = true;
	myownername2 = "";
	myOfficeAddress1 = "";
	myOfficeCity = "";
	myOfficeStateID = "0";
	myOfficeZip = "";
	iAssignedToID = "-1";
	orderBy = "name";
}
if (orderBy == null)
{
	out.print("-------------- order by null" );
	maxResults = 200;
	startID = 0;
	firstDisplay = true;
	myownername2 = "";
	myOfficeAddress1 = "";
	myOfficeCity = "";
	myOfficeStateID = "0";
	myOfficeZip = "";
	iAssignedToID = "-1";
	orderBy = "name";
}


if (firstDisplay&&savedURL==null)
{
%>



<table width = 100% cellpadding=2 cellspacing=2>
<tr>

<td>
<form name="form1" method="get" action="AdminPracticeAll_home-acc.jsp" id="apsearchform1">
  <table width="400" border="1" cellspacing="0" cellpadding="0" align="center" bordercolor="#003333">
    <tr>
      <td>
        <table width="100%" border="0" cellspacing="0" cellpadding="3" align="center">
        <tr>
          <td class="tdHeaderAlt"> Account Name </td>
          <td><input name="ownername2" type="text" id="ownername2" /></td>
        </tr>
        <tr>
          <td class="tdHeaderAlt"> Address1</td>
          <td><input name="OfficeAddress1" type="text" id="OfficeAddress1" /></td>
        </tr>
        <tr>
          <td class=tdHeaderAlt>City</td>
          <td>
            <input type=text name="OfficeCity">
            </td>
        </tr>
        <tr>
         <td class=tdHeaderAlt>
         State
         </td>
         <td>
              <select name="OfficeStateID">
                <jsp:include page="../generic/tStateLILong.jsp" flush="true" > 
                <jsp:param name="CurrentSelection" value="0" />
                </jsp:include>
              </select>
         </td>
        </tr>
        <tr>
         <td class=tdHeaderAlt>
ZIP
         </td>
         <td>
         <input type=text name="OfficeZip">
         </td>
        </tr>
            <tr class=tdHeaderAlt>
              <td>Assigned To</td>
              <td><select class="tdBaseAltYellow" onchange="document.getElementById('isSaveMe').value='1';"   name="AssignedToID" ><jsp:include page="../generic/tAssignTo_genAdminID.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=CurrentUserAccount.getUserID()%>" /><jsp:param name="CurrentSelectionOpts1" value="show" /></jsp:include></select>&nbsp;</td>
            </tr>
            <tr class=tdHeaderAlt>
              <td>Results</td>
              <td><select class="tdBaseAltYellow"  name="maxResults" id="maxResults">
                <option value="25" <%if (maxResults ==25){out.println(" selected");}%> >25</option>
                <option value="50" <%if (maxResults ==50){out.println(" selected");}%> >50</option>
                <option value="75" <%if (maxResults ==75){out.println(" selected");}%> >75</option>
                <option value="100" <%if (maxResults ==100){out.println(" selected");}%> >100</option>
                <option value="150" <%if (maxResults ==150){out.println(" selected");}%> >150</option>
                <option value="200" <%if (maxResults ==200){out.println(" selected");}%> >200</option>
                <option value="250" <%if (maxResults ==250){out.println(" selected");}%> >250</option>
                <option value="300" <%if (maxResults ==300){out.println(" selected");}%> >300</option>
                <option value="400" <%if (maxResults ==400){out.println(" selected");}%> >400</option>
                <option value="500" <%if (maxResults ==500){out.println(" selected");}%> >500</option>
                <option value="600" <%if (maxResults ==600){out.println(" selected");}%> >600</option>
                <option value="700" <%if (maxResults ==700){out.println(" selected");}%> >700</option>
                <option value="800" <%if (maxResults ==800){out.println(" selected");}%> >800</option>
              </select></td>
            </tr>
            <tr class=tdHeaderAlt>
              <td>Order</td>
              <td><select name="orderBy" id="orderBy">
                <option value="City" <%if (orderBy.equalsIgnoreCase("City")){out.println(" selected");}%> >City</option>
                <option value="StateID" <%if (orderBy.equalsIgnoreCase("StateID")){out.println(" selected");}%> >State</option>
                <option value="Zip" <%if (orderBy.equalsIgnoreCase("Zip")){out.println(" selected");}%> >Postal Code</option>
                <option value="ownername2" <%if (orderBy.equalsIgnoreCase("ownername2")){out.println(" selected");}%> >Owner Name</option>
              </select></td>
            </tr>
            <tr class=tdHeaderAlt> 
              <td><br />
                <input type=hidden name="startID" value=0></td>
              <td><input type="submit" name="Submit2" value="Submit" /></td>
            </tr>
        </table>
</td>
    </tr>
  </table>
  </form>
<p> 
  <%
}
else if (firstDisplay&&savedURL!=null)
{
pageControllerHash.remove("URL_AdminPracticeAll_MRI_query");
session.setAttribute("pageControllerHash",pageControllerHash);

%>
    <script language="javascript">
//    alert( '<%=savedURL%>');
    document.location = '<%=savedURL%>';

    </script>




    <%
}
else
{

%>
  <script language="JavaScript">
<!--
function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}
//-->
</script>
</p>
<table width=100% border=0 cellpadding=0 cellspacing=0 bordercolor=#003333>
<tr>
<td>
<table border=1 cellpadding=2 cellspacing=0 bordercolor=#003333>
<tr>
<td>
<form name="selfForm" method="get" action="AdminPracticeAll_home-acc.jsp" id="apsearchform1">
  <table border=0 cellspacing=2 width='100%' cellpadding=3>
    <tr > 
      <td colspan=2 class=tdHeader>&nbsp;</td>
      <td colspan=3 class=tdBase align=right><p>          Max Results: 
          <select  class="tdBaseAltYellow"  name="maxResults">
            <option value="25" <%if (maxResults ==25){out.println(" selected");}%> >25</option>
            <option value="50" <%if (maxResults ==50){out.println(" selected");}%> >50</option>
            <option value="75" <%if (maxResults ==75){out.println(" selected");}%> >75</option>
            <option value="100" <%if (maxResults ==100){out.println(" selected");}%> >100</option>
            <option value="150" <%if (maxResults ==150){out.println(" selected");}%> >150</option>
            <option value="200" <%if (maxResults ==200){out.println(" selected");}%> >200</option>
            <option value="250" <%if (maxResults ==250){out.println(" selected");}%> >250</option>
            <option value="300" <%if (maxResults ==300){out.println(" selected");}%> >300</option>
            <option value="400" <%if (maxResults ==400){out.println(" selected");}%> >400</option>
            <option value="500" <%if (maxResults ==500){out.println(" selected");}%> >500</option>
            <option value="600" <%if (maxResults ==600){out.println(" selected");}%> >600</option>
            <option value="700" <%if (maxResults ==700){out.println(" selected");}%> >700</option>
            <option value="800" <%if (maxResults ==800){out.println(" selected");}%> >800</option>
            </select>
          <br />
      </p>       </td>    
      <td colspan="2" nowrap class=tdHeader> 
        <input type=hidden name=startID value=0 ><p>Sort:
          <select name=orderBy>
            <option value=City <%if (orderBy.equalsIgnoreCase("City")){out.println(" selected");}%> >City</option>
            <option value=StateID <%if (orderBy.equalsIgnoreCase("StateID")){out.println(" selected");}%> >State</option>
            <option value=Zip <%if (orderBy.equalsIgnoreCase("Zip")){out.println(" selected");}%> >Postal Code</option>
            <option value="ownername2" <%if (orderBy.equalsIgnoreCase("ownername2")){out.println(" selected");}%> >Owner Name</option>
            </select>
        </p></td>

      <td align="center" class=tdHeader><input type="Submit" class="tdBaseAltGreen"  onclick="this.disabled=true;document.selfForm.submit();" name="Submit" value="Submit" align="middle" /><br />
</td>
    </tr>
    <tr class=tdHeader> 
      <td  colspan=1>&nbsp;      </td>
      <td  colspan=1>&nbsp;      </td>
      <td><select class="tdBaseAltYellow" onchange="document.getElementById('isSaveMe').value='1';"   name="AssignedToID" ><jsp:include page="../generic/tAssignTo_genAdminID.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=iAssignedToID%>" /><jsp:param name="CurrentSelectionOpts1" value="show" /></jsp:include></select></td>
      <td colspan=1> 
        <input type="text" name="ownername2" id="ownername2" value="<%=myownername2%>" size="20">
      </td>
      <td><input type="text" name="OfficeAddress1"  id="OfficeAddress1" value="<%=myOfficeAddress1%>" size="20" /></td>
      <td colspan=1> 
        <input type="text" name="OfficeCity" id="OfficeCity" value="<%=myOfficeCity%>" size="10">
      </td>
      <td colspan=1> 
              <select name="OfficeStateID">
                <jsp:include page="../generic/tStateLIShort.jsp" flush="true" > 
                <jsp:param name="CurrentSelection" value="<%=myOfficeStateID%>" />
                </jsp:include>
              </select>
      </td>
      <td colspan=1> 
        <input name="OfficeZip" type="text" id="OfficeZip" value="<%=myOfficeZip%>" size="10" maxlength="5">
      </td>
    </tr>
    <tr class=tdHeader> 
    <td>#</td>
    <td>Action</td>
    <td>Assigned To</td>
      <td colspan=1> 
        Account 
      </td>
      
      <td>Address</td>
      <td colspan=1> 
        City
      </td>
      <td colspan=1> 
        State
      </td>
      <td colspan=1> 
ZIP
      </td>
    </tr>
 

<%



		//String myWhere = "where ( (PracticeID IN (select PracticeID from tAdminPracticeLU where (adminID='"+iGenAdminID+"')))	 ";
	//String myWhere = "where ( adminID="+iGenAdminID;
	//String myWhere = "where  (adminID="+iGenAdminID+") and (practiceID in (select practiceID from tPracticeMaster where (practiceID>0";
	String myWhere = "where   (tadminmaster.adminid>0";
	
	boolean theFirst = false;

if (!firstDisplay)
{
	try
	{
	if (!myownername2.equalsIgnoreCase(""))
	{
	if (!theFirst) { myWhere+=" and ";}
	myWhere += "UPPER(tAdminMaster.Name) LIKE UPPER('%" + DataControlUtils.fixApostrophe(myownername2) +"%')";
	theFirst = false;
	}
	if (!myOfficeCity.equalsIgnoreCase(""))
	{
		if (!theFirst) { myWhere+=" and ";}
		myWhere += "UPPER(tAdminMaster.ContactCity) LIKE UPPER('%" + DataControlUtils.fixApostrophe(myOfficeCity) +"%')";
		theFirst = false;
	}
	if (!iAssignedToID.equalsIgnoreCase("-1"))
	{
		if (!theFirst) { myWhere+=" and ";}
		myWhere += "  AssignedToID = " + iAssignedToID +"";
		theFirst = false;
	}
	if (!myOfficeAddress1.equalsIgnoreCase(""))
	{
		if (!theFirst) { myWhere+=" and ";}
		myWhere += "UPPER(tAdminMaster.ContactAddress1) LIKE UPPER('%" + DataControlUtils.fixApostrophe(myOfficeAddress1) +"%')";
		theFirst = false;
	}
	if (!myOfficeStateID.equalsIgnoreCase("0"))
	{
	if (!myOfficeStateID.equalsIgnoreCase(""))
	{
	if (!theFirst) { myWhere+=" and ";}
	myWhere += "tAdminMaster.ContactStateID = " + myOfficeStateID +"";
	theFirst = false;
	}
	}
	if (!myOfficeZip.equalsIgnoreCase(""))
	{
	if (!theFirst) { myWhere+=" and ";}
	myWhere += "tAdminMaster.ContactZip LIKE '%" + myOfficeZip +"%'";
	theFirst = false;
	}
	myWhere += ")";
	//myWhere += ") ))";
	
	//System.out.println(myWhere);
	if (theFirst||myWhere.equalsIgnoreCase(")"))
	{
	myWhere = "";
	}
	
	
	}
	catch(Exception e)
	{
	out.println("FFF:"+e);
	}

}

searchDB2 mySS = new searchDB2();

java.sql.ResultSet myRS = null;;

try
{
String mySQL = "";
String mySQLMap = "";
if (true||!firstDisplay)
{
//	mySQL = ("select tAdminMaster.Name as ownername2,tAdminMaster.AdminID as AdminID, ZIP_CODE.county as ZCCounty, tPracticeMaster.PracticeID, tPracticeMaster.PracticeName,tPracticeMaster.OfficeCity,tPracticeMaster.OfficeStateID,tPracticeMaster.OfficeZIP,tAdminPracticeLU.PracticeID, tAdminPracticeLU.LookupID from tAdminMaster INNER JOIN tAdminPracticeLU on tAdminPracticeLU.adminid=tAdminMaster.adminID INNER JOIN tPracticeMaster on tPracticeMaster.practiceid = tAdminPracticeLU.practiceid INNER JOIN ZIP_CODE on ZIP_CODE.zip_code = substring(tPracticeMaster.OfficeZip,0,6) " + myWhere + "order by tPracticeMaster." + orderBy + " LIMIT " + (maxResults+1) + " OFFSET " + startID + "");	
	mySQL = ("SELECT tadminmaster.adminid, tUseraccount.logonusername,  tadminmaster.name as ownername2, tadminmaster.contactaddress1, tadminmaster.contactcity, tadminmaster.contactstateid, tadminmaster.contactzip FROM  public.tadminmaster  LEFT JOIN  tUserAccount on  tUserAccount.userid =  tadminmaster.assignedtoid " + myWhere + " order by " + orderBy + " LIMIT " + maxResults);	
}
else
{
	mySQL = savedSQL;	
}

//String mySQL = ("select tPracticeMaster.PracticeID,tPracticeMaster.PracticeName,tPracticeMaster.OfficeCity,tPracticeMaster.OfficeStateID,tPracticeMaster.OfficeZIP,tAdminPracticeLU.PracticeID, tAdminPracticeLU.LookupID from tAdminMaster INNER JOIN tAdminPracticeLU on tAdminPracticeLU.adminid=tAdminMaster.adminID INNER JOIN tPracticeMaster on tPracticeMaster.practiceid = tAdminPracticeLU.practiceid " + myWhere + "order by tPracticeMaster." + orderBy + " LIMIT " + (maxResults+1) + " OFFSET " + startID + "");
pageControllerHash.put("URL_AdminPracticeAll_Query-acc",request.getRequestURL()+"?"+request.getQueryString());
session.setAttribute("pageControllerHash",pageControllerHash);

myRS = mySS.executeStatement(mySQL);
//out.print(mySQL);
//myRS = mySS.executeStatement("select * from tAdminPracticeLU " + myWhere + " order by " + orderBy);

}
catch(Exception e)
{
out.println("ResultsSet:"+e);
}

//String myMainTable= " ";
try
{

	int endCount = 0;
	
	int cnt=0;
	int cnt2=0;
	while (myRS!=null&&myRS.next())
	{
		//bltPracticeMaster pm = new bltPracticeMaster(new Integer(myRS.getString("PracticeID")));
		
		String iOwnerID = myRS.getString("AdminID");
		
		String smyDueDate = "None";
		try
		{
			smyDueDate = PLCUtils.getDisplayDateWithTime(dbdft.parse(myRS.getString("RemindDate")));
			if (smyDueDate.equalsIgnoreCase(" ")||smyDueDate.equalsIgnoreCase(""))
			{
				smyDueDate = "None";
			}
		}
		catch(Exception edd)
		{
		}
		String smyDueDate_admin = "None";
		try
		{
			smyDueDate_admin = PLCUtils.getDisplayDateWithTime(dbdft.parse(myRS.getString("RemindDate_admin")));
			if (smyDueDate_admin.equalsIgnoreCase(" ")||smyDueDate_admin.equalsIgnoreCase(""))
			{
				smyDueDate_admin = "None";
			}
		}
		catch(Exception edd)
		{
		}
		String amownername2 = myRS.getString("ownername2");

		String iAssign_ln = "none";
		try
		{
			iAssign_ln = myRS.getString("logonusername");
			if (iAssign_ln==null||iAssign_ln.equalsIgnoreCase(" ")||iAssign_ln.equalsIgnoreCase("")||iAssign_ln.equalsIgnoreCase("EMPTY"))
			{
				iAssign_ln = "Not Assigned";
			}
		}
		catch(Exception edd)
		{
				iAssign_ln = "Not Assigned";
		}
		String pmOfficeCity = myRS.getString("ContactCity");
		String pmOfficeAddress1 = myRS.getString("ContactAddress1");
		String pmOfficeStateID  = myRS.getString("ContactStateID");
		String pmOfficeZIP = myRS.getString("ContactZIP");
		
		
		
		
		
		
		cnt++;
		cnt2++;
		
		String myClass = "tdBase";
		if (cnt%2==0)
		{
			myClass = "tdBaseAlt";
		}
		out.print("<tr class="+myClass+">");
if (false)
{
}
		out.print("<td>"+cnt+"</td>");
		//out.print("<td><a href = \"tAdminPracticeLU_main_PracticeMaster_AdminID_form_authorize.jsp?EDIT=edit&EDITID="+pmPracticeID+"&EDIT=edit\"><img border=0 src=\"ui_"+thePLCID+"/icons/edit_AdminID.gif\"></a>        <a class=linkBase  onClick=\"return confirmDelete()\"   href = \"tAdminPracticeLU_main_LU_AdminID_form_authorize.jsp?EDIT=del&EDITID="+myRS.getString("LookUpID")+"&KM=p\"><img border=0 src=\"ui_"+thePLCID+"/icons/remove_AdminID.gif\"></td>";
		out.print("<td>");
		out.print("<a  href=AdminPracticeAll_query_auth.jsp?EDIT=owner&EDITID=" + iOwnerID + ">Account</a><br>");
		out.print("</td>");
		out.print("<td>");
		out.print("" + iAssign_ln+"");
		out.print("</td>");
		out.print("<td>");
		out.print("<a href=# onClick=\"document.getElementById('ownername2').value='" + amownername2+ "';document.getElementById('apsearchform1').submit();\">" + amownername2+"</a>");
		out.print("</td>");
		out.print("<td>");
		out.print("" + pmOfficeAddress1+"");
		out.print("</td>");
		out.print("<td>");
		out.print("<a href=# onClick=\"document.getElementById('OfficeCity').value='" + pmOfficeCity+ "';document.getElementById('apsearchform1').submit();\">" + pmOfficeCity+"</a>");
		out.print("</td>");
		out.print("<td>");
		if (pmOfficeStateID.equalsIgnoreCase("49") ) {out.print( "AK");}
		else if (pmOfficeStateID.equalsIgnoreCase("30") ) {out.print( "AL");}
		else if (pmOfficeStateID.equalsIgnoreCase("21") ) {out.print( "AR");}
		else if (pmOfficeStateID.equalsIgnoreCase("8") ) {out.print( "AZ");}
		else if (pmOfficeStateID.equalsIgnoreCase("1") ) {out.print( "CA");}
		else if (pmOfficeStateID.equalsIgnoreCase("10") ) {out.print( "CO");}
		else if (pmOfficeStateID.equalsIgnoreCase("45") ) {out.print( "CT");}
		else if (pmOfficeStateID.equalsIgnoreCase("51") ) {out.print( "DC");}
		else if (pmOfficeStateID.equalsIgnoreCase("47") ) {out.print( "DE");}
		else if (pmOfficeStateID.equalsIgnoreCase("33") ) {out.print( "FL");}
		else if (pmOfficeStateID.equalsIgnoreCase("32") ) {out.print( "GA");}
		else if (pmOfficeStateID.equalsIgnoreCase("50") ) {out.print( "HI");}
		else if (pmOfficeStateID.equalsIgnoreCase("19") ) {out.print( "IA");}
		else if (pmOfficeStateID.equalsIgnoreCase("5") ) {out.print( "ID");}
		else if (pmOfficeStateID.equalsIgnoreCase("24") ) {out.print( "IL");}
		else if (pmOfficeStateID.equalsIgnoreCase("27") ) {out.print( "IN");}
		else if (pmOfficeStateID.equalsIgnoreCase("15") ) {out.print( "KS");}
		else if (pmOfficeStateID.equalsIgnoreCase("28") ) {out.print( "KY");}
		else if (pmOfficeStateID.equalsIgnoreCase("22") ) {out.print( "LA");}
		else if (pmOfficeStateID.equalsIgnoreCase("43") ) {out.print( "MA");}
		else if (pmOfficeStateID.equalsIgnoreCase("48") ) {out.print( "MD");}
		else if (pmOfficeStateID.equalsIgnoreCase("34") ) {out.print( "ME");}
		else if (pmOfficeStateID.equalsIgnoreCase("26") ) {out.print( "MI");}
		else if (pmOfficeStateID.equalsIgnoreCase("18") ) {out.print( "MN");}
		else if (pmOfficeStateID.equalsIgnoreCase("20") ) {out.print( "MO");}
		else if (pmOfficeStateID.equalsIgnoreCase("25") ) {out.print( "MS");}
		else if (pmOfficeStateID.equalsIgnoreCase("4") ) {out.print( "MT");}
		else if (pmOfficeStateID.equalsIgnoreCase("52") ) {out.print( "NA");}
		else if (pmOfficeStateID.equalsIgnoreCase("41") ) {out.print( "NC");}
		else if (pmOfficeStateID.equalsIgnoreCase("12") ) {out.print( "ND");}
		else if (pmOfficeStateID.equalsIgnoreCase("14") ) {out.print( "NE");}
		else if (pmOfficeStateID.equalsIgnoreCase("35") ) {out.print( "NH");}
		else if (pmOfficeStateID.equalsIgnoreCase("46") ) {out.print( "NJ");}
		else if (pmOfficeStateID.equalsIgnoreCase("11") ) {out.print( "NM");}
		else if (pmOfficeStateID.equalsIgnoreCase("6") ) {out.print( "NV");}
		else if (pmOfficeStateID.equalsIgnoreCase("37") ) {out.print( "NY");}
		else if (pmOfficeStateID.equalsIgnoreCase("31") ) {out.print( "OH");}
		else if (pmOfficeStateID.equalsIgnoreCase("16") ) {out.print( "OK");}
		else if (pmOfficeStateID.equalsIgnoreCase("3") ) {out.print( "OR");}
		else if (pmOfficeStateID.equalsIgnoreCase("38") ) {out.print( "PA");}
		else if (pmOfficeStateID.equalsIgnoreCase("44") ) {out.print( "RI");}
		else if (pmOfficeStateID.equalsIgnoreCase("42") ) {out.print( "SC");}
		else if (pmOfficeStateID.equalsIgnoreCase("13") ) {out.print( "SD");}
		else if (pmOfficeStateID.equalsIgnoreCase("29") ) {out.print( "TN");}
		else if (pmOfficeStateID.equalsIgnoreCase("17") ) {out.print( "TX");}
		else if (pmOfficeStateID.equalsIgnoreCase("7") ) {out.print( "UT");}
		else if (pmOfficeStateID.equalsIgnoreCase("40") ) {out.print( "VA");}
		else if (pmOfficeStateID.equalsIgnoreCase("36") ) {out.print( "VT");}
		else if (pmOfficeStateID.equalsIgnoreCase("2") ) {out.print( "WA");}
		else if (pmOfficeStateID.equalsIgnoreCase("23") ) {out.print( "WI");}
		else if (pmOfficeStateID.equalsIgnoreCase("39") ) {out.print( "WV");}
		else if (pmOfficeStateID.equalsIgnoreCase("9") ) {out.print( "WY");}
out.print("</td>");
		out.print("<td>");
		try
		{
			out.print("<a href=# onClick=\"document.getElementById('OfficeZip').value='" + pmOfficeZIP.substring(0,5)+ "';document.getElementById('apsearchform1').submit();\">" + pmOfficeZIP+"</a>");
		}
		catch (Exception eZIP)
		{
			out.print("<a href=# onClick=\"document.getElementById('OfficeZip').value='" + pmOfficeZIP + "';document.getElementById('apsearchform1').submit();\">" + pmOfficeZIP+"</a>");
		}
		out.print("</td>");
		out.print("</tr>");

	}
mySS.closeAll();
endCount = cnt;


}
catch (Exception e)
{
out.println("Error???:"+e);
System.out.println("Error:"+e);
}



%>



</table> 


  </table>
</form>
</td>
</tr>
</table>
</td>
</tr>
</table>     </td>
  </tr>
</table>



<%
}

}
catch (Exception e)
{
out.println("Error???:"+e);
System.out.println("Error:"+e);
}


  }
  else
  {
   response.sendRedirect("/errorhandler.jsp");
  }
%>

    </td></tr></table>

