<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.bltDocumentManagement,java.util.Vector,java.util.Enumeration,com.winstaff.ListElement, com.winstaff.PLCUtils, com.winstaff.SecurityCheck, com.winstaff.ConfigurationMessages,com.winstaff.bltPhysicianMaster,com.winstaff.DocumentManagerUtils" %>
<%/*
    filename: out\jsp\tPhysicianMaster_form_sub.jsp
    Created on Mar/21/2003
    Created by: Scott Ellis
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>


<%

//initial declaration of list class and parentID
    Integer        iPhysicianID        =    null;
    boolean accessValid = false;

   Integer iSecurityCheck = SecurityCheck.CheckItem("PractitionerSection1", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {

    if (pageControllerHash.containsKey("iPhysicianID")) 
    {
        iPhysicianID        =    (Integer)pageControllerHash.get("iPhysicianID");
        if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {
            accessValid = true;
        }
    }
  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);

//initial declaration of list class and parentID

    bltPhysicianMaster        PhysicianMaster        =    null;

    if ( request.getParameter( "EDIT" ).equalsIgnoreCase("edit") )
    {
        PhysicianMaster        =    new    bltPhysicianMaster(iPhysicianID,UserSecurityGroupID);
    }
    else if ( request.getParameter( "EDIT" ).equalsIgnoreCase("new") )
    {
        PhysicianMaster        =    new    bltPhysicianMaster(UserSecurityGroupID,true);
    }

String testChangeID = "0";

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("UniqueCreateDate"))) ;
    if ( !PhysicianMaster.getUniqueCreateDate().equals(testObj)  )
    {
         PhysicianMaster.setUniqueCreateDate( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PhysicianMaster UniqueCreateDate not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("UniqueModifyDate"))) ;
    if ( !PhysicianMaster.getUniqueModifyDate().equals(testObj)  )
    {
         PhysicianMaster.setUniqueModifyDate( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PhysicianMaster UniqueModifyDate not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("UniqueModifyComments")) ;
    if ( !PhysicianMaster.getUniqueModifyComments().equals(testObj)  )
    {
         PhysicianMaster.setUniqueModifyComments( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PhysicianMaster UniqueModifyComments not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("Salutation")) ;
    if ( !PhysicianMaster.getSalutation().equals(testObj)  )
    {
         PhysicianMaster.setSalutation( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PhysicianMaster Salutation not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("Title")) ;
    if ( !PhysicianMaster.getTitle().equals(testObj)  )
    {
         PhysicianMaster.setTitle( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PhysicianMaster Title not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("FirstName")) ;
    if ( !PhysicianMaster.getFirstName().equals(testObj)  )
    {
         PhysicianMaster.setFirstName( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PhysicianMaster FirstName not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("MiddleName")) ;
    if ( !PhysicianMaster.getMiddleName().equals(testObj)  )
    {
         PhysicianMaster.setMiddleName( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PhysicianMaster MiddleName not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("LastName")) ;
    if ( !PhysicianMaster.getLastName().equals(testObj)  )
    {
         PhysicianMaster.setLastName( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PhysicianMaster LastName not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("Suffix")) ;
    if ( !PhysicianMaster.getSuffix().equals(testObj)  )
    {
         PhysicianMaster.setSuffix( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PhysicianMaster Suffix not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("HomeEmail")) ;
    if ( !PhysicianMaster.getHomeEmail().equals(testObj)  )
    {
         PhysicianMaster.setHomeEmail( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PhysicianMaster HomeEmail not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("HomeAddress1")) ;
    if ( !PhysicianMaster.getHomeAddress1().equals(testObj)  )
    {
         PhysicianMaster.setHomeAddress1( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PhysicianMaster HomeAddress1 not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("HomeAddress2")) ;
    if ( !PhysicianMaster.getHomeAddress2().equals(testObj)  )
    {
         PhysicianMaster.setHomeAddress2( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PhysicianMaster HomeAddress2 not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("HomeCity")) ;
    if ( !PhysicianMaster.getHomeCity().equals(testObj)  )
    {
         PhysicianMaster.setHomeCity( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PhysicianMaster HomeCity not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("HomeStateID")) ;
    if ( !PhysicianMaster.getHomeStateID().equals(testObj)  )
    {
         PhysicianMaster.setHomeStateID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PhysicianMaster HomeStateID not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("HomeProvince")) ;
    if ( !PhysicianMaster.getHomeProvince().equals(testObj)  )
    {
         PhysicianMaster.setHomeProvince( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PhysicianMaster HomeProvince not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("HomeZIP")) ;
    if ( !PhysicianMaster.getHomeZIP().equals(testObj)  )
    {
         PhysicianMaster.setHomeZIP( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PhysicianMaster HomeZIP not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("HomeCountryID")) ;
    if ( !PhysicianMaster.getHomeCountryID().equals(testObj)  )
    {
         PhysicianMaster.setHomeCountryID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PhysicianMaster HomeCountryID not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("HomePhone")) ;
    if ( !PhysicianMaster.getHomePhone().equals(testObj)  )
    {
         PhysicianMaster.setHomePhone( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PhysicianMaster HomePhone not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("HomeFax")) ;
    if ( !PhysicianMaster.getHomeFax().equals(testObj)  )
    {
         PhysicianMaster.setHomeFax( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PhysicianMaster HomeFax not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("HomeMobile")) ;
    if ( !PhysicianMaster.getHomeMobile().equals(testObj)  )
    {
         PhysicianMaster.setHomeMobile( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PhysicianMaster HomeMobile not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("HomePager")) ;
    if ( !PhysicianMaster.getHomePager().equals(testObj)  )
    {
         PhysicianMaster.setHomePager( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PhysicianMaster HomePager not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("AlertEmail")) ;
    if ( !PhysicianMaster.getAlertEmail().equals(testObj)  )
    {
         PhysicianMaster.setAlertEmail( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PhysicianMaster AlertEmail not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("AlertDays")) ;
    if ( !PhysicianMaster.getAlertDays().equals(testObj)  )
    {
         PhysicianMaster.setAlertDays( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PhysicianMaster AlertDays not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("OtherName1")) ;
    if ( !PhysicianMaster.getOtherName1().equals(testObj)  )
    {
         PhysicianMaster.setOtherName1( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PhysicianMaster OtherName1 not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("OtherName1Start"))) ;
    if ( !PhysicianMaster.getOtherName1Start().equals(testObj)  )
    {
         PhysicianMaster.setOtherName1Start( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PhysicianMaster OtherName1Start not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("OtherName1End"))) ;
    if ( !PhysicianMaster.getOtherName1End().equals(testObj)  )
    {
         PhysicianMaster.setOtherName1End( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PhysicianMaster OtherName1End not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("OtherName2")) ;
    if ( !PhysicianMaster.getOtherName2().equals(testObj)  )
    {
         PhysicianMaster.setOtherName2( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PhysicianMaster OtherName2 not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("OtherName2Start"))) ;
    if ( !PhysicianMaster.getOtherName2Start().equals(testObj)  )
    {
         PhysicianMaster.setOtherName2Start( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PhysicianMaster OtherName2Start not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("OtherName2End"))) ;
    if ( !PhysicianMaster.getOtherName2End().equals(testObj)  )
    {
         PhysicianMaster.setOtherName2End( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PhysicianMaster OtherName2End not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("DateOfBirth"))) ;
    if ( !PhysicianMaster.getDateOfBirth().equals(testObj)  )
    {
         PhysicianMaster.setDateOfBirth( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PhysicianMaster DateOfBirth not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("PlaceOfBirth")) ;
    if ( !PhysicianMaster.getPlaceOfBirth().equals(testObj)  )
    {
         PhysicianMaster.setPlaceOfBirth( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PhysicianMaster PlaceOfBirth not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("Spouse")) ;
    if ( !PhysicianMaster.getSpouse().equals(testObj)  )
    {
         PhysicianMaster.setSpouse( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PhysicianMaster Spouse not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("CitizenshipYN")) ;
    if ( !PhysicianMaster.getCitizenshipYN().equals(testObj)  )
    {
         PhysicianMaster.setCitizenshipYN( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PhysicianMaster CitizenshipYN not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("Citizenship")) ;
    if ( !PhysicianMaster.getCitizenship().equals(testObj)  )
    {
         PhysicianMaster.setCitizenship( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PhysicianMaster Citizenship not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("VisaNumber")) ;
    if ( !PhysicianMaster.getVisaNumber().equals(testObj)  )
    {
         PhysicianMaster.setVisaNumber( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PhysicianMaster VisaNumber not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("VisaStatus")) ;
    if ( !PhysicianMaster.getVisaStatus().equals(testObj)  )
    {
         PhysicianMaster.setVisaStatus( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PhysicianMaster VisaStatus not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("EligibleToWorkInUS")) ;
    if ( !PhysicianMaster.getEligibleToWorkInUS().equals(testObj)  )
    {
         PhysicianMaster.setEligibleToWorkInUS( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PhysicianMaster EligibleToWorkInUS not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("SSN")) ;
    if ( !PhysicianMaster.getSSN().equals(testObj)  )
    {
         PhysicianMaster.setSSN( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PhysicianMaster SSN not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("Gender")) ;
    if ( !PhysicianMaster.getGender().equals(testObj)  )
    {
         PhysicianMaster.setGender( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PhysicianMaster Gender not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("MilitaryActive")) ;
    if ( !PhysicianMaster.getMilitaryActive().equals(testObj)  )
    {
         PhysicianMaster.setMilitaryActive( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PhysicianMaster MilitaryActive not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("MilitaryBranch")) ;
    if ( !PhysicianMaster.getMilitaryBranch().equals(testObj)  )
    {
         PhysicianMaster.setMilitaryBranch( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PhysicianMaster MilitaryBranch not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("MilitaryReserve")) ;
    if ( !PhysicianMaster.getMilitaryReserve().equals(testObj)  )
    {
         PhysicianMaster.setMilitaryReserve( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PhysicianMaster MilitaryReserve not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("HospitalAdmitingPrivileges")) ;
    if ( !PhysicianMaster.getHospitalAdmitingPrivileges().equals(testObj)  )
    {
         PhysicianMaster.setHospitalAdmitingPrivileges( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PhysicianMaster HospitalAdmitingPrivileges not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("HospitalAdmitingPrivilegesNo")) ;
    if ( !PhysicianMaster.getHospitalAdmitingPrivilegesNo().equals(testObj)  )
    {
         PhysicianMaster.setHospitalAdmitingPrivilegesNo( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PhysicianMaster HospitalAdmitingPrivilegesNo not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("PhysicianCategoryID")) ;
    if ( !PhysicianMaster.getPhysicianCategoryID().equals(testObj)  )
    {
         PhysicianMaster.setPhysicianCategoryID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PhysicianMaster PhysicianCategoryID not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("IPAMedicalAffiliation")) ;
    if ( !PhysicianMaster.getIPAMedicalAffiliation().equals(testObj)  )
    {
         PhysicianMaster.setIPAMedicalAffiliation( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PhysicianMaster IPAMedicalAffiliation not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("AffiliationDesc1")) ;
    if ( !PhysicianMaster.getAffiliationDesc1().equals(testObj)  )
    {
         PhysicianMaster.setAffiliationDesc1( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PhysicianMaster AffiliationDesc1 not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("PhysicianLanguages")) ;
    if ( !PhysicianMaster.getPhysicianLanguages().equals(testObj)  )
    {
         PhysicianMaster.setPhysicianLanguages( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PhysicianMaster PhysicianLanguages not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("ECFMGNo")) ;
    if ( !PhysicianMaster.getECFMGNo().equals(testObj)  )
    {
         PhysicianMaster.setECFMGNo( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PhysicianMaster ECFMGNo not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("ECFMGDateIssued"))) ;
    if ( !PhysicianMaster.getECFMGDateIssued().equals(testObj)  )
    {
         PhysicianMaster.setECFMGDateIssued( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PhysicianMaster ECFMGDateIssued not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("ECFMGDateExpires"))) ;
    if ( !PhysicianMaster.getECFMGDateExpires().equals(testObj)  )
    {
         PhysicianMaster.setECFMGDateExpires( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PhysicianMaster ECFMGDateExpires not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("MedicareUPIN")) ;
    if ( !PhysicianMaster.getMedicareUPIN().equals(testObj)  )
    {
         PhysicianMaster.setMedicareUPIN( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PhysicianMaster MedicareUPIN not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("UniqueNPI")) ;
    if ( !PhysicianMaster.getUniqueNPI().equals(testObj)  )
    {
         PhysicianMaster.setUniqueNPI( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PhysicianMaster UniqueNPI not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("MedicareParticipation")) ;
    if ( !PhysicianMaster.getMedicareParticipation().equals(testObj)  )
    {
         PhysicianMaster.setMedicareParticipation( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PhysicianMaster MedicareParticipation not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("MedicareNo")) ;
    if ( !PhysicianMaster.getMedicareNo().equals(testObj)  )
    {
         PhysicianMaster.setMedicareNo( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PhysicianMaster MedicareNo not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("MediCaidParticipation")) ;
    if ( !PhysicianMaster.getMediCaidParticipation().equals(testObj)  )
    {
         PhysicianMaster.setMediCaidParticipation( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PhysicianMaster MediCaidParticipation not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("MediCaidNo")) ;
    if ( !PhysicianMaster.getMediCaidNo().equals(testObj)  )
    {
         PhysicianMaster.setMediCaidNo( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PhysicianMaster MediCaidNo not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("SupplementalIDNumber1")) ;
    if ( !PhysicianMaster.getSupplementalIDNumber1().equals(testObj)  )
    {
         PhysicianMaster.setSupplementalIDNumber1( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PhysicianMaster SupplementalIDNumber1 not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("SupplementalIDNumber2")) ;
    if ( !PhysicianMaster.getSupplementalIDNumber2().equals(testObj)  )
    {
         PhysicianMaster.setSupplementalIDNumber2( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PhysicianMaster SupplementalIDNumber2 not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("Comments")) ;
    if ( !PhysicianMaster.getComments().equals(testObj)  )
    {
         PhysicianMaster.setComments( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PhysicianMaster Comments not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("AttestConsent")) ;
    if ( !PhysicianMaster.getAttestConsent().equals(testObj)  )
    {
         PhysicianMaster.setAttestConsent( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PhysicianMaster AttestConsent not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("AttestationComments")) ;
    if ( !PhysicianMaster.getAttestationComments().equals(testObj)  )
    {
         PhysicianMaster.setAttestationComments( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PhysicianMaster AttestationComments not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("IsViewable")) ;
    if ( !PhysicianMaster.getIsViewable().equals(testObj)  )
    {
         PhysicianMaster.setIsViewable( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PhysicianMaster IsViewable not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("IsAttested")) ;
    if ( !PhysicianMaster.getIsAttested().equals(testObj)  )
    {
         PhysicianMaster.setIsAttested( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PhysicianMaster IsAttested not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("AttestDatePending"))) ;
    if ( !PhysicianMaster.getAttestDatePending().equals(testObj)  )
    {
         PhysicianMaster.setAttestDatePending( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PhysicianMaster AttestDatePending not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("IsAttestedPending")) ;
    if ( !PhysicianMaster.getIsAttestedPending().equals(testObj)  )
    {
         PhysicianMaster.setIsAttestedPending( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PhysicianMaster IsAttestedPending not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("AttestDate"))) ;
    if ( !PhysicianMaster.getAttestDate().equals(testObj)  )
    {
         PhysicianMaster.setAttestDate( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PhysicianMaster AttestDate not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("DeAttestDate"))) ;
    if ( !PhysicianMaster.getDeAttestDate().equals(testObj)  )
    {
         PhysicianMaster.setDeAttestDate( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PhysicianMaster DeAttestDate not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("LastModifiedDate"))) ;
    if ( !PhysicianMaster.getLastModifiedDate().equals(testObj)  )
    {
         PhysicianMaster.setLastModifiedDate( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PhysicianMaster LastModifiedDate not set. this is ok-not an error");
}





try
{
    String testObj = new String(request.getParameter("QuestionValidation")) ;
    if ( !PhysicianMaster.getQuestionValidation().equals(testObj)  )
    {
         PhysicianMaster.setQuestionValidation( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PhysicianMaster QuestionValidation not set. this is ok-not an error");
}


try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("USMLEDatePassedStep1"))) ;
    if ( !PhysicianMaster.getUSMLEDatePassedStep1().equals(testObj)  )
    {
         PhysicianMaster.setUSMLEDatePassedStep1( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PhysicianMaster USMLEDatePassedStep1 not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("USMLEDatePassedStep2"))) ;
    if ( !PhysicianMaster.getUSMLEDatePassedStep2().equals(testObj)  )
    {
         PhysicianMaster.setUSMLEDatePassedStep2( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PhysicianMaster USMLEDatePassedStep2 not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("USMLEDatePassedStep3"))) ;
    if ( !PhysicianMaster.getUSMLEDatePassedStep3().equals(testObj)  )
    {
         PhysicianMaster.setUSMLEDatePassedStep3( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PhysicianMaster USMLEDatePassedStep3 not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("HaveFLEX")) ;
    if ( !PhysicianMaster.getHaveFLEX().equals(testObj)  )
    {
         PhysicianMaster.setHaveFLEX( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PhysicianMaster HaveFLEX not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("FLEXDatePassed"))) ;
    if ( !PhysicianMaster.getFLEXDatePassed().equals(testObj)  )
    {
         PhysicianMaster.setFLEXDatePassed( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PhysicianMaster FLEXDatePassed not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("VisaSponsor")) ;
    if ( !PhysicianMaster.getVisaSponsor().equals(testObj)  )
    {
         PhysicianMaster.setVisaSponsor( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PhysicianMaster VisaSponsor not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("VisaExpiration"))) ;
    if ( !PhysicianMaster.getVisaExpiration().equals(testObj)  )
    {
         PhysicianMaster.setVisaExpiration( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PhysicianMaster VisaExpiration not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("CurrentVisaTemporary")) ;
    if ( !PhysicianMaster.getCurrentVisaTemporary().equals(testObj)  )
    {
         PhysicianMaster.setCurrentVisaTemporary( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PhysicianMaster CurrentVisaTemporary not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("CurrentVisaExtended")) ;
    if ( !PhysicianMaster.getCurrentVisaExtended().equals(testObj)  )
    {
         PhysicianMaster.setCurrentVisaExtended( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PhysicianMaster CurrentVisaExtended not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("HaveGreencard")) ;
    if ( !PhysicianMaster.getHaveGreencard().equals(testObj)  )
    {
         PhysicianMaster.setHaveGreencard( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PhysicianMaster HaveGreencard not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("CountryOfIssueID")) ;
    if ( !PhysicianMaster.getCountryOfIssueID().equals(testObj)  )
    {
         PhysicianMaster.setCountryOfIssueID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PhysicianMaster CountryOfIssueID not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("VisaTemporary5Years")) ;
    if ( !PhysicianMaster.getVisaTemporary5Years().equals(testObj)  )
    {
         PhysicianMaster.setVisaTemporary5Years( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PhysicianMaster VisaTemporary5Years not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("PastVisa1DateFrom"))) ;
    if ( !PhysicianMaster.getPastVisa1DateFrom().equals(testObj)  )
    {
         PhysicianMaster.setPastVisa1DateFrom( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PhysicianMaster PastVisa1DateFrom not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("PastVisa1DateTo"))) ;
    if ( !PhysicianMaster.getPastVisa1DateTo().equals(testObj)  )
    {
         PhysicianMaster.setPastVisa1DateTo( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PhysicianMaster PastVisa1DateTo not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("PastVisa1Type")) ;
    if ( !PhysicianMaster.getPastVisa1Type().equals(testObj)  )
    {
         PhysicianMaster.setPastVisa1Type( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PhysicianMaster PastVisa1Type not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("PastVisa1Sponsor")) ;
    if ( !PhysicianMaster.getPastVisa1Sponsor().equals(testObj)  )
    {
         PhysicianMaster.setPastVisa1Sponsor( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PhysicianMaster PastVisa1Sponsor not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("PastVisa2DateFrom"))) ;
    if ( !PhysicianMaster.getPastVisa2DateFrom().equals(testObj)  )
    {
         PhysicianMaster.setPastVisa2DateFrom( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PhysicianMaster PastVisa2DateFrom not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("PastVisa2DateTo"))) ;
    if ( !PhysicianMaster.getPastVisa2DateTo().equals(testObj)  )
    {
         PhysicianMaster.setPastVisa2DateTo( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PhysicianMaster PastVisa2DateTo not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("PastVisa2Type")) ;
    if ( !PhysicianMaster.getPastVisa2Type().equals(testObj)  )
    {
         PhysicianMaster.setPastVisa2Type( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PhysicianMaster PastVisa2Type not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("PastVisa2Sponsor")) ;
    if ( !PhysicianMaster.getPastVisa2Sponsor().equals(testObj)  )
    {
         PhysicianMaster.setPastVisa2Sponsor( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PhysicianMaster PastVisa2Sponsor not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("MilitaryRank")) ;
    if ( !PhysicianMaster.getMilitaryRank().equals(testObj)  )
    {
         PhysicianMaster.setMilitaryRank( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PhysicianMaster MilitaryRank not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("MilitaryDutyStatus")) ;
    if ( !PhysicianMaster.getMilitaryDutyStatus().equals(testObj)  )
    {
         PhysicianMaster.setMilitaryDutyStatus( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PhysicianMaster MilitaryDutyStatus not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("MilitaryCurrentAssignment")) ;
    if ( !PhysicianMaster.getMilitaryCurrentAssignment().equals(testObj)  )
    {
         PhysicianMaster.setMilitaryCurrentAssignment( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PhysicianMaster MilitaryCurrentAssignment not set. this is ok-not an error");
}


boolean bINT = false;
boolean bNextStep = false;
String sRefreshDoc = "refreshMe=true&";

// If an edit, update information; if new, append information
if (testChangeID.equalsIgnoreCase("1"))
{

	PhysicianMaster.setUniqueModifyDate(new java.util.Date());
	String UserLogonDescription = "n/a";
	if (pageControllerHash.containsKey("UserLogonDescription"))
	{
	    UserLogonDescription = (String)pageControllerHash.get("UserLogonDescription");
	}
    //if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
    {
	    PhysicianMaster.setUniqueModifyComments(UserLogonDescription);
	    PhysicianMaster.commitData();
    }
}
String routePageReference = request.getParameter("routePageReference");
String nextPage=null;
if (routePageReference!=null)
{
        if (pageControllerHash.containsKey(routePageReference))
        {
            nextPage = (String)pageControllerHash.get(routePageReference);
        }
        else if (pageControllerHash.containsKey("sLocalChildReturnPage"))
        {
            nextPage = (String)pageControllerHash.get("sLocalChildReturnPage");
        }
        else if (pageControllerHash.containsKey("sParentReturnPage"))
        {
            nextPage = (String)pageControllerHash.get("sParentReturnPage");
        }
}
if (nextPage!=null)
{
	    %><script language=Javascript>
	      document.location="<%=nextPage%>";
	      </script><%
    //response.sendRedirect(nextPage+"?EDIT=edit");
}
        %>
Successful save

  <%
  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
  %>
