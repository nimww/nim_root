<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.bltDocumentManagement,java.util.Vector,java.util.Enumeration,com.winstaff.ListElement, com.winstaff.PLCUtils, com.winstaff.*, com.winstaff.SecurityCheck, com.winstaff.ConfigurationMessages, com.winstaff.bltPracticeActivity, com.winstaff.bltPracticeActivity_List_AdminID" %>
<%/*
    filename: tPracticeActivity_main_PracticeActivity_AdminID.jsp
    Created on Nov/04/2009
    Type: 1-n main class file
    Created by: Scott Ellis
    Version: 4.0
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>
    <%
    Integer iExpressMode = new Integer(1);
    if (pageControllerHash.containsKey("iExpressMode")) 
    {
        iExpressMode =    (Integer)pageControllerHash.get("iExpressMode");
    }
    %>

<link href="ui_1/style.css" rel="stylesheet" type="text/css" />

<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_AdminID.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>

    <table cellpadding=0 cellspacing=0 border=0 width=<%=MasterTableWidth%> >
    <tr><td width=10>&nbsp;</td><td>
    <%=ConfigurationMessages.getHTML("EXPRESSTopControl","tPracticeActivity",iExpressMode)%>



<%
//initial declaration of list class and parentID
    Integer        iAdminID        =    null;
    boolean accessValid = false;
   Integer iSecurityCheck = SecurityCheck.CheckItem("nim1", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {
    if (pageControllerHash.containsKey("iAdminID") ) 
    {
        iAdminID        =    (Integer)pageControllerHash.get("iAdminID");
        accessValid = true;
    }  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);
      pageControllerHash.put("sParentReturnPage","tPracticeActivity_main_PracticeActivity_AdminID_Expand.jsp");
    pageControllerHash.remove("iPracticeActivityID");
    session.setAttribute("pageControllerHash",pageControllerHash);

    bltPracticeActivity_List_AdminID        bltPracticeActivity_List_AdminID        =    new    bltPracticeActivity_List_AdminID(iAdminID);

//declaration of Enumeration
    bltPracticeActivity        PracticeActivity;
    ListElement         leCurrentElement;
    java.util.Enumeration eList = bltPracticeActivity_List_AdminID.elements();
    %>
        <%@ include file="tPracticeActivity_main_PracticeActivity_AdminID_instructions.jsp" %>

        <form action="tPracticeActivity_main_PracticeActivity_AdminID_Express_sub.jsp" name="tPracticeActivity_main_PracticeActivity_AdminID1" method="POST">
         <table border="0" bordercolor="333333" cellpadding="0"  cellspacing="0" width="100%">        <tr><td>
        <input <%=HTMLFormStyleButton%> onClick="this.disable=true;document.forms[0].nextPage.value='<%=ConfigurationMessages.getExpressLinkRaw("tPracticeActivity","next",iExpressMode)%>'" type=Submit value="Save & Continue" name=Submit>
        </td></tr>
    <%
    int iExpress=0;
    while (eList.hasMoreElements()||iExpress<=ConfigurationMessages.getExpressItemCount("tPracticeActivity"))
    {
       iExpress++;
         %>
            <tr>
              <td width="50%">
                   <table border="0" bordercolor="333333" cellpadding="0" class=tdHeaderAlt cellspacing="0" width="100%">
                   <tr> 
                   	<td rowspan="2"><img src=express/left-corner.gif></td>
                   	<td width=100%><img width=100% height=2 src=express/small-line.gif></td>
                   	<td rowspan="2" align=right><img src=express/right-corner.gif></td>
                   </tr>
                     <tr> 
                       <td>
         <%

      boolean isNewRecord= false;
      if (eList.hasMoreElements())
      {
        leCurrentElement    = (ListElement) eList.nextElement();
        PracticeActivity  = (bltPracticeActivity) leCurrentElement.getObject();
      }
      else
      {
        PracticeActivity  = new bltPracticeActivity();
        isNewRecord= true;
      }
        PracticeActivity.GroupSecurityInit(UserSecurityGroupID);
        String theClass = "tdHeader";
        %>
               <span class=<%=theClass%> ><b><%=ConfigurationMessages.getDataCategory("tPracticeActivity")%> #<%=iExpress%></span>
                  </td></tr></table>
            </td></tr>
                     <tr><td>
<%String theClassF = "textBase";%>
<%
if (isNewRecord)
{%>
<input type=hidden name=recordItemStatus_<%=iExpress%>="new">
<%}
else
{%>
<input type=hidden name=recordItemStatus_<%=iExpress%>="edit">
<%}

  {

        %>

          <%  theClass ="tdBase";%>
        <table width=100% border=1 bordercolor=#333333 cellpadding=3 cellspacing=0 class=tableBase><tr><td>
        <table cellpadding=0 border=0 cellspacing=0 width=100%>




            <%
            if ( (PracticeActivity.isRequired("AdminID",UserSecurityGroupID))&&(!PracticeActivity.isComplete("AdminID")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((PracticeActivity.isExpired("AdminID",expiredDays))&&(PracticeActivity.isExpiredCheck("AdminID",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                     <input type=hidden name="AdminID_<%=iExpress%>" value="<%=PracticeActivity.getAdminID()%>">

            <%
            if ( (PracticeActivity.isRequired("Name",UserSecurityGroupID))&&(!PracticeActivity.isComplete("Name")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((PracticeActivity.isExpired("Name",expiredDays))&&(PracticeActivity.isExpiredCheck("Name",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                     <input type=hidden name="Name_<%=iExpress%>" value="<%=PracticeActivity.getName()%>">

            <%
            if ( (PracticeActivity.isRequired("ActivityType",UserSecurityGroupID))&&(!PracticeActivity.isComplete("ActivityType")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((PracticeActivity.isExpired("ActivityType",expiredDays))&&(PracticeActivity.isExpiredCheck("ActivityType",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                     <input type=hidden name="ActivityType_<%=iExpress%>" value="<%=PracticeActivity.getActivityType()%>">

            <%
            if ( (PracticeActivity.isRequired("isCompleted",UserSecurityGroupID))&&(!PracticeActivity.isComplete("isCompleted")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((PracticeActivity.isExpired("isCompleted",expiredDays))&&(PracticeActivity.isExpiredCheck("isCompleted",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                     <input type=hidden name="isCompleted_<%=iExpress%>" value="<%=PracticeActivity.getisCompleted()%>">

            <%
            if ( (PracticeActivity.isRequired("StartDate",UserSecurityGroupID))&&(!PracticeActivity.isComplete("StartDate")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((PracticeActivity.isExpired("StartDate",expiredDays))&&(PracticeActivity.isExpiredCheck("StartDate",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                     <input type=hidden name="StartDate_<%=iExpress%>" value="<%=PracticeActivity.getStartDate()%>">

            <%
            if ( (PracticeActivity.isRequired("EndDate",UserSecurityGroupID))&&(!PracticeActivity.isComplete("EndDate")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((PracticeActivity.isExpired("EndDate",expiredDays))&&(PracticeActivity.isExpiredCheck("EndDate",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                     <input type=hidden name="EndDate_<%=iExpress%>" value="<%=PracticeActivity.getEndDate()%>">

            <%
            if ( (PracticeActivity.isRequired("RemindDate",UserSecurityGroupID))&&(!PracticeActivity.isComplete("RemindDate")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((PracticeActivity.isExpired("RemindDate",expiredDays))&&(PracticeActivity.isExpiredCheck("RemindDate",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                     <input type=hidden name="RemindDate_<%=iExpress%>" value="<%=PracticeActivity.getRemindDate()%>">

            <%
            if ( (PracticeActivity.isRequired("Description",UserSecurityGroupID))&&(!PracticeActivity.isComplete("Description")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((PracticeActivity.isExpired("Description",expiredDays))&&(PracticeActivity.isExpiredCheck("Description",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                     <input type=hidden name="Description_<%=iExpress%>" value="<%=PracticeActivity.getDescription()%>">

            <%
            if ( (PracticeActivity.isRequired("TaskFileReference",UserSecurityGroupID))&&(!PracticeActivity.isComplete("TaskFileReference")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((PracticeActivity.isExpired("TaskFileReference",expiredDays))&&(PracticeActivity.isExpiredCheck("TaskFileReference",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                     <input type=hidden name="TaskFileReference_<%=iExpress%>" value="<%=PracticeActivity.getTaskFileReference()%>">

            <%
            if ( (PracticeActivity.isRequired("TaskLogicReference",UserSecurityGroupID))&&(!PracticeActivity.isComplete("TaskLogicReference")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((PracticeActivity.isExpired("TaskLogicReference",expiredDays))&&(PracticeActivity.isExpiredCheck("TaskLogicReference",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                     <input type=hidden name="TaskLogicReference_<%=iExpress%>" value="<%=PracticeActivity.getTaskLogicReference()%>">

            <%
            if ( (PracticeActivity.isRequired("Summary",UserSecurityGroupID))&&(!PracticeActivity.isComplete("Summary")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((PracticeActivity.isExpired("Summary",expiredDays))&&(PracticeActivity.isExpiredCheck("Summary",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                     <input type=hidden name="Summary_<%=iExpress%>" value="<%=PracticeActivity.getSummary()%>">

            <%
            if ( (PracticeActivity.isRequired("Comments",UserSecurityGroupID))&&(!PracticeActivity.isComplete("Comments")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((PracticeActivity.isExpired("Comments",expiredDays))&&(PracticeActivity.isExpiredCheck("Comments",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                     <input type=hidden name="Comments_<%=iExpress%>" value="<%=PracticeActivity.getComments()%>">

            <%
            if ( (PracticeActivity.isRequired("ReferenceID",UserSecurityGroupID))&&(!PracticeActivity.isComplete("ReferenceID")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((PracticeActivity.isExpired("ReferenceID",expiredDays))&&(PracticeActivity.isExpiredCheck("ReferenceID",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                     <input type=hidden name="ReferenceID_<%=iExpress%>" value="<%=PracticeActivity.getReferenceID()%>">

            <%
            if ( (PracticeActivity.isRequired("DoculinkID",UserSecurityGroupID))&&(!PracticeActivity.isComplete("DoculinkID")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((PracticeActivity.isExpired("DoculinkID",expiredDays))&&(PracticeActivity.isExpiredCheck("DoculinkID",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                     <input type=hidden name="DoculinkID_<%=iExpress%>" value="<%=PracticeActivity.getDoculinkID()%>">




            <input type=hidden name=routePageReference value="sParentReturnPage">

        </table>
        </td></tr></table>
        </td></tr>
        <tr><td>&nbsp;</td></tr>
        <%
  }%>

        <%
    }
    %>
        <tr><td>
        <input <%=HTMLFormStyleButton%> onClick="this.disable=true;document.forms[0].nextPage.value='<%=ConfigurationMessages.getExpressLinkRaw("tPracticeActivity","next",iExpressMode)%>'" type=Submit value="Save & Continue" name=Submit>
        <input type=hidden name=nextPage value="">
        </td></tr> </table>
        </form>

    </table>
    <%
  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>

    </td></tr></table>


<%String bnIncludeFN = "ui_"+thePLCID+"\\bot-nav_AdminID.jsp";
%>
<jsp:include page="<%=bnIncludeFN%>" flush="true" ><jsp:param name="plcID" value="<%=thePLCID%>"/></jsp:include>