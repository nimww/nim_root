<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.bltDocumentManagement,java.util.Vector,java.util.Enumeration,com.winstaff.ListElement, com.winstaff.PLCUtils, com.winstaff.*, com.winstaff.SecurityCheck, com.winstaff.ConfigurationMessages, com.winstaff.bltPracticeActivity, com.winstaff.bltPracticeActivity_List" %>
<%/*
    filename: tPracticeActivity_main_PracticeActivity_PracticeID.jsp
    Created on Oct/30/2009
    Type: 1-n main class file
    Created by: Scott Ellis
    Version: 4.0
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>
    <%
    Integer iExpressMode = new Integer(1);
    String MasterTableWidthVO = "100%";
    if (pageControllerHash.containsKey("iExpressMode")) 
    {
        iExpressMode =    (Integer)pageControllerHash.get("iExpressMode");
    }
    if (pageControllerHash.containsKey("MasterTableWidthVO")) 
    {
        MasterTableWidthVO = (String)pageControllerHash.get("MasterTableWidthVO");
    }
    %>

<link href="ui_1/style.css" rel="stylesheet" type="text/css" />

<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_Clear.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>

    <table cellpadding=0 cellspacing=0 border=0 width=<%=MasterTableWidthVO%> >
    <tr><td width=10>&nbsp;</td><td>




<%
//initial declaration of list class and parentID
    Integer        iPracticeID        =    null;
    boolean accessValid = false;
   Integer iSecurityCheck = SecurityCheck.CheckItem("nim1", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {
    if (pageControllerHash.containsKey("iPracticeID") ) 
    {
        iPracticeID        =    (Integer)pageControllerHash.get("iPracticeID");
        accessValid = true;
    }  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);
      pageControllerHash.put("sParentReturnPage","tPracticeActivity_main_PracticeActivity_PracticeID_Expand.jsp");
    pageControllerHash.remove("iPracticeActivityID");
    session.setAttribute("pageControllerHash",pageControllerHash);

    bltPracticeActivity_List        bltPracticeActivity_List        =    new    bltPracticeActivity_List(iPracticeID);

//declaration of Enumeration
    bltPracticeActivity        PracticeActivity;
    ListElement         leCurrentElement;
    java.util.Enumeration eList = bltPracticeActivity_List.elements();
    %>
        <%@ include file="tPracticeActivity_main_PracticeActivity_PracticeID_instructions.jsp" %>


    <%
    int iExpress=0;
    while (eList.hasMoreElements())
    {
       iExpress++;
         %>
         <table border="0" bordercolor="333333" cellpadding="0" class=tdHeaderAlt cellspacing="0" width="100%">
            <tr>
              <td width="50%">
                   <table border="0" bordercolor="333333" cellpadding="0" class=tdHeaderAlt cellspacing="0" width="100%">
                   <tr> 
                   	<td rowspan="2"><img src=express/left-corner.gif></td>
                   	<td width=100%><img width=100% height=2 src=express/small-line.gif></td>
                   	<td rowspan="2" align=right><img src=express/right-corner.gif></td>
                   </tr>
                     <tr> 
                       <td>
         <%

      boolean isNewRecord= false;
      if (eList.hasMoreElements())
      {
        leCurrentElement    = (ListElement) eList.nextElement();
        PracticeActivity  = (bltPracticeActivity) leCurrentElement.getObject();
      }
      else
      {
        PracticeActivity  = new bltPracticeActivity();
        isNewRecord= true;
      }
        PracticeActivity.GroupSecurityInit(UserSecurityGroupID);
        String theClass = "tdHeader";
        %>
               <span class=<%=theClass%> ><b><%=ConfigurationMessages.getDataCategory("tPracticeActivity")%> #<%=iExpress%></span>
                  </td></tr></table>
            </td></tr>
                 </table>
                 <table width="100%" border="1" cellspacing="0" bordercolor="#333333">
                  <tr>
                   <td>
                     <table width="100%" border="0" cellspacing="0">
                     <tr><td>
<%String theClassF = "textBase";%>
<%
if (isNewRecord)
{%>
<input type=hidden name=recordItemStatus_<%=iExpress%>="new">
<%}
else
{%>
<input type=hidden name=recordItemStatus_<%=iExpress%>="edit">
<%}

  {

        %>

          <%  theClass ="tdBase";%>
        <table width=100% border=1 bordercolor=#333333 cellpadding=3 cellspacing=0 class=tableBase>
         <tr><td class=tableColor>


            <table cellpadding=0 cellspacing=0 width=100%>
            <%
            if ( (PracticeActivity.isRequired("Name",UserSecurityGroupID))&&(!PracticeActivity.isComplete("Name")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((PracticeActivity.isExpired("Name",expiredDays))&&(PracticeActivity.isExpiredCheck("Name",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((PracticeActivity.isWrite("Name",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Name&nbsp;</b></p></td><td valign=top><p><%=PracticeActivity.getName()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Name&amp;sTableName=tPracticeActivity&amp;sRefID=<%=PracticeActivity.getPracticeActivityID()%>&amp;sFieldNameDisp=<%=PracticeActivity.getEnglish("Name")%>&amp;sTableNameDisp=tPracticeActivity','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((PracticeActivity.isRead("Name",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Name&nbsp;</b></p></td><td valign=top><p><%=PracticeActivity.getName()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Name&amp;sTableName=tPracticeActivity&amp;sRefID=<%=PracticeActivity.getPracticeActivityID()%>&amp;sFieldNameDisp=<%=PracticeActivity.getEnglish("Name")%>&amp;sTableNameDisp=tPracticeActivity','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (PracticeActivity.isRequired("ActivityType",UserSecurityGroupID))&&(!PracticeActivity.isComplete("ActivityType")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((PracticeActivity.isExpired("ActivityType",expiredDays))&&(PracticeActivity.isExpiredCheck("ActivityType",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((PracticeActivity.isWrite("ActivityType",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>ActivityType&nbsp;</b></p></td><td valign=top><p><%=PracticeActivity.getActivityType()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ActivityType&amp;sTableName=tPracticeActivity&amp;sRefID=<%=PracticeActivity.getPracticeActivityID()%>&amp;sFieldNameDisp=<%=PracticeActivity.getEnglish("ActivityType")%>&amp;sTableNameDisp=tPracticeActivity','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((PracticeActivity.isRead("ActivityType",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>ActivityType&nbsp;</b></p></td><td valign=top><p><%=PracticeActivity.getActivityType()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ActivityType&amp;sTableName=tPracticeActivity&amp;sRefID=<%=PracticeActivity.getPracticeActivityID()%>&amp;sFieldNameDisp=<%=PracticeActivity.getEnglish("ActivityType")%>&amp;sTableNameDisp=tPracticeActivity','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (PracticeActivity.isRequired("isCompleted",UserSecurityGroupID))&&(!PracticeActivity.isComplete("isCompleted")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((PracticeActivity.isExpired("isCompleted",expiredDays))&&(PracticeActivity.isExpiredCheck("isCompleted",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((PracticeActivity.isWrite("isCompleted",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>isCompleted&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PracticeActivity.getisCompleted()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=isCompleted&amp;sTableName=tPracticeActivity&amp;sRefID=<%=PracticeActivity.getPracticeActivityID()%>&amp;sFieldNameDisp=<%=PracticeActivity.getEnglish("isCompleted")%>&amp;sTableNameDisp=tPracticeActivity','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((PracticeActivity.isRead("isCompleted",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>isCompleted&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PracticeActivity.getisCompleted()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=isCompleted&amp;sTableName=tPracticeActivity&amp;sRefID=<%=PracticeActivity.getPracticeActivityID()%>&amp;sFieldNameDisp=<%=PracticeActivity.getEnglish("isCompleted")%>&amp;sTableNameDisp=tPracticeActivity','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (PracticeActivity.isRequired("StartDate",UserSecurityGroupID))&&(!PracticeActivity.isComplete("StartDate")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((PracticeActivity.isExpired("StartDate",expiredDays))&&(PracticeActivity.isExpiredCheck("StartDate",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>

            <%
            if ((PracticeActivity.isWrite("StartDate",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>StartDate&nbsp;(mm/dd/yyyy):&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(PracticeActivity.getStartDate())%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=StartDate&amp;sTableName=tPracticeActivity&amp;sRefID=<%=PracticeActivity.getPracticeActivityID()%>&amp;sFieldNameDisp=<%=PracticeActivity.getEnglish("StartDate")%>&amp;sTableNameDisp=tPracticeActivity','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((PracticeActivity.isRead("StartDate",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>StartDate&nbsp;(mm/dd/yyyy):&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(PracticeActivity.getStartDate())%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=StartDate&amp;sTableName=tPracticeActivity&amp;sRefID=<%=PracticeActivity.getPracticeActivityID()%>&amp;sFieldNameDisp=<%=PracticeActivity.getEnglish("StartDate")%>&amp;sTableNameDisp=tPracticeActivity','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (PracticeActivity.isRequired("EndDate",UserSecurityGroupID))&&(!PracticeActivity.isComplete("EndDate")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((PracticeActivity.isExpired("EndDate",expiredDays))&&(PracticeActivity.isExpiredCheck("EndDate",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>

            <%
            if ((PracticeActivity.isWrite("EndDate",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>EndDate&nbsp;(mm/dd/yyyy):&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(PracticeActivity.getEndDate())%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=EndDate&amp;sTableName=tPracticeActivity&amp;sRefID=<%=PracticeActivity.getPracticeActivityID()%>&amp;sFieldNameDisp=<%=PracticeActivity.getEnglish("EndDate")%>&amp;sTableNameDisp=tPracticeActivity','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((PracticeActivity.isRead("EndDate",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>EndDate&nbsp;(mm/dd/yyyy):&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(PracticeActivity.getEndDate())%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=EndDate&amp;sTableName=tPracticeActivity&amp;sRefID=<%=PracticeActivity.getPracticeActivityID()%>&amp;sFieldNameDisp=<%=PracticeActivity.getEnglish("EndDate")%>&amp;sTableNameDisp=tPracticeActivity','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (PracticeActivity.isRequired("RemindDate",UserSecurityGroupID))&&(!PracticeActivity.isComplete("RemindDate")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((PracticeActivity.isExpired("RemindDate",expiredDays))&&(PracticeActivity.isExpiredCheck("RemindDate",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>

            <%
            if ((PracticeActivity.isWrite("RemindDate",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>RemindDate&nbsp;(mm/dd/yyyy):&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(PracticeActivity.getRemindDate())%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=RemindDate&amp;sTableName=tPracticeActivity&amp;sRefID=<%=PracticeActivity.getPracticeActivityID()%>&amp;sFieldNameDisp=<%=PracticeActivity.getEnglish("RemindDate")%>&amp;sTableNameDisp=tPracticeActivity','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((PracticeActivity.isRead("RemindDate",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>RemindDate&nbsp;(mm/dd/yyyy):&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(PracticeActivity.getRemindDate())%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=RemindDate&amp;sTableName=tPracticeActivity&amp;sRefID=<%=PracticeActivity.getPracticeActivityID()%>&amp;sFieldNameDisp=<%=PracticeActivity.getEnglish("RemindDate")%>&amp;sTableNameDisp=tPracticeActivity','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (PracticeActivity.isRequired("Description",UserSecurityGroupID))&&(!PracticeActivity.isComplete("Description")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((PracticeActivity.isExpired("Description",expiredDays))&&(PracticeActivity.isExpiredCheck("Description",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((PracticeActivity.isWrite("Description",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Description&nbsp;</b></p></td><td valign=top><p><%=PracticeActivity.getDescription()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Description&amp;sTableName=tPracticeActivity&amp;sRefID=<%=PracticeActivity.getPracticeActivityID()%>&amp;sFieldNameDisp=<%=PracticeActivity.getEnglish("Description")%>&amp;sTableNameDisp=tPracticeActivity','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((PracticeActivity.isRead("Description",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Description&nbsp;</b></p></td><td valign=top><p><%=PracticeActivity.getDescription()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Description&amp;sTableName=tPracticeActivity&amp;sRefID=<%=PracticeActivity.getPracticeActivityID()%>&amp;sFieldNameDisp=<%=PracticeActivity.getEnglish("Description")%>&amp;sTableNameDisp=tPracticeActivity','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (PracticeActivity.isRequired("TaskFileReference",UserSecurityGroupID))&&(!PracticeActivity.isComplete("TaskFileReference")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((PracticeActivity.isExpired("TaskFileReference",expiredDays))&&(PracticeActivity.isExpiredCheck("TaskFileReference",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((PracticeActivity.isWrite("TaskFileReference",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>TaskFileReference&nbsp;</b></p></td><td valign=top><p><%=PracticeActivity.getTaskFileReference()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=TaskFileReference&amp;sTableName=tPracticeActivity&amp;sRefID=<%=PracticeActivity.getPracticeActivityID()%>&amp;sFieldNameDisp=<%=PracticeActivity.getEnglish("TaskFileReference")%>&amp;sTableNameDisp=tPracticeActivity','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((PracticeActivity.isRead("TaskFileReference",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>TaskFileReference&nbsp;</b></p></td><td valign=top><p><%=PracticeActivity.getTaskFileReference()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=TaskFileReference&amp;sTableName=tPracticeActivity&amp;sRefID=<%=PracticeActivity.getPracticeActivityID()%>&amp;sFieldNameDisp=<%=PracticeActivity.getEnglish("TaskFileReference")%>&amp;sTableNameDisp=tPracticeActivity','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (PracticeActivity.isRequired("TaskLogicReference",UserSecurityGroupID))&&(!PracticeActivity.isComplete("TaskLogicReference")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((PracticeActivity.isExpired("TaskLogicReference",expiredDays))&&(PracticeActivity.isExpiredCheck("TaskLogicReference",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((PracticeActivity.isWrite("TaskLogicReference",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>TaskLogicReference&nbsp;</b></p></td><td valign=top><p><%=PracticeActivity.getTaskLogicReference()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=TaskLogicReference&amp;sTableName=tPracticeActivity&amp;sRefID=<%=PracticeActivity.getPracticeActivityID()%>&amp;sFieldNameDisp=<%=PracticeActivity.getEnglish("TaskLogicReference")%>&amp;sTableNameDisp=tPracticeActivity','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((PracticeActivity.isRead("TaskLogicReference",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>TaskLogicReference&nbsp;</b></p></td><td valign=top><p><%=PracticeActivity.getTaskLogicReference()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=TaskLogicReference&amp;sTableName=tPracticeActivity&amp;sRefID=<%=PracticeActivity.getPracticeActivityID()%>&amp;sFieldNameDisp=<%=PracticeActivity.getEnglish("TaskLogicReference")%>&amp;sTableNameDisp=tPracticeActivity','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (PracticeActivity.isRequired("Summary",UserSecurityGroupID))&&(!PracticeActivity.isComplete("Summary")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((PracticeActivity.isExpired("Summary",expiredDays))&&(PracticeActivity.isExpiredCheck("Summary",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((PracticeActivity.isWrite("Summary",UserSecurityGroupID)))
            {
                        %>
                       <tr><td valign=top> <p class=<%=theClass%> ><b><%=PracticeActivity.getEnglish("Summary")%>&nbsp;</b></p></td><td valign=top><p><%=PracticeActivity.getSummary()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Summary&amp;sTableName=tPracticeActivity&amp;sRefID=<%=PracticeActivity.getPracticeActivityID()%>&amp;sFieldNameDisp=<%=PracticeActivity.getEnglish("Summary")%>&amp;sTableNameDisp=tPracticeActivity','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((PracticeActivity.isRead("Summary",UserSecurityGroupID)))
            {
                        %>
                       <tr><td valign=top> <p class=<%=theClass%> ><b><%=PracticeActivity.getEnglish("Summary")%>&nbsp;</b></p></td><td valign=top><p><%=PracticeActivity.getSummary()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Summary&amp;sTableName=tPracticeActivity&amp;sRefID=<%=PracticeActivity.getPracticeActivityID()%>&amp;sFieldNameDisp=<%=PracticeActivity.getEnglish("Summary")%>&amp;sTableNameDisp=tPracticeActivity','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (PracticeActivity.isRequired("Comments",UserSecurityGroupID))&&(!PracticeActivity.isComplete("Comments")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((PracticeActivity.isExpired("Comments",expiredDays))&&(PracticeActivity.isExpiredCheck("Comments",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((PracticeActivity.isWrite("Comments",UserSecurityGroupID)))
            {
                        %>
                       <tr><td valign=top> <p class=<%=theClass%> ><b><%=PracticeActivity.getEnglish("Comments")%>&nbsp;</b></p></td><td valign=top><p><%=PracticeActivity.getComments()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Comments&amp;sTableName=tPracticeActivity&amp;sRefID=<%=PracticeActivity.getPracticeActivityID()%>&amp;sFieldNameDisp=<%=PracticeActivity.getEnglish("Comments")%>&amp;sTableNameDisp=tPracticeActivity','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((PracticeActivity.isRead("Comments",UserSecurityGroupID)))
            {
                        %>
                       <tr><td valign=top> <p class=<%=theClass%> ><b><%=PracticeActivity.getEnglish("Comments")%>&nbsp;</b></p></td><td valign=top><p><%=PracticeActivity.getComments()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Comments&amp;sTableName=tPracticeActivity&amp;sRefID=<%=PracticeActivity.getPracticeActivityID()%>&amp;sFieldNameDisp=<%=PracticeActivity.getEnglish("Comments")%>&amp;sTableNameDisp=tPracticeActivity','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (PracticeActivity.isRequired("ReferenceID",UserSecurityGroupID))&&(!PracticeActivity.isComplete("ReferenceID")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((PracticeActivity.isExpired("ReferenceID",expiredDays))&&(PracticeActivity.isExpiredCheck("ReferenceID",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((PracticeActivity.isWrite("ReferenceID",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>ReferenceID&nbsp;</b></p></td><td valign=top><p><%=PracticeActivity.getReferenceID()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ReferenceID&amp;sTableName=tPracticeActivity&amp;sRefID=<%=PracticeActivity.getPracticeActivityID()%>&amp;sFieldNameDisp=<%=PracticeActivity.getEnglish("ReferenceID")%>&amp;sTableNameDisp=tPracticeActivity','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((PracticeActivity.isRead("ReferenceID",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>ReferenceID&nbsp;</b></p></td><td valign=top><p><%=PracticeActivity.getReferenceID()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ReferenceID&amp;sTableName=tPracticeActivity&amp;sRefID=<%=PracticeActivity.getPracticeActivityID()%>&amp;sFieldNameDisp=<%=PracticeActivity.getEnglish("ReferenceID")%>&amp;sTableNameDisp=tPracticeActivity','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>




            <tr><td width=40%>&nbsp;</td><td width=60%>&nbsp;</td></tr>



        </td></tr></table>
        </td></tr></table>
        <%
  }%>

        </td></tr></table></table><br>        <%
    }
    %>




    </table>
    <%
  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>

    </td></tr></table>


