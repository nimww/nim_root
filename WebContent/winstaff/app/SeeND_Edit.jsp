
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.*" %>
<%/*
    filename: tAdminPracticeLU_main_LU_AdminID.jsp
    Created on Apr/23/2002
    Type: 1-n main class file
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>

<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_Clear.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>

    <table cellpadding=0 cellspacing=0 border=0>
    <tr><td width=10>&nbsp;</td>
    <td>
<p> <br />
   <span class=title>See NetDev Response</span>
<p>
  <input class="tdBaseAltGrey" type="button" value="Return" name="Return" onclick="document.location='AdminPracticeAll_home-seend.jsp';" />
<p>
<%
if (isScheduler3)    
{
	%>
    You are currently viewing the NetDev Worklist as a Scheduler Level 3 (Admin).  <br />
    <a href="../nim3/Billing_Home.jsp"> Return to ScanPass Billing Home</a>
    <%
}
%>


    <%
//initial declaration of list class and parentID
    Integer        iGenAdminID        =    null;
    Integer        iCommTrackID        =    null;
	String		savedSQL = null;
	String		savedURL = null;
    boolean accessValid = false;
    if ( (isScheduler3||pageControllerHash.containsKey("iGenAdminID"))&&request.getParameter("EDITID")!=null) 
    {
        iCommTrackID        =    new Integer(request.getParameter("EDITID"));
//        iGenAdminID        =    (Integer)pageControllerHash.get("iGenAdminID");
        accessValid = true;
    }
  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat dbdft = new java.text.SimpleDateFormat(PLCUtils.String_dbdft);
      java.text.SimpleDateFormat dbdf_day = new java.text.SimpleDateFormat(PLCUtils.String_displayDateDayWeek);
      java.text.SimpleDateFormat displayDateTimeSDF = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1Full);
//      pageControllerHash.put("sParentReturnPage","AdminPracticeAll_home-acc.jsp");
//			session.setAttribute("pageControllerHash",pageControllerHash);
				bltNIM3_CommTrack myCT = new bltNIM3_CommTrack (iCommTrackID);
				NIM3_EncounterObject2 myEO2 = new NIM3_EncounterObject2(myCT.getEncounterID(),"loading");
				int hoursSince = Math.abs(PLCUtils.getHoursTill(myCT.getCommStart()));
				String myClass = "";
				if (hoursSince>48)
				{
					myClass = "tdBaseAltRed";
				}
				else if (hoursSince>24 || myEO2.isEscalatedSTAT())
				{
					myClass = "tdBaseAltOrange";
				}
				else if (hoursSince>12 )
				{
					myClass = "tdBaseAltYellow";
				}
				else
				{
					myClass = "tdBaseAltGrey";
				}
				%>
				<table cellpadding="10" cellspacing="10" border="0">
<tr class="<%=myClass%>" >
                <td><em><strong><%=myCT.getCommTrackID()%></strong></em></td>
                <td>Request
				</td>
                <td>
                <%
				if (myCT.getCommTypeID().intValue()==4)
				{
					out.print("SeeND");
				}
                %>
                </td>
                <td colspan=2>
                <%
				if (myCT.getCommEnd().after(NIMUtils.getBeforeTime()))
				{
					out.print(displayDateTimeSDF.format(myCT.getCommStart()) + "<br>(Dur: <strong>" + Math.round((myCT.getCommEnd().getTime()-myCT.getCommStart().getTime())/1000/60) + "</strong> min or <strong>" + Math.round((myCT.getCommEnd().getTime()-myCT.getCommStart().getTime())/1000/60/60) + " hours</strong>)");
				}
				else 
				{
					out.print("" + displayDateTimeSDF.format(myCT.getCommStart()) + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Item Still Open&nbsp;&nbsp;&nbsp;Started: <strong>" + PLCUtils.getHoursTill_Display(myCT.getCommStart(), false)+ "  (" + PLCUtils.getDaysTill_Display(myCT.getCommStart(), true) + ")</strong>");
				}
				%>
				</td>
                <td colspan=1>From:<strong><%=myCT.getMessageName()%></strong></td>
				<td colspan=1><jsp:include page="../generic/tCommTrackAlertStatusCodeLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=myCT.getAlertStatusCode()%>" /></jsp:include>
				</td></tr>
				<tr class=<%=myClass%> >
                	<td colspan=8>
                    	<table border=1 cellpadding=5 bgcolor=EEEEEE>	
                   	  <tr>
                            	<td valign="top">Request:<br><textarea cols=80 rows=8 readonly><%=myCT.getMessageText()%></textarea></td>
								<td valign="top">Response: &nbsp;&nbsp;
								  <input name="none1" class="tdBaseAltYellow" type="button" id="none1" onclick="document.getElementById('Comments').value = '[' + Date() + ' - <%=CurrentUserAccount.getLogonUserName()%>]\n\n'+document.getElementById('Comments').value;" value="Add Date/Time Stamp" />
<form action="SeeND_Edit_sub.jsp" method="POST" name="response">
<input name="EDITID" type="hidden" value="<%=myCT.getCommTrackID()%>" />
                                <textarea cols=80 rows=8 name="Comments" id="Comments" ><%=myCT.getComments()%></textarea><br />
<input name="AlertStatusCode" type="radio" value="0" <%if (myCT.getAlertStatusCode()==0){%> checked <%}%> /> Keep Open&nbsp;&nbsp;&nbsp;
<input name="AlertStatusCode" type="radio" value="2" <%if (myCT.getAlertStatusCode()==2){%> checked <%}%> /> 
(+) Schedule In Network&nbsp;&nbsp;&nbsp;
<input name="AlertStatusCode" type="radio" value="3" <%if (myCT.getAlertStatusCode()==3){%> checked <%}%> />
(-) Can't Schedule In Network&nbsp;&nbsp;&nbsp;
<%
if (isScheduler3)
{
	%>
    <hr />
<span class="tdBaseAlt_Test">    Scheduler Admin Options:<br />
<input name="isCourtesy" type="radio" value="-1" checked /> Same<br />
<input name="isCourtesy" type="radio" value="1"  />
Courtesy=Yes <br />
 <input name="isCourtesy" type="radio" value="2"  />
Courtesy=No<br />
 <input name="isCourtesy" type="radio" value="3"  />
 Decline Case<br /> <%
}
%></span>
<br />
<input class="tdBaseAltYellow" type=Submit value="Submit" name="Submit"></form>
</td></tr>
                            <tr>
                            	<td colspan=2>
<%                                
      java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);
%>
<table bgcolor="#EEEEEE" width="98%" border="1" cellspacing="0" cellpadding="1">
  <tr class="tdHeaderAlt">
    <td class="tdHeader">Payer</td>
    <td class="tdHeader">Patient</td>
    <td class="tdHeader">Pre-Screen</td>
    <td nowrap="nowrap" class="tdHeader">Important Notes</td>
    <td class="tdHeader">Procedure&nbsp;&nbsp;&nbsp;&nbsp;[ScanPass: <%=myEO2.getNIM3_Encounter().getScanPass() %>]</td>
  </tr>
  <tr>
    <td valign="middle" ><strong><%=myEO2.getPayerFullName()%></strong><br>ADJ: <%=myEO2.getCase_Adjuster().getContactFirstName()%> <%=myEO2.getCase_Adjuster().getContactLastName()%><br>
Claim#: <%=myEO2.getNIM3_CaseAccount().getCaseClaimNumber()%></td>
    <td valign="middle" nowrap="nowrap"><strong><%=myEO2.getNIM3_CaseAccount().getPatientLastName()%></strong>, <strong><%=myEO2.getNIM3_CaseAccount().getPatientFirstName()%></strong></td>
    <td rowspan="9" valign="top">
	<%{
	java.util.Vector myV = myEO2.getPatientPreScreen();
	for (int iTemp=0;iTemp<myV.size();iTemp++)
	{
		if (iTemp!=0)
		{
			out.print("<br>");
		}
		out.print(myV.elementAt(iTemp));
	}
	}%>&nbsp;</td>
    <td rowspan="9" valign="middle"  <% if(!myEO2.getCaseDashboard_CombinedNotes("<br>").equalsIgnoreCase("")){out.println(" class=\"tdBaseAlt_Action2\" ");}%>><%=myEO2.getCaseDashboard_CombinedNotes("<br>")%></td>
    <td rowspan="9" valign="top">
	  <%
if (myEO2.getNIM3_Referral().getRxFileID()!=0)
{
%>
	  <a target="_blank" href="../tNIM3_Document_main_NIM3_Document_CaseID_form_authorize.jsp?EDIT=view&amp;EDITID=<%=myEO2.getNIM3_Referral().getRxFileID()%>&amp;KM=p';" class="tdHeader" >View Rx</a><hr />
      <%
}
%>
	Is STAT: <strong><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=myEO2.getNIM3_Encounter().getisSTAT()%>" /></jsp:include></strong>
<hr />	Requires Aging: <strong><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=myEO2.getNIM3_Encounter().getRequiresAgeInjury()%>" /></jsp:include></strong><hr />
Courtesy? <strong><jsp:include page="../generic/tCourtesyLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=myEO2.getNIM3_Encounter().getisCourtesy()%>" /></jsp:include></strong>
<hr />

	<table border="1" cellspacing="0" cellpadding="2">
	  <tr>
	    <td>CPT</td>
	    <td>BP</td>
	    <td>Payer($)</td>
	    </tr>
<%{
		
	//declaration of Enumeration
		bltNIM3_Service        NIM3_Service;
		ListElement         leCurrentElement;
		java.util.Enumeration eList = myEO2.getNIM3_Service_List().elements();
		while (eList.hasMoreElements())
		{
			leCurrentElement    = (ListElement) eList.nextElement();
			NIM3_Service  = (bltNIM3_Service) leCurrentElement.getObject();
			%>
    	  <tr>
	    <td valign="top"><strong><%=NIM3_Service.getCPT()%></strong></td>
	    <td valign="top"><strong><%=NIM3_Service.getCPTBodyPart()%></strong></td>
	    <td valign="top"><%=NIMUtils.getPayerAllowAmount(myEO2.getNIM3_PayerMaster().getPayerID(), myEO2.getNIM3_CaseAccount().getPatientZIP(), NIM3_Service.getCPT(),NIM3_Service.getCPTModifier(),PLCUtils.getToday())%>&nbsp;</td>
	    </tr>
<%
		}
		
%>
	  </table>
	  </p></td>
  </tr>
  <tr>
    <td valign="middle" nowrap="nowrap" rowspan="9">Contract Rates:<br /><%=myEO2.getFeeScheduleRateTablePayerMRI("<br>")%></td>
    <td valign="middle" nowrap="nowrap">DOB: <strong><%=PLCUtils.getDisplayDate(myEO2.getNIM3_CaseAccount().getPatientDOB(),false)%></strong></td>
  </tr>
  <tr>
    <td valign="middle" nowrap="nowrap">Address: <strong><%=myEO2.getPatientAddressObject().getAddressFull("&nbsp;&nbsp;&nbsp;")%></strong><input type="button" class="tdBaseAltCyan"  onclick="this.disabled=false;modalPost('Schedule', modalWin('../nim3/LI_ModalPass.jsp?ssPF=tNIM3_Encounter_main_NIM3_Encounter_ReferralID_form_authorize.jsp&EDIT=sched&PreSchedule=nd&EDITID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&KM=p','Schedule...','dialogWidth:1400px;dialogHeight:1000px','status=yes,scrollbars=yes,resizable=yes,width=1400,height=1000'))" value="Pre Schedule"></td>
  </tr>
  <tr>
    <td valign="middle" nowrap="nowrap">Home: <strong><%=myEO2.getNIM3_CaseAccount().getPatientHomePhone()%></strong>&nbsp;&nbsp;&nbsp;Mobile: <strong><%=myEO2.getNIM3_CaseAccount().getPatientCellPhone()%></strong></td>
  </tr>
  <tr>
    <td valign="middle" nowrap="nowrap">Height: <strong><%=myEO2.getNIM3_CaseAccount().getPatientHeight()%></strong>&nbsp;&nbsp;&nbsp;Weight: <strong><%=myEO2.getNIM3_CaseAccount().getPatientWeight()%> lbs</strong></td>
  </tr>
  <tr>
    <td valign="middle" nowrap="nowrap" class="tdHeaderAlt">Referring Dr.</td>
  </tr>
  <tr>
    <td valign="middle" nowrap="nowrap"><strong><%=myEO2.getReferral_ReferringDoctor().getContactLastName()%></strong>, <strong><%=myEO2.getReferral_ReferringDoctor().getContactFirstName()%></strong></td>
  </tr>
  <tr>
    <td valign="middle" nowrap="nowrap">Phone: <strong><%=myEO2.getReferral_ReferringDoctor().getContactPhone()%></strong>&nbsp;&nbsp;&nbsp;Fax: <strong><%=myEO2.getReferral_ReferringDoctor().getContactFax()%></strong></td>
  </tr>
  <tr>
    <td valign="middle" nowrap="nowrap"><strong><%=myEO2.getReferral_ReferringDoctor().getCompanyName()%></strong> - <strong><%=myEO2.getReferral_ReferringDoctor().getContactCity()%></strong></td>
  </tr>
</table>

</td>
</tr>
<tr>
<td colspan="2">


<table width="100%" border="0" cellspacing="0" cellpadding="1">
  <tr>
    <td><p class="tdHeader">CommTrack:<br>
			<input type="button" class="inputButton_sm_Create"  onclick="this.disabled=true;modalPost('CASEID', modalWin('LI_ModalPass.jsp?ssPF=tNIM3_Encounter_main_NIM3_Encounter_ReferralID_form_authorize.jsp&QUICKVIEW=yes&EDIT=addct&EDITID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&KM=p','Add CT...','dialogWidth:1000px;dialogHeight:1000px','status=yes,scrollbars=yes,resizable=yes,width=900,height=800'));document.location=document.location;" value="Add">&nbsp;<input name="showCT" type="button" class="inputButton_sm_Default" onclick="document.getElementById('comTrack_<%=myEO2.getNIM3_Encounter().getEncounterID()%>').style.display = 'block';" value="Show" />&nbsp;<input name="showCT" type="button" class="inputButton_sm_Default" onclick="document.getElementById('comTrack_<%=myEO2.getNIM3_Encounter().getEncounterID()%>').style.display = 'none';" value="Hide" /> </p></td>
  </tr>
  <tr style="display:none" id="comTrack_<%=myEO2.getNIM3_Encounter().getEncounterID()%>">
    <td>
    
		    <%
	searchDB2 mySS_ct = new searchDB2();
	try
	{
		java.sql.ResultSet myRS_ct = null;
		String mySQL_ct = ("SELECT commtrackid from tnim3_commtrack where (referralid = " + myEO2.getNIM3_Referral().getReferralID() + " and encounterid=0) OR (encounterid=" + myEO2.getNIM3_Encounter().getEncounterID() +") order by CommStart desc");
		//out.println(mySQL);
  
		myRS_ct = mySS_ct.executeStatement(mySQL_ct);
		int i2cnt_ct=0;
		while(myRS_ct!=null&&myRS_ct.next())
		{
			i2cnt_ct++;
			bltNIM3_CommTrack tempCT = new bltNIM3_CommTrack (new  Integer(myRS_ct.getString("commtrackid")));
			%>

		<table width="100%"  border="0" cellspacing="0" cellpadding="3" class="certborderPrintInt">
  <tr class=tdBaseAlt4Blue>
    <td class=tdBaseAlt4Blue>Type:</td>
    <td class=tdBaseAlt4Blue><%if (tempCT.getCommTypeID()==3){%>Notification<%}else if (tempCT.getCommTypeID()==4){%>See NetDev Request<%}if (tempCT.getCommTypeID()==2){%>Referral/MSG<%}if (tempCT.getCommTypeID()==1){%>MSG<%}if (tempCT.getCommTypeID()==0){%>Note<%}%></td>
    <td>&nbsp;</td>
  </tr>
  <tr class=tdBaseAlt>
    <td>From:</td>
		<td width="75%" nowrap><strong><%=tempCT.getMessageName()%></strong> @ <strong><%=tempCT.getMessageCompany()%></strong></td>
		<td width="25%" align="right"><%if (tempCT.getEncounterID().intValue()==myEO2.getNIM3_Encounter().getEncounterID().intValue())
		{
			out.println("E");
		}else
		{
			out.println("R");
		}%>&nbsp;</td>
  </tr>
  <%
	String myClass_CT_Duration = "";
	int temp_hoursSince = Math.abs(PLCUtils.getHoursTill(tempCT.getCommStart()));
	if (tempCT.getCommEnd().after(NIMUtils.getBeforeTime()))
	{
			myClass_CT_Duration = "tdBaseAlt2";
	}
	else
	{
		if (temp_hoursSince>48)
		{
			myClass_CT_Duration = "tdBaseAlt_Action3";
		}
		else if (temp_hoursSince>24 || myEO2.isEscalatedSTAT())
		{
			myClass_CT_Duration = "tdBaseAlt_Action2";
		}
		else if (temp_hoursSince>12 )
		{
			myClass_CT_Duration = "tdBaseAlt_Action1";
		}
		else
		{
			myClass_CT_Duration = "tdBaseAlt2";
		}
	}
  
  
  %>
  <tr class="<%=myClass_CT_Duration%>" >
    <td>Time:</td>
    <td>
<%				if (tempCT.getCommEnd().after(NIMUtils.getBeforeTime()))
				{
					out.print(displayDateTimeSDF.format(tempCT.getCommStart()) + "&nbsp;&nbsp;&nbsp;(Dur: <strong>" + Math.round((tempCT.getCommEnd().getTime()-tempCT.getCommStart().getTime())/1000/60) + "</strong> min)");
				}
				else 
				{
					out.print("Start: " + displayDateTimeSDF.format(tempCT.getCommStart()) + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Item Still Open&nbsp;&nbsp;&nbsp;Started: <strong>" + PLCUtils.getHoursTill_Display(tempCT.getCommStart(), false)+ "  (" + PLCUtils.getDaysTill_Display(tempCT.getCommStart(), true) + ")</strong>");
				}
%>				
</td>
    <td>Status:<jsp:include page="../generic/tCommTrackAlertStatusCodeLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=tempCT.getAlertStatusCode()%>" /><jsp:param name="UseColor" value="y" /></jsp:include>
     </td>
  </tr>
  <%
  if (tempCT.getCommTypeID()==4)
  {
  %>
  <tr bgcolor="#CCCCCC">
    <td colspan="3">Response:<br><%=tempCT.getComments().replaceAll("(\r\n|\r|\n|\n\r)", "<br>")%></td>
  </tr>
  <%
  }
  %>
  <tr bgcolor="#FFFFFF">
    <td colspan="3"><%=tempCT.getMessageText().replaceAll("(\r\n|\r|\n|\n\r)", "<br>")%></td>
  </tr>
  <tr bgcolor="#DDDDDD">
    <td>CommRef:</td>
    <td colspan="2"><%
		searchDB2 mySS_cr = new searchDB2();
		try {
			java.sql.ResultSet myRS_cr = null;
	//		String mySQL_ct = ("SELECT commtrackid from tnim3_commtrack where (referralid = " + working_bltNIM3_Referral.getReferralID() + " and encounterid=0) OR (referralid = " + working_bltNIM3_Referral.getReferralID() + " and encounterid=" + myEO2.getNIM3_Encounter().getEncounterID() +") order by CommEnd desc");
			String mySQL_cr = ("SELECT commreferenceid from tnim3_commreference where commreferenceid = " + tempCT.getCommReferenceID());
			//out.println(mySQL);
			myRS_cr = mySS_cr.executeStatement(mySQL_cr);
			int i2cnt_cr=0;
			if(myRS_cr!=null)
			{
				while(myRS_cr!=null&&myRS_cr.next())
				{
					i2cnt_cr++;
					bltNIM3_CommReference myCR = new bltNIM3_CommReference (new  Integer(myRS_cr.getString("commreferenceid")));
					if (myCR.getCRTypeID()==1)
					{
					%>	
					<a href="#">View Notification</a>
					<%
					}
					else
					{
					%>	
					Other
					<%
					}
				}
			}
			else
			{
					%>	
					None
					<%
			}
			mySS_cr.closeAll();
		} catch (Exception mySS_cr_Error) {
            DebugLogger.println("JSP:SeeND_Edit.jsp:mySS_cr.executeStatement(mySQL_cr): [Error]" + mySS_cr_Error.toString());
		} finally {
			mySS_cr.closeAll();
		}

%></td>
  </tr>
</table>
			<%
		}
		mySS_ct.closeAll();
	}
	catch(Exception e)
	{
			out.println("ResultsSet:"+e);
	}
	finally 
	{
		mySS_ct.closeAll();
	}
%>

    
    
    </td>
  </tr>
</table>





</td></tr>
</table></td></tr>
<table>

<%
	}
  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
%>
    
    