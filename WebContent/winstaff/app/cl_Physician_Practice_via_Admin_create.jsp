<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.bltDocumentManagement,java.util.Vector,java.util.Enumeration,com.winstaff.ListElement, com.winstaff.PLCUtils, com.winstaff.SecurityCheck, com.winstaff.ConfigurationMessages,com.winstaff.bltAdminPhysicianLU_List_PhysicianMaster_AdminID,com.winstaff.bltPracticeMaster,com.winstaff.bltPhysicianMaster,com.winstaff.bltAdminMaster,com.winstaff.bltPhysicianPracticeLU,com.winstaff.bltAdminPhysicianLU" %>
<%/*
    filename: out\jsp\cl_Physician_Practice_via_Admin.jsp
    Created on Mar/21/2003
    Type: n-n main class file
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>


<%
//initial declaration of list class and parentID
    Integer        iPracticeID        =    null;
    Integer        iAdminID        =    null;
    boolean accessValid = false;
   Integer iSecurityCheck = SecurityCheck.CheckItem("Practice1", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {

    if ((pageControllerHash.containsKey("iPracticeID"))&&(pageControllerHash.containsKey("iAdminID"))) 
    {
        iPracticeID        =    (Integer)pageControllerHash.get("iPracticeID");
        iAdminID        =    (Integer)pageControllerHash.get("iAdminID");
        if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {
            accessValid = true;
        }
    }
  //page security
  if (accessValid)
  {
        Integer requestID = null;
        if (request.getParameter("EDITID")!=null)
        {
            requestID = new Integer(request.getParameter("EDITID"));
            out.println(requestID);
        }

//declaration of Enumeration
    bltAdminPhysicianLU_List_PhysicianMaster_AdminID         bltAdminPhysicianLU_List_PhysicianMaster_AdminID        =    new    bltAdminPhysicianLU_List_PhysicianMaster_AdminID(iAdminID,"PhysicianID = "+requestID+" ","");
    bltPhysicianMaster        working_bltPhysicianMaster;
    ListElement         leCurrentElement;
    java.util.Enumeration eList = bltAdminPhysicianLU_List_PhysicianMaster_AdminID.elements();
    %>
    <table class=tableBase cellpadding=0 cellspacing=0 border=0>
    <%
     if (eList.hasMoreElements())
     {
        leCurrentElement    = (ListElement) eList.nextElement();
        working_bltPhysicianMaster  = (bltPhysicianMaster) leCurrentElement.getObject();
        bltPhysicianPracticeLU  working_bltPhysicianPracticeLU = new bltPhysicianPracticeLU();
        working_bltPhysicianPracticeLU.setPhysicianID(requestID);
        working_bltPhysicianPracticeLU.setPracticeID(iPracticeID);
        working_bltPhysicianPracticeLU.commitData();


        String targetRedirect = "cl_Physician_Practice_via_Admin.jsp"    ;

        if (pageControllerHash.containsKey("sParentReturnPage"))
        {
            targetRedirect = (String)pageControllerHash.get("sParentReturnPage");
        }
        response.sendRedirect(targetRedirect);
    }

  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>


