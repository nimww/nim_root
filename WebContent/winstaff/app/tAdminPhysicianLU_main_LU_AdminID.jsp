<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.bltDocumentManagement,java.util.Vector,java.util.Enumeration,com.winstaff.ListElement, com.winstaff.PLCUtils, com.winstaff.SecurityCheck, com.winstaff.ConfigurationMessages,com.winstaff.bltAdminPhysicianLU,com.winstaff.bltAdminPhysicianLU_List_LU_AdminID" %>
<%/*
    filename: tAdminPhysicianLU_main_LU_AdminID.jsp
    Created on Mar/21/2003
    Type: 1-n main class file
    Created by: Scott Ellis
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>

<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_AdminID.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>

    <table cellpadding=0 cellspacing=0 border=0 width=<%=MasterTableWidth%> >
    <tr><td width=10>&nbsp;</td><td>
    <%=ConfigurationMessages.getHTML("INTERVIEWTopControl","tAdminPhysicianLU")%>



<%
//initial declaration of list class and parentID
    Integer        iAdminID        =    null;
    boolean accessValid = false;
   Integer iSecurityCheck = SecurityCheck.CheckItem("Admin1", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {
    if (pageControllerHash.containsKey("iAdminID")) 
    {
        iAdminID        =    (Integer)pageControllerHash.get("iAdminID");
        accessValid = true;
    }
  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);
      pageControllerHash.put("sParentReturnPage","tAdminPhysicianLU_main_LU_AdminID.jsp");
    pageControllerHash.remove("iLookupID");
    pageControllerHash.put("sINTNext","tAdminPhysicianLU_main_LU_AdminID_form_create.jsp?EDIT=new&KM=p&INTNext=yes");
    session.setAttribute("pageControllerHash",pageControllerHash);

    bltAdminPhysicianLU_List_LU_AdminID        bltAdminPhysicianLU_List_LU_AdminID        =    new    bltAdminPhysicianLU_List_LU_AdminID(iAdminID);

//declaration of Enumeration
    bltAdminPhysicianLU        working_bltAdminPhysicianLU;
    ListElement         leCurrentElement;
    java.util.Enumeration eList = bltAdminPhysicianLU_List_LU_AdminID.elements();
    %>
        <%@ include file="tAdminPhysicianLU_main_LU_AdminID_instructions.jsp" %>

        <%if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {%>
        <a class=linkBase href = "tAdminPhysicianLU_main_LU_AdminID_form_create.jsp?EDIT=new&KM=p&INTNext=yes"><img border=0 src="ui_<%=thePLCID%>/icons/create_AdminID.gif"></a>
        <%}%>
         <table border="1" bordercolor="CCCCCC" cellpadding="3" class=tdBase cellspacing="0" width="100%">
    <%
    int altCnt = 0;
    if (eList.hasMoreElements())
    {
     while (eList.hasMoreElements())
     {

        altCnt++;
        String theClass = "tdBase";
        if (altCnt%2!=0)
        {
            theClass = "tdBaseAlt";
        }
        leCurrentElement    = (ListElement) eList.nextElement();
        working_bltAdminPhysicianLU  = (bltAdminPhysicianLU) leCurrentElement.getObject();
        working_bltAdminPhysicianLU.GroupSecurityInit(UserSecurityGroupID);
        if (!working_bltAdminPhysicianLU.isComplete())
        {
            theClass = "incompleteItem";
        %>
                <tr class=incompleteItem><td><b>Not Complete</b><br>
        <%
        }
        else
        {
        %>
        <tr class=<%=theClass%> ><td> 
        <%
        }
        %>

              <b>Item ID:&nbsp;</b><%=working_bltAdminPhysicianLU.getLookupID()%></td>
<%String theClassF = "textBase";%>

<%theClassF = "textBase";%>
<%if ((working_bltAdminPhysicianLU.isExpired("PhysicianID",expiredDays))&&(working_bltAdminPhysicianLU.isExpiredCheck("PhysicianID"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltAdminPhysicianLU.isRequired("PhysicianID"))&&(!working_bltAdminPhysicianLU.isComplete("PhysicianID")) ){theClassF = "requiredFieldMain";}%>
            <td><p class=<%=theClassF%> ><b>PhysicianID:&nbsp;</b><%=working_bltAdminPhysicianLU.getPhysicianID()%></p></td>

<%theClassF = "textBase";%>
<%if ((working_bltAdminPhysicianLU.isExpired("Comments",expiredDays))&&(working_bltAdminPhysicianLU.isExpiredCheck("Comments"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltAdminPhysicianLU.isRequired("Comments"))&&(!working_bltAdminPhysicianLU.isComplete("Comments")) ){theClassF = "requiredFieldMain";}%>
            <td><p class=<%=theClassF%> ><b>Extra Comments:&nbsp;</b><%=working_bltAdminPhysicianLU.getComments()%></p></td>

            <td > 
        <a class=linkBase href = "tAdminPhysicianLU_main_LU_AdminID_form_authorize.jsp?EDIT=edit&EDITID=<%=working_bltAdminPhysicianLU.getLookupID()%>&KM=p"><img border=0 src="ui_<%=thePLCID%>/icons/edit_AdminID.gif"></a>

        <%if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {%>
        <a class=linkBase  onClick="return confirmDelete()"  href = "tAdminPhysicianLU_main_LU_AdminID_form_authorize.jsp?EDIT=del&EDITID=<%=working_bltAdminPhysicianLU.getLookupID()%>&KM=p"><img border=0 src="ui_<%=thePLCID%>/icons/delete_AdminID.gif"></a>
        <% }%>
                  </td></tr>
        <%
    }//end while
       }//end of if
       else 
       {
           %>
           <tr><td><b>Please click the "create" to add <%=ConfigurationMessages.getDataCategory("tAdminPhysicianLU")%> information or click 'Continue' to go to the next section.</b>
           <script language=javascript>
           if (confirm("<%=ConfigurationMessages.getInterviewMessage("INTERVIEWNoElements","tAdminPhysicianLU")%>"))
           {
               document.location="tAdminPhysicianLU_main_LU_AdminID_form_create.jsp?EDIT=new&KM=p&INTNext=yes"; 
           }
           else
           {

           }
           </script>
           </td></tr>
           <%
       }
    %>

    </table>
    <%
  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>

    </td></tr></table><br>


<%String bnIncludeFN = "ui_"+thePLCID+"\\bot-nav_AdminID.jsp";
%>
<jsp:include page="<%=bnIncludeFN%>" flush="true" ><jsp:param name="plcID" value="<%=thePLCID%>"/></jsp:include>
