<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.bltDocumentManagement,java.util.Vector,java.util.Enumeration,com.winstaff.ListElement, com.winstaff.PLCUtils, com.winstaff.SecurityCheck, com.winstaff.ConfigurationMessages,com.winstaff.bltPracticeMaster,com.winstaff.bltAdminPracticeLU,com.winstaff.bltAdminPracticeLU_List_LU_AdminID" %>
<%/*
    filename: out\jsp\tAdminPracticeLU_main_PracticeMaster_AdminID_form_authorize.jsp
    Created on Feb/06/2003
    Type: n-n main class file
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>


<%
//initial declaration of list class and parentID
    Integer        iAdminID        =    null;
    boolean accessValid = false;

   Integer iSecurityCheck = SecurityCheck.CheckItem("Practice1", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {

    if (pageControllerHash.containsKey("iAdminID")) 
    {
        iAdminID        =    (Integer)pageControllerHash.get("iAdminID");
        accessValid = true;
    }
  //page security
  if (accessValid)
  {
        Integer requestID = null;
        if (request.getParameter("EDITID")!=null)
        {
            requestID = new Integer(request.getParameter("EDITID"));
            out.println(requestID);
        }
    bltAdminPracticeLU_List_LU_AdminID        bltAdminPracticeLU_List_LU_AdminID        =    new    bltAdminPracticeLU_List_LU_AdminID(iAdminID,"PracticeID="+requestID,"");

//declaration of Enumeration
    bltAdminPracticeLU        working_bltAdminPracticeLU;
    ListElement         leCurrentElement;
    java.util.Enumeration eList = bltAdminPracticeLU_List_LU_AdminID.elements();
    %>
    <%
    if (eList.hasMoreElements())
    {
        leCurrentElement    = (ListElement) eList.nextElement();
        working_bltAdminPracticeLU  = (bltAdminPracticeLU) leCurrentElement.getObject();
        pageControllerHash.put("iPracticeID",working_bltAdminPracticeLU.getPracticeID());
        session.setAttribute("pageControllerHash",pageControllerHash);
        //Parameter Pass Code here
String parameterPassString ="";
java.util.Enumeration myParameterPassList = request.getParameterNames();
while (myParameterPassList.hasMoreElements())
{
	String myName = (String)myParameterPassList.nextElement();
	String myS = (String) request.getParameter(myName);
	parameterPassString+="&"+myName + "=" + myS;
}
        response.sendRedirect("practice_Status.jsp");
    }
    else
    {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORInvalidQuery")+"</p>");
    }

  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>


