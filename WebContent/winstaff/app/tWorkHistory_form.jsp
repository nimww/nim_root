<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.bltDocumentManagement,java.util.Vector,java.util.Enumeration,com.winstaff.ListElement, com.winstaff.PLCUtils, com.winstaff.SecurityCheck, com.winstaff.ConfigurationMessages,com.winstaff.bltWorkHistory" %>
<%/*

    filename: out\jsp\tWorkHistory_form.jsp
    Created on Mar/21/2003
    Created by: Scott Ellis
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>

<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_PhysicianID.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>


    <table cellpadding=0 cellspacing=0 border=0 width=<%=MasterTableWidth%> >
    <tr><td width=10>&nbsp;</td><td>
    <%=ConfigurationMessages.getHTML("INTERVIEWTopControl_form","tWorkHistory")%>



<%
//initial declaration of list class and parentID
    Integer        iWorkHistoryID        =    null;
    boolean accessValid = false;
    // required for Type2
    String sKeyMasterReference = null;

   Integer iSecurityCheck = SecurityCheck.CheckItem("PractitionerSection8", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {
    if (pageControllerHash.containsKey("iWorkHistoryID")) 
    {
        iWorkHistoryID        =    (Integer)pageControllerHash.get("iWorkHistoryID");
        accessValid = true;    }
  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);      java.text.SimpleDateFormat displayDateTimeSDF = new java.text.SimpleDateFormat("MM/dd/yyyy hh:mm a");

    pageControllerHash.put("sLocalChildReturnPage","tWorkHistory_form.jsp");
//initial declaration of list class and parentID

    bltWorkHistory        WorkHistory        =    null;

    if ( request.getParameter( "EDIT" ).equalsIgnoreCase("edit") )
    {
        WorkHistory        =    new    bltWorkHistory(iWorkHistoryID,UserSecurityGroupID);
    }
    else if ( request.getParameter( "EDIT" ).equalsIgnoreCase("new") )
    {
        WorkHistory        =    new    bltWorkHistory(UserSecurityGroupID, true);
    }

//fields
        %>
        <%@ include file="tWorkHistory_form_instructions.jsp" %>
        <form action="tWorkHistory_form_sub.jsp" name="tWorkHistory_form1" method="POST">
<%
    if ( request.getParameter( "EDIT" ) != null )
    {
    %>
        <input type="hidden" name="EDIT" value = "<%=request.getParameter( "EDIT" )%>" >  
    <%
    }
%>
          <%  String theClass ="tdBase";%>
        <table width=100% border=1 bordercolor=#333333 cellpadding=3 cellspacing=0 class=tableBase>
         <tr><td class=tableColor>


            <table cellpadding=0 cellspacing=0 width=100%>
            <%
            if ( (WorkHistory.isRequired("WorkHistoryTypeID",UserSecurityGroupID))&&(!WorkHistory.isComplete("WorkHistoryTypeID")) )
            {
                theClass = "requiredField";
            }
            else if ((WorkHistory.isExpired("WorkHistoryTypeID",expiredDays))&&(WorkHistory.isExpiredCheck("WorkHistoryTypeID",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((WorkHistory.isWrite("WorkHistoryTypeID",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Work Type&nbsp;</b></p></td><td valign=top><p><select   name="WorkHistoryTypeID" ><jsp:include page="../generic/tWorkHistoryTypeLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=WorkHistory.getWorkHistoryTypeID()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=WorkHistoryTypeID&amp;sTableName=tWorkHistory&amp;sRefID=<%=WorkHistory.getWorkHistoryID()%>&amp;sFieldNameDisp=<%=WorkHistory.getEnglish("WorkHistoryTypeID")%>&amp;sTableNameDisp=tWorkHistory','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((WorkHistory.isRead("WorkHistoryTypeID",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Work Type&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tWorkHistoryTypeLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=WorkHistory.getWorkHistoryTypeID()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=WorkHistoryTypeID&amp;sTableName=tWorkHistory&amp;sRefID=<%=WorkHistory.getWorkHistoryID()%>&amp;sFieldNameDisp=<%=WorkHistory.getEnglish("WorkHistoryTypeID")%>&amp;sTableNameDisp=tWorkHistory','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (WorkHistory.isRequired("Name",UserSecurityGroupID))&&(!WorkHistory.isComplete("Name")) )
            {
                theClass = "requiredField";
            }
            else if ((WorkHistory.isExpired("Name",expiredDays))&&(WorkHistory.isExpiredCheck("Name",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((WorkHistory.isWrite("Name",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Company&nbsp;</b></p></td><td valign=top><p><input maxlength="100" type=text size="80" name="Name" value="<%=WorkHistory.getName()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Name&amp;sTableName=tWorkHistory&amp;sRefID=<%=WorkHistory.getWorkHistoryID()%>&amp;sFieldNameDisp=<%=WorkHistory.getEnglish("Name")%>&amp;sTableNameDisp=tWorkHistory','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((WorkHistory.isRead("Name",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Company&nbsp;</b></p></td><td valign=top><p><%=WorkHistory.getName()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Name&amp;sTableName=tWorkHistory&amp;sRefID=<%=WorkHistory.getWorkHistoryID()%>&amp;sFieldNameDisp=<%=WorkHistory.getEnglish("Name")%>&amp;sTableNameDisp=tWorkHistory','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (WorkHistory.isRequired("WorkDescription",UserSecurityGroupID))&&(!WorkHistory.isComplete("WorkDescription")) )
            {
                theClass = "requiredField";
            }
            else if ((WorkHistory.isExpired("WorkDescription",expiredDays))&&(WorkHistory.isExpiredCheck("WorkDescription",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((WorkHistory.isWrite("WorkDescription",UserSecurityGroupID)))
            {
                        %>
                       <tr><td valign=top><p class=<%=theClass%> ><b><%=WorkHistory.getEnglish("WorkDescription")%>&nbsp;</b></p></td><td valign=top><p><textarea onKeyDown="textAreaStop(this,200)" rows="2" name="WorkDescription" cols="40" maxlength=200><%=WorkHistory.getWorkDescription()%></textarea>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=WorkDescription&amp;sTableName=tWorkHistory&amp;sRefID=<%=WorkHistory.getWorkHistoryID()%>&amp;sFieldNameDisp=<%=WorkHistory.getEnglish("WorkDescription")%>&amp;sTableNameDisp=tWorkHistory','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((WorkHistory.isRead("WorkDescription",UserSecurityGroupID)))
            {
                        %>
                       <tr><td valign=top> <p class=<%=theClass%> ><b><%=WorkHistory.getEnglish("WorkDescription")%>&nbsp;</b></p></td><td valign=top><p><%=WorkHistory.getWorkDescription()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=WorkDescription&amp;sTableName=tWorkHistory&amp;sRefID=<%=WorkHistory.getWorkHistoryID()%>&amp;sFieldNameDisp=<%=WorkHistory.getEnglish("WorkDescription")%>&amp;sTableNameDisp=tWorkHistory','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (WorkHistory.isRequired("FromDate",UserSecurityGroupID))&&(!WorkHistory.isComplete("FromDate")) )
            {
                theClass = "requiredField";
            }
            else if ((WorkHistory.isExpired("FromDate",expiredDays))&&(WorkHistory.isExpiredCheck("FromDate",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>

            <%
            if ((WorkHistory.isWrite("FromDate",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Start Date&nbsp;(mm/dd/yyyy):&nbsp;</b></p></td><td valign=top><p><input maxlength=20  type=text size="80" name="FromDate" value='<jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(WorkHistory.getFromDate())%>" /></jsp:include>' >&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=FromDate&amp;sTableName=tWorkHistory&amp;sRefID=<%=WorkHistory.getWorkHistoryID()%>&amp;sFieldNameDisp=<%=WorkHistory.getEnglish("FromDate")%>&amp;sTableNameDisp=tWorkHistory','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((WorkHistory.isRead("FromDate",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top>
                    <p class=<%=theClass%> ><b>Start Date&nbsp;(mm/DD/yyyy):&nbsp;</b></p>
                  </td><td valign=top><p><jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(WorkHistory.getFromDate())%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=FromDate&amp;sTableName=tWorkHistory&amp;sRefID=<%=WorkHistory.getWorkHistoryID()%>&amp;sFieldNameDisp=<%=WorkHistory.getEnglish("FromDate")%>&amp;sTableNameDisp=tWorkHistory','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (WorkHistory.isRequired("ToDate",UserSecurityGroupID))&&(!WorkHistory.isComplete("ToDate")) )
            {
                theClass = "requiredField";
            }
            else if ((WorkHistory.isExpired("ToDate",expiredDays))&&(WorkHistory.isExpiredCheck("ToDate",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>

            <%
            if ((WorkHistory.isWrite("ToDate",UserSecurityGroupID)))
            {
                        %>
<tr><td colspan=2>&nbsp;</td></tr>
<tr>
                  <td colspan=2 class=instructions>If you no longer work here, please enter the date you stopped work.  If you are still 
                    currently working here, please enter "Current" otherwise, enter &quot;n/a&quot;</td>
                </tr>
                        <tr><td valign=top>
                    <p class=<%=theClass%> ><b>End Date&nbsp;(mm/DD/yyyy):&nbsp;</b></p>
                  </td><td valign=top><p><input maxlength=20  type=text size="80" name="ToDate" value='<jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(WorkHistory.getToDate())%>" /></jsp:include>' >&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ToDate&amp;sTableName=tWorkHistory&amp;sRefID=<%=WorkHistory.getWorkHistoryID()%>&amp;sFieldNameDisp=<%=WorkHistory.getEnglish("ToDate")%>&amp;sTableNameDisp=tWorkHistory','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((WorkHistory.isRead("ToDate",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top>
                    <p class=<%=theClass%> ><b>End Date&nbsp;(mm/DD/yyyy):&nbsp;</b></p>
                  </td><td valign=top><p><jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(WorkHistory.getToDate())%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ToDate&amp;sTableName=tWorkHistory&amp;sRefID=<%=WorkHistory.getWorkHistoryID()%>&amp;sFieldNameDisp=<%=WorkHistory.getEnglish("ToDate")%>&amp;sTableNameDisp=tWorkHistory','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (WorkHistory.isRequired("ReasonForLeaving",UserSecurityGroupID))&&(!WorkHistory.isComplete("ReasonForLeaving")) )
            {
                theClass = "requiredField";
            }
            else if ((WorkHistory.isExpired("ReasonForLeaving",expiredDays))&&(WorkHistory.isExpiredCheck("ReasonForLeaving",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((WorkHistory.isWrite("ReasonForLeaving",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Reason for leaving&nbsp;</b></p></td><td valign=top><p><input maxlength="50" type=text size="80" name="ReasonForLeaving" value="<%=WorkHistory.getReasonForLeaving()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ReasonForLeaving&amp;sTableName=tWorkHistory&amp;sRefID=<%=WorkHistory.getWorkHistoryID()%>&amp;sFieldNameDisp=<%=WorkHistory.getEnglish("ReasonForLeaving")%>&amp;sTableNameDisp=tWorkHistory','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((WorkHistory.isRead("ReasonForLeaving",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Reason for leaving&nbsp;</b></p></td><td valign=top><p><%=WorkHistory.getReasonForLeaving()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ReasonForLeaving&amp;sTableName=tWorkHistory&amp;sRefID=<%=WorkHistory.getWorkHistoryID()%>&amp;sFieldNameDisp=<%=WorkHistory.getEnglish("ReasonForLeaving")%>&amp;sTableNameDisp=tWorkHistory','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (WorkHistory.isRequired("Address1",UserSecurityGroupID))&&(!WorkHistory.isComplete("Address1")) )
            {
                theClass = "requiredField";
            }
            else if ((WorkHistory.isExpired("Address1",expiredDays))&&(WorkHistory.isExpiredCheck("Address1",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((WorkHistory.isWrite("Address1",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Company Address&nbsp;</b></p></td><td valign=top><p><input maxlength="50" type=text size="80" name="Address1" value="<%=WorkHistory.getAddress1()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Address1&amp;sTableName=tWorkHistory&amp;sRefID=<%=WorkHistory.getWorkHistoryID()%>&amp;sFieldNameDisp=<%=WorkHistory.getEnglish("Address1")%>&amp;sTableNameDisp=tWorkHistory','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((WorkHistory.isRead("Address1",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Company Address&nbsp;</b></p></td><td valign=top><p><%=WorkHistory.getAddress1()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Address1&amp;sTableName=tWorkHistory&amp;sRefID=<%=WorkHistory.getWorkHistoryID()%>&amp;sFieldNameDisp=<%=WorkHistory.getEnglish("Address1")%>&amp;sTableNameDisp=tWorkHistory','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (WorkHistory.isRequired("Address2",UserSecurityGroupID))&&(!WorkHistory.isComplete("Address2")) )
            {
                theClass = "requiredField";
            }
            else if ((WorkHistory.isExpired("Address2",expiredDays))&&(WorkHistory.isExpiredCheck("Address2",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((WorkHistory.isWrite("Address2",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Address 2&nbsp;</b></p></td><td valign=top><p><input maxlength="20" type=text size="80" name="Address2" value="<%=WorkHistory.getAddress2()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Address2&amp;sTableName=tWorkHistory&amp;sRefID=<%=WorkHistory.getWorkHistoryID()%>&amp;sFieldNameDisp=<%=WorkHistory.getEnglish("Address2")%>&amp;sTableNameDisp=tWorkHistory','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((WorkHistory.isRead("Address2",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Address 2&nbsp;</b></p></td><td valign=top><p><%=WorkHistory.getAddress2()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Address2&amp;sTableName=tWorkHistory&amp;sRefID=<%=WorkHistory.getWorkHistoryID()%>&amp;sFieldNameDisp=<%=WorkHistory.getEnglish("Address2")%>&amp;sTableNameDisp=tWorkHistory','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (WorkHistory.isRequired("City",UserSecurityGroupID))&&(!WorkHistory.isComplete("City")) )
            {
                theClass = "requiredField";
            }
            else if ((WorkHistory.isExpired("City",expiredDays))&&(WorkHistory.isExpiredCheck("City",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((WorkHistory.isWrite("City",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>City, Town, Province&nbsp;</b></p></td><td valign=top><p><input maxlength="30" type=text size="80" name="City" value="<%=WorkHistory.getCity()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=City&amp;sTableName=tWorkHistory&amp;sRefID=<%=WorkHistory.getWorkHistoryID()%>&amp;sFieldNameDisp=<%=WorkHistory.getEnglish("City")%>&amp;sTableNameDisp=tWorkHistory','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((WorkHistory.isRead("City",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>City, Town, Province&nbsp;</b></p></td><td valign=top><p><%=WorkHistory.getCity()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=City&amp;sTableName=tWorkHistory&amp;sRefID=<%=WorkHistory.getWorkHistoryID()%>&amp;sFieldNameDisp=<%=WorkHistory.getEnglish("City")%>&amp;sTableNameDisp=tWorkHistory','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (WorkHistory.isRequired("StateID",UserSecurityGroupID))&&(!WorkHistory.isComplete("StateID")) )
            {
                theClass = "requiredField";
            }
            else if ((WorkHistory.isExpired("StateID",expiredDays))&&(WorkHistory.isExpiredCheck("StateID",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((WorkHistory.isWrite("StateID",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>State&nbsp;</b></p></td><td valign=top><p><select   name="StateID" ><jsp:include page="../generic/tStateLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=WorkHistory.getStateID()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=StateID&amp;sTableName=tWorkHistory&amp;sRefID=<%=WorkHistory.getWorkHistoryID()%>&amp;sFieldNameDisp=<%=WorkHistory.getEnglish("StateID")%>&amp;sTableNameDisp=tWorkHistory','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((WorkHistory.isRead("StateID",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>State&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tStateLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=WorkHistory.getStateID()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=StateID&amp;sTableName=tWorkHistory&amp;sRefID=<%=WorkHistory.getWorkHistoryID()%>&amp;sFieldNameDisp=<%=WorkHistory.getEnglish("StateID")%>&amp;sTableNameDisp=tWorkHistory','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (WorkHistory.isRequired("Province",UserSecurityGroupID))&&(!WorkHistory.isComplete("Province")) )
            {
                theClass = "requiredField";
            }
            else if ((WorkHistory.isExpired("Province",expiredDays))&&(WorkHistory.isExpiredCheck("Province",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((WorkHistory.isWrite("Province",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Province, District, State&nbsp;</b></p></td><td valign=top><p><input maxlength="100" type=text size="80" name="Province" value="<%=WorkHistory.getProvince()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Province&amp;sTableName=tWorkHistory&amp;sRefID=<%=WorkHistory.getWorkHistoryID()%>&amp;sFieldNameDisp=<%=WorkHistory.getEnglish("Province")%>&amp;sTableNameDisp=tWorkHistory','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((WorkHistory.isRead("Province",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Province, District, State&nbsp;</b></p></td><td valign=top><p><%=WorkHistory.getProvince()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Province&amp;sTableName=tWorkHistory&amp;sRefID=<%=WorkHistory.getWorkHistoryID()%>&amp;sFieldNameDisp=<%=WorkHistory.getEnglish("Province")%>&amp;sTableNameDisp=tWorkHistory','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (WorkHistory.isRequired("ZIP",UserSecurityGroupID))&&(!WorkHistory.isComplete("ZIP")) )
            {
                theClass = "requiredField";
            }
            else if ((WorkHistory.isExpired("ZIP",expiredDays))&&(WorkHistory.isExpiredCheck("ZIP",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((WorkHistory.isWrite("ZIP",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>ZIP&nbsp;</b></p></td><td valign=top><p><input maxlength="50" type=text size="80" name="ZIP" value="<%=WorkHistory.getZIP()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ZIP&amp;sTableName=tWorkHistory&amp;sRefID=<%=WorkHistory.getWorkHistoryID()%>&amp;sFieldNameDisp=<%=WorkHistory.getEnglish("ZIP")%>&amp;sTableNameDisp=tWorkHistory','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((WorkHistory.isRead("ZIP",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>ZIP&nbsp;</b></p></td><td valign=top><p><%=WorkHistory.getZIP()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ZIP&amp;sTableName=tWorkHistory&amp;sRefID=<%=WorkHistory.getWorkHistoryID()%>&amp;sFieldNameDisp=<%=WorkHistory.getEnglish("ZIP")%>&amp;sTableNameDisp=tWorkHistory','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (WorkHistory.isRequired("CountryID",UserSecurityGroupID))&&(!WorkHistory.isComplete("CountryID")) )
            {
                theClass = "requiredField";
            }
            else if ((WorkHistory.isExpired("CountryID",expiredDays))&&(WorkHistory.isExpiredCheck("CountryID",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((WorkHistory.isWrite("CountryID",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Country&nbsp;</b></p></td><td valign=top><p><select   name="CountryID" ><jsp:include page="../generic/tCountryLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=WorkHistory.getCountryID()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CountryID&amp;sTableName=tWorkHistory&amp;sRefID=<%=WorkHistory.getWorkHistoryID()%>&amp;sFieldNameDisp=<%=WorkHistory.getEnglish("CountryID")%>&amp;sTableNameDisp=tWorkHistory','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((WorkHistory.isRead("CountryID",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Country&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tCountryLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=WorkHistory.getCountryID()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CountryID&amp;sTableName=tWorkHistory&amp;sRefID=<%=WorkHistory.getWorkHistoryID()%>&amp;sFieldNameDisp=<%=WorkHistory.getEnglish("CountryID")%>&amp;sTableNameDisp=tWorkHistory','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (WorkHistory.isRequired("Phone",UserSecurityGroupID))&&(!WorkHistory.isComplete("Phone")) )
            {
                theClass = "requiredField";
            }
            else if ((WorkHistory.isExpired("Phone",expiredDays))&&(WorkHistory.isExpiredCheck("Phone",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((WorkHistory.isWrite("Phone",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Phone (XXX-XXX-XXXX)&nbsp;</b></p></td><td valign=top><p><input maxlength="50" type=text size="80" name="Phone" value="<%=WorkHistory.getPhone()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Phone&amp;sTableName=tWorkHistory&amp;sRefID=<%=WorkHistory.getWorkHistoryID()%>&amp;sFieldNameDisp=<%=WorkHistory.getEnglish("Phone")%>&amp;sTableNameDisp=tWorkHistory','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((WorkHistory.isRead("Phone",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Phone (XXX-XXX-XXXX)&nbsp;</b></p></td><td valign=top><p><%=WorkHistory.getPhone()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Phone&amp;sTableName=tWorkHistory&amp;sRefID=<%=WorkHistory.getWorkHistoryID()%>&amp;sFieldNameDisp=<%=WorkHistory.getEnglish("Phone")%>&amp;sTableNameDisp=tWorkHistory','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (WorkHistory.isRequired("Fax",UserSecurityGroupID))&&(!WorkHistory.isComplete("Fax")) )
            {
                theClass = "requiredField";
            }
            else if ((WorkHistory.isExpired("Fax",expiredDays))&&(WorkHistory.isExpiredCheck("Fax",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((WorkHistory.isWrite("Fax",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Fax (XXX-XXX-XXXX)&nbsp;</b></p></td><td valign=top><p><input maxlength="50" type=text size="80" name="Fax" value="<%=WorkHistory.getFax()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Fax&amp;sTableName=tWorkHistory&amp;sRefID=<%=WorkHistory.getWorkHistoryID()%>&amp;sFieldNameDisp=<%=WorkHistory.getEnglish("Fax")%>&amp;sTableNameDisp=tWorkHistory','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((WorkHistory.isRead("Fax",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Fax (XXX-XXX-XXXX)&nbsp;</b></p></td><td valign=top><p><%=WorkHistory.getFax()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Fax&amp;sTableName=tWorkHistory&amp;sRefID=<%=WorkHistory.getWorkHistoryID()%>&amp;sFieldNameDisp=<%=WorkHistory.getEnglish("Fax")%>&amp;sTableNameDisp=tWorkHistory','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (WorkHistory.isRequired("ContactName",UserSecurityGroupID))&&(!WorkHistory.isComplete("ContactName")) )
            {
                theClass = "requiredField";
            }
            else if ((WorkHistory.isExpired("ContactName",expiredDays))&&(WorkHistory.isExpiredCheck("ContactName",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((WorkHistory.isWrite("ContactName",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Contact Name&nbsp;</b></p></td><td valign=top><p><input maxlength="90" type=text size="80" name="ContactName" value="<%=WorkHistory.getContactName()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ContactName&amp;sTableName=tWorkHistory&amp;sRefID=<%=WorkHistory.getWorkHistoryID()%>&amp;sFieldNameDisp=<%=WorkHistory.getEnglish("ContactName")%>&amp;sTableNameDisp=tWorkHistory','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((WorkHistory.isRead("ContactName",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Contact Name&nbsp;</b></p></td><td valign=top><p><%=WorkHistory.getContactName()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ContactName&amp;sTableName=tWorkHistory&amp;sRefID=<%=WorkHistory.getWorkHistoryID()%>&amp;sFieldNameDisp=<%=WorkHistory.getEnglish("ContactName")%>&amp;sTableNameDisp=tWorkHistory','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (WorkHistory.isRequired("ContactEmail",UserSecurityGroupID))&&(!WorkHistory.isComplete("ContactEmail")) )
            {
                theClass = "requiredField";
            }
            else if ((WorkHistory.isExpired("ContactEmail",expiredDays))&&(WorkHistory.isExpiredCheck("ContactEmail",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((WorkHistory.isWrite("ContactEmail",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Contact E-mail&nbsp;</b></p></td><td valign=top><p><input maxlength="95" type=text size="80" name="ContactEmail" value="<%=WorkHistory.getContactEmail()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ContactEmail&amp;sTableName=tWorkHistory&amp;sRefID=<%=WorkHistory.getWorkHistoryID()%>&amp;sFieldNameDisp=<%=WorkHistory.getEnglish("ContactEmail")%>&amp;sTableNameDisp=tWorkHistory','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((WorkHistory.isRead("ContactEmail",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Contact E-mail&nbsp;</b></p></td><td valign=top><p><%=WorkHistory.getContactEmail()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ContactEmail&amp;sTableName=tWorkHistory&amp;sRefID=<%=WorkHistory.getWorkHistoryID()%>&amp;sFieldNameDisp=<%=WorkHistory.getEnglish("ContactEmail")%>&amp;sTableNameDisp=tWorkHistory','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (WorkHistory.isRequired("Comments",UserSecurityGroupID))&&(!WorkHistory.isComplete("Comments")) )
            {
                theClass = "requiredField";
            }
            else if ((WorkHistory.isExpired("Comments",expiredDays))&&(WorkHistory.isExpiredCheck("Comments",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((WorkHistory.isWrite("Comments",UserSecurityGroupID)))
            {
                        %>
                       <tr><td valign=top><p class=<%=theClass%> ><b><%=WorkHistory.getEnglish("Comments")%>&nbsp;</b></p></td><td valign=top><p><textarea onKeyDown="textAreaStop(this,200)" rows="2" name="Comments" cols="40" maxlength=200><%=WorkHistory.getComments()%></textarea>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Comments&amp;sTableName=tWorkHistory&amp;sRefID=<%=WorkHistory.getWorkHistoryID()%>&amp;sFieldNameDisp=<%=WorkHistory.getEnglish("Comments")%>&amp;sTableNameDisp=tWorkHistory','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((WorkHistory.isRead("Comments",UserSecurityGroupID)))
            {
                        %>
                       <tr><td valign=top> <p class=<%=theClass%> ><b><%=WorkHistory.getEnglish("Comments")%>&nbsp;</b></p></td><td valign=top><p><%=WorkHistory.getComments()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Comments&amp;sTableName=tWorkHistory&amp;sRefID=<%=WorkHistory.getWorkHistoryID()%>&amp;sFieldNameDisp=<%=WorkHistory.getEnglish("Comments")%>&amp;sTableNameDisp=tWorkHistory','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>




            <tr><td width=40%>&nbsp;</td><td width=60%>&nbsp;</td></tr>
            </table>
        <%if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {%>
            <input type=hidden name=routePageReference value="sParentReturnPage">
             <%
              if (request.getParameter("INTNext")!=null&&request.getParameter("INTNext").equalsIgnoreCase("yes") ) 
              {
              %>
                  <table width=75% border=1 bordercolor=333333 align=left cellspacing=0 cellpadding=0 class=wizardTable>
                  <tr class=requiredField><td>
                  <input  <%=HTMLFormStyleButton%> type="radio" value="next" name="INTNext" checked>&nbsp;&nbsp;<%=ConfigurationMessages.getInterviewMessage("INTERVIEWNoMore","tWorkHistory")%>
                  <br>
                  <input  <%=HTMLFormStyleButton%> type="radio" value="yes" name="INTNext">&nbsp;&nbsp;<%=ConfigurationMessages.getInterviewMessage("INTERVIEWAddMore","tWorkHistory")%>
                  </td></tr></table><br><br><br>
              <%
              }
              %>
            <p><input <%=HTMLFormStyleButton%> type=Submit value="Continue" name=Submit></p>
        <%}%>
        </td></tr></table>
        </form>
        <%
  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>

    </td></tr></table>



<%String bnIncludeFN = "ui_"+thePLCID+"\\bot-nav_PhysicianID.jsp";
%>
<jsp:include page="<%=bnIncludeFN%>" flush="true" ><jsp:param name="plcID" value="<%=thePLCID%>"/></jsp:include>
