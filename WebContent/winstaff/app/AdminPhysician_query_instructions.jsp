<p class=instructions>
Below is either a list of your practitioners or a search box that will allow you to query a specific practitioner.

You can create a new practitioner and include all the credentialing information for that practitioner by clicking 
on the <b>"Add"</b> button.
<br><br>
 </p>