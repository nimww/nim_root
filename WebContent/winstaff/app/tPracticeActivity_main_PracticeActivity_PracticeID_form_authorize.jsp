<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.*" %>
<%/*
    filename: out\jsp\tPracticeActivity_main_PracticeActivity_PracticeID_form_authorize.jsp
    Created on Oct/30/2009
    Type: 1-n main class file
    Created by: Scott Ellis
    Version: 4.0
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>


<%
//initial declaration of list class and parentID
    Integer        iPracticeID        =    null;
    boolean accessValid = false;

   Integer iSecurityCheck = SecurityCheck.CheckItem("nim1", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {

    if (pageControllerHash.containsKey("iPracticeID")) 
    {
        iPracticeID        =    (Integer)pageControllerHash.get("iPracticeID");
		out.print(iPracticeID);
        accessValid = true;
    }
  //page security
  if (accessValid)
  {
        Integer requestID = null;
        if (request.getParameter("EDITID")!=null)
        {
            requestID = new Integer(request.getParameter("EDITID"));
            out.println(requestID);
        }
    bltPracticeActivity_List        bltPracticeActivity_List        =    new    bltPracticeActivity_List(iPracticeID,"PracticeActivityID="+requestID,"");

//declaration of Enumeration
    bltPracticeActivity        working_bltPracticeActivity;
    ListElement         leCurrentElement;
    java.util.Enumeration eList = bltPracticeActivity_List.elements();
    %>
    <%
    if (eList.hasMoreElements())
    {
        leCurrentElement    = (ListElement) eList.nextElement();
        working_bltPracticeActivity  = (bltPracticeActivity) leCurrentElement.getObject();
        pageControllerHash.put("iPracticeActivityID",working_bltPracticeActivity.getPracticeActivityID());
        pageControllerHash.put("sKeyMasterReference",request.getParameter("KM"));
        session.setAttribute("pageControllerHash",pageControllerHash);
        //Parameter Pass Code here
String parameterPassString ="";
java.util.Enumeration myParameterPassList = request.getParameterNames();
while (myParameterPassList.hasMoreElements())
{
	String myName = (String)myParameterPassList.nextElement();
	String myS = (String) request.getParameter(myName);
	parameterPassString+="&"+myName + "=" + myS;
}
        String targetRedirect = "tPracticeActivity_form.jsp?nullParam=null"+parameterPassString    ;
        if (request.getParameter("EDIT").equalsIgnoreCase("del"))
        {
            targetRedirect = "tPracticeActivity_form_delete.jsp?routePageReference=sParentReturnPage"+parameterPassString    ;
        }
        else if (request.getParameter("EDIT").equalsIgnoreCase("viewdoc"))
        {
			bltDocumentManagement myDM = new bltDocumentManagement(working_bltPracticeActivity.getDocuLinkID());
			pageControllerHash.put("sFileName",myDM.getDocumentFileName());
			pageControllerHash.put("sDownloadName", com.winstaff.password.RandomString.generateString(4).toLowerCase() + Math.round(Math.random()*1000) + "_" + myDM.getDocumentFileName());
			pageControllerHash.put("bDownload", new Boolean(true));
			session.setAttribute("pageControllerHash",pageControllerHash);
			targetRedirect =("fileRetrieve_nd.jsp")   ;
        }
        else if (request.getParameter("EDIT").equalsIgnoreCase("openflow"))
        {
            targetRedirect = DataControlUtils.getOpenFlow("tPracticeActivity_main_PracticeActivity_PracticeID_form_authorize")   ;
        }
        response.sendRedirect(targetRedirect);
    }
    else
    {
   out.println("invalid where query");
    }

  }
  else
  {
   out.println("illegal");
  }
}
else
{
out.println("Your Security Level does not permit you to View this.");
}
%>




