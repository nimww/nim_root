<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.bltDocumentManagement,java.util.Vector,java.util.Enumeration,com.winstaff.ListElement, com.winstaff.PLCUtils, com.winstaff.SecurityCheck, com.winstaff.ConfigurationMessages,com.winstaff.bltExperience,com.winstaff.bltExperience_List" %>
<%/*
    filename: tExperience_main_Experience_PhysicianID.jsp
    Created on Oct/22/2003
    Type: 1-n main class file
    Created by: Scott Ellis
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>
    <%
    Integer iExpressMode = new Integer(1);
    if (pageControllerHash.containsKey("iExpressMode")) 
    {
        iExpressMode =    (Integer)pageControllerHash.get("iExpressMode");
    }
    %>

<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_PhysicianID.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>

    <table cellpadding=0 cellspacing=0 border=0 width=<%=MasterTableWidth%> >
    <tr><td width=10>&nbsp;</td><td>
    <%=ConfigurationMessages.getHTML("EXPRESSTopControl","tExperience",iExpressMode)%>



<%
//initial declaration of list class and parentID
    Integer        iPhysicianID        =    null;
    boolean accessValid = false;
   Integer iSecurityCheck = SecurityCheck.CheckItem("PractitionerSection7", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {
    if (pageControllerHash.containsKey("iPhysicianID") ) 
    {
        iPhysicianID        =    (Integer)pageControllerHash.get("iPhysicianID");
        accessValid = true;
    }  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);
      pageControllerHash.put("sParentReturnPage","tExperience_main_Experience_PhysicianID_Expand.jsp");
    pageControllerHash.remove("iExperienceID");
    session.setAttribute("pageControllerHash",pageControllerHash);

    bltExperience_List        bltExperience_List        =    new    bltExperience_List(iPhysicianID);

//declaration of Enumeration
    bltExperience        Experience;
    ListElement         leCurrentElement;
    Enumeration eList = bltExperience_List.elements();
    %>
        <%@ include file="tExperience_main_Experience_PhysicianID_instructions.jsp" %>

        <form action="tExperience_main_Experience_PhysicianID_Express_sub.jsp" name="tExperience_main_Experience_PhysicianID1" method="POST">
    <%
    int iExpress=0;
    while (eList.hasMoreElements()||iExpress<=ConfigurationMessages.getExpressItemCount("tExperience"))
    {
       iExpress++;
         %>
         <table border="0" bordercolor="333333" cellpadding="0" class=tdHeaderAlt cellspacing="0" width="100%">
            <tr>
              <td width="50%">
                   <table border="0" bordercolor="333333" cellpadding="0" class=tdHeaderAlt cellspacing="0" width="100%">
                   <tr> 
                   	<td rowspan="2"><img src=express/left-corner.gif></td>
                   	<td width=100%><img width=100% height=2 src=express/small-line.gif></td>
                   	<td rowspan="2" align=right><img src=express/right-corner.gif></td>
                   </tr>
                     <tr> 
                       <td>
         <%

      boolean isNewRecord= false;
      if (eList.hasMoreElements())
      {
        leCurrentElement    = (ListElement) eList.nextElement();
        Experience  = (bltExperience) leCurrentElement.getObject();
      }
      else
      {
        Experience  = new bltExperience();
        isNewRecord= true;
      }
        Experience.GroupSecurityInit(UserSecurityGroupID);
        String theClass = "tdHeader";
        %>
               <span class=<%=theClass%> ><b><%=ConfigurationMessages.getDataCategory("tExperience")%> #<%=iExpress%></span>
                  </td></tr></table>
            </td></tr>
                 </table>
                 <table width="100%" border="1" cellspacing="0" bordercolor="#333333">
                  <tr>
                   <td>
                     <table width="100%" border="0" cellspacing="0">
                     <tr><td>
<%String theClassF = "textBase";%>
<%
if (isNewRecord)
{%>
<input type=hidden name=recordItemStatus_<%=iExpress%>="new">
<%}
else
{%>
<input type=hidden name=recordItemStatus_<%=iExpress%>="edit">
<%}

  {

        %>

          <%  theClass ="tdBase";%>
        <table width=100% border=1 bordercolor=#333333 cellpadding=3 cellspacing=0 class=tableBase>
         <tr><td class=tableColor>


            <table cellpadding=0 cellspacing=0 width=100%>
            <%
            if ( (Experience.isRequired("TypeOfExperience",UserSecurityGroupID))&&(!Experience.isComplete("TypeOfExperience")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((Experience.isExpired("TypeOfExperience",expiredDays))&&(Experience.isExpiredCheck("TypeOfExperience",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((Experience.isWrite("TypeOfExperience",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Experience Type&nbsp;</b></p></td><td valign=top><p><select   name="TypeOfExperience_<%=iExpress%>" ><jsp:include page="../generic/tExperienceTypeLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=Experience.getTypeOfExperience()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=TypeOfExperience&amp;sTableName=tExperience&amp;sRefID=<%=Experience.getExperienceID()%>&amp;sFieldNameDisp=<%=Experience.getEnglish("TypeOfExperience")%>&amp;sTableNameDisp=tExperience','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((Experience.isRead("TypeOfExperience",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Experience Type&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tExperienceTypeLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=Experience.getTypeOfExperience()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=TypeOfExperience&amp;sTableName=tExperience&amp;sRefID=<%=Experience.getExperienceID()%>&amp;sFieldNameDisp=<%=Experience.getEnglish("TypeOfExperience")%>&amp;sTableNameDisp=tExperience','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (Experience.isRequired("Other",UserSecurityGroupID))&&(!Experience.isComplete("Other")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((Experience.isExpired("Other",expiredDays))&&(Experience.isExpiredCheck("Other",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                     <input type=hidden name="Other_<%=iExpress%>" value="<%=Experience.getOther()%>">

            <%
            if ( (Experience.isRequired("Specialty",UserSecurityGroupID))&&(!Experience.isComplete("Specialty")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((Experience.isExpired("Specialty",expiredDays))&&(Experience.isExpiredCheck("Specialty",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((Experience.isWrite("Specialty",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Specialty, Focus, Course Name&nbsp;</b></p></td><td valign=top><p><input maxlength="100" type=text size="80" name="Specialty_<%=iExpress%>" value="<%=Experience.getSpecialty()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Specialty&amp;sTableName=tExperience&amp;sRefID=<%=Experience.getExperienceID()%>&amp;sFieldNameDisp=<%=Experience.getEnglish("Specialty")%>&amp;sTableNameDisp=tExperience','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((Experience.isRead("Specialty",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Specialty, Focus, Course Name&nbsp;</b></p></td><td valign=top><p><%=Experience.getSpecialty()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Specialty&amp;sTableName=tExperience&amp;sRefID=<%=Experience.getExperienceID()%>&amp;sFieldNameDisp=<%=Experience.getEnglish("Specialty")%>&amp;sTableNameDisp=tExperience','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (Experience.isRequired("DateFrom",UserSecurityGroupID))&&(!Experience.isComplete("DateFrom")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((Experience.isExpired("DateFrom",expiredDays))&&(Experience.isExpiredCheck("DateFrom",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>

            <%
            if ((Experience.isWrite("DateFrom",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Start Date&nbsp;(mm/dd/yyyy):&nbsp;</b></p></td><td valign=top><p><input maxlength=20  type=text size="80" name="DateFrom_<%=iExpress%>" value='<jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(Experience.getDateFrom())%>" /></jsp:include>' >&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=DateFrom&amp;sTableName=tExperience&amp;sRefID=<%=Experience.getExperienceID()%>&amp;sFieldNameDisp=<%=Experience.getEnglish("DateFrom")%>&amp;sTableNameDisp=tExperience','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((Experience.isRead("DateFrom",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Start Date&nbsp;(mm/dd/yyyy):&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(Experience.getDateFrom())%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=DateFrom&amp;sTableName=tExperience&amp;sRefID=<%=Experience.getExperienceID()%>&amp;sFieldNameDisp=<%=Experience.getEnglish("DateFrom")%>&amp;sTableNameDisp=tExperience','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (Experience.isRequired("DateTo",UserSecurityGroupID))&&(!Experience.isComplete("DateTo")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((Experience.isExpired("DateTo",expiredDays))&&(Experience.isExpiredCheck("DateTo",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>

            <%
            if ((Experience.isWrite("DateTo",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>End Date&nbsp;(mm/dd/yyyy):&nbsp;</b></p></td><td valign=top><p><input maxlength=20  type=text size="80" name="DateTo_<%=iExpress%>" value='<jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(Experience.getDateTo())%>" /></jsp:include>' >&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=DateTo&amp;sTableName=tExperience&amp;sRefID=<%=Experience.getExperienceID()%>&amp;sFieldNameDisp=<%=Experience.getEnglish("DateTo")%>&amp;sTableNameDisp=tExperience','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((Experience.isRead("DateTo",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>End Date&nbsp;(mm/dd/yyyy):&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(Experience.getDateTo())%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=DateTo&amp;sTableName=tExperience&amp;sRefID=<%=Experience.getExperienceID()%>&amp;sFieldNameDisp=<%=Experience.getEnglish("DateTo")%>&amp;sTableNameDisp=tExperience','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (Experience.isRequired("NoComplete",UserSecurityGroupID))&&(!Experience.isComplete("NoComplete")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((Experience.isExpired("NoComplete",expiredDays))&&(Experience.isExpiredCheck("NoComplete",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                     <input type=hidden name="NoComplete_<%=iExpress%>" value="<%=Experience.getNoComplete()%>">

            <%
            if ( (Experience.isRequired("Name",UserSecurityGroupID))&&(!Experience.isComplete("Name")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((Experience.isExpired("Name",expiredDays))&&(Experience.isExpiredCheck("Name",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((Experience.isWrite("Name",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Institution&nbsp;</b></p></td><td valign=top><p><input maxlength="100" type=text size="80" name="Name_<%=iExpress%>" value="<%=Experience.getName()%>">&nbsp;<a href="#" onClick="window.open('LI_tHospitalLI_Search.jsp?OFieldName=Name_<%=iExpress%>','BNPop','status=yes,scrollbars=yes,resizable=yes,width=400,height=300'); return false;"><img align=middle border=0 src=images/icon_lu.gif></a>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Name&amp;sTableName=tExperience&amp;sRefID=<%=Experience.getExperienceID()%>&amp;sFieldNameDisp=<%=Experience.getEnglish("Name")%>&amp;sTableNameDisp=tExperience','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((Experience.isRead("Name",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Institution&nbsp;</b></p></td><td valign=top><p><%=Experience.getName()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Name&amp;sTableName=tExperience&amp;sRefID=<%=Experience.getExperienceID()%>&amp;sFieldNameDisp=<%=Experience.getEnglish("Name")%>&amp;sTableNameDisp=tExperience','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (Experience.isRequired("Address1",UserSecurityGroupID))&&(!Experience.isComplete("Address1")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((Experience.isExpired("Address1",expiredDays))&&(Experience.isExpiredCheck("Address1",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((Experience.isWrite("Address1",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Address&nbsp;</b></p></td><td valign=top><p><input maxlength="50" type=text size="80" name="Address1_<%=iExpress%>" value="<%=Experience.getAddress1()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Address1&amp;sTableName=tExperience&amp;sRefID=<%=Experience.getExperienceID()%>&amp;sFieldNameDisp=<%=Experience.getEnglish("Address1")%>&amp;sTableNameDisp=tExperience','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((Experience.isRead("Address1",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Address&nbsp;</b></p></td><td valign=top><p><%=Experience.getAddress1()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Address1&amp;sTableName=tExperience&amp;sRefID=<%=Experience.getExperienceID()%>&amp;sFieldNameDisp=<%=Experience.getEnglish("Address1")%>&amp;sTableNameDisp=tExperience','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (Experience.isRequired("Address2",UserSecurityGroupID))&&(!Experience.isComplete("Address2")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((Experience.isExpired("Address2",expiredDays))&&(Experience.isExpiredCheck("Address2",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((Experience.isWrite("Address2",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Address 2&nbsp;</b></p></td><td valign=top><p><input maxlength="20" type=text size="80" name="Address2_<%=iExpress%>" value="<%=Experience.getAddress2()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Address2&amp;sTableName=tExperience&amp;sRefID=<%=Experience.getExperienceID()%>&amp;sFieldNameDisp=<%=Experience.getEnglish("Address2")%>&amp;sTableNameDisp=tExperience','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((Experience.isRead("Address2",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Address 2&nbsp;</b></p></td><td valign=top><p><%=Experience.getAddress2()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Address2&amp;sTableName=tExperience&amp;sRefID=<%=Experience.getExperienceID()%>&amp;sFieldNameDisp=<%=Experience.getEnglish("Address2")%>&amp;sTableNameDisp=tExperience','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (Experience.isRequired("City",UserSecurityGroupID))&&(!Experience.isComplete("City")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((Experience.isExpired("City",expiredDays))&&(Experience.isExpiredCheck("City",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((Experience.isWrite("City",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Institution City&nbsp;</b></p></td><td valign=top><p><input maxlength="30" type=text size="80" name="City_<%=iExpress%>" value="<%=Experience.getCity()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=City&amp;sTableName=tExperience&amp;sRefID=<%=Experience.getExperienceID()%>&amp;sFieldNameDisp=<%=Experience.getEnglish("City")%>&amp;sTableNameDisp=tExperience','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((Experience.isRead("City",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Institution City&nbsp;</b></p></td><td valign=top><p><%=Experience.getCity()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=City&amp;sTableName=tExperience&amp;sRefID=<%=Experience.getExperienceID()%>&amp;sFieldNameDisp=<%=Experience.getEnglish("City")%>&amp;sTableNameDisp=tExperience','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (Experience.isRequired("StateID",UserSecurityGroupID))&&(!Experience.isComplete("StateID")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((Experience.isExpired("StateID",expiredDays))&&(Experience.isExpiredCheck("StateID",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((Experience.isWrite("StateID",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>State&nbsp;</b></p></td><td valign=top><p><select   name="StateID_<%=iExpress%>" ><jsp:include page="../generic/tStateLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=Experience.getStateID()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=StateID&amp;sTableName=tExperience&amp;sRefID=<%=Experience.getExperienceID()%>&amp;sFieldNameDisp=<%=Experience.getEnglish("StateID")%>&amp;sTableNameDisp=tExperience','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((Experience.isRead("StateID",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>State&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tStateLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=Experience.getStateID()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=StateID&amp;sTableName=tExperience&amp;sRefID=<%=Experience.getExperienceID()%>&amp;sFieldNameDisp=<%=Experience.getEnglish("StateID")%>&amp;sTableNameDisp=tExperience','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (Experience.isRequired("Province",UserSecurityGroupID))&&(!Experience.isComplete("Province")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((Experience.isExpired("Province",expiredDays))&&(Experience.isExpiredCheck("Province",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                     <input type=hidden name="Province_<%=iExpress%>" value="<%=Experience.getProvince()%>">

            <%
            if ( (Experience.isRequired("ZIP",UserSecurityGroupID))&&(!Experience.isComplete("ZIP")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((Experience.isExpired("ZIP",expiredDays))&&(Experience.isExpiredCheck("ZIP",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((Experience.isWrite("ZIP",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>ZIP&nbsp;</b></p></td><td valign=top><p><input maxlength="50" type=text size="80" name="ZIP_<%=iExpress%>" value="<%=Experience.getZIP()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ZIP&amp;sTableName=tExperience&amp;sRefID=<%=Experience.getExperienceID()%>&amp;sFieldNameDisp=<%=Experience.getEnglish("ZIP")%>&amp;sTableNameDisp=tExperience','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((Experience.isRead("ZIP",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>ZIP&nbsp;</b></p></td><td valign=top><p><%=Experience.getZIP()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ZIP&amp;sTableName=tExperience&amp;sRefID=<%=Experience.getExperienceID()%>&amp;sFieldNameDisp=<%=Experience.getEnglish("ZIP")%>&amp;sTableNameDisp=tExperience','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (Experience.isRequired("CountryID",UserSecurityGroupID))&&(!Experience.isComplete("CountryID")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((Experience.isExpired("CountryID",expiredDays))&&(Experience.isExpiredCheck("CountryID",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((Experience.isWrite("CountryID",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Country&nbsp;</b></p></td><td valign=top><p><select   name="CountryID_<%=iExpress%>" ><jsp:include page="../generic/tCountryLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=Experience.getCountryID()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CountryID&amp;sTableName=tExperience&amp;sRefID=<%=Experience.getExperienceID()%>&amp;sFieldNameDisp=<%=Experience.getEnglish("CountryID")%>&amp;sTableNameDisp=tExperience','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((Experience.isRead("CountryID",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Country&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tCountryLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=Experience.getCountryID()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CountryID&amp;sTableName=tExperience&amp;sRefID=<%=Experience.getExperienceID()%>&amp;sFieldNameDisp=<%=Experience.getEnglish("CountryID")%>&amp;sTableNameDisp=tExperience','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (Experience.isRequired("Phone",UserSecurityGroupID))&&(!Experience.isComplete("Phone")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((Experience.isExpired("Phone",expiredDays))&&(Experience.isExpiredCheck("Phone",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                     <input type=hidden name="Phone_<%=iExpress%>" value="<%=Experience.getPhone()%>">

            <%
            if ( (Experience.isRequired("Fax",UserSecurityGroupID))&&(!Experience.isComplete("Fax")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((Experience.isExpired("Fax",expiredDays))&&(Experience.isExpiredCheck("Fax",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                     <input type=hidden name="Fax_<%=iExpress%>" value="<%=Experience.getFax()%>">

            <%
            if ( (Experience.isRequired("ContactName",UserSecurityGroupID))&&(!Experience.isComplete("ContactName")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((Experience.isExpired("ContactName",expiredDays))&&(Experience.isExpiredCheck("ContactName",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                     <input type=hidden name="ContactName_<%=iExpress%>" value="<%=Experience.getContactName()%>">

            <%
            if ( (Experience.isRequired("ContactEmail",UserSecurityGroupID))&&(!Experience.isComplete("ContactEmail")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((Experience.isExpired("ContactEmail",expiredDays))&&(Experience.isExpiredCheck("ContactEmail",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                     <input type=hidden name="ContactEmail_<%=iExpress%>" value="<%=Experience.getContactEmail()%>">

            <%
            if ( (Experience.isRequired("Comments",UserSecurityGroupID))&&(!Experience.isComplete("Comments")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((Experience.isExpired("Comments",expiredDays))&&(Experience.isExpiredCheck("Comments",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                     <input type=hidden name="Comments_<%=iExpress%>" value="<%=Experience.getComments()%>">


            <tr><td width=40%>&nbsp;</td><td width=60%>&nbsp;</td></tr>

            <input type=hidden name=routePageReference value="sParentReturnPage">

        </td></tr></table>
        </td></tr></table>
        <%
  }%>

        </td></tr></table></table><br>        <%
    }
    %>
        <input <%=HTMLFormStyleButton%> onClick="document.forms[0].nextPage.value='<%=ConfigurationMessages.getExpressLinkRaw("tExperience","next",iExpressMode)%>'" type=Submit value="Continue" name=Submit>
        <input type=hidden name=nextPage value="">
        </form>

    </table>
    <%
  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>

    </td></tr></table>


<%String bnIncludeFN = "ui_"+thePLCID+"\\bot-nav_PhysicianID.jsp";
%>
<jsp:include page="<%=bnIncludeFN%>" flush="true" ><jsp:param name="plcID" value="<%=thePLCID%>"/></jsp:include>
