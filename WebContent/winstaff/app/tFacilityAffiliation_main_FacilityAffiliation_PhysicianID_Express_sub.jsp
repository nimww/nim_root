<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.bltDocumentManagement,java.util.Vector,java.util.Enumeration,com.winstaff.ListElement, com.winstaff.PLCUtils, com.winstaff.SecurityCheck, com.winstaff.ConfigurationMessages,com.winstaff.bltFacilityAffiliation,com.winstaff.bltFacilityAffiliation_List,com.winstaff.DocumentManagerUtils" %>
<%/*
    filename: tFacilityAffiliation_main_FacilityAffiliation_PhysicianID.jsp
    Created on Nov/12/2003
    Type: 1-n main class file
    Created by: Scott Ellis
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>
    <%
    Integer iExpressMode = new Integer(1);
    if (pageControllerHash.containsKey("iExpressMode")) 
    {
        iExpressMode =    (Integer)pageControllerHash.get("iExpressMode");
    }
    %>

    <table cellpadding=0 cellspacing=0 border=0 width=800 >
<%
//initial declaration of list class and parentID
    Integer        iPhysicianID        =    null;
    boolean accessValid = false;
   Integer iSecurityCheck = SecurityCheck.CheckItem("PractitionerSection10", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {
    if (pageControllerHash.containsKey("iPhysicianID")) 
    {
        iPhysicianID        =    (Integer)pageControllerHash.get("iPhysicianID");
        accessValid = true;
    }

  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);
      pageControllerHash.put("sParentReturnPage","tFacilityAffiliation_main_FacilityAffiliation_PhysicianID_Expand.jsp");
    pageControllerHash.remove("iAffiliationID");
    session.setAttribute("pageControllerHash",pageControllerHash);

    bltFacilityAffiliation_List        bltFacilityAffiliation_List        =    new    bltFacilityAffiliation_List(iPhysicianID);

//declaration of Enumeration
    bltFacilityAffiliation        FacilityAffiliation;
    ListElement         leCurrentElement;
    Enumeration eList = bltFacilityAffiliation_List.elements();
    %>
    <%
    int iExpress=0;
boolean errorRouteMeTotal = false;
boolean isAllComplete = true;
    while (eList.hasMoreElements()||iExpress<=ConfigurationMessages.getExpressItemCount("tFacilityAffiliation"))
    {
       iExpress++;

      boolean isNewRecord= false;
      if (eList.hasMoreElements())
      {
        leCurrentElement    = (ListElement) eList.nextElement();
        FacilityAffiliation  = (bltFacilityAffiliation) leCurrentElement.getObject();
      }
      else
      {
        FacilityAffiliation  = new bltFacilityAffiliation();
        FacilityAffiliation.setPhysicianID(iPhysicianID);
        isNewRecord= true;
      }
        FacilityAffiliation.GroupSecurityInit(UserSecurityGroupID);
        String theClass = "tdHeader";
        %>

        <%  {

String testChangeID = "0";

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("UniqueCreateDate_"+iExpress))) ;
    if ( !FacilityAffiliation.getUniqueCreateDate().equals(testObj)  )
    {
         FacilityAffiliation.setUniqueCreateDate( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("FacilityAffiliation UniqueCreateDate not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("UniqueModifyDate_"+iExpress))) ;
    if ( !FacilityAffiliation.getUniqueModifyDate().equals(testObj)  )
    {
         FacilityAffiliation.setUniqueModifyDate( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("FacilityAffiliation UniqueModifyDate not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("UniqueModifyComments_"+iExpress)) ;
    if ( !FacilityAffiliation.getUniqueModifyComments().equals(testObj)  )
    {
         FacilityAffiliation.setUniqueModifyComments( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("FacilityAffiliation UniqueModifyComments not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("PhysicianID_"+iExpress)) ;
    if ( !FacilityAffiliation.getPhysicianID().equals(testObj)  )
    {
         FacilityAffiliation.setPhysicianID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("FacilityAffiliation PhysicianID not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("IsPrimary_"+iExpress)) ;
    if ( !FacilityAffiliation.getIsPrimary().equals(testObj)  )
    {
         FacilityAffiliation.setIsPrimary( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("FacilityAffiliation IsPrimary not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("AppointmentLevel_"+iExpress)) ;
    if ( !FacilityAffiliation.getAppointmentLevel().equals(testObj)  )
    {
         FacilityAffiliation.setAppointmentLevel( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("FacilityAffiliation AppointmentLevel not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("StartDate_"+iExpress))) ;
    if ( !FacilityAffiliation.getStartDate().equals(testObj)  )
    {
         FacilityAffiliation.setStartDate( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("FacilityAffiliation StartDate not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("EndDate_"+iExpress))) ;
    if ( !FacilityAffiliation.getEndDate().equals(testObj)  )
    {
         FacilityAffiliation.setEndDate( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("FacilityAffiliation EndDate not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("PendingDate_"+iExpress))) ;
    if ( !FacilityAffiliation.getPendingDate().equals(testObj)  )
    {
         FacilityAffiliation.setPendingDate( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("FacilityAffiliation PendingDate not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("FacilityName_"+iExpress)) ;
    if ( !FacilityAffiliation.getFacilityName().equals(testObj)  )
    {
         FacilityAffiliation.setFacilityName( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("FacilityAffiliation FacilityName not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("FacilityDepartment_"+iExpress)) ;
    if ( !FacilityAffiliation.getFacilityDepartment().equals(testObj)  )
    {
         FacilityAffiliation.setFacilityDepartment( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("FacilityAffiliation FacilityDepartment not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("FacilityAddress1_"+iExpress)) ;
    if ( !FacilityAffiliation.getFacilityAddress1().equals(testObj)  )
    {
         FacilityAffiliation.setFacilityAddress1( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("FacilityAffiliation FacilityAddress1 not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("FacilityAddress2_"+iExpress)) ;
    if ( !FacilityAffiliation.getFacilityAddress2().equals(testObj)  )
    {
         FacilityAffiliation.setFacilityAddress2( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("FacilityAffiliation FacilityAddress2 not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("FacilityCity_"+iExpress)) ;
    if ( !FacilityAffiliation.getFacilityCity().equals(testObj)  )
    {
         FacilityAffiliation.setFacilityCity( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("FacilityAffiliation FacilityCity not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("FacilityStateID_"+iExpress)) ;
    if ( !FacilityAffiliation.getFacilityStateID().equals(testObj)  )
    {
         FacilityAffiliation.setFacilityStateID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("FacilityAffiliation FacilityStateID not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("FacilityProvince_"+iExpress)) ;
    if ( !FacilityAffiliation.getFacilityProvince().equals(testObj)  )
    {
         FacilityAffiliation.setFacilityProvince( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("FacilityAffiliation FacilityProvince not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("FacilityZIP_"+iExpress)) ;
    if ( !FacilityAffiliation.getFacilityZIP().equals(testObj)  )
    {
         FacilityAffiliation.setFacilityZIP( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("FacilityAffiliation FacilityZIP not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("FacilityCountryID_"+iExpress)) ;
    if ( !FacilityAffiliation.getFacilityCountryID().equals(testObj)  )
    {
         FacilityAffiliation.setFacilityCountryID( testObj,UserSecurityGroupID   );
         //testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("FacilityAffiliation FacilityCountryID not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("FacilityPhone_"+iExpress)) ;
    if ( !FacilityAffiliation.getFacilityPhone().equals(testObj)  )
    {
         FacilityAffiliation.setFacilityPhone( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("FacilityAffiliation FacilityPhone not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("FacilityFax_"+iExpress)) ;
    if ( !FacilityAffiliation.getFacilityFax().equals(testObj)  )
    {
         FacilityAffiliation.setFacilityFax( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("FacilityAffiliation FacilityFax not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("ContactName_"+iExpress)) ;
    if ( !FacilityAffiliation.getContactName().equals(testObj)  )
    {
         FacilityAffiliation.setContactName( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("FacilityAffiliation ContactName not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("ContactEmail_"+iExpress)) ;
    if ( !FacilityAffiliation.getContactEmail().equals(testObj)  )
    {
         FacilityAffiliation.setContactEmail( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("FacilityAffiliation ContactEmail not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("ReasonForLeaving_"+iExpress)) ;
    if ( !FacilityAffiliation.getReasonForLeaving().equals(testObj)  )
    {
         FacilityAffiliation.setReasonForLeaving( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("FacilityAffiliation ReasonForLeaving not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("AdmissionPriviledges_"+iExpress)) ;
    if ( !FacilityAffiliation.getAdmissionPriviledges().equals(testObj)  )
    {
         FacilityAffiliation.setAdmissionPriviledges( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("FacilityAffiliation AdmissionPriviledges not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("AdmissionArrangements_"+iExpress)) ;
    if ( !FacilityAffiliation.getAdmissionArrangements().equals(testObj)  )
    {
         FacilityAffiliation.setAdmissionArrangements( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("FacilityAffiliation AdmissionArrangements not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("UnrestrictedAdmission_"+iExpress)) ;
    if ( !FacilityAffiliation.getUnrestrictedAdmission().equals(testObj)  )
    {
         FacilityAffiliation.setUnrestrictedAdmission( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("FacilityAffiliation UnrestrictedAdmission not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("TempPriviledges_"+iExpress)) ;
    if ( !FacilityAffiliation.getTempPriviledges().equals(testObj)  )
    {
         FacilityAffiliation.setTempPriviledges( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("FacilityAffiliation TempPriviledges not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("InpatientCare_"+iExpress)) ;
    if ( !FacilityAffiliation.getInpatientCare().equals(testObj)  )
    {
         FacilityAffiliation.setInpatientCare( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("FacilityAffiliation InpatientCare not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("PercentAdmissions_"+iExpress)) ;
    if ( !FacilityAffiliation.getPercentAdmissions().equals(testObj)  )
    {
         FacilityAffiliation.setPercentAdmissions( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("FacilityAffiliation PercentAdmissions not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("Comments_"+iExpress)) ;
    if ( !FacilityAffiliation.getComments().equals(testObj)  )
    {
         FacilityAffiliation.setComments( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("FacilityAffiliation Comments not set. this is ok-not an error");
}
boolean bINT = false;
boolean bNextStep = false;
String sRefreshDoc = "refreshMe=true&";
              if (false&&request.getParameter("INTNext")!=null&&request.getParameter("INTNext").equalsIgnoreCase("yes") ) 
	            {
	                bINT = true;
	                sRefreshDoc = "";
	            }
              else if (request.getParameter("INTNext")!=null&&request.getParameter("INTNext").equalsIgnoreCase("next") ) 
	            {
	                bINT = true;
	                sINTNext = ConfigurationMessages.getInterviewLinkRaw("tFacilityAffiliation","next");
	                sRefreshDoc = "";
	            }

// If an edit, update information; if new, append information
boolean errorRouteMe = false;
if (testChangeID.equalsIgnoreCase("1"))
{

	FacilityAffiliation.setUniqueModifyDate(new java.util.Date());
	String UserLogonDescription = "n/a";
	if (pageControllerHash.containsKey("UserLogonDescription"))
	{
	    UserLogonDescription = (String)pageControllerHash.get("UserLogonDescription");
	}
    if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
    {
	    FacilityAffiliation.setUniqueModifyComments("EX:"+UserLogonDescription);
	    try
	    {
	        FacilityAffiliation.commitData();
	        if (!FacilityAffiliation.isComplete())
	        {
	            isAllComplete = false;
	        }
	    }
	    catch (Exception e55)
	    {
	        errorRouteMe = true;
	        errorRouteMeTotal = true;
	    }
    }
}       }
    }//while

if (errorRouteMeTotal)
{
	out.println(ConfigurationMessages.getMessage("FatalError")); 
}
else 
{
String nextPage = request.getParameter("nextPage");
if (nextPage==null||nextPage.equalsIgnoreCase(""))
{
	nextPage = ConfigurationMessages.getExpressLinkRaw("tFacilityAffiliation","next",iExpressMode);
}
if (!isAllComplete)
{
	%>
	<script language=Javascript>
	      if ( confirm("<%=ConfigurationMessages.getMessage("ConfirmReqFieldsReturn")%>") )
	      {
	          document.location="<%=ConfigurationMessages.getDataCategoryLink("tFacilityAffiliation","Express")%>";
	      }
	      else 
	      {
	          document.location="<%=nextPage%>";
	      }
	</script>
  <%
}
else
{
	%>
	<script language=Javascript>
	      document.location="<%=nextPage%>";
	</script>
  <%
}
}
       
  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>

    </td></tr></table>


<%String bnIncludeFN = "ui_"+thePLCID+"\\bot-nav_PhysicianID.jsp";
%>
<jsp:include page="<%=bnIncludeFN%>" flush="true" ><jsp:param name="plcID" value="<%=thePLCID%>"/></jsp:include>
