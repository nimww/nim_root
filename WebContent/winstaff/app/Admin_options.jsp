<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN"> 
<%@page contentType="text/html" language="java" import="java.util.Vector,java.util.Enumeration,com.winstaff.ListElement, com.winstaff.PLCUtils,com.winstaff.bltAdminMaster" %>
<%/*

    filename: out\jsp\tAdminMaster_form.jsp
    Created on Apr/23/2002
*/%>
<%@ include file="../generic/CheckLogin.jsp" %>
<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_AdminID.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>
<script language="JavaScript">
<!--
function MM_callJS(jsStr) { //v2.0
  return eval(jsStr)
}
//-->
</script>
<table cellpadding=0 cellspacing=0 border=0 width=700>
  <tr> 
    <td width=10>&nbsp;</td>
    <td class=title> Options<br>
      <%
//initial declaration of list class and parentID
    Integer        iAdminID        =    null;
    boolean accessValid = false;
    // required for Type2
    String sKeyMasterReference = null;
    if (pageControllerHash.containsKey("iAdminID")) 
    {
        iAdminID        =    (Integer)pageControllerHash.get("iAdminID");
        accessValid = true;    
		}
  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);      java.text.SimpleDateFormat displayDateTimeSDF = new java.text.SimpleDateFormat("MM/dd/yyyy hh:mm a");

    pageControllerHash.put("sLocalChildReturnPage","Admin_options.jsp");
    session.setAttribute("pageControllerHash",pageControllerHash);

//initial declaration of list class and parentID

    bltAdminMaster        AdminMaster        =    null;

        AdminMaster        =    new    bltAdminMaster(iAdminID);

//fields
        %>
      <form method=POST action="Admin_options_sub.jsp" name="tAdminMaster_form1">
        <table width = 100% border=0 cellpadding=1 cellspacing=0 class=tableBase>
          <%  String theClass ="tdBase";%>
          <tr class=<%=theClass%> > 
            <td> 
              <p><b><span class=title>Admin Alert Options</span></b><br>
                In order to receive any alerts pertaining to <u><b>ALL</b></u> 
                your practitioners, you must fill out an &quot;Alert Email&quot; 
                and designate &quot;Alert Days&quot; - (Note: you can also set 
                up alerts to go to each individual practitioner that you admin. 
                To do this, simply open the practitioner account click on &quot;Options&quot; 
                under the &quot;my PRO-FILE&quot; menu.</p>
              <p><b>What is my Alert Email:</b> Alerts such as reminders of soon-to-expire 
                Licenses, important notifications of application status, and other 
                such alerts pertaining to all practitioners that you admin will 
                be sent this this E-mail address.</p>
              <p><b>What are Alert Days:</b> For any alerts pertaining to expiring 
                license, your Alert Days setting will determine how many days 
                in advance you receive these alerts. For example if you were to 
                enter &quot;30&quot; then you would receive an alert 30 days before 
                the actual expiration date of any appropriate documents, licenses, 
                professional liability policies, etc. Setting this to &quot;0&quot; 
                turns off all alerts.</p>
            </td>
            <td rowspan="7" width="25%"> 
              <div align="center"><img src="images/report_download.gif"></div>
            </td>
          </tr>
          <tr class=<%=theClass%> > 
            <td>&nbsp;</td>
          </tr>
          <input type="hidden" name="EDIT" value = "edit" >
          <input type="hidden" name="routePageReference" value = "sLocalChildReturnPage" >
          <tr class=<%=theClass%> > 
            <td><b>Alert Email:&nbsp;</b> 
              <input   type=text size="50" name="AlertEmail" value="<%=AdminMaster.getAlertEmail()%>">
            </td>
          </tr>
          <tr class=<%=theClass%> > 
            <td>&nbsp;</td>
          </tr>
          <tr class=<%=theClass%> > 
            <td><b>Alert Days:&nbsp;</b> 
              <input   type=text size="8" name="AlertDays" value="<%=AdminMaster.getAlertDays()%>">
              If you do not want to receive e-mails please enter &quot;0&quot; 
              or <a href="#" onClick="MM_callJS('document.tAdminMaster_form1.AlertDays.value=0;')">click 
              here</a></td>
          </tr>
          <tr class=<%=theClass%> > 
            <td>&nbsp;</td>
          </tr>
          <tr class=<%=theClass%> > 
            <td><b>Last Import/Sync Date:&nbsp;</b> [<%=PLCUtils.getDisplayDate(AdminMaster.getLastImportDate(),true)%>] 
              <br>
              Enter a new date here: 
              <input   type=text size="50" name="LastImportDate" value="">
              <br>
              Leave blank to keep the same date/time.
              <br>
              To set date/time to today <a href="#" onClick="MM_callJS('document.tAdminMaster_form1.LastImportDate.value=\'today\';'); return false">click here</a></td>
          </tr>
          <tr class=<%=theClass%> > 
            <td>&nbsp;</td>
          </tr>
          <%if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {%>
          <tr> 
            <td> 
              <input <%=HTMLFormStyleButton%> type=Submit value=Submit name=Submit>
            </td>
          </tr>
          <%}%>
        </table>
      </form>
      <%
  }
  else
  {
   response.sendRedirect("/errorhandler.jsp");
  }

%>
    </td>
  </tr>
</table>
<%String bnIncludeFN = "ui_"+thePLCID+"\\bot-nav_AdminID.jsp";
%>
<jsp:include page="<%=bnIncludeFN%>" flush="true" > 
<jsp:param name="plcID" value="<%=thePLCID%>"/>
</jsp:include>
