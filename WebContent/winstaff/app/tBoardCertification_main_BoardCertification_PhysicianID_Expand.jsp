<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.bltDocumentManagement,java.util.Vector,java.util.Enumeration,com.winstaff.ListElement, com.winstaff.PLCUtils, com.winstaff.SecurityCheck, com.winstaff.ConfigurationMessages,com.winstaff.bltBoardCertification,com.winstaff.bltBoardCertification_List" %>
<%/*
    filename: tBoardCertification_main_BoardCertification_PhysicianID.jsp
    Created on Mar/21/2003
    Type: 1-n main class file
    Created by: Scott Ellis
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>

<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_PhysicianID.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>

    <table cellpadding=0 cellspacing=0 border=0 width=<%=MasterTableWidth%> >
    <tr><td width=10>&nbsp;</td><td>
    <%=ConfigurationMessages.getHTML("INTERVIEWTopControl","tBoardCertification")%>

    <br><a href="tBoardCertification_main_BoardCertification_PhysicianID.jsp">Compact</a>

<%
//initial declaration of list class and parentID
    Integer        iPhysicianID        =    null;
    boolean accessValid = false;
   Integer iSecurityCheck = SecurityCheck.CheckItem("PractitionerSection4", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {
    if (pageControllerHash.containsKey("iPhysicianID")) 
    {
        iPhysicianID        =    (Integer)pageControllerHash.get("iPhysicianID");
        accessValid = true;
    }
  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);
      pageControllerHash.put("sParentReturnPage","tBoardCertification_main_BoardCertification_PhysicianID_Expand.jsp");
    pageControllerHash.remove("iBoardCertificationID");
    session.setAttribute("pageControllerHash",pageControllerHash);

    bltBoardCertification_List        bltBoardCertification_List        =    new    bltBoardCertification_List(iPhysicianID);

//declaration of Enumeration
    bltBoardCertification        working_bltBoardCertification;
    ListElement         leCurrentElement;
    Enumeration eList = bltBoardCertification_List.elements();
    %>
        <%@ include file="tBoardCertification_main_BoardCertification_PhysicianID_instructions.jsp" %>

        <%if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {%>
        <a class=linkBase href = "tBoardCertification_main_BoardCertification_PhysicianID_form_create.jsp?EDIT=new&KM=p"><img border=0 src="ui_<%=thePLCID%>/icons/create_PhysicianID.gif"></a>
        <%}%>
    <%
    while (eList.hasMoreElements())
    {
         %>
         <table border="0" bordercolor="333333" cellpadding="3" class=tdHeaderAlt cellspacing="0" width="100%">
            <tr>
              <td width="50%">
                   <table border="0" bordercolor="333333" cellpadding="3" class=tdHeaderAlt cellspacing="0" width="100%">
                     <tr> 
                       <td>
         <%

        leCurrentElement    = (ListElement) eList.nextElement();
        working_bltBoardCertification  = (bltBoardCertification) leCurrentElement.getObject();
        working_bltBoardCertification.GroupSecurityInit(UserSecurityGroupID);
        String theClass = "tdHeader";
        if (!working_bltBoardCertification.isComplete())
        {
            theClass = "incompleteItem";
        %>
                <span class=incompleteItem><b>Not Complete</b></span><br>
        <%
        }
        %>
               <span class=<%=theClass%> ><b>ID:&nbsp;</b><%=working_bltBoardCertification.getBoardCertificationID()%> </span>
                  </td></tr>
                  <tr><td><b>Item Create Date:&nbsp;</b><jsp:include page="../generic/DateTypeConvert_Main.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(working_bltBoardCertification.getUniqueCreateDate())%>" /></jsp:include></td></tr>
                  <tr><td><b>Item Modify Date:&nbsp;</b><jsp:include page="../generic/DateTypeConvert_Main.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(working_bltBoardCertification.getUniqueModifyDate())%>" /></jsp:include></td></tr>
                  <tr><td><b>Item Modify Comments:&nbsp;</b><%=working_bltBoardCertification.getUniqueModifyComments()%></td></tr>
                  </td></tr></table>
            </td><td width="50%" bgColor=#ffffff> 
        <a class=linkBase href = "tBoardCertification_main_BoardCertification_PhysicianID_form_authorize.jsp?EDIT=edit&EDITID=<%=working_bltBoardCertification.getBoardCertificationID()%>&KM=p"><img border=0 src="ui_<%=thePLCID%>/icons/edit_PhysicianID.gif"></a>


        <%if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {%>
        <br><a class=linkBase  onClick="return confirmDelete()"  href = "tBoardCertification_main_BoardCertification_PhysicianID_form_authorize.jsp?EDIT=del&EDITID=<%=working_bltBoardCertification.getBoardCertificationID()%>&KM=p"><img border=0 src="ui_<%=thePLCID%>/icons/delete_PhysicianID.gif"></a>
        <br>
        <% 
            if (working_bltBoardCertification.getDocuLinkID().intValue()>0)
            {
              bltDocumentManagement myDoc = new bltDocumentManagement(working_bltBoardCertification.getDocuLinkID());
              if (!myDoc.getDocumentFileName().equalsIgnoreCase(""))
              {
              %>
                      <a class=linkBase target=_blank href = "pdf/<%=myDoc.getDocumentFileName()%>">View Document</a>

              <%
              }
              else
              {
              %>

                      <a class=linkBase target=_blank href = "#" onClick="window.open('tDocumentManagement_main_DocumentManagement_PhysicianID_form_authorize.jsp?EDIT=faxCover&amp;EDITID=<%=working_bltBoardCertification.getDocuLinkID()%>','DocPDF','toolbar=yes,location=yes,status=yes,menubar=yes,scrollbars=yes,resizable=yes,width=500,height=500');return false;" >Print Cover Sheet</a>

              <%
              }
            }
            else
            {
             if (working_bltBoardCertification.isComplete())
             {
               %>
                      <a target=_blank class=linkBase href = "tBoardCertification_ModifyDocument.jsp?EDITID=<%=working_bltBoardCertification.getBoardCertificationID()%>&EDIT=CREATE&dType=tBoardCertification">Create Document</a>
               <%
             }
             else
             {
               %>
                      <span class=tdBase>You must complete this item before you can attach a document</span>
               <%
             }
            }
            %>

        <%}%>
                  </td></tr>
                 </table>
                 <table width="100%" border="1" cellspacing="0" bordercolor="#333333">
                  <tr>
                   <td>
                     <table width="100%" border="0" cellspacing="0">
                     <tr><td>
<%String theClassF = "textBase";%>

<%theClassF = "textBase";%>
<%if ((working_bltBoardCertification.isExpired("Specialty",expiredDays))&&(working_bltBoardCertification.isExpiredCheck("Specialty"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltBoardCertification.isRequired("Specialty"))&&(!working_bltBoardCertification.isComplete("Specialty")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Specialty:&nbsp;</b><%=working_bltBoardCertification.getSpecialty()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltBoardCertification.isExpired("SubSpecialty",expiredDays))&&(working_bltBoardCertification.isExpiredCheck("SubSpecialty"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltBoardCertification.isRequired("SubSpecialty"))&&(!working_bltBoardCertification.isComplete("SubSpecialty")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Sub-specialty related to above specialty:&nbsp;</b><%=working_bltBoardCertification.getSubSpecialty()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltBoardCertification.isExpired("IsListed",expiredDays))&&(working_bltBoardCertification.isExpiredCheck("IsListed"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltBoardCertification.isRequired("IsListed"))&&(!working_bltBoardCertification.isComplete("IsListed")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Do you wish to be listed in Directories under this specialty?:&nbsp;</b><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltBoardCertification.getIsListed()%>" /></jsp:include></p>

<%theClassF = "textBase";%>
<%if ((working_bltBoardCertification.isExpired("IsPrimary",expiredDays))&&(working_bltBoardCertification.isExpiredCheck("IsPrimary"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltBoardCertification.isRequired("IsPrimary"))&&(!working_bltBoardCertification.isComplete("IsPrimary")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Is this your primary specialty?:&nbsp;</b><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltBoardCertification.getIsPrimary()%>" /></jsp:include></p>

<%theClassF = "textBase";%>
<%if ((working_bltBoardCertification.isExpired("SpecialtyBoardCertified",expiredDays))&&(working_bltBoardCertification.isExpiredCheck("SpecialtyBoardCertified"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltBoardCertification.isRequired("SpecialtyBoardCertified"))&&(!working_bltBoardCertification.isComplete("SpecialtyBoardCertified")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Specialty Status:&nbsp;</b><jsp:include page="../generic/tSpecialtyStatusLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltBoardCertification.getSpecialtyBoardCertified()%>" /></jsp:include></p>

<%theClassF = "textBase";%>
<%if ((working_bltBoardCertification.isExpired("BoardName",expiredDays))&&(working_bltBoardCertification.isExpiredCheck("BoardName"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltBoardCertification.isRequired("BoardName"))&&(!working_bltBoardCertification.isComplete("BoardName")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Board Name:&nbsp;</b><%=working_bltBoardCertification.getBoardName()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltBoardCertification.isExpired("ContactName",expiredDays))&&(working_bltBoardCertification.isExpiredCheck("ContactName"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltBoardCertification.isRequired("ContactName"))&&(!working_bltBoardCertification.isComplete("ContactName")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Contact Name:&nbsp;</b><%=working_bltBoardCertification.getContactName()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltBoardCertification.isExpired("ContactEmail",expiredDays))&&(working_bltBoardCertification.isExpiredCheck("ContactEmail"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltBoardCertification.isRequired("ContactEmail"))&&(!working_bltBoardCertification.isComplete("ContactEmail")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Contact E-mail:&nbsp;</b><%=working_bltBoardCertification.getContactEmail()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltBoardCertification.isExpired("Address1",expiredDays))&&(working_bltBoardCertification.isExpiredCheck("Address1"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltBoardCertification.isRequired("Address1"))&&(!working_bltBoardCertification.isComplete("Address1")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Address:&nbsp;</b><%=working_bltBoardCertification.getAddress1()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltBoardCertification.isExpired("Address2",expiredDays))&&(working_bltBoardCertification.isExpiredCheck("Address2"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltBoardCertification.isRequired("Address2"))&&(!working_bltBoardCertification.isComplete("Address2")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Address 2:&nbsp;</b><%=working_bltBoardCertification.getAddress2()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltBoardCertification.isExpired("City",expiredDays))&&(working_bltBoardCertification.isExpiredCheck("City"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltBoardCertification.isRequired("City"))&&(!working_bltBoardCertification.isComplete("City")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Institution City:&nbsp;</b><%=working_bltBoardCertification.getCity()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltBoardCertification.isExpired("StateID",expiredDays))&&(working_bltBoardCertification.isExpiredCheck("StateID"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltBoardCertification.isRequired("StateID"))&&(!working_bltBoardCertification.isComplete("StateID")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>State:&nbsp;</b><jsp:include page="../generic/tStateLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltBoardCertification.getStateID()%>" /></jsp:include></p>

<%theClassF = "textBase";%>
<%if ((working_bltBoardCertification.isExpired("Province",expiredDays))&&(working_bltBoardCertification.isExpiredCheck("Province"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltBoardCertification.isRequired("Province"))&&(!working_bltBoardCertification.isComplete("Province")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Province, District, State:&nbsp;</b><%=working_bltBoardCertification.getProvince()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltBoardCertification.isExpired("ZIP",expiredDays))&&(working_bltBoardCertification.isExpiredCheck("ZIP"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltBoardCertification.isRequired("ZIP"))&&(!working_bltBoardCertification.isComplete("ZIP")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>ZIP:&nbsp;</b><%=working_bltBoardCertification.getZIP()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltBoardCertification.isExpired("CountryID",expiredDays))&&(working_bltBoardCertification.isExpiredCheck("CountryID"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltBoardCertification.isRequired("CountryID"))&&(!working_bltBoardCertification.isComplete("CountryID")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Country:&nbsp;</b><jsp:include page="../generic/tCountryLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltBoardCertification.getCountryID()%>" /></jsp:include></p>

<%theClassF = "textBase";%>
<%if ((working_bltBoardCertification.isExpired("Phone",expiredDays))&&(working_bltBoardCertification.isExpiredCheck("Phone"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltBoardCertification.isRequired("Phone"))&&(!working_bltBoardCertification.isComplete("Phone")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Phone (XXX-XXX-XXXX):&nbsp;</b><%=working_bltBoardCertification.getPhone()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltBoardCertification.isExpired("Fax",expiredDays))&&(working_bltBoardCertification.isExpiredCheck("Fax"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltBoardCertification.isRequired("Fax"))&&(!working_bltBoardCertification.isComplete("Fax")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Fax (XXX-XXX-XXXX):&nbsp;</b><%=working_bltBoardCertification.getFax()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltBoardCertification.isExpired("BoardDateInitialCertified",expiredDays))&&(working_bltBoardCertification.isExpiredCheck("BoardDateInitialCertified"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltBoardCertification.isRequired("BoardDateInitialCertified"))&&(!working_bltBoardCertification.isComplete("BoardDateInitialCertified")) ){theClassF = "requiredFieldMain";}%>

                        <p class=<%=theClassF%> ><b>Initial Certification Date:&nbsp;</b><jsp:include page="../generic/DateTypeConvert_Main.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(working_bltBoardCertification.getBoardDateInitialCertified())%>" /></jsp:include></p>


<%theClassF = "textBase";%>
<%if ((working_bltBoardCertification.isExpired("BoardDateRecertified",expiredDays))&&(working_bltBoardCertification.isExpiredCheck("BoardDateRecertified"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltBoardCertification.isRequired("BoardDateRecertified"))&&(!working_bltBoardCertification.isComplete("BoardDateRecertified")) ){theClassF = "requiredFieldMain";}%>

                        <p class=<%=theClassF%> ><b>Date of Recertification:&nbsp;</b><jsp:include page="../generic/DateTypeConvert_Main.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(working_bltBoardCertification.getBoardDateRecertified())%>" /></jsp:include></p>


<%theClassF = "textBase";%>
<%if ((working_bltBoardCertification.isExpired("ExpirationDate",expiredDays))&&(working_bltBoardCertification.isExpiredCheck("ExpirationDate"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltBoardCertification.isRequired("ExpirationDate"))&&(!working_bltBoardCertification.isComplete("ExpirationDate")) ){theClassF = "requiredFieldMain";}%>

                        <p class=<%=theClassF%> ><b>Expiration Date:&nbsp;</b><jsp:include page="../generic/DateTypeConvert_Main.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(working_bltBoardCertification.getExpirationDate())%>" /></jsp:include></p>


<%theClassF = "textBase";%>
<%if ((working_bltBoardCertification.isExpired("DocumentNumber",expiredDays))&&(working_bltBoardCertification.isExpiredCheck("DocumentNumber"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltBoardCertification.isRequired("DocumentNumber"))&&(!working_bltBoardCertification.isComplete("DocumentNumber")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Certificate/Document Number:&nbsp;</b><%=working_bltBoardCertification.getDocumentNumber()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltBoardCertification.isExpired("IfNotCert",expiredDays))&&(working_bltBoardCertification.isExpiredCheck("IfNotCert"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltBoardCertification.isRequired("IfNotCert"))&&(!working_bltBoardCertification.isComplete("IfNotCert")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>If you are not certified, please explain here:&nbsp;</b><%=working_bltBoardCertification.getIfNotCert()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltBoardCertification.isExpired("CertEligible",expiredDays))&&(working_bltBoardCertification.isExpiredCheck("CertEligible"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltBoardCertification.isRequired("CertEligible"))&&(!working_bltBoardCertification.isComplete("CertEligible")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>If you are not certified, are you eligible to take this test?:&nbsp;</b><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltBoardCertification.getCertEligible()%>" /></jsp:include></p>

<%theClassF = "textBase";%>
<%if ((working_bltBoardCertification.isExpired("EligibleStartDate",expiredDays))&&(working_bltBoardCertification.isExpiredCheck("EligibleStartDate"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltBoardCertification.isRequired("EligibleStartDate"))&&(!working_bltBoardCertification.isComplete("EligibleStartDate")) ){theClassF = "requiredFieldMain";}%>

                        <p class=<%=theClassF%> ><b>If YES, please enter the date you were eligible:&nbsp;</b><jsp:include page="../generic/DateTypeConvert_Main.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(working_bltBoardCertification.getEligibleStartDate())%>" /></jsp:include></p>


<%theClassF = "textBase";%>
<%if ((working_bltBoardCertification.isExpired("EligibleExpirationDate",expiredDays))&&(working_bltBoardCertification.isExpiredCheck("EligibleExpirationDate"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltBoardCertification.isRequired("EligibleExpirationDate"))&&(!working_bltBoardCertification.isComplete("EligibleExpirationDate")) ){theClassF = "requiredFieldMain";}%>

                        <p class=<%=theClassF%> ><b>If YES, please enter the date your eligiblity expires:&nbsp;</b><jsp:include page="../generic/DateTypeConvert_Main.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(working_bltBoardCertification.getEligibleExpirationDate())%>" /></jsp:include></p>


<%theClassF = "textBase";%>
<%if ((working_bltBoardCertification.isExpired("CertPlanningToTake",expiredDays))&&(working_bltBoardCertification.isExpiredCheck("CertPlanningToTake"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltBoardCertification.isRequired("CertPlanningToTake"))&&(!working_bltBoardCertification.isComplete("CertPlanningToTake")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Are you planning on taking this test?:&nbsp;</b><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltBoardCertification.getCertPlanningToTake()%>" /></jsp:include></p>

<%theClassF = "textBase";%>
<%if ((working_bltBoardCertification.isExpired("CertPlanDate",expiredDays))&&(working_bltBoardCertification.isExpiredCheck("CertPlanDate"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltBoardCertification.isRequired("CertPlanDate"))&&(!working_bltBoardCertification.isComplete("CertPlanDate")) ){theClassF = "requiredFieldMain";}%>

                        <p class=<%=theClassF%> ><b>If so, when are you planning on taking your certification test?:&nbsp;</b><jsp:include page="../generic/DateTypeConvert_Main.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(working_bltBoardCertification.getCertPlanDate())%>" /></jsp:include></p>


<%theClassF = "textBase";%>
<%if ((working_bltBoardCertification.isExpired("DocuLinkID",expiredDays))&&(working_bltBoardCertification.isExpiredCheck("DocuLinkID"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltBoardCertification.isRequired("DocuLinkID"))&&(!working_bltBoardCertification.isComplete("DocuLinkID")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>DocuLink:&nbsp;</b><%=working_bltBoardCertification.getDocuLinkID()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltBoardCertification.isExpired("Comments",expiredDays))&&(working_bltBoardCertification.isExpiredCheck("Comments"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltBoardCertification.isRequired("Comments"))&&(!working_bltBoardCertification.isComplete("Comments")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Extra Comments:&nbsp;</b><%=working_bltBoardCertification.getComments()%></p>

        </td></tr></table></table><br>        <%
    }
    %>
    </table>
    <%
  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>

    </td></tr></table>


<%String bnIncludeFN = "ui_"+thePLCID+"\\bot-nav_PhysicianID.jsp";
%>
<jsp:include page="<%=bnIncludeFN%>" flush="true" ><jsp:param name="plcID" value="<%=thePLCID%>"/></jsp:include>
