<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.bltDocumentManagement,java.util.Vector,java.util.Enumeration,com.winstaff.ListElement, com.winstaff.PLCUtils, com.winstaff.SecurityCheck, com.winstaff.ConfigurationMessages,com.winstaff.bltBoardCertification,com.winstaff.bltBoardCertification_List" %>
<%/*
    filename: tBoardCertification_main_BoardCertification_PhysicianID.jsp
    Created on Oct/22/2003
    Type: 1-n main class file
    Created by: Scott Ellis
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>
    <%
    Integer iExpressMode = new Integer(1);
    if (pageControllerHash.containsKey("iExpressMode")) 
    {
        iExpressMode =    (Integer)pageControllerHash.get("iExpressMode");
    }
    %>

<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_PhysicianID.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>

    <table cellpadding=0 cellspacing=0 border=0 width=<%=MasterTableWidth%> >
    <tr><td width=10>&nbsp;</td><td>
    <%=ConfigurationMessages.getHTML("EXPRESSTopControl","tBoardCertification",iExpressMode)%>



<%
//initial declaration of list class and parentID
    Integer        iPhysicianID        =    null;
    boolean accessValid = false;
   Integer iSecurityCheck = SecurityCheck.CheckItem("PractitionerSection4", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {
    if (pageControllerHash.containsKey("iPhysicianID") ) 
    {
        iPhysicianID        =    (Integer)pageControllerHash.get("iPhysicianID");
        accessValid = true;
    }  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);
      pageControllerHash.put("sParentReturnPage","tBoardCertification_main_BoardCertification_PhysicianID_Expand.jsp");
    pageControllerHash.remove("iBoardCertificationID");
    session.setAttribute("pageControllerHash",pageControllerHash);

    bltBoardCertification_List        bltBoardCertification_List        =    new    bltBoardCertification_List(iPhysicianID);

//declaration of Enumeration
    bltBoardCertification        BoardCertification;
    ListElement         leCurrentElement;
    Enumeration eList = bltBoardCertification_List.elements();
    %>
        <%@ include file="tBoardCertification_main_BoardCertification_PhysicianID_instructions.jsp" %>

        <form action="tBoardCertification_main_BoardCertification_PhysicianID_Express_sub.jsp" name="tBoardCertification_main_BoardCertification_PhysicianID1" method="POST">
    <%
    int iExpress=0;
    while (eList.hasMoreElements()||iExpress<=ConfigurationMessages.getExpressItemCount("tBoardCertification"))
    {
       iExpress++;
         %>
         <table border="0" bordercolor="333333" cellpadding="0" class=tdHeaderAlt cellspacing="0" width="100%">
            <tr>
              <td width="50%">
                   <table border="0" bordercolor="333333" cellpadding="0" class=tdHeaderAlt cellspacing="0" width="100%">
                   <tr> 
                   	<td rowspan="2"><img src=express/left-corner.gif></td>
                   	<td width=100%><img width=100% height=2 src=express/small-line.gif></td>
                   	<td rowspan="2" align=right><img src=express/right-corner.gif></td>
                   </tr>
                     <tr> 
                       <td>
         <%

      boolean isNewRecord= false;
      if (eList.hasMoreElements())
      {
        leCurrentElement    = (ListElement) eList.nextElement();
        BoardCertification  = (bltBoardCertification) leCurrentElement.getObject();
      }
      else
      {
        BoardCertification  = new bltBoardCertification();
        isNewRecord= true;
      }
        BoardCertification.GroupSecurityInit(UserSecurityGroupID);
        String theClass = "tdHeader";
        %>
               <span class=<%=theClass%> ><b><%=ConfigurationMessages.getDataCategory("tBoardCertification")%> #<%=iExpress%></span>
                  </td></tr></table>
            </td></tr>
                 </table>
                 <table width="100%" border="1" cellspacing="0" bordercolor="#333333">
                  <tr>
                   <td>
                     <table width="100%" border="0" cellspacing="0">
                     <tr><td>
<%String theClassF = "textBase";%>
<%
if (isNewRecord)
{%>
<input type=hidden name=recordItemStatus_<%=iExpress%>="new">
<%}
else
{%>
<input type=hidden name=recordItemStatus_<%=iExpress%>="edit">
<%}

  {

        %>

          <%  theClass ="tdBase";%>
        <table width=100% border=1 bordercolor=#333333 cellpadding=3 cellspacing=0 class=tableBase>
         <tr><td class=tableColor>


            <table cellpadding=0 cellspacing=0 width=100%>
            <%
            if ( (BoardCertification.isRequired("Specialty",UserSecurityGroupID))&&(!BoardCertification.isComplete("Specialty")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((BoardCertification.isExpired("Specialty",expiredDays))&&(BoardCertification.isExpiredCheck("Specialty",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((BoardCertification.isWrite("Specialty",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Specialty&nbsp;</b></p></td><td valign=top><p><input maxlength="100" type=text size="80" name="Specialty_<%=iExpress%>" value="<%=BoardCertification.getSpecialty()%>">&nbsp;<a href="#" onClick="window.open('LI_tSpecialtyLI_Search.jsp?OFieldName=Specialty_<%=iExpress%>','BNPop','status=yes,scrollbars=yes,resizable=yes,width=400,height=300'); return false;"><img align=middle border=0 src=images/icon_lu.gif></a>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Specialty&amp;sTableName=tBoardCertification&amp;sRefID=<%=BoardCertification.getBoardCertificationID()%>&amp;sFieldNameDisp=<%=BoardCertification.getEnglish("Specialty")%>&amp;sTableNameDisp=tBoardCertification','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((BoardCertification.isRead("Specialty",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Specialty&nbsp;</b></p></td><td valign=top><p><%=BoardCertification.getSpecialty()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Specialty&amp;sTableName=tBoardCertification&amp;sRefID=<%=BoardCertification.getBoardCertificationID()%>&amp;sFieldNameDisp=<%=BoardCertification.getEnglish("Specialty")%>&amp;sTableNameDisp=tBoardCertification','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (BoardCertification.isRequired("SubSpecialty",UserSecurityGroupID))&&(!BoardCertification.isComplete("SubSpecialty")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((BoardCertification.isExpired("SubSpecialty",expiredDays))&&(BoardCertification.isExpiredCheck("SubSpecialty",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((BoardCertification.isWrite("SubSpecialty",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Sub-specialty related to above specialty&nbsp;</b></p></td><td valign=top><p><input maxlength="100" type=text size="80" name="SubSpecialty_<%=iExpress%>" value="<%=BoardCertification.getSubSpecialty()%>">&nbsp;<a href="#" onClick="window.open('LI_tSpecialtyLI_Search.jsp?OFieldName=SubSpecialty_<%=iExpress%>','BNPop','status=yes,scrollbars=yes,resizable=yes,width=400,height=300'); return false;"><img align=middle border=0 src=images/icon_lu.gif></a>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SubSpecialty&amp;sTableName=tBoardCertification&amp;sRefID=<%=BoardCertification.getBoardCertificationID()%>&amp;sFieldNameDisp=<%=BoardCertification.getEnglish("SubSpecialty")%>&amp;sTableNameDisp=tBoardCertification','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((BoardCertification.isRead("SubSpecialty",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Sub-specialty related to above specialty&nbsp;</b></p></td><td valign=top><p><%=BoardCertification.getSubSpecialty()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SubSpecialty&amp;sTableName=tBoardCertification&amp;sRefID=<%=BoardCertification.getBoardCertificationID()%>&amp;sFieldNameDisp=<%=BoardCertification.getEnglish("SubSpecialty")%>&amp;sTableNameDisp=tBoardCertification','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (BoardCertification.isRequired("IsListed",UserSecurityGroupID))&&(!BoardCertification.isComplete("IsListed")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((BoardCertification.isExpired("IsListed",expiredDays))&&(BoardCertification.isExpiredCheck("IsListed",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                     <input type=hidden name="IsListed_<%=iExpress%>" value="<%=BoardCertification.getIsListed()%>">

            <%
            if ( (BoardCertification.isRequired("IsPrimary",UserSecurityGroupID))&&(!BoardCertification.isComplete("IsPrimary")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((BoardCertification.isExpired("IsPrimary",expiredDays))&&(BoardCertification.isExpiredCheck("IsPrimary",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((BoardCertification.isWrite("IsPrimary",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Is this your primary specialty?&nbsp;</b></p></td><td valign=top><p><select   name="IsPrimary_<%=iExpress%>" ><jsp:include page="../generic/tYesNoLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=BoardCertification.getIsPrimary()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=IsPrimary&amp;sTableName=tBoardCertification&amp;sRefID=<%=BoardCertification.getBoardCertificationID()%>&amp;sFieldNameDisp=<%=BoardCertification.getEnglish("IsPrimary")%>&amp;sTableNameDisp=tBoardCertification','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((BoardCertification.isRead("IsPrimary",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Is this your primary specialty?&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=BoardCertification.getIsPrimary()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=IsPrimary&amp;sTableName=tBoardCertification&amp;sRefID=<%=BoardCertification.getBoardCertificationID()%>&amp;sFieldNameDisp=<%=BoardCertification.getEnglish("IsPrimary")%>&amp;sTableNameDisp=tBoardCertification','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (BoardCertification.isRequired("SpecialtyBoardCertified",UserSecurityGroupID))&&(!BoardCertification.isComplete("SpecialtyBoardCertified")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((BoardCertification.isExpired("SpecialtyBoardCertified",expiredDays))&&(BoardCertification.isExpiredCheck("SpecialtyBoardCertified",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((BoardCertification.isWrite("SpecialtyBoardCertified",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Specialty Status&nbsp;</b></p></td><td valign=top><p><select   name="SpecialtyBoardCertified_<%=iExpress%>" ><jsp:include page="../generic/tSpecialtyStatusLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=BoardCertification.getSpecialtyBoardCertified()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SpecialtyBoardCertified&amp;sTableName=tBoardCertification&amp;sRefID=<%=BoardCertification.getBoardCertificationID()%>&amp;sFieldNameDisp=<%=BoardCertification.getEnglish("SpecialtyBoardCertified")%>&amp;sTableNameDisp=tBoardCertification','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((BoardCertification.isRead("SpecialtyBoardCertified",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Specialty Status&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tSpecialtyStatusLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=BoardCertification.getSpecialtyBoardCertified()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SpecialtyBoardCertified&amp;sTableName=tBoardCertification&amp;sRefID=<%=BoardCertification.getBoardCertificationID()%>&amp;sFieldNameDisp=<%=BoardCertification.getEnglish("SpecialtyBoardCertified")%>&amp;sTableNameDisp=tBoardCertification','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (BoardCertification.isRequired("BoardName",UserSecurityGroupID))&&(!BoardCertification.isComplete("BoardName")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((BoardCertification.isExpired("BoardName",expiredDays))&&(BoardCertification.isExpiredCheck("BoardName",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((BoardCertification.isWrite("BoardName",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Board Name&nbsp;</b></p></td><td valign=top><p><input maxlength="100" type=text size="80" name="BoardName_<%=iExpress%>" value="<%=BoardCertification.getBoardName()%>">&nbsp;<a href="#" onClick="window.open('LI_tBoardNameLI_Search.jsp?OFieldName=BoardName_<%=iExpress%>','BNPop','status=yes,scrollbars=yes,resizable=yes,width=400,height=300'); return false;"><img align=middle border=0 src=images/icon_lu.gif></a>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=BoardName&amp;sTableName=tBoardCertification&amp;sRefID=<%=BoardCertification.getBoardCertificationID()%>&amp;sFieldNameDisp=<%=BoardCertification.getEnglish("BoardName")%>&amp;sTableNameDisp=tBoardCertification','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((BoardCertification.isRead("BoardName",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Board Name&nbsp;</b></p></td><td valign=top><p><%=BoardCertification.getBoardName()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=BoardName&amp;sTableName=tBoardCertification&amp;sRefID=<%=BoardCertification.getBoardCertificationID()%>&amp;sFieldNameDisp=<%=BoardCertification.getEnglish("BoardName")%>&amp;sTableNameDisp=tBoardCertification','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (BoardCertification.isRequired("ContactName",UserSecurityGroupID))&&(!BoardCertification.isComplete("ContactName")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((BoardCertification.isExpired("ContactName",expiredDays))&&(BoardCertification.isExpiredCheck("ContactName",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                     <input type=hidden name="ContactName_<%=iExpress%>" value="<%=BoardCertification.getContactName()%>">

            <%
            if ( (BoardCertification.isRequired("ContactEmail",UserSecurityGroupID))&&(!BoardCertification.isComplete("ContactEmail")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((BoardCertification.isExpired("ContactEmail",expiredDays))&&(BoardCertification.isExpiredCheck("ContactEmail",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                     <input type=hidden name="ContactEmail_<%=iExpress%>" value="<%=BoardCertification.getContactEmail()%>">

            <%
            if ( (BoardCertification.isRequired("Address1",UserSecurityGroupID))&&(!BoardCertification.isComplete("Address1")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((BoardCertification.isExpired("Address1",expiredDays))&&(BoardCertification.isExpiredCheck("Address1",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                     <input type=hidden name="Address1_<%=iExpress%>" value="<%=BoardCertification.getAddress1()%>">

            <%
            if ( (BoardCertification.isRequired("Address2",UserSecurityGroupID))&&(!BoardCertification.isComplete("Address2")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((BoardCertification.isExpired("Address2",expiredDays))&&(BoardCertification.isExpiredCheck("Address2",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                     <input type=hidden name="Address2_<%=iExpress%>" value="<%=BoardCertification.getAddress2()%>">

            <%
            if ( (BoardCertification.isRequired("City",UserSecurityGroupID))&&(!BoardCertification.isComplete("City")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((BoardCertification.isExpired("City",expiredDays))&&(BoardCertification.isExpiredCheck("City",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                     <input type=hidden name="City_<%=iExpress%>" value="<%=BoardCertification.getCity()%>">

            <%
            if ( (BoardCertification.isRequired("StateID",UserSecurityGroupID))&&(!BoardCertification.isComplete("StateID")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((BoardCertification.isExpired("StateID",expiredDays))&&(BoardCertification.isExpiredCheck("StateID",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                     <input type=hidden name="StateID_<%=iExpress%>" value="<%=BoardCertification.getStateID()%>">

            <%
            if ( (BoardCertification.isRequired("Province",UserSecurityGroupID))&&(!BoardCertification.isComplete("Province")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((BoardCertification.isExpired("Province",expiredDays))&&(BoardCertification.isExpiredCheck("Province",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                     <input type=hidden name="Province_<%=iExpress%>" value="<%=BoardCertification.getProvince()%>">

            <%
            if ( (BoardCertification.isRequired("ZIP",UserSecurityGroupID))&&(!BoardCertification.isComplete("ZIP")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((BoardCertification.isExpired("ZIP",expiredDays))&&(BoardCertification.isExpiredCheck("ZIP",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                     <input type=hidden name="ZIP_<%=iExpress%>" value="<%=BoardCertification.getZIP()%>">

            <%
            if ( (BoardCertification.isRequired("CountryID",UserSecurityGroupID))&&(!BoardCertification.isComplete("CountryID")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((BoardCertification.isExpired("CountryID",expiredDays))&&(BoardCertification.isExpiredCheck("CountryID",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                     <input type=hidden name="CountryID_<%=iExpress%>" value="<%=BoardCertification.getCountryID()%>">

            <%
            if ( (BoardCertification.isRequired("Phone",UserSecurityGroupID))&&(!BoardCertification.isComplete("Phone")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((BoardCertification.isExpired("Phone",expiredDays))&&(BoardCertification.isExpiredCheck("Phone",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                     <input type=hidden name="Phone_<%=iExpress%>" value="<%=BoardCertification.getPhone()%>">

            <%
            if ( (BoardCertification.isRequired("Fax",UserSecurityGroupID))&&(!BoardCertification.isComplete("Fax")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((BoardCertification.isExpired("Fax",expiredDays))&&(BoardCertification.isExpiredCheck("Fax",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                     <input type=hidden name="Fax_<%=iExpress%>" value="<%=BoardCertification.getFax()%>">

            <%
            if ( (BoardCertification.isRequired("BoardDateInitialCertified",UserSecurityGroupID))&&(!BoardCertification.isComplete("BoardDateInitialCertified")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((BoardCertification.isExpired("BoardDateInitialCertified",expiredDays))&&(BoardCertification.isExpiredCheck("BoardDateInitialCertified",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>

            <%
            if ((BoardCertification.isWrite("BoardDateInitialCertified",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Initial Certification Date&nbsp;(mm/dd/yyyy):&nbsp;</b></p></td><td valign=top><p><input maxlength=20  type=text size="80" name="BoardDateInitialCertified_<%=iExpress%>" value='<jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(BoardCertification.getBoardDateInitialCertified())%>" /></jsp:include>' >&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=BoardDateInitialCertified&amp;sTableName=tBoardCertification&amp;sRefID=<%=BoardCertification.getBoardCertificationID()%>&amp;sFieldNameDisp=<%=BoardCertification.getEnglish("BoardDateInitialCertified")%>&amp;sTableNameDisp=tBoardCertification','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((BoardCertification.isRead("BoardDateInitialCertified",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Initial Certification Date&nbsp;(mm/dd/yyyy):&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(BoardCertification.getBoardDateInitialCertified())%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=BoardDateInitialCertified&amp;sTableName=tBoardCertification&amp;sRefID=<%=BoardCertification.getBoardCertificationID()%>&amp;sFieldNameDisp=<%=BoardCertification.getEnglish("BoardDateInitialCertified")%>&amp;sTableNameDisp=tBoardCertification','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (BoardCertification.isRequired("BoardDateRecertified",UserSecurityGroupID))&&(!BoardCertification.isComplete("BoardDateRecertified")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((BoardCertification.isExpired("BoardDateRecertified",expiredDays))&&(BoardCertification.isExpiredCheck("BoardDateRecertified",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>

            <%
            if ((BoardCertification.isWrite("BoardDateRecertified",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Date of Recertification&nbsp;(mm/dd/yyyy):&nbsp;</b></p></td><td valign=top><p><input maxlength=20  type=text size="80" name="BoardDateRecertified_<%=iExpress%>" value='<jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(BoardCertification.getBoardDateRecertified())%>" /></jsp:include>' >&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=BoardDateRecertified&amp;sTableName=tBoardCertification&amp;sRefID=<%=BoardCertification.getBoardCertificationID()%>&amp;sFieldNameDisp=<%=BoardCertification.getEnglish("BoardDateRecertified")%>&amp;sTableNameDisp=tBoardCertification','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((BoardCertification.isRead("BoardDateRecertified",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Date of Recertification&nbsp;(mm/dd/yyyy):&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(BoardCertification.getBoardDateRecertified())%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=BoardDateRecertified&amp;sTableName=tBoardCertification&amp;sRefID=<%=BoardCertification.getBoardCertificationID()%>&amp;sFieldNameDisp=<%=BoardCertification.getEnglish("BoardDateRecertified")%>&amp;sTableNameDisp=tBoardCertification','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (BoardCertification.isRequired("ExpirationDate",UserSecurityGroupID))&&(!BoardCertification.isComplete("ExpirationDate")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((BoardCertification.isExpired("ExpirationDate",expiredDays))&&(BoardCertification.isExpiredCheck("ExpirationDate",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>

            <%
            if ((BoardCertification.isWrite("ExpirationDate",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Expiration Date&nbsp;(mm/dd/yyyy):&nbsp;</b></p></td><td valign=top><p><input maxlength=20  type=text size="80" name="ExpirationDate_<%=iExpress%>" value='<jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(BoardCertification.getExpirationDate())%>" /></jsp:include>' >&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ExpirationDate&amp;sTableName=tBoardCertification&amp;sRefID=<%=BoardCertification.getBoardCertificationID()%>&amp;sFieldNameDisp=<%=BoardCertification.getEnglish("ExpirationDate")%>&amp;sTableNameDisp=tBoardCertification','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((BoardCertification.isRead("ExpirationDate",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Expiration Date&nbsp;(mm/dd/yyyy):&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(BoardCertification.getExpirationDate())%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ExpirationDate&amp;sTableName=tBoardCertification&amp;sRefID=<%=BoardCertification.getBoardCertificationID()%>&amp;sFieldNameDisp=<%=BoardCertification.getEnglish("ExpirationDate")%>&amp;sTableNameDisp=tBoardCertification','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (BoardCertification.isRequired("DocumentNumber",UserSecurityGroupID))&&(!BoardCertification.isComplete("DocumentNumber")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((BoardCertification.isExpired("DocumentNumber",expiredDays))&&(BoardCertification.isExpiredCheck("DocumentNumber",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((BoardCertification.isWrite("DocumentNumber",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Certificate/Document Number&nbsp;</b></p></td><td valign=top><p><input maxlength="100" type=text size="80" name="DocumentNumber_<%=iExpress%>" value="<%=BoardCertification.getDocumentNumber()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=DocumentNumber&amp;sTableName=tBoardCertification&amp;sRefID=<%=BoardCertification.getBoardCertificationID()%>&amp;sFieldNameDisp=<%=BoardCertification.getEnglish("DocumentNumber")%>&amp;sTableNameDisp=tBoardCertification','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((BoardCertification.isRead("DocumentNumber",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Certificate/Document Number&nbsp;</b></p></td><td valign=top><p><%=BoardCertification.getDocumentNumber()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=DocumentNumber&amp;sTableName=tBoardCertification&amp;sRefID=<%=BoardCertification.getBoardCertificationID()%>&amp;sFieldNameDisp=<%=BoardCertification.getEnglish("DocumentNumber")%>&amp;sTableNameDisp=tBoardCertification','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (BoardCertification.isRequired("IfNotCert",UserSecurityGroupID))&&(!BoardCertification.isComplete("IfNotCert")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((BoardCertification.isExpired("IfNotCert",expiredDays))&&(BoardCertification.isExpiredCheck("IfNotCert",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                     <input type=hidden name="IfNotCert_<%=iExpress%>" value="<%=BoardCertification.getIfNotCert()%>">

            <%
            if ( (BoardCertification.isRequired("CertEligible",UserSecurityGroupID))&&(!BoardCertification.isComplete("CertEligible")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((BoardCertification.isExpired("CertEligible",expiredDays))&&(BoardCertification.isExpiredCheck("CertEligible",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((BoardCertification.isWrite("CertEligible",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>If you are not certified, are you eligible to take this test?&nbsp;</b></p></td><td valign=top><p><select   name="CertEligible_<%=iExpress%>" ><jsp:include page="../generic/tYesNoLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=BoardCertification.getCertEligible()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CertEligible&amp;sTableName=tBoardCertification&amp;sRefID=<%=BoardCertification.getBoardCertificationID()%>&amp;sFieldNameDisp=<%=BoardCertification.getEnglish("CertEligible")%>&amp;sTableNameDisp=tBoardCertification','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((BoardCertification.isRead("CertEligible",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>If you are not certified, are you eligible to take this test?&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=BoardCertification.getCertEligible()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CertEligible&amp;sTableName=tBoardCertification&amp;sRefID=<%=BoardCertification.getBoardCertificationID()%>&amp;sFieldNameDisp=<%=BoardCertification.getEnglish("CertEligible")%>&amp;sTableNameDisp=tBoardCertification','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (BoardCertification.isRequired("EligibleStartDate",UserSecurityGroupID))&&(!BoardCertification.isComplete("EligibleStartDate")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((BoardCertification.isExpired("EligibleStartDate",expiredDays))&&(BoardCertification.isExpiredCheck("EligibleStartDate",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>

            <%
            if ((BoardCertification.isWrite("EligibleStartDate",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>If YES, please enter the date you were eligible&nbsp;(mm/dd/yyyy):&nbsp;</b></p></td><td valign=top><p><input maxlength=20  type=text size="80" name="EligibleStartDate_<%=iExpress%>" value='<jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(BoardCertification.getEligibleStartDate())%>" /></jsp:include>' >&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=EligibleStartDate&amp;sTableName=tBoardCertification&amp;sRefID=<%=BoardCertification.getBoardCertificationID()%>&amp;sFieldNameDisp=<%=BoardCertification.getEnglish("EligibleStartDate")%>&amp;sTableNameDisp=tBoardCertification','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((BoardCertification.isRead("EligibleStartDate",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>If YES, please enter the date you were eligible&nbsp;(mm/dd/yyyy):&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(BoardCertification.getEligibleStartDate())%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=EligibleStartDate&amp;sTableName=tBoardCertification&amp;sRefID=<%=BoardCertification.getBoardCertificationID()%>&amp;sFieldNameDisp=<%=BoardCertification.getEnglish("EligibleStartDate")%>&amp;sTableNameDisp=tBoardCertification','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (BoardCertification.isRequired("EligibleExpirationDate",UserSecurityGroupID))&&(!BoardCertification.isComplete("EligibleExpirationDate")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((BoardCertification.isExpired("EligibleExpirationDate",expiredDays))&&(BoardCertification.isExpiredCheck("EligibleExpirationDate",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>

            <%
            if ((BoardCertification.isWrite("EligibleExpirationDate",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>If YES, please enter the date your eligiblity expires&nbsp;(mm/dd/yyyy):&nbsp;</b></p></td><td valign=top><p><input maxlength=20  type=text size="80" name="EligibleExpirationDate_<%=iExpress%>" value='<jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(BoardCertification.getEligibleExpirationDate())%>" /></jsp:include>' >&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=EligibleExpirationDate&amp;sTableName=tBoardCertification&amp;sRefID=<%=BoardCertification.getBoardCertificationID()%>&amp;sFieldNameDisp=<%=BoardCertification.getEnglish("EligibleExpirationDate")%>&amp;sTableNameDisp=tBoardCertification','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((BoardCertification.isRead("EligibleExpirationDate",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>If YES, please enter the date your eligiblity expires&nbsp;(mm/dd/yyyy):&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(BoardCertification.getEligibleExpirationDate())%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=EligibleExpirationDate&amp;sTableName=tBoardCertification&amp;sRefID=<%=BoardCertification.getBoardCertificationID()%>&amp;sFieldNameDisp=<%=BoardCertification.getEnglish("EligibleExpirationDate")%>&amp;sTableNameDisp=tBoardCertification','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (BoardCertification.isRequired("CertPlanningToTake",UserSecurityGroupID))&&(!BoardCertification.isComplete("CertPlanningToTake")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((BoardCertification.isExpired("CertPlanningToTake",expiredDays))&&(BoardCertification.isExpiredCheck("CertPlanningToTake",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((BoardCertification.isWrite("CertPlanningToTake",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Are you planning on taking this test?&nbsp;</b></p></td><td valign=top><p><select   name="CertPlanningToTake_<%=iExpress%>" ><jsp:include page="../generic/tYesNoLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=BoardCertification.getCertPlanningToTake()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CertPlanningToTake&amp;sTableName=tBoardCertification&amp;sRefID=<%=BoardCertification.getBoardCertificationID()%>&amp;sFieldNameDisp=<%=BoardCertification.getEnglish("CertPlanningToTake")%>&amp;sTableNameDisp=tBoardCertification','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((BoardCertification.isRead("CertPlanningToTake",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Are you planning on taking this test?&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=BoardCertification.getCertPlanningToTake()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CertPlanningToTake&amp;sTableName=tBoardCertification&amp;sRefID=<%=BoardCertification.getBoardCertificationID()%>&amp;sFieldNameDisp=<%=BoardCertification.getEnglish("CertPlanningToTake")%>&amp;sTableNameDisp=tBoardCertification','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (BoardCertification.isRequired("CertPlanDate",UserSecurityGroupID))&&(!BoardCertification.isComplete("CertPlanDate")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((BoardCertification.isExpired("CertPlanDate",expiredDays))&&(BoardCertification.isExpiredCheck("CertPlanDate",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>

            <%
            if ((BoardCertification.isWrite("CertPlanDate",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>If so, when are you planning on taking your certification test?&nbsp;(mm/dd/yyyy):&nbsp;</b></p></td><td valign=top><p><input maxlength=20  type=text size="80" name="CertPlanDate_<%=iExpress%>" value='<jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(BoardCertification.getCertPlanDate())%>" /></jsp:include>' >&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CertPlanDate&amp;sTableName=tBoardCertification&amp;sRefID=<%=BoardCertification.getBoardCertificationID()%>&amp;sFieldNameDisp=<%=BoardCertification.getEnglish("CertPlanDate")%>&amp;sTableNameDisp=tBoardCertification','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((BoardCertification.isRead("CertPlanDate",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>If so, when are you planning on taking your certification test?&nbsp;(mm/dd/yyyy):&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(BoardCertification.getCertPlanDate())%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CertPlanDate&amp;sTableName=tBoardCertification&amp;sRefID=<%=BoardCertification.getBoardCertificationID()%>&amp;sFieldNameDisp=<%=BoardCertification.getEnglish("CertPlanDate")%>&amp;sTableNameDisp=tBoardCertification','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (BoardCertification.isRequired("DocuLinkID",UserSecurityGroupID))&&(!BoardCertification.isComplete("DocuLinkID")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((BoardCertification.isExpired("DocuLinkID",expiredDays))&&(BoardCertification.isExpiredCheck("DocuLinkID",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                     <input type=hidden name="DocuLinkID_<%=iExpress%>" value="<%=BoardCertification.getDocuLinkID()%>">

            <%
            if ( (BoardCertification.isRequired("Comments",UserSecurityGroupID))&&(!BoardCertification.isComplete("Comments")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((BoardCertification.isExpired("Comments",expiredDays))&&(BoardCertification.isExpiredCheck("Comments",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                     <input type=hidden name="Comments_<%=iExpress%>" value="<%=BoardCertification.getComments()%>">


            <tr><td width=40%>&nbsp;</td><td width=60%>&nbsp;</td></tr>

            <input type=hidden name=routePageReference value="sParentReturnPage">

        </td></tr></table>
        </td></tr></table>
        <%
  }%>

        </td></tr></table></table><br>        <%
    }
    %>
        <input <%=HTMLFormStyleButton%> onClick="document.forms[0].nextPage.value='<%=ConfigurationMessages.getExpressLinkRaw("tBoardCertification","next",iExpressMode)%>'" type=Submit value="Continue" name=Submit>
        <input type=hidden name=nextPage value="">
        </form>

    </table>
    <%
  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>

    </td></tr></table>


<%String bnIncludeFN = "ui_"+thePLCID+"\\bot-nav_PhysicianID.jsp";
%>
<jsp:include page="<%=bnIncludeFN%>" flush="true" ><jsp:param name="plcID" value="<%=thePLCID%>"/></jsp:include>
