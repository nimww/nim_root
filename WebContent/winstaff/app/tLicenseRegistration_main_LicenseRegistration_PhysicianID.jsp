<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.ConfigurationInformation,com.winstaff.bltDocumentManagement,java.util.Vector,java.util.Enumeration,com.winstaff.ListElement, com.winstaff.PLCUtils, com.winstaff.SecurityCheck, com.winstaff.ConfigurationMessages,com.winstaff.bltLicenseRegistration,com.winstaff.bltLicenseRegistration_List" %>
<%/*
    filename: tLicenseRegistration_main_LicenseRegistration_PhysicianID.jsp
    Created on Mar/21/2003
    Type: 1-n main class file
    Created by: Scott Ellis
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>

<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_PhysicianID.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>

    <table cellpadding=0 cellspacing=0 border=0 width=<%=MasterTableWidth%> >
    <tr><td width=10>&nbsp;</td><td>
    <%=ConfigurationMessages.getHTML("INTERVIEWTopControl","tLicenseRegistration")%>


<%
//initial declaration of list class and parentID
    Integer        iPhysicianID        =    null;
    boolean accessValid = false;
   Integer iSecurityCheck = SecurityCheck.CheckItem("PractitionerSection3", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {
    if (pageControllerHash.containsKey("iPhysicianID")) 
    {
        iPhysicianID        =    (Integer)pageControllerHash.get("iPhysicianID");
        accessValid = true;
    }
  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);
      pageControllerHash.put("sParentReturnPage","tLicenseRegistration_main_LicenseRegistration_PhysicianID.jsp");
    pageControllerHash.remove("iLicenseRegistrationID");
    pageControllerHash.put("sINTNext","tLicenseRegistration_main_LicenseRegistration_PhysicianID_form_create.jsp?EDIT=new&KM=p&INTNext=yes");
    session.setAttribute("pageControllerHash",pageControllerHash);

    bltLicenseRegistration_List        bltLicenseRegistration_List        =    new    bltLicenseRegistration_List(iPhysicianID,"","isCurrent");

//declaration of Enumeration
    bltLicenseRegistration        working_bltLicenseRegistration;
    ListElement         leCurrentElement;
    Enumeration eList = bltLicenseRegistration_List.elements();
    %>
        <%@ include file="tLicenseRegistration_main_LicenseRegistration_PhysicianID_instructions.jsp" %>

        <%if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {%>
        <a class=linkBase href = "tLicenseRegistration_main_LicenseRegistration_PhysicianID_form_create.jsp?EDIT=new&KM=p&INTNext=yes"><img border=0 src="ui_<%=thePLCID%>/icons/create_PhysicianID.gif"></a>
        <%}%>
         <table border="1" bordercolor="CCCCCC" cellpadding="3" class=tdBase cellspacing="0" width="100%">
    <%
    int altCnt = 0;
    if (eList.hasMoreElements())
    {
     while (eList.hasMoreElements())
     {

        altCnt++;
        String theClass = "tdBase";
        if (altCnt%2!=0)
        {
            theClass = "tdBaseAlt";
        }
        leCurrentElement    = (ListElement) eList.nextElement();
        working_bltLicenseRegistration  = (bltLicenseRegistration) leCurrentElement.getObject();
        working_bltLicenseRegistration.GroupSecurityInit(UserSecurityGroupID);
        if (!working_bltLicenseRegistration.isComplete())
        {
            theClass = "incompleteItem";
        %>
                <tr class=incompleteItem><td><b>Not Complete</b><br>
        <%
        }
        else
        {
        %>
        <tr class=<%=theClass%> ><td> 
        <%
        }
        %>

              <b>Item ID:&nbsp;</b><%=working_bltLicenseRegistration.getLicenseRegistrationID()%></td>
<%String theClassF = "textBase";%>

<%theClassF = "textBase";%>
<%if ((working_bltLicenseRegistration.isExpired("LicenseType",expiredDays))&&(working_bltLicenseRegistration.isExpiredCheck("LicenseType"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltLicenseRegistration.isRequired("LicenseType"))&&(!working_bltLicenseRegistration.isComplete("LicenseType")) ){theClassF = "requiredFieldMain";}%>
            <td><p class=<%=theClassF%> ><b>Type:&nbsp;</b><jsp:include page="../generic/tLicenseTypeLIShort_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltLicenseRegistration.getLicenseType()%>" /></jsp:include>
<%
if (working_bltLicenseRegistration.getLicenseType().intValue()==3)
{
}
else
{
%>
(<jsp:include page="../generic/tStateLIShort_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltLicenseRegistration.getStateID()%>" /></jsp:include>)
<%
}
%>
<br>
              <b>Document #</b>: <%=working_bltLicenseRegistration.getLicenseDocumentNumber()%>	
            </p>
          </td>

            <td><p class=<%=theClassF%> ><b>Status:&nbsp;</b>
<%
if (working_bltLicenseRegistration.getIsCurrent().intValue()==1&&working_bltLicenseRegistration.isExpired("ExpirationDate",0))
{
%>
<b>Active <span class=requiredField> - Expired</span></b>
<%
}
else if (working_bltLicenseRegistration.getIsCurrent().intValue()==1)
{
%>
<b><font color=#006600>Active - Current</font></b>
<%
}
else
{
%>
<i>Inactive</i>
<%
}
%>			
			
			</p></td>



            <td > 
        <a class=linkBase href = "tLicenseRegistration_main_LicenseRegistration_PhysicianID_form_authorize.jsp?EDIT=edit&EDITID=<%=working_bltLicenseRegistration.getLicenseRegistrationID()%>&KM=p"><img border=0 src="ui_<%=thePLCID%>/icons/edit_PhysicianID.gif"></a>

        <%if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {%>
        <a class=linkBase  onClick="return confirmDelete()"  href = "tLicenseRegistration_main_LicenseRegistration_PhysicianID_form_authorize.jsp?EDIT=del&EDITID=<%=working_bltLicenseRegistration.getLicenseRegistrationID()%>&KM=p"><img border=0 src="ui_<%=thePLCID%>/icons/delete_PhysicianID.gif"></a>
        <br>
        <% 
            if (working_bltLicenseRegistration.getDocuLinkID().intValue()>0)
            {
              bltDocumentManagement myDoc = new bltDocumentManagement(working_bltLicenseRegistration.getDocuLinkID());
              if (!myDoc.getDocumentFileName().equalsIgnoreCase(""))
              {
              %>
                      <a target=_blank href="tLicenseRegistration_main_LicenseRegistration_PhysicianID_form_authorize.jsp?EDIT=print&EDITID=<%=working_bltLicenseRegistration.getLicenseRegistrationID()%>&KM=p">View Document</a>

              <%
              }
              else
              {
              %>

                      <a class=linkBase target=_blank href = "#" onClick="window.open('tDocumentManagement_main_DocumentManagement_PhysicianID_form_authorize.jsp?EDIT=faxCover&amp;EDITID=<%=working_bltLicenseRegistration.getDocuLinkID()%>','DocPDF','toolbar=yes,location=yes,status=yes,menubar=yes,scrollbars=yes,resizable=yes,width=500,height=500');return false;" >Print Cover Sheet</a>

              <%
              }
            }
            else
            {
             if (working_bltLicenseRegistration.isComplete())
             {
               %>
                      <a target=_blank class=linkBase href = "tLicenseRegistration_ModifyDocument.jsp?EDITID=<%=working_bltLicenseRegistration.getLicenseRegistrationID()%>&EDIT=CREATE&dType=tLicenseRegistration">Create Document</a>
               <%
             }
             else
             {
               %>
                      <span class=tdBase>You must complete this item before you can attach a document</span>
               <%
             }
            }
            %>
        <% }%>
                  </td></tr>
        <%
    }//end while
       }//end of if
       else 
       {
           %>
           <tr><td><b>Please click the "create" to add <%=ConfigurationMessages.getDataCategory("tLicenseRegistration")%> information or click 'Continue' to go to the next section.</b>
           <script language=javascript>
           if (confirm("<%=ConfigurationMessages.getInterviewMessage("INTERVIEWNoElements","tLicenseRegistration")%>"))
           {
               document.location="tLicenseRegistration_main_LicenseRegistration_PhysicianID_form_create.jsp?EDIT=new&KM=p&INTNext=yes"; 
           }
           else
           {

           }
           </script>
           </td></tr>
           <%
       }
    %>

    </table>
    <%
  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>

    </td></tr></table><br>


<%String bnIncludeFN = "ui_"+thePLCID+"\\bot-nav_PhysicianID.jsp";
%>
<jsp:include page="<%=bnIncludeFN%>" flush="true" ><jsp:param name="plcID" value="<%=thePLCID%>"/></jsp:include>
