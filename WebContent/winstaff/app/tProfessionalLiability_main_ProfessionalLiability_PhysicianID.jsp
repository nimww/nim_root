<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.bltCoverageTypeLI,com.winstaff.ConfigurationInformation,com.winstaff.bltDocumentManagement,java.util.Vector,java.util.Enumeration,com.winstaff.ListElement, com.winstaff.PLCUtils, com.winstaff.SecurityCheck, com.winstaff.ConfigurationMessages,com.winstaff.bltProfessionalLiability,com.winstaff.bltProfessionalLiability_List" %>
<%/*
    filename: tProfessionalLiability_main_ProfessionalLiability_PhysicianID.jsp
    Created on Mar/21/2003
    Type: 1-n main class file
    Created by: Scott Ellis
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>

<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_PhysicianID.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>

    <table cellpadding=0 cellspacing=0 border=0 width=<%=MasterTableWidth%> >
    <tr><td width=10>&nbsp;</td><td>
    <%=ConfigurationMessages.getHTML("INTERVIEWTopControl","tProfessionalLiability")%>



<%
//initial declaration of list class and parentID
    Integer        iPhysicianID        =    null;
    boolean accessValid = false;
   Integer iSecurityCheck = SecurityCheck.CheckItem("PractitionerSection9", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {
    if (pageControllerHash.containsKey("iPhysicianID")) 
    {
        iPhysicianID        =    (Integer)pageControllerHash.get("iPhysicianID");
        accessValid = true;
    }
  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);
      pageControllerHash.put("sParentReturnPage","tProfessionalLiability_main_ProfessionalLiability_PhysicianID.jsp");
    pageControllerHash.remove("iProfessionalLiabilityID");
    pageControllerHash.put("sINTNext","tProfessionalLiability_main_ProfessionalLiability_PhysicianID_form_create.jsp?EDIT=new&KM=p&INTNext=yes");
    session.setAttribute("pageControllerHash",pageControllerHash);

    bltProfessionalLiability_List        bltProfessionalLiability_List        =    new    bltProfessionalLiability_List(iPhysicianID,"","InsuredToDate Desc");

//declaration of Enumeration
    bltProfessionalLiability        working_bltProfessionalLiability;
    ListElement         leCurrentElement;
    Enumeration eList = bltProfessionalLiability_List.elements();
    %>
        <%@ include file="tProfessionalLiability_main_ProfessionalLiability_PhysicianID_instructions.jsp" %>

        <%if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {%>
        <a class=linkBase href = "tProfessionalLiability_main_ProfessionalLiability_PhysicianID_form_create.jsp?EDIT=new&KM=p&INTNext=yes"><img border=0 src="ui_<%=thePLCID%>/icons/create_PhysicianID.gif"></a>
        <%}%>
         
      <table border="1" bordercolor="CCCCCC" cellpadding="3" class=tdBase cellspacing="0" width="100%">
        <%
    int altCnt = 0;
    if (eList.hasMoreElements())
    {
     while (eList.hasMoreElements())
     {

        altCnt++;
        String theClass = "tdBase";
        if (altCnt%2!=0)
        {
            theClass = "tdBaseAlt";
        }
        leCurrentElement    = (ListElement) eList.nextElement();
        working_bltProfessionalLiability  = (bltProfessionalLiability) leCurrentElement.getObject();
        working_bltProfessionalLiability.GroupSecurityInit(UserSecurityGroupID);
        if (!working_bltProfessionalLiability.isComplete())
        {            theClass = "incompleteItem";
        %>
        <tr class=incompleteItem><td><b>Not Complete</b><br>
        <%
        }
        else
        {
        %>
        <tr class=<%=theClass%> ><td> 
        <%
        }
        %>

              <b>Item ID:&nbsp;</b><%=working_bltProfessionalLiability.getProfessionalLiabilityID()%></td>
<%String theClassF = "textBase";%>

<%theClassF = "textBase";%>
<%if ((working_bltProfessionalLiability.isExpired("InsuranceCarrier",expiredDays))&&(working_bltProfessionalLiability.isExpiredCheck("InsuranceCarrier"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltProfessionalLiability.isRequired("InsuranceCarrier"))&&(!working_bltProfessionalLiability.isComplete("InsuranceCarrier")) ){theClassF = "requiredFieldMain";}%>
            
          <td valign="top"> 
            <p class=<%=theClassF%> >
<u><b><%=new bltCoverageTypeLI(working_bltProfessionalLiability.getCoverageType()).getCoverageLong()%></b></u><br>
<b><%=working_bltProfessionalLiability.getInsuranceCarrier()%></b>
			<br>
			<b>Policy #:</b> <%=working_bltProfessionalLiability.getPolicyNumber()%> 
			
			</p>
          </td>

<%theClassF = "textBase";%>
<%if ((working_bltProfessionalLiability.isExpired("InsuranceCarrier",expiredDays))&&(working_bltProfessionalLiability.isExpiredCheck("InsuranceCarrier"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltProfessionalLiability.isRequired("InsuranceCarrier"))&&(!working_bltProfessionalLiability.isComplete("InsuranceCarrier")) ){theClassF = "requiredFieldMain";}%>
            
          <td valign="top">
<p class=<%=theClassF%> ><b>Status:&nbsp;</b>
<%
if (working_bltProfessionalLiability.getTerminationDate().after(displayDateSDF1.parse("01/01/1850")))
{
%>
<i>Terminated</i>
<%
}
else if (working_bltProfessionalLiability.isExpired("InsuredToDate",0))
{
%>
<span class=requiredField>Expired</span>
<%
}
else 
{
%>
<font color=#006600>Active - Current</font>
<%
}
%>			
<br>
<b>Recent&nbsp;Term:</b>&nbsp;<%=PLCUtils.getDisplayDate(working_bltProfessionalLiability.getInsuredFromDate(),false)%>&nbsp;to&nbsp;<%=PLCUtils.getDisplayDate(working_bltProfessionalLiability.getInsuredToDate())%>
			<br>
			<b>Per-Claim:</b> <%=PLCUtils.getDefaultCurrencyFormat().format(working_bltProfessionalLiability.getPerClaimAmount())%><br>
			<b>Aggregate:</b> <%=PLCUtils.getDefaultCurrencyFormat().format(working_bltProfessionalLiability.getAggregateAmount())%>
</p></td>

            <td > 
        <a class=linkBase href = "tProfessionalLiability_main_ProfessionalLiability_PhysicianID_form_authorize.jsp?EDIT=edit&EDITID=<%=working_bltProfessionalLiability.getProfessionalLiabilityID()%>&KM=p"><img border=0 src="ui_<%=thePLCID%>/icons/edit_PhysicianID.gif"></a>

        <%if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {%>
        <a class=linkBase  onClick="return confirmDelete()"  href = "tProfessionalLiability_main_ProfessionalLiability_PhysicianID_form_authorize.jsp?EDIT=del&EDITID=<%=working_bltProfessionalLiability.getProfessionalLiabilityID()%>&KM=p"><img border=0 src="ui_<%=thePLCID%>/icons/delete_PhysicianID.gif"></a>
        <br>
        <% 
            if (working_bltProfessionalLiability.getDocuLinkID().intValue()>0)
            {
              bltDocumentManagement myDoc = new bltDocumentManagement(working_bltProfessionalLiability.getDocuLinkID());
              if (!myDoc.getDocumentFileName().equalsIgnoreCase(""))
              {
              %>
                      <a target=_blank href="tProfessionalLiability_main_ProfessionalLiability_PhysicianID_form_authorize.jsp?EDIT=print&EDITID=<%=working_bltProfessionalLiability.getProfessionalLiabilityID()%>&KM=p">View Document</a>

              <%
              }
              else
              {
              %>

                      <a class=linkBase target=_blank href = "#" onClick="window.open('tDocumentManagement_main_DocumentManagement_PhysicianID_form_authorize.jsp?EDIT=faxCover&amp;EDITID=<%=working_bltProfessionalLiability.getDocuLinkID()%>','DocPDF','toolbar=yes,location=yes,status=yes,menubar=yes,scrollbars=yes,resizable=yes,width=500,height=500');return false;" >Print Cover Sheet</a>

              <%
              }
            }
            else
            {
             if (working_bltProfessionalLiability.isComplete())
             {
               %>
                      <a target=_blank class=linkBase href = "tProfessionalLiability_ModifyDocument.jsp?EDITID=<%=working_bltProfessionalLiability.getProfessionalLiabilityID()%>&EDIT=CREATE&dType=tProfessionalLiability">Create Document</a>
               <%
             }
             else
             {
               %>
                      <span class=tdBase>You must complete this item before you can attach a document</span>
               <%
             }
            }
            %>
        <% }%>
                  </td></tr>
        <%
    }//end while
       }//end of if
       else 
       {
           %>
           <tr><td><b>Please click the "create" to add <%=ConfigurationMessages.getDataCategory("tProfessionalLiability")%> information or click 'Continue' to go to the next section.</b>
           <script language=javascript>
           if (confirm("<%=ConfigurationMessages.getInterviewMessage("INTERVIEWNoElements","tProfessionalLiability")%>"))
           {
               document.location="tProfessionalLiability_main_ProfessionalLiability_PhysicianID_form_create.jsp?EDIT=new&KM=p&INTNext=yes"; 
           }
           else
           {

           }
           </script>
           </td></tr>
           <%
       }
    %>

    </table>
    <%
  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>

    </td></tr></table><br>


<%String bnIncludeFN = "ui_"+thePLCID+"\\bot-nav_PhysicianID.jsp";
%>
<jsp:include page="<%=bnIncludeFN%>" flush="true" ><jsp:param name="plcID" value="<%=thePLCID%>"/></jsp:include>
