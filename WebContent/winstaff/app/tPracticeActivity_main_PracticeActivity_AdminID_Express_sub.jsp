<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.bltDocumentManagement,java.util.Vector,java.util.Enumeration,com.winstaff.ListElement, com.winstaff.PLCUtils, com.winstaff.*, com.winstaff.SecurityCheck, com.winstaff.ConfigurationMessages, com.winstaff.bltPracticeActivity, com.winstaff.bltPracticeActivity_List_AdminID, com.winstaff.DocumentManagerUtils" %>
<%/*
    filename: tPracticeActivity_main_PracticeActivity_AdminID.jsp
    Created on Nov/04/2009
    Type: 1-n main class file
    Created by: Scott Ellis
    Version: 4.0
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>
    <%
    Integer iExpressMode = new Integer(1);
    if (pageControllerHash.containsKey("iExpressMode")) 
    {
        iExpressMode =    (Integer)pageControllerHash.get("iExpressMode");
    }
    %>

    <table cellpadding=0 cellspacing=0 border=0 width=800 >
<%
//initial declaration of list class and parentID
    Integer        iAdminID        =    null;
    boolean accessValid = false;
   Integer iSecurityCheck = SecurityCheck.CheckItem("nim1", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {
    if (pageControllerHash.containsKey("iAdminID")) 
    {
        iAdminID        =    (Integer)pageControllerHash.get("iAdminID");
        accessValid = true;
    }

  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);
      pageControllerHash.put("sParentReturnPage","tPracticeActivity_main_PracticeActivity_AdminID_Expand.jsp");
    pageControllerHash.remove("iPracticeActivityID");
    session.setAttribute("pageControllerHash",pageControllerHash);

    bltPracticeActivity_List_AdminID        bltPracticeActivity_List_AdminID        =    new    bltPracticeActivity_List_AdminID(iAdminID);

//declaration of Enumeration
    bltPracticeActivity        PracticeActivity;
    ListElement         leCurrentElement;
    java.util.Enumeration eList = bltPracticeActivity_List_AdminID.elements();
    %>
    <%
    int iExpress=0;
boolean errorRouteMeTotal = false;
boolean isAllComplete = true;
    while (eList.hasMoreElements()||iExpress<=ConfigurationMessages.getExpressItemCount("tPracticeActivity"))
    {
       iExpress++;

      boolean isNewRecord= false;
      if (eList.hasMoreElements())
      {
        leCurrentElement    = (ListElement) eList.nextElement();
        PracticeActivity  = (bltPracticeActivity) leCurrentElement.getObject();
      }
      else
      {
        PracticeActivity  = new bltPracticeActivity();
        PracticeActivity.setAdminID(iAdminID);
        isNewRecord= true;
      }
        PracticeActivity.GroupSecurityInit(UserSecurityGroupID);
        String theClass = "tdHeader";
        %>

        <%  {

String testChangeID = "0";

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("UniqueCreateDate_"+iExpress))) ;
    if ( !PracticeActivity.getUniqueCreateDate().equals(testObj)  )
    {
         PracticeActivity.setUniqueCreateDate( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeActivity UniqueCreateDate not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("UniqueModifyDate_"+iExpress))) ;
    if ( !PracticeActivity.getUniqueModifyDate().equals(testObj)  )
    {
         PracticeActivity.setUniqueModifyDate( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeActivity UniqueModifyDate not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("UniqueModifyComments_"+iExpress)) ;
    if ( !PracticeActivity.getUniqueModifyComments().equals(testObj)  )
    {
         PracticeActivity.setUniqueModifyComments( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeActivity UniqueModifyComments not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("PracticeID_"+iExpress)) ;
    if ( !PracticeActivity.getPracticeID().equals(testObj)  )
    {
         PracticeActivity.setPracticeID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeActivity PracticeID not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("AdminID_"+iExpress)) ;
    if ( !PracticeActivity.getAdminID().equals(testObj)  )
    {
         PracticeActivity.setAdminID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeActivity AdminID not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("Name_"+iExpress)) ;
    if ( !PracticeActivity.getName().equals(testObj)  )
    {
         PracticeActivity.setName( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeActivity Name not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("ActivityType_"+iExpress)) ;
    if ( !PracticeActivity.getActivityType().equals(testObj)  )
    {
         PracticeActivity.setActivityType( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeActivity ActivityType not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("isCompleted_"+iExpress)) ;
    if ( !PracticeActivity.getisCompleted().equals(testObj)  )
    {
         PracticeActivity.setisCompleted( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeActivity isCompleted not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("StartDate_"+iExpress))) ;
    if ( !PracticeActivity.getStartDate().equals(testObj)  )
    {
         PracticeActivity.setStartDate( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeActivity StartDate not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("EndDate_"+iExpress))) ;
    if ( !PracticeActivity.getEndDate().equals(testObj)  )
    {
         PracticeActivity.setEndDate( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeActivity EndDate not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("RemindDate_"+iExpress))) ;
    if ( !PracticeActivity.getRemindDate().equals(testObj)  )
    {
         PracticeActivity.setRemindDate( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeActivity RemindDate not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("Description_"+iExpress)) ;
    if ( !PracticeActivity.getDescription().equals(testObj)  )
    {
         PracticeActivity.setDescription( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeActivity Description not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("TaskFileReference_"+iExpress)) ;
    if ( !PracticeActivity.getTaskFileReference().equals(testObj)  )
    {
         PracticeActivity.setTaskFileReference( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeActivity TaskFileReference not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("TaskLogicReference_"+iExpress)) ;
    if ( !PracticeActivity.getTaskLogicReference().equals(testObj)  )
    {
         PracticeActivity.setTaskLogicReference( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeActivity TaskLogicReference not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("Summary_"+iExpress)) ;
    if ( !PracticeActivity.getSummary().equals(testObj)  )
    {
         PracticeActivity.setSummary( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeActivity Summary not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("Comments_"+iExpress)) ;
    if ( !PracticeActivity.getComments().equals(testObj)  )
    {
         PracticeActivity.setComments( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeActivity Comments not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("ReferenceID_"+iExpress)) ;
    if ( !PracticeActivity.getReferenceID().equals(testObj)  )
    {
         PracticeActivity.setReferenceID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeActivity ReferenceID not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("DoculinkID_"+iExpress)) ;
    if ( !PracticeActivity.getDoculinkID().equals(testObj)  )
    {
         PracticeActivity.setDoculinkID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeActivity DoculinkID not set. this is ok-not an error");
}
boolean bINT = false;
boolean bNextStep = false;
String sRefreshDoc = "refreshMe=true&";
              if (false&&request.getParameter("INTNext")!=null&&request.getParameter("INTNext").equalsIgnoreCase("yes") ) 
	            {
	                bINT = true;
	                sRefreshDoc = "";
	            }
              else if (request.getParameter("INTNext")!=null&&request.getParameter("INTNext").equalsIgnoreCase("next") ) 
	            {
	                bINT = true;
	                sINTNext = ConfigurationMessages.getInterviewLinkRaw("tPracticeActivity","next");
	                sRefreshDoc = "";
	            }

// If an edit, update information; if new, append information
boolean errorRouteMe = false;
if (testChangeID.equalsIgnoreCase("1"))
{

	PracticeActivity.setUniqueModifyDate(new java.util.Date());
	String UserLogonDescription = "n/a";
	if (pageControllerHash.containsKey("UserLogonDescription"))
	{
	    UserLogonDescription = (String)pageControllerHash.get("UserLogonDescription");
	}
    if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
    {
	    PracticeActivity.setUniqueModifyComments("EX:"+UserLogonDescription);
	    try
	    {
	        PracticeActivity.commitData();
	        if (!PracticeActivity.isComplete())
	        {
	            isAllComplete = false;
	        }
	    }
	    catch (Exception e55)
	    {
	        errorRouteMe = true;
	        errorRouteMeTotal = true;
	    }	//Doculink Addition
	//Doculink Does Exist, prompt to Modify

     if (!errorRouteMe&&PracticeActivity.getDocuLinkID().intValue()>0)
     {
            bltDocumentManagement myDoc = new bltDocumentManagement(PracticeActivity.getDocuLinkID());
            if (myDoc.getDocumentFileName().equalsIgnoreCase("")) 
            {
                DocumentManagerUtils myDUtils = new DocumentManagerUtils();
                String myDType = "tPracticeActivity";
                myDUtils.setDocumentType(myDType, PracticeActivity.getUniqueID());
                myDoc.setDocumentName(myDUtils.getDocumentName());
                myDoc.setDateOfExpiration(myDUtils.getDateOfExpiration());
                myDoc.setDocumentTypeID(myDUtils.getDocumentTypeID());
                myDoc.setAdminID(iAdminID);
                myDoc.setArchived(new Integer(2));
                myDoc.commitData();
            }
            else
            {
                //archive then create new
                myDoc.setArchived(new Integer("1"));
                myDoc.commitData();
                myDoc = new bltDocumentManagement();
                DocumentManagerUtils myDUtils = new DocumentManagerUtils();
                String myDType = "tPracticeActivity";
                myDUtils.setDocumentType(myDType, PracticeActivity.getUniqueID());
                myDoc.setDocumentName(myDUtils.getDocumentName());
                myDoc.setDateOfExpiration(myDUtils.getDateOfExpiration());
                myDoc.setDocumentTypeID(myDUtils.getDocumentTypeID());
                myDoc.setAdminID(iAdminID);
                myDoc.setArchived(new Integer(2));
                myDoc.commitData();
                PracticeActivity.setDocuLinkID(myDoc.getUniqueID());
                PracticeActivity.commitData();
            }
     }
     else if (!errorRouteMe)
     {
            bltDocumentManagement myDoc = new bltDocumentManagement();
            DocumentManagerUtils myDUtils = new DocumentManagerUtils();
            String myDType = "tPracticeActivity";
            myDUtils.setDocumentType(myDType, PracticeActivity.getUniqueID());
            myDoc.setDocumentName(myDUtils.getDocumentName());
            myDoc.setDateOfExpiration(myDUtils.getDateOfExpiration());
            myDoc.setDocumentTypeID(myDUtils.getDocumentTypeID());
                myDoc.setAdminID(iAdminID);
            myDoc.setArchived(new Integer(2));
            myDoc.commitData();
            PracticeActivity.setDocuLinkID(myDoc.getUniqueID());
            PracticeActivity.commitData();
     }

    }
}       }
    }//while

if (errorRouteMeTotal)
{
	out.println(ConfigurationMessages.getMessage("FatalError")); 
}
else 
{
String nextPage = request.getParameter("nextPage");
if (nextPage==null||nextPage.equalsIgnoreCase(""))
{
	nextPage = ConfigurationMessages.getExpressLinkRaw("tPracticeActivity","next",iExpressMode);
}
if (!isAllComplete)
{
	%>
	<script language=Javascript>
	      if ( confirm("<%=ConfigurationMessages.getMessage("ConfirmReqFieldsReturn")%>") )
	      {
	          document.location="<%=ConfigurationMessages.getDataCategoryLink("tPracticeActivity","Express")%>";
	      }
	      else 
	      {
	          document.location="<%=nextPage%>";
	      }
	</script>
  <%
}
else
{
	%>
	<script language=Javascript>
	      document.location="<%=nextPage%>";
	</script>
  <%
}
}
       
  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>

    </td></tr></table>


<%String bnIncludeFN = "ui_"+thePLCID+"\\bot-nav_AdminID.jsp";
%>
<jsp:include page="<%=bnIncludeFN%>" flush="true" ><jsp:param name="plcID" value="<%=thePLCID%>"/></jsp:include>