<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.bltDocumentManagement,java.util.Vector,java.util.Enumeration,com.winstaff.ListElement, com.winstaff.PLCUtils, com.winstaff.SecurityCheck, com.winstaff.ConfigurationMessages,com.winstaff.bltAdminPracticeLU_List_PracticeMaster_AdminID,com.winstaff.bltPhysicianMaster,com.winstaff.bltPracticeMaster,com.winstaff.bltAdminMaster,com.winstaff.bltPhysicianPracticeLU,com.winstaff.bltAdminPracticeLU" %>
<%/*
    filename: out\jsp\cl_Practice_Physician_via_Admin.jsp
    Created on Feb/17/2003
    Type: n-n main class file
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>


<%
//initial declaration of list class and parentID
    Integer        iPhysicianID        =    null;
    Integer        iAdminID        =    null;
    boolean accessValid = false;
   Integer iSecurityCheck = SecurityCheck.CheckItem("Practice1", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {

    if ((pageControllerHash.containsKey("iPhysicianID"))&&(pageControllerHash.containsKey("iAdminID"))) 
    {
        iPhysicianID        =    (Integer)pageControllerHash.get("iPhysicianID");
        iAdminID        =    (Integer)pageControllerHash.get("iAdminID");
        if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {
            accessValid = true;
        }
    }
  //page security
  if (accessValid)
  {
        Integer requestID = null;
        if (request.getParameter("EDITID")!=null)
        {
            requestID = new Integer(request.getParameter("EDITID"));
            out.println(requestID);
        }

//declaration of Enumeration
    bltAdminPracticeLU_List_PracticeMaster_AdminID         bltAdminPracticeLU_List_PracticeMaster_AdminID        =    new    bltAdminPracticeLU_List_PracticeMaster_AdminID(iAdminID,"PracticeID = "+requestID+" ","");
    bltPracticeMaster        working_bltPracticeMaster;
    ListElement         leCurrentElement;
    java.util.Enumeration eList = bltAdminPracticeLU_List_PracticeMaster_AdminID.elements();
    %>
    <table class=tableBase cellpadding=0 cellspacing=0 border=0>
    <%
     if (eList.hasMoreElements())
     {
        leCurrentElement    = (ListElement) eList.nextElement();
        working_bltPracticeMaster  = (bltPracticeMaster) leCurrentElement.getObject();
        bltPhysicianPracticeLU  working_bltPhysicianPracticeLU = new bltPhysicianPracticeLU();
        working_bltPhysicianPracticeLU.setPracticeID(requestID);
        working_bltPhysicianPracticeLU.setPhysicianID(iPhysicianID);
        working_bltPhysicianPracticeLU.commitData();



        pageControllerHash.put("iLookupID",working_bltPhysicianPracticeLU.getUniqueID());
        pageControllerHash.put("iPracticeID",working_bltPhysicianPracticeLU.getPracticeID());
        session.setAttribute("pageControllerHash",pageControllerHash);
        //Parameter Pass Code here
String parameterPassString ="";
java.util.Enumeration myParameterPassList = request.getParameterNames();
while (myParameterPassList.hasMoreElements())
{
	String myName = (String)myParameterPassList.nextElement();
	String myS = (String) request.getParameter(myName);
	parameterPassString+="&"+myName + "=" + myS;
}
        String targetRedirect = "tPracticeMaster_form_basic.jsp?EDIT=edit";

        if (pageControllerHash.containsKey("sParentReturnPage"))
        {
//            targetRedirect = (String)pageControllerHash.get("sParentReturnPage");
        }
        response.sendRedirect(targetRedirect);
    }

  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>


