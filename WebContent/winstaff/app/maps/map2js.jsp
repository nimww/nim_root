<html>
  <head>
  
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <title>Google Maps JavaScript API Example</title>
    <script src="https://maps.google.com/maps?file=api&amp;v=2&amp;key=ABQIAAAAM7hvG1DJhf2TtXeNloLnlBT_8jAO_dNHWn0-7Sy73XDsGscKTRQ7jzNfm4K0e1tBVkjqL0sYYTOq0w"  type="text/javascript"></script>
    <script src="mapiconmaker.js" type="text/javascript"></script> 
    <script type="text/javascript">
    //<![CDATA[
var locations = {};
 //DEV: ABQIAAAAM7hvG1DJhf2TtXeNloLnlBTpjxwYLJ5LNEdt9aoK3FJrepMIohRFCg1hjxMcS8IUhg5xGUdRd70R1g
 //PRD: ABQIAAAAM7hvG1DJhf2TtXeNloLnlBT_8jAO_dNHWn0-7Sy73XDsGscKTRQ7jzNfm4K0e1tBVkjqL0sYYTOq0w
    function load() 
	{
      var map = new GMap2(document.getElementById("map"));
	  var myLatLng;
	  
      GDownloadUrl("../AdminPracticeAll_map.jsp", function(data) 
	  {
        var xml = GXml.parse(data);
		try
		{
	        var markers = xml.documentElement.getElementsByTagName("marker");
		}
		catch(exception)
		{
			alert("Practice ID:" + markers );
		}
		var bounds = new GLatLngBounds;
        for (var i = 0; i < markers.length; i++)
		{
			try
			{
			  var name = markers[i].getAttribute("name");
			  var address = markers[i].getAttribute("address");
			  var type = markers[i].getAttribute("type");
			  var icid = markers[i].getAttribute("icid");
			  var isopen = markers[i].getAttribute("isopen");
			  var tesla = markers[i].getAttribute("tesla");
			  var ownername = markers[i].getAttribute("ownername");
			  var ownerid = markers[i].getAttribute("ownerid");
			  var latlng = new GLatLng(parseFloat(markers[i].getAttribute("lat")),parseFloat(markers[i].getAttribute("lng")));
				bounds.extend(latlng);
	
	
	
			  myLatLng = latlng;
	//		  alert(myLatLng);
			  var store = {latlng: latlng, name: name, address: address, type: type, icid: icid, ownerid: ownerid, ownername: ownername, tesla: tesla, isopen: isopen};
			  var latlngHash = (latlng.lat().toFixed(6) + "" + latlng.lng().toFixed(6));
			  latlngHash = latlngHash.replace(".","").replace(".", "").replace("-","");
			  if (locations[latlngHash] == null) 
			  {
				locations[latlngHash] = []
			  }
			  locations[latlngHash].push(store);
			}
			catch(exception)
			{
				alert("Bad Address" + markers );
			}
        }
        for (var latlngHash in locations) 
		{
          var stores = locations[latlngHash];
          if (false&&stores.length > 1) 
		  {
            map.addOverlay(createClusteredMarker(stores));
          }
		  else 
		  {
			var tempMarker = createMarker(stores);
            map.addOverlay(tempMarker);
			//bounds.extend(tempMarker.getPoint());
          }
        }
//			alert(myLatLng);
			//      map.setCenter(LatLng, 13);
		int1=5
		try
		{
			int1 = map.getBoundsZoomLevel(bounds);
			//map.setZoom(int1);
		}
		catch(exception)
		{
			alert("latlng:["+int1+"]" + exception );
		}
		map.setCenter(bounds.getCenter(),int1);
		//map.setCenter(myLatLng, 9);
		map.setUIToDefault();
		
      });
	  
    }
 
    function createMarker(stores) 
	{
      var store = stores[0];
//	  alert("Type = " + store.type);
	  var sStatus = "None";
	  var isGreen=false;
	  var mod1=false;
	  <%
	  if (request.getParameter("mty")!=null)
	  {
		  if (request.getParameter("mty").equalsIgnoreCase("green"))
		  {
			  out.print("	isGreen = true;");
		  }
		  else if (request.getParameter("mty").equalsIgnoreCase("mod1"))
		  {
			  out.print("	mod1 = true;");
		  }
	  }
	  %>
	  var newIcon = MapIconMaker.createMarkerIcon({width: 32, height: 32, primaryColor: "#999999"});
	  if (store.type=="1")
	  {
		  newIcon = MapIconMaker.createMarkerIcon({width: 32, height: 32, primaryColor: "#00ffff"});
		  sStatus = "Negotiating";
	  }
	  else if (store.type=="2")
	  {
		  newIcon = MapIconMaker.createMarkerIcon({width: 32, height: 32, primaryColor: "#00ff00"});
		  sStatus = "Contracted";
	  }
	  else if (store.type=="3")
	  {
		  newIcon = MapIconMaker.createMarkerIcon({width: 32, height: 32, primaryColor: "#FFFF00"});
		  sStatus = "Target";
	  }
	  else if (store.type=="4")
	  {
		  newIcon = MapIconMaker.createMarkerIcon({width: 32, height: 32, primaryColor: "#C87286"});
		  sStatus = "Not Interested";
	  }
	  else if (store.type=="5")
	  {
		  newIcon = MapIconMaker.createMarkerIcon({width: 32, height: 32, primaryColor: "#ff00ff"});
		  sStatus = "Send Contract";
	  }
	  else if (store.type=="6")
	  {
		  newIcon = MapIconMaker.createMarkerIcon({width: 32, height: 32, primaryColor: "#ff6600"});
		  sStatus = "Talking";
	  }
	  else if (store.type=="7")
	  {
		  newIcon = MapIconMaker.createMarkerIcon({width: 32, height: 32, primaryColor: "#FF2893"});
		  sStatus = "Target-Merge";
	  }
	  else if (store.type=="8")
	  {
		  newIcon = MapIconMaker.createMarkerIcon({width: 32, height: 32, primaryColor: "#97BE81"});
		  sStatus = "Contracted [Select]";
	  }
	  else if (store.type=="9")
	  {
		  newIcon = MapIconMaker.createMarkerIcon({width: 32, height: 32, primaryColor: "#99fF00"});
		  sStatus = "Contracted [*]";
	  }
	  else if (store.type=="10")
	  {
		  newIcon = MapIconMaker.createMarkerIcon({width: 32, height: 32, primaryColor: "#D3FF62"});
		  sStatus = "Contracted [V*]";
	  }
	  else if (store.type=="11")
	  {
		  newIcon = MapIconMaker.createMarkerIcon({width: 32, height: 32, primaryColor: "#BAED00"});
		  sStatus = "Contracted [IPA]";
	  }

if (isGreen)
	  {
		  newIcon = MapIconMaker.createMarkerIcon({width: 32, height: 32, primaryColor: "#00ff00"});
//		  sStatus = "Contracted";
	  }

	  if (store.isopen=="-1")
	  {
		  if (mod1)
		  {
			  newIcon = MapIconMaker.createMarkerIcon({width: 32, height: 32, primaryColor: "#333333"});
		  }
		  mriStatus = "Model [Empty]" + store.tesla;
	  }
	  if (store.isopen=="1")
	  {
		  if (mod1)
		  {
			  newIcon = MapIconMaker.createMarkerIcon2({width: 32, height: 32, primaryColor: "#99ff999"},'O');
		  }
		  mriStatus = "Open:" + store.tesla;
	  }
	  if (store.isopen=="0")
	  {
		  if (mod1)
		  {
			  newIcon = MapIconMaker.createMarkerIcon2({width: 32, height: 32, primaryColor: "#00ff00"},'T');
		  }
		  mriStatus = "Traditional:" + store.tesla;
	  }
      var marker = new GMarker(store.latlng, {icon: newIcon});
      var html = "<div style=\" max-width: 300px; 	width: 300px; 	height: 150px; 	text-align: justify; max-height: 400px; min-height: 150px; 	min-width: 200px; 	padding-right: 6px; 	overflow-x: auto; 	overflow-y: auto;  \"><strong>" + store.name + "</strong><br>" + store.address + "<br/><br/>Owner: <strong>" + store.ownername + "</strong><br/>Status: <strong>" + sStatus + "</strong><br/>MRI: <strong>" + mriStatus + "</strong><br/>ICID: <strong>" + store.icid + "</strong> | <a href=\"../AdminPracticeAll_query_auth.jsp?EDIT=practice&EDITID=" + store.icid + "&EDIT2ID=" + store.ownerid + "\" target=editResults >Edit</a></div>";
      GEvent.addListener(marker, 'click', function() {marker.openInfoWindowHtml(html);  });
      return marker;
    }
 
    function createClusteredMarker(stores, type) {
      var newIcon = MapIconMaker.createMarkerIcon({width: 44, height: 44, primaryColor: "#00ff00"});
      var marker = new GMarker(stores[0].latlng, {icon: newIcon});
      var html = "<div style=\" max-width: 300px; 	width: 300px; 	height: 250px; 	text-align: justify; max-height: 400px; min-height: 200px; 	min-width: 200px; 	padding-right: 6px; 	overflow-x: auto; 	overflow-y: auto;  \">";
      for (var i = 0; i < stores.length; i++) 
	  {
	      	html += "<b>" + stores[i].name + "</b> <br/>" + stores[i].address + "<br/><br/>Status: " + sStatus + "<br/>MRI: " + mriStatus + "<br/><br/><br/>";
      }
	  html += "</div>";
      GEvent.addListener(marker, 'click', function() {
        marker.openInfoWindowHtml("Imaging Center", html);
      });
      return marker;
    }
    //]]>
  </script>
  </head>
  <body onLoad="load()" onUnload="GUnload()">
    <div id="map" style="width: 100%; height: 100%; border: 1px solid black"></div>
  </body>
</html>

