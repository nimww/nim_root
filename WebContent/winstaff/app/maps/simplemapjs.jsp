<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" 

      "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

  <head>

    <script src="http://maps.google.com/maps?file=api&v=1

            &key=ABQIAAAAM7hvG1DJhf2TtXeNloLnlBT_8jAO_dNHWn0-7Sy73XDsGscKTRQ7jzNfm4K0e1tBVkjqL0sYYTOq0w" type="text/javascript">

    </script>

  </head>

  <body>

    <div id="map" style="width: 400px; height: 300px"></div>

    <script type="text/javascript">

    //<![CDATA[


    var map = new GMap(document.getElementById("map"));

    map.centerAndZoom(new GPoint(-83.022206, 39.998264), 3);
	var blueIcon = new GIcon(G_DEFAULT_ICON);
	blueIcon.image = "http://www.google.com/intl/en_us/mapfiles/ms/micons/blue-dot.png";
	markerOptions = { icon:blueIcon };	
	var bounds = map.getBounds();
	var southWest = bounds.getSouthWest();
	var northEast = bounds.getNorthEast();
	var lngSpan = northEast.lng() - southWest.lng();
	var latSpan = northEast.lat() - southWest.lat();
	for (var i = 0; i < 10; i++) 
	{
		var point = new GLatLng(southWest.lat() + latSpan * Math.random(),southWest.lng() + lngSpan * Math.random());
		map.addOverlay(new GMarker(point, markerOptions));
	}

    

    //]]>

    </script>

  </body>

</html>
