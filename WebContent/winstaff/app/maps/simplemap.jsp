<%@ taglib uri="/WEB-INF/googlemaps.tld" prefix="googlemaps" %> 

<head
<googlemaps:map id="map" width="250" height="300" version="2" type="STREET" 
		zoom="12">
  	<googlemaps:key domain="https://secure.nextimagemedical.com/" key="ABQIAAAAM7hvG1DJhf2TtXeNloLnlBT_8jAO_dNHWn0-7Sy73XDsGscKTRQ7jzNfm4K0e1tBVkjqL0sYYTOq0w"/>
  	<googlemaps:point id="point1" address="3398 Carmel Mountain Road" city="San Diego" 
  			state="CA" zipcode="92121" country="US"/>
	<googlemaps:marker id="marker1" point="point1"/>
</googlemaps:map>

<googlemaps:scripttag id="map"/>
<googlemaps:javascript id="map"/>
</head>

<hr />
<googlemaps:div id="map"/>
<googlemaps:initialize id="map"/> 
<hr />



