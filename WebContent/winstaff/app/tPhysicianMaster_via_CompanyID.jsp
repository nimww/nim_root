<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.PLCUtils,com.winstaff.bltPhysicianMaster,com.winstaff.bltPhysicianMaster,com.winstaff.searchDB2,java.util.Vector,java.util.Enumeration,com.winstaff.ListElement,com.winstaff.bltAdminMaster,com.winstaff.bltCompanyAdminLU,com.winstaff.bltCompanyAdminLU_List_LU_CompanyID" %>
<%/*
    filename: out\jsp\tCompanyAdminLU_main_AdminMaster_CompanyID.jsp
    JSP AutoGen on Mar/26/2002
    Type: n-n main class file
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>

<%
//String thePLCID = (String)pageControllerHash.get("plcID");
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_CompanyID.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>
<table cellpadding=0 cellspacing=0 border=0 width=500>	
  <tr> 
    <td width=10>&nbsp;</td>
    <td> <span class=title>Physician-Admin Control</span>
    <br>
      <%
//initial declaration of list class and parentID
    Integer        iCompanyID        =    null;
    boolean accessValid = false;
    if (pageControllerHash.containsKey("iCompanyID")) 
    {
        iCompanyID        =    (Integer)pageControllerHash.get("iCompanyID");
        accessValid = true;
    }
  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);
      java.text.SimpleDateFormat displayDateLongSDF = new java.text.SimpleDateFormat("MMMM dd yyyy");
    bltCompanyAdminLU_List_LU_CompanyID        bltCompanyAdminLU_List_LU_CompanyID        =    new    bltCompanyAdminLU_List_LU_CompanyID(iCompanyID);
//if Physician
//declaration of Enumeration
    bltCompanyAdminLU        working_bltCompanyAdminLU;
    ListElement         leCurrentElement;
    java.util.Enumeration eList = bltCompanyAdminLU_List_LU_CompanyID.elements();
        String theClass = "tdHeader";
        %>
      <p class=<%=theClass%>> 
        <%String theClassF = "textBase";%>
      </p>
      <table width="100%" border="1" bordercolor=CCCCCC cellspacing="0" cellpadding="0">
        <tr  class=tdHeader> 
          <td>ID</td>
          <td><b>Create Date</b></td>
          <td><b>Last Name</b></td>
          <td><b>First Name</b></td>
          <td><b>AdminID</b></td>
          <td><b>City</b></td>
          <td><b>ZIP</b>
          </td>
        </tr>
<%
java.util.Vector myAdminsV = new java.util.Vector();
    while (eList.hasMoreElements())
    {
        leCurrentElement    = (ListElement) eList.nextElement();
        working_bltCompanyAdminLU  = (bltCompanyAdminLU) leCurrentElement.getObject();
        myAdminsV.addElement(working_bltCompanyAdminLU.getAdminID());
    }
String newWhere = "";
if (myAdminsV!=null)
{
    newWhere = "AdminID="+(Integer)myAdminsV.elementAt(0) +" ";
    for (int i=1 ;i<myAdminsV.size();i++)
    {
        newWhere +="OR AdminID="+(Integer)myAdminsV.elementAt(i) +" ";
    }
}

searchDB2 mySDB = new searchDB2();
java.sql.ResultSet myRS = mySDB.executeStatement("select * from tAdminPhysicianLU where ("+newWhere+") order by PhysicianID");


    while (myRS!=null&&myRS.next())
    {
	Integer iPhys = new Integer(myRS.getString("PhysicianID"));
	Integer iAdm = new Integer(myRS.getString("AdminID"));
//        bltAdminMaster working_bltAdminMaster  = new bltAdminMaster(iAdm);
        bltPhysicianMaster working_bltPhysicianMaster  = new bltPhysicianMaster(iPhys);
%>
        <tr class=tdbase> 
          <td><%=working_bltPhysicianMaster.getPhysicianID()%></td>
          <td><%=displayDateSDF1.format(working_bltPhysicianMaster.getUniqueCreateDate())%>&nbsp;</td>
          <td><%=working_bltPhysicianMaster.getLastName()%>&nbsp;</td>
          <td><%=working_bltPhysicianMaster.getFirstName()%>&nbsp;</td>
          <td><%=iAdm%>&nbsp;</td>
          <td><%=working_bltPhysicianMaster.getHomeCity()%>&nbsp;</td>
          <td> <%=working_bltPhysicianMaster.getHomeZIP()%>&nbsp;</td>
          <td> 
            <%if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
	        {%>
    	        <a class=linkBase href = "tPhysicianMaster_via_CompanyID_authorize.jsp?EDIT=pass&EDITID=<%=working_bltPhysicianMaster.getPhysicianID()%>"><img border=0 src="ui_<%=thePLCID%>/icons/edit_CompanyID.gif"></a> 
            <%}%>
     <%}%>
          </td>
        </tr>
      </table>
      <p> 
        <%

  }
  else
  {
   response.sendRedirect("/errorhandler.jsp");
  }
%>
      </p>
      </td>
  </tr>
</table>
<%String bnIncludeFN = "ui_"+thePLCID+"\\bot-nav_CompanyID.jsp";
%>
<jsp:include page="<%=bnIncludeFN%>" flush="true" > 
<jsp:param name="plcID" value="<%=thePLCID%>"/>
</jsp:include>
