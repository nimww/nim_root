<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.bltDocumentManagement,java.util.Vector,java.util.Enumeration,com.winstaff.ListElement, com.winstaff.PLCUtils, com.winstaff.SecurityCheck, com.winstaff.ConfigurationMessages,com.winstaff.bltWorkHistory,com.winstaff.bltWorkHistory_List" %>
<%/*
    filename: tWorkHistory_main_WorkHistory_PhysicianID.jsp
    Created on Oct/22/2003
    Type: 1-n main class file
    Created by: Scott Ellis
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>
    <%
    Integer iExpressMode = new Integer(1);
    if (pageControllerHash.containsKey("iExpressMode")) 
    {
        iExpressMode =    (Integer)pageControllerHash.get("iExpressMode");
    }
    %>

<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_PhysicianID.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>

    <table cellpadding=0 cellspacing=0 border=0 width=<%=MasterTableWidth%> >
    <tr><td width=10>&nbsp;</td><td>
    <%=ConfigurationMessages.getHTML("EXPRESSTopControl","tWorkHistory",iExpressMode)%>



<%
//initial declaration of list class and parentID
    Integer        iPhysicianID        =    null;
    boolean accessValid = false;
   Integer iSecurityCheck = SecurityCheck.CheckItem("PractitionerSection8", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {
    if (pageControllerHash.containsKey("iPhysicianID") ) 
    {
        iPhysicianID        =    (Integer)pageControllerHash.get("iPhysicianID");
        accessValid = true;
    }  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);
      pageControllerHash.put("sParentReturnPage","tWorkHistory_main_WorkHistory_PhysicianID_Expand.jsp");
    pageControllerHash.remove("iWorkHistoryID");
    session.setAttribute("pageControllerHash",pageControllerHash);

    bltWorkHistory_List        bltWorkHistory_List        =    new    bltWorkHistory_List(iPhysicianID);

//declaration of Enumeration
    bltWorkHistory        WorkHistory;
    ListElement         leCurrentElement;
    Enumeration eList = bltWorkHistory_List.elements();
    %>
        <%@ include file="tWorkHistory_main_WorkHistory_PhysicianID_instructions.jsp" %>

        <form action="tWorkHistory_main_WorkHistory_PhysicianID_Express_sub.jsp" name="tWorkHistory_main_WorkHistory_PhysicianID1" method="POST">
    <%
    int iExpress=0;
    while (eList.hasMoreElements()||iExpress<=ConfigurationMessages.getExpressItemCount("tWorkHistory"))
    {
       iExpress++;
         %>
         <table border="0" bordercolor="333333" cellpadding="0" class=tdHeaderAlt cellspacing="0" width="100%">
            <tr>
              <td width="50%">
                   <table border="0" bordercolor="333333" cellpadding="0" class=tdHeaderAlt cellspacing="0" width="100%">
                   <tr> 
                   	<td rowspan="2"><img src=express/left-corner.gif></td>
                   	<td width=100%><img width=100% height=2 src=express/small-line.gif></td>
                   	<td rowspan="2" align=right><img src=express/right-corner.gif></td>
                   </tr>
                     <tr> 
                       <td>
         <%

      boolean isNewRecord= false;
      if (eList.hasMoreElements())
      {
        leCurrentElement    = (ListElement) eList.nextElement();
        WorkHistory  = (bltWorkHistory) leCurrentElement.getObject();
      }
      else
      {
        WorkHistory  = new bltWorkHistory();
        isNewRecord= true;
      }
        WorkHistory.GroupSecurityInit(UserSecurityGroupID);
        String theClass = "tdHeader";
        %>
               <span class=<%=theClass%> ><b><%=ConfigurationMessages.getDataCategory("tWorkHistory")%> #<%=iExpress%></span>
                  </td></tr></table>
            </td></tr>
                 </table>
                 <table width="100%" border="1" cellspacing="0" bordercolor="#333333">
                  <tr>
                   <td>
                     <table width="100%" border="0" cellspacing="0">
                     <tr><td>
<%String theClassF = "textBase";%>
<%
if (isNewRecord)
{%>
<input type=hidden name=recordItemStatus_<%=iExpress%>="new">
<%}
else
{%>
<input type=hidden name=recordItemStatus_<%=iExpress%>="edit">
<%}

  {

        %>

          <%  theClass ="tdBase";%>
        <table width=100% border=1 bordercolor=#333333 cellpadding=3 cellspacing=0 class=tableBase>
         <tr><td class=tableColor>


            <table cellpadding=0 cellspacing=0 width=100%>
            <%
            if ( (WorkHistory.isRequired("WorkHistoryTypeID",UserSecurityGroupID))&&(!WorkHistory.isComplete("WorkHistoryTypeID")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((WorkHistory.isExpired("WorkHistoryTypeID",expiredDays))&&(WorkHistory.isExpiredCheck("WorkHistoryTypeID",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((WorkHistory.isWrite("WorkHistoryTypeID",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Work Type&nbsp;</b></p></td><td valign=top><p><select   name="WorkHistoryTypeID_<%=iExpress%>" ><jsp:include page="../generic/tWorkHistoryTypeLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=WorkHistory.getWorkHistoryTypeID()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=WorkHistoryTypeID&amp;sTableName=tWorkHistory&amp;sRefID=<%=WorkHistory.getWorkHistoryID()%>&amp;sFieldNameDisp=<%=WorkHistory.getEnglish("WorkHistoryTypeID")%>&amp;sTableNameDisp=tWorkHistory','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((WorkHistory.isRead("WorkHistoryTypeID",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Work Type&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tWorkHistoryTypeLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=WorkHistory.getWorkHistoryTypeID()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=WorkHistoryTypeID&amp;sTableName=tWorkHistory&amp;sRefID=<%=WorkHistory.getWorkHistoryID()%>&amp;sFieldNameDisp=<%=WorkHistory.getEnglish("WorkHistoryTypeID")%>&amp;sTableNameDisp=tWorkHistory','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (WorkHistory.isRequired("Name",UserSecurityGroupID))&&(!WorkHistory.isComplete("Name")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((WorkHistory.isExpired("Name",expiredDays))&&(WorkHistory.isExpiredCheck("Name",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((WorkHistory.isWrite("Name",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Company&nbsp;</b></p></td><td valign=top><p><input maxlength="100" type=text size="80" name="Name_<%=iExpress%>" value="<%=WorkHistory.getName()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Name&amp;sTableName=tWorkHistory&amp;sRefID=<%=WorkHistory.getWorkHistoryID()%>&amp;sFieldNameDisp=<%=WorkHistory.getEnglish("Name")%>&amp;sTableNameDisp=tWorkHistory','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((WorkHistory.isRead("Name",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Company&nbsp;</b></p></td><td valign=top><p><%=WorkHistory.getName()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Name&amp;sTableName=tWorkHistory&amp;sRefID=<%=WorkHistory.getWorkHistoryID()%>&amp;sFieldNameDisp=<%=WorkHistory.getEnglish("Name")%>&amp;sTableNameDisp=tWorkHistory','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (WorkHistory.isRequired("WorkDescription",UserSecurityGroupID))&&(!WorkHistory.isComplete("WorkDescription")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((WorkHistory.isExpired("WorkDescription",expiredDays))&&(WorkHistory.isExpiredCheck("WorkDescription",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((WorkHistory.isWrite("WorkDescription",UserSecurityGroupID)))
            {
                        %>
                       <tr><td valign=top><p class=<%=theClass%> ><b><%=WorkHistory.getEnglish("WorkDescription")%>&nbsp;</b></p></td><td valign=top><p><textarea onKeyDown="textAreaStop(this,200)" rows="2" name="WorkDescription_<%=iExpress%>" cols="40" maxlength=200><%=WorkHistory.getWorkDescription()%></textarea>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=WorkDescription&amp;sTableName=tWorkHistory&amp;sRefID=<%=WorkHistory.getWorkHistoryID()%>&amp;sFieldNameDisp=<%=WorkHistory.getEnglish("WorkDescription")%>&amp;sTableNameDisp=tWorkHistory','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((WorkHistory.isRead("WorkDescription",UserSecurityGroupID)))
            {
                        %>
                       <tr><td valign=top> <p class=<%=theClass%> ><b><%=WorkHistory.getEnglish("WorkDescription")%>&nbsp;</b></p></td><td valign=top><p><%=WorkHistory.getWorkDescription()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=WorkDescription&amp;sTableName=tWorkHistory&amp;sRefID=<%=WorkHistory.getWorkHistoryID()%>&amp;sFieldNameDisp=<%=WorkHistory.getEnglish("WorkDescription")%>&amp;sTableNameDisp=tWorkHistory','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (WorkHistory.isRequired("FromDate",UserSecurityGroupID))&&(!WorkHistory.isComplete("FromDate")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((WorkHistory.isExpired("FromDate",expiredDays))&&(WorkHistory.isExpiredCheck("FromDate",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>

            <%
            if ((WorkHistory.isWrite("FromDate",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Start Date&nbsp;(mm/dd/yyyy):&nbsp;</b></p></td><td valign=top><p><input maxlength=20  type=text size="80" name="FromDate_<%=iExpress%>" value='<jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(WorkHistory.getFromDate())%>" /></jsp:include>' >&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=FromDate&amp;sTableName=tWorkHistory&amp;sRefID=<%=WorkHistory.getWorkHistoryID()%>&amp;sFieldNameDisp=<%=WorkHistory.getEnglish("FromDate")%>&amp;sTableNameDisp=tWorkHistory','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((WorkHistory.isRead("FromDate",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Start Date&nbsp;(mm/dd/yyyy):&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(WorkHistory.getFromDate())%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=FromDate&amp;sTableName=tWorkHistory&amp;sRefID=<%=WorkHistory.getWorkHistoryID()%>&amp;sFieldNameDisp=<%=WorkHistory.getEnglish("FromDate")%>&amp;sTableNameDisp=tWorkHistory','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (WorkHistory.isRequired("ToDate",UserSecurityGroupID))&&(!WorkHistory.isComplete("ToDate")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((WorkHistory.isExpired("ToDate",expiredDays))&&(WorkHistory.isExpiredCheck("ToDate",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>

            <%
            if ((WorkHistory.isWrite("ToDate",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>End Date&nbsp;(mm/dd/yyyy):&nbsp;</b></p></td><td valign=top><p><input maxlength=20  type=text size="80" name="ToDate_<%=iExpress%>" value='<jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(WorkHistory.getToDate())%>" /></jsp:include>' >&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ToDate&amp;sTableName=tWorkHistory&amp;sRefID=<%=WorkHistory.getWorkHistoryID()%>&amp;sFieldNameDisp=<%=WorkHistory.getEnglish("ToDate")%>&amp;sTableNameDisp=tWorkHistory','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((WorkHistory.isRead("ToDate",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>End Date&nbsp;(mm/dd/yyyy):&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(WorkHistory.getToDate())%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ToDate&amp;sTableName=tWorkHistory&amp;sRefID=<%=WorkHistory.getWorkHistoryID()%>&amp;sFieldNameDisp=<%=WorkHistory.getEnglish("ToDate")%>&amp;sTableNameDisp=tWorkHistory','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (WorkHistory.isRequired("ReasonForLeaving",UserSecurityGroupID))&&(!WorkHistory.isComplete("ReasonForLeaving")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((WorkHistory.isExpired("ReasonForLeaving",expiredDays))&&(WorkHistory.isExpiredCheck("ReasonForLeaving",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((WorkHistory.isWrite("ReasonForLeaving",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Reason for leaving&nbsp;</b></p></td><td valign=top><p><input maxlength="50" type=text size="80" name="ReasonForLeaving_<%=iExpress%>" value="<%=WorkHistory.getReasonForLeaving()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ReasonForLeaving&amp;sTableName=tWorkHistory&amp;sRefID=<%=WorkHistory.getWorkHistoryID()%>&amp;sFieldNameDisp=<%=WorkHistory.getEnglish("ReasonForLeaving")%>&amp;sTableNameDisp=tWorkHistory','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((WorkHistory.isRead("ReasonForLeaving",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Reason for leaving&nbsp;</b></p></td><td valign=top><p><%=WorkHistory.getReasonForLeaving()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ReasonForLeaving&amp;sTableName=tWorkHistory&amp;sRefID=<%=WorkHistory.getWorkHistoryID()%>&amp;sFieldNameDisp=<%=WorkHistory.getEnglish("ReasonForLeaving")%>&amp;sTableNameDisp=tWorkHistory','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (WorkHistory.isRequired("Address1",UserSecurityGroupID))&&(!WorkHistory.isComplete("Address1")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((WorkHistory.isExpired("Address1",expiredDays))&&(WorkHistory.isExpiredCheck("Address1",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((WorkHistory.isWrite("Address1",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Company Address&nbsp;</b></p></td><td valign=top><p><input maxlength="50" type=text size="80" name="Address1_<%=iExpress%>" value="<%=WorkHistory.getAddress1()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Address1&amp;sTableName=tWorkHistory&amp;sRefID=<%=WorkHistory.getWorkHistoryID()%>&amp;sFieldNameDisp=<%=WorkHistory.getEnglish("Address1")%>&amp;sTableNameDisp=tWorkHistory','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((WorkHistory.isRead("Address1",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Company Address&nbsp;</b></p></td><td valign=top><p><%=WorkHistory.getAddress1()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Address1&amp;sTableName=tWorkHistory&amp;sRefID=<%=WorkHistory.getWorkHistoryID()%>&amp;sFieldNameDisp=<%=WorkHistory.getEnglish("Address1")%>&amp;sTableNameDisp=tWorkHistory','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (WorkHistory.isRequired("Address2",UserSecurityGroupID))&&(!WorkHistory.isComplete("Address2")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((WorkHistory.isExpired("Address2",expiredDays))&&(WorkHistory.isExpiredCheck("Address2",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((WorkHistory.isWrite("Address2",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Address 2&nbsp;</b></p></td><td valign=top><p><input maxlength="20" type=text size="80" name="Address2_<%=iExpress%>" value="<%=WorkHistory.getAddress2()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Address2&amp;sTableName=tWorkHistory&amp;sRefID=<%=WorkHistory.getWorkHistoryID()%>&amp;sFieldNameDisp=<%=WorkHistory.getEnglish("Address2")%>&amp;sTableNameDisp=tWorkHistory','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((WorkHistory.isRead("Address2",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Address 2&nbsp;</b></p></td><td valign=top><p><%=WorkHistory.getAddress2()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Address2&amp;sTableName=tWorkHistory&amp;sRefID=<%=WorkHistory.getWorkHistoryID()%>&amp;sFieldNameDisp=<%=WorkHistory.getEnglish("Address2")%>&amp;sTableNameDisp=tWorkHistory','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (WorkHistory.isRequired("City",UserSecurityGroupID))&&(!WorkHistory.isComplete("City")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((WorkHistory.isExpired("City",expiredDays))&&(WorkHistory.isExpiredCheck("City",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((WorkHistory.isWrite("City",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>City&nbsp;</b></p></td><td valign=top><p><input maxlength="30" type=text size="80" name="City_<%=iExpress%>" value="<%=WorkHistory.getCity()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=City&amp;sTableName=tWorkHistory&amp;sRefID=<%=WorkHistory.getWorkHistoryID()%>&amp;sFieldNameDisp=<%=WorkHistory.getEnglish("City")%>&amp;sTableNameDisp=tWorkHistory','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((WorkHistory.isRead("City",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>City&nbsp;</b></p></td><td valign=top><p><%=WorkHistory.getCity()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=City&amp;sTableName=tWorkHistory&amp;sRefID=<%=WorkHistory.getWorkHistoryID()%>&amp;sFieldNameDisp=<%=WorkHistory.getEnglish("City")%>&amp;sTableNameDisp=tWorkHistory','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (WorkHistory.isRequired("StateID",UserSecurityGroupID))&&(!WorkHistory.isComplete("StateID")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((WorkHistory.isExpired("StateID",expiredDays))&&(WorkHistory.isExpiredCheck("StateID",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((WorkHistory.isWrite("StateID",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>State&nbsp;</b></p></td><td valign=top><p><select   name="StateID_<%=iExpress%>" ><jsp:include page="../generic/tStateLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=WorkHistory.getStateID()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=StateID&amp;sTableName=tWorkHistory&amp;sRefID=<%=WorkHistory.getWorkHistoryID()%>&amp;sFieldNameDisp=<%=WorkHistory.getEnglish("StateID")%>&amp;sTableNameDisp=tWorkHistory','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((WorkHistory.isRead("StateID",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>State&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tStateLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=WorkHistory.getStateID()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=StateID&amp;sTableName=tWorkHistory&amp;sRefID=<%=WorkHistory.getWorkHistoryID()%>&amp;sFieldNameDisp=<%=WorkHistory.getEnglish("StateID")%>&amp;sTableNameDisp=tWorkHistory','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (WorkHistory.isRequired("Province",UserSecurityGroupID))&&(!WorkHistory.isComplete("Province")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((WorkHistory.isExpired("Province",expiredDays))&&(WorkHistory.isExpiredCheck("Province",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((WorkHistory.isWrite("Province",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Province, District, State&nbsp;</b></p></td><td valign=top><p><input maxlength="100" type=text size="80" name="Province_<%=iExpress%>" value="<%=WorkHistory.getProvince()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Province&amp;sTableName=tWorkHistory&amp;sRefID=<%=WorkHistory.getWorkHistoryID()%>&amp;sFieldNameDisp=<%=WorkHistory.getEnglish("Province")%>&amp;sTableNameDisp=tWorkHistory','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((WorkHistory.isRead("Province",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Province, District, State&nbsp;</b></p></td><td valign=top><p><%=WorkHistory.getProvince()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Province&amp;sTableName=tWorkHistory&amp;sRefID=<%=WorkHistory.getWorkHistoryID()%>&amp;sFieldNameDisp=<%=WorkHistory.getEnglish("Province")%>&amp;sTableNameDisp=tWorkHistory','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (WorkHistory.isRequired("ZIP",UserSecurityGroupID))&&(!WorkHistory.isComplete("ZIP")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((WorkHistory.isExpired("ZIP",expiredDays))&&(WorkHistory.isExpiredCheck("ZIP",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((WorkHistory.isWrite("ZIP",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>ZIP&nbsp;</b></p></td><td valign=top><p><input maxlength="50" type=text size="80" name="ZIP_<%=iExpress%>" value="<%=WorkHistory.getZIP()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ZIP&amp;sTableName=tWorkHistory&amp;sRefID=<%=WorkHistory.getWorkHistoryID()%>&amp;sFieldNameDisp=<%=WorkHistory.getEnglish("ZIP")%>&amp;sTableNameDisp=tWorkHistory','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((WorkHistory.isRead("ZIP",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>ZIP&nbsp;</b></p></td><td valign=top><p><%=WorkHistory.getZIP()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ZIP&amp;sTableName=tWorkHistory&amp;sRefID=<%=WorkHistory.getWorkHistoryID()%>&amp;sFieldNameDisp=<%=WorkHistory.getEnglish("ZIP")%>&amp;sTableNameDisp=tWorkHistory','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (WorkHistory.isRequired("CountryID",UserSecurityGroupID))&&(!WorkHistory.isComplete("CountryID")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((WorkHistory.isExpired("CountryID",expiredDays))&&(WorkHistory.isExpiredCheck("CountryID",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((WorkHistory.isWrite("CountryID",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Country&nbsp;</b></p></td><td valign=top><p><select   name="CountryID_<%=iExpress%>" ><jsp:include page="../generic/tCountryLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=WorkHistory.getCountryID()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CountryID&amp;sTableName=tWorkHistory&amp;sRefID=<%=WorkHistory.getWorkHistoryID()%>&amp;sFieldNameDisp=<%=WorkHistory.getEnglish("CountryID")%>&amp;sTableNameDisp=tWorkHistory','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((WorkHistory.isRead("CountryID",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Country&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tCountryLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=WorkHistory.getCountryID()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CountryID&amp;sTableName=tWorkHistory&amp;sRefID=<%=WorkHistory.getWorkHistoryID()%>&amp;sFieldNameDisp=<%=WorkHistory.getEnglish("CountryID")%>&amp;sTableNameDisp=tWorkHistory','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (WorkHistory.isRequired("Phone",UserSecurityGroupID))&&(!WorkHistory.isComplete("Phone")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((WorkHistory.isExpired("Phone",expiredDays))&&(WorkHistory.isExpiredCheck("Phone",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((WorkHistory.isWrite("Phone",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Phone (XXX-XXX-XXXX)&nbsp;</b></p></td><td valign=top><p><input maxlength="50" type=text size="80" name="Phone_<%=iExpress%>" value="<%=WorkHistory.getPhone()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Phone&amp;sTableName=tWorkHistory&amp;sRefID=<%=WorkHistory.getWorkHistoryID()%>&amp;sFieldNameDisp=<%=WorkHistory.getEnglish("Phone")%>&amp;sTableNameDisp=tWorkHistory','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((WorkHistory.isRead("Phone",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Phone (XXX-XXX-XXXX)&nbsp;</b></p></td><td valign=top><p><%=WorkHistory.getPhone()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Phone&amp;sTableName=tWorkHistory&amp;sRefID=<%=WorkHistory.getWorkHistoryID()%>&amp;sFieldNameDisp=<%=WorkHistory.getEnglish("Phone")%>&amp;sTableNameDisp=tWorkHistory','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (WorkHistory.isRequired("Fax",UserSecurityGroupID))&&(!WorkHistory.isComplete("Fax")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((WorkHistory.isExpired("Fax",expiredDays))&&(WorkHistory.isExpiredCheck("Fax",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                     <input type=hidden name="Fax_<%=iExpress%>" value="<%=WorkHistory.getFax()%>">

            <%
            if ( (WorkHistory.isRequired("ContactName",UserSecurityGroupID))&&(!WorkHistory.isComplete("ContactName")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((WorkHistory.isExpired("ContactName",expiredDays))&&(WorkHistory.isExpiredCheck("ContactName",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                     <input type=hidden name="ContactName_<%=iExpress%>" value="<%=WorkHistory.getContactName()%>">

            <%
            if ( (WorkHistory.isRequired("ContactEmail",UserSecurityGroupID))&&(!WorkHistory.isComplete("ContactEmail")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((WorkHistory.isExpired("ContactEmail",expiredDays))&&(WorkHistory.isExpiredCheck("ContactEmail",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                     <input type=hidden name="ContactEmail_<%=iExpress%>" value="<%=WorkHistory.getContactEmail()%>">

            <%
            if ( (WorkHistory.isRequired("Comments",UserSecurityGroupID))&&(!WorkHistory.isComplete("Comments")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((WorkHistory.isExpired("Comments",expiredDays))&&(WorkHistory.isExpiredCheck("Comments",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                     <input type=hidden name="Comments_<%=iExpress%>" value="<%=WorkHistory.getComments()%>">


            <tr><td width=40%>&nbsp;</td><td width=60%>&nbsp;</td></tr>

            <input type=hidden name=routePageReference value="sParentReturnPage">

        </td></tr></table>
        </td></tr></table>
        <%
  }%>

        </td></tr></table></table><br>        <%
    }
    %>
        <input <%=HTMLFormStyleButton%> onClick="document.forms[0].nextPage.value='<%=ConfigurationMessages.getExpressLinkRaw("tWorkHistory","next",iExpressMode)%>'" type=Submit value="Continue" name=Submit>
        <input type=hidden name=nextPage value="">
        </form>

    </table>
    <%
  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>

    </td></tr></table>


<%String bnIncludeFN = "ui_"+thePLCID+"\\bot-nav_PhysicianID.jsp";
%>
<jsp:include page="<%=bnIncludeFN%>" flush="true" ><jsp:param name="plcID" value="<%=thePLCID%>"/></jsp:include>
