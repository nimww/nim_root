<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.bltDocumentManagement,java.util.Vector,java.util.Enumeration,com.winstaff.ListElement, com.winstaff.PLCUtils, com.winstaff.SecurityCheck, com.winstaff.ConfigurationMessages,com.winstaff.bltPracticeMaster,com.winstaff.bltPhysicianPracticeLU,com.winstaff.bltPhysicianPracticeLU_List_LU_PhysicianID" %>
<%/*
    filename: out\jsp\tPhysicianPracticeLU_main_PracticeMaster_PhysicianID.jsp
    Created on Mar/03/2003
    Type: n-n main class file
    Created by: Scott Ellis
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>

<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_PhysicianID.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>

    <table cellpadding=0 cellspacing=0 border=0 width=<%=MasterTableWidth%> >
    <tr><td width=10>&nbsp;</td><td>

    <span class=title>Physician Practice Link</span><br>


<%
//initial declaration of list class and parentID
    Integer        iPhysicianID        =    null;
    boolean accessValid = false;
   Integer iSecurityCheck = SecurityCheck.CheckItem("Practice1", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {
    if (pageControllerHash.containsKey("iPhysicianID")) 
    {
        iPhysicianID        =    (Integer)pageControllerHash.get("iPhysicianID");
        accessValid = true;
    }
  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);

      java.text.SimpleDateFormat displayDateLongSDF = new java.text.SimpleDateFormat("MMMM dd yyyy");
    pageControllerHash.put("sParentReturnPage","tPhysicianPracticeLU_main_PracticeMaster_PhysicianID.jsp");
    pageControllerHash.remove("iLookupID");
    pageControllerHash.remove("iPracticeID");
    session.setAttribute("pageControllerHash",pageControllerHash);
    bltPhysicianPracticeLU_List_LU_PhysicianID        bltPhysicianPracticeLU_List_LU_PhysicianID        =    new    bltPhysicianPracticeLU_List_LU_PhysicianID(iPhysicianID);

//declaration of Enumeration
    bltPhysicianPracticeLU        working_bltPhysicianPracticeLU;
    ListElement         leCurrentElement;
    java.util.Enumeration eList = bltPhysicianPracticeLU_List_LU_PhysicianID.elements();
    %>
        <%@ include file="tPhysicianPracticeLU_main_PracticeMaster_PhysicianID_instructions.jsp" %>

        <%if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {%>
        <a href = "tPhysicianPracticeLU_main_LU_PhysicianID_form_create.jsp?EDIT=edit&KM=p"><img border=0 src="ui_<%=thePLCID%>/icons/add_PhysicianID.gif"></a>
        <a href = "tPhysicianPracticeLU_main_PracticeMaster_PhysicianID_form_create.jsp?EDIT=new"&EDIT=edit"><img border=0 src="ui_<%=thePLCID%>/icons/create_PhysicianID.gif"></a>

        <%}%>
    <%
    while (eList.hasMoreElements())
    {         %>
         <table border="0" bordercolor="333333" cellpadding="3" class=tdHeaderAlt cellspacing="0" width="100%">
            <tr>
              <td width="50%">
                   <table border="0" bordercolor="333333" cellpadding="3" class=tdHeaderAlt cellspacing="0" width="100%">
                     <tr> 
                       <td>
         <%

        leCurrentElement    = (ListElement) eList.nextElement();
        working_bltPhysicianPracticeLU  = (bltPhysicianPracticeLU) leCurrentElement.getObject();
        bltPracticeMaster working_bltPracticeMaster  = new bltPracticeMaster(working_bltPhysicianPracticeLU.getPracticeID());


        String theClass = "tdBase";
        if (!working_bltPracticeMaster.isComplete())
        {
            theClass = "incompleteItem";
        %>
                <span class=incompleteItem><b>This item is incomplete</b></span><br>
        <%
        }
        %>

               <span class=<%=theClass%> ><b>ID:&nbsp;</b><%=working_bltPhysicianPracticeLU.getPracticeID()%> </span>
                  </td></tr>
                  <tr><td><b>Item Create Date:&nbsp;</b><jsp:include page="../generic/DateTypeConvert_Main.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(working_bltPhysicianPracticeLU.getUniqueCreateDate())%>" /></jsp:include></td></tr>
                  <tr><td><b>Item Modify Date:&nbsp;</b><jsp:include page="../generic/DateTypeConvert_Main.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(working_bltPhysicianPracticeLU.getUniqueModifyDate())%>" /></jsp:include></td></tr>
                  <tr><td><b>Item Modify Comments:&nbsp;</b><%=working_bltPhysicianPracticeLU.getUniqueModifyComments()%></td></tr>
                  </td></tr></table>
            </td><td width="50%"> 
        <a class=linkBase  href = "tPhysicianPracticeLU_main_PracticeMaster_PhysicianID_form_authorize.jsp?EDIT=edit&EDITID=<%=working_bltPhysicianPracticeLU.getPracticeID()%>"&EDIT=edit"><img border=0 src="ui_<%=thePLCID%>/icons/edit_PhysicianID.gif"></a>

        <%if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {%>

        <br><a class=linkBase  href = "tPhysicianPracticeLU_main_LU_PhysicianID_form_authorize.jsp?EDIT=edit&KM=p&EDITID=<%=working_bltPhysicianPracticeLU.getLookupID()%>"><img border=0 src="ui_<%=thePLCID%>/icons/modify_PhysicianID.gif"></a>
        <br><a class=linkBase  onClick="return confirmDelete()"   href = "tPhysicianPracticeLU_main_LU_PhysicianID_form_authorize.jsp?EDIT=del&EDITID=<%=working_bltPhysicianPracticeLU.getLookupID()%>&KM=p"><img border=0 src="ui_<%=thePLCID%>/icons/remove_PhysicianID.gif"></a>
        <%}%>
                  </td></tr>
                 </table>
                 <table width="100%" border="1" cellspacing="0" bordercolor="#333333">
                  <tr>
                   <td>
                     <table width="100%" border="0" cellspacing="0">
                     <tr><td>

<%String theClassF = "textBase";%>

<%theClassF = "textBase";%>
<%if ((working_bltPracticeMaster.isExpired("PracticeName",expiredDays))&&(working_bltPracticeMaster.isExpiredCheck("PracticeName"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeMaster.isRequired("PracticeName"))&&(!working_bltPracticeMaster.isComplete("PracticeName")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Practice Name:&nbsp;</b><%=working_bltPracticeMaster.getPracticeName()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltPracticeMaster.isExpired("DepartmentName",expiredDays))&&(working_bltPracticeMaster.isExpiredCheck("DepartmentName"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeMaster.isRequired("DepartmentName"))&&(!working_bltPracticeMaster.isComplete("DepartmentName")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Department Name:&nbsp;</b><%=working_bltPracticeMaster.getDepartmentName()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltPracticeMaster.isExpired("TypeOfPractice",expiredDays))&&(working_bltPracticeMaster.isExpiredCheck("TypeOfPractice"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeMaster.isRequired("TypeOfPractice"))&&(!working_bltPracticeMaster.isComplete("TypeOfPractice")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Type of Practice:&nbsp;</b><jsp:include page="../generic/tPracticeTypeLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltPracticeMaster.getTypeOfPractice()%>" /></jsp:include></p>

<%theClassF = "textBase";%>
<%if ((working_bltPracticeMaster.isExpired("OfficeFederalTaxID",expiredDays))&&(working_bltPracticeMaster.isExpiredCheck("OfficeFederalTaxID"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeMaster.isRequired("OfficeFederalTaxID"))&&(!working_bltPracticeMaster.isComplete("OfficeFederalTaxID")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Office Federal Tax ID:&nbsp;</b><%=working_bltPracticeMaster.getOfficeFederalTaxID()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltPracticeMaster.isExpired("OfficeTaxIDNameAffiliation",expiredDays))&&(working_bltPracticeMaster.isExpiredCheck("OfficeTaxIDNameAffiliation"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeMaster.isRequired("OfficeTaxIDNameAffiliation"))&&(!working_bltPracticeMaster.isComplete("OfficeTaxIDNameAffiliation")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Name affiliated with tax ID:&nbsp;</b><%=working_bltPracticeMaster.getOfficeTaxIDNameAffiliation()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltPracticeMaster.isExpired("OfficeAddress1",expiredDays))&&(working_bltPracticeMaster.isExpiredCheck("OfficeAddress1"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeMaster.isRequired("OfficeAddress1"))&&(!working_bltPracticeMaster.isComplete("OfficeAddress1")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Office Address:&nbsp;</b><%=working_bltPracticeMaster.getOfficeAddress1()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltPracticeMaster.isExpired("OfficeAddress2",expiredDays))&&(working_bltPracticeMaster.isExpiredCheck("OfficeAddress2"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeMaster.isRequired("OfficeAddress2"))&&(!working_bltPracticeMaster.isComplete("OfficeAddress2")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Address 2:&nbsp;</b><%=working_bltPracticeMaster.getOfficeAddress2()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltPracticeMaster.isExpired("OfficeCity",expiredDays))&&(working_bltPracticeMaster.isExpiredCheck("OfficeCity"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeMaster.isRequired("OfficeCity"))&&(!working_bltPracticeMaster.isComplete("OfficeCity")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>City, Town, Province:&nbsp;</b><%=working_bltPracticeMaster.getOfficeCity()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltPracticeMaster.isExpired("OfficeStateID",expiredDays))&&(working_bltPracticeMaster.isExpiredCheck("OfficeStateID"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeMaster.isRequired("OfficeStateID"))&&(!working_bltPracticeMaster.isComplete("OfficeStateID")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>State:&nbsp;</b><jsp:include page="../generic/tStateLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltPracticeMaster.getOfficeStateID()%>" /></jsp:include></p>

<%theClassF = "textBase";%>
<%if ((working_bltPracticeMaster.isExpired("OfficeProvince",expiredDays))&&(working_bltPracticeMaster.isExpiredCheck("OfficeProvince"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeMaster.isRequired("OfficeProvince"))&&(!working_bltPracticeMaster.isComplete("OfficeProvince")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Province, District, State:&nbsp;</b><%=working_bltPracticeMaster.getOfficeProvince()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltPracticeMaster.isExpired("OfficeZIP",expiredDays))&&(working_bltPracticeMaster.isExpiredCheck("OfficeZIP"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeMaster.isRequired("OfficeZIP"))&&(!working_bltPracticeMaster.isComplete("OfficeZIP")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>ZIP:&nbsp;</b><%=working_bltPracticeMaster.getOfficeZIP()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltPracticeMaster.isExpired("OfficeCountryID",expiredDays))&&(working_bltPracticeMaster.isExpiredCheck("OfficeCountryID"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeMaster.isRequired("OfficeCountryID"))&&(!working_bltPracticeMaster.isComplete("OfficeCountryID")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Country:&nbsp;</b><jsp:include page="../generic/tCountryLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltPracticeMaster.getOfficeCountryID()%>" /></jsp:include></p>

<%theClassF = "textBase";%>
<%if ((working_bltPracticeMaster.isExpired("OfficePhone",expiredDays))&&(working_bltPracticeMaster.isExpiredCheck("OfficePhone"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeMaster.isRequired("OfficePhone"))&&(!working_bltPracticeMaster.isComplete("OfficePhone")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Office Phone Number (XXX-XXX-XXXX):&nbsp;</b><%=working_bltPracticeMaster.getOfficePhone()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltPracticeMaster.isExpired("BackOfficePhoneNo",expiredDays))&&(working_bltPracticeMaster.isExpiredCheck("BackOfficePhoneNo"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeMaster.isRequired("BackOfficePhoneNo"))&&(!working_bltPracticeMaster.isComplete("BackOfficePhoneNo")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Back Office Phone (if applicable) (XXX-XXX-XXXX):&nbsp;</b><%=working_bltPracticeMaster.getBackOfficePhoneNo()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltPracticeMaster.isExpired("OfficeFaxNo",expiredDays))&&(working_bltPracticeMaster.isExpiredCheck("OfficeFaxNo"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeMaster.isRequired("OfficeFaxNo"))&&(!working_bltPracticeMaster.isComplete("OfficeFaxNo")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Office Fax:&nbsp;</b><%=working_bltPracticeMaster.getOfficeFaxNo()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltPracticeMaster.isExpired("OfficeEmail",expiredDays))&&(working_bltPracticeMaster.isExpiredCheck("OfficeEmail"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeMaster.isRequired("OfficeEmail"))&&(!working_bltPracticeMaster.isComplete("OfficeEmail")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Office Email:&nbsp;</b><%=working_bltPracticeMaster.getOfficeEmail()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltPracticeMaster.isExpired("OfficeManagerFirstName",expiredDays))&&(working_bltPracticeMaster.isExpiredCheck("OfficeManagerFirstName"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeMaster.isRequired("OfficeManagerFirstName"))&&(!working_bltPracticeMaster.isComplete("OfficeManagerFirstName")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Office Manager First Name:&nbsp;</b><%=working_bltPracticeMaster.getOfficeManagerFirstName()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltPracticeMaster.isExpired("OfficeManagerLastName",expiredDays))&&(working_bltPracticeMaster.isExpiredCheck("OfficeManagerLastName"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeMaster.isRequired("OfficeManagerLastName"))&&(!working_bltPracticeMaster.isComplete("OfficeManagerLastName")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Office Manager Last Name:&nbsp;</b><%=working_bltPracticeMaster.getOfficeManagerLastName()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltPracticeMaster.isExpired("OfficeManagerPhone",expiredDays))&&(working_bltPracticeMaster.isExpiredCheck("OfficeManagerPhone"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeMaster.isRequired("OfficeManagerPhone"))&&(!working_bltPracticeMaster.isComplete("OfficeManagerPhone")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Office Manager Phone (XXX-XXX-XXXX):&nbsp;</b><%=working_bltPracticeMaster.getOfficeManagerPhone()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltPracticeMaster.isExpired("OfficeManagerEmail",expiredDays))&&(working_bltPracticeMaster.isExpiredCheck("OfficeManagerEmail"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeMaster.isRequired("OfficeManagerEmail"))&&(!working_bltPracticeMaster.isComplete("OfficeManagerEmail")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Office Manager Email:&nbsp;</b><%=working_bltPracticeMaster.getOfficeManagerEmail()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltPracticeMaster.isExpired("AnsweringService",expiredDays))&&(working_bltPracticeMaster.isExpiredCheck("AnsweringService"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeMaster.isRequired("AnsweringService"))&&(!working_bltPracticeMaster.isComplete("AnsweringService")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Do you have an answering service?:&nbsp;</b><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltPracticeMaster.getAnsweringService()%>" /></jsp:include></p>

<%theClassF = "textBase";%>
<%if ((working_bltPracticeMaster.isExpired("AnsweringServicePhone",expiredDays))&&(working_bltPracticeMaster.isExpiredCheck("AnsweringServicePhone"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeMaster.isRequired("AnsweringServicePhone"))&&(!working_bltPracticeMaster.isComplete("AnsweringServicePhone")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Answering Service Phone (XXX-XXX-XXXX):&nbsp;</b><%=working_bltPracticeMaster.getAnsweringServicePhone()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltPracticeMaster.isExpired("Coverage247",expiredDays))&&(working_bltPracticeMaster.isExpiredCheck("Coverage247"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeMaster.isRequired("Coverage247"))&&(!working_bltPracticeMaster.isComplete("Coverage247")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Do you provide 24-7 coverage?:&nbsp;</b><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltPracticeMaster.getCoverage247()%>" /></jsp:include></p>

<%theClassF = "textBase";%>
<%if ((working_bltPracticeMaster.isExpired("MinorityEnterprise",expiredDays))&&(working_bltPracticeMaster.isExpiredCheck("MinorityEnterprise"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeMaster.isRequired("MinorityEnterprise"))&&(!working_bltPracticeMaster.isComplete("MinorityEnterprise")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Does your office qualify as a minority business enterprise? :&nbsp;</b><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltPracticeMaster.getMinorityEnterprise()%>" /></jsp:include></p>

<%theClassF = "textBase";%>
<%if ((working_bltPracticeMaster.isExpired("LanguagesSpokenInOffice",expiredDays))&&(working_bltPracticeMaster.isExpiredCheck("LanguagesSpokenInOffice"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeMaster.isRequired("LanguagesSpokenInOffice"))&&(!working_bltPracticeMaster.isComplete("LanguagesSpokenInOffice")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Languages Spoken In Office:&nbsp;</b><%=working_bltPracticeMaster.getLanguagesSpokenInOffice()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltPracticeMaster.isExpired("AcceptAllNewPatients",expiredDays))&&(working_bltPracticeMaster.isExpiredCheck("AcceptAllNewPatients"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeMaster.isRequired("AcceptAllNewPatients"))&&(!working_bltPracticeMaster.isComplete("AcceptAllNewPatients")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Do you accept all new patients?:&nbsp;</b><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltPracticeMaster.getAcceptAllNewPatients()%>" /></jsp:include></p>

<%theClassF = "textBase";%>
<%if ((working_bltPracticeMaster.isExpired("AcceptExistingPayorChange",expiredDays))&&(working_bltPracticeMaster.isExpiredCheck("AcceptExistingPayorChange"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeMaster.isRequired("AcceptExistingPayorChange"))&&(!working_bltPracticeMaster.isComplete("AcceptExistingPayorChange")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Do you accept existing patients with change of payer?:&nbsp;</b><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltPracticeMaster.getAcceptExistingPayorChange()%>" /></jsp:include></p>

<%theClassF = "textBase";%>
<%if ((working_bltPracticeMaster.isExpired("AcceptNewFromReferralOnly",expiredDays))&&(working_bltPracticeMaster.isExpiredCheck("AcceptNewFromReferralOnly"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeMaster.isRequired("AcceptNewFromReferralOnly"))&&(!working_bltPracticeMaster.isComplete("AcceptNewFromReferralOnly")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Do you accept from referrals only?:&nbsp;</b><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltPracticeMaster.getAcceptNewFromReferralOnly()%>" /></jsp:include></p>

<%theClassF = "textBase";%>
<%if ((working_bltPracticeMaster.isExpired("AcceptNewMedicare",expiredDays))&&(working_bltPracticeMaster.isExpiredCheck("AcceptNewMedicare"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeMaster.isRequired("AcceptNewMedicare"))&&(!working_bltPracticeMaster.isComplete("AcceptNewMedicare")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Do you accept new Medicare patients?:&nbsp;</b><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltPracticeMaster.getAcceptNewMedicare()%>" /></jsp:include></p>

<%theClassF = "textBase";%>
<%if ((working_bltPracticeMaster.isExpired("AcceptNewMedicaid",expiredDays))&&(working_bltPracticeMaster.isExpiredCheck("AcceptNewMedicaid"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeMaster.isRequired("AcceptNewMedicaid"))&&(!working_bltPracticeMaster.isComplete("AcceptNewMedicaid")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Do you accept new Medicaid patients?:&nbsp;</b><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltPracticeMaster.getAcceptNewMedicaid()%>" /></jsp:include></p>

<%theClassF = "textBase";%>
<%if ((working_bltPracticeMaster.isExpired("PracticeLimitationAge",expiredDays))&&(working_bltPracticeMaster.isExpiredCheck("PracticeLimitationAge"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeMaster.isRequired("PracticeLimitationAge"))&&(!working_bltPracticeMaster.isComplete("PracticeLimitationAge")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Patient age limitations:&nbsp;</b><%=working_bltPracticeMaster.getPracticeLimitationAge()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltPracticeMaster.isExpired("PracticeLimitationSex",expiredDays))&&(working_bltPracticeMaster.isExpiredCheck("PracticeLimitationSex"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeMaster.isRequired("PracticeLimitationSex"))&&(!working_bltPracticeMaster.isComplete("PracticeLimitationSex")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Patient gender limitations:&nbsp;</b><%=working_bltPracticeMaster.getPracticeLimitationSex()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltPracticeMaster.isExpired("PracticeLimitationOther",expiredDays))&&(working_bltPracticeMaster.isExpiredCheck("PracticeLimitationOther"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeMaster.isRequired("PracticeLimitationOther"))&&(!working_bltPracticeMaster.isComplete("PracticeLimitationOther")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Other practice limitations:&nbsp;</b><%=working_bltPracticeMaster.getPracticeLimitationOther()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltPracticeMaster.isExpired("BillingPayableTo",expiredDays))&&(working_bltPracticeMaster.isExpiredCheck("BillingPayableTo"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeMaster.isRequired("BillingPayableTo"))&&(!working_bltPracticeMaster.isComplete("BillingPayableTo")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Payable To:&nbsp;</b><%=working_bltPracticeMaster.getBillingPayableTo()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltPracticeMaster.isExpired("BillingFirstName",expiredDays))&&(working_bltPracticeMaster.isExpiredCheck("BillingFirstName"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeMaster.isRequired("BillingFirstName"))&&(!working_bltPracticeMaster.isComplete("BillingFirstName")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>First Name:&nbsp;</b><%=working_bltPracticeMaster.getBillingFirstName()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltPracticeMaster.isExpired("BillingLastName",expiredDays))&&(working_bltPracticeMaster.isExpiredCheck("BillingLastName"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeMaster.isRequired("BillingLastName"))&&(!working_bltPracticeMaster.isComplete("BillingLastName")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Last Name:&nbsp;</b><%=working_bltPracticeMaster.getBillingLastName()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltPracticeMaster.isExpired("BillingAddress1",expiredDays))&&(working_bltPracticeMaster.isExpiredCheck("BillingAddress1"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeMaster.isRequired("BillingAddress1"))&&(!working_bltPracticeMaster.isComplete("BillingAddress1")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Address:&nbsp;</b><%=working_bltPracticeMaster.getBillingAddress1()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltPracticeMaster.isExpired("BillingAddress2",expiredDays))&&(working_bltPracticeMaster.isExpiredCheck("BillingAddress2"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeMaster.isRequired("BillingAddress2"))&&(!working_bltPracticeMaster.isComplete("BillingAddress2")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Address 2:&nbsp;</b><%=working_bltPracticeMaster.getBillingAddress2()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltPracticeMaster.isExpired("BillingCity",expiredDays))&&(working_bltPracticeMaster.isExpiredCheck("BillingCity"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeMaster.isRequired("BillingCity"))&&(!working_bltPracticeMaster.isComplete("BillingCity")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>City:&nbsp;</b><%=working_bltPracticeMaster.getBillingCity()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltPracticeMaster.isExpired("BillingStateID",expiredDays))&&(working_bltPracticeMaster.isExpiredCheck("BillingStateID"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeMaster.isRequired("BillingStateID"))&&(!working_bltPracticeMaster.isComplete("BillingStateID")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>State:&nbsp;</b><jsp:include page="../generic/tStateLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltPracticeMaster.getBillingStateID()%>" /></jsp:include></p>

<%theClassF = "textBase";%>
<%if ((working_bltPracticeMaster.isExpired("BillingProvince",expiredDays))&&(working_bltPracticeMaster.isExpiredCheck("BillingProvince"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeMaster.isRequired("BillingProvince"))&&(!working_bltPracticeMaster.isComplete("BillingProvince")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Province, District, State:&nbsp;</b><%=working_bltPracticeMaster.getBillingProvince()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltPracticeMaster.isExpired("BillingZIP",expiredDays))&&(working_bltPracticeMaster.isExpiredCheck("BillingZIP"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeMaster.isRequired("BillingZIP"))&&(!working_bltPracticeMaster.isComplete("BillingZIP")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>ZIP:&nbsp;</b><%=working_bltPracticeMaster.getBillingZIP()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltPracticeMaster.isExpired("BillingCountryID",expiredDays))&&(working_bltPracticeMaster.isExpiredCheck("BillingCountryID"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeMaster.isRequired("BillingCountryID"))&&(!working_bltPracticeMaster.isComplete("BillingCountryID")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Country:&nbsp;</b><jsp:include page="../generic/tCountryLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltPracticeMaster.getBillingCountryID()%>" /></jsp:include></p>

<%theClassF = "textBase";%>
<%if ((working_bltPracticeMaster.isExpired("BillingPhone",expiredDays))&&(working_bltPracticeMaster.isExpiredCheck("BillingPhone"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeMaster.isRequired("BillingPhone"))&&(!working_bltPracticeMaster.isComplete("BillingPhone")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Phone (XXX-XXX-XXXX):&nbsp;</b><%=working_bltPracticeMaster.getBillingPhone()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltPracticeMaster.isExpired("BillingFax",expiredDays))&&(working_bltPracticeMaster.isExpiredCheck("BillingFax"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeMaster.isRequired("BillingFax"))&&(!working_bltPracticeMaster.isComplete("BillingFax")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Fax (XXX-XXX-XXXX):&nbsp;</b><%=working_bltPracticeMaster.getBillingFax()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltPracticeMaster.isExpired("CredentialingContactFirstName",expiredDays))&&(working_bltPracticeMaster.isExpiredCheck("CredentialingContactFirstName"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeMaster.isRequired("CredentialingContactFirstName"))&&(!working_bltPracticeMaster.isComplete("CredentialingContactFirstName")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>First Name:&nbsp;</b><%=working_bltPracticeMaster.getCredentialingContactFirstName()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltPracticeMaster.isExpired("CredentialingContactLastName",expiredDays))&&(working_bltPracticeMaster.isExpiredCheck("CredentialingContactLastName"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeMaster.isRequired("CredentialingContactLastName"))&&(!working_bltPracticeMaster.isComplete("CredentialingContactLastName")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Last Name:&nbsp;</b><%=working_bltPracticeMaster.getCredentialingContactLastName()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltPracticeMaster.isExpired("CredentialingContactPhone",expiredDays))&&(working_bltPracticeMaster.isExpiredCheck("CredentialingContactPhone"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeMaster.isRequired("CredentialingContactPhone"))&&(!working_bltPracticeMaster.isComplete("CredentialingContactPhone")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Phone (XXX-XXX-XXXX):&nbsp;</b><%=working_bltPracticeMaster.getCredentialingContactPhone()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltPracticeMaster.isExpired("CredentialingContactFax",expiredDays))&&(working_bltPracticeMaster.isExpiredCheck("CredentialingContactFax"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeMaster.isRequired("CredentialingContactFax"))&&(!working_bltPracticeMaster.isComplete("CredentialingContactFax")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Fax (XXX-XXX-XXXX):&nbsp;</b><%=working_bltPracticeMaster.getCredentialingContactFax()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltPracticeMaster.isExpired("CredentiallingContactEmail",expiredDays))&&(working_bltPracticeMaster.isExpiredCheck("CredentiallingContactEmail"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeMaster.isRequired("CredentiallingContactEmail"))&&(!working_bltPracticeMaster.isComplete("CredentiallingContactEmail")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Email:&nbsp;</b><%=working_bltPracticeMaster.getCredentiallingContactEmail()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltPracticeMaster.isExpired("OfficeWorkHoursOpenMonday",expiredDays))&&(working_bltPracticeMaster.isExpiredCheck("OfficeWorkHoursOpenMonday"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeMaster.isRequired("OfficeWorkHoursOpenMonday"))&&(!working_bltPracticeMaster.isComplete("OfficeWorkHoursOpenMonday")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Office hours Monday Open:&nbsp;</b><%=working_bltPracticeMaster.getOfficeWorkHoursOpenMonday()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltPracticeMaster.isExpired("OfficeWorkHoursOpenTuesday",expiredDays))&&(working_bltPracticeMaster.isExpiredCheck("OfficeWorkHoursOpenTuesday"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeMaster.isRequired("OfficeWorkHoursOpenTuesday"))&&(!working_bltPracticeMaster.isComplete("OfficeWorkHoursOpenTuesday")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Office hours Tuesday Open:&nbsp;</b><%=working_bltPracticeMaster.getOfficeWorkHoursOpenTuesday()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltPracticeMaster.isExpired("OfficeWorkHoursOpenWednesday",expiredDays))&&(working_bltPracticeMaster.isExpiredCheck("OfficeWorkHoursOpenWednesday"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeMaster.isRequired("OfficeWorkHoursOpenWednesday"))&&(!working_bltPracticeMaster.isComplete("OfficeWorkHoursOpenWednesday")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Office hours Wednesday Open:&nbsp;</b><%=working_bltPracticeMaster.getOfficeWorkHoursOpenWednesday()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltPracticeMaster.isExpired("OfficeWorkHoursOpenThursday",expiredDays))&&(working_bltPracticeMaster.isExpiredCheck("OfficeWorkHoursOpenThursday"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeMaster.isRequired("OfficeWorkHoursOpenThursday"))&&(!working_bltPracticeMaster.isComplete("OfficeWorkHoursOpenThursday")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Office hours Thursday Open:&nbsp;</b><%=working_bltPracticeMaster.getOfficeWorkHoursOpenThursday()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltPracticeMaster.isExpired("OfficeWorkHoursOpenFriday",expiredDays))&&(working_bltPracticeMaster.isExpiredCheck("OfficeWorkHoursOpenFriday"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeMaster.isRequired("OfficeWorkHoursOpenFriday"))&&(!working_bltPracticeMaster.isComplete("OfficeWorkHoursOpenFriday")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Office hours Friday Open:&nbsp;</b><%=working_bltPracticeMaster.getOfficeWorkHoursOpenFriday()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltPracticeMaster.isExpired("OfficeWorkHoursOpenSaturday",expiredDays))&&(working_bltPracticeMaster.isExpiredCheck("OfficeWorkHoursOpenSaturday"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeMaster.isRequired("OfficeWorkHoursOpenSaturday"))&&(!working_bltPracticeMaster.isComplete("OfficeWorkHoursOpenSaturday")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Office hours Saturday Open:&nbsp;</b><%=working_bltPracticeMaster.getOfficeWorkHoursOpenSaturday()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltPracticeMaster.isExpired("OfficeWorkHoursOpenSunday",expiredDays))&&(working_bltPracticeMaster.isExpiredCheck("OfficeWorkHoursOpenSunday"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeMaster.isRequired("OfficeWorkHoursOpenSunday"))&&(!working_bltPracticeMaster.isComplete("OfficeWorkHoursOpenSunday")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Office hours Sunday Open:&nbsp;</b><%=working_bltPracticeMaster.getOfficeWorkHoursOpenSunday()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltPracticeMaster.isExpired("OfficeWorkHoursCloseMonday",expiredDays))&&(working_bltPracticeMaster.isExpiredCheck("OfficeWorkHoursCloseMonday"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeMaster.isRequired("OfficeWorkHoursCloseMonday"))&&(!working_bltPracticeMaster.isComplete("OfficeWorkHoursCloseMonday")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Office hours Monday Close:&nbsp;</b><%=working_bltPracticeMaster.getOfficeWorkHoursCloseMonday()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltPracticeMaster.isExpired("OfficeWorkHoursCloseTuesday",expiredDays))&&(working_bltPracticeMaster.isExpiredCheck("OfficeWorkHoursCloseTuesday"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeMaster.isRequired("OfficeWorkHoursCloseTuesday"))&&(!working_bltPracticeMaster.isComplete("OfficeWorkHoursCloseTuesday")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Office hours Tuesday Close:&nbsp;</b><%=working_bltPracticeMaster.getOfficeWorkHoursCloseTuesday()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltPracticeMaster.isExpired("OfficeWorkHoursCloseWednesday",expiredDays))&&(working_bltPracticeMaster.isExpiredCheck("OfficeWorkHoursCloseWednesday"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeMaster.isRequired("OfficeWorkHoursCloseWednesday"))&&(!working_bltPracticeMaster.isComplete("OfficeWorkHoursCloseWednesday")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Office hours Wednesday Close:&nbsp;</b><%=working_bltPracticeMaster.getOfficeWorkHoursCloseWednesday()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltPracticeMaster.isExpired("OfficeWorkHoursCloseThursday",expiredDays))&&(working_bltPracticeMaster.isExpiredCheck("OfficeWorkHoursCloseThursday"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeMaster.isRequired("OfficeWorkHoursCloseThursday"))&&(!working_bltPracticeMaster.isComplete("OfficeWorkHoursCloseThursday")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Office hours Thursday Close:&nbsp;</b><%=working_bltPracticeMaster.getOfficeWorkHoursCloseThursday()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltPracticeMaster.isExpired("OfficeWorkHoursCloseFriday",expiredDays))&&(working_bltPracticeMaster.isExpiredCheck("OfficeWorkHoursCloseFriday"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeMaster.isRequired("OfficeWorkHoursCloseFriday"))&&(!working_bltPracticeMaster.isComplete("OfficeWorkHoursCloseFriday")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Office hours Friday Close:&nbsp;</b><%=working_bltPracticeMaster.getOfficeWorkHoursCloseFriday()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltPracticeMaster.isExpired("OfficeWorkHoursCloseSaturday",expiredDays))&&(working_bltPracticeMaster.isExpiredCheck("OfficeWorkHoursCloseSaturday"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeMaster.isRequired("OfficeWorkHoursCloseSaturday"))&&(!working_bltPracticeMaster.isComplete("OfficeWorkHoursCloseSaturday")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Office hours Saturday Close:&nbsp;</b><%=working_bltPracticeMaster.getOfficeWorkHoursCloseSaturday()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltPracticeMaster.isExpired("OfficeWorkHoursCloseSunday",expiredDays))&&(working_bltPracticeMaster.isExpiredCheck("OfficeWorkHoursCloseSunday"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeMaster.isRequired("OfficeWorkHoursCloseSunday"))&&(!working_bltPracticeMaster.isComplete("OfficeWorkHoursCloseSunday")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Office hours Sunday Close:&nbsp;</b><%=working_bltPracticeMaster.getOfficeWorkHoursCloseSunday()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltPracticeMaster.isExpired("OfficeAfterHoursOpenMonday",expiredDays))&&(working_bltPracticeMaster.isExpiredCheck("OfficeAfterHoursOpenMonday"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeMaster.isRequired("OfficeAfterHoursOpenMonday"))&&(!working_bltPracticeMaster.isComplete("OfficeAfterHoursOpenMonday")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>After Hours Monday Open:&nbsp;</b><%=working_bltPracticeMaster.getOfficeAfterHoursOpenMonday()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltPracticeMaster.isExpired("OfficeAfterHoursOpenTuesday",expiredDays))&&(working_bltPracticeMaster.isExpiredCheck("OfficeAfterHoursOpenTuesday"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeMaster.isRequired("OfficeAfterHoursOpenTuesday"))&&(!working_bltPracticeMaster.isComplete("OfficeAfterHoursOpenTuesday")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>After Hours Tuesday Open:&nbsp;</b><%=working_bltPracticeMaster.getOfficeAfterHoursOpenTuesday()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltPracticeMaster.isExpired("OfficeAfterHoursOpenWednesday",expiredDays))&&(working_bltPracticeMaster.isExpiredCheck("OfficeAfterHoursOpenWednesday"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeMaster.isRequired("OfficeAfterHoursOpenWednesday"))&&(!working_bltPracticeMaster.isComplete("OfficeAfterHoursOpenWednesday")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>After Hours Wednesday Open:&nbsp;</b><%=working_bltPracticeMaster.getOfficeAfterHoursOpenWednesday()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltPracticeMaster.isExpired("OfficeAfterHoursOpenThursday",expiredDays))&&(working_bltPracticeMaster.isExpiredCheck("OfficeAfterHoursOpenThursday"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeMaster.isRequired("OfficeAfterHoursOpenThursday"))&&(!working_bltPracticeMaster.isComplete("OfficeAfterHoursOpenThursday")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>After Hours Thursday Open:&nbsp;</b><%=working_bltPracticeMaster.getOfficeAfterHoursOpenThursday()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltPracticeMaster.isExpired("OfficeAfterHoursOpenFriday",expiredDays))&&(working_bltPracticeMaster.isExpiredCheck("OfficeAfterHoursOpenFriday"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeMaster.isRequired("OfficeAfterHoursOpenFriday"))&&(!working_bltPracticeMaster.isComplete("OfficeAfterHoursOpenFriday")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>After Hours Friday Open:&nbsp;</b><%=working_bltPracticeMaster.getOfficeAfterHoursOpenFriday()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltPracticeMaster.isExpired("OfficeAfterHoursOpenSaturday",expiredDays))&&(working_bltPracticeMaster.isExpiredCheck("OfficeAfterHoursOpenSaturday"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeMaster.isRequired("OfficeAfterHoursOpenSaturday"))&&(!working_bltPracticeMaster.isComplete("OfficeAfterHoursOpenSaturday")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>After Hours Saturday Open:&nbsp;</b><%=working_bltPracticeMaster.getOfficeAfterHoursOpenSaturday()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltPracticeMaster.isExpired("OfficeAfterHoursOpenSunday",expiredDays))&&(working_bltPracticeMaster.isExpiredCheck("OfficeAfterHoursOpenSunday"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeMaster.isRequired("OfficeAfterHoursOpenSunday"))&&(!working_bltPracticeMaster.isComplete("OfficeAfterHoursOpenSunday")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>After Hours Sunday Open:&nbsp;</b><%=working_bltPracticeMaster.getOfficeAfterHoursOpenSunday()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltPracticeMaster.isExpired("OfficeAfterHoursCloseMonday",expiredDays))&&(working_bltPracticeMaster.isExpiredCheck("OfficeAfterHoursCloseMonday"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeMaster.isRequired("OfficeAfterHoursCloseMonday"))&&(!working_bltPracticeMaster.isComplete("OfficeAfterHoursCloseMonday")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>After Hours Monday Close:&nbsp;</b><%=working_bltPracticeMaster.getOfficeAfterHoursCloseMonday()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltPracticeMaster.isExpired("OfficeAfterHoursCloseTuesday",expiredDays))&&(working_bltPracticeMaster.isExpiredCheck("OfficeAfterHoursCloseTuesday"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeMaster.isRequired("OfficeAfterHoursCloseTuesday"))&&(!working_bltPracticeMaster.isComplete("OfficeAfterHoursCloseTuesday")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>After Hours Tuesday Close:&nbsp;</b><%=working_bltPracticeMaster.getOfficeAfterHoursCloseTuesday()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltPracticeMaster.isExpired("OfficeAfterHoursCloseWednesday",expiredDays))&&(working_bltPracticeMaster.isExpiredCheck("OfficeAfterHoursCloseWednesday"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeMaster.isRequired("OfficeAfterHoursCloseWednesday"))&&(!working_bltPracticeMaster.isComplete("OfficeAfterHoursCloseWednesday")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>After Hours Wednesday Close:&nbsp;</b><%=working_bltPracticeMaster.getOfficeAfterHoursCloseWednesday()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltPracticeMaster.isExpired("OfficeAfterHoursCloseThursday",expiredDays))&&(working_bltPracticeMaster.isExpiredCheck("OfficeAfterHoursCloseThursday"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeMaster.isRequired("OfficeAfterHoursCloseThursday"))&&(!working_bltPracticeMaster.isComplete("OfficeAfterHoursCloseThursday")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>After Hours Thursday Close:&nbsp;</b><%=working_bltPracticeMaster.getOfficeAfterHoursCloseThursday()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltPracticeMaster.isExpired("OfficeAfterHoursCloseFriday",expiredDays))&&(working_bltPracticeMaster.isExpiredCheck("OfficeAfterHoursCloseFriday"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeMaster.isRequired("OfficeAfterHoursCloseFriday"))&&(!working_bltPracticeMaster.isComplete("OfficeAfterHoursCloseFriday")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>After Hours Friday Close:&nbsp;</b><%=working_bltPracticeMaster.getOfficeAfterHoursCloseFriday()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltPracticeMaster.isExpired("OfficeAfterHoursCloseSaturday",expiredDays))&&(working_bltPracticeMaster.isExpiredCheck("OfficeAfterHoursCloseSaturday"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeMaster.isRequired("OfficeAfterHoursCloseSaturday"))&&(!working_bltPracticeMaster.isComplete("OfficeAfterHoursCloseSaturday")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>After Hours Saturday Close:&nbsp;</b><%=working_bltPracticeMaster.getOfficeAfterHoursCloseSaturday()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltPracticeMaster.isExpired("OfficeAfterHoursCloseSunday",expiredDays))&&(working_bltPracticeMaster.isExpiredCheck("OfficeAfterHoursCloseSunday"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeMaster.isRequired("OfficeAfterHoursCloseSunday"))&&(!working_bltPracticeMaster.isComplete("OfficeAfterHoursCloseSunday")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>After Hours Sunday Close:&nbsp;</b><%=working_bltPracticeMaster.getOfficeAfterHoursCloseSunday()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltPracticeMaster.isExpired("Comments",expiredDays))&&(working_bltPracticeMaster.isExpiredCheck("Comments"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeMaster.isRequired("Comments"))&&(!working_bltPracticeMaster.isComplete("Comments")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Extra Comments:&nbsp;</b><%=working_bltPracticeMaster.getComments()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltPracticeMaster.isExpired("AnesthesiaLocal",expiredDays))&&(working_bltPracticeMaster.isExpiredCheck("AnesthesiaLocal"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeMaster.isRequired("AnesthesiaLocal"))&&(!working_bltPracticeMaster.isComplete("AnesthesiaLocal")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Anesthesia Local:&nbsp;</b><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltPracticeMaster.getAnesthesiaLocal()%>" /></jsp:include></p>

<%theClassF = "textBase";%>
<%if ((working_bltPracticeMaster.isExpired("AnesthesiaRegional",expiredDays))&&(working_bltPracticeMaster.isExpiredCheck("AnesthesiaRegional"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeMaster.isRequired("AnesthesiaRegional"))&&(!working_bltPracticeMaster.isComplete("AnesthesiaRegional")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Anesthesia Regional:&nbsp;</b><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltPracticeMaster.getAnesthesiaRegional()%>" /></jsp:include></p>

<%theClassF = "textBase";%>
<%if ((working_bltPracticeMaster.isExpired("AnesthesiaConscious",expiredDays))&&(working_bltPracticeMaster.isExpiredCheck("AnesthesiaConscious"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeMaster.isRequired("AnesthesiaConscious"))&&(!working_bltPracticeMaster.isComplete("AnesthesiaConscious")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Anesthesia Conscious:&nbsp;</b><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltPracticeMaster.getAnesthesiaConscious()%>" /></jsp:include></p>

<%theClassF = "textBase";%>
<%if ((working_bltPracticeMaster.isExpired("AnesthesiaGeneral",expiredDays))&&(working_bltPracticeMaster.isExpiredCheck("AnesthesiaGeneral"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeMaster.isRequired("AnesthesiaGeneral"))&&(!working_bltPracticeMaster.isComplete("AnesthesiaGeneral")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Anesthesia General:&nbsp;</b><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltPracticeMaster.getAnesthesiaGeneral()%>" /></jsp:include></p>

<%theClassF = "textBase";%>
<%if ((working_bltPracticeMaster.isExpired("MinorSurgery",expiredDays))&&(working_bltPracticeMaster.isExpiredCheck("MinorSurgery"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeMaster.isRequired("MinorSurgery"))&&(!working_bltPracticeMaster.isComplete("MinorSurgery")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Minor Surgery:&nbsp;</b><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltPracticeMaster.getMinorSurgery()%>" /></jsp:include></p>

<%theClassF = "textBase";%>
<%if ((working_bltPracticeMaster.isExpired("Gynecology",expiredDays))&&(working_bltPracticeMaster.isExpiredCheck("Gynecology"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeMaster.isRequired("Gynecology"))&&(!working_bltPracticeMaster.isComplete("Gynecology")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Gynecology:&nbsp;</b><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltPracticeMaster.getGynecology()%>" /></jsp:include></p>

<%theClassF = "textBase";%>
<%if ((working_bltPracticeMaster.isExpired("XRayProcedures",expiredDays))&&(working_bltPracticeMaster.isExpiredCheck("XRayProcedures"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeMaster.isRequired("XRayProcedures"))&&(!working_bltPracticeMaster.isComplete("XRayProcedures")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>X-ray Procedures:&nbsp;</b><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltPracticeMaster.getXRayProcedures()%>" /></jsp:include></p>

<%theClassF = "textBase";%>
<%if ((working_bltPracticeMaster.isExpired("DrawBlood",expiredDays))&&(working_bltPracticeMaster.isExpiredCheck("DrawBlood"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeMaster.isRequired("DrawBlood"))&&(!working_bltPracticeMaster.isComplete("DrawBlood")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Draw Blood:&nbsp;</b><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltPracticeMaster.getDrawBlood()%>" /></jsp:include></p>

<%theClassF = "textBase";%>
<%if ((working_bltPracticeMaster.isExpired("BasicLab",expiredDays))&&(working_bltPracticeMaster.isExpiredCheck("BasicLab"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeMaster.isRequired("BasicLab"))&&(!working_bltPracticeMaster.isComplete("BasicLab")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Basic Lab (CBC, H&H, UA):&nbsp;</b><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltPracticeMaster.getBasicLab()%>" /></jsp:include></p>

<%theClassF = "textBase";%>
<%if ((working_bltPracticeMaster.isExpired("EKG",expiredDays))&&(working_bltPracticeMaster.isExpiredCheck("EKG"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeMaster.isRequired("EKG"))&&(!working_bltPracticeMaster.isComplete("EKG")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>EKG:&nbsp;</b><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltPracticeMaster.getEKG()%>" /></jsp:include></p>

<%theClassF = "textBase";%>
<%if ((working_bltPracticeMaster.isExpired("MinorLacerations",expiredDays))&&(working_bltPracticeMaster.isExpiredCheck("MinorLacerations"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeMaster.isRequired("MinorLacerations"))&&(!working_bltPracticeMaster.isComplete("MinorLacerations")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Minor Lacerations:&nbsp;</b><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltPracticeMaster.getMinorLacerations()%>" /></jsp:include></p>

<%theClassF = "textBase";%>
<%if ((working_bltPracticeMaster.isExpired("Pulmonary",expiredDays))&&(working_bltPracticeMaster.isExpiredCheck("Pulmonary"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeMaster.isRequired("Pulmonary"))&&(!working_bltPracticeMaster.isComplete("Pulmonary")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Pulmonary:&nbsp;</b><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltPracticeMaster.getPulmonary()%>" /></jsp:include></p>

<%theClassF = "textBase";%>
<%if ((working_bltPracticeMaster.isExpired("Allergy",expiredDays))&&(working_bltPracticeMaster.isExpiredCheck("Allergy"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeMaster.isRequired("Allergy"))&&(!working_bltPracticeMaster.isComplete("Allergy")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Allergy:&nbsp;</b><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltPracticeMaster.getAllergy()%>" /></jsp:include></p>

<%theClassF = "textBase";%>
<%if ((working_bltPracticeMaster.isExpired("VisualScreen",expiredDays))&&(working_bltPracticeMaster.isExpiredCheck("VisualScreen"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeMaster.isRequired("VisualScreen"))&&(!working_bltPracticeMaster.isComplete("VisualScreen")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Visual Screen:&nbsp;</b><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltPracticeMaster.getVisualScreen()%>" /></jsp:include></p>

<%theClassF = "textBase";%>
<%if ((working_bltPracticeMaster.isExpired("Audiometry",expiredDays))&&(working_bltPracticeMaster.isExpiredCheck("Audiometry"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeMaster.isRequired("Audiometry"))&&(!working_bltPracticeMaster.isComplete("Audiometry")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Audiometry:&nbsp;</b><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltPracticeMaster.getAudiometry()%>" /></jsp:include></p>

<%theClassF = "textBase";%>
<%if ((working_bltPracticeMaster.isExpired("Sigmoidoscopy",expiredDays))&&(working_bltPracticeMaster.isExpiredCheck("Sigmoidoscopy"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeMaster.isRequired("Sigmoidoscopy"))&&(!working_bltPracticeMaster.isComplete("Sigmoidoscopy")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Sigmoidoscopy:&nbsp;</b><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltPracticeMaster.getSigmoidoscopy()%>" /></jsp:include></p>

<%theClassF = "textBase";%>
<%if ((working_bltPracticeMaster.isExpired("Immunizations",expiredDays))&&(working_bltPracticeMaster.isExpiredCheck("Immunizations"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeMaster.isRequired("Immunizations"))&&(!working_bltPracticeMaster.isComplete("Immunizations")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Immunizations:&nbsp;</b><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltPracticeMaster.getImmunizations()%>" /></jsp:include></p>

<%theClassF = "textBase";%>
<%if ((working_bltPracticeMaster.isExpired("Asthma",expiredDays))&&(working_bltPracticeMaster.isExpiredCheck("Asthma"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeMaster.isRequired("Asthma"))&&(!working_bltPracticeMaster.isComplete("Asthma")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Asthma:&nbsp;</b><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltPracticeMaster.getAsthma()%>" /></jsp:include></p>

<%theClassF = "textBase";%>
<%if ((working_bltPracticeMaster.isExpired("IVTreatment",expiredDays))&&(working_bltPracticeMaster.isExpiredCheck("IVTreatment"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeMaster.isRequired("IVTreatment"))&&(!working_bltPracticeMaster.isComplete("IVTreatment")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>IV Treatment:&nbsp;</b><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltPracticeMaster.getIVTreatment()%>" /></jsp:include></p>

<%theClassF = "textBase";%>
<%if ((working_bltPracticeMaster.isExpired("Osteopathic",expiredDays))&&(working_bltPracticeMaster.isExpiredCheck("Osteopathic"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeMaster.isRequired("Osteopathic"))&&(!working_bltPracticeMaster.isComplete("Osteopathic")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Osteopathic:&nbsp;</b><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltPracticeMaster.getOsteopathic()%>" /></jsp:include></p>

<%theClassF = "textBase";%>
<%if ((working_bltPracticeMaster.isExpired("Hydration",expiredDays))&&(working_bltPracticeMaster.isExpiredCheck("Hydration"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeMaster.isRequired("Hydration"))&&(!working_bltPracticeMaster.isComplete("Hydration")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Hydration:&nbsp;</b><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltPracticeMaster.getHydration()%>" /></jsp:include></p>

<%theClassF = "textBase";%>
<%if ((working_bltPracticeMaster.isExpired("CardiacStress",expiredDays))&&(working_bltPracticeMaster.isExpiredCheck("CardiacStress"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeMaster.isRequired("CardiacStress"))&&(!working_bltPracticeMaster.isComplete("CardiacStress")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Cardiac Stress:&nbsp;</b><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltPracticeMaster.getCardiacStress()%>" /></jsp:include></p>

<%theClassF = "textBase";%>
<%if ((working_bltPracticeMaster.isExpired("PhysicalTherapy",expiredDays))&&(working_bltPracticeMaster.isExpiredCheck("PhysicalTherapy"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeMaster.isRequired("PhysicalTherapy"))&&(!working_bltPracticeMaster.isComplete("PhysicalTherapy")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Physical Therapy:&nbsp;</b><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltPracticeMaster.getPhysicalTherapy()%>" /></jsp:include></p>

<%theClassF = "textBase";%>
<%if ((working_bltPracticeMaster.isExpired("MaternalHealth",expiredDays))&&(working_bltPracticeMaster.isExpiredCheck("MaternalHealth"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeMaster.isRequired("MaternalHealth"))&&(!working_bltPracticeMaster.isComplete("MaternalHealth")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Maternal Health:&nbsp;</b><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltPracticeMaster.getMaternalHealth()%>" /></jsp:include></p>

<%theClassF = "textBase";%>
<%if ((working_bltPracticeMaster.isExpired("CHDP",expiredDays))&&(working_bltPracticeMaster.isExpiredCheck("CHDP"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeMaster.isRequired("CHDP"))&&(!working_bltPracticeMaster.isComplete("CHDP")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>CHDP Coordination & Education:&nbsp;</b><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltPracticeMaster.getCHDP()%>" /></jsp:include></p>

<%theClassF = "textBase";%>
<%if ((working_bltPracticeMaster.isExpired("OfficeServicesComments",expiredDays))&&(working_bltPracticeMaster.isExpiredCheck("OfficeServicesComments"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeMaster.isRequired("OfficeServicesComments"))&&(!working_bltPracticeMaster.isComplete("OfficeServicesComments")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Additional comments regarding Office Services::&nbsp;</b><%=working_bltPracticeMaster.getOfficeServicesComments()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltPracticeMaster.isExpired("LicenseDisplayed",expiredDays))&&(working_bltPracticeMaster.isExpiredCheck("LicenseDisplayed"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeMaster.isRequired("LicenseDisplayed"))&&(!working_bltPracticeMaster.isComplete("LicenseDisplayed")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Are all licenses & certification for all current staff displayed or available?:&nbsp;</b><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltPracticeMaster.getLicenseDisplayed()%>" /></jsp:include></p>

<%theClassF = "textBase";%>
<%if ((working_bltPracticeMaster.isExpired("CPRPresent",expiredDays))&&(working_bltPracticeMaster.isExpiredCheck("CPRPresent"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeMaster.isRequired("CPRPresent"))&&(!working_bltPracticeMaster.isComplete("CPRPresent")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Is a staff person or physician currently CPR Certified and in attendance during office hours?:&nbsp;</b><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltPracticeMaster.getCPRPresent()%>" /></jsp:include></p>

<%theClassF = "textBase";%>
<%if ((working_bltPracticeMaster.isExpired("AmbuBagAvailable",expiredDays))&&(working_bltPracticeMaster.isExpiredCheck("AmbuBagAvailable"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeMaster.isRequired("AmbuBagAvailable"))&&(!working_bltPracticeMaster.isComplete("AmbuBagAvailable")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Ambu Bag, Airways, Suction Devices, Adrenalin present?:&nbsp;</b><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltPracticeMaster.getAmbuBagAvailable()%>" /></jsp:include></p>

<%theClassF = "textBase";%>
<%if ((working_bltPracticeMaster.isExpired("OxygenAvailable",expiredDays))&&(working_bltPracticeMaster.isExpiredCheck("OxygenAvailable"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeMaster.isRequired("OxygenAvailable"))&&(!working_bltPracticeMaster.isComplete("OxygenAvailable")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Is oxygen available,  clearly marked & secured?:&nbsp;</b><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltPracticeMaster.getOxygenAvailable()%>" /></jsp:include></p>

<%theClassF = "textBase";%>
<%if ((working_bltPracticeMaster.isExpired("SurgicalSuite",expiredDays))&&(working_bltPracticeMaster.isExpiredCheck("SurgicalSuite"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeMaster.isRequired("SurgicalSuite"))&&(!working_bltPracticeMaster.isComplete("SurgicalSuite")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Surgical Suite:&nbsp;</b><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltPracticeMaster.getSurgicalSuite()%>" /></jsp:include></p>

<%theClassF = "textBase";%>
<%if ((working_bltPracticeMaster.isExpired("CertType",expiredDays))&&(working_bltPracticeMaster.isExpiredCheck("CertType"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeMaster.isRequired("CertType"))&&(!working_bltPracticeMaster.isComplete("CertType")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Surgical Suite Certification Type:&nbsp;</b><%=working_bltPracticeMaster.getCertType()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltPracticeMaster.isExpired("LabOnSite",expiredDays))&&(working_bltPracticeMaster.isExpiredCheck("LabOnSite"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeMaster.isRequired("LabOnSite"))&&(!working_bltPracticeMaster.isComplete("LabOnSite")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Do you have a lab on-site?:&nbsp;</b><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltPracticeMaster.getLabOnSite()%>" /></jsp:include></p>

<%theClassF = "textBase";%>
<%if ((working_bltPracticeMaster.isExpired("CLIANumber",expiredDays))&&(working_bltPracticeMaster.isExpiredCheck("CLIANumber"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeMaster.isRequired("CLIANumber"))&&(!working_bltPracticeMaster.isComplete("CLIANumber")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>CLIA Number:&nbsp;</b><%=working_bltPracticeMaster.getCLIANumber()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltPracticeMaster.isExpired("CLIAWaiver",expiredDays))&&(working_bltPracticeMaster.isExpiredCheck("CLIAWaiver"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeMaster.isRequired("CLIAWaiver"))&&(!working_bltPracticeMaster.isComplete("CLIAWaiver")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>CLIA Waiver Number:&nbsp;</b><%=working_bltPracticeMaster.getCLIAWaiver()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltPracticeMaster.isExpired("ADAAccessibility",expiredDays))&&(working_bltPracticeMaster.isExpiredCheck("ADAAccessibility"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeMaster.isRequired("ADAAccessibility"))&&(!working_bltPracticeMaster.isComplete("ADAAccessibility")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Do you have ADA Accessibility?:&nbsp;</b><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltPracticeMaster.getADAAccessibility()%>" /></jsp:include></p>

<%theClassF = "textBase";%>
<%if ((working_bltPracticeMaster.isExpired("HandicapAccessBuilding",expiredDays))&&(working_bltPracticeMaster.isExpiredCheck("HandicapAccessBuilding"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeMaster.isRequired("HandicapAccessBuilding"))&&(!working_bltPracticeMaster.isComplete("HandicapAccessBuilding")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Do you have Handicap Building Access with Ramp?:&nbsp;</b><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltPracticeMaster.getHandicapAccessBuilding()%>" /></jsp:include></p>

<%theClassF = "textBase";%>
<%if ((working_bltPracticeMaster.isExpired("HandicapAccessRampRail",expiredDays))&&(working_bltPracticeMaster.isExpiredCheck("HandicapAccessRampRail"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeMaster.isRequired("HandicapAccessRampRail"))&&(!working_bltPracticeMaster.isComplete("HandicapAccessRampRail")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Are there handrails on both sides of the ramp?:&nbsp;</b><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltPracticeMaster.getHandicapAccessRampRail()%>" /></jsp:include></p>

<%theClassF = "textBase";%>
<%if ((working_bltPracticeMaster.isExpired("HandicapAccessParking",expiredDays))&&(working_bltPracticeMaster.isExpiredCheck("HandicapAccessParking"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeMaster.isRequired("HandicapAccessParking"))&&(!working_bltPracticeMaster.isComplete("HandicapAccessParking")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Do you have Handicap Parking?:&nbsp;</b><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltPracticeMaster.getHandicapAccessParking()%>" /></jsp:include></p>

<%theClassF = "textBase";%>
<%if ((working_bltPracticeMaster.isExpired("HandicapAccessWheel",expiredDays))&&(working_bltPracticeMaster.isExpiredCheck("HandicapAccessWheel"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeMaster.isRequired("HandicapAccessWheel"))&&(!working_bltPracticeMaster.isComplete("HandicapAccessWheel")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Do you have Wheelchair Access?:&nbsp;</b><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltPracticeMaster.getHandicapAccessWheel()%>" /></jsp:include></p>

<%theClassF = "textBase";%>
<%if ((working_bltPracticeMaster.isExpired("HandicapAccessRail",expiredDays))&&(working_bltPracticeMaster.isExpiredCheck("HandicapAccessRail"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeMaster.isRequired("HandicapAccessRail"))&&(!working_bltPracticeMaster.isComplete("HandicapAccessRail")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Do you have Handicap Rails in the Bathroom?:&nbsp;</b><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltPracticeMaster.getHandicapAccessRail()%>" /></jsp:include></p>

<%theClassF = "textBase";%>
<%if ((working_bltPracticeMaster.isExpired("HandicapAccessElevator",expiredDays))&&(working_bltPracticeMaster.isExpiredCheck("HandicapAccessElevator"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeMaster.isRequired("HandicapAccessElevator"))&&(!working_bltPracticeMaster.isComplete("HandicapAccessElevator")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Do you have Elevator available if office is above the first floor?:&nbsp;</b><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltPracticeMaster.getHandicapAccessElevator()%>" /></jsp:include></p>

<%theClassF = "textBase";%>
<%if ((working_bltPracticeMaster.isExpired("HandicapAccessBraille",expiredDays))&&(working_bltPracticeMaster.isExpiredCheck("HandicapAccessBraille"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeMaster.isRequired("HandicapAccessBraille"))&&(!working_bltPracticeMaster.isComplete("HandicapAccessBraille")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Do you have Elevator Brail indicators in building with more than one story?:&nbsp;</b><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltPracticeMaster.getHandicapAccessBraille()%>" /></jsp:include></p>

<%theClassF = "textBase";%>
<%if ((working_bltPracticeMaster.isExpired("HandicapFountainPhone",expiredDays))&&(working_bltPracticeMaster.isExpiredCheck("HandicapFountainPhone"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeMaster.isRequired("HandicapFountainPhone"))&&(!working_bltPracticeMaster.isComplete("HandicapFountainPhone")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Do you have water fountains and telephones at proper height for wheelchair patients?:&nbsp;</b><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltPracticeMaster.getHandicapFountainPhone()%>" /></jsp:include></p>

<%theClassF = "textBase";%>
<%if ((working_bltPracticeMaster.isExpired("NearPublicTransportation",expiredDays))&&(working_bltPracticeMaster.isExpiredCheck("NearPublicTransportation"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeMaster.isRequired("NearPublicTransportation"))&&(!working_bltPracticeMaster.isComplete("NearPublicTransportation")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Are you near public transportation?:&nbsp;</b><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltPracticeMaster.getNearPublicTransportation()%>" /></jsp:include></p>

<%theClassF = "textBase";%>
<%if ((working_bltPracticeMaster.isExpired("DisabledTTY",expiredDays))&&(working_bltPracticeMaster.isExpiredCheck("DisabledTTY"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeMaster.isRequired("DisabledTTY"))&&(!working_bltPracticeMaster.isComplete("DisabledTTY")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Do you provide Text Telephony - TTY:&nbsp;</b><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltPracticeMaster.getDisabledTTY()%>" /></jsp:include></p>

<%theClassF = "textBase";%>
<%if ((working_bltPracticeMaster.isExpired("DisabledASL",expiredDays))&&(working_bltPracticeMaster.isExpiredCheck("DisabledASL"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeMaster.isRequired("DisabledASL"))&&(!working_bltPracticeMaster.isComplete("DisabledASL")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Do you provide American Sign Language - ASL:&nbsp;</b><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltPracticeMaster.getDisabledASL()%>" /></jsp:include></p>

<%theClassF = "textBase";%>
<%if ((working_bltPracticeMaster.isExpired("ChildcareServices",expiredDays))&&(working_bltPracticeMaster.isExpiredCheck("ChildcareServices"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeMaster.isRequired("ChildcareServices"))&&(!working_bltPracticeMaster.isComplete("ChildcareServices")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Do you provide childcare services:&nbsp;</b><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltPracticeMaster.getChildcareServices()%>" /></jsp:include></p>

<%theClassF = "textBase";%>
<%if ((working_bltPracticeMaster.isExpired("SafetyFireExtinguisher",expiredDays))&&(working_bltPracticeMaster.isExpiredCheck("SafetyFireExtinguisher"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeMaster.isRequired("SafetyFireExtinguisher"))&&(!working_bltPracticeMaster.isComplete("SafetyFireExtinguisher")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Is the fire extinguisher accessible?:&nbsp;</b><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltPracticeMaster.getSafetyFireExtinguisher()%>" /></jsp:include></p>

<%theClassF = "textBase";%>
<%if ((working_bltPracticeMaster.isExpired("SafetyExtinguisherInspected",expiredDays))&&(working_bltPracticeMaster.isExpiredCheck("SafetyExtinguisherInspected"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeMaster.isRequired("SafetyExtinguisherInspected"))&&(!working_bltPracticeMaster.isComplete("SafetyExtinguisherInspected")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Are the extinguisher inspected and logged routinely?:&nbsp;</b><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltPracticeMaster.getSafetyExtinguisherInspected()%>" /></jsp:include></p>

<%theClassF = "textBase";%>
<%if ((working_bltPracticeMaster.isExpired("SafetySmokeDetectors",expiredDays))&&(working_bltPracticeMaster.isExpiredCheck("SafetySmokeDetectors"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeMaster.isRequired("SafetySmokeDetectors"))&&(!working_bltPracticeMaster.isComplete("SafetySmokeDetectors")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Are there smoke detectors or a sprinkler system on premises?:&nbsp;</b><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltPracticeMaster.getSafetySmokeDetectors()%>" /></jsp:include></p>

<%theClassF = "textBase";%>
<%if ((working_bltPracticeMaster.isExpired("SafetyCorridorsClear",expiredDays))&&(working_bltPracticeMaster.isExpiredCheck("SafetyCorridorsClear"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeMaster.isRequired("SafetyCorridorsClear"))&&(!working_bltPracticeMaster.isComplete("SafetyCorridorsClear")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Are the corridors and hallways clear?:&nbsp;</b><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltPracticeMaster.getSafetyCorridorsClear()%>" /></jsp:include></p>

<%theClassF = "textBase";%>
<%if ((working_bltPracticeMaster.isExpired("InfectionControlHandwashing",expiredDays))&&(working_bltPracticeMaster.isExpiredCheck("InfectionControlHandwashing"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeMaster.isRequired("InfectionControlHandwashing"))&&(!working_bltPracticeMaster.isComplete("InfectionControlHandwashing")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Is there access for hand washing in exam room or proximate to exam room?:&nbsp;</b><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltPracticeMaster.getInfectionControlHandwashing()%>" /></jsp:include></p>

<%theClassF = "textBase";%>
<%if ((working_bltPracticeMaster.isExpired("InfectionControlGloves",expiredDays))&&(working_bltPracticeMaster.isExpiredCheck("InfectionControlGloves"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeMaster.isRequired("InfectionControlGloves"))&&(!working_bltPracticeMaster.isComplete("InfectionControlGloves")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Are gloves changed between patients?:&nbsp;</b><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltPracticeMaster.getInfectionControlGloves()%>" /></jsp:include></p>

<%theClassF = "textBase";%>
<%if ((working_bltPracticeMaster.isExpired("InfectionControlBloodCleaned",expiredDays))&&(working_bltPracticeMaster.isExpiredCheck("InfectionControlBloodCleaned"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeMaster.isRequired("InfectionControlBloodCleaned"))&&(!working_bltPracticeMaster.isComplete("InfectionControlBloodCleaned")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Are surfaces exposed to blood or body fluids cleansed with appropriate disinfectant?:&nbsp;</b><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltPracticeMaster.getInfectionControlBloodCleaned()%>" /></jsp:include></p>

<%theClassF = "textBase";%>
<%if ((working_bltPracticeMaster.isExpired("FacilityComments",expiredDays))&&(working_bltPracticeMaster.isExpiredCheck("FacilityComments"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeMaster.isRequired("FacilityComments"))&&(!working_bltPracticeMaster.isComplete("FacilityComments")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Additional comments regarding Facility Information::&nbsp;</b><%=working_bltPracticeMaster.getFacilityComments()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltPracticeMaster.isExpired("MedicalRecordsIndividual",expiredDays))&&(working_bltPracticeMaster.isExpiredCheck("MedicalRecordsIndividual"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeMaster.isRequired("MedicalRecordsIndividual"))&&(!working_bltPracticeMaster.isComplete("MedicalRecordsIndividual")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Does each patient have an individual medical record?:&nbsp;</b><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltPracticeMaster.getMedicalRecordsIndividual()%>" /></jsp:include></p>

<%theClassF = "textBase";%>
<%if ((working_bltPracticeMaster.isExpired("MedicalRecordsInaccessible",expiredDays))&&(working_bltPracticeMaster.isExpiredCheck("MedicalRecordsInaccessible"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeMaster.isRequired("MedicalRecordsInaccessible"))&&(!working_bltPracticeMaster.isComplete("MedicalRecordsInaccessible")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Are medical records kept in an area which is inaccessible to patients?:&nbsp;</b><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltPracticeMaster.getMedicalRecordsInaccessible()%>" /></jsp:include></p>

<%theClassF = "textBase";%>
<%if ((working_bltPracticeMaster.isExpired("MedicalRecordsUniform",expiredDays))&&(working_bltPracticeMaster.isExpiredCheck("MedicalRecordsUniform"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeMaster.isRequired("MedicalRecordsUniform"))&&(!working_bltPracticeMaster.isComplete("MedicalRecordsUniform")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Are medical records maintained in a  uniform format?:&nbsp;</b><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltPracticeMaster.getMedicalRecordsUniform()%>" /></jsp:include></p>

<%theClassF = "textBase";%>
<%if ((working_bltPracticeMaster.isExpired("MedicalRecordsPatientsNamePerPage",expiredDays))&&(working_bltPracticeMaster.isExpiredCheck("MedicalRecordsPatientsNamePerPage"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeMaster.isRequired("MedicalRecordsPatientsNamePerPage"))&&(!working_bltPracticeMaster.isComplete("MedicalRecordsPatientsNamePerPage")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Is there a place on each page for the patient's name?:&nbsp;</b><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltPracticeMaster.getMedicalRecordsPatientsNamePerPage()%>" /></jsp:include></p>

<%theClassF = "textBase";%>
<%if ((working_bltPracticeMaster.isExpired("MedicalRecordsSignatureAuthor",expiredDays))&&(working_bltPracticeMaster.isExpiredCheck("MedicalRecordsSignatureAuthor"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeMaster.isRequired("MedicalRecordsSignatureAuthor"))&&(!working_bltPracticeMaster.isComplete("MedicalRecordsSignatureAuthor")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Is there a place for signature of each author?:&nbsp;</b><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltPracticeMaster.getMedicalRecordsSignatureAuthor()%>" /></jsp:include></p>

<%theClassF = "textBase";%>
<%if ((working_bltPracticeMaster.isExpired("MedicalRecordsEntryDate",expiredDays))&&(working_bltPracticeMaster.isExpiredCheck("MedicalRecordsEntryDate"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeMaster.isRequired("MedicalRecordsEntryDate"))&&(!working_bltPracticeMaster.isComplete("MedicalRecordsEntryDate")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Is there a place for each entry to be dated?:&nbsp;</b><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltPracticeMaster.getMedicalRecordsEntryDate()%>" /></jsp:include></p>

<%theClassF = "textBase";%>
<%if ((working_bltPracticeMaster.isExpired("MedicalRecordsHistory",expiredDays))&&(working_bltPracticeMaster.isExpiredCheck("MedicalRecordsHistory"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeMaster.isRequired("MedicalRecordsHistory"))&&(!working_bltPracticeMaster.isComplete("MedicalRecordsHistory")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Is there a place for medical history in the record?:&nbsp;</b><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltPracticeMaster.getMedicalRecordsHistory()%>" /></jsp:include></p>

<%theClassF = "textBase";%>
<%if ((working_bltPracticeMaster.isExpired("MedicalRecordsAdverse",expiredDays))&&(working_bltPracticeMaster.isExpiredCheck("MedicalRecordsAdverse"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeMaster.isRequired("MedicalRecordsAdverse"))&&(!working_bltPracticeMaster.isComplete("MedicalRecordsAdverse")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Is there a place for allergies and adverse reactions to medications to be prominently noted in the record?:&nbsp;</b><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltPracticeMaster.getMedicalRecordsAdverse()%>" /></jsp:include></p>

<%theClassF = "textBase";%>
<%if ((working_bltPracticeMaster.isExpired("MedicalRecordsComments",expiredDays))&&(working_bltPracticeMaster.isExpiredCheck("MedicalRecordsComments"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeMaster.isRequired("MedicalRecordsComments"))&&(!working_bltPracticeMaster.isComplete("MedicalRecordsComments")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Additional comments regarding Medical Records::&nbsp;</b><%=working_bltPracticeMaster.getMedicalRecordsComments()%></p>

        </td></tr></table></table><br>
        <%
    }
    %>

    </table>
    <%

  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>

    </td></tr></table>


<%String bnIncludeFN = "ui_"+thePLCID+"\\bot-nav_PhysicianID.jsp";
%>
<jsp:include page="<%=bnIncludeFN%>" flush="true" ><jsp:param name="plcID" value="<%=thePLCID%>"/></jsp:include>
