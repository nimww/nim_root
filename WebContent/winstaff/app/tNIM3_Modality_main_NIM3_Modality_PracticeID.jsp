<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.*" %>
<%/*
    filename: tNIM3_Modality_main_NIM3_Modality_PracticeID.jsp
    Created on Oct/28/2009
    Type: 1-n main class file
    Created by: Scott Ellis
    Version: 4.0
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>

<link href="ui_1/style.css" rel="stylesheet" type="text/css" />

<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_PracticeID_nim.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>

    <table cellpadding=0 cellspacing=0 border=0 width=100%> >
    <tr><td width=10>&nbsp;</td><td>
    <%=ConfigurationMessages.getHTML("INTERVIEWTopControl","tNIM3_Modality")%>



<%
//initial declaration of list class and parentID
    Integer        iPracticeID        =    null;
    boolean accessValid = false;
   Integer iSecurityCheck = SecurityCheck.CheckItem("nim1", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {
    if (pageControllerHash.containsKey("iPracticeID")) 
    {
        iPracticeID        =    (Integer)pageControllerHash.get("iPracticeID");
        accessValid = true;
    }
  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);
      pageControllerHash.put("sParentReturnPage","tNIM3_Modality_main_NIM3_Modality_PracticeID.jsp");
    pageControllerHash.remove("iModalityID");
    pageControllerHash.put("sINTNext","tNIM3_Modality_main_NIM3_Modality_PracticeID_form_create.jsp?EDIT=new&KM=p&INTNext=yes");
    session.setAttribute("pageControllerHash",pageControllerHash);

    bltNIM3_Modality_List        bltNIM3_Modality_List        =    new    bltNIM3_Modality_List(iPracticeID);

//declaration of Enumeration
    bltNIM3_Modality        working_bltNIM3_Modality;
    ListElement         leCurrentElement;
    java.util.Enumeration eList = bltNIM3_Modality_List.elements();
    %>
        <%@ include file="tNIM3_Modality_main_NIM3_Modality_PracticeID_instructions.jsp" %>

        <%if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {%>
        <input class="tdBaseAltGreen" type=button onClick = "this.disabled=true;document.location ='tNIM3_Modality_main_NIM3_Modality_PracticeID_form_create.jsp?EDIT=new&KM=p&INTNext=yes'" value="Create">
        <%}%>
         <table border="1" bordercolor="CCCCCC" cellpadding="3" class=tdBase cellspacing="0" width="100%">
    <%
    int altCnt = 0;
    if (eList.hasMoreElements())
    {
     while (eList.hasMoreElements())
     {

        altCnt++;
        String theClass = "tdBase";
        if (altCnt%2!=0)
        {
            theClass = "tdBaseAlt";
        }
        leCurrentElement    = (ListElement) eList.nextElement();
        working_bltNIM3_Modality  = (bltNIM3_Modality) leCurrentElement.getObject();
        working_bltNIM3_Modality.GroupSecurityInit(UserSecurityGroupID);
        if (!working_bltNIM3_Modality.isComplete())
        {
            theClass = "incompleteItem";
        %>

        <%
        }
        else
        {
        %>
        <%
        }
        %>
        <tr class=<%=theClass%> > 

              <td><b>Item ID:&nbsp;</b><%=working_bltNIM3_Modality.getModalityID()%><br>Created: <jsp:include page="../generic/DateTypeConvert_Main.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(working_bltNIM3_Modality.getUniqueCreateDate())%>" /></jsp:include><br>Modified: <jsp:include page="../generic/DateTypeConvert_Main.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(working_bltNIM3_Modality.getUniqueModifyDate())%>" /></jsp:include></td>
<%String theClassF = "textBase";%>
 <td>
  <%theClassF = "textBase";%>
  <%if ((working_bltNIM3_Modality.isExpired("ModalityTypeID",expiredDays))&&(working_bltNIM3_Modality.isExpiredCheck("ModalityTypeID"))){theClassF = "expiredFieldMain";}%>
  <%if ( (working_bltNIM3_Modality.isRequired("ModalityTypeID"))&&(!working_bltNIM3_Modality.isComplete("ModalityTypeID")) ){theClassF = "requiredFieldMain";}%>
   <b>Modality:&nbsp;</b><jsp:include page="../generic/tModalityTypeLIShort_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltNIM3_Modality.getModalityTypeID()%>" /></jsp:include> <br>
   
  <%theClassF = "textBase";%>
  <%if ((working_bltNIM3_Modality.isExpired("ModalityModelID",expiredDays))&&(working_bltNIM3_Modality.isExpiredCheck("ModalityModelID"))){theClassF = "expiredFieldMain";}%>
  <%if ( (working_bltNIM3_Modality.isRequired("ModalityModelID"))&&(!working_bltNIM3_Modality.isComplete("ModalityModelID")) ){theClassF = "requiredFieldMain";}%>
  <b>Model:&nbsp;</b>
  <jsp:include page="../generic/tMRI_ModelLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltNIM3_Modality.getModalityModelID()%>" /></jsp:include> </td>
 <td > 
   <input type=button  class="tdBaseAltGreen" onClick = "this.disabled=true;document.location ='tNIM3_Modality_main_NIM3_Modality_PracticeID_form_authorize.jsp?EDIT=edit&EDITID=<%=working_bltNIM3_Modality.getModalityID()%>&KM=p'" value="Edit">
   
   &nbsp;&nbsp;&nbsp;&nbsp;
   <%if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {%>
   <input type=button class="tdBaseAltRed" onClick = "confirmDelete2('','tNIM3_Modality_main_NIM3_Modality_PracticeID_form_authorize.jsp?EDIT=del&EDITID=<%=working_bltNIM3_Modality.getModalityID()%>&KM=p',this)" value="Delete">
   <% }%>
 </td></tr>
        <%
    }//end while
       }//end of if
       else 
       {
           %>
           <tr><td colspan=4><b>Please click the "create" to add <%=ConfigurationMessages.getDataCategory("tNIM3_Modality")%> information or click 'Continue' to go to the next section.</b>
           <script language=javascript>
           if (confirm("<%=ConfigurationMessages.getInterviewMessage("INTERVIEWNoElements","tNIM3_Modality")%>"))
           {
               document.location="tNIM3_Modality_main_NIM3_Modality_PracticeID_form_create.jsp?EDIT=new&KM=p&INTNext=yes"; 
           }
           else
           {

           }
           </script>
           </td></tr>
           <%
       }
    %>

    </table>
    <%
  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>

    </td></tr></table><br>

