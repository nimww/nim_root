<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.*" %>
<%/*
    filename: out\jsp\tPracticeMaster_form.jsp
    JSP AutoGen on Mar/02/2002
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>

<%
//String thePLCID = (String)pageControllerHash.get("plcID");
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_Clear.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>

<style>
body {
background:#CFC;
margin:10px;
}
</style>

<%
//initial declaration of list class and parentID
    Integer        iPracticeID        =    null;
    boolean accessValid = false;
    // required for Type2
    String sKeyMasterReference = null;
    if (pageControllerHash.containsKey("iPracticeID")) 
    {
        iPracticeID        =    (Integer)pageControllerHash.get("iPracticeID");
        accessValid = true;    
   }
  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);      java.text.SimpleDateFormat displayDateTimeSDF = new java.text.SimpleDateFormat("MM/dd/yyyy hh:mm a");
	  bltPracticeMaster pm = new bltPracticeMaster(iPracticeID);
	  CPTObject cpto = new CPTObject(request.getParameter("cpt"), request.getParameter("mod"), 1);
	  
	  %>
	  
	  <h2>WC Cost: $<%=PLCUtils.getDisplayDefaultDecimalFormat2( NIMUtils.getCPTPayPriceforPracticeMaster(pm, cpto, PLCUtils.getSubDate(request.getParameter("dos")), NIMUtils.PAYMENT_TYPE_WORKCOMP) )%> </h2>
	  <h2>GC Cost: $<%=PLCUtils.getDisplayDefaultDecimalFormat2( NIMUtils.getCPTPayPriceforPracticeMaster(pm, cpto, PLCUtils.getSubDate(request.getParameter("dos")), NIMUtils.PAYMENT_TYPE_GROUPHEALTH) )%> </h2>
	  
	  
<%

  }
  else
  {
   response.sendRedirect("/errorhandler.jsp");
  }

%>

