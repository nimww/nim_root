<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.PLCUtils,java.util.Vector,java.util.Enumeration,com.winstaff.ListElement,com.winstaff.bltDocumentManagement,com.winstaff.bltDocumentManagement_List" %>
<%/*
    filename: tDocumentManagement_main_DocumentManagement_PhysicianID.jsp
    JSP AutoGen on Mar/06/2002
    Type: 1-n main class file
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>

<%
//String thePLCID = (String)pageControllerHash.get("plcID");
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_PhysicianID.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>

    <table cellpadding=0 cellspacing=0 border=0 width=700>
    <tr><td width=10>&nbsp;</td><td>
    <span class=title>File: tDocumentManagement_main_DocumentManagement_PhysicianID.jsp</span><br>
      <%
//initial declaration of list class and parentID
    Integer        iPhysicianID        =    null;
    boolean accessValid = false;
    if (pageControllerHash.containsKey("iPhysicianID")) 
    {
        iPhysicianID        =    (Integer)pageControllerHash.get("iPhysicianID");
        accessValid = true;
    }
  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);
      pageControllerHash.put("sParentReturnPage","tDocumentManagement_main2_DocumentManagement_PhysicianID.jsp");
    pageControllerHash.remove("iDocumentID");
    session.setAttribute("pageControllerHash",pageControllerHash);

    bltDocumentManagement_List        bltDocumentManagement_List        =    new    bltDocumentManagement_List(iPhysicianID);

//declaration of Enumeration
    bltDocumentManagement        working_bltDocumentManagement;
    ListElement         leCurrentElement;
    java.util.Enumeration eList = bltDocumentManagement_List.elements();
    %>
      <%@ include file="tDocumentManagement_main_DocumentManagement_PhysicianID_instructions.jsp" %>
      <table class=tableBase cellpadding=0 cellspacing=0 border=0>
    <%
    while (eList.hasMoreElements())
    {
        leCurrentElement    = (ListElement) eList.nextElement();
        working_bltDocumentManagement  = (bltDocumentManagement) leCurrentElement.getObject();
        String theClass = "tdHeader";
        if (!working_bltDocumentManagement.isComplete())
        {
            theClass = "incompleteItem";
        %>
            <tr class=incompleteItem><td colspan=3><b>This item is incomplete</b></td></tr>
        <%
        }
        %>
        <tr class=<%=theClass%>><td><b>UniqueID</b>[<i>DocumentID</i>]<%=working_bltDocumentManagement.getDocumentID()%> </td>
          <td>&nbsp;</td>
        <td><a class=linkBase href = "tDocumentManagement_main_DocumentManagement_PhysicianID_form_authorize.jsp?EDIT=del&EDITID=<%=working_bltDocumentManagement.getDocumentID()%>&KM=p"><img border=0 src="ui_<%=thePLCID%>/icons/delete_PhysicianID.gif"></a></td>
        </tr>

                        <tr class=textBase><td colspan=4><span class=textBase><b>Item Create Date</b><jsp:include page="../generic/DateTypeConvert_Main.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(working_bltDocumentManagement.getUniqueCreateDate())%>" /></jsp:include></td></tr>


                        <tr class=textBase><td colspan=4><span class=textBase><b>Item Modify Date</b><jsp:include page="../generic/DateTypeConvert_Main.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(working_bltDocumentManagement.getUniqueModifyDate())%>" /></jsp:include></td></tr>

            <tr class=textBase><td colspan=3><b>Item Modification Comments</b><%=working_bltDocumentManagement.getUniqueModifyComments()%></td></tr>
            <tr class=textBase><td colspan=3><b>PhysicianID</b><%=working_bltDocumentManagement.getPhysicianID()%></td></tr>
            <tr class=textBase><td colspan=3><b>Document Type</b><jsp:include page="../generic/tDocumentTypeLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltDocumentManagement.getDocumentTypeID()%>" /></jsp:include></td></tr>
            <tr class=textBase><td colspan=3><b>Document Name</b><%=working_bltDocumentManagement.getDocumentName()%></td></tr>
            <tr class=textBase><td colspan=3><b>File Name</b><%=working_bltDocumentManagement.getDocumentFileName()%></td></tr>

                        <tr class=textBase><td colspan=4><span class=textBase><b>Date Received</b><jsp:include page="../generic/DateTypeConvert_Main.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(working_bltDocumentManagement.getDateReceived())%>" /></jsp:include></td></tr>


                        <tr class=textBase><td colspan=4><span class=textBase><b>Expiration Date</b><jsp:include page="../generic/DateTypeConvert_Main.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(working_bltDocumentManagement.getDateOfExpiration())%>" /></jsp:include></td></tr>


                        <tr class=textBase><td colspan=4><span class=textBase><b>Date Alert Sent</b><jsp:include page="../generic/DateTypeConvert_Main.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(working_bltDocumentManagement.getDateAlertSent())%>" /></jsp:include></td></tr>

            <tr class=textBase><td colspan=3><b>Extra Comments</b><%=working_bltDocumentManagement.getComments()%></td></tr>
        <tr class=tdBaseAlt><td colspan=3>&nbsp;</td></tr>
        <%
    }
    %>
    </table>
    <%
  }
  else
  {
   response.sendRedirect("/errorhandler.jsp");
  }
%>

    </td></tr></table>


<%String bnIncludeFN = "ui_"+thePLCID+"\\bot-nav_PhysicianID.jsp";
%>
<jsp:include page="<%=bnIncludeFN%>" flush="true" ><jsp:param name="plcID" value="<%=thePLCID%>"/></jsp:include>
