<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.bltDocumentManagement,java.util.Vector,java.util.Enumeration,com.winstaff.ListElement, com.winstaff.PLCUtils, com.winstaff.SecurityCheck, com.winstaff.ConfigurationMessages,com.winstaff.bltAdminMaster,com.winstaff.bltAdminPhysicianLU,com.winstaff.bltAdminPhysicianLU_List_LU_PhysicianID" %>
<%/*
    filename: out\jsp\tAdminPhysicianLU_main_AdminMaster_PhysicianID_form_create.jsp
    Created on Mar/21/2003
    Type: n-n main class file
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>


<%
//initial declaration of list class and parentID
    Integer        iGenAdminID        =    null;
    boolean accessValid = false;
   Integer iSecurityCheck = SecurityCheck.CheckItem("company1", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {

		if (pageControllerHash.containsKey("iGenAdminID")) 
		{
			iGenAdminID        =    (Integer)pageControllerHash.get("iGenAdminID");
			accessValid = true;
		}
  //page security
  if (accessValid)
  {
//declaration of Enumeration
    bltAdminMaster        working_bltAdminMaster = new bltAdminMaster();
    working_bltAdminMaster.setAssignedToID(CurrentUserAccount.getUserID());
    working_bltAdminMaster.setUniqueCreateDate(new java.util.Date());
    working_bltAdminMaster.setUniqueModifyDate(new java.util.Date());
    if (pageControllerHash.containsKey("UserLogonDescription"))
    {
        String UserLogonDescription = (String)pageControllerHash.get("UserLogonDescription");
        working_bltAdminMaster.setUniqueModifyComments(""+UserLogonDescription);
		working_bltAdminMaster.setName("New Account [" + UserLogonDescription + "] - Please Rename");
    }
	else
	{
        String UserLogonDescription = CurrentUserAccount.getLogonUserName();
        working_bltAdminMaster.setUniqueModifyComments(""+UserLogonDescription);
		working_bltAdminMaster.setName("New Account [" + UserLogonDescription + "] - Please Rename");
	}
    working_bltAdminMaster.commitData();
        pageControllerHash.put("iAdminID",working_bltAdminMaster.getUniqueID());
        pageControllerHash.put("sKeyMasterReference","p");
        session.setAttribute("pageControllerHash",pageControllerHash);
        //Parameter Pass Code here
			String parameterPassString ="";
			java.util.Enumeration myParameterPassList = request.getParameterNames();
			while (myParameterPassList.hasMoreElements())
			{
				String myName = (String)myParameterPassList.nextElement();
				String myS = (String) request.getParameter(myName);
				parameterPassString+="&"+myName + "=" + myS;
			}
        String targetRedirect = "pro-file-manager_Status.jsp?nullParam=null"+parameterPassString    ;

        response.sendRedirect(targetRedirect);

  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>

