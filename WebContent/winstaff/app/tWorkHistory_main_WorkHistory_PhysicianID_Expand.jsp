<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.bltDocumentManagement,java.util.Vector,java.util.Enumeration,com.winstaff.ListElement, com.winstaff.PLCUtils, com.winstaff.SecurityCheck, com.winstaff.ConfigurationMessages,com.winstaff.bltWorkHistory,com.winstaff.bltWorkHistory_List" %>
<%/*
    filename: tWorkHistory_main_WorkHistory_PhysicianID.jsp
    Created on Mar/21/2003
    Type: 1-n main class file
    Created by: Scott Ellis
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>

<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_PhysicianID.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>

    <table cellpadding=0 cellspacing=0 border=0 width=<%=MasterTableWidth%> >
    <tr><td width=10>&nbsp;</td><td>
    <%=ConfigurationMessages.getHTML("INTERVIEWTopControl","tWorkHistory")%>

    <br><a href="tWorkHistory_main_WorkHistory_PhysicianID.jsp">Compact</a>

<%
//initial declaration of list class and parentID
    Integer        iPhysicianID        =    null;
    boolean accessValid = false;
   Integer iSecurityCheck = SecurityCheck.CheckItem("PractitionerSection8", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {
    if (pageControllerHash.containsKey("iPhysicianID")) 
    {
        iPhysicianID        =    (Integer)pageControllerHash.get("iPhysicianID");
        accessValid = true;
    }
  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);
      pageControllerHash.put("sParentReturnPage","tWorkHistory_main_WorkHistory_PhysicianID_Expand.jsp");
    pageControllerHash.remove("iWorkHistoryID");
    session.setAttribute("pageControllerHash",pageControllerHash);

    bltWorkHistory_List        bltWorkHistory_List        =    new    bltWorkHistory_List(iPhysicianID);

//declaration of Enumeration
    bltWorkHistory        working_bltWorkHistory;
    ListElement         leCurrentElement;
    Enumeration eList = bltWorkHistory_List.elements();
    %>
        <%@ include file="tWorkHistory_main_WorkHistory_PhysicianID_instructions.jsp" %>

        <%if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {%>
        <a class=linkBase href = "tWorkHistory_main_WorkHistory_PhysicianID_form_create.jsp?EDIT=new&KM=p"><img border=0 src="ui_<%=thePLCID%>/icons/create_PhysicianID.gif"></a>
        <%}%>
    <%
    while (eList.hasMoreElements())
    {
         %>
         <table border="0" bordercolor="333333" cellpadding="3" class=tdHeaderAlt cellspacing="0" width="100%">
            <tr>
              <td width="50%">
                   <table border="0" bordercolor="333333" cellpadding="3" class=tdHeaderAlt cellspacing="0" width="100%">
                     <tr> 
                       <td>
         <%

        leCurrentElement    = (ListElement) eList.nextElement();
        working_bltWorkHistory  = (bltWorkHistory) leCurrentElement.getObject();
        working_bltWorkHistory.GroupSecurityInit(UserSecurityGroupID);
        String theClass = "tdHeader";
        if (!working_bltWorkHistory.isComplete())
        {
            theClass = "incompleteItem";
        %>
                <span class=incompleteItem><b>Not Complete</b></span><br>
        <%
        }
        %>
               <span class=<%=theClass%> ><b>ID:&nbsp;</b><%=working_bltWorkHistory.getWorkHistoryID()%> </span>
                  </td></tr>
                  <tr><td><b>Item Create Date:&nbsp;</b><jsp:include page="../generic/DateTypeConvert_Main.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(working_bltWorkHistory.getUniqueCreateDate())%>" /></jsp:include></td></tr>
                  <tr><td><b>Item Modify Date:&nbsp;</b><jsp:include page="../generic/DateTypeConvert_Main.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(working_bltWorkHistory.getUniqueModifyDate())%>" /></jsp:include></td></tr>
                  <tr><td><b>Item Modify Comments:&nbsp;</b><%=working_bltWorkHistory.getUniqueModifyComments()%></td></tr>
                  </td></tr></table>
            </td><td width="50%" bgColor=#ffffff> 
        <a class=linkBase href = "tWorkHistory_main_WorkHistory_PhysicianID_form_authorize.jsp?EDIT=edit&EDITID=<%=working_bltWorkHistory.getWorkHistoryID()%>&KM=p"><img border=0 src="ui_<%=thePLCID%>/icons/edit_PhysicianID.gif"></a>


        <%if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {%>
        <br><a class=linkBase  onClick="return confirmDelete()"  href = "tWorkHistory_main_WorkHistory_PhysicianID_form_authorize.jsp?EDIT=del&EDITID=<%=working_bltWorkHistory.getWorkHistoryID()%>&KM=p"><img border=0 src="ui_<%=thePLCID%>/icons/delete_PhysicianID.gif"></a>

        <%}%>
                  </td></tr>
                 </table>
                 <table width="100%" border="1" cellspacing="0" bordercolor="#333333">
                  <tr>
                   <td>
                     <table width="100%" border="0" cellspacing="0">
                     <tr><td>
<%String theClassF = "textBase";%>

<%theClassF = "textBase";%>
<%if ((working_bltWorkHistory.isExpired("WorkHistoryTypeID",expiredDays))&&(working_bltWorkHistory.isExpiredCheck("WorkHistoryTypeID"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltWorkHistory.isRequired("WorkHistoryTypeID"))&&(!working_bltWorkHistory.isComplete("WorkHistoryTypeID")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Work Type:&nbsp;</b><jsp:include page="../generic/tWorkHistoryTypeLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltWorkHistory.getWorkHistoryTypeID()%>" /></jsp:include></p>

<%theClassF = "textBase";%>
<%if ((working_bltWorkHistory.isExpired("Name",expiredDays))&&(working_bltWorkHistory.isExpiredCheck("Name"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltWorkHistory.isRequired("Name"))&&(!working_bltWorkHistory.isComplete("Name")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Company:&nbsp;</b><%=working_bltWorkHistory.getName()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltWorkHistory.isExpired("WorkDescription",expiredDays))&&(working_bltWorkHistory.isExpiredCheck("WorkDescription"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltWorkHistory.isRequired("WorkDescription"))&&(!working_bltWorkHistory.isComplete("WorkDescription")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Work Description:&nbsp;</b><%=working_bltWorkHistory.getWorkDescription()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltWorkHistory.isExpired("FromDate",expiredDays))&&(working_bltWorkHistory.isExpiredCheck("FromDate"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltWorkHistory.isRequired("FromDate"))&&(!working_bltWorkHistory.isComplete("FromDate")) ){theClassF = "requiredFieldMain";}%>

                        <p class=<%=theClassF%> ><b>Start Date:&nbsp;</b><jsp:include page="../generic/DateTypeConvert_Main.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(working_bltWorkHistory.getFromDate())%>" /></jsp:include></p>


<%theClassF = "textBase";%>
<%if ((working_bltWorkHistory.isExpired("ToDate",expiredDays))&&(working_bltWorkHistory.isExpiredCheck("ToDate"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltWorkHistory.isRequired("ToDate"))&&(!working_bltWorkHistory.isComplete("ToDate")) ){theClassF = "requiredFieldMain";}%>

                        <p class=<%=theClassF%> ><b>End Date:&nbsp;</b><jsp:include page="../generic/DateTypeConvert_Main.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(working_bltWorkHistory.getToDate())%>" /></jsp:include></p>


<%theClassF = "textBase";%>
<%if ((working_bltWorkHistory.isExpired("ReasonForLeaving",expiredDays))&&(working_bltWorkHistory.isExpiredCheck("ReasonForLeaving"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltWorkHistory.isRequired("ReasonForLeaving"))&&(!working_bltWorkHistory.isComplete("ReasonForLeaving")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Reason for leaving:&nbsp;</b><%=working_bltWorkHistory.getReasonForLeaving()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltWorkHistory.isExpired("Address1",expiredDays))&&(working_bltWorkHistory.isExpiredCheck("Address1"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltWorkHistory.isRequired("Address1"))&&(!working_bltWorkHistory.isComplete("Address1")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Company Address:&nbsp;</b><%=working_bltWorkHistory.getAddress1()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltWorkHistory.isExpired("Address2",expiredDays))&&(working_bltWorkHistory.isExpiredCheck("Address2"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltWorkHistory.isRequired("Address2"))&&(!working_bltWorkHistory.isComplete("Address2")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Address 2:&nbsp;</b><%=working_bltWorkHistory.getAddress2()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltWorkHistory.isExpired("City",expiredDays))&&(working_bltWorkHistory.isExpiredCheck("City"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltWorkHistory.isRequired("City"))&&(!working_bltWorkHistory.isComplete("City")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>City, Town, Province:&nbsp;</b><%=working_bltWorkHistory.getCity()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltWorkHistory.isExpired("StateID",expiredDays))&&(working_bltWorkHistory.isExpiredCheck("StateID"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltWorkHistory.isRequired("StateID"))&&(!working_bltWorkHistory.isComplete("StateID")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>State:&nbsp;</b><jsp:include page="../generic/tStateLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltWorkHistory.getStateID()%>" /></jsp:include></p>

<%theClassF = "textBase";%>
<%if ((working_bltWorkHistory.isExpired("Province",expiredDays))&&(working_bltWorkHistory.isExpiredCheck("Province"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltWorkHistory.isRequired("Province"))&&(!working_bltWorkHistory.isComplete("Province")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Province, District, State:&nbsp;</b><%=working_bltWorkHistory.getProvince()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltWorkHistory.isExpired("ZIP",expiredDays))&&(working_bltWorkHistory.isExpiredCheck("ZIP"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltWorkHistory.isRequired("ZIP"))&&(!working_bltWorkHistory.isComplete("ZIP")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>ZIP:&nbsp;</b><%=working_bltWorkHistory.getZIP()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltWorkHistory.isExpired("CountryID",expiredDays))&&(working_bltWorkHistory.isExpiredCheck("CountryID"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltWorkHistory.isRequired("CountryID"))&&(!working_bltWorkHistory.isComplete("CountryID")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Country:&nbsp;</b><jsp:include page="../generic/tCountryLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltWorkHistory.getCountryID()%>" /></jsp:include></p>

<%theClassF = "textBase";%>
<%if ((working_bltWorkHistory.isExpired("Phone",expiredDays))&&(working_bltWorkHistory.isExpiredCheck("Phone"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltWorkHistory.isRequired("Phone"))&&(!working_bltWorkHistory.isComplete("Phone")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Phone (XXX-XXX-XXXX):&nbsp;</b><%=working_bltWorkHistory.getPhone()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltWorkHistory.isExpired("Fax",expiredDays))&&(working_bltWorkHistory.isExpiredCheck("Fax"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltWorkHistory.isRequired("Fax"))&&(!working_bltWorkHistory.isComplete("Fax")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Fax (XXX-XXX-XXXX):&nbsp;</b><%=working_bltWorkHistory.getFax()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltWorkHistory.isExpired("ContactName",expiredDays))&&(working_bltWorkHistory.isExpiredCheck("ContactName"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltWorkHistory.isRequired("ContactName"))&&(!working_bltWorkHistory.isComplete("ContactName")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Contact Name:&nbsp;</b><%=working_bltWorkHistory.getContactName()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltWorkHistory.isExpired("ContactEmail",expiredDays))&&(working_bltWorkHistory.isExpiredCheck("ContactEmail"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltWorkHistory.isRequired("ContactEmail"))&&(!working_bltWorkHistory.isComplete("ContactEmail")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Contact E-mail:&nbsp;</b><%=working_bltWorkHistory.getContactEmail()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltWorkHistory.isExpired("Comments",expiredDays))&&(working_bltWorkHistory.isExpiredCheck("Comments"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltWorkHistory.isRequired("Comments"))&&(!working_bltWorkHistory.isComplete("Comments")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Extra Comments:&nbsp;</b><%=working_bltWorkHistory.getComments()%></p>

        </td></tr></table></table><br>        <%
    }
    %>
    </table>
    <%
  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>

    </td></tr></table>


<%String bnIncludeFN = "ui_"+thePLCID+"\\bot-nav_PhysicianID.jsp";
%>
<jsp:include page="<%=bnIncludeFN%>" flush="true" ><jsp:param name="plcID" value="<%=thePLCID%>"/></jsp:include>
