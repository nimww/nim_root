<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.bltDocumentManagement,java.util.Vector,java.util.Enumeration,com.winstaff.ListElement, com.winstaff.PLCUtils, com.winstaff.SecurityCheck, com.winstaff.ConfigurationMessages,com.winstaff.bltPhysicianMaster,com.winstaff.bltPhysicianPracticeLU,com.winstaff.bltPhysicianPracticeLU_List_LU_PracticeID" %>
<%/*
    filename: out\jsp\tPhysicianPracticeLU_main_PhysicianMaster_PracticeID_form_authorize.jsp
    Created on Mar/21/2003
    Type: n-n main class file
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>


<%
//initial declaration of list class and parentID
    Integer        iPracticeID        =    null;
    boolean accessValid = false;

   Integer iSecurityCheck = SecurityCheck.CheckItem("PractitionerSection1", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {

    if (pageControllerHash.containsKey("iPracticeID")) 
    {
        iPracticeID        =    (Integer)pageControllerHash.get("iPracticeID");
        accessValid = true;
    }
  //page security
  if (accessValid)
  {
        Integer requestID = null;
        if (request.getParameter("EDITID")!=null)
        {
            requestID = new Integer(request.getParameter("EDITID"));
            out.println(requestID);
        }
    bltPhysicianPracticeLU_List_LU_PracticeID        bltPhysicianPracticeLU_List_LU_PracticeID        =    new    bltPhysicianPracticeLU_List_LU_PracticeID(iPracticeID,"PhysicianID="+requestID,"");

//declaration of Enumeration
    bltPhysicianPracticeLU        working_bltPhysicianPracticeLU;
    ListElement         leCurrentElement;
    java.util.Enumeration eList = bltPhysicianPracticeLU_List_LU_PracticeID.elements();
    %>
    <%
    if (eList.hasMoreElements())
    {
        leCurrentElement    = (ListElement) eList.nextElement();
        working_bltPhysicianPracticeLU  = (bltPhysicianPracticeLU) leCurrentElement.getObject();
        pageControllerHash.put("iPhysicianID",working_bltPhysicianPracticeLU.getPhysicianID());
        session.setAttribute("pageControllerHash",pageControllerHash);
        //Parameter Pass Code here
String parameterPassString ="";
java.util.Enumeration myParameterPassList = request.getParameterNames();
while (myParameterPassList.hasMoreElements())
{
	String myName = (String)myParameterPassList.nextElement();
	String myS = (String) request.getParameter(myName);
	parameterPassString+="&"+myName + "=" + myS;
}
        response.sendRedirect("tPhysicianMaster_form.jsp?nullParam=null"+parameterPassString);
    }
    else
    {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORInvalidQuery")+"</p>");
    }

  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>


