<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.bltDocumentManagement,java.util.Vector,java.util.Enumeration,com.winstaff.ListElement, com.winstaff.PLCUtils, com.winstaff.SecurityCheck, com.winstaff.ConfigurationMessages,com.winstaff.bltExperience,com.winstaff.bltExperience_List" %>
<%/*
    filename: tExperience_main_Experience_PhysicianID.jsp
    Created on Mar/21/2003
    Type: 1-n main class file
    Created by: Scott Ellis
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>

<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_PhysicianID.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>

    <table cellpadding=0 cellspacing=0 border=0 width=<%=MasterTableWidth%> >
    <tr><td width=10>&nbsp;</td><td>
    <%=ConfigurationMessages.getHTML("INTERVIEWTopControl","tExperience")%>

    <br><a href="tExperience_main_Experience_PhysicianID.jsp">Compact</a>

<%
//initial declaration of list class and parentID
    Integer        iPhysicianID        =    null;
    boolean accessValid = false;
   Integer iSecurityCheck = SecurityCheck.CheckItem("PractitionerSection7", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {
    if (pageControllerHash.containsKey("iPhysicianID")) 
    {
        iPhysicianID        =    (Integer)pageControllerHash.get("iPhysicianID");
        accessValid = true;
    }
  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);
      pageControllerHash.put("sParentReturnPage","tExperience_main_Experience_PhysicianID_Expand.jsp");
    pageControllerHash.remove("iExperienceID");
    session.setAttribute("pageControllerHash",pageControllerHash);

    bltExperience_List        bltExperience_List        =    new    bltExperience_List(iPhysicianID);

//declaration of Enumeration
    bltExperience        working_bltExperience;
    ListElement         leCurrentElement;
    Enumeration eList = bltExperience_List.elements();
    %>
        <%@ include file="tExperience_main_Experience_PhysicianID_instructions.jsp" %>

        <%if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {%>
        <a class=linkBase href = "tExperience_main_Experience_PhysicianID_form_create.jsp?EDIT=new&KM=p"><img border=0 src="ui_<%=thePLCID%>/icons/create_PhysicianID.gif"></a>
        <%}%>
    <%
    while (eList.hasMoreElements())
    {
         %>
         <table border="0" bordercolor="333333" cellpadding="3" class=tdHeaderAlt cellspacing="0" width="100%">
            <tr>
              <td width="50%">
                   <table border="0" bordercolor="333333" cellpadding="3" class=tdHeaderAlt cellspacing="0" width="100%">
                     <tr> 
                       <td>
         <%

        leCurrentElement    = (ListElement) eList.nextElement();
        working_bltExperience  = (bltExperience) leCurrentElement.getObject();
        working_bltExperience.GroupSecurityInit(UserSecurityGroupID);
        String theClass = "tdHeader";
        if (!working_bltExperience.isComplete())
        {
            theClass = "incompleteItem";
        %>
                <span class=incompleteItem><b>Not Complete</b></span><br>
        <%
        }
        %>
               <span class=<%=theClass%> ><b>ID:&nbsp;</b><%=working_bltExperience.getExperienceID()%> </span>
                  </td></tr>
                  <tr><td><b>Item Create Date:&nbsp;</b><jsp:include page="../generic/DateTypeConvert_Main.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(working_bltExperience.getUniqueCreateDate())%>" /></jsp:include></td></tr>
                  <tr><td><b>Item Modify Date:&nbsp;</b><jsp:include page="../generic/DateTypeConvert_Main.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(working_bltExperience.getUniqueModifyDate())%>" /></jsp:include></td></tr>
                  <tr><td><b>Item Modify Comments:&nbsp;</b><%=working_bltExperience.getUniqueModifyComments()%></td></tr>
                  </td></tr></table>
            </td><td width="50%" bgColor=#ffffff> 
        <a class=linkBase href = "tExperience_main_Experience_PhysicianID_form_authorize.jsp?EDIT=edit&EDITID=<%=working_bltExperience.getExperienceID()%>&KM=p"><img border=0 src="ui_<%=thePLCID%>/icons/edit_PhysicianID.gif"></a>


        <%if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {%>
        <br><a class=linkBase  onClick="return confirmDelete()"  href = "tExperience_main_Experience_PhysicianID_form_authorize.jsp?EDIT=del&EDITID=<%=working_bltExperience.getExperienceID()%>&KM=p"><img border=0 src="ui_<%=thePLCID%>/icons/delete_PhysicianID.gif"></a>

        <%}%>
                  </td></tr>
                 </table>
                 <table width="100%" border="1" cellspacing="0" bordercolor="#333333">
                  <tr>
                   <td>
                     <table width="100%" border="0" cellspacing="0">
                     <tr><td>
<%String theClassF = "textBase";%>

<%theClassF = "textBase";%>
<%if ((working_bltExperience.isExpired("TypeOfExperience",expiredDays))&&(working_bltExperience.isExpiredCheck("TypeOfExperience"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltExperience.isRequired("TypeOfExperience"))&&(!working_bltExperience.isComplete("TypeOfExperience")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Experience Type:&nbsp;</b><jsp:include page="../generic/tExperienceTypeLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltExperience.getTypeOfExperience()%>" /></jsp:include></p>

<%theClassF = "textBase";%>
<%if ((working_bltExperience.isExpired("Other",expiredDays))&&(working_bltExperience.isExpiredCheck("Other"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltExperience.isRequired("Other"))&&(!working_bltExperience.isComplete("Other")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>If other, list here:&nbsp;</b><%=working_bltExperience.getOther()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltExperience.isExpired("Specialty",expiredDays))&&(working_bltExperience.isExpiredCheck("Specialty"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltExperience.isRequired("Specialty"))&&(!working_bltExperience.isComplete("Specialty")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Specialty, Focus, Course Name:&nbsp;</b><%=working_bltExperience.getSpecialty()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltExperience.isExpired("DateFrom",expiredDays))&&(working_bltExperience.isExpiredCheck("DateFrom"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltExperience.isRequired("DateFrom"))&&(!working_bltExperience.isComplete("DateFrom")) ){theClassF = "requiredFieldMain";}%>

                        <p class=<%=theClassF%> ><b>Start Date:&nbsp;</b><jsp:include page="../generic/DateTypeConvert_Main.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(working_bltExperience.getDateFrom())%>" /></jsp:include></p>


<%theClassF = "textBase";%>
<%if ((working_bltExperience.isExpired("DateTo",expiredDays))&&(working_bltExperience.isExpiredCheck("DateTo"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltExperience.isRequired("DateTo"))&&(!working_bltExperience.isComplete("DateTo")) ){theClassF = "requiredFieldMain";}%>

                        <p class=<%=theClassF%> ><b>End Date:&nbsp;</b><jsp:include page="../generic/DateTypeConvert_Main.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(working_bltExperience.getDateTo())%>" /></jsp:include></p>


<%theClassF = "textBase";%>
<%if ((working_bltExperience.isExpired("NoComplete",expiredDays))&&(working_bltExperience.isExpiredCheck("NoComplete"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltExperience.isRequired("NoComplete"))&&(!working_bltExperience.isComplete("NoComplete")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>If you did not complete this program, please explain here:&nbsp;</b><%=working_bltExperience.getNoComplete()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltExperience.isExpired("Name",expiredDays))&&(working_bltExperience.isExpiredCheck("Name"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltExperience.isRequired("Name"))&&(!working_bltExperience.isComplete("Name")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Institution:&nbsp;</b><%=working_bltExperience.getName()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltExperience.isExpired("Address1",expiredDays))&&(working_bltExperience.isExpiredCheck("Address1"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltExperience.isRequired("Address1"))&&(!working_bltExperience.isComplete("Address1")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Address:&nbsp;</b><%=working_bltExperience.getAddress1()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltExperience.isExpired("Address2",expiredDays))&&(working_bltExperience.isExpiredCheck("Address2"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltExperience.isRequired("Address2"))&&(!working_bltExperience.isComplete("Address2")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Address 2:&nbsp;</b><%=working_bltExperience.getAddress2()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltExperience.isExpired("City",expiredDays))&&(working_bltExperience.isExpiredCheck("City"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltExperience.isRequired("City"))&&(!working_bltExperience.isComplete("City")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Institution City:&nbsp;</b><%=working_bltExperience.getCity()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltExperience.isExpired("StateID",expiredDays))&&(working_bltExperience.isExpiredCheck("StateID"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltExperience.isRequired("StateID"))&&(!working_bltExperience.isComplete("StateID")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>State:&nbsp;</b><jsp:include page="../generic/tStateLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltExperience.getStateID()%>" /></jsp:include></p>

<%theClassF = "textBase";%>
<%if ((working_bltExperience.isExpired("Province",expiredDays))&&(working_bltExperience.isExpiredCheck("Province"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltExperience.isRequired("Province"))&&(!working_bltExperience.isComplete("Province")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Province, District, State:&nbsp;</b><%=working_bltExperience.getProvince()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltExperience.isExpired("ZIP",expiredDays))&&(working_bltExperience.isExpiredCheck("ZIP"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltExperience.isRequired("ZIP"))&&(!working_bltExperience.isComplete("ZIP")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>ZIP:&nbsp;</b><%=working_bltExperience.getZIP()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltExperience.isExpired("CountryID",expiredDays))&&(working_bltExperience.isExpiredCheck("CountryID"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltExperience.isRequired("CountryID"))&&(!working_bltExperience.isComplete("CountryID")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Country:&nbsp;</b><jsp:include page="../generic/tCountryLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltExperience.getCountryID()%>" /></jsp:include></p>

<%theClassF = "textBase";%>
<%if ((working_bltExperience.isExpired("Phone",expiredDays))&&(working_bltExperience.isExpiredCheck("Phone"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltExperience.isRequired("Phone"))&&(!working_bltExperience.isComplete("Phone")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Phone (XXX-XXX-XXXX):&nbsp;</b><%=working_bltExperience.getPhone()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltExperience.isExpired("Fax",expiredDays))&&(working_bltExperience.isExpiredCheck("Fax"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltExperience.isRequired("Fax"))&&(!working_bltExperience.isComplete("Fax")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Fax (XXX-XXX-XXXX):&nbsp;</b><%=working_bltExperience.getFax()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltExperience.isExpired("ContactName",expiredDays))&&(working_bltExperience.isExpiredCheck("ContactName"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltExperience.isRequired("ContactName"))&&(!working_bltExperience.isComplete("ContactName")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Contact Name/Program Director:&nbsp;</b><%=working_bltExperience.getContactName()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltExperience.isExpired("ContactEmail",expiredDays))&&(working_bltExperience.isExpiredCheck("ContactEmail"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltExperience.isRequired("ContactEmail"))&&(!working_bltExperience.isComplete("ContactEmail")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Contact E-mail:&nbsp;</b><%=working_bltExperience.getContactEmail()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltExperience.isExpired("Comments",expiredDays))&&(working_bltExperience.isExpiredCheck("Comments"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltExperience.isRequired("Comments"))&&(!working_bltExperience.isComplete("Comments")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Extra Comments:&nbsp;</b><%=working_bltExperience.getComments()%></p>

        </td></tr></table></table><br>        <%
    }
    %>
    </table>
    <%
  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>

    </td></tr></table>


<%String bnIncludeFN = "ui_"+thePLCID+"\\bot-nav_PhysicianID.jsp";
%>
<jsp:include page="<%=bnIncludeFN%>" flush="true" ><jsp:param name="plcID" value="<%=thePLCID%>"/></jsp:include>
