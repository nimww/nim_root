<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.bltDocumentManagement,java.util.Vector,java.util.Enumeration,com.winstaff.ListElement, com.winstaff.PLCUtils, com.winstaff.SecurityCheck, com.winstaff.ConfigurationMessages,com.winstaff.bltFacilityAffiliation,com.winstaff.bltFacilityAffiliation_List" %>
<%/*
    filename: tFacilityAffiliation_main_FacilityAffiliation_PhysicianID.jsp
    Created on Mar/21/2003
    Type: 1-n main class file
    Created by: Scott Ellis
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>

<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_PhysicianID.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>

    <table cellpadding=0 cellspacing=0 border=0 width=<%=MasterTableWidth%> >
    <tr><td width=10>&nbsp;</td><td>
    <%=ConfigurationMessages.getHTML("INTERVIEWTopControl","tFacilityAffiliation")%>

    <br><a href="tFacilityAffiliation_main_FacilityAffiliation_PhysicianID.jsp">Compact</a>

<%
//initial declaration of list class and parentID
    Integer        iPhysicianID        =    null;
    boolean accessValid = false;
   Integer iSecurityCheck = SecurityCheck.CheckItem("PractitionerSection10", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {
    if (pageControllerHash.containsKey("iPhysicianID")) 
    {
        iPhysicianID        =    (Integer)pageControllerHash.get("iPhysicianID");
        accessValid = true;
    }
  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);
      pageControllerHash.put("sParentReturnPage","tFacilityAffiliation_main_FacilityAffiliation_PhysicianID_Expand.jsp");
    pageControllerHash.remove("iAffiliationID");
    session.setAttribute("pageControllerHash",pageControllerHash);

    bltFacilityAffiliation_List        bltFacilityAffiliation_List        =    new    bltFacilityAffiliation_List(iPhysicianID);

//declaration of Enumeration
    bltFacilityAffiliation        working_bltFacilityAffiliation;
    ListElement         leCurrentElement;
    Enumeration eList = bltFacilityAffiliation_List.elements();
    %>
        <%@ include file="tFacilityAffiliation_main_FacilityAffiliation_PhysicianID_instructions.jsp" %>

        <%if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {%>
        <a class=linkBase href = "tFacilityAffiliation_main_FacilityAffiliation_PhysicianID_form_create.jsp?EDIT=new&KM=p"><img border=0 src="ui_<%=thePLCID%>/icons/create_PhysicianID.gif"></a>
        <%}%>
    <%
    while (eList.hasMoreElements())
    {
         %>
         <table border="0" bordercolor="333333" cellpadding="3" class=tdHeaderAlt cellspacing="0" width="100%">
            <tr>
              <td width="50%">
                   <table border="0" bordercolor="333333" cellpadding="3" class=tdHeaderAlt cellspacing="0" width="100%">
                     <tr> 
                       <td>
         <%

        leCurrentElement    = (ListElement) eList.nextElement();
        working_bltFacilityAffiliation  = (bltFacilityAffiliation) leCurrentElement.getObject();
        working_bltFacilityAffiliation.GroupSecurityInit(UserSecurityGroupID);
        String theClass = "tdHeader";
        if (!working_bltFacilityAffiliation.isComplete())
        {
            theClass = "incompleteItem";
        %>
                <span class=incompleteItem><b>Not Complete</b></span><br>
        <%
        }
        %>
               <span class=<%=theClass%> ><b>ID:&nbsp;</b><%=working_bltFacilityAffiliation.getAffiliationID()%> </span>
                  </td></tr>
                  <tr><td><b>Item Create Date:&nbsp;</b><jsp:include page="../generic/DateTypeConvert_Main.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(working_bltFacilityAffiliation.getUniqueCreateDate())%>" /></jsp:include></td></tr>
                  <tr><td><b>Item Modify Date:&nbsp;</b><jsp:include page="../generic/DateTypeConvert_Main.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(working_bltFacilityAffiliation.getUniqueModifyDate())%>" /></jsp:include></td></tr>
                  <tr><td><b>Item Modify Comments:&nbsp;</b><%=working_bltFacilityAffiliation.getUniqueModifyComments()%></td></tr>
                  </td></tr></table>
            </td><td width="50%" bgColor=#ffffff> 
        <a class=linkBase href = "tFacilityAffiliation_main_FacilityAffiliation_PhysicianID_form_authorize.jsp?EDIT=edit&EDITID=<%=working_bltFacilityAffiliation.getAffiliationID()%>&KM=p"><img border=0 src="ui_<%=thePLCID%>/icons/edit_PhysicianID.gif"></a>


        <%if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {%>
        <br><a class=linkBase  onClick="return confirmDelete()"  href = "tFacilityAffiliation_main_FacilityAffiliation_PhysicianID_form_authorize.jsp?EDIT=del&EDITID=<%=working_bltFacilityAffiliation.getAffiliationID()%>&KM=p"><img border=0 src="ui_<%=thePLCID%>/icons/delete_PhysicianID.gif"></a>

        <%}%>
                  </td></tr>
                 </table>
                 <table width="100%" border="1" cellspacing="0" bordercolor="#333333">
                  <tr>
                   <td>
                     <table width="100%" border="0" cellspacing="0">
                     <tr><td>
<%String theClassF = "textBase";%>

<%theClassF = "textBase";%>
<%if ((working_bltFacilityAffiliation.isExpired("IsPrimary",expiredDays))&&(working_bltFacilityAffiliation.isExpiredCheck("IsPrimary"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltFacilityAffiliation.isRequired("IsPrimary"))&&(!working_bltFacilityAffiliation.isComplete("IsPrimary")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Appointment Type:&nbsp;</b><jsp:include page="../generic/tAppointmentTypeLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltFacilityAffiliation.getIsPrimary()%>" /></jsp:include></p>

<%theClassF = "textBase";%>
<%if ((working_bltFacilityAffiliation.isExpired("AppointmentLevel",expiredDays))&&(working_bltFacilityAffiliation.isExpiredCheck("AppointmentLevel"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltFacilityAffiliation.isRequired("AppointmentLevel"))&&(!working_bltFacilityAffiliation.isComplete("AppointmentLevel")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Appointment/status level:&nbsp;</b><%=working_bltFacilityAffiliation.getAppointmentLevel()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltFacilityAffiliation.isExpired("StartDate",expiredDays))&&(working_bltFacilityAffiliation.isExpiredCheck("StartDate"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltFacilityAffiliation.isRequired("StartDate"))&&(!working_bltFacilityAffiliation.isComplete("StartDate")) ){theClassF = "requiredFieldMain";}%>

                        <p class=<%=theClassF%> ><b>Start Date:&nbsp;</b><jsp:include page="../generic/DateTypeConvert_Main.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(working_bltFacilityAffiliation.getStartDate())%>" /></jsp:include></p>


<%theClassF = "textBase";%>
<%if ((working_bltFacilityAffiliation.isExpired("EndDate",expiredDays))&&(working_bltFacilityAffiliation.isExpiredCheck("EndDate"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltFacilityAffiliation.isRequired("EndDate"))&&(!working_bltFacilityAffiliation.isComplete("EndDate")) ){theClassF = "requiredFieldMain";}%>

                        <p class=<%=theClassF%> ><b>End Date:&nbsp;</b><jsp:include page="../generic/DateTypeConvert_Main.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(working_bltFacilityAffiliation.getEndDate())%>" /></jsp:include></p>


<%theClassF = "textBase";%>
<%if ((working_bltFacilityAffiliation.isExpired("PendingDate",expiredDays))&&(working_bltFacilityAffiliation.isExpiredCheck("PendingDate"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltFacilityAffiliation.isRequired("PendingDate"))&&(!working_bltFacilityAffiliation.isComplete("PendingDate")) ){theClassF = "requiredFieldMain";}%>

                        <p class=<%=theClassF%> ><b>If this appointment is pending, please enter the date you submitted your application:&nbsp;</b><jsp:include page="../generic/DateTypeConvert_Main.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(working_bltFacilityAffiliation.getPendingDate())%>" /></jsp:include></p>


<%theClassF = "textBase";%>
<%if ((working_bltFacilityAffiliation.isExpired("FacilityName",expiredDays))&&(working_bltFacilityAffiliation.isExpiredCheck("FacilityName"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltFacilityAffiliation.isRequired("FacilityName"))&&(!working_bltFacilityAffiliation.isComplete("FacilityName")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Facility Name:&nbsp;</b><%=working_bltFacilityAffiliation.getFacilityName()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltFacilityAffiliation.isExpired("FacilityDepartment",expiredDays))&&(working_bltFacilityAffiliation.isExpiredCheck("FacilityDepartment"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltFacilityAffiliation.isRequired("FacilityDepartment"))&&(!working_bltFacilityAffiliation.isComplete("FacilityDepartment")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Facility Department:&nbsp;</b><%=working_bltFacilityAffiliation.getFacilityDepartment()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltFacilityAffiliation.isExpired("FacilityAddress1",expiredDays))&&(working_bltFacilityAffiliation.isExpiredCheck("FacilityAddress1"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltFacilityAffiliation.isRequired("FacilityAddress1"))&&(!working_bltFacilityAffiliation.isComplete("FacilityAddress1")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Facility Address:&nbsp;</b><%=working_bltFacilityAffiliation.getFacilityAddress1()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltFacilityAffiliation.isExpired("FacilityAddress2",expiredDays))&&(working_bltFacilityAffiliation.isExpiredCheck("FacilityAddress2"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltFacilityAffiliation.isRequired("FacilityAddress2"))&&(!working_bltFacilityAffiliation.isComplete("FacilityAddress2")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Address 2:&nbsp;</b><%=working_bltFacilityAffiliation.getFacilityAddress2()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltFacilityAffiliation.isExpired("FacilityCity",expiredDays))&&(working_bltFacilityAffiliation.isExpiredCheck("FacilityCity"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltFacilityAffiliation.isRequired("FacilityCity"))&&(!working_bltFacilityAffiliation.isComplete("FacilityCity")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>City, Town, Province:&nbsp;</b><%=working_bltFacilityAffiliation.getFacilityCity()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltFacilityAffiliation.isExpired("FacilityStateID",expiredDays))&&(working_bltFacilityAffiliation.isExpiredCheck("FacilityStateID"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltFacilityAffiliation.isRequired("FacilityStateID"))&&(!working_bltFacilityAffiliation.isComplete("FacilityStateID")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>State:&nbsp;</b><jsp:include page="../generic/tStateLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltFacilityAffiliation.getFacilityStateID()%>" /></jsp:include></p>

<%theClassF = "textBase";%>
<%if ((working_bltFacilityAffiliation.isExpired("FacilityProvince",expiredDays))&&(working_bltFacilityAffiliation.isExpiredCheck("FacilityProvince"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltFacilityAffiliation.isRequired("FacilityProvince"))&&(!working_bltFacilityAffiliation.isComplete("FacilityProvince")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Province, District, State:&nbsp;</b><%=working_bltFacilityAffiliation.getFacilityProvince()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltFacilityAffiliation.isExpired("FacilityZIP",expiredDays))&&(working_bltFacilityAffiliation.isExpiredCheck("FacilityZIP"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltFacilityAffiliation.isRequired("FacilityZIP"))&&(!working_bltFacilityAffiliation.isComplete("FacilityZIP")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>ZIP:&nbsp;</b><%=working_bltFacilityAffiliation.getFacilityZIP()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltFacilityAffiliation.isExpired("FacilityCountryID",expiredDays))&&(working_bltFacilityAffiliation.isExpiredCheck("FacilityCountryID"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltFacilityAffiliation.isRequired("FacilityCountryID"))&&(!working_bltFacilityAffiliation.isComplete("FacilityCountryID")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Country:&nbsp;</b><jsp:include page="../generic/tCountryLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltFacilityAffiliation.getFacilityCountryID()%>" /></jsp:include></p>

<%theClassF = "textBase";%>
<%if ((working_bltFacilityAffiliation.isExpired("FacilityPhone",expiredDays))&&(working_bltFacilityAffiliation.isExpiredCheck("FacilityPhone"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltFacilityAffiliation.isRequired("FacilityPhone"))&&(!working_bltFacilityAffiliation.isComplete("FacilityPhone")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Facility Phone Number (XXX-XXX-XXXX):&nbsp;</b><%=working_bltFacilityAffiliation.getFacilityPhone()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltFacilityAffiliation.isExpired("FacilityFax",expiredDays))&&(working_bltFacilityAffiliation.isExpiredCheck("FacilityFax"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltFacilityAffiliation.isRequired("FacilityFax"))&&(!working_bltFacilityAffiliation.isComplete("FacilityFax")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Facility Fax (XXX-XXX-XXXX):&nbsp;</b><%=working_bltFacilityAffiliation.getFacilityFax()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltFacilityAffiliation.isExpired("ContactName",expiredDays))&&(working_bltFacilityAffiliation.isExpiredCheck("ContactName"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltFacilityAffiliation.isRequired("ContactName"))&&(!working_bltFacilityAffiliation.isComplete("ContactName")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Contact Name:&nbsp;</b><%=working_bltFacilityAffiliation.getContactName()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltFacilityAffiliation.isExpired("ContactEmail",expiredDays))&&(working_bltFacilityAffiliation.isExpiredCheck("ContactEmail"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltFacilityAffiliation.isRequired("ContactEmail"))&&(!working_bltFacilityAffiliation.isComplete("ContactEmail")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Contact E-mail:&nbsp;</b><%=working_bltFacilityAffiliation.getContactEmail()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltFacilityAffiliation.isExpired("ReasonForLeaving",expiredDays))&&(working_bltFacilityAffiliation.isExpiredCheck("ReasonForLeaving"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltFacilityAffiliation.isRequired("ReasonForLeaving"))&&(!working_bltFacilityAffiliation.isComplete("ReasonForLeaving")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>If you are no longer affiliated, please explain your reason for leaving:&nbsp;</b><%=working_bltFacilityAffiliation.getReasonForLeaving()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltFacilityAffiliation.isExpired("AdmissionPriviledges",expiredDays))&&(working_bltFacilityAffiliation.isExpiredCheck("AdmissionPriviledges"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltFacilityAffiliation.isRequired("AdmissionPriviledges"))&&(!working_bltFacilityAffiliation.isComplete("AdmissionPriviledges")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Do you have admission privileges?:&nbsp;</b><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltFacilityAffiliation.getAdmissionPriviledges()%>" /></jsp:include></p>

<%theClassF = "textBase";%>
<%if ((working_bltFacilityAffiliation.isExpired("AdmissionArrangements",expiredDays))&&(working_bltFacilityAffiliation.isExpiredCheck("AdmissionArrangements"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltFacilityAffiliation.isRequired("AdmissionArrangements"))&&(!working_bltFacilityAffiliation.isComplete("AdmissionArrangements")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>If no, Please explain you admission arrangements:&nbsp;</b><%=working_bltFacilityAffiliation.getAdmissionArrangements()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltFacilityAffiliation.isExpired("UnrestrictedAdmission",expiredDays))&&(working_bltFacilityAffiliation.isExpiredCheck("UnrestrictedAdmission"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltFacilityAffiliation.isRequired("UnrestrictedAdmission"))&&(!working_bltFacilityAffiliation.isComplete("UnrestrictedAdmission")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Do you have unresricted admission privileges:&nbsp;</b><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltFacilityAffiliation.getUnrestrictedAdmission()%>" /></jsp:include></p>

<%theClassF = "textBase";%>
<%if ((working_bltFacilityAffiliation.isExpired("TempPriviledges",expiredDays))&&(working_bltFacilityAffiliation.isExpiredCheck("TempPriviledges"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltFacilityAffiliation.isRequired("TempPriviledges"))&&(!working_bltFacilityAffiliation.isComplete("TempPriviledges")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Do you have temporary privileges?:&nbsp;</b><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltFacilityAffiliation.getTempPriviledges()%>" /></jsp:include></p>

<%theClassF = "textBase";%>
<%if ((working_bltFacilityAffiliation.isExpired("InpatientCare",expiredDays))&&(working_bltFacilityAffiliation.isExpiredCheck("InpatientCare"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltFacilityAffiliation.isRequired("InpatientCare"))&&(!working_bltFacilityAffiliation.isComplete("InpatientCare")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Do you provide inpatient care?:&nbsp;</b><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltFacilityAffiliation.getInpatientCare()%>" /></jsp:include></p>

<%theClassF = "textBase";%>
<%if ((working_bltFacilityAffiliation.isExpired("PercentAdmissions",expiredDays))&&(working_bltFacilityAffiliation.isExpiredCheck("PercentAdmissions"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltFacilityAffiliation.isRequired("PercentAdmissions"))&&(!working_bltFacilityAffiliation.isComplete("PercentAdmissions")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Percent Admissions:&nbsp;</b><%=working_bltFacilityAffiliation.getPercentAdmissions()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltFacilityAffiliation.isExpired("Comments",expiredDays))&&(working_bltFacilityAffiliation.isExpiredCheck("Comments"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltFacilityAffiliation.isRequired("Comments"))&&(!working_bltFacilityAffiliation.isComplete("Comments")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Extra Comments:&nbsp;</b><%=working_bltFacilityAffiliation.getComments()%></p>

        </td></tr></table></table><br>        <%
    }
    %>
    </table>
    <%
  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>

    </td></tr></table>


<%String bnIncludeFN = "ui_"+thePLCID+"\\bot-nav_PhysicianID.jsp";
%>
<jsp:include page="<%=bnIncludeFN%>" flush="true" ><jsp:param name="plcID" value="<%=thePLCID%>"/></jsp:include>
