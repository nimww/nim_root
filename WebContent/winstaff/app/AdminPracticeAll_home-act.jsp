
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.*" %>
<%/*
    filename: tAdminPracticeLU_main_LU_AdminID.jsp
    Created on Apr/23/2002
    Type: 1-n main class file
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>

<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_Clear.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>

    <table cellpadding=0 cellspacing=0 border=0 width=100%>
    <tr><td width=10>&nbsp;</td>
    <td>
<p> <br />
   <span class=title>All Open Activities</span><p>


    <%
//initial declaration of list class and parentID
    Integer        iGenAdminID        =    null;
	String		savedSQL = null;
	String		savedURL = null;
    boolean accessValid = false;
    if (pageControllerHash.containsKey("iGenAdminID")) 
    {
        iGenAdminID        =    (Integer)pageControllerHash.get("iGenAdminID");
        accessValid = true;
    }
    if (pageControllerHash.containsKey("SQL_AdminPracticeAll_Query-acc")) 
    {
        savedSQL        =    (String)pageControllerHash.get("SQL_AdminPracticeAll_Query-acc");
//        accessValid = true;
    }
    if (pageControllerHash.containsKey("URL_AdminPracticeAll_Query-acc")) 
    {
        savedURL        =    (String)pageControllerHash.get("URL_AdminPracticeAll_Query-acc");
//        accessValid = true;
    }
  //page security
  if (accessValid)
  {



		java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat("yyyy-MM-dd");
		java.text.SimpleDateFormat dbdf_day = new java.text.SimpleDateFormat("EEEE");
      java.text.SimpleDateFormat dbdft = new java.text.SimpleDateFormat(PLCUtils.String_dbdft);
      java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);
//      pageControllerHash.put("sParentReturnPage","AdminPracticeAll_home-acc.jsp");
    pageControllerHash.remove("iPracticeActivityID");
    pageControllerHash.remove("iAccountID");
    pageControllerHash.remove("iOwnerID");
			session.setAttribute("pageControllerHash",pageControllerHash);
	


%>



<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr> 
    <td> <table border="1">
  <tr class=tdHeaderAlt>
    <td>Assigned To&nbsp;</td>
    <td>Type:Subject&nbsp;</td>
    <td>Related To &nbsp;</td>
    <td>Type&nbsp;</td>
    <td>Due By&nbsp;</td>
  </tr>


<%
searchDB2 mySS = new searchDB2();

java.sql.ResultSet myRS = null;;

try
{
	String mysql=("select tPracticeActivity.name, logonusername, tPracticeActivity.activitytype, tPracticeActivity.reminddate, tAdminMaster.name as aname, 'Account' as stype, tAdminMaster.adminid as iOwnerID, '0' as iPracticeID, tPracticeActivity.practiceactivityid as iActivityID from tPracticeActivity INNER JOIN tAdminMaster on tAdminMaster.adminid=tPracticeActivity.adminid INNER JOIN tUserAccount on tUserAccount.userid = tPracticeActivity.assignedtoid where tPracticeActivity.assignedtoid=  " + CurrentUserAccount.getUserID() + " AND tAdminMaster.adminid>0 and (tPracticeActivity.iscompleted<>1 )    UNION select tPracticeActivity.name, logonusername, tPracticeActivity.activitytype, tPracticeActivity.reminddate, tPracticeMaster.practicename as aname, 'Practice' as stype, tAdminMaster.adminid as iOwnerID, tPracticeMaster.practiceID as iPracticeID, tPracticeActivity.practiceactivityid as iActivityID from tPracticeActivity INNER JOIN tPracticeMaster on tPracticeMaster.practiceid=tPracticeActivity.practiceid INNER JOIN tAdminPracticeLU on tAdminPracticeLU.practiceid=tPracticeMaster.practiceid INNER JOIN tAdminMaster on tAdminmaster.adminid = tAdminPracticeLU.adminid INNER JOIN tUserAccount on tUserAccount.userid = tPracticeActivity.assignedtoid where tPracticeActivity.assignedtoid=  " + CurrentUserAccount.getUserID() +"  AND tPracticeMaster.practiceid>0 and (tPracticeActivity.iscompleted<>1 )   order by reminddate, aname " );
//	out.print(mysql);
	myRS = mySS.executeStatement(mysql);
}
catch(Exception e)
{
	out.println("ResultsSet:"+e);
}
try
{
	int cnt=0;
	while (myRS!=null&&myRS.next())
	{
		cnt++;
		String iOwnerID = myRS.getString("iownerid");
		String iPracticeID = myRS.getString("ipracticeid");
		String iActivityID = myRS.getString("iactivityid");
		String name = myRS.getString("name");
		String aname = myRS.getString("aname");
		String luname = myRS.getString("logonusername");
		String stype = myRS.getString("stype");
		String stype_d = stype;
		if (stype.equalsIgnoreCase("account"))
		{
			stype_d = "<strong class=tdBaseAltPurple>" + stype + "</strong>";
		}
		String activitytype = myRS.getString("activitytype");
		String smyDueDate = "None";
		String smyDueDate_day = "None";
		try
		{
			smyDueDate = PLCUtils.getDisplayDateWithTime(dbdft.parse(myRS.getString("RemindDate")));
			smyDueDate_day = dbdf_day.format(dbdft.parse(myRS.getString("RemindDate")));
			
			if (smyDueDate.equalsIgnoreCase(" ")||smyDueDate.equalsIgnoreCase(""))
			{
				smyDueDate = "None";
				smyDueDate_day = "None";
			}
		}
		catch(Exception edd)
		{
		}

		String myClass = "tdBase";
		if (cnt%2==0)
		{
			myClass = "tdBaseAlt";
		}

%>
  <tr class="<%=myClass%>">
    <td><%=luname%>&nbsp;</td>
    <%	
    if (stype.equalsIgnoreCase("account"))
    {
		%>
        <td><a  href=AdminPracticeAll_query_auth.jsp?EDIT=activity_admin&EDIT2ID=<%=iOwnerID%>&EDIT3ID=<%=iActivityID%>><%=activitytype%>: <%=name%></a>&nbsp;</td>
        <%
	}
    else
    {
		%>
        <td><a  href=AdminPracticeAll_query_auth.jsp?EDIT=activity_practice&EDIT2ID=<%=iOwnerID%>&EDITID=<%=iPracticeID%>&EDIT3ID=<%=iActivityID%>><%=activitytype%>: <%=name%></a>&nbsp;</td>
        <%
	}
	%>
    <td><%=aname%>&nbsp;</td>
    <td><%=stype_d%>&nbsp;</td>
    <td><%=smyDueDate_day%>&nbsp;<%=smyDueDate%>&nbsp;</td>
  </tr>
        
        <%
	}
}
catch(Exception e)
{
	out.println("ResultsSet:"+e);
}

%>
</table>

     </td>
  </tr>
</table>



<%

  }
  else
  {
   response.sendRedirect("/errorhandler.jsp");
  }
%>

    </td></tr></table>

