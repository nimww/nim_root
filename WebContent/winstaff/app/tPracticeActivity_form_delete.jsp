<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.*" %>
<%/*
    filename: out\jsp\tPracticeActivity_form_delete.jsp
    Created on Oct/30/2009
    Type: _form Delete
    Created by: Scott Ellis
    Version: 4.0
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>


<%

//initial declaration of list class and parentID
    Integer        iPracticeActivityID        =    null;
    boolean accessValid = false;

   Integer iSecurityCheck = SecurityCheck.CheckItem("nim1", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {

    if (pageControllerHash.containsKey("iPracticeActivityID")) 
    {
        iPracticeActivityID        =    (Integer)pageControllerHash.get("iPracticeActivityID");
        if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {
            accessValid = true;
        }
    }
  //page security
  if (accessValid)
  {

//initial declaration of list class and parentID

    bltPracticeActivity        PracticeActivity        =    null;

    if ( request.getParameter( "EDIT" ).equalsIgnoreCase("del") )
    {
        PracticeActivity        =    new    bltPracticeActivity(iPracticeActivityID);
        PracticeActivity.setPracticeID(new Integer("0"));
        PracticeActivity.setAdminID(new Integer("0"));
        //if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {
            PracticeActivity.commitData();
            out.println("Successful delete");
        }
    }
String routePageReference = request.getParameter("routePageReference");
String nextPage=null;
if (routePageReference!=null)
{
        if (pageControllerHash.containsKey(routePageReference))
        {
            nextPage = (String)pageControllerHash.get(routePageReference);
        }
        else if (pageControllerHash.containsKey("sLocalChildReturnPage"))
        {
            nextPage = (String)pageControllerHash.get("sLocalChildReturnPage");
        }
}
if (nextPage!=null)
{
    response.sendRedirect(nextPage+"?EDIT=edit");
}
        %>
Successful Delete

  <%
  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
  %>


