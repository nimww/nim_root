<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.bltDocumentManagement,java.util.Vector,java.util.Enumeration,com.winstaff.ListElement, com.winstaff.PLCUtils, com.winstaff.SecurityCheck, com.winstaff.ConfigurationMessages,com.winstaff.bltPhysicianMaster,com.winstaff.bltAdminPhysicianLU,com.winstaff.bltAdminPhysicianLU_List_LU_AdminID" %>
<%/*
    filename: out\jsp\tAdminPhysicianLU_main_PhysicianMaster_AdminID_form_authorize.jsp
    Created on Feb/04/2003
    Type: n-n main class file
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>


<%
//initial declaration of list class and parentID
    Integer        iAdminID        =    null;
    boolean accessValid = false;

   Integer iSecurityCheck = SecurityCheck.CheckItem("PractitionerSection1", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {

    if (pageControllerHash.containsKey("iAdminID")) 
    {
        iAdminID        =    (Integer)pageControllerHash.get("iAdminID");
        accessValid = true;
    }
  //page security
  if (accessValid)
  {
        Integer requestID = null;
        String requestType = "";
        if (request.getParameter("EDITID")!=null)
        {
            requestID = new Integer(request.getParameter("EDITID"));
            out.println(requestID);
        }
    bltAdminPhysicianLU_List_LU_AdminID        bltAdminPhysicianLU_List_LU_AdminID        =    new    bltAdminPhysicianLU_List_LU_AdminID(iAdminID,"PhysicianID="+requestID,"");

//declaration of Enumeration
    bltAdminPhysicianLU        working_bltAdminPhysicianLU;


    com.winstaff.ListElement         leCurrentElement;
    java.util.Enumeration eList = bltAdminPhysicianLU_List_LU_AdminID.elements();
    %>
    <%
    if (eList.hasMoreElements())
    {
        leCurrentElement    = (com.winstaff.ListElement) eList.nextElement();
        working_bltAdminPhysicianLU  = (bltAdminPhysicianLU) leCurrentElement.getObject();
        pageControllerHash.put("iPhysicianID",working_bltAdminPhysicianLU.getPhysicianID());
        session.setAttribute("pageControllerHash",pageControllerHash);
        //Parameter Pass Code here
String parameterPassString ="";
java.util.Enumeration myParameterPassList = request.getParameterNames();
while (myParameterPassList.hasMoreElements())
{
	String myName = (String)myParameterPassList.nextElement();
	String myS = (String) request.getParameter(myName);
	parameterPassString+="&"+myName + "=" + myS;
}

        if (request.getParameter("EDIT")!=null)
        {
            requestType = new String(request.getParameter("EDIT"));
        }

	if (requestType.equalsIgnoreCase("mmsform"))
	{

	        response.sendRedirect("formsAdv/select.jsp?whichForm=Integrated_Mass_Re-credentialing_Application_WithProfile.pdf");
	}
	else
	{
	        response.sendRedirect("pro-file_Status.jsp?nullParam=null"+parameterPassString);
	}
    }
    else
    {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORInvalidQuery")+"</p>");
    }

  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>


