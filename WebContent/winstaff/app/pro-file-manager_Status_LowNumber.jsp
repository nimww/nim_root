 
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.bltPracticeMaster ,com.winstaff.bltAdminPracticeLU,com.winstaff.bltAdminPracticeLU_List_LU_AdminID,com.winstaff.bltPhysicianMaster ,com.winstaff.bltAdminPhysicianLU,com.winstaff.bltAdminPhysicianLU_List_LU_AdminID,com.winstaff.PLCUtils,java.util.Vector,java.util.Enumeration,com.winstaff.ListElement,com.winstaff.bltAdminMaster" %>
<%/*
    filename: out\jsp\tAdminMaster_form.jsp
    JSP AutoGen on Mar/02/2002
*/%>
<%@ include file="../generic/CheckLogin.jsp" %>
<%
//String thePLCID = (String)pageControllerHash.get("plcID");
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_AdminID.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>
<%
//initial declaration of list class and parentID
    Integer        iAdminID        =    null;
    boolean accessValid = false;
    // required for Type2
    String sKeyMasterReference = null;
    if (pageControllerHash.containsKey("iAdminID")) 
    {
        iAdminID        =    (Integer)pageControllerHash.get("iAdminID");
        accessValid = true;    
   }
  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);      java.text.SimpleDateFormat displayDateTimeSDF = new java.text.SimpleDateFormat("MM/dd/yyyy hh:mm a");

    pageControllerHash.put("sLocalChildReturnPage","pro-file_Status.jsp");
    pageControllerHash.put("sParentReturnPage","pro-file_Status.jsp");
    session.setAttribute("pageControllerHash",pageControllerHash);
bltAdminMaster        AdminMaster        =   new  bltAdminMaster (iAdminID);

%>
<table width="700" border="0" cellspacing="0" cellpadding="0">
  <tr> 
    <td width=10>&nbsp;</td>
    <td> 
      <p class=title>PRO-FILE Manager Status</p>
<%
String helpIncludeFN = "ui_"+thePLCID+"\\help_AdminID.jsp";
%>
<jsp:include page="<%=helpIncludeFN%>" flush="true" ></jsp:include>
      <table width="100%" border="1" cellspacing="0" cellpadding="0" bordercolor="#333333">
        <tr> 
          <td> 
            <table width="100%" border="1" cellspacing="0" cellpadding="2" bordercolor="#999999">
              <tr> 
                <td class=tdHeader width="30%" valign="top"> 
                  <div align="right">Admin Group Information:&nbsp;</div>
                </td>
                <td> 
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr class=tdBase> 
                      <td width="30%" align="right" valign="top"> <b>Name</b>:&nbsp;</td>
                      <td><%=AdminMaster.getName()%></td>
                    </tr>
                    <tr class=tdBase> 
                      <td width="30%" align="right" valign="top"> <b>Address</b>:&nbsp;</td>
                      <td><%=AdminMaster.getAddress1()%>&nbsp;<%=AdminMaster.getAddress2()%><br>
                        <%=AdminMaster.getCity()%>&nbsp; 
                        <jsp:include page="../generic/tStateLILong_translate.jsp" flush="true" > 
                        <jsp:param name="CurrentSelection" value="<%=AdminMaster.getStateID()%>" />
                        </jsp:include>
                        &nbsp;<%=AdminMaster.getZIP()%></td>
                    </tr>
                    <tr class=tdBase> 
                      <td width="30%" align="right" valign="top"><b>Email</b>:&nbsp;</td>
                      <td><a href="mailto:<%=AdminMaster.getContactEmail()%>" ><%=AdminMaster.getContactEmail()%></a></td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr> 
                <td class=tdHeader width="30%" valign="top"> 
                  <div align="right">User Information:&nbsp;</div>
                </td>
                <td> 
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr class=tdBase> 
                      <td width="30%" align="right" valign="top"><b>User Logon 
                        Name</b>:&nbsp;</td>
                      <td><%=CurrentUserAccount.getLogonUserName()%></td>
                    </tr>
                    <tr class=tdBase> 
                      <td width="30%" align="right" valign="top"><b>User Access</b>:&nbsp;</td>
                      <td> 
                        <jsp:include page="../generic/tUserAccessTypeLILong_translate.jsp" flush="true" > 
                        <jsp:param name="CurrentSelection" value="<%=CurrentUserAccount.getAccessType()%>" />
                        </jsp:include>
                      </td>
                    </tr>
                    <tr class=tdBase> 
                      <td width="30%" align="right" valign="top"><b>Contact Email</b>:&nbsp;</td>
                      <td><a href="mailto:<%=CurrentUserAccount.getContactEmail()%>" ><%=CurrentUserAccount.getContactEmail()%></a></td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr> 
                <td class=tdHeader width="30%" valign="top"> 
                  <div align="right">Alert Status:&nbsp;</div>
                </td>
                <td> 
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr class=tdBase> 
                      <td width="30%" valign="top"> 
                        <%
					  if (AdminMaster.getAlertDays().intValue()>0)
					  {
					  %>
                        &nbsp;&nbsp;&nbsp;You are currently set to receive Alerts 
                        at <a href="mailto:<%=AdminMaster.getContactEmail()%>" ><%=AdminMaster.getContactEmail()%></a> 
                        <%
					  }
					  else
					  {
					  %>
                        &nbsp;&nbsp;&nbsp;You are NOT configured to receive Alert 
                        Emails 
                        <%
					  }
					  %>
                      </td>
                    </tr>
                    <tr class=tdBase> 
                      <td width="30%" valign="top">&nbsp;&nbsp;&nbsp;To change 
                        your Alert Options, <a href="Admin_options.jsp">click 
                        here</a></td>
                    </tr>
                  </table>
                </td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td width="50%">&nbsp;</td>
          <td width="50%">&nbsp;</td>
        </tr>
        <tr class=title> 
          <td align=Center width="50%">Practitioners </td>
          <td align=Center width="50%">Practices</td>
        </tr>
        <tr class=instructions> 
          <td align=Center width="50%">(click on ID number to view)</td>
          <td align=Center width="50%">(click on ID number to view)</td>
        </tr>
      </table>
      <table width="100%" border="1" cellspacing="0" cellpadding="0" bordercolor="#333333">
        <tr> 
          <td width="50%" valign=top> 
            <table width="100%" border=1 cellspacing="0" cellpadding="2" bordercolor="#999999">
              <tr class=tdHeader> 
                <td colspan=3> <a href = "tAdminPhysicianLU_main_PhysicianMaster_AdminID_form_create.jsp?EDIT=new"&EDIT=edit"><img border=0 src="ui_<%=thePLCID%>/icons/create_AdminID.gif"></a>&nbsp; 
                  <a href = "AdminPhysician_query.jsp?PhysicianID=&LastName=&FirstName=&SSN=&HomeCity=&HomeStateID=0&HomeZIP=&orderBy=PhysicianID&startID=0&maxResults=50&Submit2=Submit"&EDIT=edit"><img border=0 src="ui_<%=thePLCID%>/icons/manage_AdminID.gif"></a></td>
              </tr>
              <tr class=tdHeaderAlt> 
                <td>ID</td>
                <td>Last Name</td>
                <td>First Name</td>
              </tr>
              <%
{
    bltAdminPhysicianLU_List_LU_AdminID        bltAdminPhysicianLU_List_LU_AdminID        =    new    bltAdminPhysicianLU_List_LU_AdminID(iAdminID);

//declaration of Enumeration
    bltAdminPhysicianLU        working_bltAdminPhysicianLU;
    ListElement         leCurrentElement;
    java.util.Enumeration eList = bltAdminPhysicianLU_List_LU_AdminID.elements();
    int cnt=0;
    while (eList.hasMoreElements())
    {
        leCurrentElement    = (ListElement) eList.nextElement();
        working_bltAdminPhysicianLU  = (bltAdminPhysicianLU) leCurrentElement.getObject();
        bltPhysicianMaster working_bltPhysicianMaster  = new bltPhysicianMaster(working_bltAdminPhysicianLU.getPhysicianID());
	cnt++;
	String myClass = "tdBaseAlt";
	if (cnt%2!=0)
	{
		myClass = "tdBase";
	}
%>
              <tr class=<%=myClass%>> 
                <td><a href="tAdminPhysicianLU_main_PhysicianMaster_AdminID_form_authorize.jsp?EDIT=edit&EDITID=<%=working_bltAdminPhysicianLU.getPhysicianID()%>"><%=working_bltPhysicianMaster.getPhysicianID()%></a></td>
                <td><a href="tAdminPhysicianLU_main_PhysicianMaster_AdminID_form_authorize.jsp?EDIT=edit&EDITID=<%=working_bltAdminPhysicianLU.getPhysicianID()%>"><%=working_bltPhysicianMaster.getLastName()%></a></td>
                <td><a href="tAdminPhysicianLU_main_PhysicianMaster_AdminID_form_authorize.jsp?EDIT=edit&EDITID=<%=working_bltAdminPhysicianLU.getPhysicianID()%>"><%=working_bltPhysicianMaster.getFirstName()%></a></td>
              </tr>
              <%
	}
}
	%>
            </table>
          </td>
          <td width="50%" valign=top> 
            <table width="100%" border=1 cellspacing="0" cellpadding="2" bordercolor="#999999">
              <tr class=tdHeader> 
                <td colspan=3> <a href = "tAdminPracticeLU_main_PracticeMaster_AdminID_form_create.jsp?EDIT=new"&EDIT=edit"><img border=0 src="ui_<%=thePLCID%>/icons/create_AdminID.gif"></a> 
                  <a href = "AdminPractice_query.jsp?PracticeID=&PracticeName=&OfficeCity=&OfficeStateID=0&OfficeZip=&orderBy=PracticeID&startID=0&maxResults=50&Submit2=Submit"&EDIT=edit"><img border=0 src="ui_<%=thePLCID%>/icons/manage_AdminID.gif"></a></td>
              </tr>
              <tr class=tdHeaderAlt> 
                <td>ID</td>
                <td>Practice Name</td>
                <td>Postal Code</td>
              </tr>
              <%
{
    bltAdminPracticeLU_List_LU_AdminID        bltAdminPracticeLU_List_LU_AdminID        =    new    bltAdminPracticeLU_List_LU_AdminID(iAdminID);

//declaration of Enumeration
    bltAdminPracticeLU        working_bltAdminPracticeLU;
    ListElement         leCurrentElement;
    java.util.Enumeration eList = bltAdminPracticeLU_List_LU_AdminID.elements();
    int cnt=0;
    while (eList.hasMoreElements())
    {
        leCurrentElement    = (ListElement) eList.nextElement();
        working_bltAdminPracticeLU  = (bltAdminPracticeLU) leCurrentElement.getObject();
        bltPracticeMaster working_bltPracticeMaster  = new bltPracticeMaster(working_bltAdminPracticeLU.getPracticeID());
	cnt++;
	String myClass = "tdBaseAlt";
	if (cnt%2!=0)
	{
		myClass = "tdBase";
	}
%>
              <tr class=<%=myClass%>> 
                <td><a href="tAdminPracticeLU_main_PracticeMaster_AdminID_form_authorize.jsp?EDIT=edit&EDITID=<%=working_bltAdminPracticeLU.getPracticeID()%>"><%=working_bltPracticeMaster.getPracticeID()%></a></td>
                <td><a href="tAdminPracticeLU_main_PracticeMaster_AdminID_form_authorize.jsp?EDIT=edit&EDITID=<%=working_bltAdminPracticeLU.getPracticeID()%>"><%=working_bltPracticeMaster.getPracticeName()%></a></td>
                <td><%=working_bltPracticeMaster.getOfficeZIP()%></td>
              </tr>
              <%
	}
}
	%>
            </table>
          </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr class=instructions> 
    <td width=10>&nbsp;</td>
    <td colspan=2>To run reports, <a href="reports_AdminID.jsp">click here</a></td>
  </tr>
  <tr> 
    <td width=10>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
</table>
<%

  }
  else
  {
   response.sendRedirect("/errorhandler.jsp");
  }

%>
<%String bnIncludeFN = "ui_"+thePLCID+"\\bot-nav_AdminID.jsp";
%>
<jsp:include page="<%=bnIncludeFN%>" flush="true" > 
<jsp:param name="plcID" value="<%=thePLCID%>"/>
</jsp:include>
