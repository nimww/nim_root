<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.*" %>
<%/*
    filename: out\jsp\tNIM3_Modality_main_NIM3_Modality_PracticeID_form_authorize.jsp
    Created on Oct/28/2009
    Type: 1-n main class file
    Created by: Scott Ellis
    Version: 4.0
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>


<%
//initial declaration of list class and parentID
    Integer        iPracticeID        =    null;
    boolean accessValid = false;

   Integer iSecurityCheck = SecurityCheck.CheckItem("nim1", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {

    if (pageControllerHash.containsKey("iPracticeID")) 
    {
        iPracticeID        =    (Integer)pageControllerHash.get("iPracticeID");
        accessValid = true;
    }
  //page security
  if (accessValid)
  {
        Integer requestID = null;
        if (request.getParameter("EDITID")!=null)
        {
            requestID = new Integer(request.getParameter("EDITID"));
            out.println(requestID);
        }
    bltNIM3_Modality_List        bltNIM3_Modality_List        =    new    bltNIM3_Modality_List(iPracticeID,"ModalityID="+requestID,"");

//declaration of Enumeration
    bltNIM3_Modality        working_bltNIM3_Modality;
    ListElement         leCurrentElement;
    java.util.Enumeration eList = bltNIM3_Modality_List.elements();
    %>
    <%
    if (eList.hasMoreElements())
    {
        leCurrentElement    = (ListElement) eList.nextElement();
        working_bltNIM3_Modality  = (bltNIM3_Modality) leCurrentElement.getObject();
        pageControllerHash.put("iModalityID",working_bltNIM3_Modality.getModalityID());
        pageControllerHash.put("sKeyMasterReference",request.getParameter("KM"));
        session.setAttribute("pageControllerHash",pageControllerHash);
        //Parameter Pass Code here
String parameterPassString ="";
java.util.Enumeration myParameterPassList = request.getParameterNames();
while (myParameterPassList.hasMoreElements())
{
	String myName = (String)myParameterPassList.nextElement();
	String myS = (String) request.getParameter(myName);
	parameterPassString+="&"+myName + "=" + myS;
}
        String targetRedirect = "tNIM3_Modality_form.jsp?nullParam=null"+parameterPassString    ;
        if (request.getParameter("EDIT").equalsIgnoreCase("del"))
        {
            targetRedirect = "tNIM3_Modality_form_delete.jsp?routePageReference=sParentReturnPage"+parameterPassString    ;
        }
        else if (request.getParameter("EDIT").equalsIgnoreCase("openflow"))
        {
            targetRedirect = DataControlUtils.getOpenFlow("tNIM3_Modality_main_NIM3_Modality_PracticeID_form_authorize")   ;
        }
        response.sendRedirect(targetRedirect);
    }
    else
    {
   out.println("invalid where query");
    }

  }
  else
  {
   out.println("illegal");
  }
}
else
{
out.println("Your Security Level does not permit you to View this.");
}
%>




