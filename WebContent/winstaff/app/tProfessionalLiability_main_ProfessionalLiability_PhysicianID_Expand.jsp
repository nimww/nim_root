<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.bltDocumentManagement,java.util.Vector,java.util.Enumeration,com.winstaff.ListElement, com.winstaff.PLCUtils, com.winstaff.SecurityCheck, com.winstaff.ConfigurationMessages,com.winstaff.bltProfessionalLiability,com.winstaff.bltProfessionalLiability_List" %>
<%/*
    filename: tProfessionalLiability_main_ProfessionalLiability_PhysicianID.jsp
    Created on Mar/21/2003
    Type: 1-n main class file
    Created by: Scott Ellis
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>

<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_PhysicianID.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>

    <table cellpadding=0 cellspacing=0 border=0 width=<%=MasterTableWidth%> >
    <tr><td width=10>&nbsp;</td><td>
    <%=ConfigurationMessages.getHTML("INTERVIEWTopControl","tProfessionalLiability")%>

    <br><a href="tProfessionalLiability_main_ProfessionalLiability_PhysicianID.jsp">Compact</a>

<%
//initial declaration of list class and parentID
    Integer        iPhysicianID        =    null;
    boolean accessValid = false;
   Integer iSecurityCheck = SecurityCheck.CheckItem("PractitionerSection9", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {
    if (pageControllerHash.containsKey("iPhysicianID")) 
    {
        iPhysicianID        =    (Integer)pageControllerHash.get("iPhysicianID");
        accessValid = true;
    }
  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);
      pageControllerHash.put("sParentReturnPage","tProfessionalLiability_main_ProfessionalLiability_PhysicianID_Expand.jsp");
    pageControllerHash.remove("iProfessionalLiabilityID");
    session.setAttribute("pageControllerHash",pageControllerHash);

    bltProfessionalLiability_List        bltProfessionalLiability_List        =    new    bltProfessionalLiability_List(iPhysicianID);

//declaration of Enumeration
    bltProfessionalLiability        working_bltProfessionalLiability;
    ListElement         leCurrentElement;
    Enumeration eList = bltProfessionalLiability_List.elements();
    %>
        <%@ include file="tProfessionalLiability_main_ProfessionalLiability_PhysicianID_instructions.jsp" %>

        <%if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {%>
        <a class=linkBase href = "tProfessionalLiability_main_ProfessionalLiability_PhysicianID_form_create.jsp?EDIT=new&KM=p"><img border=0 src="ui_<%=thePLCID%>/icons/create_PhysicianID.gif"></a>
        <%}%>
    <%
    while (eList.hasMoreElements())
    {
         %>
         <table border="0" bordercolor="333333" cellpadding="3" class=tdHeaderAlt cellspacing="0" width="100%">
            <tr>
              <td width="50%">
                   <table border="0" bordercolor="333333" cellpadding="3" class=tdHeaderAlt cellspacing="0" width="100%">
                     <tr> 
                       <td>
         <%

        leCurrentElement    = (ListElement) eList.nextElement();
        working_bltProfessionalLiability  = (bltProfessionalLiability) leCurrentElement.getObject();
        working_bltProfessionalLiability.GroupSecurityInit(UserSecurityGroupID);
        String theClass = "tdHeader";
        if (!working_bltProfessionalLiability.isComplete())
        {
            theClass = "incompleteItem";
        %>
                <span class=incompleteItem><b>Not Complete</b></span><br>
        <%
        }
        %>
               <span class=<%=theClass%> ><b>ID:&nbsp;</b><%=working_bltProfessionalLiability.getProfessionalLiabilityID()%> </span>
                  </td></tr>
                  <tr><td><b>Item Create Date:&nbsp;</b><jsp:include page="../generic/DateTypeConvert_Main.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(working_bltProfessionalLiability.getUniqueCreateDate())%>" /></jsp:include></td></tr>
                  <tr><td><b>Item Modify Date:&nbsp;</b><jsp:include page="../generic/DateTypeConvert_Main.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(working_bltProfessionalLiability.getUniqueModifyDate())%>" /></jsp:include></td></tr>
                  <tr><td><b>Item Modify Comments:&nbsp;</b><%=working_bltProfessionalLiability.getUniqueModifyComments()%></td></tr>
                  </td></tr></table>
            </td><td width="50%" bgColor=#ffffff> 
        <a class=linkBase href = "tProfessionalLiability_main_ProfessionalLiability_PhysicianID_form_authorize.jsp?EDIT=edit&EDITID=<%=working_bltProfessionalLiability.getProfessionalLiabilityID()%>&KM=p"><img border=0 src="ui_<%=thePLCID%>/icons/edit_PhysicianID.gif"></a>


        <%if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {%>
        <br><a class=linkBase  onClick="return confirmDelete()"  href = "tProfessionalLiability_main_ProfessionalLiability_PhysicianID_form_authorize.jsp?EDIT=del&EDITID=<%=working_bltProfessionalLiability.getProfessionalLiabilityID()%>&KM=p"><img border=0 src="ui_<%=thePLCID%>/icons/delete_PhysicianID.gif"></a>
        <br>
        <% 
            if (working_bltProfessionalLiability.getDocuLinkID().intValue()>0)
            {
              bltDocumentManagement myDoc = new bltDocumentManagement(working_bltProfessionalLiability.getDocuLinkID());
              if (!myDoc.getDocumentFileName().equalsIgnoreCase(""))
              {
              %>
                      <a class=linkBase target=_blank href = "pdf/<%=myDoc.getDocumentFileName()%>">View Document</a>

              <%
              }
              else
              {
              %>

                      <a class=linkBase target=_blank href = "#" onClick="window.open('tDocumentManagement_main_DocumentManagement_PhysicianID_form_authorize.jsp?EDIT=faxCover&amp;EDITID=<%=working_bltProfessionalLiability.getDocuLinkID()%>','DocPDF','toolbar=yes,location=yes,status=yes,menubar=yes,scrollbars=yes,resizable=yes,width=500,height=500');return false;" >Print Cover Sheet</a>

              <%
              }
            }
            else
            {
             if (working_bltProfessionalLiability.isComplete())
             {
               %>
                      <a target=_blank class=linkBase href = "tProfessionalLiability_ModifyDocument.jsp?EDITID=<%=working_bltProfessionalLiability.getProfessionalLiabilityID()%>&EDIT=CREATE&dType=tProfessionalLiability">Create Document</a>
               <%
             }
             else
             {
               %>
                      <span class=tdBase>You must complete this item before you can attach a document</span>
               <%
             }
            }
            %>

        <%}%>
                  </td></tr>
                 </table>
                 <table width="100%" border="1" cellspacing="0" bordercolor="#333333">
                  <tr>
                   <td>
                     <table width="100%" border="0" cellspacing="0">
                     <tr><td>
<%String theClassF = "textBase";%>

<%theClassF = "textBase";%>
<%if ((working_bltProfessionalLiability.isExpired("CoverageType",expiredDays))&&(working_bltProfessionalLiability.isExpiredCheck("CoverageType"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltProfessionalLiability.isRequired("CoverageType"))&&(!working_bltProfessionalLiability.isComplete("CoverageType")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Coverage Type:&nbsp;</b><jsp:include page="../generic/tCoverageTypeLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltProfessionalLiability.getCoverageType()%>" /></jsp:include></p>

<%theClassF = "textBase";%>
<%if ((working_bltProfessionalLiability.isExpired("InsuranceCarrier",expiredDays))&&(working_bltProfessionalLiability.isExpiredCheck("InsuranceCarrier"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltProfessionalLiability.isRequired("InsuranceCarrier"))&&(!working_bltProfessionalLiability.isComplete("InsuranceCarrier")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Insurance Carrier:&nbsp;</b><%=working_bltProfessionalLiability.getInsuranceCarrier()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltProfessionalLiability.isExpired("PolicyHolder",expiredDays))&&(working_bltProfessionalLiability.isExpiredCheck("PolicyHolder"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltProfessionalLiability.isRequired("PolicyHolder"))&&(!working_bltProfessionalLiability.isComplete("PolicyHolder")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Policy Holder:&nbsp;</b><%=working_bltProfessionalLiability.getPolicyHolder()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltProfessionalLiability.isExpired("AgentName",expiredDays))&&(working_bltProfessionalLiability.isExpiredCheck("AgentName"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltProfessionalLiability.isRequired("AgentName"))&&(!working_bltProfessionalLiability.isComplete("AgentName")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Agent Name:&nbsp;</b><%=working_bltProfessionalLiability.getAgentName()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltProfessionalLiability.isExpired("Address1",expiredDays))&&(working_bltProfessionalLiability.isExpiredCheck("Address1"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltProfessionalLiability.isRequired("Address1"))&&(!working_bltProfessionalLiability.isComplete("Address1")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Address:&nbsp;</b><%=working_bltProfessionalLiability.getAddress1()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltProfessionalLiability.isExpired("Address2",expiredDays))&&(working_bltProfessionalLiability.isExpiredCheck("Address2"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltProfessionalLiability.isRequired("Address2"))&&(!working_bltProfessionalLiability.isComplete("Address2")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Address 2:&nbsp;</b><%=working_bltProfessionalLiability.getAddress2()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltProfessionalLiability.isExpired("City",expiredDays))&&(working_bltProfessionalLiability.isExpiredCheck("City"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltProfessionalLiability.isRequired("City"))&&(!working_bltProfessionalLiability.isComplete("City")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>City:&nbsp;</b><%=working_bltProfessionalLiability.getCity()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltProfessionalLiability.isExpired("StateID",expiredDays))&&(working_bltProfessionalLiability.isExpiredCheck("StateID"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltProfessionalLiability.isRequired("StateID"))&&(!working_bltProfessionalLiability.isComplete("StateID")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>State:&nbsp;</b><jsp:include page="../generic/tStateLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltProfessionalLiability.getStateID()%>" /></jsp:include></p>

<%theClassF = "textBase";%>
<%if ((working_bltProfessionalLiability.isExpired("Province",expiredDays))&&(working_bltProfessionalLiability.isExpiredCheck("Province"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltProfessionalLiability.isRequired("Province"))&&(!working_bltProfessionalLiability.isComplete("Province")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Province, District, State:&nbsp;</b><%=working_bltProfessionalLiability.getProvince()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltProfessionalLiability.isExpired("ZIP",expiredDays))&&(working_bltProfessionalLiability.isExpiredCheck("ZIP"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltProfessionalLiability.isRequired("ZIP"))&&(!working_bltProfessionalLiability.isComplete("ZIP")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>ZIP:&nbsp;</b><%=working_bltProfessionalLiability.getZIP()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltProfessionalLiability.isExpired("CountryID",expiredDays))&&(working_bltProfessionalLiability.isExpiredCheck("CountryID"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltProfessionalLiability.isRequired("CountryID"))&&(!working_bltProfessionalLiability.isComplete("CountryID")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Country:&nbsp;</b><jsp:include page="../generic/tCountryLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltProfessionalLiability.getCountryID()%>" /></jsp:include></p>

<%theClassF = "textBase";%>
<%if ((working_bltProfessionalLiability.isExpired("Phone",expiredDays))&&(working_bltProfessionalLiability.isExpiredCheck("Phone"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltProfessionalLiability.isRequired("Phone"))&&(!working_bltProfessionalLiability.isComplete("Phone")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Phone (XXX-XXX-XXXX):&nbsp;</b><%=working_bltProfessionalLiability.getPhone()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltProfessionalLiability.isExpired("Fax",expiredDays))&&(working_bltProfessionalLiability.isExpiredCheck("Fax"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltProfessionalLiability.isRequired("Fax"))&&(!working_bltProfessionalLiability.isComplete("Fax")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Fax (XXX-XXX-XXXX):&nbsp;</b><%=working_bltProfessionalLiability.getFax()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltProfessionalLiability.isExpired("ContactName",expiredDays))&&(working_bltProfessionalLiability.isExpiredCheck("ContactName"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltProfessionalLiability.isRequired("ContactName"))&&(!working_bltProfessionalLiability.isComplete("ContactName")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Contact Name:&nbsp;</b><%=working_bltProfessionalLiability.getContactName()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltProfessionalLiability.isExpired("ContactEmail",expiredDays))&&(working_bltProfessionalLiability.isExpiredCheck("ContactEmail"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltProfessionalLiability.isRequired("ContactEmail"))&&(!working_bltProfessionalLiability.isComplete("ContactEmail")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Contact E-mail:&nbsp;</b><%=working_bltProfessionalLiability.getContactEmail()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltProfessionalLiability.isExpired("PolicyNumber",expiredDays))&&(working_bltProfessionalLiability.isExpiredCheck("PolicyNumber"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltProfessionalLiability.isRequired("PolicyNumber"))&&(!working_bltProfessionalLiability.isComplete("PolicyNumber")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Policy Number:&nbsp;</b><%=working_bltProfessionalLiability.getPolicyNumber()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltProfessionalLiability.isExpired("InsuredFromDate",expiredDays))&&(working_bltProfessionalLiability.isExpiredCheck("InsuredFromDate"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltProfessionalLiability.isRequired("InsuredFromDate"))&&(!working_bltProfessionalLiability.isComplete("InsuredFromDate")) ){theClassF = "requiredFieldMain";}%>

                        <p class=<%=theClassF%> ><b>Current Insured From Date:&nbsp;</b><jsp:include page="../generic/DateTypeConvert_Main.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(working_bltProfessionalLiability.getInsuredFromDate())%>" /></jsp:include></p>


<%theClassF = "textBase";%>
<%if ((working_bltProfessionalLiability.isExpired("InsuredToDate",expiredDays))&&(working_bltProfessionalLiability.isExpiredCheck("InsuredToDate"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltProfessionalLiability.isRequired("InsuredToDate"))&&(!working_bltProfessionalLiability.isComplete("InsuredToDate")) ){theClassF = "requiredFieldMain";}%>

                        <p class=<%=theClassF%> ><b>Current Insured To Date:&nbsp;</b><jsp:include page="../generic/DateTypeConvert_Main.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(working_bltProfessionalLiability.getInsuredToDate())%>" /></jsp:include></p>


<%theClassF = "textBase";%>
<%if ((working_bltProfessionalLiability.isExpired("OriginalEffectiveDate",expiredDays))&&(working_bltProfessionalLiability.isExpiredCheck("OriginalEffectiveDate"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltProfessionalLiability.isRequired("OriginalEffectiveDate"))&&(!working_bltProfessionalLiability.isComplete("OriginalEffectiveDate")) ){theClassF = "requiredFieldMain";}%>

                        <p class=<%=theClassF%> ><b>Original Effective Date:&nbsp;</b><jsp:include page="../generic/DateTypeConvert_Main.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(working_bltProfessionalLiability.getOriginalEffectiveDate())%>" /></jsp:include></p>


<%theClassF = "textBase";%>
<%if ((working_bltProfessionalLiability.isExpired("TerminationDate",expiredDays))&&(working_bltProfessionalLiability.isExpiredCheck("TerminationDate"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltProfessionalLiability.isRequired("TerminationDate"))&&(!working_bltProfessionalLiability.isComplete("TerminationDate")) ){theClassF = "requiredFieldMain";}%>

                        <p class=<%=theClassF%> ><b>Policy Termination Date:&nbsp;</b><jsp:include page="../generic/DateTypeConvert_Main.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(working_bltProfessionalLiability.getTerminationDate())%>" /></jsp:include></p>


<%theClassF = "textBase";%>
<%if ((working_bltProfessionalLiability.isExpired("PerClaimAmount",expiredDays))&&(working_bltProfessionalLiability.isExpiredCheck("PerClaimAmount"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltProfessionalLiability.isRequired("PerClaimAmount"))&&(!working_bltProfessionalLiability.isComplete("PerClaimAmount")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Per Claim Amount (###,###.##):&nbsp;</b><%=PLCUtils.getDisplayDefaultDecimalFormat2(working_bltProfessionalLiability.getPerClaimAmount())%></p>

<%theClassF = "textBase";%>
<%if ((working_bltProfessionalLiability.isExpired("AggregateAmount",expiredDays))&&(working_bltProfessionalLiability.isExpiredCheck("AggregateAmount"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltProfessionalLiability.isRequired("AggregateAmount"))&&(!working_bltProfessionalLiability.isComplete("AggregateAmount")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Aggregate Amount (###,###.##):&nbsp;</b><%=PLCUtils.getDisplayDefaultDecimalFormat2(working_bltProfessionalLiability.getAggregateAmount())%></p>

<%theClassF = "textBase";%>
<%if ((working_bltProfessionalLiability.isExpired("DescOfSurcharge",expiredDays))&&(working_bltProfessionalLiability.isExpiredCheck("DescOfSurcharge"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltProfessionalLiability.isRequired("DescOfSurcharge"))&&(!working_bltProfessionalLiability.isComplete("DescOfSurcharge")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Description of Surcharge (If Applicable):&nbsp;</b><%=working_bltProfessionalLiability.getDescOfSurcharge()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltProfessionalLiability.isExpired("DocuLinkID",expiredDays))&&(working_bltProfessionalLiability.isExpiredCheck("DocuLinkID"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltProfessionalLiability.isRequired("DocuLinkID"))&&(!working_bltProfessionalLiability.isComplete("DocuLinkID")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>DocuLink:&nbsp;</b><%=working_bltProfessionalLiability.getDocuLinkID()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltProfessionalLiability.isExpired("Comments",expiredDays))&&(working_bltProfessionalLiability.isExpiredCheck("Comments"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltProfessionalLiability.isRequired("Comments"))&&(!working_bltProfessionalLiability.isComplete("Comments")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Extra Comments:&nbsp;</b><%=working_bltProfessionalLiability.getComments()%></p>

        </td></tr></table></table><br>        <%
    }
    %>
    </table>
    <%
  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>

    </td></tr></table>


<%String bnIncludeFN = "ui_"+thePLCID+"\\bot-nav_PhysicianID.jsp";
%>
<jsp:include page="<%=bnIncludeFN%>" flush="true" ><jsp:param name="plcID" value="<%=thePLCID%>"/></jsp:include>
