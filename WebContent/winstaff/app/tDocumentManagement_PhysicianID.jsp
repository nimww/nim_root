<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.stepVerify_PhysicianID ,com.winstaff.bltPhysicianMaster,com.winstaff.bltPhysicianUserAccountLU_List_LU_PhysicianID,com.winstaff.bltPhysicianUserAccountLU, com.winstaff.bltHCOPhysicianLU_List_LU_PhysicianID, com.winstaff.bltHCOPhysicianLU,    com.winstaff.bltDocumentManagement,java.util.Vector,java.util.Enumeration,com.winstaff.ListElement, com.winstaff.PLCUtils, com.winstaff.SecurityCheck, com.winstaff.ConfigurationMessages,com.winstaff.bltDocumentManagement,com.winstaff.bltDocumentManagement_List" %>
<%/*
    filename: tDocumentManagement_main_DocumentManagement_PhysicianID.jsp
    Created on Feb/06/2003
    Type: 1-n main class file
    Created by: Scott Ellis
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>

<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_PhysicianID.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>
<script language="JavaScript">
<!--
function MM_popupMsg(msg) { //v1.0
  alert(msg);
}
//-->
</script>
<table cellpadding=0 cellspacing=0 border=0 width=<%=MasterTableWidth%> >
  <tr> 
    <td width=10>&nbsp;</td>
    <td> <%=ConfigurationMessages.getHTML("INTERVIEWTopControl","tDocumentManagement")%> 
      <%
//initial declaration of list class and parentID
    Integer        iPhysicianID        =    null;
    boolean accessValid = false;
   Integer iSecurityCheck = SecurityCheck.CheckItem("DocumentManagement1", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {
    if (pageControllerHash.containsKey("iPhysicianID")) 
    {
        iPhysicianID        =    (Integer)pageControllerHash.get("iPhysicianID");
        accessValid = true;
    }
  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);
      pageControllerHash.put("sParentReturnPage","tDocumentManagement_main_DocumentManagement_PhysicianID.jsp");
    pageControllerHash.remove("iDocumentID");
    pageControllerHash.put("sINTNext","tDocumentManagement_main_DocumentManagement_PhysicianID_form_create.jsp?EDIT=new&KM=p&INTNext=yes");
    session.setAttribute("pageControllerHash",pageControllerHash);


stepVerify_PhysicianID mySV = new stepVerify_PhysicianID();
mySV.setPhysicianID(iPhysicianID);
mySV.setSecurityID(UserSecurityGroupID);
bltPhysicianMaster        PhysicianMaster        =   new  bltPhysicianMaster (iPhysicianID);

//verify data
boolean ApplicationDataCore = false;
if (mySV.isAllCore())
{
	ApplicationDataCore = true;
}
boolean ApplicationDataExtra = false;
if (mySV.isAllExtra())
{
	ApplicationDataExtra = true;
}
boolean ApplicationDataAll = (ApplicationDataExtra&&ApplicationDataCore);
//verify documents are complete:


boolean SupportingDocuments = true;
{
    bltDocumentManagement_List        bltDocumentManagement_List        =    new    bltDocumentManagement_List(iPhysicianID,"Archived=2","");
	//declaration of Enumeration
    bltDocumentManagement        working_bltDocumentManagement;
    ListElement         leCurrentElement;
    java.util.Enumeration eList = bltDocumentManagement_List.elements();
    while (eList.hasMoreElements()&&SupportingDocuments)
    {
        leCurrentElement    = (ListElement) eList.nextElement();
        working_bltDocumentManagement  = (bltDocumentManagement) leCurrentElement.getObject();
		if (!working_bltDocumentManagement.isComplete())
		{
			SupportingDocuments = false;
		}
	}
}
	//check Attestation Account Status
	boolean AttestationAccount = false;
	boolean AttestationAccountCreated = false;
{
    bltPhysicianUserAccountLU_List_LU_PhysicianID        bltPhysicianUserAccountLU_List_LU_PhysicianID        =    new    bltPhysicianUserAccountLU_List_LU_PhysicianID(iPhysicianID);
	//declaration of Enumeration
    bltPhysicianUserAccountLU        working_bltPhysicianUserAccountLU;
    ListElement         leCurrentElement;
    java.util.Enumeration eList = bltPhysicianUserAccountLU_List_LU_PhysicianID.elements();
    if (eList.hasMoreElements())
    {        
 	     AttestationAccountCreated = true;
         leCurrentElement    = (ListElement) eList.nextElement();
         working_bltPhysicianUserAccountLU  = (bltPhysicianUserAccountLU) leCurrentElement.getObject();
         bltUserAccount working_bltUserAccount  = new bltUserAccount(working_bltPhysicianUserAccountLU.getUserID());
		 if (working_bltUserAccount.getStatus().intValue()==2)
		 {
	 	     AttestationAccount = true;
		 }
    }
}

//check attest status
boolean Attestation = false;
if (PhysicianMaster.getIsAttested().intValue()==1)
{
	Attestation = true;
}


//check HCO Auth Status
boolean HCOAuth = false;
{
    bltHCOPhysicianLU_List_LU_PhysicianID        bltHCOPhysicianLU_List_LU_PhysicianID        =    new    bltHCOPhysicianLU_List_LU_PhysicianID(iPhysicianID);

//declaration of Enumeration
    bltHCOPhysicianLU        working_bltHCOPhysicianLU;
    ListElement         leCurrentElement;
    java.util.Enumeration eList = bltHCOPhysicianLU_List_LU_PhysicianID.elements();
    if (eList.hasMoreElements())
    {
         HCOAuth = true;
    }

}








    bltDocumentManagement_List        bltDocumentManagement_List        =    new    bltDocumentManagement_List(iPhysicianID);

//declaration of Enumeration
    bltDocumentManagement        working_bltDocumentManagement;
    ListElement         leCurrentElement;
    java.util.Enumeration eList = bltDocumentManagement_List.elements();
    int docTotal = 0;
	java.util.Vector vDocumentIDs = new java.util.Vector();
    if (eList.hasMoreElements())
    {
     while (eList.hasMoreElements())
     {
        leCurrentElement    = (ListElement) eList.nextElement();
        working_bltDocumentManagement  = (bltDocumentManagement) leCurrentElement.getObject();
        if (working_bltDocumentManagement.getDocumentFileName().equalsIgnoreCase("")||working_bltDocumentManagement.getDocumentFileName()==null)
        {
	    vDocumentIDs.addElement(working_bltDocumentManagement.getUniqueID());
            docTotal++;
        }
     }
	}
	else
	{
	}
    pageControllerHash.put("vDocumentIDs",vDocumentIDs);
    
    if (docTotal>0&&ApplicationDataAll)
    {
           %>
      <script language=javascript>
           if (confirm("<%=ConfigurationMessages.getInterviewMessage("INTERVIEWNoUnprocDocs","")%>"))
           {
               window.open('tDocumentManagement_PhysicianID-Process-Forward.jsp?myforward=../formPop/formPop_DocFaxCover_Pro.jsp','DocPopPro','scrollbars=yes,resizable=yes,width=350,height=300');
           }
           </script>
      <%
    }



    %>
      <table  class=tdBase width="100%%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td colspan=3> 
            <table border="0" cellpadding="0" cellspacing="0" width="650">
              <!-- fwtable fwsrc="dataprocess1.png" fwbase="dataprocess-nav.gif" fwstyle="Dreamweaver" fwdocid = "742308039" fwnested="0" -->
              <tr> 
                <td><img src="images/dataprocess/spacer.gif" width="6" height="1" border="0" name="undefined_3"></td>
                <td><img src="images/dataprocess/spacer.gif" width="70" height="1" border="0" name="undefined_3"></td>
                <td><img src="images/dataprocess/spacer.gif" width="8" height="1" border="0" name="undefined_3"></td>
                <td><img src="images/dataprocess/spacer.gif" width="1" height="1" border="0" name="undefined_3"></td>
                <td><img src="images/dataprocess/spacer.gif" width="139" height="1" border="0" name="undefined_3"></td>
                <td><img src="images/dataprocess/spacer.gif" width="15" height="1" border="0" name="undefined_3"></td>
                <td><img src="images/dataprocess/spacer.gif" width="77" height="1" border="0" name="undefined_3"></td>
                <td><img src="images/dataprocess/spacer.gif" width="6" height="1" border="0" name="undefined_3"></td>
                <td><img src="images/dataprocess/spacer.gif" width="121" height="1" border="0" name="undefined_3"></td>
                <td><img src="images/dataprocess/spacer.gif" width="2" height="1" border="0" name="undefined_3"></td>
                <td><img src="images/dataprocess/spacer.gif" width="121" height="1" border="0" name="undefined_3"></td>
                <td><img src="images/dataprocess/spacer.gif" width="6" height="1" border="0" name="undefined_3"></td>
                <td><img src="images/dataprocess/spacer.gif" width="62" height="1" border="0" name="undefined_3"></td>
                <td><img src="images/dataprocess/spacer.gif" width="8" height="1" border="0" name="undefined_3"></td>
                <td><img src="images/dataprocess/spacer.gif" width="8" height="1" border="0" name="undefined_3"></td>
                <td><img src="images/dataprocess/spacer.gif" width="1" height="1" border="0" name="undefined_3"></td>
              </tr>
              <tr> 
                <td colspan="15"><img name="dataprocessnav_r1_c1_2" src="images/dataprocess/dataprocess-nav_r1_c1.gif" width="650" height="13" border="0"></td>
                <td><img src="images/dataprocess/spacer.gif" width="1" height="13" border="0" name="undefined_3"></td>
              </tr>
              <tr> 
                <td rowspan="2" colspan="3"><img name="dataprocessnav_r2_c1_2" src="images/dataprocess/dataprocess-nav_r2_c1.gif" width="84" height="49" border="0"></td>
                <%
if (ApplicationDataCore)
{
%>
                <td colspan="2"><a href="pro-file_Status_Express.jsp"><img name="dataprocessnav_r2_c4_2" src="images/dataprocess/dataprocess-nav_r2_c4_f3.gif" width="140" height="43" border="0"></a></td>
                <%
}
else
{
%>
                <td colspan="2"><a href="pro-file_Status_Express.jsp"><img name="dataprocessnav_r2_c4_2" src="images/dataprocess/dataprocess-nav_r2_c4_f4.gif" width="140" height="43" border="0"></a></td>
                <%
}
%>
                <td rowspan="6"><img name="dataprocessnav_r2_c6_2" src="images/dataprocess/dataprocess-nav_r2_c6.gif" width="15" height="187" border="0"></td>
                <td rowspan="2" colspan="8"><a href="pro-file_Status_Express.jsp"><img name="dataprocessnav_r2_c7" src="images/dataprocess/dataprocess-nav_r2_c7_f2.gif" width="403" height="49" border="0"></a></td>
                <td rowspan="6"><img name="dataprocessnav_r2_c15" src="images/dataprocess/dataprocess-nav_r2_c15.gif" width="8" height="187" border="0"></td>
                <td><img src="images/dataprocess/spacer.gif" width="1" height="43" border="0" name="undefined_3"></td>
              </tr>
              <tr> 
                <td rowspan="3" colspan="2"><img name="dataprocessnav_r3_c4_2" src="images/dataprocess/dataprocess-nav_r3_c4.gif" width="140" height="83" border="0"></td>
                <td><img src="images/dataprocess/spacer.gif" width="1" height="6" border="0" name="undefined_3"></td>
              </tr>
              <tr> 
                <td rowspan="4"><img name="dataprocessnav_r4_c1_2" src="images/dataprocess/dataprocess-nav_r4_c1.gif" width="6" height="138" border="0"></td>
                <td><a href="pro-file_Status_Express.jsp"><img name="dataprocessnav_r4_c2_2" src="images/dataprocess/dataprocess-nav_r4_c2_f4.gif" width="70" height="72" border="0"></a></td>
                <td rowspan="4"><img name="dataprocessnav_r4_c3_2" src="images/dataprocess/dataprocess-nav_r4_c3.gif" width="8" height="138" border="0"></td>
                <%
if (ApplicationDataExtra)
{
%>
                <td><a href="ExtraInformation_PhysicianID.jsp"><img name="dataprocessnav_r4_c7_2" src="images/dataprocess/dataprocess-nav_r4_c7_f3.gif" width="77" height="72" border="0"></a></td>
                <%
}
else
{
%>
                <td><a href="ExtraInformation_PhysicianID.jsp"><img name="dataprocessnav_r4_c7_2" src="images/dataprocess/dataprocess-nav_r4_c7_f4.gif" width="77" height="72" border="0"></a></td>
                <%
}
%>
                <td><img name="dataprocessnav_r4_c8_2" src="images/dataprocess/dataprocess-nav_r4_c8.gif" width="6" height="72" border="0"></td>
                <%
if (PhysicianMaster.getIsAttested().intValue()==1)
{
%>
                <td><a href="tPhysicianUserAccountLU_main2_UserAccount_PhysicianID.jsp"><img name="dataprocessnav_r4_c9_2" src="images/dataprocess/dataprocess-nav_r4_c9_f3.gif" width="121" height="72" border="0"></a></td>
                <%
}
else
{
%>
                <td><a href="tPhysicianUserAccountLU_main2_UserAccount_PhysicianID.jsp"><img name="dataprocessnav_r4_c9_2" src="images/dataprocess/dataprocess-nav_r4_c9_f4.gif" width="121" height="72" border="0"></a></td>
                <%
}
%>
                <td><img name="dataprocessnav_r4_c10_2" src="images/dataprocess/dataprocess-nav_r4_c10.gif" width="2" height="72" border="0"></td>
                <%
if (SupportingDocuments)
{
%>
                <td><a href="tDocumentManagement_PhysicianID.jsp"><img name="dataprocessnav_r4_c11_2" src="images/dataprocess/dataprocess-nav_r4_c11_f2.gif" width="121" height="72" border="0"></a></td>
                <%
}
else
{
%>
                <td><a href="tDocumentManagement_PhysicianID.jsp"><img name="dataprocessnav_r4_c11_2" src="images/dataprocess/dataprocess-nav_r4_c11_f2.gif" width="121" height="72" border="0"></a></td>
                <%
}
%>
                <td><img name="dataprocessnav_r4_c12_2" src="images/dataprocess/dataprocess-nav_r4_c12.gif" width="6" height="72" border="0"></td>
                <td colspan="2"><a href="FinishedCheckList_PhysicianID.jsp"><img name="dataprocessnav_r4_c13_2" src="images/dataprocess/dataprocess-nav_r4_c13.gif" width="70" height="72" border="0"></a></td>
                <td><img src="images/dataprocess/spacer.gif" width="1" height="72" border="0" name="undefined_3"></td>
              </tr>
              <tr> 
                <td rowspan="3"><img name="dataprocessnav_r5_c2_2" src="images/dataprocess/dataprocess-nav_r5_c2.gif" width="70" height="66" border="0"></td>
                <td rowspan="2" colspan="7"><img name="dataprocessnav_r5_c7_2" src="images/dataprocess/dataprocess-nav_r5_c7.gif" width="395" height="48" border="0"></td>
                <td rowspan="3"><img name="dataprocessnav_r5_c14" src="images/dataprocess/dataprocess-nav_r5_c14.gif" width="8" height="66" border="0"></td>
                <td><img src="images/dataprocess/spacer.gif" width="1" height="5" border="0" name="undefined_3"></td>
              </tr>
              <tr> 
                <td rowspan="2"><img name="dataprocessnav_r6_c4_2" src="images/dataprocess/dataprocess-nav_r6_c4.gif" width="1" height="61" border="0"></td>
                <%
if (ApplicationDataCore)
{
%>
                <td><a href="pro-file_Status_Advanced.jsp"><img name="dataprocessnav_r6_c5_2" src="images/dataprocess/dataprocess-nav_r6_c5_f3.gif" width="139" height="43" border="0"></a></td>
                <%
}
else
{
%>
                <td><a href="pro-file_Status_Advanced.jsp"><img name="dataprocessnav_r6_c5_2" src="images/dataprocess/dataprocess-nav_r6_c5_f4.gif" width="139" height="43" border="0"></a></td>
                <%
}
%>
                <td><img src="images/dataprocess/spacer.gif" width="1" height="43" border="0" name="undefined_3"></td>
              </tr>
              <tr> 
                <td><img name="dataprocessnav_r7_c5_2" src="images/dataprocess/dataprocess-nav_r7_c5.gif" width="139" height="18" border="0"></td>
                <td colspan="7"><img name="dataprocessnav_r7_c7" src="images/dataprocess/dataprocess-nav_r7_c7.gif" width="395" height="18" border="0"></td>
                <td><img src="images/dataprocess/spacer.gif" width="1" height="18" border="0" name="undefined_3"></td>
              </tr>
            </table>
          </td>
        </tr>
        <tr> 
          <td>&nbsp;</td>
        </tr>
        <%



if (docTotal>0&&!ApplicationDataAll)
{
%>
  <tr> 
    <td class=tdHeaderAlt>
      <table border=0 cellpadding=5 width="100%">
        <tr class=title>
          <td>Please Complete Your Application before proceeding!</td>
		  </tr>
		  <tr class=instructions>
          <td class=Instructions> 
            <ul>
              <li><b> If you have already gone through the data entry process 
                and just need to see what data is still incomplete, please <a href="FinishedCheckList_PhysicianID.jsp">click 
                here</a> to review your application.<br>
                <br>
                </b>
                    <li><b> Or, please begin the data entry process by clicking 
                      on either <a href="pro-file_Status_Express.jsp">Express 
                      Mode</a> or <a href="pro-file_Status_Advanced.jsp">Advanced 
                      Mode</a>. </b> 
                  </ul>
    </td>        </tr>
      </table>
    </td>
  </tr>

        <tr> 
          <td colspan="3"> 
            <table width="100%" cellpadding="5" border="0" cellspacing="0">
              <tr class=tdHeaderAlt> 
                <td> You have a total of <%=docTotal%> unprocessed 
                  documents. </td>
              </tr>
              <tr class=tdHeaderAlt> 
                <td class=instructions> Once you have entered all of your Credentialing infomration, these documents need to be faxed in so 
                  that we can attach them to your account. Each document 
                  must have its own unique coversheet. </td>
              </tr>
            </table>
          </td>
        </tr>
        <%
}
else if (docTotal>0)
{
%>
        <tr> 
          <td colspan="3"> 
            <table width="100%" cellpadding="5" border="0" cellspacing="0">
              <tr class=tdHeaderAlt> 
                <td class=title> You have a total of <%=docTotal%> unprocessed 
                  documents. </td>
              </tr>
              <tr class=tdHeaderAlt> 
                <td class=instructions> These documents need to be faxed in so 
                  that we can attach them to your PRO-File Information. Each document 
                  must have its own unique coversheet. </td>
              </tr>
            </table>
          </td>
        </tr>
        <%
}
else
{
%>
        <tr> 
          <td colspan="3"> 
            <table width="100%" cellpadding="5" border="0" cellspacing="0">
              <tr class=tdHeaderAlt> 
                <td class=title> All your documents have been received and processed.</td>
              </tr>
              <tr class=tdHeaderAlt> 
                <td class=instructions> If you change any of your Credentialing 
                  data please check here again to see if there are any new documents 
                  you need to send in </td>
              </tr>
            </table>
          </td>
        </tr>
        <%
}
%>
        <tr> 
          <td>&nbsp;</td>
        </tr>
        <tr> 
          <td> 
            <table width="100%" cellpadding="3" cellspacing="0" border="0" class=tdBase>
              <tr> 
                <td width="167"><a href="tDocumentManagement_PhysicianID-Process-Forward.jsp?myforward=../formPop/formPop_DocFaxCover_Pro.jsp" target="_blank"><img src="images/process.gif" width="167" height="119" border="0" ></a></td>
                <td width="10">&nbsp;</td>
                <td width=100%> 
                  <div align="left"><a href="tDocumentManagement_PhysicianID-Process-Forward.jsp?myforward=../formPop/formPop_DocFaxCover_Pro.jsp" target="_blank">Click 
                    here</a> to print all <%=docTotal%> cover sheets</div>
                </td>
              </tr>
              <tr> 
                <td>&nbsp;</td>
                <td width="10">&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
              <tr> 
                <td width="167"><a href="tDocumentManagement_query_PhysicianID.jsp?DocumentID=&DocumentTypeID=0&DocumentName=&DateOfExpiration=&Archived=2&orderBy=DocumentID&startID=0&maxResults=50&Submit2=Submit"><img src="images/search.gif" width="167" height="103" border="0"></a></td>
                <td width="10">&nbsp;</td>
                <td><a href=tDocumentManagement_query_PhysicianID.jsp?DocumentID=&DocumentTypeID=0&DocumentName=&DateOfExpiration=&Archived=2&orderBy=DocumentID&startID=0&maxResults=50&Submit2=Submit>Search</a> 
                  Documents - go here to list and view all of your attached documents. 
                </td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
      <%
  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>
    </td>
  </tr>
</table>
<br>
<%String bnIncludeFN = "ui_"+thePLCID+"\\bot-nav_PhysicianID.jsp";
%>
<jsp:include page="<%=bnIncludeFN%>" flush="true" > 
<jsp:param name="plcID" value="<%=thePLCID%>"/>
</jsp:include>
