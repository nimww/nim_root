<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.bltDocumentManagement,java.util.Vector,java.util.Enumeration,com.winstaff.ListElement, com.winstaff.PLCUtils, com.winstaff.SecurityCheck, com.winstaff.ConfigurationMessages,com.winstaff.bltLicenseRegistration,com.winstaff.bltLicenseRegistration_List,com.winstaff.DocumentManagerUtils" %>
<%/*
    filename: tLicenseRegistration_main_LicenseRegistration_PhysicianID.jsp
    Created on Nov/19/2003
    Type: 1-n main class file
    Created by: Scott Ellis
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>
    <%
    Integer iExpressMode = new Integer(1);
    if (pageControllerHash.containsKey("iExpressMode")) 
    {
        iExpressMode =    (Integer)pageControllerHash.get("iExpressMode");
    }
    %>

    <table cellpadding=0 cellspacing=0 border=0 width=800 >
<%
//initial declaration of list class and parentID
    Integer        iPhysicianID        =    null;
    boolean accessValid = false;
   Integer iSecurityCheck = SecurityCheck.CheckItem("PractitionerSection3", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {
    if (pageControllerHash.containsKey("iPhysicianID")) 
    {
        iPhysicianID        =    (Integer)pageControllerHash.get("iPhysicianID");
        accessValid = true;
    }

  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);
      pageControllerHash.put("sParentReturnPage","tLicenseRegistration_main_LicenseRegistration_PhysicianID_Expand.jsp");
    pageControllerHash.remove("iLicenseRegistrationID");
    session.setAttribute("pageControllerHash",pageControllerHash);

    bltLicenseRegistration_List        bltLicenseRegistration_List        =    new    bltLicenseRegistration_List(iPhysicianID);

//declaration of Enumeration
    bltLicenseRegistration        LicenseRegistration;
    ListElement         leCurrentElement;
    Enumeration eList = bltLicenseRegistration_List.elements();
    %>
    <%
    int iExpress=0;
boolean errorRouteMeTotal = false;
boolean isAllComplete = true;
    while (eList.hasMoreElements()||iExpress<=ConfigurationMessages.getExpressItemCount("tLicenseRegistration"))
    {
       iExpress++;

      boolean isNewRecord= false;
      if (eList.hasMoreElements())
      {
        leCurrentElement    = (ListElement) eList.nextElement();
        LicenseRegistration  = (bltLicenseRegistration) leCurrentElement.getObject();
      }
      else
      {
        LicenseRegistration  = new bltLicenseRegistration();
        LicenseRegistration.setPhysicianID(iPhysicianID);
        isNewRecord= true;
      }
        LicenseRegistration.GroupSecurityInit(UserSecurityGroupID);
        String theClass = "tdHeader";
        %>

        <%  {

String testChangeID = "0";

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("UniqueCreateDate_"+iExpress))) ;
    if ( !LicenseRegistration.getUniqueCreateDate().equals(testObj)  )
    {
         LicenseRegistration.setUniqueCreateDate( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("LicenseRegistration UniqueCreateDate not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("UniqueModifyDate_"+iExpress))) ;
    if ( !LicenseRegistration.getUniqueModifyDate().equals(testObj)  )
    {
         LicenseRegistration.setUniqueModifyDate( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("LicenseRegistration UniqueModifyDate not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("UniqueModifyComments_"+iExpress)) ;
    if ( !LicenseRegistration.getUniqueModifyComments().equals(testObj)  )
    {
         LicenseRegistration.setUniqueModifyComments( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("LicenseRegistration UniqueModifyComments not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("PhysicianID_"+iExpress)) ;
    if ( !LicenseRegistration.getPhysicianID().equals(testObj)  )
    {
         LicenseRegistration.setPhysicianID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("LicenseRegistration PhysicianID not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("IsCurrent_"+iExpress)) ;
    if ( !LicenseRegistration.getIsCurrent().equals(testObj)  )
    {
         LicenseRegistration.setIsCurrent( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("LicenseRegistration IsCurrent not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("LicenseType_"+iExpress)) ;
    if ( !LicenseRegistration.getLicenseType().equals(testObj)  )
    {
         LicenseRegistration.setLicenseType( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("LicenseRegistration LicenseType not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("SubType_"+iExpress)) ;
    if ( !LicenseRegistration.getSubType().equals(testObj)  )
    {
         LicenseRegistration.setSubType( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("LicenseRegistration SubType not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("NPDBFOL_"+iExpress)) ;
    if ( !LicenseRegistration.getNPDBFOL().equals(testObj)  )
    {
         LicenseRegistration.setNPDBFOL( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("LicenseRegistration NPDBFOL not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("StateID_"+iExpress)) ;
    if ( !LicenseRegistration.getStateID().equals(testObj)  )
    {
         LicenseRegistration.setStateID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("LicenseRegistration StateID not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("IsPractice_"+iExpress)) ;
    if ( !LicenseRegistration.getIsPractice().equals(testObj)  )
    {
         LicenseRegistration.setIsPractice( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("LicenseRegistration IsPractice not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("LicenseDocumentNumber_"+iExpress)) ;
    if ( !LicenseRegistration.getLicenseDocumentNumber().equals(testObj)  )
    {
         LicenseRegistration.setLicenseDocumentNumber( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("LicenseRegistration LicenseDocumentNumber not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("IssueDate_"+iExpress))) ;
    if ( !LicenseRegistration.getIssueDate().equals(testObj)  )
    {
         LicenseRegistration.setIssueDate( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("LicenseRegistration IssueDate not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("ExpirationDate_"+iExpress))) ;
    if ( !LicenseRegistration.getExpirationDate().equals(testObj)  )
    {
         LicenseRegistration.setExpirationDate( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("LicenseRegistration ExpirationDate not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("Name_"+iExpress)) ;
    if ( !LicenseRegistration.getName().equals(testObj)  )
    {
         LicenseRegistration.setName( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("LicenseRegistration Name not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("Address1_"+iExpress)) ;
    if ( !LicenseRegistration.getAddress1().equals(testObj)  )
    {
         LicenseRegistration.setAddress1( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("LicenseRegistration Address1 not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("Address2_"+iExpress)) ;
    if ( !LicenseRegistration.getAddress2().equals(testObj)  )
    {
         LicenseRegistration.setAddress2( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("LicenseRegistration Address2 not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("City_"+iExpress)) ;
    if ( !LicenseRegistration.getCity().equals(testObj)  )
    {
         LicenseRegistration.setCity( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("LicenseRegistration City not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("OrgStateID_"+iExpress)) ;
    if ( !LicenseRegistration.getOrgStateID().equals(testObj)  )
    {
         LicenseRegistration.setOrgStateID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("LicenseRegistration OrgStateID not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("Province_"+iExpress)) ;
    if ( !LicenseRegistration.getProvince().equals(testObj)  )
    {
         LicenseRegistration.setProvince( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("LicenseRegistration Province not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("ZIP_"+iExpress)) ;
    if ( !LicenseRegistration.getZIP().equals(testObj)  )
    {
         LicenseRegistration.setZIP( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("LicenseRegistration ZIP not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("CountryID_"+iExpress)) ;
    if ( !LicenseRegistration.getCountryID().equals(testObj)  )
    {
         LicenseRegistration.setCountryID( testObj,UserSecurityGroupID   );
         //testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("LicenseRegistration CountryID not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("Phone_"+iExpress)) ;
    if ( !LicenseRegistration.getPhone().equals(testObj)  )
    {
         LicenseRegistration.setPhone( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("LicenseRegistration Phone not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("Fax_"+iExpress)) ;
    if ( !LicenseRegistration.getFax().equals(testObj)  )
    {
         LicenseRegistration.setFax( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("LicenseRegistration Fax not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("ContactName_"+iExpress)) ;
    if ( !LicenseRegistration.getContactName().equals(testObj)  )
    {
         LicenseRegistration.setContactName( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("LicenseRegistration ContactName not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("ContactEmail_"+iExpress)) ;
    if ( !LicenseRegistration.getContactEmail().equals(testObj)  )
    {
         LicenseRegistration.setContactEmail( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("LicenseRegistration ContactEmail not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("DocuLinkID_"+iExpress)) ;
    if ( !LicenseRegistration.getDocuLinkID().equals(testObj)  )
    {
         LicenseRegistration.setDocuLinkID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("LicenseRegistration DocuLinkID not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("Comments_"+iExpress)) ;
    if ( !LicenseRegistration.getComments().equals(testObj)  )
    {
         LicenseRegistration.setComments( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("LicenseRegistration Comments not set. this is ok-not an error");
}


boolean bINT = false;
boolean bNextStep = false;
String sRefreshDoc = "refreshMe=true&";
              if (false&&request.getParameter("INTNext")!=null&&request.getParameter("INTNext").equalsIgnoreCase("yes") ) 
	            {
	                bINT = true;
	                sRefreshDoc = "";
	            }
              else if (request.getParameter("INTNext")!=null&&request.getParameter("INTNext").equalsIgnoreCase("next") ) 
	            {
	                bINT = true;
	                sINTNext = ConfigurationMessages.getInterviewLinkRaw("tLicenseRegistration","next");
	                sRefreshDoc = "";
	            }

// If an edit, update information; if new, append information
boolean errorRouteMe = false;
if (testChangeID.equalsIgnoreCase("1"))
{

	LicenseRegistration.setUniqueModifyDate(new java.util.Date());
	String UserLogonDescription = "n/a";
	if (pageControllerHash.containsKey("UserLogonDescription"))
	{
	    UserLogonDescription = (String)pageControllerHash.get("UserLogonDescription");
	}
    if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
    {
	    LicenseRegistration.setUniqueModifyComments("EX:"+UserLogonDescription);
	    try
	    {
	        LicenseRegistration.commitData();
	        if (!LicenseRegistration.isComplete())
	        {
	            isAllComplete = false;
	        }
	    }
	    catch (Exception e55)
	    {
	        errorRouteMe = true;
	        errorRouteMeTotal = true;
	    }	//Doculink Addition
	//Doculink Does Exist, prompt to Modify

     if (!errorRouteMe&&LicenseRegistration.getDocuLinkID().intValue()>0)
     {
            bltDocumentManagement myDoc = new bltDocumentManagement(LicenseRegistration.getDocuLinkID());
            if (myDoc.getDocumentFileName().equalsIgnoreCase("")) 
            {
                DocumentManagerUtils myDUtils = new DocumentManagerUtils();
                String myDType = "tLicenseRegistration";
                myDUtils.setDocumentType(myDType, LicenseRegistration.getUniqueID());
                myDoc.setDocumentName(myDUtils.getDocumentName());
                myDoc.setDateOfExpiration(myDUtils.getDateOfExpiration());
                myDoc.setDocumentTypeID(myDUtils.getDocumentTypeID());
                myDoc.setPhysicianID(iPhysicianID);
                myDoc.setArchived(new Integer(2));
                myDoc.commitData();
            }
            else
            {
                //archive then create new
                myDoc.setArchived(new Integer("1"));
                myDoc.commitData();
                myDoc = new bltDocumentManagement();
                DocumentManagerUtils myDUtils = new DocumentManagerUtils();
                String myDType = "tLicenseRegistration";
                myDUtils.setDocumentType(myDType, LicenseRegistration.getUniqueID());
                myDoc.setDocumentName(myDUtils.getDocumentName());
                myDoc.setDateOfExpiration(myDUtils.getDateOfExpiration());
                myDoc.setDocumentTypeID(myDUtils.getDocumentTypeID());
                myDoc.setPhysicianID(iPhysicianID);
                myDoc.setArchived(new Integer(2));
                myDoc.commitData();
                LicenseRegistration.setDocuLinkID(myDoc.getUniqueID());
                LicenseRegistration.commitData();
            }
     }
     else if (!errorRouteMe)
     {
            bltDocumentManagement myDoc = new bltDocumentManagement();
            DocumentManagerUtils myDUtils = new DocumentManagerUtils();
            String myDType = "tLicenseRegistration";
            myDUtils.setDocumentType(myDType, LicenseRegistration.getUniqueID());
            myDoc.setDocumentName(myDUtils.getDocumentName());
            myDoc.setDateOfExpiration(myDUtils.getDateOfExpiration());
            myDoc.setDocumentTypeID(myDUtils.getDocumentTypeID());
                myDoc.setPhysicianID(iPhysicianID);
            myDoc.setArchived(new Integer(2));
            myDoc.commitData();
            LicenseRegistration.setDocuLinkID(myDoc.getUniqueID());
            LicenseRegistration.commitData();
     }

    }
}       }
    }//while

if (errorRouteMeTotal)
{
	out.println(ConfigurationMessages.getMessage("FatalError")); 
}
else 
{
String nextPage = request.getParameter("nextPage");
if (nextPage==null||nextPage.equalsIgnoreCase(""))
{
	nextPage = ConfigurationMessages.getExpressLinkRaw("tLicenseRegistration","next",iExpressMode);
}
if (!isAllComplete)
{
	%>
	<script language=Javascript>
	      if ( confirm("<%=ConfigurationMessages.getMessage("ConfirmReqFieldsReturn")%>") )
	      {
	          document.location="<%=ConfigurationMessages.getDataCategoryLink("tLicenseRegistration","Express")%>";
	      }
	      else 
	      {
	          document.location="<%=nextPage%>";
	      }
	</script>
  <%
}
else
{
	%>
	<script language=Javascript>
	      document.location="<%=nextPage%>";
	</script>
  <%
}
}
       
  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>

    </td></tr></table>


<%String bnIncludeFN = "ui_"+thePLCID+"\\bot-nav_PhysicianID.jsp";
%>
<jsp:include page="<%=bnIncludeFN%>" flush="true" ><jsp:param name="plcID" value="<%=thePLCID%>"/></jsp:include>
