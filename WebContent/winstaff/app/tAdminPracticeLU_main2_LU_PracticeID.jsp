<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.PLCUtils,java.util.Vector,java.util.Enumeration,com.winstaff.ListElement,com.winstaff.bltAdminPracticeLU,com.winstaff.bltAdminPracticeLU_List_LU_PracticeID" %>
<%/*
    filename: tAdminPracticeLU_main_LU_PracticeID.jsp
    JSP AutoGen on Mar/26/2002
    Type: 1-n main class file
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>

<%
//String thePLCID = (String)pageControllerHash.get("plcID");
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_CompanyID.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>

    <table cellpadding=0 cellspacing=0 border=0 width=700>
    <tr><td width=10>&nbsp;</td><td>
    <span class=title>File: tAdminPracticeLU_main2_LU_PracticeID.jsp</span><br>


<%
//initial declaration of list class and parentID
    Integer        iPracticeID        =    null;
    boolean accessValid = false;
    if (pageControllerHash.containsKey("iPracticeID")) 
    {
        iPracticeID        =    (Integer)pageControllerHash.get("iPracticeID");
        accessValid = true;
    }
  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);
      pageControllerHash.put("sParentReturnPage","tAdminPracticeLU_main2_LU_PracticeID.jsp");
    pageControllerHash.remove("iLookupID");
    session.setAttribute("pageControllerHash",pageControllerHash);

    bltAdminPracticeLU_List_LU_PracticeID        bltAdminPracticeLU_List_LU_PracticeID        =    new    bltAdminPracticeLU_List_LU_PracticeID(iPracticeID);

//declaration of Enumeration
    bltAdminPracticeLU        working_bltAdminPracticeLU;
    ListElement         leCurrentElement;
    java.util.Enumeration eList = bltAdminPracticeLU_List_LU_PracticeID.elements();
    %>
        <%@ include file="tAdminPracticeLU_main2_LU_PracticeID_instructions.jsp" %>

        <%if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {%>
        <a class=linkBase href = "tAdminPracticeLU_main2_LU_PracticeID_form_create.jsp?EDIT=new&KM=s"><img border=0 src="ui_<%=thePLCID%>/icons/create_PracticeID.gif"></a>
        <%}%>

    <%
    while (eList.hasMoreElements())
    {
        leCurrentElement    = (ListElement) eList.nextElement();
        working_bltAdminPracticeLU  = (bltAdminPracticeLU) leCurrentElement.getObject();
        String theClass = "tdHeader";
        if (!working_bltAdminPracticeLU.isComplete())
        {
            theClass = "incompleteItem";
        %>
            <p class=incompleteItem><b>This item is incomplete</b></p>
        <%
        }
        %>
        <p class=<%=theClass%> ><b>UniqueID</b>[<i>LookupID</i>]<%=working_bltAdminPracticeLU.getLookupID()%> </p>
        <a class=linkBase href = "tAdminPracticeLU_main2_LU_PracticeID_form_authorize.jsp?EDIT=edit&EDITID=<%=working_bltAdminPracticeLU.getLookupID()%>&KM=s"><img border=0 src="ui_<%=thePLCID%>/icons/edit_PracticeID.gif"></a>

        <%if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {%>
        <a class=linkBase href = "tAdminPracticeLU_main2_LU_PracticeID_form_authorize.jsp?EDIT=del&EDITID=<%=working_bltAdminPracticeLU.getLookupID()%>&KM=p"><img border=0 src="ui_<%=thePLCID%>/icons/delete_PracticeID.gif"></a>
        </p>
        <%}%>
<%String theClassF = "textBase";%>

<%theClassF = "textBase";%>
<%if ((working_bltAdminPracticeLU.isExpired("UniqueCreateDate",expiredDays))&&(working_bltAdminPracticeLU.isExpiredCheck("UniqueCreateDate"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltAdminPracticeLU.isRequired("UniqueCreateDate"))&&(!working_bltAdminPracticeLU.isComplete("UniqueCreateDate")) ){theClassF = "requiredFieldMain";}%>

                        <p class=<%=theClassF%> ><b>Item Create Date</b><jsp:include page="../generic/DateTypeConvert_Main.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(working_bltAdminPracticeLU.getUniqueCreateDate())%>" /></jsp:include></p>


<%theClassF = "textBase";%>
<%if ((working_bltAdminPracticeLU.isExpired("UniqueModifyDate",expiredDays))&&(working_bltAdminPracticeLU.isExpiredCheck("UniqueModifyDate"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltAdminPracticeLU.isRequired("UniqueModifyDate"))&&(!working_bltAdminPracticeLU.isComplete("UniqueModifyDate")) ){theClassF = "requiredFieldMain";}%>

                        <p class=<%=theClassF%> ><b>Item Modify Date</b><jsp:include page="../generic/DateTypeConvert_Main.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(working_bltAdminPracticeLU.getUniqueModifyDate())%>" /></jsp:include></p>


<%theClassF = "textBase";%>
<%if ((working_bltAdminPracticeLU.isExpired("UniqueModifyComments",expiredDays))&&(working_bltAdminPracticeLU.isExpiredCheck("UniqueModifyComments"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltAdminPracticeLU.isRequired("UniqueModifyComments"))&&(!working_bltAdminPracticeLU.isComplete("UniqueModifyComments")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Item Modification Comments</b><%=working_bltAdminPracticeLU.getUniqueModifyComments()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltAdminPracticeLU.isExpired("AdminID",expiredDays))&&(working_bltAdminPracticeLU.isExpiredCheck("AdminID"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltAdminPracticeLU.isRequired("AdminID"))&&(!working_bltAdminPracticeLU.isComplete("AdminID")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>AdminID</b><%=working_bltAdminPracticeLU.getAdminID()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltAdminPracticeLU.isExpired("PracticeID",expiredDays))&&(working_bltAdminPracticeLU.isExpiredCheck("PracticeID"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltAdminPracticeLU.isRequired("PracticeID"))&&(!working_bltAdminPracticeLU.isComplete("PracticeID")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>PracticeID</b><%=working_bltAdminPracticeLU.getPracticeID()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltAdminPracticeLU.isExpired("Comments",expiredDays))&&(working_bltAdminPracticeLU.isExpiredCheck("Comments"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltAdminPracticeLU.isRequired("Comments"))&&(!working_bltAdminPracticeLU.isComplete("Comments")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Extra Comments</b><%=working_bltAdminPracticeLU.getComments()%></p>
        <%
    }
    %>
    <%
  }
  else
  {
   response.sendRedirect("/errorhandler.jsp");
  }
%>

    </td></tr></table>


<%String bnIncludeFN = "ui_"+thePLCID+"\\bot-nav_CompanyID.jsp";
%>
<jsp:include page="<%=bnIncludeFN%>" flush="true" ><jsp:param name="plcID" value="<%=thePLCID%>"/></jsp:include>
