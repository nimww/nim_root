<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.bltPhysicianPracticeLU ,com.winstaff.bltDocumentManagement,java.util.Vector,java.util.Enumeration,com.winstaff.ListElement, com.winstaff.PLCUtils, com.winstaff.SecurityCheck, com.winstaff.ConfigurationMessages,com.winstaff.bltPracticeMaster,com.winstaff.DocumentManagerUtils" %>
<%/*
    filename: out\jsp\tPracticeMaster_form_sub.jsp
    Created on Feb/14/2003
    Created by: Scott Ellis
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>


<%

//initial declaration of list class and parentID
    Integer        iPracticeID        =    null;
    Integer        iLookupID        =    null;
    boolean accessValid = false;

   Integer iSecurityCheck = SecurityCheck.CheckItem("Practice1", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {

    if (pageControllerHash.containsKey("iPracticeID")&&pageControllerHash.containsKey("iLookupID")) 
    {
        iPracticeID        =    (Integer)pageControllerHash.get("iPracticeID");
        iLookupID        =    (Integer)pageControllerHash.get("iLookupID");
        if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {
            accessValid = true;
        }
    }
  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);

//initial declaration of list class and parentID

    bltPracticeMaster        PracticeMaster        =    null;
    bltPhysicianPracticeLU        PhysicianPracticeLU        =    new bltPhysicianPracticeLU(iLookupID);


    if ( request.getParameter( "EDIT" ).equalsIgnoreCase("edit") )
    {
        PracticeMaster        =    new    bltPracticeMaster(iPracticeID,UserSecurityGroupID);
    }
    else if ( request.getParameter( "EDIT" ).equalsIgnoreCase("new") )
    {
        PracticeMaster        =    new    bltPracticeMaster(UserSecurityGroupID,true);
    }

String testChangeID = "0";










try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("UniqueCreateDate"))) ;
    if ( !PracticeMaster.getUniqueCreateDate().equals(testObj)  )
    {
         PracticeMaster.setUniqueCreateDate( testObj,UserSecurityGroupID   );
         PhysicianPracticeLU.setUniqueCreateDate( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster UniqueCreateDate not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("UniqueModifyDate"))) ;
    if ( !PracticeMaster.getUniqueModifyDate().equals(testObj)  )
    {
         PracticeMaster.setUniqueModifyDate( testObj,UserSecurityGroupID   );
         PhysicianPracticeLU.setUniqueModifyDate( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster UniqueModifyDate not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("UniqueModifyComments")) ;
    if ( !PracticeMaster.getUniqueModifyComments().equals(testObj)  )
    {
         PracticeMaster.setUniqueModifyComments( testObj,UserSecurityGroupID   );
         PhysicianPracticeLU.setUniqueModifyComments( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster UniqueModifyComments not set. this is ok-not an error");
}


//LU only stuff
try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("StartDate"))) ;
    if ( !PhysicianPracticeLU.getStartDate().equals(testObj)  )
    {
         PhysicianPracticeLU.setStartDate( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PhysicianPracticeLU StartDate not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("EndDate"))) ;
    if ( !PhysicianPracticeLU.getEndDate().equals(testObj)  )
    {
         PhysicianPracticeLU.setEndDate( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PhysicianPracticeLU EndDate not set. this is ok-not an error");
}


try
{
    Integer testObj = new Integer(request.getParameter("isPrimaryOffice")) ;
    if ( !PhysicianPracticeLU.getisPrimaryOffice().equals(testObj)  )
    {
         PhysicianPracticeLU.setisPrimaryOffice( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PhysicianPracticeLU isPrimaryOffice not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("isAdministrativeOffice")) ;
    if ( !PhysicianPracticeLU.getisAdministrativeOffice().equals(testObj)  )
    {
         PhysicianPracticeLU.setisAdministrativeOffice( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PhysicianPracticeLU isAdministrativeOffice not set. this is ok-not an error");
}



try
{
    String testObj = new String(request.getParameter("CoverageHours")) ;
    if ( !PhysicianPracticeLU.getCoverageHours().equals(testObj)  )
    {
         PhysicianPracticeLU.setCoverageHours( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PhysicianPracticeLU CoverageHours not set. this is ok-not an error");
}


//back to practice stuff






try
{
    String testObj = new String(request.getParameter("PracticeName")) ;
    if ( !PracticeMaster.getPracticeName().equals(testObj)  )
    {
         PracticeMaster.setPracticeName( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster PracticeName not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("DepartmentName")) ;
    if ( !PracticeMaster.getDepartmentName().equals(testObj)  )
    {
         PracticeMaster.setDepartmentName( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster DepartmentName not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("TypeOfPractice")) ;
    if ( !PracticeMaster.getTypeOfPractice().equals(testObj)  )
    {
         PracticeMaster.setTypeOfPractice( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster TypeOfPractice not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("OfficeFederalTaxID")) ;
    if ( !PracticeMaster.getOfficeFederalTaxID().equals(testObj)  )
    {
         PracticeMaster.setOfficeFederalTaxID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster OfficeFederalTaxID not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("OfficeTaxIDNameAffiliation")) ;
    if ( !PracticeMaster.getOfficeTaxIDNameAffiliation().equals(testObj)  )
    {
         PracticeMaster.setOfficeTaxIDNameAffiliation( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster OfficeTaxIDNameAffiliation not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("OfficeAddress1")) ;
    if ( !PracticeMaster.getOfficeAddress1().equals(testObj)  )
    {
         PracticeMaster.setOfficeAddress1( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster OfficeAddress1 not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("OfficeAddress2")) ;
    if ( !PracticeMaster.getOfficeAddress2().equals(testObj)  )
    {
         PracticeMaster.setOfficeAddress2( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster OfficeAddress2 not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("OfficeCity")) ;
    if ( !PracticeMaster.getOfficeCity().equals(testObj)  )
    {
         PracticeMaster.setOfficeCity( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster OfficeCity not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("OfficeStateID")) ;
    if ( !PracticeMaster.getOfficeStateID().equals(testObj)  )
    {
         PracticeMaster.setOfficeStateID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster OfficeStateID not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("OfficeProvince")) ;
    if ( !PracticeMaster.getOfficeProvince().equals(testObj)  )
    {
         PracticeMaster.setOfficeProvince( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster OfficeProvince not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("OfficeZIP")) ;
    if ( !PracticeMaster.getOfficeZIP().equals(testObj)  )
    {
         PracticeMaster.setOfficeZIP( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster OfficeZIP not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("OfficeCountryID")) ;
    if ( !PracticeMaster.getOfficeCountryID().equals(testObj)  )
    {
         PracticeMaster.setOfficeCountryID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster OfficeCountryID not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("OfficePhone")) ;
    if ( !PracticeMaster.getOfficePhone().equals(testObj)  )
    {
         PracticeMaster.setOfficePhone( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster OfficePhone not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("BackOfficePhoneNo")) ;
    if ( !PracticeMaster.getBackOfficePhoneNo().equals(testObj)  )
    {
         PracticeMaster.setBackOfficePhoneNo( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster BackOfficePhoneNo not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("OfficeFaxNo")) ;
    if ( !PracticeMaster.getOfficeFaxNo().equals(testObj)  )
    {
         PracticeMaster.setOfficeFaxNo( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster OfficeFaxNo not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("OfficeEmail")) ;
    if ( !PracticeMaster.getOfficeEmail().equals(testObj)  )
    {
         PracticeMaster.setOfficeEmail( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster OfficeEmail not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("OfficeManagerFirstName")) ;
    if ( !PracticeMaster.getOfficeManagerFirstName().equals(testObj)  )
    {
         PracticeMaster.setOfficeManagerFirstName( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster OfficeManagerFirstName not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("OfficeManagerLastName")) ;
    if ( !PracticeMaster.getOfficeManagerLastName().equals(testObj)  )
    {
         PracticeMaster.setOfficeManagerLastName( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster OfficeManagerLastName not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("OfficeManagerPhone")) ;
    if ( !PracticeMaster.getOfficeManagerPhone().equals(testObj)  )
    {
         PracticeMaster.setOfficeManagerPhone( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster OfficeManagerPhone not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("OfficeManagerEmail")) ;
    if ( !PracticeMaster.getOfficeManagerEmail().equals(testObj)  )
    {
         PracticeMaster.setOfficeManagerEmail( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster OfficeManagerEmail not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("AnsweringService")) ;
    if ( !PracticeMaster.getAnsweringService().equals(testObj)  )
    {
         PracticeMaster.setAnsweringService( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster AnsweringService not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("AnsweringServicePhone")) ;
    if ( !PracticeMaster.getAnsweringServicePhone().equals(testObj)  )
    {
         PracticeMaster.setAnsweringServicePhone( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster AnsweringServicePhone not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("Coverage247")) ;
    if ( !PracticeMaster.getCoverage247().equals(testObj)  )
    {
         PracticeMaster.setCoverage247( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster Coverage247 not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("MinorityEnterprise")) ;
    if ( !PracticeMaster.getMinorityEnterprise().equals(testObj)  )
    {
         PracticeMaster.setMinorityEnterprise( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster MinorityEnterprise not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("LanguagesSpokenInOffice")) ;
    if ( !PracticeMaster.getLanguagesSpokenInOffice().equals(testObj)  )
    {
         PracticeMaster.setLanguagesSpokenInOffice( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster LanguagesSpokenInOffice not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("AcceptAllNewPatients")) ;
    if ( !PracticeMaster.getAcceptAllNewPatients().equals(testObj)  )
    {
         PracticeMaster.setAcceptAllNewPatients( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster AcceptAllNewPatients not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("AcceptExistingPayorChange")) ;
    if ( !PracticeMaster.getAcceptExistingPayorChange().equals(testObj)  )
    {
         PracticeMaster.setAcceptExistingPayorChange( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster AcceptExistingPayorChange not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("AcceptNewFromReferralOnly")) ;
    if ( !PracticeMaster.getAcceptNewFromReferralOnly().equals(testObj)  )
    {
         PracticeMaster.setAcceptNewFromReferralOnly( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster AcceptNewFromReferralOnly not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("AcceptNewMedicare")) ;
    if ( !PracticeMaster.getAcceptNewMedicare().equals(testObj)  )
    {
         PracticeMaster.setAcceptNewMedicare( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster AcceptNewMedicare not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("AcceptNewMedicaid")) ;
    if ( !PracticeMaster.getAcceptNewMedicaid().equals(testObj)  )
    {
         PracticeMaster.setAcceptNewMedicaid( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster AcceptNewMedicaid not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("PracticeLimitationAge")) ;
    if ( !PracticeMaster.getPracticeLimitationAge().equals(testObj)  )
    {
         PracticeMaster.setPracticeLimitationAge( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster PracticeLimitationAge not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("PracticeLimitationSex")) ;
    if ( !PracticeMaster.getPracticeLimitationSex().equals(testObj)  )
    {
         PracticeMaster.setPracticeLimitationSex( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster PracticeLimitationSex not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("PracticeLimitationOther")) ;
    if ( !PracticeMaster.getPracticeLimitationOther().equals(testObj)  )
    {
         PracticeMaster.setPracticeLimitationOther( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster PracticeLimitationOther not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("BillingPayableTo")) ;
    if ( !PracticeMaster.getBillingPayableTo().equals(testObj)  )
    {
         PracticeMaster.setBillingPayableTo( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster BillingPayableTo not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("BillingFirstName")) ;
    if ( !PracticeMaster.getBillingFirstName().equals(testObj)  )
    {
         PracticeMaster.setBillingFirstName( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster BillingFirstName not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("BillingLastName")) ;
    if ( !PracticeMaster.getBillingLastName().equals(testObj)  )
    {
         PracticeMaster.setBillingLastName( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster BillingLastName not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("BillingAddress1")) ;
    if ( !PracticeMaster.getBillingAddress1().equals(testObj)  )
    {
         PracticeMaster.setBillingAddress1( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster BillingAddress1 not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("BillingAddress2")) ;
    if ( !PracticeMaster.getBillingAddress2().equals(testObj)  )
    {
         PracticeMaster.setBillingAddress2( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster BillingAddress2 not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("BillingCity")) ;
    if ( !PracticeMaster.getBillingCity().equals(testObj)  )
    {
         PracticeMaster.setBillingCity( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster BillingCity not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("BillingStateID")) ;
    if ( !PracticeMaster.getBillingStateID().equals(testObj)  )
    {
         PracticeMaster.setBillingStateID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster BillingStateID not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("BillingProvince")) ;
    if ( !PracticeMaster.getBillingProvince().equals(testObj)  )
    {
         PracticeMaster.setBillingProvince( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster BillingProvince not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("BillingZIP")) ;
    if ( !PracticeMaster.getBillingZIP().equals(testObj)  )
    {
         PracticeMaster.setBillingZIP( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster BillingZIP not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("BillingCountryID")) ;
    if ( !PracticeMaster.getBillingCountryID().equals(testObj)  )
    {
         PracticeMaster.setBillingCountryID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster BillingCountryID not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("BillingPhone")) ;
    if ( !PracticeMaster.getBillingPhone().equals(testObj)  )
    {
         PracticeMaster.setBillingPhone( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster BillingPhone not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("BillingFax")) ;
    if ( !PracticeMaster.getBillingFax().equals(testObj)  )
    {
         PracticeMaster.setBillingFax( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster BillingFax not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("CredentialingContactFirstName")) ;
    if ( !PracticeMaster.getCredentialingContactFirstName().equals(testObj)  )
    {
         PracticeMaster.setCredentialingContactFirstName( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster CredentialingContactFirstName not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("CredentialingContactLastName")) ;
    if ( !PracticeMaster.getCredentialingContactLastName().equals(testObj)  )
    {
         PracticeMaster.setCredentialingContactLastName( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster CredentialingContactLastName not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("CredentialingContactAddress1")) ;
    if ( !PracticeMaster.getCredentialingContactAddress1().equals(testObj)  )
    {
         PracticeMaster.setCredentialingContactAddress1( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster CredentialingContactAddress1 not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("CredentialingContactAddress2")) ;
    if ( !PracticeMaster.getCredentialingContactAddress2().equals(testObj)  )
    {
         PracticeMaster.setCredentialingContactAddress2( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster CredentialingContactAddress2 not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("CredentialingContactCity")) ;
    if ( !PracticeMaster.getCredentialingContactCity().equals(testObj)  )
    {
         PracticeMaster.setCredentialingContactCity( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster CredentialingContactCity not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("CredentialingContactStateID")) ;
    if ( !PracticeMaster.getCredentialingContactStateID().equals(testObj)  )
    {
         PracticeMaster.setCredentialingContactStateID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster CredentialingContactStateID not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("CredentialingContactProvince")) ;
    if ( !PracticeMaster.getCredentialingContactProvince().equals(testObj)  )
    {
         PracticeMaster.setCredentialingContactProvince( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster CredentialingContactProvince not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("CredentialingContactZIP")) ;
    if ( !PracticeMaster.getCredentialingContactZIP().equals(testObj)  )
    {
         PracticeMaster.setCredentialingContactZIP( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster CredentialingContactZIP not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("CredentialingContactCountryID")) ;
    if ( !PracticeMaster.getCredentialingContactCountryID().equals(testObj)  )
    {
         PracticeMaster.setCredentialingContactCountryID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster CredentialingContactCountryID not set. this is ok-not an error");
}



try
{
    String testObj = new String(request.getParameter("CredentialingContactPhone")) ;
    if ( !PracticeMaster.getCredentialingContactPhone().equals(testObj)  )
    {
         PracticeMaster.setCredentialingContactPhone( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster CredentialingContactPhone not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("CredentialingContactFax")) ;
    if ( !PracticeMaster.getCredentialingContactFax().equals(testObj)  )
    {
         PracticeMaster.setCredentialingContactFax( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster CredentialingContactFax not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("CredentiallingContactEmail")) ;
    if ( !PracticeMaster.getCredentiallingContactEmail().equals(testObj)  )
    {
         PracticeMaster.setCredentiallingContactEmail( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster CredentiallingContactEmail not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("OfficeWorkHoursOpenMonday")) ;
    if ( !PracticeMaster.getOfficeWorkHoursOpenMonday().equals(testObj)  )
    {
         PracticeMaster.setOfficeWorkHoursOpenMonday( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster OfficeWorkHoursOpenMonday not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("OfficeWorkHoursOpenTuesday")) ;
    if ( !PracticeMaster.getOfficeWorkHoursOpenTuesday().equals(testObj)  )
    {
         PracticeMaster.setOfficeWorkHoursOpenTuesday( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster OfficeWorkHoursOpenTuesday not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("OfficeWorkHoursOpenWednesday")) ;
    if ( !PracticeMaster.getOfficeWorkHoursOpenWednesday().equals(testObj)  )
    {
         PracticeMaster.setOfficeWorkHoursOpenWednesday( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster OfficeWorkHoursOpenWednesday not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("OfficeWorkHoursOpenThursday")) ;
    if ( !PracticeMaster.getOfficeWorkHoursOpenThursday().equals(testObj)  )
    {
         PracticeMaster.setOfficeWorkHoursOpenThursday( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster OfficeWorkHoursOpenThursday not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("OfficeWorkHoursOpenFriday")) ;
    if ( !PracticeMaster.getOfficeWorkHoursOpenFriday().equals(testObj)  )
    {
         PracticeMaster.setOfficeWorkHoursOpenFriday( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster OfficeWorkHoursOpenFriday not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("OfficeWorkHoursOpenSaturday")) ;
    if ( !PracticeMaster.getOfficeWorkHoursOpenSaturday().equals(testObj)  )
    {
         PracticeMaster.setOfficeWorkHoursOpenSaturday( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster OfficeWorkHoursOpenSaturday not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("OfficeWorkHoursOpenSunday")) ;
    if ( !PracticeMaster.getOfficeWorkHoursOpenSunday().equals(testObj)  )
    {
         PracticeMaster.setOfficeWorkHoursOpenSunday( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster OfficeWorkHoursOpenSunday not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("OfficeWorkHoursCloseMonday")) ;
    if ( !PracticeMaster.getOfficeWorkHoursCloseMonday().equals(testObj)  )
    {
         PracticeMaster.setOfficeWorkHoursCloseMonday( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster OfficeWorkHoursCloseMonday not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("OfficeWorkHoursCloseTuesday")) ;
    if ( !PracticeMaster.getOfficeWorkHoursCloseTuesday().equals(testObj)  )
    {
         PracticeMaster.setOfficeWorkHoursCloseTuesday( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster OfficeWorkHoursCloseTuesday not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("OfficeWorkHoursCloseWednesday")) ;
    if ( !PracticeMaster.getOfficeWorkHoursCloseWednesday().equals(testObj)  )
    {
         PracticeMaster.setOfficeWorkHoursCloseWednesday( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster OfficeWorkHoursCloseWednesday not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("OfficeWorkHoursCloseThursday")) ;
    if ( !PracticeMaster.getOfficeWorkHoursCloseThursday().equals(testObj)  )
    {
         PracticeMaster.setOfficeWorkHoursCloseThursday( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster OfficeWorkHoursCloseThursday not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("OfficeWorkHoursCloseFriday")) ;
    if ( !PracticeMaster.getOfficeWorkHoursCloseFriday().equals(testObj)  )
    {
         PracticeMaster.setOfficeWorkHoursCloseFriday( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster OfficeWorkHoursCloseFriday not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("OfficeWorkHoursCloseSaturday")) ;
    if ( !PracticeMaster.getOfficeWorkHoursCloseSaturday().equals(testObj)  )
    {
         PracticeMaster.setOfficeWorkHoursCloseSaturday( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster OfficeWorkHoursCloseSaturday not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("OfficeWorkHoursCloseSunday")) ;
    if ( !PracticeMaster.getOfficeWorkHoursCloseSunday().equals(testObj)  )
    {
         PracticeMaster.setOfficeWorkHoursCloseSunday( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster OfficeWorkHoursCloseSunday not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("OfficeAfterHoursOpenMonday")) ;
    if ( !PracticeMaster.getOfficeAfterHoursOpenMonday().equals(testObj)  )
    {
         PracticeMaster.setOfficeAfterHoursOpenMonday( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster OfficeAfterHoursOpenMonday not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("OfficeAfterHoursOpenTuesday")) ;
    if ( !PracticeMaster.getOfficeAfterHoursOpenTuesday().equals(testObj)  )
    {
         PracticeMaster.setOfficeAfterHoursOpenTuesday( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster OfficeAfterHoursOpenTuesday not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("OfficeAfterHoursOpenWednesday")) ;
    if ( !PracticeMaster.getOfficeAfterHoursOpenWednesday().equals(testObj)  )
    {
         PracticeMaster.setOfficeAfterHoursOpenWednesday( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster OfficeAfterHoursOpenWednesday not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("OfficeAfterHoursOpenThursday")) ;
    if ( !PracticeMaster.getOfficeAfterHoursOpenThursday().equals(testObj)  )
    {
         PracticeMaster.setOfficeAfterHoursOpenThursday( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster OfficeAfterHoursOpenThursday not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("OfficeAfterHoursOpenFriday")) ;
    if ( !PracticeMaster.getOfficeAfterHoursOpenFriday().equals(testObj)  )
    {
         PracticeMaster.setOfficeAfterHoursOpenFriday( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster OfficeAfterHoursOpenFriday not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("OfficeAfterHoursOpenSaturday")) ;
    if ( !PracticeMaster.getOfficeAfterHoursOpenSaturday().equals(testObj)  )
    {
         PracticeMaster.setOfficeAfterHoursOpenSaturday( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster OfficeAfterHoursOpenSaturday not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("OfficeAfterHoursOpenSunday")) ;
    if ( !PracticeMaster.getOfficeAfterHoursOpenSunday().equals(testObj)  )
    {
         PracticeMaster.setOfficeAfterHoursOpenSunday( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster OfficeAfterHoursOpenSunday not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("OfficeAfterHoursCloseMonday")) ;
    if ( !PracticeMaster.getOfficeAfterHoursCloseMonday().equals(testObj)  )
    {
         PracticeMaster.setOfficeAfterHoursCloseMonday( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster OfficeAfterHoursCloseMonday not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("OfficeAfterHoursCloseTuesday")) ;
    if ( !PracticeMaster.getOfficeAfterHoursCloseTuesday().equals(testObj)  )
    {
         PracticeMaster.setOfficeAfterHoursCloseTuesday( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster OfficeAfterHoursCloseTuesday not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("OfficeAfterHoursCloseWednesday")) ;
    if ( !PracticeMaster.getOfficeAfterHoursCloseWednesday().equals(testObj)  )
    {
         PracticeMaster.setOfficeAfterHoursCloseWednesday( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster OfficeAfterHoursCloseWednesday not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("OfficeAfterHoursCloseThursday")) ;
    if ( !PracticeMaster.getOfficeAfterHoursCloseThursday().equals(testObj)  )
    {
         PracticeMaster.setOfficeAfterHoursCloseThursday( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster OfficeAfterHoursCloseThursday not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("OfficeAfterHoursCloseFriday")) ;
    if ( !PracticeMaster.getOfficeAfterHoursCloseFriday().equals(testObj)  )
    {
         PracticeMaster.setOfficeAfterHoursCloseFriday( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster OfficeAfterHoursCloseFriday not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("OfficeAfterHoursCloseSaturday")) ;
    if ( !PracticeMaster.getOfficeAfterHoursCloseSaturday().equals(testObj)  )
    {
         PracticeMaster.setOfficeAfterHoursCloseSaturday( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster OfficeAfterHoursCloseSaturday not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("OfficeAfterHoursCloseSunday")) ;
    if ( !PracticeMaster.getOfficeAfterHoursCloseSunday().equals(testObj)  )
    {
         PracticeMaster.setOfficeAfterHoursCloseSunday( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster OfficeAfterHoursCloseSunday not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("Comments")) ;
    if ( !PracticeMaster.getComments().equals(testObj)  )
    {
         PracticeMaster.setComments( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster Comments not set. this is ok-not an error");
}






boolean bINT = false;
boolean bNextStep = false;
String sRefreshDoc = "refreshMe=true&";

// If an edit, update information; if new, append information
boolean errorRouteMe = false;

if (testChangeID.equalsIgnoreCase("1"))
{

	PracticeMaster.setUniqueModifyDate(new java.util.Date());
	PhysicianPracticeLU.setUniqueModifyDate(new java.util.Date());
	String UserLogonDescription = "n/a";
	if (pageControllerHash.containsKey("UserLogonDescription"))
	{
	    UserLogonDescription = (String)pageControllerHash.get("UserLogonDescription");
	}
    //if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
    {
	    PracticeMaster.setUniqueModifyComments(UserLogonDescription);
	    PhysicianPracticeLU.setUniqueModifyComments(UserLogonDescription);
	    try
	    {
		    PracticeMaster.commitData();
		    PhysicianPracticeLU.commitData();
	    }
	    catch (Exception e55)
	    {
	        errorRouteMe = true;
	    }

    }
}
String routePageReference = request.getParameter("routePageReference");
String nextPage=null;
if (routePageReference!=null)
{
        if (pageControllerHash.containsKey(routePageReference))
        {
            nextPage = (String)pageControllerHash.get(routePageReference);
        }
        else if (pageControllerHash.containsKey("sLocalChildReturnPage"))
        {
            nextPage = (String)pageControllerHash.get("sLocalChildReturnPage");
        }
}
if (errorRouteMe)
{
	out.println(ConfigurationMessages.getMessage("FatalError")); 
}
else if (nextPage!=null)
{
	    %><script language=Javascript>
	      document.location="<%=nextPage%>";
	      </script><%
    //response.sendRedirect(nextPage+"?EDIT=edit");
}
        %>

  <%
  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
  %>
