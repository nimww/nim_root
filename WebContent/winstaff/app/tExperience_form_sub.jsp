<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.bltDocumentManagement,java.util.Vector,java.util.Enumeration,com.winstaff.ListElement, com.winstaff.PLCUtils, com.winstaff.SecurityCheck, com.winstaff.ConfigurationMessages,com.winstaff.bltExperience,com.winstaff.DocumentManagerUtils" %>
<%/*
    filename: out\jsp\tExperience_form_sub.jsp
    Created on Sep/11/2003
    Created by: Scott Ellis
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>


<%

//initial declaration of list class and parentID
    Integer        iExperienceID        =    null;
    boolean accessValid = false;

   Integer iSecurityCheck = SecurityCheck.CheckItem("PractitionerSection7", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {

    if (pageControllerHash.containsKey("iExperienceID")) 
    {
        iExperienceID        =    (Integer)pageControllerHash.get("iExperienceID");
        if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {
            accessValid = true;
        }
    }
  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);

//initial declaration of list class and parentID

    bltExperience        Experience        =    null;

    if ( request.getParameter( "EDIT" ).equalsIgnoreCase("edit") )
    {
        Experience        =    new    bltExperience(iExperienceID,UserSecurityGroupID);
    }
    else if ( request.getParameter( "EDIT" ).equalsIgnoreCase("new") )
    {
        Experience        =    new    bltExperience(UserSecurityGroupID,true);
    }

String testChangeID = "0";

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("UniqueCreateDate"))) ;
    if ( !Experience.getUniqueCreateDate().equals(testObj)  )
    {
         Experience.setUniqueCreateDate( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("Experience UniqueCreateDate not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("UniqueModifyDate"))) ;
    if ( !Experience.getUniqueModifyDate().equals(testObj)  )
    {
         Experience.setUniqueModifyDate( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("Experience UniqueModifyDate not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("UniqueModifyComments")) ;
    if ( !Experience.getUniqueModifyComments().equals(testObj)  )
    {
         Experience.setUniqueModifyComments( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("Experience UniqueModifyComments not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("PhysicianID")) ;
    if ( !Experience.getPhysicianID().equals(testObj)  )
    {
         Experience.setPhysicianID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("Experience PhysicianID not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("TypeOfExperience")) ;
    if ( !Experience.getTypeOfExperience().equals(testObj)  )
    {
         Experience.setTypeOfExperience( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("Experience TypeOfExperience not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("Other")) ;
    if ( !Experience.getOther().equals(testObj)  )
    {
         Experience.setOther( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("Experience Other not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("Specialty")) ;
    if ( !Experience.getSpecialty().equals(testObj)  )
    {
         Experience.setSpecialty( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("Experience Specialty not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("DateFrom"))) ;
    if ( !Experience.getDateFrom().equals(testObj)  )
    {
         Experience.setDateFrom( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("Experience DateFrom not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("DateTo"))) ;
    if ( !Experience.getDateTo().equals(testObj)  )
    {
         Experience.setDateTo( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("Experience DateTo not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("NoComplete")) ;
    if ( !Experience.getNoComplete().equals(testObj)  )
    {
         Experience.setNoComplete( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("Experience NoComplete not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("Name")) ;
    if ( !Experience.getName().equals(testObj)  )
    {
         Experience.setName( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("Experience Name not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("Address1")) ;
    if ( !Experience.getAddress1().equals(testObj)  )
    {
         Experience.setAddress1( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("Experience Address1 not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("Address2")) ;
    if ( !Experience.getAddress2().equals(testObj)  )
    {
         Experience.setAddress2( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("Experience Address2 not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("City")) ;
    if ( !Experience.getCity().equals(testObj)  )
    {
         Experience.setCity( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("Experience City not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("StateID")) ;
    if ( !Experience.getStateID().equals(testObj)  )
    {
         Experience.setStateID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("Experience StateID not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("Province")) ;
    if ( !Experience.getProvince().equals(testObj)  )
    {
         Experience.setProvince( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("Experience Province not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("ZIP")) ;
    if ( !Experience.getZIP().equals(testObj)  )
    {
         Experience.setZIP( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("Experience ZIP not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("CountryID")) ;
    if ( !Experience.getCountryID().equals(testObj)  )
    {
         Experience.setCountryID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("Experience CountryID not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("Phone")) ;
    if ( !Experience.getPhone().equals(testObj)  )
    {
         Experience.setPhone( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("Experience Phone not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("Fax")) ;
    if ( !Experience.getFax().equals(testObj)  )
    {
         Experience.setFax( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("Experience Fax not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("ContactName")) ;
    if ( !Experience.getContactName().equals(testObj)  )
    {
         Experience.setContactName( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("Experience ContactName not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("ContactEmail")) ;
    if ( !Experience.getContactEmail().equals(testObj)  )
    {
         Experience.setContactEmail( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("Experience ContactEmail not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("Comments")) ;
    if ( !Experience.getComments().equals(testObj)  )
    {
         Experience.setComments( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("Experience Comments not set. this is ok-not an error");
}
boolean bINT = false;
boolean bNextStep = false;
String sRefreshDoc = "refreshMe=true&";
              if (request.getParameter("INTNext")!=null&&request.getParameter("INTNext").equalsIgnoreCase("yes") ) 
	            {
	                bINT = true;
	                sRefreshDoc = "";
	            }
              else if (request.getParameter("INTNext")!=null&&request.getParameter("INTNext").equalsIgnoreCase("next") ) 
	            {
	                bINT = true;
	                sINTNext = ConfigurationMessages.getInterviewLinkRaw("tExperience","next");
	                sRefreshDoc = "";
	            }

// If an edit, update information; if new, append information
boolean errorRouteMe = false;
if (testChangeID.equalsIgnoreCase("1"))
{

	Experience.setUniqueModifyDate(new java.util.Date());
	String UserLogonDescription = "n/a";
	if (pageControllerHash.containsKey("UserLogonDescription"))
	{
	    UserLogonDescription = (String)pageControllerHash.get("UserLogonDescription");
	}
    //if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
    {
	    Experience.setUniqueModifyComments(UserLogonDescription);
	    try
	    {
	        Experience.commitData();
	    }
	    catch (Exception e55)
	    {
	        errorRouteMe = true;
	    }
    }
}
String routePageReference = request.getParameter("routePageReference");
String nextPage=null;
if (routePageReference!=null)
{
        if (pageControllerHash.containsKey(routePageReference))
        {
            nextPage = (String)pageControllerHash.get(routePageReference);
        }
        else if (pageControllerHash.containsKey("sLocalChildReturnPage"))
        {
            nextPage = (String)pageControllerHash.get("sLocalChildReturnPage");
        }
        else if (pageControllerHash.containsKey("sParentReturnPage"))
        {
            nextPage = (String)pageControllerHash.get("sParentReturnPage");
        }
}
              if (bINT ) 
	            {
	                nextPage = sINTNext;
	                if (nextPage==null||nextPage.equalsIgnoreCase("")||nextPage.equalsIgnoreCase("#"))
	                {
	                    nextPage = (String)pageControllerHash.get("sLocalChildReturnPage");
	                }
	            }
if (errorRouteMe)
{
	out.println(ConfigurationMessages.getMessage("FatalError")); 
}
else if (nextPage!=null)
{
	    %><script language=Javascript>
	      document.location="<%=nextPage%>";
	      </script><%
    //response.sendRedirect(nextPage+"?EDIT=edit");
}
        %>


  <%
  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
  %>
