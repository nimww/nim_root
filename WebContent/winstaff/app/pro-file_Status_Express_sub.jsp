

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.ConfigurationMessages,com.winstaff.bltHCOPhysicianLU_List_LU_PhysicianID,com.winstaff.bltHCOPhysicianLU, com.winstaff.bltPhysicianUserAccountLU_List_LU_PhysicianID, com.winstaff.bltPhysicianUserAccountLU, com.winstaff.bltDocumentManagement_List,com.winstaff.bltDocumentManagement,com.winstaff.PLCUtils,java.util.Vector,java.util.Enumeration,com.winstaff.ListElement,com.winstaff.bltPhysicianMaster,com.winstaff.stepVerify_PhysicianID" %>
<%/*
    filename: out\jsp\tPhysicianMaster_form.jsp
    JSP AutoGen on Mar/02/2002
*/%>
<%@ include file="../generic/CheckLogin.jsp" %>
<%
//initial declaration of list class and parentID
    Integer        iPhysicianID        =    null;
    boolean accessValid = false;
    // required for Type2
    String sKeyMasterReference = null;
    if (pageControllerHash.containsKey("iPhysicianID")) 
    {
        iPhysicianID        =    (Integer)pageControllerHash.get("iPhysicianID");
        accessValid = true;    
   }
  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);
      java.text.SimpleDateFormat displayDateTimeSDF = new java.text.SimpleDateFormat("MM/dd/yyyy hh:mm a");

    pageControllerHash.put("sLocalChildReturnPage","pro-file_Status.jsp");
    pageControllerHash.put("sParentReturnPage","pro-file_Status.jsp");

String sExpressMode = "1";
if (request.getParameter("sExpressMode")!=null)
{
    pageControllerHash.put("iExpressMode",new Integer(request.getParameter("sExpressMode")));

}


    session.setAttribute("pageControllerHash",pageControllerHash);

stepVerify_PhysicianID mySV = new stepVerify_PhysicianID();
mySV.setPhysicianID(iPhysicianID);
mySV.setSecurityID(UserSecurityGroupID);
bltPhysicianMaster        PhysicianMaster        =   new  bltPhysicianMaster (iPhysicianID);

//verify data
boolean ApplicationData = false;
if (mySV.isAllCore())
{
	ApplicationData = true;
}
//verify documents are complete:


boolean SupportingDocuments = true;
{
    bltDocumentManagement_List        bltDocumentManagement_List        =    new    bltDocumentManagement_List(iPhysicianID,"Archived=2","");
	//declaration of Enumeration
    bltDocumentManagement        working_bltDocumentManagement;
    ListElement         leCurrentElement;
    java.util.Enumeration eList = bltDocumentManagement_List.elements();
    while (eList.hasMoreElements()&&SupportingDocuments)
    {
        leCurrentElement    = (ListElement) eList.nextElement();
        working_bltDocumentManagement  = (bltDocumentManagement) leCurrentElement.getObject();
		if (!working_bltDocumentManagement.isComplete())
		{
			SupportingDocuments = false;
		}
	}
}
	//check Attestation Account Status
	boolean AttestationAccount = false;
	boolean AttestationAccountCreated = false;
{
    bltPhysicianUserAccountLU_List_LU_PhysicianID        bltPhysicianUserAccountLU_List_LU_PhysicianID        =    new    bltPhysicianUserAccountLU_List_LU_PhysicianID(iPhysicianID);
	//declaration of Enumeration
    bltPhysicianUserAccountLU        working_bltPhysicianUserAccountLU;
    ListElement         leCurrentElement;
    java.util.Enumeration eList = bltPhysicianUserAccountLU_List_LU_PhysicianID.elements();
    if (eList.hasMoreElements())
    {        
 	     AttestationAccountCreated = true;
         leCurrentElement    = (ListElement) eList.nextElement();
         working_bltPhysicianUserAccountLU  = (bltPhysicianUserAccountLU) leCurrentElement.getObject();
         bltUserAccount working_bltUserAccount  = new bltUserAccount(working_bltPhysicianUserAccountLU.getUserID());
		 if (working_bltUserAccount.getStatus().intValue()==2)
		 {
	 	     AttestationAccount = true;
		 }
    }
}

//check attest status
boolean Attestation = false;
if (PhysicianMaster.getIsAttested().intValue()==1)
{
	Attestation = true;
}


//check HCO Auth Status
boolean HCOAuth = false;
{
    bltHCOPhysicianLU_List_LU_PhysicianID        bltHCOPhysicianLU_List_LU_PhysicianID        =    new    bltHCOPhysicianLU_List_LU_PhysicianID(iPhysicianID);

//declaration of Enumeration
    bltHCOPhysicianLU        working_bltHCOPhysicianLU;
    ListElement         leCurrentElement;
    java.util.Enumeration eList = bltHCOPhysicianLU_List_LU_PhysicianID.elements();
    if (eList.hasMoreElements())
    {
         HCOAuth = true;
    }

}

String sContinuePage = "tPhysicianMaster_form_Express.jsp?EDIT=edit";
if (request.getParameter("isSmart")!=null)
{

if (true||request.getParameter("isSmart").equalsIgnoreCase("checked"))
{


if (!mySV.isStep1Valid())
{
	sContinuePage = "tPhysicianMaster_form_Express.jsp?EDIT=edit";
}
else if (!mySV.isStep2Valid())
{
	sContinuePage = "tCoveringPhysicians_main_CoveringPhysicians_PhysicianID_Express.jsp";
}
else if (!mySV.isStep3Valid())
{
	sContinuePage = "tLicenseRegistration_main_LicenseRegistration_PhysicianID_Express.jsp";
}
else if (!mySV.isStep4Valid())
{
	sContinuePage = "tBoardCertification_main_BoardCertification_PhysicianID_Express.jsp";
}
else if (!mySV.isStep5Valid())
{
	sContinuePage = "tOtherCertification_main_OtherCertification_PhysicianID_Express.jsp";
}
else if (!mySV.isStep6Valid())
{
	sContinuePage = "tProfessionalEducation_main_ProfessionalEducation_PhysicianID_Express.jsp";
}
else if (!mySV.isStep7Valid())
{
	sContinuePage = "tExperience_main_Experience_PhysicianID_Express.jsp";
}
else if (!mySV.isStep8Valid())
{
	sContinuePage = "tWorkHistory_main_WorkHistory_PhysicianID_Express.jsp";
}
else if (!mySV.isStep9Valid())
{
	sContinuePage = "tProfessionalLiability_main_ProfessionalLiability_PhysicianID_Express.jsp";
}
else if (!mySV.isStep10Valid())
{
	sContinuePage = "tFacilityAffiliation_main_FacilityAffiliation_PhysicianID_Express.jsp";
}
else if (!mySV.isStep11Valid())
{
	sContinuePage = "tPeerReference_main_PeerReference_PhysicianID_Express.jsp";
}
else if (!mySV.isStep12Valid())
{
	sContinuePage = "tProfessionalSociety_main_ProfessionalSociety_PhysicianID_Express.jsp";
}
else if (!mySV.isStep13Valid())
{
	sContinuePage = "tAttestR_form2_Express.jsp";
}
else if (!mySV.isStep14Valid())
{
	sContinuePage = "tMalpractice_main_Malpractice_PhysicianID_Express.jsp";
}
else if (!mySV.isStep15Valid())
{
	sContinuePage = "tProfessionalMisconduct_main_ProfessionalMisconduct_PhysicianID_Express.jsp";
}
else if (!mySV.isStep16Valid())
{
	sContinuePage = "tContinuingEducation_main_ContinuingEducation_PhysicianID_Express.jsp";
}
else if (!mySV.isStep17Valid())
{
	sContinuePage = "tManagedCarePlan_main_ManagedCarePlan_PhysicianID_Express.jsp";
}

}

}

response.sendRedirect(sContinuePage);

%>
<%

  }
  else
  {
   response.sendRedirect("/errorhandler.jsp");
  }

%>
