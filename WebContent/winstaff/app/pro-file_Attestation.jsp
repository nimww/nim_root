<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.bltPhysicianUserAccountLU_List_LU_PhysicianID, com.winstaff.bltPhysicianUserAccountLU, com.winstaff.dateUtils, com.winstaff.ConfigurationMessages,com.winstaff.SecurityCheck,com.winstaff.PLCUtils,java.util.Vector,java.util.Enumeration,com.winstaff.ListElement,com.winstaff.bltPhysicianMaster,com.winstaff.stepVerify_PhysicianID" %>
<%/*
    filename: out\jsp\tPhysicianMaster_form.jsp
    JSP AutoGen on Mar/02/2002
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>

<%
//String thePLCID = (String)pageControllerHash.get("plcID");
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_PhysicianID.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>
  <%
//initial declaration of list class and parentID
    Integer        iPhysicianID        =    null;
    Integer        iUserID        =    null;
    boolean accessValid = true;
    // required for Type2
    String sKeyMasterReference = null;
   Integer iSecurityCheck = SecurityCheck.CheckItem("PractitionerAttestation1", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {

   if (pageControllerHash.containsKey("iPhysicianID")) 
   {
        iPhysicianID        =    (Integer)pageControllerHash.get("iPhysicianID");
   }
   else
   {
        accessValid = false;    
   }
  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);      java.text.SimpleDateFormat displayDateTimeSDF = new java.text.SimpleDateFormat("MM/dd/yyyy hh:mm a");

    pageControllerHash.put("sLocalChildReturnPage","pro-file_Status.jsp");
    session.setAttribute("pageControllerHash",pageControllerHash);
stepVerify_PhysicianID mySV = new stepVerify_PhysicianID();
mySV.setPhysicianID(iPhysicianID);
bltPhysicianMaster        PhysicianMaster        =   new  bltPhysicianMaster (iPhysicianID);

%>
<table width="<%=MasterTableWidth%>" border="0" cellspacing="0" cellpadding="0">
  <tr> 
    <td width=10>&nbsp;</td>
    <td> <%=ConfigurationMessages.getHTML("INTERVIEWTopControl","pro-file_Attestation")%> 
    </td>
  </tr>
  <tr> 
    <td width=10>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <%
boolean readyToAttest = false;

//verify data
boolean ApplicationDataCore = false;
if (mySV.isAllCore())
{
	ApplicationDataCore = true;
}
boolean ApplicationDataExtra = false;
if (mySV.isAllExtra())
{
	ApplicationDataExtra = true;
}
boolean ApplicationDataAll = (ApplicationDataExtra&&ApplicationDataCore);



	//check Attestation Account Status
	boolean AttestationAccount = false;
	boolean AttestationAccountCreated = false;
{
    bltPhysicianUserAccountLU_List_LU_PhysicianID        bltPhysicianUserAccountLU_List_LU_PhysicianID        =    new    bltPhysicianUserAccountLU_List_LU_PhysicianID(iPhysicianID);
	//declaration of Enumeration
    bltPhysicianUserAccountLU        working_bltPhysicianUserAccountLU;
    ListElement         leCurrentElement;
    java.util.Enumeration eList = bltPhysicianUserAccountLU_List_LU_PhysicianID.elements();
    if (eList.hasMoreElements())
    {        
 	     AttestationAccountCreated = true;
         leCurrentElement    = (ListElement) eList.nextElement();
         working_bltPhysicianUserAccountLU  = (bltPhysicianUserAccountLU) leCurrentElement.getObject();
         bltUserAccount working_bltUserAccount  = new bltUserAccount(working_bltPhysicianUserAccountLU.getUserID());
		 if (working_bltUserAccount.getStatus().intValue()==2)
		 {
	 	     AttestationAccount = true;
		 }
    }
}



if (ApplicationDataAll&&AttestationAccountCreated)
{
	readyToAttest = true;
}


if (readyToAttest)
{

if (PhysicianMaster.getIsAttested().intValue()==1)
{
%>
  <tr> 
    <td width=10>&nbsp;</td>
    <td class=tdBase> 
      <table width="100%%" border="0" cellspacing="0" cellpadding="5">
        <tr class=tdHeader> 
          <td >
            <ul>
              <li>Your application was last signed on <%=displayDateSDF.format(PhysicianMaster.getAttestDate())%>. 
              </li>
              <li>This signature <b>expires</b> on <%=displayDateSDF.format(dateUtils.dateChange(PhysicianMaster.getAttestDate(),90))%>. 
              </li>
              <li>Please be sure to re-sign your application before this expiration 
                date.</li>
            </ul>
          </td>
        </tr>
        <tr class=tdHeader> 
          <td class=title>&nbsp;</td>
        </tr>
      </table>
    </td>
  </tr>
  <%
}
else if (PhysicianMaster.getIsAttestedPending().intValue()==1 )
{
%>
  <tr> 
    <td width=10>&nbsp;</td>
    <td class=tdBase> 
      <table width="100%%" border="0" cellspacing="0" cellpadding="5">
        <tr class=tdHeader> 
          <td >Your application was last signed on <%=displayDateSDF.format(PhysicianMaster.getAttestDatePending())%>. 
            This signature expires on <%=displayDateSDF.format(dateUtils.dateChange(PhysicianMaster.getAttestDatePending(),90))%>. 
            Please be sure to re-sign your application before this expiration 
            date.</td>
        </tr>
  <tr>
          <td bgcolor="#FFDDDD" class=instructions><b>You still need to fax in 
            your WIN/Staff Release Form. If you do not yet have this form, you 
            can print it out on the previous step: <a href="tPhysicianUserAccountLU_main2_UserAccount_PhysicianID.jsp">Section 
            21</a><br>
            If you have already faxed this in, please allow up to 3 business days 
            for us to process it. If it is still not processed after 3 business 
            days, please call us at 800-995-4233.</b></td>
  </tr>
        <tr class=tdHeader> 
          <td class=title>&nbsp;</td>
        </tr>
      </table>
    </td>
  </tr>
  <%
}
%>
  <tr> 
    <td width=10>&nbsp;</td>
    <td class=tdHeaderAlt> 
      <table width="100%%" border="0" cellspacing="0" cellpadding="5">
        <tr class=tdHeaderAlt> 
          <td class=title>Review the terms below and sign at the bottom. </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr> 
    <td width=10>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td width=10>&nbsp;</td>
    <td bgcolor="#FFDDDD" class=instructions><b>You must use the information for 
      your Attestation Account created on the previous step: <a href="tPhysicianUserAccountLU_main2_UserAccount_PhysicianID.jsp">Section 
      21</a></b></td>
  </tr>
  <tr> 
    <td width=10>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>

<%
if (!AttestationAccount&&false)
{
%>
  <tr>
    <td width=10>&nbsp;</td>
    <td bgcolor="#FFDDDD" class=instructions><b>Please remember to fax in your WIN/Staff Release Form. If you do not yet have this form, you can print it out on the previous step: <a href="tPhysicianUserAccountLU_main2_UserAccount_PhysicianID.jsp">Section 
      21</a></b></td>
  </tr>
  <tr> 
    <td width=10>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
<%
}
%>
  <tr> 
    <td> </td>
    <td> 
      <p class="tdBase">I hereby affirm that the information submitted in this 
        application is true to the best of my knowledge and belief and furnished 
        in good faith. I understand that omissions or misrepresentations may result 
        in denial of my application or termination of my privileges, employment, 
        or provider participation agreement.<br>
        To sign, please enter your Attestation Keywords here:</p>
      <p></p>
      <p class=tdBase>Please enter your Attestation Keywords here:</p>
      <form name="attest1" method="post" action="pro-file_Attestation_sub.jsp">
        <table border="1" cellspacing="0" cellpadding="0" bordercolor=333333>
          <tr> 
            <td> 
              <table border="0" cellspacing="0" cellpadding="5">
                <tr class=tdHeaderAlt> 
                  <td colspan="3">For the Safety and Security of your information, 
                    we require that you enter your Practitioner <i>User Name</i> 
                    and <i>Password</i> again. </td>
                </tr>
                <tr class=tdBase> 
                  <td>Practitioner Attestation Account User Name</td>
                  <td width="10">&nbsp;</td>
                  <td> 
                    <input type="text" name="LogonUserName">
                  </td>
                </tr>
                <tr class=tdBase> 
                  <td>Practitioner Attestation Account Password</td>
                  <td width="10">&nbsp;</td>
                  <td> 
                    <input type="password" name="LogonUserPassword">
                  </td>
                </tr>
                <tr class=tdBaseAlt> 
                  <td>Attestation Keyword </td>
                  <td width="10">&nbsp;</td>
                  <td> 
                    <input type="password" name="AttestKeyword1">
                  </td>
                </tr>
                <!--
          <tr class=tdBaseAlt> 
            <td>Attestation Keyword 2</td>
            <td width="10">&nbsp;</td>
            <td> 
              <input type="password" name="AttestKeyword2">
            </td>
          </tr>
-->
              </table>
            </td>
          </tr>
        </table>
        <p> 
          <input type="submit"  <%=HTMLFormStyleButton%>  name="Submit" value="Submit">
        </p>
      </form>
      <hr>
      <%
}
else if (!ApplicationDataAll)
{
%>
  <tr> 
    <td> </td>
    <td> 
      <p class=requiredField>You cannot Sign your application yet because your 
        application data is NOT complete. You must complete all steps and have 
        an active, viewable account before you can proceed with attestation. To 
        view your checklist - <a href="FinishedCheckList_PhysicianID.jsp">click 
        here</a></p>
<br>
<br>
<br>
<br>
      <%
}
else
{
%>
  <tr> 
    <td> </td>
    <td class=instructions><font size=2><b>You must first create an Attestation Account. To do this, go here: <a href="tPhysicianUserAccountLU_main2_UserAccount_PhysicianID.jsp">Section 
      21</a></b></font>
<br>
<br>
<br>
<br>
      <%
}
%>
    </td>
  </tr>
</table>
<%

  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>

<%String bnIncludeFN = "ui_"+thePLCID+"\\bot-nav_PhysicianID.jsp";
%>
<jsp:include page="<%=bnIncludeFN%>" flush="true" > 
<jsp:param name="plcID" value="<%=thePLCID%>"/>
</jsp:include>
