<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.*" %>
<%/*

    filename: out\jsp\tNIM3_Modality_form.jsp
    Created on Oct/28/2009
    Created by: Scott Ellis
    Version: 4.0
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>

<link href="ui_1/style.css" rel="stylesheet" type="text/css" />

<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_PracticeID_nim.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>

<!--<body onUnload="if (document.getElementById('isSaveMe').value=='1'&&confirm('Want to save first?')){document.getElementById('tNIM3_Modality_form1').submit();}">-->

    <table cellpadding=0 cellspacing=0 border=0 width=100% >
    <tr><td width=10>&nbsp;</td><td>
    <%=ConfigurationMessages.getHTML("INTERVIEWTopControl_form","tNIM3_Modality")%>



<%
//initial declaration of list class and parentID
    Integer        iModalityID        =    null;
    Integer        iEDITID        =    null;
    if ( request.getParameter( "EDITID" ) != null )
    {
    	iEDITID        =    new Integer(request.getParameter ("EDITID"));
    }
    else
    {
    	iEDITID        =    new Integer(0);
    }
    boolean accessValid = false;
    // required for Type2
    String sKeyMasterReference = null;

   Integer iSecurityCheck = SecurityCheck.CheckItem("nim1", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {
    if (pageControllerHash.containsKey("iModalityID")) 
    {
        iModalityID        =    (Integer)pageControllerHash.get("iModalityID");
        accessValid = true;
        if (iModalityID.intValue() != iEDITID.intValue())
        {
        	accessValid = false;
        }
    }
  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);      java.text.SimpleDateFormat displayDateTimeSDF = new java.text.SimpleDateFormat("MM/dd/yyyy hh:mm a");

    pageControllerHash.put("sLocalChildReturnPage","tNIM3_Modality_form.jsp?EDIT=edit&EDITID=" + iEDITID);
//initial declaration of list class and parentID

    bltNIM3_Modality        NIM3_Modality        =    null;

    if ( request.getParameter( "EDIT" ).equalsIgnoreCase("edit") )
    {
        NIM3_Modality        =    new    bltNIM3_Modality(iModalityID,UserSecurityGroupID);
    }
    else if ( request.getParameter( "EDIT" ).equalsIgnoreCase("new") )
    {
        NIM3_Modality        =    new    bltNIM3_Modality(UserSecurityGroupID, true);
    }

//fields
        %>
        <%@ include file="tNIM3_Modality_form_instructions.jsp" %>
        <form action="tNIM3_Modality_form_sub.jsp" name="tNIM3_Modality_form1"  id="tNIM3_Modality_form1" method="POST">
        <input onChange="document.getElementById('isSaveMe').value='1';" onChange="document.getElementById('isSaveMe').value='1';" type="hidden" name="isSaveMe" id="isSaveMe" value="0" />
<%
    if ( request.getParameter( "EDIT" ) != null )
    {
    %>
        <input onChange="document.getElementById('isSaveMe').value='1';" type="hidden" name="EDIT" value = "<%=request.getParameter( "EDIT" )%>" >  
    <%
    }
    %>
        <input onChange="document.getElementById('isSaveMe').value='1';" type="hidden" name="EDITID" value = "<%=iEDITID%>" >  

          <%  String theClass ="tdBase";%>
        <table width=100% border=1 bordercolor=#333333 cellpadding=3 cellspacing=0 class=tableBase>
         <tr><td class=tableColor>


            <table cellpadding=0 cellspacing=0 width=100%>
            <%
            if ( (NIM3_Modality.isRequired("ModalityTypeID",UserSecurityGroupID))&&(!NIM3_Modality.isComplete("ModalityTypeID")) )
            {
                theClass = "requiredField";
            }
            else if ((NIM3_Modality.isExpired("ModalityTypeID",expiredDays))&&(NIM3_Modality.isExpiredCheck("ModalityTypeID",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((NIM3_Modality.isWrite("ModalityTypeID",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Modality Type</b></p></td><td valign=top><p><select onChange="document.getElementById('isSaveMe').value='1';"   name="ModalityTypeID" ><jsp:include page="../generic/tModalityTypeLIShort.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=NIM3_Modality.getModalityTypeID()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ModalityTypeID&amp;sTableName=tNIM3_Modality&amp;sRefID=<%=NIM3_Modality.getModalityID()%>&amp;sFieldNameDisp=<%=NIM3_Modality.getEnglish("ModalityTypeID")%>&amp;sTableNameDisp=tNIM3_Modality','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((NIM3_Modality.isRead("ModalityTypeID",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>ModalityTypeID&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tModalityTypeLIShort_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=NIM3_Modality.getModalityTypeID()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ModalityTypeID&amp;sTableName=tNIM3_Modality&amp;sRefID=<%=NIM3_Modality.getModalityID()%>&amp;sFieldNameDisp=<%=NIM3_Modality.getEnglish("ModalityTypeID")%>&amp;sTableNameDisp=tNIM3_Modality','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (NIM3_Modality.isRequired("ModalityModelID",UserSecurityGroupID))&&(!NIM3_Modality.isComplete("ModalityModelID")) )
            {
                theClass = "requiredField";
            }
            else if ((NIM3_Modality.isExpired("ModalityModelID",expiredDays))&&(NIM3_Modality.isExpiredCheck("ModalityModelID",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((NIM3_Modality.isWrite("ModalityModelID",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Model</b></p></td><td valign=top><p><select onChange="document.getElementById('isSaveMe').value='1';"   name="ModalityModelID" ><jsp:include page="../generic/tMRI_ModelLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=NIM3_Modality.getModalityModelID()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ModalityModelID&amp;sTableName=tNIM3_Modality&amp;sRefID=<%=NIM3_Modality.getModalityID()%>&amp;sFieldNameDisp=<%=NIM3_Modality.getEnglish("ModalityModelID")%>&amp;sTableNameDisp=tNIM3_Modality','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((NIM3_Modality.isRead("ModalityModelID",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>ModalityModelID&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tMRI_ModelLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=NIM3_Modality.getModalityModelID()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ModalityModelID&amp;sTableName=tNIM3_Modality&amp;sRefID=<%=NIM3_Modality.getModalityID()%>&amp;sFieldNameDisp=<%=NIM3_Modality.getEnglish("ModalityModelID")%>&amp;sTableNameDisp=tNIM3_Modality','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (NIM3_Modality.isRequired("SoftwareVersion",UserSecurityGroupID))&&(!NIM3_Modality.isComplete("SoftwareVersion")) )
            {
                theClass = "requiredField";
            }
            else if ((NIM3_Modality.isExpired("SoftwareVersion",expiredDays))&&(NIM3_Modality.isExpiredCheck("SoftwareVersion",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((NIM3_Modality.isWrite("SoftwareVersion",UserSecurityGroupID)))
            {
                        %>
                       <tr><td valign=top><p class=<%=theClass%> ><b>Software Version&nbsp;</b></p></td><td valign=top><p>
                         <input onChange="document.getElementById('isSaveMe').value='1';" name="SoftwareVersion" type="text" onKeyDown="textAreaStop(this,200)" value="<%=NIM3_Modality.getSoftwareVersion()%>" size="40" maxlength="200" />
                         &nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SoftwareVersion&amp;sTableName=tNIM3_Modality&amp;sRefID=<%=NIM3_Modality.getModalityID()%>&amp;sFieldNameDisp=<%=NIM3_Modality.getEnglish("SoftwareVersion")%>&amp;sTableNameDisp=tNIM3_Modality','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((NIM3_Modality.isRead("SoftwareVersion",UserSecurityGroupID)))
            {
                        %>
                       <tr><td valign=top> <p class=<%=theClass%> ><b><%=NIM3_Modality.getEnglish("SoftwareVersion")%>&nbsp;</b></p></td><td valign=top><p><%=NIM3_Modality.getSoftwareVersion()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SoftwareVersion&amp;sTableName=tNIM3_Modality&amp;sRefID=<%=NIM3_Modality.getModalityID()%>&amp;sFieldNameDisp=<%=NIM3_Modality.getEnglish("SoftwareVersion")%>&amp;sTableNameDisp=tNIM3_Modality','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (NIM3_Modality.isRequired("LastServiceDate",UserSecurityGroupID))&&(!NIM3_Modality.isComplete("LastServiceDate")) )
            {
                theClass = "requiredField";
            }
            else if ((NIM3_Modality.isExpired("LastServiceDate",expiredDays))&&(NIM3_Modality.isExpiredCheck("LastServiceDate",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>

            <%
            if ((NIM3_Modality.isWrite("LastServiceDate",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>LastServiceDate&nbsp;(mm/dd/yyyy):&nbsp;</b></p></td><td valign=top><p><input onChange="document.getElementById('isSaveMe').value='1';" maxlength=10  type=text size="80" name="LastServiceDate" value='<jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(NIM3_Modality.getLastServiceDate())%>" /></jsp:include>' >&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=LastServiceDate&amp;sTableName=tNIM3_Modality&amp;sRefID=<%=NIM3_Modality.getModalityID()%>&amp;sFieldNameDisp=<%=NIM3_Modality.getEnglish("LastServiceDate")%>&amp;sTableNameDisp=tNIM3_Modality','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((NIM3_Modality.isRead("LastServiceDate",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>LastServiceDate&nbsp;(mm/dd/yyyy):&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(NIM3_Modality.getLastServiceDate())%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=LastServiceDate&amp;sTableName=tNIM3_Modality&amp;sRefID=<%=NIM3_Modality.getModalityID()%>&amp;sFieldNameDisp=<%=NIM3_Modality.getEnglish("LastServiceDate")%>&amp;sTableNameDisp=tNIM3_Modality','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>




            <%
            if ( (NIM3_Modality.isRequired("CoilWrist",UserSecurityGroupID))&&(!NIM3_Modality.isComplete("CoilWrist")) )
            {
                theClass = "requiredField";
            }
            else if ((NIM3_Modality.isExpired("CoilWrist",expiredDays))&&(NIM3_Modality.isExpiredCheck("CoilWrist",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((NIM3_Modality.isWrite("CoilWrist",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Coil Wrist&nbsp;</b></p></td><td valign=top><p><select onChange="document.getElementById('isSaveMe').value='1';"   name="CoilWrist" ><jsp:include page="../generic/tYesNoLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=NIM3_Modality.getCoilWrist()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CoilWrist&amp;sTableName=tNIM3_Modality&amp;sRefID=<%=NIM3_Modality.getModalityID()%>&amp;sFieldNameDisp=<%=NIM3_Modality.getEnglish("CoilWrist")%>&amp;sTableNameDisp=tNIM3_Modality','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((NIM3_Modality.isRead("CoilWrist",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>CoilWrist&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=NIM3_Modality.getCoilWrist()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CoilWrist&amp;sTableName=tNIM3_Modality&amp;sRefID=<%=NIM3_Modality.getModalityID()%>&amp;sFieldNameDisp=<%=NIM3_Modality.getEnglish("CoilWrist")%>&amp;sTableNameDisp=tNIM3_Modality','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (NIM3_Modality.isRequired("CoilElbow",UserSecurityGroupID))&&(!NIM3_Modality.isComplete("CoilElbow")) )
            {
                theClass = "requiredField";
            }
            else if ((NIM3_Modality.isExpired("CoilElbow",expiredDays))&&(NIM3_Modality.isExpiredCheck("CoilElbow",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((NIM3_Modality.isWrite("CoilElbow",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Coil Elbow&nbsp;</b></p></td><td valign=top><p><select onChange="document.getElementById('isSaveMe').value='1';"   name="CoilElbow" ><jsp:include page="../generic/tYesNoLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=NIM3_Modality.getCoilElbow()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CoilElbow&amp;sTableName=tNIM3_Modality&amp;sRefID=<%=NIM3_Modality.getModalityID()%>&amp;sFieldNameDisp=<%=NIM3_Modality.getEnglish("CoilElbow")%>&amp;sTableNameDisp=tNIM3_Modality','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((NIM3_Modality.isRead("CoilElbow",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>CoilElbow&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=NIM3_Modality.getCoilElbow()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CoilElbow&amp;sTableName=tNIM3_Modality&amp;sRefID=<%=NIM3_Modality.getModalityID()%>&amp;sFieldNameDisp=<%=NIM3_Modality.getEnglish("CoilElbow")%>&amp;sTableNameDisp=tNIM3_Modality','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (NIM3_Modality.isRequired("CoilKnee",UserSecurityGroupID))&&(!NIM3_Modality.isComplete("CoilKnee")) )
            {
                theClass = "requiredField";
            }
            else if ((NIM3_Modality.isExpired("CoilKnee",expiredDays))&&(NIM3_Modality.isExpiredCheck("CoilKnee",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((NIM3_Modality.isWrite("CoilKnee",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Coil Knee&nbsp;</b></p></td><td valign=top><p><select onChange="document.getElementById('isSaveMe').value='1';"   name="CoilKnee" ><jsp:include page="../generic/tYesNoLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=NIM3_Modality.getCoilKnee()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CoilKnee&amp;sTableName=tNIM3_Modality&amp;sRefID=<%=NIM3_Modality.getModalityID()%>&amp;sFieldNameDisp=<%=NIM3_Modality.getEnglish("CoilKnee")%>&amp;sTableNameDisp=tNIM3_Modality','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((NIM3_Modality.isRead("CoilKnee",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>CoilKnee&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=NIM3_Modality.getCoilKnee()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CoilKnee&amp;sTableName=tNIM3_Modality&amp;sRefID=<%=NIM3_Modality.getModalityID()%>&amp;sFieldNameDisp=<%=NIM3_Modality.getEnglish("CoilKnee")%>&amp;sTableNameDisp=tNIM3_Modality','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (NIM3_Modality.isRequired("CoilAnkle",UserSecurityGroupID))&&(!NIM3_Modality.isComplete("CoilAnkle")) )
            {
                theClass = "requiredField";
            }
            else if ((NIM3_Modality.isExpired("CoilAnkle",expiredDays))&&(NIM3_Modality.isExpiredCheck("CoilAnkle",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((NIM3_Modality.isWrite("CoilAnkle",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Coil Ankle&nbsp;</b></p></td><td valign=top><p><select onChange="document.getElementById('isSaveMe').value='1';"   name="CoilAnkle" ><jsp:include page="../generic/tYesNoLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=NIM3_Modality.getCoilAnkle()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CoilAnkle&amp;sTableName=tNIM3_Modality&amp;sRefID=<%=NIM3_Modality.getModalityID()%>&amp;sFieldNameDisp=<%=NIM3_Modality.getEnglish("CoilAnkle")%>&amp;sTableNameDisp=tNIM3_Modality','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((NIM3_Modality.isRead("CoilAnkle",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>CoilAnkle&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=NIM3_Modality.getCoilAnkle()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CoilAnkle&amp;sTableName=tNIM3_Modality&amp;sRefID=<%=NIM3_Modality.getModalityID()%>&amp;sFieldNameDisp=<%=NIM3_Modality.getEnglish("CoilAnkle")%>&amp;sTableNameDisp=tNIM3_Modality','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (NIM3_Modality.isRequired("CoilShoulder",UserSecurityGroupID))&&(!NIM3_Modality.isComplete("CoilShoulder")) )
            {
                theClass = "requiredField";
            }
            else if ((NIM3_Modality.isExpired("CoilShoulder",expiredDays))&&(NIM3_Modality.isExpiredCheck("CoilShoulder",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((NIM3_Modality.isWrite("CoilShoulder",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Coil Shoulder&nbsp;</b></p></td><td valign=top><p><select onChange="document.getElementById('isSaveMe').value='1';"   name="CoilShoulder" ><jsp:include page="../generic/tYesNoLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=NIM3_Modality.getCoilShoulder()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CoilShoulder&amp;sTableName=tNIM3_Modality&amp;sRefID=<%=NIM3_Modality.getModalityID()%>&amp;sFieldNameDisp=<%=NIM3_Modality.getEnglish("CoilShoulder")%>&amp;sTableNameDisp=tNIM3_Modality','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((NIM3_Modality.isRead("CoilShoulder",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>CoilShoulder&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=NIM3_Modality.getCoilShoulder()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CoilShoulder&amp;sTableName=tNIM3_Modality&amp;sRefID=<%=NIM3_Modality.getModalityID()%>&amp;sFieldNameDisp=<%=NIM3_Modality.getEnglish("CoilShoulder")%>&amp;sTableNameDisp=tNIM3_Modality','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (NIM3_Modality.isRequired("CoilSpine",UserSecurityGroupID))&&(!NIM3_Modality.isComplete("CoilSpine")) )
            {
                theClass = "requiredField";
            }
            else if ((NIM3_Modality.isExpired("CoilSpine",expiredDays))&&(NIM3_Modality.isExpiredCheck("CoilSpine",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((NIM3_Modality.isWrite("CoilSpine",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Coil Spine&nbsp;</b></p></td><td valign=top><p><select onChange="document.getElementById('isSaveMe').value='1';"   name="CoilSpine" ><jsp:include page="../generic/tYesNoLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=NIM3_Modality.getCoilSpine()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CoilSpine&amp;sTableName=tNIM3_Modality&amp;sRefID=<%=NIM3_Modality.getModalityID()%>&amp;sFieldNameDisp=<%=NIM3_Modality.getEnglish("CoilSpine")%>&amp;sTableNameDisp=tNIM3_Modality','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((NIM3_Modality.isRead("CoilSpine",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>CoilSpine&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=NIM3_Modality.getCoilSpine()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CoilSpine&amp;sTableName=tNIM3_Modality&amp;sRefID=<%=NIM3_Modality.getModalityID()%>&amp;sFieldNameDisp=<%=NIM3_Modality.getEnglish("CoilSpine")%>&amp;sTableNameDisp=tNIM3_Modality','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>






            <%
            if ( (NIM3_Modality.isRequired("IsACR",UserSecurityGroupID))&&(!NIM3_Modality.isComplete("IsACR")) )
            {
                theClass = "requiredField";
            }
            else if ((NIM3_Modality.isExpired("IsACR",expiredDays))&&(NIM3_Modality.isExpiredCheck("IsACR",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((NIM3_Modality.isWrite("IsACR",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Is ACR Accredited</b></p></td><td valign=top><p><select onChange="document.getElementById('isSaveMe').value='1';"   name="IsACR" ><jsp:include page="../generic/tYesNoLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=NIM3_Modality.getIsACR()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=IsACR&amp;sTableName=tNIM3_Modality&amp;sRefID=<%=NIM3_Modality.getModalityID()%>&amp;sFieldNameDisp=<%=NIM3_Modality.getEnglish("IsACR")%>&amp;sTableNameDisp=tNIM3_Modality','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((NIM3_Modality.isRead("IsACR",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>IsACR&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=NIM3_Modality.getIsACR()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=IsACR&amp;sTableName=tNIM3_Modality&amp;sRefID=<%=NIM3_Modality.getModalityID()%>&amp;sFieldNameDisp=<%=NIM3_Modality.getEnglish("IsACR")%>&amp;sTableNameDisp=tNIM3_Modality','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (NIM3_Modality.isRequired("ACRExpiration",UserSecurityGroupID))&&(!NIM3_Modality.isComplete("ACRExpiration")) )
            {
                theClass = "requiredField";
            }
            else if ((NIM3_Modality.isExpired("ACRExpiration",expiredDays))&&(NIM3_Modality.isExpiredCheck("ACRExpiration",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>

            <%
            if ((NIM3_Modality.isWrite("ACRExpiration",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>ACR Expiration&nbsp;(mm/dd/yyyy):&nbsp;</b></p></td><td valign=top><p><input onChange="document.getElementById('isSaveMe').value='1';" maxlength=10  type=text size="80" name="ACRExpiration" value='<jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(NIM3_Modality.getACRExpiration())%>" /></jsp:include>' >&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ACRExpiration&amp;sTableName=tNIM3_Modality&amp;sRefID=<%=NIM3_Modality.getModalityID()%>&amp;sFieldNameDisp=<%=NIM3_Modality.getEnglish("ACRExpiration")%>&amp;sTableNameDisp=tNIM3_Modality','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((NIM3_Modality.isRead("ACRExpiration",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>ACRExpiration&nbsp;(mm/dd/yyyy):&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(NIM3_Modality.getACRExpiration())%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ACRExpiration&amp;sTableName=tNIM3_Modality&amp;sRefID=<%=NIM3_Modality.getModalityID()%>&amp;sFieldNameDisp=<%=NIM3_Modality.getEnglish("ACRExpiration")%>&amp;sTableNameDisp=tNIM3_Modality','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (NIM3_Modality.isRequired("IsACRVerified",UserSecurityGroupID))&&(!NIM3_Modality.isComplete("IsACRVerified")) )
            {
                theClass = "requiredField";
            }
            else if ((NIM3_Modality.isExpired("IsACRVerified",expiredDays))&&(NIM3_Modality.isExpiredCheck("IsACRVerified",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((NIM3_Modality.isWrite("IsACRVerified",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Is ACR Verified&nbsp;</b></p></td><td valign=top><p><select onChange="document.getElementById('isSaveMe').value='1';"   name="IsACRVerified" ><jsp:include page="../generic/tYesNoLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=NIM3_Modality.getIsACRVerified()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=IsACRVerified&amp;sTableName=tNIM3_Modality&amp;sRefID=<%=NIM3_Modality.getModalityID()%>&amp;sFieldNameDisp=<%=NIM3_Modality.getEnglish("IsACRVerified")%>&amp;sTableNameDisp=tNIM3_Modality','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((NIM3_Modality.isRead("IsACRVerified",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>IsACRVerified&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=NIM3_Modality.getIsACRVerified()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=IsACRVerified&amp;sTableName=tNIM3_Modality&amp;sRefID=<%=NIM3_Modality.getModalityID()%>&amp;sFieldNameDisp=<%=NIM3_Modality.getEnglish("IsACRVerified")%>&amp;sTableNameDisp=tNIM3_Modality','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (NIM3_Modality.isRequired("ACRVerifiedDate",UserSecurityGroupID))&&(!NIM3_Modality.isComplete("ACRVerifiedDate")) )
            {
                theClass = "requiredField";
            }
            else if ((NIM3_Modality.isExpired("ACRVerifiedDate",expiredDays))&&(NIM3_Modality.isExpiredCheck("ACRVerifiedDate",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>

            <%
            if ((NIM3_Modality.isWrite("ACRVerifiedDate",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>ACR Verified Date&nbsp;(mm/dd/yyyy):&nbsp;</b></p></td><td valign=top><p><input onChange="document.getElementById('isSaveMe').value='1';" maxlength=10  type=text size="80" name="ACRVerifiedDate" value='<jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(NIM3_Modality.getACRVerifiedDate())%>" /></jsp:include>' >&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ACRVerifiedDate&amp;sTableName=tNIM3_Modality&amp;sRefID=<%=NIM3_Modality.getModalityID()%>&amp;sFieldNameDisp=<%=NIM3_Modality.getEnglish("ACRVerifiedDate")%>&amp;sTableNameDisp=tNIM3_Modality','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((NIM3_Modality.isRead("ACRVerifiedDate",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>ACRVerifiedDate&nbsp;(mm/dd/yyyy):&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(NIM3_Modality.getACRVerifiedDate())%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ACRVerifiedDate&amp;sTableName=tNIM3_Modality&amp;sRefID=<%=NIM3_Modality.getModalityID()%>&amp;sFieldNameDisp=<%=NIM3_Modality.getEnglish("ACRVerifiedDate")%>&amp;sTableNameDisp=tNIM3_Modality','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (NIM3_Modality.isRequired("Comments",UserSecurityGroupID))&&(!NIM3_Modality.isComplete("Comments")) )
            {
                theClass = "requiredField";
            }
            else if ((NIM3_Modality.isExpired("Comments",expiredDays))&&(NIM3_Modality.isExpiredCheck("Comments",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((NIM3_Modality.isWrite("Comments",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Comments&nbsp;</b></p></td><td valign=top><p><input onChange="document.getElementById('isSaveMe').value='1';" maxlength="100" type=text size="80" name="Comments" value="<%=NIM3_Modality.getComments()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Comments&amp;sTableName=tNIM3_Modality&amp;sRefID=<%=NIM3_Modality.getModalityID()%>&amp;sFieldNameDisp=<%=NIM3_Modality.getEnglish("Comments")%>&amp;sTableNameDisp=tNIM3_Modality','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((NIM3_Modality.isRead("Comments",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Comments&nbsp;</b></p></td><td valign=top><p><%=NIM3_Modality.getComments()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Comments&amp;sTableName=tNIM3_Modality&amp;sRefID=<%=NIM3_Modality.getModalityID()%>&amp;sFieldNameDisp=<%=NIM3_Modality.getEnglish("Comments")%>&amp;sTableNameDisp=tNIM3_Modality','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>




            <tr><td width=40%>&nbsp;</td><td width=60%>&nbsp;</td></tr>
            </table>
        <%if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {%>
            <input onChange="document.getElementById('isSaveMe').value='1';" type=hidden name=routePageReference value="sParentReturnPage">
             <%
              if (request.getParameter("INTNext")!=null&&request.getParameter("INTNext").equalsIgnoreCase("yes") ) 
              {
              %>
                  <table width=75% border=1 bordercolor=333333 align=left cellspacing=0 cellpadding=0 class=wizardTable>
                  <tr class=requiredField><td>
                  <input onChange="document.getElementById('isSaveMe').value='1';"  <%=HTMLFormStyleButton%> type="radio" value="next" name="INTNext" checked>&nbsp;&nbsp;<%=ConfigurationMessages.getInterviewMessage("INTERVIEWNoMore","tNIM3_Modality")%>
                  <br>
                  <input onChange="document.getElementById('isSaveMe').value='1';"  <%=HTMLFormStyleButton%> type="radio" value="yes" name="INTNext">&nbsp;&nbsp;<%=ConfigurationMessages.getInterviewMessage("INTERVIEWAddMore","tNIM3_Modality")%>
                  </td></tr></table><br><br><br>
              <%
              }
              %>
            <p><input  class="tdBaseAltGreen" onClick = "document.getElementById('isSaveMe').value='0';this.disabled=true;submit();" type=Submit value="Continue" name=Submit></p>
        <%}%>
        </td></tr></table>
        </form>
        <%
  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>

    </td></tr></table>


