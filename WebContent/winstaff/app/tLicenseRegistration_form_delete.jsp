<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.bltDocumentManagement,java.util.Vector,java.util.Enumeration,com.winstaff.ListElement, com.winstaff.PLCUtils, com.winstaff.SecurityCheck, com.winstaff.ConfigurationMessages,com.winstaff.bltLicenseRegistration,com.winstaff.DocumentManagerUtils" %>
<%/*
    filename: out\jsp\tLicenseRegistration_form_delete.jsp
    Created on Apr/10/2003
    Type: _form Delete
    Created by: Scott Ellis
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>


<%

//initial declaration of list class and parentID
    Integer        iLicenseRegistrationID        =    null;
    boolean accessValid = false;

   Integer iSecurityCheck = SecurityCheck.CheckItem("PractitionerSection3", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {

    if (pageControllerHash.containsKey("iLicenseRegistrationID")) 
    {
        iLicenseRegistrationID        =    (Integer)pageControllerHash.get("iLicenseRegistrationID");
        if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {
            accessValid = true;
        }
    }
  //page security
  if (accessValid)
  {

//initial declaration of list class and parentID

    bltLicenseRegistration        LicenseRegistration        =    null;

    if ( request.getParameter( "EDIT" ).equalsIgnoreCase("del") )
    {
        LicenseRegistration        =    new    bltLicenseRegistration(iLicenseRegistrationID);
        LicenseRegistration.setPhysicianID(new Integer("0"));
        //if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {
            LicenseRegistration.commitData();
            DocumentManagerUtils.ArchiveDocument("tLicenseRegistration", LicenseRegistration.getDocuLinkID() , 1);
            out.println("Successful delete");
        }
    }
String routePageReference = request.getParameter("routePageReference");
String nextPage=null;
if (routePageReference!=null)
{
        if (pageControllerHash.containsKey(routePageReference))
        {
            nextPage = (String)pageControllerHash.get(routePageReference);
        }
        else if (pageControllerHash.containsKey("sLocalChildReturnPage"))
        {
            nextPage = (String)pageControllerHash.get("sLocalChildReturnPage");
        }
}
if (nextPage!=null)
{
    response.sendRedirect(nextPage+"?EDIT=edit");
}
        %>
Successful Delete

  <%
  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
  %>


