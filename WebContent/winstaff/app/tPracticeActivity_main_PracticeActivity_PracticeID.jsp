<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.*" %>
<%/*
    filename: tPracticeActivity_main_PracticeActivity_PracticeID.jsp
    Created on Oct/30/2009
    Type: 1-n main class file
    Created by: Scott Ellis
    Version: 4.0
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>

<link href="ui_1/style.css" rel="stylesheet" type="text/css" />

<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_PracticeID_nim.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>

    <table cellpadding=0 cellspacing=0 border=0 width=100% >
    <tr><td width=10>&nbsp;</td><td> <p class=title>Practice Activities</p>

    <%//ConfigurationMessages.getHTML("INTERVIEWTopControl","tPracticeActivity")%>

<%
//initial declaration of list class and parentID
    Integer        iPracticeID        =    null;
    boolean accessValid = false;
   Integer iSecurityCheck = SecurityCheck.CheckItem("nim1", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {
    if (pageControllerHash.containsKey("iPracticeID")) 
    {
        iPracticeID        =    (Integer)pageControllerHash.get("iPracticeID");
		//out.print(iPracticeID);
        accessValid = true;
    }
  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);
      pageControllerHash.put("sParentReturnPage","tPracticeActivity_main_PracticeActivity_PracticeID.jsp");
    pageControllerHash.remove("iPracticeActivityID");
    pageControllerHash.put("sINTNext","tPracticeActivity_main_PracticeActivity_PracticeID_form_create.jsp?EDIT=new&KM=p&INTNext=yes");
    session.setAttribute("pageControllerHash",pageControllerHash);

    bltPracticeActivity_List        bltPracticeActivity_List        =    new    bltPracticeActivity_List(iPracticeID,"","uniquecreatedate desc");

//declaration of Enumeration
    bltPracticeActivity        working_bltPracticeActivity;
    ListElement         leCurrentElement;
    java.util.Enumeration eList = bltPracticeActivity_List.elements();
    %>
        <%@ include file="tPracticeActivity_main_PracticeActivity_PracticeID_instructions.jsp" %>

        <%if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {%>
        <input type=button class="tdBaseAltGreen"  onClick = "this.disabled=true;document.location ='tPracticeActivity_main_PracticeActivity_PracticeID_form_create.jsp?EDIT=new&KM=p&INTNext=yes'" value="Create">
        <%}%>
         <table border="1" bordercolor="CCCCCC" cellpadding="3" class=tdBase cellspacing="0" width="100%">
    <%
    int altCnt = 0;
    if (eList.hasMoreElements())
    {
     while (eList.hasMoreElements())
     {

        altCnt++;
        String theClass = "tdBase";
        if (altCnt%2!=0)
        {
            theClass = "tdBaseAlt";
        }
        leCurrentElement    = (ListElement) eList.nextElement();
        working_bltPracticeActivity  = (bltPracticeActivity) leCurrentElement.getObject();
        working_bltPracticeActivity.GroupSecurityInit(UserSecurityGroupID);
        if (!working_bltPracticeActivity.isComplete())
        {
            theClass = "incompleteItem";
        %>

        <%
        }
        else
        {
        %>
        <%
        }
        %>
        <tr class=<%=theClass%> > 

              <td><b>Item ID:&nbsp;</b><%=working_bltPracticeActivity.getPracticeActivityID()%><br>Created: <jsp:include page="../generic/DateTypeConvert_Main.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(working_bltPracticeActivity.getUniqueCreateDate())%>" /></jsp:include><br>Modified: <jsp:include page="../generic/DateTypeConvert_Main.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(working_bltPracticeActivity.getUniqueModifyDate())%>" /></jsp:include></td>
<%String theClassF = "textBase";%>

 <td class=title>
 	<%=working_bltPracticeActivity.getName()%> <br />
 Type:&nbsp;<strong><%=working_bltPracticeActivity.getActivityType()%></strong>
 </td>
 <td>
           
            Completed:&nbsp;<strong><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltPracticeActivity.getisCompleted()%>" /></jsp:include></strong> <br />


                       Start:&nbsp;
                       <strong><jsp:include page="../generic/DateTypeConvert_Main.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(working_bltPracticeActivity.getStartDate())%>" /></jsp:include></strong>
                   &nbsp;&nbsp;    End:&nbsp;
                       <strong><jsp:include page="../generic/DateTypeConvert_Main.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(working_bltPracticeActivity.getEndDate())%>" /></jsp:include></strong>
<br />
                       Reminder:&nbsp;
                       <strong><jsp:include page="../generic/DateTypeConvert_Main.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(working_bltPracticeActivity.getRemindDate())%>" /></jsp:include></strong>
 </td>

            <td > 
        <input class="tdBaseAltGreen" type=button onClick = "this.disabled=true;document.location ='tPracticeActivity_main_PracticeActivity_PracticeID_form_authorize.jsp?EDIT=edit&EDITID=<%=working_bltPracticeActivity.getPracticeActivityID()%>&KM=p'" value="Edit">&nbsp;    
<%
if (working_bltPracticeActivity.getDocuLinkID()>0)
{
	%>
<input class="tdBaseAltGreen" type=button onclick = "this.disabled=true;document.location ='tPracticeActivity_main_PracticeActivity_PracticeID_form_authorize.jsp?EDIT=viewdoc&EDITID=<%=working_bltPracticeActivity.getPracticeActivityID()%>&KM=p';this.disabled=false" value="View Attached Doc" />
&nbsp;    
<%
}
%>


&nbsp;&nbsp;&nbsp;    
        <%if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {%>
        <input type=button class="tdBaseAltRed"  onClick = "confirmDelete2('','tPracticeActivity_main_PracticeActivity_PracticeID_form_authorize.jsp?EDIT=del&EDITID=<%=working_bltPracticeActivity.getPracticeActivityID()%>&KM=p',this)" value="Delete">
<% }%>
                  </td></tr>
        <%
    }//end while
       }//end of if
       else 
       {
           %>
           <tr><td colspan=5><b>Please click the "create" to add <%=ConfigurationMessages.getDataCategory("tPracticeActivity")%> information or click 'Continue' to go to the next section.</b>
           <script language=javascript>
           if (false&&confirm("<%=ConfigurationMessages.getInterviewMessage("INTERVIEWNoElements","tPracticeActivity")%>"))
           {
               document.location="tPracticeActivity_main_PracticeActivity_PracticeID_form_create.jsp?EDIT=new&KM=p&INTNext=yes"; 
           }
           else
           {

           }
           </script>
           </td></tr>
           <%
       }
    %>

    </table>
    <%
  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>

    </td></tr></table><br>

