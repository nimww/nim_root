<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.bltDocumentManagement,java.util.Vector,java.util.Enumeration,com.winstaff.ListElement, com.winstaff.PLCUtils, com.winstaff.SecurityCheck, com.winstaff.ConfigurationMessages,com.winstaff.bltLicenseRegistration,com.winstaff.DocumentManagerUtils" %>
<%/*
    filename: out\jsp\tLicenseRegistration_form_sub.jsp
    Created on Sep/11/2003
    Created by: Scott Ellis
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>


<%

//initial declaration of list class and parentID
    Integer        iLicenseRegistrationID        =    null;
    boolean accessValid = false;

   Integer iSecurityCheck = SecurityCheck.CheckItem("PractitionerSection3", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {

    if (pageControllerHash.containsKey("iLicenseRegistrationID")) 
    {
        iLicenseRegistrationID        =    (Integer)pageControllerHash.get("iLicenseRegistrationID");
        if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {
            accessValid = true;
        }
    }
  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);

//initial declaration of list class and parentID

    bltLicenseRegistration        LicenseRegistration        =    null;

    if ( request.getParameter( "EDIT" ).equalsIgnoreCase("edit") )
    {
        LicenseRegistration        =    new    bltLicenseRegistration(iLicenseRegistrationID,UserSecurityGroupID);
    }
    else if ( request.getParameter( "EDIT" ).equalsIgnoreCase("new") )
    {
        LicenseRegistration        =    new    bltLicenseRegistration(UserSecurityGroupID,true);
    }

String testChangeID = "0";

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("UniqueCreateDate"))) ;
    if ( !LicenseRegistration.getUniqueCreateDate().equals(testObj)  )
    {
         LicenseRegistration.setUniqueCreateDate( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("LicenseRegistration UniqueCreateDate not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("UniqueModifyDate"))) ;
    if ( !LicenseRegistration.getUniqueModifyDate().equals(testObj)  )
    {
         LicenseRegistration.setUniqueModifyDate( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("LicenseRegistration UniqueModifyDate not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("UniqueModifyComments")) ;
    if ( !LicenseRegistration.getUniqueModifyComments().equals(testObj)  )
    {
         LicenseRegistration.setUniqueModifyComments( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("LicenseRegistration UniqueModifyComments not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("PhysicianID")) ;
    if ( !LicenseRegistration.getPhysicianID().equals(testObj)  )
    {
         LicenseRegistration.setPhysicianID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("LicenseRegistration PhysicianID not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("IsCurrent")) ;
    if ( !LicenseRegistration.getIsCurrent().equals(testObj)  )
    {
         LicenseRegistration.setIsCurrent( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("LicenseRegistration IsCurrent not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("LicenseType")) ;
    if ( !LicenseRegistration.getLicenseType().equals(testObj)  )
    {
         LicenseRegistration.setLicenseType( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("LicenseRegistration LicenseType not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("SubType")) ;
    if ( !LicenseRegistration.getSubType().equals(testObj)  )
    {
         LicenseRegistration.setSubType( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("LicenseRegistration SubType not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("NPDBFOL")) ;
    if ( !LicenseRegistration.getNPDBFOL().equals(testObj)  )
    {
         LicenseRegistration.setNPDBFOL( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("LicenseRegistration NPDBFOL not set. this is ok-not an error");
}


try
{
    Integer testObj = new Integer(request.getParameter("StateID")) ;
    if ( !LicenseRegistration.getStateID().equals(testObj)  )
    {
         LicenseRegistration.setStateID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("LicenseRegistration StateID not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("IsPractice")) ;
    if ( !LicenseRegistration.getIsPractice().equals(testObj)  )
    {
         LicenseRegistration.setIsPractice( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("LicenseRegistration IsPractice not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("LicenseDocumentNumber")) ;
    if ( !LicenseRegistration.getLicenseDocumentNumber().equals(testObj)  )
    {
         LicenseRegistration.setLicenseDocumentNumber( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("LicenseRegistration LicenseDocumentNumber not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("IssueDate"))) ;
    if ( !LicenseRegistration.getIssueDate().equals(testObj)  )
    {
         LicenseRegistration.setIssueDate( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("LicenseRegistration IssueDate not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("ExpirationDate"))) ;
    if ( !LicenseRegistration.getExpirationDate().equals(testObj)  )
    {
         LicenseRegistration.setExpirationDate( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("LicenseRegistration ExpirationDate not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("Name")) ;
    if ( !LicenseRegistration.getName().equals(testObj)  )
    {
         LicenseRegistration.setName( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("LicenseRegistration Name not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("Address1")) ;
    if ( !LicenseRegistration.getAddress1().equals(testObj)  )
    {
         LicenseRegistration.setAddress1( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("LicenseRegistration Address1 not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("Address2")) ;
    if ( !LicenseRegistration.getAddress2().equals(testObj)  )
    {
         LicenseRegistration.setAddress2( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("LicenseRegistration Address2 not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("City")) ;
    if ( !LicenseRegistration.getCity().equals(testObj)  )
    {
         LicenseRegistration.setCity( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("LicenseRegistration City not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("OrgStateID")) ;
    if ( !LicenseRegistration.getOrgStateID().equals(testObj)  )
    {
         LicenseRegistration.setOrgStateID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("LicenseRegistration OrgStateID not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("Province")) ;
    if ( !LicenseRegistration.getProvince().equals(testObj)  )
    {
         LicenseRegistration.setProvince( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("LicenseRegistration Province not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("ZIP")) ;
    if ( !LicenseRegistration.getZIP().equals(testObj)  )
    {
         LicenseRegistration.setZIP( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("LicenseRegistration ZIP not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("CountryID")) ;
    if ( !LicenseRegistration.getCountryID().equals(testObj)  )
    {
         LicenseRegistration.setCountryID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("LicenseRegistration CountryID not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("Phone")) ;
    if ( !LicenseRegistration.getPhone().equals(testObj)  )
    {
         LicenseRegistration.setPhone( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("LicenseRegistration Phone not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("Fax")) ;
    if ( !LicenseRegistration.getFax().equals(testObj)  )
    {
         LicenseRegistration.setFax( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("LicenseRegistration Fax not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("ContactName")) ;
    if ( !LicenseRegistration.getContactName().equals(testObj)  )
    {
         LicenseRegistration.setContactName( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("LicenseRegistration ContactName not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("ContactEmail")) ;
    if ( !LicenseRegistration.getContactEmail().equals(testObj)  )
    {
         LicenseRegistration.setContactEmail( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("LicenseRegistration ContactEmail not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("DocuLinkID")) ;
    if ( !LicenseRegistration.getDocuLinkID().equals(testObj)  )
    {
         LicenseRegistration.setDocuLinkID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("LicenseRegistration DocuLinkID not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("Comments")) ;
    if ( !LicenseRegistration.getComments().equals(testObj)  )
    {
         LicenseRegistration.setComments( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("LicenseRegistration Comments not set. this is ok-not an error");
}

boolean bINT = false;
boolean bNextStep = false;
String sRefreshDoc = "refreshMe=true&";
              if (request.getParameter("INTNext")!=null&&request.getParameter("INTNext").equalsIgnoreCase("yes") ) 
	            {
	                bINT = true;
	                sRefreshDoc = "";
	            }
              else if (request.getParameter("INTNext")!=null&&request.getParameter("INTNext").equalsIgnoreCase("next") ) 
	            {
	                bINT = true;
	                sINTNext = ConfigurationMessages.getInterviewLinkRaw("tLicenseRegistration","next");
	                sRefreshDoc = "";
	            }

// If an edit, update information; if new, append information
boolean errorRouteMe = false;
if (testChangeID.equalsIgnoreCase("1"))
{

	LicenseRegistration.setUniqueModifyDate(new java.util.Date());
	String UserLogonDescription = "n/a";
	if (pageControllerHash.containsKey("UserLogonDescription"))
	{
	    UserLogonDescription = (String)pageControllerHash.get("UserLogonDescription");
	}
    //if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
    {
	    LicenseRegistration.setUniqueModifyComments(UserLogonDescription);
	    try
	    {
	        LicenseRegistration.commitData();
	    }
	    catch (Exception e55)
	    {
	        errorRouteMe = true;
	    }	//Doculink Addition
	//Doculink Does Exist, prompt to Modify
	Integer iPhysicianID = LicenseRegistration.getPhysicianID();
     if (!errorRouteMe&&LicenseRegistration.getDocuLinkID().intValue()>0)
     {
            bltDocumentManagement myDoc = new bltDocumentManagement(LicenseRegistration.getDocuLinkID());
            if (myDoc.getDocumentFileName().equalsIgnoreCase("")) 
            {
                DocumentManagerUtils myDUtils = new DocumentManagerUtils();
                String myDType = "tLicenseRegistration";
                myDUtils.setDocumentType(myDType, LicenseRegistration.getUniqueID());
                myDoc.setDocumentName(myDUtils.getDocumentName());
                myDoc.setDateOfExpiration(myDUtils.getDateOfExpiration());
                myDoc.setDocumentTypeID(myDUtils.getDocumentTypeID());
                myDoc.setPhysicianID(iPhysicianID);
                myDoc.setArchived(new Integer(2));
                myDoc.commitData();
            }
            else
            {
                //archive then create new
                myDoc.setArchived(new Integer("1"));
                myDoc.commitData();
                myDoc = new bltDocumentManagement();
                DocumentManagerUtils myDUtils = new DocumentManagerUtils();
                String myDType = "tLicenseRegistration";
                myDUtils.setDocumentType(myDType, LicenseRegistration.getUniqueID());
                myDoc.setDocumentName(myDUtils.getDocumentName());
                myDoc.setDateOfExpiration(myDUtils.getDateOfExpiration());
                myDoc.setDocumentTypeID(myDUtils.getDocumentTypeID());
                myDoc.setPhysicianID(iPhysicianID);
                myDoc.setArchived(new Integer(2));
                myDoc.commitData();
                LicenseRegistration.setDocuLinkID(myDoc.getUniqueID());
                LicenseRegistration.commitData();
            }
     }
     else if (!errorRouteMe)
     {
            bltDocumentManagement myDoc = new bltDocumentManagement();
            DocumentManagerUtils myDUtils = new DocumentManagerUtils();
            String myDType = "tLicenseRegistration";
            myDUtils.setDocumentType(myDType, LicenseRegistration.getUniqueID());
            myDoc.setDocumentName(myDUtils.getDocumentName());
            myDoc.setDateOfExpiration(myDUtils.getDateOfExpiration());
            myDoc.setDocumentTypeID(myDUtils.getDocumentTypeID());
                myDoc.setPhysicianID(iPhysicianID);
            myDoc.setArchived(new Integer(2));
            myDoc.commitData();
            LicenseRegistration.setDocuLinkID(myDoc.getUniqueID());
            LicenseRegistration.commitData();
     }

    }
}
String routePageReference = request.getParameter("routePageReference");
String nextPage=null;
if (routePageReference!=null)
{
        if (pageControllerHash.containsKey(routePageReference))
        {
            nextPage = (String)pageControllerHash.get(routePageReference);
        }
        else if (pageControllerHash.containsKey("sLocalChildReturnPage"))
        {
            nextPage = (String)pageControllerHash.get("sLocalChildReturnPage");
        }
        else if (pageControllerHash.containsKey("sParentReturnPage"))
        {
            nextPage = (String)pageControllerHash.get("sParentReturnPage");
        }
}
              if (bINT ) 
	            {
	                nextPage = sINTNext;
	                if (nextPage==null||nextPage.equalsIgnoreCase("")||nextPage.equalsIgnoreCase("#"))
	                {
	                    nextPage = (String)pageControllerHash.get("sLocalChildReturnPage");
	                }
	            }
if (errorRouteMe)
{
	out.println(ConfigurationMessages.getMessage("FatalError")); 
}
else if (nextPage!=null)
{
	    %><script language=Javascript>
	      document.location="<%=nextPage%>";
	      </script><%
    //response.sendRedirect(nextPage+"?EDIT=edit");
}
        %>


  <%
  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
  %>
