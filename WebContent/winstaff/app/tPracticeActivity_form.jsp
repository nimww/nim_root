<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.*" %>
<%/*

    filename: out\jsp\tPracticeActivity_form.jsp
    Created on Oct/30/2009
    Created by: Scott Ellis
    Version: 4.0
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>



<link href="ui_1/style.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="js/anytime/anytime.css" />
    <script type="text/javascript" src="js/prototype/prototype-1.6.0.3.js" ></script>
    <script type="text/javascript" src="js/anytime/anytime.js" ></script>




<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_PracticeID_nim.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>

<!--<body onUnload="if (document.getElementById('isSaveMe').value=='1'&&confirm('Want to save first?')){document.getElementById('tPracticeActivity_form1').submit();}">-->

    <table cellpadding=0 cellspacing=0 border=0 width=100% >
    <tr><td width=10>&nbsp;</td><td>
    <%//ConfigurationMessages.getHTML("INTERVIEWTopControl_form","tPracticeActivity")%>



<%
//initial declaration of list class and parentID
    Integer        iPracticeActivityID        =    null;
    Integer        iEDITID        =    null;
    if ( request.getParameter( "EDITID" ) != null )
    {
    	iEDITID        =    new Integer(request.getParameter ("EDITID"));
    }
    else
    {
    	iEDITID        =    new Integer(0);
    }
    boolean accessValid = false;
    // required for Type2
    String sKeyMasterReference = null;

   Integer iSecurityCheck = SecurityCheck.CheckItem("nim1", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {
    if (pageControllerHash.containsKey("iPracticeActivityID")) 
    {
        iPracticeActivityID        =    (Integer)pageControllerHash.get("iPracticeActivityID");
        accessValid = true;
        if (iPracticeActivityID.intValue() != iEDITID.intValue())
        {
        	accessValid = false;
        }
    }
  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);      java.text.SimpleDateFormat displayDateTimeSDF = new java.text.SimpleDateFormat("MM/dd/yyyy hh:mm a");

    pageControllerHash.put("sLocalChildReturnPage","tPracticeActivity_form.jsp?EDIT=edit&EDITID=" + iEDITID);
//initial declaration of list class and parentID

    bltPracticeActivity        PracticeActivity        =    null;

    if ( request.getParameter( "EDIT" ).equalsIgnoreCase("edit") )
    {
        PracticeActivity        =    new    bltPracticeActivity(iPracticeActivityID,UserSecurityGroupID);
    }
    else if ( request.getParameter( "EDIT" ).equalsIgnoreCase("new") )
    {
        PracticeActivity        =    new    bltPracticeActivity(UserSecurityGroupID, true);
    }
//document.getElementById('Summary').innerHTML = escape(document.getElementById('Summary').innerHTML);
//fields
        %>
        <%@ include file="tPracticeActivity_form_instructions.jsp" %>
        <form action="tPracticeActivity_form_sub.jsp" name="tPracticeActivity_form1"  id="tPracticeActivity_form1" method="POST"  onsubmit="document.getElementById('isSaveMe').value='0';" >
        <input onChange="document.getElementById('isSaveMe').value='1';" type="hidden" name="isSaveMe" id="isSaveMe" value="0" />
<%
    if ( request.getParameter( "EDIT" ) != null )
    {
    %>
        <input onChange="document.getElementById('isSaveMe').value='1';" type="hidden" name="EDIT" value = "<%=request.getParameter( "EDIT" )%>" >  
    <%
    }
    %>
        <input onChange="document.getElementById('isSaveMe').value='1';" type="hidden" name="EDITID" value = "<%=iEDITID%>" >  

          <%  String theClass ="tdBase";%>
        <table width=100% border=1 bordercolor=#333333 cellpadding=3 cellspacing=0 class=tableBase>
         <tr><td class=tableColor>


            <table cellpadding=0 cellspacing=0 width=100%>
                     <tr class="tdBaseAlt">
                       <td colspan="2" valign=top><table width="100%" border="0" cellpadding="5" cellspacing="0">
                         <tr>
                           <td valign="top"><p class="<%=theClass%>" ><b> Completed&nbsp;</b></p></td>
                           <td valign="top"><p>
                             <select  class="tdBaseAltYellow"  onchange="document.getElementById('isSaveMe').value='1';"   name="isCompleted" >
                               <jsp:include page="../generic/tYesNoLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PracticeActivity.getisCompleted()%>" /></jsp:include>
                             </select>
                             &nbsp;
                             <%if (isShowAudit){%>
                             <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=isCompleted&amp;sTableName=tPracticeActivity&amp;sRefID=<%=PracticeActivity.getPracticeActivityID()%>&amp;sFieldNameDisp=<%=PracticeActivity.getEnglish("isCompleted")%>&amp;sTableNameDisp=tPracticeActivity','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align="middle" border="0" src="images/icon_audit.gif" /></a>
                             <%}%>
                           </p></td>
                           <td valign="top" nowrap="nowrap"><p class="<%=theClass%>" ><b>Start</b></p></td>
                           <td valign="top"><p>
                             <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="10"  class="tdBaseAltGrey"  type="text" size="20" name="StartDate"  id="StartDate" value='<jsp:include page="../generic/DateTypeConvertDT_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(PracticeActivity.getStartDate())%>" /></jsp:include>' />
                             <script type="text/javascript">
    new ATWidget( "StartDate", { format: "%m/%d/%Y %h:%i %p" } );
                           </script>
                             &nbsp;
                             <%if (isShowAudit){%>
                             <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=StartDate&amp;sTableName=tPracticeActivity&amp;sRefID=<%=PracticeActivity.getPracticeActivityID()%>&amp;sFieldNameDisp=<%=PracticeActivity.getEnglish("StartDate")%>&amp;sTableNameDisp=tPracticeActivity','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align="middle" border="0" src="images/icon_audit.gif" /></a>
                             <%}%>
                           </p></td>
                           <td valign="top" nowrap="nowrap"><p class="<%=theClass%>" ><b>End</b></p></td>
                           <td valign="top"><p>
                             <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="10"  class="tdBaseAltGrey"  type="text" size="20" name="EndDate"  id="EndDate" value='<jsp:include page="../generic/DateTypeConvertDT_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(PracticeActivity.getEndDate())%>" /></jsp:include>' />
                             <script type="text/javascript">
    new ATWidget( "EndDate", { format: "%m/%d/%Y %h:%i %p" } );
                           </script>
                             &nbsp;
                             <%if (isShowAudit){%>
                             <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=EndDate&amp;sTableName=tPracticeActivity&amp;sRefID=<%=PracticeActivity.getPracticeActivityID()%>&amp;sFieldNameDisp=<%=PracticeActivity.getEnglish("EndDate")%>&amp;sTableNameDisp=tPracticeActivity','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align="middle" border="0" src="images/icon_audit.gif" /></a>
                             <%}%>
                           </p></td>
                           <td valign="top" nowrap="nowrap"><p class="<%=theClass%>" ><b>Reminder</b></p></td>
                           <td valign="top" nowrap="nowrap"><p>
                             <input onChange="document.getElementById('isSaveMe').value='1';" class="tdBaseAltGreen"  maxlength="10"  type="text" size="20" name="RemindDate" id="RemindDate" value='<jsp:include page="../generic/DateTypeConvertDT_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(PracticeActivity.getRemindDate())%>" /></jsp:include>' />
                             <script type="text/javascript">
    new ATWidget( "RemindDate", { format: "%m/%d/%Y %h:%i %p" } );
                           </script>
                             &nbsp;
                             <%if (isShowAudit){%>
                             <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=RemindDate&amp;sTableName=tPracticeActivity&amp;sRefID=<%=PracticeActivity.getPracticeActivityID()%>&amp;sFieldNameDisp=<%=PracticeActivity.getEnglish("RemindDate")%>&amp;sTableNameDisp=tPracticeActivity','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align="middle" border="0" src="images/icon_audit.gif" /></a>
                             <%}%>
                           </p></td>
                         </tr>
                       </table></td>
                     </tr>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Assigned To:&nbsp;</b></p></td><td valign=top><p><select class="tdBaseAltYellow" onchange="document.getElementById('isSaveMe').value='1';"   name="AssignedToID" ><jsp:include page="../generic/tAssignTo_genAdminID.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PracticeActivity.getAssignedToID()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=AssignedToID&amp;sTableName=tPracticeActivity&amp;sRefID=<%=PracticeActivity.getPracticeActivityID()%>&amp;sFieldNameDisp=<%=PracticeActivity.getEnglish("AssignedToID")%>&amp;sTableNameDisp=tPracticeActivity','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>

                     <tr><td valign=top><p class=<%=theClass%> ><b>Subject</b></p></td><td valign=top><p><input onChange="document.getElementById('isSaveMe').value='1';" maxlength="100" type=text size="60" class="tdHeader" name="Name" value="<%=PracticeActivity.getName()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Name&amp;sTableName=tPracticeActivity&amp;sRefID=<%=PracticeActivity.getPracticeActivityID()%>&amp;sFieldNameDisp=<%=PracticeActivity.getEnglish("Name")%>&amp;sTableNameDisp=tPracticeActivity','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                     <tr>
                       <td valign="top"><p class="<%=theClass%>" ><b>Activity Type&nbsp;</b></p></td>
                       <td valign="top"><p>
                         <input onChange="document.getElementById('isSaveMe').value='1';" name="ActivityType" type="text"  id="ActivityType" value="<%=PracticeActivity.getActivityType()%>" size="30" maxlength="20" readonly="readonly" />
                         &nbsp;
                         <%if (isShowAudit){%>
                         <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ActivityType&amp;sTableName=tPracticeActivity&amp;sRefID=<%=PracticeActivity.getPracticeActivityID()%>&amp;sFieldNameDisp=<%=PracticeActivity.getEnglish("ActivityType")%>&amp;sTableNameDisp=tPracticeActivity','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align="middle" border="0" src="images/icon_audit.gif" /></a>
                         <%}%>
                      <input onChange="document.getElementById('isSaveMe').value='1';" class="tdBaseAltYellow" name="none1" type="button" id="none1" onClick="document.getElementById('ActivityType').value = 'Phone';" value="Phone">&nbsp;&nbsp;<input onChange="document.getElementById('isSaveMe').value='1';"  class="tdBaseAltYellow" name="none1" type="button" id="none1" onClick="document.getElementById('ActivityType').value = 'Email';" value="Email">&nbsp;&nbsp;<input onChange="document.getElementById('isSaveMe').value='1';"  class="tdBaseAltYellow" name="none1" type="button" id="none1" onClick="document.getElementById('ActivityType').value = 'Contract';document.getElementById('AttachDoc').value=1;" value="Contract">&nbsp;&nbsp;<input onChange="document.getElementById('isSaveMe').value='1';"  class="tdBaseAltYellow" name="none1" type="button" id="none1" onClick="document.getElementById('ActivityType').value = 'Document';document.getElementById('AttachDoc').value=1;" value="Document">&nbsp;&nbsp; </p></td>
                     </tr>
                     <tr>
                       <td valign="top"><p class="<%=theClass%>" ><b>Description&nbsp;</b></p></td>
                       <td valign="top"><p>
                         <input name="Description" type="text" id="Description" onChange="document.getElementById('isSaveMe').value='1';" value="<%=PracticeActivity.getDescription()%>" size="80" maxlength="200" />
                         &nbsp;
                         <%if (isShowAudit){%>
                         <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Description&amp;sTableName=tPracticeActivity&amp;sRefID=<%=PracticeActivity.getPracticeActivityID()%>&amp;sFieldNameDisp=<%=PracticeActivity.getEnglish("Description")%>&amp;sTableNameDisp=tPracticeActivity','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align="middle" border="0" src="images/icon_audit.gif" /></a>
                         <%}%>
                       </p></td>
                     </tr>
                     <tr>
                       <td valign="top"><p class="<%=theClass%>" ><b>Summary Notes&nbsp;</b><br /> <input onChange="document.getElementById('isSaveMe').value='1';" name="none1" type="button" id="none1" onClick="document.getElementById('Summary').value += '\n[' + Date() + ' - <%=CurrentUserAccount.getLogonUserName()%>]\n';" value="Add Date/Time Stamp">
                       </p></td>
                       <td valign="top"><p>
                         <textarea  onchange="document.getElementById('isSaveMe').value='1';"  onkeydown="textAreaStop(this,5000)" rows="10" name="Summary"  id="Summary" cols="50"><%=PracticeActivity.getSummary()%></textarea>
                         &nbsp;
                         <%if (isShowAudit){%>
                         <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Summary&amp;sTableName=tPracticeActivity&amp;sRefID=<%=PracticeActivity.getPracticeActivityID()%>&amp;sFieldNameDisp=<%=PracticeActivity.getEnglish("Summary")%>&amp;sTableNameDisp=tPracticeActivity','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align="middle" border="0" src="images/icon_audit.gif" /></a>
                         <%}%>
                       </p></td>
                     </tr>

<% 
if (false)
{
%>
<tr>
                       <td valign=top>&nbsp;</td>
                       <td valign=top>&nbsp;</td>
                     </tr>
                     <tr><td valign=top><p class=<%=theClass%> ><b>File Reference&nbsp;</b></p></td><td valign=top><p><input onChange="document.getElementById('isSaveMe').value='1';" name="TaskFileReference" type=text value="<%=PracticeActivity.getTaskFileReference()%>" size="80" maxlength="90" readonly="readonly">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=TaskFileReference&amp;sTableName=tPracticeActivity&amp;sRefID=<%=PracticeActivity.getPracticeActivityID()%>&amp;sFieldNameDisp=<%=PracticeActivity.getEnglish("TaskFileReference")%>&amp;sTableNameDisp=tPracticeActivity','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Logic Reference&nbsp;</b></p></td><td valign=top><p><input onChange="document.getElementById('isSaveMe').value='1';" name="TaskLogicReference" type=text value="<%=PracticeActivity.getTaskLogicReference()%>" size="80" maxlength="90" readonly="readonly">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=TaskLogicReference&amp;sTableName=tPracticeActivity&amp;sRefID=<%=PracticeActivity.getPracticeActivityID()%>&amp;sFieldNameDisp=<%=PracticeActivity.getEnglish("TaskLogicReference")%>&amp;sTableNameDisp=tPracticeActivity','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                     <tr><td valign=top><p class=<%=theClass%> ><b>ReferenceID&nbsp;</b></p></td><td valign=top><p><input onChange="document.getElementById('isSaveMe').value='1';" name="ReferenceID" type=text value="<%=PracticeActivity.getReferenceID()%>" size="80" maxlength="20" readonly="readonly">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ReferenceID&amp;sTableName=tPracticeActivity&amp;sRefID=<%=PracticeActivity.getPracticeActivityID()%>&amp;sFieldNameDisp=<%=PracticeActivity.getEnglish("ReferenceID")%>&amp;sTableNameDisp=tPracticeActivity','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
<% 
}%>

                     <tr>
                       <td valign="top"><p class="<%=theClass%>" ><b>Auto-comments&nbsp;</b></p></td>
                       <td valign="top"><p>
                         <%=PracticeActivity.getComments()%>
                         &nbsp;
                         <%if (isShowAudit){%>
                         <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Comments&amp;sTableName=tPracticeActivity&amp;sRefID=<%=PracticeActivity.getPracticeActivityID()%>&amp;sFieldNameDisp=<%=PracticeActivity.getEnglish("Comments")%>&amp;sTableNameDisp=tPracticeActivity','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align="middle" border="0" src="images/icon_audit.gif" /></a>
                         <%}%>
                       </p></td>
                     </tr>



            <tr><td>&nbsp;</td><td>&nbsp;</td></tr>
<%          if (PracticeActivity.getDocuLinkID()>0)
          {
			  %>
            <tr  class="<%=theClass%>">
              <td><strong>Document</strong></td>
              <td><%
				  	bltDocumentManagement myDM = new bltDocumentManagement(PracticeActivity.getDocuLinkID());
					pageControllerHash.put("sFileName",myDM.getDocumentFileName());
					pageControllerHash.put("sDownloadName", com.winstaff.password.RandomString.generateString(4).toLowerCase() + Math.round(Math.random()*1000) + "_" + myDM.getDocumentFileName());
					pageControllerHash.put("bDownload", new Boolean(true));
					session.setAttribute("pageControllerHash",pageControllerHash);
				  	%>
                    <input onChange="document.getElementById('isSaveMe').value='1';" type="button" value="View Document" onClick="document.location='fileRetrieve_nd.jsp'" class="tdBaseAltGreen" >
&nbsp;</td>
            </tr>
            <tr  class="<%=theClass%>">
              <td><strong>Replace Document</strong></td>
              <td><label>
                <select  onchange="document.getElementById('isSaveMe').value='1';"  name="AttachDoc" id="AttachDoc">
                  <option value="0" selected="selected">No</option>
                  <option value="1">Yes</option>
                </select>
              </label></td>
            </tr>
                    <%
			  }
			  else
          {
			  %>
            <tr  class="<%=theClass%>">
              <td><strong>Attach Document</strong></td>
              <td><label>
                <select  onchange="document.getElementById('isSaveMe').value='1';"  name="AttachDoc" id="AttachDoc">
                  <option value="0" selected="selected">No</option>
                  <option value="1">Yes</option>
                </select>
              </label></td>
            </tr>
                    <%
			  }
			  
			  
				  %>            <tr>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
            </table>
        <%if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {%>
            <input onChange="document.getElementById('isSaveMe').value='1';" type=hidden name=routePageReference value="sParentReturnPage">
             <%
              if (request.getParameter("INTNext")!=null&&request.getParameter("INTNext").equalsIgnoreCase("yes") ) 
              {
              %>
                  <table width=75% border=1 bordercolor=333333 align=left cellspacing=0 cellpadding=0 class=wizardTable>
                  <tr class=requiredField><td>
                  <input onChange="document.getElementById('isSaveMe').value='1';"  <%=HTMLFormStyleButton%> type="radio" value="next" name="INTNext" checked>&nbsp;&nbsp;<%=ConfigurationMessages.getInterviewMessage("INTERVIEWNoMore","tPracticeActivity")%>
                  <br>
                  <input onChange="document.getElementById('isSaveMe').value='1';"  <%=HTMLFormStyleButton%> type="radio" value="yes" name="INTNext">&nbsp;&nbsp;<%=ConfigurationMessages.getInterviewMessage("INTERVIEWAddMore","tPracticeActivity")%>
                  </td></tr></table><br><br><br>
              <%
              }
              %>
            <p><input  onClick = "document.getElementById('isSaveMe').value='0';this.disabled=true;submit();" type=Submit class="tdBaseAltGreen" value="Continue" name=Submit></p>
        <%}%>
        </td></tr></table>
        </form>
        <%
  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>

    </td></tr></table>


