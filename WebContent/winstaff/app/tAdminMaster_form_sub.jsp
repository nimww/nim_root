<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.*" %>
<%/*
    filename: out\jsp\tAdminMaster_form_sub.jsp
    Created on Mar/21/2003
    Created by: Scott Ellis
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>


<%

//initial declaration of list class and parentID
    Integer        iAdminID        =    null;
    boolean accessValid = false;

   Integer iSecurityCheck = SecurityCheck.CheckItem("company1", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {

    if (pageControllerHash.containsKey("iAdminID")) 
    {
        iAdminID        =    (Integer)pageControllerHash.get("iAdminID");
        if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {
            accessValid = true;
        }
    }
	String EDITID = "0";

    if ( request.getParameter( "EDITID" )!=null)
    {
        EDITID        =    request.getParameter( "EDITID" );
    }
	if (new Integer(EDITID).intValue()!=iAdminID.intValue()  )
	{
		accessValid=false;
		%>
        <script language="javascript">
		alert ("Invalid Save\n\nDid you Open Another Record Prior to Saving (possibly in another window or tab)?");
		</script>
        <%
	}

//page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);

//initial declaration of list class and parentID

    bltAdminMaster        AdminMaster        =    null;

    if ( request.getParameter( "EDIT" ).equalsIgnoreCase("edit") )
    {
        AdminMaster        =    new    bltAdminMaster(iAdminID,UserSecurityGroupID);
    }
    else if ( request.getParameter( "EDIT" ).equalsIgnoreCase("new") )
    {
        AdminMaster        =    new    bltAdminMaster(UserSecurityGroupID,true);
    }

String testChangeID = "0";

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("UniqueCreateDate"))) ;
    if ( !AdminMaster.getUniqueCreateDate().equals(testObj)  )
    {
         AdminMaster.setUniqueCreateDate( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("AdminMaster UniqueCreateDate not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("UniqueModifyDate"))) ;
    if ( !AdminMaster.getUniqueModifyDate().equals(testObj)  )
    {
         AdminMaster.setUniqueModifyDate( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("AdminMaster UniqueModifyDate not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("UniqueModifyComments")) ;
    if ( !AdminMaster.getUniqueModifyComments().equals(testObj)  )
    {
         AdminMaster.setUniqueModifyComments( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("AdminMaster UniqueModifyComments not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("Name")) ;
    if ( !AdminMaster.getName().equals(testObj)  )
    {
         AdminMaster.setName( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("AdminMaster Name not set. this is ok-not an error");
}


try
{
    String testObj = new String(request.getParameter("ContactFax")) ;
    if ( !AdminMaster.getContactFax().equals(testObj)  )
    {
         AdminMaster.setContactFax( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("AdminMaster ContactFax not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("fax")) ;
    if ( !AdminMaster.getfax().equals(testObj)  )
    {
         AdminMaster.setfax( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("AdminMaster fax not set. this is ok-not an error");
}


try
{
    String testObj = new String(request.getParameter("Address1")) ;
    if ( !AdminMaster.getAddress1().equals(testObj)  )
    {
         AdminMaster.setAddress1( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("AdminMaster Address1 not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("Address2")) ;
    if ( !AdminMaster.getAddress2().equals(testObj)  )
    {
         AdminMaster.setAddress2( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("AdminMaster Address2 not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("City")) ;
    if ( !AdminMaster.getCity().equals(testObj)  )
    {
         AdminMaster.setCity( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("AdminMaster City not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("StateID")) ;
    if ( !AdminMaster.getStateID().equals(testObj)  )
    {
         AdminMaster.setStateID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("AdminMaster StateID not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("Province")) ;
    if ( !AdminMaster.getProvince().equals(testObj)  )
    {
         AdminMaster.setProvince( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("AdminMaster Province not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("ZIP")) ;
    if ( !AdminMaster.getZIP().equals(testObj)  )
    {
         AdminMaster.setZIP( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("AdminMaster ZIP not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("CountryID")) ;
    if ( !AdminMaster.getCountryID().equals(testObj)  )
    {
         AdminMaster.setCountryID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("AdminMaster CountryID not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("Phone")) ;
    if ( !AdminMaster.getPhone().equals(testObj)  )
    {
         AdminMaster.setPhone( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("AdminMaster Phone not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("AlertEmail")) ;
    if ( !AdminMaster.getAlertEmail().equals(testObj)  )
    {
         AdminMaster.setAlertEmail( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("AdminMaster AlertEmail not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("AlertDays")) ;
    if ( !AdminMaster.getAlertDays().equals(testObj)  )
    {
         AdminMaster.setAlertDays( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("AdminMaster AlertDays not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("ContactFirstName")) ;
    if ( !AdminMaster.getContactFirstName().equals(testObj)  )
    {
         AdminMaster.setContactFirstName( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("AdminMaster ContactFirstName not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("ContactLastName")) ;
    if ( !AdminMaster.getContactLastName().equals(testObj)  )
    {
         AdminMaster.setContactLastName( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("AdminMaster ContactLastName not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("ContactEmail")) ;
    if ( !AdminMaster.getContactEmail().equals(testObj)  )
    {
         AdminMaster.setContactEmail( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("AdminMaster ContactEmail not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("ContactAddress1")) ;
    if ( !AdminMaster.getContactAddress1().equals(testObj)  )
    {
         AdminMaster.setContactAddress1( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("AdminMaster ContactAddress1 not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("ContactAddress2")) ;
    if ( !AdminMaster.getContactAddress2().equals(testObj)  )
    {
         AdminMaster.setContactAddress2( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("AdminMaster ContactAddress2 not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("ContactCity")) ;
    if ( !AdminMaster.getContactCity().equals(testObj)  )
    {
         AdminMaster.setContactCity( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("AdminMaster ContactCity not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("ContactStateID")) ;
    if ( !AdminMaster.getContactStateID().equals(testObj)  )
    {
         AdminMaster.setContactStateID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("AdminMaster ContactStateID not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("ContactZIP")) ;
    if ( !AdminMaster.getContactZIP().equals(testObj)  )
    {
         AdminMaster.setContactZIP( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("AdminMaster ContactZIP not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("ContactPhone")) ;
    if ( !AdminMaster.getContactPhone().equals(testObj)  )
    {
         AdminMaster.setContactPhone( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("AdminMaster ContactPhone not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("Comments")) ;
    if ( !AdminMaster.getComments().equals(testObj)  )
    {
         AdminMaster.setComments( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("AdminMaster Comments not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("AssignedToID")) ;
    if ( !AdminMaster.getAssignedToID().equals(testObj)  )
    {
         AdminMaster.setAssignedToID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("AdminMaster AssignedToID not set. this is ok-not an error");
}


boolean bINT = false;
boolean bNextStep = false;
String sRefreshDoc = "refreshMe=true&";

// If an edit, update information; if new, append information
if (testChangeID.equalsIgnoreCase("1"))
{

	AdminMaster.setUniqueModifyDate(new java.util.Date());
	String UserLogonDescription = "n/a";
	if (pageControllerHash.containsKey("UserLogonDescription"))
	{
	    UserLogonDescription = (String)pageControllerHash.get("UserLogonDescription");
	}
    //if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
    {
	    AdminMaster.setUniqueModifyComments(UserLogonDescription);
	    AdminMaster.commitData();
    }
}
String routePageReference = request.getParameter("routePageReference");
String nextPage=null;
if (routePageReference!=null)
{
        if (pageControllerHash.containsKey(routePageReference))
        {
            nextPage = (String)pageControllerHash.get(routePageReference);
        }
        else if (pageControllerHash.containsKey("sLocalChildReturnPage"))
        {
            nextPage = (String)pageControllerHash.get("sLocalChildReturnPage");
        }
        else if (pageControllerHash.containsKey("sParentReturnPage"))
        {
            nextPage = (String)pageControllerHash.get("sParentReturnPage");
        }
}
if (nextPage!=null)
{
	    %><script language=Javascript>
	      document.location="<%=nextPage%>";
	      </script><%
    //response.sendRedirect(nextPage+"?EDIT=edit");
}
        %>
Successful save

  <%
  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
  %>
