<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.PLCUtils,java.util.Vector,java.util.Enumeration,com.winstaff.ListElement,com.winstaff.bltPhysicianPracticeLU" %>
<%/*

    filename: out\jsp\tPhysicianPracticeLU_form.jsp
    JSP AutoGen on Mar/12/2002
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>

<%
//String thePLCID = (String)pageControllerHash.get("plcID");
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_PracticeID.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>

    <table cellpadding=0 cellspacing=0 border=0 width=700>
    <tr><td width=10>&nbsp;</td><td>
    <span class=title>File: tPhysicianPracticeLU_form.jsp</span><br>


<%
//initial declaration of list class and parentID
    Integer        iLookupID        =    null;
    boolean accessValid = false;
    // required for Type2
    String sKeyMasterReference = null;
    if (pageControllerHash.containsKey("iLookupID")) 
    {
        iLookupID        =    (Integer)pageControllerHash.get("iLookupID");
        accessValid = true;
        if (pageControllerHash.containsKey("sKeyMasterReference"))
        {
            sKeyMasterReference = (String)pageControllerHash.get("sKeyMasterReference");
        }
        if (sKeyMasterReference!=null)
        {
            if (sKeyMasterReference.equalsIgnoreCase("p"))
            {
                if (!pageControllerHash.containsKey("iPhysicianID")) 
                {
                    accessValid=false;
                }
            }
            else if (sKeyMasterReference.equalsIgnoreCase("s"))
            {
                if (!pageControllerHash.containsKey("iPracticeID")) 
                {
                    accessValid=false;
                }
            }
            else
            {
                accessValid=false;
            }
        }
        else 
        {
            accessValid=false;
        }
    }
  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);      java.text.SimpleDateFormat displayDateTimeSDF = new java.text.SimpleDateFormat("MM/dd/yyyy hh:mm a");

    pageControllerHash.put("sLocalChildReturnPage","tPhysicianPracticeLU_form2.jsp");      pageControllerHash.put("navTrace",navTrace);

//initial declaration of list class and parentID

    bltPhysicianPracticeLU        PhysicianPracticeLU        =    null;

    if ( request.getParameter( "EDIT" ).equalsIgnoreCase("edit") )
    {
        PhysicianPracticeLU        =    new    bltPhysicianPracticeLU(iLookupID);
    }
    else if ( request.getParameter( "EDIT" ).equalsIgnoreCase("new") )
    {
        PhysicianPracticeLU        =    new    bltPhysicianPracticeLU();
    }

//fields
        %>
        <%@ include file="tPhysicianPracticeLU_form_instructions.jsp" %>
        <form method=POST action="tPhysicianPracticeLU_form_sub.jsp" name="tPhysicianPracticeLU_form1">
<%
    if ( request.getParameter( "EDIT" ) != null )
    {
    %>
        <input type="hidden" name="EDIT" value = "<%=request.getParameter( "EDIT" )%>" >  
    <%
    }
%>
        <table border=0 cellpadding=0 cellspacing=0 class=tableBase>
        <tr class=tdHeader>
        <td><b>UniqueID</b>[<i>LookupID</i>]<%=PhysicianPracticeLU.getLookupID()%></td>
        </tr>
        <tr class=tdBase><td><b>This Item was Created on:</b> <%=displayDateSDF.format(PhysicianPracticeLU.getUniqueCreateDate())%></td><tr>
        <tr class=tdBase><td><b>This Item was Last Modified on:</b> <%=displayDateSDF.format(PhysicianPracticeLU.getUniqueModifyDate())%></td><tr>
        <tr class=tdBase><td><b>Modification Comments:</b> <%=(PhysicianPracticeLU.getUniqueModifyComments())%></td><tr>
          <%  String theClass ="tdBase";%>
            <%
            if (sKeyMasterReference.equalsIgnoreCase("p"))
            {
            %>
            <tr class=tdBase><td><b>PhysicianID</b> <%=PhysicianPracticeLU.getPhysicianID()%></td><tr>
            <tr class=tdBaseAlt><td><b>PracticeID</b><input type=text name="PracticeID" value="<%=PhysicianPracticeLU.getPracticeID()%>"></td></tr>
            <%
            }
            else if (sKeyMasterReference.equalsIgnoreCase("s"))
            {
            %>
            <tr class=tdBase><td><b>PracticeID</b> <%=PhysicianPracticeLU.getPracticeID()%></td></tr>
            <tr class=tdBaseAlt><td><b>PhysicianID</b><input type=text name="PhysicianID" value="<%=PhysicianPracticeLU.getPhysicianID()%>"><td></tr>
            <%
            }
            %>
            <%
            if ( (PhysicianPracticeLU.isRequired("StartDate"))&&(!PhysicianPracticeLU.isComplete("StartDate")) )
            {
                theClass = "requiredField";
            }
            else
            {
                theClass = "tdBase";
            }
            {
                %>

                        <tr class=<%=theClass%> ><td><b>Start Date<%if ((PhysicianPracticeLU.isExpired("StartDate",expiredDays))&&(PhysicianPracticeLU.isExpiredCheck("StartDate"))){out.print("**");}%></b><input type=text size="10" name="StartDate" value=<jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(PhysicianPracticeLU.getStartDate())%>" /></jsp:include> >('mm/dd/yyyy' -> ex: 01/05/1982) or 'Current' or 'NA' or 'today'</td></tr>

                <%
            }
            %>
            <%
            if ( (PhysicianPracticeLU.isRequired("EndDate"))&&(!PhysicianPracticeLU.isComplete("EndDate")) )
            {
                theClass = "requiredField";
            }
            else
            {
                theClass = "tdBase";
            }
            {
                %>

                        <tr class=<%=theClass%> ><td><b>End Date<%if ((PhysicianPracticeLU.isExpired("EndDate",expiredDays))&&(PhysicianPracticeLU.isExpiredCheck("EndDate"))){out.print("**");}%></b><input type=text size="10" name="EndDate" value=<jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(PhysicianPracticeLU.getEndDate())%>" /></jsp:include> >('mm/dd/yyyy' -> ex: 01/05/1982) or 'Current' or 'NA' or 'today'</td></tr>

                <%
            }
            %>
            <%
            if ( (PhysicianPracticeLU.isRequired("CoverageHours"))&&(!PhysicianPracticeLU.isComplete("CoverageHours")) )
            {
                theClass = "requiredField";
            }
            else
            {
                theClass = "tdBase";
            }
            {
                %>
                <tr class=<%=theClass%> ><td><b>Coverage Hours</b><input type=text size="100" name="CoverageHours" value="<%=PhysicianPracticeLU.getCoverageHours()%>"></td></tr>
                <%
            }
            %>
            <%
            if ( (PhysicianPracticeLU.isRequired("Comments"))&&(!PhysicianPracticeLU.isComplete("Comments")) )
            {
                theClass = "requiredField";
            }
            else
            {
                theClass = "tdBase";
            }
            {
                %>
                <tr class=<%=theClass%> ><td><b>Extra Comments</b><input type=text size="100" name="Comments" value="<%=PhysicianPracticeLU.getComments()%>"></td></tr>
                <%
            }
            %>
        <tr><td><select name=routePageReference>
        <option value="sParentReturnPage">Save & Return</option>
        <option value="sLocalChildReturnPage">Save & Stay</option>
        </select></td></tr>
        <tr><td><input type=Submit value=Submit name=Submit></td></tr>
        </table>
        </form>
        <%
  }
  else
  {
   response.sendRedirect("/errorhandler.jsp");
  }

%>

    </td></tr></table>



<%String bnIncludeFN = "ui_"+thePLCID+"\\bot-nav_PracticeID.jsp";
%>
<jsp:include page="<%=bnIncludeFN%>" flush="true" ><jsp:param name="plcID" value="<%=thePLCID%>"/></jsp:include>
