<%
response.setHeader("Cache-Control","no-cache"); 
response.setHeader("Pragma","no-cache"); 
response.setDateHeader ("Expires", -1);
%>
<!DOCTYPE HTML>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<%@page contentType="text/html" language="java" import="com.winstaff.*" %>
<%/*

    filename: out\jsp\tPracticeMaster_form.jsp
    Created on Mar/03/2003
    Created by: Scott Ellis
*/%>
<style type="text/css">
.netdevcommsumbit {
	-moz-box-shadow:inset 0px 1px 0px 0px #ffffff;
	-webkit-box-shadow:inset 0px 1px 0px 0px #ffffff;
	box-shadow:inset 0px 1px 0px 0px #ffffff;
	background:-webkit-gradient( linear, left top, left bottom, color-stop(0.05, #ededed), color-stop(1, #dfdfdf) );
	background:-moz-linear-gradient( center top, #ededed 5%, #dfdfdf 100% );
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#ededed', endColorstr='#dfdfdf');
	background-color:#ededed;
	-moz-border-radius:6px;
	-webkit-border-radius:6px;
	border-radius:6px;
	border:1px solid #dcdcdc;
	display:inline-block;
	color:#777777;
	font-family:arial;
	font-size:15px;
	font-weight:bold;
	padding:6px 24px;
	text-decoration:none;
	text-shadow:1px 1px 0px #ffffff;
}.netdevcommsumbit:hover {
	background:-webkit-gradient( linear, left top, left bottom, color-stop(0.05, #dfdfdf), color-stop(1, #ededed) );
	background:-moz-linear-gradient( center top, #dfdfdf 5%, #ededed 100% );
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#dfdfdf', endColorstr='#ededed');
	background-color:#dfdfdf;
	cursor:pointer;
}.netdevcommsumbit:active {
	position:relative;
	top:1px;
}
.netdevcommtoggle {
	-moz-box-shadow:inset 0px 1px 0px 0px #ffffff;
	-webkit-box-shadow:inset 0px 1px 0px 0px #ffffff;
	box-shadow:inset 0px 1px 0px 0px #ffffff;
	background:-webkit-gradient( linear, left top, left bottom, color-stop(0.05, #defaff), color-stop(1, #b9e5eb) );
	background:-moz-linear-gradient( center top, #defaff 5%, #b9e5eb 100% );
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#defaff', endColorstr='#b9e5eb');
	background-color:#defaff;
	-moz-border-radius:6px;
	-webkit-border-radius:6px;
	border-radius:6px;
	border:1px solid #dcdcdc;
	display:inline-block;
	color:#777777;
	font-family:arial;
	font-size:15px;
	font-weight:bold;
	padding:6px 24px;
	text-decoration:none;
	text-shadow:1px 1px 0px #ffffff;
}.netdevcommtoggle:hover {
	background:-webkit-gradient( linear, left top, left bottom, color-stop(0.05, #b9e5eb), color-stop(1, #defaff) );
	background:-moz-linear-gradient( center top, #b9e5eb 5%, #defaff 100% );
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#b9e5eb', endColorstr='#defaff');
	cursor:pointer;
	background-color:#b9e5eb;
}.netdevcommtoggle:active {
	position:relative;
	top:1px;
}
p{
	margin:0;
}
</style>
<%@ include file="../generic/CheckLogin.jsp" %>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js"></script>



<script language = javascript>
function BillingFill()
{
	document.forms[0].BillingAddress1.value = document.forms[0].OfficeAddress1.value;
	document.forms[0].BillingAddress2.value = document.forms[0].OfficeAddress2.value;
	document.forms[0].BillingCity.value = document.forms[0].OfficeCity.value;
//	document.forms[0].BillingProvince.value = document.forms[0].OfficeProvince.value;
	document.forms[0].BillingStateID.value = document.forms[0].OfficeStateID.value;
	document.forms[0].BillingZIP.value = document.forms[0].OfficeZIP.value;
//	document.forms[0].BillingCountryID.value = document.forms[0].OfficeCountryID.value;
	document.forms[0].BillingPhone.value = document.forms[0].OfficePhone.value;
	document.forms[0].BillingFax.value = document.forms[0].OfficeFaxNo.value;
	document.forms[0].BillingFirstName.value = document.forms[0].OfficeManagerFirstName.value;
	document.forms[0].BillingLastName.value = document.forms[0].OfficeManagerLastName.value;
	document.forms[0].BillingPayableTo.value = document.forms[0].OfficeTaxIDNameAffiliation.value;
	return false;
}

function CredFill()
{
	document.forms[0].CredentialingContactAddress1.value = document.forms[0].OfficeAddress1.value;
	document.forms[0].CredentialingContactAddress2.value = document.forms[0].OfficeAddress2.value;
	document.forms[0].CredentialingContactCity.value = document.forms[0].OfficeCity.value;
//	document.forms[0].CredentialingContactProvince.value = document.forms[0].OfficeProvince.value;
	document.forms[0].CredentialingContactStateID.value = document.forms[0].OfficeStateID.value;
	document.forms[0].CredentialingContactZIP.value = document.forms[0].OfficeZIP.value;
//	document.forms[0].CredentialingContactCountryID.value = document.forms[0].OfficeCountryID.value;
	document.forms[0].CredentialingContactPhone.value = document.forms[0].OfficePhone.value;
	document.forms[0].CredentialingContactFax.value = document.forms[0].OfficeFaxNo.value;
	document.forms[0].CredentialingContactFirstName.value = document.forms[0].OfficeManagerFirstName.value;
	document.forms[0].CredentialingContactLastName.value = document.forms[0].OfficeManagerLastName.value;	
	document.forms[0].CredentiallingContactEmail.value = document.forms[0].OfficeManagerEmail.value;	

	return false;
}
$(document).ready(function(e) {
    $('.netdevcommtoggle').click(function(){
		$('#netdevcommtrack').toggle();
	});
    $('.zipCheck').blur(function(){
    	if($('.zipCheck').val().length != 5){
    		alert(" Data integrity is important. Please check your zip code. Your data will not be saved if you move forward.");
    	}
    });
    $('.zipCheck2').blur(function(){
    	if($('.zipCheck2').val().length != 5){
    		alert(" Data integrity is important. Please check your zip code. Your data will not be saved if you move forward.");
    	}
    });
    $('.zipCheck3').blur(function(){
    	if($('.zipCheck3').val().length != 5){
    		alert(" Data integrity is important. Please check your zip code. Your data will not be saved if you move forward.");
    	}
    });
});
</script>



<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_PracticeID_nim.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>

<!--body onUnload="if (document.getElementById('isSaveMe').value=='1'&&confirm('Want to save first?')){document.getElementById('tPracticeMaster_form1').submit();}">-->

<table cellpadding=0 cellspacing=0 border=0 width=100% >
  <tr>
    <td width=10>&nbsp;</td>
    <td> 
    <td> <%//ConfigurationMessages.getHTML("INTERVIEWTopControl","tPracticeMaster1")%> 
      <%
//initial declaration of list class and parentID
    Integer        iPracticeID        =    null;
    boolean accessValid = false;
    boolean isImagingCenter = true;
    // required for Type2
    String sKeyMasterReference = null;

   Integer iSecurityCheck = SecurityCheck.CheckItem("Practice1", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {
    if (pageControllerHash.containsKey("iPracticeID")) 
    {
        iPracticeID        =    (Integer)pageControllerHash.get("iPracticeID");
        accessValid = true;    
	}
  //page security
  if (accessValid)
  {%>
	  <script>
$(document).ready(function(){
	$(".netdevcommsumbit").click(function(e){
		e.preventDefault();
		if ($.trim($("#addnetdevcommtrack").val()).length>0){
			formNetDevCommTrackSubmit();
		}
		else{
			$('#addnetdevcommtrack').focus();
		}
		
	});
		
});
function buildDataString(){
	var dataString="AccountID="+<%=iPracticeID%>;
	dataString +="&MessageText="+$('#addnetdevcommtrack').val().replace("#", "%23");
	
	return dataString;
}
function formNetDevCommTrackSubmit(){
	var dataString = buildDataString();
	$.ajax({
		type: "POST",
		url: "netdevcomm.jsp",
		data: dataString,
		success: function() {
			$('#addnetdevcommtrack').val("");
			$('#netdevcommtrack').text("");
			$("#netdevcommtrack").load('retrieve_netdevcomm.jsp?practiceid=<%=iPracticeID%>');
		}
	});
	//$("#commTrackForm, #faded-bg").hide().html("");
}
</script>
	  
	  
      <%java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);      java.text.SimpleDateFormat displayDateTimeSDF = new java.text.SimpleDateFormat("MM/dd/yyyy hh:mm a");

//    pageControllerHash.put("sLocalChildReturnPage","tPracticeMaster_form2.jsp?EDIT=edit");
//    pageControllerHash.put("sParentReturnPage","tPracticeMaster_form2.jsp?EDIT=edit");
    pageControllerHash.put("sLocalChildReturnPage","practice_Status.jsp");
    pageControllerHash.put("sParentReturnPage","practice_Status.jsp");
    session.setAttribute("pageControllerHash",pageControllerHash);

//initial declaration of list class and parentID

    bltPracticeMaster        PracticeMaster        =    null;

    if ( request.getParameter( "EDIT" ).equalsIgnoreCase("edit") )
    {
        PracticeMaster        =    new    bltPracticeMaster(iPracticeID,UserSecurityGroupID);
    }
    else if ( request.getParameter( "EDIT" ).equalsIgnoreCase("new") )
    {
        PracticeMaster        =    new    bltPracticeMaster(UserSecurityGroupID, true);
    }

//fields
        %>
      <%@ include file="tPracticeMaster_form_instructions.jsp" %>
      <form action="tPracticeMaster_form_sub.jsp" name="tPracticeMaster_form1"  id="tPracticeMaster_form1" method="POST">
      <input name="EDITID" value="<%=iPracticeID%>" type="hidden">
        <input onChange="document.getElementById('isSaveMe').value='1';" type="hidden" name="isSaveMe" id="isSaveMe" value="0" />
        <%
    if ( request.getParameter( "EDIT" ) != null )
    {
    %>
        <input onChange="document.getElementById('isSaveMe').value='1';" type="hidden" name="EDIT" value = "<%=request.getParameter( "EDIT" )%>" >
        <%
    }
%>
        <%  String theClass ="tdBase";%>


        <table width=100% border=1 bordercolor=#333333 cellpadding=3 cellspacing=0 class=tableBase>
          <tr>
            <td class=tableColor> 
              <table cellpadding=0 cellspacing=0 width=100%>
                <tr class=title>
                  <td colspan=3><table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td>General Practice Information:</td>
                      <td align="right"> <input onChange="document.getElementById('isSaveMe').value='1';"  class="tdBaseAltGreen" onClick="document.getElementById('isSaveMe').value='0';"  type=Submit value="Save" name=Submit></td>
                    </tr>
                  </table>
</td>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Practice Name&nbsp;</b></p>
                  </td>
                  <td width="90%" valign=top>
                    <p>
                      <input class="tdHeader" onChange="document.getElementById('isSaveMe').value='1';" maxlength="100" type=text size="40" name="PracticeName" value="<%=PracticeMaster.getPracticeName()%>">
                      &nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PracticeName&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("PracticeName")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                  <td rowspan="25" valign=top>
                  <table border="1" cellpadding="2" cellspacing="0" align=right>
                    <tr class="tdHeaderAlt">
                      <td colspan="4">Client Type ID</td>
                      </tr>
                      <tr>
                      <td colspan="4">
                      <select name="clientTypeID">
                      	<option value="0" <%if(PracticeMaster.getClientTypeID()==0){%> selected <%}%>>-Select One-</option>
                        <option value="1" <%if(PracticeMaster.getClientTypeID()==1){%> selected <%}%>>Work Comp</option>
                        <option value="2" <%if(PracticeMaster.getClientTypeID()==2){%> selected <%}%>>Group Health</option>
                        <option value="3" <%if(PracticeMaster.getClientTypeID()==3){%> selected <%}%>>Both</option>
                      </select>
                      </td>
                      </tr>
                      </table>
                  </td>
                  <td width="90%" rowspan="25" valign=top>
                  <!-- Start WC -->
                  <table border="1" cellpadding="2" cellspacing="0">
                    <tr class="tdHeaderAlt">
                      <td colspan="4">WC Contract Pricing</td>
                      </tr>
                      
                     <tr class="tdHeaderAlt">
                       <td valign="top" nowrap>WC Procedure&nbsp;</td>
                       <td valign="top" >Global</td>
                       <td valign="top" >TC</td>
                       <td valign="top" >26</td>
                     </tr>
                     <tr>
                      <td valign="top" nowrap><p class="<%=theClass%>" ><b>MRI
wo</b></p></td>
                      <td valign="top"><p>
                        <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="10"  type="text" size="6" name="Price_Mod_MRI_WO" value="<%=PLCUtils.getDisplayDefaultDecimalFormat2(PracticeMaster.getPrice_Mod_MRI_WO())%>" />
                        &nbsp;
                        <%if (isShowAudit){%>
                        <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Price_Mod_MRI_WO&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("Price_Mod_MRI_WO")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align="middle" border="0" src="images/icon_audit.gif" /></a>
                        <%}%>
                      </p></td>
                      <td valign="top"><p>
                        <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="10"  type="text" size="6" name="Price_Mod_MRI_WO_TC" value="<%=PLCUtils.getDisplayDefaultDecimalFormat2(PracticeMaster.getPrice_Mod_MRI_WO_TC())%>" />
                        &nbsp;
                        <%if (isShowAudit){%>
                        <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Price_Mod_MRI_WO_TC&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("Price_Mod_MRI_WO_TC")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align="middle" border="0" src="images/icon_audit.gif" /></a>
                        <%}%>
                      </p></td>
                      <td valign="top"><p>
                        <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="10"  type="text" size="6" name="Price_Mod_MRI_WO_26" value="<%=PLCUtils.getDisplayDefaultDecimalFormat2(PracticeMaster.getPrice_Mod_MRI_WO_26())%>" />
                        &nbsp;
                        <%if (isShowAudit){%>
                        <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Price_Mod_MRI_WO_26&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("Price_Mod_MRI_WO_26")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align="middle" border="0" src="images/icon_audit.gif" /></a>
                        <%}%>
                      </p></td>
                    </tr>
                    <tr>
                      <td valign="top" nowrap><p class="<%=theClass%>" ><b>MRI 
                        w</b></p></td>
                      <td valign="top"><p>
                        <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="10"  type="text" size="6" name="Price_Mod_MRI_W" value="<%=PLCUtils.getDisplayDefaultDecimalFormat2(PracticeMaster.getPrice_Mod_MRI_W())%>" />
                        &nbsp;
                        <%if (isShowAudit){%>
                        <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Price_Mod_MRI_W&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("Price_Mod_MRI_W")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align="middle" border="0" src="images/icon_audit.gif" /></a>
                        <%}%>
                      </p></td>
                      <td valign="top"><p>
                        <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="10"  type="text" size="6" name="Price_Mod_MRI_W_TC" value="<%=PLCUtils.getDisplayDefaultDecimalFormat2(PracticeMaster.getPrice_Mod_MRI_W_TC())%>" />
                        &nbsp;
                        <%if (isShowAudit){%>
                        <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Price_Mod_MRI_W_TC&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("Price_Mod_MRI_W_TC")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align="middle" border="0" src="images/icon_audit.gif" /></a>
                        <%}%>
                      </p></td>
                      <td valign="top"><p>
                        <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="10"  type="text" size="6" name="Price_Mod_MRI_W_26" value="<%=PLCUtils.getDisplayDefaultDecimalFormat2(PracticeMaster.getPrice_Mod_MRI_W_26())%>" />
                        &nbsp;
                        <%if (isShowAudit){%>
                        <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Price_Mod_MRI_W_26&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("Price_Mod_MRI_W_26")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align="middle" border="0" src="images/icon_audit.gif" /></a>
                        <%}%>
                      </p></td>
                    </tr>
                    <tr>
                      <td valign="top" nowrap><p class="<%=theClass%>" ><b>MRI wwo</b></p></td>
                      <td valign="top"><p>
                        <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="10"  type="text" size="6" name="Price_Mod_MRI_WWO" value="<%=PLCUtils.getDisplayDefaultDecimalFormat2(PracticeMaster.getPrice_Mod_MRI_WWO())%>" />
                        &nbsp;
                        <%if (isShowAudit){%>
                        <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Price_Mod_MRI_WWO&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("Price_Mod_MRI_WWO")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align="middle" border="0" src="images/icon_audit.gif" /></a>
                        <%}%>
                      </p></td>
                      <td valign="top"><p>
                        <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="10"  type="text" size="6" name="Price_Mod_MRI_WWO_TC" value="<%=PLCUtils.getDisplayDefaultDecimalFormat2(PracticeMaster.getPrice_Mod_MRI_WWO_TC())%>" />
                        &nbsp;
                        <%if (isShowAudit){%>
                        <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Price_Mod_MRI_WWO_TC&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("Price_Mod_MRI_WWO_TC")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align="middle" border="0" src="images/icon_audit.gif" /></a>
                        <%}%>
                      </p></td>
                      <td valign="top"><p>
                        <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="10"  type="text" size="6" name="Price_Mod_MRI_WWO_26" value="<%=PLCUtils.getDisplayDefaultDecimalFormat2(PracticeMaster.getPrice_Mod_MRI_WWO_26())%>" />
                        &nbsp;
                        <%if (isShowAudit){%>
                        <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Price_Mod_MRI_WWO_26&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("Price_Mod_MRI_WWO_26")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align="middle" border="0" src="images/icon_audit.gif" /></a>
                        <%}%>
                      </p></td>
                    </tr>
                    <tr>
                      <td valign="top" nowrap><p class="<%=theClass%>" ><b>CT wo</b></p></td>
                      <td valign="top"><p>
                        <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="10"  type="text" size="6" name="Price_Mod_CT_WO" value="<%=PLCUtils.getDisplayDefaultDecimalFormat2(PracticeMaster.getPrice_Mod_CT_WO())%>" />
                        &nbsp;
                        <%if (isShowAudit){%>
                        <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Price_Mod_CT_WO&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("Price_Mod_CT_WO")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align="middle" border="0" src="images/icon_audit.gif" /></a>
                        <%}%>
                      </p></td>
                      <td valign="top"><p>
                        <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="10"  type="text" size="6" name="Price_Mod_CT_WO_TC" value="<%=PLCUtils.getDisplayDefaultDecimalFormat2(PracticeMaster.getPrice_Mod_CT_WO_TC())%>" />
                        &nbsp;
                        <%if (isShowAudit){%>
                        <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Price_Mod_CT_WO_TC&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("Price_Mod_CT_WO_TC")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align="middle" border="0" src="images/icon_audit.gif" /></a>
                        <%}%>
                      </p></td>
                      <td valign="top"><p>
                        <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="10"  type="text" size="6" name="Price_Mod_CT_WO_26" value="<%=PLCUtils.getDisplayDefaultDecimalFormat2(PracticeMaster.getPrice_Mod_CT_WO_26())%>" />
                        &nbsp;
                        <%if (isShowAudit){%>
                        <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Price_Mod_CT_WO_26&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("Price_Mod_CT_WO_26")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align="middle" border="0" src="images/icon_audit.gif" /></a>
                        <%}%>
                      </p></td>
                    </tr>
                    <tr>
                      <td valign="top" nowrap><p class="<%=theClass%>" ><b>CT w</b></p></td>
                      <td valign="top"><p>
                        <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="10"  type="text" size="6" name="Price_Mod_CT_W" value="<%=PLCUtils.getDisplayDefaultDecimalFormat2(PracticeMaster.getPrice_Mod_CT_W())%>" />
                        &nbsp;
                        <%if (isShowAudit){%>
                        <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Price_Mod_CT_W&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("Price_Mod_CT_W")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align="middle" border="0" src="images/icon_audit.gif" /></a>
                        <%}%>
                      </p></td>
                      <td valign="top"><p>
                        <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="10"  type="text" size="6" name="Price_Mod_CT_W_TC" value="<%=PLCUtils.getDisplayDefaultDecimalFormat2(PracticeMaster.getPrice_Mod_CT_W_TC())%>" />
                        &nbsp;
                        <%if (isShowAudit){%>
                        <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Price_Mod_CT_W_TC&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("Price_Mod_CT_W_TC")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align="middle" border="0" src="images/icon_audit.gif" /></a>
                        <%}%>
                      </p></td>
                      <td valign="top"><p>
                        <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="10"  type="text" size="6" name="Price_Mod_CT_W_26" value="<%=PLCUtils.getDisplayDefaultDecimalFormat2(PracticeMaster.getPrice_Mod_CT_W_26())%>" />
                        &nbsp;
                        <%if (isShowAudit){%>
                        <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Price_Mod_CT_W_26&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("Price_Mod_CT_W_26")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align="middle" border="0" src="images/icon_audit.gif" /></a>
                        <%}%>
                      </p></td>
                    </tr>
                    <tr>
                      <td valign="top" nowrap><p class="<%=theClass%>" ><b>CT wwo</b></p></td>
                      <td valign="top"><p>
                        <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="10"  type="text" size="6" name="Price_Mod_CT_WWO" value="<%=PLCUtils.getDisplayDefaultDecimalFormat2(PracticeMaster.getPrice_Mod_CT_WWO())%>" />
                        &nbsp;
                        <%if (isShowAudit){%>
                        <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Price_Mod_CT_WWO&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("Price_Mod_CT_WWO")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align="middle" border="0" src="images/icon_audit.gif" /></a>
                        <%}%>
                      </p></td>
                      <td valign="top"><p>
                        <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="10"  type="text" size="6" name="Price_Mod_CT_WWO_TC" value="<%=PLCUtils.getDisplayDefaultDecimalFormat2(PracticeMaster.getPrice_Mod_CT_WWO_TC())%>" />
                        &nbsp;
                        <%if (isShowAudit){%>
                        <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Price_Mod_CT_WWO_TC&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("Price_Mod_CT_WWO_TC")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align="middle" border="0" src="images/icon_audit.gif" /></a>
                        <%}%>
                      </p></td>
                      <td valign="top"><p>
                        <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="10"  type="text" size="6" name="Price_Mod_CT_WWO_26" value="<%=PLCUtils.getDisplayDefaultDecimalFormat2(PracticeMaster.getPrice_Mod_CT_WWO_26())%>" />
                        &nbsp;
                        <%if (isShowAudit){%>
                        <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Price_Mod_CT_WWO_26&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("Price_Mod_CT_WWO_26")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align="middle" border="0" src="images/icon_audit.gif" /></a>
                        <%}%>
                      </p></td>
                    </tr>
                    <tr>
                      <td valign="top" nowrap><p class="<%=theClass%>" ><b>PET/CT wwo</b></p></td>
                      <td valign="top"><p>
                        <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="10"  type="text" size="6" name="Price_Mod_PETCT_WWO" value="<%=PLCUtils.getDisplayDefaultDecimalFormat2(PracticeMaster.getPrice_Mod_PETCT_WWO())%>" />
                        &nbsp;
                        <%if (isShowAudit){%>
                        <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Price_Mod_PETCT_WWO&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("Price_Mod_PETCT_WWO")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align="middle" border="0" src="images/icon_audit.gif" /></a>
                        <%}%>
                      </p></td>
                      <td valign="top"><p>
                        <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="10"  type="text" size="6" name="Price_Mod_PETCT_WWO_TC" value="<%=PLCUtils.getDisplayDefaultDecimalFormat2(PracticeMaster.getPrice_Mod_PETCT_WWO_TC())%>" />
                        &nbsp;
                        <%if (isShowAudit){%>
                        <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Price_Mod_PETCT_WWO_TC&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("Price_Mod_PETCT_WWO_TC")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align="middle" border="0" src="images/icon_audit.gif" /></a>
                        <%}%>
                      </p></td>
                      <td valign="top"><p>
                        <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="10"  type="text" size="6" name="Price_Mod_PETCT_WWO_26" value="<%=PLCUtils.getDisplayDefaultDecimalFormat2(PracticeMaster.getPrice_Mod_PETCT_WWO_26())%>" />
                        &nbsp;
                        <%if (isShowAudit){%>
                        <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Price_Mod_PETCT_WWO_26&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("Price_Mod_PETCT_WWO_26")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align="middle" border="0" src="images/icon_audit.gif" /></a>
                        <%}%>
                      </p></td>
                    </tr>                    
                    
					                    <tr class="tdHeaderAlt">
                      <td colspan="4" valign="top" nowrap>WC Backup Pricing</td>
                      </tr>
					 
                    
                     <tr><td valign=top><p class=<%=theClass%> ><b>Backup #1<br>
                       FSID&nbsp;</b></p></td><td colspan="3" valign=top><p><input maxlength="6" type=text size="10" name="FeeScheduleRefID" value="<%=PracticeMaster.getFeeScheduleRefID()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=FeeScheduleRefID&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("FeeScheduleRefID")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%>
                         <br>
                         <span class="<%=theClass%>">1 = State WC</span>                     </p></td>
                      </tr>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Backup #1<br>
                       Percentage:&nbsp;</b></p></td><td colspan="3" valign=top><p><input maxlength=4  type=text size="10" name="FeePercentage" value="<%=PLCUtils.getDisplayDefaultDecimalFormat2(PracticeMaster.getFeePercentage())%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=FeePercentage&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("FeePercentage")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td>
                      </tr>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Backup #2<br>
                       FSID&nbsp;</b></p></td><td colspan="3" valign=top><p><input maxlength="20" type=text size="10" name="FeeScheduleOverRefID" value="<%=PracticeMaster.getFeeScheduleOverRefID()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=FeeScheduleOverRefID&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("FeeScheduleOverRefID")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%>
					   <br>
                         <span class="<%=theClass%>">1 = State WC</span>
					   </p></td>
                      </tr>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Backup #2<br>
                       Percentage:&nbsp;</b></p></td><td colspan="3" valign=top><p><input maxlength=10  type=text size="10" name="FeeOverPercentage" value="<%=PLCUtils.getDisplayDefaultDecimalFormat2(PracticeMaster.getFeeOverPercentage())%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=FeeOverPercentage&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("FeeOverPercentage")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td>
                      </tr>

					  
                    
                    <tr class="tdHeaderAlt">
                      <td colspan="4" valign="top" nowrap>WC Other</td>
                      </tr>
					  
					  
<tr><td valign=top><p class=<%=theClass%> ><b>Initial Quota&nbsp;</b></p></td><td colspan="3" valign=top><p><input maxlength="5" type=text size="5" name="InitialQuota" value="<%=PracticeMaster.getInitialQuota()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=InitialQuota&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("InitialQuota")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td>
  </tr>                     

                         <tr><td valign=top><p class=<%=theClass%> ><b>Payment Terms Method&nbsp;</b></p></td><td colspan="3" valign=top><p><input maxlength="10" type=text size="20" name="PaymentTermsMethod" value="<%=PracticeMaster.getPaymentTermsMethod()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PaymentTermsMethod&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("PaymentTermsMethod")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td>
                         </tr>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Payment Terms Net&nbsp;</b></p></td><td colspan="3" valign=top><p><input maxlength="2" type=text size="6" name="PaymentTermsNet" value="<%=PracticeMaster.getPaymentTermsNet()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PaymentTermsNet&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("PaymentTermsNet")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td>
                      </tr>
                      
                      
      <tr><td valign=top><p class=<%=theClass%> ><b>Does Aging?&nbsp;</b></p></td><td   colspan="3" valign=top><p><select   name="DoesAging" ><jsp:include page="../generic/tYesNoLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PracticeMaster.getDoesAging()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=DoesAging&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("DoesAging")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td> 
</tr>             


                        <tr><td valign=top><p class=<%=theClass%> ><b>Diagnostic MD Arthrogram&nbsp;</b></p></td><td valign=top  colspan="3"><p><select   name="DiagnosticMDArthrogram" ><jsp:include page="../generic/tYesNoLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PracticeMaster.getDiagnosticMDArthrogram()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=DiagnosticMDArthrogram&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("DiagnosticMDArthrogram")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td> 
</tr>                
                        <tr><td valign=top><p class=<%=theClass%> ><b>Diagnostic MD Myelogram&nbsp;</b></p></td><td valign=top  colspan="3"><p><select   name="DiagnosticMDMyelogram" ><jsp:include page="../generic/tYesNoLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PracticeMaster.getDiagnosticMDMyelogram()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=DiagnosticMDMyelogram&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("DiagnosticMDMyelogram")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td> 
</tr>                        
                         
                  </table>
                  <!-- End WC -->
                  </td>
                  <td width="90%" rowspan="25" valign=top>
                  <!-- Start GH -->
                  <table border="1" cellpadding="2" cellspacing="0">
                    <tr class="tdHeaderAlt">
                      <td colspan="4">GH Contract Pricing</td>
                      </tr>
                      
                     <tr class="tdHeaderAlt">
                       <td valign="top" nowrap>GH Procedure&nbsp;</td>
                       <td valign="top" >Global</td>
                       <td valign="top" >TC</td>
                       <td valign="top" >26</td>
                     </tr>
                     <tr>
                      <td valign="top" nowrap><p class="<%=theClass%>" ><b>MRI
wo</b></p></td>
                      <td valign="top"><p>
                        <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="10"  type="text" size="6" name="GH_Price_Mod_MRI_WO" value="<%=PLCUtils.getDisplayDefaultDecimalFormat2(PracticeMaster.getGH_Price_Mod_MRI_WO())%>" />
                        &nbsp;
                        <%if (isShowAudit){%>
                        <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Price_Mod_MRI_WO&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("GH_Price_Mod_MRI_WO")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align="middle" border="0" src="images/icon_audit.gif" /></a>
                        <%}%>
                      </p></td>
                      <td valign="top"><p>
                        <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="10"  type="text" size="6" name="GH_Price_Mod_MRI_WO_TC" value="<%=PLCUtils.getDisplayDefaultDecimalFormat2(PracticeMaster.getGH_Price_Mod_MRI_WO_TC())%>" />
                        &nbsp;
                        <%if (isShowAudit){%>
                        <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Price_Mod_MRI_WO_TC&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("GH_Price_Mod_MRI_WO_TC")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align="middle" border="0" src="images/icon_audit.gif" /></a>
                        <%}%>
                      </p></td>
                      <td valign="top"><p>
                        <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="10"  type="text" size="6" name="GH_Price_Mod_MRI_WO_26" value="<%=PLCUtils.getDisplayDefaultDecimalFormat2(PracticeMaster.getGH_Price_Mod_MRI_WO_26())%>" />
                        &nbsp;
                        <%if (isShowAudit){%>
                        <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Price_Mod_MRI_WO_26&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("GH_Price_Mod_MRI_WO_26")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align="middle" border="0" src="images/icon_audit.gif" /></a>
                        <%}%>
                      </p></td>
                    </tr>
                    <tr>
                      <td valign="top" nowrap><p class="<%=theClass%>" ><b>MRI 
                        w</b></p></td>
                      <td valign="top"><p>
                        <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="10"  type="text" size="6" name="GH_Price_Mod_MRI_W" value="<%=PLCUtils.getDisplayDefaultDecimalFormat2(PracticeMaster.getGH_Price_Mod_MRI_W())%>" />
                        &nbsp;
                        <%if (isShowAudit){%>
                        <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Price_Mod_MRI_W&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("GH_Price_Mod_MRI_W")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align="middle" border="0" src="images/icon_audit.gif" /></a>
                        <%}%>
                      </p></td>
                      <td valign="top"><p>
                        <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="10"  type="text" size="6" name="GH_Price_Mod_MRI_W_TC" value="<%=PLCUtils.getDisplayDefaultDecimalFormat2(PracticeMaster.getGH_Price_Mod_MRI_W_TC())%>" />
                        &nbsp;
                        <%if (isShowAudit){%>
                        <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Price_Mod_MRI_W_TC&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("GH_Price_Mod_MRI_W_TC")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align="middle" border="0" src="images/icon_audit.gif" /></a>
                        <%}%>
                      </p></td>
                      <td valign="top"><p>
                        <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="10"  type="text" size="6" name="GH_Price_Mod_MRI_W_26" value="<%=PLCUtils.getDisplayDefaultDecimalFormat2(PracticeMaster.getGH_Price_Mod_MRI_W_26())%>" />
                        &nbsp;
                        <%if (isShowAudit){%>
                        <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Price_Mod_MRI_W_26&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("GH_Price_Mod_MRI_W_26")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align="middle" border="0" src="images/icon_audit.gif" /></a>
                        <%}%>
                      </p></td>
                    </tr>
                    <tr>
                      <td valign="top" nowrap><p class="<%=theClass%>" ><b>MRI wwo</b></p></td>
                      <td valign="top"><p>
                        <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="10"  type="text" size="6" name="GH_Price_Mod_MRI_WWO" value="<%=PLCUtils.getDisplayDefaultDecimalFormat2(PracticeMaster.getGH_Price_Mod_MRI_WWO())%>" />
                        &nbsp;
                        <%if (isShowAudit){%>
                        <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Price_Mod_MRI_WWO&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("GH_Price_Mod_MRI_WWO")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align="middle" border="0" src="images/icon_audit.gif" /></a>
                        <%}%>
                      </p></td>
                      <td valign="top"><p>
                        <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="10"  type="text" size="6" name="GH_Price_Mod_MRI_WWO_TC" value="<%=PLCUtils.getDisplayDefaultDecimalFormat2(PracticeMaster.getGH_Price_Mod_MRI_WWO_TC())%>" />
                        &nbsp;
                        <%if (isShowAudit){%>
                        <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Price_Mod_MRI_WWO_TC&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("GH_Price_Mod_MRI_WWO_TC")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align="middle" border="0" src="images/icon_audit.gif" /></a>
                        <%}%>
                      </p></td>
                      <td valign="top"><p>
                        <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="10"  type="text" size="6" name="GH_Price_Mod_MRI_WWO_26" value="<%=PLCUtils.getDisplayDefaultDecimalFormat2(PracticeMaster.getGH_Price_Mod_MRI_WWO_26())%>" />
                        &nbsp;
                        <%if (isShowAudit){%>
                        <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Price_Mod_MRI_WWO_26&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("GH_Price_Mod_MRI_WWO_26")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align="middle" border="0" src="images/icon_audit.gif" /></a>
                        <%}%>
                      </p></td>
                    </tr>
                    <tr>
                      <td valign="top" nowrap><p class="<%=theClass%>" ><b>CT wo</b></p></td>
                      <td valign="top"><p>
                        <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="10"  type="text" size="6" name="GH_Price_Mod_CT_WO" value="<%=PLCUtils.getDisplayDefaultDecimalFormat2(PracticeMaster.getGH_Price_Mod_CT_WO())%>" />
                        &nbsp;
                        <%if (isShowAudit){%>
                        <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Price_Mod_CT_WO&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("GH_Price_Mod_CT_WO")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align="middle" border="0" src="images/icon_audit.gif" /></a>
                        <%}%>
                      </p></td>
                      <td valign="top"><p>
                        <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="10"  type="text" size="6" name="GH_Price_Mod_CT_WO_TC" value="<%=PLCUtils.getDisplayDefaultDecimalFormat2(PracticeMaster.getGH_Price_Mod_CT_WO_TC())%>" />
                        &nbsp;
                        <%if (isShowAudit){%>
                        <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Price_Mod_CT_WO_TC&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("GH_Price_Mod_CT_WO_TC")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align="middle" border="0" src="images/icon_audit.gif" /></a>
                        <%}%>
                      </p></td>
                      <td valign="top"><p>
                        <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="10"  type="text" size="6" name="GH_Price_Mod_CT_WO_26" value="<%=PLCUtils.getDisplayDefaultDecimalFormat2(PracticeMaster.getGH_Price_Mod_CT_WO_26())%>" />
                        &nbsp;
                        <%if (isShowAudit){%>
                        <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Price_Mod_CT_WO_26&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("GH_Price_Mod_CT_WO_26")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align="middle" border="0" src="images/icon_audit.gif" /></a>
                        <%}%>
                      </p></td>
                    </tr>
                    <tr>
                      <td valign="top" nowrap><p class="<%=theClass%>" ><b>CT w</b></p></td>
                      <td valign="top"><p>
                        <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="10"  type="text" size="6" name="GH_Price_Mod_CT_W" value="<%=PLCUtils.getDisplayDefaultDecimalFormat2(PracticeMaster.getGH_Price_Mod_CT_W())%>" />
                        &nbsp;
                        <%if (isShowAudit){%>
                        <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Price_Mod_CT_W&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("GH_Price_Mod_CT_W")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align="middle" border="0" src="images/icon_audit.gif" /></a>
                        <%}%>
                      </p></td>
                      <td valign="top"><p>
                        <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="10"  type="text" size="6" name="GH_Price_Mod_CT_W_TC" value="<%=PLCUtils.getDisplayDefaultDecimalFormat2(PracticeMaster.getGH_Price_Mod_CT_W_TC())%>" />
                        &nbsp;
                        <%if (isShowAudit){%>
                        <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Price_Mod_CT_W_TC&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("GH_Price_Mod_CT_W_TC")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align="middle" border="0" src="images/icon_audit.gif" /></a>
                        <%}%>
                      </p></td>
                      <td valign="top"><p>
                        <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="10"  type="text" size="6" name="GH_Price_Mod_CT_W_26" value="<%=PLCUtils.getDisplayDefaultDecimalFormat2(PracticeMaster.getGH_Price_Mod_CT_W_26())%>" />
                        &nbsp;
                        <%if (isShowAudit){%>
                        <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Price_Mod_CT_W_26&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("GH_Price_Mod_CT_W_26")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align="middle" border="0" src="images/icon_audit.gif" /></a>
                        <%}%>
                      </p></td>
                    </tr>
                    <tr>
                      <td valign="top" nowrap><p class="<%=theClass%>" ><b>CT wwo</b></p></td>
                      <td valign="top"><p>
                        <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="10"  type="text" size="6" name="GH_Price_Mod_CT_WWO" value="<%=PLCUtils.getDisplayDefaultDecimalFormat2(PracticeMaster.getGH_Price_Mod_CT_WWO())%>" />
                        &nbsp;
                        <%if (isShowAudit){%>
                        <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Price_Mod_CT_WWO&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("GH_Price_Mod_CT_WWO")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align="middle" border="0" src="images/icon_audit.gif" /></a>
                        <%}%>
                      </p></td>
                      <td valign="top"><p>
                        <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="10"  type="text" size="6" name="GH_Price_Mod_CT_WWO_TC" value="<%=PLCUtils.getDisplayDefaultDecimalFormat2(PracticeMaster.getGH_Price_Mod_CT_WWO_TC())%>" />
                        &nbsp;
                        <%if (isShowAudit){%>
                        <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Price_Mod_CT_WWO_TC&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("GH_Price_Mod_CT_WWO_TC")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align="middle" border="0" src="images/icon_audit.gif" /></a>
                        <%}%>
                      </p></td>
                      <td valign="top"><p>
                        <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="10"  type="text" size="6" name="GH_Price_Mod_CT_WWO_26" value="<%=PLCUtils.getDisplayDefaultDecimalFormat2(PracticeMaster.getGH_Price_Mod_CT_WWO_26())%>" />
                        &nbsp;
                        <%if (isShowAudit){%>
                        <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Price_Mod_CT_WWO_26&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("GH_Price_Mod_CT_WWO_26")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align="middle" border="0" src="images/icon_audit.gif" /></a>
                        <%}%>
                      </p></td>
                    </tr>

                    <tr>
                      <td valign="top" nowrap><p class="<%=theClass%>" ><b>PETCT wwo</b></p></td>
                      <td valign="top"><p>
                        <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="10"  type="text" size="6" name="GH_Price_Mod_PETCT_WWO" value="<%=PLCUtils.getDisplayDefaultDecimalFormat2(PracticeMaster.getGH_Price_Mod_PETCT_WWO())%>" />
                        &nbsp;
                        <%if (isShowAudit){%>
                        <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Price_Mod_PETCT_WWO&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("GH_Price_Mod_PETCT_WWO")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align="middle" border="0" src="images/icon_audit.gif" /></a>
                        <%}%>
                      </p></td>
                      <td valign="top"><p>
                        <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="10"  type="text" size="6" name="GH_Price_Mod_PETCT_WWO_TC" value="<%=PLCUtils.getDisplayDefaultDecimalFormat2(PracticeMaster.getGH_Price_Mod_PETCT_WWO_TC())%>" />
                        &nbsp;
                        <%if (isShowAudit){%>
                        <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Price_Mod_PETCT_WWO_TC&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("GH_Price_Mod_PETCT_WWO_TC")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align="middle" border="0" src="images/icon_audit.gif" /></a>
                        <%}%>
                      </p></td>
                      <td valign="top"><p>
                        <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="10"  type="text" size="6" name="GH_Price_Mod_PETCT_WWO_26" value="<%=PLCUtils.getDisplayDefaultDecimalFormat2(PracticeMaster.getGH_Price_Mod_PETCT_WWO_26())%>" />
                        &nbsp;
                        <%if (isShowAudit){%>
                        <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Price_Mod_PETCT_WWO_26&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("GH_Price_Mod_PETCT_WWO_26")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align="middle" border="0" src="images/icon_audit.gif" /></a>
                        <%}%>
                      </p></td>
                    </tr>
					
					
					
					                    <tr class="tdHeaderAlt">
                      <td colspan="4" valign="top" nowrap>GH Backup Pricing</td>
                      </tr>
					 

                    
                     <tr><td valign=top><p class=<%=theClass%> ><b>Backup #1<br>
                       FSID&nbsp;</b></p></td><td colspan="3" valign=top><p><input maxlength="6" type=text size="10" name="GH_FeeScheduleRefID" value="<%=PracticeMaster.getGH_FeeScheduleRefID()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=FeeScheduleRefID&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("GH_FeeScheduleRefID")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%>
                         <br>
                         <span class="<%=theClass%>">1 = State WC</span>                     </p></td>
                      </tr>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Backup #1<br>
                       Percentage:&nbsp;</b></p></td><td colspan="3" valign=top><p><input maxlength=4  type=text size="10" name="GH_FeePercentage" value="<%=PLCUtils.getDisplayDefaultDecimalFormat2(PracticeMaster.getGH_FeePercentage())%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=FeePercentage&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("GH_FeePercentage")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%>
					   </p></td>
                      </tr>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Backup #2<br>
                       FSID&nbsp;</b></p></td><td colspan="3" valign=top><p><input maxlength="20" type=text size="10" name="GH_FeeScheduleOverRefID" value="<%=PracticeMaster.getGH_FeeScheduleOverRefID()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=FeeScheduleOverRefID&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("GH_FeeScheduleOverRefID")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%>
					   <br>
                         <span class="<%=theClass%>">1 = State WC</span>
					   </p></td>
                      </tr>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Backup #2<br>
                       Percentage:&nbsp;</b></p></td><td colspan="3" valign=top><p><input maxlength=10  type=text size="10" name="GH_FeeOverPercentage" value="<%=PLCUtils.getDisplayDefaultDecimalFormat2(PracticeMaster.getGH_FeeOverPercentage())%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=FeeOverPercentage&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("GH_FeeOverPercentage")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td>
                      </tr>
                     
					 
                    <tr class="tdHeaderAlt">
                      <td colspan="4" valign="top" nowrap>GH Other</td>
                      </tr>
					 
					 
					 
<tr><td valign=top><p class=<%=theClass%> ><b>Initial Quota&nbsp;</b></p></td><td colspan="3" valign=top><p><input maxlength="5" type=text size="5" name="GH_InitialQuota" value="<%=PracticeMaster.getGH_InitialQuota()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=InitialQuota&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("GH_InitialQuota")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td>
  </tr>                     

                         <tr><td valign=top><p class=<%=theClass%> ><b>Payment Terms Method&nbsp;</b></p></td><td colspan="3" valign=top><p><input maxlength="10" type=text size="20" name="GH_PaymentTermsMethod" value="<%=PracticeMaster.getGH_PaymentTermsMethod()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PaymentTermsMethod&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("GH_PaymentTermsMethod")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td>
                         </tr>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Payment Terms Net&nbsp;</b></p></td><td colspan="3" valign=top><p><input maxlength="2" type=text size="6" name="GH_PaymentTermsNet" value="<%=PracticeMaster.getGH_PaymentTermsNet()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PaymentTermsNet&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("GH_PaymentTermsNet")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td>
                      </tr>
                      
                      
      <tr><td valign=top><p class=<%=theClass%> ><b>Does Aging?&nbsp;</b></p></td><td   colspan="3" valign=top><p><select   name="GH_DoesAging" ><jsp:include page="../generic/tYesNoLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PracticeMaster.getGH_DoesAging()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=DoesAging&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("GH_DoesAging")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td> 
</tr>             


                        <tr><td valign=top><p class=<%=theClass%> ><b>Diagnostic MD Arthrogram&nbsp;</b></p></td><td valign=top  colspan="3"><p><select   name="GH_DiagnosticMDArthrogram" ><jsp:include page="../generic/tYesNoLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PracticeMaster.getGH_DiagnosticMDArthrogram()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=DiagnosticMDArthrogram&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("GH_DiagnosticMDArthrogram")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td> 
</tr>                
                        <tr><td valign=top><p class=<%=theClass%> ><b>Diagnostic MD Myelogram&nbsp;</b></p></td><td valign=top  colspan="3"><p><select   name="GH_DiagnosticMDMyelogram" ><jsp:include page="../generic/tYesNoLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PracticeMaster.getGH_DiagnosticMDMyelogram()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=DiagnosticMDMyelogram&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("GH_DiagnosticMDMyelogram")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td> 
</tr>                        
                      
                  </table>
                  <!-- End GH -->
                  </td>
                </tr>

                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Department Name&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="50" type=text size="80" name="DepartmentName" value="<%=PracticeMaster.getDepartmentName()%>">
                      &nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=DepartmentName&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("DepartmentName")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>


                        <tr><td valign=top><p class=<%=theClass%> ><b>Contract Global&nbsp;</b></p></td><td valign=top><p><select   name="ContractGlobal" ><jsp:include page="../generic/tYesNoLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PracticeMaster.getContractGlobal()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ContractGlobal&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("ContractGlobal")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>

                        <tr><td valign=top><p class=<%=theClass%> ><b>Contract Tech-Only&nbsp;</b></p></td><td valign=top><p><select   name="ContractTechOnly" ><jsp:include page="../generic/tYesNoLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PracticeMaster.getContractTechOnly()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ContractTechOnly&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("ContractTechOnly")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>

                        <tr><td valign=top><p class=<%=theClass%> ><b>Contract Professional-Only&nbsp;</b></p></td><td valign=top><p><select   name="ContractProfessionalOnly" ><jsp:include page="../generic/tYesNoLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PracticeMaster.getContractProfessionalOnly()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ContractProfessionalOnly&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("ContractProfessionalOnly")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>

<tr><td valign=top><p class=<%=theClass%> ><b>Initial Contract Date:&nbsp;</b></p></td><td valign=top><p><input maxlength=20  type=text size="20" name="InitialContractDate" value='<jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(PracticeMaster.getInitialContractDate())%>" /></jsp:include>' >&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=InitialContractDate&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("InitialContractDate")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr> 

  <tr><td valign=top><p class=<%=theClass%> ><b>Recontract Date:&nbsp;</b></p></td><td valign=top><p><input maxlength=20  type=text size="20" name="ContractDate" value='<jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(PracticeMaster.getContractDate())%>" /></jsp:include>' >&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ContractDate&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("ContractDate")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
  
  
                        <tr><td valign=top><p class=<%=theClass%> ><b>Contracting Status&nbsp;</b></p></td><td valign=top><p><select  onchange="document.getElementById('isSaveMe').value='1';"    name="ContractingStatusID" ><jsp:include page="../generic/tPracticeContractStatusLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PracticeMaster.getContractingStatusID()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ContractingStatusID&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("ContractingStatusID")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
         
         
<tr><td valign=top><p class=<%=theClass%> ><b>Terminated&nbsp;</b></p></td><td valign=top><p><select name="Terminated" ><jsp:include page="../generic/tYesNoLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PracticeMaster.getTerminated()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Terminated&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("Terminated")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>

 <tr><td valign=top><p class=<%=theClass%> ><b>Terminated Date:&nbsp;</b></p></td><td valign=top><p><input maxlength=20  type=text size="20" name="TerminatedDate" value='<jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(PracticeMaster.getTerminatedDate())%>" /></jsp:include>' >&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=TerminatedDate&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("TerminatedDate")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr> 


<tr><td valign=top><p class=<%=theClass%> ><b>Star Rating&nbsp;</b></p></td><td valign=top><p><select   name="IsBlueStar" ><jsp:include page="../generic/starRating.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PracticeMaster.getIsBlueStar()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=IsBlueStar&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("IsBlueStar")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>

                                    
<%
if (false)
{
%>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Owner Name&nbsp;</b></p></td><td valign=top><p><input onChange="document.getElementById('isSaveMe').value='1';" maxlength="100" type=text size="80" name="OwnerName" value="<%=PracticeMaster.getOwnerName()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OwnerName&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OwnerName")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td>
                     </tr>
<%
}
%>


                <tr style="display:none;">
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Notes&nbsp;</b></p>
                  <input name="none1" class="tdBaseAltYellow" type="button" id="none1" onClick="document.getElementById('isSaveMe').value='1'; document.getElementById('Comments').value += '\n[' + Date() + ' - <%=CurrentUserAccount.getLogonUserName()%>]\n';" value="Add Date/Time Stamp"></td>
                  <td valign=top>
                    
                    <p>
                      <textarea  onchange="document.getElementById('isSaveMe').value='1';"   onKeyDown="textAreaStop(this,4500)" rows="6" name="Comments" id="Comments" cols="40"><%=PracticeMaster.getComments()%></textarea>
                      &nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Comments&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("Comments")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
				<tr >
                    <td colspan=3>
                    <p class=<%=theClass%> ><b>CommTracks&nbsp;</b></p>
                        <div style="width:600px;">
                            <textarea style="width: 98%;height: 100px;border-radius: 3px;padding: 5px 7px;resize: none;margin: 0 auto;display: block;margin-bottom: 10px;" name="addnetdevcommtrack" id="addnetdevcommtrack"></textarea>
                            <a href="#" class="netdevcommsumbit">submit</a><a href="#" class="netdevcommtoggle">toggle</a>
						</div>
                    </td>
                </tr>
                <tr>
					<td colspan=3 style="padding:5px;">
                    	<div id="netdevcommtrack" style="width:600px;"></div>
						
                    </td>
				</tr>

<tr><td valign=top><p class=<%=theClass%> ><b>Scheduling Notes:&nbsp;</b></p></td><td valign=top><p><textarea onKeyDown="textAreaStop(this,5000)" rows="6" name="Scheduling_Notes" cols="40" maxlength=5000><%=PracticeMaster.getScheduling_Notes()%></textarea>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Scheduling_Notes&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("Scheduling_Notes")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>


                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Office Address&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="50" type=text size="80" name="OfficeAddress1" value="<%=PracticeMaster.getOfficeAddress1()%>">
                      &nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeAddress1&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeAddress1")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Address 2&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="50" type=text size="80" name="OfficeAddress2" value="<%=PracticeMaster.getOfficeAddress2()%>">
                      &nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeAddress2&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeAddress2")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>City, Town, Province&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="30" type=text size="80" name="OfficeCity" value="<%=PracticeMaster.getOfficeCity()%>">
                      &nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeCity&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeCity")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>State&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <select  onchange="document.getElementById('isSaveMe').value='1';"    name="OfficeStateID" >
                        <jsp:include page="../generic/tStateLILong.jsp" flush="true" >
                        <jsp:param name="CurrentSelection" value="<%=PracticeMaster.getOfficeStateID()%>" />
                        </jsp:include>
                      </select>
                      &nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeStateID&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeStateID")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>ZIP&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <input class="zipCheck" onChange="document.getElementById('isSaveMe').value='1';" maxlength="10" type=text size="80" name="OfficeZIP" value="<%=PracticeMaster.getOfficeZIP()%>">
                      &nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeZIP&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeZIP")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Office Phone Number</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="20" type=text size="80" name="OfficePhone" value="<%=PracticeMaster.getOfficePhone()%>">
                      &nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficePhone&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficePhone")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>


                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Office Fax&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="20" type=text size="80" name="OfficeFaxNo" value="<%=PracticeMaster.getOfficeFaxNo()%>">
                      &nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeFaxNo&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeFaxNo")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>


                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Report Reminder Fax&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="20" type=text size="80" name="FaxReportReminderOverride" value="<%=PracticeMaster.getFaxReportReminderOverride()%>">
                      &nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=FaxReportReminderOverride&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("FaxReportReminderOverride")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                



                
                <tr> 
                  <td colspan=2> 
                    <hr noshade >
                  </td>
                  <td colspan=1>&nbsp; 
                  
                  </td>
                </tr>
                
                
                
                
                <tr>
                  <td valign=top><p class=<%=theClass%> ><b>Office Federal Tax ID&nbsp;</b></p></td>
                  <td valign=top><p>
                    <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="50" type=text size="80" name="OfficeFederalTaxID" value="<%=PracticeMaster.getOfficeFederalTaxID()%>">
                    &nbsp;
                    <%if (isShowAudit){%>
                    <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeFederalTaxID&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeFederalTaxID")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                    <%}%>
                  </p></td>
                  <td valign=top>&nbsp;</td>
                </tr>
                <tr>
                  <td valign=top><p class=<%=theClass%> ><b>Name affiliated with tax ID&nbsp;</b></p></td>
                  <td valign=top><p>
                    <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="900" type=text size="80" name="OfficeTaxIDNameAffiliation" value="<%=PracticeMaster.getOfficeTaxIDNameAffiliation()%>">
                    &nbsp;
                    <%if (isShowAudit){%>
                    <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeTaxIDNameAffiliation&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeTaxIDNameAffiliation")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                    <%}%>
                  </p></td>
                  <td valign=top>&nbsp;</td>
                </tr>
                <tr>
                  <td valign=top><p class=<%=theClass%> ><b>NPI Number&nbsp;</b></p></td>
                  <td valign=top><p>
                    <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="100" type=text size="80" name="NPINumber" value="<%=PracticeMaster.getNPINumber()%>">
                    &nbsp;
                    <%if (isShowAudit){%>
                    <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=NPINumber&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("NPINumber")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                    <%}%>
                  </p></td>
                  <td valign=top>&nbsp;</td>
                </tr>

                
              <tr> 
                  <td colspan=2> 
                    <hr noshade >
                  </td>
                  <td colspan=1>&nbsp; 
                  
                  </td>
                </tr>
                
                
                
                

                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Office Manager First Name&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="50" type=text size="80" name="OfficeManagerFirstName" value="<%=PracticeMaster.getOfficeManagerFirstName()%>">
                      &nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeManagerFirstName&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeManagerFirstName")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                  <td valign=top>&nbsp;</td>
                </tr>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Office Manager Last Name&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="50" type=text size="80" name="OfficeManagerLastName" value="<%=PracticeMaster.getOfficeManagerLastName()%>">
                      &nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeManagerLastName&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeManagerLastName")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                  <td valign=top>&nbsp;</td>
                </tr>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Office Manager Phone&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="20" type=text size="80" name="OfficeManagerPhone" value="<%=PracticeMaster.getOfficeManagerPhone()%>">
                      &nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeManagerPhone&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeManagerPhone")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                  <td valign=top>&nbsp;</td>
                </tr>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Office Manager Email&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="75" type=text size="80" name="OfficeManagerEmail" value="<%=PracticeMaster.getOfficeManagerEmail()%>">
                      &nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeManagerEmail&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeManagerEmail")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                  <td valign=top>&nbsp;</td>
                </tr>


                <tr>
                  <td valign=top>&nbsp;</td>
                  <td valign=top>&nbsp;</td>
                  <td valign=top>&nbsp;</td>
                </tr>

                <tr> 
                  <td colspan=3> 
                    <hr noshade>
                  </td>
                </tr>
                <tr class=title> 
                  <td colspan=3>Billing Information <span class=tdBase>(<a href="#" onClick="return BillingFill()">Click 
                    here</a> if this is the same as the Practice Office Information)</span></td>
                <tr>
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Make Checks Payable To&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="50" type=text size="80" name="BillingPayableTo" value="<%=PracticeMaster.getBillingPayableTo()%>">
                      &nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=BillingPayableTo&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("BillingPayableTo")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                  <td valign=top>&nbsp;</td>
                </tr>
                <tr><td valign=top><p class=<%=theClass%> ><b>Billing Address Company Name&nbsp;</b></p></td><td valign=top><p><input onChange="document.getElementById('isSaveMe').value='1';" maxlength="75" type=text size="80" name="BillingAddressName" value="<%=PracticeMaster.getBillingAddressName()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=BillingAddressName&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("BillingAddressName")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td>
                  <td valign=top>&nbsp;</td>
                </tr>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Billing Contact First Name&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="50" type=text size="80" name="BillingFirstName" value="<%=PracticeMaster.getBillingFirstName()%>">
                      &nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=BillingFirstName&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("BillingFirstName")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                  <td valign=top>&nbsp;</td>
                </tr>
                <tr>
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Billing Contact Last Name&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="50" type=text size="80" name="BillingLastName" value="<%=PracticeMaster.getBillingLastName()%>">
                      &nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=BillingLastName&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("BillingLastName")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                  <td valign=top>&nbsp;</td>
                </tr>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Address&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="50" type=text size="80" name="BillingAddress1" value="<%=PracticeMaster.getBillingAddress1()%>">
                      &nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=BillingAddress1&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("BillingAddress1")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                  <td valign=top>&nbsp;</td>
                </tr>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Address 2&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="20" type=text size="80" name="BillingAddress2" value="<%=PracticeMaster.getBillingAddress2()%>">
                      &nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=BillingAddress2&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("BillingAddress2")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                  <td valign=top>&nbsp;</td>
                </tr>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>City&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="30" type=text size="80" name="BillingCity" value="<%=PracticeMaster.getBillingCity()%>">
                      &nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=BillingCity&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("BillingCity")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                  <td valign=top>&nbsp;</td>
                </tr>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>State&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <select  onchange="document.getElementById('isSaveMe').value='1';"    name="BillingStateID" >
                        <jsp:include page="../generic/tStateLILong.jsp" flush="true" >
                        <jsp:param name="CurrentSelection" value="<%=PracticeMaster.getBillingStateID()%>" />
                        </jsp:include>
                      </select>
                      &nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=BillingStateID&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("BillingStateID")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                  <td valign=top>&nbsp;</td>
                </tr>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>ZIP&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <input class="zipCheck2" onChange="document.getElementById('isSaveMe').value='1';" maxlength="10" type=text size="80" name="BillingZIP" value="<%=PracticeMaster.getBillingZIP()%>">
                      &nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=BillingZIP&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("BillingZIP")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                  <td valign=top>&nbsp;</td>
                </tr>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Phone&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="20" type=text size="80" name="BillingPhone" value="<%=PracticeMaster.getBillingPhone()%>">
                      &nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=BillingPhone&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("BillingPhone")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                  <td valign=top>&nbsp;</td>
                </tr>
                <tr>
                  <td valign=top><p class=<%=theClass%> ><b>Fax&nbsp;</b></p></td>
                  <td valign=top><p>
                    <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="20" type=text size="80" name="BillingFax" value="<%=PracticeMaster.getBillingFax()%>">
                    &nbsp;
                    <%if (isShowAudit){%>
                    <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=BillingFax&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("BillingFax")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                    <%}%>
                  </p></td>
                  <td valign=top>&nbsp;</td>
                </tr>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Email&nbsp;</b></p></td><td valign=top><p><input onChange="document.getElementById('isSaveMe').value='1';" maxlength="50" type=text size="80" name="BillingEmail" value="<%=PracticeMaster.getBillingEmail()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=BillingEmail&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("BillingEmail")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td>                  <td valign=top>&nbsp;</td>
</tr>
                <tr> 
                  <td colspan=3> 
                    <hr noshade>
                  </td>
                </tr>
                <tr class=title> 
                  <td colspan=3>Credentialing Contact <span class=tdBase>(<a href="#" onClick="return CredFill()">Click 
                    here</a> if this is the same as the Practice Office Information)</span></td>
                <tr>
                  <td valign=top><p class=<%=theClass%> ><b> Company Name&nbsp;</b></p></td>
                  <td valign=top><p>
                    <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="75" type=text size="80" name="CredentialingContactAddressName" value="<%=PracticeMaster.getCredentialingContactAddressName()%>">
                    &nbsp;
                    <%if (isShowAudit){%>
                    <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CredentialingContactAddressName&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("CredentialingContactAddressName")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                    <%}%>
                  </p></td>
                  <td valign=top>&nbsp;</td>
                </tr>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>First Name&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="50" type=text size="80" name="CredentialingContactFirstName" value="<%=PracticeMaster.getCredentialingContactFirstName()%>">
                      &nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CredentialingContactFirstName&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("CredentialingContactFirstName")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                  <td valign=top>&nbsp;</td>
                </tr>
                <tr>
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Last Name&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="50" type=text size="80" name="CredentialingContactLastName" value="<%=PracticeMaster.getCredentialingContactLastName()%>">
                      &nbsp;
                      <%if (isShowAudit){%>
,                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CredentialingContactLastName&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("CredentialingContactLastName")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                  <td valign=top>&nbsp;</td>
                </tr>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Address&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="50" type=text size="80" name="CredentialingContactAddress1" value="<%=PracticeMaster.getCredentialingContactAddress1()%>">
                      &nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CredentialingContactAddress1&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("CredentialingContactAddress1")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                  <td valign=top>&nbsp;</td>
                </tr>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Address 2&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="20" type=text size="80" name="CredentialingContactAddress2" value="<%=PracticeMaster.getCredentialingContactAddress2()%>">
                      &nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CredentialingContactAddress2&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("CredentialingContactAddress2")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                  <td valign=top>&nbsp;</td>
                </tr>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>City&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="30" type=text size="80" name="CredentialingContactCity" value="<%=PracticeMaster.getCredentialingContactCity()%>">
                      &nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CredentialingContactCity&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("CredentialingContactCity")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                  <td valign=top>&nbsp;</td>
                </tr>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>State&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <select  onchange="document.getElementById('isSaveMe').value='1';"    name="CredentialingContactStateID" >
                        <jsp:include page="../generic/tStateLILong.jsp" flush="true" >
                        <jsp:param name="CurrentSelection" value="<%=PracticeMaster.getCredentialingContactStateID()%>" />
                        </jsp:include>
                      </select>
                      &nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CredentialingContactStateID&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("CredentialingContactStateID")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                  <td valign=top>&nbsp;</td>
                </tr>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>ZIP&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <input class="zipCheck3" onChange="document.getElementById('isSaveMe').value='1';" maxlength="50" type=text size="80" name="CredentialingContactZIP" value="<%=PracticeMaster.getCredentialingContactZIP()%>">
                      &nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CredentialingContactZIP&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("CredentialingContactZIP")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                  <td valign=top>&nbsp;</td>
                </tr>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Phone&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="50" type=text size="80" name="CredentialingContactPhone" value="<%=PracticeMaster.getCredentialingContactPhone()%>">
                      &nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CredentialingContactPhone&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("CredentialingContactPhone")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                  <td valign=top>&nbsp;</td>
                </tr>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Fax&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="50" type=text size="80" name="CredentialingContactFax" value="<%=PracticeMaster.getCredentialingContactFax()%>">
                      &nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CredentialingContactFax&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("CredentialingContactFax")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                  <td valign=top>&nbsp;</td>
                </tr>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Email&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="100" type=text size="80" name="CredentiallingContactEmail" value="<%=PracticeMaster.getCredentiallingContactEmail()%>">
                      &nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CredentiallingContactEmail&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("CredentiallingContactEmail")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                  <td valign=top>&nbsp;</td>
                </tr>
                <tr>
                  <td valign=top><p class=<%=theClass%> ><b>State License Number&nbsp;</b></p></td>
                  <td valign=top><p>
                    <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="100" type=text size="80" name="StateLicenseNumber" value="<%=PracticeMaster.getStateLicenseNumber()%>">
                    &nbsp;
                    <%if (isShowAudit){%>
                    <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=StateLicenseNumber&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("StateLicenseNumber")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                    <%}%>
                  </p></td>
                  <td valign=top>&nbsp;</td>
                </tr>
                <tr>
                  <td valign=top><p class=<%=theClass%> ><b>Medicare License Number&nbsp;</b></p></td>
                  <td valign=top><p>
                    <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="100" type=text size="80" name="MedicareLicenseNumber" value="<%=PracticeMaster.getMedicareLicenseNumber()%>">
                    &nbsp;
                    <%if (isShowAudit){%>
                    <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=MedicareLicenseNumber&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("MedicareLicenseNumber")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                    <%}%>
                  </p></td>
                  <td valign=top>&nbsp;</td>
                </tr>
                <tr>
                  <td valign=top><p class=<%=theClass%> ><b>General Liability Carrier&nbsp;</b></p></td>
                  <td valign=top><p>
                    <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="900" type=text size="80" name="MalPracticePolicyCarrier" value="<%=PracticeMaster.getMalPracticePolicyCarrier()%>">
                    &nbsp;
                    <%if (isShowAudit){%>
                    <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=MalPracticePolicyCarrier&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("MalPracticePolicyCarrier")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                    <%}%>
                  </p></td>
                  <td valign=top>&nbsp;</td>
                </tr>
                <tr>
                  <td valign=top><p class=<%=theClass%> ><b>General Liability Policy#&nbsp;</b></p></td>
                  <td valign=top><p>
                    <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="100" type=text size="80" name="MalPracticePolicyNumber" value="<%=PracticeMaster.getMalPracticePolicyNumber()%>">
                    &nbsp;
                    <%if (isShowAudit){%>
                    <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=MalPracticePolicyNumber&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("MalPracticePolicyNumber")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                    <%}%>
                  </p></td>
                  <td valign=top>&nbsp;</td>
                </tr>
                <tr>
                  <td valign=top><p class=<%=theClass%> ><b>General Liability Expiration Date</b></p></td>
                  <td valign=top><p>
                    <input onChange="document.getElementById('isSaveMe').value='1';" maxlength=10  type=text size="80" name="MalPracticePolicyExpDate" value='<jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(PracticeMaster.getMalPracticePolicyExpDate())%>" /></jsp:include>' >
                    &nbsp;
                    <%if (isShowAudit){%>
                    <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=MalPracticePolicyExpDate&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("MalPracticePolicyExpDate")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                    <%}%>
                  </p></td>
                  <td valign=top>&nbsp;</td>
                </tr>
                <tr>
                  <td valign=top><p class=<%=theClass%> ><b>Walk-ins?</b></p></td>
                  <td valign=top><p>
                    <select  onchange="document.getElementById('isSaveMe').value='1';"    name="WalkinMRICT" >
                      <jsp:include page="../generic/tYesNoLILong.jsp" flush="true" >
                        <jsp:param name="CurrentSelection" value="<%=PracticeMaster.getWalkinMRICT()%>" />                
                        </jsp:include>
                    </select>
                    &nbsp;
                    <%if (isShowAudit){%>
                    <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=WalkinMRICT&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("WalkinMRICT")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                    <%}%>
                  </p></td>
                  <td valign=top>&nbsp;</td>
                </tr>
                <tr>
                  <td valign=top><p class=<%=theClass%> ><b>Diagnostic Tech-Only?</b></p></td>
                  <td valign=top><p>
                    <select  onchange="document.getElementById('isSaveMe').value='1';"    name="DiagnosticTechOnly" >
                      <jsp:include page="../generic/tYesNoLILong.jsp" flush="true" >
                        <jsp:param name="CurrentSelection" value="<%=PracticeMaster.getDiagnosticTechOnly()%>" />                
                        </jsp:include>
                    </select>
                    &nbsp;
                    <%if (isShowAudit){%>
                    <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=DiagnosticTechOnly&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("DiagnosticTechOnly")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                    <%}%>
                  </p></td>
                  <td valign=top>&nbsp;</td>
                </tr>
                <tr>
                  <td valign=top><p class=<%=theClass%> ><b>DICOM Forward?</b></p></td>
                  <td valign=top><p>
                    <select  onchange="document.getElementById('isSaveMe').value='1';"    name="DICOMForward" >
                      <jsp:include page="../generic/tYesNoLILong.jsp" flush="true" >
                        <jsp:param name="CurrentSelection" value="<%=PracticeMaster.getDICOMForward()%>" />                
                        </jsp:include>
                    </select>
                    &nbsp;
                    <%if (isShowAudit){%>
                    <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=DICOMForward&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("DICOMForward")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                    <%}%>
                  </p></td>
                  <td valign=top>&nbsp;</td>
                </tr>
                <tr>
                  <td valign=top><p class=<%=theClass%> ><b>Diagnostic After Hours&nbsp;</b></p></td>
                  <td valign=top><p>
                    <select  onchange="document.getElementById('isSaveMe').value='1';"    name="DiagnosticAfterHours" >
                      <jsp:include page="../generic/tYesNoLILong.jsp" flush="true" >
                        <jsp:param name="CurrentSelection" value="<%=PracticeMaster.getDiagnosticAfterHours()%>" />                
                        </jsp:include>
                    </select>
                    &nbsp;
                    <%if (isShowAudit){%>
                    <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=DiagnosticAfterHours&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("DiagnosticAfterHours")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                    <%}%>
                  </p></td>
                  <td valign=top>&nbsp;</td>
                </tr>


 <tr><td valign=top><p class=<%=theClass%> ><b>PETLinQ ID&nbsp;</b></p></td><td valign=top><p><input maxlength="10" type=text size="10" name="PetLinQID" value="<%=PracticeMaster.getPetLinQID()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PetLinQID&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("PetLinQID")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>




  <tr><td valign=top><p class=<%=theClass%> ><b>PACS SystemVendor&nbsp;</b></p></td><td valign=top><p>
    <input maxlength="100" type=text size="80" name="PACSSystemVendor" value="<%=PracticeMaster.getPACSSystemVendor()%>">
    &nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PACSSystemVendor&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("PACSSystemVendor")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td>                  
  <td valign=top>&nbsp;</td>
</tr>

                     <tr><td valign=top><p class=<%=theClass%> ><b>RIS System Vendor&nbsp;</b></p></td><td valign=top><p><input maxlength="100" type=text size="80" name="RISSystemVendor" value="<%=PracticeMaster.getRISSystemVendor()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=RISSystemVendor&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("RISSystemVendor")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td> <td valign=top>&nbsp;</td></tr>

                <tr>
                  <td valign=top>&nbsp;</td>
                  <td valign=top>&nbsp;</td>
                  <td valign=top>&nbsp;</td>
                </tr>

                                

                <tr>
                  <td valign=top>&nbsp;</td>
                  <td valign=top>&nbsp;</td>
                  <td valign=top>&nbsp;</td>
                </tr>


                <tr> 
                  <td colspan=3> 
                    <hr noshade>
                  </td>
                </tr>
                <!--
                <tr class=title> 
                  <td colspan=3>Office Hours:</td>
                </tr>
                <tr class=title> 
                  <td colspan=3>&nbsp;</td>
                </tr>
                <tr> 
                  <td colspan=3> 
                    <table class=tdBase border=1 bordercolor="#333333" cellpadding="2" cellspacing="0" align="left">
                      <tr class=tdHeaderAlt> 
                        <td colspan=1> 
                          <div align="center">&nbsp;</div>
                        </td>
                        <td colspan=2> 
                          <div align="center">Normal Hours </div>
                        </td>
                        <td colspan=2><div align="center">After Hours </div></td>
                        <td colspan=2><div align="center"> MD - Arthrogram</div></td>
                        <td colspan=2> 
                          <div align="center"> MD - Myelogram </div>
                        </td>
                      </tr >
                      <tr class=tdBaseAlt> 
                        <td>Day</td>
                        <td> Open </td>
                        <td> Close </td>
                        <td> Open </td>
                        <td> Close </td>
                        <td> Open </td>
                        <td> Close </td>
                        <td> Open </td>
                        <td> Close </td>
                      </tr >
                      <tr> 
                        <td class=tdBaseAlt> 
                          <table class=tdBaseAlt  border="0" cellspacing="0" cellpadding="0" >
                            <tr> 
                              <td height="21">Monday</td>
                            </tr>
                            <tr> 
                              <td height="21">Tuesday</td>
                            </tr>
                            <tr> 
                              <td height="21">Wednesday</td>
                            </tr>
                            <tr> 
                              <td height="21">Thursday</td>
                            </tr>
                            <tr> 
                              <td height="21">Friday</td>
                            </tr>
                            <tr> 
                              <td height="21">Saturday</td>
                            </tr>
                            <tr> 
                              <td height="21">Sunday</td>
                            </tr>
                          </table>
                        </td>
                        <td> 
                          <table class=tdBase  border=0 cellpadding="0" cellspacing="0">
                            <%
            if ( (PracticeMaster.isRequired("OfficeWorkHoursOpenMonday",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeWorkHoursOpenMonday")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeWorkHoursOpenMonday",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeWorkHoursOpenMonday",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                            <%
            if ((PracticeMaster.isWrite("OfficeWorkHoursOpenMonday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p> 
                                  <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="10" type=text size="8" name="OfficeWorkHoursOpenMonday" value="<%=PracticeMaster.getOfficeWorkHoursOpenMonday()%>">
                                  &nbsp; 
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeWorkHoursOpenMonday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeWorkHoursOpenMonday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else if ((PracticeMaster.isRead("OfficeWorkHoursOpenMonday",UserSecurityGroupID)))
            {
                        %>
                            <%
            }
            else
            {
                        %>
                            <%
            }
            %>
                            <%
            if ( (PracticeMaster.isRequired("OfficeWorkHoursOpenTuesday",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeWorkHoursOpenTuesday")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeWorkHoursOpenTuesday",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeWorkHoursOpenTuesday",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                            <%
            if ((PracticeMaster.isWrite("OfficeWorkHoursOpenTuesday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p> 
                                  <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="10" type=text size="8" name="OfficeWorkHoursOpenTuesday" value="<%=PracticeMaster.getOfficeWorkHoursOpenTuesday()%>">
                                  &nbsp; 
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeWorkHoursOpenTuesday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeWorkHoursOpenTuesday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else if ((PracticeMaster.isRead("OfficeWorkHoursOpenTuesday",UserSecurityGroupID)))
            {
                        %>
                            <%
            }
            else
            {
                        %>
                            <%
            }
            %>
                            <%
            if ( (PracticeMaster.isRequired("OfficeWorkHoursOpenWednesday",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeWorkHoursOpenWednesday")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeWorkHoursOpenWednesday",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeWorkHoursOpenWednesday",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                            <%
            if ((PracticeMaster.isWrite("OfficeWorkHoursOpenWednesday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p> 
                                  <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="10" type=text size="8" name="OfficeWorkHoursOpenWednesday" value="<%=PracticeMaster.getOfficeWorkHoursOpenWednesday()%>">
                                  &nbsp; 
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeWorkHoursOpenWednesday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeWorkHoursOpenWednesday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else if ((PracticeMaster.isRead("OfficeWorkHoursOpenWednesday",UserSecurityGroupID)))
            {
                        %>
                            <%
            }
            else
            {
                        %>
                            <%
            }
            %>
                            <%
            if ( (PracticeMaster.isRequired("OfficeWorkHoursOpenThursday",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeWorkHoursOpenThursday")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeWorkHoursOpenThursday",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeWorkHoursOpenThursday",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                            <%
            if ((PracticeMaster.isWrite("OfficeWorkHoursOpenThursday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p> 
                                  <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="10" type=text size="8" name="OfficeWorkHoursOpenThursday" value="<%=PracticeMaster.getOfficeWorkHoursOpenThursday()%>">
                                  &nbsp; 
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeWorkHoursOpenThursday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeWorkHoursOpenThursday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else if ((PracticeMaster.isRead("OfficeWorkHoursOpenThursday",UserSecurityGroupID)))
            {
                        %>
                            <%
            }
            else
            {
                        %>
                            <%
            }
            %>
                            <%
            if ( (PracticeMaster.isRequired("OfficeWorkHoursOpenFriday",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeWorkHoursOpenFriday")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeWorkHoursOpenFriday",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeWorkHoursOpenFriday",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                            <%
            if ((PracticeMaster.isWrite("OfficeWorkHoursOpenFriday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p> 
                                  <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="10" type=text size="8" name="OfficeWorkHoursOpenFriday" value="<%=PracticeMaster.getOfficeWorkHoursOpenFriday()%>">
                                  &nbsp; 
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeWorkHoursOpenFriday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeWorkHoursOpenFriday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else if ((PracticeMaster.isRead("OfficeWorkHoursOpenFriday",UserSecurityGroupID)))
            {
                        %>
                            <%
            }
            else
            {
                        %>
                            <%
            }
            %>
                            <%
            if ( (PracticeMaster.isRequired("OfficeWorkHoursOpenSaturday",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeWorkHoursOpenSaturday")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeWorkHoursOpenSaturday",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeWorkHoursOpenSaturday",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                            <%
            if ((PracticeMaster.isWrite("OfficeWorkHoursOpenSaturday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p> 
                                  <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="10" type=text size="8" name="OfficeWorkHoursOpenSaturday" value="<%=PracticeMaster.getOfficeWorkHoursOpenSaturday()%>">
                                  &nbsp; 
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeWorkHoursOpenSaturday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeWorkHoursOpenSaturday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else if ((PracticeMaster.isRead("OfficeWorkHoursOpenSaturday",UserSecurityGroupID)))
            {
                        %>
                            <%
            }
            else
            {
                        %>
                            <%
            }
            %>
                            <%
            if ( (PracticeMaster.isRequired("OfficeWorkHoursOpenSunday",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeWorkHoursOpenSunday")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeWorkHoursOpenSunday",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeWorkHoursOpenSunday",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                            <%
            if ((PracticeMaster.isWrite("OfficeWorkHoursOpenSunday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p> 
                                  <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="10" type=text size="8" name="OfficeWorkHoursOpenSunday" value="<%=PracticeMaster.getOfficeWorkHoursOpenSunday()%>">
                                  &nbsp; 
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeWorkHoursOpenSunday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeWorkHoursOpenSunday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else if ((PracticeMaster.isRead("OfficeWorkHoursOpenSunday",UserSecurityGroupID)))
            {
                        %>
                            <%
            }
            else
            {
                        %>
                            <%
            }
            %>
                          </table>
                        </td>
                        <td> 
                          <table  class=tdBase border=0 cellpadding="0" cellspacing="0">
                            <%
            if ( (PracticeMaster.isRequired("OfficeWorkHoursCloseMonday",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeWorkHoursCloseMonday")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeWorkHoursCloseMonday",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeWorkHoursCloseMonday",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                            <%
            if ((PracticeMaster.isWrite("OfficeWorkHoursCloseMonday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p> 
                                  <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="10" type=text size="8" name="OfficeWorkHoursCloseMonday" value="<%=PracticeMaster.getOfficeWorkHoursCloseMonday()%>">
                                  &nbsp; 
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeWorkHoursCloseMonday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeWorkHoursCloseMonday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else if ((PracticeMaster.isRead("OfficeWorkHoursCloseMonday",UserSecurityGroupID)))
            {
                        %>
                            <%
            }
            else
            {
                        %>
                            <%
            }
            %>
                            <%
            if ( (PracticeMaster.isRequired("OfficeWorkHoursCloseTuesday",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeWorkHoursCloseTuesday")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeWorkHoursCloseTuesday",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeWorkHoursCloseTuesday",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                            <%
            if ((PracticeMaster.isWrite("OfficeWorkHoursCloseTuesday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p> 
                                  <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="10" type=text size="8" name="OfficeWorkHoursCloseTuesday" value="<%=PracticeMaster.getOfficeWorkHoursCloseTuesday()%>">
                                  &nbsp; 
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeWorkHoursCloseTuesday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeWorkHoursCloseTuesday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else if ((PracticeMaster.isRead("OfficeWorkHoursCloseTuesday",UserSecurityGroupID)))
            {
                        %>
                            <%
            }
            else
            {
                        %>
                            <%
            }
            %>
                            <%
            if ( (PracticeMaster.isRequired("OfficeWorkHoursCloseWednesday",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeWorkHoursCloseWednesday")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeWorkHoursCloseWednesday",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeWorkHoursCloseWednesday",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                            <%
            if ((PracticeMaster.isWrite("OfficeWorkHoursCloseWednesday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p> 
                                  <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="10" type=text size="8" name="OfficeWorkHoursCloseWednesday" value="<%=PracticeMaster.getOfficeWorkHoursCloseWednesday()%>">
                                  &nbsp; 
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeWorkHoursCloseWednesday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeWorkHoursCloseWednesday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else if ((PracticeMaster.isRead("OfficeWorkHoursCloseWednesday",UserSecurityGroupID)))
            {
                        %>
                            <%
            }
            else
            {
                        %>
                            <%
            }
            %>
                            <%
            if ( (PracticeMaster.isRequired("OfficeWorkHoursCloseThursday",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeWorkHoursCloseThursday")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeWorkHoursCloseThursday",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeWorkHoursCloseThursday",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                            <%
            if ((PracticeMaster.isWrite("OfficeWorkHoursCloseThursday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p> 
                                  <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="10" type=text size="8" name="OfficeWorkHoursCloseThursday" value="<%=PracticeMaster.getOfficeWorkHoursCloseThursday()%>">
                                  &nbsp; 
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeWorkHoursCloseThursday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeWorkHoursCloseThursday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else if ((PracticeMaster.isRead("OfficeWorkHoursCloseThursday",UserSecurityGroupID)))
            {
                        %>
                            <%
            }
            else
            {
                        %>
                            <%
            }
            %>
                            <%
            if ( (PracticeMaster.isRequired("OfficeWorkHoursCloseFriday",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeWorkHoursCloseFriday")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeWorkHoursCloseFriday",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeWorkHoursCloseFriday",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                            <%
            if ((PracticeMaster.isWrite("OfficeWorkHoursCloseFriday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p> 
                                  <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="10" type=text size="8" name="OfficeWorkHoursCloseFriday" value="<%=PracticeMaster.getOfficeWorkHoursCloseFriday()%>">
                                  &nbsp; 
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeWorkHoursCloseFriday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeWorkHoursCloseFriday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else if ((PracticeMaster.isRead("OfficeWorkHoursCloseFriday",UserSecurityGroupID)))
            {
                        %>
                            <%
            }
            else
            {
                        %>
                            <%
            }
            %>
                            <%
            if ( (PracticeMaster.isRequired("OfficeWorkHoursCloseSaturday",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeWorkHoursCloseSaturday")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeWorkHoursCloseSaturday",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeWorkHoursCloseSaturday",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                            <%
            if ((PracticeMaster.isWrite("OfficeWorkHoursCloseSaturday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p> 
                                  <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="10" type=text size="8" name="OfficeWorkHoursCloseSaturday" value="<%=PracticeMaster.getOfficeWorkHoursCloseSaturday()%>">
                                  &nbsp; 
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeWorkHoursCloseSaturday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeWorkHoursCloseSaturday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else if ((PracticeMaster.isRead("OfficeWorkHoursCloseSaturday",UserSecurityGroupID)))
            {
                        %>
                            <%
            }
            else
            {
                        %>
                            <%
            }
            %>
                            <%
            if ( (PracticeMaster.isRequired("OfficeWorkHoursCloseSunday",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeWorkHoursCloseSunday")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeWorkHoursCloseSunday",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeWorkHoursCloseSunday",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                            <%
            if ((PracticeMaster.isWrite("OfficeWorkHoursCloseSunday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p> 
                                  <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="10" type=text size="8" name="OfficeWorkHoursCloseSunday" value="<%=PracticeMaster.getOfficeWorkHoursCloseSunday()%>">
                                  &nbsp; 
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeWorkHoursCloseSunday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeWorkHoursCloseSunday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else if ((PracticeMaster.isRead("OfficeWorkHoursCloseSunday",UserSecurityGroupID)))
            {
                        %>
                            <%
            }
            else
            {
                        %>
                            <%
            }
            %>
                          </table>
                        </td>
                        <td><table class=tdBase  border=0 cellpadding="0" cellspacing="0">
                          <%
            if ( (PracticeMaster.isRequired("OfficeAfterHoursOpenMonday",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeAfterHoursOpenMonday")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeAfterHoursOpenMonday",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeAfterHoursOpenMonday",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                          <%
            if ((PracticeMaster.isWrite("OfficeAfterHoursOpenMonday",UserSecurityGroupID)))
            {
                        %>
                          <tr>
                            <td valign=top><p>
                              <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="10" type=text size="8" name="OfficeAfterHoursOpenMonday" value="<%=PracticeMaster.getOfficeAfterHoursOpenMonday()%>">
                              &nbsp;
                              <%if (isShowAudit){%>
                              <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeAfterHoursOpenMonday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeAfterHoursOpenMonday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img src=images/icon_audit.gif alt="" border=0 align=middle></a>
                              <%}%>
                            </p></td>
                          </tr>
                          <%
            }
            else if ((PracticeMaster.isRead("OfficeAfterHoursOpenMonday",UserSecurityGroupID)))
            {
                        %>
                          <%
            }
            else
            {
                        %>
                          <%
            }
            %>
                          <%
            if ( (PracticeMaster.isRequired("OfficeAfterHoursOpenTuesday",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeAfterHoursOpenTuesday")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeAfterHoursOpenTuesday",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeAfterHoursOpenTuesday",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                          <%
            if ((PracticeMaster.isWrite("OfficeAfterHoursOpenTuesday",UserSecurityGroupID)))
            {
                        %>
                          <tr>
                            <td valign=top><p>
                              <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="10" type=text size="8" name="OfficeAfterHoursOpenTuesday" value="<%=PracticeMaster.getOfficeAfterHoursOpenTuesday()%>">
                              &nbsp;
                              <%if (isShowAudit){%>
                              <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeAfterHoursOpenTuesday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeAfterHoursOpenTuesday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img src=images/icon_audit.gif alt="" border=0 align=middle></a>
                              <%}%>
                            </p></td>
                          </tr>
                          <%
            }
            else if ((PracticeMaster.isRead("OfficeAfterHoursOpenTuesday",UserSecurityGroupID)))
            {
                        %>
                          <%
            }
            else
            {
                        %>
                          <%
            }
            %>
                          <%
            if ( (PracticeMaster.isRequired("OfficeAfterHoursOpenWednesday",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeAfterHoursOpenWednesday")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeAfterHoursOpenWednesday",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeAfterHoursOpenWednesday",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                          <%
            if ((PracticeMaster.isWrite("OfficeAfterHoursOpenWednesday",UserSecurityGroupID)))
            {
                        %>
                          <tr>
                            <td valign=top><p>
                              <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="10" type=text size="8" name="OfficeAfterHoursOpenWednesday" value="<%=PracticeMaster.getOfficeAfterHoursOpenWednesday()%>">
                              &nbsp;
                              <%if (isShowAudit){%>
                              <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeAfterHoursOpenWednesday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeAfterHoursOpenWednesday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img src=images/icon_audit.gif alt="" border=0 align=middle></a>
                              <%}%>
                            </p></td>
                          </tr>
                          <%
            }
            else if ((PracticeMaster.isRead("OfficeAfterHoursOpenWednesday",UserSecurityGroupID)))
            {
                        %>
                          <%
            }
            else
            {
                        %>
                          <%
            }
            %>
                          <%
            if ( (PracticeMaster.isRequired("OfficeAfterHoursOpenThursday",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeAfterHoursOpenThursday")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeAfterHoursOpenThursday",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeAfterHoursOpenThursday",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                          <%
            if ((PracticeMaster.isWrite("OfficeAfterHoursOpenThursday",UserSecurityGroupID)))
            {
                        %>
                          <tr>
                            <td valign=top><p>
                              <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="10" type=text size="8" name="OfficeAfterHoursOpenThursday" value="<%=PracticeMaster.getOfficeAfterHoursOpenThursday()%>">
                              &nbsp;
                              <%if (isShowAudit){%>
                              <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeAfterHoursOpenThursday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeAfterHoursOpenThursday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img src=images/icon_audit.gif alt="" border=0 align=middle></a>
                              <%}%>
                            </p></td>
                          </tr>
                          <%
            }
            else if ((PracticeMaster.isRead("OfficeAfterHoursOpenThursday",UserSecurityGroupID)))
            {
                        %>
                          <%
            }
            else
            {
                        %>
                          <%
            }
            %>
                          <%
            if ( (PracticeMaster.isRequired("OfficeAfterHoursOpenFriday",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeAfterHoursOpenFriday")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeAfterHoursOpenFriday",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeAfterHoursOpenFriday",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                          <%
            if ((PracticeMaster.isWrite("OfficeAfterHoursOpenFriday",UserSecurityGroupID)))
            {
                        %>
                          <tr>
                            <td valign=top><p>
                              <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="10" type=text size="8" name="OfficeAfterHoursOpenFriday" value="<%=PracticeMaster.getOfficeAfterHoursOpenFriday()%>">
                              &nbsp;
                              <%if (isShowAudit){%>
                              <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeAfterHoursOpenFriday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeAfterHoursOpenFriday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img src=images/icon_audit.gif alt="" border=0 align=middle></a>
                              <%}%>
                            </p></td>
                          </tr>
                          <%
            }
            else if ((PracticeMaster.isRead("OfficeAfterHoursOpenFriday",UserSecurityGroupID)))
            {
                        %>
                          <%
            }
            else
            {
                        %>
                          <%
            }
            %>
                          <%
            if ( (PracticeMaster.isRequired("OfficeAfterHoursOpenSaturday",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeAfterHoursOpenSaturday")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeAfterHoursOpenSaturday",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeAfterHoursOpenSaturday",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                          <%
            if ((PracticeMaster.isWrite("OfficeAfterHoursOpenSaturday",UserSecurityGroupID)))
            {
                        %>
                          <tr>
                            <td valign=top><p>
                              <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="10" type=text size="8" name="OfficeAfterHoursOpenSaturday" value="<%=PracticeMaster.getOfficeAfterHoursOpenSaturday()%>">
                              &nbsp;
                              <%if (isShowAudit){%>
                              <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeAfterHoursOpenSaturday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeAfterHoursOpenSaturday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img src=images/icon_audit.gif alt="" border=0 align=middle></a>
                              <%}%>
                            </p></td>
                          </tr>
                          <%
            }
            else if ((PracticeMaster.isRead("OfficeAfterHoursOpenSaturday",UserSecurityGroupID)))
            {
                        %>
                          <%
            }
            else
            {
                        %>
                          <%
            }
            %>
                          <%
            if ( (PracticeMaster.isRequired("OfficeAfterHoursOpenSunday",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeAfterHoursOpenSunday")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeAfterHoursOpenSunday",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeAfterHoursOpenSunday",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                          <%
            if ((PracticeMaster.isWrite("OfficeAfterHoursOpenSunday",UserSecurityGroupID)))
            {
                        %>
                          <tr>
                            <td valign=top><p>
                              <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="10" type=text size="8" name="OfficeAfterHoursOpenSunday" value="<%=PracticeMaster.getOfficeAfterHoursOpenSunday()%>">
                              &nbsp;
                              <%if (isShowAudit){%>
                              <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeAfterHoursOpenSunday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeAfterHoursOpenSunday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img src=images/icon_audit.gif alt="" border=0 align=middle></a>
                              <%}%>
                            </p></td>
                          </tr>
                          <%
            }
            else if ((PracticeMaster.isRead("OfficeAfterHoursOpenSunday",UserSecurityGroupID)))
            {
                        %>
                          <%
            }
            else
            {
                        %>
                          <%
            }
            %>
                        </table></td>
                        <td><table  class=tdBase border=0 cellpadding="0" cellspacing="0">
                          <%
            if ( (PracticeMaster.isRequired("OfficeAfterHoursCloseMonday",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeAfterHoursCloseMonday")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeAfterHoursCloseMonday",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeAfterHoursCloseMonday",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                          <%
            if ((PracticeMaster.isWrite("OfficeAfterHoursCloseMonday",UserSecurityGroupID)))
            {
                        %>
                          <tr>
                            <td valign=top><p>
                              <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="10" type=text size="8" name="OfficeAfterHoursCloseMonday" value="<%=PracticeMaster.getOfficeAfterHoursCloseMonday()%>">
                              &nbsp;
                              <%if (isShowAudit){%>
                              <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeAfterHoursCloseMonday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeAfterHoursCloseMonday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img src=images/icon_audit.gif alt="" border=0 align=middle></a>
                              <%}%>
                            </p></td>
                          </tr>
                          <%
            }
            else if ((PracticeMaster.isRead("OfficeAfterHoursCloseMonday",UserSecurityGroupID)))
            {
                        %>
                          <%
            }
            else
            {
                        %>
                          <%
            }
            %>
                          <%
            if ( (PracticeMaster.isRequired("OfficeAfterHoursCloseTuesday",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeAfterHoursCloseTuesday")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeAfterHoursCloseTuesday",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeAfterHoursCloseTuesday",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                          <%
            if ((PracticeMaster.isWrite("OfficeAfterHoursCloseTuesday",UserSecurityGroupID)))
            {
                        %>
                          <tr>
                            <td valign=top><p>
                              <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="10" type=text size="8" name="OfficeAfterHoursCloseTuesday" value="<%=PracticeMaster.getOfficeAfterHoursCloseTuesday()%>">
                              &nbsp;
                              <%if (isShowAudit){%>
                              <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeAfterHoursCloseTuesday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeAfterHoursCloseTuesday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img src=images/icon_audit.gif alt="" border=0 align=middle></a>
                              <%}%>
                            </p></td>
                          </tr>
                          <%
            }
            else if ((PracticeMaster.isRead("OfficeAfterHoursCloseTuesday",UserSecurityGroupID)))
            {
                        %>
                          <%
            }
            else
            {
                        %>
                          <%
            }
            %>
                          <%
            if ( (PracticeMaster.isRequired("OfficeAfterHoursCloseWednesday",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeAfterHoursCloseWednesday")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeAfterHoursCloseWednesday",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeAfterHoursCloseWednesday",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                          <%
            if ((PracticeMaster.isWrite("OfficeAfterHoursCloseWednesday",UserSecurityGroupID)))
            {
                        %>
                          <tr>
                            <td valign=top><p>
                              <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="10" type=text size="8" name="OfficeAfterHoursCloseWednesday" value="<%=PracticeMaster.getOfficeAfterHoursCloseWednesday()%>">
                              &nbsp;
                              <%if (isShowAudit){%>
                              <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeAfterHoursCloseWednesday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeAfterHoursCloseWednesday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img src=images/icon_audit.gif alt="" border=0 align=middle></a>
                              <%}%>
                            </p></td>
                          </tr>
                          <%
            }
            else if ((PracticeMaster.isRead("OfficeAfterHoursCloseWednesday",UserSecurityGroupID)))
            {
                        %>
                          <%
            }
            else
            {
                        %>
                          <%
            }
            %>
                          <%
            if ( (PracticeMaster.isRequired("OfficeAfterHoursCloseThursday",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeAfterHoursCloseThursday")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeAfterHoursCloseThursday",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeAfterHoursCloseThursday",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                          <%
            if ((PracticeMaster.isWrite("OfficeAfterHoursCloseThursday",UserSecurityGroupID)))
            {
                        %>
                          <tr>
                            <td valign=top><p>
                              <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="10" type=text size="8" name="OfficeAfterHoursCloseThursday" value="<%=PracticeMaster.getOfficeAfterHoursCloseThursday()%>">
                              &nbsp;
                              <%if (isShowAudit){%>
                              <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeAfterHoursCloseThursday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeAfterHoursCloseThursday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img src=images/icon_audit.gif alt="" border=0 align=middle></a>
                              <%}%>
                            </p></td>
                          </tr>
                          <%
            }
            else if ((PracticeMaster.isRead("OfficeAfterHoursCloseThursday",UserSecurityGroupID)))
            {
                        %>
                          <%
            }
            else
            {
                        %>
                          <%
            }
            %>
                          <%
            if ( (PracticeMaster.isRequired("OfficeAfterHoursCloseFriday",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeAfterHoursCloseFriday")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeAfterHoursCloseFriday",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeAfterHoursCloseFriday",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                          <%
            if ((PracticeMaster.isWrite("OfficeAfterHoursCloseFriday",UserSecurityGroupID)))
            {
                        %>
                          <tr>
                            <td valign=top><p>
                              <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="10" type=text size="8" name="OfficeAfterHoursCloseFriday" value="<%=PracticeMaster.getOfficeAfterHoursCloseFriday()%>">
                              &nbsp;
                              <%if (isShowAudit){%>
                              <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeAfterHoursCloseFriday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeAfterHoursCloseFriday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img src=images/icon_audit.gif alt="" border=0 align=middle></a>
                              <%}%>
                            </p></td>
                          </tr>
                          <%
            }
            else if ((PracticeMaster.isRead("OfficeAfterHoursCloseFriday",UserSecurityGroupID)))
            {
                        %>
                          <%
            }
            else
            {
                        %>
                          <%
            }
            %>
                          <%
            if ( (PracticeMaster.isRequired("OfficeAfterHoursCloseSaturday",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeAfterHoursCloseSaturday")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeAfterHoursCloseSaturday",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeAfterHoursCloseSaturday",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                          <%
            if ((PracticeMaster.isWrite("OfficeAfterHoursCloseSaturday",UserSecurityGroupID)))
            {
                        %>
                          <tr>
                            <td valign=top><p>
                              <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="10" type=text size="8" name="OfficeAfterHoursCloseSaturday" value="<%=PracticeMaster.getOfficeAfterHoursCloseSaturday()%>">
                              &nbsp;
                              <%if (isShowAudit){%>
                              <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeAfterHoursCloseSaturday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeAfterHoursCloseSaturday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img src=images/icon_audit.gif alt="" border=0 align=middle></a>
                              <%}%>
                            </p></td>
                          </tr>
                          <%
            }
            else if ((PracticeMaster.isRead("OfficeAfterHoursCloseSaturday",UserSecurityGroupID)))
            {
                        %>
                          <%
            }
            else
            {
                        %>
                          <%
            }
            %>
                          <%
            if ( (PracticeMaster.isRequired("OfficeAfterHoursCloseSunday",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeAfterHoursCloseSunday")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeAfterHoursCloseSunday",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeAfterHoursCloseSunday",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                          <%
            if ((PracticeMaster.isWrite("OfficeAfterHoursCloseSunday",UserSecurityGroupID)))
            {
                        %>
                          <tr>
                            <td valign=top><p>
                              <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="10" type=text size="8" name="OfficeAfterHoursCloseSunday" value="<%=PracticeMaster.getOfficeAfterHoursCloseSunday()%>">
                              &nbsp;
                              <%if (isShowAudit){%>
                              <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeAfterHoursCloseSunday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeAfterHoursCloseSunday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img src=images/icon_audit.gif alt="" border=0 align=middle></a>
                              <%}%>
                            </p></td>
                          </tr>
                          <%
            }
            else if ((PracticeMaster.isRead("OfficeAfterHoursCloseSunday",UserSecurityGroupID)))
            {
                        %>
                          <%
            }
            else
            {
                        %>
                          <%
            }
            %>
                        </table></td>
                        <td><table class=tdBase  border=0 cellpadding="0" cellspacing="0">
                          <%
            if ( (PracticeMaster.isRequired("OfficeMDArthrogramHoursOpenMonday",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeMDArthrogramHoursOpenMonday")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeMDArthrogramHoursOpenMonday",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeMDArthrogramHoursOpenMonday",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                          <%
            if ((PracticeMaster.isWrite("OfficeMDArthrogramHoursOpenMonday",UserSecurityGroupID)))
            {
                        %>
                          <tr>
                            <td valign=top><p>
                              <input name="OfficeMDArthrogramHoursOpenMonday" type=text id="OfficeMDArthrogramHoursOpenMonday" onChange="document.getElementById('isSaveMe').value='1';" value="<%=PracticeMaster.getOfficeMDArthrogramHoursOpenMonday()%>" size="8" maxlength="10">
                              &nbsp;
                              <%if (isShowAudit){%>
                              <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeMDArthrogramHoursOpenMonday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeMDArthrogramHoursOpenMonday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                              <%}%>
                            </p></td>
                          </tr>
                          <%
            }
            else if ((PracticeMaster.isRead("OfficeMDArthrogramHoursOpenMonday",UserSecurityGroupID)))
            {
                        %>
                          <%
            }
            else
            {
                        %>
                          <%
            }
            %>
                          <%
            if ( (PracticeMaster.isRequired("OfficeMDArthrogramHoursOpenTuesday",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeMDArthrogramHoursOpenTuesday")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeMDArthrogramHoursOpenTuesday",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeMDArthrogramHoursOpenTuesday",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                          <%
            if ((PracticeMaster.isWrite("OfficeMDArthrogramHoursOpenTuesday",UserSecurityGroupID)))
            {
                        %>
                          <tr>
                            <td valign=top><p>
                              <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="10" type=text size="8" name="OfficeMDArthrogramHoursOpenTuesday" value="<%=PracticeMaster.getOfficeMDArthrogramHoursOpenTuesday()%>">
                              &nbsp;
                              <%if (isShowAudit){%>
                              <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeMDArthrogramHoursOpenTuesday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeMDArthrogramHoursOpenTuesday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                              <%}%>
                            </p></td>
                          </tr>
                          <%
            }
            else if ((PracticeMaster.isRead("OfficeMDArthrogramHoursOpenTuesday",UserSecurityGroupID)))
            {
                        %>
                          <%
            }
            else
            {
                        %>
                          <%
            }
            %>
                          <%
            if ( (PracticeMaster.isRequired("OfficeMDArthrogramHoursOpenWednesday",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeMDArthrogramHoursOpenWednesday")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeMDArthrogramHoursOpenWednesday",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeMDArthrogramHoursOpenWednesday",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                          <%
            if ((PracticeMaster.isWrite("OfficeMDArthrogramHoursOpenWednesday",UserSecurityGroupID)))
            {
                        %>
                          <tr>
                            <td valign=top><p>
                              <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="10" type=text size="8" name="OfficeMDArthrogramHoursOpenWednesday" value="<%=PracticeMaster.getOfficeMDArthrogramHoursOpenWednesday()%>">
                              &nbsp;
                              <%if (isShowAudit){%>
                              <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeMDArthrogramHoursOpenWednesday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeMDArthrogramHoursOpenWednesday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                              <%}%>
                            </p></td>
                          </tr>
                          <%
            }
            else if ((PracticeMaster.isRead("OfficeMDArthrogramHoursOpenWednesday",UserSecurityGroupID)))
            {
                        %>
                          <%
            }
            else
            {
                        %>
                          <%
            }
            %>
                          <%
            if ( (PracticeMaster.isRequired("OfficeMDArthrogramHoursOpenThursday",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeMDArthrogramHoursOpenThursday")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeMDArthrogramHoursOpenThursday",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeMDArthrogramHoursOpenThursday",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                          <%
            if ((PracticeMaster.isWrite("OfficeMDArthrogramHoursOpenThursday",UserSecurityGroupID)))
            {
                        %>
                          <tr>
                            <td valign=top><p>
                              <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="10" type=text size="8" name="OfficeMDArthrogramHoursOpenThursday" value="<%=PracticeMaster.getOfficeMDArthrogramHoursOpenThursday()%>">
                              &nbsp;
                              <%if (isShowAudit){%>
                              <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeMDArthrogramHoursOpenThursday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeMDArthrogramHoursOpenThursday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                              <%}%>
                            </p></td>
                          </tr>
                          <%
            }
            else if ((PracticeMaster.isRead("OfficeMDArthrogramHoursOpenThursday",UserSecurityGroupID)))
            {
                        %>
                          <%
            }
            else
            {
                        %>
                          <%
            }
            %>
                          <%
            if ( (PracticeMaster.isRequired("OfficeMDArthrogramHoursOpenFriday",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeMDArthrogramHoursOpenFriday")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeMDArthrogramHoursOpenFriday",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeMDArthrogramHoursOpenFriday",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                          <%
            if ((PracticeMaster.isWrite("OfficeMDArthrogramHoursOpenFriday",UserSecurityGroupID)))
            {
                        %>
                          <tr>
                            <td valign=top><p>
                              <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="10" type=text size="8" name="OfficeMDArthrogramHoursOpenFriday" value="<%=PracticeMaster.getOfficeMDArthrogramHoursOpenFriday()%>">
                              &nbsp;
                              <%if (isShowAudit){%>
                              <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeMDArthrogramHoursOpenFriday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeMDArthrogramHoursOpenFriday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                              <%}%>
                            </p></td>
                          </tr>
                          <%
            }
            else if ((PracticeMaster.isRead("OfficeMDArthrogramHoursOpenFriday",UserSecurityGroupID)))
            {
                        %>
                          <%
            }
            else
            {
                        %>
                          <%
            }
            %>
                          <%
            if ( (PracticeMaster.isRequired("OfficeMDArthrogramHoursOpenSaturday",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeMDArthrogramHoursOpenSaturday")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeMDArthrogramHoursOpenSaturday",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeMDArthrogramHoursOpenSaturday",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                          <%
            if ((PracticeMaster.isWrite("OfficeMDArthrogramHoursOpenSaturday",UserSecurityGroupID)))
            {
                        %>
                          <tr>
                            <td valign=top><p>
                              <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="10" type=text size="8" name="OfficeMDArthrogramHoursOpenSaturday" value="<%=PracticeMaster.getOfficeMDArthrogramHoursOpenSaturday()%>">
                              &nbsp;
                              <%if (isShowAudit){%>
                              <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeMDArthrogramHoursOpenSaturday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeMDArthrogramHoursOpenSaturday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                              <%}%>
                            </p></td>
                          </tr>
                          <%
            }
            else if ((PracticeMaster.isRead("OfficeMDArthrogramHoursOpenSaturday",UserSecurityGroupID)))
            {
                        %>
                          <%
            }
            else
            {
                        %>
                          <%
            }
            %>
                          <%
            if ( (PracticeMaster.isRequired("OfficeMDArthrogramHoursOpenSunday",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeMDArthrogramHoursOpenSunday")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeMDArthrogramHoursOpenSunday",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeMDArthrogramHoursOpenSunday",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                          <%
            if ((PracticeMaster.isWrite("OfficeMDArthrogramHoursOpenSunday",UserSecurityGroupID)))
            {
                        %>
                          <tr>
                            <td valign=top><p>
                              <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="10" type=text size="8" name="OfficeMDArthrogramHoursOpenSunday" value="<%=PracticeMaster.getOfficeMDArthrogramHoursOpenSunday()%>">
                              &nbsp;
                              <%if (isShowAudit){%>
                              <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeMDArthrogramHoursOpenSunday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeMDArthrogramHoursOpenSunday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                              <%}%>
                            </p></td>
                          </tr>
                          <%
            }
            else if ((PracticeMaster.isRead("OfficeMDArthrogramHoursOpenSunday",UserSecurityGroupID)))
            {
                        %>
                          <%
            }
            else
            {
                        %>
                          <%
            }
            %>
                        </table></td>
                        <td><table  class=tdBase border=0 cellpadding="0" cellspacing="0">
                          <%
            if ( (PracticeMaster.isRequired("OfficeMDArthrogramHoursCloseMonday",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeMDArthrogramHoursCloseMonday")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeMDArthrogramHoursCloseMonday",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeMDArthrogramHoursCloseMonday",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                          <%
            if ((PracticeMaster.isWrite("OfficeMDArthrogramHoursCloseMonday",UserSecurityGroupID)))
            {
                        %>
                          <tr>
                            <td valign=top><p>
                              <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="10" type=text size="8" name="OfficeMDArthrogramHoursCloseMonday" value="<%=PracticeMaster.getOfficeMDArthrogramHoursCloseMonday()%>">
                              &nbsp;
                              <%if (isShowAudit){%>
                              <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeMDArthrogramHoursCloseMonday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeMDArthrogramHoursCloseMonday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                              <%}%>
                            </p></td>
                          </tr>
                          <%
            }
            else if ((PracticeMaster.isRead("OfficeMDArthrogramHoursCloseMonday",UserSecurityGroupID)))
            {
                        %>
                          <%
            }
            else
            {
                        %>
                          <%
            }
            %>
                          <%
            if ( (PracticeMaster.isRequired("OfficeMDArthrogramHoursCloseTuesday",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeMDArthrogramHoursCloseTuesday")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeMDArthrogramHoursCloseTuesday",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeMDArthrogramHoursCloseTuesday",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                          <%
            if ((PracticeMaster.isWrite("OfficeMDArthrogramHoursCloseTuesday",UserSecurityGroupID)))
            {
                        %>
                          <tr>
                            <td valign=top><p>
                              <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="10" type=text size="8" name="OfficeMDArthrogramHoursCloseTuesday" value="<%=PracticeMaster.getOfficeMDArthrogramHoursCloseTuesday()%>">
                              &nbsp;
                              <%if (isShowAudit){%>
                              <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeMDArthrogramHoursCloseTuesday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeMDArthrogramHoursCloseTuesday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                              <%}%>
                            </p></td>
                          </tr>
                          <%

            }
            else if ((PracticeMaster.isRead("OfficeMDArthrogramHoursCloseTuesday",UserSecurityGroupID)))
            {
                        %>
                          <%
            }
            else
            {
                        %>
                          <%
            }
            %>
                          <%
            if ( (PracticeMaster.isRequired("OfficeMDArthrogramHoursCloseWednesday",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeMDArthrogramHoursCloseWednesday")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeMDArthrogramHoursCloseWednesday",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeMDArthrogramHoursCloseWednesday",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                          <%
            if ((PracticeMaster.isWrite("OfficeMDArthrogramHoursCloseWednesday",UserSecurityGroupID)))
            {
                        %>
                          <tr>
                            <td valign=top><p>
                              <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="10" type=text size="8" name="OfficeMDArthrogramHoursCloseWednesday" value="<%=PracticeMaster.getOfficeMDArthrogramHoursCloseWednesday()%>">
                              &nbsp;
                              <%if (isShowAudit){%>
                              <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeMDArthrogramHoursCloseWednesday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeMDArthrogramHoursCloseWednesday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                              <%}%>
                            </p></td>
                          </tr>
                          <%
            }
            else if ((PracticeMaster.isRead("OfficeMDArthrogramHoursCloseWednesday",UserSecurityGroupID)))
            {
                        %>
                          <%
            }
            else
            {
                        %>
                          <%
            }
            %>
                          <%
            if ( (PracticeMaster.isRequired("OfficeMDArthrogramHoursCloseThursday",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeMDArthrogramHoursCloseThursday")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeMDArthrogramHoursCloseThursday",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeMDArthrogramHoursCloseThursday",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                          <%
            if ((PracticeMaster.isWrite("OfficeMDArthrogramHoursCloseThursday",UserSecurityGroupID)))
            {
                        %>
                          <tr>
                            <td valign=top><p>
                              <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="10" type=text size="8" name="OfficeMDArthrogramHoursCloseThursday" value="<%=PracticeMaster.getOfficeMDArthrogramHoursCloseThursday()%>">
                              &nbsp;
                              <%if (isShowAudit){%>
                              <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeMDArthrogramHoursCloseThursday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeMDArthrogramHoursCloseThursday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                              <%}%>
                            </p></td>
                          </tr>
                          <%
            }
            else if ((PracticeMaster.isRead("OfficeMDArthrogramHoursCloseThursday",UserSecurityGroupID)))
            {
                        %>
                          <%
            }
            else
            {
                        %>
                          <%
            }
            %>
                          <%
            if ( (PracticeMaster.isRequired("OfficeMDArthrogramHoursCloseFriday",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeMDArthrogramHoursCloseFriday")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeMDArthrogramHoursCloseFriday",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeMDArthrogramHoursCloseFriday",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                          <%
            if ((PracticeMaster.isWrite("OfficeMDArthrogramHoursCloseFriday",UserSecurityGroupID)))
            {
                        %>
                          <tr>
                            <td valign=top><p>
                              <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="10" type=text size="8" name="OfficeMDArthrogramHoursCloseFriday" value="<%=PracticeMaster.getOfficeMDArthrogramHoursCloseFriday()%>">
                              &nbsp;
                              <%if (isShowAudit){%>
                              <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeMDArthrogramHoursCloseFriday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeMDArthrogramHoursCloseFriday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                              <%}%>
                            </p></td>
                          </tr>
                          <%
            }
            else if ((PracticeMaster.isRead("OfficeMDArthrogramHoursCloseFriday",UserSecurityGroupID)))
            {
                        %>
                          <%
            }
            else
            {
                        %>
                          <%
            }
            %>
                          <%
            if ( (PracticeMaster.isRequired("OfficeMDArthrogramHoursCloseSaturday",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeMDArthrogramHoursCloseSaturday")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeMDArthrogramHoursCloseSaturday",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeMDArthrogramHoursCloseSaturday",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                          <%
            if ((PracticeMaster.isWrite("OfficeMDArthrogramHoursCloseSaturday",UserSecurityGroupID)))
            {
                        %>
                          <tr>
                            <td valign=top><p>
                              <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="10" type=text size="8" name="OfficeMDArthrogramHoursCloseSaturday" value="<%=PracticeMaster.getOfficeMDArthrogramHoursCloseSaturday()%>">
                              &nbsp;
                              <%if (isShowAudit){%>
                              <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeMDArthrogramHoursCloseSaturday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeMDArthrogramHoursCloseSaturday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                              <%}%>
                            </p></td>
                          </tr>
                          <%
            }
            else if ((PracticeMaster.isRead("OfficeMDArthrogramHoursCloseSaturday",UserSecurityGroupID)))
            {
                        %>
                          <%
            }
            else
            {
                        %>
                          <%
            }
            %>
                          <%
            if ( (PracticeMaster.isRequired("OfficeMDArthrogramHoursCloseSunday",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeMDArthrogramHoursCloseSunday")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeMDArthrogramHoursCloseSunday",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeMDArthrogramHoursCloseSunday",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                          <%
            if ((PracticeMaster.isWrite("OfficeMDArthrogramHoursCloseSunday",UserSecurityGroupID)))
            {
                        %>
                          <tr>
                            <td valign=top><p>
                              <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="10" type=text size="8" name="OfficeMDArthrogramHoursCloseSunday" value="<%=PracticeMaster.getOfficeMDArthrogramHoursCloseSunday()%>">
                              &nbsp;
                              <%if (isShowAudit){%>
                              <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeMDArthrogramHoursCloseSunday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeMDArthrogramHoursCloseSunday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                              <%}%>
                            </p></td>
                          </tr>
                          <%
            }
            else if ((PracticeMaster.isRead("OfficeMDArthrogramHoursCloseSunday",UserSecurityGroupID)))
            {
                        %>
                          <%
            }
            else
            {
                        %>
                          <%
            }
            %>
                        </table></td>
                        <td> 
                          <table class=tdBase  border=0 cellpadding="0" cellspacing="0">
                            <%
            if ( (PracticeMaster.isRequired("OfficeMDMyelogramHoursOpenMonday",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeMDMyelogramHoursOpenMonday")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeMDMyelogramHoursOpenMonday",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeMDMyelogramHoursOpenMonday",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                            <%
            if ((PracticeMaster.isWrite("OfficeMDMyelogramHoursOpenMonday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p> 
                                  <input name="OfficeMDMyelogramHoursOpenMonday" type=text id="OfficeMDMyelogramHoursOpenMonday" onChange="document.getElementById('isSaveMe').value='1';" value="<%=PracticeMaster.getOfficeMDMyelogramHoursOpenMonday()%>" size="8" maxlength="10">
                                  &nbsp; 
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeMDMyelogramHoursOpenMonday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeMDMyelogramHoursOpenMonday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else if ((PracticeMaster.isRead("OfficeMDMyelogramHoursOpenMonday",UserSecurityGroupID)))
            {
                        %>
                            <%
            }
            else
            {
                        %>
                            <%
            }
            %>
                            <%
            if ( (PracticeMaster.isRequired("OfficeMDMyelogramHoursOpenTuesday",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeMDMyelogramHoursOpenTuesday")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeMDMyelogramHoursOpenTuesday",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeMDMyelogramHoursOpenTuesday",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                            <%
            if ((PracticeMaster.isWrite("OfficeMDMyelogramHoursOpenTuesday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p> 
                                  <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="10" type=text size="8" name="OfficeMDMyelogramHoursOpenTuesday" value="<%=PracticeMaster.getOfficeMDMyelogramHoursOpenTuesday()%>">
                                  &nbsp; 
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeMDMyelogramHoursOpenTuesday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeMDMyelogramHoursOpenTuesday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else if ((PracticeMaster.isRead("OfficeMDMyelogramHoursOpenTuesday",UserSecurityGroupID)))
            {
                        %>
                            <%
            }
            else
            {
                        %>
                            <%
            }
            %>
                            <%
            if ( (PracticeMaster.isRequired("OfficeMDMyelogramHoursOpenWednesday",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeMDMyelogramHoursOpenWednesday")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeMDMyelogramHoursOpenWednesday",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeMDMyelogramHoursOpenWednesday",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                            <%
            if ((PracticeMaster.isWrite("OfficeMDMyelogramHoursOpenWednesday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p> 
                                  <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="10" type=text size="8" name="OfficeMDMyelogramHoursOpenWednesday" value="<%=PracticeMaster.getOfficeMDMyelogramHoursOpenWednesday()%>">
                                  &nbsp; 
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeMDMyelogramHoursOpenWednesday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeMDMyelogramHoursOpenWednesday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else if ((PracticeMaster.isRead("OfficeMDMyelogramHoursOpenWednesday",UserSecurityGroupID)))
            {
                        %>
                            <%
            }
            else
            {
                        %>
                            <%
            }
            %>
                            <%
            if ( (PracticeMaster.isRequired("OfficeMDMyelogramHoursOpenThursday",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeMDMyelogramHoursOpenThursday")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeMDMyelogramHoursOpenThursday",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeMDMyelogramHoursOpenThursday",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                            <%
            if ((PracticeMaster.isWrite("OfficeMDMyelogramHoursOpenThursday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p> 
                                  <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="10" type=text size="8" name="OfficeMDMyelogramHoursOpenThursday" value="<%=PracticeMaster.getOfficeMDMyelogramHoursOpenThursday()%>">
                                  &nbsp; 
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeMDMyelogramHoursOpenThursday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeMDMyelogramHoursOpenThursday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else if ((PracticeMaster.isRead("OfficeMDMyelogramHoursOpenThursday",UserSecurityGroupID)))
            {
                        %>
                            <%
            }
            else
            {
                        %>
                            <%
            }
            %>
                            <%
            if ( (PracticeMaster.isRequired("OfficeMDMyelogramHoursOpenFriday",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeMDMyelogramHoursOpenFriday")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeMDMyelogramHoursOpenFriday",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeMDMyelogramHoursOpenFriday",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                            <%
            if ((PracticeMaster.isWrite("OfficeMDMyelogramHoursOpenFriday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p> 
                                  <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="10" type=text size="8" name="OfficeMDMyelogramHoursOpenFriday" value="<%=PracticeMaster.getOfficeMDMyelogramHoursOpenFriday()%>">
                                  &nbsp; 
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeMDMyelogramHoursOpenFriday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeMDMyelogramHoursOpenFriday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else if ((PracticeMaster.isRead("OfficeMDMyelogramHoursOpenFriday",UserSecurityGroupID)))
            {
                        %>
                            <%
            }
            else
            {
                        %>
                            <%
            }
            %>
                            <%
            if ( (PracticeMaster.isRequired("OfficeMDMyelogramHoursOpenSaturday",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeMDMyelogramHoursOpenSaturday")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeMDMyelogramHoursOpenSaturday",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeMDMyelogramHoursOpenSaturday",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                            <%
            if ((PracticeMaster.isWrite("OfficeMDMyelogramHoursOpenSaturday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p> 
                                  <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="10" type=text size="8" name="OfficeMDMyelogramHoursOpenSaturday" value="<%=PracticeMaster.getOfficeMDMyelogramHoursOpenSaturday()%>">
                                  &nbsp; 
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeMDMyelogramHoursOpenSaturday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeMDMyelogramHoursOpenSaturday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else if ((PracticeMaster.isRead("OfficeMDMyelogramHoursOpenSaturday",UserSecurityGroupID)))
            {
                        %>
                            <%
            }
            else
            {
                        %>
                            <%
            }
            %>
                            <%
            if ( (PracticeMaster.isRequired("OfficeMDMyelogramHoursOpenSunday",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeMDMyelogramHoursOpenSunday")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeMDMyelogramHoursOpenSunday",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeMDMyelogramHoursOpenSunday",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                            <%
            if ((PracticeMaster.isWrite("OfficeMDMyelogramHoursOpenSunday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p> 
                                  <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="10" type=text size="8" name="OfficeMDMyelogramHoursOpenSunday" value="<%=PracticeMaster.getOfficeMDMyelogramHoursOpenSunday()%>">
                                  &nbsp; 
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeMDMyelogramHoursOpenSunday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeMDMyelogramHoursOpenSunday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else if ((PracticeMaster.isRead("OfficeMDMyelogramHoursOpenSunday",UserSecurityGroupID)))
            {
                        %>
                            <%
            }
            else
            {
                        %>
                            <%
            }
            %>
                          </table>
                        </td>
                        <td> 
                          <table  class=tdBase border=0 cellpadding="0" cellspacing="0">
                            <%
            if ( (PracticeMaster.isRequired("OfficeMDMyelogramHoursCloseMonday",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeMDMyelogramHoursCloseMonday")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeMDMyelogramHoursCloseMonday",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeMDMyelogramHoursCloseMonday",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                            <%
            if ((PracticeMaster.isWrite("OfficeMDMyelogramHoursCloseMonday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p> 
                                  <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="10" type=text size="8" name="OfficeMDMyelogramHoursCloseMonday" value="<%=PracticeMaster.getOfficeMDMyelogramHoursCloseMonday()%>">
                                  &nbsp; 
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeMDMyelogramHoursCloseMonday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeMDMyelogramHoursCloseMonday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else if ((PracticeMaster.isRead("OfficeMDMyelogramHoursCloseMonday",UserSecurityGroupID)))
            {
                        %>
                            <%
            }
            else
            {
                        %>
                            <%
            }
            %>
                            <%
            if ( (PracticeMaster.isRequired("OfficeMDMyelogramHoursCloseTuesday",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeMDMyelogramHoursCloseTuesday")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeMDMyelogramHoursCloseTuesday",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeMDMyelogramHoursCloseTuesday",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                            <%
            if ((PracticeMaster.isWrite("OfficeMDMyelogramHoursCloseTuesday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p> 
                                  <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="10" type=text size="8" name="OfficeMDMyelogramHoursCloseTuesday" value="<%=PracticeMaster.getOfficeMDMyelogramHoursCloseTuesday()%>">
                                  &nbsp; 
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeMDMyelogramHoursCloseTuesday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeMDMyelogramHoursCloseTuesday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else if ((PracticeMaster.isRead("OfficeMDMyelogramHoursCloseTuesday",UserSecurityGroupID)))
            {
                        %>
                            <%
            }
            else
            {
                        %>
                            <%
            }
            %>
                            <%
            if ( (PracticeMaster.isRequired("OfficeMDMyelogramHoursCloseWednesday",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeMDMyelogramHoursCloseWednesday")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeMDMyelogramHoursCloseWednesday",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeMDMyelogramHoursCloseWednesday",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                            <%
            if ((PracticeMaster.isWrite("OfficeMDMyelogramHoursCloseWednesday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p> 
                                  <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="10" type=text size="8" name="OfficeMDMyelogramHoursCloseWednesday" value="<%=PracticeMaster.getOfficeMDMyelogramHoursCloseWednesday()%>">
                                  &nbsp; 
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeMDMyelogramHoursCloseWednesday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeMDMyelogramHoursCloseWednesday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else if ((PracticeMaster.isRead("OfficeMDMyelogramHoursCloseWednesday",UserSecurityGroupID)))
            {
                        %>
                            <%
            }
            else
            {
                        %>
                            <%
            }
            %>
                            <%
            if ( (PracticeMaster.isRequired("OfficeMDMyelogramHoursCloseThursday",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeMDMyelogramHoursCloseThursday")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeMDMyelogramHoursCloseThursday",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeMDMyelogramHoursCloseThursday",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                            <%
            if ((PracticeMaster.isWrite("OfficeMDMyelogramHoursCloseThursday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p> 
                                  <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="10" type=text size="8" name="OfficeMDMyelogramHoursCloseThursday" value="<%=PracticeMaster.getOfficeMDMyelogramHoursCloseThursday()%>">
                                  &nbsp; 
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeMDMyelogramHoursCloseThursday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeMDMyelogramHoursCloseThursday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else if ((PracticeMaster.isRead("OfficeMDMyelogramHoursCloseThursday",UserSecurityGroupID)))
            {
                        %>
                            <%
            }
            else
            {
                        %>
                            <%
            }
            %>
                            <%
            if ( (PracticeMaster.isRequired("OfficeMDMyelogramHoursCloseFriday",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeMDMyelogramHoursCloseFriday")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeMDMyelogramHoursCloseFriday",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeMDMyelogramHoursCloseFriday",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                            <%
            if ((PracticeMaster.isWrite("OfficeMDMyelogramHoursCloseFriday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p> 
                                  <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="10" type=text size="8" name="OfficeMDMyelogramHoursCloseFriday" value="<%=PracticeMaster.getOfficeMDMyelogramHoursCloseFriday()%>">
                                  &nbsp; 
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeMDMyelogramHoursCloseFriday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeMDMyelogramHoursCloseFriday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else if ((PracticeMaster.isRead("OfficeMDMyelogramHoursCloseFriday",UserSecurityGroupID)))
            {
                        %>
                            <%
            }
            else
            {
                        %>
                            <%
            }
            %>
                            <%
            if ( (PracticeMaster.isRequired("OfficeMDMyelogramHoursCloseSaturday",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeMDMyelogramHoursCloseSaturday")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeMDMyelogramHoursCloseSaturday",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeMDMyelogramHoursCloseSaturday",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                            <%
            if ((PracticeMaster.isWrite("OfficeMDMyelogramHoursCloseSaturday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p> 
                                  <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="10" type=text size="8" name="OfficeMDMyelogramHoursCloseSaturday" value="<%=PracticeMaster.getOfficeMDMyelogramHoursCloseSaturday()%>">
                                  &nbsp; 
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeMDMyelogramHoursCloseSaturday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeMDMyelogramHoursCloseSaturday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else if ((PracticeMaster.isRead("OfficeMDMyelogramHoursCloseSaturday",UserSecurityGroupID)))
            {
                        %>
                            <%
            }
            else
            {
                        %>
                            <%
            }
            %>
                            <%
            if ( (PracticeMaster.isRequired("OfficeMDMyelogramHoursCloseSunday",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeMDMyelogramHoursCloseSunday")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeMDMyelogramHoursCloseSunday",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeMDMyelogramHoursCloseSunday",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                            <%
            if ((PracticeMaster.isWrite("OfficeMDMyelogramHoursCloseSunday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p> 
                                  <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="10" type=text size="8" name="OfficeMDMyelogramHoursCloseSunday" value="<%=PracticeMaster.getOfficeMDMyelogramHoursCloseSunday()%>">
                                  &nbsp; 
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeMDMyelogramHoursCloseSunday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeMDMyelogramHoursCloseSunday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else if ((PracticeMaster.isRead("OfficeMDMyelogramHoursCloseSunday",UserSecurityGroupID)))
            {
                        %>
                            <%
            }
            else
            {
                        %>
                            <%
            }
            %>
                          </table>
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
                
                
                
                <tr> 
                  <td colspan=3>&nbsp; 
                    
                  </td>
                </tr>
                
                -->
                

                        <tr><td valign=top><p class=<%=theClass%> ><b>Rate Curb Appeal&nbsp;</b></p></td><td valign=top><p><select   name="Rate_CurbAppeal" ><jsp:include page="../generic/tRate15LILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PracticeMaster.getRate_CurbAppeal()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Rate_CurbAppeal&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("Rate_CurbAppeal")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>

                        <tr><td valign=top><p class=<%=theClass%> ><b>Rate Access Safety&nbsp;</b></p></td><td valign=top><p><select   name="Rate_AccessSafety" ><jsp:include page="../generic/tRate15LILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PracticeMaster.getRate_AccessSafety()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Rate_AccessSafety&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("Rate_AccessSafety")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
    
                        <tr><td valign=top><p class=<%=theClass%> ><b>Rate Cleanliness&nbsp;</b></p></td><td valign=top><p><select   name="Rate_Cleanliness" ><jsp:include page="../generic/tRate15LILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PracticeMaster.getRate_Cleanliness()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Rate_Cleanliness&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("Rate_Cleanliness")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>

                        <tr><td valign=top><p class=<%=theClass%> ><b>Rate Appropriateness&nbsp;</b></p></td><td valign=top><p><select   name="Rate_Appropriateness" ><jsp:include page="../generic/tRate15LILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PracticeMaster.getRate_Appropriateness()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Rate_Appropriateness&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("Rate_Appropriateness")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>

                        <tr><td valign=top><p class=<%=theClass%> ><b>Rate Staff Friendly&nbsp;</b></p></td><td valign=top><p><select   name="Rate_StaffFriendly" ><jsp:include page="../generic/tRate15LILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PracticeMaster.getRate_StaffFriendly()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Rate_StaffFriendly&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("Rate_StaffFriendly")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
              
                        <tr><td valign=top><p class=<%=theClass%> ><b>Rate Attentive Timely&nbsp;</b></p></td><td valign=top><p><select   name="Rate_AttentiveTimely" ><jsp:include page="../generic/tRate15LILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PracticeMaster.getRate_AttentiveTimely()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Rate_AttentiveTimely&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("Rate_AttentiveTimely")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        
                        
                        
                        
                        



                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Type of Practice&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <select  onchange="document.getElementById('isSaveMe').value='1';"    name="TypeOfPractice" >
                        <jsp:include page="../generic/tPracticeTypeLILong.jsp" flush="true" >
                        <jsp:param name="CurrentSelection" value="<%=PracticeMaster.getTypeOfPractice()%>" />
                        </jsp:include>
                      </select>
                      &nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=TypeOfPractice&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("TypeOfPractice")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                   <tr><td valign=top><p class=<%=theClass%> ><b>Web URL&nbsp;</b></p></td><td valign=top><p><input onChange="document.getElementById('isSaveMe').value='1';" maxlength="100" type=text size="80" name="WebURL" value="<%=PracticeMaster.getWebURL()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=WebURL&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("WebURL")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                <tr>
                  <td valign=top><p class=<%=theClass%> ><b>Back Office Phone</b></p></td>
                  <td valign=top><p>
                    <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="20" type=text size="80" name="BackOfficePhoneNo" value="<%=PracticeMaster.getBackOfficePhoneNo()%>">
                    &nbsp;
                    <%if (isShowAudit){%>
                    <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=BackOfficePhoneNo&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("BackOfficePhoneNo")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                    <%}%>
                  </p></td>
                  <td valign=top>&nbsp;</td>
                </tr>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Office Email&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="75" type=text size="80" name="OfficeEmail" value="<%=PracticeMaster.getOfficeEmail()%>">
                      &nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeEmail&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeEmail")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                  <td valign=top>&nbsp;</td>
                </tr>
                <tr>
                  <td valign=top><p class=<%=theClass%> ><b>Languages Spoken&nbsp;</b></p></td>
                  <td valign=top><p>
                    <textarea  onchange="document.getElementById('isSaveMe').value='1';"   onKeyDown="textAreaStop(this,200)" rows="2" name="LanguagesSpokenInOffice" cols="40" maxlength=200><%=PracticeMaster.getLanguagesSpokenInOffice()%></textarea>
                    &nbsp;
                    <%if (isShowAudit){%>
                    <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=LanguagesSpokenInOffice&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("LanguagesSpokenInOffice")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                    <%}%>
                  </p></td>
                  <td valign=top>&nbsp;</td>
                </tr>


                
                <tr> 
                  <td colspan=3> 
                    <hr noshade>
                  </td>
                </tr>
                
                
                
                
                <%
            if ( (PracticeMaster.isRequired("AllowPhysEdit",UserSecurityGroupID))&&(!PracticeMaster.isComplete("AllowPhysEdit")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("AllowPhysEdit",expiredDays))&&(PracticeMaster.isExpiredCheck("AllowPhysEdit",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if (false&&(PracticeMaster.isWrite("AllowPhysEdit",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>&nbsp;</td>
                  <td valign=top>&nbsp;</td>
                  <td valign=top>&nbsp;</td>
                </tr>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Allow Practitioner to edit this 
                      Practice&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <select  onchange="document.getElementById('isSaveMe').value='1';"    name="AllowPhysEdit" >
                        <jsp:include page="../generic/tYesNoLILong.jsp" flush="true" >
                        <jsp:param name="CurrentSelection" value="<%=PracticeMaster.getAllowPhysEdit()%>" />
                        </jsp:include>
                      </select>
                      &nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=AllowPhysEdit&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("AllowPhysEdit")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                  <td valign=top>&nbsp;</td>
                </tr>
                <%
            }
            else if (false&&(PracticeMaster.isRead("AllowPhysEdit",UserSecurityGroupID)))
            {
                        %>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <tr>
                  <td width=40%>&nbsp;</td>
                  <td width=90%>&nbsp;</td>
                  <td width=90%>&nbsp;</td>
                </tr>
              </table>
              <%if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {%>
              <input onChange="document.getElementById('isSaveMe').value='1';" type=hidden name=routePageReference value="sParentReturnPage">
              <%
              if (request.getParameter("INTNext")!=null&&request.getParameter("INTNext").equalsIgnoreCase("yes") ) 
              {
              %>
              <table width=75% border=1 bordercolor=333333 align=left cellspacing=0 cellpadding=0 class=wizardTable>
                <tr class=requiredField>
                  <td> 
                    <input onChange="document.getElementById('isSaveMe').value='1';"  <%=HTMLFormStyleButton%> type="radio" value="next" name="INTNext" checked>
                    &nbsp;&nbsp;<%=ConfigurationMessages.getInterviewMessage("INTERVIEWNoMore","tPracticeMaster")%> 
                    <br>
                    <input onChange="document.getElementById('isSaveMe').value='1';"  <%=HTMLFormStyleButton%> type="radio" value="yes" name="INTNext">
                    &nbsp;&nbsp;<%=ConfigurationMessages.getInterviewMessage("INTERVIEWAddMore","tPracticeMaster")%> 
                  </td>
                </tr>
              </table>
              <br>
              <br>
              <br>
              <%
              }
              %>
              <p>
                <input onChange="document.getElementById('isSaveMe').value='1';"  class="tdBaseAltGreen" onClick="document.getElementById('isSaveMe').value='0';"  type=Submit value="Save" name=Submit>
              </p>
              <%}%>
            </td>
          </tr>
         
        </table>
      </form>
      <%
  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>
    </td>
  </tr>
  
</table>
<br><br><br>
<script>
	$("#netdevcommtrack").load('retrieve_netdevcomm.jsp?practiceid=<%=iPracticeID%>');
</script>