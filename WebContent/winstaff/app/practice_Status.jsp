<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.*" %>
<%/*
    filename: out\jsp\tPracticeMaster_form.jsp
    JSP AutoGen on Mar/02/2002
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>

<%
//String thePLCID = (String)pageControllerHash.get("plcID");
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_PracticeID_nim.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>
<%
//initial declaration of list class and parentID
    Integer        iPracticeID        =    null;
    boolean accessValid = false;
    // required for Type2
    String sKeyMasterReference = null;
    if (pageControllerHash.containsKey("iPracticeID")) 
    {
        iPracticeID        =    (Integer)pageControllerHash.get("iPracticeID");
        accessValid = true;    
   }
  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);      java.text.SimpleDateFormat displayDateTimeSDF = new java.text.SimpleDateFormat("MM/dd/yyyy hh:mm a");

    pageControllerHash.put("sLocalChildReturnPage","practice_Status.jsp");
    pageControllerHash.put("sParentReturnPage","practice_Status.jsp");
    session.setAttribute("pageControllerHash",pageControllerHash);
/*
stepVerify_PracticeID mySV = new stepVerify_PracticeID();
mySV.setPracticeID(iPracticeID);
*/
bltPracticeMaster        PracticeMaster        =   new  bltPracticeMaster (iPracticeID);
String pmContractingStatusID = PracticeMaster.getContractingStatusID().toString();

String myClass = "tdBase";

		if (pmContractingStatusID.equalsIgnoreCase("0") )
		{
			myClass = "tdBaseAltGrey";
		//	out.print("A - NC";
		}
		else if (pmContractingStatusID.equalsIgnoreCase("1") )
		{
			myClass = "tdBaseAltCyan";
		//	out.print("E - Negotiating";
		}
		else if (pmContractingStatusID.equalsIgnoreCase("2") )
		{
			myClass = "tdBaseAltGreen";
		//	out.print("F - Contracted";
		}
		else if (pmContractingStatusID.equalsIgnoreCase("3") )
		{
			myClass = "tdBaseAltYellow";
		//	out.print("B - Target";
		}
		else if (pmContractingStatusID.equalsIgnoreCase("4") )
		{
			myClass = "tdBaseAltRed";
		//	out.print("F - Not Interested";
		}
		else if (pmContractingStatusID.equalsIgnoreCase("5") )
		{
			myClass = "tdBaseAltPurple";
		//	out.print("D - Send Contract";
		}
		else if (pmContractingStatusID.equalsIgnoreCase("6") )
		{
			myClass = "tdBaseAltOrange";
		//	out.print("C - Talking";
		}
		else if (pmContractingStatusID.equalsIgnoreCase("7") )
		{
			myClass = "tdBaseAltPink";
		//	out.print("TargetMerge";
		}
		else if (pmContractingStatusID.equalsIgnoreCase("8") )
		{
			myClass = "tdBaseAltOliveMed";
		//	out.print("TargetMerge";
		}
		else if (pmContractingStatusID.equalsIgnoreCase("9") )
		{
			myClass = "tdBaseAltLime";
		//	out.print("Contracted[*]";
		}
		else if (pmContractingStatusID.equalsIgnoreCase("10") )
		{
			myClass = "tdBaseAltLime2";
		//	out.print("Contracted[V*]";
		}
		else if (pmContractingStatusID.equalsIgnoreCase("11") )
		{
			myClass = "tdBaseAltLime3";
		//	out.print("Contracted[V*]";
		}

%>
      <table width="1000" border="0" cellspacing="0" cellpadding="0" >
<tr>
<td width=10>&nbsp;</td>
    <td> <p>
      <table width="100%" border="1" cellspacing="0" cellpadding="5" bordercolor="#333333">
        <tr class=tdHeaderAlt > 
          <td>Practice Profile&nbsp;&nbsp;&nbsp;     <input type="button" class="tdBaseAltGrey" onclick="this.disabled=true;document.location='tPracticeMaster_form.jsp?EDIT=edit';" name="button3" id="button3" value="Edit Practice Data" /></td>
        </tr>
        <tr class="<%=myClass%>">
          <td>Status: <strong><%=new bltPracticeContractStatusLI(PracticeMaster.getContractingStatusID()).getDescriptionLong()%></strong></td>
        </tr>
        <tr>
          <td>
          
<%          
{
    bltNIM3_Modality_List        bltNIM3_Modality_List        =    new    bltNIM3_Modality_List(iPracticeID);

//declaration of Enumeration
    bltNIM3_Modality        working_bltNIM3_Modality;
    ListElement         leCurrentElement;
    java.util.Enumeration eList = bltNIM3_Modality_List.elements();
    %>
<input type="button" class="tdHeaderAlt"  onclick="this.disabled=true;document.location='tNIM3_Modality_main_NIM3_Modality_PracticeID.jsp';" name="button2" id="button2" value="Modality Information" />
<table border="1" bordercolor="CCCCCC" cellpadding="3" class=tdBase cellspacing="0" width="100%">
    <%
    int altCnt = 0;
    if (eList.hasMoreElements())
    {
     while (eList.hasMoreElements())
     {

        altCnt++;
        String theClass = "tdBase";
        if (altCnt%2!=0)
        {
            theClass = "tdBaseAlt";
        }
        leCurrentElement    = (ListElement) eList.nextElement();
        working_bltNIM3_Modality  = (bltNIM3_Modality) leCurrentElement.getObject();
        working_bltNIM3_Modality.GroupSecurityInit(UserSecurityGroupID);
        if (!working_bltNIM3_Modality.isComplete())
        {
            theClass = "incompleteItem";
        %>

        <%
        }
        else
        {
        %>
        <%
        }
        %>
        <tr class=<%=theClass%> > 

              <%String theClassF = "textBase";%>
 <td>
   <%theClassF = "textBase";%>
   <%if ((working_bltNIM3_Modality.isExpired("ModalityTypeID",expiredDays))&&(working_bltNIM3_Modality.isExpiredCheck("ModalityTypeID"))){theClassF = "expiredFieldMain";}%>
   <%if ( (working_bltNIM3_Modality.isRequired("ModalityTypeID"))&&(!working_bltNIM3_Modality.isComplete("ModalityTypeID")) ){theClassF = "requiredFieldMain";}%>
   <b>Modality:&nbsp;</b><jsp:include page="../generic/tModalityTypeLIShort_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltNIM3_Modality.getModalityTypeID()%>" /></jsp:include> <br>
   
   <%theClassF = "textBase";%>
   <%if ((working_bltNIM3_Modality.isExpired("ModalityModelID",expiredDays))&&(working_bltNIM3_Modality.isExpiredCheck("ModalityModelID"))){theClassF = "expiredFieldMain";}%>
   <%if ( (working_bltNIM3_Modality.isRequired("ModalityModelID"))&&(!working_bltNIM3_Modality.isComplete("ModalityModelID")) ){theClassF = "requiredFieldMain";}%>
   <b>Model:&nbsp;</b>
   <jsp:include page="../generic/tMRI_ModelLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltNIM3_Modality.getModalityModelID()%>" /></jsp:include> </td>
 </tr>
        <%
    }//end while
       }//end of if
  }
 %>        
          </table>
</td>
        </tr>
        <tr>
          <td><span class="tdHeader">Relationships for this Practice:</span>
          	&nbsp;
<%
{
    bltAdminPracticeLU_List_LU_PracticeID        bltAdminPracticeLU_List_LU_PracticeID        =    new bltAdminPracticeLU_List_LU_PracticeID(iPracticeID);
//declaration of Enumeration
    bltAdminPracticeLU        working_bltAdminPracticeLU;
    ListElement         leCurrentElement;
    java.util.Enumeration eList = bltAdminPracticeLU_List_LU_PracticeID.elements();
    while (eList.hasMoreElements())
    {
        leCurrentElement    = (ListElement) eList.nextElement();
        working_bltAdminPracticeLU  = (bltAdminPracticeLU) leCurrentElement.getObject();
		%>
        <br /><span class=tdBase>&nbsp;&nbsp;&nbsp;<%=new bltAdminMaster(working_bltAdminPracticeLU.getAdminID()).getName()%> [<strong><%=new bltAdminPracticeRelationshipTypeLI(working_bltAdminPracticeLU.getRelationshipTypeID()).getDescriptionLong()%></strong>]</span>
        <%
	}
  }
    %> 
            
            </td>
        </tr>
        <tr>
          <td><span class="tdHeader">Referral History (most recent 100 non-voided referrals):</span>
<table border="1" cellspacing="0" cellpadding="4" width="100%">
  <tr class="tdHeaderAlt">
    <td>Payer</td>
    <td>DOS</td>
    <td>Type</td>
    <td>Status</td>
    <td>ScanPass</td>
    <td>Last Name</td>
    </tr>
<%
		searchDB2 mySS = new searchDB2();
		java.sql.ResultSet myRS = null;
		try
		{
			String mySQL = "SELECT \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Parent_PayerName\", \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"PatientLastName\", \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Encounter_Status\", \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Encounter_Type\", \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Encounter_ScanPass\", \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Encounter_DateOfService_display\" FROM \"public\".\"vMQ4_Encounter_NoVoid_BP\" where \"PracticeID\"=? order by \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Encounter_DateOfService_display\" DESC Limit 100 ";
            db_PreparedStatementObject myDPSO = new db_PreparedStatementObject(mySQL);
            myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.INT_TYPE,iPracticeID.toString()));
            myRS = mySS.executePreparedStatement(myDPSO);
			int myCount =0;
			while (myRS!=null&&myRS.next())
			{
				myCount++;
				String alt_class = "tdBase";
				if (myCount%2==0)
				{
					alt_class = "tdBaseAlt";
				}
					%>
				  <tr class="<%=alt_class%>">
					<td><%=myRS.getString("Parent_PayerName")%></td>
					<td><%=PLCUtils.getDisplayDateWithTime(myRS.getTimestamp("Encounter_DateOfService_display"))%></td>
					<td><%=myRS.getString("Encounter_Type")%></td>
					<td><%=myRS.getString("Encounter_Status")%></td>
					<td><%=myRS.getString("Encounter_ScanPass")%></td>
					<td><%=myRS.getString("PatientLastName")%></td>
					</tr>
				<%
			}
		}
		catch(Exception e)
		{
//			out.println("ResultsSet:"+e);
            DebugLogger.printLine("PracticeStatus:ReferralHistory:["+e+"]");
		}
%>
</table>
</td>
        </tr>
      </table>



      <%//ConfigurationMessages.getHTML("INTERVIEWTopControl","practice_status")%> 
      </p>
    <p>
      <input type="button" class="tdBaseAltGrey"  onclick="this.disabled=true;document.location='tPracticeActivity_main_PracticeActivity_PracticeID.jsp';" name="button4" id="button4" value="View Activities" />
    </p></td>

<td width=10 valign="top">&nbsp</td>
<td width=10 valign="top">

      <table width="100%" border="1" cellspacing="0" cellpadding="5" bordercolor="#333333">
        <tr class=tdHeaderAlt > 
          <td nowrap>Provider Cost Calculator</td>
        </tr>
        <tr class="tdHeaderBase" > 
          <td nowrap class="tdBase">
		  <form id="testCalc" action="practice_calc.jsp" target="calcframe">
			CPT: <input type="text" name="cpt" maxlength="5" size="10" id="cpt"/>
				<a href="#" onClick="document.getElementById('cpt').value='72148';return false;">72148</a>&nbsp;&nbsp;&nbsp;&nbsp;
				<a href="#" onClick="document.getElementById('cpt').value='';return false;"><em>blank</em></a>			<br/>
			Modifier: <input type="text" name="mod" maxlength="2" size="4" id="mod"/>  
				<a href="#" onClick="document.getElementById('mod').value='26';return false;">26</a>&nbsp;&nbsp;&nbsp;&nbsp;
				<a href="#" onClick="document.getElementById('mod').value='TC';return false;">TC</a>&nbsp;&nbsp;&nbsp;&nbsp;
				<a href="#" onClick="document.getElementById('mod').value='';return false;"><em>blank</em></a>&nbsp;&nbsp;&nbsp;&nbsp;
			<br/>
			Date: <input type="text" name="dos" value="today" />
			<br/>
			<input class="tdHeaderAlt"   type="submit" value="Show Rate" />
		  </form>
		  
		  <iframe src="blank.jsp" width="250px" height="100px" name="calcframe"></iframe>
		  
		  
		  
		  </td>
        </tr>
	</table>



</td>
	
	</tr>
      </table>
	  <p>&nbsp;</p>
<%

  }
  else
  {
   response.sendRedirect("/errorhandler.jsp");
  }

%>

