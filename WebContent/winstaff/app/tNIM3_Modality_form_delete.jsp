<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.bltDocumentManagement,java.util.Vector,java.util.Enumeration,com.winstaff.ListElement, com.winstaff.PLCUtils, com.winstaff.*, com.winstaff.SecurityCheck, com.winstaff.ConfigurationMessages, com.winstaff.bltNIM3_Modality, com.winstaff.DocumentManagerUtils" %>
<%/*
    filename: out\jsp\tNIM3_Modality_form_delete.jsp
    Created on Oct/28/2009
    Type: _form Delete
    Created by: Scott Ellis
    Version: 4.0
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>


<%

//initial declaration of list class and parentID
    Integer        iModalityID        =    null;
    boolean accessValid = false;

   Integer iSecurityCheck = SecurityCheck.CheckItem("nim1", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {

    if (pageControllerHash.containsKey("iModalityID")) 
    {
        iModalityID        =    (Integer)pageControllerHash.get("iModalityID");
        if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {
            accessValid = true;
        }
    }
  //page security
  if (accessValid)
  {

//initial declaration of list class and parentID

    bltNIM3_Modality        NIM3_Modality        =    null;

    if ( request.getParameter( "EDIT" ).equalsIgnoreCase("del") )
    {
        NIM3_Modality        =    new    bltNIM3_Modality(iModalityID);
        NIM3_Modality.setPracticeID(new Integer("0"));
        //if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {
            NIM3_Modality.commitData();
            out.println("Successful delete");
        }
    }
String routePageReference = request.getParameter("routePageReference");
String nextPage=null;
if (routePageReference!=null)
{
        if (pageControllerHash.containsKey(routePageReference))
        {
            nextPage = (String)pageControllerHash.get(routePageReference);
        }
        else if (pageControllerHash.containsKey("sLocalChildReturnPage"))
        {
            nextPage = (String)pageControllerHash.get("sLocalChildReturnPage");
        }
}
if (nextPage!=null)
{
    response.sendRedirect(nextPage+"?EDIT=edit");
}
        %>
Successful Delete

  <%
  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
  %>


