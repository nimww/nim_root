<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.*" %>
<%/*
    filename: out\jsp\cl_Practice_Physician_via_CVO.jsp
    Created on Feb/17/2003
    Type: n-n main class file
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>


<%
//initial declaration of list class and parentID
    Integer        iPhysicianID        =    null;
    Integer        iCVOID        =    null;
    boolean accessValid = false;
   Integer iSecurityCheck = SecurityCheck.CheckItem("Practice1", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {

    if ((pageControllerHash.containsKey("iPhysicianID"))&&(pageControllerHash.containsKey("iCVOID"))) 
    {
        iPhysicianID        =    (Integer)pageControllerHash.get("iPhysicianID");
        iCVOID        =    (Integer)pageControllerHash.get("iCVOID");
        if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {
            accessValid = true;
        }
    }
  //page security
  if (accessValid)
  {
        Integer requestID = null;
        if (request.getParameter("EDITID")!=null)
        {
            requestID = new Integer(request.getParameter("EDITID"));
            out.println(requestID);
        }

//declaration of Enumeration
    bltPracticeMaster        working_bltPracticeMaster;
    ListElement         leCurrentElement;
	boolean hasAccess = DataControlUtils.getCVOPracticeAccess(iCVOID,  requestID);
     if (hasAccess)
     {
        bltPhysicianPracticeLU  working_bltPhysicianPracticeLU = new bltPhysicianPracticeLU();
        working_bltPhysicianPracticeLU.setPracticeID(requestID);
        working_bltPhysicianPracticeLU.setPhysicianID(iPhysicianID);
        working_bltPhysicianPracticeLU.commitData();



        pageControllerHash.put("iLookupID",working_bltPhysicianPracticeLU.getUniqueID());
        pageControllerHash.put("iPracticeID",working_bltPhysicianPracticeLU.getPracticeID());
        session.setAttribute("pageControllerHash",pageControllerHash);
        //Parameter Pass Code here
String parameterPassString ="";
java.util.Enumeration myParameterPassList = request.getParameterNames();
while (myParameterPassList.hasMoreElements())
{
	String myName = (String)myParameterPassList.nextElement();
	String myS = (String) request.getParameter(myName);
	parameterPassString+="&"+myName + "=" + myS;
}
        String targetRedirect = "tPracticeMaster_form_basic.jsp?EDIT=edit";

        if (pageControllerHash.containsKey("sParentReturnPage"))
        {
//            targetRedirect = (String)pageControllerHash.get("sParentReturnPage");
        }
        response.sendRedirect(targetRedirect);
    }

  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>


