
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.*" %>
<%/*
    filename: tAdminPracticeLU_main_LU_AdminID.jsp
    Created on Apr/23/2002
    Type: 1-n main class file
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>

<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_Clear.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>

    <table cellpadding=0 cellspacing=0 border=0>
    <tr><td width=10>&nbsp;</td>
    <td>
<p> <br />
   <span class=title>See NetDev Worklist</span><br />
<%
if (isScheduler3)    
{
	%>
    You are currently viewing the NetDev Worklist as a Scheduler Level 3 (Admin).  <br />
    <a href="../nim3/Billing_Home.jsp"> Return to ScanPass Billing Home</a>
    <%
}
%>
<p>


    <%
//initial declaration of list class and parentID
    Integer        iGenAdminID        =    null;
	String		savedSQL = null;
	String		savedURL = null;
    boolean accessValid = false;
    if (pageControllerHash.containsKey("iGenAdminID")) 
    {
        iGenAdminID        =    (Integer)pageControllerHash.get("iGenAdminID");
        accessValid = true;
    }
    else if (isScheduler3) 
    {
        accessValid = true;
    }
  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat dbdft = new java.text.SimpleDateFormat(PLCUtils.String_dbdft);
      java.text.SimpleDateFormat dbdf_day = new java.text.SimpleDateFormat(PLCUtils.String_displayDateDayWeek);
      java.text.SimpleDateFormat displayDateTimeSDF = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1Full);
      java.text.SimpleDateFormat displayDateSDF = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);
//      pageControllerHash.put("sParentReturnPage","AdminPracticeAll_home-acc.jsp");
//			session.setAttribute("pageControllerHash",pageControllerHash);
%>



<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr> 
    <td>
    
      <p class=title><strong>Comm</strong>unication <strong>Tracks</strong></p>
<%
try
{
String myExtUserID = "";
String myCommTypeID = "";
String myCommStart = "";
String myCommEnd = "";
String myMessageText = "";
String myMessageName = "";
String myMessageCompany = "";
String myMessageEmail = "";
String myMessagePhone = "";
String myMessageFax = "";
String myAlertStatusCode = "";
String myCourtesyStatus = "";
 
String myComments = "";
String orderBy = "CommStart";
int startID = 0;
int maxResults = 50;

boolean firstDisplay = false;

try
{
maxResults = Integer.parseInt(request.getParameter("maxResults"));
startID = Integer.parseInt(request.getParameter("startID"));
//myExtUserID = request.getParameter("ExtUserID");
//myCommTypeID = request.getParameter("CommTypeID");
myCommStart = request.getParameter("CommStart");
//myCommEnd = request.getParameter("CommEnd");
myMessageText = request.getParameter("MessageText");
myMessageName = request.getParameter("MessageName");
//myMessageCompany = request.getParameter("MessageCompany");
//myMessageEmail = request.getParameter("MessageEmail");
//myMessagePhone = request.getParameter("MessagePhone");
//myMessageFax = request.getParameter("MessageFax");
myAlertStatusCode = request.getParameter("AlertStatusCode");
myCourtesyStatus = request.getParameter("CourtesyStatus");
//myComments = request.getParameter("Comments");
orderBy = request.getParameter("orderBy");
}
catch (Exception e)
{
maxResults = 50;
startID = 0;
firstDisplay = true;
myExtUserID = "";
myCommTypeID = "";
myCommStart = "";
myCommEnd = "";
myMessageText = "";
myMessageName = "";
myMessageCompany = "";
myMessageEmail = "";
myMessagePhone = "";
myMessageFax = "";
myAlertStatusCode = "0";
myCourtesyStatus = "-1";
myComments = "";
orderBy = "CommStart";
}
if (orderBy == null)
{
maxResults = 50;
startID = 0;
firstDisplay = true;
myExtUserID = "";
myCommTypeID = "";
myCommStart = "";
myCommEnd = "";
myMessageText = "";
myMessageName = "";
myMessageCompany = "";
myMessageEmail = "";
myMessagePhone = "";
myMessageFax = "";
myAlertStatusCode = "0";
myCourtesyStatus = "-1";
myComments = "";
orderBy = "CommStart";
}
firstDisplay = false;

String orderBy2;
if (orderBy.equalsIgnoreCase("CommStart")||orderBy.equalsIgnoreCase("CommEnd"))
{
	orderBy2 = orderBy + " desc";
}
else
{
	orderBy2 = orderBy + "";
}

//no first display
{


String myWhere = "where (commtrackid>0 AND CommTypeID=4  " ;
boolean theFirst = false;

try
{
if (!myExtUserID.equalsIgnoreCase(""))
{
if (!theFirst) { myWhere+=" and ";}
if (myExtUserID.indexOf("%")>=0){myWhere += "ExtUserID LIKE '" + myExtUserID +"'";}
else if (myExtUserID.indexOf("=")==0){myWhere += "ExtUserID = '" + myExtUserID.substring(1, myExtUserID.length())+"'";}
else if (myExtUserID.length()<=3){myWhere += "ExtUserID LIKE '" + myExtUserID +"%'";}
else {myWhere += "ExtUserID LIKE '%" + myExtUserID +"%'";}
theFirst = false;
}
if (!myCommTypeID.equalsIgnoreCase(""))
{
if (!theFirst) { myWhere+=" and ";}
if (myCommTypeID.indexOf("%")>=0){myWhere += "CommTypeID LIKE '" + myCommTypeID +"'";}
else if (myCommTypeID.indexOf("=")==0){myWhere += "CommTypeID = '" + myCommTypeID.substring(1, myCommTypeID.length())+"'";}
else if (myCommTypeID.length()<=3){myWhere += "CommTypeID LIKE '" + myCommTypeID +"%'";}
else {myWhere += "CommTypeID LIKE '%" + myCommTypeID +"%'";}
theFirst = false;
}
if (!myCommStart.equalsIgnoreCase(""))
{
if (!theFirst) { myWhere+=" and ";}
myWhere += "CommStart > '" + dbdf.format(displayDateSDF.parse(myCommStart))+"'";
theFirst = false;
}
if (!myCommEnd.equalsIgnoreCase(""))
{
if (!theFirst) { myWhere+=" and ";}
myWhere += "CommEnd > '" + myCommEnd+"'";
theFirst = false;
}
if (!myMessageText.equalsIgnoreCase(""))
{
if (!theFirst) { myWhere+=" and ";}
if (myMessageText.indexOf("%")>=0){myWhere += "UPPER(MessageText) LIKE '" + myMessageText.toUpperCase() +"'";}
else if (myMessageText.indexOf("=")==0){myWhere += "UPPER(MessageText) = '" + myMessageText.toUpperCase().substring(1, myMessageText.length())+"'";}
else if (myMessageText.length()<=3){myWhere += "UPPER(MessageText) LIKE '" + myMessageText.toUpperCase() +"%'";}
else {myWhere += "UPPER(MessageText) LIKE '%" + myMessageText.toUpperCase() +"%'";}
theFirst = false;
}
if (!myMessageName.equalsIgnoreCase(""))
{
if (!theFirst) { myWhere+=" and ";}
if (myMessageName.indexOf("%")>=0){myWhere += "UPPER(MessageName) LIKE '" + myMessageName.toUpperCase() +"'";}
else if (myMessageName.indexOf("=")==0){myWhere += "UPPER(MessageName) = '" + myMessageName.toUpperCase().substring(1, myMessageName.length())+"'";}
else if (myMessageName.length()<=3){myWhere += "UPPER(MessageName) LIKE '" + myMessageName.toUpperCase() +"%'";}
else {myWhere += "UPPER(MessageName) LIKE '%" + myMessageName.toUpperCase() +"%'";}
theFirst = false;
}
if (!myMessageCompany.equalsIgnoreCase(""))
{
if (!theFirst) { myWhere+=" and ";}
if (myMessageCompany.indexOf("%")>=0){myWhere += "UPPER(MessageCompany) LIKE '" + myMessageCompany.toUpperCase() +"'";}
else if (myMessageCompany.indexOf("=")==0){myWhere += "UPPER(MessageCompany) = '" + myMessageCompany.toUpperCase().substring(1, myMessageCompany.length())+"'";}
else if (myMessageCompany.length()<=3){myWhere += "UPPER(MessageCompany) LIKE '" + myMessageCompany.toUpperCase() +"%'";}
else {myWhere += "UPPER(MessageCompany) LIKE '%" + myMessageCompany.toUpperCase() +"%'";}
theFirst = false;
}
if (!myMessageEmail.equalsIgnoreCase(""))
{
if (!theFirst) { myWhere+=" and ";}
if (myMessageEmail.indexOf("%")>=0){myWhere += "UPPER(MessageEmail) LIKE '" + myMessageEmail.toUpperCase() +"'";}
else if (myMessageEmail.indexOf("=")==0){myWhere += "UPPER(MessageEmail) = '" + myMessageEmail.toUpperCase().substring(1, myMessageEmail.length())+"'";}
else if (myMessageEmail.length()<=3){myWhere += "UPPER(MessageEmail) LIKE '" + myMessageEmail.toUpperCase() +"%'";}
else {myWhere += "UPPER(MessageEmail) LIKE '%" + myMessageEmail.toUpperCase() +"%'";}
theFirst = false;
}
if (!myMessagePhone.equalsIgnoreCase(""))
{
if (!theFirst) { myWhere+=" and ";}
if (myMessagePhone.indexOf("%")>=0){myWhere += "UPPER(MessagePhone) LIKE '" + myMessagePhone.toUpperCase() +"'";}
else if (myMessagePhone.indexOf("=")==0){myWhere += "UPPER(MessagePhone) = '" + myMessagePhone.toUpperCase().substring(1, myMessagePhone.length())+"'";}
else if (myMessagePhone.length()<=3){myWhere += "UPPER(MessagePhone) LIKE '" + myMessagePhone.toUpperCase() +"%'";}
else {myWhere += "UPPER(MessagePhone) LIKE '%" + myMessagePhone +"%'";}
theFirst = false;
}
if (!myMessageFax.equalsIgnoreCase(""))
{
if (!theFirst) { myWhere+=" and ";}
if (myMessageFax.indexOf("%")>=0){myWhere += "UPPER(MessageFax) LIKE '" + myMessageFax.toUpperCase() +"'";}
else if (myMessageFax.indexOf("=")==0){myWhere += "UPPER(MessageFax) = '" + myMessageFax.toUpperCase().substring(1, myMessageFax.length())+"'";}
else if (myMessageFax.length()<=3){myWhere += "UPPER(MessageFax) LIKE '" + myMessageFax.toUpperCase() +"%'";}
else {myWhere += "UPPER(MessageFax) LIKE '%" + myMessageFax.toUpperCase() +"%'";}
theFirst = false;
}
if (!myAlertStatusCode.equalsIgnoreCase("")&&!myAlertStatusCode.equalsIgnoreCase("-1"))
{
	if (!theFirst) { myWhere+=" and ";}
	myWhere += "AlertStatusCode = '" + myAlertStatusCode.toUpperCase() +"'";
	theFirst = false;
}
if (myCourtesyStatus.equalsIgnoreCase("-2"))
{
	if (!theFirst) { myWhere+=" and ";}
	myWhere += "iscourtesy in (0,2,4)";
	theFirst = false;
}
else if (!myCourtesyStatus.equalsIgnoreCase("")&&!myCourtesyStatus.equalsIgnoreCase("-1"))
{
	if (!theFirst) { myWhere+=" and ";}
	myWhere += "iscourtesy = '" + myCourtesyStatus +"'";
	theFirst = false;
}
if (!myComments.equalsIgnoreCase(""))
{
if (!theFirst) { myWhere+=" and ";}
if (myComments.indexOf("%")>=0){myWhere += "UPPER(Comments) LIKE '" + myComments.toUpperCase() +"'";}
else if (myComments.indexOf("=")==0){myWhere += "UPPER(Comments) = '" + myComments.toUpperCase().substring(1, myComments.length())+"'";}
else if (myComments.length()<=3){myWhere += "UPPER(Comments) LIKE '" + myComments.toUpperCase() +"%'";}
else {myWhere += "UPPER(Comments) LIKE '%" + myComments.toUpperCase() +"%'";}
theFirst = false;
}
myWhere += ")";

//System.out.println(myWhere);
if (theFirst||myWhere.equalsIgnoreCase(")"))
{
myWhere = "";
}


}
catch(Exception e)
{
out.println("FFF:"+e);
}

searchDB2 mySS = new searchDB2();

java.sql.ResultSet myRS = null;;

try
{
	String mySQL = "select * from tNIM3_CommTrack INNER JOIN tNIM3_Encounter on tNIM3_Encounter.encounterid = tNIM3_CommTrack.encounterid " + myWhere + " order by " + orderBy2 + " LIMIT " + (maxResults+1) + " OFFSET " + startID;
//	out.print("<hr>" + mySQL + "<hr>");
	myRS = mySS.executeStatement(mySQL);
}
catch(Exception e)
{
out.println("ResultsSet:"+e);
}

String myMainTable= " ";
try
{

	%>
	
	  <script language="JavaScript">
	<!--
	function MM_openBrWindow(theURL,winName,features) { //v2.0
	  window.open(theURL,winName,features);
	}
	//-->
	</script>
	</p>
	<table width=100% border=0 cellpadding=0 cellspacing=0 bordercolor=#003333>
	<tr>
	<td width=10>&nbsp;</td>
	<td>
	<table width=100% border=1 cellpadding=2 cellspacing=0 bordercolor=#003333>
	<tr>
	<td>
	
	<form name="selfForm" id="selfForm" method="POST" action="AdminPracticeAll_home-seend.jsp">
	  <table border=0 cellspacing=3 width="100%" cellpadding=3>
		<tr > 
		  <td colspan=3 class=title><p>
		    <input type="button" class="tdBaseAlt" name="SearchAllNeg" onclick="document.getElementById('CourtesyStatus').value='4';document.getElementById('AlertStatusCode').value='-1';document.getElementById('selfForm').submit();" value="Pending Approval" align="middle" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="button" class="tdBaseAlt" name="SearchOpen" onclick="document.getElementById('CourtesyStatus').value='-1';document.getElementById('AlertStatusCode').value='0';document.getElementById('selfForm').submit();" value="Open See NetDev" align="middle" />
		    <br />
		    <input type="button" class="tdBaseAlt" name="SearchAllNeg2" onclick="document.getElementById('CourtesyStatus').value='-2';document.getElementById('AlertStatusCode').value='3';document.getElementById('selfForm').submit();" value="Pending Approval (2)" align="middle" />
		  </p></td>
		  <td colspan=1 align=right nowrap="nowrap" class=tdBase>
  <div id="dPrev"></div>
</td>
		  <td colspan=1 nowrap="nowrap" class=tdBase> 
  <div id="dNext"></div>
		  </td>
		  <td  nowrap> 
	<input type=hidden name=maxResults value=<%=maxResults%> >
	<input type=hidden name=startID value=0 >
	<p><strong>Message:</strong>
	<input name="MessageText" type="text" id="MessageText" value="<%=myMessageText%>" size="25">
	</p></td> 
		  <td class=tdHeader> 
			<p  align="center"> 
			  Sort:
				<select name=orderBy id="orderBy">
				  <option value=ExtUserID <%if (orderBy.equalsIgnoreCase("ExtUserID")){out.println(" selected");}%> >ExtUserID</option>
				  <option value=CommTypeID <%if (orderBy.equalsIgnoreCase("CommTypeID")){out.println(" selected");}%> >CommTypeID</option>
				  <option value=CommStart <%if (orderBy.equalsIgnoreCase("CommStart")){out.println(" selected");}%> >CommStart</option>
				  <option value=CommEnd <%if (orderBy.equalsIgnoreCase("CommEnd")){out.println(" selected");}%> >CommEnd</option>
				  <option value=MessageText <%if (orderBy.equalsIgnoreCase("MessageText")){out.println(" selected");}%> >MessageText</option>
				  <option value=MessageName <%if (orderBy.equalsIgnoreCase("MessageName")){out.println(" selected");}%> >MessageName</option>
				  <option value=MessageCompany <%if (orderBy.equalsIgnoreCase("MessageCompany")){out.println(" selected");}%> >MessageCompany</option>
				  <option value=MessageEmail <%if (orderBy.equalsIgnoreCase("MessageEmail")){out.println(" selected");}%> >MessageEmail</option>
				  <option value=MessagePhone <%if (orderBy.equalsIgnoreCase("MessagePhone")){out.println(" selected");}%> >MessagePhone</option>
				  <option value=MessageFax <%if (orderBy.equalsIgnoreCase("MessageFax")){out.println(" selected");}%> >MessageFax</option>
				  <option value=AlertStatusCode <%if (orderBy.equalsIgnoreCase("AlertStatusCode")){out.println(" selected");}%> >AlertStatusCode</option>
				  <option value=Comments <%if (orderBy.equalsIgnoreCase("Comments")){out.println(" selected");}%> >Comments</option>
				</select>
			</p>
			<p  align="center">
			  <input type="Submit" class="tdBaseAltGreen" name="Submit" value="Submit" align="middle">
			</p>
		  </td>
		</tr>
		<tr class=tdHeader> 
		  <td  colspan=1 width=50>&nbsp; 
	
		  </td>
		  <td colspan=1>&nbsp;</td>
		  <td colspan=1>&nbsp;</td>
		  <td colspan=1 nowrap> 
			Start Date
			&gt;
			<input type="text" name="CommStart" value="<%=myCommStart%>" size="14">
		  </td>
		  <td colspan=1 nowrap>&nbsp;</td>
		  <td nowrap="nowrap"> 
		    From: 
		    <input type="text" name="MessageName" value="<%=myMessageName%>" size="20">
		    </td>
		  <td nowrap="nowrap"> 
  CommTrack:
  <select   name="AlertStatusCode" id="AlertStatusCode" >
<option <%if (myAlertStatusCode.equalsIgnoreCase("-1") ) {out.println("selected");}%> value="-1">All</option>
<option <%if (myAlertStatusCode.equalsIgnoreCase("0") ) {out.println("selected");}%> value=0>Open</option>
<option <%if (myAlertStatusCode.equalsIgnoreCase("1") ) {out.println("selected");}%> value=1>Closed</option>
<option <%if (myAlertStatusCode.equalsIgnoreCase("2") ) {out.println("selected");}%> value=2>Response (+)</option>
<option <%if (myAlertStatusCode.equalsIgnoreCase("3") ) {out.println("selected");}%> value=3>Response (-)</option>
</select>
  <br />
  Courtesy:<select name="CourtesyStatus" id="CourtesyStatus">
  <option <%if (myCourtesyStatus.equalsIgnoreCase("-1") ) {out.println("selected");}%> value="-1">Show All</option>
  <option <%if (myCourtesyStatus.equalsIgnoreCase("0") ) {out.println("selected");}%> value=0>NA</option>
<option <%if (myCourtesyStatus.equalsIgnoreCase("1") ) {out.println("selected");}%> value=1>Yes</option>
<option <%if (myCourtesyStatus.equalsIgnoreCase("2") ) {out.println("selected");}%> value=2>No</option>
<option <%if (myCourtesyStatus.equalsIgnoreCase("3") ) {out.println("selected");}%> value=3>Decline</option>
<option <%if (myCourtesyStatus.equalsIgnoreCase("4") ) {out.println("selected");}%> value=4>Pending</option>
<option <%if (myCourtesyStatus.equalsIgnoreCase("-2") ) {out.println("selected");}%> value="-2">Pending, No, NA</option>
</select></td>
		  </tr>
	
	
	<%
	
	int endCount = 0;
	int cnt=0;
	int cnt2=0;
	
	
	
	while (myRS!=null&&myRS.next())
	   {
		   %>
           <tr bgcolor=#333333>
           <td colspan="8">&nbsp;      </td>
           </tr>
           <%
			cnt++;
			if (cnt<=maxResults)
			{
				cnt2++;
				String myClass = "tdBase";
				bltNIM3_CommTrack myCT = new bltNIM3_CommTrack (new  Integer(myRS.getString("commtrackid")));
				NIM3_EncounterObject myEO = new NIM3_EncounterObject(myCT.getEncounterID());
				int houseSince = Math.abs(PLCUtils.getHoursTill(myCT.getCommStart()));
//				String myClass2 = "";
				if (myCT.getAlertStatusCode()==0)
				{
					if (houseSince>48)
					{
						myClass = "tdBaseAltRed";
					}
					else if (houseSince>24 || myEO.isEscalatedSTAT())
					{
						myClass = "tdBaseAltOrange";
					}
					else if (houseSince>12 )
					{
						myClass = "tdBaseAltYellow";
					}
					else
					{
						myClass = "tdBaseAltGrey";
					}
				}
				else
				{
						myClass = "tdBase";
				}

				
				%>
                <tr class="<%=myClass%>" >
                <%
				out.print("<td><em><strong>"+myCT.getCommTrackID()+"</strong></em></td>");
				out.print("<td>");
				if (myCT.getExtUserID().intValue()==19)
				{
					out.print("[AT]");
				}
				else if (myCT.getExtUserID().intValue()==0)
				{
					out.print("[OF]");
				}
				else
				{
					myMainTable +=myRS.getString("ExtUserID")+"";
				}
				out.print("</td>");
				out.print("<td>");
				if (myCT.getCommTypeID().intValue()==1)
				{
					out.print("Msg");
				}
				else if (myCT.getCommTypeID().intValue()==3)
				{
					out.print("Notification");
				}
				else if (myCT.getCommTypeID().intValue()==4)
				{
					out.print("SeeND");
				}
				else if (myCT.getCommTypeID().intValue()==2&&myCT.getCaseID().intValue()>0)
				{
					out.print("<input class=\"inputButton_md_Action2\" type=button onClick = \"this.disabled=true;document.location ='tNIM3_CaseAccount_main_NIM3_CaseAccount_PayerID_form_authorize.jsp?EDIT=openflow&EDITID=" + myCT.getCaseID() +"&KM=p'\" value=Referral>");
				}
				else
				{
					out.print("Other");
				}
				out.print("</td>");
				out.print("<td colspan=2>");
				if (myCT.getCommEnd().after(NIMUtils.getBeforeTime()))
				{
					out.print(displayDateTimeSDF.format(myCT.getCommStart()) + "<br>(Dur: <strong>" + Math.round((myCT.getCommEnd().getTime()-myCT.getCommStart().getTime())/1000/60) + "</strong> min)");
				}
				else 
				{
					out.print("" + displayDateTimeSDF.format(myCT.getCommStart()) + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Item Still Open&nbsp;&nbsp;&nbsp;Started: <strong>" + PLCUtils.getHoursTill_Display(myCT.getCommStart(), false)+ "  (" + PLCUtils.getDaysTill_Display(myCT.getCommStart(), true) + ")</strong>");
				}
				
				out.print("</td>");
				out.print("<td colspan=1 >From: ");
				out.print("<strong>" + myRS.getString("MessageName")+"</strong>");
				out.print("</td>");
				out.print("<td colspan=1>");
				%>
<jsp:include page="../generic/tCommTrackAlertStatusCodeLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=myCT.getAlertStatusCode()%>" /></jsp:include>                
                <%
				//myMainTable +=myRS.getString("AlertStatusCode")+"";
				out.print("</td>");
				out.print("</tr>");
				
				out.print("<tr class="+myClass+">");
				out.print("<td colspan=8>");
				out.print("<table width=100% border=1 cellpadding=2 bgcolor=EEEEEE>");
				out.print("<tr>");
				out.print("<td>Request:<br><textarea cols=80 rows=8 readonly=\"readonly\">" + myRS.getString("MessageText")+"</textarea></td>");
				out.print("<td>Response: <input class=\"tdBaseAltYellow\" onClick=\"document.location='SeeND_Edit.jsp?EDITID=" + myCT.getCommTrackID() + " ';\" type=button value=\"Edit Response\" name=\"response\"><br><textarea cols=80 rows=8 readonly=\"readonly\">" + myRS.getString("Comments")+"</textarea></td>");
				out.print("</tr>");
				out.print("<tr><td colspan=2>");
//start

  {
      java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);
%>
<table bgcolor="#EEEEEE" width="98%" border="1" cellspacing="4" cellpadding="1">
  <tr class="tdHeaderAlt">
    <td class="tdHeader">Payer</td>
    <td class="tdHeader">Patient</td>
    <td colspan="2" class="tdHeader">Procedure</td>
  </tr>
  <tr>
    <td valign="middle" rowspan="4" ><strong><%=myEO.getPayerFullName()%></strong><br>ADJ: <%=myEO.Case_Adjuster.getContactFirstName()%> <%=myEO.Case_Adjuster.getContactLastName()%><br>Claim#: <%=myEO.NIM3_CaseAccount.getCaseClaimNumber()%>
</td>
    <td valign="middle" nowrap="nowrap"><strong><%=myEO.NIM3_CaseAccount.getPatientLastName()%></strong>, <strong><%=myEO.NIM3_CaseAccount.getPatientFirstName()%></strong></td>
    <td rowspan="3" nowrap>
	Is STAT: <strong><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=myEO.NIM3_Encounter.getisSTAT()%>" /></jsp:include><br>
    
    <%
        if(myEO.NIM3_Encounter.getisVIP() == 1){
           out.print("Is VIP: Yes");
        }
        else{
            out.print("Is VIP: --");
        }
          %>
          
          
    
    </strong>
<hr />	Requires Aging: <strong><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=myEO.NIM3_Encounter.getRequiresAgeInjury()%>" /></jsp:include></strong><hr />
Courtesy? <strong><jsp:include page="../generic/tCourtesyLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=myEO.NIM3_Encounter.getisCourtesy()%>" /></jsp:include></strong><hr />
    </td>
    <td rowspan="3" valign="top">
	  <%
if (myEO.NIM3_Referral.getRxFileID()!=0)
{
%>
	  <a target="_blank" href="../nim3/tNIM3_Document_main_NIM3_Document_CaseID_form_authorize.jsp?EDIT=view&amp;EDITID=<%=myEO.NIM3_Referral.getRxFileID()%>&amp;KM=p';" class="tdHeader" >View Rx</a><hr />
      <%
}
%>
<%{
		
	//declaration of Enumeration
		bltNIM3_Service        NIM3_Service;
		ListElement         leCurrentElement;
		java.util.Enumeration eList = myEO.NIM3_Service_List.elements();
		while (eList.hasMoreElements())
		{
			leCurrentElement    = (ListElement) eList.nextElement();
			NIM3_Service  = (bltNIM3_Service) leCurrentElement.getObject();
			%>
            <strong><%=NIM3_Service.getCPT()%></strong>&nbsp;&nbsp;BP: <strong><%=NIM3_Service.getCPTBodyPart()%></strong>
    <%
		}
		
		
		
		}%></td>
  </tr>
  <tr>
    <td valign="middle" nowrap="nowrap">Address: <strong><%=myEO.getPatientAddressObject().getAddressFull("&nbsp;&nbsp;&nbsp;")%></strong></td>
  </tr>
  <tr>
    <td valign="middle" nowrap="nowrap">Ref Dr: <strong><%=myEO.Referral_ReferringDoctor.getContactLastName()%></strong>, <strong><%=myEO.Referral_ReferringDoctor.getContactFirstName()%></strong></td>
  </tr>
</table>



  <%}





//end
				out.print("</td>");
				out.print("</tr></table></td></tr>");
				
				
			}
	   }
	mySS.closeAll();
endCount = cnt;


if (startID>=endCount)
{
//startID = endCount-maxResults;

}

if (maxResults<=0)
{
maxResults=5;
}


if (startID<=0)
{
startID=0;
}


if (startID>0)
{
	%>
    	<script language="javascript">
        document.getElementById('dPrev').innerHTML='<a href="tNIM3_CommTrack_query_all.jsp?startID=<%=(startID-maxResults)%>&amp;maxResults=<%=maxResults%>&amp;orderBy=<%=orderBy%>&amp;ExtUserID=<%=myExtUserID%>&amp;CommTypeID=<%=myCommTypeID%>&amp;CommStart=<%=myCommStart%>&amp;CommEnd=<%=myCommEnd%>&amp;MessageText=<%=myMessageText%>&amp;MessageName=<%=myMessageName%>&amp;MessageCompany=<%=myMessageCompany%>&amp;MessageEmail=<%=myMessageEmail%>&amp;MessagePhone=<%=myMessagePhone%>&amp;MessageFax=<%=myMessageFax%>&amp;AlertStatusCode=<%=myAlertStatusCode%>&amp;Comments=<%=myComments%>"><< previous <%=maxResults%></a> ';
        </script>
	<%
}
//if ((startID+maxResults)<endCount)
if (endCount>maxResults)
{
//out.println("[" + startID + "]");
%>
    	<script language="javascript">
        document.getElementById('dNext').innerHTML='<a href="tNIM3_CommTrack_query_all.jsp?startID=<%=(startID+maxResults)%>&amp;maxResults=<%=maxResults%>&amp;orderBy=<%=orderBy%>&amp;ExtUserID=<%=myExtUserID%>&amp;CommTypeID=<%=myCommTypeID%>&amp;CommStart=<%=myCommStart%>&amp;CommEnd=<%=myCommEnd%>&amp;MessageText=<%=myMessageText%>&amp;MessageName=<%=myMessageName%>&amp;MessageCompany=<%=myMessageCompany%>&amp;MessageEmail=<%=myMessageEmail%>&amp;MessagePhone=<%=myMessagePhone%>&amp;MessageFax=<%=myMessageFax%>&amp;AlertStatusCode=<%=myAlertStatusCode%>&amp;Comments=<%=myComments%>"> next >> <%=maxResults%></a>   ';
        </script>
        <%
}




	
	
	
	%>
	
	
	
	</table> 
	

<%

}
catch(Exception e)
{
out.println("Display:"+e);
}





}
}
catch (Exception e)
{
out.println("Error???:"+e);
System.out.println("Error:"+e);
}

%>



  </table>
</td>
</tr>
</table>
</form></td>
</tr>
</table> 

    </td>
  </tr>
</table>

<%

  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
%>
    
    
    
    </td>
  </tr>
</table>



    </td></tr></table>

