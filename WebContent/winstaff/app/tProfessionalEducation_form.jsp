<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.bltDocumentManagement,java.util.Vector,java.util.Enumeration,com.winstaff.ListElement, com.winstaff.PLCUtils, com.winstaff.SecurityCheck, com.winstaff.ConfigurationMessages,com.winstaff.bltProfessionalEducation" %>
<%/*

    filename: out\jsp\tProfessionalEducation_form.jsp
    Created on Mar/21/2003
    Created by: Scott Ellis
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>

<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_PhysicianID.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>

<script>
function addTitle()
{
	if (document.forms[0].Other.value =="")
	{
	}
	else
	{
		document.forms[0].Other.value = document.forms[0].Other.value + ", ";
	}
	document.forms[0].Other.value = document.forms[0].Other.value + document.forms[0].quickTitle.value;
        textAreaStop(document.forms[0].Other,50);
}

</script>



    <table cellpadding=0 cellspacing=0 border=0 width=<%=MasterTableWidth%> >
    <tr><td width=10>&nbsp;</td><td>
    <%=ConfigurationMessages.getHTML("INTERVIEWTopControl_form","tProfessionalEducation")%>



<%
//initial declaration of list class and parentID
    Integer        iProfessionalEducationID        =    null;
    boolean accessValid = false;
    // required for Type2
    String sKeyMasterReference = null;

   Integer iSecurityCheck = SecurityCheck.CheckItem("PractitionerSection6", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {
    if (pageControllerHash.containsKey("iProfessionalEducationID")) 
    {
        iProfessionalEducationID        =    (Integer)pageControllerHash.get("iProfessionalEducationID");
        accessValid = true;    }
  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);      java.text.SimpleDateFormat displayDateTimeSDF = new java.text.SimpleDateFormat("MM/dd/yyyy hh:mm a");

    pageControllerHash.put("sLocalChildReturnPage","tProfessionalEducation_form.jsp");
//initial declaration of list class and parentID

    bltProfessionalEducation        ProfessionalEducation        =    null;

    if ( request.getParameter( "EDIT" ).equalsIgnoreCase("edit") )
    {
        ProfessionalEducation        =    new    bltProfessionalEducation(iProfessionalEducationID,UserSecurityGroupID);
    }
    else if ( request.getParameter( "EDIT" ).equalsIgnoreCase("new") )
    {
        ProfessionalEducation        =    new    bltProfessionalEducation(UserSecurityGroupID, true);
    }

//fields
        %>
        <%@ include file="tProfessionalEducation_form_instructions.jsp" %>
        <form action="tProfessionalEducation_form_sub.jsp" name="tProfessionalEducation_form1" method="POST">
<%
    if ( request.getParameter( "EDIT" ) != null )
    {
    %>
        <input type="hidden" name="EDIT" value = "<%=request.getParameter( "EDIT" )%>" >  
    <%
    }
%>
          <%  String theClass ="tdBase";%>
        <table width=100% border=1 bordercolor=#333333 cellpadding=3 cellspacing=0 class=tableBase>
         <tr><td class=tableColor>


            <table cellpadding=0 cellspacing=0 width=100%>
            <%
            if ( (ProfessionalEducation.isRequired("DegreeID",UserSecurityGroupID))&&(!ProfessionalEducation.isComplete("DegreeID")) )
            {
                theClass = "requiredField";
            }
            else if ((ProfessionalEducation.isExpired("DegreeID",expiredDays))&&(ProfessionalEducation.isExpiredCheck("DegreeID",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((ProfessionalEducation.isWrite("DegreeID",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Degree Type&nbsp;</b></p></td><td valign=top><p><select   name="DegreeID" ><jsp:include page="../generic/tDegreeTypeLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=ProfessionalEducation.getDegreeID()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=DegreeID&amp;sTableName=tProfessionalEducation&amp;sRefID=<%=ProfessionalEducation.getProfessionalEducationID()%>&amp;sFieldNameDisp=<%=ProfessionalEducation.getEnglish("DegreeID")%>&amp;sTableNameDisp=tProfessionalEducation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((ProfessionalEducation.isRead("DegreeID",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Degree Type&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tDegreeTypeLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=ProfessionalEducation.getDegreeID()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=DegreeID&amp;sTableName=tProfessionalEducation&amp;sRefID=<%=ProfessionalEducation.getProfessionalEducationID()%>&amp;sFieldNameDisp=<%=ProfessionalEducation.getEnglish("DegreeID")%>&amp;sTableNameDisp=tProfessionalEducation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (ProfessionalEducation.isRequired("Other",UserSecurityGroupID))&&(!ProfessionalEducation.isComplete("Other")) )
            {
                theClass = "requiredField";
            }
            else if ((ProfessionalEducation.isExpired("Other",expiredDays))&&(ProfessionalEducation.isExpiredCheck("Other",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((ProfessionalEducation.isWrite("Other",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>List Degree/Title here:</b></p></td><td valign=top><p><input maxlength="50" type=text size="80" name="Other" value="<%=ProfessionalEducation.getOther()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Other&amp;sTableName=tProfessionalEducation&amp;sRefID=<%=ProfessionalEducation.getProfessionalEducationID()%>&amp;sFieldNameDisp=<%=ProfessionalEducation.getEnglish("Other")%>&amp;sTableNameDisp=tProfessionalEducation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%>
<br><b class=tdBase>Quick Add Title/Degree:</b>
					  <select name=quickTitle onChange="addTitle()">
<option value="">Please Select</option>
<option value="MD">MD - Doctor of Medicine</option>
<option value="DO">DO - Doctor of Osteopathy</option>
<option value="DPM">DPM - Podiatrist</option>
<option value="DMD">DMD - Doctor of Dental Medical Dentistry</option>
<option value="DDS">DDS - Doctor of Dental Surgery</option>
<option value="OD">OD - Optometrist</option>
<option value="PhD">PhD - Doctor of Philosophy in Psychology</option>
<option value="LM">LM - Licensed Midwife</option>
<option value="MSW">MSW - Master Social Work</option>
<option value="BS">BS - Bachelors of Science</option>
<option value="BSN">BSN - Bachelors of Science in Nursing</option>
<option value="BA">BA - Bachelors of Arts</option>
<option value="PA">PA - Physician Assistant</option>
<option value="ARNP">ARNP - Advanced Registered Nurse Practitioner</option>
<option value="CRNA">CRNA - Certified Registered Nurse Anesthetist</option>
<option value="CNM">CNM - Certified Nurse Midwife</option>
<option value="MA">MA - Masters of Arts</option>
<option value="MS">MS - Masters of Science</option>
<option value="PsyD">PsyD - Doctorate in Psychology</option>
<option value="MSN">MSN - Masters of Science in Nursing</option>
<option value="MPH">MPH - Masters in Public Health</option>
<option value="Ed.D.">Ed.D. - Doctor of Education</option>
<option value="M.Ed.">M.Ed. - Masters in Education</option>
<option value="RNFA">RNFA - Registered Nurse First Assistant</option>
<option value="RN">RN - Registered Nurse</option>
<option value="CMHC">CMHC - Clinical Mental Health Counselor</option>
<option value="DC">DC - Doctor of Chiropractic</option>
<option value="FACS">FACS - Fellow American College of Surgeons</option>
<option value="LICSW">LICSW - Licensed Independent Social Worker</option>
<option value="LMFT">LMFT - Licensed Marriage and Family Therapist</option>
<option value="LMP">LMP - Licensed Massage Practitioner</option>
<option value="PAC">PAC - Certified Physician Assistant</option>
					  </select>
</p></td></tr>
                        <%
            }
            else if ((ProfessionalEducation.isRead("Other",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>If Other, list here&nbsp;</b></p></td><td valign=top><p><%=ProfessionalEducation.getOther()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Other&amp;sTableName=tProfessionalEducation&amp;sRefID=<%=ProfessionalEducation.getProfessionalEducationID()%>&amp;sFieldNameDisp=<%=ProfessionalEducation.getEnglish("Other")%>&amp;sTableNameDisp=tProfessionalEducation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (ProfessionalEducation.isRequired("StartDate",UserSecurityGroupID))&&(!ProfessionalEducation.isComplete("StartDate")) )
            {
                theClass = "requiredField";
            }
            else if ((ProfessionalEducation.isExpired("StartDate",expiredDays))&&(ProfessionalEducation.isExpiredCheck("StartDate",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>

            <%
            if ((ProfessionalEducation.isWrite("StartDate",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top>
                    <p class=<%=theClass%> ><b>Start Date&nbsp;(mm/DD/yyyy):&nbsp;</b></p>
                  </td><td valign=top><p><input maxlength=20  type=text size="80" name="StartDate" value='<jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(ProfessionalEducation.getStartDate())%>" /></jsp:include>' >&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=StartDate&amp;sTableName=tProfessionalEducation&amp;sRefID=<%=ProfessionalEducation.getProfessionalEducationID()%>&amp;sFieldNameDisp=<%=ProfessionalEducation.getEnglish("StartDate")%>&amp;sTableNameDisp=tProfessionalEducation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((ProfessionalEducation.isRead("StartDate",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top>
                    <p class=<%=theClass%> ><b>Start Date&nbsp;(mm/DD/yyyy):&nbsp;</b></p>
                  </td><td valign=top><p><jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(ProfessionalEducation.getStartDate())%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=StartDate&amp;sTableName=tProfessionalEducation&amp;sRefID=<%=ProfessionalEducation.getProfessionalEducationID()%>&amp;sFieldNameDisp=<%=ProfessionalEducation.getEnglish("StartDate")%>&amp;sTableNameDisp=tProfessionalEducation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (ProfessionalEducation.isRequired("DateOfGraduation",UserSecurityGroupID))&&(!ProfessionalEducation.isComplete("DateOfGraduation")) )
            {
                theClass = "requiredField";
            }
            else if ((ProfessionalEducation.isExpired("DateOfGraduation",expiredDays))&&(ProfessionalEducation.isExpiredCheck("DateOfGraduation",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>

            <%
            if ((ProfessionalEducation.isWrite("DateOfGraduation",UserSecurityGroupID)))
            {
                        %>
<tr><td colspan=2>&nbsp;</td></tr>
<tr>
                  <td colspan=2 class=instructions>If you graduated from this 
                    program, please enter the graduation date. If you are still 
                    currently in this program, please enter "Current" otherwise, 
                    enter &quot;n/a&quot;</td>
                </tr>
                        <tr><td valign=top>
                    <p class=<%=theClass%> ><b>Graduation Date&nbsp;(mm/DD/yyyy):&nbsp;</b></p>
                  </td><td valign=top><p><input maxlength=20  type=text size="80" name="DateOfGraduation" value='<jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(ProfessionalEducation.getDateOfGraduation())%>" /></jsp:include>' >&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=DateOfGraduation&amp;sTableName=tProfessionalEducation&amp;sRefID=<%=ProfessionalEducation.getProfessionalEducationID()%>&amp;sFieldNameDisp=<%=ProfessionalEducation.getEnglish("DateOfGraduation")%>&amp;sTableNameDisp=tProfessionalEducation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((ProfessionalEducation.isRead("DateOfGraduation",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top>
                    <p class=<%=theClass%> ><b>Graduation Date&nbsp;(mm/DD/yyyy):&nbsp;</b></p>
                  </td><td valign=top><p><jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(ProfessionalEducation.getDateOfGraduation())%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=DateOfGraduation&amp;sTableName=tProfessionalEducation&amp;sRefID=<%=ProfessionalEducation.getProfessionalEducationID()%>&amp;sFieldNameDisp=<%=ProfessionalEducation.getEnglish("DateOfGraduation")%>&amp;sTableNameDisp=tProfessionalEducation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (ProfessionalEducation.isRequired("EndDate",UserSecurityGroupID))&&(!ProfessionalEducation.isComplete("EndDate")) )
            {
                theClass = "requiredField";
            }
            else if ((ProfessionalEducation.isExpired("EndDate",expiredDays))&&(ProfessionalEducation.isExpiredCheck("EndDate",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>

            <%
            if ((ProfessionalEducation.isWrite("EndDate",UserSecurityGroupID)))
            {
                        %>
<tr><td colspan=2>&nbsp;</td></tr>
<tr><td colspan=2 class=instructions>If you did not graduate from this program, please enter the date you ended the program, otherwise enter "n/a"</td></tr>
                        <tr><td valign=top>
                    <p class=<%=theClass%> ><b>End Date&nbsp;(mm/DD/yyyy):&nbsp;</b></p>
                  </td><td valign=top><p><input maxlength=20  type=text size="80" name="EndDate" value='<jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(ProfessionalEducation.getEndDate())%>" /></jsp:include>' >&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=EndDate&amp;sTableName=tProfessionalEducation&amp;sRefID=<%=ProfessionalEducation.getProfessionalEducationID()%>&amp;sFieldNameDisp=<%=ProfessionalEducation.getEnglish("EndDate")%>&amp;sTableNameDisp=tProfessionalEducation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((ProfessionalEducation.isRead("EndDate",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top>
                    <p class=<%=theClass%> ><b>End Date (if not graduated)&nbsp;(mm/DD/yyyy):&nbsp;</b></p>
                  </td><td valign=top><p><jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(ProfessionalEducation.getEndDate())%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=EndDate&amp;sTableName=tProfessionalEducation&amp;sRefID=<%=ProfessionalEducation.getProfessionalEducationID()%>&amp;sFieldNameDisp=<%=ProfessionalEducation.getEnglish("EndDate")%>&amp;sTableNameDisp=tProfessionalEducation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (ProfessionalEducation.isRequired("Focus",UserSecurityGroupID))&&(!ProfessionalEducation.isComplete("Focus")) )
            {
                theClass = "requiredField";
            }
            else if ((ProfessionalEducation.isExpired("Focus",expiredDays))&&(ProfessionalEducation.isExpiredCheck("Focus",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((ProfessionalEducation.isWrite("Focus",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Major/Focus&nbsp;</b></p></td><td valign=top><p><input maxlength="50" type=text size="80" name="Focus" value="<%=ProfessionalEducation.getFocus()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Focus&amp;sTableName=tProfessionalEducation&amp;sRefID=<%=ProfessionalEducation.getProfessionalEducationID()%>&amp;sFieldNameDisp=<%=ProfessionalEducation.getEnglish("Focus")%>&amp;sTableNameDisp=tProfessionalEducation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((ProfessionalEducation.isRead("Focus",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Major/Focus&nbsp;</b></p></td><td valign=top><p><%=ProfessionalEducation.getFocus()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Focus&amp;sTableName=tProfessionalEducation&amp;sRefID=<%=ProfessionalEducation.getProfessionalEducationID()%>&amp;sFieldNameDisp=<%=ProfessionalEducation.getEnglish("Focus")%>&amp;sTableNameDisp=tProfessionalEducation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (ProfessionalEducation.isRequired("SchoolName",UserSecurityGroupID))&&(!ProfessionalEducation.isComplete("SchoolName")) )
            {
                theClass = "requiredField";
            }
            else if ((ProfessionalEducation.isExpired("SchoolName",expiredDays))&&(ProfessionalEducation.isExpiredCheck("SchoolName",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((ProfessionalEducation.isWrite("SchoolName",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>School Name&nbsp;</b></p></td><td valign=top><p><input maxlength="100" type=text size="80" name="SchoolName" value="<%=ProfessionalEducation.getSchoolName()%>">&nbsp;<a href="#" onClick="window.open('LI_tSchoolLI_Search.jsp?OFieldName=SchoolName','BNPop','status=yes,scrollbars=yes,resizable=yes,width=400,height=300'); return false;"><img align=middle border=0 src=images/icon_lu.gif></a>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SchoolName&amp;sTableName=tProfessionalEducation&amp;sRefID=<%=ProfessionalEducation.getProfessionalEducationID()%>&amp;sFieldNameDisp=<%=ProfessionalEducation.getEnglish("SchoolName")%>&amp;sTableNameDisp=tProfessionalEducation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((ProfessionalEducation.isRead("SchoolName",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>School Name&nbsp;</b></p></td><td valign=top><p><%=ProfessionalEducation.getSchoolName()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SchoolName&amp;sTableName=tProfessionalEducation&amp;sRefID=<%=ProfessionalEducation.getProfessionalEducationID()%>&amp;sFieldNameDisp=<%=ProfessionalEducation.getEnglish("SchoolName")%>&amp;sTableNameDisp=tProfessionalEducation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (ProfessionalEducation.isRequired("SchoolAddress1",UserSecurityGroupID))&&(!ProfessionalEducation.isComplete("SchoolAddress1")) )
            {
                theClass = "requiredField";
            }
            else if ((ProfessionalEducation.isExpired("SchoolAddress1",expiredDays))&&(ProfessionalEducation.isExpiredCheck("SchoolAddress1",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((ProfessionalEducation.isWrite("SchoolAddress1",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Address&nbsp;</b></p></td><td valign=top><p><input maxlength="50" type=text size="80" name="SchoolAddress1" value="<%=ProfessionalEducation.getSchoolAddress1()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SchoolAddress1&amp;sTableName=tProfessionalEducation&amp;sRefID=<%=ProfessionalEducation.getProfessionalEducationID()%>&amp;sFieldNameDisp=<%=ProfessionalEducation.getEnglish("SchoolAddress1")%>&amp;sTableNameDisp=tProfessionalEducation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((ProfessionalEducation.isRead("SchoolAddress1",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Address&nbsp;</b></p></td><td valign=top><p><%=ProfessionalEducation.getSchoolAddress1()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SchoolAddress1&amp;sTableName=tProfessionalEducation&amp;sRefID=<%=ProfessionalEducation.getProfessionalEducationID()%>&amp;sFieldNameDisp=<%=ProfessionalEducation.getEnglish("SchoolAddress1")%>&amp;sTableNameDisp=tProfessionalEducation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (ProfessionalEducation.isRequired("SchoolAddress2",UserSecurityGroupID))&&(!ProfessionalEducation.isComplete("SchoolAddress2")) )
            {
                theClass = "requiredField";
            }
            else if ((ProfessionalEducation.isExpired("SchoolAddress2",expiredDays))&&(ProfessionalEducation.isExpiredCheck("SchoolAddress2",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((ProfessionalEducation.isWrite("SchoolAddress2",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Address 2&nbsp;</b></p></td><td valign=top><p><input maxlength="20" type=text size="80" name="SchoolAddress2" value="<%=ProfessionalEducation.getSchoolAddress2()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SchoolAddress2&amp;sTableName=tProfessionalEducation&amp;sRefID=<%=ProfessionalEducation.getProfessionalEducationID()%>&amp;sFieldNameDisp=<%=ProfessionalEducation.getEnglish("SchoolAddress2")%>&amp;sTableNameDisp=tProfessionalEducation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((ProfessionalEducation.isRead("SchoolAddress2",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Address 2&nbsp;</b></p></td><td valign=top><p><%=ProfessionalEducation.getSchoolAddress2()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SchoolAddress2&amp;sTableName=tProfessionalEducation&amp;sRefID=<%=ProfessionalEducation.getProfessionalEducationID()%>&amp;sFieldNameDisp=<%=ProfessionalEducation.getEnglish("SchoolAddress2")%>&amp;sTableNameDisp=tProfessionalEducation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (ProfessionalEducation.isRequired("SchoolCity",UserSecurityGroupID))&&(!ProfessionalEducation.isComplete("SchoolCity")) )
            {
                theClass = "requiredField";
            }
            else if ((ProfessionalEducation.isExpired("SchoolCity",expiredDays))&&(ProfessionalEducation.isExpiredCheck("SchoolCity",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((ProfessionalEducation.isWrite("SchoolCity",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>City&nbsp;</b></p></td><td valign=top><p><input maxlength="30" type=text size="80" name="SchoolCity" value="<%=ProfessionalEducation.getSchoolCity()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SchoolCity&amp;sTableName=tProfessionalEducation&amp;sRefID=<%=ProfessionalEducation.getProfessionalEducationID()%>&amp;sFieldNameDisp=<%=ProfessionalEducation.getEnglish("SchoolCity")%>&amp;sTableNameDisp=tProfessionalEducation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((ProfessionalEducation.isRead("SchoolCity",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>City&nbsp;</b></p></td><td valign=top><p><%=ProfessionalEducation.getSchoolCity()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SchoolCity&amp;sTableName=tProfessionalEducation&amp;sRefID=<%=ProfessionalEducation.getProfessionalEducationID()%>&amp;sFieldNameDisp=<%=ProfessionalEducation.getEnglish("SchoolCity")%>&amp;sTableNameDisp=tProfessionalEducation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (ProfessionalEducation.isRequired("SchoolStateID",UserSecurityGroupID))&&(!ProfessionalEducation.isComplete("SchoolStateID")) )
            {
                theClass = "requiredField";
            }
            else if ((ProfessionalEducation.isExpired("SchoolStateID",expiredDays))&&(ProfessionalEducation.isExpiredCheck("SchoolStateID",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((ProfessionalEducation.isWrite("SchoolStateID",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>State&nbsp;</b></p></td><td valign=top><p><select   name="SchoolStateID" ><jsp:include page="../generic/tStateLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=ProfessionalEducation.getSchoolStateID()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SchoolStateID&amp;sTableName=tProfessionalEducation&amp;sRefID=<%=ProfessionalEducation.getProfessionalEducationID()%>&amp;sFieldNameDisp=<%=ProfessionalEducation.getEnglish("SchoolStateID")%>&amp;sTableNameDisp=tProfessionalEducation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((ProfessionalEducation.isRead("SchoolStateID",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>State&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tStateLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=ProfessionalEducation.getSchoolStateID()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SchoolStateID&amp;sTableName=tProfessionalEducation&amp;sRefID=<%=ProfessionalEducation.getProfessionalEducationID()%>&amp;sFieldNameDisp=<%=ProfessionalEducation.getEnglish("SchoolStateID")%>&amp;sTableNameDisp=tProfessionalEducation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (ProfessionalEducation.isRequired("SchoolProvince",UserSecurityGroupID))&&(!ProfessionalEducation.isComplete("SchoolProvince")) )
            {
                theClass = "requiredField";
            }
            else if ((ProfessionalEducation.isExpired("SchoolProvince",expiredDays))&&(ProfessionalEducation.isExpiredCheck("SchoolProvince",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((ProfessionalEducation.isWrite("SchoolProvince",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Province, District, State&nbsp;</b></p></td><td valign=top><p><input maxlength="100" type=text size="80" name="SchoolProvince" value="<%=ProfessionalEducation.getSchoolProvince()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SchoolProvince&amp;sTableName=tProfessionalEducation&amp;sRefID=<%=ProfessionalEducation.getProfessionalEducationID()%>&amp;sFieldNameDisp=<%=ProfessionalEducation.getEnglish("SchoolProvince")%>&amp;sTableNameDisp=tProfessionalEducation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((ProfessionalEducation.isRead("SchoolProvince",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Province, District, State&nbsp;</b></p></td><td valign=top><p><%=ProfessionalEducation.getSchoolProvince()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SchoolProvince&amp;sTableName=tProfessionalEducation&amp;sRefID=<%=ProfessionalEducation.getProfessionalEducationID()%>&amp;sFieldNameDisp=<%=ProfessionalEducation.getEnglish("SchoolProvince")%>&amp;sTableNameDisp=tProfessionalEducation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (ProfessionalEducation.isRequired("SchoolZIP",UserSecurityGroupID))&&(!ProfessionalEducation.isComplete("SchoolZIP")) )
            {
                theClass = "requiredField";
            }
            else if ((ProfessionalEducation.isExpired("SchoolZIP",expiredDays))&&(ProfessionalEducation.isExpiredCheck("SchoolZIP",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((ProfessionalEducation.isWrite("SchoolZIP",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>ZIP&nbsp;</b></p></td><td valign=top><p><input maxlength="50" type=text size="80" name="SchoolZIP" value="<%=ProfessionalEducation.getSchoolZIP()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SchoolZIP&amp;sTableName=tProfessionalEducation&amp;sRefID=<%=ProfessionalEducation.getProfessionalEducationID()%>&amp;sFieldNameDisp=<%=ProfessionalEducation.getEnglish("SchoolZIP")%>&amp;sTableNameDisp=tProfessionalEducation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((ProfessionalEducation.isRead("SchoolZIP",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>ZIP&nbsp;</b></p></td><td valign=top><p><%=ProfessionalEducation.getSchoolZIP()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SchoolZIP&amp;sTableName=tProfessionalEducation&amp;sRefID=<%=ProfessionalEducation.getProfessionalEducationID()%>&amp;sFieldNameDisp=<%=ProfessionalEducation.getEnglish("SchoolZIP")%>&amp;sTableNameDisp=tProfessionalEducation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (ProfessionalEducation.isRequired("SchoolCountryID",UserSecurityGroupID))&&(!ProfessionalEducation.isComplete("SchoolCountryID")) )
            {
                theClass = "requiredField";
            }
            else if ((ProfessionalEducation.isExpired("SchoolCountryID",expiredDays))&&(ProfessionalEducation.isExpiredCheck("SchoolCountryID",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((ProfessionalEducation.isWrite("SchoolCountryID",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Country&nbsp;</b></p></td><td valign=top><p><select   name="SchoolCountryID" ><jsp:include page="../generic/tCountryLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=ProfessionalEducation.getSchoolCountryID()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SchoolCountryID&amp;sTableName=tProfessionalEducation&amp;sRefID=<%=ProfessionalEducation.getProfessionalEducationID()%>&amp;sFieldNameDisp=<%=ProfessionalEducation.getEnglish("SchoolCountryID")%>&amp;sTableNameDisp=tProfessionalEducation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((ProfessionalEducation.isRead("SchoolCountryID",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Country&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tCountryLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=ProfessionalEducation.getSchoolCountryID()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SchoolCountryID&amp;sTableName=tProfessionalEducation&amp;sRefID=<%=ProfessionalEducation.getProfessionalEducationID()%>&amp;sFieldNameDisp=<%=ProfessionalEducation.getEnglish("SchoolCountryID")%>&amp;sTableNameDisp=tProfessionalEducation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (ProfessionalEducation.isRequired("SchoolPhone",UserSecurityGroupID))&&(!ProfessionalEducation.isComplete("SchoolPhone")) )
            {
                theClass = "requiredField";
            }
            else if ((ProfessionalEducation.isExpired("SchoolPhone",expiredDays))&&(ProfessionalEducation.isExpiredCheck("SchoolPhone",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((ProfessionalEducation.isWrite("SchoolPhone",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>School Phone (XXX-XXX-XXXX)&nbsp;</b></p></td><td valign=top><p><input maxlength="50" type=text size="80" name="SchoolPhone" value="<%=ProfessionalEducation.getSchoolPhone()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SchoolPhone&amp;sTableName=tProfessionalEducation&amp;sRefID=<%=ProfessionalEducation.getProfessionalEducationID()%>&amp;sFieldNameDisp=<%=ProfessionalEducation.getEnglish("SchoolPhone")%>&amp;sTableNameDisp=tProfessionalEducation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((ProfessionalEducation.isRead("SchoolPhone",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>School Phone (XXX-XXX-XXXX)&nbsp;</b></p></td><td valign=top><p><%=ProfessionalEducation.getSchoolPhone()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SchoolPhone&amp;sTableName=tProfessionalEducation&amp;sRefID=<%=ProfessionalEducation.getProfessionalEducationID()%>&amp;sFieldNameDisp=<%=ProfessionalEducation.getEnglish("SchoolPhone")%>&amp;sTableNameDisp=tProfessionalEducation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (ProfessionalEducation.isRequired("SchoolFax",UserSecurityGroupID))&&(!ProfessionalEducation.isComplete("SchoolFax")) )
            {
                theClass = "requiredField";
            }
            else if ((ProfessionalEducation.isExpired("SchoolFax",expiredDays))&&(ProfessionalEducation.isExpiredCheck("SchoolFax",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((ProfessionalEducation.isWrite("SchoolFax",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Fax (XXX-XXX-XXXX)&nbsp;</b></p></td><td valign=top><p><input maxlength="50" type=text size="80" name="SchoolFax" value="<%=ProfessionalEducation.getSchoolFax()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SchoolFax&amp;sTableName=tProfessionalEducation&amp;sRefID=<%=ProfessionalEducation.getProfessionalEducationID()%>&amp;sFieldNameDisp=<%=ProfessionalEducation.getEnglish("SchoolFax")%>&amp;sTableNameDisp=tProfessionalEducation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((ProfessionalEducation.isRead("SchoolFax",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Fax (XXX-XXX-XXXX)&nbsp;</b></p></td><td valign=top><p><%=ProfessionalEducation.getSchoolFax()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SchoolFax&amp;sTableName=tProfessionalEducation&amp;sRefID=<%=ProfessionalEducation.getProfessionalEducationID()%>&amp;sFieldNameDisp=<%=ProfessionalEducation.getEnglish("SchoolFax")%>&amp;sTableNameDisp=tProfessionalEducation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (ProfessionalEducation.isRequired("ContactName",UserSecurityGroupID))&&(!ProfessionalEducation.isComplete("ContactName")) )
            {
                theClass = "requiredField";
            }
            else if ((ProfessionalEducation.isExpired("ContactName",expiredDays))&&(ProfessionalEducation.isExpiredCheck("ContactName",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((ProfessionalEducation.isWrite("ContactName",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Contact Name&nbsp;</b></p></td><td valign=top><p><input maxlength="90" type=text size="80" name="ContactName" value="<%=ProfessionalEducation.getContactName()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ContactName&amp;sTableName=tProfessionalEducation&amp;sRefID=<%=ProfessionalEducation.getProfessionalEducationID()%>&amp;sFieldNameDisp=<%=ProfessionalEducation.getEnglish("ContactName")%>&amp;sTableNameDisp=tProfessionalEducation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((ProfessionalEducation.isRead("ContactName",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Contact Name&nbsp;</b></p></td><td valign=top><p><%=ProfessionalEducation.getContactName()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ContactName&amp;sTableName=tProfessionalEducation&amp;sRefID=<%=ProfessionalEducation.getProfessionalEducationID()%>&amp;sFieldNameDisp=<%=ProfessionalEducation.getEnglish("ContactName")%>&amp;sTableNameDisp=tProfessionalEducation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (ProfessionalEducation.isRequired("ContactEmail",UserSecurityGroupID))&&(!ProfessionalEducation.isComplete("ContactEmail")) )
            {
                theClass = "requiredField";
            }
            else if ((ProfessionalEducation.isExpired("ContactEmail",expiredDays))&&(ProfessionalEducation.isExpiredCheck("ContactEmail",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((ProfessionalEducation.isWrite("ContactEmail",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Contact E-mail&nbsp;</b></p></td><td valign=top><p><input maxlength="75" type=text size="80" name="ContactEmail" value="<%=ProfessionalEducation.getContactEmail()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ContactEmail&amp;sTableName=tProfessionalEducation&amp;sRefID=<%=ProfessionalEducation.getProfessionalEducationID()%>&amp;sFieldNameDisp=<%=ProfessionalEducation.getEnglish("ContactEmail")%>&amp;sTableNameDisp=tProfessionalEducation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((ProfessionalEducation.isRead("ContactEmail",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Contact E-mail&nbsp;</b></p></td><td valign=top><p><%=ProfessionalEducation.getContactEmail()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ContactEmail&amp;sTableName=tProfessionalEducation&amp;sRefID=<%=ProfessionalEducation.getProfessionalEducationID()%>&amp;sFieldNameDisp=<%=ProfessionalEducation.getEnglish("ContactEmail")%>&amp;sTableNameDisp=tProfessionalEducation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (ProfessionalEducation.isRequired("Comments",UserSecurityGroupID))&&(!ProfessionalEducation.isComplete("Comments")) )
            {
                theClass = "requiredField";
            }
            else if ((ProfessionalEducation.isExpired("Comments",expiredDays))&&(ProfessionalEducation.isExpiredCheck("Comments",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((ProfessionalEducation.isWrite("Comments",UserSecurityGroupID)))
            {
                        %>
                       <tr><td valign=top><p class=<%=theClass%> ><b><%=ProfessionalEducation.getEnglish("Comments")%>&nbsp;</b></p></td><td valign=top><p><textarea onKeyDown="textAreaStop(this,200)" rows="2" name="Comments" cols="40" maxlength=200><%=ProfessionalEducation.getComments()%></textarea>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Comments&amp;sTableName=tProfessionalEducation&amp;sRefID=<%=ProfessionalEducation.getProfessionalEducationID()%>&amp;sFieldNameDisp=<%=ProfessionalEducation.getEnglish("Comments")%>&amp;sTableNameDisp=tProfessionalEducation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((ProfessionalEducation.isRead("Comments",UserSecurityGroupID)))
            {
                        %>
                       <tr><td valign=top> <p class=<%=theClass%> ><b><%=ProfessionalEducation.getEnglish("Comments")%>&nbsp;</b></p></td><td valign=top><p><%=ProfessionalEducation.getComments()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Comments&amp;sTableName=tProfessionalEducation&amp;sRefID=<%=ProfessionalEducation.getProfessionalEducationID()%>&amp;sFieldNameDisp=<%=ProfessionalEducation.getEnglish("Comments")%>&amp;sTableNameDisp=tProfessionalEducation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>




            <tr><td width=40%>&nbsp;</td><td width=60%>&nbsp;</td></tr>
            </table>
        <%if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {%>
            <input type=hidden name=routePageReference value="sParentReturnPage">
             <%
              if (request.getParameter("INTNext")!=null&&request.getParameter("INTNext").equalsIgnoreCase("yes") ) 
              {
              %>
                  <table width=75% border=1 bordercolor=333333 align=left cellspacing=0 cellpadding=0 class=wizardTable>
                  <tr class=requiredField><td>
                  <input  <%=HTMLFormStyleButton%> type="radio" value="next" name="INTNext" checked>&nbsp;&nbsp;<%=ConfigurationMessages.getInterviewMessage("INTERVIEWNoMore","tProfessionalEducation")%>
                  <br>
                  <input  <%=HTMLFormStyleButton%> type="radio" value="yes" name="INTNext">&nbsp;&nbsp;<%=ConfigurationMessages.getInterviewMessage("INTERVIEWAddMore","tProfessionalEducation")%>
                  </td></tr></table><br><br><br>
              <%
              }
              %>
            <p><input <%=HTMLFormStyleButton%> type=Submit value="Continue" name=Submit></p>
        <%}%>
        </td></tr></table>
        </form>
        <%
  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>

    </td></tr></table>



<%String bnIncludeFN = "ui_"+thePLCID+"\\bot-nav_PhysicianID.jsp";
%>
<jsp:include page="<%=bnIncludeFN%>" flush="true" ><jsp:param name="plcID" value="<%=thePLCID%>"/></jsp:include>
