<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.bltDocumentManagement,java.util.Vector,java.util.Enumeration,com.winstaff.ListElement, com.winstaff.PLCUtils, com.winstaff.SecurityCheck, com.winstaff.ConfigurationMessages,com.winstaff.bltProfessionalLiability,com.winstaff.DocumentManagerUtils" %>
<%/*
    filename: out\jsp\tProfessionalLiability_form_sub.jsp
    Created on Sep/11/2003
    Created by: Scott Ellis
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>


<%

//initial declaration of list class and parentID
    Integer        iProfessionalLiabilityID        =    null;
    boolean accessValid = false;

   Integer iSecurityCheck = SecurityCheck.CheckItem("PractitionerSection9", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {

    if (pageControllerHash.containsKey("iProfessionalLiabilityID")) 
    {
        iProfessionalLiabilityID        =    (Integer)pageControllerHash.get("iProfessionalLiabilityID");
        if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {
            accessValid = true;
        }
    }
  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);

//initial declaration of list class and parentID

    bltProfessionalLiability        ProfessionalLiability        =    null;

    if ( request.getParameter( "EDIT" ).equalsIgnoreCase("edit") )
    {
        ProfessionalLiability        =    new    bltProfessionalLiability(iProfessionalLiabilityID,UserSecurityGroupID);
    }
    else if ( request.getParameter( "EDIT" ).equalsIgnoreCase("new") )
    {
        ProfessionalLiability        =    new    bltProfessionalLiability(UserSecurityGroupID,true);
    }

String testChangeID = "0";

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("UniqueCreateDate"))) ;
    if ( !ProfessionalLiability.getUniqueCreateDate().equals(testObj)  )
    {
         ProfessionalLiability.setUniqueCreateDate( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("ProfessionalLiability UniqueCreateDate not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("UniqueModifyDate"))) ;
    if ( !ProfessionalLiability.getUniqueModifyDate().equals(testObj)  )
    {
         ProfessionalLiability.setUniqueModifyDate( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("ProfessionalLiability UniqueModifyDate not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("UniqueModifyComments")) ;
    if ( !ProfessionalLiability.getUniqueModifyComments().equals(testObj)  )
    {
         ProfessionalLiability.setUniqueModifyComments( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("ProfessionalLiability UniqueModifyComments not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("PhysicianID")) ;
    if ( !ProfessionalLiability.getPhysicianID().equals(testObj)  )
    {
         ProfessionalLiability.setPhysicianID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("ProfessionalLiability PhysicianID not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("CoverageType")) ;
    if ( !ProfessionalLiability.getCoverageType().equals(testObj)  )
    {
         ProfessionalLiability.setCoverageType( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("ProfessionalLiability CoverageType not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("InsuranceCarrier")) ;
    if ( !ProfessionalLiability.getInsuranceCarrier().equals(testObj)  )
    {
         ProfessionalLiability.setInsuranceCarrier( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("ProfessionalLiability InsuranceCarrier not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("PolicyHolder")) ;
    if ( !ProfessionalLiability.getPolicyHolder().equals(testObj)  )
    {
         ProfessionalLiability.setPolicyHolder( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("ProfessionalLiability PolicyHolder not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("AgentName")) ;
    if ( !ProfessionalLiability.getAgentName().equals(testObj)  )
    {
         ProfessionalLiability.setAgentName( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("ProfessionalLiability AgentName not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("Address1")) ;
    if ( !ProfessionalLiability.getAddress1().equals(testObj)  )
    {
         ProfessionalLiability.setAddress1( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("ProfessionalLiability Address1 not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("Address2")) ;
    if ( !ProfessionalLiability.getAddress2().equals(testObj)  )
    {
         ProfessionalLiability.setAddress2( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("ProfessionalLiability Address2 not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("City")) ;
    if ( !ProfessionalLiability.getCity().equals(testObj)  )
    {
         ProfessionalLiability.setCity( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("ProfessionalLiability City not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("StateID")) ;
    if ( !ProfessionalLiability.getStateID().equals(testObj)  )
    {
         ProfessionalLiability.setStateID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("ProfessionalLiability StateID not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("Province")) ;
    if ( !ProfessionalLiability.getProvince().equals(testObj)  )
    {
         ProfessionalLiability.setProvince( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("ProfessionalLiability Province not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("ZIP")) ;
    if ( !ProfessionalLiability.getZIP().equals(testObj)  )
    {
         ProfessionalLiability.setZIP( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("ProfessionalLiability ZIP not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("CountryID")) ;
    if ( !ProfessionalLiability.getCountryID().equals(testObj)  )
    {
         ProfessionalLiability.setCountryID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("ProfessionalLiability CountryID not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("Phone")) ;
    if ( !ProfessionalLiability.getPhone().equals(testObj)  )
    {
         ProfessionalLiability.setPhone( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("ProfessionalLiability Phone not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("Fax")) ;
    if ( !ProfessionalLiability.getFax().equals(testObj)  )
    {
         ProfessionalLiability.setFax( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("ProfessionalLiability Fax not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("ContactName")) ;
    if ( !ProfessionalLiability.getContactName().equals(testObj)  )
    {
         ProfessionalLiability.setContactName( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("ProfessionalLiability ContactName not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("ContactEmail")) ;
    if ( !ProfessionalLiability.getContactEmail().equals(testObj)  )
    {
         ProfessionalLiability.setContactEmail( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("ProfessionalLiability ContactEmail not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("PolicyNumber")) ;
    if ( !ProfessionalLiability.getPolicyNumber().equals(testObj)  )
    {
         ProfessionalLiability.setPolicyNumber( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("ProfessionalLiability PolicyNumber not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("InsuredFromDate"))) ;
    if ( !ProfessionalLiability.getInsuredFromDate().equals(testObj)  )
    {
         ProfessionalLiability.setInsuredFromDate( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("ProfessionalLiability InsuredFromDate not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("InsuredToDate"))) ;
    if ( !ProfessionalLiability.getInsuredToDate().equals(testObj)  )
    {
         ProfessionalLiability.setInsuredToDate( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("ProfessionalLiability InsuredToDate not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("OriginalEffectiveDate"))) ;
    if ( !ProfessionalLiability.getOriginalEffectiveDate().equals(testObj)  )
    {
         ProfessionalLiability.setOriginalEffectiveDate( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("ProfessionalLiability OriginalEffectiveDate not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("TerminationDate"))) ;
    if ( !ProfessionalLiability.getTerminationDate().equals(testObj)  )
    {
         ProfessionalLiability.setTerminationDate( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("ProfessionalLiability TerminationDate not set. this is ok-not an error");
}

try
{
    Double testObj = null ;
    testObj = (PLCUtils.getDecimalDefault(request.getParameter("PerClaimAmount"))) ;
    if ( !ProfessionalLiability.getPerClaimAmount().equals(testObj)  )
    {
         ProfessionalLiability.setPerClaimAmount( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("ProfessionalLiability PerClaimAmount not set. this is ok-not an error");
}

try
{
    Double testObj = null ;
    testObj = (PLCUtils.getDecimalDefault(request.getParameter("AggregateAmount"))) ;
    if ( !ProfessionalLiability.getAggregateAmount().equals(testObj)  )
    {
         ProfessionalLiability.setAggregateAmount( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("ProfessionalLiability AggregateAmount not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("DescOfSurcharge")) ;
    if ( !ProfessionalLiability.getDescOfSurcharge().equals(testObj)  )
    {
         ProfessionalLiability.setDescOfSurcharge( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("ProfessionalLiability DescOfSurcharge not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("DocuLinkID")) ;
    if ( !ProfessionalLiability.getDocuLinkID().equals(testObj)  )
    {
         ProfessionalLiability.setDocuLinkID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("ProfessionalLiability DocuLinkID not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("Comments")) ;
    if ( !ProfessionalLiability.getComments().equals(testObj)  )
    {
         ProfessionalLiability.setComments( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("ProfessionalLiability Comments not set. this is ok-not an error");
}
boolean bINT = false;
boolean bNextStep = false;
String sRefreshDoc = "refreshMe=true&";
              if (request.getParameter("INTNext")!=null&&request.getParameter("INTNext").equalsIgnoreCase("yes") ) 
	            {
	                bINT = true;
	                sRefreshDoc = "";
	            }
              else if (request.getParameter("INTNext")!=null&&request.getParameter("INTNext").equalsIgnoreCase("next") ) 
	            {
	                bINT = true;
	                sINTNext = ConfigurationMessages.getInterviewLinkRaw("tProfessionalLiability","next");
	                sRefreshDoc = "";
	            }

// If an edit, update information; if new, append information
boolean errorRouteMe = false;
if (testChangeID.equalsIgnoreCase("1"))
{

	ProfessionalLiability.setUniqueModifyDate(new java.util.Date());
	String UserLogonDescription = "n/a";
	if (pageControllerHash.containsKey("UserLogonDescription"))
	{
	    UserLogonDescription = (String)pageControllerHash.get("UserLogonDescription");
	}
    //if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
    {
	    ProfessionalLiability.setUniqueModifyComments(UserLogonDescription);
	    try
	    {
	        ProfessionalLiability.commitData();
	    }
	    catch (Exception e55)
	    {
	        errorRouteMe = true;
	    }	//Doculink Addition
	//Doculink Does Exist, prompt to Modify
	Integer iPhysicianID = ProfessionalLiability.getPhysicianID();
     if (!errorRouteMe&&ProfessionalLiability.getDocuLinkID().intValue()>0)
     {
            bltDocumentManagement myDoc = new bltDocumentManagement(ProfessionalLiability.getDocuLinkID());
            if (myDoc.getDocumentFileName().equalsIgnoreCase("")) 
            {
                DocumentManagerUtils myDUtils = new DocumentManagerUtils();
                String myDType = "tProfessionalLiability";
                myDUtils.setDocumentType(myDType, ProfessionalLiability.getUniqueID());
                myDoc.setDocumentName(myDUtils.getDocumentName());
                myDoc.setDateOfExpiration(myDUtils.getDateOfExpiration());
                myDoc.setDocumentTypeID(myDUtils.getDocumentTypeID());
                myDoc.setPhysicianID(iPhysicianID);
                myDoc.setArchived(new Integer(2));
                myDoc.commitData();
            }
            else
            {
                //archive then create new
                myDoc.setArchived(new Integer("1"));
                myDoc.commitData();
                myDoc = new bltDocumentManagement();
                DocumentManagerUtils myDUtils = new DocumentManagerUtils();
                String myDType = "tProfessionalLiability";
                myDUtils.setDocumentType(myDType, ProfessionalLiability.getUniqueID());
                myDoc.setDocumentName(myDUtils.getDocumentName());
                myDoc.setDateOfExpiration(myDUtils.getDateOfExpiration());
                myDoc.setDocumentTypeID(myDUtils.getDocumentTypeID());
                myDoc.setPhysicianID(iPhysicianID);
                myDoc.setArchived(new Integer(2));
                myDoc.commitData();
                ProfessionalLiability.setDocuLinkID(myDoc.getUniqueID());
                ProfessionalLiability.commitData();
            }
     }
     else if (!errorRouteMe)
     {
            bltDocumentManagement myDoc = new bltDocumentManagement();
            DocumentManagerUtils myDUtils = new DocumentManagerUtils();
            String myDType = "tProfessionalLiability";
            myDUtils.setDocumentType(myDType, ProfessionalLiability.getUniqueID());
            myDoc.setDocumentName(myDUtils.getDocumentName());
            myDoc.setDateOfExpiration(myDUtils.getDateOfExpiration());
            myDoc.setDocumentTypeID(myDUtils.getDocumentTypeID());
                myDoc.setPhysicianID(iPhysicianID);
            myDoc.setArchived(new Integer(2));
            myDoc.commitData();
            ProfessionalLiability.setDocuLinkID(myDoc.getUniqueID());
            ProfessionalLiability.commitData();
     }

    }
}
String routePageReference = request.getParameter("routePageReference");
String nextPage=null;
if (routePageReference!=null)
{
        if (pageControllerHash.containsKey(routePageReference))
        {
            nextPage = (String)pageControllerHash.get(routePageReference);
        }
        else if (pageControllerHash.containsKey("sLocalChildReturnPage"))
        {
            nextPage = (String)pageControllerHash.get("sLocalChildReturnPage");
        }
        else if (pageControllerHash.containsKey("sParentReturnPage"))
        {
            nextPage = (String)pageControllerHash.get("sParentReturnPage");
        }
}
              if (bINT ) 
	            {
	                nextPage = sINTNext;
	                if (nextPage==null||nextPage.equalsIgnoreCase("")||nextPage.equalsIgnoreCase("#"))
	                {
	                    nextPage = (String)pageControllerHash.get("sLocalChildReturnPage");
	                }
	            }
if (errorRouteMe)
{
	out.println(ConfigurationMessages.getMessage("FatalError")); 
}
else if (nextPage!=null)
{
	    %><script language=Javascript>
	      document.location="<%=nextPage%>";
	      </script><%
    //response.sendRedirect(nextPage+"?EDIT=edit");
}
        %>


  <%
  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
  %>
