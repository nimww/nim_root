<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.DataControlUtils,com.winstaff.DebugLogger,com.winstaff.SecurityCheck, com.winstaff.bltAdminPhysicianLU , com.winstaff.ConfigurationMessages, com.winstaff.bltPLCLI,com.winstaff.bltPhysicianMaster,com.winstaff.searchDB2, com.winstaff.bltStateLI,java.util.Vector,java.util.Enumeration,com.winstaff.ListElement, com.winstaff.PLCUtils,com.winstaff.bltAdminPhysicianLU,com.winstaff.bltAdminPhysicianLU_List_LU_AdminID" %>
<%/*
    filename: tAdminPhysicianLU_main_LU_AdminID.jsp
    Created on Apr/23/2002
    Type: 1-n main class file
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>

<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_AdminID.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>
<table cellpadding=0 cellspacing=0 border=0 width=700>
  <tr>
    <td width=10>&nbsp;</td>
    <td> <span class=title>Find & Add a Practitioner</span> 
      <%
//initial declaration of list class and parentID
    Integer        iAdminID        =    null;
    boolean accessValid = false;
   Integer iSecurityCheck = SecurityCheck.CheckItem("admin1", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {
    if (pageControllerHash.containsKey("iAdminID")) 
    {
        iAdminID        =    (Integer)pageControllerHash.get("iAdminID");
        accessValid = true;
    }
  //page security
  if (accessValid)
  {
    Integer iMasterAdminID = (new bltPLCLI(new Integer(thePLCID))).getAdminID();
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);
      pageControllerHash.put("sParentReturnPage","AdminPhysician_query.jsp?PhysicianID=&LastName=&FirstName=&SSN=&HomeCity=&HomeStateID=0&HomeZIP=&orderBy=PhysicianID&startID=0&maxResults=50&Submit2=Submit");
      pageControllerHash.remove("iLookupID");
      pageControllerHash.remove("iPhysicianID");
      session.setAttribute("pageControllerHash",pageControllerHash);


%>
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td> 
            <%
try
{

String myLastName = "";
String mySSN = "";
String myDOB = "";
String myMonth = "";
String myDay = "";
String myYear = "";
boolean firstDisplay = true;

try
{
myLastName = DataControlUtils.fixApostrophe(request.getParameter("LastName"));
mySSN = request.getParameter("SSN");
myMonth = request.getParameter("MONTH");
myDay = request.getParameter("DAY");
myYear = request.getParameter("YEAR");
firstDisplay = false;
if (myLastName==null||myLastName.equalsIgnoreCase("")||myLastName.equalsIgnoreCase("null"))
{
	firstDisplay = true;
}

}
catch (Exception e)
{
firstDisplay = true;
myLastName = "";
mySSN = "";
myDOB = "";
}

if (myYear.length()==1)
{
	myYear = "190" + myYear;
}
if (myYear.length()==2)
{
	myYear = "19" + myYear;
}
if (myYear.length()==3)
{
	myYear = "1" + myYear;
}
else
{
	try
	{
		if ( (new Integer(myYear)).intValue()<1850)
		{
			myDOB = "";
		}
		else
		{
			myDOB = PLCUtils.getDisplayDateSDF1().format(PLCUtils.getDBDF().parse(myYear+"-"+myMonth+"-"+myDay));
	    }
	}
	catch(Exception eeee)
	{
		myDOB = "";
	}
}	

String mySSNs = PLCUtils.StripDashes(mySSN);
String mySSNd = "";
try
{
	 mySSNd = mySSNs.substring(0,3) + "-" + mySSNs.substring(3,5) + "-" +mySSNs.substring(5,mySSNs.length());
}
catch (Exception eeee)
{
System.out.println("Invalid Social Security Number. Please Try Again");
}


if (!firstDisplay)
{



searchDB2 mySS = new searchDB2();

java.sql.ResultSet myRS = null;;

try
{

//String mySQL = "select tPhysicianMaster.PhysicianID from tPhysicianMaster,tAdminPhysicianLU where  (tAdminPhysicianLU.adminID="+iMasterAdminID+") and (tAdminPhysicianLU.physicianID=tPhysicianMaster.physicianID) and (tPhysicianMaster.physicianID>0) and LastName='"+myLastName+"' and SSN='"+mySSN+"' and DateOfBirth='"+myDOB+" 12:00:00PM'";
String mySQL = "select tPhysicianMaster.PhysicianID from tPhysicianMaster where (tPhysicianMaster.physicianID>0) and LastName='"+myLastName+"' and ( (SSN='"+mySSNd+"') or (SSN='"+mySSNs+"') )and ( (tPhysicianMaster.DateOfBirth='"+myDOB+" 12:00:00 PM' ) OR (tPhysicianMaster.DateOfBirth='"+myDOB+"' ) )";

myRS = mySS.executeStatement(mySQL);


}
catch(Exception e)
{
DebugLogger.println("ResultsSet:"+e);
}

try{


bltPhysicianMaster pm = null;
Integer myDoctorID = null;
int cnt=0;
try{
   while (myRS!=null&&myRS.next())
   {
cnt++;
myDoctorID = new Integer(myRS.getString("PhysicianID"));
pm = new bltPhysicianMaster(myDoctorID);

   }


}
catch(Exception e)
{
DebugLogger.println("ResultsSet:"+e);
}

    mySS.closeAll();

if (cnt==1)
{
//START-verify that the doctor is not already added:
    bltAdminPhysicianLU_List_LU_AdminID        bltAdminPhysicianLU_List_LU_AdminID        =    new    bltAdminPhysicianLU_List_LU_AdminID(iAdminID,"PhysicianID='"+myDoctorID+"'","");
    java.util.Enumeration eList = bltAdminPhysicianLU_List_LU_AdminID.elements();
    if (!eList.hasMoreElements())
    {

 myLastName = "";
 mySSN = "";
 myDOB = "";
 myMonth = "";
 myDay = "";
 myYear = "";


%>
                    <br>
                  <p class=instructions>Practitioner, <b><%=pm.getFirstName() +" "+ pm.getLastName()%></b>, 
                    has <b>now</b> been added to your admin group. 
                  <table cellpadding=2>
                    <tr> 
                      <td colspan=2 class=tdHeaderAlt>Your Options:</td>
                    </tr>
                    <tr> 
                      <td class=tdHeaderAlt>1)</td>
                      <td class=tdBase> <a href="tAdminPhysicianLU_main_PhysicianMaster_AdminID_form_authorize.jsp?EDIT=edit&EDITID=<%=myDoctorID%>">Open/Edit</a> 
                        <%=pm.getFirstName() +" "+ pm.getLastName()%> practitioner record</td>
                    </tr>
                    <tr> 
                      <td class=tdHeaderAlt>2)</td>
                      <td class=tdBase> <a href="tAdminPhysician_MasterAdd.jsp">Click 
                        here</a> to find and add another practitioner. </td>
                    </tr>
                    <tr> 
                      <td class=tdHeaderAlt rowspan="2" valign="top">3)</td>
                      <td class=tdBase>If you are not able to find your practitioner and would like to create a
new practitioner record, please <a class=linkBase href = "tAdminPhysicianLU_main_PhysicianMaster_AdminID_form_create.jsp?EDIT=new">click here</a>.</td>
                    </tr>
                  </table>
                  <%
        bltAdminPhysicianLU myLU = new bltAdminPhysicianLU();
        myLU.setAdminID(iAdminID);
	myLU.setPhysicianID(myDoctorID);
	myLU.setComments("find&add");
	myLU.commitData(); 
    }
    else
    {
%>
                  <br>
                  <p class=instructions>Practitioner, <b><%=pm.getFirstName() +" "+ pm.getLastName()%></b>, 
                    has <b>already</b> been added to your admin group. 
                  <table cellpadding=2>
                    <tr> 
                      <td colspan=2 class=tdHeaderAlt>Your Options:</td>
                    </tr>
                    <tr> 
                      <td class=tdHeaderAlt>1)</td>
                      <td class=tdBase> <a href="tAdminPhysician_MasterAdd.jsp">Click 
                        here</a> to find and add a different practitioner. </td>
                    </tr>
                    <tr> 
                      <td class=tdHeaderAlt>2)</td>
                      <td class=tdBase> <a href="tAdminPhysicianLU_main_PhysicianMaster_AdminID_form_authorize.jsp?EDIT=edit&EDITID=<%=myDoctorID%>">Open/Edit</a> 
                        <%=pm.getFirstName() +" "+ pm.getLastName()%> practitioner record</td>
                    </tr>
                    <tr> 
                      <td class=tdHeaderAlt rowspan="2" valign="top">3)</td>
                      <td class=tdBase>Otherwise, if you would like to create 
                        a new practitioner record, please <a class=linkBase href = "tAdminPhysicianLU_main_PhysicianMaster_AdminID_form_create.jsp?EDIT=new">click here</a>. </td>
                    </tr>
                  </table>
                  <%
    }
//END-verify that the doctor is not already added:





}
else
{
%>
                  <br>
                  <p class=instructions><b>There is no match on file. Please be 
                    sure you have the exact spelling of the last name and the 
                    format of the SSN and DOB is correct</b>.</p>
                  <table cellpadding=2>
                    <tr> 
                      <td colspan=2 class=tdHeaderAlt>Your Options:</td>
                    </tr>
                    <tr> 
                      <td class=tdHeaderAlt>1)</td>
                      
                <td class=instructions> Check your search criteria <a href="#retry">below</a> 
                  to ensure that the SSN and DOB look correct. Fix any errors 
                  and click Submit to retry your search.</td>
                    </tr>
                    <tr> 
                      <td class=tdHeaderAlt>2)</td>
                      <td class=tdBase>If there still is no match and you are 
                        sure that there should be, please contact us at <a href="mailto:support@winstaff.com">support@winstaff.com</a> 
                        or call us at 800-995-4233.</td>
                    </tr>
                    <tr> 
                      <td class=tdHeaderAlt rowspan="2">3)</td>
                      <td class=tdBase>Otherwise, if you would like to create 
                        a new practitioner record, please <a class=linkBase href = "tAdminPhysicianLU_main_PhysicianMaster_AdminID_form_create.jsp?EDIT=new">click here</a>. </td>
                    </tr>
                  </table>
                  <%
}




}
catch(Exception e)
{
out.println("Display:"+e);
}





}
//else
{
%>
<p class=instructions>This feature allows you to find a practitioner whose data is already in this database, either through a data conversion or from another practice. It then links his/her information to your admin user account. You must enter the Last Name, DOB, and SSN exactly in order for PRO-FILE to find a match.</p>
            <table width = 100% cellpadding=0 cellspacing=0>
              <tr> 
                <td> 
                  <form name="form1" method="get" action="tAdminPhysician_MasterAdd.jsp">
                    <table width="95%" border="1" cellspacing="0" cellpadding="0" align="center" bordercolor="#003333">
                      <tr> 
                        <td> 
                          <table width="100%" border="0" cellspacing="0" cellpadding="3" align="center">
                            <tr> 
                              <td class=tdHeaderAlt> <a name="retry"></a>Last 
                                Name </td>
                              <td> 
                                <input type=text name="LastName" value="<%=myLastName%>">
                              </td>
                            </tr>
                            <tr> 
                              <td class=tdHeaderAlt> SSN (###-##-####)</td>
                              <td> 
                                <input type=text name="SSN" value="<%=mySSN%>">
                              </td>
                            </tr>
                            <tr> 
                              <td class=tdHeaderAlt> DOB </td>
                              <td>
                                <select   name="MONTH" >
                                  <jsp:include page="../generic/tMonthTypeLILong.jsp" flush="true" >
                                  <jsp:param name="CurrentSelection" value="<%=myMonth%>" />
                                  </jsp:include>
                                </select>
                                <select   name="DAY" >
                                  <jsp:include page="../generic/tDaysTypeLILong.jsp" flush="true" >
                                  <jsp:param name="CurrentSelection" value="<%=myDay%>" />
                                  </jsp:include>
                                </select>
                                <input name="YEAR" value="<%=myYear%>" size=7 max=4 maxlength="4" >
                              </td>
                            </tr>
                            <tr class="tdHeaderAlt"> 
                              <td><!-- <%=myDOB%> --></td>
                              <td>&nbsp; </td>
                              <td> 
                                <input type="submit" name="Submit2" value="Submit">
                              </td>
                              <td>&nbsp;</td>
                            </tr>
                            <tr class="tdHeaderAlt"> 
                              <td>&nbsp;</td>
                              <td>&nbsp;</td>
                              <td>&nbsp;</td>
                              <td>&nbsp;</td>
                            </tr>
                          </table>
                        </td>
                        <td valign="top"> 
                          <div align="center"><img src="images/findadd-large.gif" width="98" height="88"></div>
                        </td>
                      </tr>
                      <tr> 
                        <td valign="top"> 
                          <table cellpadding=2>
                            <tr> 
                              <td class=tdHeader>If you are not able to find your practitioner and would like to create a
new practitioner record, please <a class=linkBase href = "tAdminPhysicianLU_main_PhysicianMaster_AdminID_form_create.jsp?EDIT=new">click here</a>.</td>
                            </tr>
                          </table>
                        </td>
                        <td> 
                          <div align="center"><img src="images/create-new.gif" ></div>
                        </td>
                      </tr>
                    </table>
                  </form>
                  <p> 
                    <%
}




}
catch (Exception e)
{
out.println("Error???:"+e);
System.out.println("Error:"+e);
}

%>
            </table></form>
            </td>
        </tr>
      </table>
    </td>
  </tr>
</table></td>
</tr> </table> 
<%

  }
  else
  {
   response.sendRedirect("/errorhandler.jsp");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%></td></tr></table>
<%String bnIncludeFN = "ui_"+thePLCID+"\\bot-nav_AdminID.jsp";
%>
<jsp:include page="<%=bnIncludeFN%>" flush="true" >
<jsp:param name="plcID" value="<%=thePLCID%>"/>
</jsp:include>
