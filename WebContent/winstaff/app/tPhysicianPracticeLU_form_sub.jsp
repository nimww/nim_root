<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.bltDocumentManagement,java.util.Vector,java.util.Enumeration,com.winstaff.ListElement, com.winstaff.PLCUtils, com.winstaff.SecurityCheck, com.winstaff.ConfigurationMessages,com.winstaff.bltPhysicianPracticeLU,com.winstaff.DocumentManagerUtils" %>
<%/*
    filename: out\jsp\tPhysicianPracticeLU_form_sub.jsp
    Created on Mar/21/2003
    Created by: Scott Ellis
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>


<%

//initial declaration of list class and parentID
    Integer        iLookupID        =    null;
    boolean accessValid = false;

   Integer iSecurityCheck = SecurityCheck.CheckItem("Practice1", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {

    if (pageControllerHash.containsKey("iLookupID")) 
    {
        iLookupID        =    (Integer)pageControllerHash.get("iLookupID");
        if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {
            accessValid = true;
        }
    }
  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);

//initial declaration of list class and parentID

    bltPhysicianPracticeLU        PhysicianPracticeLU        =    null;

    if ( request.getParameter( "EDIT" ).equalsIgnoreCase("edit") )
    {
        PhysicianPracticeLU        =    new    bltPhysicianPracticeLU(iLookupID,UserSecurityGroupID);
    }
    else if ( request.getParameter( "EDIT" ).equalsIgnoreCase("new") )
    {
        PhysicianPracticeLU        =    new    bltPhysicianPracticeLU(UserSecurityGroupID,true);
    }

String testChangeID = "0";

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("UniqueCreateDate"))) ;
    if ( !PhysicianPracticeLU.getUniqueCreateDate().equals(testObj)  )
    {
         PhysicianPracticeLU.setUniqueCreateDate( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PhysicianPracticeLU UniqueCreateDate not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("UniqueModifyDate"))) ;
    if ( !PhysicianPracticeLU.getUniqueModifyDate().equals(testObj)  )
    {
         PhysicianPracticeLU.setUniqueModifyDate( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PhysicianPracticeLU UniqueModifyDate not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("UniqueModifyComments")) ;
    if ( !PhysicianPracticeLU.getUniqueModifyComments().equals(testObj)  )
    {
         PhysicianPracticeLU.setUniqueModifyComments( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PhysicianPracticeLU UniqueModifyComments not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("PhysicianID")) ;
    if ( !PhysicianPracticeLU.getPhysicianID().equals(testObj)  )
    {
         PhysicianPracticeLU.setPhysicianID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PhysicianPracticeLU PhysicianID not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("PracticeID")) ;
    if ( !PhysicianPracticeLU.getPracticeID().equals(testObj)  )
    {
         PhysicianPracticeLU.setPracticeID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PhysicianPracticeLU PracticeID not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("StartDate"))) ;
    if ( !PhysicianPracticeLU.getStartDate().equals(testObj)  )
    {
         PhysicianPracticeLU.setStartDate( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PhysicianPracticeLU StartDate not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("EndDate"))) ;
    if ( !PhysicianPracticeLU.getEndDate().equals(testObj)  )
    {
         PhysicianPracticeLU.setEndDate( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PhysicianPracticeLU EndDate not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("isPrimaryOffice")) ;
    if ( !PhysicianPracticeLU.getisPrimaryOffice().equals(testObj)  )
    {
         PhysicianPracticeLU.setisPrimaryOffice( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PhysicianPracticeLU isPrimaryOffice not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("isAdministrativeOffice")) ;
    if ( !PhysicianPracticeLU.getisAdministrativeOffice().equals(testObj)  )
    {
         PhysicianPracticeLU.setisAdministrativeOffice( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PhysicianPracticeLU isAdministrativeOffice not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("CoverageHours")) ;
    if ( !PhysicianPracticeLU.getCoverageHours().equals(testObj)  )
    {
         PhysicianPracticeLU.setCoverageHours( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PhysicianPracticeLU CoverageHours not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("Comments")) ;
    if ( !PhysicianPracticeLU.getComments().equals(testObj)  )
    {
         PhysicianPracticeLU.setComments( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PhysicianPracticeLU Comments not set. this is ok-not an error");
}
boolean bINT = false;
boolean bNextStep = false;
String sRefreshDoc = "refreshMe=true&";

// If an edit, update information; if new, append information
if (testChangeID.equalsIgnoreCase("1"))
{

	PhysicianPracticeLU.setUniqueModifyDate(new java.util.Date());
	String UserLogonDescription = "n/a";
	if (pageControllerHash.containsKey("UserLogonDescription"))
	{
	    UserLogonDescription = (String)pageControllerHash.get("UserLogonDescription");
	}
    //if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
    {
	    PhysicianPracticeLU.setUniqueModifyComments(UserLogonDescription);
	    PhysicianPracticeLU.commitData();
    }
}
String routePageReference = request.getParameter("routePageReference");
String nextPage=null;
if (routePageReference!=null)
{
        if (pageControllerHash.containsKey(routePageReference))
        {
            nextPage = (String)pageControllerHash.get(routePageReference);
        }
        else if (pageControllerHash.containsKey("sLocalChildReturnPage"))
        {
            nextPage = (String)pageControllerHash.get("sLocalChildReturnPage");
        }
        else if (pageControllerHash.containsKey("sParentReturnPage"))
        {
            nextPage = (String)pageControllerHash.get("sParentReturnPage");
        }
}
if (nextPage!=null)
{
	    %><script language=Javascript>
	      document.location="<%=nextPage%>";
	      </script><%
    //response.sendRedirect(nextPage+"?EDIT=edit");
}
        %>
Successful save

  <%
  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
  %>
