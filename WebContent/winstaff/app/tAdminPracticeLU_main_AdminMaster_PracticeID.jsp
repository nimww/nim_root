<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.bltDocumentManagement,java.util.Vector,java.util.Enumeration,com.winstaff.ListElement, com.winstaff.PLCUtils, com.winstaff.SecurityCheck, com.winstaff.ConfigurationMessages,com.winstaff.bltAdminMaster,com.winstaff.bltAdminPracticeLU,com.winstaff.bltAdminPracticeLU_List_LU_PracticeID" %>
<%/*
    filename: out\jsp\tAdminPracticeLU_main_AdminMaster_PracticeID.jsp
    Created on Mar/21/2003
    Type: n-n main class file
    Created by: Scott Ellis
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>

<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_PracticeID.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>

    <table cellpadding=0 cellspacing=0 border=0 width=<%=MasterTableWidth%> >
    <tr><td width=10>&nbsp;</td><td>

    <span class=title>Admin Practice Link</span><br>


<%
//initial declaration of list class and parentID
    Integer        iPracticeID        =    null;
    boolean accessValid = false;
   Integer iSecurityCheck = SecurityCheck.CheckItem("Company1", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {
    if (pageControllerHash.containsKey("iPracticeID")) 
    {
        iPracticeID        =    (Integer)pageControllerHash.get("iPracticeID");
        accessValid = true;
    }
  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);

      java.text.SimpleDateFormat displayDateLongSDF = new java.text.SimpleDateFormat("MMMM dd yyyy");
    pageControllerHash.put("sParentReturnPage","tAdminPracticeLU_main_AdminMaster_PracticeID.jsp");
    pageControllerHash.remove("iLookupID");
    pageControllerHash.remove("iAdminID");
    session.setAttribute("pageControllerHash",pageControllerHash);
    bltAdminPracticeLU_List_LU_PracticeID        bltAdminPracticeLU_List_LU_PracticeID        =    new    bltAdminPracticeLU_List_LU_PracticeID(iPracticeID);

//declaration of Enumeration
    bltAdminPracticeLU        working_bltAdminPracticeLU;
    ListElement         leCurrentElement;
    java.util.Enumeration eList = bltAdminPracticeLU_List_LU_PracticeID.elements();
    %>
        <%@ include file="tAdminPracticeLU_main_AdminMaster_PracticeID_instructions.jsp" %>

        <%if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {%>
        <a href = "tAdminPracticeLU_main_LU_PracticeID_form_create.jsp?EDIT=edit&KM=s"><img border=0 src="ui_<%=thePLCID%>/icons/add_PracticeID.gif"></a>
        <a href = "tAdminPracticeLU_main_AdminMaster_PracticeID_form_create.jsp?EDIT=new"&EDIT=edit"><img border=0 src="ui_<%=thePLCID%>/icons/create_PracticeID.gif"></a>

        <%}%>
    <%
    while (eList.hasMoreElements())
    {         %>
         <table border="0" bordercolor="333333" cellpadding="3" class=tdHeaderAlt cellspacing="0" width="100%">
            <tr>
              <td width="50%">
                   <table border="0" bordercolor="333333" cellpadding="3" class=tdHeaderAlt cellspacing="0" width="100%">
                     <tr> 
                       <td>
         <%

        leCurrentElement    = (ListElement) eList.nextElement();
        working_bltAdminPracticeLU  = (bltAdminPracticeLU) leCurrentElement.getObject();
        bltAdminMaster working_bltAdminMaster  = new bltAdminMaster(working_bltAdminPracticeLU.getAdminID());


        String theClass = "tdBase";
        if (!working_bltAdminMaster.isComplete())
        {
            theClass = "incompleteItem";
        %>
                <span class=incompleteItem><b>This item is incomplete</b></span><br>
        <%
        }
        %>

               <span class=<%=theClass%> ><b>ID:&nbsp;</b><%=working_bltAdminPracticeLU.getAdminID()%> </span>
                  </td></tr>
                  <tr><td><b>Item Create Date:&nbsp;</b><jsp:include page="../generic/DateTypeConvert_Main.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(working_bltAdminPracticeLU.getUniqueCreateDate())%>" /></jsp:include></td></tr>
                  <tr><td><b>Item Modify Date:&nbsp;</b><jsp:include page="../generic/DateTypeConvert_Main.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(working_bltAdminPracticeLU.getUniqueModifyDate())%>" /></jsp:include></td></tr>
                  <tr><td><b>Item Modify Comments:&nbsp;</b><%=working_bltAdminPracticeLU.getUniqueModifyComments()%></td></tr>
                  </td></tr></table>
            </td><td width="50%"> 
        <a class=linkBase  href = "tAdminPracticeLU_main_AdminMaster_PracticeID_form_authorize.jsp?EDIT=edit&EDITID=<%=working_bltAdminPracticeLU.getAdminID()%>"&EDIT=edit"><img border=0 src="ui_<%=thePLCID%>/icons/edit_PracticeID.gif"></a>

        <%if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {%>

        <br><a class=linkBase  href = "tAdminPracticeLU_main_LU_PracticeID_form_authorize.jsp?EDIT=edit&KM=s&EDITID=<%=working_bltAdminPracticeLU.getLookupID()%>"><img border=0 src="ui_<%=thePLCID%>/icons/modify_PracticeID.gif"></a>
        <br><a class=linkBase  onClick="return confirmDelete()"   href = "tAdminPracticeLU_main_LU_PracticeID_form_authorize.jsp?EDIT=del&EDITID=<%=working_bltAdminPracticeLU.getLookupID()%>&KM=p"><img border=0 src="ui_<%=thePLCID%>/icons/remove_PracticeID.gif"></a>
        <%}%>
                  </td></tr>
                 </table>
                 <table width="100%" border="1" cellspacing="0" bordercolor="#333333">
                  <tr>
                   <td>
                     <table width="100%" border="0" cellspacing="0">
                     <tr><td>

<%String theClassF = "textBase";%>

<%theClassF = "textBase";%>
<%if ((working_bltAdminMaster.isExpired("Name",expiredDays))&&(working_bltAdminMaster.isExpiredCheck("Name"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltAdminMaster.isRequired("Name"))&&(!working_bltAdminMaster.isComplete("Name")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Organization Name:&nbsp;</b><%=working_bltAdminMaster.getName()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltAdminMaster.isExpired("Address1",expiredDays))&&(working_bltAdminMaster.isExpiredCheck("Address1"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltAdminMaster.isRequired("Address1"))&&(!working_bltAdminMaster.isComplete("Address1")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Organization Address:&nbsp;</b><%=working_bltAdminMaster.getAddress1()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltAdminMaster.isExpired("Address2",expiredDays))&&(working_bltAdminMaster.isExpiredCheck("Address2"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltAdminMaster.isRequired("Address2"))&&(!working_bltAdminMaster.isComplete("Address2")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Organization Address 2:&nbsp;</b><%=working_bltAdminMaster.getAddress2()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltAdminMaster.isExpired("City",expiredDays))&&(working_bltAdminMaster.isExpiredCheck("City"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltAdminMaster.isRequired("City"))&&(!working_bltAdminMaster.isComplete("City")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>City:&nbsp;</b><%=working_bltAdminMaster.getCity()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltAdminMaster.isExpired("StateID",expiredDays))&&(working_bltAdminMaster.isExpiredCheck("StateID"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltAdminMaster.isRequired("StateID"))&&(!working_bltAdminMaster.isComplete("StateID")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>State:&nbsp;</b><jsp:include page="../generic/tStateLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltAdminMaster.getStateID()%>" /></jsp:include></p>

<%theClassF = "textBase";%>
<%if ((working_bltAdminMaster.isExpired("Province",expiredDays))&&(working_bltAdminMaster.isExpiredCheck("Province"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltAdminMaster.isRequired("Province"))&&(!working_bltAdminMaster.isComplete("Province")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Province, District, State:&nbsp;</b><%=working_bltAdminMaster.getProvince()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltAdminMaster.isExpired("ZIP",expiredDays))&&(working_bltAdminMaster.isExpiredCheck("ZIP"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltAdminMaster.isRequired("ZIP"))&&(!working_bltAdminMaster.isComplete("ZIP")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>ZIP:&nbsp;</b><%=working_bltAdminMaster.getZIP()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltAdminMaster.isExpired("CountryID",expiredDays))&&(working_bltAdminMaster.isExpiredCheck("CountryID"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltAdminMaster.isRequired("CountryID"))&&(!working_bltAdminMaster.isComplete("CountryID")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Country:&nbsp;</b><jsp:include page="../generic/tCountryLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltAdminMaster.getCountryID()%>" /></jsp:include></p>

<%theClassF = "textBase";%>
<%if ((working_bltAdminMaster.isExpired("Phone",expiredDays))&&(working_bltAdminMaster.isExpiredCheck("Phone"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltAdminMaster.isRequired("Phone"))&&(!working_bltAdminMaster.isComplete("Phone")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Organization Phone (XXX-XXX-XXXX):&nbsp;</b><%=working_bltAdminMaster.getPhone()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltAdminMaster.isExpired("AlertEmail",expiredDays))&&(working_bltAdminMaster.isExpiredCheck("AlertEmail"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltAdminMaster.isRequired("AlertEmail"))&&(!working_bltAdminMaster.isComplete("AlertEmail")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Alert Email:&nbsp;</b><%=working_bltAdminMaster.getAlertEmail()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltAdminMaster.isExpired("AlertDays",expiredDays))&&(working_bltAdminMaster.isExpiredCheck("AlertDays"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltAdminMaster.isRequired("AlertDays"))&&(!working_bltAdminMaster.isComplete("AlertDays")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Alert Days:&nbsp;</b><%=working_bltAdminMaster.getAlertDays()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltAdminMaster.isExpired("ContactFirstName",expiredDays))&&(working_bltAdminMaster.isExpiredCheck("ContactFirstName"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltAdminMaster.isRequired("ContactFirstName"))&&(!working_bltAdminMaster.isComplete("ContactFirstName")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Contact First Name:&nbsp;</b><%=working_bltAdminMaster.getContactFirstName()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltAdminMaster.isExpired("ContactLastName",expiredDays))&&(working_bltAdminMaster.isExpiredCheck("ContactLastName"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltAdminMaster.isRequired("ContactLastName"))&&(!working_bltAdminMaster.isComplete("ContactLastName")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Contact Last Name:&nbsp;</b><%=working_bltAdminMaster.getContactLastName()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltAdminMaster.isExpired("ContactEmail",expiredDays))&&(working_bltAdminMaster.isExpiredCheck("ContactEmail"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltAdminMaster.isRequired("ContactEmail"))&&(!working_bltAdminMaster.isComplete("ContactEmail")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Contact Email:&nbsp;</b><%=working_bltAdminMaster.getContactEmail()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltAdminMaster.isExpired("ContactAddress1",expiredDays))&&(working_bltAdminMaster.isExpiredCheck("ContactAddress1"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltAdminMaster.isRequired("ContactAddress1"))&&(!working_bltAdminMaster.isComplete("ContactAddress1")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Contact Address:&nbsp;</b><%=working_bltAdminMaster.getContactAddress1()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltAdminMaster.isExpired("ContactAddress2",expiredDays))&&(working_bltAdminMaster.isExpiredCheck("ContactAddress2"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltAdminMaster.isRequired("ContactAddress2"))&&(!working_bltAdminMaster.isComplete("ContactAddress2")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Contact Address 2:&nbsp;</b><%=working_bltAdminMaster.getContactAddress2()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltAdminMaster.isExpired("ContactCity",expiredDays))&&(working_bltAdminMaster.isExpiredCheck("ContactCity"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltAdminMaster.isRequired("ContactCity"))&&(!working_bltAdminMaster.isComplete("ContactCity")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Contact City:&nbsp;</b><%=working_bltAdminMaster.getContactCity()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltAdminMaster.isExpired("ContactStateID",expiredDays))&&(working_bltAdminMaster.isExpiredCheck("ContactStateID"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltAdminMaster.isRequired("ContactStateID"))&&(!working_bltAdminMaster.isComplete("ContactStateID")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Contact State:&nbsp;</b><jsp:include page="../generic/tStateLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltAdminMaster.getContactStateID()%>" /></jsp:include></p>

<%theClassF = "textBase";%>
<%if ((working_bltAdminMaster.isExpired("ContactZIP",expiredDays))&&(working_bltAdminMaster.isExpiredCheck("ContactZIP"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltAdminMaster.isRequired("ContactZIP"))&&(!working_bltAdminMaster.isComplete("ContactZIP")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Contact ZIP:&nbsp;</b><%=working_bltAdminMaster.getContactZIP()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltAdminMaster.isExpired("ContactPhone",expiredDays))&&(working_bltAdminMaster.isExpiredCheck("ContactPhone"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltAdminMaster.isRequired("ContactPhone"))&&(!working_bltAdminMaster.isComplete("ContactPhone")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Contact Phone (XXX-XXX-XXXX):&nbsp;</b><%=working_bltAdminMaster.getContactPhone()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltAdminMaster.isExpired("Comments",expiredDays))&&(working_bltAdminMaster.isExpiredCheck("Comments"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltAdminMaster.isRequired("Comments"))&&(!working_bltAdminMaster.isComplete("Comments")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Extra Comments:&nbsp;</b><%=working_bltAdminMaster.getComments()%></p>

        </td></tr></table></table><br>
        <%
    }
    %>

    </table>
    <%

  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>

    </td></tr></table>


<%String bnIncludeFN = "ui_"+thePLCID+"\\bot-nav_PracticeID.jsp";
%>
<jsp:include page="<%=bnIncludeFN%>" flush="true" ><jsp:param name="plcID" value="<%=thePLCID%>"/></jsp:include>
