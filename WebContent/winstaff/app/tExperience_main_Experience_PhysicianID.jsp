<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.bltDocumentManagement,java.util.Vector,java.util.Enumeration,com.winstaff.ListElement, com.winstaff.PLCUtils, com.winstaff.SecurityCheck, com.winstaff.ConfigurationMessages,com.winstaff.bltExperience,com.winstaff.bltExperience_List" %>
<%/*
    filename: tExperience_main_Experience_PhysicianID.jsp
    Created on Mar/21/2003
    Type: 1-n main class file
    Created by: Scott Ellis
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>

<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_PhysicianID.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>

    <table cellpadding=0 cellspacing=0 border=0 width=<%=MasterTableWidth%> >
    <tr><td width=10>&nbsp;</td><td>
    <%=ConfigurationMessages.getHTML("INTERVIEWTopControl","tExperience")%>



<%
//initial declaration of list class and parentID
    Integer        iPhysicianID        =    null;
    boolean accessValid = false;
   Integer iSecurityCheck = SecurityCheck.CheckItem("PractitionerSection7", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {
    if (pageControllerHash.containsKey("iPhysicianID")) 
    {
        iPhysicianID        =    (Integer)pageControllerHash.get("iPhysicianID");
        accessValid = true;
    }
  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);
      pageControllerHash.put("sParentReturnPage","tExperience_main_Experience_PhysicianID.jsp");
    pageControllerHash.remove("iExperienceID");
    pageControllerHash.put("sINTNext","tExperience_main_Experience_PhysicianID_form_create.jsp?EDIT=new&KM=p&INTNext=yes");
    session.setAttribute("pageControllerHash",pageControllerHash);

    bltExperience_List        bltExperience_List        =    new    bltExperience_List(iPhysicianID);

//declaration of Enumeration
    bltExperience        working_bltExperience;
    ListElement         leCurrentElement;
    Enumeration eList = bltExperience_List.elements();
    %>
        <%@ include file="tExperience_main_Experience_PhysicianID_instructions.jsp" %>

        <%if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {%>
        <a class=linkBase href = "tExperience_main_Experience_PhysicianID_form_create.jsp?EDIT=new&KM=p&INTNext=yes"><img border=0 src="ui_<%=thePLCID%>/icons/create_PhysicianID.gif"></a>
        <%}%>
         <table border="1" bordercolor="CCCCCC" cellpadding="3" class=tdBase cellspacing="0" width="100%">
    <%
    int altCnt = 0;
    if (eList.hasMoreElements())
    {
     while (eList.hasMoreElements())
     {

        altCnt++;
        String theClass = "tdBase";
        if (altCnt%2!=0)
        {
            theClass = "tdBaseAlt";
        }
        leCurrentElement    = (ListElement) eList.nextElement();
        working_bltExperience  = (bltExperience) leCurrentElement.getObject();
        working_bltExperience.GroupSecurityInit(UserSecurityGroupID);
        if (!working_bltExperience.isComplete())
        {
            theClass = "incompleteItem";
        %>
                <tr class=incompleteItem><td><b>Not Complete</b><br>
        <%
        }
        else
        {
        %>
        <tr class=<%=theClass%> ><td> 
        <%
        }
        %>

              <b>Item ID:&nbsp;</b><%=working_bltExperience.getExperienceID()%></td>
<%String theClassF = "textBase";%>

<%theClassF = "textBase";%>
<%if ((working_bltExperience.isExpired("TypeOfExperience",expiredDays))&&(working_bltExperience.isExpiredCheck("TypeOfExperience"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltExperience.isRequired("TypeOfExperience"))&&(!working_bltExperience.isComplete("TypeOfExperience")) ){theClassF = "requiredFieldMain";}%>
            <td><p class=<%=theClassF%> ><b>Experience Type:&nbsp;</b><jsp:include page="../generic/tExperienceTypeLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltExperience.getTypeOfExperience()%>" /></jsp:include></p></td>

<%theClassF = "textBase";%>
<%if ((working_bltExperience.isExpired("Other",expiredDays))&&(working_bltExperience.isExpiredCheck("Other"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltExperience.isRequired("Other"))&&(!working_bltExperience.isComplete("Other")) ){theClassF = "requiredFieldMain";}%>
            <td><p class=<%=theClassF%> ><b>Institution:&nbsp;</b><%=working_bltExperience.getName()%></p></td>

            <td > 
        <a class=linkBase href = "tExperience_main_Experience_PhysicianID_form_authorize.jsp?EDIT=edit&EDITID=<%=working_bltExperience.getExperienceID()%>&KM=p"><img border=0 src="ui_<%=thePLCID%>/icons/edit_PhysicianID.gif"></a>

        <%if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {%>
        <a class=linkBase  onClick="return confirmDelete()"  href = "tExperience_main_Experience_PhysicianID_form_authorize.jsp?EDIT=del&EDITID=<%=working_bltExperience.getExperienceID()%>&KM=p"><img border=0 src="ui_<%=thePLCID%>/icons/delete_PhysicianID.gif"></a>
        <% }%>
                  </td></tr>
        <%
    }//end while
       }//end of if
       else 
       {
           %>
           <tr><td><b>Please click the "create" to add <%=ConfigurationMessages.getDataCategory("tExperience")%> information or click 'Continue' to go to the next section.</b>
           <script language=javascript>
           if (confirm("<%=ConfigurationMessages.getInterviewMessage("INTERVIEWNoElements","tExperience")%>"))
           {
               document.location="tExperience_main_Experience_PhysicianID_form_create.jsp?EDIT=new&KM=p&INTNext=yes"; 
           }
           else
           {

           }
           </script>
           </td></tr>
           <%
       }
    %>

    </table>
    <%
  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>

    </td></tr></table><br>


<%String bnIncludeFN = "ui_"+thePLCID+"\\bot-nav_PhysicianID.jsp";
%>
<jsp:include page="<%=bnIncludeFN%>" flush="true" ><jsp:param name="plcID" value="<%=thePLCID%>"/></jsp:include>
