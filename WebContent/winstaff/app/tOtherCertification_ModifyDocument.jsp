<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.bltDocumentManagement,java.util.Vector,java.util.Enumeration,com.winstaff.ListElement, com.winstaff.PLCUtils, com.winstaff.SecurityCheck, com.winstaff.ConfigurationMessages,com.winstaff.bltOtherCertification,com.winstaff.DocumentManagerUtils,com.winstaff.bltOtherCertification_List" %>
<%/*
    filename: out\jsp\tOtherCertification_ModifyDocument.jsp
    Created on Mar/21/2003
    Type: 1-n main class file
    Created by: Scott Ellis
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>


<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_PU.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>


<%
//initial declaration of list class and parentID
    Integer        iPhysicianID        =    null;
    boolean accessValid = false;
   Integer iSecurityCheck = SecurityCheck.CheckItem("DocumentManagement1", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {
    if (pageControllerHash.containsKey("iPhysicianID")) 
    {
        iPhysicianID        =    (Integer)pageControllerHash.get("iPhysicianID");
        accessValid = true;
    }
  //page security
  if (accessValid)
  {
        Integer requestID = null;
        if (request.getParameter("EDITID")!=null)
        {
            requestID = new Integer(request.getParameter("EDITID"));
            //out.println(requestID);
        }
    bltOtherCertification_List        bltOtherCertification_List        =    new    bltOtherCertification_List(iPhysicianID,"OtherCertID="+requestID,"");

//declaration of Enumeration
    bltOtherCertification        working_bltOtherCertification;
    ListElement         leCurrentElement;
    Enumeration eList = bltOtherCertification_List.elements();
    %>
    <%
    if (eList.hasMoreElements())
    {
        leCurrentElement    = (ListElement) eList.nextElement();
        working_bltOtherCertification  = (bltOtherCertification) leCurrentElement.getObject();



        //Parameter Pass Code here
String parameterPassString ="";
java.util.Enumeration myParameterPassList = request.getParameterNames();
while (myParameterPassList.hasMoreElements())
{
	String myName = (String)myParameterPassList.nextElement();
	String myS = (String) request.getParameter(myName);
	parameterPassString+="&"+myName + "=" + myS;
}

        if (request.getParameter("EDIT").equalsIgnoreCase("del"))
        {

        }
        else if (request.getParameter("EDIT").equalsIgnoreCase("create"))
        {
            bltDocumentManagement myDoc = new bltDocumentManagement();
            DocumentManagerUtils myDUtils = new DocumentManagerUtils();
            String myDType = request.getParameter("dType");
            myDUtils.setDocumentType(myDType, working_bltOtherCertification.getUniqueID());
            myDoc.setDocumentName(myDUtils.getDocumentName());
            myDoc.setDateOfExpiration(myDUtils.getDateOfExpiration());
            myDoc.setDocumentTypeID(myDUtils.getDocumentTypeID());
            myDoc.setPhysicianID(iPhysicianID);
            myDoc.setArchived(new Integer(2));
            myDoc.commitData();
            working_bltOtherCertification.setDocuLinkID(myDoc.getUniqueID());
            working_bltOtherCertification.commitData();
           // targetRedirect = "tOtherCertification_form_delete.jsp?routePageReference=sParentReturnPage"+parameterPassString    ;
            out.println("<p class=instructions>"+ConfigurationMessages.getMessage("CreateDocHTML")+"</p>");
        }
        else if (request.getParameter("EDIT").equalsIgnoreCase("modify"))
        {
            bltDocumentManagement myDoc = new bltDocumentManagement(working_bltOtherCertification.getDocuLinkID());
            if (myDoc.getDocumentFileName().equalsIgnoreCase("")) 
            {
                DocumentManagerUtils myDUtils = new DocumentManagerUtils();
                String myDType = request.getParameter("dType");
                myDUtils.setDocumentType(myDType, working_bltOtherCertification.getUniqueID());
                myDoc.setDocumentName(myDUtils.getDocumentName());
                myDoc.setDateOfExpiration(myDUtils.getDateOfExpiration());
                myDoc.setDocumentTypeID(myDUtils.getDocumentTypeID());
                myDoc.setPhysicianID(iPhysicianID);
                myDoc.setArchived(new Integer(2));
                myDoc.commitData();
           // targetRedirect = "tOtherCertification_form_delete.jsp?routePageReference=sParentReturnPage"+parameterPassString    ;
                out.println("<p class=instructions>"+ConfigurationMessages.getMessage("UpdatedDocHTML")+"</p>");
            }
            else
            {
                //archive then create new
                myDoc.setArchived(new Integer("1"));
                myDoc.commitData();
            myDoc = new bltDocumentManagement();
            DocumentManagerUtils myDUtils = new DocumentManagerUtils();
            String myDType = request.getParameter("dType");
            myDUtils.setDocumentType(myDType, working_bltOtherCertification.getUniqueID());
            myDoc.setDocumentName(myDUtils.getDocumentName());
            myDoc.setDateOfExpiration(myDUtils.getDateOfExpiration());
            myDoc.setDocumentTypeID(myDUtils.getDocumentTypeID());
            myDoc.setPhysicianID(iPhysicianID);
            myDoc.setArchived(new Integer(2));
            myDoc.commitData();
            working_bltOtherCertification.setDocuLinkID(myDoc.getUniqueID());
            working_bltOtherCertification.commitData();
           // targetRedirect = "tOtherCertification_form_delete.jsp?routePageReference=sParentReturnPage"+parameterPassString    ;
            out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ArchivedNewDocHTML")+"</p>");
            }
        }
        else if (request.getParameter("EDIT").equalsIgnoreCase("remove"))
        {
            bltDocumentManagement myDoc = new bltDocumentManagement(working_bltOtherCertification.getDocuLinkID());
            if (myDoc.getDocumentFileName().equalsIgnoreCase("")) 
            {
                myDoc.setPhysicianID(new Integer("0"));
                out.println("<p class=instructions>"+ConfigurationMessages.getMessage("DeletedDocHTML")+"</p>");

            }
            else
            {
                myDoc.setArchived(new Integer("1"));
                out.println("<p class=instructions>"+ConfigurationMessages.getMessage("DeletedArchivedDocHTML")+"</p>");
            }
            myDoc.commitData();
            working_bltOtherCertification.setDocuLinkID(new Integer("0"));
            working_bltOtherCertification.commitData();
           // targetRedirect = "tOtherCertification_form_delete.jsp?routePageReference=sParentReturnPage"+parameterPassString    ;
        }
            String routePageReference = request.getParameter("routePageReference");
            String nextPage=null;
            if (routePageReference!=null)
{
                   if (pageControllerHash.containsKey(routePageReference))
                  {
                      nextPage = (String)pageControllerHash.get(routePageReference);
                   }
                    else if (pageControllerHash.containsKey("sParentReturnPage"))
                   {
                        nextPage = (String)pageControllerHash.get("sParentReturnPage");
                   }
            }
            else 
            {
                  nextPage = (String)pageControllerHash.get("sParentReturnPage");
            }
            if (nextPage!=null)
            {
              //response.sendRedirect(nextPage+"?EDIT=edit");
            }

    }
    else
    {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORInvalidQuery")+"</p>");
    }

  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
    if (request.getParameter("refreshMe")==null||request.getParameter("refreshMe").equalsIgnoreCase("")   )
    {
%>
    <a href =# onClick="window.close()">close</a>
<%
    }
    else
    {
%>
    <a href =# onClick="opener.document.location=opener.document.location;window.close()">close</a>
<%
    }
%>






