<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.bltDocumentManagement,java.util.Vector,java.util.Enumeration,com.winstaff.ListElement, com.winstaff.PLCUtils, com.winstaff.SecurityCheck, com.winstaff.ConfigurationMessages, com.winstaff.bltPeerReference, com.winstaff.bltPeerReference_List, com.winstaff.DocumentManagerUtils" %>
<%/*
    filename: tPeerReference_main_PeerReference_PhysicianID.jsp
    Created on Mar/01/2004
    Type: 1-n main class file
    Created by: Scott Ellis
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>
    <%
    Integer iExpressMode = new Integer(1);
    if (pageControllerHash.containsKey("iExpressMode")) 
    {
        iExpressMode =    (Integer)pageControllerHash.get("iExpressMode");
    }
    %>

    <table cellpadding=0 cellspacing=0 border=0 width=800 >
<%
//initial declaration of list class and parentID
    Integer        iPhysicianID        =    null;
    boolean accessValid = false;
   Integer iSecurityCheck = SecurityCheck.CheckItem("PractitionerSection11", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {
    if (pageControllerHash.containsKey("iPhysicianID")) 
    {
        iPhysicianID        =    (Integer)pageControllerHash.get("iPhysicianID");
        accessValid = true;
    }

  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);
      pageControllerHash.put("sParentReturnPage","tPeerReference_main_PeerReference_PhysicianID_Expand.jsp");
    pageControllerHash.remove("iReferenceID");
    session.setAttribute("pageControllerHash",pageControllerHash);

    bltPeerReference_List        bltPeerReference_List        =    new    bltPeerReference_List(iPhysicianID);

//declaration of Enumeration
    bltPeerReference        PeerReference;
    ListElement         leCurrentElement;
    Enumeration eList = bltPeerReference_List.elements();
    %>
    <%
    int iExpress=0;
boolean errorRouteMeTotal = false;
boolean isAllComplete = true;
    while (eList.hasMoreElements()||iExpress<=ConfigurationMessages.getExpressItemCount("tPeerReference"))
    {
       iExpress++;

      boolean isNewRecord= false;
      if (eList.hasMoreElements())
      {
        leCurrentElement    = (ListElement) eList.nextElement();
        PeerReference  = (bltPeerReference) leCurrentElement.getObject();
      }
      else
      {
        PeerReference  = new bltPeerReference();
        PeerReference.setPhysicianID(iPhysicianID);
        isNewRecord= true;
      }
        PeerReference.GroupSecurityInit(UserSecurityGroupID);
        String theClass = "tdHeader";
        %>

        <%  {

String testChangeID = "0";

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("UniqueCreateDate_"+iExpress))) ;
    if ( !PeerReference.getUniqueCreateDate().equals(testObj)  )
    {
         PeerReference.setUniqueCreateDate( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PeerReference UniqueCreateDate not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("UniqueModifyDate_"+iExpress))) ;
    if ( !PeerReference.getUniqueModifyDate().equals(testObj)  )
    {
         PeerReference.setUniqueModifyDate( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PeerReference UniqueModifyDate not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("UniqueModifyComments_"+iExpress)) ;
    if ( !PeerReference.getUniqueModifyComments().equals(testObj)  )
    {
         PeerReference.setUniqueModifyComments( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PeerReference UniqueModifyComments not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("PhysicianID_"+iExpress)) ;
    if ( !PeerReference.getPhysicianID().equals(testObj)  )
    {
         PeerReference.setPhysicianID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PeerReference PhysicianID not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("Salutation_"+iExpress)) ;
    if ( !PeerReference.getSalutation().equals(testObj)  )
    {
         PeerReference.setSalutation( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PeerReference Salutation not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("FirstName_"+iExpress)) ;
    if ( !PeerReference.getFirstName().equals(testObj)  )
    {
         PeerReference.setFirstName( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PeerReference FirstName not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("LastName_"+iExpress)) ;
    if ( !PeerReference.getLastName().equals(testObj)  )
    {
         PeerReference.setLastName( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PeerReference LastName not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("Specialty_"+iExpress)) ;
    if ( !PeerReference.getSpecialty().equals(testObj)  )
    {
         PeerReference.setSpecialty( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PeerReference Specialty not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("Address1_"+iExpress)) ;
    if ( !PeerReference.getAddress1().equals(testObj)  )
    {
         PeerReference.setAddress1( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PeerReference Address1 not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("Address2_"+iExpress)) ;
    if ( !PeerReference.getAddress2().equals(testObj)  )
    {
         PeerReference.setAddress2( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PeerReference Address2 not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("City_"+iExpress)) ;
    if ( !PeerReference.getCity().equals(testObj)  )
    {
         PeerReference.setCity( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PeerReference City not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("StateID_"+iExpress)) ;
    if ( !PeerReference.getStateID().equals(testObj)  )
    {
         PeerReference.setStateID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PeerReference StateID not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("Province_"+iExpress)) ;
    if ( !PeerReference.getProvince().equals(testObj)  )
    {
         PeerReference.setProvince( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PeerReference Province not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("ZIP_"+iExpress)) ;
    if ( !PeerReference.getZIP().equals(testObj)  )
    {
         PeerReference.setZIP( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PeerReference ZIP not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("CountryID_"+iExpress)) ;
    if ( !PeerReference.getCountryID().equals(testObj)  )
    {
         PeerReference.setCountryID( testObj,UserSecurityGroupID   );
         //testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PeerReference CountryID not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("Phone_"+iExpress)) ;
    if ( !PeerReference.getPhone().equals(testObj)  )
    {
         PeerReference.setPhone( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PeerReference Phone not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("Fax_"+iExpress)) ;
    if ( !PeerReference.getFax().equals(testObj)  )
    {
         PeerReference.setFax( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PeerReference Fax not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("ContactEmail_"+iExpress)) ;
    if ( !PeerReference.getContactEmail().equals(testObj)  )
    {
         PeerReference.setContactEmail( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PeerReference ContactEmail not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("YearsAssociated_"+iExpress)) ;
    if ( !PeerReference.getYearsAssociated().equals(testObj)  )
    {
         PeerReference.setYearsAssociated( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PeerReference YearsAssociated not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("DocuLinkID_"+iExpress)) ;
    if ( !PeerReference.getDocuLinkID().equals(testObj)  )
    {
         PeerReference.setDocuLinkID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PeerReference DocuLinkID not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("Comments_"+iExpress)) ;
    if ( !PeerReference.getComments().equals(testObj)  )
    {
         PeerReference.setComments( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PeerReference Comments not set. this is ok-not an error");
}


try
{
    String testObj = new String(request.getParameter("Title_"+iExpress)) ;
    if ( !PeerReference.getTitle().equals(testObj)  )
    {
         PeerReference.setTitle( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PeerReference Title not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("HospitalAffiliation_"+iExpress)) ;
    if ( !PeerReference.getHospitalAffiliation().equals(testObj)  )
    {
         PeerReference.setHospitalAffiliation( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PeerReference HospitalAffiliation not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("HospitalDepartment_"+iExpress)) ;
    if ( !PeerReference.getHospitalDepartment().equals(testObj)  )
    {
         PeerReference.setHospitalDepartment( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PeerReference HospitalDepartment not set. this is ok-not an error");
}

boolean bINT = false;
boolean bNextStep = false;
String sRefreshDoc = "refreshMe=true&";
              if (false&&request.getParameter("INTNext")!=null&&request.getParameter("INTNext").equalsIgnoreCase("yes") ) 
	            {
	                bINT = true;
	                sRefreshDoc = "";
	            }
              else if (request.getParameter("INTNext")!=null&&request.getParameter("INTNext").equalsIgnoreCase("next") ) 
	            {
	                bINT = true;
	                sINTNext = ConfigurationMessages.getInterviewLinkRaw("tPeerReference","next");
	                sRefreshDoc = "";
	            }

// If an edit, update information; if new, append information
boolean errorRouteMe = false;
if (testChangeID.equalsIgnoreCase("1"))
{

	PeerReference.setUniqueModifyDate(new java.util.Date());
	String UserLogonDescription = "n/a";
	if (pageControllerHash.containsKey("UserLogonDescription"))
	{
	    UserLogonDescription = (String)pageControllerHash.get("UserLogonDescription");
	}
    if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
    {
	    PeerReference.setUniqueModifyComments("EX:"+UserLogonDescription);
	    try
	    {
	        PeerReference.commitData();
	        if (!PeerReference.isComplete())
	        {
	            isAllComplete = false;
	        }
	    }
	    catch (Exception e55)
	    {
	        errorRouteMe = true;
	        errorRouteMeTotal = true;
	    }	//Doculink Addition
	//Doculink Does Exist, prompt to Modify

     if (!errorRouteMe&&PeerReference.getDocuLinkID().intValue()>0)
     {
            bltDocumentManagement myDoc = new bltDocumentManagement(PeerReference.getDocuLinkID());
            if (myDoc.getDocumentFileName().equalsIgnoreCase("")) 
            {
                DocumentManagerUtils myDUtils = new DocumentManagerUtils();
                String myDType = "tPeerReference";
                myDUtils.setDocumentType(myDType, PeerReference.getUniqueID());
                myDoc.setDocumentName(myDUtils.getDocumentName());
                myDoc.setDateOfExpiration(myDUtils.getDateOfExpiration());
                myDoc.setDocumentTypeID(myDUtils.getDocumentTypeID());
                myDoc.setPhysicianID(iPhysicianID);
                myDoc.setArchived(new Integer(2));
                myDoc.commitData();
            }
            else
            {
                //archive then create new
                myDoc.setArchived(new Integer("1"));
                myDoc.commitData();
                myDoc = new bltDocumentManagement();
                DocumentManagerUtils myDUtils = new DocumentManagerUtils();
                String myDType = "tPeerReference";
                myDUtils.setDocumentType(myDType, PeerReference.getUniqueID());
                myDoc.setDocumentName(myDUtils.getDocumentName());
                myDoc.setDateOfExpiration(myDUtils.getDateOfExpiration());
                myDoc.setDocumentTypeID(myDUtils.getDocumentTypeID());
                myDoc.setPhysicianID(iPhysicianID);
                myDoc.setArchived(new Integer(2));
                myDoc.commitData();
                PeerReference.setDocuLinkID(myDoc.getUniqueID());
                PeerReference.commitData();
            }
     }
     else if (!errorRouteMe)
     {
            bltDocumentManagement myDoc = new bltDocumentManagement();
            DocumentManagerUtils myDUtils = new DocumentManagerUtils();
            String myDType = "tPeerReference";
            myDUtils.setDocumentType(myDType, PeerReference.getUniqueID());
            myDoc.setDocumentName(myDUtils.getDocumentName());
            myDoc.setDateOfExpiration(myDUtils.getDateOfExpiration());
            myDoc.setDocumentTypeID(myDUtils.getDocumentTypeID());
                myDoc.setPhysicianID(iPhysicianID);
            myDoc.setArchived(new Integer(2));
            myDoc.commitData();
            PeerReference.setDocuLinkID(myDoc.getUniqueID());
            PeerReference.commitData();
     }

    }
}       }
    }//while

if (errorRouteMeTotal)
{
	out.println(ConfigurationMessages.getMessage("FatalError")); 
}
else 
{
String nextPage = request.getParameter("nextPage");
if (nextPage==null||nextPage.equalsIgnoreCase(""))
{
	nextPage = ConfigurationMessages.getExpressLinkRaw("tPeerReference","next",iExpressMode);
}
if (!isAllComplete)
{
	%>
	<script language=Javascript>
	      if ( confirm("<%=ConfigurationMessages.getMessage("ConfirmReqFieldsReturn")%>") )
	      {
	          document.location="<%=ConfigurationMessages.getDataCategoryLink("tPeerReference","Express")%>";
	      }
	      else 
	      {
	          document.location="<%=nextPage%>";
	      }
	</script>
  <%
}
else
{
	%>
	<script language=Javascript>
	      document.location="<%=nextPage%>";
	</script>
  <%
}
}
       
  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>

    </td></tr></table>


<%String bnIncludeFN = "ui_"+thePLCID+"\\bot-nav_PhysicianID.jsp";
%>
<jsp:include page="<%=bnIncludeFN%>" flush="true" ><jsp:param name="plcID" value="<%=thePLCID%>"/></jsp:include>