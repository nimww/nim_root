 
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.SecurityCheck,com.winstaff.PLCUtils,java.util.Vector,java.util.Enumeration,com.winstaff.ListElement,com.winstaff.bltCompanyMaster" %>
<%/*
    filename: out\jsp\tCompanyMaster_form.jsp
    JSP AutoGen on Mar/02/2002
*/%>
<%@ include file="../generic/CheckLogin.jsp" %>
<%
//String thePLCID = (String)pageControllerHash.get("plcID");
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_CompanyID.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>
<%
//initial declaration of list class and parentID
    Integer        iCompanyID        =    null;
    boolean accessValid = false;
    // required for Type2
    String sKeyMasterReference = null;
    if (pageControllerHash.containsKey("iCompanyID")) 
    {
        iCompanyID        =    (Integer)pageControllerHash.get("iCompanyID");
        accessValid = true;    
   }
  //page security
   Integer iSecurityCheck = SecurityCheck.CheckItem("Company1", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);      java.text.SimpleDateFormat displayDateTimeSDF = new java.text.SimpleDateFormat("MM/dd/yyyy hh:mm a");

    pageControllerHash.put("sLocalChildReturnPage","pro-file_Status.jsp");
    pageControllerHash.put("sParentReturnPage","pro-file_Status.jsp");
    session.setAttribute("pageControllerHash",pageControllerHash);
bltCompanyMaster        CompanyMaster        =   new  bltCompanyMaster (iCompanyID);

%>
<table width="700" border="0" cellspacing="0" cellpadding="0">
  <tr> 
    <td width=10>&nbsp;</td>
    <td> 
      <p class=title>PRO-FILE Manager Status</p>
      <table width="100%" border="1" cellspacing="0" cellpadding="3" bordercolor="#333333">
        <tr> 
          <td> 
            <table width="100%" border="1" cellspacing="0" cellpadding="0" bordercolor="#CCCCCC">
              <tr> 
                <td class=tdHeader width="30%" valign="top"> 
                  <div align="right">Company Group Information:&nbsp;</div>
                </td>
                <td> 
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr class=tdBase> 
                      <td width="30%" align="right" valign="top"> Name:&nbsp;</td>
                      <td><%=CompanyMaster.getName()%></td>
                    </tr>
                    <tr class=tdBase> 
                      <td width="30%" align="right" valign="top"> Address:&nbsp;</td>
                      <td><%=CompanyMaster.getAddress1()%>&nbsp;<%=CompanyMaster.getAddress2()%><br>
                        <%=CompanyMaster.getCity()%>&nbsp; 
                        <jsp:include page="../generic/tStateLILong_translate.jsp" flush="true" > 
                        <jsp:param name="CurrentSelection" value="<%=CompanyMaster.getStateID()%>" />
                        </jsp:include>
                        &nbsp;<%=CompanyMaster.getZIP()%></td>
                    </tr>
                    <tr class=tdBase>
                      <td width="30%" align="right" valign="top">Email:&nbsp;</td>
                      <td><a href="mailto:<%=CompanyMaster.getContactEmail()%>" ><%=CompanyMaster.getContactEmail()%></a></td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr> 
                <td class=tdHeader width="30%" valign="top"> 
                  <div align="right">User Information:&nbsp;</div>
                </td>
                <td> 
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr class=tdBase> 
                      <td width="30%" align="right" valign="top">User Logon Name:&nbsp;</td>
                      <td><%=CurrentUserAccount.getLogonUserName()%></td>
                    </tr>
                    <tr class=tdBase> 
                      <td width="30%" align="right" valign="top">User Access:&nbsp;</td>
                      <td>
                        <jsp:include page="../generic/tUserAccessTypeLILong_translate.jsp" flush="true" > 
                        <jsp:param name="CurrentSelection" value="<%=CurrentUserAccount.getAccessType()%>" />
                        </jsp:include>
			</td>
                    </tr>
                    <tr class=tdBase> 
                      <td width="30%" align="right" valign="top">Contact Email:&nbsp;</td>
                      <td><a href="mailto:<%=CurrentUserAccount.getContactEmail()%>" ><%=CurrentUserAccount.getContactEmail()%></a></td>
                    </tr>
                  </table>
                </td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
<%
if (iCompanyID.intValue()==1)
{
%>
<a href="userlog_dl.jsp">download user logs</a>
<%
}
%>
      <p>&nbsp;</p>
      <p>&nbsp;</p>
      <p>&nbsp;</p>
    </td>
  </tr>
</table>
<%

  }
  else
  {
   out.println("illegal");
  }
}
else
{
out.println("Your Security Level does not permit you to View this.");
}


%>
<%String bnIncludeFN = "ui_"+thePLCID+"\\bot-nav_CompanyID.jsp";
%>
<jsp:include page="<%=bnIncludeFN%>" flush="true" > 
<jsp:param name="plcID" value="<%=thePLCID%>"/>
</jsp:include>
