<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.bltDocumentManagement,java.util.Vector,java.util.Enumeration,com.winstaff.ListElement, com.winstaff.PLCUtils, com.winstaff.SecurityCheck, com.winstaff.ConfigurationMessages,com.winstaff.bltPhysicianPracticeLU,com.winstaff.bltPhysicianPracticeLU_List_LU_PhysicianID" %>
<%/*
    filename: tPhysicianPracticeLU_main_LU_PhysicianID.jsp
    Created on Mar/21/2003
    Type: 1-n main class file
    Created by: Scott Ellis
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>

<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_PhysicianID.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>

    <table cellpadding=0 cellspacing=0 border=0 width=<%=MasterTableWidth%> >
    <tr><td width=10>&nbsp;</td><td>
    <%=ConfigurationMessages.getHTML("INTERVIEWTopControl","tPhysicianPracticeLU")%>



<%
//initial declaration of list class and parentID
    Integer        iPhysicianID        =    null;
    boolean accessValid = false;
   Integer iSecurityCheck = SecurityCheck.CheckItem("Practice1", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {
    if (pageControllerHash.containsKey("iPhysicianID")) 
    {
        iPhysicianID        =    (Integer)pageControllerHash.get("iPhysicianID");
        accessValid = true;
    }
  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);
      pageControllerHash.put("sParentReturnPage","tPhysicianPracticeLU_main_LU_PhysicianID.jsp");
    pageControllerHash.remove("iLookupID");
    pageControllerHash.put("sINTNext","tPhysicianPracticeLU_main_LU_PhysicianID_form_create.jsp?EDIT=new&KM=p&INTNext=yes");
    session.setAttribute("pageControllerHash",pageControllerHash);

    bltPhysicianPracticeLU_List_LU_PhysicianID        bltPhysicianPracticeLU_List_LU_PhysicianID        =    new    bltPhysicianPracticeLU_List_LU_PhysicianID(iPhysicianID);

//declaration of Enumeration
    bltPhysicianPracticeLU        working_bltPhysicianPracticeLU;
    ListElement         leCurrentElement;
    java.util.Enumeration eList = bltPhysicianPracticeLU_List_LU_PhysicianID.elements();
    %>
        <%@ include file="tPhysicianPracticeLU_main_LU_PhysicianID_instructions.jsp" %>

        <%if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {%>
        <a class=linkBase href = "tPhysicianPracticeLU_main_LU_PhysicianID_form_create.jsp?EDIT=new&KM=p&INTNext=yes"><img border=0 src="ui_<%=thePLCID%>/icons/create_PhysicianID.gif"></a>
        <%}%>
         <table border="1" bordercolor="CCCCCC" cellpadding="3" class=tdBase cellspacing="0" width="100%">
    <%
    int altCnt = 0;
    if (eList.hasMoreElements())
    {
     while (eList.hasMoreElements())
     {

        altCnt++;
        String theClass = "tdBase";
        if (altCnt%2!=0)
        {
            theClass = "tdBaseAlt";
        }
        leCurrentElement    = (ListElement) eList.nextElement();
        working_bltPhysicianPracticeLU  = (bltPhysicianPracticeLU) leCurrentElement.getObject();
        working_bltPhysicianPracticeLU.GroupSecurityInit(UserSecurityGroupID);
        if (!working_bltPhysicianPracticeLU.isComplete())
        {
            theClass = "incompleteItem";
        %>
                <tr class=incompleteItem><td><b>Not Complete</b><br>
        <%
        }
        else
        {
        %>
        <tr class=<%=theClass%> ><td> 
        <%
        }
        %>

              <b>Item ID:&nbsp;</b><%=working_bltPhysicianPracticeLU.getLookupID()%></td>
<%String theClassF = "textBase";%>

<%theClassF = "textBase";%>
<%if ((working_bltPhysicianPracticeLU.isExpired("PracticeID",expiredDays))&&(working_bltPhysicianPracticeLU.isExpiredCheck("PracticeID"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPhysicianPracticeLU.isRequired("PracticeID"))&&(!working_bltPhysicianPracticeLU.isComplete("PracticeID")) ){theClassF = "requiredFieldMain";}%>
            <td><p class=<%=theClassF%> ><b>PracticeID:&nbsp;</b><%=working_bltPhysicianPracticeLU.getPracticeID()%></p></td>

<%theClassF = "textBase";%>
<%if ((working_bltPhysicianPracticeLU.isExpired("StartDate",expiredDays))&&(working_bltPhysicianPracticeLU.isExpiredCheck("StartDate"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPhysicianPracticeLU.isRequired("StartDate"))&&(!working_bltPhysicianPracticeLU.isComplete("StartDate")) ){theClassF = "requiredFieldMain";}%>

                        <p class=<%=theClassF%> ><b>Start Date:&nbsp;</b><jsp:include page="../generic/DateTypeConvert_Main.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(working_bltPhysicianPracticeLU.getStartDate())%>" /></jsp:include></p>


            <td > 
        <a class=linkBase href = "tPhysicianPracticeLU_main_LU_PhysicianID_form_authorize.jsp?EDIT=edit&EDITID=<%=working_bltPhysicianPracticeLU.getLookupID()%>&KM=p"><img border=0 src="ui_<%=thePLCID%>/icons/edit_PhysicianID.gif"></a>

        <%if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {%>
        <a class=linkBase  onClick="return confirmDelete()"  href = "tPhysicianPracticeLU_main_LU_PhysicianID_form_authorize.jsp?EDIT=del&EDITID=<%=working_bltPhysicianPracticeLU.getLookupID()%>&KM=p"><img border=0 src="ui_<%=thePLCID%>/icons/delete_PhysicianID.gif"></a>
        <% }%>
                  </td></tr>
        <%
    }//end while
       }//end of if
       else 
       {
           %>
           <tr><td><b>Please click the "create" to add <%=ConfigurationMessages.getDataCategory("tPhysicianPracticeLU")%> information or click 'Continue' to go to the next section.</b>
           <script language=javascript>
           if (confirm("<%=ConfigurationMessages.getInterviewMessage("INTERVIEWNoElements","tPhysicianPracticeLU")%>"))
           {
               document.location="tPhysicianPracticeLU_main_LU_PhysicianID_form_create.jsp?EDIT=new&KM=p&INTNext=yes"; 
           }
           else
           {

           }
           </script>
           </td></tr>
           <%
       }
    %>

    </table>
    <%
  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>

    </td></tr></table><br>


<%String bnIncludeFN = "ui_"+thePLCID+"\\bot-nav_PhysicianID.jsp";
%>
<jsp:include page="<%=bnIncludeFN%>" flush="true" ><jsp:param name="plcID" value="<%=thePLCID%>"/></jsp:include>
