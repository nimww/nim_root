<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.PLCUtils,com.winstaff.bltHCOMaster,com.winstaff.bltCompanyHCOLU_List_LU_CompanyID,com.winstaff.bltCompanyHCOLU,com.winstaff.bltPracticeMaster,com.winstaff.bltPhysicianMaster,com.winstaff.searchDB2,java.util.Vector,java.util.Enumeration,com.winstaff.ListElement,com.winstaff.bltAdminMaster,com.winstaff.bltCompanyAdminLU,com.winstaff.bltCompanyAdminLU_List_LU_CompanyID" %>
<%/*
    filename: out\jsp\tCompanyAdminLU_main_AdminMaster_CompanyID.jsp
    JSP AutoGen on Mar/26/2002
    Type: n-n main class file
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>

<%
//String thePLCID = (String)pageControllerHash.get("plcID");
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_PU.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>
<table cellpadding=0 cellspacing=0 border=0 width=500>	
  <tr> 
    <td width=10>&nbsp;</td>
    <td> <span class=title>Admin LookUP</span>
    <br>
      <%
//initial declaration of list class and parentID
    Integer        iCompanyID        =    null;
    boolean accessValid = false;
    if (pageControllerHash.containsKey("iCompanyID")) 
    {
        iCompanyID        =    (Integer)pageControllerHash.get("iCompanyID");
        accessValid = true;
    }
  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);
      java.text.SimpleDateFormat displayDateLongSDF = new java.text.SimpleDateFormat("MMMM dd yyyy");
    bltCompanyAdminLU_List_LU_CompanyID        bltCompanyAdminLU_List_LU_CompanyID        =    new    bltCompanyAdminLU_List_LU_CompanyID(iCompanyID);
int whichOne = 0;

if (request.getParameter("which")!=null)
{
    whichOne = Integer.parseInt(request.getParameter("which"));
}
%>
    <form name=fload>
    <select style="font-family: Arial; font-size: 10px" name=sload onChange="javascript:document.location=document.fload.sload.value;">
    <option value=ALL_via_CompanyID.jsp?which=0>Admin</option>
    <option <%if (whichOne==1){out.println("Selected");}%> value=ALL_via_CompanyID.jsp?which=1>Physician</option>
    <option <%if (whichOne==2){out.println("Selected");}%> value=ALL_via_CompanyID.jsp?which=2>Practice</option>
    <option <%if (whichOne==3){out.println("Selected");}%> value=ALL_via_CompanyID.jsp?which=3>HCO</option>
    </select><a href=# onClick="javascript:window.close()">close</a>
    </form>

<%

if (whichOne==0)
{
//if Admin
//declaration of Enumeration
    bltCompanyAdminLU        working_bltCompanyAdminLU;
    ListElement         leCurrentElement;
    java.util.Enumeration eList = bltCompanyAdminLU_List_LU_CompanyID.elements();

        String theClass = "tdHeader";
        %>
      <p class=<%=theClass%>> 
        <%String theClassF = "textBase";%>
      </p>
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr  class=tdHeader> 
          <td>ID</td>
          <td><b>Create Date</b></td>
          <td><b>Name</b></td>
          <td><b>City</b></td>
          <td><b>State</b></td>
          <td> 
            <p class=<%=theClassF%> ><b>ZIP</b></p>
          </td>
          <td>&nbsp;</td>
        </tr>
<%
    while (eList.hasMoreElements())
    {
        leCurrentElement    = (ListElement) eList.nextElement();
        working_bltCompanyAdminLU  = (bltCompanyAdminLU) leCurrentElement.getObject();
        bltAdminMaster working_bltAdminMaster  = new bltAdminMaster(working_bltCompanyAdminLU.getAdminID());
%>
        <tr> 
          <td><%=working_bltCompanyAdminLU.getAdminID()%></td>
          <td><%=displayDateSDF1.format(working_bltAdminMaster.getUniqueCreateDate())%></td>
          <td><%=working_bltAdminMaster.getName()%></td>
          <td><%=working_bltAdminMaster.getCity()%></td>
          <td> 
            <jsp:include page="../generic/tStateLILong_translate.jsp" flush="true" > 
            <jsp:param name="CurrentSelection" value="<%=working_bltAdminMaster.getStateID()%>" />
            </jsp:include>
          </td>
          <td> <%=working_bltAdminMaster.getZIP()%></td>
          <td> 
            <%if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
	        {%>
    	        <a class=linkBase href = "#" onClick="javascript:opener.tUserAccount_form1.ReferenceID.value=<%=working_bltCompanyAdminLU.getAdminID()%>;opener.tUserAccount_form1.AccountType.value='AdminID';opener.tUserAccount_form1.StartPage.value='pro-file-manager_Status.jsp';window.close()"><img border=0 src="ui_<%=thePLCID%>/icons/add_CompanyID.gif"></a> 
            <%}%>
     <%}%>
          </td>
        </tr>
      </table>
      <p> 
        <%
}
else if (whichOne==1)
{
//if Physician
//declaration of Enumeration
    bltCompanyAdminLU        working_bltCompanyAdminLU;
    ListElement         leCurrentElement;
    java.util.Enumeration eList = bltCompanyAdminLU_List_LU_CompanyID.elements();
        String theClass = "tdHeader";
        %>
      <p class=<%=theClass%>> 
        <%String theClassF = "textBase";%>
      </p>
      <table width="100%" border="1" bordercolor=CCCCCC cellspacing="0" cellpadding="0">
        <tr  class=tdHeader> 
          <td>ID</td>
          <td><b>Create Date</b></td>
          <td><b>Last Name</b></td>
          <td><b>First Name</b></td>
          <td><b>AdminID</b></td>
          <td><b>City</b></td>
          <td><b>ZIP</b>
          </td>
        </tr>
<%
java.util.Vector myAdminsV = new java.util.Vector();
    while (eList.hasMoreElements())
    {
        leCurrentElement    = (ListElement) eList.nextElement();
        working_bltCompanyAdminLU  = (bltCompanyAdminLU) leCurrentElement.getObject();
        myAdminsV.addElement(working_bltCompanyAdminLU.getAdminID());
    }
String newWhere = "";
if (myAdminsV!=null)
{
    newWhere = "AdminID="+(Integer)myAdminsV.elementAt(0) +" ";
    for (int i=1 ;i<myAdminsV.size();i++)
    {
        newWhere +="OR AdminID="+(Integer)myAdminsV.elementAt(i) +" ";
    }
}

searchDB2 mySDB = new searchDB2();
java.sql.ResultSet myRS = mySDB.executeStatement("select * from tAdminPhysicianLU where ("+newWhere+") order by PhysicianID");


    while (myRS!=null&&myRS.next())
    {
	Integer iPhys = new Integer(myRS.getString("PhysicianID"));
	Integer iAdm = new Integer(myRS.getString("AdminID"));
//        bltAdminMaster working_bltAdminMaster  = new bltAdminMaster(iAdm);
        bltPhysicianMaster working_bltPhysicianMaster  = new bltPhysicianMaster(iPhys);
%>
        <tr> 
          <td><%=working_bltPhysicianMaster.getPhysicianID()%></td>
          <td><%=displayDateSDF1.format(working_bltPhysicianMaster.getUniqueCreateDate())%>&nbsp;</td>
          <td><%=working_bltPhysicianMaster.getLastName()%>&nbsp;</td>
          <td><%=working_bltPhysicianMaster.getFirstName()%>&nbsp;</td>
          <td><%=iAdm%>&nbsp;</td>
          <td><%=working_bltPhysicianMaster.getHomeCity()%>&nbsp;</td>
          <td> <%=working_bltPhysicianMaster.getHomeZIP()%>&nbsp;</td>
          <td> 
            <%if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
	        {%>
    	        <a class=linkBase href = "#" onClick="javascript:opener.tUserAccount_form1.ReferenceID.value=<%=working_bltPhysicianMaster.getPhysicianID()%>;opener.tUserAccount_form1.AccountType.value='PhysicianID';opener.tUserAccount_form1.StartPage.value='pro-file_Status.jsp';window.close();"><img border=0 src="ui_<%=thePLCID%>/icons/add_CompanyID.gif"></a> 
            <%}%>
     <%}%>
          </td>
        </tr>
      </table>
      <p> 
        <%
}
else if (whichOne==2)
{
//if Practice
//declaration of Enumeration
    bltCompanyAdminLU        working_bltCompanyAdminLU;
    ListElement         leCurrentElement;
    java.util.Enumeration eList = bltCompanyAdminLU_List_LU_CompanyID.elements();
        String theClass = "tdHeader";
        %>
      <p class=<%=theClass%>> 
        <%String theClassF = "textBase";%>
      </p>
      <table width="100%" border="1" bordercolor=CCCCCC cellspacing="0" cellpadding="0">
        <tr  class=tdHeader> 
          <td>ID</td>
          <td><b>Create Date</b></td>
          <td><b>Name</b></td>
          <td><b>AdminID</b></td>
          <td><b>City</b></td>
          <td><b>ZIP</b>
          </td>
        </tr>
<%
java.util.Vector myAdminsV = new java.util.Vector();
    while (eList.hasMoreElements())
    {
        leCurrentElement    = (ListElement) eList.nextElement();
        working_bltCompanyAdminLU  = (bltCompanyAdminLU) leCurrentElement.getObject();
        myAdminsV.addElement(working_bltCompanyAdminLU.getAdminID());
    }
String newWhere = "";
if (myAdminsV!=null)
{
    newWhere = "AdminID="+(Integer)myAdminsV.elementAt(0) +" ";
    for (int i=1 ;i<myAdminsV.size();i++)
    {
        newWhere +="OR AdminID="+(Integer)myAdminsV.elementAt(i) +" ";
    }
}

searchDB2 mySDB = new searchDB2();
java.sql.ResultSet myRS = mySDB.executeStatement("select * from tAdminPracticeLU where ("+newWhere+") order by PracticeID");


    while (myRS!=null&&myRS.next())
    {
	Integer iPhys = new Integer(myRS.getString("PracticeID"));
	Integer iAdm = new Integer(myRS.getString("AdminID"));
//        bltAdminMaster working_bltAdminMaster  = new bltAdminMaster(iAdm);
        bltPracticeMaster working_bltPracticeMaster  = new bltPracticeMaster(iPhys);
%>
        <tr> 
          <td><%=working_bltPracticeMaster.getPracticeID()%></td>
          <td><%=displayDateSDF1.format(working_bltPracticeMaster.getUniqueCreateDate())%>&nbsp;</td>
          <td><%=working_bltPracticeMaster.getPracticeName()%>&nbsp;</td>
          <td><%=iAdm%>&nbsp;</td>
          <td><%=working_bltPracticeMaster.getOfficeCity()%>&nbsp;</td>
          <td> <%=working_bltPracticeMaster.getOfficeZIP()%>&nbsp;</td>
          <td> 
            <%if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
	        {%>
    	        <a class=linkBase href = "#" onClick="javascript:opener.tUserAccount_form1.ReferenceID.value=<%=working_bltPracticeMaster.getPracticeID()%>;opener.tUserAccount_form1.AccountType.value='PracticeID';opener.tUserAccount_form1.StartPage.value='practice_Status.jsp';window.close()"><img border=0 src="ui_<%=thePLCID%>/icons/add_CompanyID.gif"></a> 
            <%}%>
     <%}%>
          </td>
        </tr>
      </table>
      <p> 
        <%
}

else if (whichOne==3)
{
    bltCompanyHCOLU_List_LU_CompanyID        bltCompanyHCOLU_List_LU_CompanyID        =    new    bltCompanyHCOLU_List_LU_CompanyID(iCompanyID);
//if HCO
//declaration of Enumeration
    bltCompanyHCOLU        working_bltCompanyHCOLU;
    ListElement         leCurrentElement;
    java.util.Enumeration eList = bltCompanyHCOLU_List_LU_CompanyID.elements();

        String theClass = "tdHeader";
        %>
      <p class=<%=theClass%>> 
        <%String theClassF = "textBase";%>
      </p>
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr  class=tdHeader> 
          <td>ID</td>
          <td><b>Create Date</b></td>
          <td><b>Name</b></td>
          <td><b>City</b></td>
          <td><b>State</b></td>
          <td> 
            <p class=<%=theClassF%> ><b>ZIP</b></p>
          </td>
          <td>&nbsp;</td>
        </tr>
<%
    while (eList.hasMoreElements())
    {
        leCurrentElement    = (ListElement) eList.nextElement();
        working_bltCompanyHCOLU  = (bltCompanyHCOLU) leCurrentElement.getObject();
        bltHCOMaster working_bltHCOMaster  = new bltHCOMaster(working_bltCompanyHCOLU.getHCOID());
%>
        <tr> 
          <td><%=working_bltCompanyHCOLU.getHCOID()%></td>
          <td><%=displayDateSDF1.format(working_bltHCOMaster.getUniqueCreateDate())%></td>
          <td><%=working_bltHCOMaster.getName()%></td>
          <td><%=working_bltHCOMaster.getCity()%></td>
          <td> 
            <jsp:include page="../generic/tStateLILong_translate.jsp" flush="true" > 
            <jsp:param name="CurrentSelection" value="<%=working_bltHCOMaster.getStateID()%>" />
            </jsp:include>
          </td>
          <td> <%=working_bltHCOMaster.getZIP()%></td>
          <td> 
            <%if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
	        {%>
    	        <a class=linkBase href = "#" onClick="javascript:opener.tUserAccount_form1.ReferenceID.value=<%=working_bltCompanyHCOLU.getHCOID()%>;opener.tUserAccount_form1.AccountType.value='HCOID';opener.tUserAccount_form1.StartPage.value='pro-file_HCO-Status.jsp';window.close()"><img border=0 src="ui_<%=thePLCID%>/icons/add_CompanyID.gif"></a> 
            <%}%>
     <%}%>
          </td>
        </tr>
      </table>
      <p> 
        <%
}


  }
  else
  {
   response.sendRedirect("/errorhandler.jsp");
  }
%>
      </p>
      </td>
  </tr>
</table>
<%String bnIncludeFN = "ui_"+thePLCID+"\\bot-nav_PU.jsp";
%>
<jsp:include page="<%=bnIncludeFN%>" flush="true" > 
<jsp:param name="plcID" value="<%=thePLCID%>"/>
</jsp:include>
