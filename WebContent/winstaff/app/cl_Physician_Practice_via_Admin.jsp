<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.bltDocumentManagement,java.util.Vector,java.util.Enumeration,com.winstaff.ListElement, com.winstaff.PLCUtils, com.winstaff.SecurityCheck, com.winstaff.ConfigurationMessages,com.winstaff.bltAdminPhysicianLU_List_PhysicianMaster_AdminID,com.winstaff.bltPracticeMaster,com.winstaff.bltPhysicianMaster,com.winstaff.bltAdminMaster,com.winstaff.bltPhysicianPracticeLU,com.winstaff.bltAdminPhysicianLU" %>
<%/*
    filename: out\jsp\cl_Physician_Practice_via_Admin.jsp
    Created on Mar/21/2003
    Type: cl main class file
    Created by: Scott Ellis
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>

<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_PracticeID.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>

    <table cellpadding=0 cellspacing=0 border=0 width=<%=MasterTableWidth%> >
    <tr><td width=10>&nbsp;</td><td>

    <span class=title>Step 1 - Identifying Information</span><br>


<%
//initial declaration of list class and parentID
    Integer        iPracticeID        =    null;
    Integer        iAdminID        =    null;
    boolean accessValid = false;
   Integer iSecurityCheck = SecurityCheck.CheckItem("Company1", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {
    if ((pageControllerHash.containsKey("iPracticeID"))&&(pageControllerHash.containsKey("iAdminID"))) 
    {
        iPracticeID        =    (Integer)pageControllerHash.get("iPracticeID");
        iAdminID        =    (Integer)pageControllerHash.get("iAdminID");
        accessValid = true;
    }
  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);
      java.text.SimpleDateFormat displayDateLongSDF = new java.text.SimpleDateFormat("MMMM dd yyyy");





    session.setAttribute("pageControllerHash",pageControllerHash);
    bltAdminPhysicianLU_List_PhysicianMaster_AdminID         bltAdminPhysicianLU_List_PhysicianMaster_AdminID        =    new    bltAdminPhysicianLU_List_PhysicianMaster_AdminID(iAdminID);

//declaration of Enumeration
    bltPhysicianMaster        working_bltPhysicianMaster;
    ListElement         leCurrentElement;
    java.util.Enumeration eList = bltAdminPhysicianLU_List_PhysicianMaster_AdminID.elements();
    %>
        <%@ include file="cl_Physician_Practice_via_Admin_instructions.jsp" %>
    <%
    while (eList.hasMoreElements())
    {
         %>
         <table border="0" bordercolor="333333" cellpadding="3" class=tdHeaderAlt cellspacing="0" width="100%">
            <tr>
              <td width="50%"> 
         <%

        leCurrentElement    = (ListElement) eList.nextElement();
        working_bltPhysicianMaster  = (bltPhysicianMaster) leCurrentElement.getObject();



        String theClass = "tdHeader";
        if (!working_bltPhysicianMaster.isComplete())
        {
            theClass = "incompleteItem";
        %>
                <span class=incompleteItem><b>This item is incomplete</b></span><br>
        <%
        }
        %>
        <span class=<%=theClass%>><b>Item ID:&nbsp;</b>[<i>PhysicianID</i>]<%=working_bltPhysicianMaster.getPhysicianID()%></span>
        </td><td width="50%"> 

        <%if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {%>
        <p><a class=linkBase href = "cl_Physician_Practice_via_Admin_create.jsp?EDITID=<%=working_bltPhysicianMaster.getPhysicianID()%>"><img border=0 src="ui_<%=thePLCID%>/icons/add_PracticeID.gif"></a></p>
        <%}%>
                  </td></tr>
                  <tr>
                  <td class=tdBase width="50%"><b>Item Create Date:&nbsp;</b><jsp:include page="../generic/DateTypeConvert_Main.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(working_bltPhysicianMaster.getUniqueCreateDate())%>" /></jsp:include>
                  </td>
                  <td class=tdBase width="50%"><b>Item Modify Date:&nbsp;</b><jsp:include page="../generic/DateTypeConvert_Main.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(working_bltPhysicianMaster.getUniqueModifyDate())%>" /></jsp:include>
                  </td>
                  </tr>
                  <tr>
                  <td class=tdBase width="50%"><b>Item Modify Comments:&nbsp;</b><%=working_bltPhysicianMaster.getUniqueModifyComments()%>
                  </td>
                  <td class=tdBase width="50%">&nbsp;
                  </td>
                  </tr>
                 </table>
                 <table width="100%" border="1" cellspacing="0" bordercolor="#333333">
                  <tr>
                   <td>
                     <table width="100%" border="0" cellspacing="0">
                     <tr><td>




<%String theClassF = "textBase";%>
<%theClassF = "textBase";%>
<%if ((working_bltPhysicianMaster.isExpired("Salutation",expiredDays))&&(working_bltPhysicianMaster.isExpiredCheck("Salutation"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPhysicianMaster.isRequired("Salutation"))&&(!working_bltPhysicianMaster.isComplete("Salutation")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Salutation</b><jsp:include page="../generic/tSalutationLIShort_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltPhysicianMaster.getSalutation()%>" /></jsp:include></p>

<%theClassF = "textBase";%>
<%if ((working_bltPhysicianMaster.isExpired("Title",expiredDays))&&(working_bltPhysicianMaster.isExpiredCheck("Title"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPhysicianMaster.isRequired("Title"))&&(!working_bltPhysicianMaster.isComplete("Title")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Title</b><%=working_bltPhysicianMaster.getTitle()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltPhysicianMaster.isExpired("FirstName",expiredDays))&&(working_bltPhysicianMaster.isExpiredCheck("FirstName"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPhysicianMaster.isRequired("FirstName"))&&(!working_bltPhysicianMaster.isComplete("FirstName")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>First Name</b><%=working_bltPhysicianMaster.getFirstName()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltPhysicianMaster.isExpired("MiddleName",expiredDays))&&(working_bltPhysicianMaster.isExpiredCheck("MiddleName"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPhysicianMaster.isRequired("MiddleName"))&&(!working_bltPhysicianMaster.isComplete("MiddleName")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Middle Name</b><%=working_bltPhysicianMaster.getMiddleName()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltPhysicianMaster.isExpired("LastName",expiredDays))&&(working_bltPhysicianMaster.isExpiredCheck("LastName"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPhysicianMaster.isRequired("LastName"))&&(!working_bltPhysicianMaster.isComplete("LastName")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Last Name</b><%=working_bltPhysicianMaster.getLastName()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltPhysicianMaster.isExpired("Suffix",expiredDays))&&(working_bltPhysicianMaster.isExpiredCheck("Suffix"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPhysicianMaster.isRequired("Suffix"))&&(!working_bltPhysicianMaster.isComplete("Suffix")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Suffix</b><%=working_bltPhysicianMaster.getSuffix()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltPhysicianMaster.isExpired("HomeEmail",expiredDays))&&(working_bltPhysicianMaster.isExpiredCheck("HomeEmail"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPhysicianMaster.isRequired("HomeEmail"))&&(!working_bltPhysicianMaster.isComplete("HomeEmail")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Email Address</b><%=working_bltPhysicianMaster.getHomeEmail()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltPhysicianMaster.isExpired("HomeAddress1",expiredDays))&&(working_bltPhysicianMaster.isExpiredCheck("HomeAddress1"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPhysicianMaster.isRequired("HomeAddress1"))&&(!working_bltPhysicianMaster.isComplete("HomeAddress1")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Home Mailing Address</b><%=working_bltPhysicianMaster.getHomeAddress1()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltPhysicianMaster.isExpired("HomeAddress2",expiredDays))&&(working_bltPhysicianMaster.isExpiredCheck("HomeAddress2"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPhysicianMaster.isRequired("HomeAddress2"))&&(!working_bltPhysicianMaster.isComplete("HomeAddress2")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Address 2</b><%=working_bltPhysicianMaster.getHomeAddress2()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltPhysicianMaster.isExpired("HomeCity",expiredDays))&&(working_bltPhysicianMaster.isExpiredCheck("HomeCity"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPhysicianMaster.isRequired("HomeCity"))&&(!working_bltPhysicianMaster.isComplete("HomeCity")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>City,Town,Province</b><%=working_bltPhysicianMaster.getHomeCity()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltPhysicianMaster.isExpired("HomeStateID",expiredDays))&&(working_bltPhysicianMaster.isExpiredCheck("HomeStateID"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPhysicianMaster.isRequired("HomeStateID"))&&(!working_bltPhysicianMaster.isComplete("HomeStateID")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>State</b><jsp:include page="../generic/tStateLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltPhysicianMaster.getHomeStateID()%>" /></jsp:include></p>

<%theClassF = "textBase";%>
<%if ((working_bltPhysicianMaster.isExpired("HomeProvince",expiredDays))&&(working_bltPhysicianMaster.isExpiredCheck("HomeProvince"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPhysicianMaster.isRequired("HomeProvince"))&&(!working_bltPhysicianMaster.isComplete("HomeProvince")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Province, District, State</b><%=working_bltPhysicianMaster.getHomeProvince()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltPhysicianMaster.isExpired("HomeZIP",expiredDays))&&(working_bltPhysicianMaster.isExpiredCheck("HomeZIP"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPhysicianMaster.isRequired("HomeZIP"))&&(!working_bltPhysicianMaster.isComplete("HomeZIP")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>ZIP</b><%=working_bltPhysicianMaster.getHomeZIP()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltPhysicianMaster.isExpired("HomeCountryID",expiredDays))&&(working_bltPhysicianMaster.isExpiredCheck("HomeCountryID"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPhysicianMaster.isRequired("HomeCountryID"))&&(!working_bltPhysicianMaster.isComplete("HomeCountryID")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Country</b><jsp:include page="../generic/tCountryLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltPhysicianMaster.getHomeCountryID()%>" /></jsp:include></p>

<%theClassF = "textBase";%>
<%if ((working_bltPhysicianMaster.isExpired("HomePhone",expiredDays))&&(working_bltPhysicianMaster.isExpiredCheck("HomePhone"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPhysicianMaster.isRequired("HomePhone"))&&(!working_bltPhysicianMaster.isComplete("HomePhone")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Home Phone Number (XXX-XXX-XXXX)</b><%=working_bltPhysicianMaster.getHomePhone()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltPhysicianMaster.isExpired("HomeFax",expiredDays))&&(working_bltPhysicianMaster.isExpiredCheck("HomeFax"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPhysicianMaster.isRequired("HomeFax"))&&(!working_bltPhysicianMaster.isComplete("HomeFax")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>HomeFax (XXX-XXX-XXXX)</b><%=working_bltPhysicianMaster.getHomeFax()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltPhysicianMaster.isExpired("HomeMobile",expiredDays))&&(working_bltPhysicianMaster.isExpiredCheck("HomeMobile"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPhysicianMaster.isRequired("HomeMobile"))&&(!working_bltPhysicianMaster.isComplete("HomeMobile")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Mobile Phone Number (XXX-XXX-XXXX)</b><%=working_bltPhysicianMaster.getHomeMobile()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltPhysicianMaster.isExpired("HomePager",expiredDays))&&(working_bltPhysicianMaster.isExpiredCheck("HomePager"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPhysicianMaster.isRequired("HomePager"))&&(!working_bltPhysicianMaster.isComplete("HomePager")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Pager Number (XXX-XXX-XXXX)</b><%=working_bltPhysicianMaster.getHomePager()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltPhysicianMaster.isExpired("AlertEmail",expiredDays))&&(working_bltPhysicianMaster.isExpiredCheck("AlertEmail"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPhysicianMaster.isRequired("AlertEmail"))&&(!working_bltPhysicianMaster.isComplete("AlertEmail")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Alert Email</b><%=working_bltPhysicianMaster.getAlertEmail()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltPhysicianMaster.isExpired("AlertDays",expiredDays))&&(working_bltPhysicianMaster.isExpiredCheck("AlertDays"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPhysicianMaster.isRequired("AlertDays"))&&(!working_bltPhysicianMaster.isComplete("AlertDays")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Alert Days</b><%=working_bltPhysicianMaster.getAlertDays()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltPhysicianMaster.isExpired("OtherName1",expiredDays))&&(working_bltPhysicianMaster.isExpiredCheck("OtherName1"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPhysicianMaster.isRequired("OtherName1"))&&(!working_bltPhysicianMaster.isComplete("OtherName1")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Other Name Used #1</b><%=working_bltPhysicianMaster.getOtherName1()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltPhysicianMaster.isExpired("OtherName1Start",expiredDays))&&(working_bltPhysicianMaster.isExpiredCheck("OtherName1Start"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPhysicianMaster.isRequired("OtherName1Start"))&&(!working_bltPhysicianMaster.isComplete("OtherName1Start")) ){theClassF = "requiredFieldMain";}%>

                        <p class=<%=theClassF%> ><b>Other name Used #1 From Date</b><jsp:include page="../generic/DateTypeConvert_Main.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(working_bltPhysicianMaster.getOtherName1Start())%>" /></jsp:include></p>


<%theClassF = "textBase";%>
<%if ((working_bltPhysicianMaster.isExpired("OtherName1End",expiredDays))&&(working_bltPhysicianMaster.isExpiredCheck("OtherName1End"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPhysicianMaster.isRequired("OtherName1End"))&&(!working_bltPhysicianMaster.isComplete("OtherName1End")) ){theClassF = "requiredFieldMain";}%>

                        <p class=<%=theClassF%> ><b>Other name Used #1 To Date</b><jsp:include page="../generic/DateTypeConvert_Main.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(working_bltPhysicianMaster.getOtherName1End())%>" /></jsp:include></p>


<%theClassF = "textBase";%>
<%if ((working_bltPhysicianMaster.isExpired("OtherName2",expiredDays))&&(working_bltPhysicianMaster.isExpiredCheck("OtherName2"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPhysicianMaster.isRequired("OtherName2"))&&(!working_bltPhysicianMaster.isComplete("OtherName2")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Other Name Used #2</b><%=working_bltPhysicianMaster.getOtherName2()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltPhysicianMaster.isExpired("OtherName2Start",expiredDays))&&(working_bltPhysicianMaster.isExpiredCheck("OtherName2Start"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPhysicianMaster.isRequired("OtherName2Start"))&&(!working_bltPhysicianMaster.isComplete("OtherName2Start")) ){theClassF = "requiredFieldMain";}%>

                        <p class=<%=theClassF%> ><b>Other name Used #2 From Date</b><jsp:include page="../generic/DateTypeConvert_Main.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(working_bltPhysicianMaster.getOtherName2Start())%>" /></jsp:include></p>


<%theClassF = "textBase";%>
<%if ((working_bltPhysicianMaster.isExpired("OtherName2End",expiredDays))&&(working_bltPhysicianMaster.isExpiredCheck("OtherName2End"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPhysicianMaster.isRequired("OtherName2End"))&&(!working_bltPhysicianMaster.isComplete("OtherName2End")) ){theClassF = "requiredFieldMain";}%>

                        <p class=<%=theClassF%> ><b>Other name Used #2 To Date</b><jsp:include page="../generic/DateTypeConvert_Main.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(working_bltPhysicianMaster.getOtherName2End())%>" /></jsp:include></p>


<%theClassF = "textBase";%>
<%if ((working_bltPhysicianMaster.isExpired("DateOfBirth",expiredDays))&&(working_bltPhysicianMaster.isExpiredCheck("DateOfBirth"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPhysicianMaster.isRequired("DateOfBirth"))&&(!working_bltPhysicianMaster.isComplete("DateOfBirth")) ){theClassF = "requiredFieldMain";}%>

                        <p class=<%=theClassF%> ><b>Date of Birth</b><jsp:include page="../generic/DateTypeConvert_Main.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(working_bltPhysicianMaster.getDateOfBirth())%>" /></jsp:include></p>


<%theClassF = "textBase";%>
<%if ((working_bltPhysicianMaster.isExpired("PlaceOfBirth",expiredDays))&&(working_bltPhysicianMaster.isExpiredCheck("PlaceOfBirth"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPhysicianMaster.isRequired("PlaceOfBirth"))&&(!working_bltPhysicianMaster.isComplete("PlaceOfBirth")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Place of Birth</b><%=working_bltPhysicianMaster.getPlaceOfBirth()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltPhysicianMaster.isExpired("Spouse",expiredDays))&&(working_bltPhysicianMaster.isExpiredCheck("Spouse"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPhysicianMaster.isRequired("Spouse"))&&(!working_bltPhysicianMaster.isComplete("Spouse")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Spouse Full Name (First Middle Last)</b><%=working_bltPhysicianMaster.getSpouse()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltPhysicianMaster.isExpired("CitizenshipYN",expiredDays))&&(working_bltPhysicianMaster.isExpiredCheck("CitizenshipYN"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPhysicianMaster.isRequired("CitizenshipYN"))&&(!working_bltPhysicianMaster.isComplete("CitizenshipYN")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Are you a US Citizen?</b><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltPhysicianMaster.getCitizenshipYN()%>" /></jsp:include></p>

<%theClassF = "textBase";%>
<%if ((working_bltPhysicianMaster.isExpired("Citizenship",expiredDays))&&(working_bltPhysicianMaster.isExpiredCheck("Citizenship"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPhysicianMaster.isRequired("Citizenship"))&&(!working_bltPhysicianMaster.isComplete("Citizenship")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>If not, list your citizenship</b><%=working_bltPhysicianMaster.getCitizenship()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltPhysicianMaster.isExpired("VisaNumber",expiredDays))&&(working_bltPhysicianMaster.isExpiredCheck("VisaNumber"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPhysicianMaster.isRequired("VisaNumber"))&&(!working_bltPhysicianMaster.isComplete("VisaNumber")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Visa Number</b><%=working_bltPhysicianMaster.getVisaNumber()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltPhysicianMaster.isExpired("VisaStatus",expiredDays))&&(working_bltPhysicianMaster.isExpiredCheck("VisaStatus"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPhysicianMaster.isRequired("VisaStatus"))&&(!working_bltPhysicianMaster.isComplete("VisaStatus")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Visa Status</b><%=working_bltPhysicianMaster.getVisaStatus()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltPhysicianMaster.isExpired("EligibleToWorkInUS",expiredDays))&&(working_bltPhysicianMaster.isExpiredCheck("EligibleToWorkInUS"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPhysicianMaster.isRequired("EligibleToWorkInUS"))&&(!working_bltPhysicianMaster.isComplete("EligibleToWorkInUS")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Are you eligible to work in the US?</b><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltPhysicianMaster.getEligibleToWorkInUS()%>" /></jsp:include></p>

<%theClassF = "textBase";%>
<%if ((working_bltPhysicianMaster.isExpired("SSN",expiredDays))&&(working_bltPhysicianMaster.isExpiredCheck("SSN"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPhysicianMaster.isRequired("SSN"))&&(!working_bltPhysicianMaster.isComplete("SSN")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Social Security Number</b><%=working_bltPhysicianMaster.getSSN()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltPhysicianMaster.isExpired("Gender",expiredDays))&&(working_bltPhysicianMaster.isExpiredCheck("Gender"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPhysicianMaster.isRequired("Gender"))&&(!working_bltPhysicianMaster.isComplete("Gender")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Gender</b><jsp:include page="../generic/tGenderLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltPhysicianMaster.getGender()%>" /></jsp:include></p>

<%theClassF = "textBase";%>
<%if ((working_bltPhysicianMaster.isExpired("MilitaryActive",expiredDays))&&(working_bltPhysicianMaster.isExpiredCheck("MilitaryActive"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPhysicianMaster.isRequired("MilitaryActive"))&&(!working_bltPhysicianMaster.isComplete("MilitaryActive")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Are you currently active in the Military?</b><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltPhysicianMaster.getMilitaryActive()%>" /></jsp:include></p>

<%theClassF = "textBase";%>
<%if ((working_bltPhysicianMaster.isExpired("MilitaryBranch",expiredDays))&&(working_bltPhysicianMaster.isExpiredCheck("MilitaryBranch"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPhysicianMaster.isRequired("MilitaryBranch"))&&(!working_bltPhysicianMaster.isComplete("MilitaryBranch")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>If yes, which branch?</b><%=working_bltPhysicianMaster.getMilitaryBranch()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltPhysicianMaster.isExpired("MilitaryReserve",expiredDays))&&(working_bltPhysicianMaster.isExpiredCheck("MilitaryReserve"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPhysicianMaster.isRequired("MilitaryReserve"))&&(!working_bltPhysicianMaster.isComplete("MilitaryReserve")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Are you currently in the Military Reserves?</b><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltPhysicianMaster.getMilitaryReserve()%>" /></jsp:include></p>

<%theClassF = "textBase";%>
<%if ((working_bltPhysicianMaster.isExpired("HospitalAdmitingPrivileges",expiredDays))&&(working_bltPhysicianMaster.isExpiredCheck("HospitalAdmitingPrivileges"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPhysicianMaster.isRequired("HospitalAdmitingPrivileges"))&&(!working_bltPhysicianMaster.isComplete("HospitalAdmitingPrivileges")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Do you have hospital admiting privileges?</b><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltPhysicianMaster.getHospitalAdmitingPrivileges()%>" /></jsp:include></p>

<%theClassF = "textBase";%>
<%if ((working_bltPhysicianMaster.isExpired("HospitalAdmitingPrivilegesNo",expiredDays))&&(working_bltPhysicianMaster.isExpiredCheck("HospitalAdmitingPrivilegesNo"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPhysicianMaster.isRequired("HospitalAdmitingPrivilegesNo"))&&(!working_bltPhysicianMaster.isComplete("HospitalAdmitingPrivilegesNo")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>If no, what admiting arrangements do you have?</b><%=working_bltPhysicianMaster.getHospitalAdmitingPrivilegesNo()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltPhysicianMaster.isExpired("PhysicianCategoryID",expiredDays))&&(working_bltPhysicianMaster.isExpiredCheck("PhysicianCategoryID"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPhysicianMaster.isRequired("PhysicianCategoryID"))&&(!working_bltPhysicianMaster.isComplete("PhysicianCategoryID")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Physician Category</b><jsp:include page="../generic/tPhysicianCategoryLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltPhysicianMaster.getPhysicianCategoryID()%>" /></jsp:include></p>

<%theClassF = "textBase";%>
<%if ((working_bltPhysicianMaster.isExpired("IPAMedicalAffiliation",expiredDays))&&(working_bltPhysicianMaster.isExpiredCheck("IPAMedicalAffiliation"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPhysicianMaster.isRequired("IPAMedicalAffiliation"))&&(!working_bltPhysicianMaster.isComplete("IPAMedicalAffiliation")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Are you currently affiliated with any other IPAs or Medical Groups?</b><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltPhysicianMaster.getIPAMedicalAffiliation()%>" /></jsp:include></p>

<%theClassF = "textBase";%>
<%if ((working_bltPhysicianMaster.isExpired("AffiliationDesc1",expiredDays))&&(working_bltPhysicianMaster.isExpiredCheck("AffiliationDesc1"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPhysicianMaster.isRequired("AffiliationDesc1"))&&(!working_bltPhysicianMaster.isComplete("AffiliationDesc1")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>If YES, please list below</b><%=working_bltPhysicianMaster.getAffiliationDesc1()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltPhysicianMaster.isExpired("PhysicianLanguages",expiredDays))&&(working_bltPhysicianMaster.isExpiredCheck("PhysicianLanguages"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPhysicianMaster.isRequired("PhysicianLanguages"))&&(!working_bltPhysicianMaster.isComplete("PhysicianLanguages")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Please list any languages that you speak (other than English)</b><%=working_bltPhysicianMaster.getPhysicianLanguages()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltPhysicianMaster.isExpired("ECFMGNo",expiredDays))&&(working_bltPhysicianMaster.isExpiredCheck("ECFMGNo"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPhysicianMaster.isRequired("ECFMGNo"))&&(!working_bltPhysicianMaster.isComplete("ECFMGNo")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>ECFMG Number (if applicable)</b><%=working_bltPhysicianMaster.getECFMGNo()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltPhysicianMaster.isExpired("ECFMGDateIssued",expiredDays))&&(working_bltPhysicianMaster.isExpiredCheck("ECFMGDateIssued"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPhysicianMaster.isRequired("ECFMGDateIssued"))&&(!working_bltPhysicianMaster.isComplete("ECFMGDateIssued")) ){theClassF = "requiredFieldMain";}%>

                        <p class=<%=theClassF%> ><b>ECFMG Issue Date</b><jsp:include page="../generic/DateTypeConvert_Main.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(working_bltPhysicianMaster.getECFMGDateIssued())%>" /></jsp:include></p>


<%theClassF = "textBase";%>
<%if ((working_bltPhysicianMaster.isExpired("ECFMGDateExpires",expiredDays))&&(working_bltPhysicianMaster.isExpiredCheck("ECFMGDateExpires"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPhysicianMaster.isRequired("ECFMGDateExpires"))&&(!working_bltPhysicianMaster.isComplete("ECFMGDateExpires")) ){theClassF = "requiredFieldMain";}%>

                        <p class=<%=theClassF%> ><b>ECFMG Expiration Date</b><jsp:include page="../generic/DateTypeConvert_Main.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(working_bltPhysicianMaster.getECFMGDateExpires())%>" /></jsp:include></p>


<%theClassF = "textBase";%>
<%if ((working_bltPhysicianMaster.isExpired("MedicareUPIN",expiredDays))&&(working_bltPhysicianMaster.isExpiredCheck("MedicareUPIN"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPhysicianMaster.isRequired("MedicareUPIN"))&&(!working_bltPhysicianMaster.isComplete("MedicareUPIN")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Medicare UPIN</b><%=working_bltPhysicianMaster.getMedicareUPIN()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltPhysicianMaster.isExpired("UniqueNPI",expiredDays))&&(working_bltPhysicianMaster.isExpiredCheck("UniqueNPI"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPhysicianMaster.isRequired("UniqueNPI"))&&(!working_bltPhysicianMaster.isComplete("UniqueNPI")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Unique National Provider Number (NPI) (if applicable)</b><%=working_bltPhysicianMaster.getUniqueNPI()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltPhysicianMaster.isExpired("MedicareParticipation",expiredDays))&&(working_bltPhysicianMaster.isExpiredCheck("MedicareParticipation"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPhysicianMaster.isRequired("MedicareParticipation"))&&(!working_bltPhysicianMaster.isComplete("MedicareParticipation")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Are you a Participating Medicare Provider?</b><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltPhysicianMaster.getMedicareParticipation()%>" /></jsp:include></p>

<%theClassF = "textBase";%>
<%if ((working_bltPhysicianMaster.isExpired("MedicareNo",expiredDays))&&(working_bltPhysicianMaster.isExpiredCheck("MedicareNo"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPhysicianMaster.isRequired("MedicareNo"))&&(!working_bltPhysicianMaster.isComplete("MedicareNo")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Medicare Provider Number(s)</b><%=working_bltPhysicianMaster.getMedicareNo()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltPhysicianMaster.isExpired("MediCaidParticipation",expiredDays))&&(working_bltPhysicianMaster.isExpiredCheck("MediCaidParticipation"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPhysicianMaster.isRequired("MediCaidParticipation"))&&(!working_bltPhysicianMaster.isComplete("MediCaidParticipation")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Are you a Participating Medicaid Provider?</b><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltPhysicianMaster.getMediCaidParticipation()%>" /></jsp:include></p>

<%theClassF = "textBase";%>
<%if ((working_bltPhysicianMaster.isExpired("MediCaidNo",expiredDays))&&(working_bltPhysicianMaster.isExpiredCheck("MediCaidNo"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPhysicianMaster.isRequired("MediCaidNo"))&&(!working_bltPhysicianMaster.isComplete("MediCaidNo")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Medicaid Provider Number(s)</b><%=working_bltPhysicianMaster.getMediCaidNo()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltPhysicianMaster.isExpired("SupplementalIDNumber1",expiredDays))&&(working_bltPhysicianMaster.isExpiredCheck("SupplementalIDNumber1"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPhysicianMaster.isRequired("SupplementalIDNumber1"))&&(!working_bltPhysicianMaster.isComplete("SupplementalIDNumber1")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Supplemental ID #1</b><%=working_bltPhysicianMaster.getSupplementalIDNumber1()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltPhysicianMaster.isExpired("SupplementalIDNumber2",expiredDays))&&(working_bltPhysicianMaster.isExpiredCheck("SupplementalIDNumber2"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPhysicianMaster.isRequired("SupplementalIDNumber2"))&&(!working_bltPhysicianMaster.isComplete("SupplementalIDNumber2")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Supplemental ID #2</b><%=working_bltPhysicianMaster.getSupplementalIDNumber2()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltPhysicianMaster.isExpired("Comments",expiredDays))&&(working_bltPhysicianMaster.isExpiredCheck("Comments"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPhysicianMaster.isRequired("Comments"))&&(!working_bltPhysicianMaster.isComplete("Comments")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Comments</b><%=working_bltPhysicianMaster.getComments()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltPhysicianMaster.isExpired("AttestConsent",expiredDays))&&(working_bltPhysicianMaster.isExpiredCheck("AttestConsent"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPhysicianMaster.isRequired("AttestConsent"))&&(!working_bltPhysicianMaster.isComplete("AttestConsent")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>AttestConsent</b><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltPhysicianMaster.getAttestConsent()%>" /></jsp:include></p>

<%theClassF = "textBase";%>
<%if ((working_bltPhysicianMaster.isExpired("AttestationComments",expiredDays))&&(working_bltPhysicianMaster.isExpiredCheck("AttestationComments"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPhysicianMaster.isRequired("AttestationComments"))&&(!working_bltPhysicianMaster.isComplete("AttestationComments")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>AttestationComments</b><%=working_bltPhysicianMaster.getAttestationComments()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltPhysicianMaster.isExpired("IsViewable",expiredDays))&&(working_bltPhysicianMaster.isExpiredCheck("IsViewable"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPhysicianMaster.isRequired("IsViewable"))&&(!working_bltPhysicianMaster.isComplete("IsViewable")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>IsViewable</b><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltPhysicianMaster.getIsViewable()%>" /></jsp:include></p>

<%theClassF = "textBase";%>
<%if ((working_bltPhysicianMaster.isExpired("IsAttested",expiredDays))&&(working_bltPhysicianMaster.isExpiredCheck("IsAttested"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPhysicianMaster.isRequired("IsAttested"))&&(!working_bltPhysicianMaster.isComplete("IsAttested")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>IsAttested</b><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltPhysicianMaster.getIsAttested()%>" /></jsp:include></p>

<%theClassF = "textBase";%>
<%if ((working_bltPhysicianMaster.isExpired("AttestDatePending",expiredDays))&&(working_bltPhysicianMaster.isExpiredCheck("AttestDatePending"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPhysicianMaster.isRequired("AttestDatePending"))&&(!working_bltPhysicianMaster.isComplete("AttestDatePending")) ){theClassF = "requiredFieldMain";}%>

                        <p class=<%=theClassF%> ><b>AttestDatePending</b><jsp:include page="../generic/DateTypeConvert_Main.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(working_bltPhysicianMaster.getAttestDatePending())%>" /></jsp:include></p>


<%theClassF = "textBase";%>
<%if ((working_bltPhysicianMaster.isExpired("IsAttestedPending",expiredDays))&&(working_bltPhysicianMaster.isExpiredCheck("IsAttestedPending"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPhysicianMaster.isRequired("IsAttestedPending"))&&(!working_bltPhysicianMaster.isComplete("IsAttestedPending")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>IsAttestedPending</b><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltPhysicianMaster.getIsAttestedPending()%>" /></jsp:include></p>

<%theClassF = "textBase";%>
<%if ((working_bltPhysicianMaster.isExpired("AttestDate",expiredDays))&&(working_bltPhysicianMaster.isExpiredCheck("AttestDate"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPhysicianMaster.isRequired("AttestDate"))&&(!working_bltPhysicianMaster.isComplete("AttestDate")) ){theClassF = "requiredFieldMain";}%>

                        <p class=<%=theClassF%> ><b>AttestDate</b><jsp:include page="../generic/DateTypeConvert_Main.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(working_bltPhysicianMaster.getAttestDate())%>" /></jsp:include></p>


<%theClassF = "textBase";%>
<%if ((working_bltPhysicianMaster.isExpired("DeAttestDate",expiredDays))&&(working_bltPhysicianMaster.isExpiredCheck("DeAttestDate"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPhysicianMaster.isRequired("DeAttestDate"))&&(!working_bltPhysicianMaster.isComplete("DeAttestDate")) ){theClassF = "requiredFieldMain";}%>

                        <p class=<%=theClassF%> ><b>DeAttestDate</b><jsp:include page="../generic/DateTypeConvert_Main.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(working_bltPhysicianMaster.getDeAttestDate())%>" /></jsp:include></p>


<%theClassF = "textBase";%>
<%if ((working_bltPhysicianMaster.isExpired("LastModifiedDate",expiredDays))&&(working_bltPhysicianMaster.isExpiredCheck("LastModifiedDate"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPhysicianMaster.isRequired("LastModifiedDate"))&&(!working_bltPhysicianMaster.isComplete("LastModifiedDate")) ){theClassF = "requiredFieldMain";}%>

                        <p class=<%=theClassF%> ><b>LastModifiedDate</b><jsp:include page="../generic/DateTypeConvert_Main.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(working_bltPhysicianMaster.getLastModifiedDate())%>" /></jsp:include></p>


<%theClassF = "textBase";%>
<%if ((working_bltPhysicianMaster.isExpired("QuestionValidation",expiredDays))&&(working_bltPhysicianMaster.isExpiredCheck("QuestionValidation"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPhysicianMaster.isRequired("QuestionValidation"))&&(!working_bltPhysicianMaster.isComplete("QuestionValidation")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>QuestionValidation</b><%=working_bltPhysicianMaster.getQuestionValidation()%></p>

        </td></tr></table></table><br>        <%
    }
    %>

    </table>    <%

  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>

    </td></tr></table>


<%String bnIncludeFN = "ui_"+thePLCID+"\\bot-nav_PracticeID.jsp";
%>
<jsp:include page="<%=bnIncludeFN%>" flush="true" ><jsp:param name="plcID" value="<%=thePLCID%>"/></jsp:include>
