<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.DataControlUtils,com.winstaff.bltAdminMaster,com.winstaff.searchDB2, com.winstaff.bltStateLI,java.util.Vector,java.util.Enumeration,com.winstaff.ListElement, com.winstaff.PLCUtils,com.winstaff.bltAdminPracticeLU,com.winstaff.bltAdminPracticeLU_List_LU_AdminID" %>
<%/*
    filename: tAdminPracticeLU_main_LU_AdminID.jsp
    Created on Apr/23/2002
    Type: 1-n main class file
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>

<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_Clear.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>

    <table cellpadding=0 cellspacing=0 border=0 width=100%>
    <tr><td width=10>&nbsp;</td>
    <td>
    <span class=title>Unlinked Admins</span><br />
Note: Only the first 200 are shown.


    <%
//initial declaration of list class and parentID
    Integer        iGenAdminID        =    null;
    boolean accessValid = false;
    if (pageControllerHash.containsKey("iGenAdminID")) 
    {
        iGenAdminID        =    (Integer)pageControllerHash.get("iGenAdminID");
        accessValid = true;
    }
  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);
      pageControllerHash.put("sParentReturnPage","Admin_query.jsp?AdminID=&Name=&City=&RelationshipTypeID=-1&StateID=0&Zip=&orderBy=AdminID&startID=0&maxResults=50&Submit2=Submit");

%>
    <input type=button value="Create New Admin" class=tdBaseAltGreen onClick="document.location='Admin_GenAdminID_create.jsp';" />&nbsp;&nbsp;&nbsp;&nbsp;
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr> 
    <td> 
<%
try
{

String myAdminID = "";
String myName = "";
String myCity = "";
String myStateID = "0";
String myZip = "";
String orderBy = "firstname";
int startID = 0;
int maxResults = 50;

boolean firstDisplay = false;

try
{
	maxResults = Integer.parseInt(request.getParameter("maxResults"));
	startID = Integer.parseInt(request.getParameter("startID"));
	myAdminID = request.getParameter("AdminID");
	myName = request.getParameter("Name");
	myCity = request.getParameter("City");
	myStateID = request.getParameter("StateID");
	myZip = request.getParameter("Zip");
	orderBy = request.getParameter("orderBy");
}
catch (Exception e)
{
	maxResults = 50;
	startID = 0;
	firstDisplay = true;
	myAdminID = "";
	myName = "";
	myCity = "";
	myStateID = "0";
	myZip = "";
	orderBy = "AdminID";
}
if (orderBy == null)
{
	maxResults = 50;
	startID = 0;
	firstDisplay = true;
	myAdminID = "";
	myName = "";
	myCity = "";
	myStateID = "0";
	myZip = "";
	orderBy = "AdminID";
}


firstDisplay = false;
if (firstDisplay)
{
}
else
{



%>

  <script language="JavaScript">
<!--
function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}
//-->
</script>
</p>
<table width=100% border=0 cellpadding=0 cellspacing=0 bordercolor=#003333>
<tr>
<td>
<table width=100% border=1 cellpadding=2 cellspacing=0 bordercolor=#003333>
<tr>
<td>

<form name="selfForm" method="get" action="Admin_query.jsp">
  <table border=0 cellspacing=1 width='100%' cellpadding=2>
    <tr > 
      <td colspan=3 class=tdHeader><p>Enter Search Criteria:</p></td>
      <td colspan=2 class=tdBase align=right>
       
    <input type=hidden name=maxResults value=<%=maxResults%> >
        <input type=hidden name=startID value=0 ><p>Sort:
          <select name=orderBy>
            <option value=AdminID <%if (orderBy.equalsIgnoreCase("AdminID")){out.println(" selected");}%> >ID</option>
            <option value=Name <%if (orderBy.equalsIgnoreCase("Name")){out.println(" selected");}%> >Admin Name</option>
            <option value=City <%if (orderBy.equalsIgnoreCase("City")){out.println(" selected");}%> >City</option>
            <option value=StateID <%if (orderBy.equalsIgnoreCase("StateID")){out.println(" selected");}%> >State</option>
            <option value=Zip <%if (orderBy.equalsIgnoreCase("Zip")){out.println(" selected");}%> >Postal Code</option>
          </select></p>
      </td>
      <td class=tdHeader colspan=2> 
        <p  align="center"> 
          <input type="Submit"  class="tdBaseAltGreen" name="Submit" value="Submit" align="middle">
          </p>
      </td>
    </tr>
    <tr class=tdHeader> 
      <td  colspan=1>&nbsp;      </td>
      <td  colspan=1>&nbsp;      </td>
      <td colspan=1> 
        <input type="text" name="AdminID" value="<%=myAdminID%>" size="10">
      </td>
      <td colspan=1> 
        <input type="text" name="Name" value="<%=myName%>" size="10">
      </td>
      <td colspan=1> 
        <input type="text" name="City" value="<%=myCity%>" size="10">
      </td>
      <td colspan=1> 
              <select name="StateID">
                <jsp:include page="../generic/tStateLIShort.jsp" flush="true" > 
                <jsp:param name="CurrentSelection" value="<%=myStateID%>" />
                </jsp:include>
              </select>
      </td>
      <td colspan=1> 
        <input type="text" name="Zip" value="<%=myZip%>" size="10">
      </td>
    </tr>
    <tr class=tdHeader> 
    <td>#</td>
    <td>Action</td>
      <td colspan=1> 
        AdminID
      </td>
      <td colspan=1> 
        Admin Name
      </td>
      <td colspan=1> 
        City
      </td>
      <td colspan=1> 
        State
      </td>
      <td colspan=1> 
        Postal Code
      </td>
    </tr>



<%



//String myWhere = "where ( (PracticeID IN (select PracticeID from tAdminPracticeLU where (adminID='"+iAdminID+"')))	 ";
//String myWhere = "where ( adminID="+iAdminID;
//String myWhere = "where  (adminID="+iAdminID+") and (practiceID in (select practiceID from tPracticeMaster where (practiceID>0";
String myWhere = "where  (tAdminPracticeLU.lookupid is null OR tNIM3_Modality.modalityid is null) and (tAdminMaster.adminid>0";

boolean theFirst = false;

try
{
if (!myAdminID.equalsIgnoreCase(""))
{
	if (!theFirst) { myWhere+=" and ";}
	myWhere += "tAdminMaster.AdminID = '" + myAdminID +"'";
	theFirst = false;
}
if (!myName.equalsIgnoreCase(""))
{
	if (!theFirst) { myWhere+=" and ";}
	myWhere += "UPPER(tAdminMaster.Name) LIKE UPPER('%" + DataControlUtils.fixApostrophe(myName) +"%')";
	theFirst = false;
}
if (!myCity.equalsIgnoreCase(""))
{
if (!theFirst) { myWhere+=" and ";}
myWhere += "UPPER(tAdminMaster.City) LIKE UPPER('%" + DataControlUtils.fixApostrophe(myCity) +"%')";
theFirst = false;
}
if (!myStateID.equalsIgnoreCase("0"))
{
if (!myStateID.equalsIgnoreCase(""))
{
if (!theFirst) { myWhere+=" and ";}
myWhere += "tAdminMaster.StateID = " + myStateID +"";
theFirst = false;
}
}
if (!myZip.equalsIgnoreCase(""))
{
if (!theFirst) { myWhere+=" and ";}
myWhere += "UPPER(tAdminMaster.Zip) LIKE UPPER('%" + myZip +"%')";
theFirst = false;
}
myWhere += ")";
//myWhere += ") ))";

//System.out.println(myWhere);
if (theFirst||myWhere.equalsIgnoreCase(")"))
{
myWhere = "";
}


}
catch(Exception e)
{
out.println("FFF:"+e);
}

searchDB2 mySS = new searchDB2();

java.sql.ResultSet myRS = null;;

try
{
String mySQL = ("select tAdminMaster.adminid,tAdminMaster.Name,tAdminMaster.City,tAdminMaster.StateID,tAdminMaster.ZIP from tAdminMaster LEFT JOIN tAdminPracticeLU on tAdminPracticeLU.adminid=tAdminMaster.adminid LEFT JOIN tNIM3_Modality on tNIM3_Modality.practiceid = tAdminPracticeLU.practiceID " + myWhere + "order by tAdminMaster." + orderBy + " ");
myRS = mySS.executeStatement(mySQL);
//out.print(mySQL);
//myRS = mySS.executeStatement("select * from tAdminPracticeLU " + myWhere + " order by " + orderBy);

}
catch(Exception e)
{
	out.println("ResultsSet:"+e);
}

String myMainTable= " ";
try{

int endCount = 0;

int cnt=0;
int cnt2=0;
   while (myRS!=null&&myRS.next())
   {
//bltPracticeMaster pm = new bltPracticeMaster(new Integer(myRS.getString("PracticeID")));

String pmAdminID = myRS.getString("AdminID");
String pmName = myRS.getString("Name");
String pmCity = myRS.getString("City");
String pmStateID  = ( myRS.getString("StateID"));
String pmZIP = myRS.getString("ZIP");







cnt++;
//if (cnt>=startID&&cnt<=startID+maxResults)
if (true||cnt<=maxResults)
{
cnt2++;

String myClass = "tdBase";
if (cnt2%2!=0)
{
myClass = "tdBaseAlt";
}
out.print("<tr class="+myClass+">");
out.print("<td>"+cnt+"</td>");
out.print("<td><a href=\"AdminPracticeAll_query_auth.jsp?EDIT=owner&EDITID=" + pmAdminID  +"\">Edit Admin</a></td>");
out.print("<td>");
out.print("" + pmAdminID+"");
out.print("</td>");
out.print("<td>");
out.print(pmName+"");
out.print("</td>");
out.print("<td>");
out.print(pmCity+"");
out.print("</td>");
out.print("<td>");

		if (pmStateID.equalsIgnoreCase("49") ) {out.print( "AK");}
		else if (pmStateID.equalsIgnoreCase("30") ) {out.print( "AL");}
		else if (pmStateID.equalsIgnoreCase("21") ) {out.print( "AR");}
		else if (pmStateID.equalsIgnoreCase("8") ) {out.print( "AZ");}
		else if (pmStateID.equalsIgnoreCase("1") ) {out.print( "CA");}
		else if (pmStateID.equalsIgnoreCase("10") ) {out.print( "CO");}
		else if (pmStateID.equalsIgnoreCase("45") ) {out.print( "CT");}
		else if (pmStateID.equalsIgnoreCase("51") ) {out.print( "DC");}
		else if (pmStateID.equalsIgnoreCase("47") ) {out.print( "DE");}
		else if (pmStateID.equalsIgnoreCase("33") ) {out.print( "FL");}
		else if (pmStateID.equalsIgnoreCase("32") ) {out.print( "GA");}
		else if (pmStateID.equalsIgnoreCase("50") ) {out.print( "HI");}
		else if (pmStateID.equalsIgnoreCase("19") ) {out.print( "IA");}
		else if (pmStateID.equalsIgnoreCase("5") ) {out.print( "ID");}
		else if (pmStateID.equalsIgnoreCase("24") ) {out.print( "IL");}
		else if (pmStateID.equalsIgnoreCase("27") ) {out.print( "IN");}
		else if (pmStateID.equalsIgnoreCase("15") ) {out.print( "KS");}
		else if (pmStateID.equalsIgnoreCase("28") ) {out.print( "KY");}
		else if (pmStateID.equalsIgnoreCase("22") ) {out.print( "LA");}
		else if (pmStateID.equalsIgnoreCase("43") ) {out.print( "MA");}
		else if (pmStateID.equalsIgnoreCase("48") ) {out.print( "MD");}
		else if (pmStateID.equalsIgnoreCase("34") ) {out.print( "ME");}
		else if (pmStateID.equalsIgnoreCase("26") ) {out.print( "MI");}
		else if (pmStateID.equalsIgnoreCase("18") ) {out.print( "MN");}
		else if (pmStateID.equalsIgnoreCase("20") ) {out.print( "MO");}
		else if (pmStateID.equalsIgnoreCase("25") ) {out.print( "MS");}
		else if (pmStateID.equalsIgnoreCase("4") ) {out.print( "MT");}
		else if (pmStateID.equalsIgnoreCase("52") ) {out.print( "NA");}
		else if (pmStateID.equalsIgnoreCase("41") ) {out.print( "NC");}
		else if (pmStateID.equalsIgnoreCase("12") ) {out.print( "ND");}
		else if (pmStateID.equalsIgnoreCase("14") ) {out.print( "NE");}
		else if (pmStateID.equalsIgnoreCase("35") ) {out.print( "NH");}
		else if (pmStateID.equalsIgnoreCase("46") ) {out.print( "NJ");}
		else if (pmStateID.equalsIgnoreCase("11") ) {out.print( "NM");}
		else if (pmStateID.equalsIgnoreCase("6") ) {out.print( "NV");}
		else if (pmStateID.equalsIgnoreCase("37") ) {out.print( "NY");}
		else if (pmStateID.equalsIgnoreCase("31") ) {out.print( "OH");}
		else if (pmStateID.equalsIgnoreCase("16") ) {out.print( "OK");}
		else if (pmStateID.equalsIgnoreCase("3") ) {out.print( "OR");}
		else if (pmStateID.equalsIgnoreCase("38") ) {out.print( "PA");}
		else if (pmStateID.equalsIgnoreCase("44") ) {out.print( "RI");}
		else if (pmStateID.equalsIgnoreCase("42") ) {out.print( "SC");}
		else if (pmStateID.equalsIgnoreCase("13") ) {out.print( "SD");}
		else if (pmStateID.equalsIgnoreCase("29") ) {out.print( "TN");}
		else if (pmStateID.equalsIgnoreCase("17") ) {out.print( "TX");}
		else if (pmStateID.equalsIgnoreCase("7") ) {out.print( "UT");}
		else if (pmStateID.equalsIgnoreCase("40") ) {out.print( "VA");}
		else if (pmStateID.equalsIgnoreCase("36") ) {out.print( "VT");}
		else if (pmStateID.equalsIgnoreCase("2") ) {out.print( "WA");}
		else if (pmStateID.equalsIgnoreCase("23") ) {out.print( "WI");}
		else if (pmStateID.equalsIgnoreCase("39") ) {out.print( "WV");}
		else if (pmStateID.equalsIgnoreCase("9") ) {out.print( "WY");}



out.print("</td>");
out.print("<td>");
out.print(pmZIP+"");
out.print("</td>");
out.print("</tr>");
}
   }
mySS.closeAll();
endCount = cnt;







}
catch(Exception e)
{
out.println("PrevNext:"+e);
}




try{

%>


</table> 


<%

}
catch(Exception e)
{
out.println("Display:"+e);
}





}
}
catch (Exception e)
{
out.println("Error???:"+e);
System.out.println("Error:"+e);
}

%>



  </table>
</form>
</td>
</tr>
</table>
</td>
</tr>
</table>     </td>
  </tr>
</table>



<%

  }
  else
  {
   response.sendRedirect("/errorhandler.jsp");
  }
%>

    </td></tr></table>

