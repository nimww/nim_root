<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.bltDocumentManagement,java.util.Vector,java.util.Enumeration,com.winstaff.ListElement, com.winstaff.PLCUtils, com.winstaff.SecurityCheck, com.winstaff.ConfigurationMessages,com.winstaff.bltProfessionalEducation,com.winstaff.bltProfessionalEducation_List" %>
<%/*
    filename: tProfessionalEducation_main_ProfessionalEducation_PhysicianID.jsp
    Created on Oct/22/2003
    Type: 1-n main class file
    Created by: Scott Ellis
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>
    <%
    Integer iExpressMode = new Integer(1);
    if (pageControllerHash.containsKey("iExpressMode")) 
    {
        iExpressMode =    (Integer)pageControllerHash.get("iExpressMode");
    }
    %>

<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_PhysicianID.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>

<script>
function addTitle(myField, myAddField)
{
	if (myField.value =="")
	{
	}
	else
	{
		myField.value = myField.value + ", ";
	}
	myField.value = myField.value + myAddField.value;
        textAreaStop(myField,50);
}

</script>



    <table cellpadding=0 cellspacing=0 border=0 width=<%=MasterTableWidth%> >
    <tr><td width=10>&nbsp;</td><td>
    <%=ConfigurationMessages.getHTML("EXPRESSTopControl","tProfessionalEducation",iExpressMode)%>



<%
//initial declaration of list class and parentID
    Integer        iPhysicianID        =    null;
    boolean accessValid = false;
   Integer iSecurityCheck = SecurityCheck.CheckItem("PractitionerSection6", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {
    if (pageControllerHash.containsKey("iPhysicianID") ) 
    {
        iPhysicianID        =    (Integer)pageControllerHash.get("iPhysicianID");
        accessValid = true;
    }  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);
      pageControllerHash.put("sParentReturnPage","tProfessionalEducation_main_ProfessionalEducation_PhysicianID_Expand.jsp");
    pageControllerHash.remove("iProfessionalEducationID");
    session.setAttribute("pageControllerHash",pageControllerHash);

    bltProfessionalEducation_List        bltProfessionalEducation_List        =    new    bltProfessionalEducation_List(iPhysicianID);

//declaration of Enumeration
    bltProfessionalEducation        ProfessionalEducation;
    ListElement         leCurrentElement;
    Enumeration eList = bltProfessionalEducation_List.elements();
    %>
        <%@ include file="tProfessionalEducation_main_ProfessionalEducation_PhysicianID_instructions.jsp" %>

        <form action="tProfessionalEducation_main_ProfessionalEducation_PhysicianID_Express_sub.jsp" name="tProfessionalEducation_main_ProfessionalEducation_PhysicianID1" method="POST">
    <%
    int iExpress=0;
    while (eList.hasMoreElements()||iExpress<=ConfigurationMessages.getExpressItemCount("tProfessionalEducation"))
    {
       iExpress++;
         %>
         <table border="0" bordercolor="333333" cellpadding="0" class=tdHeaderAlt cellspacing="0" width="100%">
            <tr>
              <td width="50%">
                   <table border="0" bordercolor="333333" cellpadding="0" class=tdHeaderAlt cellspacing="0" width="100%">
                   <tr> 
                   	<td rowspan="2"><img src=express/left-corner.gif></td>
                   	<td width=100%><img width=100% height=2 src=express/small-line.gif></td>
                   	<td rowspan="2" align=right><img src=express/right-corner.gif></td>
                   </tr>
                     <tr> 
                       <td>
         <%

      boolean isNewRecord= false;
      if (eList.hasMoreElements())
      {
        leCurrentElement    = (ListElement) eList.nextElement();
        ProfessionalEducation  = (bltProfessionalEducation) leCurrentElement.getObject();
      }
      else
      {
        ProfessionalEducation  = new bltProfessionalEducation();
        isNewRecord= true;
      }
        ProfessionalEducation.GroupSecurityInit(UserSecurityGroupID);
        String theClass = "tdHeader";
        %>
               <span class=<%=theClass%> ><b><%=ConfigurationMessages.getDataCategory("tProfessionalEducation")%> #<%=iExpress%></span>
                  </td></tr></table>
            </td></tr>
                 </table>
                 <table width="100%" border="1" cellspacing="0" bordercolor="#333333">
                  <tr>
                   <td>
                     <table width="100%" border="0" cellspacing="0">
                     <tr><td>
<%String theClassF = "textBase";%>
<%
if (isNewRecord)
{%>
<input type=hidden name=recordItemStatus_<%=iExpress%>="new">
<%}
else
{%>
<input type=hidden name=recordItemStatus_<%=iExpress%>="edit">
<%}

  {

        %>

          <%  theClass ="tdBase";%>
        <table width=100% border=1 bordercolor=#333333 cellpadding=3 cellspacing=0 class=tableBase>
         <tr><td class=tableColor>


            <table cellpadding=0 cellspacing=0 width=100%>
            <%
            if ( (ProfessionalEducation.isRequired("DegreeID",UserSecurityGroupID))&&(!ProfessionalEducation.isComplete("DegreeID")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((ProfessionalEducation.isExpired("DegreeID",expiredDays))&&(ProfessionalEducation.isExpiredCheck("DegreeID",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((ProfessionalEducation.isWrite("DegreeID",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Degree Type&nbsp;</b></p></td><td valign=top><p><select   name="DegreeID_<%=iExpress%>" ><jsp:include page="../generic/tDegreeTypeLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=ProfessionalEducation.getDegreeID()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=DegreeID&amp;sTableName=tProfessionalEducation&amp;sRefID=<%=ProfessionalEducation.getProfessionalEducationID()%>&amp;sFieldNameDisp=<%=ProfessionalEducation.getEnglish("DegreeID")%>&amp;sTableNameDisp=tProfessionalEducation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((ProfessionalEducation.isRead("DegreeID",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Degree Type&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tDegreeTypeLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=ProfessionalEducation.getDegreeID()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=DegreeID&amp;sTableName=tProfessionalEducation&amp;sRefID=<%=ProfessionalEducation.getProfessionalEducationID()%>&amp;sFieldNameDisp=<%=ProfessionalEducation.getEnglish("DegreeID")%>&amp;sTableNameDisp=tProfessionalEducation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (ProfessionalEducation.isRequired("Other",UserSecurityGroupID))&&(!ProfessionalEducation.isComplete("Other")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((ProfessionalEducation.isExpired("Other",expiredDays))&&(ProfessionalEducation.isExpiredCheck("Other",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                     <input type=hidden name="Other_<%=iExpress%>" value="<%=ProfessionalEducation.getOther()%>">

            <%
            if ( (ProfessionalEducation.isRequired("StartDate",UserSecurityGroupID))&&(!ProfessionalEducation.isComplete("StartDate")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((ProfessionalEducation.isExpired("StartDate",expiredDays))&&(ProfessionalEducation.isExpiredCheck("StartDate",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>

            <%
            if ((ProfessionalEducation.isWrite("StartDate",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Start Date&nbsp;(mm/dd/yyyy):&nbsp;</b></p></td><td valign=top><p><input maxlength=20  type=text size="80" name="StartDate_<%=iExpress%>" value='<jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(ProfessionalEducation.getStartDate())%>" /></jsp:include>' >&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=StartDate&amp;sTableName=tProfessionalEducation&amp;sRefID=<%=ProfessionalEducation.getProfessionalEducationID()%>&amp;sFieldNameDisp=<%=ProfessionalEducation.getEnglish("StartDate")%>&amp;sTableNameDisp=tProfessionalEducation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((ProfessionalEducation.isRead("StartDate",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Start Date&nbsp;(mm/dd/yyyy):&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(ProfessionalEducation.getStartDate())%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=StartDate&amp;sTableName=tProfessionalEducation&amp;sRefID=<%=ProfessionalEducation.getProfessionalEducationID()%>&amp;sFieldNameDisp=<%=ProfessionalEducation.getEnglish("StartDate")%>&amp;sTableNameDisp=tProfessionalEducation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (ProfessionalEducation.isRequired("DateOfGraduation",UserSecurityGroupID))&&(!ProfessionalEducation.isComplete("DateOfGraduation")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((ProfessionalEducation.isExpired("DateOfGraduation",expiredDays))&&(ProfessionalEducation.isExpiredCheck("DateOfGraduation",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>

            <%
            if ((ProfessionalEducation.isWrite("DateOfGraduation",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Graduation Date&nbsp;(mm/dd/yyyy):&nbsp;</b></p></td><td valign=top><p><input maxlength=20  type=text size="80" name="DateOfGraduation_<%=iExpress%>" value='<jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(ProfessionalEducation.getDateOfGraduation())%>" /></jsp:include>' >&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=DateOfGraduation&amp;sTableName=tProfessionalEducation&amp;sRefID=<%=ProfessionalEducation.getProfessionalEducationID()%>&amp;sFieldNameDisp=<%=ProfessionalEducation.getEnglish("DateOfGraduation")%>&amp;sTableNameDisp=tProfessionalEducation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((ProfessionalEducation.isRead("DateOfGraduation",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Graduation Date&nbsp;(mm/dd/yyyy):&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(ProfessionalEducation.getDateOfGraduation())%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=DateOfGraduation&amp;sTableName=tProfessionalEducation&amp;sRefID=<%=ProfessionalEducation.getProfessionalEducationID()%>&amp;sFieldNameDisp=<%=ProfessionalEducation.getEnglish("DateOfGraduation")%>&amp;sTableNameDisp=tProfessionalEducation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (ProfessionalEducation.isRequired("EndDate",UserSecurityGroupID))&&(!ProfessionalEducation.isComplete("EndDate")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((ProfessionalEducation.isExpired("EndDate",expiredDays))&&(ProfessionalEducation.isExpiredCheck("EndDate",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>

            <%
            if ((ProfessionalEducation.isWrite("EndDate",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>End Date (if not graduated)&nbsp;(mm/dd/yyyy):&nbsp;</b></p></td><td valign=top><p><input maxlength=20  type=text size="80" name="EndDate_<%=iExpress%>" value='<jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(ProfessionalEducation.getEndDate())%>" /></jsp:include>' >&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=EndDate&amp;sTableName=tProfessionalEducation&amp;sRefID=<%=ProfessionalEducation.getProfessionalEducationID()%>&amp;sFieldNameDisp=<%=ProfessionalEducation.getEnglish("EndDate")%>&amp;sTableNameDisp=tProfessionalEducation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((ProfessionalEducation.isRead("EndDate",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>End Date (if not graduated)&nbsp;(mm/dd/yyyy):&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(ProfessionalEducation.getEndDate())%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=EndDate&amp;sTableName=tProfessionalEducation&amp;sRefID=<%=ProfessionalEducation.getProfessionalEducationID()%>&amp;sFieldNameDisp=<%=ProfessionalEducation.getEnglish("EndDate")%>&amp;sTableNameDisp=tProfessionalEducation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (ProfessionalEducation.isRequired("Focus",UserSecurityGroupID))&&(!ProfessionalEducation.isComplete("Focus")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((ProfessionalEducation.isExpired("Focus",expiredDays))&&(ProfessionalEducation.isExpiredCheck("Focus",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((ProfessionalEducation.isWrite("Focus",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Major/Focus&nbsp;</b></p></td><td valign=top><p><input maxlength="50" type=text size="80" name="Focus_<%=iExpress%>" value="<%=ProfessionalEducation.getFocus()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Focus&amp;sTableName=tProfessionalEducation&amp;sRefID=<%=ProfessionalEducation.getProfessionalEducationID()%>&amp;sFieldNameDisp=<%=ProfessionalEducation.getEnglish("Focus")%>&amp;sTableNameDisp=tProfessionalEducation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((ProfessionalEducation.isRead("Focus",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Major/Focus&nbsp;</b></p></td><td valign=top><p><%=ProfessionalEducation.getFocus()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Focus&amp;sTableName=tProfessionalEducation&amp;sRefID=<%=ProfessionalEducation.getProfessionalEducationID()%>&amp;sFieldNameDisp=<%=ProfessionalEducation.getEnglish("Focus")%>&amp;sTableNameDisp=tProfessionalEducation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (ProfessionalEducation.isRequired("SchoolName",UserSecurityGroupID))&&(!ProfessionalEducation.isComplete("SchoolName")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((ProfessionalEducation.isExpired("SchoolName",expiredDays))&&(ProfessionalEducation.isExpiredCheck("SchoolName",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((ProfessionalEducation.isWrite("SchoolName",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>School Name&nbsp;</b></p></td><td valign=top><p><input maxlength="100" type=text size="80" name="SchoolName_<%=iExpress%>" value="<%=ProfessionalEducation.getSchoolName()%>">&nbsp;<a href="#" onClick="window.open('LI_tSchoolLI_Search.jsp?OFieldName=SchoolName_<%=iExpress%>','BNPop','status=yes,scrollbars=yes,resizable=yes,width=400,height=300'); return false;"><img align=middle border=0 src=images/icon_lu.gif></a>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SchoolName&amp;sTableName=tProfessionalEducation&amp;sRefID=<%=ProfessionalEducation.getProfessionalEducationID()%>&amp;sFieldNameDisp=<%=ProfessionalEducation.getEnglish("SchoolName")%>&amp;sTableNameDisp=tProfessionalEducation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((ProfessionalEducation.isRead("SchoolName",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>School Name&nbsp;</b></p></td><td valign=top><p><%=ProfessionalEducation.getSchoolName()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SchoolName&amp;sTableName=tProfessionalEducation&amp;sRefID=<%=ProfessionalEducation.getProfessionalEducationID()%>&amp;sFieldNameDisp=<%=ProfessionalEducation.getEnglish("SchoolName")%>&amp;sTableNameDisp=tProfessionalEducation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (ProfessionalEducation.isRequired("SchoolAddress1",UserSecurityGroupID))&&(!ProfessionalEducation.isComplete("SchoolAddress1")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((ProfessionalEducation.isExpired("SchoolAddress1",expiredDays))&&(ProfessionalEducation.isExpiredCheck("SchoolAddress1",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((ProfessionalEducation.isWrite("SchoolAddress1",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Address&nbsp;</b></p></td><td valign=top><p><input maxlength="50" type=text size="80" name="SchoolAddress1_<%=iExpress%>" value="<%=ProfessionalEducation.getSchoolAddress1()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SchoolAddress1&amp;sTableName=tProfessionalEducation&amp;sRefID=<%=ProfessionalEducation.getProfessionalEducationID()%>&amp;sFieldNameDisp=<%=ProfessionalEducation.getEnglish("SchoolAddress1")%>&amp;sTableNameDisp=tProfessionalEducation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((ProfessionalEducation.isRead("SchoolAddress1",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Address&nbsp;</b></p></td><td valign=top><p><%=ProfessionalEducation.getSchoolAddress1()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SchoolAddress1&amp;sTableName=tProfessionalEducation&amp;sRefID=<%=ProfessionalEducation.getProfessionalEducationID()%>&amp;sFieldNameDisp=<%=ProfessionalEducation.getEnglish("SchoolAddress1")%>&amp;sTableNameDisp=tProfessionalEducation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (ProfessionalEducation.isRequired("SchoolAddress2",UserSecurityGroupID))&&(!ProfessionalEducation.isComplete("SchoolAddress2")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((ProfessionalEducation.isExpired("SchoolAddress2",expiredDays))&&(ProfessionalEducation.isExpiredCheck("SchoolAddress2",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((ProfessionalEducation.isWrite("SchoolAddress2",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Address 2&nbsp;</b></p></td><td valign=top><p><input maxlength="20" type=text size="80" name="SchoolAddress2_<%=iExpress%>" value="<%=ProfessionalEducation.getSchoolAddress2()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SchoolAddress2&amp;sTableName=tProfessionalEducation&amp;sRefID=<%=ProfessionalEducation.getProfessionalEducationID()%>&amp;sFieldNameDisp=<%=ProfessionalEducation.getEnglish("SchoolAddress2")%>&amp;sTableNameDisp=tProfessionalEducation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((ProfessionalEducation.isRead("SchoolAddress2",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Address 2&nbsp;</b></p></td><td valign=top><p><%=ProfessionalEducation.getSchoolAddress2()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SchoolAddress2&amp;sTableName=tProfessionalEducation&amp;sRefID=<%=ProfessionalEducation.getProfessionalEducationID()%>&amp;sFieldNameDisp=<%=ProfessionalEducation.getEnglish("SchoolAddress2")%>&amp;sTableNameDisp=tProfessionalEducation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (ProfessionalEducation.isRequired("SchoolCity",UserSecurityGroupID))&&(!ProfessionalEducation.isComplete("SchoolCity")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((ProfessionalEducation.isExpired("SchoolCity",expiredDays))&&(ProfessionalEducation.isExpiredCheck("SchoolCity",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((ProfessionalEducation.isWrite("SchoolCity",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>City&nbsp;</b></p></td><td valign=top><p><input maxlength="30" type=text size="80" name="SchoolCity_<%=iExpress%>" value="<%=ProfessionalEducation.getSchoolCity()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SchoolCity&amp;sTableName=tProfessionalEducation&amp;sRefID=<%=ProfessionalEducation.getProfessionalEducationID()%>&amp;sFieldNameDisp=<%=ProfessionalEducation.getEnglish("SchoolCity")%>&amp;sTableNameDisp=tProfessionalEducation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((ProfessionalEducation.isRead("SchoolCity",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>City&nbsp;</b></p></td><td valign=top><p><%=ProfessionalEducation.getSchoolCity()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SchoolCity&amp;sTableName=tProfessionalEducation&amp;sRefID=<%=ProfessionalEducation.getProfessionalEducationID()%>&amp;sFieldNameDisp=<%=ProfessionalEducation.getEnglish("SchoolCity")%>&amp;sTableNameDisp=tProfessionalEducation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (ProfessionalEducation.isRequired("SchoolStateID",UserSecurityGroupID))&&(!ProfessionalEducation.isComplete("SchoolStateID")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((ProfessionalEducation.isExpired("SchoolStateID",expiredDays))&&(ProfessionalEducation.isExpiredCheck("SchoolStateID",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((ProfessionalEducation.isWrite("SchoolStateID",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>State&nbsp;</b></p></td><td valign=top><p><select   name="SchoolStateID_<%=iExpress%>" ><jsp:include page="../generic/tStateLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=ProfessionalEducation.getSchoolStateID()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SchoolStateID&amp;sTableName=tProfessionalEducation&amp;sRefID=<%=ProfessionalEducation.getProfessionalEducationID()%>&amp;sFieldNameDisp=<%=ProfessionalEducation.getEnglish("SchoolStateID")%>&amp;sTableNameDisp=tProfessionalEducation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((ProfessionalEducation.isRead("SchoolStateID",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>State&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tStateLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=ProfessionalEducation.getSchoolStateID()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SchoolStateID&amp;sTableName=tProfessionalEducation&amp;sRefID=<%=ProfessionalEducation.getProfessionalEducationID()%>&amp;sFieldNameDisp=<%=ProfessionalEducation.getEnglish("SchoolStateID")%>&amp;sTableNameDisp=tProfessionalEducation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (ProfessionalEducation.isRequired("SchoolProvince",UserSecurityGroupID))&&(!ProfessionalEducation.isComplete("SchoolProvince")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((ProfessionalEducation.isExpired("SchoolProvince",expiredDays))&&(ProfessionalEducation.isExpiredCheck("SchoolProvince",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                     <input type=hidden name="SchoolProvince_<%=iExpress%>" value="<%=ProfessionalEducation.getSchoolProvince()%>">

            <%
            if ( (ProfessionalEducation.isRequired("SchoolZIP",UserSecurityGroupID))&&(!ProfessionalEducation.isComplete("SchoolZIP")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((ProfessionalEducation.isExpired("SchoolZIP",expiredDays))&&(ProfessionalEducation.isExpiredCheck("SchoolZIP",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((ProfessionalEducation.isWrite("SchoolZIP",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>ZIP&nbsp;</b></p></td><td valign=top><p><input maxlength="50" type=text size="80" name="SchoolZIP_<%=iExpress%>" value="<%=ProfessionalEducation.getSchoolZIP()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SchoolZIP&amp;sTableName=tProfessionalEducation&amp;sRefID=<%=ProfessionalEducation.getProfessionalEducationID()%>&amp;sFieldNameDisp=<%=ProfessionalEducation.getEnglish("SchoolZIP")%>&amp;sTableNameDisp=tProfessionalEducation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((ProfessionalEducation.isRead("SchoolZIP",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>ZIP&nbsp;</b></p></td><td valign=top><p><%=ProfessionalEducation.getSchoolZIP()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SchoolZIP&amp;sTableName=tProfessionalEducation&amp;sRefID=<%=ProfessionalEducation.getProfessionalEducationID()%>&amp;sFieldNameDisp=<%=ProfessionalEducation.getEnglish("SchoolZIP")%>&amp;sTableNameDisp=tProfessionalEducation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (ProfessionalEducation.isRequired("SchoolCountryID",UserSecurityGroupID))&&(!ProfessionalEducation.isComplete("SchoolCountryID")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((ProfessionalEducation.isExpired("SchoolCountryID",expiredDays))&&(ProfessionalEducation.isExpiredCheck("SchoolCountryID",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((ProfessionalEducation.isWrite("SchoolCountryID",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Country&nbsp;</b></p></td><td valign=top><p><select   name="SchoolCountryID_<%=iExpress%>" ><jsp:include page="../generic/tCountryLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=ProfessionalEducation.getSchoolCountryID()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SchoolCountryID&amp;sTableName=tProfessionalEducation&amp;sRefID=<%=ProfessionalEducation.getProfessionalEducationID()%>&amp;sFieldNameDisp=<%=ProfessionalEducation.getEnglish("SchoolCountryID")%>&amp;sTableNameDisp=tProfessionalEducation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((ProfessionalEducation.isRead("SchoolCountryID",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Country&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tCountryLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=ProfessionalEducation.getSchoolCountryID()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SchoolCountryID&amp;sTableName=tProfessionalEducation&amp;sRefID=<%=ProfessionalEducation.getProfessionalEducationID()%>&amp;sFieldNameDisp=<%=ProfessionalEducation.getEnglish("SchoolCountryID")%>&amp;sTableNameDisp=tProfessionalEducation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (ProfessionalEducation.isRequired("SchoolPhone",UserSecurityGroupID))&&(!ProfessionalEducation.isComplete("SchoolPhone")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((ProfessionalEducation.isExpired("SchoolPhone",expiredDays))&&(ProfessionalEducation.isExpiredCheck("SchoolPhone",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                     <input type=hidden name="SchoolPhone_<%=iExpress%>" value="<%=ProfessionalEducation.getSchoolPhone()%>">

            <%
            if ( (ProfessionalEducation.isRequired("SchoolFax",UserSecurityGroupID))&&(!ProfessionalEducation.isComplete("SchoolFax")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((ProfessionalEducation.isExpired("SchoolFax",expiredDays))&&(ProfessionalEducation.isExpiredCheck("SchoolFax",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                     <input type=hidden name="SchoolFax_<%=iExpress%>" value="<%=ProfessionalEducation.getSchoolFax()%>">

            <%
            if ( (ProfessionalEducation.isRequired("ContactName",UserSecurityGroupID))&&(!ProfessionalEducation.isComplete("ContactName")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((ProfessionalEducation.isExpired("ContactName",expiredDays))&&(ProfessionalEducation.isExpiredCheck("ContactName",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                     <input type=hidden name="ContactName_<%=iExpress%>" value="<%=ProfessionalEducation.getContactName()%>">

            <%
            if ( (ProfessionalEducation.isRequired("ContactEmail",UserSecurityGroupID))&&(!ProfessionalEducation.isComplete("ContactEmail")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((ProfessionalEducation.isExpired("ContactEmail",expiredDays))&&(ProfessionalEducation.isExpiredCheck("ContactEmail",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                     <input type=hidden name="ContactEmail_<%=iExpress%>" value="<%=ProfessionalEducation.getContactEmail()%>">

            <%
            if ( (ProfessionalEducation.isRequired("Comments",UserSecurityGroupID))&&(!ProfessionalEducation.isComplete("Comments")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((ProfessionalEducation.isExpired("Comments",expiredDays))&&(ProfessionalEducation.isExpiredCheck("Comments",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                     <input type=hidden name="Comments_<%=iExpress%>" value="<%=ProfessionalEducation.getComments()%>">


            <tr><td width=40%>&nbsp;</td><td width=60%>&nbsp;</td></tr>

            <input type=hidden name=routePageReference value="sParentReturnPage">

        </td></tr></table>
        </td></tr></table>
        <%
  }%>

        </td></tr></table></table><br>        <%
    }
    %>
        <input <%=HTMLFormStyleButton%> onClick="document.forms[0].nextPage.value='<%=ConfigurationMessages.getExpressLinkRaw("tProfessionalEducation","next",iExpressMode)%>'" type=Submit value="Continue" name=Submit>
        <input type=hidden name=nextPage value="">
        </form>

    </table>
    <%
  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>

    </td></tr></table>


<%String bnIncludeFN = "ui_"+thePLCID+"\\bot-nav_PhysicianID.jsp";
%>
<jsp:include page="<%=bnIncludeFN%>" flush="true" ><jsp:param name="plcID" value="<%=thePLCID%>"/></jsp:include>
