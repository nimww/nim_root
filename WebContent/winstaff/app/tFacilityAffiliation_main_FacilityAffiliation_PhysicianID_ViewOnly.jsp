<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.bltDocumentManagement,java.util.Vector,java.util.Enumeration,com.winstaff.ListElement, com.winstaff.PLCUtils, com.winstaff.SecurityCheck, com.winstaff.ConfigurationMessages, com.winstaff.bltFacilityAffiliation, com.winstaff.bltFacilityAffiliation_List" %>
<%/*
    filename: tFacilityAffiliation_main_FacilityAffiliation_PhysicianID.jsp
    Created on May/26/2004
    Type: 1-n main class file
    Created by: Scott Ellis
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>
    <%
    Integer iExpressMode = new Integer(1);
    String MasterTableWidthVO = "100%";
    if (pageControllerHash.containsKey("iExpressMode")) 
    {
        iExpressMode =    (Integer)pageControllerHash.get("iExpressMode");
    }
    if (pageControllerHash.containsKey("MasterTableWidthVO")) 
    {
        MasterTableWidthVO = (String)pageControllerHash.get("MasterTableWidthVO");
    }
    %>

<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_Clear.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>

    <table cellpadding=0 cellspacing=0 border=0 width=<%=MasterTableWidthVO%> >
    <tr><td width=10>&nbsp;</td><td>




<%
//initial declaration of list class and parentID
    Integer        iPhysicianID        =    null;
    boolean accessValid = false;
   Integer iSecurityCheck = SecurityCheck.CheckItem("PractitionerSection10", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {
    if (pageControllerHash.containsKey("iPhysicianID") ) 
    {
        iPhysicianID        =    (Integer)pageControllerHash.get("iPhysicianID");
        accessValid = true;
    }  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);
      pageControllerHash.put("sParentReturnPage","tFacilityAffiliation_main_FacilityAffiliation_PhysicianID_Expand.jsp");
    pageControllerHash.remove("iAffiliationID");
    session.setAttribute("pageControllerHash",pageControllerHash);

    bltFacilityAffiliation_List        bltFacilityAffiliation_List        =    new    bltFacilityAffiliation_List(iPhysicianID);

//declaration of Enumeration
    bltFacilityAffiliation        FacilityAffiliation;
    ListElement         leCurrentElement;
    Enumeration eList = bltFacilityAffiliation_List.elements();
    %>
        <%@ include file="tFacilityAffiliation_main_FacilityAffiliation_PhysicianID_instructions.jsp" %>


    <%
    int iExpress=0;
    while (eList.hasMoreElements())
    {
       iExpress++;
         %>
         <table border="0" bordercolor="333333" cellpadding="0" class=tdHeaderAlt cellspacing="0" width="100%">
            <tr>
              <td width="50%">
                   <table border="0" bordercolor="333333" cellpadding="0" class=tdHeaderAlt cellspacing="0" width="100%">
                   <tr> 
                   	<td rowspan="2"><img src=express/left-corner.gif></td>
                   	<td width=100%><img width=100% height=2 src=express/small-line.gif></td>
                   	<td rowspan="2" align=right><img src=express/right-corner.gif></td>
                   </tr>
                     <tr> 
                       <td>
         <%

      boolean isNewRecord= false;
      if (eList.hasMoreElements())
      {
        leCurrentElement    = (ListElement) eList.nextElement();
        FacilityAffiliation  = (bltFacilityAffiliation) leCurrentElement.getObject();
      }
      else
      {
        FacilityAffiliation  = new bltFacilityAffiliation();
        isNewRecord= true;
      }
        FacilityAffiliation.GroupSecurityInit(UserSecurityGroupID);
        String theClass = "tdHeader";
        %>
               <span class=<%=theClass%> ><b><%=ConfigurationMessages.getDataCategory("tFacilityAffiliation")%> #<%=iExpress%></span>
                  </td></tr></table>
            </td></tr>
                 </table>
                 <table width="100%" border="1" cellspacing="0" bordercolor="#333333">
                  <tr>
                   <td>
                     <table width="100%" border="0" cellspacing="0">
                     <tr><td>
<%String theClassF = "textBase";%>
<%
if (isNewRecord)
{%>
<input type=hidden name=recordItemStatus_<%=iExpress%>="new">
<%}
else
{%>
<input type=hidden name=recordItemStatus_<%=iExpress%>="edit">
<%}

  {

        %>

          <%  theClass ="tdBase";%>
        <table width=100% border=1 bordercolor=#333333 cellpadding=3 cellspacing=0 class=tableBase>
         <tr><td class=tableColor>


            <table cellpadding=0 cellspacing=0 width=100%>
            <%
            if ( (FacilityAffiliation.isRequired("IsPrimary",UserSecurityGroupID))&&(!FacilityAffiliation.isComplete("IsPrimary")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((FacilityAffiliation.isExpired("IsPrimary",expiredDays))&&(FacilityAffiliation.isExpiredCheck("IsPrimary",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((FacilityAffiliation.isWrite("IsPrimary",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Appointment Type&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tAppointmentTypeLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=FacilityAffiliation.getIsPrimary()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=IsPrimary&amp;sTableName=tFacilityAffiliation&amp;sRefID=<%=FacilityAffiliation.getAffiliationID()%>&amp;sFieldNameDisp=<%=FacilityAffiliation.getEnglish("IsPrimary")%>&amp;sTableNameDisp=tFacilityAffiliation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((FacilityAffiliation.isRead("IsPrimary",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Appointment Type&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tAppointmentTypeLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=FacilityAffiliation.getIsPrimary()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=IsPrimary&amp;sTableName=tFacilityAffiliation&amp;sRefID=<%=FacilityAffiliation.getAffiliationID()%>&amp;sFieldNameDisp=<%=FacilityAffiliation.getEnglish("IsPrimary")%>&amp;sTableNameDisp=tFacilityAffiliation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (FacilityAffiliation.isRequired("AppointmentLevel",UserSecurityGroupID))&&(!FacilityAffiliation.isComplete("AppointmentLevel")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((FacilityAffiliation.isExpired("AppointmentLevel",expiredDays))&&(FacilityAffiliation.isExpiredCheck("AppointmentLevel",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((FacilityAffiliation.isWrite("AppointmentLevel",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Appointment/status level&nbsp;</b></p></td><td valign=top><p><%=FacilityAffiliation.getAppointmentLevel()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=AppointmentLevel&amp;sTableName=tFacilityAffiliation&amp;sRefID=<%=FacilityAffiliation.getAffiliationID()%>&amp;sFieldNameDisp=<%=FacilityAffiliation.getEnglish("AppointmentLevel")%>&amp;sTableNameDisp=tFacilityAffiliation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((FacilityAffiliation.isRead("AppointmentLevel",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Appointment/status level&nbsp;</b></p></td><td valign=top><p><%=FacilityAffiliation.getAppointmentLevel()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=AppointmentLevel&amp;sTableName=tFacilityAffiliation&amp;sRefID=<%=FacilityAffiliation.getAffiliationID()%>&amp;sFieldNameDisp=<%=FacilityAffiliation.getEnglish("AppointmentLevel")%>&amp;sTableNameDisp=tFacilityAffiliation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (FacilityAffiliation.isRequired("StartDate",UserSecurityGroupID))&&(!FacilityAffiliation.isComplete("StartDate")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((FacilityAffiliation.isExpired("StartDate",expiredDays))&&(FacilityAffiliation.isExpiredCheck("StartDate",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>

            <%
            if ((FacilityAffiliation.isWrite("StartDate",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Start Date&nbsp;(mm/dd/yyyy):&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(FacilityAffiliation.getStartDate())%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=StartDate&amp;sTableName=tFacilityAffiliation&amp;sRefID=<%=FacilityAffiliation.getAffiliationID()%>&amp;sFieldNameDisp=<%=FacilityAffiliation.getEnglish("StartDate")%>&amp;sTableNameDisp=tFacilityAffiliation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((FacilityAffiliation.isRead("StartDate",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Start Date&nbsp;(mm/dd/yyyy):&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(FacilityAffiliation.getStartDate())%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=StartDate&amp;sTableName=tFacilityAffiliation&amp;sRefID=<%=FacilityAffiliation.getAffiliationID()%>&amp;sFieldNameDisp=<%=FacilityAffiliation.getEnglish("StartDate")%>&amp;sTableNameDisp=tFacilityAffiliation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (FacilityAffiliation.isRequired("EndDate",UserSecurityGroupID))&&(!FacilityAffiliation.isComplete("EndDate")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((FacilityAffiliation.isExpired("EndDate",expiredDays))&&(FacilityAffiliation.isExpiredCheck("EndDate",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>

            <%
            if ((FacilityAffiliation.isWrite("EndDate",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>End Date&nbsp;(mm/dd/yyyy):&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(FacilityAffiliation.getEndDate())%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=EndDate&amp;sTableName=tFacilityAffiliation&amp;sRefID=<%=FacilityAffiliation.getAffiliationID()%>&amp;sFieldNameDisp=<%=FacilityAffiliation.getEnglish("EndDate")%>&amp;sTableNameDisp=tFacilityAffiliation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((FacilityAffiliation.isRead("EndDate",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>End Date&nbsp;(mm/dd/yyyy):&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(FacilityAffiliation.getEndDate())%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=EndDate&amp;sTableName=tFacilityAffiliation&amp;sRefID=<%=FacilityAffiliation.getAffiliationID()%>&amp;sFieldNameDisp=<%=FacilityAffiliation.getEnglish("EndDate")%>&amp;sTableNameDisp=tFacilityAffiliation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (FacilityAffiliation.isRequired("PendingDate",UserSecurityGroupID))&&(!FacilityAffiliation.isComplete("PendingDate")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((FacilityAffiliation.isExpired("PendingDate",expiredDays))&&(FacilityAffiliation.isExpiredCheck("PendingDate",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>

            <%
            if ((FacilityAffiliation.isWrite("PendingDate",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>If this appointment is pending, please enter the date you submitted your application&nbsp;(mm/dd/yyyy):&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(FacilityAffiliation.getPendingDate())%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PendingDate&amp;sTableName=tFacilityAffiliation&amp;sRefID=<%=FacilityAffiliation.getAffiliationID()%>&amp;sFieldNameDisp=<%=FacilityAffiliation.getEnglish("PendingDate")%>&amp;sTableNameDisp=tFacilityAffiliation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((FacilityAffiliation.isRead("PendingDate",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>If this appointment is pending, please enter the date you submitted your application&nbsp;(mm/dd/yyyy):&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(FacilityAffiliation.getPendingDate())%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PendingDate&amp;sTableName=tFacilityAffiliation&amp;sRefID=<%=FacilityAffiliation.getAffiliationID()%>&amp;sFieldNameDisp=<%=FacilityAffiliation.getEnglish("PendingDate")%>&amp;sTableNameDisp=tFacilityAffiliation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (FacilityAffiliation.isRequired("FacilityName",UserSecurityGroupID))&&(!FacilityAffiliation.isComplete("FacilityName")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((FacilityAffiliation.isExpired("FacilityName",expiredDays))&&(FacilityAffiliation.isExpiredCheck("FacilityName",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((FacilityAffiliation.isWrite("FacilityName",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Facility Name&nbsp;</b></p></td><td valign=top><p><%=FacilityAffiliation.getFacilityName()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=FacilityName&amp;sTableName=tFacilityAffiliation&amp;sRefID=<%=FacilityAffiliation.getAffiliationID()%>&amp;sFieldNameDisp=<%=FacilityAffiliation.getEnglish("FacilityName")%>&amp;sTableNameDisp=tFacilityAffiliation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((FacilityAffiliation.isRead("FacilityName",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Facility Name&nbsp;</b></p></td><td valign=top><p><%=FacilityAffiliation.getFacilityName()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=FacilityName&amp;sTableName=tFacilityAffiliation&amp;sRefID=<%=FacilityAffiliation.getAffiliationID()%>&amp;sFieldNameDisp=<%=FacilityAffiliation.getEnglish("FacilityName")%>&amp;sTableNameDisp=tFacilityAffiliation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (FacilityAffiliation.isRequired("FacilityDepartment",UserSecurityGroupID))&&(!FacilityAffiliation.isComplete("FacilityDepartment")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((FacilityAffiliation.isExpired("FacilityDepartment",expiredDays))&&(FacilityAffiliation.isExpiredCheck("FacilityDepartment",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((FacilityAffiliation.isWrite("FacilityDepartment",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Facility Department&nbsp;</b></p></td><td valign=top><p><%=FacilityAffiliation.getFacilityDepartment()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=FacilityDepartment&amp;sTableName=tFacilityAffiliation&amp;sRefID=<%=FacilityAffiliation.getAffiliationID()%>&amp;sFieldNameDisp=<%=FacilityAffiliation.getEnglish("FacilityDepartment")%>&amp;sTableNameDisp=tFacilityAffiliation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((FacilityAffiliation.isRead("FacilityDepartment",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Facility Department&nbsp;</b></p></td><td valign=top><p><%=FacilityAffiliation.getFacilityDepartment()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=FacilityDepartment&amp;sTableName=tFacilityAffiliation&amp;sRefID=<%=FacilityAffiliation.getAffiliationID()%>&amp;sFieldNameDisp=<%=FacilityAffiliation.getEnglish("FacilityDepartment")%>&amp;sTableNameDisp=tFacilityAffiliation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (FacilityAffiliation.isRequired("FacilityAddress1",UserSecurityGroupID))&&(!FacilityAffiliation.isComplete("FacilityAddress1")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((FacilityAffiliation.isExpired("FacilityAddress1",expiredDays))&&(FacilityAffiliation.isExpiredCheck("FacilityAddress1",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((FacilityAffiliation.isWrite("FacilityAddress1",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Facility Address&nbsp;</b></p></td><td valign=top><p><%=FacilityAffiliation.getFacilityAddress1()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=FacilityAddress1&amp;sTableName=tFacilityAffiliation&amp;sRefID=<%=FacilityAffiliation.getAffiliationID()%>&amp;sFieldNameDisp=<%=FacilityAffiliation.getEnglish("FacilityAddress1")%>&amp;sTableNameDisp=tFacilityAffiliation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((FacilityAffiliation.isRead("FacilityAddress1",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Facility Address&nbsp;</b></p></td><td valign=top><p><%=FacilityAffiliation.getFacilityAddress1()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=FacilityAddress1&amp;sTableName=tFacilityAffiliation&amp;sRefID=<%=FacilityAffiliation.getAffiliationID()%>&amp;sFieldNameDisp=<%=FacilityAffiliation.getEnglish("FacilityAddress1")%>&amp;sTableNameDisp=tFacilityAffiliation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (FacilityAffiliation.isRequired("FacilityAddress2",UserSecurityGroupID))&&(!FacilityAffiliation.isComplete("FacilityAddress2")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((FacilityAffiliation.isExpired("FacilityAddress2",expiredDays))&&(FacilityAffiliation.isExpiredCheck("FacilityAddress2",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((FacilityAffiliation.isWrite("FacilityAddress2",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Address 2&nbsp;</b></p></td><td valign=top><p><%=FacilityAffiliation.getFacilityAddress2()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=FacilityAddress2&amp;sTableName=tFacilityAffiliation&amp;sRefID=<%=FacilityAffiliation.getAffiliationID()%>&amp;sFieldNameDisp=<%=FacilityAffiliation.getEnglish("FacilityAddress2")%>&amp;sTableNameDisp=tFacilityAffiliation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((FacilityAffiliation.isRead("FacilityAddress2",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Address 2&nbsp;</b></p></td><td valign=top><p><%=FacilityAffiliation.getFacilityAddress2()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=FacilityAddress2&amp;sTableName=tFacilityAffiliation&amp;sRefID=<%=FacilityAffiliation.getAffiliationID()%>&amp;sFieldNameDisp=<%=FacilityAffiliation.getEnglish("FacilityAddress2")%>&amp;sTableNameDisp=tFacilityAffiliation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (FacilityAffiliation.isRequired("FacilityCity",UserSecurityGroupID))&&(!FacilityAffiliation.isComplete("FacilityCity")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((FacilityAffiliation.isExpired("FacilityCity",expiredDays))&&(FacilityAffiliation.isExpiredCheck("FacilityCity",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((FacilityAffiliation.isWrite("FacilityCity",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>City, Town, Province&nbsp;</b></p></td><td valign=top><p><%=FacilityAffiliation.getFacilityCity()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=FacilityCity&amp;sTableName=tFacilityAffiliation&amp;sRefID=<%=FacilityAffiliation.getAffiliationID()%>&amp;sFieldNameDisp=<%=FacilityAffiliation.getEnglish("FacilityCity")%>&amp;sTableNameDisp=tFacilityAffiliation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((FacilityAffiliation.isRead("FacilityCity",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>City, Town, Province&nbsp;</b></p></td><td valign=top><p><%=FacilityAffiliation.getFacilityCity()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=FacilityCity&amp;sTableName=tFacilityAffiliation&amp;sRefID=<%=FacilityAffiliation.getAffiliationID()%>&amp;sFieldNameDisp=<%=FacilityAffiliation.getEnglish("FacilityCity")%>&amp;sTableNameDisp=tFacilityAffiliation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (FacilityAffiliation.isRequired("FacilityStateID",UserSecurityGroupID))&&(!FacilityAffiliation.isComplete("FacilityStateID")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((FacilityAffiliation.isExpired("FacilityStateID",expiredDays))&&(FacilityAffiliation.isExpiredCheck("FacilityStateID",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((FacilityAffiliation.isWrite("FacilityStateID",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>State&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tStateLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=FacilityAffiliation.getFacilityStateID()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=FacilityStateID&amp;sTableName=tFacilityAffiliation&amp;sRefID=<%=FacilityAffiliation.getAffiliationID()%>&amp;sFieldNameDisp=<%=FacilityAffiliation.getEnglish("FacilityStateID")%>&amp;sTableNameDisp=tFacilityAffiliation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((FacilityAffiliation.isRead("FacilityStateID",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>State&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tStateLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=FacilityAffiliation.getFacilityStateID()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=FacilityStateID&amp;sTableName=tFacilityAffiliation&amp;sRefID=<%=FacilityAffiliation.getAffiliationID()%>&amp;sFieldNameDisp=<%=FacilityAffiliation.getEnglish("FacilityStateID")%>&amp;sTableNameDisp=tFacilityAffiliation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (FacilityAffiliation.isRequired("FacilityProvince",UserSecurityGroupID))&&(!FacilityAffiliation.isComplete("FacilityProvince")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((FacilityAffiliation.isExpired("FacilityProvince",expiredDays))&&(FacilityAffiliation.isExpiredCheck("FacilityProvince",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((FacilityAffiliation.isWrite("FacilityProvince",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Province, District, State&nbsp;</b></p></td><td valign=top><p><%=FacilityAffiliation.getFacilityProvince()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=FacilityProvince&amp;sTableName=tFacilityAffiliation&amp;sRefID=<%=FacilityAffiliation.getAffiliationID()%>&amp;sFieldNameDisp=<%=FacilityAffiliation.getEnglish("FacilityProvince")%>&amp;sTableNameDisp=tFacilityAffiliation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((FacilityAffiliation.isRead("FacilityProvince",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Province, District, State&nbsp;</b></p></td><td valign=top><p><%=FacilityAffiliation.getFacilityProvince()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=FacilityProvince&amp;sTableName=tFacilityAffiliation&amp;sRefID=<%=FacilityAffiliation.getAffiliationID()%>&amp;sFieldNameDisp=<%=FacilityAffiliation.getEnglish("FacilityProvince")%>&amp;sTableNameDisp=tFacilityAffiliation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (FacilityAffiliation.isRequired("FacilityZIP",UserSecurityGroupID))&&(!FacilityAffiliation.isComplete("FacilityZIP")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((FacilityAffiliation.isExpired("FacilityZIP",expiredDays))&&(FacilityAffiliation.isExpiredCheck("FacilityZIP",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((FacilityAffiliation.isWrite("FacilityZIP",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>ZIP&nbsp;</b></p></td><td valign=top><p><%=FacilityAffiliation.getFacilityZIP()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=FacilityZIP&amp;sTableName=tFacilityAffiliation&amp;sRefID=<%=FacilityAffiliation.getAffiliationID()%>&amp;sFieldNameDisp=<%=FacilityAffiliation.getEnglish("FacilityZIP")%>&amp;sTableNameDisp=tFacilityAffiliation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((FacilityAffiliation.isRead("FacilityZIP",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>ZIP&nbsp;</b></p></td><td valign=top><p><%=FacilityAffiliation.getFacilityZIP()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=FacilityZIP&amp;sTableName=tFacilityAffiliation&amp;sRefID=<%=FacilityAffiliation.getAffiliationID()%>&amp;sFieldNameDisp=<%=FacilityAffiliation.getEnglish("FacilityZIP")%>&amp;sTableNameDisp=tFacilityAffiliation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (FacilityAffiliation.isRequired("FacilityCountryID",UserSecurityGroupID))&&(!FacilityAffiliation.isComplete("FacilityCountryID")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((FacilityAffiliation.isExpired("FacilityCountryID",expiredDays))&&(FacilityAffiliation.isExpiredCheck("FacilityCountryID",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((FacilityAffiliation.isWrite("FacilityCountryID",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Country&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tCountryLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=FacilityAffiliation.getFacilityCountryID()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=FacilityCountryID&amp;sTableName=tFacilityAffiliation&amp;sRefID=<%=FacilityAffiliation.getAffiliationID()%>&amp;sFieldNameDisp=<%=FacilityAffiliation.getEnglish("FacilityCountryID")%>&amp;sTableNameDisp=tFacilityAffiliation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((FacilityAffiliation.isRead("FacilityCountryID",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Country&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tCountryLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=FacilityAffiliation.getFacilityCountryID()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=FacilityCountryID&amp;sTableName=tFacilityAffiliation&amp;sRefID=<%=FacilityAffiliation.getAffiliationID()%>&amp;sFieldNameDisp=<%=FacilityAffiliation.getEnglish("FacilityCountryID")%>&amp;sTableNameDisp=tFacilityAffiliation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (FacilityAffiliation.isRequired("FacilityPhone",UserSecurityGroupID))&&(!FacilityAffiliation.isComplete("FacilityPhone")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((FacilityAffiliation.isExpired("FacilityPhone",expiredDays))&&(FacilityAffiliation.isExpiredCheck("FacilityPhone",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((FacilityAffiliation.isWrite("FacilityPhone",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Facility Phone Number (XXX-XXX-XXXX)&nbsp;</b></p></td><td valign=top><p><%=FacilityAffiliation.getFacilityPhone()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=FacilityPhone&amp;sTableName=tFacilityAffiliation&amp;sRefID=<%=FacilityAffiliation.getAffiliationID()%>&amp;sFieldNameDisp=<%=FacilityAffiliation.getEnglish("FacilityPhone")%>&amp;sTableNameDisp=tFacilityAffiliation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((FacilityAffiliation.isRead("FacilityPhone",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Facility Phone Number (XXX-XXX-XXXX)&nbsp;</b></p></td><td valign=top><p><%=FacilityAffiliation.getFacilityPhone()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=FacilityPhone&amp;sTableName=tFacilityAffiliation&amp;sRefID=<%=FacilityAffiliation.getAffiliationID()%>&amp;sFieldNameDisp=<%=FacilityAffiliation.getEnglish("FacilityPhone")%>&amp;sTableNameDisp=tFacilityAffiliation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (FacilityAffiliation.isRequired("FacilityFax",UserSecurityGroupID))&&(!FacilityAffiliation.isComplete("FacilityFax")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((FacilityAffiliation.isExpired("FacilityFax",expiredDays))&&(FacilityAffiliation.isExpiredCheck("FacilityFax",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((FacilityAffiliation.isWrite("FacilityFax",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Facility Fax (XXX-XXX-XXXX)&nbsp;</b></p></td><td valign=top><p><%=FacilityAffiliation.getFacilityFax()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=FacilityFax&amp;sTableName=tFacilityAffiliation&amp;sRefID=<%=FacilityAffiliation.getAffiliationID()%>&amp;sFieldNameDisp=<%=FacilityAffiliation.getEnglish("FacilityFax")%>&amp;sTableNameDisp=tFacilityAffiliation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((FacilityAffiliation.isRead("FacilityFax",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Facility Fax (XXX-XXX-XXXX)&nbsp;</b></p></td><td valign=top><p><%=FacilityAffiliation.getFacilityFax()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=FacilityFax&amp;sTableName=tFacilityAffiliation&amp;sRefID=<%=FacilityAffiliation.getAffiliationID()%>&amp;sFieldNameDisp=<%=FacilityAffiliation.getEnglish("FacilityFax")%>&amp;sTableNameDisp=tFacilityAffiliation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (FacilityAffiliation.isRequired("ContactName",UserSecurityGroupID))&&(!FacilityAffiliation.isComplete("ContactName")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((FacilityAffiliation.isExpired("ContactName",expiredDays))&&(FacilityAffiliation.isExpiredCheck("ContactName",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((FacilityAffiliation.isWrite("ContactName",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Contact Name&nbsp;</b></p></td><td valign=top><p><%=FacilityAffiliation.getContactName()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ContactName&amp;sTableName=tFacilityAffiliation&amp;sRefID=<%=FacilityAffiliation.getAffiliationID()%>&amp;sFieldNameDisp=<%=FacilityAffiliation.getEnglish("ContactName")%>&amp;sTableNameDisp=tFacilityAffiliation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((FacilityAffiliation.isRead("ContactName",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Contact Name&nbsp;</b></p></td><td valign=top><p><%=FacilityAffiliation.getContactName()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ContactName&amp;sTableName=tFacilityAffiliation&amp;sRefID=<%=FacilityAffiliation.getAffiliationID()%>&amp;sFieldNameDisp=<%=FacilityAffiliation.getEnglish("ContactName")%>&amp;sTableNameDisp=tFacilityAffiliation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (FacilityAffiliation.isRequired("ContactEmail",UserSecurityGroupID))&&(!FacilityAffiliation.isComplete("ContactEmail")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((FacilityAffiliation.isExpired("ContactEmail",expiredDays))&&(FacilityAffiliation.isExpiredCheck("ContactEmail",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((FacilityAffiliation.isWrite("ContactEmail",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Contact E-mail&nbsp;</b></p></td><td valign=top><p><%=FacilityAffiliation.getContactEmail()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ContactEmail&amp;sTableName=tFacilityAffiliation&amp;sRefID=<%=FacilityAffiliation.getAffiliationID()%>&amp;sFieldNameDisp=<%=FacilityAffiliation.getEnglish("ContactEmail")%>&amp;sTableNameDisp=tFacilityAffiliation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((FacilityAffiliation.isRead("ContactEmail",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Contact E-mail&nbsp;</b></p></td><td valign=top><p><%=FacilityAffiliation.getContactEmail()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ContactEmail&amp;sTableName=tFacilityAffiliation&amp;sRefID=<%=FacilityAffiliation.getAffiliationID()%>&amp;sFieldNameDisp=<%=FacilityAffiliation.getEnglish("ContactEmail")%>&amp;sTableNameDisp=tFacilityAffiliation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (FacilityAffiliation.isRequired("ReasonForLeaving",UserSecurityGroupID))&&(!FacilityAffiliation.isComplete("ReasonForLeaving")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((FacilityAffiliation.isExpired("ReasonForLeaving",expiredDays))&&(FacilityAffiliation.isExpiredCheck("ReasonForLeaving",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((FacilityAffiliation.isWrite("ReasonForLeaving",UserSecurityGroupID)))
            {
                        %>
                       <tr><td valign=top> <p class=<%=theClass%> ><b><%=FacilityAffiliation.getEnglish("ReasonForLeaving")%>&nbsp;</b></p></td><td valign=top><p><%=FacilityAffiliation.getReasonForLeaving()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ReasonForLeaving&amp;sTableName=tFacilityAffiliation&amp;sRefID=<%=FacilityAffiliation.getAffiliationID()%>&amp;sFieldNameDisp=<%=FacilityAffiliation.getEnglish("ReasonForLeaving")%>&amp;sTableNameDisp=tFacilityAffiliation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((FacilityAffiliation.isRead("ReasonForLeaving",UserSecurityGroupID)))
            {
                        %>
                       <tr><td valign=top> <p class=<%=theClass%> ><b><%=FacilityAffiliation.getEnglish("ReasonForLeaving")%>&nbsp;</b></p></td><td valign=top><p><%=FacilityAffiliation.getReasonForLeaving()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ReasonForLeaving&amp;sTableName=tFacilityAffiliation&amp;sRefID=<%=FacilityAffiliation.getAffiliationID()%>&amp;sFieldNameDisp=<%=FacilityAffiliation.getEnglish("ReasonForLeaving")%>&amp;sTableNameDisp=tFacilityAffiliation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (FacilityAffiliation.isRequired("AdmissionPriviledges",UserSecurityGroupID))&&(!FacilityAffiliation.isComplete("AdmissionPriviledges")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((FacilityAffiliation.isExpired("AdmissionPriviledges",expiredDays))&&(FacilityAffiliation.isExpiredCheck("AdmissionPriviledges",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((FacilityAffiliation.isWrite("AdmissionPriviledges",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Do you have admission privileges?&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=FacilityAffiliation.getAdmissionPriviledges()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=AdmissionPriviledges&amp;sTableName=tFacilityAffiliation&amp;sRefID=<%=FacilityAffiliation.getAffiliationID()%>&amp;sFieldNameDisp=<%=FacilityAffiliation.getEnglish("AdmissionPriviledges")%>&amp;sTableNameDisp=tFacilityAffiliation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((FacilityAffiliation.isRead("AdmissionPriviledges",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Do you have admission privileges?&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=FacilityAffiliation.getAdmissionPriviledges()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=AdmissionPriviledges&amp;sTableName=tFacilityAffiliation&amp;sRefID=<%=FacilityAffiliation.getAffiliationID()%>&amp;sFieldNameDisp=<%=FacilityAffiliation.getEnglish("AdmissionPriviledges")%>&amp;sTableNameDisp=tFacilityAffiliation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (FacilityAffiliation.isRequired("AdmissionArrangements",UserSecurityGroupID))&&(!FacilityAffiliation.isComplete("AdmissionArrangements")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((FacilityAffiliation.isExpired("AdmissionArrangements",expiredDays))&&(FacilityAffiliation.isExpiredCheck("AdmissionArrangements",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((FacilityAffiliation.isWrite("AdmissionArrangements",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>If no, Please explain your admission arrangements&nbsp;</b></p></td><td valign=top><p><%=FacilityAffiliation.getAdmissionArrangements()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=AdmissionArrangements&amp;sTableName=tFacilityAffiliation&amp;sRefID=<%=FacilityAffiliation.getAffiliationID()%>&amp;sFieldNameDisp=<%=FacilityAffiliation.getEnglish("AdmissionArrangements")%>&amp;sTableNameDisp=tFacilityAffiliation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((FacilityAffiliation.isRead("AdmissionArrangements",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>If no, Please explain your admission arrangements&nbsp;</b></p></td><td valign=top><p><%=FacilityAffiliation.getAdmissionArrangements()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=AdmissionArrangements&amp;sTableName=tFacilityAffiliation&amp;sRefID=<%=FacilityAffiliation.getAffiliationID()%>&amp;sFieldNameDisp=<%=FacilityAffiliation.getEnglish("AdmissionArrangements")%>&amp;sTableNameDisp=tFacilityAffiliation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (FacilityAffiliation.isRequired("UnrestrictedAdmission",UserSecurityGroupID))&&(!FacilityAffiliation.isComplete("UnrestrictedAdmission")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((FacilityAffiliation.isExpired("UnrestrictedAdmission",expiredDays))&&(FacilityAffiliation.isExpiredCheck("UnrestrictedAdmission",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((FacilityAffiliation.isWrite("UnrestrictedAdmission",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Do you have unresricted admission privileges&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=FacilityAffiliation.getUnrestrictedAdmission()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=UnrestrictedAdmission&amp;sTableName=tFacilityAffiliation&amp;sRefID=<%=FacilityAffiliation.getAffiliationID()%>&amp;sFieldNameDisp=<%=FacilityAffiliation.getEnglish("UnrestrictedAdmission")%>&amp;sTableNameDisp=tFacilityAffiliation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((FacilityAffiliation.isRead("UnrestrictedAdmission",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Do you have unresricted admission privileges&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=FacilityAffiliation.getUnrestrictedAdmission()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=UnrestrictedAdmission&amp;sTableName=tFacilityAffiliation&amp;sRefID=<%=FacilityAffiliation.getAffiliationID()%>&amp;sFieldNameDisp=<%=FacilityAffiliation.getEnglish("UnrestrictedAdmission")%>&amp;sTableNameDisp=tFacilityAffiliation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (FacilityAffiliation.isRequired("TempPriviledges",UserSecurityGroupID))&&(!FacilityAffiliation.isComplete("TempPriviledges")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((FacilityAffiliation.isExpired("TempPriviledges",expiredDays))&&(FacilityAffiliation.isExpiredCheck("TempPriviledges",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((FacilityAffiliation.isWrite("TempPriviledges",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Do you have temporary privileges?&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=FacilityAffiliation.getTempPriviledges()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=TempPriviledges&amp;sTableName=tFacilityAffiliation&amp;sRefID=<%=FacilityAffiliation.getAffiliationID()%>&amp;sFieldNameDisp=<%=FacilityAffiliation.getEnglish("TempPriviledges")%>&amp;sTableNameDisp=tFacilityAffiliation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((FacilityAffiliation.isRead("TempPriviledges",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Do you have temporary privileges?&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=FacilityAffiliation.getTempPriviledges()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=TempPriviledges&amp;sTableName=tFacilityAffiliation&amp;sRefID=<%=FacilityAffiliation.getAffiliationID()%>&amp;sFieldNameDisp=<%=FacilityAffiliation.getEnglish("TempPriviledges")%>&amp;sTableNameDisp=tFacilityAffiliation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (FacilityAffiliation.isRequired("InpatientCare",UserSecurityGroupID))&&(!FacilityAffiliation.isComplete("InpatientCare")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((FacilityAffiliation.isExpired("InpatientCare",expiredDays))&&(FacilityAffiliation.isExpiredCheck("InpatientCare",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((FacilityAffiliation.isWrite("InpatientCare",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Do you provide inpatient care?&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=FacilityAffiliation.getInpatientCare()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=InpatientCare&amp;sTableName=tFacilityAffiliation&amp;sRefID=<%=FacilityAffiliation.getAffiliationID()%>&amp;sFieldNameDisp=<%=FacilityAffiliation.getEnglish("InpatientCare")%>&amp;sTableNameDisp=tFacilityAffiliation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((FacilityAffiliation.isRead("InpatientCare",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Do you provide inpatient care?&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=FacilityAffiliation.getInpatientCare()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=InpatientCare&amp;sTableName=tFacilityAffiliation&amp;sRefID=<%=FacilityAffiliation.getAffiliationID()%>&amp;sFieldNameDisp=<%=FacilityAffiliation.getEnglish("InpatientCare")%>&amp;sTableNameDisp=tFacilityAffiliation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (FacilityAffiliation.isRequired("PercentAdmissions",UserSecurityGroupID))&&(!FacilityAffiliation.isComplete("PercentAdmissions")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((FacilityAffiliation.isExpired("PercentAdmissions",expiredDays))&&(FacilityAffiliation.isExpiredCheck("PercentAdmissions",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((FacilityAffiliation.isWrite("PercentAdmissions",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Percent Admissions&nbsp;</b></p></td><td valign=top><p><%=FacilityAffiliation.getPercentAdmissions()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PercentAdmissions&amp;sTableName=tFacilityAffiliation&amp;sRefID=<%=FacilityAffiliation.getAffiliationID()%>&amp;sFieldNameDisp=<%=FacilityAffiliation.getEnglish("PercentAdmissions")%>&amp;sTableNameDisp=tFacilityAffiliation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((FacilityAffiliation.isRead("PercentAdmissions",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Percent Admissions&nbsp;</b></p></td><td valign=top><p><%=FacilityAffiliation.getPercentAdmissions()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PercentAdmissions&amp;sTableName=tFacilityAffiliation&amp;sRefID=<%=FacilityAffiliation.getAffiliationID()%>&amp;sFieldNameDisp=<%=FacilityAffiliation.getEnglish("PercentAdmissions")%>&amp;sTableNameDisp=tFacilityAffiliation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (FacilityAffiliation.isRequired("Comments",UserSecurityGroupID))&&(!FacilityAffiliation.isComplete("Comments")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((FacilityAffiliation.isExpired("Comments",expiredDays))&&(FacilityAffiliation.isExpiredCheck("Comments",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((FacilityAffiliation.isWrite("Comments",UserSecurityGroupID)))
            {
                        %>
                       <tr><td valign=top> <p class=<%=theClass%> ><b><%=FacilityAffiliation.getEnglish("Comments")%>&nbsp;</b></p></td><td valign=top><p><%=FacilityAffiliation.getComments()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Comments&amp;sTableName=tFacilityAffiliation&amp;sRefID=<%=FacilityAffiliation.getAffiliationID()%>&amp;sFieldNameDisp=<%=FacilityAffiliation.getEnglish("Comments")%>&amp;sTableNameDisp=tFacilityAffiliation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((FacilityAffiliation.isRead("Comments",UserSecurityGroupID)))
            {
                        %>
                       <tr><td valign=top> <p class=<%=theClass%> ><b><%=FacilityAffiliation.getEnglish("Comments")%>&nbsp;</b></p></td><td valign=top><p><%=FacilityAffiliation.getComments()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Comments&amp;sTableName=tFacilityAffiliation&amp;sRefID=<%=FacilityAffiliation.getAffiliationID()%>&amp;sFieldNameDisp=<%=FacilityAffiliation.getEnglish("Comments")%>&amp;sTableNameDisp=tFacilityAffiliation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>




            <tr><td width=40%>&nbsp;</td><td width=60%>&nbsp;</td></tr>



        </td></tr></table>
        </td></tr></table>
        <%
  }%>

        </td></tr></table></table><br>        <%
    }
    %>




    </table>
    <%
  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>

    </td></tr></table>


