<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.bltPhysicianUserAccountLU,com.winstaff.bltPhysicianUserAccountLU_List_LU_PhysicianID,com.winstaff.SecurityCheck,com.winstaff.ConfigurationMessages,com.winstaff.PLCUtils,java.util.Vector,java.util.Enumeration,com.winstaff.ListElement,com.winstaff.bltPhysicianMaster,com.winstaff.stepVerify_PhysicianID" %>
<%/*
    filename: out\jsp\tPhysicianMaster_form.jsp
    JSP AutoGen on Mar/02/2002
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>

<%
//String thePLCID = (String)pageControllerHash.get("plcID");
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_PhysicianID.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>


<table width=<%=MasterTableWidth%>>
  <tr> 
    <td width=10>&nbsp;</td>
    <td class=tdBase><%=ConfigurationMessages.getHTML("INTERVIEWTopControl","pro-file_Attestation")%></td>
  </tr>
  <tr > 
    <td width=10>&nbsp;</td>
    <td >&nbsp;</td>
  </tr>
  <tr > 
    <td width=10>&nbsp;</td>
    <td class=tdHeaderAlt> 
      <p class=title> 
        <%
//initial declaration of list class and parentID
    Integer        iPhysicianID        =    null;
    Integer        iUserID        =    null;
    boolean accessValid = true;
    // required for Type2
    String sKeyMasterReference = null;

   Integer iSecurityCheck = SecurityCheck.CheckItem("PractitionerAttestation1", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {

   if (pageControllerHash.containsKey("iPhysicianID")) 
   {
        iPhysicianID        =    (Integer)pageControllerHash.get("iPhysicianID");
   }
   else
   {
        accessValid = false;    
   }

  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);      java.text.SimpleDateFormat displayDateTimeSDF = new java.text.SimpleDateFormat("MM/dd/yyyy hh:mm a");

	bltPhysicianMaster        PhysicianMaster        =   new  bltPhysicianMaster (iPhysicianID);





    bltPhysicianUserAccountLU_List_LU_PhysicianID        bltPhysicianUserAccountLU_List_LU_PhysicianID        =    new    bltPhysicianUserAccountLU_List_LU_PhysicianID(iPhysicianID);
    bltPhysicianUserAccountLU        working_bltPhysicianUserAccountLU;
    ListElement         leCurrentElement;
    java.util.Enumeration eList = bltPhysicianUserAccountLU_List_LU_PhysicianID.elements();
    if (eList.hasMoreElements())
    {        
        leCurrentElement    = (ListElement) eList.nextElement();
        working_bltPhysicianUserAccountLU  = (bltPhysicianUserAccountLU) leCurrentElement.getObject();
        bltUserAccount working_bltUserAccount  = new bltUserAccount(working_bltPhysicianUserAccountLU.getUserID());
	

	String Keyword1 = request.getParameter("AttestKeyword1");
	String Keyword2 = request.getParameter("AttestKeyword2");
	String userName = request.getParameter("LogonUserName");
	String Password1 = request.getParameter("LogonUserPassword");

	com.winstaff.password.Encrypt myEnc = new com.winstaff.password.Encrypt();
//old 2 keywords	if ( (userName.equalsIgnoreCase(working_bltUserAccount.getLogonUserName()))&&(myEnc.checkPasswords(working_bltUserAccount.getLogonUserPassword(), myEnc.getMD5Base64(Password1))) && ( myEnc.checkAttestation( myEnc.getMD5Base64(Keyword1),working_bltUserAccount.getAttestKeyword1(), myEnc.getMD5Base64(Keyword2),working_bltUserAccount.getAttestKeyword2()) ) )
	if ( (userName.equalsIgnoreCase(working_bltUserAccount.getLogonUserName()))&&(myEnc.checkPasswords(working_bltUserAccount.getLogonUserPassword(), myEnc.getMD5Base64(Password1))) && ( myEnc.checkPasswords( myEnc.getMD5Base64(Keyword1),working_bltUserAccount.getAttestKeyword1())  ) )
	{
		if (working_bltUserAccount.getStatus().intValue()==2)
		{
			PhysicianMaster.setIsAttested(new Integer("1"));
			PhysicianMaster.setAttestationComments("Attested by["+CurrentUserAccount.getLogonUserName()+"]");
			PhysicianMaster.setAttestDate(new java.util.Date());
			%>
        <i><%=displayDateSDF.format(PhysicianMaster.getAttestDate())%><br>
        </i> Your attestation was successful. Please <a href="tDocumentManagement_PhysicianID.jsp">click 
        here</a> to continue. 
        <%
			PhysicianMaster.commitData();
			PhysicianMaster.setIsAttested(new Integer("1"));
			PhysicianMaster.commitDataForced();
		}
		else
		{
			PhysicianMaster.setAttestationComments("Pending Account Approval");
			PhysicianMaster.setAttestDatePending(new java.util.Date());
			%>
        <i><%=displayDateSDF.format(PhysicianMaster.getAttestDatePending())%><br>
        </i> Your attestation was successful, however since your account has still 
        not been approved, your attestation will be "pending" until we receive 
        your "Attestation Consent" Form and processed it. If you have not already 
        done so in the previous step, print your "Attestation Consent" Form by 
        <a href="tPhysicianUserAccountLU_main2_UserAccount_PhysicianID.jsp">clicking 
        here</a>. Once we receive and process this form, your account will become 
        fully active. 
        <%
			PhysicianMaster.commitData();
			PhysicianMaster.setIsAttestedPending(new Integer("1"));
			PhysicianMaster.commitDataForced();
		}
	}
	else
	{
		%>
        Sorry your attestation failed. Please <a href="pro-file_Attestation.jsp">try 
        again</a>. 
        <%
	}

   }
   else
   {
 	%>
        You have not yet created an attestation account. To do so <a href="tPhysicianUserAccountLU_main2_UserAccount_PhysicianID.jsp">go 
        here</a> and try again. 
        <%
   }
%>
        <%
  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>
      </p><br>
    </td>
  </tr>
  <tr >
    <td width=10>&nbsp;</td>
    <td >&nbsp;</td>
  </tr>
</table>



<br><br>
<%String bnIncludeFN = "ui_"+thePLCID+"\\bot-nav_PhysicianID.jsp";
%>
<jsp:include page="<%=bnIncludeFN%>" flush="true" ><jsp:param name="plcID" value="<%=thePLCID%>"/></jsp:include>
