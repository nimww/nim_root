<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.searchDB2,com.winstaff.PLCUtils,java.util.Vector,java.util.Enumeration,com.winstaff.ListElement,com.winstaff.bltAudit" %><title>Audit By Field</title>
<script language="JavaScript">
<!--
function MM_callJS(jsStr) { //v2.0
  return eval(jsStr)
}
//-->
</script>
<body bgcolor="#FFFFFF" leftmargin="3" topmargin="2" marginwidth="3" marginheight="2">
<div align="center"><a href=# onClick="MM_callJS('window.close()');return false;">close</a> 
</div>
<link rel="stylesheet" href="ui_1\style_PhysicianID.css" type="text/css">
<%
//initial declaration of list class and parentID
    Integer        iProfessionalLicenseID        =    null;
    boolean accessValid = false;
    // required for Type2
    String sKeyMasterReference = null;
    String sFieldName = null;
    String sTableName = null;
    String sRefID = null;
    String sFieldNameDisp = null;
    String sTableNameDisp = null;
    try
    {
        sFieldName = request.getParameter("sFieldName");
        sTableName = request.getParameter("sTableName");
        sRefID = request.getParameter("sRefID");
        sFieldNameDisp = request.getParameter("sFieldNameDisp");
        sTableNameDisp= request.getParameter("sTableNameDisp");
        accessValid = true;
    }
    catch(Exception e)
    {
    }
  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);


    searchDB2 mySS = new searchDB2();

    java.sql.ResultSet myRS = null;


    try
    {
       //out.print("<hr>select AuditID from tAudit where ( upper(fieldName) = upper('"+sFieldName+"') and upper(tableName) = upper('"+sTableName+"') and refID = '"+sRefID+"')<hr>");
       myRS = mySS.executeStatement("select AuditID from tAudit where ( upper(fieldName) = upper('"+sFieldName+"') and upper(tableName) = upper('"+sTableName+"') and refID = '"+sRefID+"')");
    }
    catch(Exception e)
    {
        out.println("ResultsSet:"+e);
    }
%>
<p class=Instructions>
<b>Audit Trail</b><br>
<br>Field: <%=sFieldNameDisp%>
<br>Record: <%=sTableNameDisp%>
<br>Record ID: <%=sRefID%>
</p>
<table border = 1 class=tdBase cellpadding=3 cellspacing =0 width=325>
<tr class=tdHeaderAlt>
<td>#</td>
<td>Date</td>
<td>New Value</td>
<td>Modified By</td>
</tr>

<%
    int cnt=0;
    try
    {
        while (myRS!=null&&myRS.next())
        {
            cnt++;
            String myClass="tdBase";
            if (cnt%2==0)
            {
                myClass = "tdBaseAlt";
            }
	    bltAudit myAudit = new bltAudit (new Integer(myRS.getString("AuditID")));
	    out.println("<tr class="+myClass+">");
	    out.println("<td align=Center valign=top>"+myAudit.getAuditID()+"</td>");
	    out.println("<td align=Center valign=top>"+displayDateSDF.format(myAudit.getAuditDate())+"</td>");
	    out.println("<td>"+myAudit.getValueChange()+"</td>");
	    out.println("<td>"+myAudit.getModifier()+"</td>");
//	    out.println("<td>"+myAudit.getComments()+"</td>");
	    out.println("</tr>");
        }
    }
    catch(Exception e)
    {
        out.println("ResultsSet:"+e);
    }
    mySS.closeAll();

  }
  else
  {
   response.sendRedirect("/errorhandler.jsp");
  }

%>
</table>
<div align="center"><br>
  <a href=# onClick="MM_callJS('window.close()');return false;">close</a> </div>
