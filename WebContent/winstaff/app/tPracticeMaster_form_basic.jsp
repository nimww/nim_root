<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.bltPhysicianPracticeLU,com.winstaff.bltDocumentManagement,java.util.Vector,java.util.Enumeration,com.winstaff.ListElement, com.winstaff.PLCUtils, com.winstaff.SecurityCheck, com.winstaff.ConfigurationMessages,com.winstaff.bltPracticeMaster" %>
<%/*

    filename: out\jsp\tPracticeMaster_form.jsp
    Created on Feb/06/2003
    Created by: Scott Ellis
*/%>
<%@ include file="../generic/generalDisplay.jsp" %>
<%@ include file="../generic/CheckLogin.jsp" %>



<script language = javascript>
function BillingFill()
{
	document.forms[0].BillingAddress1.value = document.forms[0].OfficeAddress1.value;
	document.forms[0].BillingAddress2.value = document.forms[0].OfficeAddress2.value;
	document.forms[0].BillingCity.value = document.forms[0].OfficeCity.value;
	document.forms[0].BillingProvince.value = document.forms[0].OfficeProvince.value;
	document.forms[0].BillingStateID.value = document.forms[0].OfficeStateID.value;
	document.forms[0].BillingZIP.value = document.forms[0].OfficeZIP.value;
	document.forms[0].BillingCountryID.value = document.forms[0].OfficeCountryID.value;
	document.forms[0].BillingPhone.value = document.forms[0].OfficePhone.value;
	document.forms[0].BillingFax.value = document.forms[0].OfficeFaxNo.value;
	document.forms[0].BillingFirstName.value = document.forms[0].OfficeManagerFirstName.value;
	document.forms[0].BillingLastName.value = document.forms[0].OfficeManagerLastName.value;
	document.forms[0].BillingPayableTo.value = document.forms[0].OfficeTaxIDNameAffiliation.value;
	return false;
}

function CredFill()
{
	document.forms[0].CredentialingContactAddress1.value = document.forms[0].OfficeAddress1.value;
	document.forms[0].CredentialingContactAddress2.value = document.forms[0].OfficeAddress2.value;
	document.forms[0].CredentialingContactCity.value = document.forms[0].OfficeCity.value;
	document.forms[0].CredentialingContactProvince.value = document.forms[0].OfficeProvince.value;
	document.forms[0].CredentialingContactStateID.value = document.forms[0].OfficeStateID.value;
	document.forms[0].CredentialingContactZIP.value = document.forms[0].OfficeZIP.value;
	document.forms[0].CredentialingContactCountryID.value = document.forms[0].OfficeCountryID.value;
	document.forms[0].CredentialingContactPhone.value = document.forms[0].OfficePhone.value;
	document.forms[0].CredentialingContactFax.value = document.forms[0].OfficeFaxNo.value;
	document.forms[0].CredentialingContactFirstName.value = document.forms[0].OfficeManagerFirstName.value;
	document.forms[0].CredentialingContactLastName.value = document.forms[0].OfficeManagerLastName.value;	
	document.forms[0].CredentiallingContactEmail.value = document.forms[0].OfficeManagerEmail.value;	

	return false;
}

</script>



<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_PhysicianID.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<table cellpadding=0 cellspacing=0 border=0 width=<%=MasterTableWidth%> >
  <tr> 
    <td width=10>&nbsp;</td>
    <td> <%=ConfigurationMessages.getHTML("INTERVIEWTopControl_form","tPracticeMasterBasic")%> 
      <%
//initial declaration of list class and parentID
    Integer        iPracticeID        =    null;
    Integer        iLookupID        =    null;
    boolean accessValid = false;
    // required for Type2
    String sKeyMasterReference = null;

   Integer iSecurityCheck = SecurityCheck.CheckItem("Practice1", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {
    if (pageControllerHash.containsKey("iPracticeID")&&pageControllerHash.containsKey("iLookupID")) 
    {
        iPracticeID        =    (Integer)pageControllerHash.get("iPracticeID");
        iLookupID        =    (Integer)pageControllerHash.get("iLookupID");
        accessValid = true;    }
  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);      java.text.SimpleDateFormat displayDateTimeSDF = new java.text.SimpleDateFormat("MM/dd/yyyy hh:mm a");

    pageControllerHash.put("sLocalChildReturnPage","tPracticeMaster_form2_basic.jsp?EDIT=edit");
    pageControllerHash.put("sParentReturnPage","tPracticeMaster_form2_basic.jsp?EDIT=edit");
    session.setAttribute("pageControllerHash",pageControllerHash);

//initial declaration of list class and parentID

    bltPracticeMaster        PracticeMaster        =    null;

    bltPhysicianPracticeLU        PhysicianPracticeLU        =    new bltPhysicianPracticeLU(iLookupID);

    if ( request.getParameter( "EDIT" ).equalsIgnoreCase("edit") )
    {
        PracticeMaster        =    new    bltPracticeMaster(iPracticeID,UserSecurityGroupID);
    }
    else if ( request.getParameter( "EDIT" ).equalsIgnoreCase("new") )
    {
        PracticeMaster        =    new    bltPracticeMaster(UserSecurityGroupID, true);
    }

//fields
        %>
      <%@ include file="tPracticeMaster_form_instructions.jsp" %>
      <form action="tPracticeMaster_form_basic_sub.jsp" name="tPracticeMaster_form1" method="POST">
        <%
    if ( request.getParameter( "EDIT" ) != null )
    {
    %>
        <input type="hidden" name="EDIT" value = "<%=request.getParameter( "EDIT" )%>" >
        <%
    }
%>
        <%  String theClass ="tdBase";%>
        <table cellpadding="1" cellspacing="0" border="0" width="100%" class=tdBase>
          <tr>
            <td><b>Page 1 - Core Information</b></td>
            <td>
              <div align="right">
                <input <%=HTMLFormStyleButton%> type=Submit value=Continue name=Submit2>
              </div>
            </td></tr>
			</table>
        <table width=100% border=1 bordercolor=#333333 cellpadding=3 cellspacing=0 class=tableBase>
          <tr> 
            <td class=tableColor> 
              <table cellpadding=0 cellspacing=0 width=100%>
                <tr>
                  <td  class=title colspan=2>Practice Affiliation:</td>
                </tr>
                <tr>
                  <td colspan=2>&nbsp;</td>
                </tr>
                <%
            if ( (PhysicianPracticeLU.isRequired("StartDate",UserSecurityGroupID))&&(!PhysicianPracticeLU.isComplete("StartDate")) )
            {
                theClass = "requiredField";
            }
            else if ((PhysicianPracticeLU.isExpired("StartDate",expiredDays))&&(PhysicianPracticeLU.isExpiredCheck("StartDate",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if ((PhysicianPracticeLU.isWrite("StartDate",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Start Date&nbsp;(mm/dd/yyyy):&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p> 
                      <input maxlength=10  type=text size="80" name="StartDate" value='<jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(PhysicianPracticeLU.getStartDate())%>" /></jsp:include>' >&nbsp;<%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=StartDate&amp;sTableName=tPhysicianPracticeLU&amp;sRefID=<%=PhysicianPracticeLU.getLookupID()%>&amp;sFieldNameDisp=<%=PhysicianPracticeLU.getEnglish("StartDate")%>&amp;sTableNameDisp=tPhysicianPracticeLU','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((PhysicianPracticeLU.isRead("StartDate",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Start Date&nbsp;(mm/dd/yyyy):&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p> 
                      <jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" > 
                      <jsp:param name="CurrentSelection" value="<%=dbdf.format(PhysicianPracticeLU.getStartDate())%>" />
                      </jsp:include>
&nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=StartDate&amp;sTableName=tPhysicianPracticeLU&amp;sRefID=<%=PhysicianPracticeLU.getLookupID()%>&amp;sFieldNameDisp=<%=PhysicianPracticeLU.getEnglish("StartDate")%>&amp;sTableNameDisp=tPhysicianPracticeLU','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (PhysicianPracticeLU.isRequired("EndDate",UserSecurityGroupID))&&(!PhysicianPracticeLU.isComplete("EndDate")) )
            {
                theClass = "requiredField";
            }
            else if ((PhysicianPracticeLU.isExpired("EndDate",expiredDays))&&(PhysicianPracticeLU.isExpiredCheck("EndDate",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if ((PhysicianPracticeLU.isWrite("EndDate",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>End Date&nbsp;(mm/dd/yyyy):&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p> 
                      <input maxlength=10  type=text size="80" name="EndDate" value='<jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(PhysicianPracticeLU.getEndDate())%>" /></jsp:include>' >
&nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=EndDate&amp;sTableName=tPhysicianPracticeLU&amp;sRefID=<%=PhysicianPracticeLU.getLookupID()%>&amp;sFieldNameDisp=<%=PhysicianPracticeLU.getEnglish("EndDate")%>&amp;sTableNameDisp=tPhysicianPracticeLU','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((PhysicianPracticeLU.isRead("EndDate",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>End Date&nbsp;(mm/dd/yyyy):&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p> 
                      <jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" > 
                      <jsp:param name="CurrentSelection" value="<%=dbdf.format(PhysicianPracticeLU.getEndDate())%>" />
                      </jsp:include>
&nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=EndDate&amp;sTableName=tPhysicianPracticeLU&amp;sRefID=<%=PhysicianPracticeLU.getLookupID()%>&amp;sFieldNameDisp=<%=PhysicianPracticeLU.getEnglish("EndDate")%>&amp;sTableNameDisp=tPhysicianPracticeLU','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (PhysicianPracticeLU.isRequired("isPrimaryOffice",UserSecurityGroupID))&&(!PhysicianPracticeLU.isComplete("isPrimaryOffice")) )
            {
                theClass = "requiredField";
            }
            else if ((PhysicianPracticeLU.isExpired("isPrimaryOffice",expiredDays))&&(PhysicianPracticeLU.isExpiredCheck("isPrimaryOffice",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if ((PhysicianPracticeLU.isWrite("isPrimaryOffice",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Is this your primary office?&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <select   name="isPrimaryOffice" >
                        <jsp:include page="../generic/tYesNoLILong.jsp" flush="true" >
                        <jsp:param name="CurrentSelection" value="<%=PhysicianPracticeLU.getisPrimaryOffice()%>" />
                        </jsp:include>
                      </select>
&nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=isPrimaryOffice&amp;sTableName=tPhysicianPracticeLU&amp;sRefID=<%=PhysicianPracticeLU.getLookupID()%>&amp;sFieldNameDisp=<%=PhysicianPracticeLU.getEnglish("isPrimaryOffice")%>&amp;sTableNameDisp=tPhysicianPracticeLU','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((PhysicianPracticeLU.isRead("isPrimaryOffice",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Is this your primary office?&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" >
                      <jsp:param name="CurrentSelection" value="<%=PhysicianPracticeLU.getisPrimaryOffice()%>" />
                      </jsp:include>
&nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=isPrimaryOffice&amp;sTableName=tPhysicianPracticeLU&amp;sRefID=<%=PhysicianPracticeLU.getLookupID()%>&amp;sFieldNameDisp=<%=PhysicianPracticeLU.getEnglish("isPrimaryOffice")%>&amp;sTableNameDisp=tPhysicianPracticeLU','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (PhysicianPracticeLU.isRequired("isAdministrativeOffice",UserSecurityGroupID))&&(!PhysicianPracticeLU.isComplete("isAdministrativeOffice")) )
            {
                theClass = "requiredField";
            }
            else if ((PhysicianPracticeLU.isExpired("isAdministrativeOffice",expiredDays))&&(PhysicianPracticeLU.isExpiredCheck("isAdministrativeOffice",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if ((PhysicianPracticeLU.isWrite("isAdministrativeOffice",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Is this your administrative office?&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <select   name="isAdministrativeOffice" >
                        <jsp:include page="../generic/tYesNoLILong.jsp" flush="true" >
                        <jsp:param name="CurrentSelection" value="<%=PhysicianPracticeLU.getisAdministrativeOffice()%>" />
                        </jsp:include>
                      </select>
&nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=isAdministrativeOffice&amp;sTableName=tPhysicianPracticeLU&amp;sRefID=<%=PhysicianPracticeLU.getLookupID()%>&amp;sFieldNameDisp=<%=PhysicianPracticeLU.getEnglish("isAdministrativeOffice")%>&amp;sTableNameDisp=tPhysicianPracticeLU','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((PhysicianPracticeLU.isRead("isAdministrativeOffice",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Is this your administrative office?&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" >
                      <jsp:param name="CurrentSelection" value="<%=PhysicianPracticeLU.getisAdministrativeOffice()%>" />
                      </jsp:include>
&nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=isAdministrativeOffice&amp;sTableName=tPhysicianPracticeLU&amp;sRefID=<%=PhysicianPracticeLU.getLookupID()%>&amp;sFieldNameDisp=<%=PhysicianPracticeLU.getEnglish("isAdministrativeOffice")%>&amp;sTableNameDisp=tPhysicianPracticeLU','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (PhysicianPracticeLU.isRequired("CoverageHours",UserSecurityGroupID))&&(!PhysicianPracticeLU.isComplete("CoverageHours")) )
            {
                theClass = "requiredField";
            }
            else if ((PhysicianPracticeLU.isExpired("CoverageHours",expiredDays))&&(PhysicianPracticeLU.isExpiredCheck("CoverageHours",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if ((PhysicianPracticeLU.isWrite("CoverageHours",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b><%=PhysicianPracticeLU.getEnglish("CoverageHours")%>&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p> 
                      <textarea onKeyDown="textAreaStop(this,200)" rows="2" name="CoverageHours" cols="40" maxlength=200><%=PhysicianPracticeLU.getCoverageHours()%></textarea>
&nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CoverageHours&amp;sTableName=tPhysicianPracticeLU&amp;sRefID=<%=PhysicianPracticeLU.getLookupID()%>&amp;sFieldNameDisp=<%=PhysicianPracticeLU.getEnglish("CoverageHours")%>&amp;sTableNameDisp=tPhysicianPracticeLU','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((PhysicianPracticeLU.isRead("CoverageHours",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b><%=PhysicianPracticeLU.getEnglish("CoverageHours")%>&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p><%=PhysicianPracticeLU.getCoverageHours()%>&nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CoverageHours&amp;sTableName=tPhysicianPracticeLU&amp;sRefID=<%=PhysicianPracticeLU.getLookupID()%>&amp;sFieldNameDisp=<%=PhysicianPracticeLU.getEnglish("CoverageHours")%>&amp;sTableNameDisp=tPhysicianPracticeLU','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <tr>
                  <td colspan=2>
                    <hr noshade>
                  </td>
                </tr>
                <tr class=title>
                  <td colspan=2 class=title >General Practice Information:</td>
                </tr>
                <tr>
                  <td colspan=2>&nbsp;</td>
                </tr>
                <%
            if ( (PracticeMaster.isRequired("PracticeName",UserSecurityGroupID))&&(!PracticeMaster.isComplete("PracticeName")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("PracticeName",expiredDays))&&(PracticeMaster.isExpiredCheck("PracticeName",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if ((PracticeMaster.isWrite("PracticeName",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Practice Name&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <input maxlength="100" type=text size="80" name="PracticeName" value="<%=PracticeMaster.getPracticeName()%>">
&nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PracticeName&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("PracticeName")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((PracticeMaster.isRead("PracticeName",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Practice Name&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p><%=PracticeMaster.getPracticeName()%>&nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PracticeName&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("PracticeName")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (PracticeMaster.isRequired("DepartmentName",UserSecurityGroupID))&&(!PracticeMaster.isComplete("DepartmentName")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("DepartmentName",expiredDays))&&(PracticeMaster.isExpiredCheck("DepartmentName",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if ((PracticeMaster.isWrite("DepartmentName",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Department Name&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <input maxlength="50" type=text size="80" name="DepartmentName" value="<%=PracticeMaster.getDepartmentName()%>">
&nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=DepartmentName&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("DepartmentName")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((PracticeMaster.isRead("DepartmentName",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Department Name&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p><%=PracticeMaster.getDepartmentName()%>&nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=DepartmentName&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("DepartmentName")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (PracticeMaster.isRequired("TypeOfPractice",UserSecurityGroupID))&&(!PracticeMaster.isComplete("TypeOfPractice")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("TypeOfPractice",expiredDays))&&(PracticeMaster.isExpiredCheck("TypeOfPractice",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if ((PracticeMaster.isWrite("TypeOfPractice",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Type of Practice&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <select   name="TypeOfPractice" >
                        <jsp:include page="../generic/tPracticeTypeLILong.jsp" flush="true" >
                        <jsp:param name="CurrentSelection" value="<%=PracticeMaster.getTypeOfPractice()%>" />
                        </jsp:include>
                      </select>
&nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=TypeOfPractice&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("TypeOfPractice")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((PracticeMaster.isRead("TypeOfPractice",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Type of Practice&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <jsp:include page="../generic/tPracticeTypeLILong_translate.jsp" flush="true" >
                      <jsp:param name="CurrentSelection" value="<%=PracticeMaster.getTypeOfPractice()%>" />
                      </jsp:include>
&nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=TypeOfPractice&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("TypeOfPractice")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (PracticeMaster.isRequired("OfficeFederalTaxID",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeFederalTaxID")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeFederalTaxID",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeFederalTaxID",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if ((PracticeMaster.isWrite("OfficeFederalTaxID",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Office Federal Tax ID&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <input maxlength="50" type=text size="80" name="OfficeFederalTaxID" value="<%=PracticeMaster.getOfficeFederalTaxID()%>">
&nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeFederalTaxID&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeFederalTaxID")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((PracticeMaster.isRead("OfficeFederalTaxID",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Office Federal Tax ID&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p><%=PracticeMaster.getOfficeFederalTaxID()%>&nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeFederalTaxID&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeFederalTaxID")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (PracticeMaster.isRequired("OfficeTaxIDNameAffiliation",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeTaxIDNameAffiliation")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeTaxIDNameAffiliation",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeTaxIDNameAffiliation",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if ((PracticeMaster.isWrite("OfficeTaxIDNameAffiliation",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Name affiliated with tax ID&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <input maxlength="50" type=text size="80" name="OfficeTaxIDNameAffiliation" value="<%=PracticeMaster.getOfficeTaxIDNameAffiliation()%>">
&nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeTaxIDNameAffiliation&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeTaxIDNameAffiliation")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((PracticeMaster.isRead("OfficeTaxIDNameAffiliation",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Name affiliated with tax ID&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p><%=PracticeMaster.getOfficeTaxIDNameAffiliation()%>&nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeTaxIDNameAffiliation&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeTaxIDNameAffiliation")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (PracticeMaster.isRequired("OfficeAddress1",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeAddress1")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeAddress1",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeAddress1",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if ((PracticeMaster.isWrite("OfficeAddress1",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Office Address&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <input maxlength="50" type=text size="80" name="OfficeAddress1" value="<%=PracticeMaster.getOfficeAddress1()%>">
&nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeAddress1&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeAddress1")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((PracticeMaster.isRead("OfficeAddress1",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Office Address&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p><%=PracticeMaster.getOfficeAddress1()%>&nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeAddress1&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeAddress1")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (PracticeMaster.isRequired("OfficeAddress2",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeAddress2")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeAddress2",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeAddress2",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if ((PracticeMaster.isWrite("OfficeAddress2",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Address 2&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <input maxlength="20" type=text size="80" name="OfficeAddress2" value="<%=PracticeMaster.getOfficeAddress2()%>">
&nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeAddress2&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeAddress2")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((PracticeMaster.isRead("OfficeAddress2",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Address 2&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p><%=PracticeMaster.getOfficeAddress2()%>&nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeAddress2&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeAddress2")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (PracticeMaster.isRequired("OfficeCity",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeCity")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeCity",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeCity",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if ((PracticeMaster.isWrite("OfficeCity",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>City, Town, Province&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <input maxlength="30" type=text size="80" name="OfficeCity" value="<%=PracticeMaster.getOfficeCity()%>">
&nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeCity&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeCity")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((PracticeMaster.isRead("OfficeCity",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>City, Town, Province&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p><%=PracticeMaster.getOfficeCity()%>&nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeCity&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeCity")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (PracticeMaster.isRequired("OfficeStateID",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeStateID")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeStateID",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeStateID",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if ((PracticeMaster.isWrite("OfficeStateID",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>State&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <select   name="OfficeStateID" >
                        <jsp:include page="../generic/tStateLILong.jsp" flush="true" >
                        <jsp:param name="CurrentSelection" value="<%=PracticeMaster.getOfficeStateID()%>" />
                        </jsp:include>
                      </select>
&nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeStateID&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeStateID")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((PracticeMaster.isRead("OfficeStateID",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>State&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <jsp:include page="../generic/tStateLILong_translate.jsp" flush="true" >
                      <jsp:param name="CurrentSelection" value="<%=PracticeMaster.getOfficeStateID()%>" />
                      </jsp:include>
&nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeStateID&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeStateID")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (PracticeMaster.isRequired("OfficeProvince",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeProvince")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeProvince",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeProvince",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if ((PracticeMaster.isWrite("OfficeProvince",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Province, District, State&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <input maxlength="100" type=text size="80" name="OfficeProvince" value="<%=PracticeMaster.getOfficeProvince()%>">
&nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeProvince&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeProvince")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((PracticeMaster.isRead("OfficeProvince",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Province, District, State&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p><%=PracticeMaster.getOfficeProvince()%>&nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeProvince&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeProvince")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (PracticeMaster.isRequired("OfficeZIP",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeZIP")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeZIP",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeZIP",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if ((PracticeMaster.isWrite("OfficeZIP",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>ZIP&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <input maxlength="10" type=text size="80" name="OfficeZIP" value="<%=PracticeMaster.getOfficeZIP()%>">
&nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeZIP&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeZIP")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((PracticeMaster.isRead("OfficeZIP",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>ZIP&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p><%=PracticeMaster.getOfficeZIP()%>&nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeZIP&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeZIP")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (PracticeMaster.isRequired("OfficeCountryID",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeCountryID")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeCountryID",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeCountryID",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if ((PracticeMaster.isWrite("OfficeCountryID",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Country&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <select   name="OfficeCountryID" >
                        <jsp:include page="../generic/tCountryLILong.jsp" flush="true" >
                        <jsp:param name="CurrentSelection" value="<%=PracticeMaster.getOfficeCountryID()%>" />
                        </jsp:include>
                      </select>
&nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeCountryID&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeCountryID")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((PracticeMaster.isRead("OfficeCountryID",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Country&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <jsp:include page="../generic/tCountryLILong_translate.jsp" flush="true" >
                      <jsp:param name="CurrentSelection" value="<%=PracticeMaster.getOfficeCountryID()%>" />
                      </jsp:include>
&nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeCountryID&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeCountryID")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (PracticeMaster.isRequired("OfficePhone",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficePhone")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficePhone",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficePhone",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if ((PracticeMaster.isWrite("OfficePhone",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Office Phone Number (XXX-XXX-XXXX)&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <input maxlength="20" type=text size="80" name="OfficePhone" value="<%=PracticeMaster.getOfficePhone()%>">
&nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficePhone&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficePhone")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((PracticeMaster.isRead("OfficePhone",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Office Phone Number (XXX-XXX-XXXX)&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p><%=PracticeMaster.getOfficePhone()%>&nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficePhone&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficePhone")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (PracticeMaster.isRequired("BackOfficePhoneNo",UserSecurityGroupID))&&(!PracticeMaster.isComplete("BackOfficePhoneNo")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("BackOfficePhoneNo",expiredDays))&&(PracticeMaster.isExpiredCheck("BackOfficePhoneNo",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if ((PracticeMaster.isWrite("BackOfficePhoneNo",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Back Office Phone (if applicable) 
                      (XXX-XXX-XXXX)&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <input maxlength="20" type=text size="80" name="BackOfficePhoneNo" value="<%=PracticeMaster.getBackOfficePhoneNo()%>">
&nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=BackOfficePhoneNo&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("BackOfficePhoneNo")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((PracticeMaster.isRead("BackOfficePhoneNo",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Back Office Phone (if applicable) 
                      (XXX-XXX-XXXX)&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p><%=PracticeMaster.getBackOfficePhoneNo()%>&nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=BackOfficePhoneNo&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("BackOfficePhoneNo")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (PracticeMaster.isRequired("OfficeFaxNo",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeFaxNo")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeFaxNo",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeFaxNo",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if ((PracticeMaster.isWrite("OfficeFaxNo",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Office Fax&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <input maxlength="20" type=text size="80" name="OfficeFaxNo" value="<%=PracticeMaster.getOfficeFaxNo()%>">
&nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeFaxNo&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeFaxNo")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((PracticeMaster.isRead("OfficeFaxNo",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Office Fax&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p><%=PracticeMaster.getOfficeFaxNo()%>&nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeFaxNo&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeFaxNo")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (PracticeMaster.isRequired("OfficeEmail",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeEmail")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeEmail",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeEmail",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if ((PracticeMaster.isWrite("OfficeEmail",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Office Email&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <input maxlength="75" type=text size="80" name="OfficeEmail" value="<%=PracticeMaster.getOfficeEmail()%>">
&nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeEmail&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeEmail")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((PracticeMaster.isRead("OfficeEmail",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Office Email&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p><%=PracticeMaster.getOfficeEmail()%>&nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeEmail&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeEmail")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (PracticeMaster.isRequired("OfficeManagerFirstName",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeManagerFirstName")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeManagerFirstName",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeManagerFirstName",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if ((PracticeMaster.isWrite("OfficeManagerFirstName",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Office Manager First Name&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <input maxlength="50" type=text size="80" name="OfficeManagerFirstName" value="<%=PracticeMaster.getOfficeManagerFirstName()%>">
&nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeManagerFirstName&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeManagerFirstName")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((PracticeMaster.isRead("OfficeManagerFirstName",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Office Manager First Name&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p><%=PracticeMaster.getOfficeManagerFirstName()%>&nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeManagerFirstName&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeManagerFirstName")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (PracticeMaster.isRequired("OfficeManagerLastName",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeManagerLastName")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeManagerLastName",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeManagerLastName",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if ((PracticeMaster.isWrite("OfficeManagerLastName",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Office Manager Last Name&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <input maxlength="50" type=text size="80" name="OfficeManagerLastName" value="<%=PracticeMaster.getOfficeManagerLastName()%>">
&nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeManagerLastName&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeManagerLastName")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((PracticeMaster.isRead("OfficeManagerLastName",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Office Manager Last Name&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p><%=PracticeMaster.getOfficeManagerLastName()%>&nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeManagerLastName&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeManagerLastName")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (PracticeMaster.isRequired("OfficeManagerPhone",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeManagerPhone")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeManagerPhone",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeManagerPhone",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if ((PracticeMaster.isWrite("OfficeManagerPhone",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Office Manager Phone (XXX-XXX-XXXX)&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <input maxlength="20" type=text size="80" name="OfficeManagerPhone" value="<%=PracticeMaster.getOfficeManagerPhone()%>">
&nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeManagerPhone&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeManagerPhone")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((PracticeMaster.isRead("OfficeManagerPhone",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Office Manager Phone (XXX-XXX-XXXX)&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p><%=PracticeMaster.getOfficeManagerPhone()%>&nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeManagerPhone&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeManagerPhone")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (PracticeMaster.isRequired("OfficeManagerEmail",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeManagerEmail")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeManagerEmail",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeManagerEmail",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if ((PracticeMaster.isWrite("OfficeManagerEmail",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Office Manager Email&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <input maxlength="75" type=text size="80" name="OfficeManagerEmail" value="<%=PracticeMaster.getOfficeManagerEmail()%>">
&nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeManagerEmail&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeManagerEmail")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((PracticeMaster.isRead("OfficeManagerEmail",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Office Manager Email&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p><%=PracticeMaster.getOfficeManagerEmail()%>&nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeManagerEmail&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeManagerEmail")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (PracticeMaster.isRequired("AnsweringService",UserSecurityGroupID))&&(!PracticeMaster.isComplete("AnsweringService")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("AnsweringService",expiredDays))&&(PracticeMaster.isExpiredCheck("AnsweringService",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if ((PracticeMaster.isWrite("AnsweringService",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Do you have an answering service?&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <select   name="AnsweringService" >
                        <jsp:include page="../generic/tYesNoLILong.jsp" flush="true" >
                        <jsp:param name="CurrentSelection" value="<%=PracticeMaster.getAnsweringService()%>" />
                        </jsp:include>
                      </select>
&nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=AnsweringService&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("AnsweringService")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((PracticeMaster.isRead("AnsweringService",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Do you have an answering service?&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" >
                      <jsp:param name="CurrentSelection" value="<%=PracticeMaster.getAnsweringService()%>" />
                      </jsp:include>
&nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=AnsweringService&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("AnsweringService")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (PracticeMaster.isRequired("AnsweringServicePhone",UserSecurityGroupID))&&(!PracticeMaster.isComplete("AnsweringServicePhone")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("AnsweringServicePhone",expiredDays))&&(PracticeMaster.isExpiredCheck("AnsweringServicePhone",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if ((PracticeMaster.isWrite("AnsweringServicePhone",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Answering Service Phone (XXX-XXX-XXXX)&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <input maxlength="20" type=text size="80" name="AnsweringServicePhone" value="<%=PracticeMaster.getAnsweringServicePhone()%>">
&nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=AnsweringServicePhone&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("AnsweringServicePhone")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((PracticeMaster.isRead("AnsweringServicePhone",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Answering Service Phone (XXX-XXX-XXXX)&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p><%=PracticeMaster.getAnsweringServicePhone()%>&nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=AnsweringServicePhone&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("AnsweringServicePhone")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (PracticeMaster.isRequired("Coverage247",UserSecurityGroupID))&&(!PracticeMaster.isComplete("Coverage247")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("Coverage247",expiredDays))&&(PracticeMaster.isExpiredCheck("Coverage247",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if ((PracticeMaster.isWrite("Coverage247",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Do you provide 24-7 coverage?&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <select   name="Coverage247" >
                        <jsp:include page="../generic/tYesNoLILong.jsp" flush="true" >
                        <jsp:param name="CurrentSelection" value="<%=PracticeMaster.getCoverage247()%>" />
                        </jsp:include>
                      </select>
&nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Coverage247&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("Coverage247")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((PracticeMaster.isRead("Coverage247",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Do you provide 24-7 coverage?&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" >
                      <jsp:param name="CurrentSelection" value="<%=PracticeMaster.getCoverage247()%>" />
                      </jsp:include>
&nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Coverage247&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("Coverage247")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (PracticeMaster.isRequired("MinorityEnterprise",UserSecurityGroupID))&&(!PracticeMaster.isComplete("MinorityEnterprise")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("MinorityEnterprise",expiredDays))&&(PracticeMaster.isExpiredCheck("MinorityEnterprise",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if ((PracticeMaster.isWrite("MinorityEnterprise",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Does your office qualify as a minority 
                      business enterprise? &nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <select   name="MinorityEnterprise" >
                        <jsp:include page="../generic/tYesNoLILong.jsp" flush="true" >
                        <jsp:param name="CurrentSelection" value="<%=PracticeMaster.getMinorityEnterprise()%>" />
                        </jsp:include>
                      </select>
&nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=MinorityEnterprise&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("MinorityEnterprise")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((PracticeMaster.isRead("MinorityEnterprise",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Does your office qualify as a minority 
                      business enterprise? &nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" >
                      <jsp:param name="CurrentSelection" value="<%=PracticeMaster.getMinorityEnterprise()%>" />
                      </jsp:include>
&nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=MinorityEnterprise&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("MinorityEnterprise")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (PracticeMaster.isRequired("LanguagesSpokenInOffice",UserSecurityGroupID))&&(!PracticeMaster.isComplete("LanguagesSpokenInOffice")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("LanguagesSpokenInOffice",expiredDays))&&(PracticeMaster.isExpiredCheck("LanguagesSpokenInOffice",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if ((PracticeMaster.isWrite("LanguagesSpokenInOffice",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Languages Spoken In Office&nbsp;</b></p>
                  </td>
                  <td valign=top nowrap>
                    <p> 
                      <input onKeyDown="textAreaStop(this,100)" maxlength="100" type=text size="40" name="LanguagesSpokenInOffice" value="<%=PracticeMaster.getLanguagesSpokenInOffice()%>">
                      &nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=LanguagesSpokenInOffice&amp;sTableName=tPracticeMasterMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("LanguagesSpokenInOffice")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%> Quick Add:
					  <select name=quickLanguage onChange="javascript:document.forms[0].LanguagesSpokenInOffice.value = document.forms[0].LanguagesSpokenInOffice.value + ' ' + document.forms[0].quickLanguage.value;textAreaStop(document.forms[0].LanguagesSpokenInOffice,100)">
						<option value="">Please Select</option>
						<option value="Arabic">Arabic</option>
						<option value="Armenian">Armenian</option>
						<option value="ASL">ASL</option>
						<option value="Cebrisno">Cebrisno</option>
						<option value="Chinese Cantonese">Chinese Cantonese</option>
						<option value="Chinese Fukienese">Chinese Fukienese</option>
						<option value="Chinese Mandarin">Chinese Mandarin</option>
						<option value="Croatian">Croatian</option>
						<option value="Czech">Czech</option>
						<option value="Danish">Danish</option>
						<option value="Farsi">Farsi</option>
						<option value="Filipino">Filipino</option>
						<option value="Finnish">Finnish</option>
						<option value="French">French</option>
						<option value="German">German</option>
						<option value="Greek">Greek</option>
						<option value="Hungarian">Hungarian</option>
						<option value="Indonesian">Indonesian</option>
						<option value="Italian">Italian</option>
						<option value="Japanese">Japanese</option>
						<option value="Korean">Korean</option>
						<option value="None">None</option>
						<option value="Norwegian">Norwegian</option>
						<option value="Portugese">Portugese</option>
						<option value="Romanian">Romanian</option>
						<option value="Russian">Russian</option>
						<option value="Sign Language">Sign Language</option>
						<option value="Spanish">Spanish</option>
						<option value="Swedish">Swedish</option>
						<option value="Tagalog">Tagalog</option>
						<option value="Thai">Thai</option>
						<option value="Urdu">Urdu</option>
						<option value="Vietnamese">Vietnamese</option>
					  </select>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((PracticeMaster.isRead("LanguagesSpokenInOffice",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Languages Spoken In Office&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p><%=PracticeMaster.getLanguagesSpokenInOffice()%>&nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=LanguagesSpokenInOffice&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("LanguagesSpokenInOffice")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (PracticeMaster.isRequired("AcceptAllNewPatients",UserSecurityGroupID))&&(!PracticeMaster.isComplete("AcceptAllNewPatients")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("AcceptAllNewPatients",expiredDays))&&(PracticeMaster.isExpiredCheck("AcceptAllNewPatients",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if ((PracticeMaster.isWrite("AcceptAllNewPatients",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Do you accept all new patients?&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <select   name="AcceptAllNewPatients" >
                        <jsp:include page="../generic/tYesNoLILong.jsp" flush="true" >
                        <jsp:param name="CurrentSelection" value="<%=PracticeMaster.getAcceptAllNewPatients()%>" />
                        </jsp:include>
                      </select>
&nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=AcceptAllNewPatients&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("AcceptAllNewPatients")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((PracticeMaster.isRead("AcceptAllNewPatients",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Do you accept all new patients?&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" >
                      <jsp:param name="CurrentSelection" value="<%=PracticeMaster.getAcceptAllNewPatients()%>" />
                      </jsp:include>
&nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=AcceptAllNewPatients&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("AcceptAllNewPatients")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (PracticeMaster.isRequired("AcceptExistingPayorChange",UserSecurityGroupID))&&(!PracticeMaster.isComplete("AcceptExistingPayorChange")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("AcceptExistingPayorChange",expiredDays))&&(PracticeMaster.isExpiredCheck("AcceptExistingPayorChange",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if ((PracticeMaster.isWrite("AcceptExistingPayorChange",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Do you accept existing patients 
                      with change of payer?&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <select   name="AcceptExistingPayorChange" >
                        <jsp:include page="../generic/tYesNoLILong.jsp" flush="true" >
                        <jsp:param name="CurrentSelection" value="<%=PracticeMaster.getAcceptExistingPayorChange()%>" />
                        </jsp:include>
                      </select>
&nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=AcceptExistingPayorChange&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("AcceptExistingPayorChange")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((PracticeMaster.isRead("AcceptExistingPayorChange",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Do you accept existing patients 
                      with change of payer?&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" >
                      <jsp:param name="CurrentSelection" value="<%=PracticeMaster.getAcceptExistingPayorChange()%>" />
                      </jsp:include>
&nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=AcceptExistingPayorChange&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("AcceptExistingPayorChange")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (PracticeMaster.isRequired("AcceptNewFromReferralOnly",UserSecurityGroupID))&&(!PracticeMaster.isComplete("AcceptNewFromReferralOnly")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("AcceptNewFromReferralOnly",expiredDays))&&(PracticeMaster.isExpiredCheck("AcceptNewFromReferralOnly",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if ((PracticeMaster.isWrite("AcceptNewFromReferralOnly",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Do you accept from referrals only?&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <select   name="AcceptNewFromReferralOnly" >
                        <jsp:include page="../generic/tYesNoLILong.jsp" flush="true" >
                        <jsp:param name="CurrentSelection" value="<%=PracticeMaster.getAcceptNewFromReferralOnly()%>" />
                        </jsp:include>
                      </select>
&nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=AcceptNewFromReferralOnly&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("AcceptNewFromReferralOnly")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((PracticeMaster.isRead("AcceptNewFromReferralOnly",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Do you accept from referrals only?&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" >
                      <jsp:param name="CurrentSelection" value="<%=PracticeMaster.getAcceptNewFromReferralOnly()%>" />
                      </jsp:include>
&nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=AcceptNewFromReferralOnly&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("AcceptNewFromReferralOnly")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (PracticeMaster.isRequired("AcceptNewMedicare",UserSecurityGroupID))&&(!PracticeMaster.isComplete("AcceptNewMedicare")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("AcceptNewMedicare",expiredDays))&&(PracticeMaster.isExpiredCheck("AcceptNewMedicare",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if ((PracticeMaster.isWrite("AcceptNewMedicare",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Do you accept new Medicare patients?&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <select   name="AcceptNewMedicare" >
                        <jsp:include page="../generic/tYesNoLILong.jsp" flush="true" >
                        <jsp:param name="CurrentSelection" value="<%=PracticeMaster.getAcceptNewMedicare()%>" />
                        </jsp:include>
                      </select>
&nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=AcceptNewMedicare&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("AcceptNewMedicare")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((PracticeMaster.isRead("AcceptNewMedicare",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Do you accept new Medicare patients?&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" >
                      <jsp:param name="CurrentSelection" value="<%=PracticeMaster.getAcceptNewMedicare()%>" />
                      </jsp:include>
&nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=AcceptNewMedicare&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("AcceptNewMedicare")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (PracticeMaster.isRequired("AcceptNewMedicaid",UserSecurityGroupID))&&(!PracticeMaster.isComplete("AcceptNewMedicaid")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("AcceptNewMedicaid",expiredDays))&&(PracticeMaster.isExpiredCheck("AcceptNewMedicaid",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if ((PracticeMaster.isWrite("AcceptNewMedicaid",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Do you accept new Medicaid patients?&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <select   name="AcceptNewMedicaid" >
                        <jsp:include page="../generic/tYesNoLILong.jsp" flush="true" >
                        <jsp:param name="CurrentSelection" value="<%=PracticeMaster.getAcceptNewMedicaid()%>" />
                        </jsp:include>
                      </select>
&nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=AcceptNewMedicaid&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("AcceptNewMedicaid")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((PracticeMaster.isRead("AcceptNewMedicaid",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Do you accept new Medicaid patients?&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" >
                      <jsp:param name="CurrentSelection" value="<%=PracticeMaster.getAcceptNewMedicaid()%>" />
                      </jsp:include>
&nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=AcceptNewMedicaid&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("AcceptNewMedicaid")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (PracticeMaster.isRequired("PracticeLimitationAge",UserSecurityGroupID))&&(!PracticeMaster.isComplete("PracticeLimitationAge")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("PracticeLimitationAge",expiredDays))&&(PracticeMaster.isExpiredCheck("PracticeLimitationAge",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if ((PracticeMaster.isWrite("PracticeLimitationAge",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Patient age limitations&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <input maxlength="50" type=text size="80" name="PracticeLimitationAge" value="<%=PracticeMaster.getPracticeLimitationAge()%>">
&nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PracticeLimitationAge&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("PracticeLimitationAge")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((PracticeMaster.isRead("PracticeLimitationAge",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Patient age limitations&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p><%=PracticeMaster.getPracticeLimitationAge()%>&nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PracticeLimitationAge&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("PracticeLimitationAge")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (PracticeMaster.isRequired("PracticeLimitationSex",UserSecurityGroupID))&&(!PracticeMaster.isComplete("PracticeLimitationSex")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("PracticeLimitationSex",expiredDays))&&(PracticeMaster.isExpiredCheck("PracticeLimitationSex",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if ((PracticeMaster.isWrite("PracticeLimitationSex",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Patient gender limitations&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <input maxlength="50" type=text size="80" name="PracticeLimitationSex" value="<%=PracticeMaster.getPracticeLimitationSex()%>">
&nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PracticeLimitationSex&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("PracticeLimitationSex")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((PracticeMaster.isRead("PracticeLimitationSex",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Patient gender limitations&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p><%=PracticeMaster.getPracticeLimitationSex()%>&nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PracticeLimitationSex&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("PracticeLimitationSex")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (PracticeMaster.isRequired("PracticeLimitationOther",UserSecurityGroupID))&&(!PracticeMaster.isComplete("PracticeLimitationOther")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("PracticeLimitationOther",expiredDays))&&(PracticeMaster.isExpiredCheck("PracticeLimitationOther",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if ((PracticeMaster.isWrite("PracticeLimitationOther",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Other practice limitations&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <input maxlength="50" type=text size="80" name="PracticeLimitationOther" value="<%=PracticeMaster.getPracticeLimitationOther()%>">
&nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PracticeLimitationOther&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("PracticeLimitationOther")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((PracticeMaster.isRead("PracticeLimitationOther",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Other practice limitations&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p><%=PracticeMaster.getPracticeLimitationOther()%>&nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PracticeLimitationOther&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("PracticeLimitationOther")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <tr >
                  <td colspan=2>
                    <hr noshade>
                  </td>
                </tr>
                <tr class=title>
                  <td colspan=2><span  class=title >Billing Information  </span><span class=tdBase>(<a href="#" onClick="return BillingFill()">Click 
                    here</a> if this is the same as the Practice Office Information)</span></td>
                </tr>
                <tr >
                  <td colspan=2>&nbsp;</td>
                </tr>
                <%
            if ( (PracticeMaster.isRequired("BillingPayableTo",UserSecurityGroupID))&&(!PracticeMaster.isComplete("BillingPayableTo")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("BillingPayableTo",expiredDays))&&(PracticeMaster.isExpiredCheck("BillingPayableTo",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if ((PracticeMaster.isWrite("BillingPayableTo",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Make Checks Payable To&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <input maxlength="50" type=text size="80" name="BillingPayableTo" value="<%=PracticeMaster.getBillingPayableTo()%>">
&nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=BillingPayableTo&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("BillingPayableTo")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((PracticeMaster.isRead("BillingPayableTo",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Make Checks Payable To&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p><%=PracticeMaster.getBillingPayableTo()%>&nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=BillingPayableTo&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("BillingPayableTo")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (PracticeMaster.isRequired("BillingFirstName",UserSecurityGroupID))&&(!PracticeMaster.isComplete("BillingFirstName")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("BillingFirstName",expiredDays))&&(PracticeMaster.isExpiredCheck("BillingFirstName",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if ((PracticeMaster.isWrite("BillingFirstName",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Billing Contact First Name&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <input maxlength="50" type=text size="80" name="BillingFirstName" value="<%=PracticeMaster.getBillingFirstName()%>">
&nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=BillingFirstName&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("BillingFirstName")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((PracticeMaster.isRead("BillingFirstName",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Billing Contact First Name&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p><%=PracticeMaster.getBillingFirstName()%>&nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=BillingFirstName&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("BillingFirstName")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (PracticeMaster.isRequired("BillingLastName",UserSecurityGroupID))&&(!PracticeMaster.isComplete("BillingLastName")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("BillingLastName",expiredDays))&&(PracticeMaster.isExpiredCheck("BillingLastName",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if ((PracticeMaster.isWrite("BillingLastName",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Billing Contact Last Name&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <input maxlength="50" type=text size="80" name="BillingLastName" value="<%=PracticeMaster.getBillingLastName()%>">
&nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=BillingLastName&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("BillingLastName")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((PracticeMaster.isRead("BillingLastName",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Billing Contact Last Name&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p><%=PracticeMaster.getBillingLastName()%>&nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=BillingLastName&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("BillingLastName")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (PracticeMaster.isRequired("BillingAddress1",UserSecurityGroupID))&&(!PracticeMaster.isComplete("BillingAddress1")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("BillingAddress1",expiredDays))&&(PracticeMaster.isExpiredCheck("BillingAddress1",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if ((PracticeMaster.isWrite("BillingAddress1",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Address&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <input maxlength="50" type=text size="80" name="BillingAddress1" value="<%=PracticeMaster.getBillingAddress1()%>">
&nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=BillingAddress1&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("BillingAddress1")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((PracticeMaster.isRead("BillingAddress1",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Address&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p><%=PracticeMaster.getBillingAddress1()%>&nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=BillingAddress1&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("BillingAddress1")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (PracticeMaster.isRequired("BillingAddress2",UserSecurityGroupID))&&(!PracticeMaster.isComplete("BillingAddress2")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("BillingAddress2",expiredDays))&&(PracticeMaster.isExpiredCheck("BillingAddress2",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if ((PracticeMaster.isWrite("BillingAddress2",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Address 2&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <input maxlength="20" type=text size="80" name="BillingAddress2" value="<%=PracticeMaster.getBillingAddress2()%>">
&nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=BillingAddress2&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("BillingAddress2")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((PracticeMaster.isRead("BillingAddress2",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Address 2&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p><%=PracticeMaster.getBillingAddress2()%>&nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=BillingAddress2&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("BillingAddress2")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (PracticeMaster.isRequired("BillingCity",UserSecurityGroupID))&&(!PracticeMaster.isComplete("BillingCity")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("BillingCity",expiredDays))&&(PracticeMaster.isExpiredCheck("BillingCity",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if ((PracticeMaster.isWrite("BillingCity",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>City&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <input maxlength="30" type=text size="80" name="BillingCity" value="<%=PracticeMaster.getBillingCity()%>">
&nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=BillingCity&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("BillingCity")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((PracticeMaster.isRead("BillingCity",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>City&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p><%=PracticeMaster.getBillingCity()%>&nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=BillingCity&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("BillingCity")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (PracticeMaster.isRequired("BillingStateID",UserSecurityGroupID))&&(!PracticeMaster.isComplete("BillingStateID")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("BillingStateID",expiredDays))&&(PracticeMaster.isExpiredCheck("BillingStateID",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if ((PracticeMaster.isWrite("BillingStateID",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>State&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <select   name="BillingStateID" >
                        <jsp:include page="../generic/tStateLILong.jsp" flush="true" >
                        <jsp:param name="CurrentSelection" value="<%=PracticeMaster.getBillingStateID()%>" />
                        </jsp:include>
                      </select>
&nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=BillingStateID&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("BillingStateID")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((PracticeMaster.isRead("BillingStateID",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>State&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <jsp:include page="../generic/tStateLILong_translate.jsp" flush="true" >
                      <jsp:param name="CurrentSelection" value="<%=PracticeMaster.getBillingStateID()%>" />
                      </jsp:include>
&nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=BillingStateID&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("BillingStateID")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (PracticeMaster.isRequired("BillingProvince",UserSecurityGroupID))&&(!PracticeMaster.isComplete("BillingProvince")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("BillingProvince",expiredDays))&&(PracticeMaster.isExpiredCheck("BillingProvince",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if ((PracticeMaster.isWrite("BillingProvince",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Province, District, State&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <input maxlength="100" type=text size="80" name="BillingProvince" value="<%=PracticeMaster.getBillingProvince()%>">
&nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=BillingProvince&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("BillingProvince")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((PracticeMaster.isRead("BillingProvince",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Province, District, State&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p><%=PracticeMaster.getBillingProvince()%>&nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=BillingProvince&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("BillingProvince")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (PracticeMaster.isRequired("BillingZIP",UserSecurityGroupID))&&(!PracticeMaster.isComplete("BillingZIP")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("BillingZIP",expiredDays))&&(PracticeMaster.isExpiredCheck("BillingZIP",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if ((PracticeMaster.isWrite("BillingZIP",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>ZIP&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <input maxlength="10" type=text size="80" name="BillingZIP" value="<%=PracticeMaster.getBillingZIP()%>">
&nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=BillingZIP&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("BillingZIP")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((PracticeMaster.isRead("BillingZIP",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>ZIP&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p><%=PracticeMaster.getBillingZIP()%>&nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=BillingZIP&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("BillingZIP")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (PracticeMaster.isRequired("BillingCountryID",UserSecurityGroupID))&&(!PracticeMaster.isComplete("BillingCountryID")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("BillingCountryID",expiredDays))&&(PracticeMaster.isExpiredCheck("BillingCountryID",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if ((PracticeMaster.isWrite("BillingCountryID",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Country&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <select   name="BillingCountryID" >
                        <jsp:include page="../generic/tCountryLILong.jsp" flush="true" >
                        <jsp:param name="CurrentSelection" value="<%=PracticeMaster.getBillingCountryID()%>" />
                        </jsp:include>
                      </select>
&nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=BillingCountryID&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("BillingCountryID")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((PracticeMaster.isRead("BillingCountryID",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Country&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <jsp:include page="../generic/tCountryLILong_translate.jsp" flush="true" >
                      <jsp:param name="CurrentSelection" value="<%=PracticeMaster.getBillingCountryID()%>" />
                      </jsp:include>
&nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=BillingCountryID&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("BillingCountryID")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (PracticeMaster.isRequired("BillingPhone",UserSecurityGroupID))&&(!PracticeMaster.isComplete("BillingPhone")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("BillingPhone",expiredDays))&&(PracticeMaster.isExpiredCheck("BillingPhone",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if ((PracticeMaster.isWrite("BillingPhone",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Phone (XXX-XXX-XXXX)&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <input maxlength="20" type=text size="80" name="BillingPhone" value="<%=PracticeMaster.getBillingPhone()%>">
&nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=BillingPhone&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("BillingPhone")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((PracticeMaster.isRead("BillingPhone",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Phone (XXX-XXX-XXXX)&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p><%=PracticeMaster.getBillingPhone()%>&nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=BillingPhone&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("BillingPhone")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (PracticeMaster.isRequired("BillingFax",UserSecurityGroupID))&&(!PracticeMaster.isComplete("BillingFax")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("BillingFax",expiredDays))&&(PracticeMaster.isExpiredCheck("BillingFax",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if ((PracticeMaster.isWrite("BillingFax",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Fax (XXX-XXX-XXXX)&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <input maxlength="20" type=text size="80" name="BillingFax" value="<%=PracticeMaster.getBillingFax()%>">
&nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=BillingFax&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("BillingFax")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((PracticeMaster.isRead("BillingFax",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Fax (XXX-XXX-XXXX)&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p><%=PracticeMaster.getBillingFax()%>&nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=BillingFax&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("BillingFax")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <tr >
                  <td colspan=2>
                    <hr noshade>
                  </td>
                </tr>
                <tr class=title>
                  <td colspan=2><span  class=title >Credentialing Contact</span> <span class=tdBase>(<a href="#" onClick="return CredFill()">Click 
                    here</a> if this is the same as the Practice Office Information)</span></td>
                </tr>
                <tr >
                  <td colspan=2>&nbsp;</td>
                </tr>
                <%
            if ( (PracticeMaster.isRequired("CredentialingContactFirstName",UserSecurityGroupID))&&(!PracticeMaster.isComplete("CredentialingContactFirstName")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("CredentialingContactFirstName",expiredDays))&&(PracticeMaster.isExpiredCheck("CredentialingContactFirstName",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if ((PracticeMaster.isWrite("CredentialingContactFirstName",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Credentialing Contact First Name&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <input maxlength="50" type=text size="80" name="CredentialingContactFirstName" value="<%=PracticeMaster.getCredentialingContactFirstName()%>">
&nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CredentialingContactFirstName&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("CredentialingContactFirstName")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((PracticeMaster.isRead("CredentialingContactFirstName",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Credentialing Contact First Name&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p><%=PracticeMaster.getCredentialingContactFirstName()%>&nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CredentialingContactFirstName&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("CredentialingContactFirstName")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (PracticeMaster.isRequired("CredentialingContactLastName",UserSecurityGroupID))&&(!PracticeMaster.isComplete("CredentialingContactLastName")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("CredentialingContactLastName",expiredDays))&&(PracticeMaster.isExpiredCheck("CredentialingContactLastName",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if ((PracticeMaster.isWrite("CredentialingContactLastName",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Credentialing Contact Last Name&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <input maxlength="50" type=text size="80" name="CredentialingContactLastName" value="<%=PracticeMaster.getCredentialingContactLastName()%>">
&nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CredentialingContactLastName&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("CredentialingContactLastName")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((PracticeMaster.isRead("CredentialingContactLastName",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Credentialing Contact Last Name&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p><%=PracticeMaster.getCredentialingContactLastName()%>&nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CredentialingContactLastName&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("CredentialingContactLastName")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (PracticeMaster.isRequired("CredentialingContactAddress1",UserSecurityGroupID))&&(!PracticeMaster.isComplete("CredentialingContactAddress1")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("CredentialingContactAddress1",expiredDays))&&(PracticeMaster.isExpiredCheck("CredentialingContactAddress1",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if ((PracticeMaster.isWrite("CredentialingContactAddress1",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Address&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <input maxlength="50" type=text size="80" name="CredentialingContactAddress1" value="<%=PracticeMaster.getCredentialingContactAddress1()%>">
&nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CredentialingContactAddress1&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("CredentialingContactAddress1")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((PracticeMaster.isRead("CredentialingContactAddress1",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Address&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p><%=PracticeMaster.getCredentialingContactAddress1()%>&nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CredentialingContactAddress1&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("CredentialingContactAddress1")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (PracticeMaster.isRequired("CredentialingContactAddress2",UserSecurityGroupID))&&(!PracticeMaster.isComplete("CredentialingContactAddress2")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("CredentialingContactAddress2",expiredDays))&&(PracticeMaster.isExpiredCheck("CredentialingContactAddress2",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if ((PracticeMaster.isWrite("CredentialingContactAddress2",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Address 2&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <input maxlength="20" type=text size="80" name="CredentialingContactAddress2" value="<%=PracticeMaster.getCredentialingContactAddress2()%>">
&nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CredentialingContactAddress2&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("CredentialingContactAddress2")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((PracticeMaster.isRead("CredentialingContactAddress2",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Address 2&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p><%=PracticeMaster.getCredentialingContactAddress2()%>&nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CredentialingContactAddress2&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("CredentialingContactAddress2")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (PracticeMaster.isRequired("CredentialingContactCity",UserSecurityGroupID))&&(!PracticeMaster.isComplete("CredentialingContactCity")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("CredentialingContactCity",expiredDays))&&(PracticeMaster.isExpiredCheck("CredentialingContactCity",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if ((PracticeMaster.isWrite("CredentialingContactCity",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>City&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <input maxlength="30" type=text size="80" name="CredentialingContactCity" value="<%=PracticeMaster.getCredentialingContactCity()%>">
&nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CredentialingContactCity&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("CredentialingContactCity")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((PracticeMaster.isRead("CredentialingContactCity",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>City&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p><%=PracticeMaster.getCredentialingContactCity()%>&nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CredentialingContactCity&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("CredentialingContactCity")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (PracticeMaster.isRequired("CredentialingContactStateID",UserSecurityGroupID))&&(!PracticeMaster.isComplete("CredentialingContactStateID")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("CredentialingContactStateID",expiredDays))&&(PracticeMaster.isExpiredCheck("CredentialingContactStateID",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if ((PracticeMaster.isWrite("CredentialingContactStateID",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>State&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <select   name="CredentialingContactStateID" >
                        <jsp:include page="../generic/tStateLILong.jsp" flush="true" >
                        <jsp:param name="CurrentSelection" value="<%=PracticeMaster.getCredentialingContactStateID()%>" />
                        </jsp:include>
                      </select>
&nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CredentialingContactStateID&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("CredentialingContactStateID")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((PracticeMaster.isRead("CredentialingContactStateID",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>State&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <jsp:include page="../generic/tStateLILong_translate.jsp" flush="true" >
                      <jsp:param name="CurrentSelection" value="<%=PracticeMaster.getCredentialingContactStateID()%>" />
                      </jsp:include>
&nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CredentialingContactStateID&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("CredentialingContactStateID")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (PracticeMaster.isRequired("CredentialingContactProvince",UserSecurityGroupID))&&(!PracticeMaster.isComplete("CredentialingContactProvince")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("CredentialingContactProvince",expiredDays))&&(PracticeMaster.isExpiredCheck("CredentialingContactProvince",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if ((PracticeMaster.isWrite("CredentialingContactProvince",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Province, District, State&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <input maxlength="100" type=text size="80" name="CredentialingContactProvince" value="<%=PracticeMaster.getCredentialingContactProvince()%>">
&nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CredentialingContactProvince&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("CredentialingContactProvince")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((PracticeMaster.isRead("CredentialingContactProvince",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Province, District, State&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p><%=PracticeMaster.getCredentialingContactProvince()%>&nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CredentialingContactProvince&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("CredentialingContactProvince")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (PracticeMaster.isRequired("CredentialingContactZIP",UserSecurityGroupID))&&(!PracticeMaster.isComplete("CredentialingContactZIP")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("CredentialingContactZIP",expiredDays))&&(PracticeMaster.isExpiredCheck("CredentialingContactZIP",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if ((PracticeMaster.isWrite("CredentialingContactZIP",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>ZIP&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <input maxlength="50" type=text size="80" name="CredentialingContactZIP" value="<%=PracticeMaster.getCredentialingContactZIP()%>">
&nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CredentialingContactZIP&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("CredentialingContactZIP")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((PracticeMaster.isRead("CredentialingContactZIP",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>ZIP&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p><%=PracticeMaster.getCredentialingContactZIP()%>&nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CredentialingContactZIP&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("CredentialingContactZIP")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (PracticeMaster.isRequired("CredentialingContactCountryID",UserSecurityGroupID))&&(!PracticeMaster.isComplete("CredentialingContactCountryID")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("CredentialingContactCountryID",expiredDays))&&(PracticeMaster.isExpiredCheck("CredentialingContactCountryID",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if ((PracticeMaster.isWrite("CredentialingContactCountryID",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Country&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <select   name="CredentialingContactCountryID" >
                        <jsp:include page="../generic/tCountryLILong.jsp" flush="true" >
                        <jsp:param name="CurrentSelection" value="<%=PracticeMaster.getCredentialingContactCountryID()%>" />
                        </jsp:include>
                      </select>
&nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CredentialingContactCountryID&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("CredentialingContactCountryID")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((PracticeMaster.isRead("CredentialingContactCountryID",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Country&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <jsp:include page="../generic/tCountryLILong_translate.jsp" flush="true" >
                      <jsp:param name="CurrentSelection" value="<%=PracticeMaster.getCredentialingContactCountryID()%>" />
                      </jsp:include>
&nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CredentialingContactCountryID&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("CredentialingContactCountryID")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (PracticeMaster.isRequired("CredentialingContactPhone",UserSecurityGroupID))&&(!PracticeMaster.isComplete("CredentialingContactPhone")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("CredentialingContactPhone",expiredDays))&&(PracticeMaster.isExpiredCheck("CredentialingContactPhone",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if ((PracticeMaster.isWrite("CredentialingContactPhone",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Phone (XXX-XXX-XXXX)&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <input maxlength="50" type=text size="80" name="CredentialingContactPhone" value="<%=PracticeMaster.getCredentialingContactPhone()%>">
&nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CredentialingContactPhone&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("CredentialingContactPhone")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((PracticeMaster.isRead("CredentialingContactPhone",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Phone (XXX-XXX-XXXX)&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p><%=PracticeMaster.getCredentialingContactPhone()%>&nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CredentialingContactPhone&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("CredentialingContactPhone")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (PracticeMaster.isRequired("CredentialingContactFax",UserSecurityGroupID))&&(!PracticeMaster.isComplete("CredentialingContactFax")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("CredentialingContactFax",expiredDays))&&(PracticeMaster.isExpiredCheck("CredentialingContactFax",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if ((PracticeMaster.isWrite("CredentialingContactFax",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Fax (XXX-XXX-XXXX)&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <input maxlength="50" type=text size="80" name="CredentialingContactFax" value="<%=PracticeMaster.getCredentialingContactFax()%>">
&nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CredentialingContactFax&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("CredentialingContactFax")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((PracticeMaster.isRead("CredentialingContactFax",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Fax (XXX-XXX-XXXX)&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p><%=PracticeMaster.getCredentialingContactFax()%>&nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CredentialingContactFax&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("CredentialingContactFax")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (PracticeMaster.isRequired("CredentiallingContactEmail",UserSecurityGroupID))&&(!PracticeMaster.isComplete("CredentiallingContactEmail")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("CredentiallingContactEmail",expiredDays))&&(PracticeMaster.isExpiredCheck("CredentiallingContactEmail",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if ((PracticeMaster.isWrite("CredentiallingContactEmail",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Email&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <input maxlength="100" type=text size="80" name="CredentiallingContactEmail" value="<%=PracticeMaster.getCredentiallingContactEmail()%>">
&nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CredentiallingContactEmail&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("CredentiallingContactEmail")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((PracticeMaster.isRead("CredentiallingContactEmail",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Email&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p><%=PracticeMaster.getCredentiallingContactEmail()%>&nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CredentiallingContactEmail&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("CredentiallingContactEmail")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <tr> 
                  <td colspan=2> 
                    <hr noshade>
                  </td>
                </tr>
                <tr class=title> 
                  <td colspan=2 class=title >Office Hours:</td>
                </tr>
                <tr class=title> 
                  <td colspan=2>&nbsp;</td>
                </tr>
                <tr> 
                  <td colspan=2> 
                    <table class=tdBase border=1 bordercolor="#333333" cellpadding="2" cellspacing="0" align="left">
                      <tr class=tdHeaderAlt> 
                        <td colspan=1> 
                          <div align="center">&nbsp;</div>
                        </td>
                        <td colspan=2> 
                          <div align="center">Normal Hours </div>
                        </td>
                        <td colspan=2> 
                          <div align="center">After Hours </div>
                        </td>
                      </tr >
                      <tr class=tdBaseAlt> 
                        <td>Day</td>
                        <td> Open </td>
                        <td> Close </td>
                        <td> Open </td>
                        <td> Close </td>
                      </tr >
                      <tr> 
                        <td class=tdBaseAlt> 
                          <table class=tdBaseAlt  width="100" border="0" cellspacing="0" cellpadding="0" height="100%">
                            <tr> 
                              <td height="21">Monday</td>
                            </tr>
                            <tr> 
                              <td height="21">Tuesday</td>
                            </tr>
                            <tr> 
                              <td height="21">Wednesday</td>
                            </tr>
                            <tr> 
                              <td height="21">Thursday</td>
                            </tr>
                            <tr> 
                              <td height="21">Friday</td>
                            </tr>
                            <tr> 
                              <td height="21">Saturday</td>
                            </tr>
                            <tr> 
                              <td height="21">Sunday</td>
                            </tr>
                          </table>
                        </td>
                        <td> 
                          <table class=tdBase  border=0 cellpadding="0" cellspacing="0">
                            <%
            if ( (PracticeMaster.isRequired("OfficeWorkHoursOpenMonday",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeWorkHoursOpenMonday")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeWorkHoursOpenMonday",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeWorkHoursOpenMonday",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                            <%
            if ((PracticeMaster.isWrite("OfficeWorkHoursOpenMonday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p> 
                                  <input maxlength="10" type=text size="8" name="OfficeWorkHoursOpenMonday" value="<%=PracticeMaster.getOfficeWorkHoursOpenMonday()%>">
            &nbsp;
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeWorkHoursOpenMonday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeWorkHoursOpenMonday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else if ((PracticeMaster.isRead("OfficeWorkHoursOpenMonday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p><%=PracticeMaster.getOfficeWorkHoursOpenMonday()%>&nbsp;
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeWorkHoursOpenMonday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeWorkHoursOpenMonday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else
            {
                        %>
                            <%
            }
            %>
                            <%
            if ( (PracticeMaster.isRequired("OfficeWorkHoursOpenTuesday",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeWorkHoursOpenTuesday")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeWorkHoursOpenTuesday",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeWorkHoursOpenTuesday",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                            <%
            if ((PracticeMaster.isWrite("OfficeWorkHoursOpenTuesday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p> 
                                  <input maxlength="10" type=text size="8" name="OfficeWorkHoursOpenTuesday" value="<%=PracticeMaster.getOfficeWorkHoursOpenTuesday()%>">
            &nbsp;
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeWorkHoursOpenTuesday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeWorkHoursOpenTuesday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else if ((PracticeMaster.isRead("OfficeWorkHoursOpenTuesday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p><%=PracticeMaster.getOfficeWorkHoursOpenTuesday()%>&nbsp;
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeWorkHoursOpenTuesday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeWorkHoursOpenTuesday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else
            {
                        %>
                            <%
            }
            %>
                            <%
            if ( (PracticeMaster.isRequired("OfficeWorkHoursOpenWednesday",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeWorkHoursOpenWednesday")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeWorkHoursOpenWednesday",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeWorkHoursOpenWednesday",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                            <%
            if ((PracticeMaster.isWrite("OfficeWorkHoursOpenWednesday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p> 
                                  <input maxlength="10" type=text size="8" name="OfficeWorkHoursOpenWednesday" value="<%=PracticeMaster.getOfficeWorkHoursOpenWednesday()%>">
            &nbsp;
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeWorkHoursOpenWednesday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeWorkHoursOpenWednesday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else if ((PracticeMaster.isRead("OfficeWorkHoursOpenWednesday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p><%=PracticeMaster.getOfficeWorkHoursOpenWednesday()%>&nbsp;
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeWorkHoursOpenWednesday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeWorkHoursOpenWednesday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else
            {
                        %>
                            <%
            }
            %>
                            <%
            if ( (PracticeMaster.isRequired("OfficeWorkHoursOpenThursday",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeWorkHoursOpenThursday")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeWorkHoursOpenThursday",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeWorkHoursOpenThursday",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                            <%
            if ((PracticeMaster.isWrite("OfficeWorkHoursOpenThursday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p> 
                                  <input maxlength="10" type=text size="8" name="OfficeWorkHoursOpenThursday" value="<%=PracticeMaster.getOfficeWorkHoursOpenThursday()%>">
            &nbsp;
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeWorkHoursOpenThursday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeWorkHoursOpenThursday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else if ((PracticeMaster.isRead("OfficeWorkHoursOpenThursday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p><%=PracticeMaster.getOfficeWorkHoursOpenThursday()%>&nbsp;
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeWorkHoursOpenThursday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeWorkHoursOpenThursday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else
            {
                        %>
                            <%
            }
            %>
                            <%
            if ( (PracticeMaster.isRequired("OfficeWorkHoursOpenFriday",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeWorkHoursOpenFriday")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeWorkHoursOpenFriday",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeWorkHoursOpenFriday",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                            <%
            if ((PracticeMaster.isWrite("OfficeWorkHoursOpenFriday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p> 
                                  <input maxlength="10" type=text size="8" name="OfficeWorkHoursOpenFriday" value="<%=PracticeMaster.getOfficeWorkHoursOpenFriday()%>">
            &nbsp;
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeWorkHoursOpenFriday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeWorkHoursOpenFriday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else if ((PracticeMaster.isRead("OfficeWorkHoursOpenFriday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p><%=PracticeMaster.getOfficeWorkHoursOpenFriday()%>&nbsp;
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeWorkHoursOpenFriday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeWorkHoursOpenFriday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else
            {
                        %>
                            <%
            }
            %>
                            <%
            if ( (PracticeMaster.isRequired("OfficeWorkHoursOpenSaturday",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeWorkHoursOpenSaturday")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeWorkHoursOpenSaturday",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeWorkHoursOpenSaturday",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                            <%
            if ((PracticeMaster.isWrite("OfficeWorkHoursOpenSaturday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p> 
                                  <input maxlength="10" type=text size="8" name="OfficeWorkHoursOpenSaturday" value="<%=PracticeMaster.getOfficeWorkHoursOpenSaturday()%>">
            &nbsp;
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeWorkHoursOpenSaturday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeWorkHoursOpenSaturday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else if ((PracticeMaster.isRead("OfficeWorkHoursOpenSaturday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p><%=PracticeMaster.getOfficeWorkHoursOpenSaturday()%>&nbsp;
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeWorkHoursOpenSaturday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeWorkHoursOpenSaturday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else
            {
                        %>
                            <%
            }
            %>
                            <%
            if ( (PracticeMaster.isRequired("OfficeWorkHoursOpenSunday",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeWorkHoursOpenSunday")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeWorkHoursOpenSunday",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeWorkHoursOpenSunday",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                            <%
            if ((PracticeMaster.isWrite("OfficeWorkHoursOpenSunday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p> 
                                  <input maxlength="10" type=text size="8" name="OfficeWorkHoursOpenSunday" value="<%=PracticeMaster.getOfficeWorkHoursOpenSunday()%>">
            &nbsp;
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeWorkHoursOpenSunday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeWorkHoursOpenSunday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else if ((PracticeMaster.isRead("OfficeWorkHoursOpenSunday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p><%=PracticeMaster.getOfficeWorkHoursOpenSunday()%>&nbsp;
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeWorkHoursOpenSunday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeWorkHoursOpenSunday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else
            {
                        %>
                            <%
            }
            %>
                          </table>
                        </td>
                        <td> 
                          <table  class=tdBase border=0 cellpadding="0" cellspacing="0">
                            <%
            if ( (PracticeMaster.isRequired("OfficeWorkHoursCloseMonday",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeWorkHoursCloseMonday")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeWorkHoursCloseMonday",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeWorkHoursCloseMonday",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                            <%
            if ((PracticeMaster.isWrite("OfficeWorkHoursCloseMonday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p> 
                                  <input maxlength="10" type=text size="8" name="OfficeWorkHoursCloseMonday" value="<%=PracticeMaster.getOfficeWorkHoursCloseMonday()%>">
            &nbsp;
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeWorkHoursCloseMonday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeWorkHoursCloseMonday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else if ((PracticeMaster.isRead("OfficeWorkHoursCloseMonday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p><%=PracticeMaster.getOfficeWorkHoursCloseMonday()%>&nbsp;
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeWorkHoursCloseMonday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeWorkHoursCloseMonday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else
            {
                        %>
                            <%
            }
            %>
                            <%
            if ( (PracticeMaster.isRequired("OfficeWorkHoursCloseTuesday",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeWorkHoursCloseTuesday")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeWorkHoursCloseTuesday",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeWorkHoursCloseTuesday",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                            <%
            if ((PracticeMaster.isWrite("OfficeWorkHoursCloseTuesday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p> 
                                  <input maxlength="10" type=text size="8" name="OfficeWorkHoursCloseTuesday" value="<%=PracticeMaster.getOfficeWorkHoursCloseTuesday()%>">
            &nbsp;
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeWorkHoursCloseTuesday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeWorkHoursCloseTuesday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else if ((PracticeMaster.isRead("OfficeWorkHoursCloseTuesday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p><%=PracticeMaster.getOfficeWorkHoursCloseTuesday()%>&nbsp;
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeWorkHoursCloseTuesday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeWorkHoursCloseTuesday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else
            {
                        %>
                            <%
            }
            %>
                            <%
            if ( (PracticeMaster.isRequired("OfficeWorkHoursCloseWednesday",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeWorkHoursCloseWednesday")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeWorkHoursCloseWednesday",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeWorkHoursCloseWednesday",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                            <%
            if ((PracticeMaster.isWrite("OfficeWorkHoursCloseWednesday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p> 
                                  <input maxlength="10" type=text size="8" name="OfficeWorkHoursCloseWednesday" value="<%=PracticeMaster.getOfficeWorkHoursCloseWednesday()%>">
            &nbsp;
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeWorkHoursCloseWednesday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeWorkHoursCloseWednesday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else if ((PracticeMaster.isRead("OfficeWorkHoursCloseWednesday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p><%=PracticeMaster.getOfficeWorkHoursCloseWednesday()%>&nbsp;
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeWorkHoursCloseWednesday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeWorkHoursCloseWednesday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else
            {
                        %>
                            <%
            }
            %>
                            <%
            if ( (PracticeMaster.isRequired("OfficeWorkHoursCloseThursday",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeWorkHoursCloseThursday")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeWorkHoursCloseThursday",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeWorkHoursCloseThursday",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                            <%
            if ((PracticeMaster.isWrite("OfficeWorkHoursCloseThursday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p> 
                                  <input maxlength="10" type=text size="8" name="OfficeWorkHoursCloseThursday" value="<%=PracticeMaster.getOfficeWorkHoursCloseThursday()%>">
            &nbsp;
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeWorkHoursCloseThursday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeWorkHoursCloseThursday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else if ((PracticeMaster.isRead("OfficeWorkHoursCloseThursday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p><%=PracticeMaster.getOfficeWorkHoursCloseThursday()%>&nbsp;
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeWorkHoursCloseThursday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeWorkHoursCloseThursday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else
            {
                        %>
                            <%
            }
            %>
                            <%
            if ( (PracticeMaster.isRequired("OfficeWorkHoursCloseFriday",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeWorkHoursCloseFriday")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeWorkHoursCloseFriday",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeWorkHoursCloseFriday",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                            <%
            if ((PracticeMaster.isWrite("OfficeWorkHoursCloseFriday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p> 
                                  <input maxlength="10" type=text size="8" name="OfficeWorkHoursCloseFriday" value="<%=PracticeMaster.getOfficeWorkHoursCloseFriday()%>">
            &nbsp;
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeWorkHoursCloseFriday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeWorkHoursCloseFriday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else if ((PracticeMaster.isRead("OfficeWorkHoursCloseFriday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p><%=PracticeMaster.getOfficeWorkHoursCloseFriday()%>&nbsp;
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeWorkHoursCloseFriday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeWorkHoursCloseFriday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else
            {
                        %>
                            <%
            }
            %>
                            <%
            if ( (PracticeMaster.isRequired("OfficeWorkHoursCloseSaturday",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeWorkHoursCloseSaturday")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeWorkHoursCloseSaturday",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeWorkHoursCloseSaturday",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                            <%
            if ((PracticeMaster.isWrite("OfficeWorkHoursCloseSaturday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p> 
                                  <input maxlength="10" type=text size="8" name="OfficeWorkHoursCloseSaturday" value="<%=PracticeMaster.getOfficeWorkHoursCloseSaturday()%>">
            &nbsp;
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeWorkHoursCloseSaturday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeWorkHoursCloseSaturday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else if ((PracticeMaster.isRead("OfficeWorkHoursCloseSaturday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p><%=PracticeMaster.getOfficeWorkHoursCloseSaturday()%>&nbsp;
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeWorkHoursCloseSaturday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeWorkHoursCloseSaturday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else
            {
                        %>
                            <%
            }
            %>
                            <%
            if ( (PracticeMaster.isRequired("OfficeWorkHoursCloseSunday",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeWorkHoursCloseSunday")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeWorkHoursCloseSunday",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeWorkHoursCloseSunday",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                            <%
            if ((PracticeMaster.isWrite("OfficeWorkHoursCloseSunday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p> 
                                  <input maxlength="10" type=text size="8" name="OfficeWorkHoursCloseSunday" value="<%=PracticeMaster.getOfficeWorkHoursCloseSunday()%>">
            &nbsp;
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeWorkHoursCloseSunday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeWorkHoursCloseSunday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else if ((PracticeMaster.isRead("OfficeWorkHoursCloseSunday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p><%=PracticeMaster.getOfficeWorkHoursCloseSunday()%>&nbsp;
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeWorkHoursCloseSunday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeWorkHoursCloseSunday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else
            {
                        %>
                            <%
            }
            %>
                          </table>
                        </td>
                        <td> 
                          <table class=tdBase  border=0 cellpadding="0" cellspacing="0">
                            <%
            if ( (PracticeMaster.isRequired("OfficeAfterHoursOpenMonday",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeAfterHoursOpenMonday")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeAfterHoursOpenMonday",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeAfterHoursOpenMonday",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                            <%
            if ((PracticeMaster.isWrite("OfficeAfterHoursOpenMonday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p> 
                                  <input maxlength="10" type=text size="8" name="OfficeAfterHoursOpenMonday" value="<%=PracticeMaster.getOfficeAfterHoursOpenMonday()%>">
            &nbsp;
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeAfterHoursOpenMonday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeAfterHoursOpenMonday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else if ((PracticeMaster.isRead("OfficeAfterHoursOpenMonday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p><%=PracticeMaster.getOfficeAfterHoursOpenMonday()%>&nbsp;
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeAfterHoursOpenMonday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeAfterHoursOpenMonday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else
            {
                        %>
                            <%
            }
            %>
                            <%
            if ( (PracticeMaster.isRequired("OfficeAfterHoursOpenTuesday",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeAfterHoursOpenTuesday")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeAfterHoursOpenTuesday",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeAfterHoursOpenTuesday",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                            <%
            if ((PracticeMaster.isWrite("OfficeAfterHoursOpenTuesday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p> 
                                  <input maxlength="10" type=text size="8" name="OfficeAfterHoursOpenTuesday" value="<%=PracticeMaster.getOfficeAfterHoursOpenTuesday()%>">
            &nbsp;
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeAfterHoursOpenTuesday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeAfterHoursOpenTuesday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else if ((PracticeMaster.isRead("OfficeAfterHoursOpenTuesday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p><%=PracticeMaster.getOfficeAfterHoursOpenTuesday()%>&nbsp;
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeAfterHoursOpenTuesday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeAfterHoursOpenTuesday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else
            {
                        %>
                            <%
            }
            %>
                            <%
            if ( (PracticeMaster.isRequired("OfficeAfterHoursOpenWednesday",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeAfterHoursOpenWednesday")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeAfterHoursOpenWednesday",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeAfterHoursOpenWednesday",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                            <%
            if ((PracticeMaster.isWrite("OfficeAfterHoursOpenWednesday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p> 
                                  <input maxlength="10" type=text size="8" name="OfficeAfterHoursOpenWednesday" value="<%=PracticeMaster.getOfficeAfterHoursOpenWednesday()%>">
            &nbsp;
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeAfterHoursOpenWednesday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeAfterHoursOpenWednesday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else if ((PracticeMaster.isRead("OfficeAfterHoursOpenWednesday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p><%=PracticeMaster.getOfficeAfterHoursOpenWednesday()%>&nbsp;
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeAfterHoursOpenWednesday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeAfterHoursOpenWednesday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else
            {
                        %>
                            <%
            }
            %>
                            <%
            if ( (PracticeMaster.isRequired("OfficeAfterHoursOpenThursday",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeAfterHoursOpenThursday")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeAfterHoursOpenThursday",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeAfterHoursOpenThursday",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                            <%
            if ((PracticeMaster.isWrite("OfficeAfterHoursOpenThursday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p> 
                                  <input maxlength="10" type=text size="8" name="OfficeAfterHoursOpenThursday" value="<%=PracticeMaster.getOfficeAfterHoursOpenThursday()%>">
            &nbsp;
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeAfterHoursOpenThursday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeAfterHoursOpenThursday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else if ((PracticeMaster.isRead("OfficeAfterHoursOpenThursday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p><%=PracticeMaster.getOfficeAfterHoursOpenThursday()%>&nbsp;
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeAfterHoursOpenThursday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeAfterHoursOpenThursday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else
            {
                        %>
                            <%
            }
            %>
                            <%
            if ( (PracticeMaster.isRequired("OfficeAfterHoursOpenFriday",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeAfterHoursOpenFriday")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeAfterHoursOpenFriday",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeAfterHoursOpenFriday",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                            <%
            if ((PracticeMaster.isWrite("OfficeAfterHoursOpenFriday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p> 
                                  <input maxlength="10" type=text size="8" name="OfficeAfterHoursOpenFriday" value="<%=PracticeMaster.getOfficeAfterHoursOpenFriday()%>">
            &nbsp;
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeAfterHoursOpenFriday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeAfterHoursOpenFriday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else if ((PracticeMaster.isRead("OfficeAfterHoursOpenFriday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p><%=PracticeMaster.getOfficeAfterHoursOpenFriday()%>&nbsp;
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeAfterHoursOpenFriday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeAfterHoursOpenFriday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else
            {
                        %>
                            <%
            }
            %>
                            <%
            if ( (PracticeMaster.isRequired("OfficeAfterHoursOpenSaturday",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeAfterHoursOpenSaturday")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeAfterHoursOpenSaturday",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeAfterHoursOpenSaturday",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                            <%
            if ((PracticeMaster.isWrite("OfficeAfterHoursOpenSaturday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p> 
                                  <input maxlength="10" type=text size="8" name="OfficeAfterHoursOpenSaturday" value="<%=PracticeMaster.getOfficeAfterHoursOpenSaturday()%>">
            &nbsp;
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeAfterHoursOpenSaturday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeAfterHoursOpenSaturday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else if ((PracticeMaster.isRead("OfficeAfterHoursOpenSaturday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p><%=PracticeMaster.getOfficeAfterHoursOpenSaturday()%>&nbsp;
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeAfterHoursOpenSaturday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeAfterHoursOpenSaturday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else
            {
                        %>
                            <%
            }
            %>
                            <%
            if ( (PracticeMaster.isRequired("OfficeAfterHoursOpenSunday",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeAfterHoursOpenSunday")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeAfterHoursOpenSunday",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeAfterHoursOpenSunday",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                            <%
            if ((PracticeMaster.isWrite("OfficeAfterHoursOpenSunday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p> 
                                  <input maxlength="10" type=text size="8" name="OfficeAfterHoursOpenSunday" value="<%=PracticeMaster.getOfficeAfterHoursOpenSunday()%>">
            &nbsp;
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeAfterHoursOpenSunday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeAfterHoursOpenSunday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else if ((PracticeMaster.isRead("OfficeAfterHoursOpenSunday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p><%=PracticeMaster.getOfficeAfterHoursOpenSunday()%>&nbsp;
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeAfterHoursOpenSunday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeAfterHoursOpenSunday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else
            {
                        %>
                            <%
            }
            %>
                          </table>
                        </td>
                        <td> 
                          <table  class=tdBase border=0 cellpadding="0" cellspacing="0">
                            <%
            if ( (PracticeMaster.isRequired("OfficeAfterHoursCloseMonday",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeAfterHoursCloseMonday")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeAfterHoursCloseMonday",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeAfterHoursCloseMonday",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                            <%
            if ((PracticeMaster.isWrite("OfficeAfterHoursCloseMonday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p> 
                                  <input maxlength="10" type=text size="8" name="OfficeAfterHoursCloseMonday" value="<%=PracticeMaster.getOfficeAfterHoursCloseMonday()%>">
            &nbsp;
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeAfterHoursCloseMonday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeAfterHoursCloseMonday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else if ((PracticeMaster.isRead("OfficeAfterHoursCloseMonday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p><%=PracticeMaster.getOfficeAfterHoursCloseMonday()%>&nbsp;
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeAfterHoursCloseMonday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeAfterHoursCloseMonday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else
            {
                        %>
                            <%
            }
            %>
                            <%
            if ( (PracticeMaster.isRequired("OfficeAfterHoursCloseTuesday",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeAfterHoursCloseTuesday")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeAfterHoursCloseTuesday",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeAfterHoursCloseTuesday",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                            <%
            if ((PracticeMaster.isWrite("OfficeAfterHoursCloseTuesday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p> 
                                  <input maxlength="10" type=text size="8" name="OfficeAfterHoursCloseTuesday" value="<%=PracticeMaster.getOfficeAfterHoursCloseTuesday()%>">
            &nbsp;
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeAfterHoursCloseTuesday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeAfterHoursCloseTuesday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else if ((PracticeMaster.isRead("OfficeAfterHoursCloseTuesday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p><%=PracticeMaster.getOfficeAfterHoursCloseTuesday()%>&nbsp;
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeAfterHoursCloseTuesday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeAfterHoursCloseTuesday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else
            {
                        %>
                            <%
            }
            %>
                            <%
            if ( (PracticeMaster.isRequired("OfficeAfterHoursCloseWednesday",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeAfterHoursCloseWednesday")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeAfterHoursCloseWednesday",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeAfterHoursCloseWednesday",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                            <%
            if ((PracticeMaster.isWrite("OfficeAfterHoursCloseWednesday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p> 
                                  <input maxlength="10" type=text size="8" name="OfficeAfterHoursCloseWednesday" value="<%=PracticeMaster.getOfficeAfterHoursCloseWednesday()%>">
            &nbsp;
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeAfterHoursCloseWednesday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeAfterHoursCloseWednesday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else if ((PracticeMaster.isRead("OfficeAfterHoursCloseWednesday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p><%=PracticeMaster.getOfficeAfterHoursCloseWednesday()%>&nbsp;
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeAfterHoursCloseWednesday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeAfterHoursCloseWednesday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else
            {
                        %>
                            <%
            }
            %>
                            <%
            if ( (PracticeMaster.isRequired("OfficeAfterHoursCloseThursday",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeAfterHoursCloseThursday")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeAfterHoursCloseThursday",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeAfterHoursCloseThursday",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                            <%
            if ((PracticeMaster.isWrite("OfficeAfterHoursCloseThursday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p> 
                                  <input maxlength="10" type=text size="8" name="OfficeAfterHoursCloseThursday" value="<%=PracticeMaster.getOfficeAfterHoursCloseThursday()%>">
            &nbsp;
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeAfterHoursCloseThursday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeAfterHoursCloseThursday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else if ((PracticeMaster.isRead("OfficeAfterHoursCloseThursday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p><%=PracticeMaster.getOfficeAfterHoursCloseThursday()%>&nbsp;
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeAfterHoursCloseThursday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeAfterHoursCloseThursday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else
            {
                        %>
                            <%
            }
            %>
                            <%
            if ( (PracticeMaster.isRequired("OfficeAfterHoursCloseFriday",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeAfterHoursCloseFriday")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeAfterHoursCloseFriday",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeAfterHoursCloseFriday",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                            <%
            if ((PracticeMaster.isWrite("OfficeAfterHoursCloseFriday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p> 
                                  <input maxlength="10" type=text size="8" name="OfficeAfterHoursCloseFriday" value="<%=PracticeMaster.getOfficeAfterHoursCloseFriday()%>">
            &nbsp;
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeAfterHoursCloseFriday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeAfterHoursCloseFriday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else if ((PracticeMaster.isRead("OfficeAfterHoursCloseFriday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p><%=PracticeMaster.getOfficeAfterHoursCloseFriday()%>&nbsp;
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeAfterHoursCloseFriday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeAfterHoursCloseFriday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else
            {
                        %>
                            <%
            }
            %>
                            <%
            if ( (PracticeMaster.isRequired("OfficeAfterHoursCloseSaturday",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeAfterHoursCloseSaturday")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeAfterHoursCloseSaturday",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeAfterHoursCloseSaturday",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                            <%
            if ((PracticeMaster.isWrite("OfficeAfterHoursCloseSaturday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p> 
                                  <input maxlength="10" type=text size="8" name="OfficeAfterHoursCloseSaturday" value="<%=PracticeMaster.getOfficeAfterHoursCloseSaturday()%>">
            &nbsp;
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeAfterHoursCloseSaturday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeAfterHoursCloseSaturday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else if ((PracticeMaster.isRead("OfficeAfterHoursCloseSaturday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p><%=PracticeMaster.getOfficeAfterHoursCloseSaturday()%>&nbsp;
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeAfterHoursCloseSaturday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeAfterHoursCloseSaturday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else
            {
                        %>
                            <%
            }
            %>
                            <%
            if ( (PracticeMaster.isRequired("OfficeAfterHoursCloseSunday",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeAfterHoursCloseSunday")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeAfterHoursCloseSunday",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeAfterHoursCloseSunday",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                            <%
            if ((PracticeMaster.isWrite("OfficeAfterHoursCloseSunday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p> 
                                  <input maxlength="10" type=text size="8" name="OfficeAfterHoursCloseSunday" value="<%=PracticeMaster.getOfficeAfterHoursCloseSunday()%>">
            &nbsp;
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeAfterHoursCloseSunday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeAfterHoursCloseSunday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else if ((PracticeMaster.isRead("OfficeAfterHoursCloseSunday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p><%=PracticeMaster.getOfficeAfterHoursCloseSunday()%>&nbsp;
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeAfterHoursCloseSunday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeAfterHoursCloseSunday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else
            {
                        %>
                            <%
            }
            %>
                          </table>
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
                <tr> 
                  <td colspan=2> 
                    <hr noshade>
                  </td>
                </tr>
                <%
            if ( (PracticeMaster.isRequired("Comments",UserSecurityGroupID))&&(!PracticeMaster.isComplete("Comments")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("Comments",expiredDays))&&(PracticeMaster.isExpiredCheck("Comments",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if ((PracticeMaster.isWrite("Comments",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b><%=PracticeMaster.getEnglish("Comments")%>&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <textarea onKeyDown="textAreaStop(this,200)" rows="2" name="Comments" cols="40" maxlength=200><%=PracticeMaster.getComments()%></textarea>
&nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Comments&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("Comments")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((PracticeMaster.isRead("Comments",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top> 
                    <p class=<%=theClass%> ><b><%=PracticeMaster.getEnglish("Comments")%>&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p><%=PracticeMaster.getComments()%>&nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Comments&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("Comments")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
              </table>
              <%if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {%>
              <input type=hidden name=routePageReference value="sParentReturnPage">
              <p> 
                <input <%=HTMLFormStyleButton%> type=Submit value=Continue name=Submit>
              </p>
              <%}%>
            </td>
          </tr>
        </table>
      </form>
      <%
  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>
    </td>
  </tr>
</table>
<%String bnIncludeFN = "ui_"+thePLCID+"\\bot-nav_PhysicianID.jsp";
%>
<jsp:include page="<%=bnIncludeFN%>" flush="true" > 
<jsp:param name="plcID" value="<%=thePLCID%>"/>
</jsp:include>
