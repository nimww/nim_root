<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.bltDocumentManagement,java.util.Vector,java.util.Enumeration,com.winstaff.ListElement, com.winstaff.PLCUtils, com.winstaff.SecurityCheck, com.winstaff.ConfigurationMessages,com.winstaff.bltPeerReference,com.winstaff.bltPeerReference_List" %>
<%/*
    filename: tPeerReference_main_PeerReference_PhysicianID.jsp
    Created on Mar/21/2003
    Type: 1-n main class file
    Created by: Scott Ellis
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>

<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_PhysicianID.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>

    <table cellpadding=0 cellspacing=0 border=0 width=<%=MasterTableWidth%> >
    <tr><td width=10>&nbsp;</td><td>
    <%=ConfigurationMessages.getHTML("INTERVIEWTopControl","tPeerReference")%>

    <br><a href="tPeerReference_main_PeerReference_PhysicianID.jsp">Compact</a>

<%
//initial declaration of list class and parentID
    Integer        iPhysicianID        =    null;
    boolean accessValid = false;
   Integer iSecurityCheck = SecurityCheck.CheckItem("PractitionerSection11", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {
    if (pageControllerHash.containsKey("iPhysicianID")) 
    {
        iPhysicianID        =    (Integer)pageControllerHash.get("iPhysicianID");
        accessValid = true;
    }
  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);
      pageControllerHash.put("sParentReturnPage","tPeerReference_main_PeerReference_PhysicianID_Expand.jsp");
    pageControllerHash.remove("iReferenceID");
    session.setAttribute("pageControllerHash",pageControllerHash);

    bltPeerReference_List        bltPeerReference_List        =    new    bltPeerReference_List(iPhysicianID);

//declaration of Enumeration
    bltPeerReference        working_bltPeerReference;
    ListElement         leCurrentElement;
    Enumeration eList = bltPeerReference_List.elements();
    %>
        <%@ include file="tPeerReference_main_PeerReference_PhysicianID_instructions.jsp" %>

        <%if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {%>
        <a class=linkBase href = "tPeerReference_main_PeerReference_PhysicianID_form_create.jsp?EDIT=new&KM=p"><img border=0 src="ui_<%=thePLCID%>/icons/create_PhysicianID.gif"></a>
        <%}%>
    <%
    while (eList.hasMoreElements())
    {
         %>
         <table border="0" bordercolor="333333" cellpadding="3" class=tdHeaderAlt cellspacing="0" width="100%">
            <tr>
              <td width="50%">
                   <table border="0" bordercolor="333333" cellpadding="3" class=tdHeaderAlt cellspacing="0" width="100%">
                     <tr> 
                       <td>
         <%

        leCurrentElement    = (ListElement) eList.nextElement();
        working_bltPeerReference  = (bltPeerReference) leCurrentElement.getObject();
        working_bltPeerReference.GroupSecurityInit(UserSecurityGroupID);
        String theClass = "tdHeader";
        if (!working_bltPeerReference.isComplete())
        {
            theClass = "incompleteItem";
        %>
                <span class=incompleteItem><b>Not Complete</b></span><br>
        <%
        }
        %>
               <span class=<%=theClass%> ><b>ID:&nbsp;</b><%=working_bltPeerReference.getReferenceID()%> </span>
                  </td></tr>
                  <tr><td><b>Item Create Date:&nbsp;</b><jsp:include page="../generic/DateTypeConvert_Main.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(working_bltPeerReference.getUniqueCreateDate())%>" /></jsp:include></td></tr>
                  <tr><td><b>Item Modify Date:&nbsp;</b><jsp:include page="../generic/DateTypeConvert_Main.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(working_bltPeerReference.getUniqueModifyDate())%>" /></jsp:include></td></tr>
                  <tr><td><b>Item Modify Comments:&nbsp;</b><%=working_bltPeerReference.getUniqueModifyComments()%></td></tr>
                  </td></tr></table>
            </td><td width="50%" bgColor=#ffffff> 
        <a class=linkBase href = "tPeerReference_main_PeerReference_PhysicianID_form_authorize.jsp?EDIT=edit&EDITID=<%=working_bltPeerReference.getReferenceID()%>&KM=p"><img border=0 src="ui_<%=thePLCID%>/icons/edit_PhysicianID.gif"></a>


        <%if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {%>
        <br><a class=linkBase  onClick="return confirmDelete()"  href = "tPeerReference_main_PeerReference_PhysicianID_form_authorize.jsp?EDIT=del&EDITID=<%=working_bltPeerReference.getReferenceID()%>&KM=p"><img border=0 src="ui_<%=thePLCID%>/icons/delete_PhysicianID.gif"></a>
        <br>
        <% 
            if (working_bltPeerReference.getDocuLinkID().intValue()>0)
            {
              bltDocumentManagement myDoc = new bltDocumentManagement(working_bltPeerReference.getDocuLinkID());
              if (!myDoc.getDocumentFileName().equalsIgnoreCase(""))
              {
              %>
                      <a class=linkBase target=_blank href = "pdf/<%=myDoc.getDocumentFileName()%>">View Document</a>

              <%
              }
              else
              {
              %>

                      <a class=linkBase target=_blank href = "#" onClick="window.open('tDocumentManagement_main_DocumentManagement_PhysicianID_form_authorize.jsp?EDIT=faxCover&amp;EDITID=<%=working_bltPeerReference.getDocuLinkID()%>','DocPDF','toolbar=yes,location=yes,status=yes,menubar=yes,scrollbars=yes,resizable=yes,width=500,height=500');return false;" >Print Cover Sheet</a>

              <%
              }
            }
            else
            {
             if (working_bltPeerReference.isComplete())
             {
               %>
                      <a target=_blank class=linkBase href = "tPeerReference_ModifyDocument.jsp?EDITID=<%=working_bltPeerReference.getReferenceID()%>&EDIT=CREATE&dType=tPeerReference">Create Document</a>
               <%
             }
             else
             {
               %>
                      <span class=tdBase>You must complete this item before you can attach a document</span>
               <%
             }
            }
            %>

        <%}%>
                  </td></tr>
                 </table>
                 <table width="100%" border="1" cellspacing="0" bordercolor="#333333">
                  <tr>
                   <td>
                     <table width="100%" border="0" cellspacing="0">
                     <tr><td>
<%String theClassF = "textBase";%>

<%theClassF = "textBase";%>
<%if ((working_bltPeerReference.isExpired("FirstName",expiredDays))&&(working_bltPeerReference.isExpiredCheck("FirstName"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPeerReference.isRequired("FirstName"))&&(!working_bltPeerReference.isComplete("FirstName")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>First Name:&nbsp;</b><%=working_bltPeerReference.getFirstName()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltPeerReference.isExpired("LastName",expiredDays))&&(working_bltPeerReference.isExpiredCheck("LastName"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPeerReference.isRequired("LastName"))&&(!working_bltPeerReference.isComplete("LastName")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Last Name:&nbsp;</b><%=working_bltPeerReference.getLastName()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltPeerReference.isExpired("Address1",expiredDays))&&(working_bltPeerReference.isExpiredCheck("Address1"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPeerReference.isRequired("Address1"))&&(!working_bltPeerReference.isComplete("Address1")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Address:&nbsp;</b><%=working_bltPeerReference.getAddress1()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltPeerReference.isExpired("Address2",expiredDays))&&(working_bltPeerReference.isExpiredCheck("Address2"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPeerReference.isRequired("Address2"))&&(!working_bltPeerReference.isComplete("Address2")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Address 2:&nbsp;</b><%=working_bltPeerReference.getAddress2()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltPeerReference.isExpired("City",expiredDays))&&(working_bltPeerReference.isExpiredCheck("City"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPeerReference.isRequired("City"))&&(!working_bltPeerReference.isComplete("City")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>City:&nbsp;</b><%=working_bltPeerReference.getCity()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltPeerReference.isExpired("StateID",expiredDays))&&(working_bltPeerReference.isExpiredCheck("StateID"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPeerReference.isRequired("StateID"))&&(!working_bltPeerReference.isComplete("StateID")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>State:&nbsp;</b><jsp:include page="../generic/tStateLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltPeerReference.getStateID()%>" /></jsp:include></p>

<%theClassF = "textBase";%>
<%if ((working_bltPeerReference.isExpired("Province",expiredDays))&&(working_bltPeerReference.isExpiredCheck("Province"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPeerReference.isRequired("Province"))&&(!working_bltPeerReference.isComplete("Province")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Province, District, State:&nbsp;</b><%=working_bltPeerReference.getProvince()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltPeerReference.isExpired("ZIP",expiredDays))&&(working_bltPeerReference.isExpiredCheck("ZIP"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPeerReference.isRequired("ZIP"))&&(!working_bltPeerReference.isComplete("ZIP")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>ZIP:&nbsp;</b><%=working_bltPeerReference.getZIP()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltPeerReference.isExpired("CountryID",expiredDays))&&(working_bltPeerReference.isExpiredCheck("CountryID"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPeerReference.isRequired("CountryID"))&&(!working_bltPeerReference.isComplete("CountryID")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Country:&nbsp;</b><jsp:include page="../generic/tCountryLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltPeerReference.getCountryID()%>" /></jsp:include></p>

<%theClassF = "textBase";%>
<%if ((working_bltPeerReference.isExpired("Phone",expiredDays))&&(working_bltPeerReference.isExpiredCheck("Phone"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPeerReference.isRequired("Phone"))&&(!working_bltPeerReference.isComplete("Phone")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Phone (XXX-XXX-XXXX):&nbsp;</b><%=working_bltPeerReference.getPhone()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltPeerReference.isExpired("Fax",expiredDays))&&(working_bltPeerReference.isExpiredCheck("Fax"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPeerReference.isRequired("Fax"))&&(!working_bltPeerReference.isComplete("Fax")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Fax (XXX-XXX-XXXX):&nbsp;</b><%=working_bltPeerReference.getFax()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltPeerReference.isExpired("ContactEmail",expiredDays))&&(working_bltPeerReference.isExpiredCheck("ContactEmail"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPeerReference.isRequired("ContactEmail"))&&(!working_bltPeerReference.isComplete("ContactEmail")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Contact E-mail:&nbsp;</b><%=working_bltPeerReference.getContactEmail()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltPeerReference.isExpired("YearsAssociated",expiredDays))&&(working_bltPeerReference.isExpiredCheck("YearsAssociated"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPeerReference.isRequired("YearsAssociated"))&&(!working_bltPeerReference.isComplete("YearsAssociated")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Years associated:&nbsp;</b><%=working_bltPeerReference.getYearsAssociated()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltPeerReference.isExpired("DocuLinkID",expiredDays))&&(working_bltPeerReference.isExpiredCheck("DocuLinkID"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPeerReference.isRequired("DocuLinkID"))&&(!working_bltPeerReference.isComplete("DocuLinkID")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>DocuLink:&nbsp;</b><%=working_bltPeerReference.getDocuLinkID()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltPeerReference.isExpired("Comments",expiredDays))&&(working_bltPeerReference.isExpiredCheck("Comments"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPeerReference.isRequired("Comments"))&&(!working_bltPeerReference.isComplete("Comments")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Extra Comments:&nbsp;</b><%=working_bltPeerReference.getComments()%></p>

        </td></tr></table></table><br>        <%
    }
    %>
    </table>
    <%
  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>

    </td></tr></table>


<%String bnIncludeFN = "ui_"+thePLCID+"\\bot-nav_PhysicianID.jsp";
%>
<jsp:include page="<%=bnIncludeFN%>" flush="true" ><jsp:param name="plcID" value="<%=thePLCID%>"/></jsp:include>
