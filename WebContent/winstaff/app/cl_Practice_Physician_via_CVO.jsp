<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.*, java.util.Enumeration" %>
<%/*
    filename: out\jsp\cl_Practice_Physician_via_CVO.jsp
    Created on Feb/17/2003
    Type: cl main class file
    Created by: Scott Ellis
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>

<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_PhysicianID.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %><table cellpadding=0 cellspacing=0 border=0 width=<%=MasterTableWidth%> > 
<tr> 
  <td width=10>&nbsp;</td>
  <td> <%=ConfigurationMessages.getHTML("INTERVIEWTopControl_form","Add_Practice")%> 
    <br>
    <a href="tPhysicianPracticeLU_PhysicianID.jsp">return</a> 
    <%
//initial declaration of list class and parentID
    Integer        iPhysicianID        =    null;
    Integer        iCVOID        =    null;
    boolean accessValid = false;
   Integer iSecurityCheck = SecurityCheck.CheckItem("Practice1", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {
    if ((pageControllerHash.containsKey("iPhysicianID"))&&(pageControllerHash.containsKey("iCVOID"))) 
    {
        iPhysicianID        =    (Integer)pageControllerHash.get("iPhysicianID");
        iCVOID        =    (Integer)pageControllerHash.get("iCVOID");
        accessValid = true;
    }
  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);
      java.text.SimpleDateFormat displayDateLongSDF = new java.text.SimpleDateFormat("MMMM dd yyyy");

%>
    <br>
    <br>
    <table width="100%" border="1" cellspacing="0" cellpadding="0" bordercolor="#333333">
      <tr> 
        <td> 
          <table width="100%" border="0" cellspacing="0" cellpadding="0" bordercolor="#CCCCCC">
            <tr class=tdHeaderAlt> 
              <td width="50%" class=title> 
                <div align="center">Option 1</div>
              </td>
              <td width="10" bgcolor="#333333"> 
                <div align="center"></div>
              </td>
              <td width="50%" class=title> 
                <div align="center">Option 2</div>
              </td>
            </tr>
            <tr class=tdBase> 
              <td width="50%"> 
                <div align="center">Select an existing practice from the list 
                  below</div>
              </td>
              <td width="10" bgcolor="#333333">&nbsp;</td>
              <td width="50%"> 
                <div align="center">Create a new practice<br>
                  <a href="tPhysicianPracticeLU_PhysicianID_form_create-PhysOnly.jsp"><img border=0 src="ui_<%=thePLCID%>/icons/create_PhysicianID.gif"></a> 
                </div>
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
    <p class=instructions>If you do not want to add a practice, <a href="tPhysicianPracticeLU_PhysicianID.jsp">click 
      here</a> to return.</p>
    <%

//		myRS = mySS.executeStatement("SELECT  tPracticeMaster.PracticeID FROM tPracticeMaster INNER JOIN tPhysicianPracticeLU ON tPhysicianPracticeLU.PracticeID = tPracticeMaster.PracticeID INNER JOIN tHCOPhysicianLU ON tPhysicianPracticeLU.PhysicianID = tHCOPhysicianLU.PhysicianID INNER JOIN  tCVOHCOLU ON tCVOHCOLU.HCOID = tHCOPhysicianLU.HCOID WHERE  (tCVOHCOLU.CVOID = "+iCVOID+") GROUP BY tPracticeMaster.PracticeID, tCVOHCOLU.CVOID ORDER BY tPracticeMaster.PracticeID");
//***

try
{

String myPracticeID = "";
String myPracticeName = "";
String myOfficeCity = "";
String myOfficeStateID = "0";
String myOfficeZip = "";
String orderBy = "firstname";
int startID = 0;
int maxResults = 0;

boolean firstDisplay = true;

try
{
maxResults = Integer.parseInt(request.getParameter("maxResults"));
startID = Integer.parseInt(request.getParameter("startID"));
myPracticeID = request.getParameter("PracticeID");
myPracticeName = request.getParameter("PracticeName");
myOfficeCity = request.getParameter("OfficeCity");
myOfficeStateID = request.getParameter("OfficeStateID");
myOfficeZip = request.getParameter("OfficeZip");
orderBy = request.getParameter("orderBy");
firstDisplay = false;
maxResults = 50;
}
catch (Exception e)
{
maxResults = 0;
startID = 0;
firstDisplay = true;
myPracticeID = "";
myPracticeName = "";
myOfficeCity = "";
myOfficeStateID = "0";
myOfficeZip = "";
orderBy = "PracticeName";
}
if (orderBy == null)
{
maxResults = 0;
startID = 0;
firstDisplay = true;
myPracticeID = "";
myPracticeName = "";
myOfficeCity = "";
myOfficeStateID = "0";
myOfficeZip = "";
orderBy = "PracticeName";
}


if (false)
{
}
else
{

String myWhere = "where  ( tCVOHCOLU.CVOID = "+iCVOID+" and tPracticeMaster.PracticeID>0";

boolean theFirst = false;

try
{
if (!myPracticeID.equalsIgnoreCase(""))
{
if (!theFirst) { myWhere+=" and ";}
myWhere += "tPracticeMaster.PracticeID LIKE '%" + myPracticeID +"%'";
theFirst = false;
}
if (!myPracticeName.equalsIgnoreCase(""))
{
	if (!theFirst) { myWhere+=" and ";}
	if ((myPracticeName).indexOf("%")>=0)
	{
	  myWhere += "tPracticeMaster.PracticeName LIKE '" + DataControlUtils.fixApostrophe(myPracticeName) +"'";
	}
	else if (myPracticeName.indexOf("=")==0)
	{
	  myWhere += "tPracticeMaster.PracticeName = '" + DataControlUtils.fixApostrophe(myPracticeName).substring(1,myPracticeName.length()) +"'";
	}
	else if (myPracticeName.length()<=3)
	{
	  myWhere += "tPracticeMaster.PracticeName LIKE '" + DataControlUtils.fixApostrophe(myPracticeName) +"%'";
	}
	else
	{
	  myWhere += "tPracticeMaster.PracticeName LIKE '%" + DataControlUtils.fixApostrophe(myPracticeName) +"%'";
	}
	theFirst = false;
}
if (!myOfficeCity.equalsIgnoreCase(""))
{
	if (!theFirst) { myWhere+=" and ";}
	if ((myOfficeCity).indexOf("%")>=0)
	{
	  myWhere += "tPracticeMaster.OfficeCity LIKE '" + DataControlUtils.fixApostrophe(myOfficeCity) +"'";
	}
	else if (myOfficeCity.indexOf("=")==0)
	{
	  myWhere += "tPracticeMaster.OfficeCity = '" + DataControlUtils.fixApostrophe(myOfficeCity).substring(1,myOfficeCity.length()) +"'";
	}
	else if (myOfficeCity.length()<=3)
	{
	  myWhere += "tPracticeMaster.OfficeCity LIKE '" + DataControlUtils.fixApostrophe(myOfficeCity) +"%'";
	}
	else
	{
	  myWhere += "tPracticeMaster.OfficeCity LIKE '%" + DataControlUtils.fixApostrophe(myOfficeCity) +"%'";
	}
	theFirst = false;
}
if (!myOfficeStateID.equalsIgnoreCase("0"))
{
	if (!myOfficeStateID.equalsIgnoreCase(""))
	{
		if (!theFirst) { myWhere+=" and ";}
		myWhere += "tPracticeMaster.OfficeStateID = " + myOfficeStateID +"";
		theFirst = false;
	}
}
if (!myOfficeZip.equalsIgnoreCase(""))
{
	if (!theFirst) { myWhere+=" and ";}
	if ((myOfficeZip).indexOf("%")>=0)
	{
	  myWhere += "tPracticeMaster.OfficeZip LIKE '" + DataControlUtils.fixApostrophe(myOfficeZip) +"'";
	}
	else if (myOfficeZip.indexOf("=")==0)
	{
	  myWhere += "tPracticeMaster.OfficeZip = '" + DataControlUtils.fixApostrophe(myOfficeZip).substring(1,myOfficeZip.length()) +"'";
	}
	else if (myOfficeZip.length()<=3)
	{
	  myWhere += "tPracticeMaster.OfficeZip LIKE '" + DataControlUtils.fixApostrophe(myOfficeZip) +"%'";
	}
	else
	{
	  myWhere += "tPracticeMaster.OfficeZip LIKE '%" + DataControlUtils.fixApostrophe(myOfficeZip) +"%'";
	}
	theFirst = false;
}
myWhere += ")";
//myWhere += ") ))";

//System.out.println(myWhere);
if (theFirst||myWhere.equalsIgnoreCase(")"))
{
myWhere = "";
}


}
catch(Exception e)
{
out.println("FFF:"+e);
}


searchDB2 mySS = new searchDB2();

java.sql.ResultSet myRS = null;;

try
{
myRS = mySS.executeStatement("SELECT  tPracticeMaster.PracticeID, tPracticeMaster.PracticeName, tPracticeMaster.OfficeCity, tPracticeMaster.OfficeStateID, tPracticeMaster.OfficeZIP FROM tPracticeMaster INNER JOIN tPhysicianPracticeLU ON tPhysicianPracticeLU.PracticeID = tPracticeMaster.PracticeID INNER JOIN tHCOPhysicianLU ON tPhysicianPracticeLU.PhysicianID = tHCOPhysicianLU.PhysicianID INNER JOIN  tCVOHCOLU ON tCVOHCOLU.HCOID = tHCOPhysicianLU.HCOID "+ myWhere +" GROUP BY tPracticeMaster.PracticeID, tPracticeMaster.PracticeName, tPracticeMaster.OfficeCity, tPracticeMaster.OfficeStateID, tPracticeMaster.OfficeZIP, tCVOHCOLU.CVOID order by tPracticeMaster." + orderBy);

}
catch(Exception e)
{
out.println("ResultsSet:"+e);
}

String myMainTable= " ";
try{

int endCount = 0;

int cnt=0;
int cnt2=0;
   while (myRS!=null&&myRS.next()&&(cnt<=startID+maxResults))
   {







cnt++;
if (cnt>=startID&&cnt<=startID+maxResults)
{
cnt2++;

String myClass = "tdBase";
if (cnt2%2!=0)
{
myClass = "tdBaseAlt";
}
bltPracticeMaster pm = new bltPracticeMaster(new Integer(myRS.getString("PracticeID")));

String pmPracticeID = myRS.getString("PracticeID");
String pmPracticeName = myRS.getString("PracticeName");
String pmOfficeCity = myRS.getString("OfficeCity");
Integer pmOfficeStateID  = new Integer( myRS.getString("OfficeStateID"));
String pmOfficeZIP = myRS.getString("OfficeZIP");

myMainTable +="<tr class="+myClass+">";
myMainTable +="<td>"+cnt+"</td>";
if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
{
	bltPhysicianPracticeLU_List_LU_PhysicianID        bltPhysicianPracticeLU_List_LU_PhysicianID        =    new    bltPhysicianPracticeLU_List_LU_PhysicianID(iPhysicianID,"PracticeID="+pmPracticeID,"");
	java.util.Enumeration eList2 = bltPhysicianPracticeLU_List_LU_PhysicianID.elements();
	if (eList2.hasMoreElements())
	{
		myMainTable +="<td>Already Linked</td>";
	}
	else
	{
		myMainTable +="<td><a href = \"cl_Practice_Physician_via_CVO_create.jsp?EDITID="+pmPracticeID+"\"><img border=0 src=\"ui_"+thePLCID+"/icons/add_PhysicianID.gif\"></a>  </td>";
	}
}
myMainTable +="<td>";
myMainTable +=pmPracticeID+"";
myMainTable +="</td>";
myMainTable +="<td>";

myMainTable +="<b>" + pm.getPracticeName()+"</b><br>" + pm.getOfficeAddress1() + "<br>"+pm.getOfficeAddress2() + "<br>";
myMainTable +="</td>";
myMainTable +="<td>";
myMainTable +=pmOfficeCity+"";
myMainTable +="</td>";
myMainTable +="<td>";
myMainTable +=(new bltStateLI (pmOfficeStateID ).getShortState()+"");
myMainTable +="</td>";
myMainTable +="<td>";
myMainTable +=pmOfficeZIP+"";
myMainTable +="</td>";
myMainTable +="</tr>";
}
   }
mySS.closeAll();
endCount = cnt;



if (startID>=endCount)
{
startID = endCount-maxResults;

}

if (maxResults<=0)
{
//maxResults=10;
}


if (startID<=0)
{
startID=0;
}



%>
    <script language="JavaScript">
<!--
function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}
//-->
</script></p>
    <table width=100% border=0 cellpadding=0 cellspacing=0 bordercolor=#003333>
      <tr> 
        <td> 
          <table width=100% border=1 cellpadding=2 cellspacing=0 bordercolor=#003333>
            <tr> 
              <td> <form name="selfForm" method="get" action="cl_Practice_Physician_via_CVO.jsp"> 
                <table border=0 cellspacing=1 width='100%' cellpadding=2>
                  <tr > 
                    <td colspan=2 class=tdHeader>
                      <p>Enter Search Criteria:</p>
                    </td>
                    <td colspan=1 class=tdBase align=right nowrap> 
                      <%if (startID>0)
{%>
                      <a href="cl_Practice_Physician_via_CVO.jsp?startID=<%=(startID-maxResults)%>&maxResults=<%=maxResults%>&orderBy=<%=orderBy%>&PracticeID=<%=myPracticeID%>&PracticeName=<%=myPracticeName%>&OfficeCity=<%=myOfficeCity%>&OfficeStateID=<%=myOfficeStateID%>&OfficeZip=<%=myOfficeZip%>"><< 
                      previous <%=maxResults%></a> 
                      <%
}
else
{
%>
                      <p>&nbsp;</p>
                      <%
}
%>
                    </td>
                    <td colspan=1 class=tdBase nowrap> 
                      <%
if (maxResults>0&&(startID+maxResults)<endCount)
{
%>
                      <a href="cl_Practice_Physician_via_CVO.jsp?startID=<%=(startID+maxResults)%>&maxResults=<%=maxResults%>&orderBy=<%=orderBy%>&PracticeID=<%=myPracticeID%>&PracticeName=<%=myPracticeName%>&OfficeCity=<%=myOfficeCity%>&OfficeStateID=<%=myOfficeStateID%>&OfficeZip=<%=myOfficeZip%>"> 
                      next >> <%=maxResults%></a> 
                      <%
}
else
{
%>
                      <p>&nbsp;</p>
                      <%
}


}
catch(Exception e)
{
out.println("PrevNext:"+e);
}




try{

%>
                    </td>
                    <td colspan=2 class=tdHeader nowrap> 
                      <input type=hidden name=maxResults value=<%=maxResults%> >
                      <input type=hidden name=startID value=0 >
                      <p>Sort: 
                        <select name=orderBy>
                          <option value=PracticeID <%if (orderBy.equalsIgnoreCase("PracticeID")){out.println(" selected");}%> >ID</option>
                          <option value=PracticeName <%if (orderBy.equalsIgnoreCase("PracticeName")){out.println(" selected");}%> >Practice 
                          Name</option>
                          <option value=OfficeCity <%if (orderBy.equalsIgnoreCase("OfficeCity")){out.println(" selected");}%> >City</option>
                          <option value=OfficeStateID <%if (orderBy.equalsIgnoreCase("OfficeStateID")){out.println(" selected");}%> >State</option>
                          <option value=OfficeZip <%if (orderBy.equalsIgnoreCase("OfficeZip")){out.println(" selected");}%> >Postal 
                          Code</option>
                        </select>
                      </p>
                    </td>
                    <td class=tdHeader colspan=1> 
                      <p  align="center"> 
                        <input type="Submit" name="Submit" value="Submit" align="middle">
                      </p>
                    </td>
                  </tr>
                  <tr class=tdHeader> 
                    <td  colspan=1>&nbsp; </td>
                    <td  colspan=1>&nbsp; </td>
                    <td colspan=1> 
                      <input type="text" name="PracticeID" value="<%=myPracticeID%>" size="5">
                    </td>
                    <td colspan=1> 
                      <input type="text" name="PracticeName" value="<%=myPracticeName%>" size="20">
                    </td>
                    <td colspan=1> 
                      <input type="text" name="OfficeCity" value="<%=myOfficeCity%>" size="20">
                    </td>
                    <td colspan=1> 
                      <select name="OfficeStateID">
                        <jsp:include page="../generic/tStateLIShort.jsp" flush="true" > 
                        <jsp:param name="CurrentSelection" value="<%=myOfficeStateID%>" />
                        </jsp:include>
                      </select>
                    </td>
                    <td colspan=1> 
                      <input type="text" name="OfficeZip" value="<%=myOfficeZip%>" size="10">
                    </td>
                  </tr>
                  <tr class=tdHeader> 
                    <td>#</td>
                    <td>Action</td>
                    <td colspan=1> ID </td>
                    <td colspan=1> Practice Name </td>
                    <td colspan=1> City </td>
                    <td colspan=1> State </td>
                    <td colspan=1> Postal Code </td>
                  </tr>
                  <%=myMainTable%> 
                </table>
                <%

}
catch(Exception e)
{
out.println("Display:"+e);
}





}
}
catch (Exception e)
{
out.println("Error???:"+e);
System.out.println("Error:"+e);
}











//***







    %>
          </table>
          <br>
          <%

  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>
        </td>
      </tr>
    </table>
    <%String bnIncludeFN = "ui_"+thePLCID+"\\bot-nav_PhysicianID.jsp";
%>
    <jsp:include page="<%=bnIncludeFN%>" flush="true" > 
    <jsp:param name="plcID" value="<%=thePLCID%>"/>
    </jsp:include>
